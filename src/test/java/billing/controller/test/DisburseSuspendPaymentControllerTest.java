package billing.controller.test;

import static core.utils.CommonConstants.SHORT_ONE;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;

import billing.controller.DisburseSuspendedPaymentController;
import billing.model.PayableOption;
import billing.service.DisbursementSuspendPaymentService;
import billing.validation.DisbursementSuspendPaymentValidator;
import core.api.ExecContext;
import core.exception.validation.InvalidDataException;
import core.security.service.SecurityService;
import core.utils.CommonConstants;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "classpath:test-servlet-context.xml",
        "classpath:test-applicationContext-persistence.xml", "classpath:test-root-context.xml" })
@WebAppConfiguration
public class DisburseSuspendPaymentControllerTest {

    @Autowired
    @InjectMocks
    private DisburseSuspendedPaymentController disbursementControllerTest;

    @Mock
    private DisbursementSuspendPaymentService disbursementServiceMock;

    @Mock
    private PayableOption payableOptionMock;

    @Mock
    private SecurityService securityServiceMock;

    @Mock
    private DisbursementSuspendPaymentValidator disbursementValidatorMock;

    @Autowired
    ExecContext execContext;

    @BeforeAll
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterAll
    public static void tearDownAfterClass() throws Exception {
    }

    private static final String POST_DISBURSEMENT_ACTIVITY = "postDisbursement";

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.reset(disbursementServiceMock);
        disbursementValidatorMock = new DisbursementSuspendPaymentValidator();
        payableOptionMock = createPayableOptionMock();

    }

    private PayableOption createPayableOptionMock() {
        PayableOption payableOptionMock = new PayableOption();
        payableOptionMock.setPayeeId("MRR7MM2MNLB71WEBSRVC");
        payableOptionMock.setCheckNumber(12);
        payableOptionMock.setReason("Refund-Credit");
        payableOptionMock.setRefundMethod("Manual Check");
        payableOptionMock.setAddressSequenceNumber(SHORT_ONE);
        return payableOptionMock;
    }

    @Test
    public void test_validateRefundMethod_fail() {
        try {
            PayableOption payableOptionMock = createPayableOptionMock();
            payableOptionMock.setRefundMethod(null);
            doNothing().when(securityServiceMock).authenticateFunction(POST_DISBURSEMENT_ACTIVITY);
            BindException errors = new BindException(payableOptionMock, "postPayableOptionMock");
            ValidationUtils.invokeValidator(disbursementValidatorMock, payableOptionMock, errors);
            assertTrue(errors.hasErrors());
            assertNotNull(errors.getFieldError("refundMethod"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_validateReason_fail() {
        try {
            PayableOption payableOptionMock = createPayableOptionMock();
            payableOptionMock.setReason(null);
            doNothing().when(securityServiceMock).authenticateFunction(POST_DISBURSEMENT_ACTIVITY);
            BindException errors = new BindException(payableOptionMock, "postPayableOptionMock");
            ValidationUtils.invokeValidator(disbursementValidatorMock, payableOptionMock, errors);
            assertTrue(errors.hasErrors());
            assertNotNull(errors.getFieldError("reason"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_validateCheckNumber_fail() {
        try {
            PayableOption payableOptionMock = createPayableOptionMock();
            payableOptionMock.setCheckNumber(null);
            doNothing().when(securityServiceMock).authenticateFunction(POST_DISBURSEMENT_ACTIVITY);
            BindException errors = new BindException(payableOptionMock, "postPayableOptionMock");
            ValidationUtils.invokeValidator(disbursementValidatorMock, payableOptionMock, errors);
            assertTrue(errors.hasErrors());
            assertNotNull(errors.getFieldError("checkNumber"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_validatePayee_fail() {
        try {
            PayableOption payableOptionMock = createPayableOptionMock();
            payableOptionMock.setPayeeId(null);
            doNothing().when(securityServiceMock).authenticateFunction(POST_DISBURSEMENT_ACTIVITY);
            BindException errors = new BindException(payableOptionMock, "postPayableOptionMock");
            ValidationUtils.invokeValidator(disbursementValidatorMock, payableOptionMock, errors);
            assertTrue(errors.hasErrors());
            assertNotNull(errors.getFieldError("payeeId"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_validateDisbursement_fail() {
        try {
            PayableOption payableOptionMock = createPayableOptionMock();
            doNothing().when(securityServiceMock).authenticateFunction(POST_DISBURSEMENT_ACTIVITY);
            disbursementValidatorMock.setUser("1017");
            disbursementValidatorMock.setBatchId("BACT");
            disbursementValidatorMock.setDistributionDate("2015-12-22");
            disbursementValidatorMock.setSequenceNumber(CommonConstants.SHORT_ZERO);
            BindException errors = new BindException(payableOptionMock, "postPayableOptionMock");
            ValidationUtils.invokeValidator(disbursementValidatorMock, payableOptionMock, errors);
            assertTrue(errors.hasErrors());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_validateSaveDisbursement_success() {
        try {

            Short sequenceNumber = CommonConstants.SHORT_ZERO;
            ZonedDateTime distributionDate = ZonedDateTime.now();
            ZonedDateTime postedDate = ZonedDateTime.now();
            String user = "SE30700";
            String batchId = "BACT";
            PayableOption payableOptionMock = createPayableOptionMock();
            doNothing().when(securityServiceMock).authenticateFunction(POST_DISBURSEMENT_ACTIVITY);

            doNothing().when(disbursementServiceMock).save(sequenceNumber, postedDate, user, batchId, distributionDate,
                    payableOptionMock);
            verify(disbursementServiceMock, times(0)).save(sequenceNumber, postedDate, user, batchId, distributionDate,
                    payableOptionMock);
            verifyNoMoreInteractions(disbursementServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_validateSaveDisbursement_fail() {
        try {

            Short sequenceNumber = CommonConstants.SHORT_ZERO;
            ZonedDateTime distributionDate = ZonedDateTime.now();
            ZonedDateTime postedDate = ZonedDateTime.now();
            String user = "SE30700";
            String batchId = "BACT";
            PayableOption payableOptionMock = createPayableOptionMock();
            payableOptionMock.setRefundMethod("");

            doNothing().when(securityServiceMock).authenticateFunction(POST_DISBURSEMENT_ACTIVITY);
            doThrow(new InvalidDataException("bad.request.data.invalid")).when(disbursementServiceMock)
                    .save(sequenceNumber, postedDate, user, batchId, distributionDate, payableOptionMock);

            verify(disbursementServiceMock, times(0)).save(sequenceNumber, postedDate, user, batchId, distributionDate,
                    payableOptionMock);
            verifyNoMoreInteractions(disbursementServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
