package billing.controller.test;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import billing.controller.PaymentController;
import billing.model.Payment;
import billing.model.PaymentList;
import billing.service.PaymentService;
import billing.validation.PaymentValidator;
import core.exception.ExceptionControllerHandler;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.interceptor.SecurityInterceptor;
import core.security.service.SecurityService;
import core.utils.DateRoutine;
import core.utils.TestUtil;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "classpath:test-servlet-context.xml",
        "classpath:test-applicationContext-persistence.xml", "classpath:test-root-context.xml" })
@WebAppConfiguration
public class PaymentControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private PaymentValidator paymentValidatorMock;

    @Autowired
    private ExceptionControllerHandler exceptionControllerHandler;

    @Autowired
    private SecurityInterceptor pb360SecurityInterceptor;

    @Mock
    private PaymentService paymentServiceMock;

    @Autowired
    @InjectMocks
    private PaymentController paymentControllerTest;

    private Payment paymentMock;

    private PaymentList paymentListMock;

    @Mock
    private SecurityService securityServiceMock;

    private static final String MAKE_PAYMENT = "createPayment";
    private static final String SEARCH_PAYMENT = "searchPayments";
    private static final String FIND_PAYMENT = "findPayment";

    @BeforeAll
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterAll
    public static void tearDownAfterClass() throws Exception {
    }

    @BeforeEach
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        Mockito.reset(paymentServiceMock);
        mockMvc = MockMvcBuilders.standaloneSetup(paymentControllerTest).setControllerAdvice(exceptionControllerHandler)
                .addInterceptors(pb360SecurityInterceptor).setValidator(paymentValidatorMock).build();

        paymentMock = createPaymentMock();
    }

    private Payment createPaymentMock() {
        paymentMock = new Payment();
        paymentMock.setPaymentMethod("Bank Account");
        paymentMock.setPostedDate(ZonedDateTime.now());
        paymentMock.setPaymentSequenceNumber((short) 0);
        paymentMock.setAccountType("Personal Savings");
        paymentMock.setPaymentType("Direct Check 1x ACH DC");
        paymentMock.setRoutingNumber("011000138");
        paymentMock.setBankAccountNumber("4121853519");
        paymentMock.setPaymentAmount(12.68);
        paymentMock.setIdentifierType("Group");
        paymentMock.setIdentifier("GROUP_NOT_PRESENT");
        paymentMock.setBatchId("KCOU");
        paymentMock.setUserId("1017");
        paymentMock.setIsRecurring(false);
        return paymentMock;
    }

    @AfterEach
    public void tearDown() throws Exception {
    }

    @Test
    public void test_makeAPayment_Group_success() {

        try {
            Payment savedPayment = new Payment();
            doNothing().when(securityServiceMock).authenticateFunction(MAKE_PAYMENT);
            savedPayment
                    .add(linkTo(methodOn(PaymentController.class).findPayment(paymentMock.getPaymentSequenceNumber(),
                            DateRoutine.dateTimeAsYYYYMMDDString(paymentMock.getPostedDate()), paymentMock.getUserId(),
                            paymentMock.getBatchId())).withRel("payment"));

            ResponseEntity<Payment> expectedResponseEntity = new ResponseEntity<Payment>(savedPayment,
                    HttpStatus.CREATED);

            when(paymentServiceMock.makeAPayment(paymentMock)).thenReturn(paymentMock);

            mockMvc.perform(post("/v1/billing/payments").contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").header("Accept", "application/json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentMock))).andExpect(status().isCreated())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.links[0].href",
                            is(expectedResponseEntity.getBody().getLink("payment").getHref())));

            verify(paymentServiceMock, times(1)).makeAPayment(paymentMock);
            verifyNoMoreInteractions(paymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_makeAPayment_Group_fail_paymentMethodIsBlank() {

        try {
            Payment invalidPaymentMock = createPaymentMock();
            invalidPaymentMock.setPaymentMethod("");
            doNothing().when(securityServiceMock).authenticateFunction(MAKE_PAYMENT);
            doThrow(new InvalidDataException("paymentMethod.empty")).when(paymentServiceMock).makeAPayment(paymentMock);

            mockMvc.perform(post("/v1/billing/payments").contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").header("Accept", "application/json")
                    .content(TestUtil.convertObjectToJsonBytes(invalidPaymentMock))).andExpect(status().isBadRequest())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.errors", hasSize(1)))
                    .andExpect(jsonPath("$.errors[0].field", is("paymentMethod")))
                    .andExpect(jsonPath("$.errors[0].message",
                            is("Bad Request-Your request is missing parameter - paymentMethod.")));

            verify(paymentServiceMock, times(0)).makeAPayment(paymentMock);
            verifyNoMoreInteractions(paymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_makeAPayment_Group_fail_paymentMethodIsNull() {

        try {
            Payment invalidPaymentMock = createPaymentMock();
            invalidPaymentMock.setPaymentMethod(null);
            doNothing().when(securityServiceMock).authenticateFunction(MAKE_PAYMENT);
            doThrow(new InvalidDataException("paymentMethod.empty")).when(paymentServiceMock).makeAPayment(paymentMock);

            mockMvc.perform(post("/v1/billing/payments").contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").header("Accept", "application/json")
                    .content(TestUtil.convertObjectToJsonBytes(invalidPaymentMock))).andExpect(status().isBadRequest())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.errors", hasSize(1)))
                    .andExpect(jsonPath("$.errors[0].field", is("paymentMethod")))
                    .andExpect(jsonPath("$.errors[0].message",
                            is("Bad Request-Your request is missing parameter - paymentMethod.")));

            verify(paymentServiceMock, times(0)).makeAPayment(paymentMock);
            verifyNoMoreInteractions(paymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_makeAPayment_Group_fail_paymentMethodIsInvalid() {

        try {
            Payment invalidPaymentMock = createPaymentMock();
            invalidPaymentMock.setPaymentMethod("PAY");
            doNothing().when(securityServiceMock).authenticateFunction(MAKE_PAYMENT);
            doThrow(new InvalidDataException("paymentMethod.invalid")).when(paymentServiceMock)
                    .makeAPayment(paymentMock);

            mockMvc.perform(post("/v1/billing/payments").contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").header("Accept", "application/json")
                    .content(TestUtil.convertObjectToJsonBytes(invalidPaymentMock))).andExpect(status().isBadRequest())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.errors", hasSize(2)))
                    .andExpect(jsonPath("$.errors[0].field", is("paymentMethod")))
                    .andExpect(jsonPath("$.errors[0].message",
                            is("Bad Request-Your request is having invalid data for - paymentMethod.")));

            verify(paymentServiceMock, times(0)).makeAPayment(paymentMock);
            verifyNoMoreInteractions(paymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_makeAPayment_Group_fail_paymentTypeIsBlank() {

        try {
            Payment invalidPaymentMock = createPaymentMock();
            invalidPaymentMock.setPaymentType("");
            doNothing().when(securityServiceMock).authenticateFunction(MAKE_PAYMENT);
            doThrow(new InvalidDataException("paymentType.empty")).when(paymentServiceMock).makeAPayment(paymentMock);

            mockMvc.perform(post("/v1/billing/payments").contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").header("Accept", "application/json")
                    .content(TestUtil.convertObjectToJsonBytes(invalidPaymentMock))).andExpect(status().isBadRequest())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.errors", hasSize(1)))
                    .andExpect(jsonPath("$.errors[0].field", is("paymentType"))).andExpect(jsonPath(
                            "$.errors[0].message", is("Bad Request-Your request is missing parameter - paymentType.")));

            verify(paymentServiceMock, times(0)).makeAPayment(paymentMock);
            verifyNoMoreInteractions(paymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_makeAPayment_Group_fail_paymentTypeIsNull() {

        try {
            Payment invalidPaymentMock = createPaymentMock();
            invalidPaymentMock.setPaymentType(null);
            doNothing().when(securityServiceMock).authenticateFunction(MAKE_PAYMENT);
            doThrow(new InvalidDataException("paymentType.empty")).when(paymentServiceMock).makeAPayment(paymentMock);

            mockMvc.perform(post("/v1/billing/payments").contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").header("Accept", "application/json")
                    .content(TestUtil.convertObjectToJsonBytes(invalidPaymentMock))).andExpect(status().isBadRequest())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.errors", hasSize(1)))
                    .andExpect(jsonPath("$.errors[0].field", is("paymentType"))).andExpect(jsonPath(
                            "$.errors[0].message", is("Bad Request-Your request is missing parameter - paymentType.")));

            verify(paymentServiceMock, times(0)).makeAPayment(paymentMock);
            verifyNoMoreInteractions(paymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_makeAPayment_Group_fail_paymentTypeIsInvalid() {

        try {
            Payment invalidPaymentMock = createPaymentMock();
            invalidPaymentMock.setPaymentType("TYP");
            doNothing().when(securityServiceMock).authenticateFunction(MAKE_PAYMENT);
            doThrow(new InvalidDataException("paymentType.invalid")).when(paymentServiceMock).makeAPayment(paymentMock);

            mockMvc.perform(post("/v1/billing/payments").contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").header("Accept", "application/json")
                    .content(TestUtil.convertObjectToJsonBytes(invalidPaymentMock))).andExpect(status().isBadRequest())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.errors", hasSize(1)))
                    .andExpect(jsonPath("$.errors[0].field", is("paymentType")))
                    .andExpect(jsonPath("$.errors[0].message",
                            is("Bad Request-Your request is having invalid data for - paymentType.")));

            verify(paymentServiceMock, times(0)).makeAPayment(paymentMock);
            verifyNoMoreInteractions(paymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_makeAPayment_Group_fail_paymentAmountIsInvalid() {

        try {
            Payment invalidPaymentMock = createPaymentMock();

            invalidPaymentMock.setPaymentAmount(-1.00d);
            doNothing().when(securityServiceMock).authenticateFunction(MAKE_PAYMENT);
            doThrow(new InvalidDataException("paymentAmount.invalid")).when(paymentServiceMock)
                    .makeAPayment(paymentMock);

            mockMvc.perform(post("/v1/billing/payments").contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").header("Accept", "application/json")
                    .content(TestUtil.convertObjectToJsonBytes(invalidPaymentMock))).andExpect(status().isBadRequest())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON));

            verify(paymentServiceMock, times(0)).makeAPayment(paymentMock);
            verifyNoMoreInteractions(paymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_getAccountPayments_fail() {
        try {
            int page = 5;
            int size = 10;
            String filters = null;

            doNothing().when(securityServiceMock).authenticateFunction(SEARCH_PAYMENT);

            when(paymentServiceMock.findPayments(page, size, filters)).thenReturn(paymentListMock);

            mockMvc.perform(MockMvcRequestBuilders.get("/v1/billing/payments").header("X-CSC-User-Id", "SE30700"))
                    .andExpect(status().isOk());

            verify(paymentServiceMock, times(0)).findPayments(page, size, filters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_getAccountPayment_success() {
        try {
            String batchId = "KCOU";
            short sequenceNumber = 0;
            String postedDate = "2029-09-10";

            doNothing().when(securityServiceMock).authenticateFunction(FIND_PAYMENT);
            when(paymentServiceMock.findPayment(batchId, sequenceNumber, postedDate, "1017")).thenReturn(paymentMock);

            mockMvc.perform(MockMvcRequestBuilders.get(
                    "/v1/billing/payments/{sequenceNumber}?postedDate={postedDate}&userId={userId}&batchId={batchId}",
                    sequenceNumber, postedDate, "1017", batchId).header("X-CSC-User-Id", "SE30700")
                    .header("Accept", "application/json")).andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON));

            verify(paymentServiceMock, times(1)).findPayment(batchId, sequenceNumber, postedDate, "1017");
            verifyNoMoreInteractions(paymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_getAccountPayment_fail() {
        try {
            String batchId = "XXXX";
            short sequenceNumber = 1;
            String postedDate = "2016-10-27";
            doNothing().when(securityServiceMock).authenticateFunction(FIND_PAYMENT);

            when(paymentServiceMock.findPayment(batchId, sequenceNumber, postedDate, "SE30700"))
                    .thenThrow(new DataNotFoundException("no.bil.account.exists", new Object[] { batchId }));

            mockMvc.perform(MockMvcRequestBuilders.get(
                    "/v1/billing/payments/{sequenceNumber}?postedDate={postedDate}&userId={userId}&batchId={batchId}",
                    sequenceNumber, postedDate, "SE30700", batchId).header("X-CSC-User-Id", "SE30700"))
                    .andExpect(status().isNotFound());

            verify(paymentServiceMock, times(1)).findPayment(batchId, sequenceNumber, postedDate, "SE30700");
            verifyNoMoreInteractions(paymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_getAccountPaymentPdf_success() {
        try {
            String batchId = "KCOU";
            short sequenceNumber = 0;
            String postedDate = "2029-09-10";

            doNothing().when(securityServiceMock).authenticateFunction(FIND_PAYMENT);
            when(paymentServiceMock.findPayment(batchId, sequenceNumber, postedDate, "1017")).thenReturn(paymentMock);

            mockMvc.perform(MockMvcRequestBuilders.get(
                    "/v1/billing/payments/{sequenceNumber}/pdf?postedDate={postedDate}&userId={userId}&batchId={batchId}",
                    sequenceNumber, postedDate, "1017", batchId).header("X-CSC-User-Id", "SE30700")
                    .header("Accept", "application/pdf")).andExpect(status().isOk())
                    .andExpect(content().contentType("application/pdf"));

            verify(paymentServiceMock, times(1)).findPayment(batchId, sequenceNumber, postedDate, "1017");
            verifyNoMoreInteractions(paymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
