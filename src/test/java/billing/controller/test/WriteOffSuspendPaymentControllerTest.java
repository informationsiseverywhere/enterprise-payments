package billing.controller.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;

import billing.controller.WriteOffSuspendedPaymentController;
import billing.model.WriteOffOption;
import billing.service.WriteOffSuspendPaymentService;
import billing.validation.WriteOffSuspendPaymentValidator;
import core.api.ExecContext;
import core.exception.ExceptionControllerHandler;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.interceptor.SecurityInterceptor;
import core.security.service.SecurityService;
import core.utils.CommonConstants;
import core.utils.DateRoutine;
import core.utils.TestUtil;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "classpath:test-servlet-context.xml",
        "classpath:test-applicationContext-persistence.xml", "classpath:test-root-context.xml" })
@WebAppConfiguration
public class WriteOffSuspendPaymentControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private ExceptionControllerHandler exceptionControllerHandler;

    @Autowired
    private SecurityInterceptor pb360SecurityInterceptor;

    @Mock
    private SecurityService securityServiceMock;

    @Autowired
    ExecContext execContext;

    @InjectMocks
    @Autowired
    private WriteOffSuspendedPaymentController writeOffControllerTest;

    @Mock
    private WriteOffSuspendPaymentService writeOffServiceMock;

    @Autowired
    private WriteOffSuspendPaymentValidator writeOffValidator;

    @Mock
    private WriteOffOption writeOffOptionMock;

    @BeforeAll
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterAll
    public static void tearDownAfterClass() throws Exception {
    }

    private static final String PAYMENT_WRITE_OFF = "postUnidentifiedWriteOff";

    @BeforeEach
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        Mockito.reset(writeOffServiceMock);
        mockMvc = MockMvcBuilders.standaloneSetup(writeOffControllerTest).addInterceptors(pb360SecurityInterceptor)
                .setControllerAdvice(exceptionControllerHandler).setValidator(writeOffValidator).build();

        writeOffOptionMock = createWriteOffOptionMock();

    }

    private WriteOffOption createWriteOffOptionMock() {
        WriteOffOption writeOffOption = new WriteOffOption();
        writeOffOption.setReason("Manual Payment Wrt-Off");
        return writeOffOption;
    }

    @Test
    public void test_postWriteOff_fail_reasonAsNull() {
        String reason = null;
        doNothing().when(securityServiceMock).authenticateFunction(PAYMENT_WRITE_OFF);

        try {
            writeOffOptionMock.setReason(reason);
            BindException errors = new BindException(writeOffOptionMock, "postWriteOffOptionMock");
            ValidationUtils.invokeValidator(writeOffValidator, writeOffOptionMock, errors);
            assertTrue(errors.hasErrors());
            assertNotNull(errors.getFieldError("reason"));

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Test
    public void test_postWriteOff_fail_invallidReason() {
        String reason = "WRONG_REASON";
        Short sequenceNumber = CommonConstants.SHORT_ZERO;
        String distributionDate = "2018-11-22";
        String postedDate = "2018-11-12";
        String userId = "1017";
        String batchId = "BACT";
        try {
            writeOffOptionMock.setReason(reason);
            doNothing().when(securityServiceMock).authenticateFunction(PAYMENT_WRITE_OFF);
            doThrow(new DataNotFoundException("support.data.missing", new Object[] { reason }))
                    .when(writeOffServiceMock).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate), userId,
                            batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate), writeOffOptionMock);

            mockMvc.perform(MockMvcRequestBuilders.post(
                    "/v1/billing/suspendedPayments/{sequenceNumber}/_writeOff?postedDate={postedDate}&userId={userId}&batchId={batchId}&distributionDate={distributionDate}",
                    sequenceNumber, postedDate, userId, batchId, distributionDate)
                    .contentType(MediaType.APPLICATION_JSON).header("X-CSC-User-Id", "SE30700")
                    .content(TestUtil.convertObjectToJsonBytes(writeOffOptionMock))).andExpect(status().isNotFound());

            verify(writeOffServiceMock, times(0)).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate),
                    userId, batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate), writeOffOptionMock);
            verifyNoMoreInteractions(writeOffServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_postWriteOff_fail_paymentNotFound() {

        try {
            Short sequenceNumber = CommonConstants.SHORT_ZERO;
            String distributionDate = "2018-11-23";
            String postedDate = "2018-11-12";
            String userId = "1017";
            String batchId = "1233";
            doNothing().when(securityServiceMock).authenticateFunction(PAYMENT_WRITE_OFF);
            doThrow(new DataNotFoundException("payment.does.not.exists")).when(writeOffServiceMock).save(sequenceNumber,
                    DateRoutine.dateTimeAsYYYYMMDD(postedDate), userId, batchId,
                    DateRoutine.dateTimeAsYYYYMMDD(distributionDate), writeOffOptionMock);
            mockMvc.perform(MockMvcRequestBuilders.post(
                    "/v1/billing/suspendedPayments/{sequenceNumber}/_writeOff?postedDate={postedDate}&userId={userId}&batchId={batchId}&distributionDate={distributionDate}",
                    sequenceNumber, postedDate, userId, batchId, distributionDate)
                    .contentType(MediaType.APPLICATION_JSON).header("X-CSC-User-Id", "SE30700")
                    .content(TestUtil.convertObjectToJsonBytes(writeOffOptionMock))).andExpect(status().isNotFound());

            verify(writeOffServiceMock, times(0)).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate),
                    userId, batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate), writeOffOptionMock);
            verifyNoMoreInteractions(writeOffServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_postWriteOff_fail_invalidPayment() {

        try {
            Short sequenceNumber = 5;
            String distributionDate = "2018-11-21";
            String postedDate = "2018-11-12";
            String userId = "1017";
            String batchId = "1233";
            doNothing().when(securityServiceMock).authenticateFunction(PAYMENT_WRITE_OFF);
            doThrow(new InvalidDataException("row.selected.does.not.qualify.for", new Object[] { "Write-Off" }))
                    .when(writeOffServiceMock).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate), userId,
                            batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate), writeOffOptionMock);
            mockMvc.perform(MockMvcRequestBuilders.post(
                    "/v1/billing/suspendedPayments/{sequenceNumber}/_writeOff?postedDate={postedDate}&userId={userId}&batchId={batchId}&distributionDate={distributionDate}",
                    sequenceNumber, postedDate, userId, batchId, distributionDate)
                    .contentType(MediaType.APPLICATION_JSON).header("X-CSC-User-Id", "SE30700")
                    .content(TestUtil.convertObjectToJsonBytes(writeOffOptionMock)))
                    .andExpect(status().isUnprocessableEntity());

            verify(writeOffServiceMock, times(0)).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate),
                    userId, batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate), writeOffOptionMock);
            verifyNoMoreInteractions(writeOffServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
