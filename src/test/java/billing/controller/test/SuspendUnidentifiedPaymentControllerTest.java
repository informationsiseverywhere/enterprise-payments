package billing.controller.test;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import billing.controller.SuspendUnidentifiedPaymentController;
import billing.service.SuspendUnidentifiedPaymentService;
import billing.validation.SuspendUnidentifiedPaymentValidator;
import core.exception.ExceptionControllerHandler;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.interceptor.SecurityInterceptor;
import core.security.service.SecurityService;
import core.utils.CommonConstants;
import core.utils.DateRoutine;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "classpath:test-servlet-context.xml",
        "classpath:test-applicationContext-persistence.xml", "classpath:test-root-context.xml" })
@WebAppConfiguration
public class SuspendUnidentifiedPaymentControllerTest {

    MockMvc mockMvc;

    @Autowired
    private ExceptionControllerHandler exceptionControllerHandler;

    @Autowired
    private SecurityInterceptor pb360SecurityInterceptor;

    @Mock
    private SecurityService securityServiceMock;

    @Autowired
    private SuspendUnidentifiedPaymentValidator suspendValidator;

    @Autowired
    @InjectMocks
    private SuspendUnidentifiedPaymentController suspendControllerTest;

    @Mock
    private SuspendUnidentifiedPaymentService suspendServiceMock;

    @BeforeAll
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterAll
    public static void tearDownAfterClass() throws Exception {
    }

    private static final String SUSPEND = "postUnidentifiedSuspend";

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.reset(suspendServiceMock);
        mockMvc = MockMvcBuilders.standaloneSetup(suspendControllerTest).addInterceptors(pb360SecurityInterceptor)
                .setControllerAdvice(exceptionControllerHandler).setValidator(suspendValidator).build();
    }

    @Test
    public void test_postSuspend_fail_disbursedRecord() {
        Short sequenceNumber = 6;
        String distributionDate = "2018-11-21";
        String postedDate = "2018-11-12";
        String userId = "1017";
        String batchId = "1233";
        try {

            doNothing().when(securityServiceMock).authenticateFunction(SUSPEND);
            doThrow(new InvalidDataException("disbursement.cannot.suspend")).when(suspendServiceMock).save(
                    sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate), userId, batchId,
                    DateRoutine.dateTimeAsYYYYMMDD(distributionDate));

            mockMvc.perform(MockMvcRequestBuilders.post(
                    "/v1/billing/suspendedPayments/{sequenceNumber}/_suspend?postedDate={postedDate}&userId={userId}&batchId={batchId}&distributionDate={distributionDate}",
                    sequenceNumber, postedDate, userId, batchId, distributionDate)
                    .contentType(MediaType.APPLICATION_JSON).header("X-CSC-User-Id", "SE30700"))
                    .andExpect(status().isUnprocessableEntity());

            verify(suspendServiceMock, times(0)).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate),
                    userId, batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate));
            verifyNoMoreInteractions(suspendServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_postSuspend_fail_paymentNotFound() {

        try {
            Short sequenceNumber = CommonConstants.SHORT_ZERO;
            String distributionDate = "2018-11-23";
            String postedDate = "2018-11-12";
            String userId = "1017";
            String batchId = "1233";
            doNothing().when(securityServiceMock).authenticateFunction(SUSPEND);
            doThrow(new DataNotFoundException("payment.does.not.exists")).when(suspendServiceMock).save(sequenceNumber,
                    DateRoutine.dateTimeAsYYYYMMDD(postedDate), userId, batchId,
                    DateRoutine.dateTimeAsYYYYMMDD(distributionDate));
            mockMvc.perform(MockMvcRequestBuilders.post(
                    "/v1/billing/suspendedPayments/{sequenceNumber}/_suspend?postedDate={postedDate}&userId={userId}&batchId={batchId}&distributionDate={distributionDate}",
                    sequenceNumber, postedDate, userId, batchId, distributionDate)
                    .contentType(MediaType.APPLICATION_JSON).header("X-CSC-User-Id", "SE30700"))
                    .andExpect(status().isNotFound());

            verify(suspendServiceMock, times(0)).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate),
                    userId, batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate));
            verifyNoMoreInteractions(suspendServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_postWriteOff_fail_invalidPayment() {

        try {
            Short sequenceNumber = 0;
            String distributionDate = "2019-07-01";
            String postedDate = "2019-07-01";
            String userId = "1017";
            String batchId = "GH23";
            doNothing().when(securityServiceMock).authenticateFunction(SUSPEND);
            doThrow(new InvalidDataException("row.selected.does.not.qualify.for", new Object[] { "Suspend" }))
                    .when(suspendServiceMock).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate), userId,
                            batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate));
            mockMvc.perform(MockMvcRequestBuilders.post(
                    "/v1/billing/suspendedPayments/{sequenceNumber}/_suspend?postedDate={postedDate}&userId={userId}&batchId={batchId}&distributionDate={distributionDate}",
                    sequenceNumber, postedDate, userId, batchId, distributionDate)
                    .contentType(MediaType.APPLICATION_JSON).header("X-CSC-User-Id", "SE30700"))
                    .andExpect(status().isUnprocessableEntity());

            verify(suspendServiceMock, times(0)).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate),
                    userId, batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate));
            verifyNoMoreInteractions(suspendServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_postWriteOff_success() {

        try {
            Short sequenceNumber = 5;
            String distributionDate = "2018-11-21";
            String postedDate = "2018-11-12";
            String userId = "1017";
            String batchId = "1233";
            doNothing().when(securityServiceMock).authenticateFunction(SUSPEND);
            doNothing().when(suspendServiceMock).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate),
                    userId, batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate));
            mockMvc.perform(MockMvcRequestBuilders.post(
                    "/v1/billing/suspendedPayments/{sequenceNumber}/_suspend?postedDate={postedDate}&userId={userId}&batchId={batchId}&distributionDate={distributionDate}",
                    sequenceNumber, postedDate, userId, batchId, distributionDate)
                    .contentType(MediaType.APPLICATION_JSON).header("X-CSC-User-Id", "SE30700"))
                    .andExpect(status().isCreated());

            verify(suspendServiceMock, times(1)).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate),
                    userId, batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate));
            verifyNoMoreInteractions(suspendServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
