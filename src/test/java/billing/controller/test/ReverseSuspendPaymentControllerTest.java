package billing.controller.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;

import billing.controller.ReverseSuspendedPaymentController;
import billing.model.ReverseOption;
import billing.service.ReverseSuspendPaymentService;
import billing.validation.ReverseSuspendPaymentValidator;
import core.exception.ExceptionControllerHandler;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.interceptor.SecurityInterceptor;
import core.security.service.SecurityService;
import core.utils.CommonConstants;
import core.utils.DateRoutine;
import core.utils.TestUtil;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "classpath:test-servlet-context.xml",
        "classpath:test-applicationContext-persistence.xml", "classpath:test-root-context.xml" })
@WebAppConfiguration
public class ReverseSuspendPaymentControllerTest {

    private MockMvc mockMvc;

    @Autowired
    @InjectMocks
    private ReverseSuspendedPaymentController reverseControllerTest;

    @Autowired
    private ExceptionControllerHandler exceptionControllerHandler;

    @Autowired
    private SecurityInterceptor pb360SecurityInterceptor;

    @Mock
    private SecurityService securityServiceMock;

    @Mock
    private ReverseSuspendPaymentService reverseServiceMock;

    @Mock
    private ReverseOption reverseOptionMock;

    @Autowired
    private ReverseSuspendPaymentValidator reverseValidatorMock;

    @BeforeAll
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterAll
    public static void tearDownAfterClass() throws Exception {
    }

    private static final String POST_REVERSE = "postReverse";

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.reset(reverseServiceMock);
        mockMvc = MockMvcBuilders.standaloneSetup(reverseControllerTest).addInterceptors(pb360SecurityInterceptor)
                .setControllerAdvice(exceptionControllerHandler).setValidator(reverseValidatorMock).build();
        reverseOptionMock = createReverseOptionMock();

    }

    private ReverseOption createReverseOptionMock() {
        reverseOptionMock = new ReverseOption();
        reverseOptionMock.setReason("Cash Reversal-NSF");
        reverseOptionMock.setDepositDate("2019-01-09");
        return reverseOptionMock;
    }

    @Test
    public void test_validateReason_fail() {
        try {
            ReverseOption reverseOptionMock = createReverseOptionMock();
            reverseOptionMock.setReason(null);
            doNothing().when(securityServiceMock).authenticateFunction(POST_REVERSE);
            BindException errors = new BindException(reverseOptionMock, "postreverseOptionMock");
            ValidationUtils.invokeValidator(reverseValidatorMock, reverseOptionMock, errors);
            assertTrue(errors.hasErrors());
            assertNotNull(errors.getFieldError("reason"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_postReverse_fail_invalidReason() {
        ReverseOption reverseOptionMock = createReverseOptionMock();
        reverseOptionMock.setReason("WRONG_REASON");
        Short sequenceNumber = CommonConstants.SHORT_ZERO;
        String date = "2019-07-01";
        String user = "1017";
        String batchId = "BACT";
        try {
            doNothing().when(securityServiceMock).authenticateFunction(POST_REVERSE);
            doThrow(new InvalidDataException("support.data.not.found", new Object[] { reverseOptionMock.getReason() }))
                    .when(reverseServiceMock).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(date), user, batchId,
                            DateRoutine.dateTimeAsYYYYMMDD(date), reverseOptionMock);

            mockMvc.perform(MockMvcRequestBuilders.post(
                    "/v1/billing/suspendedPayments/{sequenceNumber}/_reverse?postedDate={date}&userId={user}&batchId={batchId}&distributionDate={date}",
                    sequenceNumber, date, user, batchId, date).contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").content(TestUtil.convertObjectToJsonBytes(reverseOptionMock)))
                    .andExpect(status().isUnprocessableEntity());

            verify(reverseServiceMock, times(0)).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(date), user,
                    batchId, DateRoutine.dateTimeAsYYYYMMDD(date), reverseOptionMock);
            verifyNoMoreInteractions(reverseServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_postReverse_fail_noExistingPayment() {

        Short sequenceNumber = CommonConstants.SHORT_ZERO;
        String date = "2019-07-01";
        String user = "1017";
        String batchId = "BACT";
        try {
            doNothing().when(securityServiceMock).authenticateFunction(POST_REVERSE);
            doThrow(new DataNotFoundException("payment.does.not.exists",
                    new Object[] { reverseOptionMock.getReason() })).when(reverseServiceMock).save(sequenceNumber,
                            DateRoutine.dateTimeAsYYYYMMDD(date), user, batchId, DateRoutine.dateTimeAsYYYYMMDD(date),
                            reverseOptionMock);

            mockMvc.perform(MockMvcRequestBuilders.post(
                    "/v1/billing/suspendedPayments/{sequenceNumber}/_reverse?postedDate={date}&userId={user}&batchId={batchId}&distributionDate={date}",
                    sequenceNumber, date, user, batchId, date).contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").content(TestUtil.convertObjectToJsonBytes(reverseOptionMock)))
                    .andExpect(status().isNotFound());

            verify(reverseServiceMock, times(0)).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(date), user,
                    batchId, DateRoutine.dateTimeAsYYYYMMDD(date), reverseOptionMock);
            verifyNoMoreInteractions(reverseServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_postReverse_fail_invalidActivity() {
        Short sequenceNumber = CommonConstants.SHORT_ZERO;
        String date = "2019-07-01";
        String user = "1017";
        String batchId = "GH23";
        try {
            doNothing().when(securityServiceMock).authenticateFunction(POST_REVERSE);
            doThrow(new InvalidDataException("activity.does.not.qualify.for.reverse",
                    new Object[] { reverseOptionMock.getReason() })).when(reverseServiceMock).save(sequenceNumber,
                            DateRoutine.dateTimeAsYYYYMMDD(date), user, batchId, DateRoutine.dateTimeAsYYYYMMDD(date),
                            reverseOptionMock);

            mockMvc.perform(MockMvcRequestBuilders.post(
                    "/v1/billing/suspendedPayments/{sequenceNumber}/_reverse?postedDate={date}&userId={user}&batchId={batchId}&distributionDate={date}",
                    sequenceNumber, date, user, batchId, date).contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").content(TestUtil.convertObjectToJsonBytes(reverseOptionMock)))
                    .andExpect(status().isUnprocessableEntity());

            verify(reverseServiceMock, times(0)).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(date), user,
                    batchId, DateRoutine.dateTimeAsYYYYMMDD(date), reverseOptionMock);
            verifyNoMoreInteractions(reverseServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_postReverse_fail_invalidDepositDateFormat() {
        ReverseOption reverseOptionMock = createReverseOptionMock();
        reverseOptionMock.setDepositDate("ABC");
        Short sequenceNumber = CommonConstants.SHORT_ZERO;
        String date = "2019-07-01";
        String user = "1017";
        String batchId = "GH23";
        try {
            doNothing().when(securityServiceMock).authenticateFunction(POST_REVERSE);
            doThrow(new InvalidDataException("depositDate.invalid", new Object[] { reverseOptionMock.getReason() }))
                    .when(reverseServiceMock).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(date), user, batchId,
                            DateRoutine.dateTimeAsYYYYMMDD(date), reverseOptionMock);

            mockMvc.perform(MockMvcRequestBuilders.post(
                    "/v1/billing/suspendedPayments/{sequenceNumber}/_reverse?postedDate={date}&userId={user}&batchId={batchId}&distributionDate={date}",
                    sequenceNumber, date, user, batchId, date).contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").content(TestUtil.convertObjectToJsonBytes(reverseOptionMock)))
                    .andExpect(status().isUnprocessableEntity());

            verify(reverseServiceMock, times(0)).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(date), user,
                    batchId, DateRoutine.dateTimeAsYYYYMMDD(date), reverseOptionMock);
            verifyNoMoreInteractions(reverseServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_postReverse_success() {
        Short sequenceNumber = CommonConstants.SHORT_ZERO;
        String date = "2019-07-05";
        String user = "1017";
        String batchId = "BN45";

        try {

            doNothing().when(securityServiceMock).authenticateFunction(POST_REVERSE);
            doNothing().when(reverseServiceMock).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(date), user,
                    batchId, DateRoutine.dateTimeAsYYYYMMDD(date), reverseOptionMock);

            mockMvc.perform(MockMvcRequestBuilders.post(
                    "/v1/billing/suspendedPayments/{sequenceNumber}/_reverse?postedDate={date}&userId={user}&batchId={batchId}&distributionDate={date}",
                    sequenceNumber, date, user, batchId, date).contentType(MediaType.APPLICATION_JSON)
                    .header("X-CSC-User-Id", "SE30700").content(TestUtil.convertObjectToJsonBytes(reverseOptionMock)))
                    .andExpect(status().isCreated());

            verify(reverseServiceMock, times(1)).save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(date), user,
                    batchId, DateRoutine.dateTimeAsYYYYMMDD(date), reverseOptionMock);
            verifyNoMoreInteractions(reverseServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
