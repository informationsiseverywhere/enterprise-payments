package billing.controller.test;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import billing.controller.SuspendedPaymentController;
import billing.model.SuspendedPayment;
import billing.model.SuspendedPaymentList;
import billing.service.SuspendedPaymentService;
import billing.utils.BillingConstants.PaymentOperations;
import billing.validation.UpdateSuspendedPaymentValidator;
import core.exception.ExceptionControllerHandler;
import core.exception.database.DataNotFoundException;
import core.interceptor.SecurityInterceptor;
import core.security.service.SecurityService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "classpath:test-servlet-context.xml",
        "classpath:test-applicationContext-persistence.xml", "classpath:test-root-context.xml" })
@WebAppConfiguration
public class SuspendedPaymentControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private UpdateSuspendedPaymentValidator suspendedPaymentValidatorMock;

    @Autowired
    private ExceptionControllerHandler exceptionControllerHandler;

    @Autowired
    private SecurityInterceptor pb360SecurityInterceptor;

    @Mock
    private SuspendedPaymentService suspendedPaymentServiceMock;

    @Autowired
    @InjectMocks
    private SuspendedPaymentController suspendedPaymentControllerTest;

    private SuspendedPayment suspendedPaymentMock;

    private SuspendedPaymentList suspendedPaymentListMock;

    @Mock
    private SecurityService securityServiceMock;

    private static final String SEARCH_PAYMENT = "searchPayments";
    private static final String FIND_PAYMENT = "findPayment";

    @BeforeAll
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterAll
    public static void tearDownAfterClass() throws Exception {
    }

    @BeforeEach
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        Mockito.reset(suspendedPaymentServiceMock);
        mockMvc = MockMvcBuilders.standaloneSetup(suspendedPaymentControllerTest)
                .setControllerAdvice(exceptionControllerHandler).addInterceptors(pb360SecurityInterceptor)
                .setValidator(suspendedPaymentValidatorMock).build();

        suspendedPaymentMock = createSuspendedPaymentMock();
    }

    private SuspendedPayment createSuspendedPaymentMock() {
        SuspendedPayment suspendedPayment = new SuspendedPayment();
        suspendedPayment.setBatchId("1233");
        return suspendedPayment;
    }

    @Test
    public void test_getAccountPayments_fail() {
        try {
            int page = 5;
            int size = 10;
            String filters = null;

            doNothing().when(securityServiceMock).authenticateFunction(SEARCH_PAYMENT);

            when(suspendedPaymentServiceMock.findSuspendedPayments(page, size, filters))
                    .thenReturn(suspendedPaymentListMock);

            mockMvc.perform(
                    MockMvcRequestBuilders.get("/v1/billing/suspendedPayments").header("X-CSC-User-Id", "SE30700"))
                    .andExpect(status().isOk());

            verify(suspendedPaymentServiceMock, times(0)).findSuspendedPayments(page, size, filters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_getAccountPayment_success() {
        try {
            String batchId = "KCOU";
            short sequenceNumber = 0;
            String postedDate = "2029-09-10";
            String distributionDate = "2029-09-10";

            Map<String, Boolean> allowedOperations = new HashMap<String, Boolean>();
            allowedOperations.put(PaymentOperations.ALLOW_REVERSE.getValue(), true);
            allowedOperations.put(PaymentOperations.ALLOW_DISBUSEMENT.getValue(), true);
            allowedOperations.put(PaymentOperations.ALLOW_SUSPENSE.getValue(), true);
            allowedOperations.put(PaymentOperations.ALLOW_UPDATE.getValue(), true);
            allowedOperations.put(PaymentOperations.ALLOW_WRITEOFF.getValue(), true);

            doNothing().when(securityServiceMock).authenticateFunction(FIND_PAYMENT);
            when(suspendedPaymentServiceMock.findSuspendedPayment(batchId, sequenceNumber, postedDate, "SE30700",
                    distributionDate)).thenReturn(suspendedPaymentMock);

            when(suspendedPaymentServiceMock.areOperationsAllowed(sequenceNumber, postedDate, "SE30700", batchId,
                    distributionDate)).thenReturn(allowedOperations);

            mockMvc.perform(MockMvcRequestBuilders.get(
                    "/v1/billing/suspendedPayments/{sequenceNumber}?postedDate={postedDate}&userId={userId}&batchId={batchId}&distributionDate={distributionDate}",
                    sequenceNumber, postedDate, "SE30700", batchId, distributionDate).header("X-CSC-User-Id", "SE30700")
                    .header("Accept", "application/json")).andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON));

            verify(suspendedPaymentServiceMock, times(1)).findSuspendedPayment(batchId, sequenceNumber, postedDate,
                    "SE30700", distributionDate);
            verify(suspendedPaymentServiceMock, times(1)).areOperationsAllowed(sequenceNumber, postedDate, "SE30700",
                    batchId, distributionDate);
            verifyNoMoreInteractions(suspendedPaymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_getAccountPayment_fail() {
        try {
            String batchId = "XXXX";
            short sequenceNumber = 1;
            String postedDate = "2016-10-27";
            String distributionDate = "2029-09-10";
            doNothing().when(securityServiceMock).authenticateFunction(FIND_PAYMENT);

            when(suspendedPaymentServiceMock.findSuspendedPayment(batchId, sequenceNumber, postedDate, "SE30700",
                    distributionDate))
                            .thenThrow(new DataNotFoundException("no.bil.account.exists", new Object[] { batchId }));

            mockMvc.perform(MockMvcRequestBuilders.get(
                    "/v1/billing/suspendedPayments/{sequenceNumber}?postedDate={postedDate}&userId={userId}&batchId={batchId}&distributionDate={distributionDate}",
                    sequenceNumber, postedDate, "SE30700", batchId, distributionDate)
                    .header("X-CSC-User-Id", "SE30700")).andExpect(status().isNotFound());

            verify(suspendedPaymentServiceMock, times(1)).findSuspendedPayment(batchId, sequenceNumber, postedDate,
                    "SE30700", distributionDate);
            verifyNoMoreInteractions(suspendedPaymentServiceMock);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
