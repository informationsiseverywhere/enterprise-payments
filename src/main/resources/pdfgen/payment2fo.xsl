<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:output method="xml" encoding="UTF-8"/>
   <xsl:template match="Payment">
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <fo:layout-master-set>
            <fo:simple-page-master master-name="main"
                  page-width="8.5in" page-height="11in"
                  margin-top="1in" margin-bottom="1in"
                  margin-left="1in" margin-right="1in">
               <fo:region-body/>
               <fo:region-after extent="1cm"/>
            </fo:simple-page-master>
         </fo:layout-master-set>
         <fo:page-sequence master-reference="main">
            <fo:flow flow-name="xsl-region-body">
               <fo:wrapper font-size="12pt" font-family="Arial, Geneva, Helvetica, sans-serif">

                  <fo:table table-layout="fixed" width="100%">
                     <fo:table-column column-width="2in"/>
                     <fo:table-column column-width="4.5in"/>
                     <fo:table-body>
                        <fo:table-row>
                           <fo:table-cell>
                              <fo:block>
                                 <fo:external-graphic content-width="30%" src="url('company_logo.png')"/>
                              </fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                              <fo:block font-size="16pt" font-weight="bold" padding-after="1in">
                                 <fo:inline>PAYMENT RECEIPT</fo:inline>
                              </fo:block>
                           </fo:table-cell>
                        </fo:table-row>
                     </fo:table-body>
                  </fo:table>

                  <fo:table table-layout="fixed" width="100%">
                     <fo:table-column column-width="2in"/>
                     <fo:table-column column-width="4.5in"/>
                     <fo:table-body>
                        <fo:table-row>
                           <fo:table-cell>
                              <fo:block>Payment Date:</fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                              <fo:block><xsl:value-of select="paymentDate"/></fo:block>
                           </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                           <fo:table-cell>
                              <fo:block>Payor Name:</fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                              <fo:block><xsl:value-of select="payorName"/></fo:block>
                           </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                           <fo:table-cell>
                              <fo:block>Account:</fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                              <fo:block><xsl:value-of select="identifier"/></fo:block>
                           </fo:table-cell>
                        </fo:table-row>
                        <xsl:if test="policyNumber">
                           <fo:table-row>
                              <fo:table-cell>
                                 <fo:block>Policy Number:</fo:block>
                              </fo:table-cell>
                              <fo:table-cell>
                                 <fo:block><xsl:value-of select="policyNumber"/></fo:block>
                              </fo:table-cell>
                           </fo:table-row>
                        </xsl:if>
                        <fo:table-row>
                           <fo:table-cell>
                              <fo:block>Transaction ID:</fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                              <fo:block><xsl:value-of select="paymentId"/></fo:block>
                           </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                           <fo:table-cell>
                              <fo:block>Payment Method:</fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                              <fo:block><xsl:value-of select="paymentMethod"/></fo:block>
                           </fo:table-cell>
                        </fo:table-row>

                        <xsl:choose>
                           <xsl:when test="bankAccountNumber">
                              <fo:table-row>
                                 <fo:table-cell>
                                    <fo:block>Bank Account Number:</fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell>
                                    <fo:block><xsl:value-of select="bankAccountNumber"/></fo:block>
                                 </fo:table-cell>
                              </fo:table-row>
                           </xsl:when>
                           <xsl:when test="creditCardNumber">
                              <fo:table-row>
                                 <fo:table-cell>
                                    <fo:block>Credit Card Type:</fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell>
                                    <fo:block><xsl:value-of select="creditCardType"/></fo:block>
                                 </fo:table-cell>
                              </fo:table-row>
                              <fo:table-row>
                                 <fo:table-cell>
                                    <fo:block>Credit Card Number:</fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell>
                                    <fo:block><xsl:value-of select="creditCardNumber"/></fo:block>
                                 </fo:table-cell>
                              </fo:table-row>
                           </xsl:when>
                           <xsl:when test="batchId">
                              <fo:table-row>
                                 <fo:table-cell>
                                    <fo:block>Deposit Bank:</fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell>
                                    <fo:block><xsl:value-of select="depositBank"/></fo:block>
                                 </fo:table-cell>
                              </fo:table-row>
                              <fo:table-row>
                                 <fo:table-cell>
                                    <fo:block>Batch ID:</fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell>
                                    <fo:block><xsl:value-of select="batchId"/></fo:block>
                                 </fo:table-cell>
                              </fo:table-row>
                           </xsl:when>
                        </xsl:choose>

                        <fo:table-row>
                           <fo:table-cell>
                              <fo:block>Payment Amount:</fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                              <fo:block><xsl:value-of select="currency"/><xsl:value-of select="format-number(paymentAmount, '#,##0.00')"/></fo:block>
                           </fo:table-cell>
                        </fo:table-row>
                     </fo:table-body>
                  </fo:table>
               </fo:wrapper>
            </fo:flow>
         </fo:page-sequence>
      </fo:root>
   </xsl:template>
</xsl:stylesheet>