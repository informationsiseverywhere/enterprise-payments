CREATE TABLE IF NOT EXISTS BIL_TTY_ACT_RLT
(
    BIL_THIRD_PARTY_ID          CHAR(8) NOT NULL,
    BIL_ACCOUNT_ID              CHAR(8) NOT NULL,
    CONSTRAINT SQL151110033227320 PRIMARY KEY (BIL_THIRD_PARTY_ID, BIL_ACCOUNT_ID)
);
