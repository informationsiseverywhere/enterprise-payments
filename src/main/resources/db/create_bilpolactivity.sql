CREATE TABLE IF NOT EXISTS BIL_POL_ACTIVITY
(
    BIL_ACCOUNT_ID              CHAR(8) NOT NULL,
    POLICY_ID                   CHAR(16) NOT NULL,
    POL_EFFECTIVE_DT            DATE NOT NULL,
    BIL_ACY_TYPE_CD             CHAR(3) NOT NULL,
    BIL_NXT_ACY_DT              DATE NOT NULL,
    CONSTRAINT SQL151110033220730 PRIMARY KEY (BIL_ACCOUNT_ID, POLICY_ID, POL_EFFECTIVE_DT, BIL_ACY_TYPE_CD),
    CONSTRAINT BCMIBPA2 UNIQUE (BIL_NXT_ACY_DT, BIL_ACY_TYPE_CD, POL_EFFECTIVE_DT, POLICY_ID, BIL_ACCOUNT_ID)
);
