CREATE TABLE IF NOT EXISTS BIL_ORG_SUPPORT
(
    BIL_FILE_TYPE_CD            SMALLINT NOT NULL,
    PRI_LGG_CD                  CHAR(3) NOT NULL,
    BIL_RTE_TRA_NBR             CHAR(10) NOT NULL,
    BIL_BANK_NM                 CHAR(40) NOT NULL,
    BOS_TAPE_FRM_CD             CHAR(4) NOT NULL,
    BOS_COMPANY_ID              CHAR(25) NOT NULL,
    BOS_COMPANY_NM              CHAR(40) NOT NULL,
    BOS_PHY_TAPE_IND            CHAR(2) NOT NULL,
    BOS_DISC_DATA               CHAR(20) NOT NULL,
    BOS_FILE_DES_TXT            CHAR(40) NOT NULL,
    BOS_CPA_DSN_CEN             CHAR(5) NOT NULL,
    BOS_CPA_CRN_NBR             CHAR(4) NOT NULL,
    BOS_MERCHANT_ID             CHAR(17) NOT NULL,
    BIL_BANK_CD                 CHAR(3) NOT NULL,
    CONSTRAINT SQL151110033220510 PRIMARY KEY (BIL_FILE_TYPE_CD, PRI_LGG_CD)
);

INSERT INTO BIL_ORG_SUPPORT ( BIL_FILE_TYPE_CD, PRI_LGG_CD, BIL_RTE_TRA_NBR, BIL_BANK_NM, BOS_TAPE_FRM_CD, BOS_COMPANY_ID, BOS_COMPANY_NM, BOS_PHY_TAPE_IND, BOS_DISC_DATA, BOS_FILE_DES_TXT, BOS_CPA_DSN_CEN, BOS_CPA_CRN_NBR, BOS_MERCHANT_ID, BIL_BANK_CD ) VALUES (1,'   ','503904483 ','Nations Bank                            ','ACH ','057-12345                ','CSC                                     ','  ','Insurance Premium   ','File type one - ACH                     ','00000','0000','0123456789       ','311');
INSERT INTO BIL_ORG_SUPPORT ( BIL_FILE_TYPE_CD, PRI_LGG_CD, BIL_RTE_TRA_NBR, BIL_BANK_NM, BOS_TAPE_FRM_CD, BOS_COMPANY_ID, BOS_COMPANY_NM, BOS_PHY_TAPE_IND, BOS_DISC_DATA, BOS_FILE_DES_TXT, BOS_CPA_DSN_CEN, BOS_CPA_CRN_NBR, BOS_MERCHANT_ID, BIL_BANK_CD ) VALUES (2,'   ','233434434 ','First Federal                           ','ACH ','057-12345                ','CSC                                     ','  ','Insurance Premium   ','File type two - ACH                     ','0    ','0   ','0123456789       ','300');
INSERT INTO BIL_ORG_SUPPORT ( BIL_FILE_TYPE_CD, PRI_LGG_CD, BIL_RTE_TRA_NBR, BIL_BANK_NM, BOS_TAPE_FRM_CD, BOS_COMPANY_ID, BOS_COMPANY_NM, BOS_PHY_TAPE_IND, BOS_DISC_DATA, BOS_FILE_DES_TXT, BOS_CPA_DSN_CEN, BOS_CPA_CRN_NBR, BOS_MERCHANT_ID, BIL_BANK_CD ) VALUES (3,'   ','655678863 ','Nations Bank                            ','CPA ','057-12345                ','CSC                                     ','  ','Insurance Premium   ','File type three - CPA                   ','0    ','0   ','0123456789       ','   ');
INSERT INTO BIL_ORG_SUPPORT ( BIL_FILE_TYPE_CD, PRI_LGG_CD, BIL_RTE_TRA_NBR, BIL_BANK_NM, BOS_TAPE_FRM_CD, BOS_COMPANY_ID, BOS_COMPANY_NM, BOS_PHY_TAPE_IND, BOS_DISC_DATA, BOS_FILE_DES_TXT, BOS_CPA_DSN_CEN, BOS_CPA_CRN_NBR, BOS_MERCHANT_ID, BIL_BANK_CD ) VALUES (4,'   ','987766783 ','First Federal                           ','CPA ','057-12345                ','CSC                                     ','  ','Insurance Premium   ','File type four - CPA                    ','0    ','0   ','0123456789       ','   ');
INSERT INTO BIL_ORG_SUPPORT ( BIL_FILE_TYPE_CD, PRI_LGG_CD, BIL_RTE_TRA_NBR, BIL_BANK_NM, BOS_TAPE_FRM_CD, BOS_COMPANY_ID, BOS_COMPANY_NM, BOS_PHY_TAPE_IND, BOS_DISC_DATA, BOS_FILE_DES_TXT, BOS_CPA_DSN_CEN, BOS_CPA_CRN_NBR, BOS_MERCHANT_ID, BIL_BANK_CD ) VALUES (7,'   ','133258869 ','NECHA Clearing House                    ','ECH ','00142                    ','CSC                                     ','  ','Credit Card Pymt    ','File Type Seven - ECH                   ','00000','0000','1029384756       ','305');
