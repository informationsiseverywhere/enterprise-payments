CREATE TABLE IF NOT EXISTS BIL_AGT_CASH_DSP
(
    BIL_AGT_ACT_ID              CHAR(8) NOT NULL,
    BIL_DTB_DT                  DATE NOT NULL,
    BIL_DTB_SEQ_NBR             SMALLINT NOT NULL,
    BIL_DSP_SEQ_NBR             SMALLINT NOT NULL,
    BIL_ADJ_DUE_DT              DATE NOT NULL,
    USER_ID                     CHAR(8) NOT NULL,
    BIL_DSP_DT                  DATE NOT NULL,
    BIL_DSP_TYPE_CD             CHAR(2) NOT NULL,
    BIL_DSP_REASON_CD           CHAR(3) NOT NULL,
    BIL_DSP_AMT                 DECIMAL(14,2) NOT NULL,
    BIL_PAYEE_CLT_ID            CHAR(20) NOT NULL,
    BIL_PAYEE_ADR_SEQ           SMALLINT NOT NULL,
    BIL_MANUAL_SUS_IND          CHAR(1) NOT NULL,
    DWS_CK_DRF_NBR              INTEGER,
    BIL_REVS_RSUS_IND           CHAR(1) NOT NULL,
    BIL_TO_FRO_TRF_NBR          CHAR(30) NOT NULL,
    BIL_TRF_TYPE_CD             CHAR(1) NOT NULL,
    DDS_DSB_DT                  DATE,
    DWS_STATUS_CD               CHAR(1) NOT NULL,
    BIL_DSB_ID                  CHAR(20) NOT NULL,
    BIL_CHK_PRD_MTH_CD          CHAR(1) NOT NULL,
    BIL_STT_SEQ_NBR             SMALLINT NOT NULL,
    CONSTRAINT SQL160608180725660 PRIMARY KEY (BIL_AGT_ACT_ID, BIL_DTB_DT, BIL_DTB_SEQ_NBR, BIL_DSP_SEQ_NBR)
    
);