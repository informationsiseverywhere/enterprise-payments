CREATE TABLE IF NOT EXISTS CLIENT_ADDRESS (
    ADR_ID                      CHAR(20) NOT NULL,
    ST_CD                       CHAR(3) NOT NULL,
    CICA_PST_CD                 CHAR(20) NOT NULL,
    CTR_CD                      CHAR(4) NOT NULL,
    CICA_ADD_ADR_IND            CHAR(1),
    USER_ID                     CHAR(8) NOT NULL,
    STATUS_CD                   CHAR(1) NOT NULL,
    TERMINAL_ID                 CHAR(8) NOT NULL,
    CICA_EFF_ACY_TS             TIMESTAMP NOT NULL,
    CICA_ADR_1                  VARCHAR(64) NOT NULL,
    CICA_ADR_2                  VARCHAR(64),
    CICA_CIT_NM                 VARCHAR(64) NOT NULL,
    CICA_CTY                    VARCHAR(30),
    CONSTRAINT SQL160608180729420 PRIMARY KEY (ADR_ID)
);
