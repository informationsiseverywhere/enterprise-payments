CREATE TABLE IF NOT EXISTS BIL_BANK
(
    BIL_BANK_CD                 CHAR(3) NOT NULL,
    BBK_ACCOUNT_NM              CHAR(40) NOT NULL,
    BBK_ACCOUNT_NBR             CHAR(25) NOT NULL,
    BIL_RTE_TRA_NBR             CHAR(10) NOT NULL,
    BBK_NO_DISPLAY_IND          CHAR(1) NOT NULL,
    CONSTRAINT SQL151110033208760 PRIMARY KEY (BIL_BANK_CD)
);


INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('100','Lexington State                         ','                         ','          ','Y');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('200','First Citizens                          ','                         ','          ','Y');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('300','First Federal                           ','                         ','          ','Y');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('305','ECH Bank One                            ','673920741                ','133258869 ','Y');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('309','Wells Fargo                             ','4121853519               ','500008780 ','Y');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('310','JPMC                                    ','1065986                  ','071000013 ','Y');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('311','FIFTH THIRD BANK                        ','7238668094               ','242071758 ','Y');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('312','Chase                                   ','938001732                ','021000021 ','Y');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('500','Metropolitan National Bank              ','1000638421               ','65300279  ',' ');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('501','Metropolitan National Bank - NE Branch  ','0000000501               ','501       ','Y');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('510','Pulaski Cnty Main Office                ','0000000510               ','510       ','N');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('511','Pulaski Cnty Conway Branch              ','0000000511               ','511       ','N');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('600','Wachovia National Bank                  ','2000537310               ','54300168  ',' ');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('601','Wachovia National Bank - South Branch   ','0000000601               ','601       ','Y');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('610','First Union                             ','0000000610               ','610       ','N');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('611','First Union - North Branch              ','0000000611               ','611       ','N');
INSERT INTO BIL_BANK ( BIL_BANK_CD, BBK_ACCOUNT_NM, BBK_ACCOUNT_NBR, BIL_RTE_TRA_NBR, BBK_NO_DISPLAY_IND ) VALUES ('999','Refund Account                          ','                         ','          ','N');
