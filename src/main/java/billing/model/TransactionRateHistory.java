package billing.model;

import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TransactionRateHistory extends ResourceSupport {

    private String paymentCurrency;
    private Double paymentAmount;
    private String receivedCurrency;
    private Double receivedAmount;
    private Double rate;

    public TransactionRateHistory(@JsonProperty("paymentCurrency") String paymentCurrency,
            @JsonProperty("paymentAmount") Double paymentAmount,
            @JsonProperty("receivedCurrency") String receivedCurrency,
            @JsonProperty("receivedAmount") Double receivedAmount, @JsonProperty("rate") Double rate) {
        super();
        this.paymentCurrency = paymentCurrency;
        this.paymentAmount = paymentAmount;
        this.receivedCurrency = receivedCurrency;
        this.receivedAmount = receivedAmount;
        this.rate = rate;
    }

    public TransactionRateHistory() {

    }

    public String getReceivedCurrency() {
        return receivedCurrency;
    }

    public void setReceivedCurrency(String receivedCurrency) {
        this.receivedCurrency = receivedCurrency;
    }

    public Double getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(Double receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(paymentAmount, paymentCurrency, rate, receivedAmount, receivedCurrency);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        TransactionRateHistory other = (TransactionRateHistory) obj;
        return Objects.equals(paymentAmount, other.paymentAmount)
                && Objects.equals(paymentCurrency, other.paymentCurrency) && Objects.equals(rate, other.rate)
                && Objects.equals(receivedAmount, other.receivedAmount)
                && Objects.equals(receivedCurrency, other.receivedCurrency);
    }

}
