package billing.model;

import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PayableOption extends ResourceSupport {

    private String refundMethod;
    private Integer checkNumber;
    private String reason;
    private String payeeId;
    private Short addressSequenceNumber;
    private String transactionId;

    @JsonCreator
    public PayableOption(@JsonProperty("refundMethod") String refundMethod,
            @JsonProperty("checkNumber") Integer checkNumber, @JsonProperty("reason") String reason,
            @JsonProperty("payeeId") String payeeId, @JsonProperty("addressSequenceNumber") Short addressSquenceNumber,
            @JsonProperty("transactionId") String transactionId) {
        super();
        this.refundMethod = refundMethod;
        this.checkNumber = checkNumber;
        this.reason = reason;
        this.payeeId = payeeId;
        this.addressSequenceNumber = addressSquenceNumber;
        this.transactionId = transactionId;
    }

    public PayableOption() {
        super();
    }

    public PayableOption(PayableOption payableOptions) {
        this(payableOptions.getRefundMethod(), payableOptions.getCheckNumber(), payableOptions.getReason(),
                payableOptions.getPayeeId(), payableOptions.getAddressSequenceNumber(),
                payableOptions.getTransactionId());
    }

    public String getRefundMethod() {
        return refundMethod;
    }

    public void setRefundMethod(String refundMethod) {
        this.refundMethod = refundMethod;
    }

    public Integer getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(Integer checkNumber) {
        this.checkNumber = checkNumber;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPayeeId() {
        return payeeId;
    }

    public void setPayeeId(String payeeId) {
        this.payeeId = payeeId;
    }

    public Short getAddressSequenceNumber() {
        return addressSequenceNumber;
    }

    public void setAddressSequenceNumber(Short addressSequenceNumber) {
        this.addressSequenceNumber = addressSequenceNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + Objects.hash(addressSequenceNumber, checkNumber, payeeId, reason, refundMethod, transactionId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PayableOption other = (PayableOption) obj;
        return Objects.equals(addressSequenceNumber, other.addressSequenceNumber)
                && Objects.equals(checkNumber, other.checkNumber) && Objects.equals(payeeId, other.payeeId)
                && Objects.equals(reason, other.reason) && Objects.equals(refundMethod, other.refundMethod)
                && Objects.equals(transactionId, other.transactionId);
    }

}
