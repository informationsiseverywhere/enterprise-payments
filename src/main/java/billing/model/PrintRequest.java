package billing.model;

import static core.utils.CommonConstants.BLANK_STRING;

import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PrintRequest extends ResourceSupport {

    private String applicationVersion = BLANK_STRING;
    private String applicationId = BLANK_STRING;
    private String printClientId = BLANK_STRING;
    private String referenceId = BLANK_STRING;
    private String startPrintFromId = BLANK_STRING;
    private String dataTimestamp = BLANK_STRING;
    private String businessTransaction = BLANK_STRING;
    private String businessCaseId = BLANK_STRING;
    private String requestUserId = BLANK_STRING;
    private String requestApplicationName = BLANK_STRING;
    private String endPrintFromId = BLANK_STRING;
    private char readRowsCode;
    private char clientFunction;
    private String fillerNewUserCodes = BLANK_STRING;
    private String printInstructions = BLANK_STRING;
    private char platformCode;
    private String incidentSpecificdData = BLANK_STRING;
    private String additionalKey = BLANK_STRING;
    private String databaseName = BLANK_STRING;
    private String printRequestId = BLANK_STRING;
    private String applicationTsQId = BLANK_STRING;
    private String startAccountId = BLANK_STRING;
    private String endAccountId = BLANK_STRING;

    public PrintRequest() {
        super();
    }

    public PrintRequest(@JsonProperty("applicationVersion") String applicationVersion,
            @JsonProperty("applicationId") String applicationId, @JsonProperty("printClientId") String printClientId,
            @JsonProperty("referenceId") String referenceId, @JsonProperty("startPrintFromId") String startPrintFromId,
            @JsonProperty("dataTimestamp") String dataTimestamp,
            @JsonProperty("businessTransaction") String businessTransaction,
            @JsonProperty("businessCaseId") String businessCaseId, @JsonProperty("requestUserId") String requestUserId,
            @JsonProperty("requestApplicationName") String requestApplicationName,
            @JsonProperty("endPrintFromId") String endPrintFromId, @JsonProperty("readRowsCode") char readRowsCode,
            @JsonProperty("clientFunction") char clientFunction,
            @JsonProperty("fillerNewUserCodes") String fillerNewUserCodes,
            @JsonProperty("printInstructions") String printInstructions,
            @JsonProperty("platformCode") char platformCode,
            @JsonProperty("incidentSpecificdData") String incidentSpecificdData,
            @JsonProperty("additionalKey") String additionalKey, @JsonProperty("databaseName") String databaseName,
            @JsonProperty("printRequestId") String printRequestId,
            @JsonProperty("applicationTsQId") String applicationTsQId,
            @JsonProperty("startAccountId") String startAccountId, @JsonProperty("endAccountId") String endAccountId) {
        super();
        this.applicationVersion = applicationVersion;
        this.applicationId = applicationId;
        this.printClientId = printClientId;
        this.referenceId = referenceId;
        this.startPrintFromId = startPrintFromId;
        this.dataTimestamp = dataTimestamp;
        this.businessTransaction = businessTransaction;
        this.businessCaseId = businessCaseId;
        this.requestUserId = requestUserId;
        this.requestApplicationName = requestApplicationName;
        this.endPrintFromId = endPrintFromId;
        this.readRowsCode = readRowsCode;
        this.clientFunction = clientFunction;
        this.fillerNewUserCodes = fillerNewUserCodes;
        this.printInstructions = printInstructions;
        this.platformCode = platformCode;
        this.incidentSpecificdData = incidentSpecificdData;
        this.additionalKey = additionalKey;
        this.databaseName = databaseName;
        this.printRequestId = printRequestId;
        this.applicationTsQId = applicationTsQId;
        this.startAccountId = startAccountId;
        this.endAccountId = endAccountId;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getPrintClientId() {
        return printClientId;
    }

    public void setPrintClientId(String printClientId) {
        this.printClientId = printClientId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getStartPrintFromId() {
        return startPrintFromId;
    }

    public void setStartPrintFromId(String startPrintFromId) {
        this.startPrintFromId = startPrintFromId;
    }

    public String getDataTimestamp() {
        return dataTimestamp;
    }

    public void setDataTimestamp(String dataTimestamp) {
        this.dataTimestamp = dataTimestamp;
    }

    public String getBusinessTransaction() {
        return businessTransaction;
    }

    public void setBusinessTransaction(String businessTransaction) {
        this.businessTransaction = businessTransaction;
    }

    public String getBusinessCaseId() {
        return businessCaseId;
    }

    public void setBusinessCaseId(String businessCaseId) {
        this.businessCaseId = businessCaseId;
    }

    public String getRequestUserId() {
        return requestUserId;
    }

    public void setRequestUserId(String requestUserId) {
        this.requestUserId = requestUserId;
    }

    public String getRequestApplicationName() {
        return requestApplicationName;
    }

    public void setRequestApplicationName(String requestApplicationName) {
        this.requestApplicationName = requestApplicationName;
    }

    public String getEndPrintFromId() {
        return endPrintFromId;
    }

    public void setEndPrintFromId(String endPrintFromId) {
        this.endPrintFromId = endPrintFromId;
    }

    public char getReadRowsCode() {
        return readRowsCode;
    }

    public void setReadRowsCode(char readRowsCode) {
        this.readRowsCode = readRowsCode;
    }

    public char getClientFunction() {
        return clientFunction;
    }

    public void setClientFunction(char clientFunction) {
        this.clientFunction = clientFunction;
    }

    public String getFillerNewUserCodes() {
        return fillerNewUserCodes;
    }

    public void setFillerNewUserCodes(String fillerNewUserCodes) {
        this.fillerNewUserCodes = fillerNewUserCodes;
    }

    public String getPrintInstructions() {
        return printInstructions;
    }

    public void setPrintInstructions(String printInstructions) {
        this.printInstructions = printInstructions;
    }

    public char getPlatformCode() {
        return platformCode;
    }

    public void setPlatformCode(char platformCode) {
        this.platformCode = platformCode;
    }

    public String getIncidentSpecificdData() {
        return incidentSpecificdData;
    }

    public void setIncidentSpecificdData(String incidentSpecificdData) {
        this.incidentSpecificdData = incidentSpecificdData;
    }

    public String getAdditionalKey() {
        return additionalKey;
    }

    public void setAdditionalKey(String additionalKey) {
        this.additionalKey = additionalKey;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getPrintRequestId() {
        return printRequestId;
    }

    public void setPrintRequestId(String printRequestId) {
        this.printRequestId = printRequestId;
    }

    public String getApplicationTsQId() {
        return applicationTsQId;
    }

    public void setApplicationTsQId(String applicationTsQId) {
        this.applicationTsQId = applicationTsQId;
    }

    public String getStartAccountId() {
        return startAccountId;
    }

    public void setStartAccountId(String startAccountId) {
        this.startAccountId = startAccountId;
    }

    public String getEndAccountId() {
        return endAccountId;
    }

    public void setEndAccountId(String endAccountId) {
        this.endAccountId = endAccountId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(additionalKey, applicationId, applicationTsQId, applicationVersion,
                businessCaseId, businessTransaction, clientFunction, dataTimestamp, databaseName, endAccountId,
                endPrintFromId, fillerNewUserCodes, incidentSpecificdData, platformCode, printClientId,
                printInstructions, printRequestId, readRowsCode, referenceId, requestApplicationName, requestUserId,
                startAccountId, startPrintFromId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PrintRequest other = (PrintRequest) obj;
        return Objects.equals(additionalKey, other.additionalKey) && Objects.equals(applicationId, other.applicationId)
                && Objects.equals(applicationTsQId, other.applicationTsQId)
                && Objects.equals(applicationVersion, other.applicationVersion)
                && Objects.equals(businessCaseId, other.businessCaseId)
                && Objects.equals(businessTransaction, other.businessTransaction)
                && clientFunction == other.clientFunction && Objects.equals(dataTimestamp, other.dataTimestamp)
                && Objects.equals(databaseName, other.databaseName) && Objects.equals(endAccountId, other.endAccountId)
                && Objects.equals(endPrintFromId, other.endPrintFromId)
                && Objects.equals(fillerNewUserCodes, other.fillerNewUserCodes)
                && Objects.equals(incidentSpecificdData, other.incidentSpecificdData)
                && platformCode == other.platformCode && Objects.equals(printClientId, other.printClientId)
                && Objects.equals(printInstructions, other.printInstructions)
                && Objects.equals(printRequestId, other.printRequestId) && readRowsCode == other.readRowsCode
                && Objects.equals(referenceId, other.referenceId)
                && Objects.equals(requestApplicationName, other.requestApplicationName)
                && Objects.equals(requestUserId, other.requestUserId)
                && Objects.equals(startAccountId, other.startAccountId)
                && Objects.equals(startPrintFromId, other.startPrintFromId);
    }

}
