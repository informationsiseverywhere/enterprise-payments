package billing.model;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SuspendedPayment extends ResourceSupport {

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime distributionDate;
    private Short paymentSequenceNumber;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime postedDate;
    private String batchId;
    private String userId;
    private String userName;
    private String identifier;
    private String identifierType;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime dueDate;
    private Boolean holdSuspenseIndicator;
    private String suspenseReason;
    private String policySymbol;
    private String payableItem;
    private String policyNumber;
    private String paymentId;
    private String paymentType;
    private String additionalId;
    private String paymentComments;
    private Double paymentAmount;
    private String status;
    private String payorId;
    private Short addressSequenceNumber;
    private String activityUserId;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime depositDate;
    private String payorName;
    private String paymentMethod;
    private String currency;
    private String businessGroup;
    private Boolean overrideBusinessGroupEditIndicator;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime postMarkDate;
    private String depositBank;
    private String controlBank;
    private String businessGroupCode;
    private Boolean transferToDirect;
    private Boolean transferToGroup;
    private Boolean transferToAgency;
    private String walletType;
    private String walletNumber;
    private String walletIdentifier;
    private String bankAccountNumber;
    private String creditCardType;
    private String creditCardNumber;

    public SuspendedPayment(@JsonProperty("identifier") String identifier,
            @JsonProperty("identifierType") String identifierType,
            @JsonProperty("ZonedDateTimedueDate") ZonedDateTime dueDate,
            @JsonProperty("holdSuspenseIndicator") Boolean holdSuspenseIndicator,
            @JsonProperty("suspenseReason") String suspenseReason, @JsonProperty("policySymbol") String policySymbol,
            @JsonProperty("payableItem") String payableItem, @JsonProperty("policyNumber") String policyNumber,
            @JsonProperty("paymentId") String paymentId, @JsonProperty("paymentType") String paymentType,
            @JsonProperty("additionalId") String additionalId, @JsonProperty("paymentComments") String paymentComments,
            @JsonProperty("paymentAmount") Double paymentAmount, @JsonProperty("status") String status,
            @JsonProperty("payorId") String payorId, @JsonProperty("addressSequenceNumber") Short addressSequenceNumber,
            @JsonProperty("activityUserId") String activityUserId,
            @JsonProperty("distributionDate") ZonedDateTime distributionDate,
            @JsonProperty("paymentSequenceNumber") Short paymentSequenceNumber,
            @JsonProperty("postedDate") ZonedDateTime postedDate, @JsonProperty("batchId") String batchId,
            @JsonProperty("userId") String userId, @JsonProperty("userName") String userName,
            @JsonProperty("depositDate") ZonedDateTime depositDate, @JsonProperty("payorName") String payorName,
            @JsonProperty("paymentMethod") String paymentMethod, @JsonProperty("currency") String currency,
            @JsonProperty("businessGroup") String businessGroup,
            @JsonProperty("overrideBusinessGroupEditIndicator") Boolean overrideBusinessGroupEditIndicator,
            @JsonProperty("postMarkDate") ZonedDateTime postMarkDate, @JsonProperty("depositBank") String depositBank,
            @JsonProperty("controlBank") String controlBank,
            @JsonProperty("businessGroupCode") String businessGroupCode,
            @JsonProperty("transferToDirect") Boolean transferToDirect,
            @JsonProperty("transferToGroup") Boolean transferToGroup,
            @JsonProperty("transferToAgency") Boolean transferToAgency, @JsonProperty("walletType") String walletType,
            @JsonProperty("walletNumber") String walletNumber,
            @JsonProperty("walletIdentifier") String walletIdentifier) {
        super();
        this.identifier = identifier;
        this.identifierType = identifierType;
        this.dueDate = dueDate;
        this.holdSuspenseIndicator = holdSuspenseIndicator;
        this.suspenseReason = suspenseReason;
        this.policySymbol = policySymbol;
        this.payableItem = payableItem;
        this.policyNumber = policyNumber;
        this.paymentId = paymentId;
        this.paymentType = paymentType;
        this.additionalId = additionalId;
        this.paymentComments = paymentComments;
        this.paymentAmount = paymentAmount;
        this.status = status;
        this.payorId = payorId;
        this.addressSequenceNumber = addressSequenceNumber;
        this.activityUserId = activityUserId;
        this.distributionDate = distributionDate;
        this.paymentSequenceNumber = paymentSequenceNumber;
        this.postedDate = postedDate;
        this.batchId = batchId;
        this.userId = userId;
        this.userName = userName;
        this.depositDate = depositDate;
        this.payorName = payorName;
        this.paymentMethod = paymentMethod;
        this.currency = currency;
        this.businessGroup = businessGroup;
        this.overrideBusinessGroupEditIndicator = overrideBusinessGroupEditIndicator;
        this.postMarkDate = postMarkDate;
        this.depositBank = depositBank;
        this.controlBank = controlBank;
        this.businessGroupCode = businessGroupCode;
        this.transferToDirect = transferToDirect;
        this.transferToGroup = transferToGroup;
        this.transferToAgency = transferToAgency;
        this.walletType = walletType;
        this.walletNumber = walletNumber;
        this.walletIdentifier = walletIdentifier;
    }

    public SuspendedPayment() {
        super();
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifierType() {
        return identifierType;
    }

    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    public ZonedDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(ZonedDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public Boolean getHoldSuspenseIndicator() {
        return holdSuspenseIndicator;
    }

    public void setHoldSuspenseIndicator(Boolean holdSuspenseIndicator) {
        this.holdSuspenseIndicator = holdSuspenseIndicator;
    }

    public String getSuspenseReason() {
        return suspenseReason;
    }

    public void setSuspenseReason(String suspenseReason) {
        this.suspenseReason = suspenseReason;
    }

    public String getPolicySymbol() {
        return policySymbol;
    }

    public void setPolicySymbol(String policySymbol) {
        this.policySymbol = policySymbol;
    }

    public String getPayableItem() {
        return payableItem;
    }

    public void setPayableItem(String payableItem) {
        this.payableItem = payableItem;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getAdditionalId() {
        return additionalId;
    }

    public void setAdditionalId(String additionalId) {
        this.additionalId = additionalId;
    }

    public String getPaymentComments() {
        return paymentComments;
    }

    public void setPaymentComments(String paymentComments) {
        this.paymentComments = paymentComments;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPayorId() {
        return payorId;
    }

    public Short getAddressSequenceNumber() {
        return addressSequenceNumber;
    }

    public void setPayorId(String payorId) {
        this.payorId = payorId;
    }

    public void setAddressSequenceNumber(Short addressSequenceNumber) {
        this.addressSequenceNumber = addressSequenceNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ZonedDateTime getDistributionDate() {
        return distributionDate;
    }

    public Short getPaymentSequenceNumber() {
        return paymentSequenceNumber;
    }

    public ZonedDateTime getPostedDate() {
        return postedDate;
    }

    public String getBatchId() {
        return batchId;
    }

    public String getActivityUserId() {
        return activityUserId;
    }

    public void setDistributionDate(ZonedDateTime distributionDate) {
        this.distributionDate = distributionDate;
    }

    public void setPaymentSequenceNumber(Short paymentSequenceNumber) {
        this.paymentSequenceNumber = paymentSequenceNumber;
    }

    public void setPostedDate(ZonedDateTime postedDate) {
        this.postedDate = postedDate;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public void setActivityUserId(String activityUserId) {
        this.activityUserId = activityUserId;
    }

    public ZonedDateTime getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(ZonedDateTime depositDate) {
        this.depositDate = depositDate;
    }

    public String getPayorName() {
        return payorName;
    }

    public void setPayorName(String payorName) {
        this.payorName = payorName;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBusinessGroup() {
        return businessGroup;
    }

    public void setBusinessGroup(String businessGroup) {
        this.businessGroup = businessGroup;
    }

    public Boolean getOverrideBusinessGroupEditIndicator() {
        return overrideBusinessGroupEditIndicator;
    }

    public void setOverrideBusinessGroupEditIndicator(Boolean overrideBusinessGroupEditIndicator) {
        this.overrideBusinessGroupEditIndicator = overrideBusinessGroupEditIndicator;
    }

    public ZonedDateTime getPostMarkDate() {
        return postMarkDate;
    }

    public void setPostMarkDate(ZonedDateTime postMarkDate) {
        this.postMarkDate = postMarkDate;
    }

    public String getDepositBank() {
        return depositBank;
    }

    public void setDepositBank(String depositBank) {
        this.depositBank = depositBank;
    }

    public String getControlBank() {
        return controlBank;
    }

    public void setControlBank(String controlBank) {
        this.controlBank = controlBank;
    }

    public String getBusinessGroupCode() {
        return businessGroupCode;
    }

    public void setBusinessGroupCode(String businessGroupCode) {
        this.businessGroupCode = businessGroupCode;
    }

    public Boolean getTransferToDirect() {
        return transferToDirect;
    }

    public void setTransferToDirect(Boolean transferToDirect) {
        this.transferToDirect = transferToDirect;
    }

    public Boolean getTransferToGroup() {
        return transferToGroup;
    }

    public void setTransferToGroup(Boolean transferToGroup) {
        this.transferToGroup = transferToGroup;
    }

    public Boolean getTransferToAgency() {
        return transferToAgency;
    }

    public void setTransferToAgency(Boolean transferToAgency) {
        this.transferToAgency = transferToAgency;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getWalletNumber() {
        return walletNumber;
    }

    public void setWalletNumber(String walletNumber) {
        this.walletNumber = walletNumber;
    }

    public String getWalletIdentifier() {
        return walletIdentifier;
    }

    public void setWalletIdentifier(String walletIdentifier) {
        this.walletIdentifier = walletIdentifier;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getCreditCardType() {
        return creditCardType;
    }

    public void setCreditCardType(String creditCardType) {
        this.creditCardType = creditCardType;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(activityUserId, additionalId, addressSequenceNumber, bankAccountNumber,
                batchId, businessGroup, businessGroupCode, controlBank, creditCardNumber, creditCardType, currency,
                depositBank, depositDate, distributionDate, dueDate, holdSuspenseIndicator, identifier, identifierType,
                overrideBusinessGroupEditIndicator, payableItem, paymentAmount, paymentComments, paymentId,
                paymentMethod, paymentSequenceNumber, paymentType, payorId, payorName, policyNumber, policySymbol,
                postMarkDate, postedDate, status, suspenseReason, transferToAgency, transferToDirect, transferToGroup,
                userId, userName, walletIdentifier, walletNumber, walletType);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SuspendedPayment other = (SuspendedPayment) obj;
        return Objects.equals(activityUserId, other.activityUserId) && Objects.equals(additionalId, other.additionalId)
                && Objects.equals(addressSequenceNumber, other.addressSequenceNumber)
                && Objects.equals(bankAccountNumber, other.bankAccountNumber) && Objects.equals(batchId, other.batchId)
                && Objects.equals(businessGroup, other.businessGroup)
                && Objects.equals(businessGroupCode, other.businessGroupCode)
                && Objects.equals(controlBank, other.controlBank)
                && Objects.equals(creditCardNumber, other.creditCardNumber)
                && Objects.equals(creditCardType, other.creditCardType) && Objects.equals(currency, other.currency)
                && Objects.equals(depositBank, other.depositBank) && Objects.equals(depositDate, other.depositDate)
                && Objects.equals(distributionDate, other.distributionDate) && Objects.equals(dueDate, other.dueDate)
                && Objects.equals(holdSuspenseIndicator, other.holdSuspenseIndicator)
                && Objects.equals(identifier, other.identifier) && Objects.equals(identifierType, other.identifierType)
                && Objects.equals(overrideBusinessGroupEditIndicator, other.overrideBusinessGroupEditIndicator)
                && Objects.equals(payableItem, other.payableItem) && Objects.equals(paymentAmount, other.paymentAmount)
                && Objects.equals(paymentComments, other.paymentComments) && Objects.equals(paymentId, other.paymentId)
                && Objects.equals(paymentMethod, other.paymentMethod)
                && Objects.equals(paymentSequenceNumber, other.paymentSequenceNumber)
                && Objects.equals(paymentType, other.paymentType) && Objects.equals(payorId, other.payorId)
                && Objects.equals(payorName, other.payorName) && Objects.equals(policyNumber, other.policyNumber)
                && Objects.equals(policySymbol, other.policySymbol) && Objects.equals(postMarkDate, other.postMarkDate)
                && Objects.equals(postedDate, other.postedDate) && Objects.equals(status, other.status)
                && Objects.equals(suspenseReason, other.suspenseReason)
                && Objects.equals(transferToAgency, other.transferToAgency)
                && Objects.equals(transferToDirect, other.transferToDirect)
                && Objects.equals(transferToGroup, other.transferToGroup) && Objects.equals(userId, other.userId)
                && Objects.equals(userName, other.userName) && Objects.equals(walletIdentifier, other.walletIdentifier)
                && Objects.equals(walletNumber, other.walletNumber) && Objects.equals(walletType, other.walletType);
    }

}