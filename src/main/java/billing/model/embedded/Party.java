package billing.model.embedded;

import java.util.Date;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Party extends ResourceSupport {
    private String partyId;
    private String name;
    private String type;
    private String title;
    private List<String> agencies;
    private String agentNumber;
    private String externalId;
    private String groupId;
    private Boolean isOwner;
    @JsonProperty("addresses")
    private List<Address> addresses;
    private String emailId;
    private String phoneNumber;
    @JsonProperty("additionalNames")
    private List<AdditionalNameSearch> additionalNames;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private Date createdOn;
    private String pictureId;
    private Date timestamp;

    @JsonCreator
    public Party(@JsonProperty("partyId") String partyId, @JsonProperty("name") String name,
            @JsonProperty("type") String type, @JsonProperty("title") String title,
            @JsonProperty("agencies") List<String> agencies, @JsonProperty("agentNumber") String agentNumber,
            @JsonProperty("externalId") String externalId, @JsonProperty("groupId") String groupId,
            @JsonProperty("isOwner") Boolean isOwner, @JsonProperty("addresses") List<Address> addresses,
            @JsonProperty("emailId") String emailId, @JsonProperty("phoneNumber") String phoneNumber,
            @JsonProperty("additionalNames") List<AdditionalNameSearch> additionalNames,
            @JsonProperty("createdOn") Date createdOn, @JsonProperty("pictureId") String pictureId) {
        super();
        this.partyId = partyId;
        this.name = name;
        this.type = type;
        this.title = title;
        this.agencies = agencies;
        this.agentNumber = agentNumber;
        this.externalId = externalId;
        this.groupId = groupId;
        this.isOwner = isOwner;
        this.addresses = addresses;
        this.emailId = emailId;
        this.phoneNumber = phoneNumber;
        this.additionalNames = additionalNames;
        this.createdOn = createdOn;
        this.pictureId = pictureId;
    }

    public Party() {

    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getAgencies() {
        return agencies;
    }

    public void setAgencies(List<String> agencies) {
        this.agencies = agencies;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getAgentNumber() {
        return agentNumber;
    }

    public void setAgentNumber(String agentNumber) {
        this.agentNumber = agentNumber;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Boolean getIsOwner() {
        return isOwner;
    }

    public void setIsOwner(Boolean isOwner) {
        this.isOwner = isOwner;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public List<AdditionalNameSearch> getAdditionalNames() {
        return additionalNames;
    }

    public void setAdditionalNames(List<AdditionalNameSearch> additionalNames) {
        this.additionalNames = additionalNames;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((additionalNames == null) ? 0 : additionalNames.hashCode());
        result = prime * result + ((addresses == null) ? 0 : addresses.hashCode());
        result = prime * result + ((agencies == null) ? 0 : agencies.hashCode());
        result = prime * result + ((agentNumber == null) ? 0 : agentNumber.hashCode());
        result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());
        result = prime * result + ((externalId == null) ? 0 : externalId.hashCode());
        result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
        result = prime * result + ((isOwner == null) ? 0 : isOwner.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((partyId == null) ? 0 : partyId.hashCode());
        result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        result = prime * result + ((pictureId == null) ? 0 : pictureId.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Party other = (Party) obj;
        if (additionalNames == null) {
            if (other.additionalNames != null)
                return false;
        } else if (!additionalNames.equals(other.additionalNames))
            return false;
        if (addresses == null) {
            if (other.addresses != null)
                return false;
        } else if (!addresses.equals(other.addresses))
            return false;
        if (agencies == null) {
            if (other.agencies != null)
                return false;
        } else if (!agencies.equals(other.agencies))
            return false;
        if (agentNumber == null) {
            if (other.agentNumber != null)
                return false;
        } else if (!agentNumber.equals(other.agentNumber))
            return false;
        if (emailId == null) {
            if (other.emailId != null)
                return false;
        } else if (!emailId.equals(other.emailId))
            return false;
        if (externalId == null) {
            if (other.externalId != null)
                return false;
        } else if (!externalId.equals(other.externalId))
            return false;
        if (groupId == null) {
            if (other.groupId != null)
                return false;
        } else if (!groupId.equals(other.groupId))
            return false;
        if (isOwner == null) {
            if (other.isOwner != null)
                return false;
        } else if (!isOwner.equals(other.isOwner))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (partyId == null) {
            if (other.partyId != null)
                return false;
        } else if (!partyId.equals(other.partyId))
            return false;
        if (phoneNumber == null) {
            if (other.phoneNumber != null)
                return false;
        } else if (!phoneNumber.equals(other.phoneNumber))
            return false;
        if (pictureId == null) {
            if (other.pictureId != null)
                return false;
        } else if (!pictureId.equals(other.pictureId))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }
}