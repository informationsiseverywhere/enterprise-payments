package billing.model.embedded;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Address {
    private String addressType;
    private String tag;
    private String attentionLine;
    private String address;
    private Integer addressSequence;
    private Double latitude;
    private Double longitude;

    @JsonCreator
    public Address(@JsonProperty("addressType") String addressType, @JsonProperty("tag") String tag,
            @JsonProperty("attentionLine") String attentionLine, @JsonProperty("address") String address,
            @JsonProperty("addressSequence") Integer addressSequence, @JsonProperty("latitude") Double latitude,
            @JsonProperty("longitude") Double longitude) {
        super();
        this.addressType = addressType;
        this.tag = tag;
        this.attentionLine = attentionLine;
        this.address = address;
        this.addressSequence = addressSequence;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Address() {
        super();
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getAttentionLine() {
        return attentionLine;
    }

    public void setAttentionLine(String attentionLine) {
        this.attentionLine = attentionLine;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getAddressSequence() {
        return addressSequence;
    }

    public void setAddressSequence(Integer addressSequence) {
        this.addressSequence = addressSequence;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((addressSequence == null) ? 0 : addressSequence.hashCode());
        result = prime * result + ((addressType == null) ? 0 : addressType.hashCode());
        result = prime * result + ((attentionLine == null) ? 0 : attentionLine.hashCode());
        result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
        result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Address other = (Address) obj;
        if (address == null) {
            if (other.address != null)
                return false;
        } else if (!address.equals(other.address))
            return false;
        if (addressSequence == null) {
            if (other.addressSequence != null)
                return false;
        } else if (!addressSequence.equals(other.addressSequence))
            return false;
        if (addressType == null) {
            if (other.addressType != null)
                return false;
        } else if (!addressType.equals(other.addressType))
            return false;
        if (attentionLine == null) {
            if (other.attentionLine != null)
                return false;
        } else if (!attentionLine.equals(other.attentionLine))
            return false;
        if (latitude == null) {
            if (other.latitude != null)
                return false;
        } else if (!latitude.equals(other.latitude))
            return false;
        if (longitude == null) {
            if (other.longitude != null)
                return false;
        } else if (!longitude.equals(other.longitude))
            return false;
        return true;
    }
}