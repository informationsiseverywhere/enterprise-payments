package billing.model.embedded;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AdditionalNameSearch {

    private Integer additionalNameSequence;
    private String additionalNameType;
    private String additionalName;

    @JsonCreator
    public AdditionalNameSearch(@JsonProperty("additionalNameType") String additionalNameType,
            @JsonProperty("additionalName") String additionalName,
            @JsonProperty("additionalNameSequence") Integer additionalNameSequence) {
        super();
        this.additionalNameType = additionalNameType;
        this.additionalName = additionalName;
        this.additionalNameSequence = additionalNameSequence;
    }

    public AdditionalNameSearch() {
    }

    public String getAdditionalNameType() {
        return additionalNameType;
    }

    public void setAdditionalNameType(String additionalNameType) {
        this.additionalNameType = additionalNameType;
    }

    public String getAdditionalName() {
        return additionalName;
    }

    public void setAdditionalName(String additionalName) {
        this.additionalName = additionalName;
    }

    public Integer getAdditionalNameSequence() {
        return additionalNameSequence;
    }

    public void setAdditionalNameSequence(Integer additionalNameSequence) {
        this.additionalNameSequence = additionalNameSequence;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((additionalName == null) ? 0 : additionalName.hashCode());
        result = prime * result + ((additionalNameSequence == null) ? 0 : additionalNameSequence.hashCode());
        result = prime * result + ((additionalNameType == null) ? 0 : additionalNameType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AdditionalNameSearch other = (AdditionalNameSearch) obj;
        if (additionalName == null) {
            if (other.additionalName != null)
                return false;
        } else if (!additionalName.equals(other.additionalName))
            return false;
        if (additionalNameSequence == null) {
            if (other.additionalNameSequence != null)
                return false;
        } else if (!additionalNameSequence.equals(other.additionalNameSequence))
            return false;
        if (additionalNameType == null) {
            if (other.additionalNameType != null)
                return false;
        } else if (!additionalNameType.equals(other.additionalNameType))
            return false;
        return true;
    }
}
