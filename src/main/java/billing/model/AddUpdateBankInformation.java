package billing.model;

import java.time.ZonedDateTime;

public class AddUpdateBankInformation {
    
    private String bilAccountId;
    private String userId;
    private ZonedDateTime bilEntryDate;
    private String bilEntryNumber;
    private short bilEntrySeqNumber;
    private ZonedDateTime effectiveDate;
    
    public AddUpdateBankInformation(String bilAccountId, String userId, ZonedDateTime bilEntryDate,
            String bilEntryNumber, short bilEntrySeqNumber, ZonedDateTime effectiveDate) {
        super();
        this.bilAccountId = bilAccountId;
        this.userId = userId;
        this.bilEntryDate = bilEntryDate;
        this.bilEntryNumber = bilEntryNumber;
        this.bilEntrySeqNumber = bilEntrySeqNumber;
        this.effectiveDate = effectiveDate;
    }

    public AddUpdateBankInformation() {
        super();
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ZonedDateTime getBilEntryDate() {
        return bilEntryDate;
    }

    public void setBilEntryDate(ZonedDateTime bilEntryDate) {
        this.bilEntryDate = bilEntryDate;
    }

    public String getBilEntryNumber() {
        return bilEntryNumber;
    }

    public void setBilEntryNumber(String bilEntryNumber) {
        this.bilEntryNumber = bilEntryNumber;
    }

    public short getBilEntrySeqNumber() {
        return bilEntrySeqNumber;
    }

    public void setBilEntrySeqNumber(short bilEntrySeqNumber) {
        this.bilEntrySeqNumber = bilEntrySeqNumber;
    }

    public ZonedDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

}
