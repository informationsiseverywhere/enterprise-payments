package billing.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

public class CashApply implements Serializable {

    private static final long serialVersionUID = 1L;
    private String accountId = BLANK_STRING;
    private ZonedDateTime bilDtbDate;
    private short bilDtbSequenceNumber = SHORT_ZERO;
    private short bilDspSequenceNumber = SHORT_ZERO;
    private char paymentPriorityInd = BLANK_CHAR;
    private char cashOverpaymentInd = BLANK_CHAR;
    private String policyId = BLANK_STRING;
    private ZonedDateTime policyEffectiveDate;
    private ZonedDateTime cashApplyDate;
    private char premiumPayIndicator = BLANK_CHAR;
    private String creditPolicyId = BLANK_STRING;
    private boolean downPaymentDateSw = false;
    private String quoteIndicator = BLANK_STRING;
    private String applyReference = BLANK_STRING;
    private String userSequenceId = BLANK_STRING;

    public CashApply() {
        super();
    }

    public CashApply(String accountId, ZonedDateTime bilDtbDate, short bilDtbSequenceNumber, short bilDspSequenceNumber,
            char paymentPriorityInd, char cashOverpaymentInd, String policyId, ZonedDateTime policyEffectiveDate,
            ZonedDateTime cashApplyDate, char premiumPayIndicator, String creditPolicyId, boolean downPaymentDateSw,
            String quoteIndicator, String applyReference, String userSequenceId) {
        super();
        this.accountId = accountId;
        this.bilDtbDate = bilDtbDate;
        this.bilDtbSequenceNumber = bilDtbSequenceNumber;
        this.bilDspSequenceNumber = bilDspSequenceNumber;
        this.paymentPriorityInd = paymentPriorityInd;
        this.cashOverpaymentInd = cashOverpaymentInd;
        this.policyId = policyId;
        this.policyEffectiveDate = policyEffectiveDate;
        this.cashApplyDate = cashApplyDate;
        this.premiumPayIndicator = premiumPayIndicator;
        this.creditPolicyId = creditPolicyId;
        this.downPaymentDateSw = downPaymentDateSw;
        this.quoteIndicator = quoteIndicator;
        this.applyReference = applyReference;
        this.userSequenceId = userSequenceId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public ZonedDateTime getBilDtbDate() {
        return bilDtbDate;
    }

    public void setBilDtbDate(ZonedDateTime bilDtbDate) {
        this.bilDtbDate = bilDtbDate;
    }

    public short getBilDtbSequenceNumber() {
        return bilDtbSequenceNumber;
    }

    public void setBilDtbSequenceNumber(short bilDtbSequenceNumber) {
        this.bilDtbSequenceNumber = bilDtbSequenceNumber;
    }

    public short getBilDspSequenceNumber() {
        return bilDspSequenceNumber;
    }

    public void setBilDspSequenceNumber(short bilDspSequenceNumber) {
        this.bilDspSequenceNumber = bilDspSequenceNumber;
    }

    public char getPaymentPriorityInd() {
        return paymentPriorityInd;
    }

    public void setPaymentPriorityInd(char paymentPriorityInd) {
        this.paymentPriorityInd = paymentPriorityInd;
    }

    public char getCashOverpaymentInd() {
        return cashOverpaymentInd;
    }

    public void setCashOverpaymentInd(char cashOverpaymentInd) {
        this.cashOverpaymentInd = cashOverpaymentInd;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public ZonedDateTime getPolicyEffectiveDate() {
        return policyEffectiveDate;
    }

    public void setPolicyEffectiveDate(ZonedDateTime policyEffectiveDate) {
        this.policyEffectiveDate = policyEffectiveDate;
    }

    public ZonedDateTime getCashApplyDate() {
        return cashApplyDate;
    }

    public void setCashApplyDate(ZonedDateTime cashApplyDate) {
        this.cashApplyDate = cashApplyDate;
    }

    public char getPremiumPayIndicator() {
        return premiumPayIndicator;
    }

    public void setPremiumPayIndicator(char premiumPayIndicator) {
        this.premiumPayIndicator = premiumPayIndicator;
    }

    public String getCreditPolicyId() {
        return creditPolicyId;
    }

    public void setCreditPolicyId(String creditPolicyId) {
        this.creditPolicyId = creditPolicyId;
    }

    public boolean isDownPaymentDateSw() {
        return downPaymentDateSw;
    }

    public void setDownPaymentDateSw(boolean downPaymentDateSw) {
        this.downPaymentDateSw = downPaymentDateSw;
    }

    public String getQuoteIndicator() {
        return quoteIndicator;
    }

    public void setQuoteIndicator(String quoteIndicator) {
        this.quoteIndicator = quoteIndicator;
    }

    public String getApplyReference() {
        return applyReference;
    }

    public void setApplyReference(String applyReference) {
        this.applyReference = applyReference;
    }

    public String getUserSequenceId() {
        return userSequenceId;
    }

    public void setUserSequenceId(String userSequenceId) {
        this.userSequenceId = userSequenceId;
    }
    
    
}
