package billing.model;

import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReverseOption extends ResourceSupport {

    private String reason;
    private String depositDate;

    public ReverseOption(@JsonProperty("reason") String reason, @JsonProperty("depositDate") String depositDate) {
        super();
        this.reason = reason;
        this.depositDate = depositDate;
    }

    public ReverseOption() {
        super();
    }

    public ReverseOption(ReverseOption reverseOption) {
        this(reverseOption.getReason(), reverseOption.getDepositDate());
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(String depositDate) {
        this.depositDate = depositDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(depositDate, reason);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ReverseOption other = (ReverseOption) obj;
        return Objects.equals(depositDate, other.depositDate) && Objects.equals(reason, other.reason);
    }

}
