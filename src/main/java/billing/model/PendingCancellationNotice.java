package billing.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.DECIMAL_ZERO;

import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PendingCancellationNotice extends ResourceSupport {
    private String accountId = BLANK_STRING;
    private String accountNumber = BLANK_STRING;
    private String policyId = BLANK_STRING;
    private String polSymbolCd = BLANK_STRING;
    private String policyNumber = BLANK_STRING;
    private String polEffDt = BLANK_STRING;
    private String polExpDt = BLANK_STRING;
    private String issueSystemId = BLANK_STRING;
    private String reqPgmName = BLANK_STRING;
    private String polPcnReason = BLANK_STRING;
    private Character effectiveTypeCd = BLANK_CHAR;
    private String userId = BLANK_STRING;
    private String activityDt = BLANK_STRING;
    private Character cancelType = BLANK_CHAR;
    private String equityDt = BLANK_STRING;
    private String standCncDate = BLANK_STRING;
    private String lobCd = BLANK_STRING;
    private String masterCoNbr = BLANK_STRING;
    private String polPriRskStCd = BLANK_STRING;
    private Double pcnAmount = DECIMAL_ZERO;
    private Character autoColMthChg = BLANK_CHAR;
    private Character nsfActivityInd = BLANK_CHAR;
    private Character restartPcnInd = BLANK_CHAR;
    private String restartPcnDt = BLANK_STRING;
    private String bilTypeCd = BLANK_STRING;
    private String bilClassCd = BLANK_STRING;
    private String bilCollectionPln = BLANK_STRING;
    private String payCltId = BLANK_STRING;
    private Short payAdrSeq = Short.MIN_VALUE;
    private Character payAdrSeqSign = BLANK_CHAR;
    private String isuCltId = BLANK_STRING;
    private Short isuAdrSeq = Short.MIN_VALUE;
    private Character isuAdrSeqSign = BLANK_CHAR;
    private String agtCltId = BLANK_STRING;
    private Short agtAdrSeq = Short.MIN_VALUE;
    private Character agtAdrSeqSign = BLANK_CHAR;
    private String se3Dt = BLANK_STRING;
    private String fillerSe3DtTs = BLANK_STRING;
    private String customerInput = BLANK_STRING;
    private Character queueWrtInd = BLANK_CHAR;
    private Short ldnLegalDays = Short.MIN_VALUE;
    private Character ldnLegalDaysSign = BLANK_CHAR;
    private Short ldnMailDays = Short.MIN_VALUE;
    private Character ldnMailDaysSign = BLANK_CHAR;
    private Short ldnMaxDays = Short.MIN_VALUE;
    private Character ldnMaxDaysSign = BLANK_CHAR;
    private String calcLegDate = BLANK_STRING;
    private String pcnNoticeDate = BLANK_STRING;

    public PendingCancellationNotice() {
        super();
    }

    public PendingCancellationNotice(@JsonProperty("accountId") String accountId,
            @JsonProperty("accountNumber") String accountNumber, @JsonProperty("policyId") String policyId,
            @JsonProperty("polSymbolCd") String polSymbolCd, @JsonProperty("policyNumber") String policyNumber,
            @JsonProperty("polEffDt") String polEffDt, @JsonProperty("polExpDt") String polExpDt,
            @JsonProperty("issueSystemId") String issueSystemId, @JsonProperty("reqPgmName") String reqPgmName,
            @JsonProperty("polPcnReason") String polPcnReason,
            @JsonProperty("effectiveTypeCd") Character effectiveTypeCd, @JsonProperty("userId") String userId,
            @JsonProperty("activityDt") String activityDt, @JsonProperty("cancelType") Character cancelType,
            @JsonProperty("equityDt") String equityDt, @JsonProperty("standCncDate") String standCncDate,
            @JsonProperty("lobCd") String lobCd, @JsonProperty("masterCoNbr") String masterCoNbr,
            @JsonProperty("polPriRskStCd") String polPriRskStCd, @JsonProperty("pcnAmount") Double pcnAmount,
            @JsonProperty("autoColMthChg") Character autoColMthChg,
            @JsonProperty("nsfActivityInd") Character nsfActivityInd,
            @JsonProperty("restartPcnInd") Character restartPcnInd, @JsonProperty("restartPcnDt") String restartPcnDt,
            @JsonProperty("bilTypeCd") String bilTypeCd, @JsonProperty("bilClassCd") String bilClassCd,
            @JsonProperty("bilCollectionPln") String bilCollectionPln, @JsonProperty("payCltId") String payCltId,
            @JsonProperty("payAdrSeq") Short payAdrSeq, @JsonProperty("payAdrSeqSign") Character payAdrSeqSign,
            @JsonProperty("isuCltId") String isuCltId, @JsonProperty("isuAdrSeq") Short isuAdrSeq,
            @JsonProperty("isuAdrSeqSign") Character isuAdrSeqSign, @JsonProperty("agtCltId") String agtCltId,
            @JsonProperty("agtAdrSeq") Short agtAdrSeq, @JsonProperty("agtAdrSeqSign") Character agtAdrSeqSign,
            @JsonProperty("se3Dt") String se3Dt, @JsonProperty("fillerSe3DtTs") String fillerSe3DtTs,
            @JsonProperty("customerInput") String customerInput, @JsonProperty("queueWrtInd") Character queueWrtInd,
            @JsonProperty("ldnLegalDays") Short ldnLegalDays,
            @JsonProperty("ldnLegalDaysSign") Character ldnLegalDaysSign,
            @JsonProperty("ldnMailDays") Short ldnMailDays, @JsonProperty("ldnMailDaysSign") Character ldnMailDaysSign,
            @JsonProperty("ldnMaxDays") Short ldnMaxDays, @JsonProperty("ldnMaxDaysSign") Character ldnMaxDaysSign,
            @JsonProperty("calcLegDate") String calcLegDate, @JsonProperty("pcnNoticeDate") String pcnNoticeDate) {
        super();
        this.accountId = accountId;
        this.accountNumber = accountNumber;
        this.policyId = policyId;
        this.polSymbolCd = polSymbolCd;
        this.policyNumber = policyNumber;
        this.polEffDt = polEffDt;
        this.polExpDt = polExpDt;
        this.issueSystemId = issueSystemId;
        this.reqPgmName = reqPgmName;
        this.polPcnReason = polPcnReason;
        this.effectiveTypeCd = effectiveTypeCd;
        this.userId = userId;
        this.activityDt = activityDt;
        this.cancelType = cancelType;
        this.equityDt = equityDt;
        this.standCncDate = standCncDate;
        this.lobCd = lobCd;
        this.masterCoNbr = masterCoNbr;
        this.polPriRskStCd = polPriRskStCd;
        this.pcnAmount = pcnAmount;
        this.autoColMthChg = autoColMthChg;
        this.nsfActivityInd = nsfActivityInd;
        this.restartPcnInd = restartPcnInd;
        this.restartPcnDt = restartPcnDt;
        this.bilTypeCd = bilTypeCd;
        this.bilClassCd = bilClassCd;
        this.bilCollectionPln = bilCollectionPln;
        this.payCltId = payCltId;
        this.payAdrSeq = payAdrSeq;
        this.payAdrSeqSign = payAdrSeqSign;
        this.isuCltId = isuCltId;
        this.isuAdrSeq = isuAdrSeq;
        this.isuAdrSeqSign = isuAdrSeqSign;
        this.agtCltId = agtCltId;
        this.agtAdrSeq = agtAdrSeq;
        this.agtAdrSeqSign = agtAdrSeqSign;
        this.se3Dt = se3Dt;
        this.fillerSe3DtTs = fillerSe3DtTs;
        this.customerInput = customerInput;
        this.queueWrtInd = queueWrtInd;
        this.ldnLegalDays = ldnLegalDays;
        this.ldnLegalDaysSign = ldnLegalDaysSign;
        this.ldnMailDays = ldnMailDays;
        this.ldnMailDaysSign = ldnMailDaysSign;
        this.ldnMaxDays = ldnMaxDays;
        this.ldnMaxDaysSign = ldnMaxDaysSign;
        this.calcLegDate = calcLegDate;
        this.pcnNoticeDate = pcnNoticeDate;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolSymbolCd() {
        return polSymbolCd;
    }

    public void setPolSymbolCd(String polSymbolCd) {
        this.polSymbolCd = polSymbolCd;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolEffDt() {
        return polEffDt;
    }

    public void setPolEffDt(String polEffDt) {
        this.polEffDt = polEffDt;
    }

    public String getPolExpDt() {
        return polExpDt;
    }

    public void setPolExpDt(String polExpDt) {
        this.polExpDt = polExpDt;
    }

    public String getIssueSystemId() {
        return issueSystemId;
    }

    public void setIssueSystemId(String issueSystemId) {
        this.issueSystemId = issueSystemId;
    }

    public String getReqPgmName() {
        return reqPgmName;
    }

    public void setReqPgmName(String reqPgmName) {
        this.reqPgmName = reqPgmName;
    }

    public String getPolPcnReason() {
        return polPcnReason;
    }

    public void setPolPcnReason(String polPcnReason) {
        this.polPcnReason = polPcnReason;
    }

    public Character getEffectiveTypeCd() {
        return effectiveTypeCd;
    }

    public void setEffectiveTypeCd(Character effectiveTypeCd) {
        this.effectiveTypeCd = effectiveTypeCd;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getActivityDt() {
        return activityDt;
    }

    public void setActivityDt(String activityDt) {
        this.activityDt = activityDt;
    }

    public Character getCancelType() {
        return cancelType;
    }

    public void setCancelType(Character cancelType) {
        this.cancelType = cancelType;
    }

    public String getEquityDt() {
        return equityDt;
    }

    public void setEquityDt(String equityDt) {
        this.equityDt = equityDt;
    }

    public String getStandCncDate() {
        return standCncDate;
    }

    public void setStandCncDate(String standCncDate) {
        this.standCncDate = standCncDate;
    }

    public String getLobCd() {
        return lobCd;
    }

    public void setLobCd(String lobCd) {
        this.lobCd = lobCd;
    }

    public String getMasterCoNbr() {
        return masterCoNbr;
    }

    public void setMasterCoNbr(String masterCoNbr) {
        this.masterCoNbr = masterCoNbr;
    }

    public String getPolPriRskStCd() {
        return polPriRskStCd;
    }

    public void setPolPriRskStCd(String polPriRskStCd) {
        this.polPriRskStCd = polPriRskStCd;
    }

    public Double getPcnAmount() {
        return pcnAmount;
    }

    public void setPcnAmount(Double pcnAmount) {
        this.pcnAmount = pcnAmount;
    }

    public Character getAutoColMthChg() {
        return autoColMthChg;
    }

    public void setAutoColMthChg(Character autoColMthChg) {
        this.autoColMthChg = autoColMthChg;
    }

    public Character getNsfActivityInd() {
        return nsfActivityInd;
    }

    public void setNsfActivityInd(Character nsfActivityInd) {
        this.nsfActivityInd = nsfActivityInd;
    }

    public Character getRestartPcnInd() {
        return restartPcnInd;
    }

    public void setRestartPcnInd(Character restartPcnInd) {
        this.restartPcnInd = restartPcnInd;
    }

    public String getRestartPcnDt() {
        return restartPcnDt;
    }

    public void setRestartPcnDt(String restartPcnDt) {
        this.restartPcnDt = restartPcnDt;
    }

    public String getBilTypeCd() {
        return bilTypeCd;
    }

    public void setBilTypeCd(String bilTypeCd) {
        this.bilTypeCd = bilTypeCd;
    }

    public String getBilClassCd() {
        return bilClassCd;
    }

    public void setBilClassCd(String bilClassCd) {
        this.bilClassCd = bilClassCd;
    }

    public String getBilCollectionPln() {
        return bilCollectionPln;
    }

    public void setBilCollectionPln(String bilCollectionPln) {
        this.bilCollectionPln = bilCollectionPln;
    }

    public String getPayCltId() {
        return payCltId;
    }

    public void setPayCltId(String payCltId) {
        this.payCltId = payCltId;
    }

    public Short getPayAdrSeq() {
        return payAdrSeq;
    }

    public void setPayAdrSeq(Short payAdrSeq) {
        this.payAdrSeq = payAdrSeq;
    }

    public Character getPayAdrSeqSign() {
        return payAdrSeqSign;
    }

    public void setPayAdrSeqSign(Character payAdrSeqSign) {
        this.payAdrSeqSign = payAdrSeqSign;
    }

    public String getIsuCltId() {
        return isuCltId;
    }

    public void setIsuCltId(String isuCltId) {
        this.isuCltId = isuCltId;
    }

    public Short getIsuAdrSeq() {
        return isuAdrSeq;
    }

    public void setIsuAdrSeq(Short isuAdrSeq) {
        this.isuAdrSeq = isuAdrSeq;
    }

    public Character getIsuAdrSeqSign() {
        return isuAdrSeqSign;
    }

    public void setIsuAdrSeqSign(Character isuAdrSeqSign) {
        this.isuAdrSeqSign = isuAdrSeqSign;
    }

    public String getAgtCltId() {
        return agtCltId;
    }

    public void setAgtCltId(String agtCltId) {
        this.agtCltId = agtCltId;
    }

    public Short getAgtAdrSeq() {
        return agtAdrSeq;
    }

    public void setAgtAdrSeq(Short agtAdrSeq) {
        this.agtAdrSeq = agtAdrSeq;
    }

    public Character getAgtAdrSeqSign() {
        return agtAdrSeqSign;
    }

    public void setAgtAdrSeqSign(Character agtAdrSeqSign) {
        this.agtAdrSeqSign = agtAdrSeqSign;
    }

    public String getSe3Dt() {
        return se3Dt;
    }

    public void setSe3Dt(String se3Dt) {
        this.se3Dt = se3Dt;
    }

    public String getFillerSe3DtTs() {
        return fillerSe3DtTs;
    }

    public void setFillerSe3DtTs(String fillerSe3DtTs) {
        this.fillerSe3DtTs = fillerSe3DtTs;
    }

    public String getCustomerInput() {
        return customerInput;
    }

    public void setCustomerInput(String customerInput) {
        this.customerInput = customerInput;
    }

    public Character getQueueWrtInd() {
        return queueWrtInd;
    }

    public void setQueueWrtInd(Character queueWrtInd) {
        this.queueWrtInd = queueWrtInd;
    }

    public Short getLdnLegalDays() {
        return ldnLegalDays;
    }

    public void setLdnLegalDays(Short ldnLegalDays) {
        this.ldnLegalDays = ldnLegalDays;
    }

    public Character getLdnLegalDaysSign() {
        return ldnLegalDaysSign;
    }

    public void setLdnLegalDaysSign(Character ldnLegalDaysSign) {
        this.ldnLegalDaysSign = ldnLegalDaysSign;
    }

    public Short getLdnMailDays() {
        return ldnMailDays;
    }

    public void setLdnMailDays(Short ldnMailDays) {
        this.ldnMailDays = ldnMailDays;
    }

    public Character getLdnMailDaysSign() {
        return ldnMailDaysSign;
    }

    public void setLdnMailDaysSign(Character ldnMailDaysSign) {
        this.ldnMailDaysSign = ldnMailDaysSign;
    }

    public Short getLdnMaxDays() {
        return ldnMaxDays;
    }

    public void setLdnMaxDays(Short ldnMaxDays) {
        this.ldnMaxDays = ldnMaxDays;
    }

    public Character getLdnMaxDaysSign() {
        return ldnMaxDaysSign;
    }

    public void setLdnMaxDaysSign(Character ldnMaxDaysSign) {
        this.ldnMaxDaysSign = ldnMaxDaysSign;
    }

    public String getCalcLegDate() {
        return calcLegDate;
    }

    public void setCalcLegDate(String calcLegDate) {
        this.calcLegDate = calcLegDate;
    }

    public String getPcnNoticeDate() {
        return pcnNoticeDate;
    }

    public void setPcnNoticeDate(String pcnNoticeDate) {
        this.pcnNoticeDate = pcnNoticeDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountId, accountNumber, activityDt, agtAdrSeq, agtAdrSeqSign, agtCltId,
                autoColMthChg, bilClassCd, bilCollectionPln, bilTypeCd, calcLegDate, cancelType, customerInput,
                effectiveTypeCd, equityDt, fillerSe3DtTs, issueSystemId, isuAdrSeq, isuAdrSeqSign, isuCltId,
                ldnLegalDays, ldnLegalDaysSign, ldnMailDays, ldnMailDaysSign, ldnMaxDays, ldnMaxDaysSign, lobCd,
                masterCoNbr, nsfActivityInd, payAdrSeq, payAdrSeqSign, payCltId, pcnAmount, pcnNoticeDate, polEffDt,
                polExpDt, polPcnReason, polPriRskStCd, polSymbolCd, policyId, policyNumber, queueWrtInd, reqPgmName,
                restartPcnDt, restartPcnInd, se3Dt, standCncDate, userId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PendingCancellationNotice other = (PendingCancellationNotice) obj;
        return Objects.equals(accountId, other.accountId) && Objects.equals(accountNumber, other.accountNumber)
                && Objects.equals(activityDt, other.activityDt) && Objects.equals(agtAdrSeq, other.agtAdrSeq)
                && Objects.equals(agtAdrSeqSign, other.agtAdrSeqSign) && Objects.equals(agtCltId, other.agtCltId)
                && Objects.equals(autoColMthChg, other.autoColMthChg) && Objects.equals(bilClassCd, other.bilClassCd)
                && Objects.equals(bilCollectionPln, other.bilCollectionPln)
                && Objects.equals(bilTypeCd, other.bilTypeCd) && Objects.equals(calcLegDate, other.calcLegDate)
                && Objects.equals(cancelType, other.cancelType) && Objects.equals(customerInput, other.customerInput)
                && Objects.equals(effectiveTypeCd, other.effectiveTypeCd) && Objects.equals(equityDt, other.equityDt)
                && Objects.equals(fillerSe3DtTs, other.fillerSe3DtTs)
                && Objects.equals(issueSystemId, other.issueSystemId) && Objects.equals(isuAdrSeq, other.isuAdrSeq)
                && Objects.equals(isuAdrSeqSign, other.isuAdrSeqSign) && Objects.equals(isuCltId, other.isuCltId)
                && Objects.equals(ldnLegalDays, other.ldnLegalDays)
                && Objects.equals(ldnLegalDaysSign, other.ldnLegalDaysSign)
                && Objects.equals(ldnMailDays, other.ldnMailDays)
                && Objects.equals(ldnMailDaysSign, other.ldnMailDaysSign)
                && Objects.equals(ldnMaxDays, other.ldnMaxDays) && Objects.equals(ldnMaxDaysSign, other.ldnMaxDaysSign)
                && Objects.equals(lobCd, other.lobCd) && Objects.equals(masterCoNbr, other.masterCoNbr)
                && Objects.equals(nsfActivityInd, other.nsfActivityInd) && Objects.equals(payAdrSeq, other.payAdrSeq)
                && Objects.equals(payAdrSeqSign, other.payAdrSeqSign) && Objects.equals(payCltId, other.payCltId)
                && Objects.equals(pcnAmount, other.pcnAmount) && Objects.equals(pcnNoticeDate, other.pcnNoticeDate)
                && Objects.equals(polEffDt, other.polEffDt) && Objects.equals(polExpDt, other.polExpDt)
                && Objects.equals(polPcnReason, other.polPcnReason)
                && Objects.equals(polPriRskStCd, other.polPriRskStCd) && Objects.equals(polSymbolCd, other.polSymbolCd)
                && Objects.equals(policyId, other.policyId) && Objects.equals(policyNumber, other.policyNumber)
                && Objects.equals(queueWrtInd, other.queueWrtInd) && Objects.equals(reqPgmName, other.reqPgmName)
                && Objects.equals(restartPcnDt, other.restartPcnDt)
                && Objects.equals(restartPcnInd, other.restartPcnInd) && Objects.equals(se3Dt, other.se3Dt)
                && Objects.equals(standCncDate, other.standCncDate) && Objects.equals(userId, other.userId);
    }
}
