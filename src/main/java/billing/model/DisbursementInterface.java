package billing.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.AppDateDeserializer;
import core.utils.AppDateSerializer;
import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DisbursementInterface extends ResourceSupport {

    private String application = BLANK_STRING;
    private String callingProgramName = BLANK_STRING;
    private String functionValue = BLANK_STRING;
    private String userId = BLANK_STRING;
    private String referenceId = BLANK_STRING;
    private String businessCaseId = BLANK_STRING;
    private String subFunction = BLANK_STRING;
    private String techDisburId = BLANK_STRING;
    private String userDisburId = BLANK_STRING;
    @JsonSerialize(using = AppDateSerializer.class)
    @JsonDeserialize(using = AppDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime disburAcyTimestamp;
    private String disburTypeCode = BLANK_STRING;
    private String disburMediumCode = BLANK_STRING;
    private String processLocCode = BLANK_STRING;
    private String masterCompanyNumber = BLANK_STRING;
    private String riskStateCode = BLANK_STRING;
    private int checkNumber;
    private String billAccountAdditionId = BLANK_STRING;
    private char prevDisburStatusCode = BLANK_CHAR;
    private char disburStatusCode = BLANK_CHAR;
    private String reasonDescription = BLANK_STRING;
    private String userMemoDescription = BLANK_STRING;
    private String printLocCode = BLANK_STRING;
    private String originatorId = BLANK_STRING;
    private short originatorAddressNumber = Short.MIN_VALUE;
    private double disburAmount;
    private String currencyCode = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime checkDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime disburScheduleDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime disburActualDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime disburHonoredDate;
    private String polSymbolCode = BLANK_STRING;
    private String policyNumber = BLANK_STRING;
    private String lineBusinessCode = BLANK_STRING;
    private String productCode = BLANK_STRING;
    private int bankNumber;
    private String bankRouteTrasitNumber = BLANK_STRING;
    private String bankFracRouteNumber = BLANK_STRING;
    private String bankAccount = BLANK_STRING;
    private String bankLongName = BLANK_STRING;
    private String firstBankAddress = BLANK_STRING;
    private String secondBankAddress = BLANK_STRING;
    private String bankCityName = BLANK_STRING;
    private String bankStateCode = BLANK_STRING;
    private String bankPostCode = BLANK_STRING;
    private String bankAssociateGroupId = BLANK_STRING;
    private List<Object> payeeClientsList;
    private String payeePrefix = BLANK_STRING;
    private String payeeFirstName = BLANK_STRING;
    private String payeeLastName = BLANK_STRING;
    private String payeeSuffix = BLANK_STRING;
    private String payeeLongName = BLANK_STRING;
    private String firstPayeeAddress = BLANK_STRING;
    private String secondPayeeAddress = BLANK_STRING;
    private String payeeCityName = BLANK_STRING;
    private String payeeStateCode = BLANK_STRING;
    private String payeePostCode = BLANK_STRING;
    private String mailToId = BLANK_STRING;
    private short mailToAddressNumber;
    private String mailToPrefix = BLANK_STRING;
    private String mailToFirstName = BLANK_STRING;
    private String mailToLastName = BLANK_STRING;
    private String mailToSuffix = BLANK_STRING;
    private String mailToLongName = BLANK_STRING;
    private String firstMailToAddress = BLANK_STRING;
    private String secondMailToAddress = BLANK_STRING;
    private String mailToCityName = BLANK_STRING;
    private String mailToStateCode = BLANK_STRING;
    private String mailToPostCode = BLANK_STRING;
    private String insurAgentId = BLANK_STRING;
    private short insurAgentAddressNumber;
    private String insAgentPrefix = BLANK_STRING;
    private String insAgentFirstName = BLANK_STRING;
    private String insAgentLastName = BLANK_STRING;
    private String insAgentSuffix = BLANK_STRING;
    private String insAgentLongName = BLANK_STRING;
    private String firstInsAgentAddress = BLANK_STRING;
    private String secondInsAgentAddress = BLANK_STRING;
    private String insAgentCityName = BLANK_STRING;
    private String insAgentStateCode = BLANK_STRING;
    private String insAgentPostCode = BLANK_STRING;
    private String namedInsId = BLANK_STRING;
    private short namedInsAddressNumber;
    private String namedInsPrefix = BLANK_STRING;
    private String namedInsFirstName = BLANK_STRING;
    private String namedInsLastName = BLANK_STRING;
    private String namedInsSuffix = BLANK_STRING;
    private String namedInsLongName = BLANK_STRING;
    private String firstNamedInsAddress = BLANK_STRING;
    private String secondNamedInsAddress = BLANK_STRING;
    private String namedInsCityName = BLANK_STRING;
    private String namedInsStateCode = BLANK_STRING;
    private String namedInsPostCode = BLANK_STRING;
    private String firstFiller = BLANK_STRING;
    private String secondFiller = BLANK_STRING;
    private char queueWriteIndicator = BLANK_CHAR;
    private String programName = BLANK_STRING;
    private String parameterName = BLANK_STRING;

    public DisbursementInterface() {
        super();
    }

    @JsonCreator
    public DisbursementInterface(@JsonProperty("application") String application,
            @JsonProperty("callingProgramName") String callingProgramName,
            @JsonProperty("functionValue") String functionValue, @JsonProperty("userId") String userId,
            @JsonProperty("referenceId") String referenceId, @JsonProperty("businessCaseId") String businessCaseId,
            @JsonProperty("subFunction") String subFunction, @JsonProperty("techDisburId") String techDisburId,
            @JsonProperty("userDisburId") String userDisburId,
            @JsonProperty("disburAcyTimestamp") ZonedDateTime disburAcyTimestamp,
            @JsonProperty("disburTypeCode") String disburTypeCode,
            @JsonProperty("disburMediumCode") String disburMediumCode,
            @JsonProperty("processLocCode") String processLocCode,
            @JsonProperty("masterCompanyNumber") String masterCompanyNumber,
            @JsonProperty("riskStateCode") String riskStateCode, @JsonProperty("checkNumber") int checkNumber,
            @JsonProperty("billAccountAdditionId") String billAccountAdditionId,
            @JsonProperty("prevDisburStatusCode") char prevDisburStatusCode,
            @JsonProperty("disburStatusCode") char disburStatusCode,
            @JsonProperty("reasonDescription") String reasonDescription,
            @JsonProperty("userMemoDescription") String userMemoDescription,
            @JsonProperty("printLocCode") String printLocCode, @JsonProperty("originatorId") String originatorId,
            @JsonProperty("originatorAddressNumber") short originatorAddressNumber,
            @JsonProperty("disburAmount") double disburAmount, @JsonProperty("currencyCode") String currencyCode,
            @JsonProperty("checkDate") ZonedDateTime checkDate,
            @JsonProperty("disburScheduleDate") ZonedDateTime disburScheduleDate,
            @JsonProperty("disburActualDate") ZonedDateTime disburActualDate,
            @JsonProperty("disburHonoredDate") ZonedDateTime disburHonoredDate,
            @JsonProperty("polSymbolCode") String polSymbolCode, @JsonProperty("policyNumber") String policyNumber,
            @JsonProperty("lineBusinessCode") String lineBusinessCode, @JsonProperty("productCode") String productCode,
            @JsonProperty("bankNumber") int bankNumber,
            @JsonProperty("bankRouteTrasitNumber") String bankRouteTrasitNumber,
            @JsonProperty("bankFracRouteNumber") String bankFracRouteNumber,
            @JsonProperty("bankAccount") String bankAccount, @JsonProperty("bankLongName") String bankLongName,
            @JsonProperty("firstBankAddress") String firstBankAddress,
            @JsonProperty("secondBankAddress") String secondBankAddress,
            @JsonProperty("bankCityName") String bankCityName, @JsonProperty("bankStateCode") String bankStateCode,
            @JsonProperty("bankPostCode") String bankPostCode,
            @JsonProperty("bankAssociateGroupId") String bankAssociateGroupId,
            @JsonProperty("payeeClientsList") List<Object> payeeClientsList,
            @JsonProperty("payeePrefix") String payeePrefix, @JsonProperty("payeeFirstName") String payeeFirstName,
            @JsonProperty("payeeLastName") String payeeLastName, @JsonProperty("payeeSuffix") String payeeSuffix,
            @JsonProperty("payeeLongName") String payeeLongName,
            @JsonProperty("firstPayeeAddress") String firstPayeeAddress,
            @JsonProperty("secondPayeeAddress") String secondPayeeAddress,
            @JsonProperty("payeeCityName") String payeeCityName, @JsonProperty("payeeStateCode") String payeeStateCode,
            @JsonProperty("payeePostCode") String payeePostCode, @JsonProperty("mailToId") String mailToId,
            @JsonProperty("mailToAddressNumber") short mailToAddressNumber,
            @JsonProperty("mailToPrefix") String mailToPrefix, @JsonProperty("mailToFirstName") String mailToFirstName,
            @JsonProperty("mailToLastName") String mailToLastName, @JsonProperty("mailToSuffix") String mailToSuffix,
            @JsonProperty("mailToLongName") String mailToLongName,
            @JsonProperty("firstMailToAddress") String firstMailToAddress,
            @JsonProperty("secondMailToAddress") String secondMailToAddress,
            @JsonProperty("mailToCityName") String mailToCityName,
            @JsonProperty("mailToStateCode") String mailToStateCode,
            @JsonProperty("mailToPostCode") String mailToPostCode, @JsonProperty("insurAgentId") String insurAgentId,
            @JsonProperty("insurAgentAddressNumber") short insurAgentAddressNumber,
            @JsonProperty("insAgentPrefix") String insAgentPrefix,
            @JsonProperty("insAgentFirstName") String insAgentFirstName,
            @JsonProperty("insAgentLastName") String insAgentLastName,
            @JsonProperty("insAgentSuffix") String insAgentSuffix,
            @JsonProperty("insAgentLongName") String insAgentLongName,
            @JsonProperty("firstInsAgentAddress") String firstInsAgentAddress,
            @JsonProperty("secondInsAgentAddress") String secondInsAgentAddress,
            @JsonProperty("insAgentCityName") String insAgentCityName,
            @JsonProperty("insAgentStateCode") String insAgentStateCode,
            @JsonProperty("insAgentPostCode") String insAgentPostCode, @JsonProperty("namedInsId") String namedInsId,
            @JsonProperty("namedInsAddressNumber") short namedInsAddressNumber,
            @JsonProperty("namedInsPrefix") String namedInsPrefix,
            @JsonProperty("namedInsFirstName") String namedInsFirstName,
            @JsonProperty("namedInsLastName") String namedInsLastName,
            @JsonProperty("namedInsSuffix") String namedInsSuffix,
            @JsonProperty("namedInsLongName") String namedInsLongName,
            @JsonProperty("firstNamedInsAddress") String firstNamedInsAddress,
            @JsonProperty("secondNamedInsAddress") String secondNamedInsAddress,
            @JsonProperty("namedInsCityName") String namedInsCityName,
            @JsonProperty("namedInsStateCode") String namedInsStateCode,
            @JsonProperty("namedInsPostCode") String namedInsPostCode, @JsonProperty("firstFiller") String firstFiller,
            @JsonProperty("secondFiller") String secondFiller,
            @JsonProperty("queueWriteIndicator") char queueWriteIndicator,
            @JsonProperty("programName") String programName, @JsonProperty("parameterName") String parameterName) {
        super();
        this.application = application;
        this.callingProgramName = callingProgramName;
        this.functionValue = functionValue;
        this.userId = userId;
        this.referenceId = referenceId;
        this.businessCaseId = businessCaseId;
        this.subFunction = subFunction;
        this.techDisburId = techDisburId;
        this.userDisburId = userDisburId;
        this.disburAcyTimestamp = disburAcyTimestamp;
        this.disburTypeCode = disburTypeCode;
        this.disburMediumCode = disburMediumCode;
        this.processLocCode = processLocCode;
        this.masterCompanyNumber = masterCompanyNumber;
        this.riskStateCode = riskStateCode;
        this.checkNumber = checkNumber;
        this.billAccountAdditionId = billAccountAdditionId;
        this.prevDisburStatusCode = prevDisburStatusCode;
        this.disburStatusCode = disburStatusCode;
        this.reasonDescription = reasonDescription;
        this.userMemoDescription = userMemoDescription;
        this.printLocCode = printLocCode;
        this.originatorId = originatorId;
        this.originatorAddressNumber = originatorAddressNumber;
        this.disburAmount = disburAmount;
        this.currencyCode = currencyCode;
        this.checkDate = checkDate;
        this.disburScheduleDate = disburScheduleDate;
        this.disburActualDate = disburActualDate;
        this.disburHonoredDate = disburHonoredDate;
        this.polSymbolCode = polSymbolCode;
        this.policyNumber = policyNumber;
        this.lineBusinessCode = lineBusinessCode;
        this.productCode = productCode;
        this.bankNumber = bankNumber;
        this.bankRouteTrasitNumber = bankRouteTrasitNumber;
        this.bankFracRouteNumber = bankFracRouteNumber;
        this.bankAccount = bankAccount;
        this.bankLongName = bankLongName;
        this.firstBankAddress = firstBankAddress;
        this.secondBankAddress = secondBankAddress;
        this.bankCityName = bankCityName;
        this.bankStateCode = bankStateCode;
        this.bankPostCode = bankPostCode;
        this.bankAssociateGroupId = bankAssociateGroupId;
        this.payeeClientsList = payeeClientsList;
        this.payeePrefix = payeePrefix;
        this.payeeFirstName = payeeFirstName;
        this.payeeLastName = payeeLastName;
        this.payeeSuffix = payeeSuffix;
        this.payeeLongName = payeeLongName;
        this.firstPayeeAddress = firstPayeeAddress;
        this.secondPayeeAddress = secondPayeeAddress;
        this.payeeCityName = payeeCityName;
        this.payeeStateCode = payeeStateCode;
        this.payeePostCode = payeePostCode;
        this.mailToId = mailToId;
        this.mailToAddressNumber = mailToAddressNumber;
        this.mailToPrefix = mailToPrefix;
        this.mailToFirstName = mailToFirstName;
        this.mailToLastName = mailToLastName;
        this.mailToSuffix = mailToSuffix;
        this.mailToLongName = mailToLongName;
        this.firstMailToAddress = firstMailToAddress;
        this.secondMailToAddress = secondMailToAddress;
        this.mailToCityName = mailToCityName;
        this.mailToStateCode = mailToStateCode;
        this.mailToPostCode = mailToPostCode;
        this.insurAgentId = insurAgentId;
        this.insurAgentAddressNumber = insurAgentAddressNumber;
        this.insAgentPrefix = insAgentPrefix;
        this.insAgentFirstName = insAgentFirstName;
        this.insAgentLastName = insAgentLastName;
        this.insAgentSuffix = insAgentSuffix;
        this.insAgentLongName = insAgentLongName;
        this.firstInsAgentAddress = firstInsAgentAddress;
        this.secondInsAgentAddress = secondInsAgentAddress;
        this.insAgentCityName = insAgentCityName;
        this.insAgentStateCode = insAgentStateCode;
        this.insAgentPostCode = insAgentPostCode;
        this.namedInsId = namedInsId;
        this.namedInsAddressNumber = namedInsAddressNumber;
        this.namedInsPrefix = namedInsPrefix;
        this.namedInsFirstName = namedInsFirstName;
        this.namedInsLastName = namedInsLastName;
        this.namedInsSuffix = namedInsSuffix;
        this.namedInsLongName = namedInsLongName;
        this.firstNamedInsAddress = firstNamedInsAddress;
        this.secondNamedInsAddress = secondNamedInsAddress;
        this.namedInsCityName = namedInsCityName;
        this.namedInsStateCode = namedInsStateCode;
        this.namedInsPostCode = namedInsPostCode;
        this.firstFiller = firstFiller;
        this.secondFiller = secondFiller;
        this.queueWriteIndicator = queueWriteIndicator;
        this.programName = programName;
        this.parameterName = parameterName;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getCallingProgramName() {
        return callingProgramName;
    }

    public void setCallingProgramName(String callingProgramName) {
        this.callingProgramName = callingProgramName;
    }

    public String getFunctionValue() {
        return functionValue;
    }

    public void setFunctionValue(String functionValue) {
        this.functionValue = functionValue;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getBusinessCaseId() {
        return businessCaseId;
    }

    public void setBusinessCaseId(String businessCaseId) {
        this.businessCaseId = businessCaseId;
    }

    public String getSubFunction() {
        return subFunction;
    }

    public void setSubFunction(String subFunction) {
        this.subFunction = subFunction;
    }

    public String getTechDisburId() {
        return techDisburId;
    }

    public void setTechDisburId(String techDisburId) {
        this.techDisburId = techDisburId;
    }

    public String getUserDisburId() {
        return userDisburId;
    }

    public void setUserDisburId(String userDisburId) {
        this.userDisburId = userDisburId;
    }

    public ZonedDateTime getDisburAcyTimestamp() {
        return disburAcyTimestamp;
    }

    public void setDisburAcyTimestamp(ZonedDateTime disburAcyTimestamp) {
        this.disburAcyTimestamp = disburAcyTimestamp;
    }

    public String getDisburTypeCode() {
        return disburTypeCode;
    }

    public void setDisburTypeCode(String disburTypeCode) {
        this.disburTypeCode = disburTypeCode;
    }

    public String getDisburMediumCode() {
        return disburMediumCode;
    }

    public void setDisburMediumCode(String disburMediumCode) {
        this.disburMediumCode = disburMediumCode;
    }

    public String getProcessLocCode() {
        return processLocCode;
    }

    public void setProcessLocCode(String processLocCode) {
        this.processLocCode = processLocCode;
    }

    public String getMasterCompanyNumber() {
        return masterCompanyNumber;
    }

    public void setMasterCompanyNumber(String masterCompanyNumber) {
        this.masterCompanyNumber = masterCompanyNumber;
    }

    public String getRiskStateCode() {
        return riskStateCode;
    }

    public void setRiskStateCode(String riskStateCode) {
        this.riskStateCode = riskStateCode;
    }

    public int getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(int checkNumber) {
        this.checkNumber = checkNumber;
    }

    public String getBillAccountAdditionId() {
        return billAccountAdditionId;
    }

    public void setBillAccountAdditionId(String billAccountAdditionId) {
        this.billAccountAdditionId = billAccountAdditionId;
    }

    public char getPrevDisburStatusCode() {
        return prevDisburStatusCode;
    }

    public void setPrevDisburStatusCode(char prevDisburStatusCode) {
        this.prevDisburStatusCode = prevDisburStatusCode;
    }

    public char getDisburStatusCode() {
        return disburStatusCode;
    }

    public void setDisburStatusCode(char disburStatusCode) {
        this.disburStatusCode = disburStatusCode;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getUserMemoDescription() {
        return userMemoDescription;
    }

    public void setUserMemoDescription(String userMemoDescription) {
        this.userMemoDescription = userMemoDescription;
    }

    public String getPrintLocCode() {
        return printLocCode;
    }

    public void setPrintLocCode(String printLocCode) {
        this.printLocCode = printLocCode;
    }

    public String getOriginatorId() {
        return originatorId;
    }

    public void setOriginatorId(String originatorId) {
        this.originatorId = originatorId;
    }

    public short getOriginatorAddressNumber() {
        return originatorAddressNumber;
    }

    public void setOriginatorAddressNumber(short originatorAddressNumber) {
        this.originatorAddressNumber = originatorAddressNumber;
    }

    public double getDisburAmount() {
        return disburAmount;
    }

    public void setDisburAmount(double disburAmount) {
        this.disburAmount = disburAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public ZonedDateTime getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(ZonedDateTime checkDate) {
        this.checkDate = checkDate;
    }

    public ZonedDateTime getDisburScheduleDate() {
        return disburScheduleDate;
    }

    public void setDisburScheduleDate(ZonedDateTime disburScheduleDate) {
        this.disburScheduleDate = disburScheduleDate;
    }

    public ZonedDateTime getDisburActualDate() {
        return disburActualDate;
    }

    public void setDisburActualDate(ZonedDateTime disburActualDate) {
        this.disburActualDate = disburActualDate;
    }

    public ZonedDateTime getDisburHonoredDate() {
        return disburHonoredDate;
    }

    public void setDisburHonoredDate(ZonedDateTime disburHonoredDate) {
        this.disburHonoredDate = disburHonoredDate;
    }

    public String getPolSymbolCode() {
        return polSymbolCode;
    }

    public void setPolSymbolCode(String polSymbolCode) {
        this.polSymbolCode = polSymbolCode;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getLineBusinessCode() {
        return lineBusinessCode;
    }

    public void setLineBusinessCode(String lineBusinessCode) {
        this.lineBusinessCode = lineBusinessCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public int getBankNumber() {
        return bankNumber;
    }

    public void setBankNumber(int bankNumber) {
        this.bankNumber = bankNumber;
    }

    public String getBankRouteTrasitNumber() {
        return bankRouteTrasitNumber;
    }

    public void setBankRouteTrasitNumber(String bankRouteTrasitNumber) {
        this.bankRouteTrasitNumber = bankRouteTrasitNumber;
    }

    public String getBankFracRouteNumber() {
        return bankFracRouteNumber;
    }

    public void setBankFracRouteNumber(String bankFracRouteNumber) {
        this.bankFracRouteNumber = bankFracRouteNumber;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankLongName() {
        return bankLongName;
    }

    public void setBankLongName(String bankLongName) {
        this.bankLongName = bankLongName;
    }

    public String getFirstBankAddress() {
        return firstBankAddress;
    }

    public void setFirstBankAddress(String firstBankAddress) {
        this.firstBankAddress = firstBankAddress;
    }

    public String getSecondBankAddress() {
        return secondBankAddress;
    }

    public void setSecondBankAddress(String secondBankAddress) {
        this.secondBankAddress = secondBankAddress;
    }

    public String getBankCityName() {
        return bankCityName;
    }

    public void setBankCityName(String bankCityName) {
        this.bankCityName = bankCityName;
    }

    public String getBankStateCode() {
        return bankStateCode;
    }

    public void setBankStateCode(String bankStateCode) {
        this.bankStateCode = bankStateCode;
    }

    public String getBankPostCode() {
        return bankPostCode;
    }

    public void setBankPostCode(String bankPostCode) {
        this.bankPostCode = bankPostCode;
    }

    public String getBankAssociateGroupId() {
        return bankAssociateGroupId;
    }

    public void setBankAssociateGroupId(String bankAssociateGroupId) {
        this.bankAssociateGroupId = bankAssociateGroupId;
    }

    public List<Object> getPayeeClientsList() {
        return payeeClientsList;
    }

    public void setPayeeClientsList(List<Object> payeeClientsList) {
        this.payeeClientsList = payeeClientsList;
    }

    public String getPayeePrefix() {
        return payeePrefix;
    }

    public void setPayeePrefix(String payeePrefix) {
        this.payeePrefix = payeePrefix;
    }

    public String getPayeeFirstName() {
        return payeeFirstName;
    }

    public void setPayeeFirstName(String payeeFirstName) {
        this.payeeFirstName = payeeFirstName;
    }

    public String getPayeeLastName() {
        return payeeLastName;
    }

    public void setPayeeLastName(String payeeLastName) {
        this.payeeLastName = payeeLastName;
    }

    public String getPayeeSuffix() {
        return payeeSuffix;
    }

    public void setPayeeSuffix(String payeeSuffix) {
        this.payeeSuffix = payeeSuffix;
    }

    public String getPayeeLongName() {
        return payeeLongName;
    }

    public void setPayeeLongName(String payeeLongName) {
        this.payeeLongName = payeeLongName;
    }

    public String getFirstPayeeAddress() {
        return firstPayeeAddress;
    }

    public void setFirstPayeeAddress(String firstPayeeAddress) {
        this.firstPayeeAddress = firstPayeeAddress;
    }

    public String getSecondPayeeAddress() {
        return secondPayeeAddress;
    }

    public void setSecondPayeeAddress(String secondPayeeAddress) {
        this.secondPayeeAddress = secondPayeeAddress;
    }

    public String getPayeeCityName() {
        return payeeCityName;
    }

    public void setPayeeCityName(String payeeCityName) {
        this.payeeCityName = payeeCityName;
    }

    public String getPayeeStateCode() {
        return payeeStateCode;
    }

    public void setPayeeStateCode(String payeeStateCode) {
        this.payeeStateCode = payeeStateCode;
    }

    public String getPayeePostCode() {
        return payeePostCode;
    }

    public void setPayeePostCode(String payeePostCode) {
        this.payeePostCode = payeePostCode;
    }

    public String getMailToId() {
        return mailToId;
    }

    public void setMailToId(String mailToId) {
        this.mailToId = mailToId;
    }

    public short getMailToAddressNumber() {
        return mailToAddressNumber;
    }

    public void setMailToAddressNumber(short mailToAddressNumber) {
        this.mailToAddressNumber = mailToAddressNumber;
    }

    public String getMailToPrefix() {
        return mailToPrefix;
    }

    public void setMailToPrefix(String mailToPrefix) {
        this.mailToPrefix = mailToPrefix;
    }

    public String getMailToFirstName() {
        return mailToFirstName;
    }

    public void setMailToFirstName(String mailToFirstName) {
        this.mailToFirstName = mailToFirstName;
    }

    public String getMailToLastName() {
        return mailToLastName;
    }

    public void setMailToLastName(String mailToLastName) {
        this.mailToLastName = mailToLastName;
    }

    public String getMailToSuffix() {
        return mailToSuffix;
    }

    public void setMailToSuffix(String mailToSuffix) {
        this.mailToSuffix = mailToSuffix;
    }

    public String getMailToLongName() {
        return mailToLongName;
    }

    public void setMailToLongName(String mailToLongName) {
        this.mailToLongName = mailToLongName;
    }

    public String getFirstMailToAddress() {
        return firstMailToAddress;
    }

    public void setFirstMailToAddress(String firstMailToAddress) {
        this.firstMailToAddress = firstMailToAddress;
    }

    public String getSecondMailToAddress() {
        return secondMailToAddress;
    }

    public void setSecondMailToAddress(String secondMailToAddress) {
        this.secondMailToAddress = secondMailToAddress;
    }

    public String getMailToCityName() {
        return mailToCityName;
    }

    public void setMailToCityName(String mailToCityName) {
        this.mailToCityName = mailToCityName;
    }

    public String getMailToStateCode() {
        return mailToStateCode;
    }

    public void setMailToStateCode(String mailToStateCode) {
        this.mailToStateCode = mailToStateCode;
    }

    public String getMailToPostCode() {
        return mailToPostCode;
    }

    public void setMailToPostCode(String mailToPostCode) {
        this.mailToPostCode = mailToPostCode;
    }

    public String getInsurAgentId() {
        return insurAgentId;
    }

    public void setInsurAgentId(String insurAgentId) {
        this.insurAgentId = insurAgentId;
    }

    public short getInsurAgentAddressNumber() {
        return insurAgentAddressNumber;
    }

    public void setInsurAgentAddressNumber(short insurAgentAddressNumber) {
        this.insurAgentAddressNumber = insurAgentAddressNumber;
    }

    public String getInsAgentPrefix() {
        return insAgentPrefix;
    }

    public void setInsAgentPrefix(String insAgentPrefix) {
        this.insAgentPrefix = insAgentPrefix;
    }

    public String getInsAgentFirstName() {
        return insAgentFirstName;
    }

    public void setInsAgentFirstName(String insAgentFirstName) {
        this.insAgentFirstName = insAgentFirstName;
    }

    public String getInsAgentLastName() {
        return insAgentLastName;
    }

    public void setInsAgentLastName(String insAgentLastName) {
        this.insAgentLastName = insAgentLastName;
    }

    public String getInsAgentSuffix() {
        return insAgentSuffix;
    }

    public void setInsAgentSuffix(String insAgentSuffix) {
        this.insAgentSuffix = insAgentSuffix;
    }

    public String getInsAgentLongName() {
        return insAgentLongName;
    }

    public void setInsAgentLongName(String insAgentLongName) {
        this.insAgentLongName = insAgentLongName;
    }

    public String getFirstInsAgentAddress() {
        return firstInsAgentAddress;
    }

    public void setFirstInsAgentAddress(String firstInsAgentAddress) {
        this.firstInsAgentAddress = firstInsAgentAddress;
    }

    public String getSecondInsAgentAddress() {
        return secondInsAgentAddress;
    }

    public void setSecondInsAgentAddress(String secondInsAgentAddress) {
        this.secondInsAgentAddress = secondInsAgentAddress;
    }

    public String getInsAgentCityName() {
        return insAgentCityName;
    }

    public void setInsAgentCityName(String insAgentCityName) {
        this.insAgentCityName = insAgentCityName;
    }

    public String getInsAgentStateCode() {
        return insAgentStateCode;
    }

    public void setInsAgentStateCode(String insAgentStateCode) {
        this.insAgentStateCode = insAgentStateCode;
    }

    public String getInsAgentPostCode() {
        return insAgentPostCode;
    }

    public void setInsAgentPostCode(String insAgentPostCode) {
        this.insAgentPostCode = insAgentPostCode;
    }

    public String getNamedInsId() {
        return namedInsId;
    }

    public void setNamedInsId(String namedInsId) {
        this.namedInsId = namedInsId;
    }

    public short getNamedInsAddressNumber() {
        return namedInsAddressNumber;
    }

    public void setNamedInsAddressNumber(short namedInsAddressNumber) {
        this.namedInsAddressNumber = namedInsAddressNumber;
    }

    public String getNamedInsPrefix() {
        return namedInsPrefix;
    }

    public void setNamedInsPrefix(String namedInsPrefix) {
        this.namedInsPrefix = namedInsPrefix;
    }

    public String getNamedInsFirstName() {
        return namedInsFirstName;
    }

    public void setNamedInsFirstName(String namedInsFirstName) {
        this.namedInsFirstName = namedInsFirstName;
    }

    public String getNamedInsLastName() {
        return namedInsLastName;
    }

    public void setNamedInsLastName(String namedInsLastName) {
        this.namedInsLastName = namedInsLastName;
    }

    public String getNamedInsSuffix() {
        return namedInsSuffix;
    }

    public void setNamedInsSuffix(String namedInsSuffix) {
        this.namedInsSuffix = namedInsSuffix;
    }

    public String getNamedInsLongName() {
        return namedInsLongName;
    }

    public void setNamedInsLongName(String namedInsLongName) {
        this.namedInsLongName = namedInsLongName;
    }

    public String getFirstNamedInsAddress() {
        return firstNamedInsAddress;
    }

    public void setFirstNamedInsAddress(String firstNamedInsAddress) {
        this.firstNamedInsAddress = firstNamedInsAddress;
    }

    public String getSecondNamedInsAddress() {
        return secondNamedInsAddress;
    }

    public void setSecondNamedInsAddress(String secondNamedInsAddress) {
        this.secondNamedInsAddress = secondNamedInsAddress;
    }

    public String getNamedInsCityName() {
        return namedInsCityName;
    }

    public void setNamedInsCityName(String namedInsCityName) {
        this.namedInsCityName = namedInsCityName;
    }

    public String getNamedInsStateCode() {
        return namedInsStateCode;
    }

    public void setNamedInsStateCode(String namedInsStateCode) {
        this.namedInsStateCode = namedInsStateCode;
    }

    public String getNamedInsPostCode() {
        return namedInsPostCode;
    }

    public void setNamedInsPostCode(String namedInsPostCode) {
        this.namedInsPostCode = namedInsPostCode;
    }

    public String getFirstFiller() {
        return firstFiller;
    }

    public void setFirstFiller(String firstFiller) {
        this.firstFiller = firstFiller;
    }

    public String getSecondFiller() {
        return secondFiller;
    }

    public void setSecondFiller(String secondFiller) {
        this.secondFiller = secondFiller;
    }

    public char getQueueWriteIndicator() {
        return queueWriteIndicator;
    }

    public void setQueueWriteIndicator(char queueWriteIndicator) {
        this.queueWriteIndicator = queueWriteIndicator;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(application, bankAccount, bankAssociateGroupId, bankCityName,
                bankFracRouteNumber, bankLongName, bankNumber, bankPostCode, bankRouteTrasitNumber, bankStateCode,
                billAccountAdditionId, businessCaseId, callingProgramName, checkDate, checkNumber, currencyCode,
                disburActualDate, disburAcyTimestamp, disburAmount, disburHonoredDate, disburMediumCode,
                disburScheduleDate, disburStatusCode, disburTypeCode, firstBankAddress, firstFiller,
                firstInsAgentAddress, firstMailToAddress, firstNamedInsAddress, firstPayeeAddress, functionValue,
                insAgentCityName, insAgentFirstName, insAgentLastName, insAgentLongName, insAgentPostCode,
                insAgentPrefix, insAgentStateCode, insAgentSuffix, insurAgentAddressNumber, insurAgentId,
                lineBusinessCode, mailToAddressNumber, mailToCityName, mailToFirstName, mailToId, mailToLastName,
                mailToLongName, mailToPostCode, mailToPrefix, mailToStateCode, mailToSuffix, masterCompanyNumber,
                namedInsAddressNumber, namedInsCityName, namedInsFirstName, namedInsId, namedInsLastName,
                namedInsLongName, namedInsPostCode, namedInsPrefix, namedInsStateCode, namedInsSuffix,
                originatorAddressNumber, originatorId, parameterName, payeeCityName, payeeClientsList, payeeFirstName,
                payeeLastName, payeeLongName, payeePostCode, payeePrefix, payeeStateCode, payeeSuffix, polSymbolCode,
                policyNumber, prevDisburStatusCode, printLocCode, processLocCode, productCode, programName,
                queueWriteIndicator, reasonDescription, referenceId, riskStateCode, secondBankAddress, secondFiller,
                secondInsAgentAddress, secondMailToAddress, secondNamedInsAddress, secondPayeeAddress, subFunction,
                techDisburId, userDisburId, userId, userMemoDescription);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        DisbursementInterface other = (DisbursementInterface) obj;
        return Objects.equals(application, other.application) && Objects.equals(bankAccount, other.bankAccount)
                && Objects.equals(bankAssociateGroupId, other.bankAssociateGroupId)
                && Objects.equals(bankCityName, other.bankCityName)
                && Objects.equals(bankFracRouteNumber, other.bankFracRouteNumber)
                && Objects.equals(bankLongName, other.bankLongName) && bankNumber == other.bankNumber
                && Objects.equals(bankPostCode, other.bankPostCode)
                && Objects.equals(bankRouteTrasitNumber, other.bankRouteTrasitNumber)
                && Objects.equals(bankStateCode, other.bankStateCode)
                && Objects.equals(billAccountAdditionId, other.billAccountAdditionId)
                && Objects.equals(businessCaseId, other.businessCaseId)
                && Objects.equals(callingProgramName, other.callingProgramName)
                && Objects.equals(checkDate, other.checkDate) && checkNumber == other.checkNumber
                && Objects.equals(currencyCode, other.currencyCode)
                && Objects.equals(disburActualDate, other.disburActualDate)
                && Objects.equals(disburAcyTimestamp, other.disburAcyTimestamp)
                && Double.doubleToLongBits(disburAmount) == Double.doubleToLongBits(other.disburAmount)
                && Objects.equals(disburHonoredDate, other.disburHonoredDate)
                && Objects.equals(disburMediumCode, other.disburMediumCode)
                && Objects.equals(disburScheduleDate, other.disburScheduleDate)
                && disburStatusCode == other.disburStatusCode && Objects.equals(disburTypeCode, other.disburTypeCode)
                && Objects.equals(firstBankAddress, other.firstBankAddress)
                && Objects.equals(firstFiller, other.firstFiller)
                && Objects.equals(firstInsAgentAddress, other.firstInsAgentAddress)
                && Objects.equals(firstMailToAddress, other.firstMailToAddress)
                && Objects.equals(firstNamedInsAddress, other.firstNamedInsAddress)
                && Objects.equals(firstPayeeAddress, other.firstPayeeAddress)
                && Objects.equals(functionValue, other.functionValue)
                && Objects.equals(insAgentCityName, other.insAgentCityName)
                && Objects.equals(insAgentFirstName, other.insAgentFirstName)
                && Objects.equals(insAgentLastName, other.insAgentLastName)
                && Objects.equals(insAgentLongName, other.insAgentLongName)
                && Objects.equals(insAgentPostCode, other.insAgentPostCode)
                && Objects.equals(insAgentPrefix, other.insAgentPrefix)
                && Objects.equals(insAgentStateCode, other.insAgentStateCode)
                && Objects.equals(insAgentSuffix, other.insAgentSuffix)
                && insurAgentAddressNumber == other.insurAgentAddressNumber
                && Objects.equals(insurAgentId, other.insurAgentId)
                && Objects.equals(lineBusinessCode, other.lineBusinessCode)
                && mailToAddressNumber == other.mailToAddressNumber
                && Objects.equals(mailToCityName, other.mailToCityName)
                && Objects.equals(mailToFirstName, other.mailToFirstName) && Objects.equals(mailToId, other.mailToId)
                && Objects.equals(mailToLastName, other.mailToLastName)
                && Objects.equals(mailToLongName, other.mailToLongName)
                && Objects.equals(mailToPostCode, other.mailToPostCode)
                && Objects.equals(mailToPrefix, other.mailToPrefix)
                && Objects.equals(mailToStateCode, other.mailToStateCode)
                && Objects.equals(mailToSuffix, other.mailToSuffix)
                && Objects.equals(masterCompanyNumber, other.masterCompanyNumber)
                && namedInsAddressNumber == other.namedInsAddressNumber
                && Objects.equals(namedInsCityName, other.namedInsCityName)
                && Objects.equals(namedInsFirstName, other.namedInsFirstName)
                && Objects.equals(namedInsId, other.namedInsId)
                && Objects.equals(namedInsLastName, other.namedInsLastName)
                && Objects.equals(namedInsLongName, other.namedInsLongName)
                && Objects.equals(namedInsPostCode, other.namedInsPostCode)
                && Objects.equals(namedInsPrefix, other.namedInsPrefix)
                && Objects.equals(namedInsStateCode, other.namedInsStateCode)
                && Objects.equals(namedInsSuffix, other.namedInsSuffix)
                && originatorAddressNumber == other.originatorAddressNumber
                && Objects.equals(originatorId, other.originatorId)
                && Objects.equals(parameterName, other.parameterName)
                && Objects.equals(payeeCityName, other.payeeCityName)
                && Objects.equals(payeeClientsList, other.payeeClientsList)
                && Objects.equals(payeeFirstName, other.payeeFirstName)
                && Objects.equals(payeeLastName, other.payeeLastName)
                && Objects.equals(payeeLongName, other.payeeLongName)
                && Objects.equals(payeePostCode, other.payeePostCode) && Objects.equals(payeePrefix, other.payeePrefix)
                && Objects.equals(payeeStateCode, other.payeeStateCode)
                && Objects.equals(payeeSuffix, other.payeeSuffix) && Objects.equals(polSymbolCode, other.polSymbolCode)
                && Objects.equals(policyNumber, other.policyNumber)
                && prevDisburStatusCode == other.prevDisburStatusCode
                && Objects.equals(printLocCode, other.printLocCode)
                && Objects.equals(processLocCode, other.processLocCode)
                && Objects.equals(productCode, other.productCode) && Objects.equals(programName, other.programName)
                && queueWriteIndicator == other.queueWriteIndicator
                && Objects.equals(reasonDescription, other.reasonDescription)
                && Objects.equals(referenceId, other.referenceId) && Objects.equals(riskStateCode, other.riskStateCode)
                && Objects.equals(secondBankAddress, other.secondBankAddress)
                && Objects.equals(secondFiller, other.secondFiller)
                && Objects.equals(secondInsAgentAddress, other.secondInsAgentAddress)
                && Objects.equals(secondMailToAddress, other.secondMailToAddress)
                && Objects.equals(secondNamedInsAddress, other.secondNamedInsAddress)
                && Objects.equals(secondPayeeAddress, other.secondPayeeAddress)
                && Objects.equals(subFunction, other.subFunction) && Objects.equals(techDisburId, other.techDisburId)
                && Objects.equals(userDisburId, other.userDisburId) && Objects.equals(userId, other.userId)
                && Objects.equals(userMemoDescription, other.userMemoDescription);
    }

}
