package billing.model;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Batch extends ResourceSupport {

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime postedDate;
    private String batchId;
    private Double batchAmount;
    private String depositBank;
    private String controlBank;
    private String businessGroup;
    private Integer numberOfTransactions;
    private String user;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime depositDate;
    private String status;
    private Double cashAmount;
    private Double nonCashAmount;
    private Boolean isAccept;
    private Boolean isDeposit;
    private String paymentMethod;
    private Double accumulatedAmount;
    private String currency;
    private Boolean validBatch;
    private Boolean isEdit;
    private Boolean isDelete;
    private Boolean isAcceptAndDeposit;
    private String userId;
    private Boolean enableAddPayment;

    public Batch(@JsonProperty("postedDate") ZonedDateTime postedDate, @JsonProperty("batchId") String batchId,
            @JsonProperty("batchAmount") Double batchAmount, @JsonProperty("depositBank") String depositBank,
            @JsonProperty("controlBank") String controlBank, @JsonProperty("businessGroup") String businessGroup,
            @JsonProperty("numberOfTransactions") Integer numberOfTransactions, @JsonProperty("user") String user,
            @JsonProperty("depositDate") ZonedDateTime depositDate, @JsonProperty("status") String status,
            @JsonProperty("cashAmount") Double cashAmount, @JsonProperty("nonCashAmount") Double nonCashAmount,
            @JsonProperty("isAccept") Boolean isAccept, @JsonProperty("isDeposit") Boolean isDeposit,
            @JsonProperty("paymentMethod") String paymentMethod,
            @JsonProperty("accumulatedAmount") Double accumulatedAmount, @JsonProperty("currency") String currency,
            @JsonProperty("validBatch") Boolean validBatch) {
        super();
        this.postedDate = postedDate;
        this.batchId = batchId;
        this.batchAmount = batchAmount;
        this.depositBank = depositBank;
        this.controlBank = controlBank;
        this.businessGroup = businessGroup;
        this.numberOfTransactions = numberOfTransactions;
        this.user = user;
        this.depositDate = depositDate;
        this.status = status;
        this.cashAmount = cashAmount;
        this.nonCashAmount = nonCashAmount;
        this.isAccept = isAccept;
        this.isDeposit = isDeposit;
        this.paymentMethod = paymentMethod;
        this.accumulatedAmount = accumulatedAmount;
        this.currency = currency;
        this.validBatch = validBatch;
    }

    public Batch() {

    }

    public ZonedDateTime getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(ZonedDateTime postedDate) {
        this.postedDate = postedDate;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Double getBatchAmount() {
        return batchAmount;
    }

    public void setBatchAmount(Double batchAmount) {
        this.batchAmount = batchAmount;
    }

    public String getDepositBank() {
        return depositBank;
    }

    public void setDepositBank(String depositBank) {
        this.depositBank = depositBank;
    }

    public String getControlBank() {
        return controlBank;
    }

    public void setControlBank(String controlBank) {
        this.controlBank = controlBank;
    }

    public Integer getNumberOfTransactions() {
        return numberOfTransactions;
    }

    public void setNumberOfTransactions(Integer numberOfTransactions) {
        this.numberOfTransactions = numberOfTransactions;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public ZonedDateTime getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(ZonedDateTime depositDate) {
        this.depositDate = depositDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(Double cashAmount) {
        this.cashAmount = cashAmount;
    }

    public Double getNonCashAmount() {
        return nonCashAmount;
    }

    public void setNonCashAmount(Double nonCashAmount) {
        this.nonCashAmount = nonCashAmount;
    }

    public Boolean getIsAccept() {
        return isAccept;
    }

    public void setIsAccept(Boolean isAccept) {
        this.isAccept = isAccept;
    }

    public Boolean getIsDeposit() {
        return isDeposit;
    }

    public void setIsDeposit(Boolean isDeposit) {
        this.isDeposit = isDeposit;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Double getAccumulatedAmount() {
        return accumulatedAmount;
    }

    public void setAccumulatedAmount(Double accumulatedAmount) {
        this.accumulatedAmount = accumulatedAmount;
    }

    public String getBusinessGroup() {
        return businessGroup;
    }

    public void setBusinessGroup(String businessGroup) {
        this.businessGroup = businessGroup;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getValidBatch() {
        return validBatch;
    }

    public void setValidBatch(Boolean validBatch) {
        this.validBatch = validBatch;
    }

    public Boolean getIsEdit() {
        return isEdit;
    }

    public void setIsEdit(Boolean isEdit) {
        this.isEdit = isEdit;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsAcceptAndDeposit() {
        return isAcceptAndDeposit;
    }

    public void setIsAcceptAndDeposit(Boolean isAcceptAndDeposit) {
        this.isAcceptAndDeposit = isAcceptAndDeposit;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getEnableAddPayment() {
        return enableAddPayment;
    }

    public void setEnableAddPayment(Boolean enableAddPayment) {
        this.enableAddPayment = enableAddPayment;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accumulatedAmount, batchAmount, batchId, businessGroup, cashAmount,
                controlBank, currency, depositBank, depositDate, enableAddPayment, isAccept, isAcceptAndDeposit,
                isDelete, isDeposit, isEdit, nonCashAmount, numberOfTransactions, paymentMethod, postedDate, status,
                user, userId, validBatch);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Batch other = (Batch) obj;
        return Objects.equals(accumulatedAmount, other.accumulatedAmount)
                && Objects.equals(batchAmount, other.batchAmount) && Objects.equals(batchId, other.batchId)
                && Objects.equals(businessGroup, other.businessGroup) && Objects.equals(cashAmount, other.cashAmount)
                && Objects.equals(controlBank, other.controlBank) && Objects.equals(currency, other.currency)
                && Objects.equals(depositBank, other.depositBank) && Objects.equals(depositDate, other.depositDate)
                && Objects.equals(enableAddPayment, other.enableAddPayment) && Objects.equals(isAccept, other.isAccept)
                && Objects.equals(isAcceptAndDeposit, other.isAcceptAndDeposit)
                && Objects.equals(isDelete, other.isDelete) && Objects.equals(isDeposit, other.isDeposit)
                && Objects.equals(isEdit, other.isEdit) && Objects.equals(nonCashAmount, other.nonCashAmount)
                && Objects.equals(numberOfTransactions, other.numberOfTransactions)
                && Objects.equals(paymentMethod, other.paymentMethod) && Objects.equals(postedDate, other.postedDate)
                && Objects.equals(status, other.status) && Objects.equals(user, other.user)
                && Objects.equals(userId, other.userId) && Objects.equals(validBatch, other.validBatch);
    }

}
