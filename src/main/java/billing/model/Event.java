package billing.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

public class Event extends ResourceSupport {
    private String eventKeyId = BLANK_STRING;
    private String eventKeyTypeCode = BLANK_STRING;
    private String bussinessObjectName = BLANK_STRING;
    private char eventStatusCode = BLANK_CHAR;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime effectiveDate;
    private String eventMduNm = BLANK_STRING;
    private String issueSystemCode = BLANK_STRING;
    private String policyId = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime policyEffectiveDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime plannedExpirationDate;
    private String policySymbolCode = BLANK_STRING;
    private String policyNumber = BLANK_STRING;
    private String lineOfBusinessCode = BLANK_STRING;
    private String masterCompanyNumber = BLANK_STRING;
    private String stateProvinceCode = BLANK_STRING;
    private String countryCode = BLANK_STRING;
    private String policyStatusCode = BLANK_STRING;
    private String bilPlanCode = BLANK_STRING;
    private String filler1 = BLANK_STRING;
    private String bilTechKeyId = BLANK_STRING;
    private char bilTechKeyTypeCode = BLANK_CHAR;
    private String bilAccountId = BLANK_STRING;
    private String bilAccountNumber = BLANK_STRING;
    private String bilTypeCode = BLANK_STRING;
    private String bilClassCode = BLANK_STRING;
    private String collectionMethod = BLANK_STRING;
    private String collectionPlan = BLANK_STRING;
    private String bilThirdPartyNumber = BLANK_STRING;
    private String bilAdditionalId = BLANK_STRING;
    private String filler2 = BLANK_STRING;
    private String requestProgramName = BLANK_STRING;
    private char queueWriteIndicator = BLANK_CHAR;
    private String context = BLANK_STRING;
    private String filler3 = BLANK_STRING;
    private String customerInput = BLANK_STRING;
    private String customerOutput = BLANK_STRING;
    private String programName = BLANK_STRING;
    private String paramName = BLANK_STRING;
    private int sqlCd = 0;
    private String errorDescription = BLANK_STRING;
    private String errorKeyId = BLANK_STRING;
    private String eventCode = BLANK_STRING;
    private String bilAccountType = BLANK_STRING;
    private char errorFlag = BLANK_CHAR;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime paymentInstallmentDueDate;
    private String paymentDepositTypeCode = BLANK_STRING;
    private String paymentReceiveCode = BLANK_STRING;
    private Double paymentDepositAmount = DECIMAL_ZERO;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime paymentEntryDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime paymentLoanDate;
    private Double paymentAdjustedInstallmentAmount = DECIMAL_ZERO;
    private String paymentFilter = BLANK_STRING;
    private String receivableCode = BLANK_STRING;
    private String receiptTypeCode = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime bilDepositDate;
    private String chgFiller = BLANK_STRING;
    private String rcnThirdPartyNumber = BLANK_STRING;
    private String rcnAdditionalId = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime rcnInvEntryDate;
    private short rcnInvSequenceNumber = SHORT_ZERO;
    private String rcnPayableItem = BLANK_STRING;
    private String rcnAction = BLANK_STRING;
    private char rcnReason = BLANK_CHAR;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime rcnEffectiveDate;
    private String rcnComment = BLANK_STRING;
    private Double rcnBillAmount = DECIMAL_ZERO;
    private Double rcnPaidAmount = DECIMAL_ZERO;
    private String rcnFiller = BLANK_STRING;
    private String userSequenceId = BLANK_STRING;

    public Event() {
        super();
    }

    public String getEventKeyId() {
        return eventKeyId;
    }

    public void setEventKeyId(String eventKeyId) {
        this.eventKeyId = eventKeyId;
    }

    public String getEventKeyTypeCode() {
        return eventKeyTypeCode;
    }

    public void setEventKeyTypeCode(String eventKeyTypeCode) {
        this.eventKeyTypeCode = eventKeyTypeCode;
    }

    public String getBussinessObjectName() {
        return bussinessObjectName;
    }

    public void setBussinessObjectName(String bussinessObjectName) {
        this.bussinessObjectName = bussinessObjectName;
    }

    public char getEventStatusCode() {
        return eventStatusCode;
    }

    public void setEventStatusCode(char eventStatusCode) {
        this.eventStatusCode = eventStatusCode;
    }

    public ZonedDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getEventMduNm() {
        return eventMduNm;
    }

    public void setEventMduNm(String eventMduNm) {
        this.eventMduNm = eventMduNm;
    }

    public String getIssueSystemCode() {
        return issueSystemCode;
    }

    public void setIssueSystemCode(String issueSystemCode) {
        this.issueSystemCode = issueSystemCode;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public ZonedDateTime getPolicyEffectiveDate() {
        return policyEffectiveDate;
    }

    public void setPolicyEffectiveDate(ZonedDateTime policyEffectiveDate) {
        this.policyEffectiveDate = policyEffectiveDate;
    }

    public ZonedDateTime getPlannedExpirationDate() {
        return plannedExpirationDate;
    }

    public void setPlannedExpirationDate(ZonedDateTime plannedExpirationDate) {
        this.plannedExpirationDate = plannedExpirationDate;
    }

    public String getPolicySymbolCode() {
        return policySymbolCode;
    }

    public void setPolicySymbolCode(String policySymbolCode) {
        this.policySymbolCode = policySymbolCode;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getLineOfBusinessCode() {
        return lineOfBusinessCode;
    }

    public void setLineOfBusinessCode(String lineOfBusinessCode) {
        this.lineOfBusinessCode = lineOfBusinessCode;
    }

    public String getMasterCompanyNumber() {
        return masterCompanyNumber;
    }

    public void setMasterCompanyNumber(String masterCompanyNumber) {
        this.masterCompanyNumber = masterCompanyNumber;
    }

    public String getStateProvinceCode() {
        return stateProvinceCode;
    }

    public void setStateProvinceCode(String stateProvinceCode) {
        this.stateProvinceCode = stateProvinceCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPolicyStatusCode() {
        return policyStatusCode;
    }

    public void setPolicyStatusCode(String policyStatusCode) {
        this.policyStatusCode = policyStatusCode;
    }

    public String getBilPlanCode() {
        return bilPlanCode;
    }

    public void setBilPlanCode(String bilPlanCode) {
        this.bilPlanCode = bilPlanCode;
    }

    public String getFiller1() {
        return filler1;
    }

    public void setFiller1(String filler1) {
        this.filler1 = filler1;
    }

    public String getBilTechKeyId() {
        return bilTechKeyId;
    }

    public void setBilTechKeyId(String bilTechKeyId) {
        this.bilTechKeyId = bilTechKeyId;
    }

    public char getBilTechKeyTypeCode() {
        return bilTechKeyTypeCode;
    }

    public void setBilTechKeyTypeCode(char bilTechKeyTypeCode) {
        this.bilTechKeyTypeCode = bilTechKeyTypeCode;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public String getBilAccountNumber() {
        return bilAccountNumber;
    }

    public void setBilAccountNumber(String bilAccountNumber) {
        this.bilAccountNumber = bilAccountNumber;
    }

    public String getBilTypeCode() {
        return bilTypeCode;
    }

    public void setBilTypeCode(String bilTypeCode) {
        this.bilTypeCode = bilTypeCode;
    }

    public String getBilClassCode() {
        return bilClassCode;
    }

    public void setBilClassCode(String bilClassCode) {
        this.bilClassCode = bilClassCode;
    }

    public String getCollectionMethod() {
        return collectionMethod;
    }

    public void setCollectionMethod(String collectionMethod) {
        this.collectionMethod = collectionMethod;
    }

    public String getCollectionPlan() {
        return collectionPlan;
    }

    public void setCollectionPlan(String collectionPlan) {
        this.collectionPlan = collectionPlan;
    }

    public String getBilThirdPartyNumber() {
        return bilThirdPartyNumber;
    }

    public void setBilThirdPartyNumber(String bilThirdPartyNumber) {
        this.bilThirdPartyNumber = bilThirdPartyNumber;
    }

    public String getBilAdditionalId() {
        return bilAdditionalId;
    }

    public void setBilAdditionalId(String bilAdditionalId) {
        this.bilAdditionalId = bilAdditionalId;
    }

    public String getFiller2() {
        return filler2;
    }

    public void setFiller2(String filler2) {
        this.filler2 = filler2;
    }

    public String getRequestProgramName() {
        return requestProgramName;
    }

    public void setRequestProgramName(String requestProgramName) {
        this.requestProgramName = requestProgramName;
    }

    public char getQueueWriteIndicator() {
        return queueWriteIndicator;
    }

    public void setQueueWriteIndicator(char queueWriteIndicator) {
        this.queueWriteIndicator = queueWriteIndicator;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getFiller3() {
        return filler3;
    }

    public void setFiller3(String filler3) {
        this.filler3 = filler3;
    }

    public String getCustomerInput() {
        return customerInput;
    }

    public void setCustomerInput(String customerInput) {
        this.customerInput = customerInput;
    }

    public String getCustomerOutput() {
        return customerOutput;
    }

    public void setCustomerOutput(String customerOutput) {
        this.customerOutput = customerOutput;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public int getSqlCd() {
        return sqlCd;
    }

    public void setSqlCd(int sqlCd) {
        this.sqlCd = sqlCd;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getErrorKeyId() {
        return errorKeyId;
    }

    public void setErrorKeyId(String errorKeyId) {
        this.errorKeyId = errorKeyId;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getBilAccountType() {
        return bilAccountType;
    }

    public void setBilAccountType(String bilAccountType) {
        this.bilAccountType = bilAccountType;
    }

    public char getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(char errorFlag) {
        this.errorFlag = errorFlag;
    }

    public ZonedDateTime getPaymentInstallmentDueDate() {
        return paymentInstallmentDueDate;
    }

    public void setPaymentInstallmentDueDate(ZonedDateTime paymentInstallmentDueDate) {
        this.paymentInstallmentDueDate = paymentInstallmentDueDate;
    }

    public String getPaymentDepositTypeCode() {
        return paymentDepositTypeCode;
    }

    public void setPaymentDepositTypeCode(String paymentDepositTypeCode) {
        this.paymentDepositTypeCode = paymentDepositTypeCode;
    }

    public String getPaymentReceiveCode() {
        return paymentReceiveCode;
    }

    public void setPaymentReceiveCode(String paymentReceiveCode) {
        this.paymentReceiveCode = paymentReceiveCode;
    }

    public Double getPaymentDepositAmount() {
        return paymentDepositAmount;
    }

    public void setPaymentDepositAmount(Double paymentDepositAmount) {
        this.paymentDepositAmount = paymentDepositAmount;
    }

    public ZonedDateTime getPaymentEntryDate() {
        return paymentEntryDate;
    }

    public void setPaymentEntryDate(ZonedDateTime paymentEntryDate) {
        this.paymentEntryDate = paymentEntryDate;
    }

    public ZonedDateTime getPaymentLoanDate() {
        return paymentLoanDate;
    }

    public void setPaymentLoanDate(ZonedDateTime paymentLoanDate) {
        this.paymentLoanDate = paymentLoanDate;
    }

    public Double getPaymentAdjustedInstallmentAmount() {
        return paymentAdjustedInstallmentAmount;
    }

    public void setPaymentAdjustedInstallmentAmount(Double paymentAdjustedInstallmentAmount) {
        this.paymentAdjustedInstallmentAmount = paymentAdjustedInstallmentAmount;
    }

    public String getPaymentFilter() {
        return paymentFilter;
    }

    public void setPaymentFilter(String paymentFilter) {
        this.paymentFilter = paymentFilter;
    }

    public String getReceivableCode() {
        return receivableCode;
    }

    public void setReceivableCode(String receivableCode) {
        this.receivableCode = receivableCode;
    }

    public String getReceiptTypeCode() {
        return receiptTypeCode;
    }

    public void setReceiptTypeCode(String receiptTypeCode) {
        this.receiptTypeCode = receiptTypeCode;
    }

    public ZonedDateTime getBilDepositDate() {
        return bilDepositDate;
    }

    public void setBilDepositDate(ZonedDateTime bilDepositDate) {
        this.bilDepositDate = bilDepositDate;
    }

    public String getChgFiller() {
        return chgFiller;
    }

    public void setChgFiller(String chgFiller) {
        this.chgFiller = chgFiller;
    }

    public String getRcnAdditionalId() {
        return rcnAdditionalId;
    }

    public void setRcnAdditionalId(String rcnAdditionalId) {
        this.rcnAdditionalId = rcnAdditionalId;
    }

    public String getRcnAction() {
        return rcnAction;
    }

    public void setRcnAction(String rcnAction) {
        this.rcnAction = rcnAction;
    }

    public char getRcnReason() {
        return rcnReason;
    }

    public void setRcnReason(char rcnReason) {
        this.rcnReason = rcnReason;
    }

    public String getRcnComment() {
        return rcnComment;
    }

    public void setRcnComment(String rcnComment) {
        this.rcnComment = rcnComment;
    }

    public String getRcnFiller() {
        return rcnFiller;
    }

    public void setRcnFiller(String rcnFiller) {
        this.rcnFiller = rcnFiller;
    }

    public String getRcnThirdPartyNumber() {
        return rcnThirdPartyNumber;
    }

    public void setRcnThirdPartyNumber(String rcnThirdPartyNumber) {
        this.rcnThirdPartyNumber = rcnThirdPartyNumber;
    }

    public ZonedDateTime getRcnInvEntryDate() {
        return rcnInvEntryDate;
    }

    public void setRcnInvEntryDate(ZonedDateTime rcnInvEntryDate) {
        this.rcnInvEntryDate = rcnInvEntryDate;
    }

    public short getRcnInvSequenceNumber() {
        return rcnInvSequenceNumber;
    }

    public void setRcnInvSequenceNumber(short rcnInvSequenceNumber) {
        this.rcnInvSequenceNumber = rcnInvSequenceNumber;
    }

    public String getRcnPayableItem() {
        return rcnPayableItem;
    }

    public void setRcnPayableItem(String rcnPayableItem) {
        this.rcnPayableItem = rcnPayableItem;
    }

    public ZonedDateTime getRcnEffectiveDate() {
        return rcnEffectiveDate;
    }

    public void setRcnEffectiveDate(ZonedDateTime rcnEffectiveDate) {
        this.rcnEffectiveDate = rcnEffectiveDate;
    }

    public Double getRcnBillAmount() {
        return rcnBillAmount;
    }

    public void setRcnBillAmount(Double rcnBillAmount) {
        this.rcnBillAmount = rcnBillAmount;
    }

    public Double getRcnPaidAmount() {
        return rcnPaidAmount;
    }

    public void setRcnPaidAmount(Double rcnPaidAmount) {
        this.rcnPaidAmount = rcnPaidAmount;
    }
    
    public String getUserSequenceId() {
        return userSequenceId;
    }

    public void setUserSequenceId(String userSequenceId) {
        this.userSequenceId = userSequenceId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(bilAccountId, bilAccountNumber, bilAccountType, bilAdditionalId,
                bilClassCode, bilDepositDate, bilPlanCode, bilTechKeyId, bilTechKeyTypeCode, bilThirdPartyNumber,
                bilTypeCode, bussinessObjectName, chgFiller, collectionMethod, collectionPlan, context, countryCode,
                customerInput, customerOutput, effectiveDate, errorDescription, errorFlag, errorKeyId, eventCode,
                eventKeyId, eventKeyTypeCode, eventMduNm, eventStatusCode, filler1, filler2, filler3, issueSystemCode,
                lineOfBusinessCode, masterCompanyNumber, paramName, paymentAdjustedInstallmentAmount,
                paymentDepositAmount, paymentDepositTypeCode, paymentEntryDate, paymentFilter,
                paymentInstallmentDueDate, paymentLoanDate, paymentReceiveCode, plannedExpirationDate,
                policyEffectiveDate, policyId, policyNumber, policyStatusCode, policySymbolCode, programName,
                queueWriteIndicator, rcnAction, rcnAdditionalId, rcnBillAmount, rcnComment, rcnEffectiveDate, rcnFiller,
                rcnInvEntryDate, rcnInvSequenceNumber, rcnPaidAmount, rcnPayableItem, rcnReason, rcnThirdPartyNumber,
                receiptTypeCode, receivableCode, requestProgramName, sqlCd, stateProvinceCode, userSequenceId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Event other = (Event) obj;
        return Objects.equals(bilAccountId, other.bilAccountId)
                && Objects.equals(bilAccountNumber, other.bilAccountNumber)
                && Objects.equals(bilAccountType, other.bilAccountType)
                && Objects.equals(bilAdditionalId, other.bilAdditionalId)
                && Objects.equals(bilClassCode, other.bilClassCode)
                && Objects.equals(bilDepositDate, other.bilDepositDate)
                && Objects.equals(bilPlanCode, other.bilPlanCode) && Objects.equals(bilTechKeyId, other.bilTechKeyId)
                && bilTechKeyTypeCode == other.bilTechKeyTypeCode
                && Objects.equals(bilThirdPartyNumber, other.bilThirdPartyNumber)
                && Objects.equals(bilTypeCode, other.bilTypeCode)
                && Objects.equals(bussinessObjectName, other.bussinessObjectName)
                && Objects.equals(chgFiller, other.chgFiller)
                && Objects.equals(collectionMethod, other.collectionMethod)
                && Objects.equals(collectionPlan, other.collectionPlan) && Objects.equals(context, other.context)
                && Objects.equals(countryCode, other.countryCode) && Objects.equals(customerInput, other.customerInput)
                && Objects.equals(customerOutput, other.customerOutput)
                && Objects.equals(effectiveDate, other.effectiveDate)
                && Objects.equals(errorDescription, other.errorDescription) && errorFlag == other.errorFlag
                && Objects.equals(errorKeyId, other.errorKeyId) && Objects.equals(eventCode, other.eventCode)
                && Objects.equals(eventKeyId, other.eventKeyId)
                && Objects.equals(eventKeyTypeCode, other.eventKeyTypeCode)
                && Objects.equals(eventMduNm, other.eventMduNm) && eventStatusCode == other.eventStatusCode
                && Objects.equals(filler1, other.filler1) && Objects.equals(filler2, other.filler2)
                && Objects.equals(filler3, other.filler3) && Objects.equals(issueSystemCode, other.issueSystemCode)
                && Objects.equals(lineOfBusinessCode, other.lineOfBusinessCode)
                && Objects.equals(masterCompanyNumber, other.masterCompanyNumber)
                && Objects.equals(paramName, other.paramName)
                && Objects.equals(paymentAdjustedInstallmentAmount, other.paymentAdjustedInstallmentAmount)
                && Objects.equals(paymentDepositAmount, other.paymentDepositAmount)
                && Objects.equals(paymentDepositTypeCode, other.paymentDepositTypeCode)
                && Objects.equals(paymentEntryDate, other.paymentEntryDate)
                && Objects.equals(paymentFilter, other.paymentFilter)
                && Objects.equals(paymentInstallmentDueDate, other.paymentInstallmentDueDate)
                && Objects.equals(paymentLoanDate, other.paymentLoanDate)
                && Objects.equals(paymentReceiveCode, other.paymentReceiveCode)
                && Objects.equals(plannedExpirationDate, other.plannedExpirationDate)
                && Objects.equals(policyEffectiveDate, other.policyEffectiveDate)
                && Objects.equals(policyId, other.policyId) && Objects.equals(policyNumber, other.policyNumber)
                && Objects.equals(policyStatusCode, other.policyStatusCode)
                && Objects.equals(policySymbolCode, other.policySymbolCode)
                && Objects.equals(programName, other.programName) && queueWriteIndicator == other.queueWriteIndicator
                && Objects.equals(rcnAction, other.rcnAction) && Objects.equals(rcnAdditionalId, other.rcnAdditionalId)
                && Objects.equals(rcnBillAmount, other.rcnBillAmount) && Objects.equals(rcnComment, other.rcnComment)
                && Objects.equals(rcnEffectiveDate, other.rcnEffectiveDate)
                && Objects.equals(rcnFiller, other.rcnFiller) && Objects.equals(rcnInvEntryDate, other.rcnInvEntryDate)
                && rcnInvSequenceNumber == other.rcnInvSequenceNumber
                && Objects.equals(rcnPaidAmount, other.rcnPaidAmount)
                && Objects.equals(rcnPayableItem, other.rcnPayableItem) && rcnReason == other.rcnReason
                && Objects.equals(rcnThirdPartyNumber, other.rcnThirdPartyNumber)
                && Objects.equals(receiptTypeCode, other.receiptTypeCode)
                && Objects.equals(receivableCode, other.receivableCode)
                && Objects.equals(requestProgramName, other.requestProgramName) && sqlCd == other.sqlCd
                && Objects.equals(userSequenceId, other.userSequenceId) && Objects.equals(stateProvinceCode, other.stateProvinceCode);
    }

}
