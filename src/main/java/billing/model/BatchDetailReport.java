package billing.model;

import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BatchDetailReport extends ResourceSupport {
    private double paymentAmount;
    private String paymentType;
    private String identifier;
    private String identifierType;
    private String review;
    private double currentDue;

    public BatchDetailReport() {
        super();
    }

    public BatchDetailReport(double paymentAmount, String paymentType, String identifier, String identifierType,
            String review, double currentDue) {
        super();
        this.paymentAmount = paymentAmount;
        this.paymentType = paymentType;
        this.identifier = identifier;
        this.identifierType = identifierType;
        this.review = review;
        this.currentDue = currentDue;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifierType() {
        return identifierType;
    }

    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public double getCurrentDue() {
        return currentDue;
    }

    public void setCurrentDue(double currentDue) {
        this.currentDue = currentDue;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + Objects.hash(currentDue, identifier, identifierType, paymentAmount, paymentType, review);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        BatchDetailReport other = (BatchDetailReport) obj;
        return Double.doubleToLongBits(currentDue) == Double.doubleToLongBits(other.currentDue)
                && Objects.equals(identifier, other.identifier) && Objects.equals(identifierType, other.identifierType)
                && Double.doubleToLongBits(paymentAmount) == Double.doubleToLongBits(other.paymentAmount)
                && Objects.equals(paymentType, other.paymentType) && Objects.equals(review, other.review);
    }

}
