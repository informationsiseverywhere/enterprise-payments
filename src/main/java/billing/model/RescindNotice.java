package billing.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.INVALID_INT_VAL;

import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RescindNotice extends ResourceSupport {

	private String accountId = BLANK_STRING;
	private String accountNumber = BLANK_STRING;
	private String policyId = BLANK_STRING;
	private String policySymbolCode = BLANK_STRING;
	private String policyNumber = BLANK_STRING;
	private String policyEffDate = BLANK_STRING;
	private String policyExpDate = BLANK_STRING;
	private String issueSystemId = BLANK_STRING;
	private String reqPgmName = BLANK_STRING;
	private String activityDate = BLANK_STRING;
	private String lobCode = BLANK_STRING;
	private String masterCoNumber = BLANK_STRING;
	private String policyPrintRskStCode = BLANK_STRING;
	private String bilTypeCode = BLANK_STRING;
	private String bilClassCode = BLANK_STRING;
	private String bilCollectionPlan = BLANK_STRING;
	private String payClientId = BLANK_STRING;
	private Short payAdrSeq = Short.MIN_VALUE;
	private Character payAdrSeqSign = BLANK_CHAR;
	private String issueCltId = BLANK_STRING;
	private Short issueAdrSeq = Short.MIN_VALUE;
	private Character issueAdrSeqSign = BLANK_CHAR;
	private String agtCltId = BLANK_STRING;
	private Short agtAdrSeq = Short.MIN_VALUE;
	private Character agtAdrSeqSign = BLANK_CHAR;
	private String se3Date = BLANK_STRING;
	private String fillerSe3DateTs = BLANK_STRING;
	private String customerInput = BLANK_STRING;
	private Character queueWriteInd = BLANK_CHAR;
	public Boolean errorSw = false;
	private String programName = BLANK_STRING;
	private String paramName = BLANK_STRING;
	private String returnCode = BLANK_STRING;
	private Integer sqlCode = INVALID_INT_VAL;
	private String errDesc = BLANK_STRING;
	private String errKeyId = BLANK_STRING;

	public RescindNotice() {
		super();
	}

	public RescindNotice(@JsonProperty("accountId") String accountId,
			@JsonProperty("accountNumber") String accountNumber, @JsonProperty("policyId") String policyId,
			@JsonProperty("policySymbolCode") String policySymbolCode,
			@JsonProperty("policyNumber") String policyNumber, @JsonProperty("policyEffDate") String policyEffDate,
			@JsonProperty("policyExpDate") String policyExpDate, @JsonProperty("issueSystemId") String issueSystemId,
			@JsonProperty("reqPgmName") String reqPgmName, @JsonProperty("activityDate") String activityDate,
			@JsonProperty("lobCode") String lobCode, @JsonProperty("masterCoNumber") String masterCoNumber,
			@JsonProperty("policyPrintRskStCode") String policyPrintRskStCode,
			@JsonProperty("bilTypeCode") String bilTypeCode, @JsonProperty("bilClassCode") String bilClassCode,
			@JsonProperty("bilCollectionPlan") String bilCollectionPlan,
			@JsonProperty("payClientId") String payClientId, @JsonProperty("payAdrSeq") Short payAdrSeq,
			@JsonProperty("payAdrSeqSign") Character payAdrSeqSign, @JsonProperty("issueCltId") String issueCltId,
			@JsonProperty("issueAdrSeq") Short issueAdrSeq, @JsonProperty("issueAdrSeqSign") Character issueAdrSeqSign,
			@JsonProperty("agtCltId") String agtCltId, @JsonProperty("agtAdrSeq") Short agtAdrSeq,
			@JsonProperty("agtAdrSeqSign") Character agtAdrSeqSign, @JsonProperty("se3Date") String se3Date,
			@JsonProperty("fillerSe3DateTs") String fillerSe3DateTs,
			@JsonProperty("customerInput") String customerInput, @JsonProperty("queueWriteInd") Character queueWriteInd,
			@JsonProperty("errorSw") Boolean errorSw, @JsonProperty("programName") String programName,
			@JsonProperty("paramName") String paramName, @JsonProperty("returnCode") String returnCode,
			@JsonProperty("sqlCode") Integer sqlCode, @JsonProperty("errDesc") String errDesc,
			@JsonProperty("errKeyId") String errKeyId) {
		super();
		this.accountId = accountId;
		this.accountNumber = accountNumber;
		this.policyId = policyId;
		this.policySymbolCode = policySymbolCode;
		this.policyNumber = policyNumber;
		this.policyEffDate = policyEffDate;
		this.policyExpDate = policyExpDate;
		this.issueSystemId = issueSystemId;
		this.reqPgmName = reqPgmName;
		this.activityDate = activityDate;
		this.lobCode = lobCode;
		this.masterCoNumber = masterCoNumber;
		this.policyPrintRskStCode = policyPrintRskStCode;
		this.bilTypeCode = bilTypeCode;
		this.bilClassCode = bilClassCode;
		this.bilCollectionPlan = bilCollectionPlan;
		this.payClientId = payClientId;
		this.payAdrSeq = payAdrSeq;
		this.payAdrSeqSign = payAdrSeqSign;
		this.issueCltId = issueCltId;
		this.issueAdrSeq = issueAdrSeq;
		this.issueAdrSeqSign = issueAdrSeqSign;
		this.agtCltId = agtCltId;
		this.agtAdrSeq = agtAdrSeq;
		this.agtAdrSeqSign = agtAdrSeqSign;
		this.se3Date = se3Date;
		this.fillerSe3DateTs = fillerSe3DateTs;
		this.customerInput = customerInput;
		this.queueWriteInd = queueWriteInd;
		this.errorSw = errorSw;
		this.programName = programName;
		this.paramName = paramName;
		this.returnCode = returnCode;
		this.sqlCode = sqlCode;
		this.errDesc = errDesc;
		this.errKeyId = errKeyId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getPolicyId() {
		return policyId;
	}

	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}

	public String getPolicySymbolCode() {
		return policySymbolCode;
	}

	public void setPolicySymbolCode(String policySymbolCode) {
		this.policySymbolCode = policySymbolCode;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getPolicyEffDate() {
		return policyEffDate;
	}

	public void setPolicyEffDate(String policyEffDate) {
		this.policyEffDate = policyEffDate;
	}

	public String getPolicyExpDate() {
		return policyExpDate;
	}

	public void setPolicyExpDate(String policyExpDate) {
		this.policyExpDate = policyExpDate;
	}

	public String getIssueSystemId() {
		return issueSystemId;
	}

	public void setIssueSystemId(String issueSystemId) {
		this.issueSystemId = issueSystemId;
	}

	public String getReqPgmName() {
		return reqPgmName;
	}

	public void setReqPgmName(String reqPgmName) {
		this.reqPgmName = reqPgmName;
	}

	public String getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}

	public String getLobCode() {
		return lobCode;
	}

	public void setLobCode(String lobCode) {
		this.lobCode = lobCode;
	}

	public String getMasterCoNumber() {
		return masterCoNumber;
	}

	public void setMasterCoNumber(String masterCoNumber) {
		this.masterCoNumber = masterCoNumber;
	}

	public String getPolicyPrintRskStCode() {
		return policyPrintRskStCode;
	}

	public void setPolicyPrintRskStCode(String policyPrintRskStCode) {
		this.policyPrintRskStCode = policyPrintRskStCode;
	}

	public String getBilTypeCode() {
		return bilTypeCode;
	}

	public void setBilTypeCode(String bilTypeCode) {
		this.bilTypeCode = bilTypeCode;
	}

	public String getBilClassCode() {
		return bilClassCode;
	}

	public void setBilClassCode(String bilClassCode) {
		this.bilClassCode = bilClassCode;
	}

	public String getBilCollectionPlan() {
		return bilCollectionPlan;
	}

	public void setBilCollectionPlan(String bilCollectionPlan) {
		this.bilCollectionPlan = bilCollectionPlan;
	}

	public String getPayClientId() {
		return payClientId;
	}

	public void setPayClientId(String payCollectionId) {
		this.payClientId = payCollectionId;
	}

	public Short getPayAdrSeq() {
		return payAdrSeq;
	}

	public void setPayAdrSeq(Short payAdrSeq) {
		this.payAdrSeq = payAdrSeq;
	}

	public Character getPayAdrSeqSign() {
		return payAdrSeqSign;
	}

	public void setPayAdrSeqSign(Character payAdrSeqSign) {
		this.payAdrSeqSign = payAdrSeqSign;
	}

	public String getIssueCltId() {
		return issueCltId;
	}

	public void setIssueCltId(String issueCltId) {
		this.issueCltId = issueCltId;
	}

	public Short getIssueAdrSeq() {
		return issueAdrSeq;
	}

	public void setIssueAdrSeq(Short issueAdrSeq) {
		this.issueAdrSeq = issueAdrSeq;
	}

	public Character getIssueAdrSeqSign() {
		return issueAdrSeqSign;
	}

	public void setIssueAdrSeqSign(Character issueAdrSeqSign) {
		this.issueAdrSeqSign = issueAdrSeqSign;
	}

	public String getAgtCltId() {
		return agtCltId;
	}

	public void setAgtCltId(String agtCltId) {
		this.agtCltId = agtCltId;
	}

	public Short getAgtAdrSeq() {
		return agtAdrSeq;
	}

	public void setAgtAdrSeq(Short agtAdrSeq) {
		this.agtAdrSeq = agtAdrSeq;
	}

	public Character getAgtAdrSeqSign() {
		return agtAdrSeqSign;
	}

	public void setAgtAdrSeqSign(Character agtAdrSeqSign) {
		this.agtAdrSeqSign = agtAdrSeqSign;
	}

	public String getSe3Date() {
		return se3Date;
	}

	public void setSe3Date(String se3Date) {
		this.se3Date = se3Date;
	}

	public String getFillerSe3DateTs() {
		return fillerSe3DateTs;
	}

	public void setFillerSe3DateTs(String fillerSe3DateTs) {
		this.fillerSe3DateTs = fillerSe3DateTs;
	}

	public String getCustomerInput() {
		return customerInput;
	}

	public void setCustomerInput(String customerInput) {
		this.customerInput = customerInput;
	}

	public Character getQueueWriteInd() {
		return queueWriteInd;
	}

	public void setQueueWriteInd(Character queueWriteInd) {
		this.queueWriteInd = queueWriteInd;
	}

	public Boolean isErrorSw() {
		return errorSw;
	}

	public void setErrorSw(Boolean errorSw) {
		this.errorSw = errorSw;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public Integer getSqlCode() {
		return sqlCode;
	}

	public void setSqlCode(Integer sqlCode) {
		this.sqlCode = sqlCode;
	}

	public String getErrDesc() {
		return errDesc;
	}

	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}

	public String getErrKeyId() {
		return errKeyId;
	}

	public void setErrKeyId(String errKeyId) {
		this.errKeyId = errKeyId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(accountId, accountNumber, activityDate, agtAdrSeq, agtAdrSeqSign,
				agtCltId, bilClassCode, bilCollectionPlan, bilTypeCode, customerInput, errDesc, errKeyId, errorSw,
				fillerSe3DateTs, issueAdrSeq, issueAdrSeqSign, issueCltId, issueSystemId, lobCode, masterCoNumber,
				paramName, payAdrSeq, payAdrSeqSign, payClientId, policyEffDate, policyExpDate, policyId, policyNumber,
				policyPrintRskStCode, policySymbolCode, programName, queueWriteInd, reqPgmName, returnCode, se3Date,
				sqlCode);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RescindNotice other = (RescindNotice) obj;
		return Objects.equals(accountId, other.accountId) && Objects.equals(accountNumber, other.accountNumber)
				&& Objects.equals(activityDate, other.activityDate) && Objects.equals(agtAdrSeq, other.agtAdrSeq)
				&& Objects.equals(agtAdrSeqSign, other.agtAdrSeqSign) && Objects.equals(agtCltId, other.agtCltId)
				&& Objects.equals(bilClassCode, other.bilClassCode)
				&& Objects.equals(bilCollectionPlan, other.bilCollectionPlan)
				&& Objects.equals(bilTypeCode, other.bilTypeCode) && Objects.equals(customerInput, other.customerInput)
				&& Objects.equals(errDesc, other.errDesc) && Objects.equals(errKeyId, other.errKeyId)
				&& Objects.equals(errorSw, other.errorSw) && Objects.equals(fillerSe3DateTs, other.fillerSe3DateTs)
				&& Objects.equals(issueAdrSeq, other.issueAdrSeq)
				&& Objects.equals(issueAdrSeqSign, other.issueAdrSeqSign)
				&& Objects.equals(issueCltId, other.issueCltId) && Objects.equals(issueSystemId, other.issueSystemId)
				&& Objects.equals(lobCode, other.lobCode) && Objects.equals(masterCoNumber, other.masterCoNumber)
				&& Objects.equals(paramName, other.paramName) && Objects.equals(payAdrSeq, other.payAdrSeq)
				&& Objects.equals(payAdrSeqSign, other.payAdrSeqSign) && Objects.equals(payClientId, other.payClientId)
				&& Objects.equals(policyEffDate, other.policyEffDate)
				&& Objects.equals(policyExpDate, other.policyExpDate) && Objects.equals(policyId, other.policyId)
				&& Objects.equals(policyNumber, other.policyNumber)
				&& Objects.equals(policyPrintRskStCode, other.policyPrintRskStCode)
				&& Objects.equals(policySymbolCode, other.policySymbolCode)
				&& Objects.equals(programName, other.programName) && Objects.equals(queueWriteInd, other.queueWriteInd)
				&& Objects.equals(reqPgmName, other.reqPgmName) && Objects.equals(returnCode, other.returnCode)
				&& Objects.equals(se3Date, other.se3Date) && Objects.equals(sqlCode, other.sqlCode);
	}

}
