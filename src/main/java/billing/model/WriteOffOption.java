package billing.model;

import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class WriteOffOption extends ResourceSupport {

    private String reason;

    public WriteOffOption() {
        super();
    }

    @JsonCreator
    public WriteOffOption(@JsonProperty("reason") String reason) {
        super();
        this.reason = reason;
    }

    public WriteOffOption(WriteOffOption writeOffOption) {
        this(writeOffOption.getReason());
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(reason);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        WriteOffOption other = (WriteOffOption) obj;
        return Objects.equals(reason, other.reason);
    }
}
