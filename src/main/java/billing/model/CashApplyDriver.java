package billing.model;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

public class CashApplyDriver extends ResourceSupport {

    private String accountId;
    private boolean isNoReinstatement = false;
    private String tranferCashPolicyNumber = SEPARATOR_BLANK;
    private String tranferCashPolicyId = SEPARATOR_BLANK;
    private boolean isCallBcmoax;
    private String correctedInvoiceIndicator = SEPARATOR_BLANK;
    private String quoteIndicator = SEPARATOR_BLANK;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime cashApplyDate;
    private String userSequenceId = SEPARATOR_BLANK;

    public CashApplyDriver() {
        super();
    }

    public CashApplyDriver(String accountId, boolean isNoReinstatement, String tranferCashPolicyNumber,
            String tranferCashPolicyId, boolean isCallBcmoax, String correctedInvoiceIndicator, String quoteIndicator,
            ZonedDateTime cashApplyDate, String userSequenceId) {
        super();
        this.accountId = accountId;
        this.isNoReinstatement = isNoReinstatement;
        this.tranferCashPolicyNumber = tranferCashPolicyNumber;
        this.tranferCashPolicyId = tranferCashPolicyId;
        this.isCallBcmoax = isCallBcmoax;
        this.correctedInvoiceIndicator = correctedInvoiceIndicator;
        this.quoteIndicator = quoteIndicator;
        this.cashApplyDate = cashApplyDate;
        this.userSequenceId = userSequenceId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getTranferCashPolicyNumber() {
        return tranferCashPolicyNumber;
    }

    public void setTranferCashPolicyNumber(String tranferCashPolicyNumber) {
        this.tranferCashPolicyNumber = tranferCashPolicyNumber;
    }

    public String getTranferCashPolicyId() {
        return tranferCashPolicyId;
    }

    public void setTranferCashPolicyId(String tranferCashPolicyId) {
        this.tranferCashPolicyId = tranferCashPolicyId;
    }

    public boolean isCallBcmoax() {
        return isCallBcmoax;
    }

    public void setCallBcmoax(boolean isCallBcmoax) {
        this.isCallBcmoax = isCallBcmoax;
    }

    public String getCorrectedInvoiceIndicator() {
        return correctedInvoiceIndicator;
    }

    public void setCorrectedInvoiceIndicator(String correctedInvoiceIndicator) {
        this.correctedInvoiceIndicator = correctedInvoiceIndicator;
    }

    public String getQuoteIndicator() {
        return quoteIndicator;
    }

    public void setQuoteIndicator(String quoteIndicator) {
        this.quoteIndicator = quoteIndicator;
    }

    public ZonedDateTime getCashApplyDate() {
        return cashApplyDate;
    }

    public void setCashApplyDate(ZonedDateTime cashApplyDate) {
        this.cashApplyDate = cashApplyDate;
    }

    public boolean isNoReinstatement() {
        return isNoReinstatement;
    }

    public void setNoReinstatement(boolean isNoReinstatement) {
        this.isNoReinstatement = isNoReinstatement;
    }
    
    public String getUserSequenceId() {
        return userSequenceId;
    }

    public void setUserSequenceId(String userSequenceId) {
        this.userSequenceId = userSequenceId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountId, cashApplyDate, correctedInvoiceIndicator, isCallBcmoax,
                isNoReinstatement, quoteIndicator, tranferCashPolicyId, tranferCashPolicyNumber, userSequenceId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        CashApplyDriver other = (CashApplyDriver) obj;
        return Objects.equals(accountId, other.accountId) && Objects.equals(cashApplyDate, other.cashApplyDate)
                && Objects.equals(correctedInvoiceIndicator, other.correctedInvoiceIndicator)
                && isCallBcmoax == other.isCallBcmoax && isNoReinstatement == other.isNoReinstatement
                && Objects.equals(quoteIndicator, other.quoteIndicator)
                && Objects.equals(tranferCashPolicyId, other.tranferCashPolicyId)
                && Objects.equals(userSequenceId, other.userSequenceId)
                && Objects.equals(tranferCashPolicyNumber, other.tranferCashPolicyNumber);
    }

}
