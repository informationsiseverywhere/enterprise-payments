package billing.model;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RejectionPayment extends ResourceSupport {

    @JsonInclude(JsonInclude.Include.ALWAYS)
    private short paymentSequenceNumber;
    private String batchId;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime entryDate;
    private String transactionNumber;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime settlementDate;
    private Double settlementAmount;
    private String rejectionReason;
    private String identifier;
    private String identifierType;
    private String bankAccountNumber;
    private String creditCardNumber;
    private String creditCardType;
    private String payorName;
    private String agencyNumber;
    private String policySymbol;
    private String policyNumber;
    private Boolean isUnidentified = false;
    private String rejectionStatus;
    private String referenceNumber;

    @JsonCreator
    public RejectionPayment(@JsonProperty("paymentSequenceNumber") short paymentSequenceNumber,
            @JsonProperty("transactionNumber") String transactionNumber,
            @JsonProperty("settlementDate") ZonedDateTime settlementDate,
            @JsonProperty("rejectionReason") String rejectionReason,
            @JsonProperty("settlementAmount") Double settlementAmount, @JsonProperty("identifier") String identifier,
            @JsonProperty("identifierType") String identifierType, @JsonProperty("payorName") String payorName,
            @JsonProperty("agencyNumber") String agencyNumber, @JsonProperty("isUnidentified") Boolean isUnidentified,
            @JsonProperty("policySymbol") String policySymbol, @JsonProperty("policyNumber") String policyNumber,
            @JsonProperty("rejectionStatus") String rejectionStatus,
            @JsonProperty("bankAccountNumber") String bankAccountNumber,
            @JsonProperty("creditCardNumber") String creditCardNumber,
            @JsonProperty("creditCardType") String creditCardType, @JsonProperty("batchId") String batchId,
            @JsonProperty("entryDate") ZonedDateTime entryDate) {

        super();
        this.paymentSequenceNumber = paymentSequenceNumber;
        this.transactionNumber = transactionNumber;
        this.settlementDate = settlementDate;
        this.rejectionReason = rejectionReason;
        this.settlementAmount = settlementAmount;
        this.identifier = identifier;
        this.identifierType = identifierType;
        this.payorName = payorName;
        this.agencyNumber = agencyNumber;
        this.isUnidentified = isUnidentified;
        this.policySymbol = policySymbol;
        this.policyNumber = policyNumber;
        this.rejectionStatus = rejectionStatus;
        this.bankAccountNumber = bankAccountNumber;
        this.creditCardNumber = creditCardNumber;
        this.creditCardType = creditCardType;
        this.batchId = batchId;
        this.entryDate = entryDate;

    }

    public RejectionPayment() {
    }

    public RejectionPayment(RejectionPayment rejectionPayment) {
        this(rejectionPayment.getPaymentSequenceNumber(), rejectionPayment.getTransactionNumber(),
                rejectionPayment.getSettlementDate(), rejectionPayment.getRejectionReason(),
                rejectionPayment.getSettlementAmount(), rejectionPayment.getIdentifier(),
                rejectionPayment.getIdentifierType(), rejectionPayment.getPayorName(),
                rejectionPayment.getAgencyNumber(), rejectionPayment.getIsUnidentified(),
                rejectionPayment.getPolicySymbol(), rejectionPayment.getPolicyNumber(),
                rejectionPayment.getRejectionStatus(), rejectionPayment.getBankAccountNumber(),
                rejectionPayment.getCreditCardNumber(), rejectionPayment.getCreditCardType(),
                rejectionPayment.getBatchId(), rejectionPayment.getEntryDate());

    }

    public short getPaymentSequenceNumber() {
        return paymentSequenceNumber;
    }

    public void setPaymentSequenceNumber(short paymentSequenceNumber) {
        this.paymentSequenceNumber = paymentSequenceNumber;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public ZonedDateTime getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(ZonedDateTime settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public Double getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(Double settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getPayorName() {
        return payorName;
    }

    public void setPayorName(String payorName) {
        this.payorName = payorName;
    }

    public String getAgencyNumber() {
        return agencyNumber;
    }

    public void setAgencyNumber(String agencyNumber) {
        this.agencyNumber = agencyNumber;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getCreditCardType() {
        return creditCardType;
    }

    public void setCreditCardType(String creditCardType) {
        this.creditCardType = creditCardType;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public ZonedDateTime getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(ZonedDateTime entryDate) {
        this.entryDate = entryDate;
    }

    public String getIdentifierType() {
        return identifierType;
    }

    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    public Boolean getIsUnidentified() {
        return isUnidentified;
    }

    public void setIsUnidentified(Boolean isUnidentified) {
        this.isUnidentified = isUnidentified;
    }

    public String getPolicySymbol() {
        return policySymbol;
    }

    public void setPolicySymbol(String policySymbol) {
        this.policySymbol = policySymbol;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getRejectionStatus() {
        return rejectionStatus;
    }

    public void setRejectionStatus(String rejectionStatus) {
        this.rejectionStatus = rejectionStatus;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(agencyNumber, bankAccountNumber, batchId, creditCardNumber,
                creditCardType, entryDate, identifier, identifierType, isUnidentified, paymentSequenceNumber, payorName,
                policyNumber, policySymbol, referenceNumber, rejectionReason, rejectionStatus, settlementAmount,
                settlementDate, transactionNumber);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        RejectionPayment other = (RejectionPayment) obj;
        return Objects.equals(agencyNumber, other.agencyNumber)
                && Objects.equals(bankAccountNumber, other.bankAccountNumber) && Objects.equals(batchId, other.batchId)
                && Objects.equals(creditCardNumber, other.creditCardNumber)
                && Objects.equals(creditCardType, other.creditCardType) && Objects.equals(entryDate, other.entryDate)
                && Objects.equals(identifier, other.identifier) && Objects.equals(identifierType, other.identifierType)
                && Objects.equals(isUnidentified, other.isUnidentified)
                && paymentSequenceNumber == other.paymentSequenceNumber && Objects.equals(payorName, other.payorName)
                && Objects.equals(policyNumber, other.policyNumber) && Objects.equals(policySymbol, other.policySymbol)
                && Objects.equals(referenceNumber, other.referenceNumber)
                && Objects.equals(rejectionReason, other.rejectionReason)
                && Objects.equals(rejectionStatus, other.rejectionStatus)
                && Objects.equals(settlementAmount, other.settlementAmount)
                && Objects.equals(settlementDate, other.settlementDate)
                && Objects.equals(transactionNumber, other.transactionNumber);
    }

}