package billing.model;

import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import core.model.Paging;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PaymentList extends ResourceSupport {
    private List<Payment> payments;
    private Paging page;

    @JsonCreator
    public PaymentList(@JsonProperty("payments") List<Payment> payments, @JsonProperty("page") Paging page) {
        super();
        this.payments = payments;
        this.page = page;
    }

    public PaymentList() {

    }

    public PaymentList(PaymentList paymentList) {
        this(paymentList.getPayments(), paymentList.getPage());
    }

    public Paging getPage() {
        return page;
    }

    public void setPage(Paging page) {
        this.page = page;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(page, payments);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PaymentList other = (PaymentList) obj;
        return Objects.equals(page, other.page) && Objects.equals(payments, other.payments);
    }
}
