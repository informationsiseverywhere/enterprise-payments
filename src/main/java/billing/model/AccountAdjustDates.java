package billing.model;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AccountAdjustDates extends ResourceSupport {

    private String accountId;
    private String folderId;
    private String businessCaseId;
    private String applicationName;
    private String userId;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime se3Date;
    private char dateResetIndicator;
    private char startDateIndicator;
    private char referenceDateIndicator;
    private char billPlanChangeIndicator;
    private char supportPlanIndicator;
    private char invoiceIndicator;
    private char suspendBillIndicator;
    private char addDateIndicator;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime oldDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime invoiceDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime newDate;
    private char addChargeIndicator;
    private char chargeType;
    private Double chargeAmount;
    private char errorIndicator;
    private String programName;
    private String paragraphName;
    private int sqlCode;
    private String errorDescription;
    private String errorKeyId;
    private String policyId;
    private String policyEffectiveDate;
    private boolean quoteIndicator;
    private String oldBilClass;
    private String activityDescriptionCode;
    private String reasonType;
    private String thirdPartyId;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime startReferenceDate;
    private String collectionMethod;
    private String collectionPlan;
    private int referenceLastDay;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime dueDate;
    private char statementLastIndicator;

    @JsonCreator
    public AccountAdjustDates(@JsonProperty("accountId") String accountId, @JsonProperty("folderId") String folderId,
            @JsonProperty("businessCaseId") String businessCaseId,
            @JsonProperty("applicationName") String applicationName, @JsonProperty("userId") String userId,
            @JsonProperty("se3Date") ZonedDateTime se3Date, @JsonProperty("dateResetIndicator") char dateResetIndicator,
            @JsonProperty("startDateIndicator") char startDateIndicator,
            @JsonProperty("referenceDateIndicator") char referenceDateIndicator,
            @JsonProperty("billPlanChangeIndicator") char billPlanChangeIndicator,
            @JsonProperty("supportPlanIndicator") char supportPlanIndicator,
            @JsonProperty("invoiceIndicator") char invoiceIndicator,
            @JsonProperty("suspendBillIndicator") char suspendBillIndicator,
            @JsonProperty("addDateIndicator") char addDateIndicator, @JsonProperty("oldDate") ZonedDateTime oldDate,
            @JsonProperty("invoiceDate") ZonedDateTime invoiceDate, @JsonProperty("newDate") ZonedDateTime newDate,
            @JsonProperty("addChargeIndicator") char addChargeIndicator, @JsonProperty("chargeType") char chargeType,
            @JsonProperty("chargeAmount") Double chargeAmount, @JsonProperty("errorIndicator") char errorIndicator,
            @JsonProperty("programName") String programName, @JsonProperty("paragraphName") String paragraphName,
            @JsonProperty("sqlCode") int sqlCode, @JsonProperty("errorDescription") String errorDescription,
            @JsonProperty("errorKeyId") String errorKeyId, @JsonProperty("policyId") String policyId,
            @JsonProperty("policyEffectiveDate") String policyEffectiveDate,
            @JsonProperty("quoteIndicator") boolean quoteIndicator, @JsonProperty("oldBilClass") String oldBilClass,
            @JsonProperty("activityDescriptionCode") String activityDescriptionCode,
            @JsonProperty("reasonType") String reasonType, @JsonProperty("thirdPartyId") String thirdPartyId,
            @JsonProperty("startReferenceDate") ZonedDateTime startReferenceDate,
            @JsonProperty("collectionMethod") String collectionMethod,
            @JsonProperty("collectionPlan") String collectionPlan,
            @JsonProperty("referenceLastDay") int referenceLastDay, @JsonProperty("dueDate") ZonedDateTime dueDate,
            @JsonProperty("statementLastIndicator") char statementLastIndicator) {
        super();
        this.accountId = accountId;
        this.folderId = folderId;
        this.businessCaseId = businessCaseId;
        this.applicationName = applicationName;
        this.userId = userId;
        this.se3Date = se3Date;
        this.dateResetIndicator = dateResetIndicator;
        this.startDateIndicator = startDateIndicator;
        this.referenceDateIndicator = referenceDateIndicator;
        this.billPlanChangeIndicator = billPlanChangeIndicator;
        this.supportPlanIndicator = supportPlanIndicator;
        this.invoiceIndicator = invoiceIndicator;
        this.suspendBillIndicator = suspendBillIndicator;
        this.addDateIndicator = addDateIndicator;
        this.oldDate = oldDate;
        this.invoiceDate = invoiceDate;
        this.newDate = newDate;
        this.addChargeIndicator = addChargeIndicator;
        this.chargeType = chargeType;
        this.chargeAmount = chargeAmount;
        this.errorIndicator = errorIndicator;
        this.programName = programName;
        this.paragraphName = paragraphName;
        this.sqlCode = sqlCode;
        this.errorDescription = errorDescription;
        this.errorKeyId = errorKeyId;
        this.policyId = policyId;
        this.policyEffectiveDate = policyEffectiveDate;
        this.quoteIndicator = quoteIndicator;
        this.oldBilClass = oldBilClass;
        this.activityDescriptionCode = activityDescriptionCode;
        this.reasonType = reasonType;
        this.thirdPartyId = thirdPartyId;
        this.startReferenceDate = startReferenceDate;
        this.collectionMethod = collectionMethod;
        this.collectionPlan = collectionPlan;
        this.referenceLastDay = referenceLastDay;
        this.dueDate = dueDate;
        this.statementLastIndicator = statementLastIndicator;
    }

    public AccountAdjustDates() {
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getBusinessCaseId() {
        return businessCaseId;
    }

    public void setBusinessCaseId(String businessCaseId) {
        this.businessCaseId = businessCaseId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ZonedDateTime getSe3Date() {
        return se3Date;
    }

    public void setSe3Date(ZonedDateTime se3Date) {
        this.se3Date = se3Date;
    }

    public char getDateResetIndicator() {
        return dateResetIndicator;
    }

    public void setDateResetIndicator(char dateResetIndicator) {
        this.dateResetIndicator = dateResetIndicator;
    }

    public char getStartDateIndicator() {
        return startDateIndicator;
    }

    public void setStartDateIndicator(char startDateIndicator) {
        this.startDateIndicator = startDateIndicator;
    }

    public char getReferenceDateIndicator() {
        return referenceDateIndicator;
    }

    public void setReferenceDateIndicator(char referenceDateIndicator) {
        this.referenceDateIndicator = referenceDateIndicator;
    }

    public char getBillPlanChangeIndicator() {
        return billPlanChangeIndicator;
    }

    public void setBillPlanChangeIndicator(char billPlanChangeIndicator) {
        this.billPlanChangeIndicator = billPlanChangeIndicator;
    }

    public char getSupportPlanIndicator() {
        return supportPlanIndicator;
    }

    public void setSupportPlanIndicator(char supportPlanIndicator) {
        this.supportPlanIndicator = supportPlanIndicator;
    }

    public char getInvoiceIndicator() {
        return invoiceIndicator;
    }

    public void setInvoiceIndicator(char invoiceIndicator) {
        this.invoiceIndicator = invoiceIndicator;
    }

    public char getSuspendBillIndicator() {
        return suspendBillIndicator;
    }

    public void setSuspendBillIndicator(char suspendBillIndicator) {
        this.suspendBillIndicator = suspendBillIndicator;
    }

    public char getAddDateIndicator() {
        return addDateIndicator;
    }

    public void setAddDateIndicator(char addDateIndicator) {
        this.addDateIndicator = addDateIndicator;
    }

    public ZonedDateTime getOldDate() {
        return oldDate;
    }

    public void setOldDate(ZonedDateTime oldDate) {
        this.oldDate = oldDate;
    }

    public ZonedDateTime getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(ZonedDateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public ZonedDateTime getNewDate() {
        return newDate;
    }

    public void setNewDate(ZonedDateTime newDate) {
        this.newDate = newDate;
    }

    public char getAddChargeIndicator() {
        return addChargeIndicator;
    }

    public void setAddChargeIndicator(char addChargeIndicator) {
        this.addChargeIndicator = addChargeIndicator;
    }

    public char getChargeType() {
        return chargeType;
    }

    public void setChargeType(char chargeType) {
        this.chargeType = chargeType;
    }

    public Double getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(Double chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public char getErrorIndicator() {
        return errorIndicator;
    }

    public void setErrorIndicator(char errorIndicator) {
        this.errorIndicator = errorIndicator;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getParagraphName() {
        return paragraphName;
    }

    public void setParagraphName(String paragraphName) {
        this.paragraphName = paragraphName;
    }

    public int getSqlCode() {
        return sqlCode;
    }

    public void setSqlCode(int sqlCode) {
        this.sqlCode = sqlCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getErrorKeyId() {
        return errorKeyId;
    }

    public void setErrorKeyId(String errorKeyId) {
        this.errorKeyId = errorKeyId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicyEffectiveDate() {
        return policyEffectiveDate;
    }

    public void setPolicyEffectiveDate(String policyEffectiveDate) {
        this.policyEffectiveDate = policyEffectiveDate;
    }

    public boolean isQuoteIndicator() {
        return quoteIndicator;
    }

    public void setQuoteIndicator(boolean quoteIndicator) {
        this.quoteIndicator = quoteIndicator;
    }

    public String getOldBilClass() {
        return oldBilClass;
    }

    public void setOldBilClass(String oldBilClass) {
        this.oldBilClass = oldBilClass;
    }

    public String getActivityDescriptionCode() {
        return activityDescriptionCode;
    }

    public void setActivityDescriptionCode(String activityDescriptionCode) {
        this.activityDescriptionCode = activityDescriptionCode;
    }

    public String getReasonType() {
        return reasonType;
    }

    public void setReasonType(String reasonType) {
        this.reasonType = reasonType;
    }

    public String getThirdPartyId() {
        return thirdPartyId;
    }

    public void setThirdPartyId(String thirdPartyId) {
        this.thirdPartyId = thirdPartyId;
    }

    public ZonedDateTime getStartReferenceDate() {
        return startReferenceDate;
    }

    public void setStartReferenceDate(ZonedDateTime startReferenceDate) {
        this.startReferenceDate = startReferenceDate;
    }

    public String getCollectionMethod() {
        return collectionMethod;
    }

    public void setCollectionMethod(String collectionMethod) {
        this.collectionMethod = collectionMethod;
    }

    public String getCollectionPlan() {
        return collectionPlan;
    }

    public void setCollectionPlan(String collectionPlan) {
        this.collectionPlan = collectionPlan;
    }

    public int getReferenceLastDay() {
        return referenceLastDay;
    }

    public void setReferenceLastDay(int referenceLastDay) {
        this.referenceLastDay = referenceLastDay;
    }

    public ZonedDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(ZonedDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public char getStatementLastIndicator() {
        return statementLastIndicator;
    }

    public void setStatementLastIndicator(char statementLastIndicator) {
        this.statementLastIndicator = statementLastIndicator;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountId, activityDescriptionCode, addChargeIndicator, addDateIndicator,
                applicationName, billPlanChangeIndicator, businessCaseId, chargeAmount, chargeType, collectionMethod,
                collectionPlan, dateResetIndicator, dueDate, errorDescription, errorIndicator, errorKeyId, folderId,
                invoiceDate, invoiceIndicator, newDate, oldBilClass, oldDate, paragraphName, policyEffectiveDate,
                policyId, programName, quoteIndicator, reasonType, referenceDateIndicator, referenceLastDay, se3Date,
                sqlCode, startDateIndicator, startReferenceDate, statementLastIndicator, supportPlanIndicator,
                suspendBillIndicator, thirdPartyId, userId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AccountAdjustDates other = (AccountAdjustDates) obj;
        return Objects.equals(accountId, other.accountId)
                && Objects.equals(activityDescriptionCode, other.activityDescriptionCode)
                && addChargeIndicator == other.addChargeIndicator && addDateIndicator == other.addDateIndicator
                && Objects.equals(applicationName, other.applicationName)
                && billPlanChangeIndicator == other.billPlanChangeIndicator
                && Objects.equals(businessCaseId, other.businessCaseId)
                && Objects.equals(chargeAmount, other.chargeAmount) && chargeType == other.chargeType
                && Objects.equals(collectionMethod, other.collectionMethod)
                && Objects.equals(collectionPlan, other.collectionPlan)
                && dateResetIndicator == other.dateResetIndicator && Objects.equals(dueDate, other.dueDate)
                && Objects.equals(errorDescription, other.errorDescription) && errorIndicator == other.errorIndicator
                && Objects.equals(errorKeyId, other.errorKeyId) && Objects.equals(folderId, other.folderId)
                && Objects.equals(invoiceDate, other.invoiceDate) && invoiceIndicator == other.invoiceIndicator
                && Objects.equals(newDate, other.newDate) && Objects.equals(oldBilClass, other.oldBilClass)
                && Objects.equals(oldDate, other.oldDate) && Objects.equals(paragraphName, other.paragraphName)
                && Objects.equals(policyEffectiveDate, other.policyEffectiveDate)
                && Objects.equals(policyId, other.policyId) && Objects.equals(programName, other.programName)
                && quoteIndicator == other.quoteIndicator && Objects.equals(reasonType, other.reasonType)
                && referenceDateIndicator == other.referenceDateIndicator && referenceLastDay == other.referenceLastDay
                && Objects.equals(se3Date, other.se3Date) && sqlCode == other.sqlCode
                && startDateIndicator == other.startDateIndicator
                && Objects.equals(startReferenceDate, other.startReferenceDate)
                && statementLastIndicator == other.statementLastIndicator
                && supportPlanIndicator == other.supportPlanIndicator
                && suspendBillIndicator == other.suspendBillIndicator
                && Objects.equals(thirdPartyId, other.thirdPartyId) && Objects.equals(userId, other.userId);
    }

}