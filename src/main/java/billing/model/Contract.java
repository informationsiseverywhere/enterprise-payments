package billing.model;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Contract extends ResourceSupport {

    @JsonProperty("id")
    private String accountId;
    private String policySymbol;
    private String policyNumber;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime policyEffectiveDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime policyExpirationDate;
    private String status;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime paidToDate;
    private String policyPlan;
    private String policyHolderName;
    private String pendingPlan;
    private Boolean splitIndicator;
    private String insuredId;
    private String insuredName;
    private String insuredAddress;
    private Integer insuredAddressId;
    private String insuredPictureId;
    private String policyId;
    private String lineOfBusiness;
    private Double totalPremium;
    private Boolean pendingPolicyPlan;
    private Boolean manualReinvoice;
    private Boolean overrideEquity;
    private String transferredTo;
    private String transferredFrom;
    private String transferredToAccountId;
    private String transferredFromAccountId;
    private Double accruedPremium;
    private Double premiumAmount;

    public Contract() {
        super();
    }

    @JsonCreator
    public Contract(@JsonProperty("id") String accountId, @JsonProperty("policySymbol") String policySymbol,
            @JsonProperty("policyNumber") String policyNumber,
            @JsonProperty("policyEffectiveDate") ZonedDateTime policyEffectiveDate,
            @JsonProperty("policyExpirationDate") ZonedDateTime policyExpirationDate,
            @JsonProperty("status") String status, @JsonProperty("paidToDate") ZonedDateTime paidToDate,
            @JsonProperty("policyPlan") String policyPlan, @JsonProperty("policyHolderName") String policyHolderName,
            @JsonProperty("pendingPlan") String pendingPlan, @JsonProperty("splitIndicator") Boolean splitIndicator,
            @JsonProperty("insuredId") String insuredId, @JsonProperty("insuredName") String insuredName,
            @JsonProperty("insuredAddress") String insuredAddress,
            @JsonProperty("insuredAddressId") Integer insuredAddressId,
            @JsonProperty("insuredPictureId") String insuredPictureId, @JsonProperty("policyId") String policyId,
            @JsonProperty("lineOfBusiness") String lineOfBusiness, @JsonProperty("totalPremium") Double totalPremium,
            @JsonProperty("pendingPolicyPlan") Boolean pendingPolicyPlan,
            @JsonProperty("manualReinvoice") Boolean manualReinvoice,
            @JsonProperty("overrideEquity") Boolean overrideEquity, @JsonProperty("transferredTo") String transferredTo,
            @JsonProperty("transferredFrom") String transferredFrom,
            @JsonProperty("transferredToAccountId") String transferredToAccountId,
            @JsonProperty("transferredFromAccountId") String transferredFromAccountId,
            @JsonProperty("accruedPremium") Double accruedPremium,
            @JsonProperty("premiumAmount") Double premiumAmount) {
        super();
        this.accountId = accountId;
        this.policySymbol = policySymbol;
        this.policyNumber = policyNumber;
        this.policyEffectiveDate = policyEffectiveDate;
        this.policyExpirationDate = policyExpirationDate;
        this.status = status;
        this.paidToDate = paidToDate;
        this.policyPlan = policyPlan;
        this.policyHolderName = policyHolderName;
        this.pendingPlan = pendingPlan;
        this.splitIndicator = splitIndicator;
        this.insuredId = insuredId;
        this.insuredName = insuredName;
        this.insuredAddress = insuredAddress;
        this.insuredAddressId = insuredAddressId;
        this.insuredPictureId = insuredPictureId;
        this.policyId = policyId;
        this.lineOfBusiness = lineOfBusiness;
        this.totalPremium = totalPremium;
        this.pendingPolicyPlan = pendingPolicyPlan;
        this.manualReinvoice = manualReinvoice;
        this.overrideEquity = overrideEquity;
        this.transferredTo = transferredTo;
        this.transferredFrom = transferredFrom;
        this.transferredToAccountId = transferredToAccountId;
        this.transferredFromAccountId = transferredFromAccountId;
        this.accruedPremium = accruedPremium;
        this.premiumAmount = premiumAmount;
    }

    public Contract(Contract contract) {
        this(contract.getAccountId(), contract.getPolicySymbol(), contract.getPolicyNumber(),
                contract.getPolicyEffectiveDate(), contract.getPolicyExpirationDate(), contract.getStatus(),
                contract.getPaidToDate(), contract.getPolicyPlan(), contract.getPolicyHolderName(),
                contract.getPendingPlan(), contract.getSplitIndicator(), contract.getInsuredId(),
                contract.getInsuredName(), contract.getInsuredAddress(), contract.getInsuredAddressId(),
                contract.getInsuredPictureId(), contract.getPolicyId(), contract.getLineOfBusiness(),
                contract.getTotalPremium(), contract.getPendingPolicyPlan(), contract.getManualReinvoice(),
                contract.getOverrideEquity(), contract.getTransferredTo(), contract.getTransferredFrom(),
                contract.getTransferredToAccountId(), contract.getTransferredFromAccountId(),
                contract.getAccruedPremium(), contract.getPremiumAmount());
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPolicySymbol() {
        return policySymbol;
    }

    public void setPolicySymbol(String policySymbol) {
        this.policySymbol = policySymbol;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public ZonedDateTime getPolicyEffectiveDate() {
        return policyEffectiveDate;
    }

    public void setPolicyEffectiveDate(ZonedDateTime policyEffectiveDate) {
        this.policyEffectiveDate = policyEffectiveDate;
    }

    public ZonedDateTime getPolicyExpirationDate() {
        return policyExpirationDate;
    }

    public void setPolicyExpirationDate(ZonedDateTime policyExpirationDate) {
        this.policyExpirationDate = policyExpirationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ZonedDateTime getPaidToDate() {
        return paidToDate;
    }

    public void setPaidToDate(ZonedDateTime paidToDate) {
        this.paidToDate = paidToDate;
    }

    public String getPolicyPlan() {
        return policyPlan;
    }

    public void setPolicyPlan(String policyPlan) {
        this.policyPlan = policyPlan;
    }

    public String getPolicyHolderName() {
        return policyHolderName;
    }

    public void setPolicyHolderName(String policyHolderName) {
        this.policyHolderName = policyHolderName;
    }

    public String getPendingPlan() {
        return pendingPlan;
    }

    public void setPendingPlan(String pendingPlan) {
        this.pendingPlan = pendingPlan;
    }

    public Boolean getSplitIndicator() {
        return splitIndicator;
    }

    public void setSplitIndicator(Boolean splitIndicator) {
        this.splitIndicator = splitIndicator;
    }

    public String getInsuredId() {
        return insuredId;
    }

    public void setInsuredId(String insuredId) {
        this.insuredId = insuredId;
    }

    public String getInsuredName() {
        return insuredName;
    }

    public void setInsuredName(String insuredName) {
        this.insuredName = insuredName;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getLineOfBusiness() {
        return lineOfBusiness;
    }

    public void setLineOfBusiness(String lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }

    public Double getTotalPremium() {
        return totalPremium;
    }

    public void setTotalPremium(Double totalPremium) {
        this.totalPremium = totalPremium;
    }

    public Boolean getPendingPolicyPlan() {
        return pendingPolicyPlan;
    }

    public void setPendingPolicyPlan(Boolean pendingPolicyPlan) {
        this.pendingPolicyPlan = pendingPolicyPlan;
    }

    public Boolean getManualReinvoice() {
        return manualReinvoice;
    }

    public void setManualReinvoice(Boolean manualReinvoice) {
        this.manualReinvoice = manualReinvoice;
    }

    public Boolean getOverrideEquity() {
        return overrideEquity;
    }

    public void setOverrideEquity(Boolean overrideEquity) {
        this.overrideEquity = overrideEquity;
    }

    public String getInsuredAddress() {
        return insuredAddress;
    }

    public void setInsuredAddress(String insuredAddress) {
        this.insuredAddress = insuredAddress;
    }

    public Integer getInsuredAddressId() {
        return insuredAddressId;
    }

    public void setInsuredAddressId(Integer insuredAddressId) {
        this.insuredAddressId = insuredAddressId;
    }

    public String getInsuredPictureId() {
        return insuredPictureId;
    }

    public void setInsuredPictureId(String insuredPictureId) {
        this.insuredPictureId = insuredPictureId;
    }

    public String getTransferredTo() {
        return transferredTo;
    }

    public void setTransferredTo(String transferredTo) {
        this.transferredTo = transferredTo;
    }

    public String getTransferredFrom() {
        return transferredFrom;
    }

    public void setTransferredFrom(String transferredFrom) {
        this.transferredFrom = transferredFrom;
    }

    public String getTransferredToAccountId() {
        return transferredToAccountId;
    }

    public void setTransferredToAccountId(String transferredToAccountId) {
        this.transferredToAccountId = transferredToAccountId;
    }

    public String getTransferredFromAccountId() {
        return transferredFromAccountId;
    }

    public void setTransferredFromAccountId(String transferredFromAccountId) {
        this.transferredFromAccountId = transferredFromAccountId;
    }

    public Double getAccruedPremium() {
        return accruedPremium;
    }

    public void setAccruedPremium(Double accruedPremium) {
        this.accruedPremium = accruedPremium;
    }

    public Double getPremiumAmount() {
        return premiumAmount;
    }

    public void setPremiumAmount(Double premiumAmount) {
        this.premiumAmount = premiumAmount;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountId, accruedPremium, insuredAddress, insuredAddressId, insuredId,
                insuredName, insuredPictureId, lineOfBusiness, manualReinvoice, overrideEquity, paidToDate, pendingPlan,
                pendingPolicyPlan, policyEffectiveDate, policyExpirationDate, policyHolderName, policyId, policyNumber,
                policyPlan, policySymbol, premiumAmount, splitIndicator, status, totalPremium, transferredFrom,
                transferredFromAccountId, transferredTo, transferredToAccountId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Contract other = (Contract) obj;
        return Objects.equals(accountId, other.accountId) && Objects.equals(accruedPremium, other.accruedPremium)
                && Objects.equals(insuredAddress, other.insuredAddress)
                && Objects.equals(insuredAddressId, other.insuredAddressId)
                && Objects.equals(insuredId, other.insuredId) && Objects.equals(insuredName, other.insuredName)
                && Objects.equals(insuredPictureId, other.insuredPictureId)
                && Objects.equals(lineOfBusiness, other.lineOfBusiness)
                && Objects.equals(manualReinvoice, other.manualReinvoice)
                && Objects.equals(overrideEquity, other.overrideEquity) && Objects.equals(paidToDate, other.paidToDate)
                && Objects.equals(pendingPlan, other.pendingPlan)
                && Objects.equals(pendingPolicyPlan, other.pendingPolicyPlan)
                && Objects.equals(policyEffectiveDate, other.policyEffectiveDate)
                && Objects.equals(policyExpirationDate, other.policyExpirationDate)
                && Objects.equals(policyHolderName, other.policyHolderName) && Objects.equals(policyId, other.policyId)
                && Objects.equals(policyNumber, other.policyNumber) && Objects.equals(policyPlan, other.policyPlan)
                && Objects.equals(policySymbol, other.policySymbol)
                && Objects.equals(premiumAmount, other.premiumAmount)
                && Objects.equals(splitIndicator, other.splitIndicator) && Objects.equals(status, other.status)
                && Objects.equals(totalPremium, other.totalPremium)
                && Objects.equals(transferredFrom, other.transferredFrom)
                && Objects.equals(transferredFromAccountId, other.transferredFromAccountId)
                && Objects.equals(transferredTo, other.transferredTo)
                && Objects.equals(transferredToAccountId, other.transferredToAccountId);
    }

}
