package billing.model;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BatchRejection extends ResourceSupport {

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime entryDate;
    private String batchId;
    private Double accumulatedAmount;
    private String userName;
    private Integer numberOfTransactions;
    private Boolean isAccept;

    public BatchRejection(@JsonProperty("entryDate") ZonedDateTime entryDate, @JsonProperty("batchId") String batchId,
            @JsonProperty("isAccept") Boolean isAccept, @JsonProperty("userName") String userName,
            @JsonProperty("accumulatedAmount") Double accumulatedAmount,
            @JsonProperty("numberOfTransactions") Integer numberOfTransactions) {
        super();
        this.entryDate = entryDate;
        this.batchId = batchId;
        this.isAccept = isAccept;
        this.userName = userName;
        this.accumulatedAmount = accumulatedAmount;
        this.numberOfTransactions = numberOfTransactions;
    }

    public BatchRejection() {

    }

    public ZonedDateTime getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(ZonedDateTime entryDate) {
        this.entryDate = entryDate;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Double getAccumulatedAmount() {
        return accumulatedAmount;
    }

    public void setAccumulatedAmount(Double accumulatedAmount) {
        this.accumulatedAmount = accumulatedAmount;
    }

    public Boolean getIsAccept() {
        return isAccept;
    }

    public void setIsAccept(Boolean isAccept) {
        this.isAccept = isAccept;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getNumberOfTransactions() {
        return numberOfTransactions;
    }

    public void setNumberOfTransactions(Integer numberOfTransactions) {
        this.numberOfTransactions = numberOfTransactions;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + Objects.hash(accumulatedAmount, batchId, entryDate, isAccept, numberOfTransactions, userName);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        BatchRejection other = (BatchRejection) obj;
        return Objects.equals(accumulatedAmount, other.accumulatedAmount) && Objects.equals(batchId, other.batchId)
                && Objects.equals(entryDate, other.entryDate) && Objects.equals(isAccept, other.isAccept)
                && Objects.equals(numberOfTransactions, other.numberOfTransactions)
                && Objects.equals(userName, other.userName);
    }

}
