package billing.model;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountHistory extends ResourceSupport {
    private String accountId;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private ZonedDateTime activityDate;
    private short sequence;
    private String activityDescription;
    private double amount;
    private String userName;
    private String activityCd;
    private String activityType;
    private String policyNumber;
    private String policySymbolCd;
    private String additionalDataTxt;
    private ZonedDateTime activityDesc1Dt;
    private ZonedDateTime activityDesc2Dt;

    public AccountHistory(@JsonProperty("accountId") String accountId,
            @JsonProperty("activityDate") ZonedDateTime activityDate, @JsonProperty("sequence") short sequence,
            @JsonProperty("activityDescription") String activityDescription, @JsonProperty("amount") double amount,
            @JsonProperty("userName") String userName, @JsonProperty("activityCd") String activityCd,
            @JsonProperty("activityType") String activityType, @JsonProperty("policyNumber") String policyNumber,
            @JsonProperty("policySymbolCd") String policySymbolCd,
            @JsonProperty("additionalDataTxt") String additionalDataTxt,
            @JsonProperty("activityDesc1Dt") ZonedDateTime activityDesc1Dt,
            @JsonProperty("activityDesc2Dt") ZonedDateTime activityDesc2Dt) {
        super();
        this.accountId = accountId;
        this.activityDate = activityDate;
        this.sequence = sequence;
        this.activityDescription = activityDescription;
        this.amount = amount;
        this.userName = userName;
        this.activityCd = activityCd;
        this.activityType = activityType;
        this.policyNumber = policyNumber;
        this.policySymbolCd = policySymbolCd;
        this.additionalDataTxt = additionalDataTxt;
        this.activityDesc1Dt = activityDesc1Dt;
        this.activityDesc2Dt = activityDesc2Dt;
    }

    public AccountHistory() {
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public ZonedDateTime getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(ZonedDateTime activityDate) {
        this.activityDate = activityDate;
    }

    public short getSequence() {
        return sequence;
    }

    public void setSequence(short sequence) {
        this.sequence = sequence;
    }

    public String getActivityDescription() {
        return activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getActivityCd() {
        return activityCd;
    }

    public void setActivityCd(String activityCd) {
        this.activityCd = activityCd;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicySymbolCd() {
        return policySymbolCd;
    }

    public void setPolicySymbolCd(String policySymbolCd) {
        this.policySymbolCd = policySymbolCd;
    }

    public String getAdditionalDataTxt() {
        return additionalDataTxt;
    }

    public void setAdditionalDataTxt(String additionalDataTxt) {
        this.additionalDataTxt = additionalDataTxt;
    }

    public ZonedDateTime getActivityDesc1Dt() {
        return activityDesc1Dt;
    }

    public void setActivityDesc1Dt(ZonedDateTime activityDesc1Dt) {
        this.activityDesc1Dt = activityDesc1Dt;
    }

    public ZonedDateTime getActivityDesc2Dt() {
        return activityDesc2Dt;
    }

    public void setActivityDesc2Dt(ZonedDateTime activityDesc2Dt) {
        this.activityDesc2Dt = activityDesc2Dt;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountId, activityCd, activityDate, activityDesc1Dt, activityDesc2Dt,
                activityDescription, activityType, additionalDataTxt, amount, policyNumber, policySymbolCd, sequence,
                userName);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AccountHistory other = (AccountHistory) obj;
        return Objects.equals(accountId, other.accountId) && Objects.equals(activityCd, other.activityCd)
                && Objects.equals(activityDate, other.activityDate)
                && Objects.equals(activityDesc1Dt, other.activityDesc1Dt)
                && Objects.equals(activityDesc2Dt, other.activityDesc2Dt)
                && Objects.equals(activityDescription, other.activityDescription)
                && Objects.equals(activityType, other.activityType)
                && Objects.equals(additionalDataTxt, other.additionalDataTxt)
                && Double.doubleToLongBits(amount) == Double.doubleToLongBits(other.amount)
                && Objects.equals(policyNumber, other.policyNumber)
                && Objects.equals(policySymbolCd, other.policySymbolCd) && sequence == other.sequence
                && Objects.equals(userName, other.userName);
    }

}
