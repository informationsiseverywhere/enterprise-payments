package billing.model;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class EftRejectionDetail extends ResourceSupport {

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime postedDate;
    private String batchId;
    private String userId;
    private String traceNumber;
    private String accountNumber;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime settlementDate;
    private Double settlementAmount;
    private String returnReasonCode;
    private String reviewCode;
    private String currency;

    public EftRejectionDetail(@JsonProperty("postedDate") ZonedDateTime postedDate,
            @JsonProperty("batchId") String batchId, @JsonProperty("userId") String userId,
            @JsonProperty("traceNumber") String traceNumber, @JsonProperty("accountNumber") String accountNumber,
            @JsonProperty("settlementDate") ZonedDateTime settlementDate,
            @JsonProperty("settlementAmount") Double settlementAmount,
            @JsonProperty("returnReasonCode") String returnReasonCode, @JsonProperty("reviewCode") String reviewCode,
            @JsonProperty("currency") String currency) {
        super();
        this.postedDate = postedDate;
        this.batchId = batchId;
        this.userId = userId;
        this.traceNumber = traceNumber;
        this.accountNumber = accountNumber;
        this.settlementDate = settlementDate;
        this.settlementAmount = settlementAmount;
        this.returnReasonCode = returnReasonCode;
        this.reviewCode = reviewCode;
        this.currency = currency;

    }

    public EftRejectionDetail() {
        super();
    }

    public ZonedDateTime getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(ZonedDateTime postedDate) {
        this.postedDate = postedDate;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTraceNumber() {
        return traceNumber;
    }

    public void setTraceNumber(String traceNumber) {
        this.traceNumber = traceNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public ZonedDateTime getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(ZonedDateTime settlementDate) {
        this.settlementDate = settlementDate;
    }

    public Double getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(Double settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public String getReturnReasonCode() {
        return returnReasonCode;
    }

    public void setReturnReasonCode(String returnReasonCode) {
        this.returnReasonCode = returnReasonCode;
    }

    public String getReviewCode() {
        return reviewCode;
    }

    public void setReviewCode(String reviewCode) {
        this.reviewCode = reviewCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountNumber, batchId, currency, postedDate, returnReasonCode,
                reviewCode, settlementAmount, settlementDate, traceNumber, userId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        EftRejectionDetail other = (EftRejectionDetail) obj;
        return Objects.equals(accountNumber, other.accountNumber) && Objects.equals(batchId, other.batchId)
                && Objects.equals(currency, other.currency) && Objects.equals(postedDate, other.postedDate)
                && Objects.equals(returnReasonCode, other.returnReasonCode)
                && Objects.equals(reviewCode, other.reviewCode)
                && Objects.equals(settlementAmount, other.settlementAmount)
                && Objects.equals(settlementDate, other.settlementDate)
                && Objects.equals(traceNumber, other.traceNumber) && Objects.equals(userId, other.userId);
    }

}
