package billing.model;

import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ImmediateActivity extends ResourceSupport {

    private String activityDate;
    private String parameter;
    private String moduleName;
    private String referenceId;
    private String userId;

    public ImmediateActivity() {
        super();
    }

    @JsonCreator
    public ImmediateActivity(@JsonProperty("activityDate") String activityDate,
            @JsonProperty("parameter") String parameter, @JsonProperty("moduleName") String moduleName,
            @JsonProperty("referenceId") String referenceId, @JsonProperty("userId") String userId) {
        super();
        this.activityDate = activityDate;
        this.parameter = parameter;
        this.moduleName = moduleName;
        this.referenceId = referenceId;
        this.userId = userId;
    }

    public String getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(String activityDate) {
        this.activityDate = activityDate;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(activityDate, moduleName, parameter, referenceId, userId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ImmediateActivity other = (ImmediateActivity) obj;
        return Objects.equals(activityDate, other.activityDate) && Objects.equals(moduleName, other.moduleName)
                && Objects.equals(parameter, other.parameter) && Objects.equals(referenceId, other.referenceId)
                && Objects.equals(userId, other.userId);
    }

}
