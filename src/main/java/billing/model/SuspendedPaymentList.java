package billing.model;

import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import core.model.Paging;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SuspendedPaymentList extends ResourceSupport {
    private List<SuspendedPayment> suspendedPayments;
    private Paging page;

    @JsonCreator
    public SuspendedPaymentList(@JsonProperty("suspendedPayments") List<SuspendedPayment> suspendedPayments,
            @JsonProperty("page") Paging page) {
        super();
        this.suspendedPayments = suspendedPayments;
        this.page = page;
    }

    public SuspendedPaymentList() {

    }

    public SuspendedPaymentList(SuspendedPaymentList suspendedPaymentList) {
        this(suspendedPaymentList.getSuspendedPayments(), suspendedPaymentList.getPage());
    }

    public Paging getPage() {
        return page;
    }

    public void setPage(Paging page) {
        this.page = page;
    }

    public List<SuspendedPayment> getSuspendedPayments() {
        return suspendedPayments;
    }

    public void setPayments(List<SuspendedPayment> suspendedPayments) {
        this.suspendedPayments = suspendedPayments;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(page, suspendedPayments);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SuspendedPaymentList other = (SuspendedPaymentList) obj;
        return Objects.equals(page, other.page) && Objects.equals(suspendedPayments, other.suspendedPayments);
    }
}
