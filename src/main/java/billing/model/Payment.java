package billing.model;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Payment extends ResourceSupport {

    @JsonInclude(JsonInclude.Include.ALWAYS)
    private short paymentSequenceNumber;
    private Double paymentAmount;
    private String paymentType;
    private String paymentMethod;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime paymentDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime postedDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime postmarkDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime entryDateTime;
    private String batchId;
    private String depositBank;
    private String controlBank;
    private String userId;
    private String userName;
    private String paymentComments;
    private String paymentId;
    private String creditCardType;
    private String creditCardNumber;
    private String creditCardExpiryDate;
    private String creditCardHolderName;
    private String postalCode;
    private String authorization;
    private String routingNumber;
    private String bankAccountNumber;
    private Double suspenseAmount;
    private Double appliedAmount;
    private Double disbursedAmount;
    private Double writeOffAmount;
    private String status;
    private Boolean paymentOnHold;
    private String suspenseReason;
    private String identifier;
    private String identifierType;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime dueDate;
    private String payableItem;
    private String payorId;
    private String payorName;
    private String additionalId;
    private String policySymbol;
    private String policyNumber;
    private String accountId;
    private String accountType;
    private String type;
    private Boolean isUnidentified = false;
    private String transactionId;
    private String authorizationResponse;
    private String authorizationResponseType;
    private String remitterId;
    private String remitterName;
    private String reviewCode;
    private Double currentDue;
    private Double totalDue;
    private Boolean isRecurring;
    private String collectionType;
    private String collectionMethod;
    private String paymentProfileId;
    private String agencyNumber;
    private String agencyClientId; // for navigation
    private String agencyName; // for display
    private String bankHolderName;
    private String payorAddress;
    private String currency;
    private String receivedCurrency;
    private Double receivedAmount;
    private Double rate;
    private String businessGroup;
    private Boolean isEdit;
    private Boolean isDelete;
    private String customerProfileId;
    private String walletType;
    private String walletNumber;
    private String walletIdentifier;

    @JsonCreator
    public Payment(@JsonProperty("paymentSequenceNumber") short paymentSequenceNumber,
            @JsonProperty("paymentAmount") Double paymentAmount, @JsonProperty("paymentType") String paymentType,
            @JsonProperty("paymentMethod") String paymentMethod, @JsonProperty("paymentDate") ZonedDateTime paymentDate,
            @JsonProperty("postedDate") ZonedDateTime postedDate,
            @JsonProperty("postmarkDate") ZonedDateTime postmarkDate,
            @JsonProperty("entryDateTime") ZonedDateTime entryDateTime, @JsonProperty("batchId") String batchId,
            @JsonProperty("depositBank") String depositBank, @JsonProperty("controlBank") String controlBank,
            @JsonProperty("userId") String userId, @JsonProperty("userName") String userName,
            @JsonProperty("paymentComments") String paymentComments, @JsonProperty("paymentId") String paymentId,
            @JsonProperty("creditCardType") String creditCardType,
            @JsonProperty("creditCardNumber") String creditCardNumber,
            @JsonProperty("creditCardExpiryDate") String creditCardExpiryDate,
            @JsonProperty("creditCardHolderName") String creditCardHolderName,
            @JsonProperty("postalCode") String postalCode, @JsonProperty("authorization") String authorization,
            @JsonProperty("routingNumber") String routingNumber,
            @JsonProperty("bankAccountNumber") String bankAccountNumber,
            @JsonProperty("suspenseAmount") Double suspenseAmount, @JsonProperty("appliedAmount") Double appliedAmount,
            @JsonProperty("disbursedAmount") Double disbursedAmount,
            @JsonProperty("writeOffAmount") Double writeOffAmount, @JsonProperty("status") String status,
            @JsonProperty("paymentOnHold") Boolean paymentOnHold, @JsonProperty("suspenseReason") String suspenseReason,
            @JsonProperty("identifier") String identifier, @JsonProperty("identifierType") String identifierType,
            @JsonProperty("dueDate") ZonedDateTime dueDate, @JsonProperty("payableItem") String payableItem,
            @JsonProperty("payorId") String payorId, @JsonProperty("payorName") String payorName,
            @JsonProperty("additionalId") String additionalId, @JsonProperty("policySymbol") String policySymbol,
            @JsonProperty("policyNumber") String policyNumber, @JsonProperty("accountId") String accountId,
            @JsonProperty("accountType") String accountType, @JsonProperty("type") String type,
            @JsonProperty("isUnidentified") Boolean isUnidentified, @JsonProperty("transactionId") String transactionId,
            @JsonProperty("authorizationResponse") String authorizationResponse,
            @JsonProperty("authorizationResponseType") String authorizationResponseType,
            @JsonProperty("remitterId") String remitterId, @JsonProperty("remitterName") String remitterName,
            @JsonProperty("reviewCode") String reviewCode, @JsonProperty("currentDue") Double currentDue,
            @JsonProperty("isRecurring") Boolean isRecurring, @JsonProperty("collectionType") String collectionType,
            @JsonProperty("collectionMethod") String collectionMethod,
            @JsonProperty("agencyNumber") String agencyNumber, @JsonProperty("agencyClientId") String agencyClientId,
            @JsonProperty("agencyName") String agencyName, @JsonProperty("paymentProfileId") String paymentProfileId,
            @JsonProperty("bankHolderName") String bankHolderName, @JsonProperty("payorAddress") String payorAddress,
            @JsonProperty("currency") String currency, @JsonProperty("receivedCurrency") String receivedCurrency,
            @JsonProperty("receivedAmount") Double receivedAmount, @JsonProperty("rate") Double rate,
            @JsonProperty("businessGroup") String businessGroup, @JsonProperty("walletType") String walletType,
            @JsonProperty("walletNumber") String walletNumber,
            @JsonProperty("walletIdentifier") String walletIdentifier) {
        super();
        this.paymentSequenceNumber = paymentSequenceNumber;
        this.paymentAmount = paymentAmount;
        this.paymentType = paymentType;
        this.paymentMethod = paymentMethod;
        this.paymentDate = paymentDate;
        this.postedDate = postedDate;
        this.postmarkDate = postmarkDate;
        this.entryDateTime = entryDateTime;
        this.batchId = batchId;
        this.depositBank = depositBank;
        this.controlBank = controlBank;
        this.userId = userId;
        this.userName = userName;
        this.paymentComments = paymentComments;
        this.paymentId = paymentId;
        this.creditCardType = creditCardType;
        this.creditCardNumber = creditCardNumber;
        this.creditCardExpiryDate = creditCardExpiryDate;
        this.creditCardHolderName = creditCardHolderName;
        this.postalCode = postalCode;
        this.authorization = authorization;
        this.routingNumber = routingNumber;
        this.bankAccountNumber = bankAccountNumber;
        this.suspenseAmount = suspenseAmount;
        this.appliedAmount = appliedAmount;
        this.disbursedAmount = disbursedAmount;
        this.writeOffAmount = writeOffAmount;
        this.status = status;
        this.paymentOnHold = paymentOnHold;
        this.suspenseReason = suspenseReason;
        this.identifier = identifier;
        this.identifierType = identifierType;
        this.dueDate = dueDate;
        this.payableItem = payableItem;
        this.payorId = payorId;
        this.payorName = payorName;
        this.additionalId = additionalId;
        this.policySymbol = policySymbol;
        this.policyNumber = policyNumber;
        this.accountId = accountId;
        this.accountType = accountType;
        this.type = type;
        this.isUnidentified = isUnidentified;
        this.transactionId = transactionId;
        this.authorizationResponse = authorizationResponse;
        this.authorizationResponseType = authorizationResponseType;
        this.remitterId = remitterId;
        this.remitterName = remitterName;
        this.reviewCode = reviewCode;
        this.currentDue = currentDue;
        this.isRecurring = isRecurring;
        this.collectionType = collectionType;
        this.collectionMethod = collectionMethod;
        this.agencyNumber = agencyNumber;
        this.agencyClientId = agencyClientId;
        this.agencyName = agencyName;
        this.paymentProfileId = paymentProfileId;
        this.bankHolderName = bankHolderName;
        this.payorAddress = payorAddress;
        this.currency = currency;
        this.receivedCurrency = receivedCurrency;
        this.receivedAmount = receivedAmount;
        this.rate = rate;
        this.businessGroup = businessGroup;
        this.walletType = walletType;
        this.walletNumber = walletNumber;
        this.walletIdentifier = walletIdentifier;
    }

    public Payment() {
    }

    public Payment(Payment payment) {
        this(payment.getPaymentSequenceNumber(), payment.getPaymentAmount(), payment.getPaymentType(),
                payment.getPaymentMethod(), payment.getPaymentDate(), payment.getPostedDate(),
                payment.getPostmarkDate(), payment.getEntryDateTime(), payment.getBatchId(), payment.getDepositBank(),
                payment.getControlBank(), payment.getUserId(), payment.getUserName(), payment.getPaymentComments(),
                payment.getPaymentId(), payment.getCreditCardType(), payment.getCreditCardNumber(),
                payment.getCreditCardExpiryDate(), payment.getCreditCardHolderName(), payment.getPostalCode(),
                payment.getAuthorization(), payment.getRoutingNumber(), payment.getBankAccountNumber(),
                payment.getSuspenseAmount(), payment.getAppliedAmount(), payment.getDisbursedAmount(),
                payment.getWriteOffAmount(), payment.getStatus(), payment.getPaymentOnHold(),
                payment.getSuspenseReason(), payment.getIdentifier(), payment.getIdentifierType(), payment.getDueDate(),
                payment.getPayableItem(), payment.getPayorId(), payment.getPayorName(), payment.getAdditionalId(),
                payment.getPolicySymbol(), payment.getPolicyNumber(), payment.getAccountId(), payment.getAccountType(),
                payment.getType(), payment.getIsUnidentified(), payment.getTransactionId(),
                payment.getAuthorizationResponse(), payment.getAuthorizationResponseType(), payment.getRemitterId(),
                payment.getRemitterName(), payment.getReviewCode(), payment.getCurrentDue(), payment.getIsRecurring(),
                payment.getCollectionType(), payment.getCollectionMethod(), payment.getAgencyNumber(),
                payment.getAgencyClientId(), payment.getAgencyName(), payment.getPaymentProfileId(),
                payment.getBankHolderName(), payment.getPayorAddress(), payment.getCurrency(),
                payment.getReceivedCurrency(), payment.getReceivedAmount(), payment.getRate(),
                payment.getBusinessGroup(), payment.getWalletType(), payment.getWalletNumber(),
                payment.getWalletIdentifier());
    }

    public short getPaymentSequenceNumber() {
        return paymentSequenceNumber;
    }

    public void setPaymentSequenceNumber(short paymentSequenceNumber) {
        this.paymentSequenceNumber = paymentSequenceNumber;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public ZonedDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(ZonedDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public ZonedDateTime getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(ZonedDateTime postedDate) {
        this.postedDate = postedDate;
    }

    public ZonedDateTime getPostmarkDate() {
        return postmarkDate;
    }

    public void setPostmarkDate(ZonedDateTime postmarkDate) {
        this.postmarkDate = postmarkDate;
    }

    public ZonedDateTime getEntryDateTime() {
        return entryDateTime;
    }

    public void setEntryDateTime(ZonedDateTime entryDateTime) {
        this.entryDateTime = entryDateTime;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getDepositBank() {
        return depositBank;
    }

    public void setDepositBank(String depositBank) {
        this.depositBank = depositBank;
    }

    public String getControlBank() {
        return controlBank;
    }

    public void setControlBank(String controlBank) {
        this.controlBank = controlBank;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPaymentComments() {
        return paymentComments;
    }

    public void setPaymentComments(String paymentComments) {
        this.paymentComments = paymentComments;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getCreditCardType() {
        return creditCardType;
    }

    public void setCreditCardType(String creditCardType) {
        this.creditCardType = creditCardType;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getCreditCardExpiryDate() {
        return creditCardExpiryDate;
    }

    public void setCreditCardExpiryDate(String creditCardExpiryDate) {
        this.creditCardExpiryDate = creditCardExpiryDate;
    }

    public String getCreditCardHolderName() {
        return creditCardHolderName;
    }

    public void setCreditCardHolderName(String creditCardHolderName) {
        this.creditCardHolderName = creditCardHolderName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public Double getSuspenseAmount() {
        return suspenseAmount;
    }

    public void setSuspenseAmount(Double suspenseAmount) {
        this.suspenseAmount = suspenseAmount;
    }

    public Double getAppliedAmount() {
        return appliedAmount;
    }

    public void setAppliedAmount(Double appliedAmount) {
        this.appliedAmount = appliedAmount;
    }

    public Double getDisbursedAmount() {
        return disbursedAmount;
    }

    public void setDisbursedAmount(Double disbursedAmount) {
        this.disbursedAmount = disbursedAmount;
    }

    public Double getWriteOffAmount() {
        return writeOffAmount;
    }

    public void setWriteOffAmount(Double writeOffAmount) {
        this.writeOffAmount = writeOffAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSuspenseReason() {
        return suspenseReason;
    }

    public void setSuspenseReason(String suspenseReason) {
        this.suspenseReason = suspenseReason;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifierType() {
        return identifierType;
    }

    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    public ZonedDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(ZonedDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public String getPayableItem() {
        return payableItem;
    }

    public void setPayableItem(String payableItem) {
        this.payableItem = payableItem;
    }

    public String getPayorId() {
        return payorId;
    }

    public void setPayorId(String payorId) {
        this.payorId = payorId;
    }

    public String getPayorName() {
        return payorName;
    }

    public void setPayorName(String payorName) {
        this.payorName = payorName;
    }

    public String getAdditionalId() {
        return additionalId;
    }

    public void setAdditionalId(String additionalId) {
        this.additionalId = additionalId;
    }

    public String getPolicySymbol() {
        return policySymbol;
    }

    public void setPolicySymbol(String policySymbol) {
        this.policySymbol = policySymbol;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getIsUnidentified() {
        return isUnidentified;
    }

    public void setIsUnidentified(Boolean isUnidentified) {
        this.isUnidentified = isUnidentified;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getAuthorizationResponse() {
        return authorizationResponse;
    }

    public void setAuthorizationResponse(String authorizationResponse) {
        this.authorizationResponse = authorizationResponse;
    }

    public String getAuthorizationResponseType() {
        return authorizationResponseType;
    }

    public void setAuthorizationResponseType(String authorizationResponseType) {
        this.authorizationResponseType = authorizationResponseType;
    }

    public String getRemitterId() {
        return remitterId;
    }

    public void setRemitterId(String remitterId) {
        this.remitterId = remitterId;
    }

    public String getRemitterName() {
        return remitterName;
    }

    public void setRemitterName(String remitterName) {
        this.remitterName = remitterName;
    }

    public String getReviewCode() {
        return reviewCode;
    }

    public void setReviewCode(String reviewCode) {
        this.reviewCode = reviewCode;
    }

    public Double getCurrentDue() {
        return currentDue;
    }

    public void setCurrentDue(Double currentDue) {
        this.currentDue = currentDue;
    }

    public Double getTotalDue() {
        return totalDue;
    }

    public void setTotalDue(Double totalDue) {
        this.totalDue = totalDue;
    }

    public Boolean getIsRecurring() {
        return isRecurring;
    }

    public void setIsRecurring(Boolean isRecurring) {
        this.isRecurring = isRecurring;
    }

    public String getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(String collectionType) {
        this.collectionType = collectionType;
    }

    public String getCollectionMethod() {
        return collectionMethod;
    }

    public void setCollectionMethod(String collectionMethod) {
        this.collectionMethod = collectionMethod;
    }

    public Boolean getPaymentOnHold() {
        return paymentOnHold;
    }

    public void setPaymentOnHold(Boolean paymentOnHold) {
        this.paymentOnHold = paymentOnHold;
    }

    public String getAgencyNumber() {
        return agencyNumber;
    }

    public void setAgencyNumber(String agencyNumber) {
        this.agencyNumber = agencyNumber;
    }

    public String getAgencyClientId() {
        return agencyClientId;
    }

    public void setAgencyClientId(String agencyClientId) {
        this.agencyClientId = agencyClientId;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getPaymentProfileId() {
        return paymentProfileId;
    }

    public void setPaymentProfileId(String paymentProfileId) {
        this.paymentProfileId = paymentProfileId;
    }

    public String getBankHolderName() {
        return bankHolderName;
    }

    public void setBankHolderName(String bankHolderName) {
        this.bankHolderName = bankHolderName;
    }

    public String getPayorAddress() {
        return payorAddress;
    }

    public void setPayorAddress(String payorAddress) {
        this.payorAddress = payorAddress;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBusinessGroup() {
        return businessGroup;
    }

    public void setBusinessGroup(String businessGroup) {
        this.businessGroup = businessGroup;
    }

    public String getReceivedCurrency() {
        return receivedCurrency;
    }

    public void setReceivedCurrency(String receivedCurrency) {
        this.receivedCurrency = receivedCurrency;
    }

    public Double getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(Double receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Boolean getIsEdit() {
        return isEdit;
    }

    public void setIsEdit(Boolean isEdit) {
        this.isEdit = isEdit;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public String getCustomerProfileId() {
        return customerProfileId;
    }

    public void setCustomerProfileId(String customerProfileId) {
        this.customerProfileId = customerProfileId;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getWalletNumber() {
        return walletNumber;
    }

    public void setWalletNumber(String walletNumber) {
        this.walletNumber = walletNumber;
    }

    public String getWalletIdentifier() {
        return walletIdentifier;
    }

    public void setWalletIdentifier(String walletIdentifier) {
        this.walletIdentifier = walletIdentifier;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountId, accountType, additionalId, agencyClientId, agencyName,
                agencyNumber, appliedAmount, authorization, authorizationResponse, authorizationResponseType,
                bankAccountNumber, bankHolderName, batchId, businessGroup, collectionMethod, collectionType,
                controlBank, creditCardExpiryDate, creditCardHolderName, creditCardNumber, creditCardType, currency,
                currentDue, customerProfileId, depositBank, disbursedAmount, dueDate, entryDateTime, identifier,
                identifierType, isDelete, isEdit, isRecurring, isUnidentified, payableItem, paymentAmount,
                paymentComments, paymentDate, paymentId, paymentMethod, paymentOnHold, paymentProfileId,
                paymentSequenceNumber, paymentType, payorAddress, payorId, payorName, policyNumber, policySymbol,
                postalCode, postedDate, postmarkDate, rate, receivedAmount, receivedCurrency, remitterId, remitterName,
                reviewCode, routingNumber, status, suspenseAmount, suspenseReason, totalDue, transactionId, type,
                userId, userName, walletIdentifier, walletNumber, walletType, writeOffAmount);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Payment other = (Payment) obj;
        return Objects.equals(accountId, other.accountId) && Objects.equals(accountType, other.accountType)
                && Objects.equals(additionalId, other.additionalId)
                && Objects.equals(agencyClientId, other.agencyClientId) && Objects.equals(agencyName, other.agencyName)
                && Objects.equals(agencyNumber, other.agencyNumber)
                && Objects.equals(appliedAmount, other.appliedAmount)
                && Objects.equals(authorization, other.authorization)
                && Objects.equals(authorizationResponse, other.authorizationResponse)
                && Objects.equals(authorizationResponseType, other.authorizationResponseType)
                && Objects.equals(bankAccountNumber, other.bankAccountNumber)
                && Objects.equals(bankHolderName, other.bankHolderName) && Objects.equals(batchId, other.batchId)
                && Objects.equals(businessGroup, other.businessGroup)
                && Objects.equals(collectionMethod, other.collectionMethod)
                && Objects.equals(collectionType, other.collectionType)
                && Objects.equals(controlBank, other.controlBank)
                && Objects.equals(creditCardExpiryDate, other.creditCardExpiryDate)
                && Objects.equals(creditCardHolderName, other.creditCardHolderName)
                && Objects.equals(creditCardNumber, other.creditCardNumber)
                && Objects.equals(creditCardType, other.creditCardType) && Objects.equals(currency, other.currency)
                && Objects.equals(currentDue, other.currentDue)
                && Objects.equals(customerProfileId, other.customerProfileId)
                && Objects.equals(depositBank, other.depositBank)
                && Objects.equals(disbursedAmount, other.disbursedAmount) && Objects.equals(dueDate, other.dueDate)
                && Objects.equals(entryDateTime, other.entryDateTime) && Objects.equals(identifier, other.identifier)
                && Objects.equals(identifierType, other.identifierType) && Objects.equals(isDelete, other.isDelete)
                && Objects.equals(isEdit, other.isEdit) && Objects.equals(isRecurring, other.isRecurring)
                && Objects.equals(isUnidentified, other.isUnidentified)
                && Objects.equals(payableItem, other.payableItem) && Objects.equals(paymentAmount, other.paymentAmount)
                && Objects.equals(paymentComments, other.paymentComments)
                && Objects.equals(paymentDate, other.paymentDate) && Objects.equals(paymentId, other.paymentId)
                && Objects.equals(paymentMethod, other.paymentMethod)
                && Objects.equals(paymentOnHold, other.paymentOnHold)
                && Objects.equals(paymentProfileId, other.paymentProfileId)
                && paymentSequenceNumber == other.paymentSequenceNumber
                && Objects.equals(paymentType, other.paymentType) && Objects.equals(payorAddress, other.payorAddress)
                && Objects.equals(payorId, other.payorId) && Objects.equals(payorName, other.payorName)
                && Objects.equals(policyNumber, other.policyNumber) && Objects.equals(policySymbol, other.policySymbol)
                && Objects.equals(postalCode, other.postalCode) && Objects.equals(postedDate, other.postedDate)
                && Objects.equals(postmarkDate, other.postmarkDate) && Objects.equals(rate, other.rate)
                && Objects.equals(receivedAmount, other.receivedAmount)
                && Objects.equals(receivedCurrency, other.receivedCurrency)
                && Objects.equals(remitterId, other.remitterId) && Objects.equals(remitterName, other.remitterName)
                && Objects.equals(reviewCode, other.reviewCode) && Objects.equals(routingNumber, other.routingNumber)
                && Objects.equals(status, other.status) && Objects.equals(suspenseAmount, other.suspenseAmount)
                && Objects.equals(suspenseReason, other.suspenseReason) && Objects.equals(totalDue, other.totalDue)
                && Objects.equals(transactionId, other.transactionId) && Objects.equals(type, other.type)
                && Objects.equals(userId, other.userId) && Objects.equals(userName, other.userName)
                && Objects.equals(walletIdentifier, other.walletIdentifier)
                && Objects.equals(walletNumber, other.walletNumber) && Objects.equals(walletType, other.walletType)
                && Objects.equals(writeOffAmount, other.writeOffAmount);
    }

}