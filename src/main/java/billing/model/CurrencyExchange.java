package billing.model;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CurrencyExchange extends ResourceSupport {

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime exchangeDate;
    private Double fromAmount;
    private Double toAmount;
    private String fromCurrency;
    private String toCurrency;
    private Double rate;

    public CurrencyExchange(@JsonProperty("exchangeDate") ZonedDateTime exchangeDate,
            @JsonProperty("fromAmount") Double fromAmount, @JsonProperty("toAmount") Double toAmount,
            @JsonProperty("fromCurrency") String fromCurrency, @JsonProperty("toCurrency") String toCurrency,
            @JsonProperty("rate") Double rate) {
        super();
        this.exchangeDate = exchangeDate;
        this.fromAmount = fromAmount;
        this.toAmount = toAmount;
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
        this.rate = rate;
    }

    public CurrencyExchange() {

    }

    public ZonedDateTime getExchangeDate() {
        return exchangeDate;
    }

    public void setExchangeDate(ZonedDateTime exchangeDate) {
        this.exchangeDate = exchangeDate;
    }

    public Double getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(Double fromAmount) {
        this.fromAmount = fromAmount;
    }

    public Double getToAmount() {
        return toAmount;
    }

    public void setToAmount(Double toAmount) {
        this.toAmount = toAmount;
    }

    public String getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(String fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public String getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(String toCurrency) {
        this.toCurrency = toCurrency;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(exchangeDate, fromAmount, fromCurrency, rate, toAmount, toCurrency);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        CurrencyExchange other = (CurrencyExchange) obj;
        return Objects.equals(exchangeDate, other.exchangeDate) && Objects.equals(fromAmount, other.fromAmount)
                && Objects.equals(fromCurrency, other.fromCurrency) && Objects.equals(rate, other.rate)
                && Objects.equals(toAmount, other.toAmount) && Objects.equals(toCurrency, other.toCurrency);
    }

}
