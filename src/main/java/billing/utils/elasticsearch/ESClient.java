package billing.utils.elasticsearch;

import javax.annotation.PostConstruct;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import core.exception.application.ApplicationException;

@Service
public class ESClient {

    private RestHighLevelClient client;
    public static final Logger logger = LogManager.getLogger(ESClient.class);

    @Value("${elastic.host}")
    private String esHost;

    @Value("${elastic.port}")
    private String esPort;

    @Value("${elastic.protocol}")
    private String esProtocol;

    @Value("${elastic.username}")
    private String esUserName;

    @Value("${elastic.password}")
    private String esPassword;

    @PostConstruct
    public RestHighLevelClient getClient() {
        if (client == null) {
            client = createClient();
        }

        return client;
    }

    private RestHighLevelClient createClient() {
        if (client == null) {
            try {

                if (esHost != null && !esHost.isEmpty()) {
                    HttpHost httpHost = null;
                    if (esProtocol != null && !esProtocol.isEmpty()) {
                        httpHost = new HttpHost(esHost, Integer.parseInt(esPort), esProtocol);
                    } else {
                        httpHost = new HttpHost(esHost, Integer.parseInt(esPort));
                    }
                    RestClientBuilder builder = null;
                    if (esUserName != null && !esUserName.isEmpty()) {

                        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
                        credentialsProvider.setCredentials(AuthScope.ANY,
                                new UsernamePasswordCredentials(esUserName, esPassword));

                        builder = RestClient.builder(httpHost)
                                .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
                                    @Override
                                    public HttpAsyncClientBuilder customizeHttpClient(
                                            HttpAsyncClientBuilder httpClientBuilder) {
                                        return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                                    }
                                });
                    } else {
                        builder = RestClient.builder(httpHost);
                    }

                    RestHighLevelClient client = new RestHighLevelClient(builder);
                    return client;
                }
            } catch (Exception ex) {
                logger.error("Error in connecting to Elastic Search Cluster");
                throw new ApplicationException("search.connect", ex);
            }
        }
        return client;
    }

}
