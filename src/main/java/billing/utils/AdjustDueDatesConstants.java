package billing.utils;

import static core.utils.CommonConstants.CHAR_G;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_U;
import static core.utils.CommonConstants.CHAR_W;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.ChargeType;
import billing.utils.BillingConstants.InvoiceTypeCode;
import core.utils.DateRoutine;

public class AdjustDueDatesConstants {

    public static final String RULE_DPIF = "DPIF";
    public static final String RULE_WSCE = "WSCE";
    public static final String RULE_FFOT = "FFOT";
    public static final String RULE_SVC = "SVC";
    public static final BigDecimal MIN_AMOUNT = new BigDecimal("0.00");
    public static final ZonedDateTime DATE_LOW = DateRoutine.dateTimeAsYYYYMMDD("1900-01-01");
    public static final String CHARGES_INTERFACE = "BCMOCRG1";
    public static final String SVCO_RULE_PREFIX = "SV";

    private static final List<Character> CHARGE_TYPES = Arrays.asList(ChargeType.LATECHARGES.getChargeType(),
            ChargeType.PENALTYCHARGES.getChargeType());

    private static final List<Character> POLICY_STATUS_CODES_CANCELLATION = Arrays
            .asList(BilPolicyStatusCode.CANCELLED.getValue(), BilPolicyStatusCode.FLAT_CANCELLATION.getValue());

    private static final List<Character> FUTURE_INVOICE_CODES = Arrays.asList(InvoiceTypeCode.EMPTY_CHAR.getValue(),
            InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue());

    private static final List<Character> DATETYPE_CODES = Arrays.asList(DateTypeCode.FIRST_POSSIBLEDUE_LATE.getValue(),
            DateTypeCode.FIRSTPOSSIBLEDUEDATE_SINGLEPOLICYCANCELLATIONINVOICE.getValue());

    private static final List<Character> INVOICE_CODES = Arrays.asList(
            InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
            InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());

    private static final List<Character> RENEWAL_AMT_EFFECTIVE_INDICATOR = Arrays.asList(CHAR_G, CHAR_R, CHAR_U,
            CHAR_W);

    private static final List<Character> RENEWAL_AMT_PROCESS_INDICATOR = Arrays.asList(
            AmountProcessIndicator.INTERMEDIATE_PROCESSING.getValue(), AmountProcessIndicator.VOID_TERM.getValue());

    private static final List<Character> AMOUNT_EFFECTIVE_INDICATOR = Arrays.asList(
            AmountEffectiveIndicator.EPICS_NSF_AMOUNT.getValue(),
            AmountEffectiveIndicator.NONPAYCANCELLATION_OR_NSTALLMENTFEE.getValue(),
            AmountEffectiveIndicator.AUTO_DISBURSE_REBILL.getValue());

    private static final List<String> DESCRIPTION_REASONS = Arrays
            .asList(BusCodeTranslationType.TRANSACTION_TYPE.getValue(), BusCodeTranslationType.BILLING_ITEM.getValue());

    public static final String RENEWAL_HALF_DOWNPAY = "RDP";

    private static final List<Character> POLICY_STATUS_CODES_SVCRULE = Arrays
            .asList(BilPolicyStatusCode.TRANSFERRED.getValue());

    private static final List<Character> POLICY_STATUS_CODES = Arrays.asList(
            BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue(), BilPolicyStatusCode.OPEN.getValue(),
            BilPolicyStatusCode.PENDING_CANCELLATION.getValue());

    private static final List<Character> POLICY_STATUS_CODES_DPIFRULE = Arrays.asList(
            BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(), BilPolicyStatusCode.TRANSFERRED.getValue(),
            BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
            BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue());

    private static final List<Character> POLICY_STATUS_CODES_REDISTRIBUTE = Arrays.asList(
            BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(), BilPolicyStatusCode.TRANSFERRED.getValue(),
            BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
            BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(),
            BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue());

    private static final List<Character> POLICY_ISSUE_INDICATOR = Arrays.asList(
            PolicyIssueIndicator.CANCELLED_FOR_REWRITE.getValue(),
            PolicyIssueIndicator.CANCELLED_FOR_REISSUE.getValue());

    public enum BillingExclusionIndicator {
        DEDUCTIONDATE_EXCLUDED('X'), DEDUCTIONDATE_NOTEXCLUDED(' ');

        private final Character indicator;

        private BillingExclusionIndicator(Character indicator) {
            this.indicator = indicator;
        }

        public Character getValue() {
            return indicator;
        }
    }

    public enum DateTypeCode {

        SCHEDULED('S'),
        OUTOFCYCLE_BILLING('O'),
        GROUPACCOUNTINVOICE_DUEDATECHANGE('T'),
        FIRST_POSSIBLEDUE_LATE('I'),
        FIRSTPOSSIBLEDUEDATE_SINGLEPOLICYCANCELLATIONINVOICE('F');

        private final Character code;

        private DateTypeCode(Character code) {
            this.code = code;
        }

        public Character getValue() {
            return code;
        }

    }

    public enum DateIndicator {

        DATENOTOVERRIDEN_BYUSER('N'), DATEOVERRIDEN_BYUSER('Y'), LOOKAHEAD_DUEDATE('L');

        private final Character indicator;

        private DateIndicator(Character indicator) {
            this.indicator = indicator;
        }

        public Character getValue() {
            return indicator;
        }

    }

    public enum SuspendedFullReasonCode {
        SUSPEND_BILLING_FOLLOWUPPRECOLLECTION("AP"), SUSPEND_BILLING_FOLLOWUPCOLLECTION("AC");

        private final String code;

        private SuspendedFullReasonCode(String code) {
            this.code = code;
        }

        public String getValue() {
            return code;
        }
    }

    public enum OutOfCycleDaysIndicator {
        PROCESS_OUTOFCYCLEBILL('N'), DONOTPROCESS_OUTOFCYCLEBILL('Y');

        private final Character indicator;

        private OutOfCycleDaysIndicator(Character indicator) {
            this.indicator = indicator;
        }

        public Character getValue() {
            return indicator;
        }
    }

    public enum WaiveServiceChargeCode {
        EXCLUDESERVICECHARGE('1'),
        INCLUDESERVICECHARGE('2'),
        OFF('N'),
        EXCLUDESERVICECHARGE_NBSFIRSTINSTALLMENT('3'),
        EMPTY_CHAR(' ');

        private final Character code;

        private WaiveServiceChargeCode(Character code) {
            this.code = code;
        }

        public Character getValue() {
            return code;
        }

        public boolean hasValue(Character value) {
            return value != null && code.equals(value);
        }

        public static WaiveServiceChargeCode getEnumValue(Character waiveCd) {
            for (WaiveServiceChargeCode waiveCode : values()) {
                if (waiveCode.code.equals(waiveCd)) {
                    return waiveCode;
                }

            }
            return EMPTY_CHAR;
        }
    }

    public enum BillingTypeCode {
        SINGLEPOLICY("SP"), ACCOUNTBILL("AB");

        private final String code;

        private BillingTypeCode(String code) {
            this.code = code;
        }

        public String getValue() {
            return code;
        }
    }

    public enum ServiceChargeActionCode {
        DONOTASSESS('N'),
        ASSES_ALLSCHEDULEDINVOICES('A'),
        SUPPRESS_RENEWALDOWNPAYINVOICE('R'),
        SUPPRESS_RENEWAL_PRERENEWAL_DOWNPAYINVOICFE('S'),
        SUPPRESS_PRERENEWAL_DOWNPAYINVOICE('P'),
        EMPTY_CHAR(' ');

        private final Character code;

        private ServiceChargeActionCode(Character code) {
            this.code = code;
        }

        public Character getValue() {
            return code;
        }

        public boolean hasValue(Character value) {
            return value != null && code.equals(value);
        }

        public static ServiceChargeActionCode getEnumValue(Character actionCd) {
            for (ServiceChargeActionCode actionCode : values()) {
                if (actionCode.code.equals(actionCd)) {
                    return actionCode;
                }

            }
            return EMPTY_CHAR;
        }

    }

    public enum AmountProcessIndicator {
        ROW_PROCESSED('Y'),
        ROW_NOT_PROCESSED('N'),
        ROWPROCESSED_NOTSCHEDULED('P'),
        VOID_TERM('V'),
        INTERMEDIATE_PROCESSING('H');

        private final Character indicator;

        private AmountProcessIndicator(Character indicator) {
            this.indicator = indicator;
        }

        public Character getValue() {
            return indicator;
        }
    }

    public enum AmountEffectiveIndicator {
        EPICS_NSF_AMOUNT('A'), NONPAYCANCELLATION_OR_NSTALLMENTFEE('E'), AUTO_DISBURSE_REBILL('X');

        private final Character indicator;

        private AmountEffectiveIndicator(Character indicator) {
            this.indicator = indicator;
        }

        public Character getValue() {
            return indicator;
        }

    }

    public enum ObjectTypeCode {

        BATCH('B'), BILLING_DRIVER('D'), PROCESS('P'), USEREXIT('U');

        private final Character type;

        private ObjectTypeCode(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum ObjectProcesingCode {

        INITIATE_PROCESSING('Y'), DONOT_INITIATEPROCESSING('N');

        private final Character code;

        private ObjectProcesingCode(Character code) {
            this.code = code;
        }

        public Character getValue() {
            return code;
        }
    }

    public enum PolicyIssueIndicator {

        CANCELLED_FOR_REWRITE('W'), CANCELLED_FOR_REISSUE('K');

        private final Character indicator;

        private PolicyIssueIndicator(Character indicator) {
            this.indicator = indicator;
        }

        public Character getValue() {
            return indicator;
        }
    }

    public enum PolicyRedistributeCode {

        REDISTRIBUTE_PROCESSED('Y'), REDISTRIBUTE_NOTPROCESSED(' ');

        private final Character code;

        private PolicyRedistributeCode(Character code) {
            this.code = code;
        }

        public Character getValue() {
            return code;
        }
    }

    public enum DateSetIndicator {

        RESUME_RESET('S'), RESCIND_RESET('B'), ONE_RESET_2('T'), ONE_RESET_1('O'), ALL_RESET('A'), EMPTY_CHAR(' ');

        private final Character code;

        private DateSetIndicator(Character code) {
            this.code = code;
        }

        public Character getValue() {
            return code;
        }

        public boolean hasValue(Character value) {
            return value != null && code.equals(value);
        }

        public static DateSetIndicator getEnumValue(Character dateSetCd) {
            for (DateSetIndicator datesetCode : values()) {
                if (datesetCode.code.equals(dateSetCd)) {
                    return datesetCode;
                }

            }
            return EMPTY_CHAR;
        }

    }

    public enum ScheduleCode {

        NO_SCHEDULE('N'), CURRENT_SCHEDULE('Y'), FUTURE_SCHEDULE('F');

        private final Character code;

        private ScheduleCode(Character code) {
            this.code = code;
        }

        public Character getValue() {
            return code;
        }
    }

    public static List<Character> getChargeTypes() {
        return CHARGE_TYPES;
    }

    public static List<Character> getPolicyStatusCodesCancellation() {
        return POLICY_STATUS_CODES_CANCELLATION;
    }

    public static List<Character> getFutureInvoiceCodes() {
        return FUTURE_INVOICE_CODES;
    }

    public static List<Character> getDatetypeCodes() {
        return DATETYPE_CODES;
    }

    public static List<Character> getInvoiceCodes() {
        return INVOICE_CODES;
    }

    public static List<Character> getRenewalAmtEffectiveIndicator() {
        return RENEWAL_AMT_EFFECTIVE_INDICATOR;
    }

    public static List<Character> getRenewalAmtProcessIndicator() {
        return RENEWAL_AMT_PROCESS_INDICATOR;
    }

    public static List<Character> getAmountEffectiveIndicator() {
        return AMOUNT_EFFECTIVE_INDICATOR;
    }

    public static List<String> getDescriptionReasons() {
        return DESCRIPTION_REASONS;
    }

    public static List<Character> getPolicyStatusCodesSvcrule() {
        return POLICY_STATUS_CODES_SVCRULE;
    }

    public static List<Character> getPolicyStatusCodes() {
        return POLICY_STATUS_CODES;
    }

    public static List<Character> getPolicyStatusCodesDpifrule() {
        return POLICY_STATUS_CODES_DPIFRULE;
    }

    public static List<Character> getPolicyStatusCodesRedistribute() {
        return POLICY_STATUS_CODES_REDISTRIBUTE;
    }

    public static List<Character> getPolicyIssueIndicator() {
        return POLICY_ISSUE_INDICATOR;
    }

}
