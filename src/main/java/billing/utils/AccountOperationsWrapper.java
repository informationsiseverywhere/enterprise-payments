package billing.utils;

import java.time.ZonedDateTime;

import billing.model.AccountHistory;

public class AccountOperationsWrapper {

    private AccountOperationsWrapper() {
        super();
    }

    public static AccountHistory createActivity(String activityDesCd, String activityDesType, String policyNumber,
            String policySymbolCd, ZonedDateTime activityDesc1Dt, ZonedDateTime activityDesc2Dt, double amount,
            String additionalDataTxt, String userId) {
        AccountHistory accountHistory = new AccountHistory();
        accountHistory.setPolicyNumber(policyNumber);
        accountHistory.setPolicySymbolCd(policySymbolCd);
        accountHistory.setActivityCd(activityDesCd);
        accountHistory.setActivityType(activityDesType);
        accountHistory.setActivityDesc1Dt(activityDesc1Dt);
        accountHistory.setActivityDesc2Dt(activityDesc2Dt);
        accountHistory.setAmount(amount);
        accountHistory.setAdditionalDataTxt(additionalDataTxt);
        accountHistory.setUserName(userId);
        return accountHistory;
    }

}
