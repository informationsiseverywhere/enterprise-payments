package billing.utils;

import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Currency;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import billing.mongo.data.model.ReportHeader;
import billing.mongo.data.model.ReportMetadata;
import billing.mongo.data.repository.ReportMetadataRepository;

@Component
public class XLSReportRenderer {

    private static final Logger logger = LoggerFactory.getLogger(XLSReportRenderer.class);

    private ReportMetadata reportMetadata;

    @Autowired
    private ReportMetadataRepository reportMetadataRepository;

    private static final String DECIMAL_FORMAT = "0.00";
    private static final String NUMERIC_FORMAT = "0";

    public XSSFSheet setPrintProperties(XSSFSheet sheet, boolean isLandscapeOrientation, boolean printGridLines) {

        sheet.setPrintGridlines(printGridLines);
        sheet.getPrintSetup().setLandscape(isLandscapeOrientation);
        sheet.getPrintSetup().setPaperSize(PrintSetup.A4_PAPERSIZE);
        sheet.getPrintSetup().setFitWidth((short) 1);
        return sheet;
    }

    public XSSFRow createNumericCell(XSSFRow excelRow, int index, XSSFCellStyle style, double data, String currencyCode,
            XSSFDataFormat format) {
        String currencyFormat = getCurrencyFormatByCode(currencyCode);
        Cell cell = excelRow.createCell(index, CellType.NUMERIC);
        style.setDataFormat(format.getFormat(currencyFormat));
        cell.setCellValue(data);
        cell.setCellStyle(style);
        return excelRow;
    }

    public XSSFRow createStringCell(XSSFRow excelRow, int index, XSSFCellStyle style, String data) {
        Cell cell = excelRow.createCell(index, CellType.STRING);
        cell.setCellValue(data);
        cell.setCellStyle(style);
        return excelRow;
    }

    public XSSFRow createStringCell(XSSFRow excelRow, int index, String data) {
        Cell cell = excelRow.createCell(index, CellType.STRING);
        cell.setCellValue(data);
        return excelRow;
    }

    public XSSFRow createBooleanCell(XSSFRow excelRow, int index, XSSFCellStyle style, boolean data) {
        Cell cell = excelRow.createCell(index, CellType.BOOLEAN);
        cell.setCellValue(data);
        cell.setCellStyle(style);
        return excelRow;
    }

    public XSSFRow createFormulaCell(XSSFRow excelRow, int index, XSSFCellStyle style, String data) {
        Cell cell = excelRow.createCell(index, CellType.FORMULA);
        cell.setCellValue(data);
        cell.setCellStyle(style);
        return excelRow;
    }

    public void formatMergedRegion(CellRangeAddress cellRangeAddress, XSSFSheet sheet, BorderStyle borderStyle) {
        RegionUtil.setBorderTop(borderStyle, cellRangeAddress, sheet);
        RegionUtil.setBorderBottom(borderStyle, cellRangeAddress, sheet);
        RegionUtil.setBorderLeft(borderStyle, cellRangeAddress, sheet);
        RegionUtil.setBorderRight(borderStyle, cellRangeAddress, sheet);
    }

    public Font createFont(XSSFWorkbook workbook, int fontheight, boolean isBold, String fontName, boolean isItalic,
            String underlineType, int colorCode) {
        Font font = workbook.createFont();
        font.setFontName(fontName);
        font.setFontHeightInPoints((short) fontheight);
        font.setColor((short) colorCode);
        font.setItalic(isItalic);
        font.setBold(isBold);
        font.setUnderline(Byte.valueOf(underlineType));
        return font;
    }

    public XSSFCellStyle createHeading1Style(XSSFWorkbook workbook) {
        Font headingFont = createFont(workbook, reportMetadata.getHeading1Metadata().getFontSize(),
                reportMetadata.getHeading1Metadata().isBold(), reportMetadata.getHeading1Metadata().getFont(),
                reportMetadata.getHeading1Metadata().isItalic(),
                reportMetadata.getHeading1Metadata().getUnderLineType(),
                reportMetadata.getHeading1Metadata().getFontColor());
        XSSFCellStyle style = setStylingAttributes(workbook, FillPatternType.FINE_DOTS, HorizontalAlignment.CENTER,
                VerticalAlignment.CENTER, BorderStyle.THIN, reportMetadata.getHeading1Metadata().getFillColor(),
                reportMetadata.getHeading1Metadata().isWrapText());
        style.setFont(headingFont);
        return style;
    }

    public Font createFont(XSSFWorkbook workbook, int fontHeight, boolean isBold, String fontName, boolean isItalic,
            String underlineType) {
        Font font = workbook.createFont();
        font.setBold(isBold);
        font.setFontHeightInPoints((short) fontHeight);
        font.setItalic(isItalic);
        font.setFontName(fontName);
        font.setUnderline(Byte.valueOf(underlineType));
        return font;
    }

    public XSSFCellStyle createTitleStyle(XSSFWorkbook workbook) {
        Font titleFont = createFont(workbook, reportMetadata.getTitleMetadata().getFontSize(),
                reportMetadata.getTitleMetadata().isBold(), reportMetadata.getTitleMetadata().getFont(),
                reportMetadata.getTitleMetadata().isItalic(), reportMetadata.getTitleMetadata().getUnderLineType(),
                reportMetadata.getTitleMetadata().getFontColor());
        XSSFCellStyle style = setStylingAttributes(workbook, FillPatternType.SOLID_FOREGROUND,
                HorizontalAlignment.CENTER, VerticalAlignment.CENTER, BorderStyle.THIN,
                reportMetadata.getTitleMetadata().getFillColor(), reportMetadata.getTitleMetadata().isWrapText());
        style.setFont(titleFont);
        return style;
    }

    public XSSFCellStyle createDataStyle(XSSFWorkbook workbook) {
        Font dataFont = createFont(workbook, reportMetadata.getDataMetadata().getFontSize(),
                reportMetadata.getDataMetadata().isBold(), reportMetadata.getDataMetadata().getFont(),
                reportMetadata.getDataMetadata().isItalic(), reportMetadata.getDataMetadata().getUnderLineType(),
                reportMetadata.getDataMetadata().getFontColor());
        XSSFCellStyle style = setStylingAttributes(workbook, FillPatternType.SOLID_FOREGROUND,
                HorizontalAlignment.CENTER, VerticalAlignment.CENTER, BorderStyle.HAIR,
                reportMetadata.getDataMetadata().getFillColor(), reportMetadata.getDataMetadata().isWrapText());
        style.setFont(dataFont);
        return style;
    }

    public XSSFCellStyle createReportPropertiesStyle(XSSFWorkbook workbook) {
        Font tablePropertyFont = createFont(workbook, reportMetadata.getPropertyMetadata().getFontSize(),
                reportMetadata.getPropertyMetadata().isBold(), reportMetadata.getPropertyMetadata().getFont(),
                reportMetadata.getPropertyMetadata().isItalic(),
                reportMetadata.getPropertyMetadata().getUnderLineType(),
                reportMetadata.getPropertyMetadata().getFontColor());
        XSSFCellStyle style = setStylingAttributes(workbook, FillPatternType.SOLID_FOREGROUND,
                HorizontalAlignment.CENTER_SELECTION, VerticalAlignment.CENTER, BorderStyle.MEDIUM,
                reportMetadata.getPropertyMetadata().getFillColor(), reportMetadata.getPropertyMetadata().isWrapText());
        style.setFont(tablePropertyFont);
        return style;
    }

    public XSSFCellStyle createTableHeaderBorderStyle(XSSFWorkbook workbook) {
        Font tableHeaderFont = createFont(workbook, reportMetadata.getHeaderMetadata().getFontSize(),
                reportMetadata.getHeaderMetadata().isBold(), reportMetadata.getHeaderMetadata().getFont(),
                reportMetadata.getHeaderMetadata().isItalic(), reportMetadata.getHeaderMetadata().getUnderLineType(),
                reportMetadata.getHeaderMetadata().getFontColor());
        XSSFCellStyle style = setStylingAttributes(workbook, FillPatternType.SOLID_FOREGROUND,
                HorizontalAlignment.CENTER, VerticalAlignment.CENTER, BorderStyle.THIN,
                reportMetadata.getHeaderMetadata().getFillColor(), reportMetadata.getHeaderMetadata().isWrapText());
        style.setFont(tableHeaderFont);
        return style;
    }

    public XSSFSheet setReportTitle(XSSFSheet sheet, String reportTitle, XSSFCellStyle style) {
        XSSFRow titleRow = createRow(sheet, 0, reportMetadata.getTitleMetadata().getRowHeight());
      createStringCell(titleRow, reportMetadata.getTitleMetadata().getTitleBlockFirstColumn(), style,
                reportTitle);
        CellRangeAddress cellRangeAddress = new CellRangeAddress(
                reportMetadata.getTitleMetadata().getTitleBlockFirstRow(),
                reportMetadata.getTitleMetadata().getTitleBlockLastRow(),
                reportMetadata.getTitleMetadata().getTitleBlockFirstColumn(),
                reportMetadata.getTitleMetadata().getTitleBlockLastColumn());
        sheet.addMergedRegion(cellRangeAddress);
        formatMergedRegion(cellRangeAddress, sheet, BorderStyle.THIN);
        return sheet;
    }

    private XSSFRow createRow(XSSFSheet sheet, int index, int rowHeight) {
        XSSFRow row = sheet.createRow(index);
        row.setHeight((short) rowHeight);
        return row;
    }

    public XSSFSheet setReportProperties(XSSFSheet sheet, Map<String, String> reportHeader, XSSFCellStyle style) {
        int index = 3;
        Set<String> kSet = reportHeader.keySet();
        for (String key : kSet) {
            XSSFRow excelHeader = createRow(sheet, index, reportMetadata.getPropertyMetadata().getRowHeight());
            excelHeader = createStringCell(excelHeader, 1, style, key);
            createStringCell(excelHeader, 3, reportHeader.get(key));
            CellRangeAddress cellRangeAddress = new CellRangeAddress(index, index, 1, 2);
            sheet.addMergedRegion(cellRangeAddress);
            formatMergedRegion(cellRangeAddress, sheet, BorderStyle.MEDIUM);
            index++;
        }
        return sheet;
    }

    public XSSFSheet setUnmergedReportProperties(XSSFSheet sheet, Map<String, String> reportHeader,
            XSSFCellStyle style) {
        int index = 3;
        Set<String> kSet = reportHeader.keySet();
        for (String key : kSet) {
            XSSFRow excelHeader = createRow(sheet, index, reportMetadata.getPropertyMetadata().getRowHeight());
            excelHeader = createStringCell(excelHeader, 1, style, key);
            createStringCell(excelHeader, 2, reportHeader.get(key));
            index++;
        }
        return sheet;
    }

    public XSSFSheet setReportHeaders(XSSFSheet sheet, List<String> reportHeader, int count,
            XSSFCellStyle tableHeaderStyle) {
        XSSFRow headerRow = createRow(sheet, count + 2, reportMetadata.getHeaderMetadata().getRowHeight() * 20);
        int i = 1;
        for (String headerName : reportHeader) {
            if (reportMetadata.getIndividualColumnWidth() != null) {
                for (String key : reportMetadata.getIndividualColumnWidth().keySet()) {
                    if (headerName.equalsIgnoreCase(key)) {
                        sheet.setColumnWidth(i, reportMetadata.getIndividualColumnWidth().get(key) * 256);
                    }
                }
            }
            headerRow = createStringCell(headerRow, i++, tableHeaderStyle, headerName);
        }
        if (reportMetadata.getHeaderMetadata().isFreezePanel()) {
            sheet.createFreezePane(0, count + 3);
        }
        return sheet;
    }

    private XSSFCellStyle setStylingAttributes(XSSFWorkbook workBook, FillPatternType fillType,
            HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment, BorderStyle borderStyle,
            String colorCode, boolean wrapText) {
        XSSFCellStyle style = workBook.createCellStyle();
        style.setFillForegroundColor(new XSSFColor(Color.decode(colorCode), null));
        style.setFillPattern(fillType);
        style.setAlignment(horizontalAlignment);
        style.setVerticalAlignment(verticalAlignment);
        style.setBorderLeft(borderStyle);
        style.setBorderRight(borderStyle);
        style.setBorderTop(borderStyle);
        style.setBorderBottom(borderStyle);
        style.setWrapText(wrapText);
        return style;
    }

    public XSSFWorkbook createNewSheet(String metadataId, List<ReportHeader> reportProperty) {
        XSSFWorkbook workbook = createReportTitle(metadataId);
        XSSFSheet sheet = getWorksheet(workbook);
        Map<String, String> reportProperties = createReportProperties(reportProperty);
        if (reportProperties != null) {
            XSSFCellStyle tablePropertyStyle = createReportPropertiesStyle(workbook);
            setReportProperties(sheet, reportProperties, tablePropertyStyle);
        }
        createReportHeader(workbook);
        return workbook;
    }

    public XSSFSheet getWorksheet(XSSFWorkbook workbook) {
        return workbook.getSheetAt(0);
    }

    public Map<String, String> createReportProperties(List<ReportHeader> reportProperty) {
        Map<String, String> reportProperties = new LinkedHashMap<>();
        for (String key : reportMetadata.getReportProperties().keySet()) {
            for (ReportHeader reportHeader : reportProperty) {
                if (reportHeader.getName().equals(key)) {
                    reportProperties.put(reportMetadata.getReportProperties().get(key), reportHeader.getValue());
                }
            }
        }
        return reportProperties;
    }

    public XSSFSheet createReportHeader(XSSFWorkbook workbook) {
        XSSFSheet sheet = workbook.getSheetAt(0);
        if (reportMetadata.getReportHeader() != null) {
            XSSFCellStyle tableHeaderStyle = createTableHeaderBorderStyle(workbook);
            sheet = setReportHeaders(sheet, reportMetadata.getReportHeader(), sheet.getLastRowNum(), tableHeaderStyle);
        }
        return sheet;
    }

    public XSSFWorkbook createReportTitle(String metadataId) {
        reportMetadata = reportMetadataRepository.findBymetadataId(metadataId);
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(reportMetadata.getReportName());
        sheet.setDefaultColumnWidth(reportMetadata.getColumnWidth());
        sheet = setPrintProperties(sheet, reportMetadata.isLandscapeOrientation(), reportMetadata.isPrintGridLines());
        XSSFCellStyle titleStyle = createTitleStyle(workbook);
       setReportTitle(sheet, reportMetadata.getReportTitle(), titleStyle);
        return workbook;
    }

    public XSSFWorkbook createNoDataRow(XSSFWorkbook workbook, short lastColumn) {
        String dataMissing = "No Data Found";
        Font titleFont = createFont(workbook, reportMetadata.getTitleMetadata().getFontSize(),
                reportMetadata.getTitleMetadata().isBold(), reportMetadata.getTitleMetadata().getFont(),
                reportMetadata.getTitleMetadata().isItalic(), reportMetadata.getTitleMetadata().getUnderLineType(),
                reportMetadata.getTitleMetadata().getFontColor());
        XSSFCellStyle style = setStylingAttributes(workbook, FillPatternType.SOLID_FOREGROUND,
                HorizontalAlignment.CENTER, VerticalAlignment.CENTER, BorderStyle.THIN,
                reportMetadata.getTitleMetadata().getFillColor(), reportMetadata.getTitleMetadata().isWrapText());
        style.setFont(titleFont);
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow titleRow = createRow(sheet, 12, reportMetadata.getTitleMetadata().getRowHeight());
        createStringCell(titleRow, 1, style, dataMissing);
        CellRangeAddress cellRangeAddress = new CellRangeAddress(12, 13, 1, lastColumn - 1);
        sheet.addMergedRegion(cellRangeAddress);
        formatMergedRegion(cellRangeAddress, sheet, BorderStyle.THIN);
        return workbook;
    }

    public XSSFWorkbook createNoDataRowWithIndex(XSSFWorkbook workbook, short lastColumn, int index) {
        String dataMissing = "No Data Found";
        Font titleFont = createFont(workbook, reportMetadata.getTitleMetadata().getFontSize(),
                reportMetadata.getTitleMetadata().isBold(), reportMetadata.getTitleMetadata().getFont(),
                reportMetadata.getTitleMetadata().isItalic(), reportMetadata.getTitleMetadata().getUnderLineType(),
                reportMetadata.getTitleMetadata().getFontColor());
        XSSFCellStyle style = setStylingAttributes(workbook, FillPatternType.SOLID_FOREGROUND,
                HorizontalAlignment.CENTER, VerticalAlignment.CENTER, BorderStyle.THIN,
                reportMetadata.getTitleMetadata().getFillColor(), reportMetadata.getTitleMetadata().isWrapText());
        style.setFont(titleFont);
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow titleRow = createRow(sheet, index, reportMetadata.getTitleMetadata().getRowHeight());
       createStringCell(titleRow, 1, style, dataMissing);
        CellRangeAddress cellRangeAddress = new CellRangeAddress(index, index + 1, 1, lastColumn - 1);
        sheet.addMergedRegion(cellRangeAddress);
        formatMergedRegion(cellRangeAddress, sheet, BorderStyle.THIN);
        return workbook;
    }

    public XSSFRow createDataRow(XSSFSheet sheet, int index) {
        XSSFRow row = sheet.createRow(index);
        row.setHeight((short) (reportMetadata.getDataMetadata().getRowHeight() * 20));
        return row;
    }

    public XSSFCellStyle createTotalAmountStyle(XSSFCellStyle style, XSSFWorkbook workbook) {
        XSSFCellStyle totalAmountStyle = workbook.createCellStyle();
        totalAmountStyle.cloneStyleFrom(style);
        Font font = workbook.createFont();
        font.setBold(true);
        short fontSize = style.getFont().getFontHeightInPoints();
        font.setFontHeightInPoints(fontSize);
        totalAmountStyle.setFont(font);
        return totalAmountStyle;
    }

    public XSSFCellStyle createCellStyle(XSSFCellStyle style, HorizontalAlignment horizontalAlignment,
            VerticalAlignment verticalAlignment) {
        style.setAlignment(horizontalAlignment);
        style.setVerticalAlignment(verticalAlignment);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        return style;
    }

    public XSSFRow createIntegerCell(XSSFRow excelRow, int index, XSSFCellStyle style, double data,
            XSSFDataFormat format) {
        Cell cell = excelRow.createCell(index, CellType.NUMERIC);
        cell.setCellValue(data);
        style.setDataFormat(format.getFormat(NUMERIC_FORMAT));
        cell.setCellStyle(style);
        return excelRow;
    }

    public XSSFRow createPercentCell(XSSFRow excelRow, int index, XSSFCellStyle style, double data,
            XSSFDataFormat format) {
        Cell cell = excelRow.createCell(index, CellType.NUMERIC);
        style.setDataFormat(format.getFormat(DECIMAL_FORMAT));
        cell.setCellValue(data);
        cell.setCellStyle(style);
        return excelRow;
    }

    public XSSFCellStyle createPercentStyle(XSSFWorkbook workbook) {
        Font dataFont = createFont(workbook, reportMetadata.getDataMetadata().getFontSize(),
                reportMetadata.getDataMetadata().isBold(), reportMetadata.getDataMetadata().getFont(),
                reportMetadata.getDataMetadata().isItalic(), reportMetadata.getDataMetadata().getUnderLineType(),
                reportMetadata.getDataMetadata().getFontColor());
        XSSFCellStyle style = setStylingAttributes(workbook, FillPatternType.SOLID_FOREGROUND,
                HorizontalAlignment.CENTER, VerticalAlignment.CENTER, BorderStyle.HAIR,
                reportMetadata.getDataMetadata().getFillColor(), reportMetadata.getDataMetadata().isWrapText());
        style.setFont(dataFont);
        return style;
    }

    public XSSFWorkbook insertLogo(XSSFSheet sheet, XSSFWorkbook workbook) {
        try {
            InputStream inputStream = getClass().getResourceAsStream("../../images/LOGO.PNG");
            if (inputStream != null) {
                byte[] imageBytes = IOUtils.toByteArray(inputStream);
                int pictureIndex = workbook.addPicture(imageBytes, Workbook.PICTURE_TYPE_PNG);
                inputStream.close();
                XSSFClientAnchor anchor = new XSSFClientAnchor();
                anchor.setCol1(1);
                anchor.setRow1(0);
                XSSFDrawing xssfDrawing = sheet.createDrawingPatriarch();
                XSSFPicture xssfPicture = xssfDrawing.createPicture(anchor, pictureIndex);
                xssfPicture.resize();
            } else {
                logger.debug("Company logo file not present. Generating the report without logo");
            }
        } catch (IOException e) {
            logger.error("Error in inserting the logo");
        }
        return workbook;
    }

    private String getCurrencyFormatByCode(String currencyCode) {
        String currencyFormat;
        String currencySymbol = currencyCode;
        if (currencyCode == null || currencyCode.trim().isEmpty()) {
            currencyCode = "USD";
        }
        Locale locale = LOCALE_CURRENCY_MAP.get(currencyCode.trim());
        if (locale != null) {
            Currency currency = Currency.getInstance(locale);
            currencySymbol = currency.getSymbol(locale);
        }

        if (currencySymbol != null && currencySymbol.equals(currencyCode)) {
            currencyFormat = "[$" + currencySymbol + "]#,##0.00_);[Red]([$" + currencySymbol + "]#,##0.00)";
        } else {
            currencyFormat = currencySymbol + "#,##0.00_);[Red](" + currencySymbol + "#,##0.00)";
        }

        return currencyFormat;
    }

    public static final Map<String, Locale> LOCALE_CURRENCY_MAP;
    static {
        final Map<String, Locale> m = new HashMap<>();

        m.put("USD", Locale.US);
        m.put("EUR", Locale.GERMANY);
        m.put("GBP", Locale.UK);
        m.put("JPY", Locale.JAPAN);

        LOCALE_CURRENCY_MAP = Collections.unmodifiableMap(m);
    }
}
