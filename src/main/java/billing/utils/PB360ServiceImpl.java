package billing.utils;

import static core.utils.CommonConstants.CHAR_H;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.ZonedDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import billing.handler.model.AutomaticPolicyChange;
import billing.model.AccountAdjustDates;
import billing.model.DisbursementInterface;
import billing.model.ImmediateActivity;
import billing.model.PrintRequest;
import billing.model.RescindNotice;
import core.api.ExecContext;
import core.exception.application.ApplicationException;
import core.security.service.RestService;
import core.utils.DateRoutine;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;

@SuppressWarnings("unchecked")
@Component
public class PB360ServiceImpl implements PB360Service {

    @Value("${pm.url}")
    private String exceedUrl;

    @Autowired
    private RestService restService;

    @Autowired
    private ExecContext execContext;

    private static final String POST_ADJUST_DATES = "/adjustDates";
    private static final String ACCOUNTS = "/accounts";
    private static final String SLASH = "/";
    private static final char CHAR_A = 'A';
    private static final String POST_PRINTS = "/prints";
    private static final String POST_PRINTS_REQUEST = "/_request";
    private static final String APPRELEASE = "V80";
    private static final String POST_CONTRACT = "/contracts";
    private static final String POST_AUTO_CHANGE = "/plans/_automatic";
    private static final String POST_DISBUR_STATUS = "/recovery/disbursement/_update";
    private static final String PM_CHECK_UTILITY = "/_check";
    private static final String SCHEDULE = "/_immediateSchedule";
    private static final String EXTERNAL_SERVICE_ISSUE = "external.service.issue";
    private static final String RESCIND_NOTICE_REQUEST = "/_rescindNotice";

    private AccountAdjustDates updateDateSchedule(AccountAdjustDates accountAdjustDates) {
        if (exceedUrl == null || exceedUrl.isEmpty()) {
            return accountAdjustDates;
        }
        HttpHeaders httpHeaders = execContext.getHttpHeaders();
        restService.setHeaders(httpHeaders);
        restService.setURL(exceedUrl + ACCOUNTS + SLASH + accountAdjustDates.getAccountId() + POST_ADJUST_DATES);
        restService.setModel(AccountAdjustDates.class);
        restService.setEntity(accountAdjustDates);
        try {
            HttpEntity<AccountAdjustDates> response = (HttpEntity<AccountAdjustDates>) restService.postResource();
            if (response == null || response.getBody() == null) {
                throw new ApplicationException(EXTERNAL_SERVICE_ISSUE);
            } else if (response.getBody() != null && response.getBody().getErrorDescription() != null
                    && !response.getBody().getErrorDescription().trim().isEmpty()) {
                Exception ex = new Exception(response.getBody().getErrorDescription().trim());
                throw new ApplicationException(EXTERNAL_SERVICE_ISSUE, ex);
            }
            return response.getBody();
        } catch (ApplicationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new ApplicationException(EXTERNAL_SERVICE_ISSUE, ex);
        }
    }

    @Override
    public void patchFutureDate(String accountId) {
        AccountAdjustDates accountAdjustDates = new AccountAdjustDates();
        accountAdjustDates.setAccountId(accountId);
        accountAdjustDates.setSe3Date(execContext.getApplicationDate());
        accountAdjustDates.setDateResetIndicator(CHAR_A);
        updateDateSchedule(accountAdjustDates);
    }

    @Override
    public AccountAdjustDates patchChargeDate(String accountId, char addChargeInd, boolean quoteInd) {
        AccountAdjustDates accountAdjustDates = new AccountAdjustDates();
        accountAdjustDates.setAccountId(accountId);
        accountAdjustDates.setApplicationName("BCMOCA");
        accountAdjustDates.setQuoteIndicator(quoteInd);
        accountAdjustDates.setAddChargeIndicator(addChargeInd);
        accountAdjustDates.setSe3Date(execContext.getApplicationDate());
        accountAdjustDates = updateDateSchedule(accountAdjustDates);
        return accountAdjustDates;
    }

    @Override
    public void pathRecoveryDate(String accountId, String policyId, String polEffDate, char dateResetSwitch,
            char supportPlanSwitch) {
        AccountAdjustDates accountAdjustDates = new AccountAdjustDates();
        accountAdjustDates.setAccountId(accountId);
        accountAdjustDates.setPolicyId(policyId);
        if (polEffDate != null) {
            accountAdjustDates.setPolicyEffectiveDate(polEffDate);
        }
        accountAdjustDates.setDateResetIndicator(dateResetSwitch);
        accountAdjustDates.setSupportPlanIndicator(supportPlanSwitch);

        updateDateSchedule(accountAdjustDates);
    }

    private void postPrintInterfaceRequest(String accountId, String referenceId, PrintRequest printRequest) {
        if (exceedUrl == null || exceedUrl.isEmpty()) {
            return;
        }
        HttpHeaders httpHeaders = execContext.getHttpHeaders();
        restService.setHeaders(httpHeaders);
        restService.setURL(
                exceedUrl + ACCOUNTS + SLASH + accountId + POST_PRINTS + SLASH + referenceId + POST_PRINTS_REQUEST);
        restService.setModel(PrintRequest.class);
        restService.setEntity(printRequest);
        try {
            HttpEntity<PrintRequest> response = (HttpEntity<PrintRequest>) restService.postResource();
            if (response == null || response.getBody() == null) {
                throw new ApplicationException(EXTERNAL_SERVICE_ISSUE);
            }
        } catch (ApplicationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new ApplicationException(EXTERNAL_SERVICE_ISSUE, ex);
        }
    }

    @Override
    public void postGeneratePrintNoticeRequest(String accountId, String programId, String payorClientId, String fromId,
            String additionalKey, String businessTransaction) {
        PrintRequest printRequest = new PrintRequest();
        printRequest.setApplicationVersion(APPRELEASE);
        printRequest.setApplicationId(accountId);
        printRequest.setPrintClientId(payorClientId);
        printRequest.setStartPrintFromId(fromId);
        printRequest.setBusinessTransaction(businessTransaction);
        printRequest.setRequestUserId(ApplicationProduct.BCMS.toString());
        printRequest.setRequestApplicationName(ApplicationProduct.BCMS.toString());
        printRequest.setPlatformCode(CHAR_H);
        printRequest.setDataTimestamp(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        printRequest.setAdditionalKey(additionalKey);
        postPrintInterfaceRequest(accountId, programId, printRequest);
    }

    @Override
    public void postAutomaticPolicyPlanChange(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            String programCalling, boolean isPolPlanChnage, char eftReversalIndicator) {
        AutomaticPolicyChange automaticPolicyChange = new AutomaticPolicyChange();
        automaticPolicyChange.setAccountId(accountId);
        automaticPolicyChange.setPolicyId(policyId);
        automaticPolicyChange.setPolicyEffDate(polEffectiveDate);
        automaticPolicyChange.setCallingModuleName(programCalling);
        automaticPolicyChange.setEftReversalIndicator(eftReversalIndicator);
        automaticPolicyChange.setPolPlanChange(isPolPlanChnage);
        sendRequestAutomaticPolicyPlanChange(accountId, policyId, automaticPolicyChange);
    }

    private void sendRequestAutomaticPolicyPlanChange(String accountId, String policyId,
            AutomaticPolicyChange automaticPolicyChange) {
        if (exceedUrl == null || exceedUrl.isEmpty()) {
            return;
        }
        try {
            policyId = URLEncoder.encode(policyId, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Exception ex = new Exception("Unsupported Encoding of Policy Id");
            throw new ApplicationException(EXTERNAL_SERVICE_ISSUE, ex);
        }
        HttpHeaders httpHeaders = execContext.getHttpHeaders();
        restService.setHeaders(httpHeaders);
        restService
                .setURL(exceedUrl + ACCOUNTS + SLASH + accountId + POST_CONTRACT + SLASH + policyId + POST_AUTO_CHANGE);
        restService.setModel(AutomaticPolicyChange.class);
        restService.setEntity(automaticPolicyChange);
        try {
            HttpEntity<AutomaticPolicyChange> response = (HttpEntity<AutomaticPolicyChange>) restService.postResource();
            if (response == null || response.getBody() == null) {
                throw new ApplicationException(EXTERNAL_SERVICE_ISSUE);
            }
        } catch (ApplicationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new ApplicationException(EXTERNAL_SERVICE_ISSUE, ex);
        }
    }

    @Override
    public void updateDisbursementStatus(String callingProgramName, char disburStatusCode, String disburTypeCode,
            String techDisburId, String userDisburId, ZonedDateTime recoveryDate, String userId, String functionValue) {
        DisbursementInterface disbursementInterface = new DisbursementInterface();
        disbursementInterface.setCallingProgramName(callingProgramName);
        disbursementInterface.setDisburStatusCode(disburStatusCode);
        disbursementInterface.setDisburTypeCode(disburTypeCode);
        disbursementInterface.setTechDisburId(techDisburId);
        disbursementInterface.setUserDisburId(userDisburId);
        disbursementInterface.setDisburActualDate(recoveryDate);
        disbursementInterface.setUserId(userId);
        disbursementInterface.setDisburScheduleDate(recoveryDate);
        disbursementInterface.setFunctionValue(functionValue);
        postToDisbursementInterface(techDisburId, disbursementInterface);
    }

    private void postToDisbursementInterface(String accountId, DisbursementInterface disbursementInterface) {
        if (exceedUrl == null || exceedUrl.isEmpty()) {
            return;
        }
        HttpHeaders httpHeaders = execContext.getHttpHeaders();
        restService.setHeaders(httpHeaders);
        restService.setURL(exceedUrl + ACCOUNTS + SLASH + accountId + POST_DISBUR_STATUS);
        restService.setModel(DisbursementInterface.class);
        restService.setEntity(disbursementInterface);
        try {
            HttpEntity<DisbursementInterface> response = (HttpEntity<DisbursementInterface>) restService.postResource();
            if (response == null || response.getBody() == null) {
                throw new ApplicationException(EXTERNAL_SERVICE_ISSUE);
            }
        } catch (ApplicationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new ApplicationException(EXTERNAL_SERVICE_ISSUE, ex);
        }
    }

    @Override
    public void checkPMStatus() {
        if (exceedUrl == null || exceedUrl.isEmpty()) {
            return;
        }
        HttpHeaders httpHeaders = execContext.getHttpHeaders();
        restService.setHeaders(httpHeaders);
        restService.setURL(exceedUrl + ACCOUNTS + PM_CHECK_UTILITY);
        restService.setModel(String.class);
        try {
            HttpEntity<String> response = (HttpEntity<String>) restService.getResource();
            if (response == null || response.getBody() == null) {
                throw new ApplicationException("connection.issue", new Object[] { "PM" });
            }
        } catch (ApplicationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new ApplicationException("connection.issue", new Object[] { "PM" }, ex);
        }
    }

    @Override
    public void scheduleImmediateActivity(ImmediateActivity immediateActivity) {
        if (exceedUrl == null || exceedUrl.isEmpty()) {
            return;
        }
        HttpHeaders httpHeaders = execContext.getHttpHeaders();
        restService.setHeaders(httpHeaders);
        restService.setURL(exceedUrl + ACCOUNTS + SCHEDULE);
        restService.setModel(ImmediateActivity.class);
        restService.setEntity(immediateActivity);
        try {
            HttpEntity<ImmediateActivity> response = (HttpEntity<ImmediateActivity>) restService.postResource();
            if (response == null) {
                throw new ApplicationException(EXTERNAL_SERVICE_ISSUE);
            }
        } catch (ApplicationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new ApplicationException(EXTERNAL_SERVICE_ISSUE, ex);
        }
    }

    @Override
	public void postRescindNoticeRequest(String accountId, RescindNotice rescindNotice) {
		if (exceedUrl == null || exceedUrl.isEmpty()) {
			return;
		}
		HttpHeaders httpHeaders = execContext.getHttpHeaders();
		restService.setHeaders(httpHeaders);
		restService.setURL(exceedUrl + ACCOUNTS + SLASH + accountId + POST_PRINTS + SLASH + RESCIND_NOTICE_REQUEST);
		restService.setModel(RescindNotice.class);
		restService.setEntity(rescindNotice);
		try {
			HttpEntity<RescindNotice> response = (HttpEntity<RescindNotice>) restService.postResource();
			if (response == null || response.getBody() == null) {
				throw new ApplicationException(EXTERNAL_SERVICE_ISSUE);
			} else {
				RescindNotice body = response.getBody();
				if (body != null) {
					String errDesc = body.getErrDesc();
					if (errDesc != null && !errDesc.trim().isEmpty()) {
						Exception ex = new Exception(errDesc.trim());
						throw new ApplicationException(EXTERNAL_SERVICE_ISSUE, ex);
					}
				}
			}
		} catch (ApplicationException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new ApplicationException(EXTERNAL_SERVICE_ISSUE, ex);
		}
	}
}
