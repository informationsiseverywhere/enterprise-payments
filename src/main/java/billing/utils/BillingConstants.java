package billing.utils;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;

import core.utils.CommonConstants;

public class BillingConstants {
    public static final String STATEMENT_ROW = "00";
    public static final String BIL_ACCOUNT_ROW = "01";
    public static final String PAYMENT_REVERSED = "02";
    public static final String ENTRY_NUMBER = "0000";
    public static final String BIL_BANK_CODE = "999";
    public static final String CURRENCY_USA = "USD";
    public static final String PAYMENT = "PY";
    public static final Integer ZERO_VALUE = 0;
    public static final String PLUS_SIGN = "+";
    public static final String DEFAULT_DATE = "9999-12-31";
    public static final String MASTER_COMPANY_NUMBER = "99";
    public static final String ERROR_CODE = "0000";
    public static final String COMPANY_LOCATION_NUMBER = "00";
    public static final String THIRD_PARTY = "T";
    public static final String SYSTEM_USER_ID = "SYSTEM";
    public static final String MINUS_SIGN = "-";
    public static final String DOLLAR_SIGN = "$";
    public static final String EVENT_CONTEXT = "CASH-WAS-WRITTEN-OFF";
    public static final Character TECH_KEY_TYPE_CODE = 'G';

    public enum AccountType {
        GROUP_ACCOUNT("GA"),
        DIRECTBILL("DirectBill"),
        AGENCYBILL("AgencyBill"),
        GROUPBILL("GroupBill"),
        AGENCY_ACCOUNT("AG"),
        BILLACCOUNT("DB"),
        UNIDENTIFIED("UnIdentified"),
        EMPTY_STRING(BLANK_STRING),
        AGENCY("Agency"),
        GROUP("Group");

        private final String type;

        private AccountType(String type) {
            this.type = type;
        }

        public boolean hasValue(String value) {
            return value != null && type.equals(value);
        }

        @Override
        public String toString() {
            return this.type;
        }

        public static AccountType getEnumKey(String type) {
            for (AccountType accountType : values()) {
                if (accountType.type.equals(type)) {
                    return accountType;
                }
            }
            return EMPTY_STRING;
        }
    }

    public enum IdentifierType {
        CONSUMER_DIRECT("DB"), AGENCY("AG"), GROUP("GA"), POLICY("P"), ADDITIONALID("A"), EMPTY_STRING(BLANK_STRING);

        private final String type;

        private IdentifierType(String type) {
            this.type = type;
        }

        public boolean hasValue(String value) {
            return value == null ? Boolean.FALSE : type.equals(value);
        }

        @Override
        public String toString() {
            return this.type;
        }

        public static IdentifierType getEnumKey(String type) {
            for (IdentifierType accountType : values()) {
                if (accountType.type.equals(type)) {
                    return accountType;
                }
            }
            return EMPTY_STRING;
        }
    }

    public enum PaymentType {
        BANK_ACCOUNT("6"), CREDIT_CARD("H"), AGENCY_SWEEP("8");

        private final String type;

        private PaymentType(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum AccountTypeCode {
        ACCOUNT_BILLCD("AB"),
        DIRECTBILLCD("CB"),
        AGENCYREIMBURSEMENTSCD("AR"),
        COMMISSIONACCOUNTCD("CM"),
        SINGLE_POLICY("SP"),
        ACCOUNTCURRENTCD("AC"),
        STATEMENTAGENTCD("AG"),
        LISTBILLCD("LB"),
        GROUPACCOUNTCD("TP");

        private final String typeCd;

        private AccountTypeCode(String typeCd) {
            this.typeCd = typeCd;
        }

        public String getValue() {
            return typeCd;
        }
    }

    public enum BillingMethod {
        EFT("EFT"), ECC("ECC"), DI("DI"), ELECTRONIC_WALLET("EWT");

        private final String method;

        private BillingMethod(String method) {
            this.method = method;
        }

        public boolean hasValue(String value) {
            return value == null ? Boolean.FALSE : method.equals(value);
        }

        @Override
        public String toString() {
            return this.method;
        }
    }

    public enum BusCodeTranslationType {
        SORT_SEQUENCE("SSQ"),
        ACCOUNTPLAN("BCL"),
        COLLECTION_LETTER("CL "),
        LAST_COLLECTION_LETTER("CL9"),
        BILLINGMETHOD("BILLING_METHOD"),
        PRESENTMENTMETHOD("PMD"),
        STATEMENT_STATUS("GSS"),
        PAYMENTTYPE("PaymentType"),
        PAYMENTMETHOD("PMS"),
        GROUPRECONACTION("RTP"),
        GROUPRECONREASON("GroupReconciliationReason"),
        WRITEOFF_MANUAL_TYPE("CWM"),
        DIRECT_TYPE("SUS"),
        DISBURSE_SUSPENSE_TYPE("DAD"),
        REVERSE_SUSPENSE_TYPE("PRV"),
        CREDIT_CARD_TYPE("CCT"),
        EQUITY("EQT"),
        UNDERPAY_WRITEOFF_TYPE("WPI"),
        EXCLUSION_TYPE("STX"),
        WRITEOFF_MANUAL_PAYMENT_REVERSAL("CMR"),
        WRITEOFF_MANUAL_LIFE_REVERSAL("CLR"),
        SLOT_CODE("SLT"),
        GROUP_TYPE("BTP"),
        EFT_ACCOUNT_TYPE("EAT"),
        EFT_PLAN("EFT_PLAN"),
        LINE_OF_BUSINESS("LOB"),
        CREDIT_CARD_PAYMENT("CPY"),
        BANK_ACCOUNT_PAYMENT("1XA"),
        CASH_PAYMENT("PYT"),
        DEFER("DFT"),
        WRITEOFF("WOT"),
        EXCLUDE("EXT"),
        BILLING_TYPE("BTS"),
        GROUP_ACCOUNT_STATUS("TST"),
        WRITEOFF_LIFE_TYPE("CWL"),
        AMOUNT("AMT"),
        ADDRESS_TYPE("CLT_ADR_TYPE_V"),
        DOWN_PAYMENT("DPT"),
        CCD("CCD"),
        BTY("BTY"),
        BTG("BTG"),
        BTP("BTP"),
        BANK("BANK"),
        BILLACCOUNTTYPE("ACCOUNT_TYPE"),
        UNACCOUNTTYPE("UNACCOUNT_TYPE"),
        DISBURSE_AGENCY_SWEEP("DAS"),
        GROUP_BILL_TYPE("BTP"),
        TRANSACTION_TYPE("BAT"),
        BILLING_ITEM("BIT"),
        POLICY_SYMBOL_TYPE("SYM"),
        DISPOSITION_TYPE("DispositionType"),
        BPS("BPS"),
        COMMISION("CIT"),
        EIT("EIT"),
        BPA("BPA"),
        PAYMENT_TYPE_CODE("PYC"),
        COLLECT_AGENCY_PAYMENT("COL"),
        EXTERNAL_BANK_PAYMENT("ACH"),
        OTHER_PAYMENT("OTH"),
        BUSINESS_GROUP("BGRP"),
        BANK_PROFILE_ATTR_SUB_TYPE("BankProfileAttrSubType"),
        ADDITIONALID("AMI"),
        DIGITAL_WALLET_TYPE("DWT"),
        WALLET_PAYMENT_TYPE("DWP"),
        NO_BALANCE_FOUND("NBU"),
        PAYMENT_OVER_TOTAL_DUE("POT"),
        NY_RENEWAL_CANCELLED("NYR");

        private final String type;

        private BusCodeTranslationType(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum BusCodeTranslationParentCode {
        PAYABLE_ITEM("PAYITEM");

        private final String type;

        private BusCodeTranslationParentCode(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum AccountDescriptionCode {
        BTC("BTC"),
        BCC("BCC"),
        BMC("BMC"),
        SDC("SDC"),
        RDC("RDC"),
        PMC("PMC"),
        PNC("PNC"),
        RPL("RPL"),
        SGC("SGC"),
        AIC("AIC"),
        DPC("DPC"),
        SSC("SDC"),
        FCC("FCC"),
        SCC("SCC"),
        TSC("TSC"),
        DBS("DBS"),
        DSB("DSB");

        private final String type;

        private AccountDescriptionCode(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum BilDesReasonType {
        ONE_XA("1XA"),
        ECH("ECH"),
        ACH("ACH"),
        FTC("FTC"),
        CPY("CPY"),
        DSP("DSP"),
        RCT("RCT"),
        STATEMENT_STATUS("GSS"),
        EQT("EQT"),
        COL("COL"),
        PYT("PYT"),
        PYS("PYS"),
        PYC("PYC"),
        GPY("GPY"),
        GPT("GPT"),
        ASW("ASW"),
        SOP("SOP"),
        SUP("SUP"),
        REC("REC"),
        RAD("RAD"),
        RELEASE_REVERSE_HOLD("RRH"),
        DISBURSE_MANUAL("DBR"),
        DISBURSE_AUTOMATIC("DBA"),
        REFUND_METHOD("CPM"),
        REVERSE_REASON("CWR"),
        NON_SUFFICIENT_FUND("NSF"),
        SUSPENDED_BILLING("SBI"),
        TRANSFER("CTR"),
        RESUME_BILLING_FOLLOWUP_AUTOMATIC("RBA"),
        AUTOMATIC_RESUME_BILLING("RSA"),
        RESUME_FOLLOWUP_AUTOMATIC("RFA"),
        MAN_REINV_CANCELLED("REP"),
        PRORATA_CANCELLATION_NONPAYMENT_PREMIUMS("PNP"),
        RENEWAL_NONPAYMENT_CANCELLATION("RNP"),
        DISPOSITION_REASON("DSP"),
        BUSINESS_GROUP("BGRP"),
        CAP("CAP"),
        ONE_CC("1CC"),
        WALLET_PAYMENT_TYPE("DWP"),
        DIGITAL_WALLET_HOUSE("DWH"),
        EFT_WALLET_PAYMENT_TYPE("EWP"),
        DELAY_DAYS("DLY");

        private final String type;

        private BilDesReasonType(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum BilDesReasonCode {
        PAC("PAC"),
        DES("DES"),
        RV("RV"),
        CY("CY"),
        STATEMENT("STT"),
        OVER_PAYMENT("OW"),
        UNDER_PAYMENT("UW"),
        UNDER_PAY_WRITE_OFF("IW"),
        CET("CET"),
        ACCOUNT_TYPE("CCD"),
        FTC("FTC"),
        REOPEN_TO_PAID("OM"),
        REOPENED("RO"),
        AGENY_REFUND_CHECK("AD"),
        REFUND_TO_AGENCY("AS"),
        OPERATOR_SUSPEND("OSP"),
        DOWN_PAYMENT_RECEIPT("DPR"),
        RPRINT("RRP"),
        SECOND_NOTICE("SND"),
        MANUAL_PENDING_CANCEL("CMP"),
        FILE_TYPE("FTC"),
        EQUITY_NO_POLICY_NUMBER("ENP"),
        OVERPAYMENT("OVP");

        private final String type;

        private BilDesReasonCode(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum BilDspTypeCode {
        SUSPENDED("SP"),
        DISBURSED("DB"),
        WRITE_OFF("WO"),
        TRANSFERRED("TF"),
        REVERSED("RV"),
        APPLIED("AP"),
        WRITE_OFF_FOR_RECONCILIATION("WU"),
        DIRECT("DR"),
        SUSPENDED_WRITE_OFF("WR"),
        EMPTY_STRING(BLANK_STRING),
        EXCLUDE("XC");

        private final String type;

        private BilDspTypeCode(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }

        public static BilDspTypeCode getEnumKey(String typeCd) {
            for (BilDspTypeCode bilDspTypeCode : values()) {
                if (bilDspTypeCode.type.equals(typeCd)) {
                    return bilDspTypeCode;
                }
            }
            return EMPTY_STRING;
        }
    }

    public enum BilCashEntryMethodCode {

        WRITE_OFF("WRO"),
        SUSPENDED("SUS"),
        APPLIED("API"),
        APPLY("APP"),
        TRANSFERED("TRF"),
        CASH("CSH"),
        UNIDENTIFIED("UCS");

        private final String type;

        private BilCashEntryMethodCode(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum Language {
        ENGLISH("EN"), FRENCH("FR");

        private final String type;

        private Language(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum BilReceiptTypeCode {

        CREDIT_CASH("CR"),
        CREDIT_STATEMENT("CS"),
        UNDERPAY_WRITEOFF("IW"),
        COMMISSION_PAYMENT_CASH("CM"),
        EVEN_ADJUSTMENT("IE"),
        GROSS_OVERPAY("GO"),
        GROSS_WRITEOFF("GT"),
        GROSS_PAYMENT("GG"),
        EFT("EY"),
        EFT_CREDIT_CARD("EC");

        private final String typeCd;

        private BilReceiptTypeCode(String typeCd) {
            this.typeCd = typeCd;
        }

        public String getValue() {
            return typeCd;
        }
    }

    public enum PaymentOperations {
        ALLOW_DISBUSEMENT("allowDisburse"),
        ALLOW_SUSPENSE("allowSuspense"),
        ALLOW_UPDATE("allowUpdate"),
        ALLOW_REVERSE("allowReverse"),
        ALLOW_WRITEOFF("allowWriteOff");

        private final String typeCd;

        private PaymentOperations(String typeCd) {
            this.typeCd = typeCd;
        }

        public String getValue() {
            return typeCd;
        }
    }

    public enum CashEntryMethod {
        EFT('A'),
        EFT_CREDIT_CARD('E'),
        CREDIT_CARD('H'),
        ONE_TIME_ACH('3'),
        BANK_ACCOUNT('6'),
        CREDIT('C'),
        EMPTY_CHAR(CommonConstants.BLANK_CHAR),
        RECONCILED_CASH('T'),
        AGENT_ACH('1'),
        INSURED('2'),
        AGENCY_SWEEP('8'),
        AGENT_CREDIT_CARD('5'),
        TOLERANCE_WRITEOFF('I'),
        STATEMENT('S'),
        ONLINE_ENTRY('G'),
        TAPE_FROM_CASH_RECONCILIATION('T'),
        LOCKBOX('D'),
        OCR('O'),
        VENDOR_COLLECTIONS('4');

        private final Character cashEntryMethod;

        private CashEntryMethod(Character cashEntryMethod) {
            this.cashEntryMethod = cashEntryMethod;
        }

        public Character getCashEntryMethod() {
            return cashEntryMethod;
        }

        public boolean hasValue(Character value) {
            return value != null && cashEntryMethod.equals(value);
        }

        public static CashEntryMethod getEnumValue(Character cashEntryMtd) {
            for (CashEntryMethod cashEntry : values()) {
                if (cashEntry.cashEntryMethod.equals(cashEntryMtd)) {
                    return cashEntry;
                }

            }
            return EMPTY_CHAR;
        }

    }

    public enum BankAccountTypeCd {
        PERSONAL_SAVINGS("001"),
        PERSONAL_CHECKING("002"),
        MONEY_MARKET("003"),
        BUSINESS_SAVINGS("004"),
        BUSINESS_CHECKING("005"),
        SAVINGS("SAV"),
        BUSINESS_SAVE("BSV"),
        CHECK("CHK"),
        BUSINESS_CHECK("BCK");

        private final String bankAccountTypeCode;

        private BankAccountTypeCd(String bankAccountTypeCode) {
            this.bankAccountTypeCode = bankAccountTypeCode;
        }

        public boolean hasValue(String value) {
            return bankAccountTypeCode.equals(value);
        }

        @Override
        public String toString() {
            return this.bankAccountTypeCode;
        }

    }

    public enum ManualSuspendIndicator {
        BLANK(CommonConstants.BLANK_CHAR), ENABLED(CommonConstants.CHAR_Y), DISABLED(CommonConstants.CHAR_N);

        private final Character typeCd;

        private ManualSuspendIndicator(Character typeCd) {
            this.typeCd = typeCd;
        }

        public Character getValue() {
            return typeCd;
        }

    }

    public enum ReverseReSuspendIndicator {
        BLANK(CommonConstants.BLANK_CHAR),
        DSP_FULLY_REVERESED('R'),
        RESUSPENDED('S'),
        TRANSFER_PENDING_BILL('A'),
        TRANSFER_PENDING_GROUP('T'),
        TRANSFER_PENDING_AGENCY('G'),
        PAYMENTS_REAPPLIED('Z'),
        RECONCILIATION_NOT_COMPLETE('N');

        private final Character typeCd;

        private ReverseReSuspendIndicator(Character typeCd) {
            this.typeCd = typeCd;
        }

        public Character getValue() {
            return typeCd;
        }

    }

    public enum BankAccountType {
        SAVING('S'), CHECKING('C'), MONEY_MARKET('M');

        private final Character bankAccountTyp;

        private BankAccountType(Character bankAccountType) {
            this.bankAccountTyp = bankAccountType;
        }

        public boolean hasValue(Character value) {
            return bankAccountTyp.equals(value);
        }

        public Character toChar() {
            return this.bankAccountTyp;
        }

    }

    public enum PayableItemCode {
        TRR("TRR"), SUS("SUS");

        private final String type;

        private PayableItemCode(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum EftRecordType {
        ORIGINAL('O'),
        PRENOTE('P'),
        REPRESENTED('R'),
        ORIGINAL_REPRINT_REQUESTED('D'),
        REPRINTED_REQUESTED('Z'),
        ONE_TIME_ACH('H'),
        AGENCY_SWEEP('S'),
        RECONCILED_CASH('T'),
        UNIDENTIFIED('U'),
        COMPLETE('C'),
        DIGITAL_WALLET('W');

        private final Character type;

        private EftRecordType(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum CashProcessingCode {

        CASH_APPLY('C'), CASH_REVERSAL('R'), GROUP_REVERSAL('V'), GROUP_APPLY('Y'), GROUP_WAIT('W');

        private final Character cashProcessCode;

        private CashProcessingCode(Character cashProcessCode) {
            this.cashProcessCode = cashProcessCode;
        }

        public boolean hasValue(Character value) {
            return cashProcessCode.equals(value);
        }

        public Character getValue() {
            return this.cashProcessCode;
        }

    }

    public enum ProcessIndicator {

        NOT_PROCESSED('N'), RE_KICK('R'), PROCESSED('Y'), WARNING('W'), REVERSAL_PENDING('X'), ERROR('E');

        private final Character processInd;

        private ProcessIndicator(Character processInd) {
            this.processInd = processInd;
        }

        public Character getValue() {
            return this.processInd;
        }

    }

    public enum ReverseAction {

        SUSPEND_BILLING_FOLLOW_UP('B'), SUSUPEND_BILLING('S'), WORK_IN_PROGRESS('Y'), NON_SUFFICIENT_FUND('Y');

        private final Character revAction;

        private ReverseAction(Character revAction) {
            this.revAction = revAction;
        }

        public boolean hasValue(Character value) {
            return revAction.equals(value);
        }

        public Character getValue() {
            return this.revAction;
        }

    }

    public enum TechnicalKeyTypeCode {
        THIRD_PARTY('T'), AGENCY('A'), UNIDENTIFIED('U');

        private final Character type;

        private TechnicalKeyTypeCode(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum ObjectIdentifier {

        BCMOSRVP("BCMOSRVP"), BCMOTRP("BCMOTRP"), DEFAULT("DEFAULT"), PROCESSCONTROLSCHEDULER("BCMOSCH");

        private final String objIdentifier;

        private ObjectIdentifier(String objIdentifier) {
            this.objIdentifier = objIdentifier;
        }

        public boolean hasValue(String value) {
            return objIdentifier.equals(value);
        }

        @Override
        public String toString() {
            return this.objIdentifier;
        }
    }

    public enum InvoiceTypeCode {

        SCHEDULE_AMOUNT_INVOICED('Y'),
        SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING('B'),
        SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE('A'),
        PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE('L'),
        EMPTY_CHAR(CommonConstants.BLANK_CHAR);

        private final Character type;

        private InvoiceTypeCode(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }

        public static InvoiceTypeCode getEnumValue(Character invoiceTypeCd) {
            for (InvoiceTypeCode invoiceType : values()) {
                if (invoiceType.type.equals(invoiceTypeCd)) {
                    return invoiceType;
                }

            }
            return EMPTY_CHAR;
        }
    }

    public enum CashOverPayIndicators {

        SPREAD_OVER_INSTALLMENTS('S'), SATISFY_OLDEST_DUE_FIRST('C'), SUSPEND('D');

        private final char type;

        private CashOverPayIndicators(char type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum BilPolicyStatusCode {
        CANCELLED_SUBJECT_TO_AUDIT('A'),
        SUSPENDBILLING_FOLLOWUP('B'),
        CANCELLED('C'),
        CANCELLED_FOR_REISSUED('D'),
        FLAT_CANCELLATION('F'),
        FLATCANCELLED_FOR_REISSUED('G'),
        SUSPEND_PRECOLLECTIONS('H'),
        SUSPEND_COLLECTIONS('I'),
        SUSPEND_FOLLOWUP('L'),
        PENDING_CANCELLATION_NONSUFFICIENTFUNDS('N'),
        OPEN('O'),
        PENDING_CANCELLATION('P'),
        QUOTE('Q'),
        SUSPEND_BILLING('S'),
        TRANSFERRED('T'),
        CANCELLED_FOR_REWRITE('W'),
        CLOSED('X'),
        PENDING_TRANSFER('Z'),
        PENDING_TRANSFER_APPLYPAYMENTS('1'),
        PENDING_TRANSFER_HOLDPAYMENTS('2'),
        PENDINGCANCELLATION_NORESPONSE('E'),
        BILLACCOUNT_PENDINGCANCELLATION_MAX('M'),
        BILL_FOLLOWUP_SUSPEND('V'),
        REISSUE_NEWTERM('9');

        private final Character type;

        private BilPolicyStatusCode(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }

        public static BilPolicyStatusCode getEnumValue(Character statusCode) {
            for (BilPolicyStatusCode bilPolicyStatusCode : values()) {
                if (bilPolicyStatusCode.type.equals(statusCode)) {
                    return bilPolicyStatusCode;
                }
            }
            return null;
        }
    }

    public enum PolicyIssueIndicators {
        CANCELLED_REISSUE('K'),
        CANCELLED_REWRITE('W'),
        NOT_ISSUED('O'),
        ISSUED_NOT_EDIT('Z'),
        AGGREMENT_ADD('A'),
        VOID('V'),
        ISSUED_EDIT('Y'),
        NOT_ISSUED_BILLING_NOT_EDIT('N');

        private final Character type;

        private PolicyIssueIndicators(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum EftNoticeIndicator {
        DEBIT_NOTICE('Y'), DEBIT_CREDIT_NOTICE('B'), NO_NOTICE('N');

        private final Character type;

        private EftNoticeIndicator(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum ChargeTypeCodes {
        SERVICE_CHARGE('S'), PENALTY_CHARGE('P'), LATE_CHARGE('L'), DOWN_PAYMENT_FEE('D');

        private final Character type;

        private ChargeTypeCodes(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum WipAcitivityId {
        PAY_REVERSE_PROCESS("PAYREVPRC"),
        NSF_EXCEED("NSFEXCEED"),
        DISBUSRE_REBILL("DISBREBILL"),
        DISBURSE_AUTHENTICATION("DISBAUTH"),
        AUTO_DISBURSE_REBILL("AUTODISBREBILL"),
        GOODPSTMRK("GOODPSTMRK"),
        CANCELPCN("CANCELPCN"),
        CANCELPCN2("CANCELPCN2"),
        PCNEXCEED("PCNEXCEED"),
        RESCIND("RESCINDAFTCNCREQ");

        private final String type;

        private WipAcitivityId(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum WipObjectCode {
        BILL_ACCOUNT("Billing Accounts"),
        BILL_POLICY("Billing Policy"),
        GROUP_ACCOUNT("Billing Group Account"),
        UNIDENTIFIED_CASH("Unidentified cash");

        private final String type;

        private WipObjectCode(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum BilRules {
        AGENCY_SWEEP_PROCESSING("AGSW"),
        ONE_TIME_CREDIT_CARD("1XCC"),
        UNIDENTIFIED_CASH_HISTORY("UCHS"),
        ACCOUNT_BALANCE("BABL"),
        AUTOGENERATE_DISBURSEMENT_REBILL("ADRB"),
        DOWN_PAYMENT_CASH_ALLOCATION("DPCA"),
        BUSINESS_GROUP("BGRP");

        private final String type;

        private BilRules(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum WebAutomatedIndicator {
        AUTHORIZED('Y'), NOT_AUTHORIZED('N');

        private final Character type;

        private WebAutomatedIndicator(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum CreditIndicator {
        CANCELLATION_CREDIT('C'),
        CANCELLATION_SUBJECT_TO_AUDIT('A'),
        CREDIT('Y'),
        CANCELLATION_FOR_REWRITE('R'),
        OPERATOR('N'),
        EXCESS_CREDIT_CASH('P'),
        COMMISSION_PAYMENT('M'),
        DISBURSED_CASH('D'),
        UNIDENTIFIED_CASH('B');

        private final Character type;

        private CreditIndicator(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum DedudctionDate {
        NOT_EXCLUDE(BLANK_CHAR), EXCLUDE('X');

        private final Character type;

        private DedudctionDate(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum FullPayServiceChargeWriteOff {
        ACTUAL_INVOICE('A'), SCHEDULE_INVOICE('B'), USER_EXIT('C'), DISABLED('D');

        private final Character type;

        private FullPayServiceChargeWriteOff(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum BilAmountsEftIndicator {
        CHANGE_AUDIT_FREQUENCY('A'),
        AUDIT_LEVEL_CHANGE('B'),
        INTERIM_AUDIT('C'),
        FINAL_AUDIT('D'),
        AMENDED_INTERIM_AUDIT('E'),
        REVERSE_AUDIT_OPERATOR('G'),
        NEW_BUSINESS('N'),
        NEWBUSINESS('M'),
        RENEWAL('R'),
        RENEWAL_UNDER('U'),
        RENEWALS('W'),
        REISSUE_NEW_TERM('9'),
        CANCELLATION_F('F'),
        CANCELLATION_P('P'),
        CANCELLATION_S('S'),
        REINSTATEMENT_K('K'),
        REINSTATEMENT_L('L'),
        REINSTATEMENT_O('O'),
        REINSTATEMENT_T('T'),
        REINSTATEMENT_Z('Z'),
        REINSTATEMENT_Y('Y'),
        FLAT_CANCEL_FOR_REWRITE('7');

        private final Character type;

        private BilAmountsEftIndicator(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum CollectionLayout {
        COLLECTION_FULL('F'), COLLECTION_PARTIAL('P');

        private final Character type;

        private CollectionLayout(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum BillCollections {
        COLLECTION('H'), PRE_COLLECTION('P');

        private final Character type;

        private BillCollections(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum EventIssueSystemCode {
        DEFAULTALL("99"), POINTIN("PT"), UWS("UW");

        private final String type;

        private EventIssueSystemCode(String type) {
            this.type = type;
        }

        public String getValue() {
            return this.type;
        }
    }

    public enum EventCode {
        EVENT_PREM_PYMT("PMT"), EVENT_BILL_CHG("CHG"), EVENT_RECN_REA("RCN");

        private final String type;

        private EventCode(String type) {
            this.type = type;
        }

        public String getValue() {
            return this.type;
        }
    }

    public enum EventKeyTypeCode {
        UNIDENTIFIED_CASH("UI"), BILLACCOUNT("BA"), THIRDPARTYACCOUNT("TP"), POLICY("P"), AGENTACCOUNT("AG");

        private final String type;

        private EventKeyTypeCode(String type) {
            this.type = type;
        }

        public String getValue() {
            return this.type;
        }
    }

    public enum BilTechKeyTypeCode {
        BILL_ACCOUNT('A'), THIRD_PARTY('T'), UNIDENTIFIED('U');

        private final Character type;

        private BilTechKeyTypeCode(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum BillingEftTransactionStatus {
        APPLIED('A'),
        REJECTED('X'),
        REJECTED_DIRECTINVOICED('D'),
        REJECTED_REPRESENTED('R'),
        PENDING_ASSUMEDPAYMENT('P');

        private final Character status;

        private BillingEftTransactionStatus(Character status) {
            this.status = status;
        }

        public Character getValue() {
            return status;
        }
    }

    public enum CheckProductionMethod {
        SYSTEM_COMBINED_PAY('C'),
        ELECTRONIC('E'),
        WIRE_TRANSFER('W'),
        MANUAL('M'),
        SYSTEM_GENERATED('S'),
        WIRE_TRANSFER_COMBINED('F');

        private final char type;

        private CheckProductionMethod(char type) {
            this.type = type;
        }

        public char getValue() {
            return type;
        }
    }

    public enum AccountStatus {
        ACTIVE('A'),
        INACTIVE('I'),
        PENDING('P'),
        OLD('O'),
        SUSPEND_BILLING_FOLLOW_UP('B'),
        SUSPEND_BILLING('S'),
        SUSPEND_FOLLOW_UP('F'),
        DISBURSEMENT('D'),
    	COLLECTIONS('C'),
    	PRE_COLLECTION('P');

        private final Character accStatus;

        private AccountStatus(Character accStatus) {
            this.accStatus = accStatus;
        }

        public Character toChar() {
            return this.accStatus;
        }
    }

    public enum StatementReconciliationIndicator {
        COMPLETE(CommonConstants.BLANK_CHAR),
        ACCEPTED('C'),
        INCOMPLETE('I'),
        CREDIT_TAKEN('T'),
        REOPEN('O'),
        WORKING('W'),
        SAVED('S'),
        PENDING('P'),
        PENDING_REPROCESS('U'),
        RECONCILED('R');

        private final Character type;

        private StatementReconciliationIndicator(Character type) {

            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum ActivityType {
        BILLING_INVOICE("AIV"),
        BILLING_RESUME("ARS"),
        BILLING_REVIEW("ARV"),
        DISBURSE_RESUME("DRS"),
        POLICY_RESUME("PRS"),
        POLICY_EQUITY_DATE("PED"),
        ACCOUNT_EFT_NOTICE("AEF"),
        ACCOUNT_PRINT_SCHEDULE("APS"),
        EFT_PAYMENT_ADJUSTMENT("EPA"),
        EFT_REPRINT("ERP"),
        RENEWAL_INFORCE("RIF"),
        ACCOUNT_DISBURSEMENT("ADB"),
        ONETIME_ACH("ADA"),
        DISBURSE_EFT_CREDIT("ADC"),
        DISBURSE_CHECK_DRAFT("ADD"),
        DISBURSE_EFT("ADE"),
        DISBURSE_CREDIT("ADO"),
        DISBURSE_ACH("ADP"),
        POLICY_CANCELLED_CASH_APPLIED("PCC"),
        CIV("CIV"),
        BIV("BIV"),
        EIV("EIV"),
        ECC("ECC"),
        EBC("EBC"),
        EEC("EEC"),
        LAST_DAY_REINSTATE("LDR"),
        CANCEL_GRACE_DATE("CGD"),
        PCN_NO_RESPONSE_RENEWAL("RPC");

        private final String activityTyp;

        private ActivityType(String activityTyp) {
            this.activityTyp = activityTyp;
        }

        public String getValue() {
            return activityTyp;
        }

        public static ActivityType getEnumValue(String activityTypeCode) {
            for (ActivityType activityType : values()) {
                if (activityType.activityTyp.equals(activityTypeCode)) {
                    return activityType;
                }
            }
            return null;
        }
    }

    public enum ChargeType {
        LATECHARGES('L'),
        PENALTYCHARGES('P'),
        SERVICECHARGES('S'),
        DOWNPAYMENTCHARGES('D'),
        EMPTY_CHAR(CommonConstants.BLANK_CHAR);

        private final Character type;

        private ChargeType(Character type) {
            this.type = type;
        }

        public Character getChargeType() {
            return type;
        }

        public boolean hasValue(Character value) {
            return value != null && type.equals(value);
        }

        public static ChargeType getEnumValue(Character chargetyp) {
            for (ChargeType chargeType : values()) {
                if (chargeType.type.equals(chargetyp)) {
                    return chargeType;
                }

            }
            return EMPTY_CHAR;
        }
    }

    public enum EftStatusCode {
        ACTIVE('A'), PENDING('P'), OLD('O'), INACTIVE('I');

        private final Character status;

        private EftStatusCode(Character status) {
            this.status = status;
        }

        public Character getValue() {
            return status;
        }
    }

    public enum RuleId {

        BAD_MONEY_TRANSFER("BMTR"),
        THIRD_PARTY_STATIC_DATES("TPSD"),
        AUTOMATIC_DISBURSEMENT_RE_BILL("ADRB"),
        DOWN_PAYMENT_CASH_ALLOCATION("DPCA"),
        COLLECTIONS_RULE("COLL"),
        RESUSPEND_RECEIPT_AFTER_TRANSFER("RTRN");

        private final String ruleIdentifier;

        private RuleId(String ruleIdentifier) {
            this.ruleIdentifier = ruleIdentifier;
        }

        public boolean hasValue(String value) {
            return ruleIdentifier.equals(value);
        }

        @Override
        public String toString() {
            return this.ruleIdentifier;
        }
    }

    public enum BilAcyDesCode {
        AGENT_STATEMENT_BALANCED("ASB"),
        BILL_CLASS_CHANGE("BCC"),
        ACCOUNT_BILL_TYPE_CHANGE("BTA"),
        BILL_TYPE_CHANGE("BTC"),
        BILL_ACCOUNT_TRANSFER("BTT"),
        MANUAL_PAYMENT_WRITEOFF("CM"),
        COLLECTION_METHOD_CHANGE("CMC"),
        REVERSE_MANUAL_PAYMENT_WRITEOFF("CMR"),
        CREDIT_CASH("CR"),
        CASH_CREDIT("CS"),
        COMPANY_STATEMENT("CST"),
        DISBURSE_MANUAL("DBR"),
        REVERSE_MANUAL_DISBURSE("DMR"),
        STATEMENT_DUE_DATE_CHANGE("IDC"),
        BALANCE_WRITEOFF_OVERPAY("OPY"),
        COLLECTION_PLAN_CHANGE("PLN"),
        PAYOR_NAMEADDRESS_CHANGE("PNC"),
        REVERSE_PAYMENT_COMPANY_MISTAKE("RC"),
        REFERENCE_DATE_CHANGE("RDC"),
        REVERSE_PAYMENT_NSF("RN"),
        REVERSE_PAYMENT_PROTESTED("RP"),
        REPORTING_LEVEL_CHANGE("RPL"),
        START_DATE_CHANGE("SDC"),
        STATEMENT("STT"),
        BALANCE_UNDERPAY("UPY"),
        REQUEST_AUTO_REINSTATEMENT("REI"),
        NO_RESCIND_DUE_POSTMARK_DATE("NRD"),
        NO_AUTO_REINSTATEMENT("NRE"),
        CORRECTED_INVOICE("CIA"),
        RENEWAL_INFORCE("RIF"),
        CREDIT_CARD_TAPE("CCT"),
        AUTO_REISSUE_NEW_TERM("RDR"),
        REQUEST_NONPAY_CANCEL("C"),
        RESTART_PCN("RCN"),
        PND("PND"),
        REISSUE_TRANSACTION("RNT");

        private final String type;

        private BilAcyDesCode(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }

        public static BilAcyDesCode getEnumKey(String typeCd) {
            for (BilAcyDesCode activityDescriptionCode : values()) {
                if (activityDescriptionCode.type.equals(typeCd)) {
                    return activityDescriptionCode;
                }
            }
            return null;
        }
    }

    public enum ReinstatePrintCode {
        LDR_DATE_REACHED('L'), CASH_RECEIVED('R'), BATCH('B'), SPACE(' '), BILSUMMARY('A');

        private final Character type;

        private ReinstatePrintCode(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum PcnReasonType {
        FLAT_REINSTATEMENT_LAPSE("RFL"),
        PRORATA_REINSTATEMENT_WITH_LAPSE("RPL"),
        SHORTRATE_REINSTATEMENT_LAPSE("RSL"),
        KFR("KFR"),
        OPR("OPR"),
        SHORTRATE_REINSTATEMENT("RSR");

        private final String type;

        private PcnReasonType(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    public enum ReinstatementTypeCode {
        FLAT('F'), LAPSE('L'), REISSUE_NEWTERM('N'), REINSTATE_LAPSE_REISSUE_NEWTERM('A'), SPACE(' ');

        private final Character type;

        private ReinstatementTypeCode(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum AmountProcessIndicator {
        ROWPROCESSED(
                'Y'),
        ROWNOTPROCESSED('N'),
        ROWPROCESSD_NOTSCHEDULED('P'),
        VOIDTERM('V'),
        INTERMEDIATEPROCESSING('H');

        private final Character indicator;

        private AmountProcessIndicator(Character indicator) {
            this.indicator = indicator;
        }

        public Character getValue() {
            return indicator;
        }
    }

    public enum ReconciliationProcessType {
        ACKNOWLEDGE("ACK"),
        CHANGE("CHG"),
        CHANGE_WITH_HOLD("CHH"),
        DEFER("DFR"),
        EXCLUDE("EXC"),
        WRITEOFF("WRO"),
        WAIVE("WAV"),
        UNACKNOWLEDGE("CAN"),
        PENDING_EXCPETION("PEX"),
        ADD_RECEIVABLE("ADD"),
        IMPORT("IMP"),
        DEFER_EXCLUSION("DEX"),
        WAIVE_EXCLUSION("WEX"),
        EXPORT("EXP"),
        TAKE_CREDIT("TCR"),
        SUBMIT("SUB"),
        ADD("Add Receivable"),
        CLOSED_BY_CREDIT("CRD"),
        PAID_DEBIT_ITEM("DBT"),
        CREDIT_TO_CASH("PMT"),
        ACKNOWLEDGE_ALL("ACA"),
        UNACKNOWLEDGE_ALL("CAA"),
        EMPTY_STRING(BLANK_STRING),
        EXCLUDE_REMAINING_ALL("EXR");

        private final String typeCd;

        private ReconciliationProcessType(String typeCd) {
            this.typeCd = typeCd;
        }

        public String getValue() {
            return typeCd;
        }

        public static ReconciliationProcessType getEnumKey(String typeCd) {
            for (ReconciliationProcessType reconGroupProcessType : values()) {
                if (reconGroupProcessType.typeCd.equals(typeCd)) {
                    return reconGroupProcessType;
                }
            }
            return EMPTY_STRING;
        }
    }

    public enum StatementLevelType {
        ACCOUNT_LEVEL('T'), DETAIL_LEVEL('D');

        private final Character type;

        private StatementLevelType(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }
    }

    public enum ReconGroupProcessType {
        ACKNOWLEDGE("ACK"),
        CHANGE("CHG"),
        CHANGE_WITH_HOLD("CHH"),
        DEFER("DFR"),
        EXCLUDE("EXC"),
        WRITEOFF("WRO"),
        WAIVE("WAV"),
        UNACKNOWLEDGE("CAN"),
        PENDING_EXCPETION("PEX"),
        ADD_RECEIVABLE("ADD"),
        IMPORT("IMP"),
        DEFER_EXCLUSION("DEX"),
        WAIVE_EXCLUSION("WEX"),
        EXPORT("EXP"),
        TAKE_CREDIT("TCR"),
        SUBMIT("SUB"),
        ADD("Add Receivable"),
        EMPTY_STRING(BLANK_STRING);

        private final String typeCd;

        private ReconGroupProcessType(String typeCd) {
            this.typeCd = typeCd;
        }

        public String getValue() {
            return typeCd;
        }

        public static ReconGroupProcessType getEnumKey(String typeCd) {
            for (ReconGroupProcessType reconGroupProcessType : values()) {
                if (reconGroupProcessType.typeCd.equals(typeCd)) {
                    return reconGroupProcessType;
                }
            }
            return EMPTY_STRING;
        }
    }

    public enum BilFunctionCtl {
        BANK("BNK"), BUSINESSGROUP("BGP");

        private final String billFunctionCtl;

        private BilFunctionCtl(String billFunctionCtl) {
            this.billFunctionCtl = billFunctionCtl;
        }

        public boolean hasValue(String value) {
            return billFunctionCtl.equals(value);
        }

        @Override
        public String toString() {
            return this.billFunctionCtl;
        }

    }

    public enum Status {
        SUSPENDED("Suspended"),
        DISBURSED("Disbursed"),
        REVERSED("Reversed"),
        WRITE_OFF("Overpay Write-Off"),
        EMPTY_STRING(BLANK_STRING);

        private final String type;

        private Status(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }

        public static Status getEnumKey(String type) {
            for (Status status : values()) {
                if (status.type.equals(type)) {
                    return status;
                }
            }
            return EMPTY_STRING;
        }
    }

    public enum PaymentProviderType {
        PAYMENTUS("paymentus"), AUTHORIZE_NET("authorize.net");

        private final String type;

        private PaymentProviderType(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }
}
