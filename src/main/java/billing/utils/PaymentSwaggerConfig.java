package billing.utils;

import org.springframework.context.annotation.Configuration;

import core.configurations.SwaggerConfig;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class PaymentSwaggerConfig extends SwaggerConfig {

    @Override
    public AlternateTypeRule[] alternateTypeRules() {
        return new AlternateTypeRule[0];
    }

    @Override
    public String title() {
        return "Enterprise Payments REST API";
    }

    @Override
    public String description() {
        return "This API provides restful implementation of enterprise payments module of Enterprise Billing.";
    }

}