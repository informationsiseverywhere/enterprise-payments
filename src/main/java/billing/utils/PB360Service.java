package billing.utils;

import java.time.ZonedDateTime;

import billing.model.AccountAdjustDates;
import billing.model.ImmediateActivity;
import billing.model.RescindNotice;

public interface PB360Service {

    public void patchFutureDate(String accountId);

    public AccountAdjustDates patchChargeDate(String accountId, char addChargeInd, boolean quoteInd);

    public void pathRecoveryDate(String accountId, String policyId, String polEffDate, char dateResetSwitch,
            char supportPlanSwitch);

    public void postGeneratePrintNoticeRequest(String accountId, String programId, String payorClientId, String fromId,
            String additionalKey, String businessTransaction);

    public void postAutomaticPolicyPlanChange(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            String programCalling, boolean isPolPlanChnage, char eftReversalIndicator);

    public void updateDisbursementStatus(String callingProgramName, char disburStatusCode, String disburTypeCode,
            String techDisburId, String userDisburId, ZonedDateTime recoveryDate, String userId, String functionValue);

    public void checkPMStatus();

    public void scheduleImmediateActivity(ImmediateActivity immediateActivity);
    
    public void postRescindNoticeRequest(String accountId, RescindNotice rescindNotice);

}
