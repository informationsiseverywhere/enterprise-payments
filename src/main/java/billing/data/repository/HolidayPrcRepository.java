package billing.data.repository;

import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.HolidayPrc;
import billing.data.entity.id.HolidayPrcId;

public interface HolidayPrcRepository extends JpaRepository<HolidayPrc, HolidayPrcId> {

    @Query("select b from HolidayPrc b where b.holidayPrcId.holidayDt = :holidayDt")
    public HolidayPrc findHolidayRow(@Param("holidayDt") ZonedDateTime holidayDt);
}
