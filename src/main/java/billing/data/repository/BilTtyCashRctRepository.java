package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilTtyCashRct;
import billing.data.entity.id.BilTtyCashRctId;

public interface BilTtyCashRctRepository extends CrudRepository<BilTtyCashRct, BilTtyCashRctId> {

    @Query(value = "SELECT MAX(b.bilTtyCashRctId.bilDtbSeqNbr) FROM BilTtyCashRct b WHERE b.bilTtyCashRctId.bilThirdPartyId = :bilThirdPartyId and "
            + "b.bilTtyCashRctId.bilDtbDt = :bilDtbDt")
    public Short getMaxBilDtbSequenceNumber(@Param("bilThirdPartyId") String bilThirdPartyId,
            @Param("bilDtbDt") ZonedDateTime bilDtbDt);

    @Query(value = "select b from BilTtyCashRct b where b.bilEntryDt = :bilEntryDt and b.bilEntryNbr = :bilEntryNbr and b.userId = :userId"
            + " and b.bilEntrySeqNbr = :entrySequenceNumber and b.bilToFroTrfNbr = :bilToFroTrfNbr "
            + " order by b.bilTtyCashRctId.bilDtbDt asc, b.bilTtyCashRctId.bilDtbSeqNbr asc ")
    public List<BilTtyCashRct> getTtyCashReciptRow(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNbr") String bilEntryNbr, @Param("userId") String userId,
            @Param("entrySequenceNumber") short entrySequenceNumber, @Param("bilToFroTrfNbr") String bilToFroTrfNbr,
            Pageable pageable);

    @Query(value = "select b from BilTtyCashRct b where b.bilEntryDt = :bilEntryDt and b.bilEntryNbr = :bilEntryNbr and b.userId = :userId"
            + " and b.bilEntrySeqNbr = :entrySequenceNumber ")
    public BilTtyCashRct findBilTtyCashReceipt(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNbr") String bilEntryNbr, @Param("userId") String userId,
            @Param("entrySequenceNumber") short entrySequenceNumber);

}
