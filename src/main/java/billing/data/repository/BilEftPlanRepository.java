package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilEftPlan;

public interface BilEftPlanRepository extends JpaRepository<BilEftPlan, String> {

    @Query(value = "select b.bepPlanNm from BilEftPlan b where b.bilFileTypeCd = :bilFileTypeCd ")
    public List<String> findByFileTypeCode(@Param("bilFileTypeCd") short bilFileTypeCd);

    @Query(value = "select b.bepPlanNm from BilEftPlan b where b.bilFileTypeCd = :bilFileTypeCd and b.bepNoticeInd = :bepNoticeInd and b.bepRpsMth = :bepRpsMth")
    public List<String> findByFileTypeCode(@Param("bilFileTypeCd") short bilFileTypeCd,
            @Param("bepNoticeInd") char bepNoticeInd, @Param("bepRpsMth") String bepRpsMth);

    @Query(value = "select b.bepPlanNm from BilEftPlan b where b.bilFileTypeCd not in :bilFileTypeCdList and b.bepNoticeInd = :bepNoticeInd and b.bepRpsMth = :bepRpsMth")
    public List<String> findByOtherFileTypeCode(@Param("bilFileTypeCdList") List<Short> bilFileTypeCdList,
            @Param("bepNoticeInd") char bepNoticeInd, @Param("bepRpsMth") String bepRpsMth);

    @Query(value = "select b from BilEftPlan b where b.bepPlanNm = :bepPlanNm and b.bilFileTypeCd = :bilFileTypeCd ")
    public List<BilEftPlan> findByCollectionPlan(@Param("bepPlanNm") String bepPlanNm,
            @Param("bilFileTypeCd") short bilFileTypeCd);

    @Query(value = "select b.bepPlanNm from BilEftPlan b where b.bilFileTypeCd = :bilFileTypeCd and b.bepNoticeInd = :bepNoticeInd and b.bepRpsMth = :bepRpsMth")
    public List<String> findPlanNamesByFileTypeAndNoticeAndReportMethod(@Param("bilFileTypeCd") short bilFileTypeCd,
            @Param("bepNoticeInd") char bepNoticeInd, @Param("bepRpsMth") String bepRpsMth);

    @Query(value = "select b.bepPlanNm from BilEftPlan b where b.bilFileTypeCd = :bilFileTypeCd ")
    public List<String> findPlanNamesByFileTypeCode(@Param("bilFileTypeCd") short bilFileTypeCd);

}
