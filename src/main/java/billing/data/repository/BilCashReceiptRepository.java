package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilCashReceipt;
import billing.data.entity.id.BilCashReceiptId;

public interface BilCashReceiptRepository extends JpaRepository<BilCashReceipt, BilCashReceiptId> {

    @Query(value = "SELECT MAX(b.bilCashReceiptId.dtbSequenceNbr) FROM BilCashReceipt b WHERE b.bilCashReceiptId.accountId = :bilAccountId "
            + "and b.bilCashReceiptId.bilDtbDate = :bilDtbDt")
    public Short getMaxBilDtbSeqNbr(@Param("bilAccountId") String bilAccountId,
            @Param("bilDtbDt") ZonedDateTime bilDtbDt);

    @Query(value = "select b from BilCashReceipt b where b.entryDate = :bilEntryDt and b.entryNumber = :bilEntryNbr and b.userId = :userId"
            + " and b.entrySequenceNumber = :entrySequenceNumber and b.toFroTransferNumber = :toFroTransferNumber "
            + " order by b.bilCashReceiptId.bilDtbDate asc, b.bilCashReceiptId.dtbSequenceNbr asc ")
    public List<BilCashReceipt> getBilCashReciptRow(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNbr") String bilEntryNbr, @Param("userId") String userId,
            @Param("entrySequenceNumber") short entrySequenceNumber,
            @Param("toFroTransferNumber") String toFroTransferNumber, Pageable pageable);

    @Query("select b from BilCashReceipt b where b.bilCashReceiptId.accountId = :bilAccountId and b.postMarkDate <= :postMarkDate "
            + "and b.bilCashReceiptId.bilDtbDate >= :bilDtbDate")
    public List<BilCashReceipt> getAvailableCashReceipt(@Param("bilAccountId") String bilAccountId,
            @Param("postMarkDate") ZonedDateTime postMarkDate, @Param("bilDtbDate") ZonedDateTime bilDtbDate);

    @Query(value = "select b from BilCashReceipt b where b.bilCashReceiptId.accountId = :bilAccountId "
            + "and b.receiptTypeCd in :receiptTypeCdList")
    public List<BilCashReceipt> getCreditCommissionCash(@Param("bilAccountId") String bilAccountId,
            @Param("receiptTypeCdList") List<String> receiptTypeCdList);

    @Query(value = "select b from BilCashReceipt b where b.bilCashReceiptId.accountId = :bilAccountId and b.thirdPartyCashIdentifier <> :thirdPartyCashIdentifier")
    public List<BilCashReceipt> fetchWriteOffReceipt(@Param("bilAccountId") String bilAccountId,
            @Param("thirdPartyCashIdentifier") char thirdPartyCashIdentifier);

    @Query(value = "SELECT MAX(b.entrySequenceNumber) FROM BilCashReceipt b WHERE b.bilCashReceiptId.accountId = :bilAccountId "
            + "and b.entryDate = :bilEntryDt and b.entryNumber = :bilEntryNbr and b.userId = :userId and b.receiptTypeCd = :receiptTypeCd ")
    public Short getMaxEntrySeqNbr(@Param("bilAccountId") String bilAccountId,
            @Param("bilEntryDt") ZonedDateTime bilEntryDt, @Param("bilEntryNbr") String bilEntryNbr,
            @Param("userId") String userId, @Param("receiptTypeCd") String receiptTypeCd);

    @Query(value = "select b from BilCashReceipt b where b.bilCashReceiptId.accountId = :accountId and SUBSTRING( b.receiptComment,1,2) <> :receiptComment"
            + " order by b.bilCashReceiptId.bilDtbDate desc, b.bilCashReceiptId.dtbSequenceNbr desc")
    public List<BilCashReceipt> getCreditCashRow(@Param("accountId") String accountId,
            @Param("receiptComment") String receiptComment);

    @Query(value = "select b from BilCashReceipt b where b.bilCashReceiptId.accountId = :accountId and b.cashEntryMethodCd in :cashEntryMethodCd "
            + " order by b.bilCashReceiptId.bilDtbDate desc, b.entrySequenceNumber desc ")
    public List<BilCashReceipt> getMaxBilDtbDateRow(@Param("accountId") String accountId,
            @Param("cashEntryMethodCd") List<Character> cashEntryMethodCd);

    @Query(value = "Select Max(b.entrySequenceNumber) from BilCashReceipt b where b.bilCashReceiptId.accountId = :bilAccountId "
            + "and b.entryNumber = :bilEntryNbr and b.bilCashReceiptId.bilDtbDate = :bilDtbDate")
    public Short getMaxEntrySeqNbrByEntryNumber(@Param("bilAccountId") String bilAccountId,
            @Param("bilEntryNbr") String bilEntryNbr, @Param("bilDtbDate") ZonedDateTime bilDtbDate);

    @Query("select b from BilCashReceipt b where b.bilCashReceiptId.accountId = :bilAccountId and ((b.bilCashReceiptId.bilDtbDate = :bilDtbDate "
            + " and b.bilCashReceiptId.dtbSequenceNbr > :dtbSequenceNbr) or b.bilCashReceiptId.bilDtbDate > :bilDtbDate) and "
            + " b.bilCashReceiptId.bilDtbDate >= :bilDtbDate")
    public List<BilCashReceipt> getPaymentReceiptsByDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate, @Param("dtbSequenceNbr") short dtbSequenceNbr);

    @Query(value = "select b from BilCashReceipt b where b.bilCashReceiptId.accountId = :accountId and b.bilCashReceiptId.bilDtbDate = :bilDtbDt"
            + " order by b.bilCashReceiptId.dtbSequenceNbr desc")
    public List<BilCashReceipt> getMaxPostMarkDateRow(@Param("accountId") String accountId,
            @Param("bilDtbDt") ZonedDateTime bilDtbDt);

    @Query(value = "select b from BilCashReceipt b where b.entryDate = :bilEntryDt and b.entryNumber = :bilEntryNbr and b.userId = :userId"
            + " and b.entrySequenceNumber = :entrySequenceNumber ")
    public BilCashReceipt findBilCashReciptRow(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNbr") String bilEntryNbr, @Param("userId") String userId,
            @Param("entrySequenceNumber") short entrySequenceNumber);

}