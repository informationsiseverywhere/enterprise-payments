package billing.data.repository;

import org.springframework.data.repository.CrudRepository;

import billing.data.entity.BilObjectCfg;
import billing.data.entity.id.BilObjectCfgId;

public interface BilObjectCfgRepository extends CrudRepository<BilObjectCfg, BilObjectCfgId> {

}
