package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilPolicy;
import billing.data.entity.id.BilPolicyId;

public interface BilPolicyRepository extends JpaRepository<BilPolicy, BilPolicyId> {

    @Query("Select b from BilPolicy b WHERE b.polNbr = :polNbr and b.bilIssueInd IN :bilIssueIndList and "
            + "(b.polSymbolCd = :polSymbolCd or :polSymbolCd = '   ')")
    public List<BilPolicy> findPolicyIdByPolicyNumber(@Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilIssueIndList") List<Character> bilIssueIndList);
    
    @Query("Select b from BilPolicy b WHERE b.bilAccountNbr = :bilAccountNbr and b.polNbr = :polNbr and b.bilIssueInd IN :bilIssueIndList and "
            + "(b.polSymbolCd = :polSymbolCd or :polSymbolCd = '   ')")
    public List<BilPolicy> findAccountAndPolicyNumber(@Param("bilAccountNbr") String bilAccountNbr, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilIssueIndList") List<Character> bilIssueIndList);

    @Query("Select MAX(b.bilPolicyId.policyId) from BilPolicy b WHERE b.bilPolicyId.bilAccountId = :bilAccountId "
            + " and b.polNbr = :polNbr and b.bilIssueInd not IN :bilIssueIndList and "
            + " b.polSymbolCd = :polSymbolCd ")
    public String findMaxPolicyId(@Param("bilAccountId") String bilAccountId, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilIssueIndList") List<Character> bilIssueIndList);

    @Query("Select MAX(b.bilPolicyId.policyId) from BilPolicy b WHERE b.bilPolicyId.bilAccountId = :bilAccountId "
            + " and b.polNbr = :polNbr and b.bilIssueInd not IN :bilIssueIndList ")
    public String findMaxPolicyId1(@Param("bilAccountId") String bilAccountId, @Param("polNbr") String polNbr,
            @Param("bilIssueIndList") List<Character> bilIssueIndList);

    @Query("Select b from BilPolicy b WHERE b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilPolicyId.bilAccountId = :bilAccountId"
            + " and b.bilIssueInd IN :bilIssueIndList")
    public List<BilPolicy> findCivPolicyId(@Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAccountId") String bilAccountId, @Param("bilIssueIndList") List<Character> bilIssueIndList);

    @Query("Select DISTINCT b.bilPlanCd from BilPolicy b WHERE b.bilPolicyId.bilAccountId = :bilAccountId and b.polNbr = :polNbr "
            + "and b.bilIssueInd IN :bilIssueIndList and (b.polSymbolCd = :polSymbolCd or :polSymbolCd = :blankSymbol)")
    public List<String> findBilPlanCode(@Param("bilAccountId") String bilAccountId, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilIssueIndList") List<Character> bilIssueIndList,
            @Param("blankSymbol") String blankSymbol);

    @Query("Select b.bilPolicyId.policyId from BilPolicy b WHERE b.bilPolicyId.bilAccountId = :bilAccountId and b.polNbr = :polNbr "
            + "and b.bilIssueInd IN :bilIssueIndList and (b.polSymbolCd = :polSymbolCd or :polSymbolCd = :blankSymbol)")
    public List<String> findPolicyData(@Param("bilAccountId") String bilAccountId, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilIssueIndList") List<Character> bilIssueIndList,
            @Param("blankSymbol") String blankSymbol);

    @Query("Select b from BilPolicy b where b.bilPolicyId.policyId = :policyId and b.bilPolicyId.bilAccountId = :bilAccountId and "
            + " b.polNbr = :polNbr and (b.polSymbolCd = :polSymbolCd or :polSymbolCd = :polSymbolCd1)")
    public BilPolicy findPolicyByPolicyNumber(@Param("policyId") String policyId,
            @Param("bilAccountId") String bilAccountId, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("polSymbolCd1") String polSymbolCd1);

    @Query("Select b.bilPolicyId.policyId from BilPolicy b WHERE b.bilPolicyId.policyId <> :policyId and b.bilPolicyId.bilAccountId = :bilAccountId "
            + "and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilIssueInd = :bilIssueInd")
    public List<String> findReissuePolicyId(@Param("policyId") String policyId,
            @Param("bilAccountId") String bilAccountId, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilIssueInd") char bilIssueInd);

    @Query("Select b from BilPolicy b where b.bilPolicyId.bilAccountId = :bilAccountId and b.polNbr = :polNbr "
            + "and b.bilIssueInd not in :bilIssueIndList and (b.polSymbolCd = :polSymbolCd or :polSymbolCd = :blankSymbol)")
    public List<BilPolicy> findBilPolicyId(@Param("bilAccountId") String bilAccountId, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilIssueIndList") List<Character> bilIssueIndList,
            @Param("blankSymbol") String blankSymbol);

    @Query("Select b from BilPolicy b where b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd "
            + "and b.bilIssueInd in :bilIssueIndList ")
    public List<BilPolicy> findBilPolicyByPolicyNumber(@Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilIssueIndList") List<Character> bilIssueIndList);

    @Query("Select b from BilPolicy b where b.polNbr = :policyNumber and b.polSymbolCd = :policySymbolCode")
    public List<BilPolicy> findBilPolicyByPolicyNumber(@Param("policyNumber") String policyNumber,
            @Param("policySymbolCode") String policySymbolCode);

    @Query("Select b from BilPolicy b WHERE b.bilPolicyId.bilAccountId = :bilAccountId and b.bilPolicyId.policyId <> :policyId and b.polNbr = :polNbr "
            + " and b.polSymbolCd = :polSymbolCd and b.bilIssueInd = :bilIssueInd")
    public List<BilPolicy> getRntBalance(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilIssueInd") char bilIssueInd);

    @Query("Select b.bilPolicyId.policyId from BilPolicy b where b.bilPolicyId.bilAccountId = :bilAccountId and "
            + " b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd ")
    public List<String> findPolicyIdsByPolicyNumber(@Param("bilAccountId") String bilAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd);

    @Query(value = "select b from BilPolicy b where b.bilPolicyId.policyId = :policyId and b.bilIssueInd not in :bilIssueIndList")
    public BilPolicy findByPolicyId(@Param("policyId") String policyId,
            @Param("bilIssueIndList") List<Character> bilIssueIndList);

    @Query(value = "select MIN(b.bilPolicyId.policyId) from BilPolicy b where b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilIssueInd not in :bilIssueIndList")
    public String findMinPolicyIdByPolNumberPolSymbol(@Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilIssueIndList") List<Character> bilIssueIndList);

    @Query(value = "select MAX(b.bilPolicyId.policyId) from BilPolicy b where b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilIssueInd not in :bilIssueIndList")
    public String findMaxPolicyIdByPolNumberPolSymbol(String polNbr, String polSymbolCd,
            List<Character> policyIssueIndicator);

    @Query("Select b.bilPolicyId.policyId from BilPolicy b, BilAccount c where b.bilPolicyId.bilAccountId = :bilAccountId and b.bilAccountNbr = c.accountNumber and b.bilIssueInd = :bilIssueIndList")
    public String getPolicyIdByBilAccount(@Param("bilAccountId") String bilAccountId,
            @Param("bilIssueIndList") List<Character> bilIssueIndList);

    @Query(value = "select b from BilPolicy b where b.polNbr = :polNbr and b.bilIssueInd <> :bilIssueInd")
    public List<BilPolicy> findByPolicyNumber(@Param("polNbr") String polNbr,
            @Param("bilIssueInd") Character bilIssueInd);
    
    @Query(value = "select b from BilPolicy b where b.polNbr = :polNbr")
    public List<BilPolicy> findByPolicyNumber(@Param("polNbr") String polNbr);
    
    @Query(value = "select DISTINCT b.bilPolicyId.bilAccountId from BilPolicy b, BilPolicyTerm t where b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and t.billPolStatusCd not in ('W','T','D','G','V') "
            + "and b.bilPolicyId.bilAccountId = t.bilPolicyTermId.bilAccountId and b.bilPolicyId.policyId = t.bilPolicyTermId.policyId and b.bilIssueInd in ('Y','Z') ")
    public List<String> findAccountIdByPolicyNumberAndPolicySymbolCode(@Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd);

    @Query(value = "select DISTINCT b.bilPolicyId.bilAccountId from BilPolicy b, BilPolicyTerm t where b.polNbr = :polNbr and t.billPolStatusCd not in ('W','T','D','G','V') "
            + "and b.bilPolicyId.bilAccountId = t.bilPolicyTermId.bilAccountId and b.bilPolicyId.policyId = t.bilPolicyTermId.policyId and b.bilIssueInd in ('Y','Z') ")
    public List<String> findAccountIdByPolicyNumber(@Param("polNbr") String polNbr);

    @Query(value = "select DISTINCT b.bilPolicyId.policyId from BilPolicy b, BilPolicyTerm t where b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and t.billPolStatusCd not in ('W','T','D','G') "
            + "and b.bilPolicyId.bilAccountId = :accountId and b.bilPolicyId.bilAccountId = t.bilPolicyTermId.bilAccountId and b.bilPolicyId.policyId = t.bilPolicyTermId.policyId and b.bilIssueInd in ('Y','Z') ")
    public String findPolicyIdByPolicyNumberAndPolicySymbolCode(@Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("accountId") String accountId);

    @Query(value = "select DISTINCT b.bilPolicyId.policyId from BilPolicy b, BilPolicyTerm t where b.polNbr = :polNbr and t.billPolStatusCd not in ('W','T','D','G') "
            + "and b.bilPolicyId.bilAccountId = :accountId and b.bilPolicyId.bilAccountId = t.bilPolicyTermId.bilAccountId and b.bilPolicyId.policyId = t.bilPolicyTermId.policyId and b.bilIssueInd in ('Y','Z') ")
    public String findPolicyIdByPolicyNumber(@Param("polNbr") String polNbr, @Param("accountId") String accountId);
    
    @Query(value = "select b from BilPolicy b where b.bilPolicyId.bilAccountId = :accountId and b.polNbr = :polNbr and b.bilPolicyId.policyId = :policyId and b.bilIssueInd in :bilIssueIndList")
    public BilPolicy findByPolicyIdAndPolicyNumberAndAccountId(@Param("policyId") String policyId,
            @Param("polNbr") String polNbr, @Param("accountId") String accountId,
            @Param("bilIssueIndList") List<Character> bilIssueIndList);
    
}
