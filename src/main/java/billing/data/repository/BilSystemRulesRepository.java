package billing.data.repository;

import org.springframework.data.repository.CrudRepository;

import billing.data.entity.BilSystemRules;

public interface BilSystemRulesRepository extends CrudRepository<BilSystemRules, String> {

}
