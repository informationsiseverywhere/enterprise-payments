package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilMstCoSt;
import billing.data.entity.id.BilMstCoStId;

public interface BilMstCoStRepository extends JpaRepository<BilMstCoSt, BilMstCoStId> {

    @Query(value = "select b from BilMstCoSt b where b.bilMstCoStId.bilTypeCd = :bilTypeCd and b.bilMstCoStId.bilClassCd = :bilClassCd and "
            + "b.bilMstCoStId.bilBankCd = :bilBankCd ")
    public BilMstCoSt findRow(@Param("bilClassCd") String bilClassCd, @Param("bilTypeCd") String bilTypeCd,
            @Param("bilBankCd") String bilBankCd);

}
