package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilAmtTrmRlt;
import billing.data.entity.id.BilAmtTrmRltId;

public interface BilAmtTrmRltRepository extends JpaRepository<BilAmtTrmRlt, BilAmtTrmRltId> {
    @Query(value = "select rlt from BilAmtTrmRlt rlt JOIN BilAmounts bam ON rlt.bilAmtTrmRltId.bilAccountId = bam.bilAmountsId.bilAccountId and "
            + "rlt.bilAmtTrmRltId.policyId = bam.bilAmountsId.policyId and rlt.bilAmtTrmRltId.bilSeqNbr = bam.bilAmountsId.bilSeqNbr and "
            + "rlt.bamAmtTs = bam.bamAmtTs and rlt.bilRltTypeCd = 'XLN' JOIN BilAmtSchRlt brs ON brs.bilAmtSchRltId.bilAccountId = bam.bilAmountsId.bilAccountId and "
            + "brs.bilAmtSchRltId.policyId = bam.bilAmountsId.policyId and brs.bilAmtSchRltId.brsBamSeqNbr = bam.bilAmountsId.bilSeqNbr where bam.bilAmountsId.bilAccountId = :bilAccountId and "
            + "bam.bilAmountsId.policyId = :policyId and bam.bilPblItemCd = :bilPblItemCd and bam.bamAmt > 0 and brs.bilAmtSchRltId.brsBisSeqNbr = :brsBisSeqNbr ORDER BY bam.bamAmtTs DESC")
    public BilAmtTrmRlt getLoanInceptionDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("brsBisSeqNbr") short brsBisSeqNbr);
}
