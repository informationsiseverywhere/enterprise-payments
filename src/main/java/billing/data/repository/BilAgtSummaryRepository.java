package billing.data.repository;

import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilAgtSummary;
import billing.data.entity.id.BilAgtSummaryId;

public interface BilAgtSummaryRepository extends JpaRepository<BilAgtSummary, BilAgtSummaryId> {

    @Query(value = "select max(b.bilAgtSummaryId.bilAcySeq) from BilAgtSummary b WHERE b.bilAgtSummaryId.bilAgtActId = :bilAgtActId and "
            + "b.bilAgtSummaryId.bilAcyDt <= :bilAcyDt")
    public Short getMaxSeqNumber(@Param("bilAgtActId") String bilAgtActId, @Param("bilAcyDt") ZonedDateTime bilAcyDt);
    
    @Query(value = "select max(b.bilAgtSummaryId.bilAcySeq) from BilAgtSummary b WHERE b.bilAgtSummaryId.bilAgtActId = :bilAgtActId and "
            + "b.bilAgtSummaryId.bilAcyDt = :bilAcyDt")
    public Short getMaxSequenceNumber(@Param("bilAgtActId") String bilAgtActId, @Param("bilAcyDt") ZonedDateTime bilAcyDt);
}
