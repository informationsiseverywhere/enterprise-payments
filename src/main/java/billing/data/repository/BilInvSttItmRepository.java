package billing.data.repository;

import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilInvSttItm;
import billing.data.entity.id.BilInvSttItmId;

public interface BilInvSttItmRepository extends CrudRepository<BilInvSttItm, BilInvSttItmId> {

    @Query(value = "select SUM(b.bilSttGrossAmt) from BilInvSttItm b where b.bilInvSttItmId.bilTchKeyId = :bilTchKeyId "
            + "and b.bilInvSttItmId.bilTchKeyTypCd = :bilTchKeyTypCd and b.bilInvSttItmId.bilInvDt = :bilInvDt")
    public Double getSumForStatementReceivable(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Query(value = "select SUM(b.bilSttGrossAmt) from BilInvSttItm b where b.bilInvSttItmId.bilTchKeyId = :bilTchKeyId "
            + "and b.bilInvSttItmId.bilTchKeyTypCd = :bilTchKeyTypCd and b.bilSttReconInd = :bilSttReconInd")
    public Double getStatementsReceivableAmount(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilSttReconInd") char bilSttReconInd);
}
