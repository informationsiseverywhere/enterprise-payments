package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.HalUniversalCtl2;
import billing.data.entity.id.HalUniversalCtl2Id;

public interface HalUniversalCtl2Repository extends JpaRepository<HalUniversalCtl2, HalUniversalCtl2Id> {

    @Query("select b from HalUniversalCtl2 b where b.halUniversalCtl2Id.entryKey = :huc2EntryKeyCd and b.halUniversalCtl2Id.label = :huc2TblLblTxt and "
            + "b.halUniversalCtl2Id.effectiveDate = :effectiveDt")
    public HalUniversalCtl2 findByEntryKeyAndLabelTextAndEffectiveDate(@Param("huc2EntryKeyCd") String huc2EntryKeyCd,
            @Param("huc2TblLblTxt") String huc2TblLblTxt, @Param("effectiveDt") ZonedDateTime effectiveDt);

    @Query("select MAX(b.halUniversalCtl2Id.effectiveDate) from HalUniversalCtl2 b where b.halUniversalCtl2Id.entryKey = :huc2EntryKeyCd and b.halUniversalCtl2Id.label = :huc2TblLblTxt and "
            + "b.halUniversalCtl2Id.effectiveDate <= :effectiveDt")
    public ZonedDateTime findMaxEffectiveDate(@Param("huc2EntryKeyCd") String huc2EntryKeyCd,
            @Param("huc2TblLblTxt") String huc2TblLblTxt, @Param("effectiveDt") ZonedDateTime effectiveDt);

    @Query("select b from HalUniversalCtl2 b where b.halUniversalCtl2Id.entryKey = :huc2EntryKeyCd and b.entryData = :entryData and "
            + "b.halUniversalCtl2Id.effectiveDate <= :effectiveDt")
    public List<HalUniversalCtl2> findActiveEntryByCode(@Param("huc2EntryKeyCd") String huc2EntryKeyCd,
            @Param("entryData") String entryData, @Param("effectiveDt") ZonedDateTime effectiveDt);

}
