package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilSptPlnCrg;
import billing.data.entity.id.BilSptPlnCrgId;

public interface BilSptPlnCrgRepository extends JpaRepository<BilSptPlnCrg, BilSptPlnCrgId> {

    @Query(value = "select count(b) from BilSptPlnCrg b where b.bilSptPlnCrgId.bilTypeCd = :bilTypeCd and b.bilSptPlnCrgId.bilClassCd = :bilClassCd and "
            + "b.bilSptPlnCrgId.historyVldNbr = :historyVldNbr and b.bilSptScgInd = :bilSptScgInd and b.bilScgTypeCd = :bilScgTypeCd ")
    public Integer getCount(@Param("bilTypeCd") String bilTypeCode, @Param("bilClassCd") String bilClassCode,
            @Param("historyVldNbr") short historyVldNbr, @Param("bilSptScgInd") char bilSptScgInd,
            @Param("bilScgTypeCd") String bilScgTypeCd);

    @Query(value = "select count(b) from BilSptPlnCrg b where b.bilSptPlnCrgId.bilTypeCd = :bilTypeCd and b.bilSptPlnCrgId.bilClassCd = :bilClassCd and "
            + "(b.bilLateCrgAmt > :bilLateCrgAmt or b.bilLateCrgPct > :bilLateCrgPct) ")
    public Integer countByTypeClassCd(@Param("bilTypeCd") String bilTypeCode, @Param("bilClassCd") String bilClassCode,
            @Param("bilLateCrgAmt") double bilLateCrgAmt, @Param("bilLateCrgPct") double bilLateCrgPct);

    @Query(value = "select b from BilSptPlnCrg b where b.bilSptPlnCrgId.bilTypeCd = :bilTypeCd and b.bilSptPlnCrgId.bilClassCd = :bilClassCd")
    public List<BilSptPlnCrg> findByBilTypeCodeClassCode(@Param("bilTypeCd") String bilTypeCode,
            @Param("bilClassCd") String bilClassCode);

}
