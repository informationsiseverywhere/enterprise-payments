package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilEftPlnCrg;
import billing.data.entity.id.BilEftPlnCrgId;

public interface BilEftPlnCrgRepository extends JpaRepository<BilEftPlnCrg, BilEftPlnCrgId> {

    @Query(value = "select b from BilEftPlnCrg b where b.bilEftPlnCrgId.bilCollectionPln = :bilCollectionPln and b.bilEftPlnCrgId.historyVldNbr = 0"
            + " order by b.bilEftPlnCrgId.effectiveDt desc, b.bilEftPlnCrgId.bilCrgEffTypCd desc")
    public List<BilEftPlnCrg> findByCollectionPlan(@Param("bilCollectionPln") String bilCollectionPln);

}
