package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilAgtActRlt;
import billing.data.entity.id.BilAgtActRltId;

public interface BilAgtActRltRepository extends CrudRepository<BilAgtActRlt, BilAgtActRltId> {

    @Query(value = "select b.bilAgtActRltId.bilAgtActId FROM BilAgtActRlt b WHERE b.bilAgtActRltId.bilAccountId = :bilAccountId")
    public List<String> getBilAgtActIdByAccountId(@Param("bilAccountId") String bilAccountId);

}
