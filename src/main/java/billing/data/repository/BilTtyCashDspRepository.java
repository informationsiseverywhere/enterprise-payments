package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilTtyCashDsp;
import billing.data.entity.id.BilTtyCashDspId;

public interface BilTtyCashDspRepository extends JpaRepository<BilTtyCashDsp, BilTtyCashDspId> {

    @Query(value = "select COUNT(b) from BilTtyCashDsp b where b.bilTtyCashDspId.bilThirdPartyId = :accountId and b.bilTtyCashDspId.bilDtbDt = :distributionDate and "
            + "b.bilTtyCashDspId.bilDtbSeqNbr = :sequenceNumber and b.bilDspTypeCd = :bilDspTypeCode")
    public Integer findCountByBilDspTypeCode(@Param("accountId") String accountId,
            @Param("distributionDate") ZonedDateTime distributionDate, @Param("sequenceNumber") short sequenceNumber,
            @Param("bilDspTypeCode") String bilDspTypeCode);

    @Query(value = "select COUNT(b) from BilTtyCashDsp b where b.bilTtyCashDspId.bilThirdPartyId =:accountId and b.bilTtyCashDspId.bilDtbDt =:distributionDate and "
            + "b.bilTtyCashDspId.bilDtbSeqNbr =:sequenceNumber and b.bilDspTypeCd =:bilDspTypeCode and b.bilDspReasonCd <>:bilDspReasonCode and b.bilRevsRsusInd =:revsRsusIndicator and  "
            + "b.bilManualSusInd =:manualSuspendIndicator")
    public Integer findCountByTypeCodeAndReasonCode(@Param("accountId") String accountId,
            @Param("distributionDate") ZonedDateTime distributionDate, @Param("sequenceNumber") short sequenceNumber,
            @Param("bilDspTypeCode") String bilDspTypeCode, @Param("bilDspReasonCode") String bilDspReasonCode,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspendIndicator") char manualSuspendIndicator);

    @Query(value = "select SUM(b.bilDspAmt) from BilTtyCashDsp b where b.bilTtyCashDspId.bilThirdPartyId = :accountId and b.bilTtyCashDspId.bilDtbDt =:distributionDate and "
            + "b.bilTtyCashDspId.bilDtbSeqNbr =:bilDtbSequenceNumber and b.bilDspTypeCd =:bilDspTypeCode and b.bilRevsRsusInd =:revsRsusIndicator")
    public Double findPaymentSuspenseAmount(@Param("accountId") String accountId,
            @Param("bilDtbSequenceNumber") short bilDtbSequenceNumber,
            @Param("distributionDate") ZonedDateTime distributionDate, @Param("bilDspTypeCode") String bilDspTypeCode,
            @Param("revsRsusIndicator") char revsRsusIndicator);

    @Query(value = "select SUM(b.bilDspAmt) from BilTtyCashDsp b where b.bilTtyCashDspId.bilThirdPartyId = :accountId and b.bilTtyCashDspId.bilDtbDt =:distributionDate and "
            + "b.bilTtyCashDspId.bilDtbSeqNbr =:bilDtbSequenceNumber and b.bilDspTypeCd in :bilDspTypeCodeList and b.bilRevsRsusInd =:revsRsusIndicator")
    public Double findPaymentWriteOffAmount(@Param("accountId") String accountId,
            @Param("bilDtbSequenceNumber") short bilDtbSequenceNumber,
            @Param("distributionDate") ZonedDateTime distributionDate,
            @Param("bilDspTypeCodeList") List<String> bilDspTypeCodeList,
            @Param("revsRsusIndicator") char revsRsusIndicator);

    @Query(value = "select MAX(b.bilTtyCashDspId.bilDspSeqNbr) from BilTtyCashDsp b where b.bilTtyCashDspId.bilThirdPartyId = :bilThirdPartyId and"
            + " b.bilTtyCashDspId.bilDtbDt = :distributionDate and b.bilTtyCashDspId.bilDtbSeqNbr = :distributionSequenceNumber")
    public Short findMaxDispositionNumber(@Param("bilThirdPartyId") String bilThirdPartyId,
            @Param("distributionDate") ZonedDateTime distributionDate,
            @Param("distributionSequenceNumber") short distributionSequenceNumber);
    
    @Query(value = "select SUM(b.bilDspAmt) from BilTtyCashDsp b where b.bilTtyCashDspId.bilThirdPartyId = :bilThirdPartyId and b.bilDspTypeCd = :bilDspTypeCd and "
            + "b.bilAdjDueDt = :bilAdjDueDt and b.bilManualSusInd <> :bilManualSusInd and b.bilRevsRsusInd = :bilRevsRsusInd")
    public Double getStatementAmountReceived(@Param("bilThirdPartyId") String bilThirdPartyId,
            @Param("bilDspTypeCd") String bilDspTypeCd, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilManualSusInd") char bilManualSusInd, @Param("bilRevsRsusInd") char bilRevsRsusInd);
}