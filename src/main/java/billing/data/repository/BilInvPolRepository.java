package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilInvPol;
import billing.data.entity.id.BilInvPolId;

public interface BilInvPolRepository extends JpaRepository<BilInvPol, BilInvPolId> {

    @Query(value = "select b from BilInvPol b where b.bilInvPolId.bilTchKeyId = :bilTchKeyId and b.bilInvPolId.bilTchKeyTypCd = :bilTchKeyTypCd and "
            + "b.bilInvPolId.bilAccountId = :bilAccountId and b.bilInvPolId.bilInvEntryDt = :bilInvEntryDt order by b.bilInvPolId.bilInvSeqNbr")
    public List<BilInvPol> fetchBilInvPol(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilAccountId") String bilAccountId,
            @Param("bilInvEntryDt") ZonedDateTime bilInvEntryDt);

    @Query(value = "select b from BilInvPol b where b.bilInvPolId.bilTchKeyId = :bilTchKeyId and b.bilInvPolId.bilTchKeyTypCd = :bilTchKeyTypCd and "
            + "b.bilInvPolId.bilAccountId = :bilAccountId and b.bilInvPolId.bilInvEntryDt = :bilInvEntryDt "
            + "and (b.bilPrevDueAmt <> :bilPrevDueAmt or b.bilMinDueAmt <> :bilMinDueAmt)")
    public List<BilInvPol> fetchBilInvWithDueAmount(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilAccountId") String bilAccountId,
            @Param("bilInvEntryDt") ZonedDateTime bilInvEntryDt, @Param("bilPrevDueAmt") double bilPrevDueAmt,
            @Param("bilMinDueAmt") double bilMinDueAmt);

    @Query(value = "select COUNT(b) from BilInvPol b where b.bilInvPolId.bilTchKeyId = :bilTchKeyId and b.bilInvPolId.bilTchKeyTypCd = :bilTchKeyTypCd and "
            + "b.bilInvPolId.bilAccountId = :bilAccountId and b.bilInvPolId.bilInvEntryDt = :bilInvEntryDt and b.polSymbolCd = :polSymbolCd "
            + "and b.polNbr = :polNbr and b.polEffectiveDt = :polEffectiveDt and b.bilNewBalAmt > :bilNewBalAmt")
    public Integer checkTermParticipatedInvoice(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilAccountId") String bilAccountId,
            @Param("bilInvEntryDt") ZonedDateTime bilInvEntryDt, @Param("polSymbolCd") String polSymbolCd,
            @Param("polNbr") String polNbr, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bilNewBalAmt") double bilNewBalAmt);

}
