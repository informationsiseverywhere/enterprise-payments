package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilSttReconDtl;
import billing.data.entity.id.BilSttReconDtlId;

public interface BilSttReconDtlRepository extends JpaRepository<BilSttReconDtl, BilSttReconDtlId> {

    @Query(value = "select b from BilSttReconDtl b where b.bilSttReconDtlId.bilTchKeyId = :bilTchKeyId and b.bilSttReconDtlId.bilTchKeyTypCd = "
            + ":bilTchKeyTypCd and b.bilSttReconDtlId.bilAccountId = :bilAccountId and b.bilReconAcyCd IN :bilReconAcyCdList  and "
            + " b.bilAcyAmt > :bilAcyAmt and b.bilSttReconInd = :bilSttReconInd")
    public List<BilSttReconDtl> findExcluteItemRows(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilAccountId") String bilAccountId,
            @Param("bilReconAcyCdList") List<String> bilReconAcyCdList, @Param("bilSttReconInd") char bilSttReconInd,
            @Param("bilAcyAmt") Double bilAcyAmt);

    @Query(value = "select b.bilReconAcyCd from BilSttReconDtl b where b.bilSttReconDtlId.bilTchKeyId = :bilTchKeyId and "
            + " b.bilSttReconDtlId.bilTchKeyTypCd = :bilTchKeyTypCd and b.bilSttReconDtlId.bilAccountId = :bilAccountId and "
            + " b.bilSttReconDtlId.bilInvEntryDt = :bilInvEntryDt and b.bilSttReconDtlId.bilInvSeqNbr = :bilInvSeqNbr and "
            + " b.bilReconAcyCd IN :bilReconAcyCdList  order by b.bilReconAcyCd desc ")
    public List<String> findReconAcyCd(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilAccountId") String bilAccountId,
            @Param("bilInvEntryDt") ZonedDateTime bilInvEntryDt, @Param("bilInvSeqNbr") short bilInvSeqNbr,
            @Param("bilReconAcyCdList") List<String> bilReconAcyCdList);
    
    @Query(value = "select SUM(b.bilAcyAmt) from BilSttReconDtl b where b.bilSttReconDtlId.bilTchKeyId = :bilTchKeyId and b.bilSttReconDtlId.bilTchKeyTypCd = "
            + ":bilTchKeyTypCd and b.bilSttReconDtlId.bilInvEntryDt = :bilInvEntryDt and b.bilReconAcyCd IN :bilReconAcyCdList  and b.bilSttReconInd = :bilSttReconInd "
            + "and b.bilSttReconDtlId.bilSttTypeCd IN :bilSttReconDtlIdList")
    public Double getStatementAmount(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilInvEntryDt") ZonedDateTime bilInvEntryDt,
            @Param("bilReconAcyCdList") List<String> bilReconAcyCdList, @Param("bilSttReconInd") char bilSttReconInd,
            @Param("bilSttReconDtlIdList") List<Character> bilSttReconDtlIdList);
    
    @Query(value = "select SUM(b.bilAcyAmt) from BilSttReconDtl b where b.bilSttReconDtlId.bilTchKeyId = :bilTchKeyId and b.bilSttReconDtlId.bilTchKeyTypCd = "
            + ":bilTchKeyTypCd and b.bilSttReconDtlId.bilInvEntryDt = :bilInvEntryDt and b.bilReconAcyCd IN :bilReconAcyCdList "
            + "and b.bilSttReconDtlId.bilSttTypeCd IN :bilSttReconDtlIdList")
    public Double getStatementAmount(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilInvEntryDt") ZonedDateTime bilInvEntryDt,
            @Param("bilReconAcyCdList") List<String> bilReconAcyCdList,
            @Param("bilSttReconDtlIdList") List<Character> bilSttReconDtlIdList);
    
    @Query(value = "select SUM(b.bilAcyAmt) from BilSttReconDtl b where b.bilSttReconDtlId.bilTchKeyId = :bilTchKeyId and b.bilSttReconDtlId.bilTchKeyTypCd = "
            + ":bilTchKeyTypCd and b.bilReconAcyCd IN :bilReconAcyCdList and b.bilSttReconInd = :bilSttReconInd and b.bilSttReconDtlId.bilSttTypeCd IN :bilSttReconDtlIdList")
    public Double getStatementsAmount(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilReconAcyCdList") List<String> bilReconAcyCdList,
            @Param("bilSttReconInd") char bilSttReconInd,
            @Param("bilSttReconDtlIdList") List<Character> bilSttReconDtlIdList);

}