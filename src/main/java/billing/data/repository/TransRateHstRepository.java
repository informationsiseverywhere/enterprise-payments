package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.TransRateHst;
import billing.data.entity.id.TransRateHstId;

public interface TransRateHstRepository extends JpaRepository<TransRateHst, TransRateHstId> {
}
