package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilFunctionCtl;
import billing.data.entity.id.BilFunctionCtlId;

public interface BilFunctionCtlRepository extends JpaRepository<BilFunctionCtl, BilFunctionCtlId> {

    @Query(value = "select b from BilFunctionCtl b where b.bilFunctionCtlId.bilFunTypCd =:bilFunTypCd and b.bilFunctionCtlId.bilFunCd =:bilFunCd")
    public List<BilFunctionCtl> getBussinessGroup(@Param("bilFunTypCd") String bilFunTypCd,
            @Param("bilFunCd") String bilFunCd);

}
