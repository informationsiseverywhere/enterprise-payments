package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilAmounts;
import billing.data.entity.id.BilAmountsId;

public interface BilAmountsRepository extends JpaRepository<BilAmounts, BilAmountsId> {

    @Query(value = "select b from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt = :bamAmtEffDt and "
            + "b.bilAmtEffInd IN :bilAmtEffIndList and b.bamAmtPrcInd = :bamAmtPrcInd order by b.bilAmountsId.bilSeqNbr DESC")
    public List<BilAmounts> findMaxAmountRow(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList, @Param("bamAmtPrcInd") char bamAmtPrcInd,
            Pageable pageable);

    @Query(value = "select MAX( b.bilAmountsId.bilSeqNbr) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and "
            + "b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and "
            + "b.bilAmtEffInd in :bilAmtEffIndList and b.bamAmtPrcInd = :bamAmtPrcInd")
    public Short getMaxLastBilSeqNbr(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList, @Param("bamAmtPrcInd") char bamAmtPrcInd);

    @Query(value = "select b from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and "
            + "b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt = :bamAmtEffDt and b.bamAmtExpDt = :bamAmtExpDt and "
            + "b.bilAmtEffInd in :bilAmtEffIndList and b.bamAmtPrcInd = :bamAmtPrcInd")
    public BilAmounts getBilAmountByEffInd(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList, @Param("bamAmtPrcInd") char bamAmtPrcInd);

    @Query(value = "select b from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId "
            + " and b.bamAmtEffDt = :bamAmtEffDt and b.bilAmtEffInd = :bilAmtEffInd and b.bamAmtPrcInd not in :bamAmtPrcIndList")
    public BilAmounts getInstTermType(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bilAmtEffInd") char bilAmtEffInd,
            @Param("bamAmtPrcIndList") List<Character> bamAmtPrcIndList);

    @Query(value = "select count(b) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId and "
            + "b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtExpDt > :bamAmtExpDtSecond and b.bamAmtPrcInd = :bamAmtPrcInd and "
            + " b.bilAmtEffInd = :bilAmtEffInd")
    public Integer getMaxQuoteAmountEftInd(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtExpDtSecond") ZonedDateTime bamAmtExpDtSecond, @Param("bamAmtPrcInd") char bamAmtPrcInd,
            @Param("bilAmtEffInd") char bilAmtEffInd);

    @Query(value = "select MAX(b.bamAmtTs) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId and "
            + "b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtTs > :bamAmtTs and b.bamAmtPrcInd = :bamAmtPrcInd and "
            + " b.bilAmtEffInd in :bilAmtEffIndList and b.bilDesReaTyp in :bilDesReaTypList")
    public ZonedDateTime getMaxReInstallmentAmtTs(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bamAmtTs") ZonedDateTime bamAmtTs,
            @Param("bamAmtPrcInd") char bamAmtPrcInd, @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select MAX(b.bilAmtEffInd) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId and "
            + "b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtTs > :bamAmtTs and b.bamAmtPrcInd = :bamAmtPrcInd and "
            + "(b.bilAmtEffInd = :bilAmtEffInd and b.bilDesReaTyp in :bilDesReaTypList or (b.bilAmtEffInd in :bilAmtEffIndList and b.bilDesReaTyp in :bilDesReaTypListSecond))")
    public Character getMaxAmountEftInd(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtTs") ZonedDateTime bamAmtTs, @Param("bamAmtPrcInd") char bamAmtPrcInd,
            @Param("bilAmtEffInd") char bilAmtEffInd, @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypListSecond") List<String> bilDesReaTypListSecond);

    @Query(value = "select MAX( b.bamAmtTs) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= "
            + ":bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = 'Y' and ((b.bilAmtEffInd = 'F' and b.bilDesReaTyp in ('BAM','AMT','BAS','BPS','BEQ')) "
            + "or (b.bilAmtEffInd in ('P','S','7','3','5') and b.bilDesReaTyp in ('BAM','AMT','BAS','BPS','BAA','BPA','BEQ')))")
    public ZonedDateTime getmaxAmtTs(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt);

    @Query(value = "select b from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId and b.bamAmtExpDt = :bamAmtExpDt and "
            + "b.bilAmtEffInd IN :bilAmtEffIndList and b.bamAmtTs >= :bamAmtTs and b.bamAmt < :bamAmt ")
    public List<BilAmounts> findStaticPcnBamAmountRows(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList, @Param("bamAmtTs") ZonedDateTime bamAmtTs,
            @Param("bamAmt") double bamAmt);

    @Query(value = "select count(b) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId and "
            + "b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd")
    public Integer countBamAmounts(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd);

    @Query(value = "select count(b) from BilAmounts b where b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and "
            + " b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd")
    public Integer countBamAmountsForPolicy(@Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd);

    @Query(value = "select count(b) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId and "
            + "b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd "
            + "and b.bilDesReaTyp not in :bilDesReaTypList")
    public Integer countBamAmountsByReasonType(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bamAmtPrcInd") char bamAmtPrcInd,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select count(b) from BilAmounts b where b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and "
            + " b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd and b.bilDesReaTyp not in :bilDesReaTypList")
    public Integer countBamAmountsByReasonTypeForPolicy(@Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd, @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select SUM(b.bamAmt) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId and "
            + "b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd "
            + "and b.bilDesReaTyp not in :bilDesReaTypList")
    public Double getSumBamAmountByReasonType(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bamAmtPrcInd") char bamAmtPrcInd,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select SUM(b.bamAmt) from BilAmounts b where b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and "
            + "b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd and b.bilDesReaTyp not in :bilDesReaTypList")
    public Double getSumBamAmountByReasonTypeForPolicy(@Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd, @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select SUM(b.bamAmt) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId and "
            + " b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd")
    public Double getSumBamAmounts(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd);

    @Query(value = "select SUM(b.bamAmt) from BilAmounts b where b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and "
            + " b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd")
    public Double getSumBamAmountsForPolicy(@Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd);

    @Query(value = "select MAX( b.bamAmtTs) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId "
            + " and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd and "
            + " ((b.bilAmtEffInd = :bilAmtEffInd and b.bilDesReaTyp in :bilDesReaTypList) "
            + " or (b.bilAmtEffInd in :bilAmtEffIndList and b.bilDesReaTyp in :bilDesReaTypListSecond)) ")
    public ZonedDateTime getMaxBamAmtTs(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd, @Param("bilAmtEffInd") char bilAmtEffInd,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypListSecond") List<String> bilDesReaTypListSecond);

    @Query(value = "select MAX( b.bamAmtTs) from BilAmounts b where b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and "
            + " b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd and ((b.bilAmtEffInd = :bilAmtEffInd and b.bilDesReaTyp in :bilDesReaTypList) "
            + " or (b.bilAmtEffInd in :bilAmtEffIndList and b.bilDesReaTyp in :bilDesReaTypListSecond)) ")
    public ZonedDateTime getMaxBamAmtTsForPolicy(@Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd, @Param("bilAmtEffInd") char bilAmtEffInd,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypListSecond") List<String> bilDesReaTypListSecond);

    @Query(value = "select MAX( b.bamAmtTs) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId "
            + " and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd and "
            + " b.bilAmtEffInd in :bilAmtEffIndList and b.bilDesReaTyp in :bilDesReaTypList ")
    public ZonedDateTime getMaxReinstAmountTimestamp(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bamAmtPrcInd") char bamAmtPrcInd,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select MAX( b.bamAmtTs) from BilAmounts b where b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and "
            + " b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd and b.bilAmtEffInd in :bilAmtEffIndList and "
            + " b.bilDesReaTyp in :bilDesReaTypList ")
    public ZonedDateTime getMaxReinstAmountTimestampForPolicy(@Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd, @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select SUM( b.bamAmt) AS amount, MAX(b.bamAmtEffDt) AS effDate, MAX(b.bamAmtExpDt) AS expDate from BilAmounts b where "
            + " b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId "
            + " and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd and "
            + " b.bamAmtTs = :bamAmtTs and b.bamAmt < :bamAmt and ((b.bilAmtEffInd = :bilAmtEffInd and b.bilDesReaTyp in :bilDesReaTypList) "
            + " or (b.bilAmtEffInd in :bilAmtEffIndList and b.bilDesReaTyp in :bilDesReaTypListSecond)) ")
    public List<Object[]> getMaxBilAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bamAmtTs") ZonedDateTime bamAmtTs,
            @Param("bamAmt") double bamAmt, @Param("bamAmtPrcInd") char bamAmtPrcInd,
            @Param("bilAmtEffInd") char bilAmtEffInd, @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypListSecond") List<String> bilDesReaTypListSecond);

    @Query(value = "select SUM( b.bamAmt) AS amount, MAX(b.bamAmtEffDt) AS effDate, MAX(b.bamAmtExpDt) AS expDate from BilAmounts b where "
            + " b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and "
            + " b.bamAmtPrcInd = :bamAmtPrcInd and b.bamAmtTs = :bamAmtTs and b.bamAmt < :bamAmt and "
            + " ((b.bilAmtEffInd = :bilAmtEffInd and b.bilDesReaTyp in :bilDesReaTypList) "
            + " or (b.bilAmtEffInd in :bilAmtEffIndList and b.bilDesReaTyp in :bilDesReaTypListSecond)) ")
    public List<Object[]> getMaxBilAmountsForPolicy(@Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtTs") ZonedDateTime bamAmtTs, @Param("bamAmt") double bamAmt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd, @Param("bilAmtEffInd") char bilAmtEffInd,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypListSecond") List<String> bilDesReaTypListSecond);

    @Query(value = "select SUM( b.bamAmt) AS amount, MAX(b.bamAmtEffDt) AS effDate, MAX(b.bamAmtExpDt) AS expDate from BilAmounts b where "
            + " b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId "
            + " and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd and "
            + " b.bamAmtTs = :bamAmtTs and b.bamAmt < :bamAmt and ((b.bilAmtEffInd = :bilAmtEffInd and b.bilDesReaTyp in :bilDesReaTypList) "
            + " or (b.bilAmtEffInd in :bilAmtEffIndList and b.bilDesReaTyp in :bilDesReaTypListSecond)) ")
    public List<Object[]> getMaxBilAmountsByLessBamAmount(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bamAmtTs") ZonedDateTime bamAmtTs,
            @Param("bamAmt") double bamAmt, @Param("bamAmtPrcInd") char bamAmtPrcInd,
            @Param("bilAmtEffInd") char bilAmtEffInd, @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypListSecond") List<String> bilDesReaTypListSecond);

    @Query(value = "select SUM( b.bamAmt) AS amount, MAX(b.bamAmtEffDt) AS effDate, MAX(b.bamAmtExpDt) AS expDate from BilAmounts b where "
            + " b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd and "
            + " b.bamAmtTs = :bamAmtTs and b.bamAmt < :bamAmt and ((b.bilAmtEffInd = :bilAmtEffInd and b.bilDesReaTyp in :bilDesReaTypList) "
            + " or (b.bilAmtEffInd in :bilAmtEffIndList and b.bilDesReaTyp in :bilDesReaTypListSecond)) ")
    public List<Object[]> getMaxBilAmountsByLessBamAmountForPolicy(@Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtTs") ZonedDateTime bamAmtTs, @Param("bamAmt") double bamAmt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd, @Param("bilAmtEffInd") char bilAmtEffInd,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypListSecond") List<String> bilDesReaTypListSecond);

    @Query(value = "select MAX(b.bamAmtExpDt) AS expDate from BilAmounts b where "
            + " b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId "
            + " and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd and "
            + " b.bilDesReaTyp not in :bilDesReaTypList")
    public ZonedDateTime getMaxBamAmtExpDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bamAmtPrcInd") char bamAmtPrcInd,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select MAX(b.bamAmtExpDt) AS expDate from BilAmounts b where "
            + " b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and "
            + " b.bamAmtPrcInd = :bamAmtPrcInd and b.bilDesReaTyp not in :bilDesReaTypList")
    public ZonedDateTime getMaxBamAmtExpDateForPolicy(@Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd, @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select MIN(b.bamAmtEffDt) AS expDate from BilAmounts b where "
            + " b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId "
            + " and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd and "
            + " b.bilDesReaTyp not in :bilDesReaTypList")
    public ZonedDateTime getMinBamAmtEffDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bamAmtPrcInd") char bamAmtPrcInd,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select MIN(b.bamAmtEffDt) AS expDate from BilAmounts b where "
            + " b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and "
            + " b.bamAmtPrcInd = :bamAmtPrcInd and b.bilDesReaTyp not in :bilDesReaTypList")
    public ZonedDateTime getMinBamAmtEffDateForPolicy(@Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd, @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select b.bamAmtEffDt, b.bamAmtExpDt, SUM( b.bamAmt) AS amount from BilAmounts b where "
            + " b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId "
            + " and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd and "
            + " b.bilDesReaTyp not in :bilDesReaTypList group by b.bamAmtEffDt, b.bamAmtExpDt "
            + " order by b.bamAmtEffDt, b.bamAmtExpDt ")
    public List<Object[]> fetchGroupAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bamAmtPrcInd") char bamAmtPrcInd,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select b.bamAmtEffDt, b.bamAmtExpDt, SUM( b.bamAmt) AS amount from BilAmounts b where "
            + " b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and "
            + " b.bamAmtPrcInd = :bamAmtPrcInd and b.bilDesReaTyp not in :bilDesReaTypList group by b.bamAmtEffDt, b.bamAmtExpDt "
            + " order by b.bamAmtEffDt, b.bamAmtExpDt ")
    public List<Object[]> fetchGroupAmountsForPolicy(@Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd, @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select b.bamAmtEffDt, b.bamAmtExpDt, SUM( b.bamAmt) AS amount from BilAmounts b where "
            + " b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId "
            + " and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd "
            + " group by b.bamAmtEffDt, b.bamAmtExpDt order by b.bamAmtEffDt, b.bamAmtExpDt ")
    public List<Object[]> fetchGroupAmountsForAgreement(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bamAmtPrcInd") char bamAmtPrcInd);

    @Query(value = "select b.bamAmtEffDt, b.bamAmtExpDt, SUM( b.bamAmt) AS amount from BilAmounts b where "
            + " b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd = :bamAmtPrcInd "
            + " group by b.bamAmtEffDt, b.bamAmtExpDt order by b.bamAmtEffDt, b.bamAmtExpDt ")
    public List<Object[]> fetchGroupAmountsForAgreementForPolicy(@Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd);

    @Query(value = "select MAX(b.bamAmtTs) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId and "
            + " b.bamAmtExpDt = :bamAmtExpDt and b.bilAmtEffInd in :bilAmtEffIndList and b.polReaAmdCd in :polReaAmdCdList and b.bamAmtPrcInd not in :bamAmtPrcIndList")
    public ZonedDateTime getMaxReInstateTime(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("polReaAmdCdList") List<String> polReaAmdCdList,
            @Param("bamAmtPrcIndList") List<Character> bamAmtPrcIndList);

    @Query(value = "select distinct b.bilAmtEffInd, b.bamAmtEffDt from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and "
            + " b.bilAmountsId.policyId = :policyId and b.bamAmtTs = :bamAmtTs and b.bilAmtEffInd in :bilAmtEffIndList and b.bamAmtPrcInd not in :bamAmtPrcIndList")
    public List<Object[]> getDistinctAmtEfts(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtTs") ZonedDateTime bamAmtTs,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bamAmtPrcIndList") List<Character> bamAmtPrcIndList);

    @Query(value = "select count(b) from BilAmounts b, BilAmtSchRlt c, BilIstSchedule d where b.bilAmountsId.bilAccountId = c.bilAmtSchRltId.bilAccountId "
            + "and b.bilAmountsId.policyId = c.bilAmtSchRltId.policyId and b.bilAmountsId.bilSeqNbr = c.bilAmtSchRltId.brsBamSeqNbr and "
            + "c.bilAmtSchRltId.bilAccountId = d.bilIstScheduleId.billAccountId and c.bilAmtSchRltId.policyId = d.bilIstScheduleId.policyId and "
            + "c.bilAmtSchRltId.brsBisSeqNbr = d.bilIstScheduleId.billSeqNbr and b.bilAmountsId.bilAccountId = :bilAccountId and "
            + "b.bilAmtEffInd not in :bilAmtEffIndList and b.bilDesReaTyp not in :bilDesReaTypList and "
            + "c.bilPrmIstAmt <> :bilPrmIstAmt and d.billAdjDueDt = :adjustDueDate and d.billInvoiceCd in :billInvoiceCdList")
    public Integer checkBilAmountsExistanceForExcludeIncludeRule(@Param("bilAccountId") String bilAccountId,
            @Param("adjustDueDate") ZonedDateTime adjustDueDate,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilPrmIstAmt") double bilPrmIstAmt);

    @Query(value = "select count(b) from BilAmounts b, BilAmtSchRlt c, BilIstSchedule d where b.bilAmountsId.bilAccountId = c.bilAmtSchRltId.bilAccountId "
            + "and b.bilAmountsId.policyId = c.bilAmtSchRltId.policyId and b.bilAmountsId.bilSeqNbr = c.bilAmtSchRltId.brsBamSeqNbr and "
            + "c.bilAmtSchRltId.bilAccountId = d.bilIstScheduleId.billAccountId and c.bilAmtSchRltId.policyId = d.bilIstScheduleId.policyId and "
            + "d.billIstDueDt <> d.policyEffectiveDt and c.bilAmtSchRltId.brsBisSeqNbr = d.bilIstScheduleId.billSeqNbr and b.bilAmountsId.bilAccountId = :bilAccountId and "
            + "b.bilAmtEffInd not in :bilAmtEffIndList and b.bilDesReaTyp not in :bilDesReaTypList and "
            + "c.bilPrmIstAmt <> :bilPrmIstAmt and d.billAdjDueDt = :adjustDueDate and d.billInvoiceCd in :billInvoiceCdList")
    public Integer checkBilAmountsExistance(@Param("bilAccountId") String bilAccountId,
            @Param("adjustDueDate") ZonedDateTime adjustDueDate,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilPrmIstAmt") double bilPrmIstAmt);

    @Query(value = "select count(b) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and "
            + "b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtExpDt <= :bamAmtExpDt and "
            + "b.bilAmtEffInd in :bilAmtEffIndList and b.bilAmountsId.bilSeqNbr in :bilSeqNbrList")
    public Integer checkExistanceByPolicyIdDatesEffectiveIndicator(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bilAmtEffIndList") List<Character> bilAmtEffInd,
            @Param("bilSeqNbrList") List<Short> bilSeqNbrList);

    @Query(value = "select count(b) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and "
            + "b.bilAmountsId.policyId = :policyId and b.bilAmtEffInd in :bilAmtEffIndList")
    public Integer checkExistanceByPolicyIdEffectiveIndicator(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList);

    @Query(value = "select count(b) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmtEffInd in :bilAmtEffIndList and "
            + "b.bamAmtPrcInd not in :bamAmtPrcIndList")
    public Integer findCountByAmountEffectiveAndProcessIndicator(@Param("bilAccountId") String bilAccountId,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bamAmtPrcIndList") List<Character> bamAmtPrcIndList);

    @Query(value = "select count(b) from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and "
            + "(b.bilAmtEffInd in :bilAmtEffIndList or b.bilDesReaTyp in :bilDesReaTypList)")
    public Integer findCountByEffectiveIndicatorOrReasonType(@Param("bilAccountId") String bilAccountId,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select b from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and  b.bilAmountsId.policyId = :policyId "
            + " and b.bamAmtEffDt = :bamAmtEffDt and b.bilAmtEffInd in :bilAmtEffIndList and b.bamAmtPrcInd <> :bamAmtPrcInd")
    public List<BilAmounts> getBilAmountsByEffectiveDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList, @Param("bamAmtPrcInd") char bamAmtPrcInd);

    @Query(value = "select b from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId "
            + " and b.bamAmtEffDt >= :bamAmtEffDt and b.bamAmtEffDt <= :bamAmtEffDtSecond and b.bamAmtPrcInd <> :bamAmtPrcInd and "
            + " ((b.bilAmtEffInd = :bilAmtEffInd and b.bilDesReaTyp in :bilDesReaTypList) "
            + " or (b.bilAmtEffInd in :bilAmtEffIndList and b.bilDesReaTyp in :bilDesReaTypListSecond)) "
            + " order by b.bamAmtTs desc, b.bamAmtEffDt asc ")
    public List<BilAmounts> getMaxCancelBamAmtTs(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bamAmtEffDtSecond") ZonedDateTime bamAmtEffDtSecond, @Param("bamAmtPrcInd") char bamAmtPrcInd,
            @Param("bilAmtEffInd") char bilAmtEffInd, @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bilDesReaTypListSecond") List<String> bilDesReaTypListSecond);

    @Query(value = "select b from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId and b.bilAmountsId.policyId = :policyId "
            + " and b.bilAmtEffInd in :bilAmtEffIndList and b.bamAmtPrcInd in :bamAmtPrcIndList  "
            + " order by b.bamAmtTs desc ")
    public List<BilAmounts> getAmountEffectiveInd(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList,
            @Param("bamAmtPrcIndList") List<Character> bamAmtPrcIndList);

    @Query(value = "select b.bilDesReaTyp from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId "
            + "and b.bilAmountsId.policyId = :policyId "
            + "and b.bilDesReaTyp not in :bilDesReaTypList and b.bamAmtEffDt >= :bamAmtEffDt "
            + "and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd <> :bamAmtPrcInd")
    public String findBilDesReaTypeByBilAmountEffDateAndBilAmountExDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd);

    @Query(value = "select b from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId "
            + "and b.bilAmountsId.policyId = :policyId "
            + "and b.polReaAmdCd = :polReaAmdCd and b.bamAmtEffDt >= :bamAmtEffDt "
            + "and b.bamAmtExpDt <= :bamAmtExpDt and b.bamAmtPrcInd <> :bamAmtPrcInd")
    public List<BilAmounts> findBilAmountByPolicyReasonAmountCode(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polReaAmdCd") String polReaAmdCd,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt, @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt,
            @Param("bamAmtPrcInd") char bamAmtPrcInd);

    @Query(value = "select b.bilAmtEffInd from BilAmounts b where b.bilAmountsId.bilAccountId = :bilAccountId "
            + "and b.bilAmountsId.policyId = :policyId and b.bamAmtEffDt = :bamAmtEffDt and b.bilAmtEffInd in :bilAmtEffIndList")
    public List<Character> getBilAmountsEffectiveInd(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList);

    @Query(value = "select b.bilAmtEffInd from BilAmounts b, BilPolicy c where  b.bilAmountsId.bilAccountId = :bilAccountId "
            + "and b.bilAmtEffInd in :bilAmtEffIndList and c.polNbr = :polNbr and  b.bilAmountsId.bilAccountId =  c.bilPolicyId.bilAccountId and c.polSymbolCd = :polSymbolCd")
    public List<Character> getBilAmountsEffectiveIndByPolicyNbr(@Param("bilAccountId") String bilAccountId,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd);

}
