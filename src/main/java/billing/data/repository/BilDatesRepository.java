package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilDates;
import billing.data.entity.id.BilDatesId;

public interface BilDatesRepository extends JpaRepository<BilDates, BilDatesId> {

    @Query(value = "select MAX(b.bilInvDt), MAX(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId  "
            + " and b.bilInvoiceCd = :bilInvoiceCd and b.bilAdjDueDt >= :bilAdjDueDt and b.bilExcInd <> :bilExcInd ")
    public List<Object[]> getMaxDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvoiceCd") char bilInvoiceCd,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilInvDt), MAX(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId  "
            + " and b.bilInvoiceCd = :bilInvoiceCd and b.bilAdjDueDt > :bilAdjDueDt and b.bilExcInd <> :bilExcInd ")
    public List<Object[]> getMaxDate1(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvoiceCd") char bilInvoiceCd,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId  "
            + " and b.bilInvoiceCd in :bilInvoiceCdList and b.bilAdjDueDt > :bilAdjDueDt and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getRevisedMinAdjDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select COUNT(b) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd = :billInvoiceCd and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilExcInd <> :bilExcInd")
    public Integer checkThirdPartyCash(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCd") char billInvoiceCd, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilExcInd") char bilExcInd);

    @Query("select MAX(b.bilInvDt), MAX(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd = :bilInvoiceCd ")
    public List<Object[]> getMaxDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCd") char bilInvoiceCd);

    @Query(value = "select MIN(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd in :bilInvoiceCdList")
    public ZonedDateTime getMinAdjustDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "select MAX(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd IN :billInvoiceCdList and "
            + "b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMaxCivAdjDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd IN :billInvoiceCdList and "
            + "b.bilAdjDueDt = :bilAdjDueDt and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMaxCivInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMinInvoicedDateByInvoiceCd(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd IN :billInvoiceCdList "
            + " and b.bdtLateCrgInd = :bdtLateCrgInd and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMaxAdjustDueDateByLateCharge(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bdtLateCrgInd") char bdtLateCrgInd,
            @Param("bilExcInd") char bilExcInd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilDates b set b.bdtLateCrgInd = :lateCrgIndUpdate where b.bilDatesId.bilAccountId = :bilAccountId and b.bilAdjDueDt <= :bilAdjDueDt "
            + " and b.bilInvoiceCd IN :billInvoiceCdList and b.bdtLateCrgInd = :bdtLateCrgInd")
    public Integer updateLateChargeInd(@Param("lateCrgIndUpdate") char lateCrgIndUpdate,
            @Param("bilAccountId") String bilAccountId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bdtLateCrgInd") char bdtLateCrgInd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilDates b set b.bdtNoncpiInd = :bdtNoncpiInd  where b.bilDatesId.bilAccountId = :bilAccountId and b.bilAdjDueDt <= :bilAdjDueDt "
            + "and b.bilInvoiceCd in :bilInvoiceCdList and b.bdtNoncpiInd = :bdtNoncpiInd1 ")
    public void updateByBilAccountIdRec(@Param("bdtNoncpiInd") char bdtNoncpiInd,
            @Param("bilAccountId") String bilAccountId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList, @Param("bdtNoncpiInd1") char bdtNoncpiInd1);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd = :billInvoiceCd and "
            + "b.bilInvDt <= :bilInvDt and b.bilExcInd <> :bilExcInd " + " order by b.bilAdjDueDt desc")
    public List<BilDates> getMaxAdjDueDateRow(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billInvoiceCd") char billInvoiceCd,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd = :billInvoiceCd "
            + "and b.bdtNoncpiInd IN ('N','S') and b.bilAdjDueDt = :bilAdjDueDt and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getdMinAdjDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCd") char billInvoiceCd, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd = :billInvoiceCd and "
            + "b.bdtNoncpiInd in :bdtNoncpiIndList and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMaxNoticeInvDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCd") char billInvoiceCd, @Param("bdtNoncpiIndList") List<Character> bdtNoncpiIndList,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd = :billInvoiceCd and "
            + "b.bdtNoncpiInd in :bdtNoncpiIndList and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMaxNoticeDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCd") char billInvoiceCd, @Param("bdtNoncpiIndList") List<Character> bdtNoncpiIndList,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bdtDateInd) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd IN :billInvoiceCdList "
            + " and b.bilAdjDueDt = :bilAdjDueDt and b.bilExcInd <> :bilExcInd")
    public Character getMaxBillDatesInd(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b.bilInvDt from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public List<ZonedDateTime> getInvoicedDateCount(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilDatesId.bilReferenceDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilDatesId.bilReferenceDt > :bilReferenceDt and "
            + "b.bilSysDueDt = :bilSysDueDt and b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDt and b.bdtDateType = :bdtDateType and "
            + "b.bilInvoiceCd in :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMinReferenceDateForScheduledRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilReferenceDt") ZonedDateTime bilReferenceDt, @Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bdtDateType") char bdtDateType, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd in :billInvoiceCdList "
            + "and b.bdtDateType in :dateTypeCodes and b.bilExcInd <> :bilExcInd")
    public List<BilDates> findRowsForCleanUp(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("dateTypeCodes") List<Character> dateTypeCodes, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilDatesId.bilReferenceDt = :bilReferenceDt "
            + "and b.bilExcInd <> :bilExcInd")
    public BilDates findByReferenceDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilReferenceDt") ZonedDateTime bilReferenceDt, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilInvoiceCd) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilAdjDueDt = :bilAdjDueDt and "
            + "b.bilInvDt = :bilInvDt and b.bilExcInd <> :bilExcInd")
    public Character getMaxInvoiceCodeByDueDateInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilDatesId.bilReferenceDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + "b.bilInvoiceCd not in :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMaxReferenceDateByInvoiceCode(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilDatesId.bilReferenceDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + "b.bdtDateType <> :dateType")
    public ZonedDateTime getMaxReferenceDateByDateType(@Param("bilAccountId") String bilAccountId,
            @Param("dateType") char dateType);

    @Query(value = "select MAX(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMaxAdjustDueDateByInvoiceCd(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId "
            + "and b.bilInvDt > :bilInvDt and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMinInvoicedDateByInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMinInvoicedDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b.bilAdjDueDt, MIN(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + "b.bilInvoiceCd in :billInvoiceCdList and b.bilExcInd <> :bilExcInd group by b.bilAdjDueDt order by b.bilAdjDueDt")
    public List<Object[]> getMinInvoiceDateForAdjustDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bdtDateType <> :dateType and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMaxInvoicedDateByDateType(@Param("bilAccountId") String bilAccountId,
            @Param("dateType") char dateType, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + "b.bdtDateInd <> :bdtDateInd and b.bilInvoiceCd IN :billInvoiceCdList and b.bilAdjDueDt in :adjustDueDateList and "
            + "b.bilExcInd <> :bilExcInd")
    public List<BilDates> findDataForLookAheadDateReset(@Param("bilAccountId") String bilAccountId,
            @Param("bdtDateInd") char bdtDateInd, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("adjustDueDateList") List<ZonedDateTime> adjustDueDateList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + "b.bilSysDueDt = :bilSysDueDt")
    public List<BilDates> findBySystemDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bilInvDt <= :bilInvDt and b.bilInvoiceCd in :billInvoiceCdList and b.bilExcInd <> :bilExcInd"
            + " order by b.bilInvDt desc, b.bilAdjDueDt desc, b.bdtDateType desc ")
    public List<BilDates> getMaxInvoicedDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bilInvDt = :bilInvDt and b.bilInvoiceCd in :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMaxAdjustDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId  "
            + " and b.bilInvoiceCd = :bilInvoiceCd and b.bilInvDt <= :bilInvDt and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMinAdjDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilInvoiceCd") char bilInvoiceCd,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select DISTINCT (b.bilSysDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd IN :billInvoiceCdList "
            + " order by b.bilSysDueDt ")
    public List<ZonedDateTime> selectSysDueDateByInvoiceCdList(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "delete BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilSysDueDt = :bilSysDueDt and b.bilInvoiceCd IN :billInvoiceCdList ")
    public Integer deleteRowsByInvoiceCd(@Param("bilAccountId") String bilAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd in :billInvoiceCdList "
            + " and b.bilDatesId.bilReferenceDt >= :bilReferenceDt and b.bilAdjDueDt >= :bilAdjDueDt and b.bilSysDueDt >= :bilSysDueDt "
            + " and b.bilExcInd <> :bilExcInd order by b.bilDatesId.bilReferenceDt desc ")
    public List<BilDates> fetchBilDatesBySetDates(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilReferenceDt") ZonedDateTime bilReferenceDt, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd in :billInvoiceCdList "
            + " and (b.bilSysDueDt <> b.bilAdjDueDt or b.bilInvDt = :bilInvDt) and b.bilExcInd <> :bilExcInd order by b.bilSysDueDt ")
    public List<BilDates> fetchRescindDates(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilInvDt = :bilInvDt and b.bdtDateType <> :bdtDateType and b.bilInvoiceCd in :billInvoiceCdList and "
            + " b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getPrevMinAdjDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bdtDateType") char bdtDateType,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE BilDates b SET  b.bilAdjDueDt = :bilAdjDueDtUpdate,"
            + "  b.bilInvDt = :bilInvDtUpdate, b.bilInvoiceCd = :bilInvoiceCd "
            + " where b.bilDatesId.bilAccountId = :bilAccountId and b.bilAdjDueDt = :bilAdjDueDt "
            + " and b.bilInvDt = :bilInvDt ")
    public Integer updateBilDatesByAdjDueDate(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("bilInvoiceCd") char bilInvoiceCd,
            @Param("bilAccountId") String bilAccountId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE BilDates b SET  b.bilAdjDueDt = :bilAdjDueDtUpdate,"
            + "  b.bilInvDt = :bilInvDtUpdate, b.bdtDateInd = :bdtDateInd, b.bdtNoncpiInd = :bdtNoncpiInd "
            + " where b.bilDatesId.bilAccountId = :bilAccountId and b.bilAdjDueDt = :bilAdjDueDt "
            + " and b.bilInvDt = :bilInvDt ")
    public Integer updateRescindBilDatesByAdjDueDate(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("bdtDateInd") char bdtDateInd,
            @Param("bdtNoncpiInd") char bdtNoncpiInd, @Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE BilDates b SET  b.bilAdjDueDt = :bilAdjDueDtUpdate,"
            + "  b.bilInvDt = :bilInvDtUpdate, b.bdtDateInd = :bdtDateInd "
            + " where b.bilDatesId.bilAccountId = :bilAccountId and b.bilSysDueDt = :bilSysDueDt ")
    public Integer updateRescindCashRowBySysDate(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("bdtDateInd") char bdtDateInd,
            @Param("bilAccountId") String bilAccountId, @Param("bilSysDueDt") ZonedDateTime bilSysDueDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilDates b SET  b.bilInvoiceCd = :bilInvoiceCd "
            + " where b.bilDatesId.bilAccountId = :bilAccountId and b.bilAdjDueDt = :bilAdjDueDt "
            + " and b.bilInvDt = :bilInvDt and b.bilInvoiceCd in :billInvoiceCdList")
    public Integer updateInvoiceCodeByAdjDueDate(@Param("bilInvoiceCd") char bilInvoiceCd,
            @Param("bilAccountId") String bilAccountId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select MIN( b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd in :invoiceCdList and  b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMinDueDateByInvoiceCd(@Param("bilAccountId") String bilAccountId,
            @Param("invoiceCdList") List<Character> invoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query("select MAX(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd in :bilInvoiceCdList "
            + "and b.bilDatesId.bilReferenceDt >= :bilReferenceDt and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMaxInvoiceDateByRefDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilReferenceDt") ZonedDateTime bilReferenceDt, @Param("bilExcInd") char bilExcInd);

    @Query("select MIN(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd in :bilInvoiceCdList "
            + "and b.bilDatesId.bilReferenceDt >= :bilReferenceDt and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMinInvoiceDateByRefDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilReferenceDt") ZonedDateTime bilReferenceDt, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilDatesId.bilReferenceDt in (select MIN(c.bilDatesId.bilReferenceDt) from BilDates c where "
            + " c.bilDatesId.bilAccountId = :bilAccountId and c.bilDatesId.bilReferenceDt > :bilReferenceDt) "
            + " and b.bilExcInd <> :bilExcInd ")
    public List<BilDates> fetchFlatBilDates(@Param("bilAccountId") String bilAccountId,
            @Param("bilReferenceDt") ZonedDateTime bilReferenceDt, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId "
            + " and b.bilInvDt < :bilInvDt and b.bilExcInd <> :bilExcInd "
            + " order by b.bilInvDt desc, b.bilAdjDueDt desc, b.bilInvoiceCd desc ")
    public List<BilDates> getMaxInvoicedDateByExcCd(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId "
            + " and b.bilInvDt >= :bilInvDt and b.bilInvoiceCd in :bilInvoiceCdList and "
            + " b.bilExcInd <> :bilExcInd order by b.bilAdjDueDt, b.bilInvDt ")
    public List<BilDates> getFutureInvoicedDateByInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId "
            + " and b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDt and "
            + " b.bilInvoiceCd in :bilInvoiceCdList and b.bilExcInd <> :bilExcInd "
            + " order by b.bilSysDueDt desc , b.bilDatesId.bilReferenceDt desc ")
    public List<BilDates> getFutureSystemDateByInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilSysDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd IN :billInvoiceCdList "
            + " and b.bilInvDt >= :bilInvDt and b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDtSecond and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime selectMinSysDueDateByAdjInvDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDtSecond") ZonedDateTime bilInvDtSecond,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN( b.bilDatesId.bilReferenceDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvDt = :bilInvDt and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilSysDueDt = :bilSysDueDt and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMinReferenceDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select distinct b.bilSysDueDt from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd order by b.bilSysDueDt")
    public List<ZonedDateTime> fetchDistinctSystemDateCount(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMaxInvoicedDateByInvoiceCode(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilInvoiceCd in :bilInvoiceCdList and b.bdtDateType in :bdtDateTypeList and b.bilExcInd <> :bilExcInd "
            + " order by b.bilDatesId.bilReferenceDt ")
    public List<BilDates> getAdjDueDateAndInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvDtlist,
            @Param("bdtDateTypeList") List<Character> bdtDateTypeList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bdtDateType), MIN(b.bdtDateInd) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDt and b.bilExcInd <> :bilExcInd")
    public List<Object[]> getBilDateTypeAndInd(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilInvoiceCd in :billInvoiceCdList and b.bilAdjDueDt > :bilAdjDueDt and b.bdtDateType in :bdtDateTypeList  "
            + " and b.bilExcInd <> :bilExcInd order by b.bilAdjDueDt, b.bilInvDt ")
    public List<BilDates> getMinAdjustDueDateByInvoiceCode(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bdtDateTypeList") List<Character> bdtDateTypeList,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilInvoiceCd in :billInvoiceCdList and b.bilAdjDueDt > :bilAdjDueDt and  "
            + " b.bilExcInd <> :bilExcInd order by b.bilAdjDueDt, b.bilInvDt ")
    public List<BilDates> getMinAdjustDueDateByInvoiceCode(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilInvoiceCd in :billInvoiceCdList and b.bilExcInd <> :bilExcInd "
            + " order by b.bilAdjDueDt, b.bilInvDt ")
    public List<BilDates> getMinAdjustDueDateByInvoiceCode(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilSysDueDt > :bilSysDueDt and b.bilAdjDueDt <= :bilAdjDueDt and b.bdtDateType = :bdtDateType and "
            + " b.bdtDateInd <> :bdtDateInd and b.bilInvoiceCd in :billInvoiceCdList and b.bilExcInd <> :bilExcInd "
            + " order by b.bilDatesId.bilReferenceDt desc ")
    public List<BilDates> getMaxLaddReferenceDateByInvoiceCode(@Param("bilAccountId") String bilAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bdtDateType") char bdtDateType, @Param("bdtDateInd") char bdtDateInd,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + "b.bdtDateInd <> :bdtDateInd and b.bilInvoiceCd IN :billInvoiceCdList and b.bilAdjDueDt = :bilAdjDueDt and b.bilSysDueDt <> b.bilAdjDueDt"
            + " and b.bilExcInd <> :bilExcInd")
    public List<BilDates> findAdjustDateReset(@Param("bilAccountId") String bilAccountId,
            @Param("bdtDateInd") char bdtDateInd, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilSysDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bdtDateType = :bdtDateType "
            + " and b.bilAdjDueDt = :bilAdjDueDt and b.bilInvoiceCd IN :billInvoiceCdList "
            + "  and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMinSysDueDtBybilAdjDueDt(@Param("bilAccountId") String bilAccountId,
            @Param("bdtDateType") char bdtDateType, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bdtDateType = :bdtDateType "
            + " and b.bilAdjDueDt >= :bilAdjDueDt and b.bilInvoiceCd IN :billInvoiceCdList "
            + "  and b.bilExcInd <> :bilExcInd order by bilSysDueDt asc, b.bilAdjDueDt asc, b.bilInvDt asc")
    public List<BilDates> getMinSysDueDtDatebilAdjDueDt(@Param("bilAccountId") String bilAccountId,
            @Param("bdtDateType") char bdtDateType, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bdtDateType = :bdtDateType "
            + " and b.bilInvoiceCd in :bilInvoiceCdList and b.bilExcInd <> :bilExcInd order by b.bilAdjDueDt asc, b.bilInvDt asc, b.bilSysDueDt asc")
    public List<BilDates> getMinAdjustDueDateForInvoice(@Param("bilAccountId") String bilAccountId,
            @Param("bdtDateType") char bdtDateType, @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilExcInd") char bilExcInd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilDates b set b.bilAdjDueDt = :billAdjDueDt ,b.bilInvDt = :bilInvDt, "
            + " b.bilSysDueDt = :billSysDueDt, b.bdtDateInd = :bdtDateInd "
            + " where b.bilDatesId.bilAccountId = :billAccountId and b.bilSysDueDt = :bilSysDueDt1")
    public void updatAdjInvSysDates(@Param("billAdjDueDt") ZonedDateTime billAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("bdtDateInd") char bdtDateInd, @Param("billAccountId") String billAccountId,
            @Param("bilSysDueDt1") ZonedDateTime bilSysDueDt1);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId "
            + " and b.bilInvDt < :bilInvDt and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd "
            + " order by b.bilInvDt desc,  b.bilAdjDueDt desc ")
    public List<BilDates> getMaxInvoicedDateByInvoiceCdList(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilInvDt < :bilInvDt and b.bilInvoiceCd in :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getNextMinAdjDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvoiceCd IN :billInvoiceCdList "
            + " and b.bdtDateType = :bdtDateType and b.bilInvDt >= :bilInvDt and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMaxInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bdtDateType") char bdtDateType,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilInvoiceCd in :billInvoiceCdList and b.bilAdjDueDt <= :bilAdjDueDt "
            + " and b.bilExcInd <> :bilExcInd")
    public List<BilDates> getBilDatesByInvoiceCode(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where "
            + " b.bilDatesId.bilAccountId = :bilAccountId and b.bilInvDt = :bilInvDt and b.bilAdjDueDt > :bilAdjDueDt"
            + " and b.bilInvoiceCd in :bilInvoiceCdList "
            + "and b.bdtDateType <> :bdtDateType and b.bilExcInd <> :bilExcInd order by b.bilAdjDueDt")
    public List<BilDates> getEffectiveInvoice(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvoiceCdList") List<Character> bilInvDtlist, @Param("bdtDateType") char bdtDateType,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilSysDueDt = :bilSysDueDt and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMinInvoicedDateBySystemDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMinInvoicedDateByAdjustDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bilAdjDueDt >= :bilAdjDueDt and b.bilAdjDueDt >= :earliestEftDueDt and b.bilInvoiceCd in :bilInvoiceCdList and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMinAdjustDateByEarliestEftDueDt(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("earliestEftDueDt") ZonedDateTime earliestEftDueDt,
            @Param("bilInvoiceCdList") List<Character> bilInvDtlist, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bilAdjDueDt > :bilAdjDueDt and b.bilAdjDueDt >= :earliestEftDueDt and b.bilInvoiceCd in :bilInvoiceCdList and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMinAdjustDateByEarliestEftDueDt1(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("earliestEftDueDt") ZonedDateTime earliestEftDueDt,
            @Param("bilInvoiceCdList") List<Character> bilInvDtlist, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilSysDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bilAdjDueDt >= :bilAdjDueDt and b.bilAdjDueDt >= :earliestEftDueDt and b.bilInvoiceCd in :bilInvoiceCdList and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMinSysDueDateByEarliestEftDueDt(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("earliestEftDueDt") ZonedDateTime earliestEftDueDt,
            @Param("bilInvoiceCdList") List<Character> bilInvDtlist, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilDatesId.bilReferenceDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + "b.bilInvoiceCd in :billInvoiceCdList and b.bdtDateType = :bdtDateType and  b.bilAdjDueDt <= b.bilSysDueDt and "
            + "b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMinReferenceDateForCheckFpdd(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bdtDateType") char bdtDateType,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilDatesId.bilReferenceDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + "b.bilSysDueDt <= :bilSysDueDt and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMaxReferenceDateBySysDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilDatesId.bilReferenceDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + "(b.bilSysDueDt < :lookAheadDate or b.bilSysDueDt <= :fpddDate) and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMaxReferenceDateByFpddDate(@Param("bilAccountId") String bilAccountId,
            @Param("lookAheadDate") ZonedDateTime lookAheadDate, @Param("fpddDate") ZonedDateTime fpddDate,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilDatesId.bilReferenceDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + "b.bilDatesId.bilReferenceDt >= :bilReferenceDt and b.bdtDateType = :bdtDateType and "
            + "b.bilInvoiceCd IN :billInvoiceCdList and b.bilAdjDueDt < b.bilSysDueDt and "
            + "b.bilAdjDueDt = (select MAX(c.bilAdjDueDt) from BilDates c where c.bilDatesId.bilAccountId = :accountId and "
            + "c.bilInvoiceCd IN :billInvoiceCdList1) and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMaxReferenceDateForFpddEffectiveCheck(@Param("bilAccountId") String bilAccountId,
            @Param("bilReferenceDt") ZonedDateTime bilReferenceDt, @Param("bdtDateType") char bdtDateType,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("accountId") String accountId,
            @Param("billInvoiceCdList1") List<Character> billInvoiceCdList1, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select distinct b.bilAdjDueDt,b.bilInvDt from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilSysDueDt < :bilSysDueDt"
            + " and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public List<Object[]> fetchDistinctAdjustDateAndInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select distinct b.bilAdjDueDt,b.bilInvDt from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and b.bilSysDueDt <= :bilSysDueDt"
            + " and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public List<Object[]> fetchDistinctAdjustDateAndInvoiceDatebyFpdd(@Param("bilAccountId") String bilAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilSysDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId "
            + " and b.bilSysDueDt <= :bilSysDueDt and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMinSysDueDtBybilSysDueDt(@Param("bilAccountId") String bilAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select distinct b.bilAdjDueDt from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd order by b.bilAdjDueDt")
    public List<ZonedDateTime> fetchDistinctAdjustDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId "
            + "and b.bilAdjDueDt > :bilAdjDueDt and b.bilInvoiceCd in :billInvoiceCdList and b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getAdjustDueDateByBillInvoiceCd(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilInvDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId "
            + " and b.bilAdjDueDt = :bilAdjDueDt and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMinInvDateByAdjDueDt(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilSysDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId "
            + " and b.bilInvDt >= :bilInvDt and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd ")
    public ZonedDateTime getMinSysDueDtByBilInvDt(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilDatesId.bilReferenceDt < :bilReferenceDt and b.bilInvDt <> :bilInvDt and b.bilInvoiceCd IN :billInvoiceCdList and  "
            + " b.bilExcInd <> :bilExcInd order by b.bilDatesId.bilReferenceDt ")
    public List<BilDates> findEftPastReferenceData(@Param("bilAccountId") String bilAccountId,
            @Param("bilReferenceDt") ZonedDateTime bilReferenceDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilAdjDueDt < :bilAdjDueDt and b.bilInvoiceCd not in :billInvoiceCdList and "
            + " b.bilInvoiceCd <> :bilInvoiceCd and b.bilExcInd = :bilExcInd "
            + " order by b.bilDatesId.bilReferenceDt ")
    public List<BilDates> getCleanupSeasonalData(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvoiceCd") char bilInvoiceCd,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bilInvoiceCd) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId "
            + " and b.bilExcInd <> :bilExcInd ")
    public Character getMaxInvoicedCode(@Param("bilAccountId") String bilAccountId, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilDatesId.bilReferenceDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + "b.bilDatesId.bilReferenceDt >= :bilReferenceDt and b.bilInvoiceCd in :billInvoiceCdList and b.bdtDateType <> :bdtDateType and  "
            + "b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMinReferenceDateForAssignProcess(@Param("bilAccountId") String bilAccountId,
            @Param("bilReferenceDt") ZonedDateTime bilReferenceDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bdtDateType") char bdtDateType,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MIN(b.bilDatesId.bilReferenceDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + "b.bilDatesId.bilReferenceDt >= :bilReferenceDt and b.bilDatesId.bilReferenceDt >= :bilReferenceDate and b.bilInvoiceCd in :billInvoiceCdList and b.bdtDateType <> :bdtDateType and  "
            + "b.bilExcInd <> :bilExcInd")
    public ZonedDateTime getMinReferenceDateForCheckRefDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilReferenceDt") ZonedDateTime bilReferenceDt,
            @Param("bilReferenceDate") ZonedDateTime bilReferenceDate,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bdtDateType") char bdtDateType,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bilDatesId.bilReferenceDt = :bilReferenceDt and b.bilInvoiceCd in :billInvoiceCdList and b.bdtDateType <> :bdtDateType"
            + " and b.bilExcInd <> :bilExcInd")
    public BilDates getBilDatesByReferenceDt(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilReferenceDt") ZonedDateTime bilReferenceDt, @Param("bdtDateType") char bdtDateType,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId"
            + " and b.bilDatesId.bilReferenceDt > :bilReferenceDt and b.bilInvoiceCd in :billInvoiceCdList and b.bdtDateType <> :bdtDateType"
            + " and b.bilExcInd <> :bilExcInd order by b.bilDatesId.bilAccountId, b.bilDatesId.bilReferenceDt")
    public List<BilDates> fetchBilDatesForCheckBdtFpdd(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilReferenceDt") ZonedDateTime bilReferenceDt, @Param("bdtDateType") char bdtDateType,
            @Param("bilExcInd") char bilExcInd);

    @Query(value = "select MAX(b.bdtDateType) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDt and b.bilInvoiceCd = :bilInvoiceCd "
            + " and b.bilExcInd <> :bilExcInd")
    public Character getBilDateType(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bilInvoiceCd") char bilInvoiceCd, @Param("bilExcInd") char bilExcInd);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilInvoiceCd IN :billInvoiceCdList and b.bilExcInd <> :bilExcInd "
            + " order by b.bilSysDueDt desc, b.bilDatesId.bilReferenceDt desc ")
    public List<BilDates> getMaxSystemDueDateByInvoiceCode(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilDates b SET  b.bilAdjDueDt = :bilAdjDueDtUpdate,"
            + "  b.bilInvDt = :bilInvDtUpdate, b.bdtDateInd = :bdtDateInd, b.bdtDateType = :bdtDateType, "
            + " b.bdtNoncpiInd = :bdtNoncpiInd, b.bilInvoiceCd = :bilInvoiceCd "
            + " where b.bilDatesId.bilAccountId = :bilAccountId and b.bilAdjDueDt = :bilAdjDueDt "
            + " and b.bilInvDt = :bilInvDt ")
    public Integer updateOneDateBilDatesByAdjDueDate(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("bdtDateInd") char bdtDateInd,
            @Param("bdtDateType") char bdtDateType, @Param("bdtNoncpiInd") char bdtNoncpiInd,
            @Param("bilInvoiceCd") char bilInvoiceCd, @Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilDates b SET  b.bilAdjDueDt = :bilAdjDueDtUpdate,"
            + "  b.bilInvDt = :bilInvDtUpdate, b.bdtDateInd = :bdtDateInd,  "
            + " b.bdtNoncpiInd = :bdtNoncpiInd, b.bilInvoiceCd = :bilInvoiceCd "
            + " where b.bilDatesId.bilAccountId = :bilAccountId and b.bilAdjDueDt = :bilAdjDueDt "
            + " and b.bilInvDt = :bilInvDt ")
    public Integer updateOneDateBilDatesByNewAdjDueDate(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("bdtDateInd") char bdtDateInd,
            @Param("bdtNoncpiInd") char bdtNoncpiInd, @Param("bilInvoiceCd") char bilInvoiceCd,
            @Param("bilAccountId") String bilAccountId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Query(value = "select b from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId "
            + " and b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDt and "
            + " b.bilInvoiceCd = :bilInvoiceCd and b.bdtDateType = :bdtDateType and " + " b.bilExcInd <> :bilExcInd ")
    public List<BilDates> getSystemDateByInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bilInvoiceCd") char bilInvoiceCd, @Param("bdtDateType") char bdtDateType,
            @Param("bilExcInd") char bilExcInd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "delete BilDates b where b.bilDatesId.bilAccountId = :bilAccountId and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDt and " + " b.bdtDateType = :bdtDateType ")
    public Integer deleteRowsByDateType(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bdtDateType") char bdtDateType);

    @Query(value = "select MAX(b.bilInvDt), MAX(b.bilAdjDueDt) from BilDates b where b.bilDatesId.bilAccountId = :bilAccountId  "
            + " and b.bilInvoiceCd in :billInvoiceCdList and b.bilSysDueDt = :bilSysDueDt and b.bilExcInd <> :bilExcInd ")
    public List<Object[]> getMaxDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("bilExcInd") char bilExcInd);

}
