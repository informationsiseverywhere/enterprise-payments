package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilCashEntryTot;
import billing.data.entity.id.BilCashEntryTotId;

public interface BilCashEntryTotRepository extends CrudRepository<BilCashEntryTot, BilCashEntryTotId> {

    @Query(value = "select b from BilCashEntryTot b where b.bilCashEntryTotId.userId = :userId and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') "
            + "order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc")
    public Page<BilCashEntryTot> getBilCashEntryTotRowsByPaging(@Param("userId") String userId, Pageable pageable);

    @Query(value = "select b from BilCashEntryTot b where b.bilCashEntryTotId.bilEntryDt between :beginDt and :endDt and b.bilCashEntryTotId.userId = :userId and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') and b.currencyCd in :currency "
            + "order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc ")
    public Page<BilCashEntryTot> getBilCashEntryTotRowsWithDateRangeAndCurrency(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("userId") String userId, @Param("currency") List<String> currency, Pageable pageable);
    
    @Query(value = "select b from BilCashEntryTot b where b.bilCashEntryTotId.bilEntryDt between :beginDt and :endDt and b.bilCashEntryTotId.userId = :userId and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') "
            + "order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc ")
    public Page<BilCashEntryTot> getBilCashEntryTotRowsWithDateRange(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("userId") String userId, Pageable pageable);

    @Query(value = "select b from BilCashEntryTot b where b.bctTotCashAmt between :beginAmount and :endAmount and b.bilCashEntryTotId.userId = :userId "
            + "and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc")
    public Page<BilCashEntryTot> getBilCashEntryTotRowsWithAmountRange(@Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("userId") String userId, Pageable pageable);
    
    @Query(value = "select b from BilCashEntryTot b where b.bctTotCashAmt between :beginAmount and :endAmount and b.bilCashEntryTotId.userId = :userId "
            + "and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') and b.currencyCd in :currency order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc")
    public Page<BilCashEntryTot> getBilCashEntryTotRowsWithAmountRangeAndCurrency(@Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("userId") String userId, @Param("currency") List<String> currency, Pageable pageable);

    @Query(value = "select b from BilCashEntryTot b where b.bilCashEntryTotId.bilEntryDt between :beginDt and :endDt and b.bctTotCashAmt between :beginAmount and :endAmount and b.bilCashEntryTotId.userId = :userId "
            + "and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc ")
    public Page<BilCashEntryTot> getRowsWithDateRangeAmountRange(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("userId") String userId, Pageable pageable);
    
    @Query(value = "select b from BilCashEntryTot b where b.bilCashEntryTotId.bilEntryDt between :beginDt and :endDt and b.bctTotCashAmt between :beginAmount and :endAmount and b.bilCashEntryTotId.userId = :userId "
            + "and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') and b.currencyCd in :currency order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc ")
    public Page<BilCashEntryTot> getRowsWithDateRangeAmountRangeAndCurrency(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("userId") String userId, @Param("currency") List<String> currency, Pageable pageable);

    public long countByBilCashEntryTotIdUserId(String userId);

    @Query(value = "select COUNT(b) from BilCashEntryTot b where b.bilCashEntryTotId.bilEntryDt = :bilEntryDt and b.bilCashEntryTotId.bilEntryNbr = :bilEntryNbr and "
            + "b.bilCashEntryTotId.userId = :userId")
    public Integer getBilEntryNumber(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNbr") String bilEntryNbr, @Param("userId") String userId);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashEntryTot b SET b.bctAcceptInd = 'Y' WHERE b.bilCashEntryTotId.bilEntryDt = :bilEntryDt and b.bilCashEntryTotId.bilEntryNbr = :bilEntryNbr and "
            + "b.bilCashEntryTotId.userId = :userId")
    public void updateRow(@Param("bilEntryDt") ZonedDateTime bilEntryDt, @Param("bilEntryNbr") String bilEntryNbr,
            @Param("userId") String userId);
    
    @Query(value = "select b from BilCashEntryTot b where b.bilCashEntryTotId.bilEntryDt between :beginDt and :endDt and b.bctTotCashAmt between :beginAmount and :endAmount "
            + "and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') and b.currencyCd in :currency order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc ")
    public Page<BilCashEntryTot> getRowsWithDateRangeAmountRangeAndCurrency(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("currency") List<String> currency, Pageable pageable);
    
    @Query(value = "select b from BilCashEntryTot b where b.bilCashEntryTotId.bilEntryDt between :beginDt and :endDt and b.bctTotCashAmt between :beginAmount and :endAmount "
            + "and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc ")
    public Page<BilCashEntryTot> getRowsWithDateRangeAmountRange(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, Pageable pageable);
    
    @Query(value = "select b from BilCashEntryTot b where b.bctTotCashAmt between :beginAmount and :endAmount "
            + "and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') and b.currencyCd in :currency order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc")
    public Page<BilCashEntryTot> getBilCashEntryTotRowsWithAmountRangeAndCurrency(@Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("currency") List<String> currency, Pageable pageable);
    
    @Query(value = "select b from BilCashEntryTot b where b.bctTotCashAmt between :beginAmount and :endAmount "
            + "and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc")
    public Page<BilCashEntryTot> getBilCashEntryTotRowsWithAmountRange(@Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, Pageable pageable);
    
    @Query(value = "select b from BilCashEntryTot b where b.bilCashEntryTotId.bilEntryDt between :beginDt and :endDt and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') and b.currencyCd in :currency "
            + "order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc ")
    public Page<BilCashEntryTot> getBilCashEntryTotRowsWithDateRangeAndCurrency(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("currency") List<String> currency, Pageable pageable);

    @Query(value = "select b from BilCashEntryTot b where b.bilCashEntryTotId.bilEntryDt between :beginDt and :endDt and b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') "
            + "order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc ")
    public Page<BilCashEntryTot> getBilCashEntryTotRowsWithDateRange(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, Pageable pageable);
    
    @Query(value = "select b from BilCashEntryTot b where b.bctDepositInd = 'N' and b.bilCshEtrMthCd NOT IN ('8','6','H') "
            + "order by b.bilCashEntryTotId.bilEntryDt desc, b.bilCashEntryTotId.bilEntryNbr desc")
    public Page<BilCashEntryTot> getBilCashEntryTotRowsByPaging(Pageable pageable);

    @Query(value = "select b from BilCashEntryTot b where b.bilCashEntryTotId.bilEntryDt = :bilEntryDt and b.bilCashEntryTotId.bilEntryNbr = :bilEntryNbr and "
            + "b.bilCashEntryTotId.userId = :userId and b.bctAcceptInd = :bctAcceptInd")
    public BilCashEntryTot findBilCashEntryTotal(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNbr") String bilEntryNbr, @Param("userId") String userId,
            @Param("bctAcceptInd") char bctAcceptInd);
}
