package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilPolicyTerm;
import billing.data.entity.id.BilPolicyTermId;

public interface BilPolicyTermRepository extends JpaRepository<BilPolicyTerm, BilPolicyTermId> {

    @Query("select DISTINCT b.bilPolicyTermId.bilAccountId from BilPolicyTerm b, BilPolicy c where b.bilPolicyTermId.bilAccountId = c.bilPolicyId.bilAccountId and "
            + " b.bilPolicyTermId.policyId = c.bilPolicyId.policyId and b.billPolStatusCd not in :billPolStatusCdList and "
            + " c.bilIssueInd in :bilIssueIndList and c.polNbr = :polNbr and c.polSymbolCd = :polSymbolCode")
    public List<String> findAccountByPolicy(@Param("polNbr") String polNbr,
            @Param("polSymbolCode") String polSymbolCode,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("bilIssueIndList") List<Character> bilIssueIndList, Pageable pageable);

    @Query("select COUNT(b) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + "and b.billPolStatusCd <> :billPolStatusCd")
    public Integer getPolicyRowCount(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("billPolStatusCd") char billPolStatusCd);

    @Query("select MAX(b.bilPolicyTermId.polEffectiveDt) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId ")
    public ZonedDateTime getMaxPolicyEffectiveDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + "and b.bilPolicyTermId.polEffectiveDt = :polEffectiveDt")
    public BilPolicyTerm getPolicyRow(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd = :billPolStatusCd order by b.bilPolicyTermId.polEffectiveDt desc")
    public List<BilPolicyTerm> findContractsByStatus(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCd") Character billPolStatusCd);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + "and b.bilPolicyTermId.polEffectiveDt <= :polEffectiveDt order by b.bilPolicyTermId.polEffectiveDt desc ")
    public List<BilPolicyTerm> getMaxPolicyEffectiveRow(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            Pageable pageable);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + "and b.bilPolicyTermId.polEffectiveDt > :polEffectiveDt order by b.bilPolicyTermId.polEffectiveDt asc ")
    public List<BilPolicyTerm> getMinPolicyEffectiveRow(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            Pageable pageable);

    @Modifying
    @Transactional
    @Query(value = "UPDATE BilPolicyTerm b SET b.fullPmtDscCd = :fullPmtDscCd where b.bilPolicyTermId = :bilPolicyTermId ")
    public Integer updateFullPmtDscCd(@Param("bilPolicyTermId") BilPolicyTermId bilPolicyTermId,
            @Param("fullPmtDscCd") Character fullPmtDscCd);

    @Modifying
    @Transactional
    @Query(value = "UPDATE BilPolicyTerm b SET b.bptWroPrcInd = :bptWroPrcInd1 where b.bilPolicyTermId = :bilPolicyTermId "
            + " and b.bptWroPrcInd = :bptWroPrcInd ")
    public Integer updateWroPrcIndicator(@Param("bilPolicyTermId") BilPolicyTermId bilPolicyTermId,
            @Param("bptWroPrcInd") Character bptWroPrcInd, @Param("bptWroPrcInd1") Character bptWroPrcInd1);

    @Query("select COUNT(b) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + "and b.billPolStatusCd not in :billPolStatusCdList")
    public Integer getPolicyRowCountByAccountId(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + "and b.billPolStatusCd not in :billPolStatusCdList order by b.bilPolicyTermId.polEffectiveDt desc ")
    public List<BilPolicyTerm> getPolicyRowsByAccountId(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Modifying
    @Transactional
    @Query(value = "update BilPolicyTerm b set b.billPolStatusCd = :statusUpdate where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + " and b.billPolStatusCd = :status and exists (select 1 from BilPolicy c where c.bilPolicyId.bilAccountId = b.bilPolicyTermId.bilAccountId "
            + " and c.bilPolicyId.policyId = b.bilPolicyTermId.policyId and c.polNbr = :polNbr)")
    public Integer updatePolicyCancelForRewrite(@Param("statusUpdate") char statusUpdate,
            @Param("bilAccountId") String bilAccountId, @Param("status") char status, @Param("polNbr") String polNbr);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd not in :billPolStatusCdList "
            + " order by b.bilPolicyTermId.polEffectiveDt, b.bilPolicyTermId.policyId ")
    public List<BilPolicyTerm> findBilPolicyByPolicyStatus(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query("select b from BilPolicyTerm b, BilPolicy c where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + " and b.bilPolicyTermId.bilAccountId = c.bilPolicyId.bilAccountId and b.bilPolicyTermId.policyId = c.bilPolicyId.policyId and "
            + " b.billPolStatusCd not in :billPolStatusCdList and b.bilPolicyTermId.polEffectiveDt <= :polEffectiveDt and c.bilIssueInd not in :bilIssueIndList "
            + " order by b.bilPolicyTermId.polEffectiveDt desc ")
    public List<BilPolicyTerm> getWriteOffPolicyRows(@Param("bilAccountId") String bilAccountId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("bilIssueIndList") List<Character> bilIssueIndList);

    @Query("select MAX( b.bilPolicyTermId.polEffectiveDt) from BilPolicyTerm b, BilPolicy c where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + " and b.bilPolicyTermId.bilAccountId = c.bilPolicyId.bilAccountId and b.bilPolicyTermId.policyId = c.bilPolicyId.policyId and "
            + " b.billPolStatusCd not in :billPolStatusCdList and b.bilPolicyTermId.polEffectiveDt <= :polEffectiveDt and c.bilIssueInd not in :bilIssueIndList")
    public ZonedDateTime getMaxWriteOffEffectiveDate(@Param("bilAccountId") String bilAccountId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("bilIssueIndList") List<Character> bilIssueIndList);

    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + "and b.bptPolColAmt <> :bptPolColAmt")
    public List<BilPolicyTerm> getCollectionCode(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bptPolColAmt") double bptPolColAmt);

    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd = :billPolStatusCd "
            + "and b.bptPolColAmt <> :bptPolColAmt")
    public List<BilPolicyTerm> fetchCollectionProcess(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCd") char billPolStatusCd, @Param("bptPolColAmt") double bptPolColAmt);

    @Modifying
    @Transactional
    @Query(value = "update BilPolicyTerm b set b.bptPolColAmt = :bptPolColAmtUpdate where b.bilPolicyTermId.bilAccountId = :bilAccountId and "
            + "b.bilPolicyTermId.policyId = :policyId and b.bptPolColAmt <> :bptPolColAmt")
    public Integer updateCollectionAmounts(@Param("bptPolColAmtUpdate") double bptPolColAmtUpdate,
            @Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bptPolColAmt") double bptPolColAmt);

    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and "
            + " b.bilPolicyTermId.policyId = :policyId and b.billPolStatusCd not in :billPolStatusCdList "
            + " order by b.bilPolicyTermId.polEffectiveDt desc")
    public List<BilPolicyTerm> getPolicyByMaxPolEffectiveDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query("select MAX(b.bilPolicyTermId.polEffectiveDt) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId")
    public ZonedDateTime getMaxPolicyEffectiveDateByAccountId(@Param("bilAccountId") String bilAccountId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE BilPolicyTerm b SET b.bptWroPrcInd = :bptWroPrcInd where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + " and b.bptWroPrcInd in :bptWroPrcIndList ")
    public Integer updateWroPrcIndicator(@Param("bilAccountId") String bilAccountId,
            @Param("bptWroPrcIndList") List<Character> bptWroPrcIndList, @Param("bptWroPrcInd") Character bptWroPrcInd);

    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId <> :policyId and "
            + " b.billPolStatusCd in :billPolStatusCdlist and b.bptActColCd = :bptActColCd")
    public List<BilPolicyTerm> getLastCollectionTerm(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billPolStatusCdlist") List<Character> billPolStatusCdlist,
            @Param("bptActColCd") char bptActColCd, Pageable pageable);

    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId and "
            + "b.billPolStatusCd in :billPolStatusCdlist order by b.bilPolicyTermId.polEffectiveDt desc")
    public List<BilPolicyTerm> findPolicyData1(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billPolStatusCdlist") List<Character> billPolStatusCdlist);

    @Modifying
    @Transactional
    @Query(value = "update BilPolicyTerm b set b.bptActColCd = :bptActColCd where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + " and b.bilPolicyTermId.policyId = :policyId")
    public Integer updateCollectionIndicator(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bptActColCd") char bptActColCd);

    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and "
            + " b.bptAgreementInd <> :bptAgreementInd and b.billPolStatusCd not in :billPolStatusCdList ")
    public List<BilPolicyTerm> getDownPaymentPolicies(@Param("bilAccountId") String bilAccountId,
            @Param("bptAgreementInd") char bptAgreementInd,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Modifying
    @Transactional
    @Query(value = "update BilPolicyTerm b set b.billPolStatusCd = :billPolStatusCd, b.billSusFuReaCd = :billSusFuReaCd, b.bptStatusEffDt = :bptStatusEffDt, b.bptPolColAmt = :bptPolColAmt, "
            + " b.bptActColCd = :bptActColCd where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId and "
            + " b.bilPolicyTermId.polEffectiveDt = :polEffectiveDt and b.billPolStatusCd in :billPolStatusCdlist ")
    public Integer updatePolicyStatus(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bptStatusEffDt") ZonedDateTime bptStatusEffDt, @Param("bptPolColAmt") double bptPolColAmt,
            @Param("billSusFuReaCd") String billSusFuReaCd, @Param("bptActColCd") char bptActColCd,
            @Param("billPolStatusCd") char billPolStatusCd,
            @Param("billPolStatusCdlist") List<Character> billPolStatusCdlist);

    @Modifying
    @Transactional
    @Query(value = "update BilPolicyTerm b set b.billPolStatusCd = :billPolStatusCd, b.billSusFuReaCd = :billSusFuReaCd, b.bptStatusEffDt = :bptStatusEffDt, b.bptPolColAmt = :bptPolColAmt, "
            + " b.bptActColCd = :bptActColCd where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId and "
            + " b.bilPolicyTermId.polEffectiveDt = :polEffectiveDt ")
    public Integer updatePolicyStatus1(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bptStatusEffDt") ZonedDateTime bptStatusEffDt, @Param("bptPolColAmt") double bptPolColAmt,
            @Param("billSusFuReaCd") String billSusFuReaCd, @Param("bptActColCd") char bptActColCd,
            @Param("billPolStatusCd") char billPolStatusCd);

    @Modifying
    @Transactional
    @Query(value = "update BilPolicyTerm b set b.bptWroPrcInd = :bptWroPrcInd where b.bilPolicyTermId = :bilPolicyTermId ")
    public Integer updateWroProcessIndicator(@Param("bilPolicyTermId") BilPolicyTermId bilPolicyTermId,
            @Param("bptWroPrcInd") char bptWroPrcInd);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + "and b.billPolStatusCd not in :billPolStatusCdList ORDER BY b.bptIssueSysId")
    public List<BilPolicyTerm> getPolicyRowByAccountId(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId order by b.bilPolicyTermId.polEffectiveDt desc")
    public List<BilPolicyTerm> getPolicyRowByAccountIdAndPolicyId(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId);

    @Query("select COUNT(b) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + "and b.bptAgreementInd <> :agreementIndicator")
    public Short findPolicyByAccountId(@Param("bilAccountId") String bilAccountId,
            @Param("agreementIndicator") char agreementIndicator);

    @Query(value = "SELECT COUNT(b) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId")
    public Integer checkIfExistsByBilAccountId(@Param("bilAccountId") String bilAccountId);

    @Query(value = "select MIN(b.plnExpDt) from BilPolicyTerm b where "
            + " b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd not in :billPolStatusCdList  and b.plnExpDt >= :plnExpDt and"
            + " b.bptWroPrcInd = :bptWroPrcInd and  b.bptAgreementInd <> :bptAgreementInd")
    public ZonedDateTime getMinPlnExpDates(@Param("bilAccountId") String billAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("plnExpDt") ZonedDateTime plnExpDt, @Param("bptWroPrcInd") char bptWroPrcInd,
            @Param("bptAgreementInd") char bptAgreementInd);

    @Query(value = "select MIN(b.bptStatusEffDt) from BilPolicyTerm b where "
            + " b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd not in :billPolStatusCdList and b.bptStatusEffDt >= :bptStatusEffDt and"
            + " b.bptWroPrcInd = :bptWroPrcInd and  b.bptAgreementInd <> :bptAgreementInd")
    public ZonedDateTime getMinStatusEffDates(@Param("bilAccountId") String billAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("bptStatusEffDt") ZonedDateTime bptStatusEffDt, @Param("bptWroPrcInd") char bptWroPrcInd,
            @Param("bptAgreementInd") char bptAgreementInd);

    @Query(value = "select COUNT(b) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId  "
            + " and b.billPolStatusCd not in :billPolStatusCdList and b.plnExpDt > :plnExpDt ")
    public Integer getPolicStatusCount(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("plnExpDt") ZonedDateTime plnExpDt);

    @Query(value = "select MAX(b.billPlanCd) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd not in ('T','W','D','G','V')")
    public String getMaxBilPlanCdByAccountId(@Param("bilAccountId") String bilAccountId);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd "
            + "in :billPolStatusCdList order by b.bilPolicyTermId.policyId, b.bilPolicyTermId.polEffectiveDt ")
    public List<BilPolicyTerm> findCancelPolicyByBilAccountId(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query("select MAX(b.bilPolicyTermId.policyId) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + "and b.billPolStatusCd not in :billPolStatusCdList")
    public String getMaxPolicyIdForPayee(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query(value = "Select COUNT(DISTINCT b.bilPolicyTermId.bilAccountId) from BilPolicyTerm b where b.bilPolicyTermId.policyId = :policyId and b.bilPolicyTermId.polEffectiveDt = :polEffectiveDt "
            + " and b.billPolStatusCd <> :billPolStatusCd")
    public Short countBilPolicyTermByStatus(@Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("billPolStatusCd") char billPolStatusCd);

    @Query(value = "Select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId and "
            + "b.bilPolicyTermId.polEffectiveDt = :polEffectiveDt and b.billPolStatusCd not in :billPolStatusCdList")
    public BilPolicyTerm findRenewalTermIsPresent(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query("select distinct(b.bilPolicyTermId.bilAccountId) from BilPolicyTerm b where b.bilPolicyTermId.policyId = :policyId "
            + " and b.bilPolicyTermId.polEffectiveDt = :polEffectiveDt ")
    public List<String> getDistinctBilAccountIdByEffDate(@Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);

    @Query(value = "select MIN(b.bilPolicyTermId.polEffectiveDt) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + "and b.bilPolicyTermId.policyId = :policyId and b.bilPolicyTermId.polEffectiveDt >= :polEffectiveDt "
            + "and b.bptAgreementInd = :bptAgreementInd ")
    public ZonedDateTime getMinPolicyEffDateByAgreement(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bptAgreementInd") char bptAgreementInd);

    @Query(value = "select MAX(b.bilPolicyTermId.polEffectiveDt) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + "and b.bilPolicyTermId.policyId = :policyId and b.bilPolicyTermId.polEffectiveDt <= :polEffectiveDt and b.plnExpDt > :plnExpDt "
            + "and b.bptAgreementInd = :bptAgreementInd ")
    public ZonedDateTime getMaxPolicyEffDateByAgreement(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("plnExpDt") ZonedDateTime plnExpDt, @Param("bptAgreementInd") char bptAgreementInd);

    @Query(value = "select MAX(b.bilPolicyTermId.polEffectiveDt) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + "and b.bilPolicyTermId.policyId = :policyId and b.bilPolicyTermId.polEffectiveDt <= :polEffectiveDt and b.plnExpDt >= :plnExpDt "
            + "and b.bptAgreementInd = :bptAgreementInd and b.billPolStatusCd NOT IN :billPolStatusCdlist")
    public ZonedDateTime getMaxPolicyEffDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("plnExpDt") ZonedDateTime plnExpDt, @Param("bptAgreementInd") char bptAgreementInd,
            @Param("billPolStatusCdlist") List<Character> billPolStatusCdlist);

    @Query(value = "select MIN(b.bilPolicyTermId.polEffectiveDt) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + "and b.bilPolicyTermId.policyId = :policyId and b.bilPolicyTermId.polEffectiveDt > :polEffectiveDt and b.plnExpDt > :plnExpDt "
            + "and b.bptAgreementInd = :bptAgreementInd and b.billPolStatusCd NOT IN :billPolStatusCdlist")
    public ZonedDateTime getMinPolicyEffDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("plnExpDt") ZonedDateTime plnExpDt, @Param("bptAgreementInd") char bptAgreementInd,
            @Param("billPolStatusCdlist") List<Character> billPolStatusCdlist);

    @Query(value = "Select b from BilPolicyTerm b where b.bilPolicyTermId.policyId = :policyId and b.bilPolicyTermId.polEffectiveDt = :polEffectiveDt "
            + " and b.billPolStatusCd = :billPolStatusCd")
    public List<BilPolicyTerm> findBilPolicyTermByStatus(@Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("billPolStatusCd") char billPolStatusCd);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + " and b.bilPolicyTermId.polEffectiveDt=:polEffectiveDt and b.billPolStatusCd not in :billPolStatusCdList")
    public BilPolicyTerm findSuspendActivityData(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query(value = "select MAX(b.plnExpDt) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + "and b.billPolStatusCd = :billPolStatusCd")
    public ZonedDateTime getMaxCollectionPolicyExpiDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billPolStatusCd") char billPolStatusCd);

    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + "and b.plnExpDt = :planExpDate")
    public BilPolicyTerm getPolicyCollectionAmount(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("planExpDate") ZonedDateTime planExpDate);

    @Query(value = "select distinct b.bilPolicyTermId.policyId from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + " and b.bilPolicyTermId.policyId <> :policyId and b.billPolStatusCd in :billPolStatusCdList and b.bptActColCd = :actColCode")
    public String getChargePolicyId(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("actColCode") char actColCode, @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query(value = "SELECT MAX(b.bilPolicyTermId.polEffectiveDt) FROM BilPolicyTerm b WHERE b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + " and b.bilPolicyTermId.policyId = :policyId and b.billPolStatusCd IN :billPolStatusCdlist")
    public ZonedDateTime getMaxChargePolicyEffDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billPolStatusCdlist") List<Character> billPolStatusCdlist);

    @Query("SELECT b FROM BilPolicyTerm b WHERE b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId ORDER BY "
            + "b.bilPolicyTermId.polEffectiveDt DESC")
    public List<BilPolicyTerm> findPolicyDetails(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId);

    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + " and b.billPolStatusCd NOT IN :billPolStatusCdList")
    public List<BilPolicyTerm> getPolicyBalance(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId <> :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + "and b.bilPolicyTermId.polEffectiveDt = :polEffectiveDt and b.billPolStatusCd NOT IN :billPolStatusCdList")
    public List<BilPolicyTerm> fetchSplitPoliciesRow(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + " and b.bilPolicyTermId.polEffectiveDt = :polEffectiveDt and b.plnExpDt = :planExpDate")
    public BilPolicyTerm getPendingCancelTerm(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("planExpDate") ZonedDateTime planExpDate);

    @Query(value = "select b.billStatePvnCd from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd not in :billPolStatusCdlist and "
            + "b.bilPolicyTermId.polEffectiveDt in (select MAX(b.bilPolicyTermId.polEffectiveDt) from BilPolicyTerm b where "
            + "b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd not in :billPolStatusCdlist) and b.bilPolicyTermId.bilAccountId = :bilAccountId and "
            + "b.billPolStatusCd not in :billPolStatusCdlist")
    public String findStateByMaxPolicyEffectiveDate(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdlist") List<Character> billPolStatusCdlist);

    @Query(value = "select count(*) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd not in :billPolStatusCdlist and "
            + "b.bptReDtbCd = :bptReDtbCd")
    public Integer findCountByRedistributeCode(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdlist") List<Character> billPolStatusCdlist, @Param("bptReDtbCd") char bptReDtbCd);

    @Query(value = "select MAX(b.plnExpDt) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId")
    public ZonedDateTime getMaxPlanExpirationDate(@Param("bilAccountId") String bilAccountId);

    @Query(value = "select count(b) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd in :billPolStatusCdlist")
    public Integer checkCancelledPolicyExistance(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdlist") List<Character> billPolStatusCdlist);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd in :billPolStatusCdList "
            + " and exists (select 1 from BilIstSchedule c where c.bilIstScheduleId.billAccountId = b.bilPolicyTermId.bilAccountId "
            + " and c.bilIstScheduleId.policyId = b.bilPolicyTermId.policyId and c.policyEffectiveDt = b.bilPolicyTermId.polEffectiveDt "
            + " and c.billSysDueDt = :billSysDueDt and c.billInvoiceCd in :billInvoiceCdList ) ")
    public List<BilPolicyTerm> getTermCancelReset(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query("select MAX(b.bilPolicyTermId.policyId) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + "and b.bilPolicyTermId.polEffectiveDt = :polEffectiveDt and b.billPolStatusCd not in :billPolStatusCdList")
    public String getMaxPolicyIdByEffDate(@Param("bilAccountId") String bilAccountId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd not in :billPolStatusCdList "
            + " and b.bilPolicyTermId.policyId in (select c.bilIstScheduleId.policyId from BilIstSchedule c where "
            + " c.bilIstScheduleId.billAccountId = :bilAccountId and c.billAdjDueDt = :billAdjDueDt ) ")
    public List<BilPolicyTerm> evaluateCheckDate(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt);

    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd in :billPolStatusCdList"
            + " and b.bptStatusEffDt = :bptStatusEffDt ")
    public List<BilPolicyTerm> fetchBilPolicyTermByStatusEffective(@Param("bilAccountId") String billAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("bptStatusEffDt") ZonedDateTime bptStatusEffDt);

    @Query("select MIN(b.billPolStatusCd) as minStatus, MAX(b.billPolStatusCd) as maxStatus from BilPolicyTerm b where "
            + " b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd in :billPolStatusCdList and "
            + " b.bptStatusEffDt = :bptStatusEffDt ")
    public List<Object[]> getMaxMinPolicyStatusByEffDate(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("bptStatusEffDt") ZonedDateTime bptStatusEffDt);

    @Query("select MIN(b.billPolStatusCd) as minStatus, MAX(b.billPolStatusCd) as maxStatus from BilPolicyTerm b where "
            + " b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd in :billPolStatusCdList "
            + " and exists (select c from BilIstSchedule c where c.bilIstScheduleId.billAccountId = b.bilPolicyTermId.bilAccountId "
            + " and c.bilIstScheduleId.policyId = b.bilPolicyTermId.policyId and c.policyEffectiveDt = b.bilPolicyTermId.polEffectiveDt "
            + " and c.billSysDueDt = :billSysDueDt and c.billInvoiceCd in :billInvoiceCdList ) ")
    public List<Object[]> getMaxMinPolicyStatus(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select count(b) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bptWroPrcInd <> :bptWroPrcInd")
    public Integer checkWriteoffPolicyExistance(@Param("bilAccountId") String bilAccountId,
            @Param("bptWroPrcInd") Character bptWroPrcInd);

    @Query("select COUNT(b) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId")
    public Integer countPolicyTerm(@Param("bilAccountId") String bilAccountId);
    
    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd not in ('T','Z','3','1','2','4','W','V') order by b.bilPolicyTermId.policyId,b.bilPolicyTermId.polEffectiveDt")
    public List<BilPolicyTerm> findByAccountid1(@Param("bilAccountId") String billAccountId);
    
    @Query("select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId and b.billPolStatusCd not in ('T','Z','3','1','2','4') order by b.bilPolicyTermId.polEffectiveDt")
    public List<BilPolicyTerm> findByAccountIdPolicyId(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId);
    
    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId"
            + " and b.billPolStatusCd IN :billPolStatusCdList")
    public List<BilPolicyTerm> getPolicyTermsByStatusCode(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);
    
    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId and "
            + "b.bilPolicyTermId.polEffectiveDt = :polEffectiveDt")
    public BilPolicyTerm getPolicyTerm(@Param("bilAccountId") String billAccountId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("policyId") String policyId);
    
    @Query(value = "select count(b) from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + "and b.bilPolicyTermId.polEffectiveDt in (select MAX(b.bilPolicyTermId.polEffectiveDt) from BilPolicyTerm b where "
            + "b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId and b.bilPolicyTermId.polEffectiveDt <:polEffectiveDt) "
            + "and b.billPolStatusCd in :billPolStatusCdList ")
    public Integer getPriorTermCount(@Param("bilAccountId") String bilAccountId,@Param("policyId") String policyId, 
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("billPolStatusCdList") List<Character> billPolStatusCdList);
    
    @Query("select DISTINCT b.bilPolicyTermId.bilAccountId from BilPolicyTerm b, BilPolicy c where b.bilPolicyTermId.bilAccountId = c.bilPolicyId.bilAccountId and "
            + " b.bilPolicyTermId.policyId = c.bilPolicyId.policyId and b.billPolStatusCd not in :billPolStatusCdList and "
            + " c.bilIssueInd in :bilIssueIndList and c.polNbr = :polNbr and c.polSymbolCd = :polSymbolCode and b.bilPolicyTermId.bilAccountId = :bilAccountId")
    public List<String> findAccountByPolicy(@Param("polNbr") String polNbr,
            @Param("polSymbolCode") String polSymbolCode,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("bilIssueIndList") List<Character> bilIssueIndList, @Param("bilAccountId") String bilAccountId);
    
    @Query("select DISTINCT b.bilPolicyTermId.bilAccountId from BilPolicyTerm b, BilPolicy c where b.bilPolicyTermId.bilAccountId = c.bilPolicyId.bilAccountId and "
            + " b.bilPolicyTermId.policyId = c.bilPolicyId.policyId and b.billPolStatusCd not in :billPolStatusCdList and "
            + " c.bilIssueInd in :bilIssueIndList and c.polNbr = :polNbr and c.polSymbolCd = :polSymbolCode")
    public List<String> findAccountByPolicy(@Param("polNbr") String polNbr,
            @Param("polSymbolCode") String polSymbolCode,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("bilIssueIndList") List<Character> bilIssueIndList);
    
    @Query(value = "select b from BilPolicyTerm b, BilPolicy c where c.polNbr = :polNbr and c.bilPolicyId.bilAccountId = :bilAccountId and c.bilIssueInd in ('Y','Z') "
            + "and b.bilPolicyTermId.policyId = c.bilPolicyId.policyId and b.bilPolicyTermId.bilAccountId = c.bilPolicyId.bilAccountId and b.bilPolicyTermId.polEffectiveDt in (select MAX(b.bilPolicyTermId.polEffectiveDt) from BilPolicyTerm b, BilPolicy c "
            + "where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd not in ('W','D','G','V') and c.polNbr = :polNbr)")
    public BilPolicyTerm getBilPolicyTermByPolicyNumber(@Param("bilAccountId") String bilAccountId, @Param("polNbr") String polNbr);
    
    @Query(value = "select b from BilPolicyTerm b, BilPolicy c where c.polNbr = :polNbr and c.bilPolicyId.bilAccountId = :bilAccountId and c.bilIssueInd in ('Y','Z') and c.polSymbolCd = :polSymbolCd "
            + "and b.bilPolicyTermId.bilAccountId = c.bilPolicyId.bilAccountId and b.bilPolicyTermId.policyId = c.bilPolicyId.policyId and b.bilPolicyTermId.polEffectiveDt in (select MAX(b.bilPolicyTermId.polEffectiveDt) from BilPolicyTerm b, BilPolicy c "
            + "where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd not in ('W','D','G','V') and c.polNbr = :polNbr and c.polSymbolCd = :polSymbolCd)")
    public BilPolicyTerm getBilPolicyTermByPolicyNumberAndPolicySymbol(@Param("bilAccountId") String bilAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd);
    
    @Query(value = "SELECT MAX(b.bilPolicyTermId.polEffectiveDt) FROM BilPolicyTerm b WHERE b.bilPolicyTermId.bilAccountId = :bilAccountId "
            + " and b.bilPolicyTermId.policyId = :policyId and b.billPolStatusCd <> :billPolStatusCd")
    public ZonedDateTime getMaxPolicyEffDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billPolStatusCd") char billPolStatusCd);
    
    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.bilPolicyTermId.policyId = :policyId "
            + "and b.bptPolColAmt <> :bptPolColAmt and b.billPolStatusCd in ('I','H') and b.bptActColCd = :bptActColCd")
    public BilPolicyTerm getCollectionCodeAndAmount(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bptPolColAmt") double bptPolColAmt, @Param("bptActColCd") char bptActColCd);
    
    @Query(value = "select DISTINCT b.bilPolicyTermId.policyId from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId"
            + " and b.billPolStatusCd not IN :billPolStatusCdList")
    public String getPolicyId(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);
    
    @Query(value = "select b from BilPolicyTerm b where b.bilPolicyTermId.bilAccountId = :bilAccountId and b.billPolStatusCd not in :billPolStatusCdList "
            + "and b.bptAgreementInd <> :bptAgreementInd")
    public BilPolicyTerm getPolicyId(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList, @Param("bptAgreementInd") char bptAgreementInd);

    @Query(value = "select DISTINCT b.bilPolicyTermId.policyId from BilPolicyTerm b where b.bilPolicyTermId.policyId = :policyId and b.billPolStatusCd not in :billPolStatusCdList "
            + "and b.billStatePvnCd = :billStatePvnCd")
    public String getPolicyIdByStatusCodeAndStatePvnCode(@Param("policyId") String policyId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("billStatePvnCd") String billStatePvnCd);

}
