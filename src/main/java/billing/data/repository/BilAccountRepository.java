package billing.data.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilAccount;

public interface BilAccountRepository extends JpaRepository<BilAccount, String> {

    public BilAccount findByAccountNumber(String accountNumber);

    @Query(value = "select b from BilAccount b where b.accountId in :accountIdList and b.additionalId = :additionalId")
    public List<BilAccount> findAccountByAdditionalId(String additionalId, List<String> accountIdList);

    @Modifying
    @Transactional
    @Query(value = "update BilAccount b set b.status = :status, b.suspendedFullReasonCd = :suspendedFullReasonCd"
            + " where b.accountId = :accountId ")
    public Integer updateByAccountId(@Param("accountId") String accountId, @Param("status") char status,
            @Param("suspendedFullReasonCd") String suspendedFullReasonCd);

    public BilAccount findTopByAdditionalIdOrderByAdditionalIdDesc(String additionalId);

    @Query(value = "Select COUNT(b) from BilAccount b where b.accountId in :bilAccountList and "
            + " b.status in :statusList and b.suspendedFullReasonCd = :suspendedFullReasonCd ")
    public Integer countSplitCatSusAccount(@Param("bilAccountList") List<String> bilAccountList,
            @Param("statusList") List<Character> statusList,
            @Param("suspendedFullReasonCd") String suspendedFullReasonCd);

    @Query(value = "select b from BilAccount b where b.additionalId = :bilAdditionalId and b.accountId in (select r.bilTtyActRltId.bilAccountId from BilTtyActRlt r, BilThirdParty t "
            + "where r.bilTtyActRltId.bilThirdPartyId = t.billThirdPartyId and t.billThirdPartyId = :billThirdPartyId) ")
    public BilAccount findBilAccountByAdditionalIdAndTtyNumber(@Param("billThirdPartyId") String billThirdPartyId, @Param("bilAdditionalId") String bilAdditionalId);

}
