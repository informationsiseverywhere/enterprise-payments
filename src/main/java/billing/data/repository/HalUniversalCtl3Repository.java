package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.HalUniversalCtl3;
import billing.data.entity.id.HalUniversalCtl3Id;

public interface HalUniversalCtl3Repository extends JpaRepository<HalUniversalCtl3, HalUniversalCtl3Id> {

    @Query("select b from HalUniversalCtl3 b where b.halUniversalCtl3Id.entryKeyCd = :entryKeyCd and b.entryDataTag IN :entryDataTagList "
            + "Order by b.entryDataTag ASC")
    public HalUniversalCtl3 findByEntryKeyCdAndEntryDataTag(@Param("entryKeyCd") String entryKeyCd,
            @Param("entryDataTagList") List<String> entryDataTagList);
}
