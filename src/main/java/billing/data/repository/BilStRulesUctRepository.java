package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilStRulesUct;
import billing.data.entity.id.BilStRulesUctId;

public interface BilStRulesUctRepository extends JpaRepository<BilStRulesUct, BilStRulesUctId> {

    @Query(value = "select b from BilStRulesUct b where b.bilStRulesUctId.bilRuleId = :bilRuleId and "
            + " b.bilStRulesUctId.effectiveDt <= :effectiveDt and b.expirationDt > :expirationDt and "
            + " b.bilStRulesUctId.riskStateCd in :riskStateCdList and b.bilStRulesUctId.lobCd in :lobCdList and "
            + " b.bilStRulesUctId.masterCompanyNbr in :masterCompanyNbrList "
            + " order by b.bilStRulesUctId.riskStateCd desc, b.bilStRulesUctId.lobCd desc, b.bilStRulesUctId.masterCompanyNbr desc")
    public List<BilStRulesUct> getRulesRow(@Param("bilRuleId") String bilRuleId,
            @Param("effectiveDt") ZonedDateTime effectiveDt, @Param("expirationDt") ZonedDateTime expirationDt,
            @Param("riskStateCdList") List<String> riskStateCdList, @Param("lobCdList") List<String> lobCdList,
            @Param("masterCompanyNbrList") List<String> masterCompanyNbrList);

}
