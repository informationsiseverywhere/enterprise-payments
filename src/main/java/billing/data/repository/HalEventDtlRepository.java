package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.HalEventDtl;
import billing.data.entity.id.HalEventDtlId;

public interface HalEventDtlRepository extends JpaRepository<HalEventDtl, HalEventDtlId> {

    @Query("select b.issueSysCd from HalEventDtl b where b.halEventDtlId.halEventKeyId = :halEventKeyId and b.halEventDtlId.halEvtKeyTypCd = :halEvtKeyTypCd and "
            + "b.halEvtStatusCd = :halEvtStatusCd GROUP BY b.issueSysCd")
    public List<String> getIssueSystemList(@Param("halEventKeyId") String halEventKeyId,
            @Param("halEvtKeyTypCd") String halEvtKeyTypCd, @Param("halEvtStatusCd") char halEvtStatusCd);

    @Query("select b from HalEventDtl b where b.halEventDtlId.halEventKeyId = :halEventKeyId and b.halEventDtlId.halEvtKeyTypCd = :halEvtKeyTypCd and "
            + "b.issueSysCd = :issueSysCd and b.halEvtStatusCd = :halEvtStatusCd")
    public List<HalEventDtl> getEventDetailRows(@Param("halEventKeyId") String halEventKeyId,
            @Param("halEvtKeyTypCd") String halEvtKeyTypCd, @Param("issueSysCd") String issueSysCd,
            @Param("halEvtStatusCd") char halEvtStatusCd);

}
