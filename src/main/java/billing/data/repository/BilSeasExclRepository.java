package billing.data.repository;

import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilSeasExcl;
import billing.data.entity.id.BilSeasExclId;

public interface BilSeasExclRepository extends JpaRepository<BilSeasExcl, BilSeasExclId> {

    @Query(value = "select count(b) from BilSeasExcl b where b.bilSeasExclId.bilSeaXclCd = :bilSeaXclCd")
    public Integer getBillingItemRow(@Param("bilSeaXclCd") String bilSeaXclCd);

    @Query(value = "select b from BilSeasExcl b where b.bilSeasExclId.bilSeaXclCd = :bilSeaXclCd and b.bilSeasExclId.bilSeaStartDt <= :deductionDt "
            + " and b.bilSeaEndDt >= :deductionDt")
    public BilSeasExcl getBilSeaRowByDate(@Param("bilSeaXclCd") String bilSeaXclCd,
            @Param("deductionDt") ZonedDateTime deductionDt);

    @Query(value = "select count(b) from BilSeasExcl b where b.bilSeasExclId.bilSeaXclCd = :bilSeaXclCd and b.bilSeasExclId.bilSeaStartDt <= :bilSeaStartDt "
            + "and bilSeaEndDt >= :date")
    public Integer checkDateForExclusion(@Param("date") ZonedDateTime date, @Param("bilSeaXclCd") String bilSeaXclCd);

}
