package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.BilAgent;

public interface BilAgentRepository extends JpaRepository<BilAgent, String> {

    public BilAgent findByBilAgtActNbr(String bilAgtActNbr);

}
