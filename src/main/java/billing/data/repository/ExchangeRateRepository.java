package billing.data.repository;

import java.time.ZonedDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.ExchangeRate;
import billing.data.entity.id.ExchangeRateId;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, ExchangeRateId> {

    Page<ExchangeRate> findByExchangeRateIdExchangeDate(ZonedDateTime exchangeDate, Pageable pageable);

    Page<ExchangeRate> findByExchangeRateIdExchangeDateAndExchangeRateIdFromCurrency(ZonedDateTime exchangeDate,
            String fromCurrency, Pageable pageable);

    Page<ExchangeRate> findByExchangeRateIdExchangeDateAndExchangeRateIdToCurrency(ZonedDateTime exchangeDate,
            String toCurrency, Pageable pageable);

    Page<ExchangeRate> findByExchangeRateIdFromCurrency(String fromCurrency, Pageable pageable);

    Page<ExchangeRate> findByExchangeRateIdToCurrency(String toCurrency, Pageable pageable);

    Page<ExchangeRate> findByExchangeRateIdFromCurrencyAndExchangeRateIdToCurrency(String fromCurrency,
            String toCurrency, Pageable pageable);
}
