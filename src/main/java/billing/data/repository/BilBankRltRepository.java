package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilBankRlt;
import billing.data.entity.id.BilBankRltId;

public interface BilBankRltRepository extends CrudRepository<BilBankRlt, BilBankRltId> {

    @Query(value = "select b.bbrParentBankCd from BilBankRlt b where b.bilBankRltId.bbrBankTypeCd in :bankTypeCodeList and b.bilBankRltId.bilBankCd = :bankCode")
    public List<String> getBankCode(@Param("bankTypeCodeList") List<Character> bankTypeCodeList,
            @Param("bankCode") String bankCode);

}
