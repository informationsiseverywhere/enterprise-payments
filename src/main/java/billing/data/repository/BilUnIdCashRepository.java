package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilUnIdCashId;

public interface BilUnIdCashRepository extends JpaRepository<BilUnIdCash, BilUnIdCashId> {

    @Query(value = "select MAX(b.bilUnIdCashId.bilDtbSeqNbr) FROM BilUnIdCash b WHERE b.bilUnIdCashId.bilDtbDt= :bilDtbDt ")
    public Short findMaxSequenceNumber(@Param("bilDtbDt") ZonedDateTime bilDtbDt);

    @Query(value = "select COUNT(b) from BilUnIdCash b where b.billUnidCashId = :billUnidCashId ")
    public Integer getBillUnidCashId(@Param("billUnidCashId") String billUnidCashId);

    @Query(value = "select SUM(b.bilRctAmt) as Total, b.bilDspTypeCd from BilUnIdCash b where b.bucRevsDsbInd IN :reverseIndicatorList and b.bilDspTypeCd IN :dispositionTypeCodeList GROUP BY b.bilDspTypeCd")
    public List<Object[]> getAmountByDspTypeCode(@Param("reverseIndicatorList") List<Character> reverseIndicatorList,
            @Param("dispositionTypeCodeList") List<String> dispositionTypeCodeList);

    @Query(value = "select SUM(b.bilRctAmt) from BilUnIdCash b where b.bilUnIdCashId = :bilUnIdCashId and b.bilDspTypeCd = :bilDspTypeCd and b.bucRevsDsbInd in :reverseIndicatorList ")
    public Double getAmountByDspType(@Param("bilUnIdCashId") BilUnIdCashId bilUnIdCashId,
            @Param("bilDspTypeCd") String bilDspTypeCd,
            @Param("reverseIndicatorList") List<Character> reverseIndicatorList);

    @Query(value = "select b from BilUnIdCash b where b.bilTtyNbr =:thirdPartyNumber and  b.bilAccountNbr =:accountNumber "
            + "and b.bilAgtActNbr =:bilAgtActNbr and b.polNbr =:policyNumber and b.bilDspTypeCd =:dispositionTypeCode")
    public List<BilUnIdCash> findUnidentifiedPayment(@Param("thirdPartyNumber") String thirdPartyNumber,
            @Param("accountNumber") String accountNumber, @Param("bilAgtActNbr") String bilAgtActNbr,
            @Param("policyNumber") String policyNumber, @Param("dispositionTypeCode") String dispositionTypeCode);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilCshEtrMthCd = :cashEntryMethod "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> getSuspendedPaymentListWithPaymentMethod(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("statusList") List<String> statusList, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> getSuspendedPaymentList(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId,  @Param("statusList") List<String> statusList, 
            Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string) and "
            + "cast(:toUserId as string) and b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> getSuspendedPaymentListWithCurrency(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("currency") String currency,
            @Param("statusList") List<String> statusList, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> getSuspendedPaymentListWithPaymentMethodAndCurrency(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("currency") String currency, @Param("statusList") List<String> statusList, 
            Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilTtyNbr != :bilTtyNbr "
            + "and b.bilTtyNbr between cast(:fromIdentifier as string) " 
            + "and cast(:toIdentifier as string) and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findGroupSuspendedPayments(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilTtyNbr") String bilTtyNbr,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilTtyNbr != :bilTtyNbr "
            + "and b.bilTtyNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findGroupSuspendedPaymentsWithPaymentMethod(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("bilTtyNbr") String bilTtyNbr, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until "
            + "and b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) "
            + "and b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilTtyNbr != :bilTtyNbr "
            + "and b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr "
            + "and b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilTtyNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findGroupSuspendedPaymentsWithCurrency(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilTtyNbr") String bilTtyNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until "
            + "and b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod "
            + "and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilTtyNbr != :bilTtyNbr "
            + "and b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr "
            + "and b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilTtyNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findGroupSuspendedPaymentsWithPaymentMethodAndCurrency(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("bilTtyNbr") String bilTtyNbr, @Param("currency") String currency,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string) and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAccountNbr != :bilAccountNbr "
            + "and b.bilAccountNbr between cast(:fromIdentifier as string) "
            + "and cast(:toIdentifier as string) and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findBillAccountSuspendedPayments(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAccountNbr") String bilAccountNbr,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAccountNbr != :bilAccountNbr "
            + "and b.bilAccountNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findBillAccountSuspendedPaymentsWithPaymentMethod(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("bilAccountNbr") String bilAccountNbr, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList, 
            Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) "
            + "and b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAccountNbr != :bilAccountNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAccountNbr between cast(:fromIdentifier as string) "
            + "and cast(:toIdentifier as string) and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findBillAccountSuspendedPaymentsWithCurrency(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAccountNbr") String bilAccountNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier,@Param("statusList") List<String> statusList, 
            Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAccountNbr != :bilAccountNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAccountNbr between cast(:fromIdentifier as string) "
            + "and cast(:toIdentifier as string) and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findBillAccountSuspendedPaymentsWithPaymentMethodAndCurrency(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAccountNbr") String bilAccountNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList, 
            Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAgtActNbr != :bilAgtActNbr "
            + "and b.bilAgtActNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findAgencySuspendedPayments(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAgtActNbr") String bilAgtActNbr,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAgtActNbr != :bilAgtActNbr "
            + "and b.bilAgtActNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findAgencySuspendedPaymentsWithPaymentMethod(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("bilAgtActNbr") String bilAgtActNbr, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList, 
            Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAgtActNbr != :bilAgtActNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAgtActNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findAgencySuspendedPaymentsWithCurrency(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAgtActNbr") String bilAgtActNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList, 
            Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAgtActNbr != :bilAgtActNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAgtActNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findAgencySuspendedPaymentsWithPaymentMethodAndCurrency(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAgtActNbr") String bilAgtActNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList, 
            Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.billUnidCashId = :billUnidCashId")
    public List<BilUnIdCash> findByBillUnidCashId(@Param("billUnidCashId") String billUnidCashId);
    
    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.polNbr != :polNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.polNbr between cast(:fromPolicy as string)  and cast(:toPolicy as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findPolicySuspendedPaymentsWithCurrency(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("polNbr") String polNbr,
            @Param("currency") String currency, @Param("fromPolicy") String fromPolicy,
            @Param("toPolicy") String toPolicy, @Param("statusList") List<String> statusList, 
            Pageable pageable);
    
    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.polNbr != :polNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.polNbr between cast(:fromPolicy as string)  and cast(:toPolicy as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findPolicySuspendedPaymentsWithPaymentMethodAndCurrency(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("polNbr") String polNbr,
            @Param("currency") String currency, @Param("fromPolicy") String fromPolicy,
            @Param("toPolicy") String toPolicy, @Param("statusList") List<String> statusList, 
            Pageable pageable);
    
    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.polNbr != :polNbr "
            + "and b.polNbr between cast(:fromPolicy as string)  and cast(:toPolicy as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findPolicySuspendedPayments(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("polNbr") String polNbr,
            @Param("fromPolicy") String fromPolicy, @Param("toPolicy") String toPolicy, 
            @Param("statusList") List<String> statusList, Pageable pageable);
    
    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.polNbr != :polNbr "
            + "and b.polNbr between cast(:fromPolicy as string)  and cast(:toPolicy as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findPolicySuspendedPaymentsWithPaymentMethod(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("polNbr") String polNbr, @Param("fromPolicy") String fromPolicy, @Param("toPolicy") String toPolicy,
            @Param("statusList") List<String> statusList, Pageable pageable);
    
    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAdditionalId != :bilAdditionalId and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAdditionalId between cast(:fromIdentifier as string)  and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findAdditionalIdSuspendedPaymentsWithCurrency(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAdditionalId") String bilAdditionalId,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier,  @Param("statusList") List<String> statusList, 
            Pageable pageable);
    
    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAdditionalId != :bilAdditionalId and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAdditionalId between cast(:fromIdentifier as string)  and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findAdditionalIdSuspendedPaymentsWithPaymentMethodAndCurrency(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAdditionalId") String bilAdditionalId,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier,  @Param("statusList") List<String> statusList, 
            Pageable pageable);
    
    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAdditionalId != :bilAdditionalId "
            + "and b.bilAdditionalId between cast(:fromIdentifier as string)  and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findAdditionalIdSuspendedPayments(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAdditionalId") String bilAdditionalId,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, Pageable pageable);
    
    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAdditionalId != :bilAdditionalId "
            + "and b.bilAdditionalId between cast(:fromIdentifier as string)  and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList")
    public Page<BilUnIdCash> findAdditionalIdSuspendedPaymentsWithPaymentMethod(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("bilAdditionalId") String bilAdditionalId, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList, 
            Pageable pageable);
    
    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until "
            + "and b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) "
            + "and b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilTtyNbr != :bilTtyNbr "
            + "and b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr "
            + "and b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilTtyNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findGroupSuspendedPaymentsWithCurrencyAndCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilTtyNbr") String bilTtyNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until "
            + "and b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) "
            + "and b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilTtyNbr != :bilTtyNbr "
            + "and b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr "
            + "and b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilTtyNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findGroupSuspendedPaymentsWithCurrencyAndCAPFalse(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilTtyNbr") String bilTtyNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until "
            + "and b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod "
            + "and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilTtyNbr != :bilTtyNbr "
            + "and b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr "
            + "and b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilTtyNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findGroupSuspendedPaymentsWithPaymentMethodAndCurrencyWithCAPTrue(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilTtyNbr") String bilTtyNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until "
            + "and b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod "
            + "and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilTtyNbr != :bilTtyNbr "
            + "and b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr "
            + "and b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilTtyNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findGroupSuspendedPaymentsWithPaymentMethodAndCurrencyWithCAPFalse(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilTtyNbr") String bilTtyNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilTtyNbr != :bilTtyNbr "
            + "and b.bilTtyNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findGroupSuspendedPaymentsWithCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilTtyNbr") String bilTtyNbr,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilTtyNbr != :bilTtyNbr "
            + "and b.bilTtyNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findGroupSuspendedPaymentsWithCAPFalse(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilTtyNbr") String bilTtyNbr,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilTtyNbr != :bilTtyNbr "
            + "and b.bilTtyNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findGroupSuspendedPaymentsWithPaymentMethodAndCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("bilTtyNbr") String bilTtyNbr, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilTtyNbr != :bilTtyNbr "
            + "and b.bilTtyNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findGroupSuspendedPaymentsWithPaymentMethodAndCAPFalse(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("bilTtyNbr") String bilTtyNbr, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) "
            + "and b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAccountNbr != :bilAccountNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAccountNbr between cast(:fromIdentifier as string) "
            + "and cast(:toIdentifier as string) and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findBillAccountSuspendedPaymentsWithCurrencyAndCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAccountNbr") String bilAccountNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) "
            + "and b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAccountNbr != :bilAccountNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAccountNbr between cast(:fromIdentifier as string) "
            + "and cast(:toIdentifier as string) and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findBillAccountSuspendedPaymentsWithCurrencyAndCAPFalse(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("bilAccountNbr") String bilAccountNbr, @Param("currency") String currency,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAccountNbr != :bilAccountNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAccountNbr between cast(:fromIdentifier as string) "
            + "and cast(:toIdentifier as string) and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findBillAccountSuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPTrue(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAccountNbr") String bilAccountNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAccountNbr != :bilAccountNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAccountNbr between cast(:fromIdentifier as string) "
            + "and cast(:toIdentifier as string) and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findBillAccountSuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPFalse(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAccountNbr") String bilAccountNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string) and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAccountNbr != :bilAccountNbr "
            + "and b.bilAccountNbr between cast(:fromIdentifier as string) "
            + "and cast(:toIdentifier as string) and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findBillAccountSuspendedPaymentsWithCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAccountNbr") String bilAccountNbr,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string) and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAccountNbr != :bilAccountNbr "
            + "and b.bilAccountNbr between cast(:fromIdentifier as string) "
            + "and cast(:toIdentifier as string) and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findBillAccountSuspendedPaymentsWithCAPFalse(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAccountNbr") String bilAccountNbr,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAccountNbr != :bilAccountNbr "
            + "and b.bilAccountNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findBillAccountSuspendedPaymentsWithPaymentMethodAndCAPTrue(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAccountNbr") String bilAccountNbr,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAccountNbr != :bilAccountNbr "
            + "and b.bilAccountNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findBillAccountSuspendedPaymentsWithPaymentMethodAndCAPFalse(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAccountNbr") String bilAccountNbr,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAgtActNbr != :bilAgtActNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAgtActNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAgencySuspendedPaymentsWithCurrencyAndCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAgtActNbr") String bilAgtActNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAgtActNbr != :bilAgtActNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAgtActNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAgencySuspendedPaymentsWithCurrencyAndCAPFalse(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAgtActNbr") String bilAgtActNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAgtActNbr != :bilAgtActNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAgtActNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAgencySuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPTrue(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAgtActNbr") String bilAgtActNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAgtActNbr != :bilAgtActNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAgtActNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAgencySuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPFalse(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAgtActNbr") String bilAgtActNbr,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAgtActNbr != :bilAgtActNbr "
            + "and b.bilAgtActNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAgencySuspendedPaymentsWithCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAgtActNbr") String bilAgtActNbr,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAgtActNbr != :bilAgtActNbr "
            + "and b.bilAgtActNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAgencySuspendedPaymentsWithCAPFalse(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAgtActNbr") String bilAgtActNbr,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAgtActNbr != :bilAgtActNbr "
            + "and b.bilAgtActNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAgencySuspendedPaymentsWithPaymentMethodAndCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("bilAgtActNbr") String bilAgtActNbr, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAgtActNbr != :bilAgtActNbr "
            + "and b.bilAgtActNbr between cast(:fromIdentifier as string) and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAgencySuspendedPaymentsWithPaymentMethodAndCAPFalse(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAgtActNbr") String bilAgtActNbr,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.polNbr != :polNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.polNbr between cast(:fromPolicy as string)  and cast(:toPolicy as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findPolicySuspendedPaymentsWithCurrencyAndCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("polNbr") String polNbr,
            @Param("currency") String currency, @Param("fromPolicy") String fromPolicy,
            @Param("toPolicy") String toPolicy, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.polNbr != :polNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.polNbr between cast(:fromPolicy as string)  and cast(:toPolicy as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findPolicySuspendedPaymentsWithCurrencyAndCAPFalse(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("polNbr") String polNbr,
            @Param("currency") String currency, @Param("fromPolicy") String fromPolicy,
            @Param("toPolicy") String toPolicy, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.polNbr != :polNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.polNbr between cast(:fromPolicy as string)  and cast(:toPolicy as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findPolicySuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPTrue(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("polNbr") String polNbr,
            @Param("currency") String currency, @Param("fromPolicy") String fromPolicy,
            @Param("toPolicy") String toPolicy, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.polNbr != :polNbr and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.polNbr between cast(:fromPolicy as string)  and cast(:toPolicy as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findPolicySuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPFalse(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("polNbr") String polNbr,
            @Param("currency") String currency, @Param("fromPolicy") String fromPolicy,
            @Param("toPolicy") String toPolicy, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.polNbr != :polNbr "
            + "and b.polNbr between cast(:fromPolicy as string)  and cast(:toPolicy as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findPolicySuspendedPaymentsAndCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("polNbr") String polNbr,
            @Param("fromPolicy") String fromPolicy, @Param("toPolicy") String toPolicy,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.polNbr != :polNbr "
            + "and b.polNbr between cast(:fromPolicy as string)  and cast(:toPolicy as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findPolicySuspendedPaymentsAndCAPFalse(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("polNbr") String polNbr,
            @Param("fromPolicy") String fromPolicy, @Param("toPolicy") String toPolicy,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.polNbr != :polNbr "
            + "and b.polNbr between cast(:fromPolicy as string)  and cast(:toPolicy as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findPolicySuspendedPaymentsWithPaymentMethodAndCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("polNbr") String polNbr, @Param("fromPolicy") String fromPolicy, @Param("toPolicy") String toPolicy,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.polNbr != :polNbr "
            + "and b.polNbr between cast(:fromPolicy as string)  and cast(:toPolicy as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findPolicySuspendedPaymentsWithPaymentMethodAndCAPFalse(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("polNbr") String polNbr,
            @Param("fromPolicy") String fromPolicy, @Param("toPolicy") String toPolicy,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAdditionalId != :bilAdditionalId and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAdditionalId between cast(:fromIdentifier as string)  and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAdditionalIdSuspendedPaymentsWithCurrencyAndCAPTrue(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("bilAdditionalId") String bilAdditionalId, @Param("currency") String currency,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAdditionalId != :bilAdditionalId and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAdditionalId between cast(:fromIdentifier as string)  and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAdditionalIdSuspendedPaymentsWithCurrencyAndCAPFalse(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("bilAdditionalId") String bilAdditionalId, @Param("currency") String currency,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAdditionalId != :bilAdditionalId and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAdditionalId between cast(:fromIdentifier as string)  and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAdditionalIdSuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPTrue(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAdditionalId") String bilAdditionalId,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAdditionalId != :bilAdditionalId and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilAdditionalId between cast(:fromIdentifier as string)  and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAdditionalIdSuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPFalse(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAdditionalId") String bilAdditionalId,
            @Param("currency") String currency, @Param("fromIdentifier") String fromIdentifier,
            @Param("toIdentifier") String toIdentifier, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAdditionalId != :bilAdditionalId "
            + "and b.bilAdditionalId between cast(:fromIdentifier as string)  and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAdditionalIdSuspendedPaymentsWithCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAdditionalId") String bilAdditionalId,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAdditionalId != :bilAdditionalId "
            + "and b.bilAdditionalId between cast(:fromIdentifier as string)  and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAdditionalIdSuspendedPaymentsWithCAPFalse(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAdditionalId") String bilAdditionalId,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAdditionalId != :bilAdditionalId "
            + "and b.bilAdditionalId between cast(:fromIdentifier as string)  and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAdditionalIdSuspendedPaymentsWithPaymentMethodAndCAPTrue(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAdditionalId") String bilAdditionalId,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilAdditionalId != :bilAdditionalId "
            + "and b.bilAdditionalId between cast(:fromIdentifier as string)  and cast(:toIdentifier as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> findAdditionalIdSuspendedPaymentsWithPaymentMethodAndCAPFalse(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("bilAdditionalId") String bilAdditionalId,
            @Param("fromIdentifier") String fromIdentifier, @Param("toIdentifier") String toIdentifier,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string) and "
            + "cast(:toUserId as string) and b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> getSuspendedPaymentListWithCurrencyAndCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("currency") String currency,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string) and "
            + "cast(:toUserId as string) and b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> getSuspendedPaymentListWithCurrencyAndCAPFalse(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("currency") String currency,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> getSuspendedPaymentListWithPaymentMethodAndCurrencyAndCAPTrue(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("currency") String currency,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b, BilCashEntryTot d where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + "b.bilRctAmt between :fromAmount and :toAmount and b.bilCshEtrMthCd = :cashEntryMethod and "
            + "b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and "
            + "b.bilUnIdCashId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt and b.bilUnIdCashId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and "
            + "b.bilUnIdCashId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> getSuspendedPaymentListWithPaymentMethodAndCurrencyAndCAPFalse(
            @Param("since") ZonedDateTime since, @Param("until") ZonedDateTime until,
            @Param("fromAmount") Double fromAmount, @Param("toAmount") Double toAmount,
            @Param("cashEntryMethod") Character cashEntryMethod, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("currency") String currency,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> getSuspendedPaymentListWithCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> getSuspendedPaymentListWithCAPFalse(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("statusList") List<String> statusList,
            @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilCshEtrMthCd = :cashEntryMethod "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd in (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> getSuspendedPaymentListWithPaymentMethodAndCAPTrue(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt between :since and :until and "
            + " b.bilRctAmt between :fromAmount and :toAmount and b.bilUnIdCashId.userId between cast(:fromUserId as string)  and cast(:toUserId as string) and "
            + "b.bilRctId between cast(:fromPaymentId as string)  and cast(:toPaymentId as string) and b.bilCshEtrMthCd = :cashEntryMethod "
            + "and b.bilDspTypeCd in :statusList "
            + "and b.bilRctTypeCd NOT IN (select c.bilDesReasonId.bilDesReasonCd from BilDesReason c where c.bilDesReasonId.bilDesReasonType = :paymentType)")
    public Page<BilUnIdCash> getSuspendedPaymentListWithPaymentMethodAndCAPFalse(@Param("since") ZonedDateTime since,
            @Param("until") ZonedDateTime until, @Param("fromAmount") Double fromAmount,
            @Param("toAmount") Double toAmount, @Param("cashEntryMethod") Character cashEntryMethod,
            @Param("fromUserId") String fromUserId, @Param("toUserId") String toUserId,
            @Param("fromPaymentId") String fromPaymentId, @Param("toPaymentId") String toPaymentId,
            @Param("statusList") List<String> statusList, @Param("paymentType") String paymentType, Pageable pageable);

    public List<BilUnIdCash> findByBilDsbId(String disbursementId);

    @Query(value = "select b from BilUnIdCash b where b.bilUnIdCashId.bilEntryDt = :bilEntryDt and b.bilUnIdCashId.bilEntryNbr = :bilEntryNbr and b.bilEntrySeqNbr = :bilEntrySeqNbr"
            + " and b.bilUnIdCashId.userId = :userId and b.bilRctAmt = :bilRctAmt")
    public BilUnIdCash findUnIdCash(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNbr") String bilEntryNbr, @Param("bilEntrySeqNbr") short bilEntrySeqNbr,
            @Param("userId") String userId, @Param("bilRctAmt") double bilRctAmt);
}
