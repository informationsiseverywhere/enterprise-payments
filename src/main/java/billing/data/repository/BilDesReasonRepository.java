package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilDesReason;
import billing.data.entity.id.BilDesReasonId;

public interface BilDesReasonRepository extends JpaRepository<BilDesReason, BilDesReasonId> {

    @Query("select b from BilDesReason b where b.bilDesReasonId.bilDesReasonType = :bilDesReasonType and b.bilDesReasonId.primaryLanguageCd = :primaryLanguageCd and b.bilDesReasonId.bilDesReasonCd = :bilDesReasonCd")
    public BilDesReason findDescriptionByCodeAndType(@Param("bilDesReasonCd") String bilDesReasonCd,
            @Param("bilDesReasonType") String bilDesReasonType, @Param("primaryLanguageCd") String primaryLanguageCd);

}