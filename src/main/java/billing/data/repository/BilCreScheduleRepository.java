package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilCreSchedule;
import billing.data.entity.id.BilCreScheduleId;

public interface BilCreScheduleRepository extends JpaRepository<BilCreSchedule, BilCreScheduleId> {

    List<BilCreSchedule> findByBilCreScheduleIdBilAccountId(String accountId);

    @Query("Select b from BilCreSchedule b where b.bilCreScheduleId.bilAccountId = :billAccountId "
            + "and b.bilCreScheduleId.policyId = :policyId and b.bilCreScheduleId.bilSeqNbr = :billSeqNbr")
    public List<BilCreSchedule> fetchBilCreScheduleRow(@Param("billAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("billSeqNbr") short billSeqNbr);

    @Query(value = "select b from BilCreSchedule b where b.bilCreScheduleId.bilAccountId = :bilAccountId and b.bilSysDueDt = :bilSysDueDt")
    public List<BilCreSchedule> findBySystemDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt);

    @Query(value = "select b from BilCreSchedule b where b.bilCreScheduleId.bilAccountId = :accountId and "
            + "b.bilCreScheduleId.policyId = :policyId and bilCreScheduleId.bilSeqNbr = :sequenceNumber")
    public List<BilCreSchedule> findByAccountIdPolicyId(@Param("accountId") String accountId,
            @Param("policyId") String policyId, @Param("sequenceNumber") short sequenceNumber);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCreSchedule b set b.bilSysDueDt = :bilSysDueDtUpdate, b.bilAdjDueDt = :bilAdjDueDt, "
            + "  b.bilInvDt = :bilInvDt where b.bilCreScheduleId.bilAccountId = :billAccountId "
            + " and b.bilSysDueDt = :bilSysDueDt ")
    public Integer updateCreScheduleRowBySystemDate(@Param("bilSysDueDtUpdate") ZonedDateTime bilSysDueDtUpdate,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("billAccountId") String billAccountId, @Param("bilSysDueDt") ZonedDateTime bilSysDueDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCreSchedule b set b.bilAdjDueDt = :bilAdjDueDt, b.bilInvDt = :bilInvDt "
            + " where b.bilCreScheduleId.bilAccountId = :billAccountId "
            + " and b.bilSysDueDt = :bilSysDueDt and b.bcsCreAmtDsp in :bcsCreAmtDspList")
    public Integer updateRescindCreScheduleRowBySystemDate(@Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billAccountId") String billAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("bcsCreAmtDspList") List<Character> bcsCreAmtDspList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCreSchedule b set b.bilSysDueDt = :bilSysDueDt, b.bilAdjDueDt = :bilAdjDueDt, "
            + "  b.bilInvDt = :bilInvDt where b.bilCreScheduleId.bilAccountId = :billAccountId and "
            + " b.bilCreScheduleId.policyId = :policyId and bilCreScheduleId.bilSeqNbr = :sequenceNumber ")
    public Integer updateCreScheduleRowBybilSeqNbr(@Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("billAccountId") String billAccountId, @Param("policyId") String policyId,
            @Param("sequenceNumber") short sequenceNumber);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCreSchedule b set b.bilAdjDueDt = :bilAdjDueDtUpdate, b.bilInvDt = :bilInvDtUpdate "
            + " where b.bilCreScheduleId.bilAccountId = :billAccountId and b.bilAdjDueDt = :bilAdjDueDt "
            + " and b.bilInvDt = :bilInvDt")
    public Integer updateRescindCreScheduleRowBybilAdjDueDt(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("billAccountId") String billAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCreSchedule b set b.bilSysDueDt = :bilSysDueDtUpdate, b.bilAdjDueDt = :bilAdjDueDt, "
            + "  b.bilInvDt = :bilInvDt where b.bilCreScheduleId.bilAccountId = :billAccountId and "
            + " b.bilCreScheduleId.policyId = :policyId and b.polEffectiveDt = :polEffectiveDt and "
            + " b.bilSysDueDt = :bilSysDueDt")
    public Integer updateCreScheduleRowByPolicyId(@Param("bilSysDueDtUpdate") ZonedDateTime bilSysDueDtUpdate,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("billAccountId") String billAccountId, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("bilSysDueDt") ZonedDateTime bilSysDueDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilCreSchedule b set b.bilInvDt = :bilInvDt where b.bilCreScheduleId.bilAccountId = :billAccountId and b.bilSysDueDt = :bilSysDueDt")
    public void updateInvoiceDateRowBySystemDueDate(@Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("billAccountId") String billAccountId, @Param("bilSysDueDt") ZonedDateTime bilSysDueDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCreSchedule b set b.bilAdjDueDt = :bilAdjDueDtUpdate, b.bilInvDt = :bilInvDtUpdate "
            + " where b.bilCreScheduleId.bilAccountId = :billAccountId and b.bilSysDueDt = :bilSysDueDt")
    public Integer updateInvDateAndAdjDateRowBySystemDueDate(
            @Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("billAccountId") String billAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCreSchedule b set b.bilAdjDueDt = :bilAdjDueDt, b.bilInvDt = :bilInvDt, b.bilSysDueDt = :bilSysDueDtUpdate "
            + " where b.bilCreScheduleId.bilAccountId = :billAccountId "
            + " and b.bilSysDueDt = :bilSysDueDt and b.bcsCreAmtDsp in :bcsCreAmtDspList")
    public Integer updateCreScheduleRowByFpdd(@Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilSysDueDtUpdate") ZonedDateTime bilSysDueDtUpdate,
            @Param("billAccountId") String billAccountId, @Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("bcsCreAmtDspList") List<Character> bcsCreAmtDspList);

    @Query(value = "select b from BilCreSchedule b where b.bilCreScheduleId.bilAccountId = :bilAccountId and b.bilSysDueDt = :bilSysDueDt"
            + " and b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDt ")
    public List<BilCreSchedule> findDefaultCreScheduleData(@Param("bilAccountId") String bilAccountId,
            @Param("bilSysDueDt") ZonedDateTime bilSysDueDt, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCreSchedule b set b.bilSysDueDt = :bilSysDueDtUpdate "
            + "  where b.bilCreScheduleId.bilAccountId = :billAccountId "
            + " and b.bilSysDueDt = :bilSysDueDt and b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDt")
    public Integer updateCreScheduleRowByOldScheduleDate(@Param("bilSysDueDtUpdate") ZonedDateTime bilSysDueDtUpdate,
            @Param("billAccountId") String billAccountId, @Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCreSchedule b set b.bilSysDueDt = :bilSysDueDt, b.bilAdjDueDt = :bilAdjDueDtUpdate, "
            + "  b.bilInvDt = :bilInvDtUpdate where b.bilCreScheduleId.bilAccountId = :billAccountId and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDt ")
    public Integer updateCreScheduleRowByOldScheduleDate(@Param("bilSysDueDt") ZonedDateTime bilSysDueDt,
            @Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("billAccountId") String billAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt);

}