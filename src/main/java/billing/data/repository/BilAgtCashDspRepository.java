package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilAgtCashDsp;
import billing.data.entity.id.BilAgtCashDspId;

public interface BilAgtCashDspRepository extends JpaRepository<BilAgtCashDsp, BilAgtCashDspId> {

    @Query(value = "SELECT SUM(b.bilDspAmt) FROM BilAgtCashDsp b WHERE b.bilAgtCashDspId.bilAgtActId = :bilAgtActId and b.bilAgtCashDspId.bilDtbDt = :bilDtbDt "
            + "and b.bilAgtCashDspId.bilDtbSeqNbr = :bilDtbSeqNbr and b.bilDspTypeCd = :bilDspTypeCd and b.bilRevsRsusInd = :bilRevsRsusInd")
    public Double findDspAmountByDspTypeCodeRevsRsusInd(@Param("bilAgtActId") String bilAgtActId,
            @Param("bilDtbDt") ZonedDateTime bilDtbDt, @Param("bilDtbSeqNbr") short bilDtbSeqNbr,
            @Param("bilDspTypeCd") String bilDspTypeCd, @Param("bilRevsRsusInd") char bilRevsRsusInd);

    @Query(value = "SELECT SUM(b.bilDspAmt) FROM BilAgtCashDsp b WHERE b.bilAgtCashDspId.bilAgtActId = :bilAgtActId and b.bilAgtCashDspId.bilDtbDt = :bilDtbDt "
            + "and b.bilAgtCashDspId.bilDtbSeqNbr = :bilDtbSeqNbr and b.bilDspTypeCd in :bilDspTypeCdList and b.bilRevsRsusInd = :bilRevsRsusInd")
    public Double findPaymentWriteOffAmount(@Param("bilAgtActId") String bilAgtActId,
            @Param("bilDtbDt") ZonedDateTime bilDtbDt, @Param("bilDtbSeqNbr") short bilDtbSeqNbr,
            @Param("bilDspTypeCdList") List<String> bilDspTypeCdList, @Param("bilRevsRsusInd") char bilRevsRsusInd);

    @Query(value = "select COUNT(b) from BilAgtCashDsp b where b.bilAgtCashDspId.bilAgtActId = :accountId and b.bilAgtCashDspId.bilDtbDt = :bilDtbDate and "
            + "b.bilAgtCashDspId.bilDtbSeqNbr = :dtbSequenceNbr and b.bilDspTypeCd = :dspTypeCd")
    public Integer findCountForPaymentDescription(@Param("accountId") String accountId,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate, @Param("dtbSequenceNbr") short dtbSequenceNbr,
            @Param("dspTypeCd") String dspTypeCd);

    @Query(value = "select MAX(b.bilAgtCashDspId.bilDspSeqNbr) from BilAgtCashDsp b where b.bilAgtCashDspId.bilAgtActId = :bilAgtActId and"
            + " b.bilAgtCashDspId.bilDtbDt = :bilDtbDt and b.bilAgtCashDspId.bilDtbSeqNbr = :bilDtbSeqNbr")
    public Short findMaxDispositionNumber(@Param("bilAgtActId") String bilAgtActId,
            @Param("bilDtbDt") ZonedDateTime bilDtbDt, @Param("bilDtbSeqNbr") short bilDtbSeqNbr);
    
    @Query(value = "SELECT SUM(b.bilDspAmt) FROM BilAgtCashDsp b WHERE b.bilAgtCashDspId.bilAgtActId = :accountId and b.bilAdjDueDt = :dueDate "
            + "and b.bilDspTypeCd = :bilDspTypeCode and b.bilRevsRsusInd = :reverseResuspendIndicator and b.bilManualSusInd <> :manualSuspendIndicator")
    public Double findDspAmountByDspTypeCode(@Param("accountId") String accountId,
            @Param("dueDate") ZonedDateTime dueDate, @Param("bilDspTypeCode") String bilDspTypeCode,
            @Param("reverseResuspendIndicator") Character reverseResuspendIndicator,
            @Param("manualSuspendIndicator") Character manualSuspendIndicator);
}