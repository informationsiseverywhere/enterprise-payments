package billing.data.repository;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilCashDsp;
import billing.data.entity.id.BilCashDspId;

public interface BilCashDspRepository extends JpaRepository<BilCashDsp, BilCashDspId> {

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.bilCashDspId.bilDtbDate = :distributionDate and "
            + "b.bilCashDspId.dtbSequenceNbr = :sequenceNumber and b.dspTypeCd = :dspTypeCode and b.revsRsusIndicator = :revsRsusIndicator")
    public Double findSuspenseAmount(@Param("accountId") String accountId,
            @Param("distributionDate") ZonedDateTime distributionDate, @Param("sequenceNumber") short sequenceNumber,
            @Param("dspTypeCode") String dspTypeCode, @Param("revsRsusIndicator") char revsRsusIndicator);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.bilCashDspId.bilDtbDate = :distributionDate and "
            + "b.bilCashDspId.dtbSequenceNbr = :sequenceNumber and b.dspTypeCd in :dspTypeCodeList and b.revsRsusIndicator = :revsRsusIndicator")
    public Double findWriteOffAmount(@Param("accountId") String accountId,
            @Param("distributionDate") ZonedDateTime distributionDate, @Param("sequenceNumber") short sequenceNumber,
            @Param("dspTypeCodeList") List<String> dspTypeCodeList, @Param("revsRsusIndicator") char revsRsusIndicator);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :bilAccountId and b.dspTypeCd = :bilDspTypeCode and "
            + "b.revsRsusIndicator in :reverseIndicatorList")
    public BigDecimal getSusAmountForAccountBalance(@Param("bilAccountId") String bilAccountId,
            @Param("bilDspTypeCode") String bilDspTypeCode,
            @Param("reverseIndicatorList") List<Character> reverseIndicatorList);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :bilAccountId and b.dspTypeCd = :bilDspTypeCode and "
            + "b.revsRsusIndicator = :revsRsusIndicator")
    public BigDecimal getAmountForAccountBalance(@Param("bilAccountId") String bilAccountId,
            @Param("bilDspTypeCode") String bilDspTypeCode, @Param("revsRsusIndicator") char revsRsusIndicator);

    @Query(value = "select COUNT(b) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.bilCashDspId.bilDtbDate = :bilDtbDate and "
            + "b.bilCashDspId.dtbSequenceNbr = :dtbSequenceNbr and b.dspTypeCd = :dspTypeCd")
    public Integer findCountForPaymentDescription(@Param("accountId") String accountId,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate, @Param("dtbSequenceNbr") short dtbSequenceNbr,
            @Param("dspTypeCd") String dspTypeCd);

    @Query(value = "select COUNT(b) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.bilCashDspId.bilDtbDate = :bilDtbDate and "
            + "b.bilCashDspId.dtbSequenceNbr = :dtbSequenceNbr and b.dspTypeCd = :dspTypeCd and ((b.revsRsusIndicator = :revsRsusIndicator and  "
            + "b.manualSuspenseIndicator = :manualSuspenseIndicator) or b.creditIndicator in :creditIndicatorList)")
    public Integer findCountForPaymentDescriptions(@Param("accountId") String accountId,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate, @Param("dtbSequenceNbr") short dtbSequenceNbr,
            @Param("dspTypeCd") String dspTypeCd, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList);

    @Query(value = "select MAX(b.bilCashDspId.dspSequenceNbr) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.bilCashDspId.dtbSequenceNbr = :bilDtbSequenceNumber and b.bilCashDspId.bilDtbDate = :bilDtbDate")
    public Short findMaxDispositionNumber(@Param("accountId") String accountId,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate, @Param("bilDtbSequenceNumber") short bilDtbSequenceNumber);

    @Query(value = "select MAX(b.bilCashDspId.dtbSequenceNbr) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.bilCashDspId.bilDtbDate = :bilDtbDate")
    public Short findMaxDistributionNumber(@Param("accountId") String accountId,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET b.dspAmount = :dspAmount where b.bilCashDspId = :bilCashDspId ")
    public void updateRow(@Param("dspAmount") double dspAmount, @Param("bilCashDspId") BilCashDspId bilCashDspId);

    @Query(value = "select b from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.bilCashDspId.bilDtbDate = :bilDtbDate and "
            + "b.bilCashDspId.dtbSequenceNbr = :dtbSequenceNbr and b.bilCashDspId.dspSequenceNbr = :dspSequenceNbr and b.dspTypeCd = :dspTypeCd ")
    public BilCashDsp findSuspendedSchedule(@Param("accountId") String accountId,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate, @Param("dtbSequenceNbr") short dtbSequenceNbr,
            @Param("dspSequenceNbr") short dspSequenceNbr, @Param("dspTypeCd") String dspTypeCd);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.dspTypeCd = :bilDspTypeCode and b.policyId = :policyId and b.policyEffectiveDate = :policyEffectiveDate and "
            + " b.revsRsusIndicator = :revsRsusIndicator")
    public Double getDspAmountForApplied(@Param("bilAccountId") String bilAccountId,
            @Param("bilDspTypeCode") String bilDspTypeCode, @Param("policyId") String policyId,
            @Param("policyEffectiveDate") ZonedDateTime policyEffectiveDate,
            @Param("revsRsusIndicator") char revsRsusIndicator);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and "
            + " b.dspTypeCd = :dspTypeCode and b.revsRsusIndicator = :revsRsusIndicator and "
            + " (b.manualSuspenseIndicator in :manualSuspenseIndicatorList or "
            + "(b.manualSuspenseIndicator = :manualSuspenseIndicator and b.dspReasonCd = :dspReasonCd))")
    public Double findSuspenseAmountByAccountId(@Param("accountId") String accountId,
            @Param("dspTypeCode") String dspTypeCode, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("manualSuspenseIndicatorList") List<Character> manualSuspenseIndicatorList,
            @Param("dspReasonCd") String dspReasonCd);

    @Query(value = "select b from BilCashDsp b where b.bilCashDspId.accountId = :accountId and "
            + " b.bilCashDspId.bilDtbDate = :bilDtbDate and b.bilCashDspId.dspSequenceNbr = :dspSequenceNbr and "
            + " b.dspTypeCd = :dspTypeCode and b.userId = :userId ")
    public List<BilCashDsp> findHideExcludePremiumRow(@Param("accountId") String accountId,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate, @Param("dspTypeCode") String dspTypeCode,
            @Param("dspSequenceNbr") short dspSequenceNbr, @Param("userId") String userId);

    @Query(value = "select b from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.dspTypeCd = :dspTypeCd "
            + "and b.manualSuspenseIndicator = :manualSuspenseIndicator and b.dspReasonCd = :dspReasonCd and b.revsRsusIndicator in :revsRsusIndicatorList "
            + " order by b.bilCashDspId.bilDtbDate, b.bilCashDspId.dtbSequenceNbr, b.bilCashDspId.dspSequenceNbr ")
    public List<BilCashDsp> fetchDownPaymentCashRows(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("dspReasonCd") String dspReasonCd,
            @Param("revsRsusIndicatorList") List<Character> revsRsusIndicatorList);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.dspTypeCd = :dspTypeCd "
            + "and b.revsRsusIndicator in :revsRsusIndicatorList and b.manualSuspenseIndicator <> :manualSuspenseIndicator")
    public Double readTotalCashAmount(@Param("accountId") String accountId, @Param("dspTypeCd") String dspTypeCd,
            @Param("revsRsusIndicatorList") List<Character> revsRsusIndicatorList,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.bilCashDspId.bilDtbDate = :bilDtbDate"
            + " and b.bilCashDspId.dtbSequenceNbr = :dtbSequenceNbr and b.dspTypeCd = :dspTypeCd and b.revsRsusIndicator = :revsRsusIndicator and "
            + " b.manualSuspenseIndicator <> :manualSuspenseIndicator and b.creditIndicator in :creditIndicatorList and b.policyNumber = :policyNumber")
    public Double fetchAvailableAmountDsp(@Param("accountId") String accountId,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate, @Param("dtbSequenceNbr") short dtbSequenceNbr,
            @Param("dspTypeCd") String dspTypeCd, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("policyNumber") String policyNumber);

    @Query(value = "select COUNT(b) from BilCashDsp b where b.dspTypeCd = :dspTypeCode and b.manualSuspenseIndicator <> :manualSuspenseIndicator "
            + " and b.revsRsusIndicator = :revsRsusIndicator and b.bilCashDspId.accountId = :accountId and b.invoiceDate <> :invoiceDate and "
            + " b.creditIndicator = :creditIndicator and b.creditPolicyId > :creditPolicyId")
    public Integer cancelRewriteCountByAccountId(@Param("dspTypeCode") String dspTypeCode,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("accountId") String accountId,
            @Param("invoiceDate") ZonedDateTime invoiceDate, @Param("creditIndicator") char creditIndicator,
            @Param("creditPolicyId") String creditPolicyId);

    @Query(value = "select b from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.dspTypeCd = :dspTypeCd and "
            + "b.revsRsusIndicator = :revsRsusIndicator and b.manualSuspenseIndicator <> :manualSuspenseIndicator and b.invoiceDate <> :invoiceDate "
            + "and b.creditIndicator = :creditIndicator and b.creditPolicyId > :creditPolicyId "
            + "order by b.payableItemCd desc, b.bilCashDspId.bilDtbDate, b.bilCashDspId.dtbSequenceNbr, b.dspDate asc")
    public List<BilCashDsp> fetchCancelForRewriteCash(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("invoiceDate") ZonedDateTime invoiceDate, @Param("creditIndicator") char creditIndicator,
            @Param("creditPolicyId") String creditPolicyId);

    @Query(value = "select b from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.dspTypeCd = :dspTypeCd and "
            + "b.policyNumber = :policyNumber and b.revsRsusIndicator = :revsRsusIndicator and b.manualSuspenseIndicator <> :manualSuspenseIndicator "
            + "order by b.payableItemCd desc, b.bilCashDspId.bilDtbDate, b.bilCashDspId.dtbSequenceNbr, b.dspDate asc")
    public List<BilCashDsp> fetchBilTranferCash(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator, @Param("policyNumber") String policyNumber);

    @Query(value = "select b from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.dspTypeCd = :dspTypeCd and "
            + "b.revsRsusIndicator = :revsRsusIndicator and b.manualSuspenseIndicator <> :manualSuspenseIndicator "
            + "order by b.payableItemCd desc, b.bilCashDspId.bilDtbDate, b.bilCashDspId.dtbSequenceNbr, b.dspDate asc")
    public List<BilCashDsp> fetchCashBySpreadCashOver(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator);

    @Query(value = "select MAX( b.policyNumber), MAX(b.creditPolicyId) from BilCashDsp b, BilCashReceipt c where b.bilCashDspId.accountId = :accountId and "
            + " b.bilCashDspId.accountId = c.bilCashReceiptId.accountId and b.bilCashDspId.bilDtbDate = c.bilCashReceiptId.bilDtbDate "
            + " and b.bilCashDspId.dtbSequenceNbr = c.bilCashReceiptId.dtbSequenceNbr and b.dspTypeCd = :dspTypeCd and b.revsRsusIndicator = :revsRsusIndicator and "
            + " b.manualSuspenseIndicator <> :manualSuspenseIndicator and c.receiptTypeCd in :receiptTypeCdList")
    public List<Object[]> fetchMaxPolicyData(@Param("accountId") String accountId, @Param("dspTypeCd") String dspTypeCd,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("receiptTypeCdList") List<String> receiptTypeCdList);

    @Query(value = "select MAX( b.policySymbolCd) from BilCashDsp b, BilCashReceipt c where b.bilCashDspId.accountId = :accountId and "
            + " b.bilCashDspId.accountId = c.bilCashReceiptId.accountId and b.bilCashDspId.bilDtbDate = c.bilCashReceiptId.bilDtbDate "
            + " and b.bilCashDspId.dtbSequenceNbr = c.bilCashReceiptId.dtbSequenceNbr and b.dspTypeCd = :dspTypeCd and b.revsRsusIndicator = :revsRsusIndicator and "
            + " b.manualSuspenseIndicator <> :manualSuspenseIndicator and b.policyNumber = :policyNumber")
    public String fetchMaxPolicySymbolCode(@Param("accountId") String accountId, @Param("dspTypeCd") String dspTypeCd,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator, @Param("policyNumber") String policyNumber);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.policySymbolCd = :policySymbolCd and "
            + "b.policyNumber = :policyNumber and b.dspTypeCd = :dspTypeCd and b.revsRsusIndicator in :revsRsusIndicatorList and b.manualSuspenseIndicator <> :manualSuspenseIndicator")
    public Double readTotalCashAmountByPolicy(@Param("accountId") String accountId,
            @Param("policySymbolCd") String policySymbolCd, @Param("policyNumber") String policyNumber,
            @Param("dspTypeCd") String dspTypeCd, @Param("revsRsusIndicatorList") List<Character> revsRsusIndicatorList,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator);

    @Query(value = "select MAX(b.bilCashDspId.bilDtbDate) from BilCashDsp b where b.bilCashDspId.accountId = :bilAccountId and b.dspTypeCd = :bilDspTypeCode and "
            + "b.revsRsusIndicator = :revsRsusIndicator and b.dspDate = :dspDate")
    public ZonedDateTime getMaxBilDistributionDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilDspTypeCode") String bilDspTypeCode, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("dspDate") ZonedDateTime dspDate);

    @Query(value = "select b from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.bilCashDspId.bilDtbDate = :bilDtbDate "
            + " and b.bilCashDspId.dtbSequenceNbr = :dtbSequenceNbr and b.dspTypeCd = :dspTypeCd and b.revsRsusIndicator = :revsRsusIndicator and "
            + " b.manualSuspenseIndicator <> :manualSuspenseIndicator order by b.payableItemCd desc, b.bilCashDspId.bilDtbDate, b.bilCashDspId.dtbSequenceNbr,"
            + " b.dspDate asc, b.adjustmentDueDate asc")
    public BilCashDsp fetchCreditCashFirst(@Param("accountId") String accountId,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate, @Param("dtbSequenceNbr") short dtbSequenceNbr,
            @Param("dspTypeCd") String dspTypeCd, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator);

    @Query(value = "select count(b) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and "
            + " b.adjustmentDueDate = :adjustmentDueDate and b.policyNumber = :policyNumber and b.dspTypeCd = :dspTypeCd and "
            + " b.payableItemCd = :payableItemCd and b.revsRsusIndicator = :revsRsusIndicator and "
            + " b.manualSuspenseIndicator <> :manualSuspenseIndicator and b.userId = :userId ")
    public Integer checkInvoiceWriteOff(@Param("accountId") String accountId,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate, @Param("policyNumber") String policyNumber,
            @Param("payableItemCd") String payableItemCd, @Param("userId") String userId,
            @Param("dspTypeCd") String dspTypeCd, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.adjustmentDueDate = :adjustmentDueDate and b.bilCashDspId.accountId = :accountId "
            + "and b.bilCashDspId.bilDtbDate = :bilDtbDate and b.bilCashDspId.dtbSequenceNbr = :dtbSequenceNbr and b.dspTypeCd = :dspTypeCd and "
            + "b.revsRsusIndicator = :revsRsusIndicator and b.manualSuspenseIndicator <> :manualSuspenseIndicator and b.policyNumber = :policyNumber "
            + "and b.payableItemCd = :payableItemCd")
    public Double fetchSuspendWriteOff(@Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("accountId") String accountId, @Param("bilDtbDate") ZonedDateTime bilDtbDate,
            @Param("dtbSequenceNbr") short dtbSequenceNbr, @Param("dspTypeCd") String dspTypeCd,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator, @Param("policyNumber") String policyNumber,
            @Param("payableItemCd") String payableItemCd);

    @Query(value = "select b from BilCashDsp b where b.bilCashDspId.accountId = :accountId and "
            + " b.dspTypeCd = :dspTypeCode and b.adjustmentDueDate = :adjustmentDueDate and b.policyNumber = :policyNumber and "
            + " b.payableItemCd = :payableItemCd and b.revsRsusIndicator = :revsRsusIndicator and "
            + " b.manualSuspenseIndicator <> :manualSuspenseIndicator ")
    public List<BilCashDsp> findSuspendOverPaymentRows(@Param("accountId") String accountId,
            @Param("dspTypeCode") String dspTypeCode, @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("policyNumber") String policyNumber, @Param("payableItemCd") String payableItemCd,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator);

    @Query(value = "select b from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.dspTypeCd = :dspTypeCd and "
            + "b.adjustmentDueDate = :adjustmentDueDate and b.invoiceDate = :invoiceDate and "
            + "b.chargeTypeCd = :chargeTypeCd and b.revsRsusIndicator = :revsRsusIndicator "
            + "order by b.bilCashDspId.bilDtbDate, b.bilCashDspId.dtbSequenceNbr, b.bilCashDspId.dspSequenceNbr ")
    public List<BilCashDsp> fetchAppliedRowsByInvoiceDate(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("invoiceDate") ZonedDateTime invoiceDate, @Param("chargeTypeCd") char chargeTypeCd);

    @Query(value = "select SUM(b.dspAmount) from BilCashDsp b, BilCashReceipt c where "
            + " b.bilCashDspId.accountId = :accountId and b.creditPolicyId = :bcdCrePolId and  b.dspTypeCd = :dspTypeCd and  b.revsRsusIndicator = :revsRsusIndicator  and b.creditIndicator in :creditIndicatorList and"
            + " b.manualSuspenseIndicator in :manualSuspenseIndicatorList and (b.bilCashDspId.accountId = c.bilCashReceiptId.accountId and b.bilCashDspId.bilDtbDate = c.bilCashReceiptId.bilDtbDate "
            + "  and b.bilCashDspId.dtbSequenceNbr = c.bilCashReceiptId.dtbSequenceNbr and SUBSTRING(c.receiptComment,1,2) <> :receiptComment)")
    public Double getCreditSuspenseDspAmt(@Param("accountId") String accountId,
            @Param("bcdCrePolId") String bcdCrePolId, @Param("dspTypeCd") String dspTypeCd,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("manualSuspenseIndicatorList") List<Character> manualSuspenseIndicatorList,
            @Param("receiptComment") String receiptComment);

    @Query(value = "select min(b.dspDate) from BilCashDsp b, BilCashReceipt c where "
            + " b.bilCashDspId.accountId = :accountId and b.creditPolicyId = :bcdCrePolId and  b.dspTypeCd = :dspTypeCd and  b.revsRsusIndicator = :revsRsusIndicator  and b.creditIndicator in :creditIndicatorList and "
            + " b.manualSuspenseIndicator IN :manualSuspenseIndicatorList and b.dspAmount  <:bilDspAmt  and (b.bilCashDspId.accountId = c.bilCashReceiptId.accountId and b.bilCashDspId.bilDtbDate = c.bilCashReceiptId.bilDtbDate and"
            + " b.bilCashDspId.dtbSequenceNbr = c.bilCashReceiptId.dtbSequenceNbr and SUBSTRING(c.receiptComment,1,2) <> :receiptComment)")
    public ZonedDateTime getMinDspDtCrCNSuspenseDspAmt(@Param("accountId") String accountId,
            @Param("bcdCrePolId") String bcdCrePolId, @Param("dspTypeCd") String dspTypeCd,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("manualSuspenseIndicatorList") List<Character> manualSuspenseIndicatorList,
            @Param("bilDspAmt") Double bilDspAmt, @Param("receiptComment") String receiptComment);

    @Query(value = "select min(b.dspDate) from BilCashDsp b, BilCashReceipt c where "
            + " b.bilCashDspId.accountId = :accountId and b.creditPolicyId = :bcdCrePolId and  b.dspTypeCd = :dspTypeCd and  b.revsRsusIndicator = :revsRsusIndicator  and b.creditIndicator IN :creditIndicatorList and"
            + " b.manualSuspenseIndicator IN :manualSuspenseIndicatorList and (b.bilCashDspId.accountId = c.bilCashReceiptId.accountId and b.bilCashDspId.bilDtbDate = c.bilCashReceiptId.bilDtbDate and"
            + " b.bilCashDspId.dtbSequenceNbr = c.bilCashReceiptId.dtbSequenceNbr and SUBSTRING(c.receiptComment,1,2) <> :receiptComment)")
    public ZonedDateTime getMinDspDtForExcessCredit(@Param("accountId") String accountId,
            @Param("bcdCrePolId") String bcdCrePolId, @Param("dspTypeCd") String dspTypeCd,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("manualSuspenseIndicatorList") List<Character> manualSuspenseIndicatorList,
            @Param("receiptComment") String receiptComment);

    @Query(value = "select min(b.dspDate) from BilCashDsp b where "
            + " b.bilCashDspId.accountId = :accountId and b.creditPolicyId = :bcdCrePolId and  b.policyEffectiveDate = :polEffectiveDt and b.dspTypeCd = :dspTypeCd  "
            + " and b.creditIndicator = :creditIndicatorList and b.revsRsusIndicator = :revsRsusIndicatorList ")
    public ZonedDateTime getMinDspDtForCancelationCredit(@Param("accountId") String accountId,
            @Param("bcdCrePolId") String bcdCrePolId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("dspTypeCd") String dspTypeCd, @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicatorList") List<Character> revsRsusIndicatorList);

    @Query(value = "select SUM(b.dspAmount) from BilCashDsp b , BilCashReceipt c where "
            + " b.bilCashDspId.accountId = :accountId and b.creditPolicyId = :bcdCrePolId and  b.dspTypeCd = :dspTypeCd and  b.revsRsusIndicator = :revsRsusIndicator  and b.creditIndicator in :creditIndicatorList and"
            + " b.manualSuspenseIndicator in :manualSuspenseIndicatorList and (b.bilCashDspId.accountId = c.bilCashReceiptId.accountId and b.bilCashDspId.bilDtbDate = c.bilCashReceiptId.bilDtbDate and"
            + " b.bilCashDspId.dtbSequenceNbr = c.bilCashReceiptId.dtbSequenceNbr and SUBSTRING(c.receiptComment,1,2) <> :receiptComment)")
    public Double getCancelationCreditSuspenseRow(@Param("accountId") String accountId,
            @Param("bcdCrePolId") String bcdCrePolId, @Param("dspTypeCd") String dspTypeCd,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("manualSuspenseIndicatorList") List<Character> manualSuspenseIndicatorList,
            @Param("receiptComment") String receiptComment);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilCashDsp b set b.revsRsusIndicator = :revsRsusIndicator where "
            + "b.bilCashDspId.accountId = :accountId and b.dspTypeCd = :dspTypeCd and  b.manualSuspenseIndicator <> :manualSuspenseIndicator and b.revsRsusIndicator = :revsRsusIndicator1")
    public void updateRsusIndForAChByBilAccountId(@Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("accountId") String accountId, @Param("dspTypeCd") String dspTypeCd,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("revsRsusIndicator1") char revsRsusIndicator1);

    @Query("select SUM(a.dspAmount) from BilCashDsp a, BilCashReceipt b where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.creditIndicator not in :creditIndicatorList and a.bilCashDspId.bilDtbDate = :bilDtbDate and a.bilCashDspId.dtbSequenceNbr = :dtbSequenceNbr and "
            + " (b.bilCashReceiptId.accountId = a.bilCashDspId.accountId and b.bilCashReceiptId.bilDtbDate = a.bilCashDspId.bilDtbDate and "
            + " b.bilCashReceiptId.dtbSequenceNbr = a.bilCashDspId.dtbSequenceNbr and SUBSTRING( b.receiptComment,1,2) <> :receiptComment)")
    public Double getTransferDspAmount1(@Param("accountId") String accountId, @Param("dspTypeCd") String dspTypeCd,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate, @Param("dtbSequenceNbr") short dtbSequenceNbr,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("receiptComment") String receiptComment);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilCashDsp b set b.revsRsusIndicator = :revsRsusIndicator where "
            + " b.bilCashDspId.accountId = :accountId and b.bilCashDspId.bilDtbDate = :bilDtbDate and b.bilCashDspId.dtbSequenceNbr = :bilDtbSeqNbr  "
            + " and b.dspTypeCd = :dspTypeCd and  b.manualSuspenseIndicator <> :manualSuspenseIndicator and b.revsRsusIndicator = :revsRsusIndicator1 ")
    public Integer updateRsusIndByBilAccountId(@Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("accountId") String accountId, @Param("dspTypeCd") String dspTypeCd,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate, @Param("bilDtbSeqNbr") short bilDtbSeqNbr,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("revsRsusIndicator1") char revsRsusIndicator1);

    @Query("select MIN(a.dspDate) from BilCashDsp a, BilCashReceipt b where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + " and a.manualSuspenseIndicator in :manualSuspenseIndicatorList and a.creditPolicyId = :creditPolicyId and "
            + " a.policyEffectiveDate = :policyEffectiveDate and a.creditIndicator in :creditIndicatorList and a.revsRsusIndicator = :revsRsusIndicator "
            + " and (b.bilCashReceiptId.accountId = a.bilCashDspId.accountId and b.bilCashReceiptId.bilDtbDate = a.bilCashDspId.bilDtbDate and "
            + " b.bilCashReceiptId.dtbSequenceNbr = a.bilCashDspId.dtbSequenceNbr and SUBSTRING( b.receiptComment,1,2) <> :receiptComment)")
    public ZonedDateTime getMinDspDateByDisbToSource(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd,
            @Param("manualSuspenseIndicatorList") List<Character> manualSuspenseIndicatorList,
            @Param("creditPolicyId") String creditPolicyId,
            @Param("policyEffectiveDate") ZonedDateTime policyEffectiveDate,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("receiptComment") String receiptComment);

    @Query("select a from BilCashDsp a where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + " and a.manualSuspenseIndicator <> :manualSuspenseIndicator and a.revsRsusIndicator = :revsRsusIndicator"
            + " and a.disbursementDate <= :disbursementDate ")
    public List<BilCashDsp> getCreditCashInPast(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("disbursementDate") ZonedDateTime disbursementDate);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilCashDsp b set b.disbursementDate = :defaultDate where "
            + " b.bilCashDspId.accountId = :accountId and b.dspTypeCd = :dspTypeCd  and  b.manualSuspenseIndicator <> :manualSuspenseIndicator and b.revsRsusIndicator = :revsRsusIndicator and b.disbursementDate <> :defaultDate1 and "
            + " b.bilCashDspId.accountId in (select c.bilCashReceiptId.accountId from BilCashReceipt c where b.bilCashDspId.accountId = c.bilCashReceiptId.accountId and "
            + " b.bilCashDspId.bilDtbDate = c.bilCashReceiptId.bilDtbDate and"
            + " b.bilCashDspId.dtbSequenceNbr = c.bilCashReceiptId.dtbSequenceNbr and SUBSTRING(c.receiptComment,1,2) <> :receiptComment)")
    public void updateDsbDtByBilAccontId(@Param("defaultDate") ZonedDateTime defaultDate,
            @Param("accountId") String accountId, @Param("dspTypeCd ") String dspTypeCd,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("defaultDate1") ZonedDateTime defaultDate1,
            @Param("receiptComment") String receiptComment);

    @Query(value = "select COUNT(b) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.policyId = :policyId and "
            + " b.policyEffectiveDate = :policyEffectiveDate and b.dspDate = :dspDate and b.dspTypeCd = :dspTypeCd and b.revsRsusIndicator = :bilRevsRsusInd")
    public Integer findCashAppliedToPolicy(@Param("accountId") String accountId, @Param("policyId") String policyId,
            @Param("policyEffectiveDate") ZonedDateTime policyEffectiveDate, @Param("dspDate") ZonedDateTime dspDate,
            @Param("dspTypeCd") String dspTypeCd, @Param("bilRevsRsusInd") char bilRevsRsusInd);

    @Query("select SUM(a.dspAmount) from BilCashDsp a, BilCashReceipt b where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.manualSuspenseIndicator <> :manualSuspenseIndicator and a.creditIndicator in :creditIndicatorList and a.revsRsusIndicator = :revsRsusIndicator and "
            + " (b.bilCashReceiptId.accountId = a.bilCashDspId.accountId and b.bilCashReceiptId.bilDtbDate = a.bilCashDspId.bilDtbDate and "
            + " b.bilCashReceiptId.dtbSequenceNbr = a.bilCashDspId.dtbSequenceNbr and SUBSTRING( b.receiptComment,1,2) <> :receiptComment)")
    public Double getSuspendDspAmount(@Param("accountId") String accountId, @Param("dspTypeCd") String dspTypeCd,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("receiptComment") String receiptComment);

    @Query("select a from BilCashDsp a where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.manualSuspenseIndicator <> :manualSuspenseIndicator and a.creditIndicator in :creditIndicatorList and "
            + " a.revsRsusIndicator = :revsRsusIndicator and a.policyEffectiveDate < :policyEffectiveDate "
            + "order by a.creditPolicyId, a.policyEffectiveDate ")
    public List<BilCashDsp> getAcwTriggerIsExisting(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("policyEffectiveDate") ZonedDateTime policyEffectiveDate);

    @Query("select SUM(a.dspAmount) from BilCashDsp a, BilCashReceipt b where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.manualSuspenseIndicator <> :manualSuspenseIndicator and a.creditIndicator not in :creditIndicatorList and a.revsRsusIndicator = :revsRsusIndicator and "
            + " (b.bilCashReceiptId.accountId = a.bilCashDspId.accountId and b.bilCashReceiptId.bilDtbDate = a.bilCashDspId.bilDtbDate and "
            + " b.bilCashReceiptId.dtbSequenceNbr = a.bilCashDspId.dtbSequenceNbr and SUBSTRING( b.receiptComment,1,2) <> :receiptComment)")
    public Double getTransferDspAmount(@Param("accountId") String accountId, @Param("dspTypeCd") String dspTypeCd,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("receiptComment") String receiptComment);

    @Query("select SUM(a.dspAmount) from BilCashDsp a, BilCashReceipt b where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.manualSuspenseIndicator <> :manualSuspenseIndicator and a.creditIndicator not in :creditIndicatorList and "
            + " a.revsRsusIndicator = :revsRsusIndicator and a.disbursementDate <= :disbursementDate and "
            + " (b.bilCashReceiptId.accountId = a.bilCashDspId.accountId and b.bilCashReceiptId.bilDtbDate = a.bilCashDspId.bilDtbDate and "
            + " b.bilCashReceiptId.dtbSequenceNbr = a.bilCashDspId.dtbSequenceNbr and SUBSTRING( b.receiptComment,1,2) <> :receiptComment)")
    public Double getSuspendDspAmountInPast(@Param("accountId") String accountId, @Param("dspTypeCd") String dspTypeCd,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("disbursementDate") ZonedDateTime disbursementDate, @Param("receiptComment") String receiptComment);

    @Query("select SUM(a.dspAmount) from BilCashDsp a, BilCashReceipt b where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.manualSuspenseIndicator <> :manualSuspenseIndicator and a.creditIndicator not in :creditIndicatorList and "
            + " a.revsRsusIndicator = :revsRsusIndicator and a.disbursementDate = :disbursementDate and "
            + " (b.bilCashReceiptId.accountId = a.bilCashDspId.accountId and b.bilCashReceiptId.bilDtbDate = a.bilCashDspId.bilDtbDate and "
            + " b.bilCashReceiptId.dtbSequenceNbr = a.bilCashDspId.dtbSequenceNbr and SUBSTRING( b.receiptComment,1,2) <> :receiptComment)")
    public Double getSuspendDspAmountByDisbursementDate(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("disbursementDate") ZonedDateTime disbursementDate, @Param("receiptComment") String receiptComment);

    @Query("select a from BilCashDsp a where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.manualSuspenseIndicator <> :manualSuspenseIndicator and a.creditIndicator not in :creditIndicatorList and "
            + " a.revsRsusIndicator = :revsRsusIndicator and (a.disbursementDate = :firstDisbursementDate or a.disbursementDate = :secondDisbursementDate)")
    public List<BilCashDsp> getCreditCashIsExisting(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("firstDisbursementDate") ZonedDateTime firstDisbursementDate,
            @Param("secondDisbursementDate") ZonedDateTime secondDisbursementDate);

    @Query("select SUM(a.dspAmount) from BilCashDsp a, BilCashReceipt b where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.manualSuspenseIndicator <> :manualSuspenseIndicator and a.creditIndicator not in :creditIndicatorList and "
            + " a.revsRsusIndicator = :revsRsusIndicator and (a.disbursementDate = :firstDisbursementDate or a.disbursementDate = :secondDisbursementDate) and "
            + " (b.bilCashReceiptId.accountId = a.bilCashDspId.accountId and b.bilCashReceiptId.bilDtbDate = a.bilCashDspId.bilDtbDate and "
            + " b.bilCashReceiptId.dtbSequenceNbr = a.bilCashDspId.dtbSequenceNbr and SUBSTRING( b.receiptComment,1,2) <> :receiptComment)")
    public Double getSuspendDspAmountIsExisting(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("firstDisbursementDate") ZonedDateTime firstDisbursementDate,
            @Param("secondDisbursementDate") ZonedDateTime secondDisbursementDate,
            @Param("receiptComment") String receiptComment);

    @Query("select a from BilCashDsp a where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.manualSuspenseIndicator = :manualSuspenseIndicator or a.creditIndicator in :creditIndicatorList and "
            + " a.revsRsusIndicator = :revsRsusIndicator")
    public List<BilCashDsp> getFirstDisburToSourceRow(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicator") char revsRsusIndicator);

    @Query("select SUM(a.dspAmount) from BilCashDsp a, BilCashReceipt b where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.manualSuspenseIndicator <> :manualSuspenseIndicator and a.revsRsusIndicator = :revsRsusIndicator and "
            + " (b.bilCashReceiptId.accountId = a.bilCashDspId.accountId and b.bilCashReceiptId.bilDtbDate = a.bilCashDspId.bilDtbDate and "
            + " b.bilCashReceiptId.dtbSequenceNbr = a.bilCashDspId.dtbSequenceNbr and SUBSTRING( b.receiptComment,1,2) <> :receiptComment)")
    public Double getSuspendPaymentDspAmount(@Param("accountId") String accountId, @Param("dspTypeCd") String dspTypeCd,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("receiptComment") String receiptComment);

    @Query("select SUM(a.dspAmount) from BilCashDsp a, BilCashReceipt b where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.manualSuspenseIndicator in :manualSuspenseIndicatorList and a.creditIndicator in :creditIndicatorList and a.revsRsusIndicator = :revsRsusIndicator "
            + " and (b.bilCashReceiptId.accountId = a.bilCashDspId.accountId and b.bilCashReceiptId.bilDtbDate = a.bilCashDspId.bilDtbDate and "
            + " b.bilCashReceiptId.dtbSequenceNbr = a.bilCashDspId.dtbSequenceNbr and SUBSTRING( b.receiptComment,1,2) <> :receiptComment)")
    public Double getCashEnteredByAnOperator(@Param("accountId") String accountId, @Param("dspTypeCd") String dspTypeCd,
            @Param("manualSuspenseIndicatorList") List<Character> manualSuspenseIndicatorList,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("receiptComment") String receiptComment);

    @Query("select MIN(a.dspDate) from BilCashDsp a, BilCashReceipt b where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.manualSuspenseIndicator in :manualSuspenseIndicatorList and a.creditIndicator in :creditIndicatorList and "
            + " a.revsRsusIndicator = :revsRsusIndicator and a.dspAmount <= :dspAmount and "
            + " (b.bilCashReceiptId.accountId = a.bilCashDspId.accountId and b.bilCashReceiptId.bilDtbDate = a.bilCashDspId.bilDtbDate and "
            + " b.bilCashReceiptId.dtbSequenceNbr = a.bilCashDspId.dtbSequenceNbr and SUBSTRING( b.receiptComment,1,2) <> :receiptComment)")
    public ZonedDateTime getMinDisburseDate(@Param("accountId") String accountId, @Param("dspTypeCd") String dspTypeCd,
            @Param("manualSuspenseIndicatorList") List<Character> manualSuspenseIndicatorList,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("dspAmount") double dspAmount,
            @Param("receiptComment") String receiptComment);

    @Query("select MIN(a.dspDate) from BilCashDsp a, BilCashReceipt b where a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd "
            + "and a.manualSuspenseIndicator in :manualSuspenseIndicatorList and a.creditIndicator in :creditIndicatorList and a.revsRsusIndicator = :revsRsusIndicator "
            + " and (b.bilCashReceiptId.accountId = a.bilCashDspId.accountId and b.bilCashReceiptId.bilDtbDate = a.bilCashDspId.bilDtbDate and "
            + " b.bilCashReceiptId.dtbSequenceNbr = a.bilCashDspId.dtbSequenceNbr and SUBSTRING( b.receiptComment,1,2) <> :receiptComment)")
    public ZonedDateTime getMinCashEnteredDate(@Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd,
            @Param("manualSuspenseIndicatorList") List<Character> manualSuspenseIndicatorList,
            @Param("creditIndicatorList") List<Character> creditIndicatorList,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("receiptComment") String receiptComment);

    @Query(value = "select COUNT(b) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.policyId = :policyId and "
            + "b.policyEffectiveDate = :polEffectiveDt and b.revsRsusIndicator = :reverseReSusInd and b.dspTypeCd = :dspTypeCode and "
            + "b.dspAmount > :dspAmount ")
    public Integer checkPcnnRow(@Param("accountId") String accountId, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("reverseReSusInd") char reverseReSusInd,
            @Param("dspTypeCode") String dspTypeCode, @Param("dspAmount") double dspAmount);

    @Query("select b from BilCashDsp b where b.policyId = :policyId and b.bilCashDspId.accountId = :accountId and "
            + "b.dspTypeCd = :dspTypeCd and b.bilSequenceNumber = :bilSequenceNumber ")
    public List<BilCashDsp> fetchPcnCashRow(@Param("policyId") String policyId, @Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("bilSequenceNumber") short bilSequenceNumber);

    @Query("select a.dspDate from BilCashDsp a, BilCashReceipt b where a.policyId = :policyId and a.policyEffectiveDate = :policyEffectiveDate "
            + " and a.bilCashDspId.accountId = :accountId and a.dspTypeCd = :dspTypeCd and a.revsRsusIndicator = :revsRsusIndicator "
            + " and (b.bilCashReceiptId.accountId = a.bilCashDspId.accountId and b.bilCashReceiptId.bilDtbDate = a.bilCashDspId.bilDtbDate and "
            + " b.bilCashReceiptId.dtbSequenceNbr = a.bilCashDspId.dtbSequenceNbr) and b.postMarkDate > :postMarkDate")
    public List<ZonedDateTime> getBilAcyDateByPortMarkDate(@Param("policyId") String policyId,
            @Param("policyEffectiveDate") ZonedDateTime policyEffectiveDate, @Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("postMarkDate") ZonedDateTime postMarkDate);

    @Query(value = "select COUNT(b) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.bilCashDspId.bilDtbDate = :bilDtbDate and "
            + "b.dspTypeCd in :dspTypeCodeList and b.revsRsusIndicator in :reverseReSusIndList and b.creditIndicator in :creditIndList")
    public Integer checkForPaymentToday(@Param("accountId") String accountId,
            @Param("bilDtbDate") ZonedDateTime bilDtbDate, @Param("dspTypeCodeList") List<String> dspTypeCodeList,
            @Param("reverseReSusIndList") List<Character> reverseReSusIndList,
            @Param("creditIndList") List<Character> creditIndList);

    @Query(value = "select COUNT( b) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.bilCashDspId.bilDtbDate = :distributionDate and "
            + "b.bilCashDspId.dtbSequenceNbr = :sequenceNumber and b.policyNumber = :policyNumber and b.policySymbolCd = :policySymbolCd and "
            + "b.dspTypeCd= :dspTypeCode and b.creditIndicator in :creditIndList and b.revsRsusIndicator in :revsRsusIndList")
    public Integer countPaymentCashToday(@Param("accountId") String accountId,
            @Param("distributionDate") ZonedDateTime distributionDate, @Param("sequenceNumber") Short sequenceNumber,
            @Param("policyNumber") String policyNumber, @Param("policySymbolCd") String policySymbolCd,
            @Param("dspTypeCode") String dspTypeCode, @Param("creditIndList") List<Character> creditIndList,
            @Param("revsRsusIndList") List<Character> revsRsusIndList);

    @Query(value = "select COUNT(b) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.dspDate = :dspDate and "
            + "b.dspTypeCd in :dspTypeCodeList and b.creditIndicator in :creditIndList and b.revsRsusIndicator = :reverseReSusInd ")
    public Integer checkForCreditToday(@Param("accountId") String accountId, @Param("dspDate") ZonedDateTime dspDate,
            @Param("dspTypeCodeList") List<String> dspTypeCodeList,
            @Param("creditIndList") List<Character> creditIndList, @Param("reverseReSusInd") char reverseReSusInd);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.dspTypeCd = :bilDspTypeCode and b.policyId = :policyId and b.policyEffectiveDate = :policyEffectiveDate and "
            + " b.dspDate = :dspDate and b.revsRsusIndicator = :revsRsusIndicator")
    public Double getDspAmountForAppliedByDspDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilDspTypeCode") String bilDspTypeCode, @Param("policyId") String policyId,
            @Param("policyEffectiveDate") ZonedDateTime policyEffectiveDate, @Param("dspDate") ZonedDateTime dspDate,
            @Param("revsRsusIndicator") char revsRsusIndicator);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.bilCashDspId.bilDtbDate = :distributionDate and b.bilCashDspId.dtbSequenceNbr = :sequenceNumber and "
            + " b.dspTypeCd = :bilDspTypeCode and b.policyNumber = :policyNumber and b.policySymbolCd = :policySymbolCd and "
            + " b.adjustmentDueDate <= :adjustmentDueDate and b.revsRsusIndicator = :revsRsusIndicator and b.chargeTypeCd = :chargeTypeCd ")
    public Double getDspAmountForAppliedBydistributionDate(@Param("bilAccountId") String bilAccountId,
            @Param("distributionDate") ZonedDateTime distributionDate, @Param("sequenceNumber") short sequenceNumber,
            @Param("bilDspTypeCode") String bilDspTypeCode, @Param("policyNumber") String policyNumber,
            @Param("policySymbolCd") String policySymbolCd, @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("chargeTypeCd") char chargeTypeCd);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.bilCashDspId.bilDtbDate = :distributionDate and b.bilCashDspId.dtbSequenceNbr = :sequenceNumber and "
            + " b.policyNumber = :policyNumber and b.policySymbolCd in :policySymbolCdList and b.manualSuspenseIndicator <> :manualSuspenseIndicator and "
            + " b.revsRsusIndicator = :revsRsusIndicator and b.creditIndicator in :creditIndList and b.dspTypeCd in :dspTypeCodeList")
    public Double getDspAmountForReinstateCash(@Param("bilAccountId") String bilAccountId,
            @Param("distributionDate") ZonedDateTime distributionDate, @Param("sequenceNumber") short sequenceNumber,
            @Param("policyNumber") String policyNumber, @Param("policySymbolCdList") List<String> policySymbolCdList,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("creditIndList") List<Character> creditIndList,
            @Param("dspTypeCodeList") List<String> dspTypeCodeList);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.bilCashDspId.bilDtbDate = :distributionDate and b.bilCashDspId.dtbSequenceNbr = :sequenceNumber and "
            + " b.policyNumber = :policyNumber and b.policySymbolCd = :policySymbolCd and b.manualSuspenseIndicator <> :manualSuspenseIndicator and "
            + " b.revsRsusIndicator = :revsRsusIndicator and b.creditIndicator in :creditIndList and b.chargeTypeCd = :chargeTypeCd and "
            + " b.dspTypeCd = :dspTypeCode")
    public Double getDspAmountAfterActDir(@Param("bilAccountId") String bilAccountId,
            @Param("distributionDate") ZonedDateTime distributionDate, @Param("sequenceNumber") short sequenceNumber,
            @Param("policyNumber") String policyNumber, @Param("policySymbolCd") String policySymbolCd,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("creditIndList") List<Character> creditIndList,
            @Param("chargeTypeCd") char chargeTypeCd, @Param("dspTypeCode") String dspTypeCode);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.manualSuspenseIndicator <> :manualSuspenseIndicator and b.revsRsusIndicator = :revsRsusIndicator and "
            + " b.creditIndicator not in :creditIndList and b.chargeTypeCd = :chargeTypeCd and b.dspTypeCd = :dspTypeCode")
    public Double getDspAmountForCanCredit(@Param("bilAccountId") String bilAccountId,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("revsRsusIndicator") char revsRsusIndicator, @Param("creditIndList") List<Character> creditIndList,
            @Param("chargeTypeCd") char chargeTypeCd, @Param("dspTypeCode") String dspTypeCode);

    @Query(value = "select SUM( b.dspAmount) from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.policyNumber = :policyNumber "
            + " and b.policySymbolCd = :policySymbolCd and b.dspDate = :dspDate and b.dspTypeCd = :dspTypeCd "
            + " and b.revsRsusIndicator = :revsRsusIndicator and b.manualSuspenseIndicator <> :manualSuspenseIndicator "
            + " and b.creditIndicator in :creditIndicatorList ")
    public Double fetchPcnDspAmounts(@Param("accountId") String accountId, @Param("policyNumber") String policyNumber,
            @Param("policySymbolCd") String policySymbolCd, @Param("dspDate") ZonedDateTime dspDate,
            @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("manualSuspenseIndicator") char manualSuspenseIndicator,
            @Param("creditIndicatorList") List<Character> creditIndicatorList);

    @Query(value = "select b from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.systemDueDate = :systemDueDate")
    public List<BilCashDsp> findBySystemDueDate(@Param("accountId") String accountId,
            @Param("systemDueDate") ZonedDateTime systemDueDate);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET  b.systemDueDate = :systemDueDateUpdate, b.adjustmentDueDate = :adjustmentDueDate,"
            + "  b.invoiceDate = :invoiceDate where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.systemDueDate = :systemDueDate and b.chargeTypeCd = :chargeTypeCd ")
    public Integer updateCashRowBySysDate(@Param("systemDueDateUpdate") ZonedDateTime systemDueDateUpdate,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("invoiceDate") ZonedDateTime invoiceDate, @Param("bilAccountId") String bilAccountId,
            @Param("systemDueDate") ZonedDateTime systemDueDate, @Param("chargeTypeCd") char chargeTypeCd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET  b.adjustmentDueDate = :adjustmentDueDate,"
            + "  b.invoiceDate = :invoiceDate where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.systemDueDate = :systemDueDate and b.chargeTypeCd <> :chargeTypeCd ")
    public Integer updateRescindCashRowBySysDate(@Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("invoiceDate") ZonedDateTime invoiceDate, @Param("bilAccountId") String bilAccountId,
            @Param("systemDueDate") ZonedDateTime systemDueDate, @Param("chargeTypeCd") char chargeTypeCd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET  b.systemDueDate = :systemDueDate, b.adjustmentDueDate = :adjustmentDueDate,"
            + "  b.invoiceDate = :invoiceDate where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.policyId = :policyId and b.bilSequenceNumber = :bilSequenceNumber ")
    public Integer updateCashRowBybilSeqNbr(@Param("systemDueDate") ZonedDateTime systemDueDate,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("invoiceDate") ZonedDateTime invoiceDate, @Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bilSequenceNumber") short bilSequenceNumber);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET  b.adjustmentDueDate = :adjustmentDueDateUpdate,"
            + "  b.invoiceDate = :invoiceDateUpdate where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.adjustmentDueDate = :adjustmentDueDate and b.invoiceDate = :invoiceDate and "
            + " b.chargeTypeCd <> :chargeTypeCd ")
    public Integer updateRescindCashRowByAdjDueDate(
            @Param("adjustmentDueDateUpdate") ZonedDateTime adjustmentDueDateUpdate,
            @Param("invoiceDateUpdate") ZonedDateTime invoiceDateUpdate, @Param("bilAccountId") String bilAccountId,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("invoiceDate") ZonedDateTime invoiceDate, @Param("chargeTypeCd") char chargeTypeCd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET  b.adjustmentDueDate = :adjustmentDueDateUpdate,"
            + "  b.invoiceDate = :invoiceDateUpdate where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.adjustmentDueDate = :adjustmentDueDate and b.invoiceDate = :invoiceDate ")
    public Integer updateRescindCashRowByAdjDueDate(
            @Param("adjustmentDueDateUpdate") ZonedDateTime adjustmentDueDateUpdate,
            @Param("invoiceDateUpdate") ZonedDateTime invoiceDateUpdate, @Param("bilAccountId") String bilAccountId,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("invoiceDate") ZonedDateTime invoiceDate);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET  b.adjustmentDueDate = :adjustmentDueDateUpdate,"
            + "  b.invoiceDate = :invoiceDateUpdate where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.adjustmentDueDate = :adjustmentDueDate and b.invoiceDate = :invoiceDate and "
            + " b.chargeTypeCd in :chargeTypeCdList ")
    public Integer updateRescindCashRowByAdjDueDate(
            @Param("adjustmentDueDateUpdate") ZonedDateTime adjustmentDueDateUpdate,
            @Param("invoiceDateUpdate") ZonedDateTime invoiceDateUpdate, @Param("bilAccountId") String bilAccountId,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("invoiceDate") ZonedDateTime invoiceDate,
            @Param("chargeTypeCdList") List<Character> chargeTypeCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET  b.systemDueDate = :systemDueDateUpdate, b.adjustmentDueDate = :adjustmentDueDate,"
            + "  b.invoiceDate = :invoiceDate where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.policyId = :policyId and b.policyEffectiveDate = :policyEffectiveDate and b.systemDueDate = :systemDueDate")
    public Integer updateCashRowByPolicyId(@Param("systemDueDateUpdate") ZonedDateTime systemDueDateUpdate,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("invoiceDate") ZonedDateTime invoiceDate, @Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("policyEffectiveDate") ZonedDateTime policyEffectiveDate,
            @Param("systemDueDate") ZonedDateTime systemDueDate);

    @Query(value = "select b from BilCashDsp b where b.bilCashDspId.accountId = :accountId and "
            + " b.dspTypeCd = :dspTypeCode and b.revsRsusIndicator = :revsRsusIndicator and b.policyId = :policyId and b.systemDueDate = :systemDueDate and "
            + "b.bilSequenceNumber = :bilSequenceNumber and b.chargeTypeCd = :chargeTypeCd and b.invoiceDate = :invoiceDate "
            + "order by b.dspDate desc, b.bilCashDspId.bilDtbDate desc")
    public List<BilCashDsp> getAdjustApplied(@Param("accountId") String accountId,
            @Param("dspTypeCode") String dspTypeCode, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("policyId") String policyId, @Param("systemDueDate") ZonedDateTime systemDueDate,
            @Param("bilSequenceNumber") short bilSequenceNumber, @Param("chargeTypeCd") char chargeTypeCd,
            @Param("invoiceDate") ZonedDateTime invoiceDate);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET b.invoiceDate = :invoiceDate where b.bilCashDspId.accountId = :accountId "
            + "and b.systemDueDate = :systemDueDate and  b.chargeTypeCd <> :chargeTypeCd")
    public void updateInvoiceDateRowBySystemDueDate(@Param("invoiceDate") ZonedDateTime invoiceDate,
            @Param("accountId") String accountId, @Param("systemDueDate") ZonedDateTime systemDueDate,
            @Param("chargeTypeCd") char chargeTypeCd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET b.invoiceDate = :invoiceDate,b.adjustmentDueDate = :adjustmentDueDate where b.bilCashDspId.accountId = :accountId "
            + "and b.systemDueDate = :systemDueDate and  b.chargeTypeCd <> :chargeTypeCd")
    public void updateInvDateAndAdjDateRowBySystemDueDate(@Param("invoiceDate") ZonedDateTime invoiceDate,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate, @Param("accountId") String accountId,
            @Param("systemDueDate") ZonedDateTime systemDueDate, @Param("chargeTypeCd") char chargeTypeCd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET b.invoiceDate = :invoiceDate,b.adjustmentDueDate = :adjustmentDueDate where b.bilCashDspId.accountId = :accountId "
            + "and b.systemDueDate = :systemDueDate ")
    public void updateInvDateAndAdjDateRowBySystemDueDate(@Param("invoiceDate") ZonedDateTime invoiceDate,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate, @Param("accountId") String accountId,
            @Param("systemDueDate") ZonedDateTime systemDueDate);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET b.invoiceDate = :invoiceDate,b.adjustmentDueDate = :adjustmentDueDate, b.systemDueDate = :systemDueDateUpdate "
            + " where b.bilCashDspId.accountId = :accountId and b.systemDueDate = :systemDueDate and  b.chargeTypeCd <> :chargeTypeCd")
    public void updateInvDateAndAdjDateRowByFpdd(@Param("invoiceDate") ZonedDateTime invoiceDate,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("systemDueDateUpdate") ZonedDateTime systemDueDateUpdate, @Param("accountId") String accountId,
            @Param("systemDueDate") ZonedDateTime systemDueDate, @Param("chargeTypeCd") char chargeTypeCd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET b.invoiceDate = :invoiceDate,b.adjustmentDueDate = :adjustmentDueDate, b.systemDueDate = :systemDueDateUpdate "
            + " where b.bilCashDspId.accountId = :accountId and b.systemDueDate = :systemDueDate ")
    public void updateInvDateAndAdjDateRowByOcb(@Param("invoiceDate") ZonedDateTime invoiceDate,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("systemDueDateUpdate") ZonedDateTime systemDueDateUpdate, @Param("accountId") String accountId,
            @Param("systemDueDate") ZonedDateTime systemDueDate);

    @Query("select b from BilCashDsp b where b.bilCashDspId.accountId = :accountId and b.policyId <> :policyId and "
            + "b.dspTypeCd = :dspTypeCd and b.chargeTypeCd = :chargeTypeCd and b.systemDueDate = :systemDueDate and "
            + "b.adjustmentDueDate = :adjustmentDueDate and b.invoiceDate = :invoiceDate ")
    public List<BilCashDsp> fetchDefaultCashRow(@Param("accountId") String accountId,
            @Param("policyId") String policyId, @Param("dspTypeCd") String dspTypeCd,
            @Param("chargeTypeCd") char chargeTypeCd, @Param("systemDueDate") ZonedDateTime systemDueDate,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("invoiceDate") ZonedDateTime invoiceDate);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET b.invoiceDate = :invoiceDateUpdate,b.adjustmentDueDate = :adjustmentDueDateUpdate, b.bilSequenceNumber = :bilSequenceNumberUpdate "
            + " where b.bilCashDspId.accountId = :accountId and b.dspTypeCd = :dspTypeCd and b.revsRsusIndicator = :revsRsusIndicator and "
            + " b.policyId = :policyId and b.systemDueDate = :systemDueDate and b.chargeTypeCd = :chargeTypeCd and b.invoiceDate = :invoiceDate")
    public void updateInvDateAndAdjDateRowBySequence(@Param("invoiceDateUpdate") ZonedDateTime invoiceDateUpdate,
            @Param("adjustmentDueDateUpdate") ZonedDateTime adjustmentDueDateUpdate,
            @Param("bilSequenceNumberUpdate") short bilSequenceNumberUpdate, @Param("accountId") String accountId,
            @Param("dspTypeCd") String dspTypeCd, @Param("revsRsusIndicator") char revsRsusIndicator,
            @Param("policyId") String policyId, @Param("systemDueDate") ZonedDateTime systemDueDate,
            @Param("bilSequenceNumber") short bilSequenceNumber, @Param("chargeTypeCd") char chargeTypeCd,
            @Param("invoiceDate") ZonedDateTime invoiceDate);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET  b.systemDueDate = :systemDueDateUpdate  "
            + " where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.systemDueDate = :systemDueDate and b.adjustmentDueDate = :adjustmentDueDate and  b.invoiceDate = :invoiceDate ")
    public Integer updateCashRowByOldScheduleDate(@Param("systemDueDateUpdate") ZonedDateTime systemDueDateUpdate,
            @Param("bilAccountId") String bilAccountId, @Param("systemDueDate") ZonedDateTime systemDueDate,
            @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("invoiceDate") ZonedDateTime invoiceDate);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilCashDsp b SET  b.systemDueDate = :systemDueDate,"
            + " b.invoiceDate = :invoiceDateUpdate,b.adjustmentDueDate = :adjustmentDueDateUpdate "
            + " where b.bilCashDspId.accountId = :bilAccountId and "
            + " b.adjustmentDueDate = :adjustmentDueDate and  b.invoiceDate = :invoiceDate ")
    public Integer updateCashRowByOldScheduleDate(@Param("systemDueDate") ZonedDateTime systemDueDate,
            @Param("invoiceDateUpdate") ZonedDateTime invoiceDateUpdate,
            @Param("adjustmentDueDateUpdate") ZonedDateTime adjustmentDueDateUpdate,
            @Param("bilAccountId") String bilAccountId, @Param("adjustmentDueDate") ZonedDateTime adjustmentDueDate,
            @Param("invoiceDate") ZonedDateTime invoiceDate);
}
