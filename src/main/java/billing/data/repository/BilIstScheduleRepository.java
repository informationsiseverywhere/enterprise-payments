package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilIstSchedule;
import billing.data.entity.id.BilIstScheduleId;

public interface BilIstScheduleRepository extends JpaRepository<BilIstSchedule, BilIstScheduleId> {

    @Query(value = "select SUM(b.billPrmIstAmt) , SUM(b.bisCreFutAmt) , SUM(b.bisCreOpnAmt) , SUM(b.bisCncFutAmt) , SUM(b.bisCncOpnAmt) , "
            + "SUM(b.bisPrmPaidAmt) , SUM(b.bisWroPrmAmt) , SUM(b.bisCrePaidAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId")
    public List<Object[]> getAccountBalanceSum(@Param("bilAccountId") String bilAccountId);

    @Query(value = "select SUM(b.billPrmIstAmt) , SUM(b.bisCreFutAmt) , SUM(b.bisCreOpnAmt) , SUM(b.bisCncFutAmt) , SUM(b.bisCncOpnAmt) , SUM(b.bisPrmPaidAmt) , "
            + "SUM(b.bisWroPrmAmt) , SUM(b.bisCrePaidAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + "b.billInvoiceCd IN :billInvoiceCdList")
    public List<Object[]> getInvoicedAccountBalanceSum(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select COUNT(b) from BilIstSchedule b where b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.billAccountId = :accountId and (b.billAdjDueDt <= :bilAdjDueDt "
            + "or :arg = :arg2)")
    public Integer getRowCountNumber(@Param("accountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("arg") String arg, @Param("arg2") String arg2);

    @Query(value = "select COUNT(b) from BilIstSchedule b where b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.billAccountId = :accountId and b.bilIstScheduleId.policyId = :policyId and "
            + "(b.billAdjDueDt = :bilAdjDueDt or :arg = :arg2)")
    public Integer getRowCountNumberByPolicyId(@Param("accountId") String accountId, @Param("policyId") String policyId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("arg") String arg, @Param("arg2") String arg2);

    @Query(value = "select COUNT(b) from BilIstSchedule b where b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.billAccountId = :accountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.billPblItemCd = :billPblItemCd and (b.billAdjDueDt = :bilAdjDueDt or :arg = :arg2)")
    public Integer getRowCountNumberByPayableItem(@Param("accountId") String accountId,
            @Param("policyId") String policyId, @Param("billPblItemCd") String billPblItemCd,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("arg") String arg, @Param("arg2") String arg2);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and  "
            + "b.bilIstScheduleId.policyId = :policyId and b.billPblItemCd = :billPblItemCd and (b.billAdjDueDt = :bilAdjDueDt or :arg = :arg2)")
    public Double getTotalDueAmountByPayableItem(@Param("accountId") String accountId,
            @Param("policyId") String policyId, @Param("billPblItemCd") String billPblItemCd,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("arg") String arg, @Param("arg2") String arg2);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and  "
            + "b.billPblItemCd = :billPblItemCd and b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = :policyId and (b.billAdjDueDt = :bilAdjDueDt or :arg = :arg2)")
    public Double getCurrentDueAmountByPayableItem(@Param("accountId") String accountId,
            @Param("policyId") String policyId, @Param("billPblItemCd") String billPblItemCd,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("arg") String arg, @Param("arg2") String arg2);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and  "
            + "b.billInvoiceCd in :billInvoiceCdList and (b.billAdjDueDt <= :bilAdjDueDt or :arg = :arg2)")
    public Double getCurrentlDueAmountByAccount(@Param("accountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("arg") String arg, @Param("arg2") String arg2);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and  "
            + "(b.billAdjDueDt = :bilAdjDueDt or :arg = :arg2)")
    public Double getTotalDueAmount(@Param("accountId") String accountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("arg") String arg, @Param("arg2") String arg2);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and  "
            + "b.bilIstScheduleId.policyId = :policyId and (b.billAdjDueDt = :bilAdjDueDt or :arg = :arg2)")
    public Double getTotalDueAmountByPolicyId(@Param("accountId") String accountId, @Param("policyId") String policyId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("arg") String arg, @Param("arg2") String arg2);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and  "
            + "b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = :policyId and (b.billAdjDueDt = :bilAdjDueDt or :arg = :arg2)")
    public Double getCurrentDueAmountByPolicyId(@Param("accountId") String accountId,
            @Param("policyId") String policyId, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("arg") String arg, @Param("arg2") String arg2);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId")
    public Double getTotalDueAmountByAccountId(@Param("accountId") String accountId);

    @Query(value = "select MAX(b.bilIstScheduleId.billSeqNbr) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and b.bilIstScheduleId.policyId = :policyId and b.billPblItemCd = :billPblItemCd")
    public Short getMaxSeqNbrByBilPayableItem(@Param("accountId") String accountId, @Param("policyId") String policyId,
            @Param("billPblItemCd") String billPblItemCd);

    @Query(value = "select distinct b.billInvoiceCd from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + " and b.policyEffectiveDt = :polEffectiveDt and b.billInvoiceCd in :billInvoiceCdList")
    public List<Character> fetchInvoiceCode(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilIstSchedule b SET b.billInvoiceCd = :invoiceCd where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + " and b.policyEffectiveDt = :polEffectiveDt and b.billInvoiceCd = :billInvoiceCd")
    public void updateRow(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("billInvoiceCd") Character billInvoiceCd,
            @Param("invoiceCd") Character invoiceCd);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId"
            + " and b.policyEffectiveDt = :policyEffectiveDt")
    public Double calculateBalanceAmount(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt)"
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and (b.billAdjDueDt = :bilAdjDueDt or :bilAdjDueDt1 = :arg4) and b.billPrmIstAmt > :billPrmIstAmt and"
            + " (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg5)")
    public Double fetchOpensAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilAdjDueDt1") ZonedDateTime bilAdjDueDt1,
            @Param("arg4") ZonedDateTime arg4, @Param("billPrmIstAmt") double billPrmIstAmt,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg5") String ar5);

    @Query(value = "select MIN(b.billAdjDueDt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and"
            + " (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and"
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public ZonedDateTime fetchMinAdjustDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt)"
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and b.billAdjDueDt = :bilAdjDueDt and b.billSysDueDt =:billSysDueDt "
            + " and b.billPrmIstAmt > :billPrmIstAmt and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4)")
    public Double fetchOpensAdjustAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("billPrmIstAmt") double billPrmIstAmt, @Param("bilSttDtlTypCd") String bilSttDtlTypCd,
            @Param("arg4") String arg4);

    @Query(value = "select COUNT(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billSysDueDt =:billSysDueDt and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public Integer findExludeRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Query(value = "select DISTINCT b.bilIstScheduleId.policyId, b.policyEffectiveDt from BilIstSchedule b where "
            + " b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt and "
            + "b.billInvoiceCd in :billInvoiceCdList and b.billSysDueDt =:billSysDueDt and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 "
            + " order by b.bilIstScheduleId.policyId, b.policyEffectiveDt asc")
    public List<Object[]> getExcludePolicyRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select COUNT(b) from BilIstSchedule b, BilPolicy c where c.bilPolicyId.bilAccountId = :bilAccountId and "
            + " c.bilPolicyId.policyId <> :policyId and b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt "
            + " and b.billSysDueDt =:billSysDueDt and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " c.bilIssueInd = :bilIssueInd and c.polNbr =:polNbr and c.polSymbolCd = :polSymbolCd and "
            + " b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = c.bilPolicyId.policyId and  "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public Integer findRNTRowByPolicyId(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("bilIssueInd") char bilIssueInd, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select COUNT(b) from BilIstSchedule b, BilPolicyTerm c, BilPolicy d where c.bilPolicyTermId.bilAccountId = :bilAccountId and "
            + " c.bilPolicyTermId.policyId = :policyId and c.bptCnrPolSymCd <> :bptCnrPolSymCd and c.bptCnrPolNbr <> :bptCnrPolNbr and "
            + " c.bilPolicyTermId.policyId <> d.bilPolicyId.policyId and c.bptCnrPolSymCd = d.polSymbolCd and c.bptCnrPolNbr = d.polNbr and "
            + " d.bilIssueInd = :bilIssueInd and b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt "
            + " and b.billSysDueDt =:billSysDueDt and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = d.bilPolicyId.policyId and  "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public Integer findCrwRowByPolicyId(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bptCnrPolSymCd") String bptCnrPolSymCd, @Param("bptCnrPolNbr") String bptCnrPolNbr,
            @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("bilIssueInd") char bilIssueInd, @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilIstSchedule b SET b.billInvoiceCd = :invoiceCd where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + " and b.billAdjDueDt = :bilAdjDueDt and b.billSysDueDt =:billSysDueDt and b.billInvoiceCd = :bilInvoiceCd")
    public Integer updateInvoiceRow(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("bilInvoiceCd") Character bilInvoiceCd, @Param("invoiceCd") Character invoiceCd);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt)"
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3 ) and b.billPrmIstAmt > :billPrmIstAmt and "
            + " (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 )")
    public Double fetchFuturesAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("billPrmIstAmt") double billPrmIstAmt, @Param("bilSttDtlTypCd") String bilSttDtlTypCd,
            @Param("arg4") String arg4);

    @Query(value = "select MIN(b.policyEffectiveDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and "
            + " b.bilIstScheduleId.policyId = :policyId and b.billAdjDueDt = :bilAdjDueDt and b.billSysDueDt =:billSysDueDt and "
            + " (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public ZonedDateTime getMinPolicyEffectiveDt(@Param("billAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilIstSchedule b SET b.billInvoiceCd = :invoiceCd where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and b.policyEffectiveDt <> :policyEffectiveDt "
            + " and b.billAdjDueDt = :bilAdjDueDt and b.billSysDueDt =:billSysDueDt and b.billInvoiceCd = :bilInvoiceCd")
    public Integer updateInvoiceRow(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt, @Param("bilInvoiceCd") Character bilInvoiceCd,
            @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("invoiceCd") Character invoiceCd);

    @Query(value = "select DISTINCT b.billPayPtyCd from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billInvoiceCd in :billInvoiceCdList and b.billSysDueDt =:billSysDueDt and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and"
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 "
            + " order by b.billPayPtyCd desc ")
    public List<Character> getPaymentPriorityRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billInvoiceCd in :billInvoiceCdList and b.billSysDueDt =:billSysDueDt and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and"
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " b.billPrmIstAmt > 0 and b.billPayPtyCd = :billPayPtyCd ")
    public Double getPriorityAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("billPayPtyCd") char billPayPtyCd);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billInvoiceCd in :billInvoiceCdList and (b.billSysDueDt =:billSysDueDt or :billSysDueDt1 = :defaultDate) and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and"
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 "
            + " and b.billPayPtyCd = :billPayPtyCd  order by b.policyEffectiveDt, b.bilIstScheduleId.policyId, b.billIstDueDt, b.billPblItemCd ")
    public List<BilIstSchedule> findPaymentPriorityRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billPayPtyCd") char billPayPtyCd, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("defaultDate") ZonedDateTime defaultDate, @Param("billSysDueDt1") ZonedDateTime billSysDueDt1);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b where "
            + " b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + " b.bilIstScheduleId.billSeqNbr = :billSeqNbr and b.billSysDueDt =:billSysDueDt and b.billAdjDueDt = :bilAdjDueDt "
            + " and b.billIstDueDt = :billIstDueDt and b.billPblItemCd = :bilPblItemCd and b.billPayPtyCd = :billPayPtyCd "
            + " and b.billInvoiceCd in :billInvoiceCdList and b.billPrmIstAmt > 0 ")
    public Double getPriorityAmountByPolicyId(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billSeqNbr") short billSeqNbr,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("billPayPtyCd") char billPayPtyCd, @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilIstSchedule b set b.billInvoiceCd = :invoiceCodeUpdate where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd = :billInvoiceCd")
    public Integer restoreHideAuditPremium(@Param("invoiceCodeUpdate") char invoiceCodeUpdate,
            @Param("bilAccountId") String bilAccountId, @Param("billInvoiceCd") char billInvoiceCd);

    @Query(nativeQuery = true, value = "select b from BIL_IST_SCHEDULE where b.BIL_ACCOUNT_ID = :bilAccountId and b.BIL_INVOICE_CD in :billInvoiceCdList and "
            + " (b.POLICY_ID = :policyId or :policyId = CAST(:arg AS varchar(32672))) and (b.BIL_PBL_ITEM_CD = :bilPblItemCd or :bilPblItemCd = CAST(:arg2 AS varchar(32672)))"
            + " and b.BIL_PRM_IST_AMT + b.BIS_CRE_OPN_AMT + b.BIS_CNC_OPN_AMT + b.BIS_CRE_FUT_AMT + b.BIS_CNC_FUT_AMT - b.BIS_PRM_PAID_AMT - b.BIS_CRE_PAID_AMT - b.BIS_WRO_PRM_AMT > 0"
            + " and EXISTS (SELECT 1 FROM BIL_POLICY_TRM BPT WHERE BPT.BIL_ACCOUNT_ID = b.BIL_ACCOUNT_ID and BPT.POLICY_ID = b.POLICY_ID"
            + " and BPT.POL_EFFECTIVE_DT = b.POL_EFFECTIVE_DT and BPT.PLN_EXP_DT <= :se3Date) ")
    public List<BilIstSchedule> fetchHideAuditPremium(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("se3Date") ZonedDateTime se3Date);

    @Query(value = "select MIN(b.billSysDueDt), MAX(b.billSysDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billInvoiceCd in :billInvoiceCdList and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and (b.billCommissionPct = :bilCommissionPct or "
            + " :bilCommissionPct = :arg3) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and b.billAdjDueDt = :bilAdjDueDt and "
            + " (b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) > 0 ")
    public List<Object[]> fetchSystemDueDateRange(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt);

    @Query(value = "select MIN(b.billSysDueDt)from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billInvoiceCd in :billInvoiceCdList and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and (b.billCommissionPct = :bilCommissionPct or "
            + " :bilCommissionPct = :arg3) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billSysDueDt > :billSysDueDt and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public ZonedDateTime fetchMinSystemDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billInvoiceCd in :billInvoiceCdList and (b.billSysDueDt =:billSysDueDt or :billSysDueDt1 = :defaultDate) and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and"
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public List<BilIstSchedule> findPaymentAdjustRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("defaultDate") ZonedDateTime defaultDate, @Param("billSysDueDt1") ZonedDateTime billSysDueDt1);

    @Query(value = "select b from BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billAdjDueDt = :bilAdjDueDt and (b.billSysDueDt =:billSysDueDt or :billSysDueDt1 = :defaultDate) "
            + " and b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId "
            + " and b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt "
            + " and c.billPolStatusCd <> :billPolStatusCd and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and (b.billCommissionPct = :bilCommissionPct or "
            + " :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public List<BilIstSchedule> findPaymentAdjustRowsByPolicy(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("defaultDate") ZonedDateTime defaultDate, @Param("billPolStatusCd") char billPolStatusCd,
            @Param("billSysDueDt1") ZonedDateTime billSysDueDt1);

    @Query(value = "select b from BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billAdjDueDt = :bilAdjDueDt and (b.billSysDueDt =:billSysDueDt or :billSysDueDt1 = :defaultDate) "
            + " and b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId "
            + " and b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt "
            + " and c.billPolStatusCd <> :billPolStatusCd and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and (b.billCommissionPct = :bilCommissionPct or "
            + " :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " b.billPayPtyCd = :billPayPtyCd and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public List<BilIstSchedule> findPartialAdjustRowsByPolicy(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("defaultDate") ZonedDateTime defaultDate, @Param("billPolStatusCd") char billPolStatusCd,
            @Param("billPayPtyCd") char billPayPtyCd, @Param("billSysDueDt1") ZonedDateTime billSysDueDt1);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilIstSchedule b SET b.billInvoiceCd = :invoiceCd where b.bilIstScheduleId.billAccountId = :bilAccountId  "
            + " and b.billAdjDueDt = :bilAdjDueDt and b.billSysDueDt =:billSysDueDt and b.billInvoiceCd = :bilInvoiceCd")
    public Integer updateInvoiceRow(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("bilInvoiceCd") Character bilInvoiceCd, @Param("invoiceCd") Character invoiceCd);

    @Query(value = "select MIN(b.billAdjDueDt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billSysDueDt =:billSysDueDt and"
            + "(b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and"
            + " (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and"
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public ZonedDateTime fetchMinAdjustDateRow(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = :policyId "
            + " and b.policyEffectiveDt = :policyEffectiveDt  and b.billPblItemCd not in :billPblItemCdList and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 "
            + " order by b.billAdjDueDt desc, b.billSysDueDt desc, b.bilIstScheduleId.billSeqNbr desc ")
    public List<BilIstSchedule> findPaymentByPolicy(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billPblItemCdList") List<String> billPblItemCdList,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = :policyId "
            + " and b.policyEffectiveDt = :policyEffectiveDt  and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 "
            + " order by b.billAdjDueDt desc, b.billSysDueDt desc, b.bilIstScheduleId.billSeqNbr desc ")
    public List<BilIstSchedule> findPaymentByAgreement(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.billInvoiceCd in :billInvoiceCdList and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg ) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and "
            + " (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4)"
            + " and b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0")
    public List<BilIstSchedule> fetchAllFuturesAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4);

    @Query(value = "select b from BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId "
            + " and b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt "
            + " and c.billPolStatusCd <> :billPolStatusCd and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and (b.billCommissionPct = :bilCommissionPct or "
            + " :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public List<BilIstSchedule> findPaymentFutureRowsByPolicy(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billPolStatusCd") char billPolStatusCd);

    @Query(value = "select MIN(b.billAdjDueDt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + "(b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and"
            + " (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and "
            + " (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) and"
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public ZonedDateTime futureMinAdjustDateRow(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("defaultDate") ZonedDateTime defaultDate,
            @Param("billIstDueDt1") ZonedDateTime billIstDueDt1);

    @Query(value = "select MIN(b.billAdjDueDt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + "(b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and"
            + " (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and "
            + " b.billIstDueDt = :billIstDueDt and"
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public ZonedDateTime futureIEAdjustDateRow(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt);

    @Query(value = "select MIN(b.billSysDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billInvoiceCd in :billInvoiceCdList and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and (b.billCommissionPct = :bilCommissionPct or "
            + " :bilCommissionPct = :arg3) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and "
            + " b.billAdjDueDt = :bilAdjDueDt and (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public ZonedDateTime futureSystemDueDateRange(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billIstDueDt") ZonedDateTime billIstDueDt,
            @Param("defaultDate") ZonedDateTime defaultDate, @Param("billIstDueDt1") ZonedDateTime billIstDueDt1);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt)"
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and b.billAdjDueDt = :bilAdjDueDt and b.billSysDueDt =:billSysDueDt "
            + " and b.billPrmIstAmt > :billPrmIstAmt and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and "
            + " (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) ")
    public Double futureOpensAdjustAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("billPrmIstAmt") double billPrmIstAmt, @Param("bilSttDtlTypCd") String bilSttDtlTypCd,
            @Param("arg4") String arg4, @Param("billIstDueDt") ZonedDateTime billIstDueDt,
            @Param("defaultDate") ZonedDateTime defaultDate, @Param("billIstDueDt1") ZonedDateTime billIstDueDt1);

    @Query(value = "select DISTINCT b.billPayPtyCd from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billInvoiceCd in :billInvoiceCdList and b.billSysDueDt =:billSysDueDt and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and"
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or "
            + " :bilSttDtlTypCd = :arg4 ) and (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 "
            + " order by b.billPayPtyCd desc ")
    public List<Character> futurePaymentPriorityRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("defaultDate") ZonedDateTime defaultDate,
            @Param("billIstDueDt1") ZonedDateTime billIstDueDt1);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billInvoiceCd in :billInvoiceCdList and b.billSysDueDt =:billSysDueDt and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and"
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) and "
            + " b.billPrmIstAmt > 0 and b.billPayPtyCd = :billPayPtyCd ")
    public Double futurePriorityAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("billPayPtyCd") char billPayPtyCd,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("defaultDate") ZonedDateTime defaultDate,
            @Param("billIstDueDt1") ZonedDateTime billIstDueDt1);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billInvoiceCd in :billInvoiceCdList and (b.billSysDueDt =:billSysDueDt or :billSysDueDt1 = :defaultDate) and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and"
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 "
            + " and b.billPayPtyCd = :billPayPtyCd  order by b.policyEffectiveDt, b.bilIstScheduleId.policyId, b.billIstDueDt, b.billPblItemCd ")
    public List<BilIstSchedule> futurePaymentPriorityRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billPayPtyCd") char billPayPtyCd, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("defaultDate") ZonedDateTime defaultDate, @Param("billIstDueDt") ZonedDateTime billIstDueDt,
            @Param("billIstDueDt1") ZonedDateTime billIstDueDt1, @Param("billSysDueDt1") ZonedDateTime billSysDueDt1);

    @Query(value = "select b from BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId "
            + " and b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt "
            + " and b.billAdjDueDt = :bilAdjDueDt and (b.billSysDueDt =:billSysDueDt or :billSysDueDt1 = :defaultDate) "
            + " and b.billPayPtyCd = :billPayPtyCd and (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) "
            + " and c.billPolStatusCd <> :billPolStatusCd and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and (b.billCommissionPct = :bilCommissionPct or "
            + " :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 "
            + " order by b.policyEffectiveDt, b.bilIstScheduleId.policyId, b.billIstDueDt, b.billPblItemCd ")
    public List<BilIstSchedule> futurePaymentPriorityRowsByPolicy(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billPolStatusCd") char billPolStatusCd, @Param("defaultDate") ZonedDateTime defaultDate,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("billPayPtyCd") char billPayPtyCd,
            @Param("billIstDueDt1") ZonedDateTime billIstDueDt1, @Param("billSysDueDt1") ZonedDateTime billSysDueDt1);

    @Query(value = "select b.billIstDueDt,  "
            + " SUM(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt), "
            + " count(b), MAX(b.bilIstScheduleId.policyId), MAX(b.bilIstScheduleId.billSeqNbr), b.billInvoiceCd "
            + " from BilIstSchedule b  where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billInvoiceCd in :billInvoiceCdList and  b.billAdjDueDt = :bilAdjDueDt "
            + " and b.billSysDueDt =:billSysDueDt and b.billPayPtyCd = :billPayPtyCd "
            + " and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and (b.billCommissionPct = :bilCommissionPct or "
            + " :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 "
            + " group by b.billIstDueDt, b.billInvoiceCd " + " order by b.billIstDueDt, b.billInvoiceCd ")
    public List<Object[]> futurePaymentPartialRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("defaultDate") ZonedDateTime defaultDate, @Param("billIstDueDt") ZonedDateTime billIstDueDt,
            @Param("billPayPtyCd") char billPayPtyCd, @Param("billIstDueDt1") ZonedDateTime billIstDueDt1);

    @Query(value = "select b.billIstDueDt,  "
            + " SUM(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt), "
            + " count(b), MAX(b.bilIstScheduleId.policyId), MAX(b.bilIstScheduleId.billSeqNbr), b.billInvoiceCd "
            + " from BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId "
            + " and b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt "
            + " and b.billAdjDueDt = :bilAdjDueDt and b.billSysDueDt =:billSysDueDt "
            + " and b.billPayPtyCd = :billPayPtyCd and (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) "
            + " and c.billPolStatusCd <> :billPolStatusCd and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and (b.billCommissionPct = :bilCommissionPct or "
            + " :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 "
            + " group by b.billIstDueDt, b.billInvoiceCd " + " order by b.billIstDueDt, b.billInvoiceCd ")
    public List<Object[]> fetchCatchupPaymentRowsByPolicy(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billPolStatusCd") char billPolStatusCd, @Param("defaultDate") ZonedDateTime defaultDate,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("billPayPtyCd") char billPayPtyCd,
            @Param("billIstDueDt1") ZonedDateTime billIstDueDt1);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billInvoiceCd in :billInvoiceCdList and (b.billSysDueDt =:billSysDueDt or :billSysDueDt1 = :defaultDate) and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and"
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public List<BilIstSchedule> futureFullPaymentRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("defaultDate") ZonedDateTime defaultDate, @Param("billIstDueDt") ZonedDateTime billIstDueDt,
            @Param("billSysDueDt1") ZonedDateTime billSysDueDt1, @Param("billIstDueDt1") ZonedDateTime billIstDueDt1);

    @Query(value = "select b from BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billAdjDueDt = :bilAdjDueDt and (b.billSysDueDt =:billSysDueDt or :billSysDueDt1 = :defaultDate) "
            + " and b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId "
            + " and b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt "
            + " and c.billPolStatusCd <> :billPolStatusCd and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and (b.billCommissionPct = :bilCommissionPct or "
            + " :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public List<BilIstSchedule> futureFullPaymentRowsByPolicy(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("defaultDate") ZonedDateTime defaultDate, @Param("billPolStatusCd") char billPolStatusCd,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("billSysDueDt1") ZonedDateTime billSysDueDt1,
            @Param("billIstDueDt1") ZonedDateTime billIstDueDt1);

    @Query(value = "select MIN(b.billAdjDueDt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + "(b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and"
            + " (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and "
            + " b.billAdjDueDt >= :bilAdjDueDt and (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) and"
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public ZonedDateTime catchupfutureMinAdjustDate(@Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilAccountId") String bilAccountId, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("defaultDate") ZonedDateTime defaultDate,
            @Param("billIstDueDt1") ZonedDateTime billIstDueDt1);

    @Query(value = "select MIN(b.billAdjDueDt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billSysDueDt =:billSysDueDt and "
            + "(b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and "
            + " (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and "
            + " (b.billIstDueDt = :billIstDueDt or :billIstDueDt1 = :defaultDate) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public ZonedDateTime fetchCatchupMinAdjustDateRow(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("defaultDate") ZonedDateTime defaultDate,
            @Param("billIstDueDt1") ZonedDateTime billIstDueDt1);

    @Query(value = "select b from BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId "
            + " and b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt "
            + " and b.billAdjDueDt = :bilAdjDueDt and b.billSysDueDt =:billSysDueDt "
            + " and b.billPayPtyCd = :billPayPtyCd and b.billIstDueDt = :billIstDueDt "
            + " and c.billPolStatusCd <> :billPolStatusCd and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and (b.billCommissionPct = :bilCommissionPct or "
            + " :bilCommissionPct = :arg3 ) and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public List<BilIstSchedule> groupCatchupRowsByPolicy(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billPolStatusCd") char billPolStatusCd, @Param("billIstDueDt") ZonedDateTime billIstDueDt,
            @Param("billPayPtyCd") char billPayPtyCd);

    @Query(value = "select b from BilIstSchedule b  where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billInvoiceCd in :billInvoiceCdList and  b.billAdjDueDt = :bilAdjDueDt and "
            + " b.billSysDueDt =:billSysDueDt and b.billPayPtyCd = :billPayPtyCd "
            + " and (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) "
            + " and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3 ) and "
            + " (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4 ) and "
            + " b.billIstDueDt = :billIstDueDt and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public List<BilIstSchedule> groupCatchupRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("billPayPtyCd") char billPayPtyCd);

    @Query(value = "select count(DISTINCT b.billAdjDueDt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and"
            + " (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and"
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public Integer spreadCountByAdjustDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4);

    @Query(value = "select MIN(b.billAdjDueDt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + "(b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and"
            + " (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and "
            + " b.billAdjDueDt > :bilAdjDueDt and "
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public ZonedDateTime spreadfutureMinAdjustDate(@Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilAccountId") String bilAccountId, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("policyId") String policyId, @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd,
            @Param("arg2") String arg2, @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4);

    @Query(value = "select MIN(b.billAdjDueDt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.bilIstScheduleId.policyId = :policyId or :policyId = :arg) and (b.billPblItemCd = :bilPblItemCd or :bilPblItemCd = :arg2) and "
            + " (b.billCommissionPct = :bilCommissionPct or :bilCommissionPct = :arg3) and "
            + " b.billAdjDueDt >= :billAdjDueDt and (b.billSttDtlTypeCd = :bilSttDtlTypCd or :bilSttDtlTypCd = :arg4) and"
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public ZonedDateTime fetchNextMinAdjustDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("arg") String arg, @Param("bilPblItemCd") String bilPblItemCd, @Param("arg2") String arg2,
            @Param("bilCommissionPct") double bilCommissionPct, @Param("arg3") double arg3,
            @Param("bilSttDtlTypCd") String bilSttDtlTypCd, @Param("arg4") String arg4,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt);

    @Query(value = "select MIN(b.billAdjDueDt), MAX(b.billAdjDueDt), MAX(b.billInvoiceCd) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.bilIstScheduleId.policyId = :policyId and  b.policyEffectiveDt = :polEffectiveDt "
            + " and b.billInvoiceCd in :billInvoiceCdList ")
    public List<Object[]> fetchAdjDueDateRange(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);

    @Query(value = "select MIN(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.bilIstScheduleId.policyId = :policyId and  b.policyEffectiveDt = :polEffectiveDt ")
    public ZonedDateTime fetchMinAdjDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);

    @Query(value = "select b from BilIstSchedule b  where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.bilIstScheduleId.policyId = :policyId and b.bilIstScheduleId.billSeqNbr =:billSeqNbr and "
            + " b.billInvoiceCd in :billInvoiceCdList ")
    public List<BilIstSchedule> getBilIstSchedulRowsByAccountId(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billSeqNbr") short billSeqNbr,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b "
            + " where b.bilIstScheduleId.billAccountId = :accountId and  "
            + " b.billInvoiceCd in :billInvoiceCdList and b.billAdjDueDt <= :bilAdjDueDt")
    public Double getCurrentDueAmount(@Param("accountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt), "
            + " SUM(b.bisPrmPaidAmt + b.bisCrePaidAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and b.bilIstScheduleId.policyId = :policyId "
            + " and b.policyEffectiveDt = :policyEffectiveDt and b.billInvoiceCd in :billInvoiceCdList")
    public List<Object[]> getCivAmounts(@Param("accountId") String accountId, @Param("policyId") String policyId,
            @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) AS SUM1, "
            + " SUM(b.bisPrmPaidAmt + b.bisCrePaidAmt) AS SUM2 from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and b.bilIstScheduleId.policyId = :policyId "
            + " and b.policyEffectiveDt = :policyEffectiveDt and ((b.billInvoiceCd = :billInvoiceCd and b.billInvDt = :billInvDt) or (b.billInvoiceCd = :billInvoiceCdSecond and b.billInvDt < :billInvDt)) ")
    public List<Object[]> getCivAmountsWithInvoiceDate(@Param("accountId") String accountId,
            @Param("policyId") String policyId, @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt,
            @Param("billInvoiceCd") char billInvoiceCd, @Param("billInvDt") ZonedDateTime billInvDt,
            @Param("billInvoiceCdSecond") char billInvoiceCdSecond);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + " from BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :accountId and b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId and "
            + " b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt and b.billAdjDueDt <= :bilAdjDueDt "
            + " and b.billInvoiceCd in :billInvoiceCdList")
    public Double getOutstandPremiumBalance(@Param("accountId") String accountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select SUM(b.billPrmIstAmt) + SUM(b.bisCreFutAmt) + SUM(b.bisCncFutAmt) - SUM(b.bisPrmPaidAmt) - SUM(b.bisCrePaidAmt) + SUM(b.bisCreOpnAmt) + SUM(b.bisCncOpnAmt) - SUM(b.bisWroPrmAmt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.billPblItemCd = :billPblItemCd and b.billInvoiceCd in :billInvoiceCdList and "
            + " (b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 )")
    public Double getBalanceDueAmount(@Param("accountId") String accountId, @Param("policyId") String policyId,
            @Param("billPblItemCd") String billPblItemCd,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select COUNT(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and b.billInvoiceCd = :billInvoiceCd and b.billInvDt >= :billInvDt")
    public Integer checkOverRidePenalty(@Param("accountId") String accountId,
            @Param("billInvoiceCd") char billInvoiceCd, @Param("billInvDt") ZonedDateTime billInvDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt) AS SUM1, "
            + " SUM(b.bisPrmPaidAmt + b.bisCrePaidAmt + b.bisWroPrmAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId")
    public List<Object[]> getFullpayTotalAmounts(@Param("accountId") String accountId);

    @Query(value = "select distinct b.bilIstScheduleId.policyId, b.policyEffectiveDt from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billInvDt >= :bilInvDate")
    public List<Object[]> fetchDistinctSchedulePolicy(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDate") ZonedDateTime bilInvDate);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b "
            + "where b.bilIstScheduleId.billAccountId = :accountId and b.bilIstScheduleId.policyId = :policyId")
    public Double getUnpaidPremAmount(@Param("accountId") String accountId, @Param("policyId") String policyId);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + " from BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :accountId and b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId and "
            + " b.bilIstScheduleId.policyId = :policyId and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt and "
            + " c.billPolStatusCd not in :billPolStatusCdList")
    public Double getPolicyBalance(@Param("accountId") String accountId, @Param("policyId") String policyId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + " from BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :accountId and b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId and "
            + " b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt and c.billPolStatusCd not in :billPolStatusCdList")
    public Double getAccountBalance(@Param("accountId") String accountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query(value = "select COUNT(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :policyEffectiveDt and b.billInvDt = :bilInvDate and b.billInvoiceCd = :billInvoiceCd")
    public Integer checkWorkInvoiceDate(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt, @Param("bilInvDate") ZonedDateTime bilInvDate,
            @Param("billInvoiceCd") char billInvoiceCd);

    @Query(value = "select COUNT(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :policyEffectiveDt and b.billInvDt > :bilInvDate")
    public Integer checkMoreTerms(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt, @Param("bilInvDate") ZonedDateTime bilInvDate);

    @Query(value = "select distinct b.billInvDt from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :policyEffectiveDt and b.billInvoiceCd = :billInvoiceCd")
    public List<ZonedDateTime> getWorkInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt,
            @Param("billInvoiceCd") char billInvoiceCd);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and b.billInvoiceCd in :billInvoiceCdList and b.billAdjDueDt = :bilAdjDueDt")
    public Double getScheduleDueAmount(@Param("accountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt);

    @Query(value = "select b from BilIstSchedule b  where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.bilIstScheduleId.policyId = :policyId and b.bisWroPrmAmt  <> :bisWroPrmAmt "
            + " order by b.billAdjDueDt, b.billPayPtyCd desc, b.bilIstScheduleId.billSeqNbr ")
    public List<BilIstSchedule> getPremiumWriteOffRows(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bisWroPrmAmt") double bisWroPrmAmt);

    @Query(value = "select MIN(b.billIstDueDt) from BilIstSchedule b  where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :policyEffectiveDt and "
            + " (b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) > 0 ")
    public ZonedDateTime getMinnimumIstDueDt(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt);

    @Query(value = "select b from BilIstSchedule b  where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :policyEffectiveDt and b.billIstDueDt = :billIstDueDt "
            + " order by b.billPayPtyCd desc ")
    public List<BilIstSchedule> getScheduleRowsByDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt) "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.billIstDueDt = :billIstDueDt and b.billPblItemCd = :billPblItemCd and b.policyEffectiveDt = :polEffectiveDt")
    public Double findAdjustAmount(@Param("bilAccountId") String billAccountId, @Param("policyId") String policyId,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("billPblItemCd") String billPblItemCd,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);

    @Query(value = "select MAX(b.billAdjDueDt) from BilIstSchedule b , BilPolicyTerm  c where b.bilIstScheduleId.billAccountId = :billAccountId and "
            + " b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId and "
            + " b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt and c.plnExpDt > :plnExpDt and c.billPolStatusCd in :billPolStatusCd "
            + " and b.billInvoiceCd in :billInvoiceCd and (b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + " > 0 and not exists (select 1 from BilDesReason d where b.billPblItemCd = d.bilDesReasonId.bilDesReasonType and d.bilDesReasonId.bilDesReasonCd = :bilDesReaCd ) ")
    public ZonedDateTime getMaxAdjDueDtByPolicyStatus(@Param("billAccountId") String accountId,
            @Param("plnExpDt") ZonedDateTime plnExpDt, @Param("billPolStatusCd") List<Character> bilPolStatuCd,
            @Param("billInvoiceCd") List<Character> billInvoiceCd, @Param("bilDesReaCd") String bilDesReaCd);

    @Query(value = "select MAX(b.billAdjDueDt) from BilIstSchedule b  where b.bilIstScheduleId.billAccountId = :billAccountId and "
            + "b.billAdjDueDt  > :billAdjDueDt and (b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - "
            + "b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) <> 0 ")
    public ZonedDateTime selectMaxAdjDueDtByBilAccountId(@Param("billAccountId") String accountId,
            @Param("billAdjDueDt") ZonedDateTime billlAdjDueDt);

    @Query(value = "select b.bilIstScheduleId.policyId, b.policyEffectiveDt, b.billAdjDueDt from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId"
            + " and b.billInvoiceCd IN :billInvoiceCdList and exists (select 1 from BilPolicyTerm c where b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId "
            + " and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt "
            + " and c.billPolStatusCd NOT IN :billPolStatusCdList )")
    public List<Object[]> getLateChargeScheduleRow(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt), b.billPblItemCd "
            + " from BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId "
            + " and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt and c.billPolStatusCd NOT IN :billPolStatusCdList "
            + " and b.billInvoiceCd IN :billInvoiceCdList ")
    public List<Object[]> getLateChargeBalance(@Param("bilAccountId") String bilAccountId,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select b.bilIstScheduleId.policyId, b.policyEffectiveDt from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId"
            + " and b.billAdjDueDt = :billAdjDueDt and exists (select 1 from BilPolicyTerm c where b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId "
            + " and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt "
            + " and c.billPolStatusCd NOT IN :billPolStatusCdList )")
    public List<Object[]> getChargeSchedule(@Param("bilAccountId") String bilAccountId,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt,
            @Param("billPolStatusCdList") List<Character> billPolStatusCdList);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId")
    public Double findBillingAccountBalance(@Param("bilAccountId") String billAccountId);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd NOT IN :billInvoiceCdList")
    public List<BilIstSchedule> getScheduleInvoicedRows(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) as SUM1 "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and b.billAdjDueDt <= :billAdjDueDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public Double findTermLastAmtByAdjDate(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt, @Param("busType") String busType);

    @Query(value = "select MAX(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billInvoiceCd in :billInvoiceCdList "
            + "and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and b.billInvDt = :billInvDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public ZonedDateTime getLastTermPastInvDate(@Param("billAccountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("billInvDt") ZonedDateTime billInvDt,
            @Param("busType") String busType);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt) AS PremiumSum "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and b.billInvDt = :billInvDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public Double findTermPremiumAmtByInvDate(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billInvDt") ZonedDateTime billInvDt, @Param("busType") String busType);

    @Query(value = "select MAX(b.billIstDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId "
            + "and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and b.billIstDueDt <= :billIstDueDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public ZonedDateTime getMaxBilIstDueDate(@Param("billAccountId") String accountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("busType") String busType);

    @Query(value = "select MIN(b.billIstDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId "
            + "and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public ZonedDateTime getMinBilIstDueDate(@Param("billAccountId") String accountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("busType") String busType);

    @Query(value = "select  SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) AS SUM "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId "
            + "and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and b.billIstDueDt <= :billIstDueDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public Double getAmountByBilIstDueDate(@Param("billAccountId") String accountId, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("billIstDueDt") ZonedDateTime billIstDueDt,
            @Param("busType") String busType);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt) AS PremiumSum "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and  b.billIstDueDt <= :billIstDueDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public Double findTermPremiumAmtByIstDueDate(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("busType") String busType);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCncOpnAmt) AS PremiumSum "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and b.billInvDt <= :billInvDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public Double findNsfTermPremiumAmtByInvDate(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billInvDt") ZonedDateTime billInvDt, @Param("busType") String busType);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) as SUM1 "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and b.billInvDt <= :billInvDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public Double findTermLastAmtByInvoiceDate(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billInvDt") ZonedDateTime billInvDt, @Param("busType") String busType);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) as SUM1, "
            + " MAX(b.billInvoiceCd) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + " and b.policyEffectiveDt = :polEffectiveDt ")
    public List<Object[]> fetchTermBalanceInvoiceCd(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt) AS PremiumSum "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and b.billAdjDueDt <= :billAdjDueDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public Double findTermBalanceByAdjDate(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt, @Param("busType") String busType);

    @Query(value = "select MAX(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billInvoiceCd in :billInvoiceCdList "
            + "and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and b.billAdjDueDt > :billAdjDueDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public ZonedDateTime getLastInvoiceAdjDate(@Param("billAccountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("billAdjDueDt") ZonedDateTime billAdjDueDt,
            @Param("busType") String busType);

    @Query(value = "select MAX(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billInvoiceCd in :billInvoiceCdList "
            + "and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and b.billAdjDueDt <= :billAdjDueDt and "
            + "(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) > :pastAmount "
            + " and not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public ZonedDateTime getMaxTermPastAdjDate(@Param("billAccountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("billAdjDueDt") ZonedDateTime billAdjDueDt,
            @Param("pastAmount") double pastAmount, @Param("busType") String busType);

    @Query(value = "select MIN(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billInvoiceCd in :billInvoiceCdList "
            + "and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and b.billAdjDueDt <= :billAdjDueDt and "
            + "(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) > :pastAmount "
            + " and not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public ZonedDateTime getMinTermPastAdjDate(@Param("billAccountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("billAdjDueDt") ZonedDateTime billAdjDueDt,
            @Param("pastAmount") double pastAmount, @Param("busType") String busType);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt) AS PremiumSum, "
            + "SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) AS PcnSum "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and b.billIstDueDt <= :billIstDueDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public List<Object[]> findNsfSumAmountsByDueDate(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("busType") String busType);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt) AS PremiumSum, "
            + "SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) AS PcnSum "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and b.billInvDt <= :billInvDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public List<Object[]> findNsfSumAmountsByInvoiceDate(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billInvDt") ZonedDateTime billInvDt, @Param("busType") String busType);

    @Query(value = "select MIN(b.billInvDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and "
            + "c.busCdTranslationId.busType = :busType)")
    public ZonedDateTime getMinBilInvoiceDateByPayItem(@Param("billAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("busType") String busType);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.billInvoiceCd NOT IN :billInvoiceCdList and b.policyEffectiveDt = :policyEffectiveDt")
    public List<BilIstSchedule> getRenewalPresentRows(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt);

    @Query(value = "select MAX(b.billReferenceDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt and b.billPblItemCd not in :billPblItemCdList and b.billInvoiceCd IN :billInvoiceCdList ")
    public ZonedDateTime getMaxReferenceDateByInvoiceCd(@Param("billAccountId") String accountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billPblItemCdList") List<String> billPblItemCdList,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select MIN(b.billReferenceDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt and b.billPblItemCd not in :billPblItemCdList and b.billInvoiceCd IN :billInvoiceCdList ")
    public ZonedDateTime getMinReferenceDateByInvoiceCd(@Param("billAccountId") String accountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billPblItemCdList") List<String> billPblItemCdList,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt and b.billPblItemCd not in :billPblItemCdList and b.billInvoiceCd IN :billInvoiceCdList "
            + "and b.billReferenceDt = :billReferenceDt ")
    public BilIstSchedule getScheduleRowByInvoiceCd(@Param("billAccountId") String accountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billPblItemCdList") List<String> billPblItemCdList,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billReferenceDt") ZonedDateTime billReferenceDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt")
    public Double getLastPolicyBalance(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);

    @Query(value = "select MAX(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and "
            + " b.bilIstScheduleId.policyId = :policyId and b.billInvoiceCd in :billInvoiceCdList ")
    public ZonedDateTime getMaxAdjDueDtByInvoice(@Param("billAccountId") String accountId,
            @Param("policyId") String policyId, @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select MAX(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and "
            + " b.bilIstScheduleId.policyId = :policyId and b.billAdjDueDt < :billAdjDueDt and b.billInvoiceCd = :billInvoiceCd ")
    public ZonedDateTime getMaxAdjDueDtByAdjustDate(@Param("billAccountId") String accountId,
            @Param("policyId") String policyId, @Param("billAdjDueDt") ZonedDateTime billAdjDueDt,
            @Param("billInvoiceCd") char billInvoiceCd);

    @Query(value = "select MIN(b.billInvDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt and b.billInvDt <= :billInvDt and b.billPblItemCd not in :billPblItemCdList "
            + "and b.billInvoiceCd IN :billInvoiceCdList and b.billReferenceDt = :billReferenceDt ")
    public ZonedDateTime getMinInvoidDateRowByInvoiceCd(@Param("billAccountId") String accountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billInvDt") ZonedDateTime billInvDt, @Param("billPblItemCdList") List<String> billPblItemCdList,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billReferenceDt") ZonedDateTime billReferenceDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) AS SUM, "
            + "b.billCommissionPct, b.billAdjDueDt, b.billSysDueDt, b.billInvDt, b.billPblItemCd, b.billReferenceDt, b.bilIstScheduleId.billSeqNbr "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt and b.billPblItemCd not in :billPblItemCdList and b.billInvoiceCd IN :billInvoiceCdList "
            + "and b.billInvDt <= :billInvDt group by b.billCommissionPct, b.billAdjDueDt, b.billSysDueDt, b.billInvDt, b.billPblItemCd, b.billReferenceDt, b.bilIstScheduleId.billSeqNbr "
            + "order by b.billCommissionPct, b.billAdjDueDt, b.billSysDueDt, b.billInvDt, b.billPblItemCd, b.billReferenceDt, b.bilIstScheduleId.billSeqNbr ")
    public List<Object[]> getPcnObjectPresent(@Param("billAccountId") String accountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billPblItemCdList") List<String> billPblItemCdList,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("billInvDt") ZonedDateTime billInvDt);

    @Query(value = "select MAX(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billInvoiceCd in :billInvoiceCdList ")
    public ZonedDateTime getMaxAdjDueDt(@Param("billAccountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select MAX(b.billInvDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and "
            + "c.busCdTranslationId.busType = :busType)")
    public ZonedDateTime getMaxBilInvoiceDateByPayItem(@Param("billAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("busType") String busType);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and b.billInvDt <= :billInvDt and b.billInvoiceCd IN :billInvoiceCdList and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public Double findNsfPcnAmountByInvoiceDateAndCode(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billInvDt") ZonedDateTime billInvDt, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("busType") String busType);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType)")
    public Double getTotalIssueBalance(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("busType") String busType);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId")
    public Double getSumPolicyBalance(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt)"
            + " from BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId "
            + " and b.bilIstScheduleId.billAccountId = c.bilPolicyTermId.bilAccountId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt and c.billPolStatusCd not in :bilpolStatusCodeList")
    public Double getCollectionAccountBalance(@Param("bilAccountId") String bilAccountId,
            @Param("bilpolStatusCodeList") List<Character> bilpolStatusCodeList);

    @Query(value = "select MAX(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.bilIstScheduleId.policyId = :policyId and  b.policyEffectiveDt = :polEffectiveDt "
            + " and b.billInvoiceCd in :billInvoiceCdList ")
    public ZonedDateTime getMaxAdjDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId  and "
            + " b.bilIstScheduleId.policyId in :policyIdList and b.billInvoiceCd in :billInvoiceCdList and "
            + " b.policyEffectiveDt < :polEffectiveDt ")
    public Double getPriorPolicyBalance(@Param("bilAccountId") String bilAccountId,
            @Param("policyIdList") List<String> policyIdList, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select MIN(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billInvoiceCd in :billInvoiceCdList "
            + "and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and b.billInvDt = :billInvDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public ZonedDateTime getMinTermPastInvDate(@Param("billAccountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("billInvDt") ZonedDateTime billInvDt,
            @Param("busType") String busType);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt) AS PremiumSum, "
            + "SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) AS PcnSum "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd "
            + " and c.busCdTranslationId.busType = :busType) ")
    public List<Double[]> findPcnmSumAmounts(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("busType") String busType);

    @Query(value = "select SUM(b.bisPrmPaidAmt + b.bisWroPrmAmt) AS totalPaid, SUM(b.billPrmIstAmt + b.bisCreFutAmt) AS totalDue "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt")
    public List<Object[]> getSumPaidAmountsForAnnual(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);

    @Query(value = "select SUM(b.bisPrmPaidAmt + b.bisWroPrmAmt) AS totalPaid, SUM(b.billPrmIstAmt + b.bisCreFutAmt) AS totalDue "
            + "from BilIstSchedule b where b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt")
    public List<Object[]> getSumPaidAmountsForAnnualForPolicy(@Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);

    @Query(value = "select SUM(b.bisPrmPaidAmt + b.bisCrePaidAmt + b.bisWroPrmAmt) AS totalPaid, "
            + "SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt) AS totalDue "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt")
    public List<Object[]> getSumPaidAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);

    @Query(value = "select SUM(b.bisPrmPaidAmt + b.bisCrePaidAmt + b.bisWroPrmAmt) AS totalPaid, "
            + "SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt) AS totalDue "
            + "from BilIstSchedule b where b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt")
    public List<Object[]> getSumPaidAmountsForPolicy(@Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);

    @Query(value = "select SUM(b.bisPrmPaidAmt + b.bisWroPrmAmt) AS totalPaid, SUM(b.billPrmIstAmt + b.bisCreFutAmt) AS totalDue "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt and b.billPblItemCd not in "
            + "(select d.bilDesReasonId.bilDesReasonCd from BilDesReason d where d.bilDesReasonId.bilDesReasonType in :bilDesReasonTypeList) ")
    public List<Object[]> getSumPaidAmountsForAnnualByPayItem(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bilDesReasonTypeList") List<String> bilDesReasonTypeList);

    @Query(value = "select SUM(b.bisPrmPaidAmt + b.bisWroPrmAmt) AS totalPaid, SUM(b.billPrmIstAmt + b.bisCreFutAmt) AS totalDue "
            + "from BilIstSchedule b where b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt and b.billPblItemCd not in "
            + "(select d.bilDesReasonId.bilDesReasonCd from BilDesReason d where d.bilDesReasonId.bilDesReasonType in :bilDesReasonTypeList) ")
    public List<Object[]> getSumPaidAmountsForAnnualByPayItemForPolicy(@Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bilDesReasonTypeList") List<String> bilDesReasonTypeList);

    @Query(value = "select SUM(b.bisPrmPaidAmt + b.bisCrePaidAmt + b.bisWroPrmAmt) AS totalPaid, "
            + "SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt) AS totalDue "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt and b.billPblItemCd not in "
            + "(select d.bilDesReasonId.bilDesReasonCd from BilDesReason d where d.bilDesReasonId.bilDesReasonType in :bilDesReasonTypeList) ")
    public List<Object[]> getSumPaidAmountsByPayItem(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bilDesReasonTypeList") List<String> bilDesReasonTypeList);

    @Query(value = "select SUM(b.bisPrmPaidAmt + b.bisCrePaidAmt + b.bisWroPrmAmt) AS totalPaid, "
            + "SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt) AS totalDue "
            + "from BilIstSchedule b where b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :polEffectiveDt and b.billPblItemCd not in "
            + "(select d.bilDesReasonId.bilDesReasonCd from BilDesReason d where d.bilDesReasonId.bilDesReasonType in :bilDesReasonTypeList) ")
    public List<Object[]> getSumPaidAmountsByPayItemForPolicy(@Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bilDesReasonTypeList") List<String> bilDesReasonTypeList);

    @Query(value = "select SUM(b.bisCreOpnAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and "
            + " b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and "
            + " b.billPrmIstAmt = :prmIstAmount and b.bisCreOpnAmt > :creOpnAmount and "
            + " not exists (select 1 from BilDesReason d where b.billPblItemCd = d.bilDesReasonId.bilDesReasonCd and "
            + " d.bilDesReasonId.bilDesReasonType in :bilDesReasonTypeList) ")
    public Double getSumCreditOpenAmounts(@Param("billAccountId") String accountId, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("prmIstAmount") double prmIstAmount,
            @Param("creOpnAmount") double creOpnAmount,
            @Param("bilDesReasonTypeList") List<String> bilDesReasonTypeList);

    @Query(value = "select SUM(b.bisCreOpnAmt) from BilIstSchedule b where "
            + " b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and "
            + " b.billPrmIstAmt = :prmIstAmount and b.bisCreOpnAmt > :creOpnAmount and "
            + " not exists (select 1 from BilDesReason d where b.billPblItemCd = d.bilDesReasonId.bilDesReasonCd and "
            + " d.bilDesReasonId.bilDesReasonType in :bilDesReasonTypeList) ")
    public Double getSumCreditOpenAmountsForPolicy(@Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("prmIstAmount") double prmIstAmount,
            @Param("creOpnAmount") double creOpnAmount,
            @Param("bilDesReasonTypeList") List<String> bilDesReasonTypeList);

    @Query(value = "select b.billSysDueDt, SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from "
            + "BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :billAdjDueDt and "
            + "b.billSysDueDt <= :billSysDueDt and b.billInvoiceCd in :billInvoiceCdList group by b.billSysDueDt order by b.billSysDueDt")
    public List<Object[]> getFirstPossibleDueDateSum(@Param("bilAccountId") String bilAccountId,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from "
            + "BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :fpddDate and "
            + "b.billSysDueDt > :fpddDate and b.billSysDueDt < :lookAheadDate")
    public Double getLookAheadDateSum(@Param("bilAccountId") String bilAccountId,
            @Param("fpddDate") ZonedDateTime fpddDate, @Param("lookAheadDate") ZonedDateTime lookAheadDate);

    @Query(value = "select MAX(b.billIstDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + "b.billInvoiceCd in :billInvoiceCdList")
    public ZonedDateTime findMaxInstallmentScheduleDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select distinct(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + "b.billAdjDueDt < b.policyEffectiveDt and b.billAdjDueDt < b.billSysDueDt and b.billInvoiceCd in :billInvoiceCdList")
    public List<ZonedDateTime> findDistinctAdjustDueDates(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + "b.billSysDueDt = :billSysDueDt")
    public List<BilIstSchedule> findBySystemDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Query(value = "select MAX(b.policyEffectiveDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt and "
            + "b.billSysDueDt = :billSysDueDt and b.billInvoiceCd not in :billInvoiceCdList and b.billPblItemCd = :billPblItemCd")
    public ZonedDateTime findMaxPolicyEffectiveDateByAdjustDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billPblItemCd") String billPblItemCd);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt and "
            + "b.billInvDt = :bilInvDt and b.billInvoiceCd in :billInvoiceCdList order by b.bilIstScheduleId.policyId, b.policyEffectiveDt,b.bilIstScheduleId.billSeqNbr")
    public List<BilIstSchedule> findByAdjustDueDateInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select MAX(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId")
    public ZonedDateTime findMaxAdjustDueDateByAccountId(@Param("bilAccountId") String bilAccountId);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and "
            + "b.billAdjDueDt = :billAdjDueDt and b.billInvDt = :billInvDt and b.billPrmIstAmt > :billPrmIstAmt")
    public Double getBalanceAmountForNonZeroPremiumAmount(@Param("billAccountId") String billAccountId,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt, @Param("billInvDt") ZonedDateTime billInvDt,
            @Param("billPrmIstAmt") double billPrmIstAmt);

    @Query(value = "select COUNT(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and b.billInvoiceCd in :billInvoiceCdList and b.billInvDt = :currentDate")
    public Integer getIfInvoicedToday(@Param("accountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("currentDate") ZonedDateTime currentDate);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billInvoiceCd IN :billInvoiceCdList "
            + " order by b.billReferenceDt desc, b.billSysDueDt desc, b.billInvDt desc ")
    public List<BilIstSchedule> getMaxReferenceDateByInvoiceCd(@Param("billAccountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select MAX(b.billSysDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billReferenceDt = :billReferenceDt and "
            + " b.billInvoiceCd in :billInvoiceCdList")
    public ZonedDateTime findMaxSystemDueDateByInvoiceCd(@Param("bilAccountId") String bilAccountId,
            @Param("billReferenceDt") ZonedDateTime billReferenceDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select MAX(b.billInvDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billReferenceDt = :billReferenceDt and "
            + " b.billSysDueDt = :billSysDueDt and b.billInvoiceCd IN :billInvoiceCdList  ")
    public ZonedDateTime findMaxInvoiceDateByInvoiceCd(@Param("billAccountId") String billAccountId,
            @Param("billReferenceDt") ZonedDateTime billReferenceDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billPrmIstAmt = :amount and "
            + "b.bisCreFutAmt < :amount and b.billInvoiceCd in :billInvoiceCdList and b.billIstDueDt < :amount and b.billReferenceDt > :billReferenceDt")
    public List<BilIstSchedule> findExcessCreditData(@Param("bilAccountId") String bilAccountId,
            @Param("billReferenceDt") ZonedDateTime billReferenceDt, @Param("amount") double amount,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select count(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :policyEffectiveDt and b.billAdjDueDt = :bilAdjDueDt and (b.billPrmIstAmt <> :amount or "
            + "(b.billPrmIstAmt = :amount and b.bisCreFutAmt <> :amount))")
    public Integer findNonZeroAmountRowCount(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("amount") double amount);

    @Query(value = "select count(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :policyEffectiveDt and b.billAdjDueDt < :bilAdjDueDt")
    public Integer findEarlierDueDateRowCount(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt);

    @Query(value = "select count(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + "and b.policyEffectiveDt = :policyEffectiveDt and b.billAdjDueDt > :bilAdjDueDt")
    public Integer findFutureDueDateRowCount(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from "
            + "BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt")
    public Double findBalanceByAccountIdDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt);

    @Query(value = "select count(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt and "
            + "b.billInvDt = :bilInvDt and b.billInvoiceCd not in :billInvoiceCdList and b.billPblItemCd = :billPblItemCd")
    public Integer findCountByDatesInvoiceCodeAndPayableItem(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billPblItemCd") String billPblItemCd);

    @Query(value = "select MAX(b.billSysDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt and "
            + "b.billInvDt = :bilInvDt and b.billInvoiceCd not in :billInvoiceCdList and b.billPblItemCd = :billPblItemCd")
    public ZonedDateTime findMaxSystemDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billPblItemCd") String billPblItemCd);

    @Query(value = "select MIN(b.billInvDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.policyEffectiveDt = :policyEffectiveDt")
    public ZonedDateTime findMinInvoiceDateByPolicyEffectiveDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from "
            + "BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt and b.bilIstScheduleId.policyId = :policyId")
    public Double findBalanceByAccountIdDueDatePolicyId(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("policyId") String policyId);

    @Query(value = "select count(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt and "
            + "b.billInvoiceCd in :billInvoiceCdList and b.billPrmIstAmt <> :billPrmIstAmt")
    public Integer findCountByAdjustDateInvoiceCodeAndAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billPrmIstAmt") double billPrmIstAmt);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billReferenceDt = :billReferenceDt")
    public List<BilIstSchedule> findByAccountIdAndReferenceDate(@Param("bilAccountId") String bilAccountId,
            @Param("billReferenceDt") ZonedDateTime billReferenceDt);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billReferenceDt = :billReferenceDt "
            + " and b.billSysDueDt = :billSysDueDt and b.billAdjDueDt = :bilAdjDueDt and b.billInvDt = :bilInvDt ")
    public List<BilIstSchedule> findByAccountIdAndReferenceDate(@Param("bilAccountId") String bilAccountId,
            @Param("billReferenceDt") ZonedDateTime billReferenceDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.billIstDueDt <= :billIstDueDt and b.billInvoiceCd in :billInvoiceCdList ")
    public List<BilIstSchedule> findResumeSchedule(@Param("bilAccountId") String billAccountId,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select COUNT(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billSysDueDt = :billSysDueDt")
    public Integer getUnassignedBisRowCount(@Param("billAccountId") String billAccountId,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Query(value = "select MIN(b.billIstDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billInvoiceCd IN :billInvoiceCdList ")
    public ZonedDateTime getMinIstDueDate(@Param("billAccountId") String billAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilIstSchedule b set b.billSysDueDt = :billSysDueDtUpdate, b.billAdjDueDt = :bilAdjDueDt, "
            + " b.billReferenceDt = :billReferenceDt,  b.billInvDt = :bilInvDt, b.billInvoiceCd = :invoiceCodeUpdate, "
            + " b.bisTypeWhenInv = :bisTypeWhenInv where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList"
            + " and b.billSysDueDt = :billSysDueDt")
    public Integer updateScheduleRowByInvoiceCode(@Param("billSysDueDtUpdate") ZonedDateTime billSysDueDtUpdate,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billReferenceDt") ZonedDateTime billReferenceDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("invoiceCodeUpdate") char invoiceCodeUpdate,
            @Param("bisTypeWhenInv") char bisTypeWhenInv, @Param("bilAccountId") String bilAccountId,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilIstSchedule b set b.billAdjDueDt = :bilAdjDueDtUpdate, b.billInvDt = :bilInvDtUpdate, b.billInvoiceCd = :invoiceCode "
            + " where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt"
            + " and b.billInvDt = :bilInvDt")
    public Integer updateRescindScheduleRowByAdjustDate(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("invoiceCode") char invoiceCode,
            @Param("bilAccountId") String bilAccountId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilIstSchedule b set b.billInvoiceCd = :invoiceCode "
            + " where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt"
            + " and b.billInvDt = :bilInvDt and b.billInvoiceCd in :billInvoiceCdList ")
    public Integer updateInvoiceCodeByAdjustDate(@Param("invoiceCode") char invoiceCode,
            @Param("bilAccountId") String bilAccountId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilIstSchedule b set b.billAdjDueDt = :bilAdjDueDt, b.billInvDt = :bilInvDt "
            + " where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd in :billInvoiceCdList"
            + " and b.billSysDueDt = :billSysDueDt")
    public Integer updateRescindScheduleRowByInvoiceCode(@Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilAccountId") String bilAccountId,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and "
            + "b.billReferenceDt = :billReferenceDt")
    public List<BilIstSchedule> getScheduleByReferenceDate(@Param("billAccountId") String accountId,
            @Param("billReferenceDt") ZonedDateTime billReferenceDt);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId "
            + " and b.policyEffectiveDt = :policyEffectiveDt and b.billInvoiceCd IN :billInvoiceCdList order by b.bilIstScheduleId.billSeqNbr ")
    public List<BilIstSchedule> getRescindRows(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt);

    @Query(value = "select MAX(b.policyEffectiveDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId "
            + "and b.billAdjDueDt = :billAdjDueDt")
    public ZonedDateTime getMaxPolicyEffectiveDate(@Param("billAccountId") String accountId,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt);

    @Query(value = "select b.billInvoiceCd from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.policyEffectiveDt = :policyEffectiveDt and b.billInvoiceCd in :billInvoiceCdList ")
    public List<Character> fetchInvoiceCodeForBis(@Param("bilAccountId") String bilAccountId,
            @Param("policyEffectiveDt") ZonedDateTime policyEffectiveDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select b.billInvoiceCd from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + " b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and "
            + " (b.bisCncOpnAmt < :bisCncOpnAmt or b.bisCncFutAmt < :bisCncFutAmt) ")
    public List<Character> findInvoiceCodeByAmounts(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bisCncOpnAmt") double bisCncOpnAmt, @Param("bisCncFutAmt") double bisCncFutAmt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilIstSchedule b set b.billSysDueDt = :billSysDueDtUpdate, b.billAdjDueDt = :bilAdjDueDt, "
            + " b.billReferenceDt = :billReferenceDt,  b.billInvDt = :bilInvDt where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt "
            + " and b.billSysDueDt = :billSysDueDt")
    public Integer updateScheduleRowsByPolicyId(@Param("billSysDueDtUpdate") ZonedDateTime billSysDueDtUpdate,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billReferenceDt") ZonedDateTime billReferenceDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and  b.billAdjDueDt = :bilAdjDueDt and  b.billInvoiceCd in :billInvoiceCdList")
    public Double getWorkAmtForInvoiceSchedule(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billAdjDueDt = :bilAdjDueDt and b.billInvDt = :billInvDt ")
    public Double getBalanceAmtForInvoiceSchedule(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billInvDt") ZonedDateTime billInvDt);

    @Query(value = "select MAX(b.billSysDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId  "
            + "  and b.billAdjDueDt = :bilAdjDueDt ")
    public ZonedDateTime selectMaxSysDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt);

    @Query(value = "select COUNT(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billSysDueDt <= :billSysDueDt and b.billInvoiceCd = :billInvoiceCd")
    public Integer getBisRowCountBySysDueDt(@Param("billAccountId") String billAccountId,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt, @Param("billInvoiceCd") char billInvoiceCd);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and b.billIstDueDt <= :bilIstDueDt and b.billAdjDueDt = :bilAdjDueDt and "
            + " b.billSysDueDt > :billSysDueDt and b.billSysDueDt < :billSysDueDt1")
    public Double getAccountBalancebyDueDates(@Param("accountId") String accountId,
            @Param("bilIstDueDt") ZonedDateTime bilIstDueDt, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt, @Param("billSysDueDt1") ZonedDateTime billSysDueDt1);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilIstSchedule b set b.billAdjDueDt = :billAdjDueDt ,b.billInvDt = :billInvDt, b.billSysDueDt = :billSysDueDt"
            + " where b.bilIstScheduleId.billAccountId = :billAccountId "
            + "and b.billSysDueDt = :billSysDueDt1 and b.billInvoiceCd IN :billInvoiceCdList")
    public void updatAdjInvSysDates(@Param("billAdjDueDt") ZonedDateTime billAdjDueDt,
            @Param("billInvDt") ZonedDateTime billInvDt, @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("billAccountId") String billAccountId, @Param("billSysDueDt1") ZonedDateTime billSysDueDt1,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilIstSchedule b SET b.billInvDt = :billInvDt where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billSysDueDt = :billSysDueDt")
    public void updateInvoiceDateRowBySystemDueDate(@Param("billInvDt") ZonedDateTime billInvDt,
            @Param("bilAccountId") String bilAccountId, @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE BilIstSchedule b SET b.billInvDt = :billInvDt, b.billAdjDueDt = :billAdjDueDt where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billSysDueDt = :billSysDueDt")
    public void updateInvoiceDateAndAdjustDateBySystemDueDate(@Param("billInvDt") ZonedDateTime billInvDt,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt, @Param("bilAccountId") String bilAccountId,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Query(value = "select COUNT(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and b.billSysDueDt <> :billSysDueDt")
    public Integer getRowCountNumber(@Param("accountId") String accountId,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Query(value = "select MAX(b.billInvDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billSysDueDt <> :billSysDueDt and "
            + " b.billAdjDueDt < :billAdjDueDt and b.billInvDt < :billInvDt and b.billInvoiceCd IN :billInvoiceCdList  ")
    public ZonedDateTime findMaxInvoiceDateByInvoiceCd(@Param("billAccountId") String billAccountId,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt, @Param("billAdjDueDt") ZonedDateTime billAdjDueDt,
            @Param("billInvDt") ZonedDateTime billInvDt, @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and  "
            + "b.billInvDt = :billInvDt ")
    public Double getDueAmountByInvoiceDate(@Param("accountId") String accountId,
            @Param("billInvDt") ZonedDateTime billInvDt);

    @Query(value = "select max(b.bilIstScheduleId.billSeqNbr) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd not IN :billInvoiceCdList")
    public Short getMaxBilSequenceNumber(@Param("bilAccountId") String billAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select count(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and"
            + " b.billInvoiceCd = :billInvoiceCd and b.billSysDueDt <> :billSysDueDt")
    public Integer countBilIstScheduleByBillInvoiceCd(@Param("bilAccountId") String billAccountId,
            @Param("billInvoiceCd") char billInvoiceCd, @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Query(value = "select count(b) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and "
            + "b.billAdjDueDt = :billAdjDueDt and b.billInvoiceCd in :billInvoiceCdList and "
            + "(b.billIstDueDt <= :dateMinus1Freq or (b.billIstDueDt <= :minUninvBdtRfrDate and b.bisNonTrmDtInd <> :bisNonTrmDtInd and "
            + " not exists(select 1 from BilIstSchedule c "
            + "where c.bilIstScheduleId.billAccountId = b.bilIstScheduleId.billAccountId and "
            + "c.bilIstScheduleId.policyId = b.bilIstScheduleId.policyId and "
            + "c.policyEffectiveDt = b.policyEffectiveDt and c.billIstDueDt > b.billIstDueDt and "
            + "b.bisNonTrmDtInd = :bisNonTrmDtInd)))")
    public Integer countBilIstScheduleforCheckFpdd(@Param("billAccountId") String billAccountId,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("dateMinus1Freq") ZonedDateTime dateMinus1Freq,
            @Param("minUninvBdtRfrDate") ZonedDateTime minUninvBdtRfrDate,
            @Param("bisNonTrmDtInd") char bisNonTrmDtInd);

    @Query(value = "select MIN(b.billIstDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId "
            + "and b.billSysDueDt = :billSysDueDt and b.billInvoiceCd IN :billInvoiceCdList ")
    public ZonedDateTime getMinIstDueDateBySysDueDate(@Param("billAccountId") String billAccountId,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select b from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and"
            + "  b.billSysDueDt = :billSysDueDt")
    public List<BilIstSchedule> getBilIstScheduleByBilSysDueDate(@Param("bilAccountId") String billAccountId,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Query(value = "select DISTINCT b.bilIstScheduleId.policyId from BilIstSchedule b where "
            + "b.bilIstScheduleId.billAccountId = :billAccountId and b.billAdjDueDt = :billAdjDueDt")
    public List<String> getPolicyIdByBilAdjDueDt(@Param("billAccountId") String billAccountId,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt);

    @Query(value = "select MAX(b.policyEffectiveDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId"
            + " and b.bilIstScheduleId.policyId = :policyId and b.billAdjDueDt = :billAdjDueDt")
    public ZonedDateTime getMaxPolicyEffectiveDateByPolicyId(@Param("billAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("billAdjDueDt") ZonedDateTime billAdjDueDt);

    @Query(value = "select MIN(b.billSysDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd IN :billInvoiceCdList"
            + " and b.billSysDueDt <= :billSysDueDt")
    public ZonedDateTime getMinSysDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and "
            + "b.billAdjDueDt <= :billAdjDueDt and b.billInvDt <= :billInvDt and b.billInvoiceCd IN :billInvoiceCdList ")
    public Double getBalanceAmountForNonZeroPremiumAmount(@Param("billAccountId") String billAccountId,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt, @Param("billInvDt") ZonedDateTime billInvDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select MIN(b.billSysDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billInvoiceCd IN :billInvoiceCdList"
            + " and b.billSysDueDt < :billSysDueDt")
    public ZonedDateTime getMinSystemDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilIstSchedule b set b.billSysDueDt = :billSysDueDt, b.billAdjDueDt = :bilAdjDueDt, "
            + " b.billInvoiceCd = :billInvoiceCd,  b.billInvDt = :bilInvDt where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billReferenceDt = :billReferenceDt and b.billInvoiceCd IN :billInvoiceCdList")
    public Integer updateScheduleRowsByFpdd(@Param("billSysDueDt") ZonedDateTime billSysDueDt,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("billInvoiceCd") char billInvoiceCd,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilAccountId") String bilAccountId,
            @Param("billReferenceDt") ZonedDateTime billReferenceDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billAdjDueDt = :billAdjDueDt and b.billInvDt = :billInvDt")
    public Double getBalanceAmountByAdjustDueDateInvoiceDate(@Param("billAccountId") String billAccountId,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt, @Param("billInvDt") ZonedDateTime billInvDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b "
            + " where b.bilIstScheduleId.billAccountId = :accountId and  "
            + " b.billInvoiceCd in :billInvoiceCdList and b.billSysDueDt <= :billSysDueDt")
    public Double getSystemDueAmount(@Param("accountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from "
            + "BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billSysDueDt = :billSysDueDt")
    public Double findBalanceByAccountIdSysDueDt(@Param("bilAccountId") String bilAccountId,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilIstSchedule b set b.billAdjDueDt = :bilAdjDueDt, b.billInvDt = :bilInvDt "
            + " where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billReferenceDt = :billReferenceDt")
    public Integer updateEftPastReferenceData(@Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilAccountId") String bilAccountId,
            @Param("billReferenceDt") ZonedDateTime billReferenceDt);

    @Query(value = "select distinct b.billIstDueDt, c.billPlanCd,"
            + " c.bilPolicyTermId.policyId,c.bilPolicyTermId.polEffectiveDt from "
            + "BilIstSchedule b, BilPolicyTerm c where b.bilIstScheduleId.billAccountId = :bilAccountId and "
            + "c.bilPolicyTermId.bilAccountId = b.bilIstScheduleId.billAccountId and "
            + "b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId and "
            + "c.bilPolicyTermId.polEffectiveDt = b.policyEffectiveDt and b.billSysDueDt = :billSysDueDt and "
            + "b.billAdjDueDt = :bilAdjDueDt and b.billInvDt = :bilInvDt")
    public List<Object[]> getBilIstScheduleAndPolicyTerm(@Param("bilAccountId") String bilAccountId,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilIstSchedule b set b.billAdjDueDt = :bilAdjDueDtUpdate, b.billInvDt = :bilInvDtUpdate,"
            + " b.billReferenceDt = :billReferenceDtUpdate, b.billSysDueDt = :billSysDueDtUpdate "
            + " where b.bilIstScheduleId.billAccountId = :bilAccountId and b.billIstDueDt = :billIstDueDt and "
            + " b.billAdjDueDt = :bilAdjDueDt and b.billInvDt = :bilInvDt and b.billReferenceDt = :billReferenceDt and "
            + " b.billSysDueDt = :billSysDueDt ")
    public Integer updateDefaultDataByBillIstDueDt(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate,
            @Param("billReferenceDtUpdate") ZonedDateTime billReferenceDtUpdate,
            @Param("billSysDueDtUpdate") ZonedDateTime billSysDueDtUpdate, @Param("bilAccountId") String bilAccountId,
            @Param("billIstDueDt") ZonedDateTime billIstDueDt, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billReferenceDt") ZonedDateTime billReferenceDt,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt);

    @Query(value = "select b.bilIstScheduleId.policyId, b.policyEffectiveDt from BilIstSchedule b "
            + "where b.bilIstScheduleId.billAccountId = :billAccountId and b.billAdjDueDt = :billAdjDueDt "
            + "and exists(select 1 from BilPolicyTerm c where c.bilPolicyTermId.bilAccountId = b.bilIstScheduleId.billAccountId "
            + "and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt "
            + "and c.billPolStatusCd not in :billPolStatusCd) order by b.policyEffectiveDt desc, b.bilIstScheduleId.policyId")
    public List<Object[]> getPolicyIdAndPolicyEffectiveDate(@Param("billAccountId") String billAccountId,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt, @Param("billPolStatusCd") List<Character> bilPolStatuCd);

    @Query(value = "select b.bilIstScheduleId.policyId, b.policyEffectiveDt from BilIstSchedule b "
            + "where b.bilIstScheduleId.billAccountId = :billAccountId and b.billInvoiceCd IN :billInvoiceCdList "
            + "and exists(select 1 from BilPolicyTerm c where c.bilPolicyTermId.bilAccountId = b.bilIstScheduleId.billAccountId "
            + "and b.bilIstScheduleId.policyId = c.bilPolicyTermId.policyId and b.policyEffectiveDt = c.bilPolicyTermId.polEffectiveDt "
            + "and c.billPolStatusCd not in :billPolStatusCd) order by b.policyEffectiveDt desc, b.bilIstScheduleId.policyId")
    public List<Object[]> getPolicyIdAndPolicyEffectiveDate(@Param("billAccountId") String billAccountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("billPolStatusCd") List<Character> bilPolStatuCd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilIstSchedule b set b.billSysDueDt = :billSysDueDtUpdate, b.billReferenceDt = :billReferenceDtUpdate  "
            + " where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billReferenceDt = :billReferenceDt and b.billSysDueDt = :billSysDueDt "
            + " and b.billAdjDueDt = :bilAdjDueDt and b.billInvDt = :bilInvDt and b.billInvoiceCd = :billInvoiceCd")
    public Integer updateScheduleRowsByOldScheduleDate(@Param("billSysDueDtUpdate") ZonedDateTime billSysDueDtUpdate,
            @Param("billReferenceDtUpdate") ZonedDateTime billReferenceDtUpdate,
            @Param("bilAccountId") String bilAccountId, @Param("billReferenceDt") ZonedDateTime billReferenceDt,
            @Param("billSysDueDt") ZonedDateTime billSysDueDt, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("billInvoiceCd") char billInvoiceCd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilIstSchedule b set b.billSysDueDt = :billSysDueDtUpdate, b.billInvDt = :bilInvDtUpdate, b.billAdjDueDt = :bilAdjDueDtUpdate  "
            + " where b.bilIstScheduleId.billAccountId = :bilAccountId "
            + " and b.billAdjDueDt = :billAdjDueDt and b.billInvDt = :bilInvDt ")
    public Integer updateScheduleRowsByOldScheduleDate(@Param("billSysDueDtUpdate") ZonedDateTime billSysDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate,
            @Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate, @Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt);
    
    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = "
            + ":polEffectiveDt")
    public Double findPolicyTermBalance(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt);
    
    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) as SUM1 "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and b.billAdjDueDt <= :billAdjDueDt and b.billInvoiceCd in :billInvoiceCdList and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public Double findTermLastAmtByAdjDateWithBillInvoiceCd(@Param("bilAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("billAdjDueDt") ZonedDateTime billAdjDueDt, @Param("busType") String busType,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);
    
    @Query(value = "select MAX(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billInvoiceCd in :billInvoiceCdList "
            + "and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and "
            + "(b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) > :pastAmount "
            + " and not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public ZonedDateTime getMaxTermPastAdjDateWithouBillAdjDueDt(@Param("billAccountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("pastAmount") double pastAmount,
            @Param("busType") String busType);
    
    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCreOpnAmt + b.bisCncFutAmt + b.bisCncOpnAmt) AS PremiumSum "
            + "from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :bilAccountId and b.bilIstScheduleId.policyId = :policyId and "
            + "b.policyEffectiveDt = :polEffectiveDt and b.billAdjDueDt = :billAdjDueDt and b.billInvoiceCd in :billInvoiceCdList and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType)")
    public Double getFirstInstDueAmount(@Param("bilAccountId") String billAccountId, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("billAdjDueDt") ZonedDateTime billAdjDueDt,
            @Param("busType") String busType);
    
    @Query(value = "select SUM(b.bisPrmPaidAmt +  b.bisCrePaidAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billInvoiceCd in :billInvoiceCdList "
            + "and b.bilIstScheduleId.policyId = :policyId and b.policyEffectiveDt = :polEffectiveDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public Double getFirstInstPaidAmountWithoutBillAdjDueDate(@Param("billAccountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("polEffectiveDt") ZonedDateTime polEffectiveDt, @Param("busType") String busType);
    
    @Query(value = "select SUM(b.bisPrmPaidAmt +  b.bisCrePaidAmt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and b.billInvoiceCd in :billInvoiceCdList "
            + "and b.bilIstScheduleId.policyId = :policyId and b.billAdjDueDt = :billAdjDueDt and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public Double getFirstInstPaidAmount(@Param("billAccountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("busType") String busType, @Param("billAdjDueDt") ZonedDateTime billAdjDueDt);
    
    @Query(value = "select MIN(b.billAdjDueDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId "
            + " and b.billInvoiceCd in :billInvoiceCdList and b.bilIstScheduleId.policyId = :policyId and "
            + "not exists (select 1 from BusCdTranslation c where b.billPblItemCd = c.busCdTranslationId.busCd and c.busCdTranslationId.busType = :busType) ")
    public ZonedDateTime getMinBilAdjDueDate(@Param("billAccountId") String accountId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList, @Param("policyId") String policyId,
            @Param("busType") String busType);
    
    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b, BilPolicy c where "
            + "c.polNbr = :polNbr and c.polSymbolCd = :polSymbolCd and c.bilPolicyId.policyId = b.bilIstScheduleId.policyId and b.bilIstScheduleId.policyId <> :policyId and c.bilPolicyId.bilAccountId = b.bilIstScheduleId.billAccountId "
            + "and b.bilIstScheduleId.billAccountId = :bilAccountId and c.bilIssueInd = 'W' ")
    public Double findSumByPolNbrAndPolSymbol(@Param("polNbr") String polNbr, @Param("policyId") String policyId,
            @Param("bilAccountId") String bilAccountId, @Param("polSymbolCd") String polSymbolCd);
    
    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) from BilIstSchedule b, BilPolicy c where "
            + "c.polNbr = :polNbr and c.bilPolicyId.policyId = b.bilIstScheduleId.policyId and b.bilIstScheduleId.policyId <> :policyId and c.bilPolicyId.bilAccountId = b.bilIstScheduleId.billAccountId "
            + "and b.bilIstScheduleId.billAccountId = :bilAccountId and c.bilIssueInd = 'W' ")
    public Double findSumByPolNbr(@Param("polNbr") String polNbr, @Param("policyId") String policyId,
            @Param("bilAccountId") String bilAccountId);
    
    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and b.bilIstScheduleId.policyId = :policyId "
            + " and b.billInvoiceCd in :billInvoiceCdList")
    public Double getCivAmount(@Param("accountId") String accountId, @Param("policyId") String policyId,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);
    
    @Query(value = "select SUM(b.billPrmIstAmt + b.bisCreFutAmt + b.bisCncFutAmt + b.bisCreOpnAmt + b.bisCncOpnAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt) "
            + " from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :accountId and b.bilIstScheduleId.policyId = :policyId")
    public Double getCivAmountByAccountIdAndPolicyId(@Param("accountId") String accountId, @Param("policyId") String policyId);
    
    @Query(value = "select MIN(b.policyEffectiveDt) from BilIstSchedule b where b.bilIstScheduleId.billAccountId = :billAccountId and "
            + " b.bilIstScheduleId.policyId = :policyId and (b.billAdjDueDt = :bilAdjDueDt or :bilAdjDueDt = :arg) and b.billInvoiceCd in :billInvoiceCdList and"
            + " b.billPrmIstAmt + b.bisCreOpnAmt + b.bisCncOpnAmt + b.bisCreFutAmt + b.bisCncFutAmt - b.bisPrmPaidAmt - b.bisCrePaidAmt - b.bisWroPrmAmt > 0 ")
    public ZonedDateTime getMinPolicyEffectiveDt(@Param("billAccountId") String billAccountId,
            @Param("policyId") String policyId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList,
            @Param("arg") ZonedDateTime arg);
}
