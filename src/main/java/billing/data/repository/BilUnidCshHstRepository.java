package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.BilUnidCshHst;
import billing.data.entity.id.BilUnIdCshHstId;

public interface BilUnidCshHstRepository extends JpaRepository<BilUnidCshHst, BilUnIdCshHstId> {

}
