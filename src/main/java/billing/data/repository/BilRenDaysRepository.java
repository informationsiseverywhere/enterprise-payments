package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.BilRenDays;
import billing.data.entity.id.BilRenDaysId;

public interface BilRenDaysRepository extends JpaRepository<BilRenDays, BilRenDaysId> {

}
