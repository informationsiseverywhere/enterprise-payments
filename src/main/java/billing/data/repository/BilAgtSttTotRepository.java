package billing.data.repository;

import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilAgtSttTot;
import billing.data.entity.id.BilAgtSttTotId;

public interface BilAgtSttTotRepository extends JpaRepository<BilAgtSttTot, BilAgtSttTotId> {

    @Query(value = "select SUM(b.bilSttNetAmt - b.bilSttPaidAmt) from BilAgtSttTot b where b.bilAgtSttTotId.bilAgtActId = :bilAgtActId and "
            + "b.bilAgtSttTotId.bilAccountDt <= :bilAccountDt and b.bstAcceptInd = :bstAcceptInd")
    public Double getTotalDueAmount(@Param("bilAgtActId") String bilAgtActId,
            @Param("bilAccountDt") ZonedDateTime bilAccountDt, @Param("bstAcceptInd") char bstAcceptInd);

    @Query(value = "select SUM(b.bilSttNetAmt - b.bilSttPaidAmt) from BilAgtSttTot b where b.bilAgtSttTotId.bilAgtActId = :bilAgtActId and "
            + "b.bilAgtSttTotId.bilAccountDt = :bilAccountDt and b.bstAcceptInd = :bstAcceptInd")
    public Double getTotalDueAmount1(@Param("bilAgtActId") String bilAgtActId,
            @Param("bilAccountDt") ZonedDateTime bilAccountDt, @Param("bstAcceptInd") char bstAcceptInd);

}