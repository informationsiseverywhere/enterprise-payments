package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilInvDtl;
import billing.data.entity.id.BilInvDtlId;

public interface BilInvDtlRepository extends JpaRepository<BilInvDtl, BilInvDtlId> {

    @Query(value = "select b from BilInvDtl b where b.bilInvDtlId.bilTchKeyId = :bilTchKeyId and b.bilInvDtlId.bilAccountId = :bilAccountId and "
            + "b.bilInvDtlId.bilInvSeqNbr = :bilInvSeqNbr")
    public List<BilInvDtl> findCommissionPersent(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilAccountId") String bilAccountId, @Param("bilInvSeqNbr") short bilInvSeqNbr);

}
