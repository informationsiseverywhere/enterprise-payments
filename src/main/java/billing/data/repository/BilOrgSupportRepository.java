package billing.data.repository;



import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


import billing.data.entity.BilOrgSupport;
import billing.data.entity.id.BilOrgSupportId;

public interface BilOrgSupportRepository extends CrudRepository<BilOrgSupport, BilOrgSupportId> {

    @Query(value = "select b from BilOrgSupport b where b.bilOrgSupportId.bilFileTypeCd = :bilFileTypeCd and b.bosTapeFrmCd = :bosTapeFrmCd")
    public BilOrgSupport getByBilFileTypeCd(@Param("bilFileTypeCd") short bilFileTypeCd,
            @Param("bosTapeFrmCd") String bosTapeFrmCd);

    @Query(value = "select count(b) from BilOrgSupport b where b.bilOrgSupportId.bilFileTypeCd = :bilFileTypeCd and b.bosTapeFrmCd = :bosTapeFrmCd")
    public Integer getCountByFileTypeCdAndFormCd(@Param("bilFileTypeCd") short bilFileTypeCd,
            @Param("bosTapeFrmCd") String bosTapeFrmCd);

}
