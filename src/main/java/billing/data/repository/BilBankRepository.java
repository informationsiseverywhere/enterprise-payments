package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.BilBank;

public interface BilBankRepository extends JpaRepository<BilBank, String> {

    public BilBank findByBbkAccountNm(String depositBank);

}
