package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilActSummary;
import billing.data.entity.id.BilActSummaryId;

public interface BilActSummaryRepository extends JpaRepository<BilActSummary, BilActSummaryId> {

    @Query(value = "select MAX( b.bilActSummaryId.bilAcySeq) from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId and "
            + "b.bilActSummaryId.bilAcyDt = :bilAcyDt")
    public Short findByMaxSeqNumberBilAccountId(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt);

    @Query(value = "select COUNT(b) from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId and b.bilDesReaTyp = :bilDesReaTyp")
    public Integer getCashSuspendActivity(@Param("bilAccountId") String bilAccountId,
            @Param("bilDesReaTyp") String bilDesReaTyp);

    @Query(value = "select SUM(b.bilAcyAmt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId and b.bilAcyDesCd in :bilAcyDesCdList "
            + "and b.bilDesReaTyp = :bilDesReaTyp and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd")
    public Double getPremiumActivityAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList, @Param("bilDesReaTyp") String bilDesReaTyp,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd);

    @Query(value = "select SUM(b.bilAcyAmt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId and b.bilAcyDesCd = :bilAcyDesCd "
            + "and b.bilDesReaTyp = :bilDesReaTyp")
    public Double getActivityAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyDesCd") String bilAcyDesCd, @Param("bilDesReaTyp") String bilDesReaTyp);

    @Query(value = "select b from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId and b.polNbr = :polNbr "
            + " and b.polSymbolCd = :polSymbolCd and b.bilAcyDesCd in :bilAcyDesCdList "
            + " order by b.bilActSummaryId.bilAcyDt desc, b.bilActSummaryId.bilAcySeq desc ")
    public List<BilActSummary> getMaxBilAcyDate(@Param("bilAccountId") String bilAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + " and b.polSymbolCd = :polSymbolCd and b.bilAcyDesCd = :bilAcyDesCd and b.bilAcyDes1Dt = :bilAcyDes1Dt and ((b.bilActSummaryId.bilAcyDt = :bilAcyDate and "
            + " b.bilActSummaryId.bilAcySeq > :bilSequence) or b.bilActSummaryId.bilAcyDt > :bilAcyDate)")
    public ZonedDateTime findMaxIniBilActivityDate(@Param("billAccountId") String billAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDesCd") String bilAcyDesCd, @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt,
            @Param("bilSequence") short bilSequence, @Param("bilAcyDate") ZonedDateTime bilAcyDate);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcySeq) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + " and b.polSymbolCd = :polSymbolCd and b.bilAcyDesCd = :bilAcyDesCd and b.bilActSummaryId.bilAcyDt = :bilAcyDate")
    public Short findMaxIniBilSequence(@Param("billAccountId") String billAccountId, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilAcyDesCd") String bilAcyDesCd,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate);

    @Query(value = "select SUM(b.bilAcyAmt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "((b.bilActSummaryId.bilAcyDt = :bilAcyDate and b.bilActSummaryId.bilAcySeq > :bilSequence) or b.bilActSummaryId.bilAcyDt > :bilAcyDateSecond)"
            + " and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilAcyDesCd in :bilAcyDesCdList and b.bilDesReaTyp = :bilDesReaTyp")
    public Double getPrimeumWriteOffAmounts(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilSequence") short bilSequence,
            @Param("bilAcyDateSecond") ZonedDateTime bilAcyDateSecond, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilAcyDesCdList") List<String> bilAcyDesCdList,
            @Param("bilDesReaTyp") String bilDesReaTyp);

    @Query(value = "select SUM(b.bilAcyAmt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId and ((b.bilActSummaryId.bilAcyDt = :bilAcyDt"
            + " and b.bilActSummaryId.bilAcySeq > :bilAcySeq) or b.bilActSummaryId.bilAcyDt = :bilAcyDtSecond) and b.polNbr = :polNbr and "
            + "b.polSymbolCd = :polSymbolCd and b.bilDesReaTyp in :bilDesReaTypList")
    public Double getRevManWriteOffPremiumAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt, @Param("bilAcySeq") short bilAcySeq,
            @Param("bilAcyDtSecond") ZonedDateTime bilAcyDtSecond, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select SUM(b.bilAcyAmt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId "
            + " and b.bilDesReaTyp = :bilDesReaTyp and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd")
    public Double getPremiumActivityAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilDesReaTyp") String bilDesReaTyp, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd);

    @Query(value = "select MAX( b.bilActSummaryId.bilAcySeq) from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId and "
            + "b.bilActSummaryId.bilAcyDt = :bilAcyDt and b.polNbr = :polNbr and b.bilAcyAmt < :bilAcyAmt ")
    public Short findByMaxSeqNumberForCancelRow(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt, @Param("polNbr") String polNbr,
            @Param("bilAcyAmt") Double bilAcyAmt);

    @Query(value = "select MAX( b.bilActSummaryId.bilAcySeq) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.bilActSummaryId.bilAcyDt = :bilAcyDt"
            + " and b.bilAcyDesCd = :bilAcyDesCd and b.bilDesReaTyp in :bilDesReaTypList ")
    public Short findMaxSeqNumberByPaymentType(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt, @Param("bilAcyDesCd") String bilAcyDesCd,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId and b.bilAcyDesCd = :bilAcyDesCd and "
            + " b.bilAcyDes1Dt = :bilAcyDes1Dt")
    public ZonedDateTime getMaxNoticeBilAcyDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyDesCd") String bilAcyDesCd, @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcySeq) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "b.bilActSummaryId.bilAcyDt = :bilAcyDate and b.bilDesReaTyp in :bilDesReaTypList and b.bilAcyAmt < :bilAcyAmt")
    public Short getMaxCreditSequence(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAcyAmt") double bilAcyAmt);

    @Query(value = "Select b from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + "and b.polSymbolCd = :polSymbolCd and b.bilAcyDesCd in :bilAcyDesCdList and b.bilDesReaTyp in :bilDesReaTypList "
            + "and b.userId <> :userId")
    public List<BilActSummary> findActSummaryByUser(@Param("billAccountId") String billAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList, @Param("userId") String userId);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "b.bilAcyDesCd in :bilAcyDesCdList and b.bilActSummaryId.bilAcyDt >= :bilAcyDt and b.bilAcyAmt <> :bilAcyAmt and "
            + "b.bilDesReaTyp = :bilDesReaTyp")
    public ZonedDateTime findMaxAutoColAcyDate(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList, @Param("bilAcyDt") ZonedDateTime bilAcyDt,
            @Param("bilAcyAmt") double bilAcyAmt, @Param("bilDesReaTyp") String bilDesReaTyp);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "b.bilAcyDesCd = :bilAcyDesCd and b.bilActSummaryId.bilAcyDt = :bilAcyDt and b.bilDesReaTyp = :bilDesReaTyp")
    public ZonedDateTime findMaxAutoColAcyDateByDesCode(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDesCd") String bilAcyDesCd, @Param("bilAcyDt") ZonedDateTime bilAcyDt,
            @Param("bilDesReaTyp") String bilDesReaTyp);

    @Query(value = "select b from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId and b.bilActSummaryId.bilAcyDt >= :bilAcyDt"
            + " and b.bilActSummaryId.bilAcyDt <= :bilAcyDtSecond and ((b.bilAcyDesCd in :bilAcyDesCdList and b.bilDesReaTyp = :bilDesReaTyp) or "
            + " (b.bilAcyDesCd = :bilAcyDesCdSecond and b.bilDesReaTyp = :bilDesReaTypSecond)) ")
    public BilActSummary getBilAcyDateByDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt, @Param("bilAcyDtSecond") ZonedDateTime bilAcyDtSecond,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList, @Param("bilDesReaTyp") String bilDesReaTyp,
            @Param("bilAcyDesCdSecond") String bilAcyDesCdSecond,
            @Param("bilDesReaTypSecond") String bilDesReaTypSecond);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "b.bilActSummaryId.bilAcyDt >= :bilAcyDt and b.bilActSummaryId.bilAcyDt <= :bilAcyDtSecond and "
            + "((b.bilAcyDesCd = :bilAcyDesCd and b.bilDesReaTyp = :bilDesReaTyp) or (b.bilAcyDesCd = :bilAcyDesCdSecond and b.bilDesReaTyp = :bilDesReaTypSecond)"
            + " or (b.bilAcyDesCd = :bilAcyDesCdThird and b.bilDesReaTyp = :bilDesReaTypThird) or b.bilDesReaTyp = :bilDesReaTypLast) ")
    public ZonedDateTime findMaxNsfAcyDate(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt, @Param("bilAcyDtSecond") ZonedDateTime bilAcyDtSecond,
            @Param("bilAcyDesCd") String bilAcyDesCd, @Param("bilDesReaTyp") String bilDesReaTyp,
            @Param("bilAcyDesCdSecond") String bilAcyDesCdSecond,
            @Param("bilDesReaTypSecond") String bilDesReaTypSecond, @Param("bilAcyDesCdThird") String bilAcyDesCdThird,
            @Param("bilDesReaTypThird") String bilDesReaTypThird, @Param("bilDesReaTypLast") String bilDesReaTypLast);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + " and b.polSymbolCd = :polSymbolCd and b.bilAcyDesCd = :bilAcyDesCd and b.bilDesReaTyp = :bilDesReaTyp and b.bilAcyDes2Dt = :bilAcyDes2Dt ")
    public ZonedDateTime findMaxBilActivityByAcy2Date(@Param("billAccountId") String billAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDesCd") String bilAcyDesCd, @Param("bilDesReaTyp") String bilDesReaTyp,
            @Param("bilAcyDes2Dt") ZonedDateTime bilAcyDes2Dt);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + " and b.polSymbolCd = :polSymbolCd and b.bilAcyDes1Dt = :bilAcyDes1Dt and b.bilAcyDesCd in :bilAcyDesCdList "
            + " and b.bilDesReaTyp in :bilDesReaTypList ")
    public ZonedDateTime findOriginPcnDate(@Param("billAccountId") String billAccountId, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcyDt), MAX(b.bilActSummaryId.bilAcySeq) from BilActSummary b "
            + " where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd "
            + " and b.bilAcyDesCd in :bilAcyDesCdList and b.bilAcyDes1Dt = :bilAcyDes1Dt and b.bilActSummaryId.bilAcyDt in :bilAcyDate and "
            + " b.bilActSummaryId.bilAcySeq in :bilAcySeq")
    public List<Object[]> getMaxAcyDateAndSequenceWithAcyCode(@Param("billAccountId") String billAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList, @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilAcySeq") short bilAcySeq);

    @Query(value = "Select b from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.bilActSummaryId.bilAcyDt = :bilAcyDate "
            + "and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilAcyDes1Dt = :bilAcyDes1Dt and b.bilAcyDesCd = :bilAcyDesCd "
            + "and b.bilDesReaTyp = :bilDesReaTyp")
    public List<BilActSummary> findRescindTrigger(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt,
            @Param("bilAcyDesCd") String bilAcyDesCd, @Param("bilDesReaTyp") String bilDesReaTyp);

    @Query(value = "Select b from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + "and b.polSymbolCd = :polSymbolCd and b.bilAcyDes1Dt = :bilAcyDes1Dt and b.bilAcyDesCd in :bilAcyDesCdList "
            + "and b.bilDesReaTyp in :bilDesReaTypList order by b.bilActSummaryId.bilAcyDt desc, b.bilActSummaryId.bilAcySeq desc")
    public List<BilActSummary> findUserIdRescinded(@Param("billAccountId") String billAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt, @Param("bilAcyDesCdList") List<String> bilAcyDesCdList,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "Select MAX(b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + "and b.polSymbolCd = :polSymbolCd and b.bilAcyDes1Dt = :bilAcyDes1Dt and b.bilAcyDesCd in :bilAcyDesCdList")
    public ZonedDateTime getMaxPriorCnrAcyDate(@Param("billAccountId") String billAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt, @Param("bilAcyDesCdList") List<String> bilAcyDesCdList);

    @Query(value = "Select MAX(b.bilActSummaryId.bilAcySeq) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + "and b.polSymbolCd = :polSymbolCd and b.bilAcyDes1Dt = :bilAcyDes1Dt and b.bilAcyDesCd in :bilAcyDesCdList and b.bilActSummaryId.bilAcyDt = :bilAcyDt")
    public Short getMaxPriorCnrAcySequence(@Param("billAccountId") String billAccountId, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList, @Param("bilAcyDt") ZonedDateTime bilAcyDt);

    @Query(value = "Select MAX(b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "b.bilActSummaryId.bilAcyDt <= :bilAcyDt and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilAcyDes1Dt = :bilAcyDes1Dt and "
            + "b.bilAcyDesCd = :bilAcyDesCd and b.bilDesReaTyp in :bilDesReaTypList")
    public ZonedDateTime getLastAcyDate(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt,
            @Param("bilAcyDesCd") String bilAcyDesCd, @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "Select MAX(b.bilActSummaryId.bilAcySeq) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "b.bilActSummaryId.bilAcyDt = :bilAcyDt and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilAcyDes1Dt = :bilAcyDes1Dt and "
            + "b.bilAcyDesCd = :bilAcyDesCd and b.bilDesReaTyp in :bilDesReaTypList")
    public Short getLastAcySequenceNumber(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt,
            @Param("bilAcyDesCd") String bilAcyDesCd, @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select b from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "((b.bilActSummaryId.bilAcyDt = :bilAcyDate and b.bilActSummaryId.bilAcySeq > :bilSequence) or b.bilActSummaryId.bilAcyDt > :bilAcyDate) and "
            + "b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilAcyDes1Dt = :bilAcyDes1Dt and b.bilAcyDesCd in :bilAcyDesCdList and b.bilDesReaTyp = :bilDesReaTyp")
    public List<BilActSummary> getLastBilActivity(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilSequence") short bilSequence,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt, @Param("bilAcyDesCdList") List<String> bilAcyDesCdList,
            @Param("bilDesReaTyp") String bilDesReaTyp);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcySeq) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "b.bilActSummaryId.bilAcyDt = :bilAcyDate and (b.bilDesReaTyp in :bilDesReaTypList or "
            + "(b.bilAcyDesCd in :bilAcyDesCdList and b.bilDesReaTyp = :bilDesReaTyp) or (b.bilAcyDesCd like :secondBilAcyDesCd and b.bilDesReaTyp = :secondBilDesReaTyp))")
    public Short getMaxPaymentSequence(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList, @Param("bilDesReaTyp") String bilDesReaTyp,
            @Param("secondBilAcyDesCd") String secondBilAcyDesCd,
            @Param("secondBilDesReaTyp") String secondBilDesReaTyp);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + " and b.polSymbolCd = :polSymbolCd and b.bilAcyDesCd in :bilAcyDesCdList")
    public ZonedDateTime findMaxBilActivityDateWithPolicySymBol(@Param("billAccountId") String billAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId and b.bilAcyDesCd in :bilAcyDesCdList and "
            + " b.bilActSummaryId.bilAcyDt <= :bilAcyDt")
    public ZonedDateTime getMaxCorrectedBilAcyDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList, @Param("bilAcyDt") ZonedDateTime bilAcyDt);

    @Query(value = "select SUM(b.bilAcyAmt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "((b.bilActSummaryId.bilAcyDt = :bilAcyDate and b.bilActSummaryId.bilAcySeq > :bilSequence) or b.bilActSummaryId.bilAcyDt > :bilAcyDateSecond"
            + " and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilDesReaTyp = :bilDesReaTyp)")
    public Double getRevertManWriteOffAmounts(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilSequence") short bilSequence,
            @Param("bilAcyDateSecond") ZonedDateTime bilAcyDateSecond, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilDesReaTyp") String bilDesReaTyp);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcyDt), MAX(b.bilActSummaryId.bilAcySeq) from BilActSummary b "
            + "where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd "
            + " and b.bilAcyDesCd in :bilAcyDesCdList and b.bilActSummaryId.bilAcyDt = :bilAcyDate")
    public List<Object[]> getMaxAcyDateAndSequenceWithPolicy(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList, @Param("bilAcyDate") ZonedDateTime bilAcyDate);

    @Query(value = "select SUM(b.bilAcyAmt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "((b.bilActSummaryId.bilAcyDt = :bilAcyDate and b.bilActSummaryId.bilAcySeq > :bilSequence) or b.bilActSummaryId.bilAcyDt > :bilAcyDateSecond"
            + " and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilDesReaTyp in :bilDesReaTypList)")
    public Double getWriteOffChargeAmounts(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilSequence") short bilSequence,
            @Param("bilAcyDateSecond") ZonedDateTime bilAcyDateSecond, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "Select b from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + " and b.polSymbolCd = :polSymbolCd and b.bilAcyDes1Dt = :bilAcyDes1Dt and b.bilAcyDesCd in :bilAcyDesCdList "
            + " order by b.bilActSummaryId.bilAcyDt desc, b.bilActSummaryId.bilAcySeq desc")
    public List<BilActSummary> getMaxRescindData(@Param("billAccountId") String billAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt, @Param("bilAcyDesCdList") List<String> bilAcyDesCdList);

    @Query(value = "Select COUNT(b) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + " ((b.bilActSummaryId.bilAcyDt = :bilAcyDate and b.bilActSummaryId.bilAcySeq > :bilSequence) or b.bilActSummaryId.bilAcyDt > :bilAcyDate) "
            + " and b.bilDesReaTyp in :bilDesReaTypList and b.bilAcyAmt <> :bilAcyAmt")
    public Integer checkCashReverseData(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilSequence") short bilSequence,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList, @Param("bilAcyAmt") Double bilAcyAmt);

    @Query(value = "select COUNT(b) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "((b.bilActSummaryId.bilAcyDt = :bilAcyDate and b.bilActSummaryId.bilAcySeq > :bilSequence) or b.bilActSummaryId.bilAcyDt > :bilAcyDate)"
            + " and b.bilAcyDesCd like :bilAcyDesCd and b.bilDesReaTyp = :bilDesReaTyp and "
            + " b.bilAcyAmt <> :bilAcyAmt  ")
    public Integer checkCashTransferData(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilSequence") short bilSequence,
            @Param("bilAcyDesCd") String bilAcyDesCd, @Param("bilDesReaTyp") String bilDesReaTyp,
            @Param("bilAcyAmt") Double bilAcyAmt);

    @Query(value = "select COUNT(b) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "((b.bilActSummaryId.bilAcyDt = :bilAcyDate and b.bilActSummaryId.bilAcySeq > :bilSequence) or b.bilActSummaryId.bilAcyDt > :bilAcyDate)"
            + " and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilDesReaTyp = :bilDesReaTyp and "
            + " b.bilAcyAmt <> :bilAcyAmt and b.bilAcyDes1Dt <= :bilAcyDes1Dt ")
    public Integer checkPremiumnWriteOffRevertAmountData(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilSequence") short bilSequence,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilDesReaTyp") String bilDesReaTyp, @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt,
            @Param("bilAcyAmt") Double bilAcyAmt);

    @Query(value = "select COUNT(b) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "b.bilActSummaryId.bilAcyDt = :bilAcyDate and b.bilActSummaryId.bilAcySeq < :bilSequence "
            + " and b.bilDesReaTyp in :bilDesReaTypList and b.bilAcyDesCd in :bilAcyDesCdList ")
    public Integer paymentsSameDayCount(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilSequence") short bilSequence,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList);

    @Query(value = "select MIN(b.bilAcyTs) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "((b.bilActSummaryId.bilAcyDt = :bilAcyDate and b.bilActSummaryId.bilAcySeq > :bilSequence) or b.bilActSummaryId.bilAcyDt > :bilAcyDate) and "
            + " b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilDesReaTyp in :bilDesReaTypList and b.bilAcyDesCd in :bilAcyDesCdList ")
    public ZonedDateTime getMinBilAcyTs(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilSequence") short bilSequence,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList);

    @Query(value = "Select b from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "b.bilActSummaryId.bilAcyDt <= :bilAcyDt and b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilAcyDes1Dt = :bilAcyDes1Dt "
            + " and b.bilAcyDesCd = :bilAcyDesCd and b.bilDesReaTyp in :bilDesReaTypList "
            + " order by b.bilActSummaryId.bilAcyDt desc, b.bilActSummaryId.bilAcySeq desc")
    public List<BilActSummary> getLastMaxAcyDate(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt,
            @Param("bilAcyDesCd") String bilAcyDesCd, @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select b from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + " and b.polSymbolCd = :polSymbolCd and b.bilAcyDesCd in :bilAcyDesCdList")
    public List<BilActSummary> getMaxEquityData(@Param("billAccountId") String billAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList);

    @Query(value = "select COUNT(b) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + " b.bilActSummaryId.bilAcyDt = :bilAcyDt and  b.bilAcyDesCd = :bilAcyDesCd")
    public Integer checkBpcRowExist(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt, @Param("bilAcyDesCd") String bilAcyDesCd);

    @Query(value = "select MAX(b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + " and b.polSymbolCd = :polSymbolCd and b.bilAcyDesCd in :bilAcyDesCdList and b.bilActSummaryId.bilAcyDt <= :bilAcyDate ")
    public ZonedDateTime findMaxActivityDateByAcyDesCode(@Param("billAccountId") String billAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList, @Param("bilAcyDate") ZonedDateTime bilAcyDate);

    @Query(value = "select MAX( b.bilActSummaryId.bilAcySeq) from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId and "
            + "b.polNbr = :polNbr and b.polSymbolCd = :polSymbolCd and b.bilActSummaryId.bilAcyDt = :bilAcyDt and b.bilAcyDesCd in :bilAcyDesCdList")
    public Short findByMaxSeqNumberByActivityDate(@Param("bilAccountId") String bilAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt, @Param("bilAcyDesCdList") List<String> bilAcyDesCdList);

    @Query(value = "select COUNT(b) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "b.bilActSummaryId.bilAcyDt = :bilAcyDate and b.bilActSummaryId.bilAcySeq < :bilSequence "
            + " and (b.bilDesReaTyp in :bilDesReaTypList or b.bilAcyDesCd in :bilAcyDesCdList)")
    public Integer checkPaymentSameDay(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilSequence") short bilSequence,
            @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList);

    @Query(value = "select b from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + " and b.polSymbolCd = :polSymbolCd and b.bilActSummaryId.bilAcyDt = :bilAcyDate and b.bilActSummaryId.bilAcySeq = :bilSequence "
            + " and b.bilAcyDesCd in :bilAcyDesCdList")
    public BilActSummary fetchBilAmountsByDesCode(@Param("billAccountId") String billAccountId,
            @Param("polNbr") String polNbr, @Param("polSymbolCd") String polSymbolCd,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate, @Param("bilSequence") short bilSequence,
            @Param("bilAcyDesCdList") List<String> bilAcyDesCdList);

    @Query(value = "select MAX( b.bilActSummaryId.bilAcyDt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :bilAccountId")
    public ZonedDateTime findByMaxAcyDateBilAccountId(@Param("bilAccountId") String bilAccountId);

    @Query(value = "select SUM(b.bilAcyAmt) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and b.polNbr = :polNbr "
            + " and b.polSymbolCd = :polSymbolCd and b.bilAcyDesCd in :bilAcyDesCdList and b.bilActSummaryId.bilAcyDt <= :bilAcyDate ")
    public Double getSumPcnAcyAmount(@Param("billAccountId") String billAccountId, @Param("polNbr") String polNbr,
            @Param("polSymbolCd") String polSymbolCd, @Param("bilAcyDesCdList") List<String> bilAcyDesCdList,
            @Param("bilAcyDate") ZonedDateTime bilAcyDate);

    @Modifying(clearAutomatically = true)
    @Query(value = "update BilActSummary b set b.bilAcyDes1Dt = :bilAcyDes1DtUpdate, b.userId = :userId,  b.bilAcyTs = :bilAcyTs "
            + " where b.bilActSummaryId.bilAccountId = :billAccountId and b.bilActSummaryId.bilAcyDt = :bilAcyDate "
            + " and b.bilAcyDes1Dt = :bilAcyDes1Dt and b.bilAcyDesCd in :bilAcyDesCdList "
            + " and b.bilDesReaTyp = :bilDesReaTyp")
    public Integer updateBilAtSummaryRowsByOldDate(@Param("bilAcyDes1DtUpdate") ZonedDateTime bilAcyDes1DtUpdate,
            @Param("userId") String userId, @Param("bilAcyTs") ZonedDateTime bilAcyTs,
            @Param("billAccountId") String billAccountId, @Param("bilAcyDate") ZonedDateTime bilAcyDate,
            @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt, @Param("bilAcyDesCdList") List<String> bilAcyDesCdList,
            @Param("bilDesReaTyp") String bilDesReaTyp);

    @Modifying(clearAutomatically = true)
    @Query(value = "update BilActSummary b set b.bilAcyDes1Dt = :bilAcyDes1DtUpdate, b.bilAcyDes2Dt = :bilAcyDes2Dt, "
            + " b.userId = :userId,  b.bilAcyTs = :bilAcyTs "
            + " where b.bilActSummaryId.bilAccountId = :billAccountId and b.bilActSummaryId.bilAcyDt = :bilAcyDate "
            + " and b.bilAcyDes1Dt = :bilAcyDes1Dt and b.bilAcyDesCd in :bilAcyDesCdList "
            + " and b.bilDesReaTyp = :bilDesReaTyp")
    public Integer updateBilAtSummaryRowsByOldDate(@Param("bilAcyDes1DtUpdate") ZonedDateTime bilAcyDes1DtUpdate,
            @Param("bilAcyDes2Dt") ZonedDateTime bilAcyDes2Dt, @Param("userId") String userId,
            @Param("bilAcyTs") ZonedDateTime bilAcyTs, @Param("billAccountId") String billAccountId,
            @Param("bilAcyDes1Dt") ZonedDateTime bilAcyDes1Dt, @Param("bilAcyDesCdList") List<String> bilAcyDesCdList,
            @Param("bilDesReaTyp") String bilDesReaTyp);
    
    @Query(value = "Select count(b) from BilActSummary b where b.bilActSummaryId.bilAccountId = :billAccountId and "
            + "b.bilActSummaryId.bilAcyDt >= :bilAcyDt and b.bilActSummaryId.bilAcyDt <= :bilAcyDtSecond and "
            + "((b.bilAcyDesCd = :bilAcyDesCd and b.bilDesReaTyp = :bilDesReaTyp) or (b.bilAcyDesCd = :bilAcyDesCdSecond and b.bilDesReaTyp = :bilDesReaTypSecond)"
            + " or (b.bilAcyDesCd = :bilAcyDesCdThird and b.bilDesReaTyp = :bilDesReaTypThird))")
    public Integer countPcnByActDate(@Param("billAccountId") String billAccountId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt, @Param("bilAcyDtSecond") ZonedDateTime bilAcyDtSecond,
            @Param("bilAcyDesCd") String bilAcyDesCd, @Param("bilDesReaTyp") String bilDesReaTyp,
            @Param("bilAcyDesCdSecond") String bilAcyDesCdSecond,
            @Param("bilDesReaTypSecond") String bilDesReaTypSecond, @Param("bilAcyDesCdThird") String bilAcyDesCdThird,
            @Param("bilDesReaTypThird") String bilDesReaTypThird);

}
