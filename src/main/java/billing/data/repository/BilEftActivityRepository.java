package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilEftActivity;
import billing.data.entity.id.BilEftActivityId;

public interface BilEftActivityRepository extends CrudRepository<BilEftActivity, BilEftActivityId> {

    @Query(value = "select COUNT(b) from BilEftActivity b where b.bilEftActivityId.bilTchKeyId = :bilTchKeyId and b.bilEftRcdTyc IN :bilEftRcdTycList")
    public Integer checkIfExists(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilEftRcdTycList") List<Character> bilEftRcdTycList);

    @Query(value = "select COUNT(b) from BilEftActivity b where b.bilEftActivityId.bilTchKeyId = :bilTchKeyId and b.bilEftActivityId.bilTchKeyTypCd = :bilTechKeyTypeCode and "
            + "b.bilEftActivityId.bilDtbSeqNbr = :sequenceNumber and b.bilEftRcdTyc = :bilEftReceiptType")
    public Short checkIfExists(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTechKeyTypeCode") Character bilTechKeyTypeCode, @Param("sequenceNumber") Short sequenceNumber,
            @Param("bilEftReceiptType") Character bilEftReceiptType);

    @Query(value = "select b from BilEftActivity b where b.bilEftActivityId.bilTchKeyId = :bilTchKeyId and b.bilEftActivityId.bilTchKeyTypCd = :bilTechKeyTypeCode and "
            + "b.bilDsbId = :disbursementId")
    public List<BilEftActivity> findByIdTechKeyTypeCodeDisburseId(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTechKeyTypeCode") Character bilTechKeyTypeCode, @Param("disbursementId") String disbursementId);

    @Query(value = "select MAX(b.bilEftActivityId.bilDtbSeqNbr) from BilEftActivity b where b.bilEftActivityId.bilTchKeyId = :accountId")
    public Short getMaxSequenceNumber(@Param("accountId") String accountId);

    @Query(value = "select b from BilEftActivity b where b.bilEftActivityId.bilTchKeyId = :technicalKey and b.bilEftActivityId.bilTchKeyTypCd =:bilTchKeyTypeCode and b.bilEftDrAmt > :amount")
    public BilEftActivity findUnidentifiedActivityRow(@Param("technicalKey") String technicalKey,
            @Param("bilTchKeyTypeCode") Character bilTchKeyTypeCode, @Param("amount") Double amount);

    @Query(value = "select b from BilEftActivity b where b.bilEftActivityId.bilTchKeyId = :bilTchKeyId and b.bilEftActivityId.bilTchKeyTypCd = :bilTechKeyTypeCode and "
            + " b.bilEftAcyDt = :bilEftAcyDt and b.bilEftActivityId.bilDtbSeqNbr = :sequenceNumber and b.beaTrsSta = :beaTrsSta")
    public List<BilEftActivity> getPaymentIdRow(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTechKeyTypeCode") Character bilTechKeyTypeCode, @Param("bilEftAcyDt") ZonedDateTime bilEftAcyDt,
            @Param("sequenceNumber") Short sequenceNumber, @Param("beaTrsSta") Character beaTrsSta);
    
    @Query(value = "select b from BilEftActivity b where b.bilEftActivityId.bilEftTraceNbr = :bilEftTraceNbr and b.bilEftActivityId.bilEftDrDt = :bilEftDrDt ")
    public List<BilEftActivity> getEftRowByTranceNumber(@Param("bilEftTraceNbr") String bilEftTraceNbr,
            @Param("bilEftDrDt") ZonedDateTime bilEftDrDt);
    
    @Query(value = "Select b from BilEftActivity b where b.bilEftActivityId.bilEftDrDt = :bilEftDrDt and b.bilEftActivityId.bilEftTraceNbr = :bilEftTraceNbr")
    public List<BilEftActivity> checkTraceNumber(@Param("bilEftDrDt") ZonedDateTime bilEftDrDt,
            @Param("bilEftTraceNbr") String bilEftTraceNbr, Pageable pageable);
    
    @Query(value = "select sum(b.bilEftDrAmt) from BilEftActivity b where b.bilEftActivityId.bilEftDrDt = :bilEftDrDt and "
            + " b.bilEftActivityId.bilEftTraceNbr =:bilEftTraceNbr")
    public Double findAmountByTraceNbr(@Param("bilEftDrDt") ZonedDateTime bilEftDrDt,
            @Param("bilEftTraceNbr") String bilEftTraceNbr);

    @Query(value = "select b from BilEftActivity b where b.bilEftActivityId.bilEftDrDt = :bilEftDrDt and "
            + " b.bilEftActivityId.bilEftTraceNbr = :bilEftTraceNbr and b.bilPmtOrderId =:bilPmtOrderId")
    public BilEftActivity checkTraceNumberAndTransactionId(@Param("bilEftDrDt") ZonedDateTime bilEftDrDt,
            @Param("bilEftTraceNbr") String bilEftTraceNbr, @Param("bilPmtOrderId") String bilPmtOrderId);
    
    @Query(value = "select b from BilEftActivity b where b.bilEftActivityId.bilEftDrDt = :bilEftDrDt and b.bilPmtOrderId =:bilPmtOrderId")
    public BilEftActivity checkTransactionId(@Param("bilEftDrDt") ZonedDateTime bilEftDrDt, @Param("bilPmtOrderId") String bilPmtOrderId);
    
    
}
