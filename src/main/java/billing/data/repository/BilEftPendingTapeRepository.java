package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilEftPendingTape;
import billing.data.entity.id.BilEftPendingTapeId;

public interface BilEftPendingTapeRepository extends JpaRepository<BilEftPendingTape, BilEftPendingTapeId> {

    @Query(value = "select MAX(b.bilEftPendingTapeId.sequenceNbr) from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :bilTchKeyId and "
            + "b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypCd and b.bilEftPendingTapeId.eftTapeDate = :bilEftTapeDt and "
            + "b.bilEftPendingTapeId.eftActivityDate = :bilEftAcyDt")
    public Short getMaximunSequenceNumber(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilEftTapeDt") ZonedDateTime bilEftTapeDt,
            @Param("bilEftAcyDt") ZonedDateTime bilEftAcyDt);

    @Query(value = "select COUNT(b) from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :technicalKey and "
            + "b.eftReceivedType IN :eftReceivedTypeList")
    public Integer checkIfExists(@Param("technicalKey") String technicalKey,
            @Param("eftReceivedTypeList") List<Character> eftReceivedTypeList);

    @Query(value = "select b.insertedRowTime from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :technicalKey and "
            + "b.bilEftPendingTapeId.technicalKeyType = :technicalKeyType and b.eftPostPaymentDate = :eftPostPaymentDate and b.receiptSequenceNumber = :receiptSequenceNumber and "
            + "b.eftReceivedType IN :eftReceivedTypeList and b.eftDrAmount > 0")
    public ZonedDateTime getTimeStamp(@Param("technicalKey") String technicalKey,
            @Param("eftPostPaymentDate") ZonedDateTime eftPostPaymentDate,
            @Param("receiptSequenceNumber") short receiptSequenceNumber,
            @Param("eftReceivedTypeList") List<Character> eftReceivedTypeList,
            @Param("technicalKeyType") Character technicalKeyType);

    @Query(value = "select MIN(b.bilEftPendingTapeId.eftTapeDate) from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :bilTchKeyId and "
            + "b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypCd and b.eftReceivedType = :eftReceivedType and "
            + "b.disbursementId = :disbursementId")
    public ZonedDateTime getMinEftTapeDate(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("eftReceivedType") char eftReceivedType,
            @Param("disbursementId") String disbursementId);

    @Query(value = "select MIN(b.eftDrDate) from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :bilTchKeyId and "
            + "b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypCd and b.eftDrAmount > :eftDrAmount and "
            + " b.eftReceivedType not in :eftReceivedTypeList ")
    public ZonedDateTime getMinimunEftDrDate(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("eftDrAmount") double eftDrAmount,
            @Param("eftReceivedTypeList") List<Character> eftReceivedTypeList);

    @Query(value = "select b from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :bilTchKeyId and "
            + "b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypCd and b.bilEftPendingTapeId.eftActivityDate = :eftActivityDate and "
            + " b.eftReceivedType = :eftReceivedType ")
    public List<BilEftPendingTape> fetchFuturePrenote(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("eftActivityDate") ZonedDateTime eftActivityDate,
            @Param("eftReceivedType") char eftReceivedType);

    @Modifying
    @Query(value = "delete BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :bilTchKeyId and "
            + "b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypCd and b.bilEftPendingTapeId.eftActivityDate = :eftActivityDate and "
            + " b.eftReceivedType in :eftReceivedTypeList ")
    public Integer deletePreNoteRows(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("eftActivityDate") ZonedDateTime eftActivityDate,
            @Param("eftReceivedTypeList") List<Character> eftReceivedTypeList);

    @Query(value = "select b from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey =:technicalKey and b.bilEftPendingTapeId.technicalKeyType =:bilTchKeyTypeCode "
            + "and b.eftPostPaymentDate =:eftPostPaymentDate and b.receiptSequenceNumber =:receiptSequenceNumber and b.eftReceivedType = :eftReceivedType and b.eftDrAmount > :amount")
    public BilEftPendingTape checkIfPendingAchRow(@Param("technicalKey") String technicalKey,
            @Param("bilTchKeyTypeCode") char bilTchKeyTypeCode,
            @Param("eftPostPaymentDate") ZonedDateTime eftPostPaymentDate,
            @Param("receiptSequenceNumber") short receiptSequenceNumber, @Param("eftReceivedType") char eftReceivedType,
            @Param("amount") Double amount);

    @Query(value = "select MAX(b.bilEftPendingTapeId.sequenceNbr) from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :technicalKey and "
            + "b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypeCode and b.bilEftPendingTapeId.eftTapeDate = :bilEftTapeDate")
    public Short getMaxSequenceNumber(@Param("technicalKey") String technicalKey,
            @Param("bilTchKeyTypeCode") char bilTchKeyTypeCode, @Param("bilEftTapeDate") ZonedDateTime bilEftTapeDate);

    @Query(value = "select b from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :bilTchKeyId and "
            + " b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypCd and b.eftReceivedType in :eftReceivedTypeList "
            + " and b.disbursementId = :disbursementId")
    public List<BilEftPendingTape> fetchEftDrAmount(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd,
            @Param("eftReceivedTypeList") List<Character> eftReceivedTypeList,
            @Param("disbursementId") String disbursementId);

    @Query(value = "select b from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey =:technicalKey and b.bilEftPendingTapeId.technicalKeyType =:bilTchKeyTypeCode "
            + "and b.eftReceivedType in :eftRecordTypeCodes and b.eftDrAmount > :amount")
    public List<BilEftPendingTape> findUnidentifiedPendingAchRows(@Param("technicalKey") String technicalKey,
            @Param("bilTchKeyTypeCode") char bilTchKeyTypeCode,
            @Param("eftRecordTypeCodes") List<Character> eftRecordTypeCodes, @Param("amount") Double amount);

    @Query(value = "select MAX(b.bilEftPendingTapeId.sequenceNbr) from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :accountId")
    public Short getMaxSequenceNumber(@Param("accountId") String accountId);

    @Query(value = "select b from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :technicalKey and b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypeCode"
            + " and b.eftDrAmount > :amount")
    public BilEftPendingTape findUnidentifiedPendingRow(@Param("technicalKey") String technicalKey,
            @Param("bilTchKeyTypeCode") char bilTchKeyTypeCode, @Param("amount") Double amount);

    @Query(value = "select b from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :technicalKey and b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypCd and "
            + "b.eftReceivedType IN :eftReceivedTypeList and b.eftDrAmount > :eftDrAmount")
    public List<BilEftPendingTape> findEftpendingTapeRows(@Param("technicalKey") String technicalKey,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd,
            @Param("eftReceivedTypeList") List<Character> eftReceivedTypeList,
            @Param("eftDrAmount") Double eftDrAmount);

    @Query(value = "select b.insertedRowTime from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :technicalKey and "
            + "b.bilEftPendingTapeId.technicalKeyType = 'A' and b.eftPostPaymentDate = :eftPostPaymentDate and b.receiptSequenceNumber = :receiptSequenceNumber and "
            + "b.eftReceivedType IN ('T','U', 'H', 'S') and b.eftDrAmount > 0")
    public ZonedDateTime getTimeStamp(@Param("technicalKey") String technicalKey,
            @Param("eftPostPaymentDate") ZonedDateTime eftPostPaymentDate,
            @Param("receiptSequenceNumber") short receiptSequenceNumber);

    @Modifying
    @Query(value = "delete BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :bilTchKeyId and "
            + " b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypCd and b.eftReceivedType in :eftReceivedTypeList and "
            + " b.bilEftPendingTapeId.eftActivityDate < :eftActivityDate")
    public Integer deleteEftPendingRows(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd,
            @Param("eftReceivedTypeList") List<Character> eftReceivedTypeList,
            @Param("eftActivityDate") ZonedDateTime eftActivityDate);

    @Modifying
    @Query(value = "Delete BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :technicalKey and "
            + " b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypCd and b.disbursementId = :disbursementId ")
    public Integer removePendingDisbursement(@Param("technicalKey") String technicalKey,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("disbursementId") String disbursementId);

    @Query(value = "select b from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :technicalKey and "
            + "b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypCd and b.eftDrAmount < :eftDrAmount and "
            + "b.eftReceivedType not in :eftReceivedTypeList and b.disbursementId <> :disbursementId ")
    public List<BilEftPendingTape> fetchDisbursementRows(@Param("technicalKey") String technicalKey,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("eftDrAmount") double eftDrAmount,
            @Param("eftReceivedTypeList") List<Character> eftReceivedTypeList,
            @Param("disbursementId") String disbursementId);
    
    @Query(value = "select b from BilEftPendingTape b where b.bilEftPendingTapeId.technicalKey = :technicalKey and "
            + "b.bilEftPendingTapeId.technicalKeyType = :bilTchKeyTypCd and b.bilEftPendingTapeId.eftActivityDate = :bilEftAcyDt "
            + " and b.receiptSequenceNumber = :receiptSequenceNumber and b.paymentOrderId = :paymentOrderId ")
    public List<BilEftPendingTape> fetchDigitalPaymentRows(@Param("technicalKey") String technicalKey,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilEftAcyDt") ZonedDateTime bilEftAcyDt,
            @Param("receiptSequenceNumber") short receiptSequenceNumber,
            @Param("paymentOrderId") String paymentOrderId);
}