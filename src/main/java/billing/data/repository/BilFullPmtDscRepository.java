package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilFullPmtDsc;
import billing.data.entity.id.BilFullPmtDscId;

public interface BilFullPmtDscRepository extends JpaRepository<BilFullPmtDsc, BilFullPmtDscId> {

    @Query(value = "select b from BilFullPmtDsc b where b.bilFullPmtDscId.bilStatePvnCd = :bilStatePvnCd and "
            + " b.bilFullPmtDscId.lobCd = :lobCd and b.bilFullPmtDscId.masterCompanyNbr = :masterCompanyNbr and "
            + " b.bilFullPmtDscId.polTermTypeCd = :polTermTypeCd and b.bilFullPmtDscId.effectiveDt <= :effectiveDt "
            + " and b.expirationDt > :expirationDt ")
    public List<BilFullPmtDsc> findBilFullPmtDscRow(@Param("bilStatePvnCd") String bilStatePvnCd,
            @Param("lobCd") String lobCd, @Param("masterCompanyNbr") String masterCompanyNbr,
            @Param("polTermTypeCd") char polTermTypeCd, @Param("effectiveDt") ZonedDateTime effectiveDt,
            @Param("expirationDt") ZonedDateTime expirationDt);

}
