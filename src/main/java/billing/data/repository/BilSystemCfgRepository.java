package billing.data.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import billing.data.entity.BilSystemCfg;
import billing.data.entity.id.BilSystemCfgId;

public interface BilSystemCfgRepository extends CrudRepository<BilSystemCfg, BilSystemCfgId> {

    @Query(value = "select b from BilSystemCfg b")
    public BilSystemCfg findRow();

}
