package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilEftBankEd;

public interface BilEftBankEdRepository extends JpaRepository<BilEftBankEd, Integer> {

    public BilEftBankEd findByRoutingTransitNbr(@Param("routingTransitNbr") String routingTransitNbr);

    public BilEftBankEd findTopByOrderByEftBankIdDesc();
    
    @Query(value = "select Max(b.eftBankId) from BilEftBankEd b")
    public Integer findMaxEftBankId();
}
