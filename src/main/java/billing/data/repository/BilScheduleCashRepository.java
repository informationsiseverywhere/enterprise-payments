package billing.data.repository;

import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilScheduleCsh;
import billing.data.entity.id.BilScheduleCshId;

public interface BilScheduleCashRepository extends JpaRepository<BilScheduleCsh, BilScheduleCshId> {

    @Query(value = "select COUNT(b) from BilScheduleCsh b where b.bilScheduleCshId.bilAccountId =:accountId and b.bilScheduleCshId.bchCshPrcCd =:bchCshPrcCd and "
            + "b.bilEntryDt =:bilEntryDt and b.bilEntrySeqNbr =:bilEntrySeqNbr and b.processInd =:processInd")
    public Integer getPendingReversalRow(@Param("accountId") String accountId,
            @Param("bchCshPrcCd") Character bchCshPrcCd, @Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntrySeqNbr") Short bilDistributionSequenceNumber, @Param("processInd") Character processInd);

}
