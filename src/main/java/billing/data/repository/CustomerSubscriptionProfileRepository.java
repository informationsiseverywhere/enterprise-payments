package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.CustomerSubscriptionProfile;
import billing.data.entity.id.CustomerSubscriptionProfileId;


public interface CustomerSubscriptionProfileRepository
        extends JpaRepository<CustomerSubscriptionProfile, CustomerSubscriptionProfileId> {

}
