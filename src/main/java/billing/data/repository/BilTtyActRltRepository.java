package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilTtyActRlt;
import billing.data.entity.id.BilTtyActRltId;

public interface BilTtyActRltRepository extends JpaRepository<BilTtyActRlt, BilTtyActRltId> {

    @Query(value = "select b.bilTtyActRltId.bilAccountId FROM BilTtyActRlt b WHERE b.bilTtyActRltId.bilThirdPartyId = :bilThirdPartyId")
    public List<String> getAccountIdByBilThirdPartyId(@Param("bilThirdPartyId") String bilThirdPartyId);

    @Query(value = "select b.bilTtyActRltId.bilThirdPartyId FROM BilTtyActRlt b WHERE b.bilTtyActRltId.bilAccountId = :bilAccountId")
    public List<String> getBilThirdPartyIdByBilAccountId(@Param("bilAccountId") String bilAccountId);

}
