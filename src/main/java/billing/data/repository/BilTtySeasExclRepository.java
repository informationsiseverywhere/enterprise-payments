package billing.data.repository;

import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilTtySeasExcl;
import billing.data.entity.id.BilTtySeasExclId;

public interface BilTtySeasExclRepository extends JpaRepository<BilTtySeasExcl, BilTtySeasExclId> {

    @Query(value = "select b from BilTtySeasExcl b where b.bilTtySeasExclId.bilTchKeyId = :bilTchKeyId and "
            + "b.bilTtySeasExclId.historyVldNbr = :historyVldNbr and b.bilTtySeasExclId.bilSeaXclCd = :bilSeaXclCd "
            + "and b.bilTtySeasExclId.bilSeaStartDt < :deductionDate and b.bilSeaEndDt > :deductionDate")
    public BilTtySeasExcl findBilTtySeasExcl(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("historyVldNbr") short historyVldNbr, @Param("bilSeaXclCd") String bilSeaXclCd,
            @Param("deductionDate") ZonedDateTime deductionDate);

}
