package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.BilEftExtPyt;
import billing.data.entity.id.BilEftExtPytId;

public interface BilEftExtPytRepository extends JpaRepository<BilEftExtPyt, BilEftExtPytId>{

}
