package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilEftBank;
import billing.data.entity.id.BilEftBankId;

public interface BilEftBankRepository extends JpaRepository<BilEftBank, BilEftBankId> {

    @Query("select b from BilEftBank b where b.bilEftBankId.bilTchKeyId = :bilTchKeyId and b.bilEftBankId.bilTchKeyTypCd = :bilTchKeyTypCd "
            + "and b.bilEftBankId.bebEffectiveDt <= :effectiveDate and b.bebAccountStaCd = :bebAccountStaCd "
            + " ORDER BY b.bilEftBankId.bebEffectiveDt DESC")
    public List<BilEftBank> findBilEftBankEntry(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("effectiveDate") ZonedDateTime effectiveDate,
            @Param("bebAccountStaCd") char bebAccountStaCd, Pageable pageable);

    @Query(value = "select MAX(b.bilEftBankId.bebEffectiveDt) from BilEftBank b where b.bilEftBankId.bilTchKeyId = :bilTchKeyId and b.bilEftBankId.bilTchKeyTypCd = "
            + ":bilTchKeyTypCd and b.bilEftBankId.bebEffectiveDt <= :effectiveDate and b.bebAccountStaCd <> :bebAccountStaCd ")
    public ZonedDateTime getMaxEffectiveDateByStatus(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("effectiveDate") ZonedDateTime effectiveDate,
            @Param("bebAccountStaCd") char bebAccountStaCd);

    @Query(value = "Select b from BilEftBank b where b.bilEftBankId.bilTchKeyId = :bilTchKeyId and b.bilEftBankId.bilTchKeyTypCd = :bilTchKeyTypCd "
            + "and b.bilEftBankId.bebEffectiveDt <= :effectiveDate ")
    public List<BilEftBank> fetchOldEftRow(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("effectiveDate") ZonedDateTime effectiveDate);

    @Query(value = "select count(b) from BilEftBank b where b.bilEftBankId.bilTchKeyId = :bilTchKeyId and b.bilEftBankId.bilTchKeyTypCd = "
            + ":bilTchKeyTypCd and b.bilEftBankId.bebEffectiveDt <= :effectiveDate and b.bebAccountStaCd NOT IN :bebAccountStaCdList ")
    public Integer countActiveEftBank(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("effectiveDate") ZonedDateTime effectiveDate,
            @Param("bebAccountStaCdList") List<Character> bebAccountStaCdList);

    @Query(value = "select MAX(b.bilEftBankId.bebEffectiveDt) from BilEftBank b where b.bilEftBankId.bilTchKeyId = :bilTchKeyId and b.bilEftBankId.bilTchKeyTypCd = "
            + ":bilTchKeyTypCd and b.bilEftBankId.bebEffectiveDt <= :effectiveDate and b.bebAccountStaCd NOT IN :bebAccountStaCdList ")
    public ZonedDateTime getMaxEffectiveDate(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("effectiveDate") ZonedDateTime effectiveDate,
            @Param("bebAccountStaCd") List<Character> bebAccountStaCdList);
    
    @Query("select b from BilEftBank b where b.bilEftBankId.bilTchKeyId = :bilTchKeyId and b.bilEftBankId.bilTchKeyTypCd = :bilTchKeyTypCd "
            + "and b.bilEftBankId.bebEffectiveDt <= :effectiveDate ORDER BY b.bilEftBankId.bebEffectiveDt DESC")
    public List<BilEftBank> findEftBankEntry(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("effectiveDate") ZonedDateTime effectiveDate);
    
    @Query("select b from BilEftBank b where b.bilEftBankId.bilTchKeyId = :bilTchKeyId and b.bilEftBankId.bilTchKeyTypCd = :bilTchKeyTypCd "
            + "and b.bilEftBankId.bebEffectiveDt < :effectiveDate and b.bebAccountStaCd = :bebAccountStaCd")
    public BilEftBank findEftBankEntry(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("effectiveDate") ZonedDateTime effectiveDate,
            @Param("bebAccountStaCd") char bebAccountStaCd);

    @Modifying(clearAutomatically = true)
    @Query("UPDATE BilEftBank b SET b.bebAccountStaCd = :newAccountStaCd where b.bilEftBankId.bilTchKeyId = :bilTchKeyId and b.bilEftBankId.bilTchKeyTypCd = :bilTchKeyTypCd "
            + "and b.bilEftBankId.bebEffectiveDt < :effectiveDate and b.bebAccountStaCd = :bebAccountStaCd")
    public Integer updateStatusCode(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("effectiveDate") ZonedDateTime effectiveDate,
            @Param("bebAccountStaCd") char bebAccountStaCd,  @Param("newAccountStaCd") char newAccountStaCd);

}
