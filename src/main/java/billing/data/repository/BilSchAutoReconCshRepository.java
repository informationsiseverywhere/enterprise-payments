package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilSchAutoReconCsh;
import billing.data.entity.id.BilSchAutoReconCshId;

public interface BilSchAutoReconCshRepository extends JpaRepository<BilSchAutoReconCsh, BilSchAutoReconCshId> {

    @Query("select MAX(b.bilSchAutoReconCshId.bilSeqNbr) from BilSchAutoReconCsh b where b.bilSchAutoReconCshId.bilTchKeyId= :bilTchKeyId and b.bilSchAutoReconCshId.bilTchKeyTypCd = :bilTchKeyTypCd ")
    public Short findMaxSequenceNumber(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") Character bilTchKeyTypCd);

    @Query("select b from BilSchAutoReconCsh b where b.bilSchAutoReconCshId.bilTchKeyId= :technicalKey and b.bilSchAutoReconCshId.bilTchKeyTypCd = :keyTypeCode "
            + "and b.bilEntryDt= :entryDate and b.bilEntryNbr= :entryNumber and b.bilEntrySeqNbr= :entrySequenceNumber and b.bilDtbDt= :distributionDate and b.bilDtbSeqNbr= "
            + ":distributionSequenceNumber and b.processInd IN :processIndicator")
    public BilSchAutoReconCsh findRow(@Param("technicalKey") String technicalKey,
            @Param("keyTypeCode") Character keyTypeCode, @Param("entryDate") ZonedDateTime entryDate,
            @Param("entryNumber") String entryNumber, @Param("entrySequenceNumber") Short entrySequenceNumber,
            @Param("distributionDate") ZonedDateTime distributionDate,
            @Param("distributionSequenceNumber") Short distributionSequenceNumber,
            @Param("processIndicator") List<Character> processIndicator);

}
