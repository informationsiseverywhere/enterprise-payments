package billing.data.repository;

import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilRulesUct;
import billing.data.entity.id.BilRulesUctId;

public interface BilRulesUctRepository extends JpaRepository<BilRulesUct, BilRulesUctId> {

    @Query(value = "select b from BilRulesUct b where b.bilRulesUctId.brtRuleId = :brtRuleId and b.bilRulesUctId.bilTypeCd = :bilTypeCd and "
            + "b.bilRulesUctId.bilClassCd = :bilClassCd and b.bilRulesUctId.bilPlanCd = :bilPlanCd and b.bilRulesUctId.effectiveDt <= :effectiveDt and "
            + "b.expirationDt > :expirationDt")
    public BilRulesUct getBilRulesRow(@Param("brtRuleId") String brtRuleId, @Param("bilTypeCd") String bilTypeCd,
            @Param("bilClassCd") String bilClassCd, @Param("bilPlanCd") String bilPlanCd,
            @Param("effectiveDt") ZonedDateTime effectiveDt, @Param("expirationDt") ZonedDateTime expirationDt);

}
