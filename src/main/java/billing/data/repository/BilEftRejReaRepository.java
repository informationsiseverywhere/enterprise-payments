package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilEftRejRea;
import billing.data.entity.id.BilEftRejReaId;

public interface BilEftRejReaRepository extends JpaRepository<BilEftRejRea, BilEftRejReaId> {
    
    @Query(value = "select b from BilEftRejRea b where b.bilEftRejReaId.bilEftRejCd = :bilEftRejCd ")
    public BilEftRejRea findEftRejCdDescription(@Param("bilEftRejCd") String bilEftRejCd);
    
    @Query(value = "select b.bilEftRejReaId.bilEftRejCd from BilEftRejRea b where b.brrRejTxt = :brrRejTxt and"
    		+ " b.bilEftRejReaId.bilEftRejCd not like 'C%' ")
    public String  findEftRejCd(@Param("brrRejTxt") String brrRejTxt);
    
    @Query(value = "select b.brrRejTxt from BilEftRejRea b where b.bilEftRejReaId.bilEftRejCd not like 'C%'"
            + " order by  b.bilEftRejReaId.bilEftRejCd")
    public List<String> findsupportData();
    
}
