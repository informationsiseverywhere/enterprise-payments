package billing.data.repository;

import org.springframework.data.repository.CrudRepository;

import billing.data.entity.BilBankEntry;
import billing.data.entity.id.BilBankEntryId;

public interface BilBankEntryRepository extends CrudRepository<BilBankEntry, BilBankEntryId> {

    public BilBankEntry findByConfirmationNbr(String confirmationNbr);

}
