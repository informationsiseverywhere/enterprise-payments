package billing.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilPrmSchedule;
import billing.data.entity.id.BilPrmScheduleId;

public interface BilPrmScheduleRepository extends JpaRepository<BilPrmSchedule, BilPrmScheduleId> {

    @Query(value = "select MAX(b.bilPrmScheduleId.bilSeqNbr) from BilPrmSchedule b where b.bilPrmScheduleId.bilAccountId = :bilAccountId "
            + "and b.bilPrmScheduleId.policyId = :policyId")
    public Short findMaxBilSequenceNumber(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId);

    @Query(value = "select b from BilPrmSchedule b where b.bilPrmScheduleId.bilAccountId = :bilAccountId "
            + "and b.bilPrmScheduleId.policyId = :policyId and b.busCaseId = :busCaseId and b.processInd = :processInd ")
    public List<BilPrmSchedule> findBilPrmScheduleByBusCase(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("busCaseId") String busCaseId,
            @Param("processInd") char processInd);

}
