package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.LegalDaysNotice;
import billing.data.entity.id.LegalDaysNoticeId;

public interface LegalDaysNoticeRepository extends JpaRepository<LegalDaysNotice, LegalDaysNoticeId> {

    @Query("select b from LegalDaysNotice b where b.legalDaysNoticeId.lineOfBusiness in :lineOfBusinessList and "
            + " b.legalDaysNoticeId.policySymbol in :policySymbolList and b.legalDaysNoticeId.riskState in :riskStateList and "
            + " b.legalDaysNoticeId.masterCompanyNumber in :masterCompanyNumberList and b.legalDaysNoticeId.effectiveTypeCode in :effectiveTypeCodeList and "
            + " b.legalDaysNoticeId.processCode = :processCode and b.legalDaysNoticeId.effectiveDt <= :effectiveDt "
            + " ORDER BY b.legalDaysNoticeId.effectiveDt DESC, b.legalDaysNoticeId.masterCompanyNumber asc, b.legalDaysNoticeId.riskState asc, "
            + " b.legalDaysNoticeId.policySymbol asc, b.legalDaysNoticeId.effectiveTypeCode, b.legalDaysNoticeId.lineOfBusiness ")
    public List<LegalDaysNotice> getLegalDaysNoticeData(@Param("lineOfBusinessList") List<String> lineOfBusinessList,
            @Param("policySymbolList") List<String> policySymbolList,
            @Param("riskStateList") List<String> riskStateList,
            @Param("masterCompanyNumberList") List<String> masterCompanyNumberList,
            @Param("processCode") String processCode, @Param("effectiveDt") ZonedDateTime effectiveDt,
            @Param("effectiveTypeCodeList") List<Character> effectiveTypeCodeList);

}
