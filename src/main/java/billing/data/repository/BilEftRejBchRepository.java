package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilEftRejBch;
import billing.data.entity.id.BilEftRejBchId;


public interface BilEftRejBchRepository extends JpaRepository<BilEftRejBch, BilEftRejBchId> {
    
    @Query(value = "select count(b) from BilEftRejBch b where b.bilEftRejBchId.brbBchNbr = :brbBchNbr and "
            + "b.bilEftRejBchId.brbEntryDt = :brbEntryDt and b.bilEftRejBchId.userId = :userId")
    public Integer countbatchExits(@Param("brbBchNbr") String brbBchNbr,
            @Param("brbEntryDt") ZonedDateTime brbEntryDt,@Param("userId") String userId);
    
    @Query(value = "select count(b) from BilEftRejBch b where b.bilEftTraceNbr = :bilEftTraceNbr and "
            + "b.bilEftDrDt = :bilEftDrDt and b.brbRejStaInd = :brbRejStaInd")
    public Integer countTraceNumberExits(@Param("bilEftTraceNbr") String bilEftTraceNbr,
            @Param("bilEftDrDt") ZonedDateTime bilEftDrDt,@Param("brbRejStaInd") char brbRejStaInd);
    
    @Query(value = "select b from BilEftRejBch b where b.bilEftRejBchId.brbBchNbr = :brbBchNbr and "
            + "b.bilEftRejBchId.brbEntryDt = :brbEntryDt and b.bilEftRejBchId.userId = :userId and "
            + " b.brbRejStaInd <> :brbRejStaInd ")
    public List<BilEftRejBch> getbatchPayments(@Param("brbBchNbr") String brbBchNbr,
            @Param("brbEntryDt") ZonedDateTime brbEntryDt,@Param("userId") String userId,
            @Param("brbRejStaInd") char brbRejStaInd, Pageable pageable);
    
    @Query(value = "select sum(b.bilEftDrAmt) from BilEftRejBch b where b.bilEftRejBchId.brbBchNbr = :brbBchNbr and "
            + "b.bilEftRejBchId.brbEntryDt = :brbEntryDt and b.bilEftRejBchId.userId = :userId"
            + " group by b.bilEftRejBchId.brbBchNbr, b.bilEftRejBchId.brbEntryDt, b.bilEftRejBchId.userId")
    public Double getAmountByBatchId(@Param("brbBchNbr") String brbBchNbr, 
            @Param("brbEntryDt") ZonedDateTime brbEntryDt,@Param("userId") String userId);
    
    @Query(value = "select b.bilEftRejBchId.brbBchNbr, b.bilEftRejBchId.brbEntryDt, sum(b.bilEftDrAmt) from BilEftRejBch b where "
            + " b.bilEftRejBchId.userId = :userId and b.brbRejStaInd in :brbRejStaIndList "
            + " group by b.bilEftRejBchId.brbBchNbr, b.bilEftRejBchId.brbEntryDt"
            + " order by b.bilEftRejBchId.brbEntryDt desc ")
    public List<Object[]> getAmountByBatchId(@Param("userId") String userId, 
            @Param("brbRejStaIndList") List<Character> brbRejStaIndList,Pageable pageable);
    
    @Query(value = "select b.bilEftRejBchId.brbBchNbr, b.bilEftRejBchId.brbEntryDt,  "
            + " sum(b.bilEftDrAmt) from BilEftRejBch b where b.bilEftRejBchId.userId = :userId"
            + " and b.bilEftRejBchId.brbEntryDt between :beginDt and :endDt "
            + " and b.brbRejStaInd in :brbRejStaIndList "
            + " group by b.bilEftRejBchId.brbBchNbr, b.bilEftRejBchId.brbEntryDt "
            + " having sum(b.bilEftDrAmt) between :beginAmount and :endAmount "
            + " order by b.bilEftRejBchId.brbEntryDt desc ")
    public List<Object[]> getAllRejectionData(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("userId") String userId,  
            @Param("brbRejStaIndList") List<Character> brbRejStaIndList, Pageable pageable);
    
    @Query(value = "select b.bilEftRejBchId.brbBchNbr, b.bilEftRejBchId.brbEntryDt,  "
            + " sum(b.bilEftDrAmt) from BilEftRejBch b where b.bilEftRejBchId.userId = :userId"
            + " and b.bilEftRejBchId.brbEntryDt between :beginDt and :endDt "
            + " and b.brbRejStaInd not in :brbRejStaIndList"
            + " group by b.bilEftRejBchId.brbBchNbr, b.bilEftRejBchId.brbEntryDt "
            + " having sum(b.bilEftDrAmt) between :beginAmount and :endAmount "
            + " order by b.bilEftRejBchId.brbEntryDt desc ")
    public List<Object[]> getAllRejectionDatawithAccept(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("userId") String userId,  
            @Param("brbRejStaIndList") List<Character> brbRejStaIndList, Pageable pageable);
    
    @Query(value = "select b from BilEftRejBch b where b.bilEftRejBchId.brbBchNbr = :brbBchNbr and "
            + "b.bilEftRejBchId.brbEntryDt = :brbEntryDt and b.bilEftRejBchId.userId = :userId"
            + " order by b.bilEftRejBchId.brbEntryDt desc ")
    public Page<BilEftRejBch> getRejectionsByBatchId(@Param("brbBchNbr") String brbBchNbr,
            @Param("brbEntryDt") ZonedDateTime brbEntryDt,@Param("userId") String userId,
            Pageable pageable);
    
    @Transactional
    @Modifying
    public Integer deleteByBilEftRejBchIdBrbEntryDtAndBilEftRejBchIdBrbBchNbrAndBilEftRejBchIdUserId(
            ZonedDateTime bilEntryDt, String bilEntryNbr, String userId);
    
    @Query(value = "select MAX(b.bilEftRejBchId.brbEntrySeqNbr) from BilEftRejBch b where b.bilEftRejBchId.brbBchNbr = :brbBchNbr "
            + " and b.bilEftRejBchId.brbEntryDt = :brbEntryDt and b.bilEftRejBchId.userId = :userId")
    public Integer getMaxBilEntrySeq(@Param("brbBchNbr") String brbBchNbr,
            @Param("brbEntryDt") ZonedDateTime brbEntryDt, @Param("userId") String userId);
    
    @Query(value = "select b from BilEftRejBch b where b.bilEftRejBchId.brbBchNbr = :brbBchNbr and "
            + "b.bilEftRejBchId.brbEntryDt = :brbEntryDt and b.bilEftRejBchId.userId = :userId and "
            + "b.bilEftRejCd = :bilEftRejCd order by b.brbRejStaInd asc ")
    public Page<BilEftRejBch> getBilEftRejBchRowsByRejectioncode(@Param("brbBchNbr") String brbBchNbr,
            @Param("brbEntryDt") ZonedDateTime brbEntryDt,@Param("userId") String userId,
            @Param("bilEftRejCd") String bilEftRejCd, Pageable pageable);
    
    @Query(value = "select b from BilEftRejBch b where b.bilEftRejBchId.brbBchNbr = :brbBchNbr and "
            + "b.bilEftRejBchId.brbEntryDt = :brbEntryDt and b.bilEftRejBchId.userId = :userId")
    public List<BilEftRejBch> getBilEftDataRows(@Param("brbBchNbr") String brbBchNbr,
            @Param("brbEntryDt") ZonedDateTime brbEntryDt,@Param("userId") String userId);
    
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE BilEftRejBch b SET b.brbRejStaInd = :brbRejStaInd, b.brbAceDt = :brbAceDt where b.bilEftRejBchId.brbBchNbr = :brbBchNbr "
            + " and b.bilEftRejBchId.brbEntryDt = :brbEntryDt and b.bilEftRejBchId.userId = :userId")
    public void updateRow(@Param("brbRejStaInd") char brbRejStaInd, @Param("brbAceDt") ZonedDateTime brbAceDt,
            @Param("brbBchNbr") String brbBchNbr, @Param("brbEntryDt") ZonedDateTime brbEntryDt,@Param("userId") String userId);

}
