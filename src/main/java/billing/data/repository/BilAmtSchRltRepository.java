package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilAmtSchRlt;
import billing.data.entity.id.BilAmtSchRltId;

public interface BilAmtSchRltRepository extends JpaRepository<BilAmtSchRlt, BilAmtSchRltId> {

    @Query(value = "select SUM(b.bilPrmIstAmt) from BilAmtSchRlt b, BilAmounts c where b.bilAmtSchRltId.bilAccountId = :bilAccountId and b.bilAmtSchRltId.bilAccountId = c.bilAmountsId.bilAccountId and "
            + " b.bilAmtSchRltId.policyId = :policyId and b.bilAmtSchRltId.policyId = c.bilAmountsId.policyId and c.bamAmtEffDt = :bamAmtEffDt and c.bamAmtExpDt <= :bamAmtExpDt and "
            + " b.bilAmtSchRltId.brsBisSeqNbr = :brsBisSeqNbr and b.bilAmtSchRltId.brsBamSeqNbr = c.bilAmountsId.bilSeqNbr and "
            + " (c.bilAmtEffInd in :bilAmtEffIndList or c.bilDesReaTyp in :bilDesReaTypList)")
    public Double getDownPaymentAmount(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList, @Param("brsBisSeqNbr") Short brsBisSeqNbr,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bilDesReaTypList") List<String> bilDesReaTypList);

    @Query(value = "select SUM(b.bilPrmIstAmt) from BilAmtSchRlt b, BilAmounts c where b.bilAmtSchRltId.bilAccountId = :bilAccountId and b.bilAmtSchRltId.bilAccountId = c.bilAmountsId.bilAccountId and "
            + " b.bilAmtSchRltId.policyId = :policyId and b.bilAmtSchRltId.policyId = c.bilAmountsId.policyId and c.bamAmtEffDt = :bamAmtEffDt and c.bamAmtExpDt <= :bamAmtExpDt and "
            + " b.bilAmtSchRltId.brsBisSeqNbr = :brsBisSeqNbr and b.bilAmtSchRltId.brsBamSeqNbr = c.bilAmountsId.bilSeqNbr and "
            + " (c.bilAmtEffInd in :bilAmtEffIndList or c.bilDesReaTyp in :bilDesReaTypList) and b.bilPrmIstAmt < :bilPrmIstAmt ")
    public Double getDownPaymentCreditAmount(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bamAmtEffDt") ZonedDateTime bamAmtEffDt,
            @Param("bilAmtEffIndList") List<Character> bilAmtEffIndList, @Param("brsBisSeqNbr") Short brsBisSeqNbr,
            @Param("bamAmtExpDt") ZonedDateTime bamAmtExpDt, @Param("bilDesReaTypList") List<String> bilDesReaTypList,
            @Param("bilPrmIstAmt") double bilPrmIstAmt);

    @Query(value = "select SUM(b.bilPrmIstAmt) from BilAmtSchRlt b, BilIstSchedule c where b.bilAmtSchRltId.bilAccountId = :bilAccountId and "
            + " b.bilAmtSchRltId.bilAccountId = c.bilIstScheduleId.billAccountId and "
            + " b.bilAmtSchRltId.policyId = :policyId and b.bilAmtSchRltId.policyId = c.bilIstScheduleId.policyId and  "
            + " b.bilAmtSchRltId.brsBisSeqNbr = :brsBisSeqNbr and b.bilAmtSchRltId.brsBamSeqNbr = c.bilIstScheduleId.billSeqNbr and "
            + " c.billAdjDueDt <= :billAdjDueDt")
    public Double getStaticPcnBamAmount(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("brsBisSeqNbr") Short brsBisSeqNbr, @Param("billAdjDueDt") ZonedDateTime billAdjDueDt);

    @Query(value = "select b.bilAmtSchRltId.brsBamSeqNbr from BilAmtSchRlt b where b.bilAmtSchRltId.bilAccountId = :bilAccountId and "
            + "b.bilAmtSchRltId.policyId = :policyId and b.bilAmtSchRltId.brsBisSeqNbr = :sequenceNumber and b.bilPrmIstAmt <> :amount")
    public List<Short> findBilAmountSequenceNumbers(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("sequenceNumber") short sequenceNumber,
            @Param("amount") double amount);
}
