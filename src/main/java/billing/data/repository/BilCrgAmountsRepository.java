package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilCrgAmounts;
import billing.data.entity.id.BilCrgAmountsId;

public interface BilCrgAmountsRepository extends JpaRepository<BilCrgAmounts, BilCrgAmountsId> {

    @Query(value = "SELECT SUM(b.bcaCrgAmt) , SUM( b.bcaCrgPaidAmt) , SUM( b.bcaWroCrgAmt) FROM BilCrgAmounts b WHERE (b.bilCrgAmountsId.bilAccountId = :bilAccountId "
            + "and b.bilCrgTypeCd in :bilCrgTypeCdList) or (b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilCrgTypeCd = :bilCrgTypeCd and b.bilInvoiceCd in "
            + ":bilInvoiceCdList)")
    public List<Object[]> getChargeAmountForAccountBalance(@Param("bilAccountId") String bilAccountId,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList, @Param("bilCrgTypeCd") char bilCrgTypeCd,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "SELECT SUM(b.bcaCrgAmt) , SUM( b.bcaCrgPaidAmt),  SUM( b.bcaWroCrgAmt) FROM BilCrgAmounts b WHERE b.bilCrgAmountsId.bilAccountId = :bilAccountId and  "
            + "b.bilInvoiceCd in :bilInvoiceCdList")
    public List<Object[]> getInvoicedChargeAmountForAccountBalance(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "SELECT SUM(b.bcaCrgAmt) AS ARG1 FROM BilCrgAmounts b WHERE b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilCrgTypeCd = :bilCrgTypeCd and "
            + "b.bilInvoiceCd  in :bilInvoiceCdList")
    public Double getServiceChargeAmountForAccountBalance(@Param("bilAccountId") String bilAccountId,
            @Param("bilCrgTypeCd") char bilCrgTypeCd, @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "SELECT SUM(b.bcaCrgAmt) AS ARG1 FROM BilCrgAmounts b WHERE b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilCrgTypeCd = :bilCrgTypeCd ")
    public Double getLateChargeAmountForAccountBalance(@Param("bilAccountId") String bilAccountId,
            @Param("bilCrgTypeCd") char bilCrgTypeCd);

    @Query(value = "select COUNT(b) from BilCrgAmounts b where b.bilInvoiceCd in :bilInvoiceCdList and b.bilCrgAmountsId.bilAccountId = :bilAccountId and (b.bilAdjDueDt <= :bilAdjDueDt "
            + "or :arg = :arg2)")
    public Integer getRowCountNumber(@Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilAccountId") String bilAccountId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("arg") String arg, @Param("arg2") String arg2);

    @Query(value = "SELECT SUM(b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) AS ARG1 FROM BilCrgAmounts b WHERE b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + "b.bilInvoiceCd  in :bilInvoiceCdList and (b.bilAdjDueDt <= :bilAdjDueDt or :arg = :arg2)")
    public Double getServiceChargeAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("arg") String arg, @Param("arg2") String arg2);

    @Query(value = "select SUM(b.bcaCrgAmt  - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where b.bilInvoiceCd in :bilInvoiceCdList and b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + "b.bilCrgTypeCd = :bilCrgTypeCd")
    public Double getTotalCrgAmount1(@Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilAccountId") String bilAccountId, @Param("bilCrgTypeCd") char bilCrgTypeCd);

    @Query(value = "select SUM(b.bcaCrgAmt  - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where b.bilCrgTypeCd in :bilCrgTypeCdList and b.bilCrgAmountsId.bilAccountId = :bilAccountId")
    public Double getTotalCrgAmount2(@Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList,
            @Param("bilAccountId") String bilAccountId);

    @Query(value = "select b.bilAdjDueDt, MAX(CASE WHEN b.bilCrgTypeCd = 'P' THEN 'Y' ELSE ' ' END) AS CrgType, "
            + "MAX(CASE WHEN b.bilCrgTypeCd = 'S' THEN 'Y' ELSE ' ' END) AS CrgType2, "
            + "MAX(CASE WHEN b.bilCrgTypeCd = 'L' THEN 'Y' ELSE ' ' END) AS CrgType3, "
            + "MAX(CASE WHEN b.bilCrgTypeCd = 'D' THEN 'Y' ELSE ' ' END) AS CrgType4 "
            + "from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and (b.bilAdjDueDt = :bilAdjDueDt OR :arg = :arg2) "
            + "and b.bilInvoiceCd in :invoiceCdList and (b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt  > 0) "
            + " group by b.bilAdjDueDt order by b.bilAdjDueDt ")
    public List<Object[]> findServiceChargeCodes(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("arg") String arg, @Param("arg2") String arg2,
            @Param("invoiceCdList") List<Character> invoiceCdList);

    @Query(value = "select b.bilAdjDueDt, MAX(CASE WHEN b.bilCrgTypeCd = 'P' THEN 'Y' ELSE ' ' END) AS CrgType, "
            + "MAX(CASE WHEN b.bilCrgTypeCd = 'S' THEN 'Y' ELSE ' ' END) AS CrgType2, "
            + "MAX(CASE WHEN b.bilCrgTypeCd = 'L' THEN 'Y' ELSE ' ' END) AS CrgType3, "
            + "MAX(CASE WHEN b.bilCrgTypeCd = 'D' THEN 'Y' ELSE ' ' END) AS CrgType4 "
            + "from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilAdjDueDt < :bilAdjDueDt "
            + "and b.bilInvoiceCd in :invoiceCdList and (b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt  > 0) "
            + " group by b.bilAdjDueDt order by b.bilAdjDueDt ")
    public List<Object[]> findOldServiceChargeCodes(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("invoiceCdList") List<Character> invoiceCdList);

    @Query(value = "select b from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + " b.bilInvoiceCd  in :bilInvoiceCdList and b.bilAdjDueDt <= :bilAdjDueDt and b.bilCrgTypeCd = :bilCrgTypeCd and "
            + " b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt > 0 ")
    public List<BilCrgAmounts> readChargeRowsByDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilCrgTypeCd") char bilCrgTypeCd,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "SELECT b from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilCrgTypeCd = :bilCrgTypeCd "
            + "and b.bilInvoiceCd in :bilInvoiceCodeList and b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt > 0")
    public List<BilCrgAmounts> fetchFuturesCharge(@Param("bilAccountId") String bilAccountId,
            @Param("bilCrgTypeCd") char bilCrgTypeCd, @Param("bilInvoiceCodeList") List<Character> bilInvoiceCodeList);

    @Query(value = "select SUM(b.bcaCrgAmt  - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where "
            + " b.bilCrgAmountsId.bilAccountId = :bilAccountId and (b.bilCrgTypeCd in :bilCrgTypeCdList or "
            + " (b.bilCrgTypeCd = :bilCrgTypeCd1 and b.bilInvoiceCd in :bilInvoiceCdList)) ")
    public Double getTotalCrgAmount(@Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilAccountId") String bilAccountId, @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList,
            @Param("bilCrgTypeCd1") char bilCrgTypeCd1);

    @Query(value = "SELECT SUM(b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) AS ARG1 FROM BilCrgAmounts b WHERE b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + "b.bilInvoiceCd  in :bilInvoiceCdList and b.bilAdjDueDt <= :bilAdjDueDt ")
    public Double getBalanceAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt);

    @Query(value = "select SUM(b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt), SUM(b.bcaCrgPaidAmt) from BilCrgAmounts b where "
            + "b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilInvoiceCd in :bilInvoiceCdList ")
    public List<Object[]> getCivChargeAmounts(@Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilAccountId") String bilAccountId);

    @Query(value = "select SUM(b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + "b.bilInvoiceCd in :bilInvoiceCdList and b.bilCrgTypeCd in :bilCrgTypeCdList and b.bilAdjDueDt <= :bilAdjDueDt")
    public Double getOutstandChargeBalanceAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt);

    @Query(value = "select MAX( b.bilAdjDueDt) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilCrgTypeCd = :bilCrgTypeCd")
    public ZonedDateTime getAdjustDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilCrgTypeCd") char bilCrgTypeCd);

    @Query(value = "select SUM(CASE WHEN b.bilCrgTypeCd = 'S' THEN (b.bcaCrgAmt - b.bcaWroCrgAmt) ELSE 0.00 END) AS SUM1, "
            + " SUM(CASE WHEN b.bilCrgTypeCd = 'S' THEN b.bcaCrgAmt ELSE 0.00 END) AS SUM2,"
            + " SUM(CASE WHEN b.bilCrgTypeCd = 'D' THEN (b.bcaCrgAmt - b.bcaWroCrgAmt) ELSE 0.00 END) AS SUM3,"
            + " SUM(CASE WHEN b.bilCrgTypeCd = 'D' THEN b.bcaCrgAmt ELSE 0.00 END) AS SUM4"
            + " from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilInvDt = :bilInvDt and "
            + " b.bilInvoiceCd in :invoiceCodeList and b.bilCrgTypeCd in :crgTypeCdList")
    public List<Object[]> getFullPaymentAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("invoiceCodeList") List<Character> invoiceCodeList,
            @Param("crgTypeCdList") List<Character> crgTypeCdList);

    @Query(value = "Select SUM(b.bcaCrgAmt), SUM( b.bcaCrgPaidAmt), SUM( b.bcaWroCrgAmt) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId "
            + " and b.bilAdjDueDt = :bilAdjDueDt and b.bilCrgTypeCd = :bilCrgTypeCd and b.bilInvoiceCd in :bilInvoiceCdList")
    public List<Object[]> getDownpaymentAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilCrgTypeCd") char bilCrgTypeCd,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "SELECT SUM(b.bcaCrgAmt), SUM( b.bcaCrgPaidAmt + b.bcaWroCrgAmt) FROM BilCrgAmounts b WHERE b.bilCrgAmountsId.bilAccountId = :bilAccountId "
            + " and b.bilCrgTypeCd in :bilCrgTypeCdList")
    public List<Object[]> getFullpayChargeAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList);

    @Query(value = "select SUM(b.bcaCrgAmt), SUM( b.bcaCrgPaidAmt + b.bcaWroCrgAmt) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId "
            + " and b.bilCrgTypeCd = :bilCrgTypeCd and b.bilInvoiceCd = :bilInvoiceCd")
    public List<Object[]> getFullpayServiceChargeAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("bilCrgTypeCd") char bilCrgTypeCd, @Param("bilInvoiceCd") char bilInvoiceCd);

    @Query(value = "select SUM(b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + "b.bilInvoiceCd in :bilInvoiceCdList and b.bilAdjDueDt = :bilAdjDueDt")
    public Double getChargeAmountDue(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt);

    @Query(value = "select b from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilCrgTypeCd = :bilCrgTypeCd ")
    public List<BilCrgAmounts> readWriteOffRowsByDueDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilCrgTypeCd") char bilCrgTypeCd);

    @Query(value = "select b from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bcaWroCrgAmt  <> :bcaWroCrgAmt "
            + " order by  b.bilAdjDueDt, b.bilCrgAmountsId.bilSeqNbr")
    public List<BilCrgAmounts> readReverseChargeRows(@Param("bilAccountId") String bilAccountId,
            @Param("bcaWroCrgAmt") double bcaWroCrgAmt);

    @Query(value = "select SUM(b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + " (b.bilCrgTypeCd in :bilCrgTypeCdList or (b.bilCrgTypeCd = :bilCrgTypeCd and b.bilInvoiceCd in :bilInvoiceCdList))")
    public Double getCollectionScgBalanceAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList, @Param("bilCrgTypeCd") char bilCrgTypeCd,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "Select b.bilInvoiceCd from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId "
            + " and b.bilInvoiceCd not in :bilInvoiceCdList and b.bilCrgTypeCd in :bilCrgTypeCdList")
    public Character getInvoiceCd(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList);

    @Query(value = "SELECT MAX(b.bilAdjDueDt) from BilCrgAmounts b "
            + "where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilInvoiceCd in :bilInvoiceCdList "
            + "and (b.bcaCrgAmt -  b.bcaCrgPaidAmt -  b.bcaWroCrgAmt) > 0 ")
    public ZonedDateTime getMaxbilAdjDueDt(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "Select SUM( b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt ) from BilCrgAmounts b "
            + " where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilInvoiceCd in :bilInvoiceCdList "
            + " and b.bilCrgTypeCd in :bilCrgTypeCdList")
    public Double findNextChargesDueAmt(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList);

    @Query(value = "SELECT MAX(b.bilAdjDueDt) from BilCrgAmounts b "
            + "where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilInvoiceCd in :bilInvoiceCdList ")
    public ZonedDateTime getMaxAdjDueDt(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "select SUM(b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + " (b.bilCrgTypeCd in :bilCrgTypeCdList or (b.bilCrgTypeCd = :bilCrgTypeCd and b.bilInvoiceCd in :bilInvoiceCdList)) "
            + " or (b.bilCrgTypeCd = :bilCrgTypeCdSecond and b.bilInvoiceCd = :bilInvoiceCd and b.bilInvDt = :bilInvDt)")
    public Double getCollectionScgBalanceAmountWithInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList, @Param("bilCrgTypeCd") char bilCrgTypeCd,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilCrgTypeCdSecond") char bilCrgTypeCdSecond, @Param("bilInvoiceCd") char bilInvoiceCd,
            @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Query(value = "select SUM(b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + " b.bilInvDt > :bilInvDt and (b.bilCrgTypeCd in :bilCrgTypeCdList or (b.bilCrgTypeCd = :bilCrgTypeCd and b.bilInvoiceCd in :bilInvoiceCdList))")
    public Double getAccessedBalanceAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList,
            @Param("bilCrgTypeCd") char bilCrgTypeCd, @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "select SUM(b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + " b.bilInvDt <= :bilInvDt and b.bilCrgTypeCd in :bilCrgTypeCdList and b.bilInvoiceCd in :bilInvoiceCdList ")
    public Double getInvoiceFeeAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "select count(b) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :accountId and b.bilAdjDueDt = :bilAdjDueDt and "
            + "b.bilInvDt = :bilInvDt")
    public Integer findCountByAccountIdAndDates(@Param("accountId") String accountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Query(value = "select MAX(b.bilCrgAmountsId.bilSeqNbr) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :accountId")
    public Integer getMaxSeqNumberByAccountId(@Param("accountId") String accountId);

    @Query(value = "select count(b) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :accountId and b.bilAdjDueDt = :bilAdjDueDt and "
            + "b.bilCrgTypeCd in :chargeTypeCodes")
    public Integer findPenaltyChargesCount(@Param("accountId") String accountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("chargeTypeCodes") List<Character> chargeTypeCodes);

    @Query(value = "select count(b) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :accountId and b.bilAdjDueDt = :bilAdjDueDt and "
            + "b.bilInvDt = :bilInvDt and b.bilCrgTypeCd = :chargeTypeCode")
    public Integer findServiceChargesCount(@Param("accountId") String accountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("chargeTypeCode") Character chargeTypeCode);

    @Query(value = "select b from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :accountId and b.bilAdjDueDt = :bilAdjDueDt and "
            + "b.bilInvDt = :bilInvDt and b.bilInvoiceCd in :billInvoiceCdList")
    public List<BilCrgAmounts> findByDueDateInvoiceDateInvoiceCode(@Param("accountId") String accountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select b from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :accountId and b.bilAdjDueDt = :bilAdjDueDt and "
            + "b.bilInvDt = :bilInvDt and b.bilCrgTypeCd in :chargeTypeCodes and b.bilInvoiceCd in :billInvoiceCdList")
    public List<BilCrgAmounts> findByDueDateInvoiceDateChargeTypeInvoiceCode(@Param("accountId") String accountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("chargeTypeCodes") List<Character> chargeTypeCodes,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilCrgAmounts b set b.bilInvoiceCd = :bilInvoiceCd, b.bcaCrgAmt = :bcaCrgAmt "
            + " where b.bilCrgAmountsId.bilAccountId = :accountId and b.bilAdjDueDt = :bilAdjDueDt and "
            + "b.bilInvDt = :bilInvDt and b.bilCrgTypeCd = :bilCrgTypeCd and b.bilInvoiceCd in :billInvoiceCdList")
    public Integer updateInvoiceDateChargeTypeInvoiceCode(@Param("bilInvoiceCd") char bilInvoiceCd,
            @Param("bcaCrgAmt") Double bcaCrgAmt, @Param("accountId") String accountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bilCrgTypeCd") Character bilCrgTypeCd,
            @Param("billInvoiceCdList") List<Character> billInvoiceCdList);

    @Query(value = "select b from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :accountId and b.bilAdjDueDt = :bilAdjDueDt and "
            + "b.bilInvDt = :bilInvDt and b.bilCrgTypeCd in :chargeTypeCodes")
    public List<BilCrgAmounts> findByDueDateInvoiceDateChargeType(@Param("accountId") String accountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("chargeTypeCodes") List<Character> chargeTypeCodes);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "delete from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :accountId and b.bilCrgTypeCd = :chargeTypeCode and "
            + "b.bilInvoiceCd in :invoiceCodeList")
    public void deleteServiceCharges(@Param("accountId") String accountId,
            @Param("invoiceCodeList") List<Character> invoiceCodeList,
            @Param("chargeTypeCode") Character chargeTypeCode);

    @Query(value = "Select b from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :accountId and b.bilCrgTypeCd = :chargeTypeCode and "
            + "b.bilInvoiceCd in :invoiceCodeList order by b.bilInvDt")
    public List<BilCrgAmounts> findDownPaymentCharges(@Param("accountId") String accountId,
            @Param("invoiceCodeList") List<Character> invoiceCodeList,
            @Param("chargeTypeCode") Character chargeTypeCode);

    @Query(value = "select SUM(b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :accountId "
            + "and b.bilAdjDueDt <= :bilAdjDueDt and b.bilInvDt <= :bilInvDt and b.bilCrgTypeCd in :chargeTypeCodes and b.bilInvoiceCd in :invoiceCodeList")
    public Double getPenaltyOrLateChargeBalanceAmount(@Param("accountId") String accountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("invoiceCodeList") List<Character> invoiceCodeList,
            @Param("chargeTypeCodes") List<Character> chargeTypeCodes);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "delete from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :accountId and b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDt and "
            + "b.bilCrgTypeCd = :chargeTypeCode and b.bilInvoiceCd in :invoiceCodeList")
    public void deleteServiceChargesByDates(@Param("accountId") String accountId,
            @Param("invoiceCodeList") List<Character> invoiceCodeList,
            @Param("chargeTypeCode") Character chargeTypeCode, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt);

    @Query(value = "select MAX(b.bilInvoiceCd) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilAdjDueDt = :adjustDueDate and b.bilInvDt = :invoiceDate")
    public Character findInvoiceCode(@Param("bilAccountId") String bilAccountId,
            @Param("adjustDueDate") ZonedDateTime adjustDueDate, @Param("invoiceDate") ZonedDateTime invoiceDate);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCrgAmounts b set b.bilAdjDueDt = :bilAdjDueDtUpdate, b.bilInvDt = :bilInvDtUpdate "
            + " where b.bilCrgAmountsId.bilAccountId = :billAccountId and b.bilAdjDueDt = :bilAdjDueDt "
            + " and b.bilInvDt = :bilInvDt and b.bilCrgTypeCd in :chargeTypeCodes")
    public Integer updateRescindCrgAmountsBybilAdjDueDt(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("billAccountId") String billAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("chargeTypeCodes") List<Character> chargeTypeCodes);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCrgAmounts b set b.bilInvoiceCd = :bilInvoiceCd "
            + " where b.bilCrgAmountsId.bilAccountId = :billAccountId and b.bilAdjDueDt = :bilAdjDueDt "
            + " and b.bilInvDt = :bilInvDt and b.bilInvoiceCd in :bilInvoiceCdList")
    public Integer updateInvoiceCdBybilAdjDueDt(@Param("bilInvoiceCd") char bilInvoiceCd,
            @Param("billAccountId") String billAccountId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCrgAmounts b set b.bilAdjDueDt = :bilAdjDueDtUpdate, b.bilInvDt = :bilInvDtUpdate, "
            + " b.bilInvoiceCd = :bilInvoiceCd where b.bilCrgAmountsId.bilAccountId = :billAccountId and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDt and b.bilInvoiceCd in :bilInvoiceCdList")
    public Integer updateInvoiceCdBybilAdjDueDt(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("bilInvoiceCd") char bilInvoiceCd,
            @Param("billAccountId") String billAccountId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "select b from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilAdjDueDt <= :bilAdjDueDt and "
            + " b.bilCrgTypeCd in :bilCrgTypeCdList and b.bilInvoiceCd in :bilInvoiceCdList ")
    public List<BilCrgAmounts> evaluateChargeAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "select b from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + " b.bilInvDt = :bilInvDt and b.bilCrgTypeCd = :bilCrgTypeCd ")
    public List<BilCrgAmounts> findBilCrgAmounts(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilCrgTypeCd") char bilCrgTypeCd);

    @Query(value = "select SUM(b.bcaCrgPaidAmt) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId "
            + "and b.bilCrgTypeCd in :bilCrgTypeCdList and b.bilInvoiceCd in :bilInvoiceCdList and b.bcaCrgPaidAmt > :bcaCrgPaidAmt")
    public Double getChargePaidAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList, @Param("bcaCrgPaidAmt") Double bcaCrgPaidAmt);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilCrgAmounts b set b.bilAdjDueDt = :bilAdjDueDt, b.bilInvDt = :bilInvDt where b.bilCrgAmountsId.bilAccountId = :bilAccountId "
            + "and b.bilCrgTypeCd in :bilCrgTypeCdList and b.bilInvoiceCd in :bilInvoiceCdList ")
    public void updateChargePaidAmount(@Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilAccountId") String bilAccountId,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList);

    @Query(value = "select b from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId "
            + "and b.bilCrgTypeCd in :bilCrgTypeCdList and b.bilInvoiceCd in :bilInvoiceCdList")
    public List<BilCrgAmounts> FetchPenaltyCharges(@Param("bilAccountId") String bilAccountId,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Query(value = "SELECT SUM( b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilInvDt = :bilInvDt and b.bilCrgTypeCd in :bilCrgTypeCdList ")
    public Double getChargeAmountForSchedule(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "delete from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilAdjDueDt = :bilAdjDueDt "
            + "and b.bilInvDt = :bilInvDt  and b.bilCrgTypeCd = :bilCrgTypeCd and b.bilInvoiceCd in :bilInvoiceCdList")
    public Integer deleteServiceChargesByDates(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bilCrgTypeCd") char bilCrgTypeCd, @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCrgAmounts b set b.bilAdjDueDt = :bilAdjDueDtUpdate, b.bilInvDt = :bilInvDtUpdate, "
            + " b.bilInvoiceCd = :bilInvoiceCd where b.bilCrgAmountsId.bilAccountId = :billAccountId and "
            + " b.bilAdjDueDt = :bilAdjDueDt and b.bilCrgTypeCd in :bilCrgTypeCdList")
    public Integer updateEftPastReferenceData(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("bilInvoiceCd") char bilInvoiceCd,
            @Param("billAccountId") String billAccountId, @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilCrgAmounts b set b.bilAdjDueDt = :bilAdjDueDtUpdate, b.bilInvDt = :bilInvDtUpdate where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilAdjDueDt = :bilAdjDueDt "
            + "and b.bilInvDt = :bilInvDt and b.bilCrgTypeCd in :chargeTypeCode")
    public void updateChargeAmount(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("chargeTypeCode") List<Character> chargeTypeCode);

    @Query(value = "select b from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + " b.bilInvoiceCd  in :bilInvoiceCdList and b.bilInvDt > :bilInvDt and b.bilCrgTypeCd = :bilCrgTypeCd and "
            + " b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt > 0  order by b.bilInvDt asc, b.bilAdjDueDt asc ")
    public List<BilCrgAmounts> readChargeRowsByInvoiceDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilCrgTypeCd") char bilCrgTypeCd,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update BilCrgAmounts b set b.bcaWroCrgAmt = b.bcaCrgAmt, b.bcaCrgPaidAmt = :bcaCrgPaidAmt where b.bilCrgAmountsId.bilAccountId = :bilAccountId and "
            + " b.bilInvoiceCd  in :bilInvoiceCdList and b.bilInvDt = :bilInvDt and b.bilCrgTypeCd = :bilCrgTypeCd ")
    public void updateWriteOffChargeAmount(@Param("bcaCrgPaidAmt") double bcaCrgPaidAmt,
            @Param("bilAccountId") String bilAccountId, @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilInvDt") ZonedDateTime bilInvDt, @Param("bilCrgTypeCd") char bilCrgTypeCd);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "delete from BilCrgAmounts b where b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilAdjDueDt <= :bilAdjDueDt "
            + "and b.bilCrgTypeCd = :bilCrgTypeCd and b.bilInvoiceCd in :bilInvoiceCdList")
    public Integer deleteServiceChargesByDates(@Param("bilAccountId") String bilAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilCrgTypeCd") char bilCrgTypeCd,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update BilCrgAmounts b set b.bilAdjDueDt = :bilAdjDueDtUpdate, b.bilInvDt = :bilInvDtUpdate "
            + " where b.bilCrgAmountsId.bilAccountId = :billAccountId and b.bilAdjDueDt = :bilAdjDueDt "
            + " and b.bilInvDt = :bilInvDt and b.bilCrgTypeCd in :bilCrgTypeCdList "
            + " and b.bilInvoiceCd in :bilInvoiceCdList")
    public Integer updateSuspendCrgAmountsBybilAdjDueDt(@Param("bilAdjDueDtUpdate") ZonedDateTime bilAdjDueDtUpdate,
            @Param("bilInvDtUpdate") ZonedDateTime bilInvDtUpdate, @Param("billAccountId") String billAccountId,
            @Param("bilAdjDueDt") ZonedDateTime bilAdjDueDt, @Param("bilInvDt") ZonedDateTime bilInvDt,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList,
            @Param("bilInvoiceCdList") List<Character> bilInvoiceCdList);
    
    @Query(value = "select SUM(b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where "
            + "b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilInvoiceCd in :bilInvoiceCdList and b.bilCrgTypeCd in :bilCrgTypeCdList")
    public Double getCivChargeAmount(@Param("bilInvoiceCdList") List<Character> bilInvoiceCdList,
            @Param("bilAccountId") String bilAccountId, @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList);
    
    @Query(value = "select SUM(b.bcaCrgAmt - b.bcaCrgPaidAmt - b.bcaWroCrgAmt) from BilCrgAmounts b where "
            + "b.bilCrgAmountsId.bilAccountId = :bilAccountId and b.bilCrgTypeCd in :bilCrgTypeCdList")
    public Double getChargeAmount(@Param("bilAccountId") String bilAccountId,
            @Param("bilCrgTypeCdList") List<Character> bilCrgTypeCdList);
}
