package billing.data.repository;

import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilTtySummary;
import billing.data.entity.id.BilTtySummaryId;

public interface BilTtySummaryRepository extends JpaRepository<BilTtySummary, BilTtySummaryId> {

    @Query(value = "select max(b.bilTtySummaryId.bilAcySeq) from BilTtySummary b WHERE b.bilTtySummaryId.bilThirdPartyId = :bilThirdPartyId and "
            + "b.bilTtySummaryId.bilAcyDt <= :bilAcyDt")
    public Short getMaxSeqNumber(@Param("bilThirdPartyId") String bilThirdPartyId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt);
    
    @Query(value = "select max(b.bilTtySummaryId.bilAcySeq) from BilTtySummary b WHERE b.bilTtySummaryId.bilThirdPartyId = :bilThirdPartyId and "
            + "b.bilTtySummaryId.bilAcyDt = :bilAcyDt")
    public Short getMaxSequenceNumber(@Param("bilThirdPartyId") String bilThirdPartyId,
            @Param("bilAcyDt") ZonedDateTime bilAcyDt);

}
