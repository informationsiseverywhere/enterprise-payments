package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.BilAccountBalance;
import billing.data.entity.id.BilAccountBalanceId;

public interface BilAccountBalanceRepository extends JpaRepository<BilAccountBalance, BilAccountBalanceId> {

}
