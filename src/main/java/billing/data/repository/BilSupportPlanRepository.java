package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.BilSupportPlan;
import billing.data.entity.id.BilSupportPlanId;

public interface BilSupportPlanRepository extends JpaRepository<BilSupportPlan, BilSupportPlanId> {

}
