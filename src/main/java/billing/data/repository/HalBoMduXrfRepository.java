package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.HalBoMduXrf;

public interface HalBoMduXrfRepository extends JpaRepository<HalBoMduXrf, String> {

}
