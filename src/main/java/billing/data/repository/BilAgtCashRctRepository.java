package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilAgtCashRct;
import billing.data.entity.id.BilAgtCashRctId;

public interface BilAgtCashRctRepository extends JpaRepository<BilAgtCashRct, BilAgtCashRctId> {

    @Query(value = "SELECT MAX(b.bilAgtCashRctId.bilDtbSeqNbr) FROM BilAgtCashRct b WHERE b.bilAgtCashRctId.bilAgtActId = :bilAgtActId and "
            + "b.bilAgtCashRctId.bilDtbDt = :bilDtbDt")
    public Short getMaxBilDtbSequenceNumber(@Param("bilAgtActId") String bilAgtActId,
            @Param("bilDtbDt") ZonedDateTime bilDtbDt);

    @Query(value = "select b from BilAgtCashRct b where b.bilEntryDt = :bilEntryDt and b.bilEntryNbr = :bilEntryNbr and b.userId = :userId"
            + " and b.bilEntrySeqNbr = :entrySequenceNumber and b.bilToFroTrfNbr = :bilToFroTrfNbr "
            + " order by b.bilAgtCashRctId.bilDtbDt asc, b.bilAgtCashRctId.bilDtbSeqNbr asc ")
    public List<BilAgtCashRct> getAgentCashReciptRow(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNbr") String bilEntryNbr, @Param("userId") String userId,
            @Param("entrySequenceNumber") short entrySequenceNumber, @Param("bilToFroTrfNbr") String bilToFroTrfNbr,
            Pageable pageable);

    @Query(value = "select b from BilAgtCashRct b where b.bilEntryDt = :bilEntryDt and b.bilEntryNbr = :bilEntryNbr and b.userId = :userId"
            + " and b.bilEntrySeqNbr = :entrySequenceNumber")
    public BilAgtCashRct findBilAgtCashReceipt(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNbr") String bilEntryNbr, @Param("userId") String userId,
            @Param("entrySequenceNumber") short entrySequenceNumber);

}
