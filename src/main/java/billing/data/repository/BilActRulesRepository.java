package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.BilActRules;
import billing.data.entity.id.BilActRulesId;

public interface BilActRulesRepository extends JpaRepository<BilActRules, BilActRulesId> {

}
