package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilActInquiry;
import billing.data.entity.id.BilActInquiryId;

public interface BilActInquiryRepository extends JpaRepository<BilActInquiry, BilActInquiryId> {

    @Query(value = "select b from BilActInquiry b where b.bilActInquiryId.bilAccountId = :bilAccountId and"
            + " b.bilActInquiryId.bilAcyTypeCd in :bilAcyTypeCdList")
    public List<BilActInquiry> fetchBilActInquiry(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyTypeCdList") List<String> bilAcyTypeCdList);

    @Modifying
    @Transactional
    @Query(value = "update BilActInquiry b set b.bilNxtAcyDt = :bilNxtAcyDt1 where b.bilActInquiryId = :bilActInquiryId and b.bilNxtAcyDt = bilNxtAcyDt")
    public Integer updateMwoBilActInquiry(@Param("bilActInquiryId") BilActInquiryId bilActInquiryId,
            @Param("bilNxtAcyDt1") ZonedDateTime bilNxtAcyDt1, @Param("bilNxtAcyDt") ZonedDateTime bilNxtAcyDt);

    @Query(value = "select b from BilActInquiry b where b.bilActInquiryId.bilAccountId = :bilAccountId and b.bilActInquiryId.bilAcyTypeCd in :bilAcyTypeCdList"
            + " and b.bilNxtAcyDt <= :bilNxtAcyDt")
    public List<BilActInquiry> getSuspendDisbRow(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyTypeCdList") List<String> bilAcyTypeCdList, @Param("bilNxtAcyDt") ZonedDateTime bilNxtAcyDt);

    @Modifying
    @Transactional
    @Query(value = "update BilActInquiry b set b.bilNxtAcyDt = :bilNxtAcyDt where b.bilActInquiryId.bilAccountId = :bilAccountId and "
            + "b.bilActInquiryId.bilAcyTypeCd in :bilAcyTypeCdList and b.bilNxtAcyDt <> :bilNxtAcyDt")
    public Integer updateOtherDisbTriggers(@Param("bilNxtAcyDt") ZonedDateTime bilNxtAcyDt,
            @Param("bilAccountId") String accountId, @Param("bilAcyTypeCdList") List<String> bilAcyTypeCdList);

    @Query(value = "select b from BilActInquiry b where b.bilActInquiryId.bilAccountId = :bilAccountId and b.bilActInquiryId.bilAcyTypeCd in :bilAcyTypeCdList"
            + " and b.bilNxtAcyDt = :bilNxtAcyDt")
    public List<BilActInquiry> getBilActInquiryByTypeCode(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyTypeCdList") List<String> bilAcyTypeCdList, @Param("bilNxtAcyDt") ZonedDateTime bilNxtAcyDt);

    @Modifying
    @Transactional
    @Query(value = "UPDATE BilActInquiry b SET b.bilNxtAcyDt = :bilNxtAcyDt where b.bilActInquiryId.bilAccountId = :bilAccountId and "
            + " b.bilActInquiryId.bilAcyTypeCd in :bilAcyTypeCdList ")
    public Integer updateExistTrigger(@Param("bilNxtAcyDt") ZonedDateTime bilNxtAcyDt,
            @Param("bilAccountId") String bilAccountId, @Param("bilAcyTypeCdList") List<String> bilAcyTypeCdList);

    @Modifying
    @Transactional
    @Query(value = "update BilActInquiry b set b.bilActInquiryId.bilAcyTypeCd = :bilAcyTypeCd where b.bilActInquiryId = :bilActInquiryId")
    public Integer updateBilAcyTypeCode(@Param("bilActInquiryId") BilActInquiryId bilActInquiryId,
            @Param("bilAcyTypeCd") String bilAcyTypeCd);

    @Modifying
    @Transactional
    @Query(value = "update BilActInquiry b set b.bilActInquiryId.bilAcyTypeCd = :bilAcyTypeCd, b.bilNxtAcyDt = :bilNxtAcyDt where b.bilActInquiryId = :bilActInquiryId")
    public Integer updateBilAcyTypeCode(@Param("bilActInquiryId") BilActInquiryId bilActInquiryId,
            @Param("bilAcyTypeCd") String bilAcyTypeCd, @Param("bilNxtAcyDt") ZonedDateTime bilNxtAcyDt);

    @Modifying
    @Transactional
    @Query(value = "delete BilActInquiry b where b.bilActInquiryId.bilAccountId = :bilAccountId and "
            + " b.bilActInquiryId.bilAcyTypeCd in :bilAcyTypeCdList ")
    public Integer deleteCorrectedInvoiceRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyTypeCdList") List<String> bilAcyTypeCdList);

    @Modifying
    @Transactional
    @Query(value = "delete BilActInquiry b where b.bilActInquiryId.bilAccountId = :bilAccountId and "
            + " b.bilActInquiryId.bilAcyTypeCd = :bilAcyTypeCd ")
    public Integer deleteManualReInvoiceRows(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyTypeCd") String bilAcyTypeCd);
    
    @Modifying
    @Transactional
    @Query(value = "Delete from BilActInquiry b Where b.bilActInquiryId.bilAccountId = :bilAccountId and b.bilActInquiryId.bilAcyTypeCd IN :bilAcyTypeCdList")
    public void deleteByTechnicalKeyIdentifier(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyTypeCdList") List<String> bilAcyTypeCdList);
    @Query(value = "select count(b) from BilActInquiry b where b.bilActInquiryId.bilAccountId = :bilAccountId and ((b.bilActInquiryId.bilAcyTypeCd between :bilAcyTypeStart and :bilAcyTypeEnd) or b.bilActInquiryId.bilAcyTypeCd = :bilAcyTypeCd ) ")
    public Integer existenceCheck(@Param("bilAccountId") String bilAccountId,
    @Param("bilAcyTypeStart") String bilAcyTypeStart,
    @Param("bilAcyTypeEnd") String bilAcyTypeEnd, @Param("bilAcyTypeCd") String bilAcyTypeCd);
    
    @Query(value = "select count(b) from BilActInquiry b where b.bilActInquiryId.bilAccountId = :bilAccountId and "
            + " b.bilActInquiryId.bilAcyTypeCd = :bilAcyTypeCd ")
    public Integer checkExistenceByTypeCd(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyTypeCd") String bilAcyTypeCd);


}
