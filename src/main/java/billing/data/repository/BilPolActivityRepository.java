package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilPolActivity;
import billing.data.entity.id.BilPolActivityId;

public interface BilPolActivityRepository extends JpaRepository<BilPolActivity, BilPolActivityId> {

    @Modifying
    @Query(value = "DELETE FROM BilPolActivity b WHERE b.bilPolActivityId = :bilPolActivityId ")
    public Integer deleteActivityRow(@Param("bilPolActivityId") BilPolActivityId bilPolActivityId);

    @Query(value = "select count(b) from BilPolActivity b where b.bilPolActivityId.policyId = :policyId and b.bilPolActivityId.bilAccountId = :bilAccountId "
            + " and b.bilPolActivityId.polEffectiveDt = :polEffectiveDt and ((b.bilPolActivityId.bilAcyTypeCd between :bilAcyTypeStart and :bilAcyTypeEnd) "
            + "or b.bilPolActivityId.bilAcyTypeCd = :bilAcyTypeCd)")
    public Integer checkPolicyPreCollection(@Param("policyId") String policyId,
            @Param("bilAccountId") String bilAccountId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bilAcyTypeCd") String bilAcyTypeCd, @Param("bilAcyTypeStart") String bilAcyTypeStart,
            @Param("bilAcyTypeEnd") String bilAcyTypeEnd);

    @Query(value = "select b from BilPolActivity b where b.bilPolActivityId.bilAccountId = :bilAccountId "
            + " and b.bilPolActivityId.bilAcyTypeCd = :bilAcyTypeCd")
    public List<BilPolActivity> fetchBilPolActivityTriger(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyTypeCd") String bilAcyTypeCd);

    @Modifying
    @Query(value = "DELETE FROM BilPolActivity b WHERE b.bilPolActivityId.bilAccountId = :bilAccountId and b.bilPolActivityId.policyId = :policyId "
            + "and (b.bilPolActivityId.bilAcyTypeCd between :bilAcyTypeCd1 and :bilAcyTypeCd2 or b.bilPolActivityId.bilAcyTypeCd  IN :bilAcyTypeCdList)")
    public Integer deleteBilPolActivityByAccountId(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bilAcyTypeCd1") String bilAcyTypeCd1,
            @Param("bilAcyTypeCd2") String bilAcyTypeCd2, @Param("bilAcyTypeCdList") List<String> bilAcyTypeCdList);

    @Query(value = "select b from BilPolActivity b where b.bilPolActivityId.bilAccountId = :bilAccountId "
            + " and b.bilPolActivityId.bilAcyTypeCd = :bilAcyTypeCd and b.bilNxtAcyDt <= :bilNxtAcyDt"
            + " order by b.bilPolActivityId.polEffectiveDt")
    public List<BilPolActivity> fetchBilPolActivityByNextAcyDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyTypeCd") String bilAcyTypeCd, @Param("bilNxtAcyDt") ZonedDateTime bilNxtAcyDt);

    @Query(value = "delete from BilPolActivity b where b.bilPolActivityId.bilAccountId = :bilAccountId and  b.bilPolActivityId.bilAcyTypeCd = :bilAcyTypeCd ")
    public Integer deleteBilPolActivityRow(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyTypeCd") String bilAcyTypeCd);

    @Query(value = "select b from BilPolActivity b where b.bilPolActivityId.bilAccountId = :bilAccountId and b.bilPolActivityId.policyId = :policyId "
            + " and b.bilPolActivityId.bilAcyTypeCd = :bilAcyTypeCd")
    public List<BilPolActivity> fetchBilPolActivityTriger(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("bilAcyTypeCd") String bilAcyTypeCd);

    @Query(value = "select b from BilPolActivity b where b.bilPolActivityId.bilAccountId = :bilAccountId and b.bilPolActivityId.policyId = :policyId "
            + " and b.bilPolActivityId.polEffectiveDt = :polEffectiveDt and b.bilPolActivityId.bilAcyTypeCd in :bilAcyTypeCdList ")
    public List<BilPolActivity> fetchNrdActivityTriger(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bilAcyTypeCdList") List<String> bilAcyTypeCdList);

    @Query(value = "select b from BilPolActivity b where b.bilPolActivityId.bilAccountId = :bilAccountId and b.bilPolActivityId.policyId = :policyId "
            + " and b.bilPolActivityId.polEffectiveDt <> :polEffectiveDt and b.bilPolActivityId.bilAcyTypeCd = :bilAcyTypeCd")
    public List<BilPolActivity> fetchBilPolActivityByDate(@Param("bilAccountId") String bilAccountId,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bilAcyTypeCd") String bilAcyTypeCd);

    @Query(value = "select COUNT(b) from BilPolActivity b where b.bilPolActivityId.bilAccountId = :bilAccountId and b.bilPolActivityId.policyId = :policyId and "
            + "b.bilPolActivityId.polEffectiveDt = '9999-12-31' and b.bilPolActivityId.bilAcyTypeCd = :bilAcyTypeCd")
    public Integer getPolicyAmountsCount(@Param("bilAccountId") String bilAccountId, @Param("policyId") String policyId,
            @Param("bilAcyTypeCd") String bilAcyTypeCd);

    @Query(value = "select b from BilPolActivity b where b.bilPolActivityId.bilAccountId = :bilAccountId "
            + " and b.bilPolActivityId.bilAcyTypeCd = :bilAcyTypeCd and b.bilNxtAcyDt >= :bilNxtAcyDt"
            + " order by b.bilPolActivityId.polEffectiveDt")
    public List<BilPolActivity> fetchActivityByNextAcyDate(@Param("bilAccountId") String bilAccountId,
            @Param("bilAcyTypeCd") String bilAcyTypeCd, @Param("bilNxtAcyDt") ZonedDateTime bilNxtAcyDt);

}
