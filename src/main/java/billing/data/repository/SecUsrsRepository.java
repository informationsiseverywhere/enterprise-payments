package billing.data.repository;

import org.springframework.data.repository.CrudRepository;

import billing.data.entity.SecUsrs;

public interface SecUsrsRepository extends CrudRepository<SecUsrs, String> {

}
