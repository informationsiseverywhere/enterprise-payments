package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.BilBillPlan;

public interface BilBillPlanRepository extends JpaRepository<BilBillPlan, String> {

}
