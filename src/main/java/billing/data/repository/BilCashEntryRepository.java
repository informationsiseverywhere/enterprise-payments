package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilCashEntry;
import billing.data.entity.id.BilCashEntryId;

public interface BilCashEntryRepository extends CrudRepository<BilCashEntry, BilCashEntryId> {

    @Query(value = "select SUM(b.bilRctAmt) from BilCashEntry b where b.bilCashEntryId.bilEntryDt = :bilEntryDt and "
            + "b.bilCashEntryId.bilEntryNbr = :bilEntryNumber and b.bilCashEntryId.userId = :userId")
    public Double findAccumulatedAmount(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNumber") String bilEntryNumber, @Param("userId") String userId);

    @Query(value = "select SUM(b.bilRctAmt) from BilCashEntry b where b.bilCashEntryId.bilEntryDt = :bilEntryDt and "
            + "b.bilCashEntryId.bilEntryNbr = :bilEntryNumber and b.bilCashEntryId.userId = :userId and b.bilRctTypeCd IN :paymentTypes")
    public Double findCashAmount(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNumber") String bilEntryNumber, @Param("userId") String userId,
            @Param("paymentTypes") List<String> paymentTypes);

    @Query(value = "select b from BilCashEntry b where b.bilCashEntryId.bilEntryDt = :bilEntryDt and "
            + "b.bilCashEntryId.bilEntryNbr = :bilEntryNumber and b.bilCashEntryId.userId = :userId "
            + " order by b.insertedRowTs desc ")
    public Page<BilCashEntry> getBilCashEntryRowsByPaging(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNumber") String bilEntryNumber, @Param("userId") String userId, Pageable pageable);

    @Query(value = "select b from BilCashEntry b where b.bilCashEntryId.bilEntryDt = :bilEntryDt and "
            + "b.bilCashEntryId.bilEntryNbr = :bilEntryNumber and b.bilCashEntryId.userId = :userId and "
            + "(b.bilAccountNbr like CONCAT('%',:identifier,'%') OR b.bilTtyNbr like CONCAT('%',:identifier,'%')  OR b.bilAgtActNbr like CONCAT('%',:identifier,'%')) "
            + " order by  b.insertedRowTs desc ")
    public Page<BilCashEntry> getBilCashEntryRowsByIdentifier(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNumber") String bilEntryNumber, @Param("userId") String userId,
            @Param("identifier") String identifier, Pageable pageable);

    @Query(value = "select b from BilCashEntry b where b.bilCashEntryId.bilEntryDt = :bilEntryDt and "
            + "b.bilCashEntryId.bilEntryNbr = :bilEntryNumber and b.bilCashEntryId.userId = :userId and b.bilRctTypeCd = :bilRctTypeCd "
            + "order by b.insertedRowTs desc ")
    public Page<BilCashEntry> getBilCashEntryRowsByPaymentType(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNumber") String bilEntryNumber, @Param("userId") String userId,
            @Param("bilRctTypeCd") String bilRctTypeCd, Pageable pageable);

    @Query(value = "select b from BilCashEntry b where b.bilCashEntryId.bilEntryDt = :bilEntryDt and "
            + "b.bilCashEntryId.bilEntryNbr = :bilEntryNumber and b.bilCashEntryId.userId = :userId and b.bilRctTypeCd = :bilRctTypeCd and "
            + "(b.bilAccountNbr like CONCAT('%',:identifier,'%') OR b.bilTtyNbr like CONCAT('%',:identifier,'%')  OR b.bilAgtActNbr like CONCAT('%',:identifier,'%'))"
            + " order by b.insertedRowTs desc ")
    public Page<BilCashEntry> getBilCashEntryRowsByFilters(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNumber") String bilEntryNumber, @Param("userId") String userId,
            @Param("bilRctTypeCd") String bilRctTypeCd, @Param("identifier") String identifier, Pageable pageable);

    @Transactional
    @Modifying
    public Integer deleteByBilCashEntryIdBilEntryDtAndBilCashEntryIdBilEntryNbrAndBilCashEntryIdUserId(
            ZonedDateTime bilEntryDt, String bilEntryNbr, String userId);

    @Query(value = "select MAX(b.bilCashEntryId.bilEntrySeqNbr) from BilCashEntry b where b.bilCashEntryId.bilEntryDt = :bilEntryDt and "
            + "b.bilCashEntryId.bilEntryNbr = :bilEntryNumber and b.bilCashEntryId.userId = :userId")
    public Short getMaxBilEntrySeqByBilEntry(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNumber") String bilEntryNumber, @Param("userId") String userId);

    @Query(value = "select MAX(b.bilCashEntryId.bilEntrySeqNbr) from BilCashEntry b where b.bilCashEntryId.bilEntryNbr = :bilEntryNumber "
            + " and b.bilCashEntryId.bilEntryDt = :bilEntryDt and b.bilCashEntryId.userId = :userId")
    public Integer getMaxBilEntrySeq(@Param("bilEntryNumber") String bilEntryNumber,
            @Param("bilEntryDt") ZonedDateTime bilEntryDt, @Param("userId") String userId);

    @Query(value = "select b from BilCashEntry b JOIN BilCashEntryTot c ON b.bilCashEntryId.bilEntryDt = c.bilCashEntryTotId.bilEntryDt "
            + "and b.bilCashEntryId.bilEntryNbr = c.bilCashEntryTotId.bilEntryNbr and b.bilCashEntryId.userId = c.bilCashEntryTotId.userId "
            + "LEFT OUTER JOIN BilUnIdCash u ON b.bilCashEntryId.bilEntryDt = u.bilUnIdCashId.bilEntryDt and b.bilCashEntryId.bilEntryNbr = u.bilUnIdCashId.bilEntryNbr "
            + "and b.bilCashEntryId.userId = u.bilUnIdCashId.userId and b.bilCashEntryId.bilEntrySeqNbr = u.bilEntrySeqNbr where u.bilUnIdCashId.userId IS NULL and "
            + "u.bilEntrySeqNbr IS NULL and u.bilUnIdCashId.bilEntryDt IS NULL and b.bilCashEntryId.bilEntryDt between :beginDt and :endDt and "
            + "b.bilRctAmt between :beginAmount and :endAmount and c.bilCshEtrMthCd in :bilCashEntryList and c.bctAcceptInd = :bctAcceptInd and b.bilCashEntryId.userId between :fromUserId and :toUserId "
            + "and ((b.bilPmtOrderId between :fromPaymentId and :toPaymentId) or(b.bilPmtOrderId = '' and b.bilRctId between :fromPaymentId and :toPaymentId))  order by b.bilCashEntryId.bilEntryDt desc, b.insertedRowTs desc ")
    public Page<BilCashEntry> getAllPayments(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("bilCashEntryList") List<Character> bilCashEntryList,
            @Param("bctAcceptInd") char bctAcceptInd, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, Pageable pageable);

    @Query(value = "select b from BilCashEntry b JOIN BilCashEntryTot c ON b.bilCashEntryId.bilEntryDt = c.bilCashEntryTotId.bilEntryDt "
            + "and b.bilCashEntryId.bilEntryNbr = c.bilCashEntryTotId.bilEntryNbr and b.bilCashEntryId.userId = c.bilCashEntryTotId.userId "
            + "LEFT OUTER JOIN BilUnIdCash u ON b.bilCashEntryId.bilEntryDt = u.bilUnIdCashId.bilEntryDt and b.bilCashEntryId.bilEntryNbr = u.bilUnIdCashId.bilEntryNbr "
            + "and b.bilCashEntryId.userId = u.bilUnIdCashId.userId and b.bilCashEntryId.bilEntrySeqNbr = u.bilEntrySeqNbr where u.bilUnIdCashId.userId IS NULL and "
            + "u.bilEntrySeqNbr IS NULL and u.bilUnIdCashId.bilEntryDt IS NULL and b.bilRctReceiveDt between :beginDt and :endDt and "
            + "b.bilRctAmt between :beginAmount and :endAmount and c.bilCshEtrMthCd in :bilCashEntryList and c.bctAcceptInd = :bctAcceptInd and b.bilCashEntryId.userId between :fromUserId and :toUserId "
            + "and ((b.bilPmtOrderId between :fromPaymentId and :toPaymentId) or(b.bilPmtOrderId = '' and b.bilRctId between :fromPaymentId and :toPaymentId))  order by b.bilRctReceiveDt desc, b.insertedRowTs desc ")
    public Page<BilCashEntry> getAllPaymentsByReceiptDate(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("bilCashEntryList") List<Character> bilCashEntryList,
            @Param("bctAcceptInd") char bctAcceptInd, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, Pageable pageable);

    @Query(value = "select b from BilCashEntry b JOIN BilCashEntryTot c ON b.bilCashEntryId.bilEntryDt = c.bilCashEntryTotId.bilEntryDt "
            + "and b.bilCashEntryId.bilEntryNbr = c.bilCashEntryTotId.bilEntryNbr and b.bilCashEntryId.userId = c.bilCashEntryTotId.userId and c.currencyCd = :currency "
            + "LEFT OUTER JOIN BilUnIdCash u ON b.bilCashEntryId.bilEntryDt = u.bilUnIdCashId.bilEntryDt and b.bilCashEntryId.bilEntryNbr = u.bilUnIdCashId.bilEntryNbr "
            + "and b.bilCashEntryId.userId = u.bilUnIdCashId.userId and b.bilCashEntryId.bilEntrySeqNbr = u.bilEntrySeqNbr where u.bilUnIdCashId.userId IS NULL and "
            + "u.bilEntrySeqNbr IS NULL and u.bilUnIdCashId.bilEntryDt IS NULL and b.bilRctReceiveDt between :beginDt and :endDt and "
            + "b.bilRctAmt between :beginAmount and :endAmount and c.bilCshEtrMthCd in :bilCashEntryList and c.bctAcceptInd = :bctAcceptInd and b.bilCashEntryId.userId between :fromUserId and :toUserId "
            + "and ((b.bilPmtOrderId between :fromPaymentId and :toPaymentId) or(b.bilPmtOrderId = '' and b.bilRctId between :fromPaymentId and :toPaymentId))  order by b.bilRctReceiveDt desc, b.insertedRowTs desc ")
    public Page<BilCashEntry> getAllPaymentsByReceiptDateAndCurrency(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("bilCashEntryList") List<Character> bilCashEntryList,
            @Param("bctAcceptInd") char bctAcceptInd, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("currency") String currency, Pageable pageable);

    @Query(value = "select b from BilCashEntry b, BilCashReceipt c where b.bilCashEntryId.bilEntryDt between :beginDt and :endDt and "
            + "b.bilRctAmt between :beginAmount and :endAmount and b.bilCashEntryId.bilEntryDt = c.entryDate "
            + "and b.bilCashEntryId.bilEntryNbr = c.entryNumber and b.bilCashEntryId.userId = c.userId and b.bilAccountNbr != :bilAccountNbr and "
            + "b.bilCashEntryId.bilEntrySeqNbr = c.entrySequenceNumber and c.cashEntryMethodCd in :bilCashEntryList and "
            + "b.bilCashEntryId.userId between :fromUserId and :toUserId and ((b.bilPmtOrderId between :fromPaymentId and :toPaymentId) "
            + " or(b.bilPmtOrderId = '' and b.bilRctId between :fromPaymentId and :toPaymentId)) order by b.insertedRowTs desc ")
    public Page<BilCashEntry> getAllPaymentsByBillAccount(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("bilCashEntryList") List<Character> bilCashEntryList,
            @Param("bilAccountNbr") String bilAccountNbr, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, Pageable pageable);

    @Query(value = "select b from BilCashEntry b, BilTtyCashRct c where b.bilCashEntryId.bilEntryDt between :beginDt and :endDt and "
            + "b.bilRctAmt between :beginAmount and :endAmount and b.bilCashEntryId.bilEntryDt = c.bilEntryDt "
            + "and b.bilCashEntryId.bilEntryNbr = c.bilEntryNbr and b.bilCashEntryId.userId = c.userId and b.bilTtyNbr != :bilTtyNbr and "
            + "b.bilCashEntryId.bilEntrySeqNbr = c.bilEntrySeqNbr and c.bilCshEtrMthCd in :bilCashEntryList and "
            + "b.bilCashEntryId.userId between :fromUserId and :toUserId and ((b.bilPmtOrderId between :fromPaymentId and :toPaymentId) "
            + " or(b.bilPmtOrderId = '' and b.bilRctId between :fromPaymentId and :toPaymentId)) order by b.insertedRowTs desc ")
    public Page<BilCashEntry> getAllPaymentsByGroup(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("bilCashEntryList") List<Character> bilCashEntryList,
            @Param("bilTtyNbr") String bilTtyNbr, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, Pageable pageable);

    @Query(value = "select b from BilCashEntry b, BilAgtCashRct c where b.bilCashEntryId.bilEntryDt between :beginDt and :endDt and "
            + "b.bilRctAmt between :beginAmount and :endAmount and b.bilCashEntryId.bilEntryDt = c.bilEntryDt "
            + "and b.bilCashEntryId.bilEntryNbr = c.bilEntryNbr and b.bilCashEntryId.userId = c.userId and b.bilAgtActNbr != :bilAgtActNbr and "
            + "b.bilCashEntryId.bilEntrySeqNbr = c.bilEntrySeqNbr and c.bilCshEtrMthCd in :bilCashEntryList and "
            + "b.bilCashEntryId.userId between :fromUserId and :toUserId and ((b.bilPmtOrderId between :fromPaymentId and :toPaymentId) "
            + "or(b.bilPmtOrderId = '' and b.bilRctId between :fromPaymentId and :toPaymentId)) order by b.insertedRowTs desc ")
    public Page<BilCashEntry> getAllPaymentsByAgency(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("bilCashEntryList") List<Character> bilCashEntryList,
            @Param("bilAgtActNbr") String bilAgtActNbr, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, Pageable pageable);

    @Query(value = "select b from BilCashEntry b where b.bilCashEntryId.bilEntryDt = :bilEntryDt "
            + " and b.bilCashEntryId.bilEntryNbr = :bilEntryNumber and b.bilCashEntryId.bilEntrySeqNbr = :bilEntrySeqNbr "
            + " and b.bilManualSusInd = :bilManualSusInd ")
    public List<BilCashEntry> getPaymentsByManualSusInd(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNumber") String bilEntryNumber, @Param("bilEntrySeqNbr") short bilEntrySeqNbr,
            @Param("bilManualSusInd") char bilManualSusInd);

    @Query(value = "select b from BilCashEntry b, BilTtyCashRct c where b.bilRctReceiveDt between :beginDt and :endDt and "
            + "b.bilRctAmt between :beginAmount and :endAmount and b.bilCashEntryId.bilEntryDt = c.bilEntryDt "
            + "and b.bilCashEntryId.bilEntryNbr = c.bilEntryNbr and b.bilCashEntryId.userId = c.userId and b.bilTtyNbr != :bilTtyNbr and "
            + "b.bilCashEntryId.bilEntrySeqNbr = c.bilEntrySeqNbr and c.bilCshEtrMthCd in :bilCashEntryList and "
            + "b.bilCashEntryId.userId between :fromUserId and :toUserId and ((b.bilPmtOrderId between :fromPaymentId and :toPaymentId) "
            + "or(b.bilPmtOrderId = '' and b.bilRctId between :fromPaymentId and :toPaymentId)) order by b.insertedRowTs desc ")
    public Page<BilCashEntry> getAllPaymentsByGroupWithReceiptDate(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("bilCashEntryList") List<Character> bilCashEntryList,
            @Param("bilTtyNbr") String bilTtyNbr, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, Pageable pageable);
    
    @Query(value = "select b from BilCashEntry b, BilTtyCashRct c, BilCashEntryTot d where b.bilRctReceiveDt between :beginDt and :endDt and "
            + "b.bilRctAmt between :beginAmount and :endAmount and b.bilCashEntryId.bilEntryDt = c.bilEntryDt "
            + "and b.bilCashEntryId.bilEntryNbr = c.bilEntryNbr and b.bilCashEntryId.userId = c.userId and b.bilTtyNbr != :bilTtyNbr and "
            + "b.bilCashEntryId.bilEntrySeqNbr = c.bilEntrySeqNbr and c.bilCshEtrMthCd in :bilCashEntryList and "
            + "b.bilCashEntryId.userId between :fromUserId and :toUserId and ((b.bilPmtOrderId between :fromPaymentId and :toPaymentId) "
            + "or(b.bilPmtOrderId = '' and b.bilRctId between :fromPaymentId and :toPaymentId)) and b.bilCashEntryId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt "
            + "and b.bilCashEntryId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and b.bilCashEntryId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "order by b.insertedRowTs desc ")
    public Page<BilCashEntry> getAllPaymentsByGroupWithCurrency(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("bilCashEntryList") List<Character> bilCashEntryList,
            @Param("bilTtyNbr") String bilTtyNbr, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("currency") String currency, Pageable pageable);

    @Query(value = "select b from BilCashEntry b, BilCashReceipt c where b.bilRctReceiveDt between :beginDt and :endDt and "
            + "b.bilRctAmt between :beginAmount and :endAmount and b.bilCashEntryId.bilEntryDt = c.entryDate "
            + "and b.bilCashEntryId.bilEntryNbr = c.entryNumber and b.bilCashEntryId.userId = c.userId and b.bilAccountNbr != :bilAccountNbr and "
            + "b.bilCashEntryId.bilEntrySeqNbr = c.entrySequenceNumber and c.cashEntryMethodCd in :bilCashEntryList and "
            + "b.bilCashEntryId.userId between :fromUserId and :toUserId and ((b.bilPmtOrderId between :fromPaymentId and :toPaymentId) "
            + "or(b.bilPmtOrderId = '' and b.bilRctId between :fromPaymentId and :toPaymentId)) order by b.insertedRowTs desc ")
    public Page<BilCashEntry> getAllPaymentsByBillAccountWithReceiptDate(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("bilCashEntryList") List<Character> bilCashEntryList,
            @Param("bilAccountNbr") String bilAccountNbr, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, Pageable pageable);

    @Query(value = "select b from BilCashEntry b, BilCashReceipt c, BilCashEntryTot d where b.bilRctReceiveDt between :beginDt and :endDt and "
            + "b.bilRctAmt between :beginAmount and :endAmount and b.bilCashEntryId.bilEntryDt = c.entryDate "
            + "and b.bilCashEntryId.bilEntryNbr = c.entryNumber and b.bilCashEntryId.userId = c.userId and b.bilAccountNbr != :bilAccountNbr and "
            + "b.bilCashEntryId.bilEntrySeqNbr = c.entrySequenceNumber and c.cashEntryMethodCd in :bilCashEntryList and "
            + "b.bilCashEntryId.userId between :fromUserId and :toUserId and ((b.bilPmtOrderId between :fromPaymentId and :toPaymentId) "
            + "or(b.bilPmtOrderId = '' and b.bilRctId between :fromPaymentId and :toPaymentId)) and b.bilCashEntryId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt "
            + "and b.bilCashEntryId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and b.bilCashEntryId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + "order by b.insertedRowTs desc ")
    public Page<BilCashEntry> getAllPaymentsByBillAccountWithCurrency(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("bilCashEntryList") List<Character> bilCashEntryList,
            @Param("bilAccountNbr") String bilAccountNbr, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("currency") String currency, Pageable pageable);

    @Query(value = "select b from BilCashEntry b, BilAgtCashRct c where b.bilRctReceiveDt between :beginDt and :endDt and "
            + "b.bilRctAmt between :beginAmount and :endAmount and b.bilCashEntryId.bilEntryDt = c.bilEntryDt "
            + "and b.bilCashEntryId.bilEntryNbr = c.bilEntryNbr and b.bilCashEntryId.userId = c.userId and b.bilAgtActNbr != :bilAgtActNbr and "
            + "b.bilCashEntryId.bilEntrySeqNbr = c.bilEntrySeqNbr and c.bilCshEtrMthCd in :bilCashEntryList and "
            + "b.bilCashEntryId.userId between :fromUserId and :toUserId and ((b.bilPmtOrderId between :fromPaymentId and :toPaymentId) "
            + "or(b.bilPmtOrderId = '' and b.bilRctId between :fromPaymentId and :toPaymentId)) order by b.insertedRowTs desc ")
    public Page<BilCashEntry> getAllPaymentsByAgencyWithReceiptDate(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("bilCashEntryList") List<Character> bilCashEntryList,
            @Param("bilAgtActNbr") String bilAgtActNbr, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, Pageable pageable);

    @Query(value = "select b from BilCashEntry b, BilAgtCashRct c, BilCashEntryTot d where b.bilRctReceiveDt between :beginDt and :endDt and "
            + "b.bilRctAmt between :beginAmount and :endAmount and b.bilCashEntryId.bilEntryDt = c.bilEntryDt "
            + "and b.bilCashEntryId.bilEntryNbr = c.bilEntryNbr and b.bilCashEntryId.userId = c.userId and b.bilAgtActNbr != :bilAgtActNbr and "
            + "b.bilCashEntryId.bilEntrySeqNbr = c.bilEntrySeqNbr and c.bilCshEtrMthCd in :bilCashEntryList and "
            + "b.bilCashEntryId.userId between :fromUserId and :toUserId and ((b.bilPmtOrderId between :fromPaymentId and :toPaymentId) "
            + "or(b.bilPmtOrderId = '' and b.bilRctId between :fromPaymentId and :toPaymentId)) and b.bilCashEntryId.bilEntryDt = d.bilCashEntryTotId.bilEntryDt "
            + "and b.bilCashEntryId.bilEntryNbr = d.bilCashEntryTotId.bilEntryNbr and b.bilCashEntryId.userId = d.bilCashEntryTotId.userId and d.currencyCd = :currency "
            + " order by b.insertedRowTs desc ")
    public Page<BilCashEntry> getAllPaymentsByAgencyWithCurrency(@Param("beginDt") ZonedDateTime beginDt,
            @Param("endDt") ZonedDateTime endDt, @Param("beginAmount") double beginAmount,
            @Param("endAmount") double endAmount, @Param("bilCashEntryList") List<Character> bilCashEntryList,
            @Param("bilAgtActNbr") String bilAgtActNbr, @Param("fromUserId") String fromUserId,
            @Param("toUserId") String toUserId, @Param("fromPaymentId") String fromPaymentId,
            @Param("toPaymentId") String toPaymentId, @Param("currency") String currency, Pageable pageable);

    @Query(value = "select b from BilCashEntry b where b.bilCashEntryId.bilEntryDt = :bilEntryDt and "
            + "b.bilCashEntryId.bilEntryNbr = :bilEntryNumber and b.bilCashEntryId.userId = :userId ")
    public List<BilCashEntry> findPaymentsByEntryDtAndEntryNumber(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNumber") String bilEntryNumber, @Param("userId") String userId);
    
    @Query(value = "select b from BilCashEntry b where b.bilCashEntryId.bilEntryDt = :bilEntryDt and b.bilCashEntryId.bilEntryNbr = :bilEntryNumber "
            + "and b.bilCashEntryId.userId = :userId order by b.bilCashEntryId.bilEntrySeqNbr")
    public List<BilCashEntry> findBilCashEntry(@Param("bilEntryDt") ZonedDateTime bilEntryDt,
            @Param("bilEntryNumber") String bilEntryNumber, @Param("userId") String userId);

}
