package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilInvTtyAct;
import billing.data.entity.id.BilInvTtyActId;

public interface BilInvTtyActRepository extends JpaRepository<BilInvTtyAct, BilInvTtyActId> {

    @Query(value = "select b from BilInvTtyAct b where b.bilInvTtyActId.bilTchKeyId = :bilTchKeyId and b.bilInvTtyActId.bilTchKeyTypCd = :bilTchKeyTypCd and b.bilDueDt = "
            + ":bilDueDt and b.bilInvTtyActId.biaRcdTypeCd = :biaRcdTypeCd")
    public BilInvTtyAct getRow(@Param("bilTchKeyId") String bilTchKeyId, @Param("bilTchKeyTypCd") char bilTchKeyTypCd,
            @Param("bilDueDt") ZonedDateTime bilDueDt, @Param("biaRcdTypeCd") String biaRcdTypeCd);

    @Modifying
    @Query(value = "update BilInvTtyAct b SET b.bilSttReconInd = :bilSttReconInd, b.bilSttReconAmt = b.bilMinDueAmt where b.bilInvTtyActId.bilTchKeyId = :bilTchKeyId and "
            + "b.bilInvTtyActId.bilTchKeyTypCd = :bilTchKeyTypCd and b.bilDueDt = :bilDueDt and b.bilInvTtyActId.biaRcdTypeCd = :biaRcdTypeCd")
    public void updateRow(@Param("bilSttReconInd") char bilSttReconInd, @Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilDueDt") ZonedDateTime bilDueDt,
            @Param("biaRcdTypeCd") String biaRcdTypeCd);

    @Query(value = "select SUM(b.bilMinDueAmt - b.bilSttReconAmt) from BilInvTtyAct b where b.bilInvTtyActId.bilTchKeyId = :bilTchKeyId and b.bilInvTtyActId.bilTchKeyTypCd = :bilTchKeyTypCd and "
            + "b.bilInvTtyActId.biaRcdTypeCd = :biaRcdTypeCd")
    public Double getStatementDueAmount(@Param("bilTchKeyId") String accountId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("biaRcdTypeCd") String biaRcdTypeCd);

    @Query(value = "select SUM(b.bilMinDueAmt - b.bilSttReconAmt) from BilInvTtyAct b where b.bilInvTtyActId.bilTchKeyId = :bilTchKeyId and b.bilInvTtyActId.bilTchKeyTypCd = :bilTchKeyTypCd and "
            + "b.bilInvTtyActId.biaRcdTypeCd = :biaRcdTypeCd and b.bilDueDt = :bilDueDt")
    public Double getStatementDueAmount(@Param("bilTchKeyId") String accountId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("biaRcdTypeCd") String biaRcdTypeCd,
            @Param("bilDueDt") ZonedDateTime bilDueDt);
    
    @Query(value = "select SUM(b.bilMinNetAmt - b.bilSttReconAmt) from BilInvTtyAct b where b.bilInvTtyActId.bilTchKeyId = :bilTchKeyId and b.bilInvTtyActId.bilTchKeyTypCd = :bilTchKeyTypCd and "
            + "b.bilInvTtyActId.biaRcdTypeCd = :biaRcdTypeCd and b.bilDueDt = :bilDueDt")
    public Double getAgencyStatementDueAmount(@Param("bilTchKeyId") String accountId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("biaRcdTypeCd") String biaRcdTypeCd,
            @Param("bilDueDt") ZonedDateTime bilDueDt);

    @Modifying
    @Query(value = "update BilInvTtyAct b set b.bilSttReconInd = :reconciliationIndicator, b.biaLastAcyDt = :lastAcyDate where b.bilInvTtyActId.bilTchKeyId = :technicalKey and b.bilInvTtyActId.bilTchKeyTypCd = :keyTypeCode and "
            + "b.bilInvTtyActId.bilInvDt = :invoiceDate and b.bilInvTtyActId.biaRcdTypeCd = :receiptTypeCode")
    public void updateStatementLevelRow(@Param("technicalKey") String technicalKey,
            @Param("keyTypeCode") char keyTypeCode, @Param("invoiceDate") ZonedDateTime invoiceDate,
            @Param("receiptTypeCode") String receiptTypeCode,
            @Param("reconciliationIndicator") char reconciliationIndicator,
            @Param("lastAcyDate") ZonedDateTime lastAcyDate);

    @Query(value = "select SUM(b.bilMinDueAmt - b.bilSttSvgAmt) from BilInvTtyAct b where b.bilInvTtyActId.bilTchKeyId = :bilTchKeyId and b.bilInvTtyActId.bilTchKeyTypCd = :bilTchKeyTypCd and "
            + "b.bilInvTtyActId.biaRcdTypeCd = :biaRcdTypeCd")
    public Double getReverseDueAmount(@Param("bilTchKeyId") String accountId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("biaRcdTypeCd") String biaRcdTypeCd);

    @Query(value = "select SUM(b.bilMinDueAmt - b.bilSttSvgAmt) from BilInvTtyAct b where b.bilInvTtyActId.bilTchKeyId = :bilTchKeyId and b.bilInvTtyActId.bilTchKeyTypCd = :bilTchKeyTypCd and "
            + "b.bilInvTtyActId.biaRcdTypeCd = :biaRcdTypeCd and b.bilDueDt = :bilDueDt")
    public Double getReverseDueAmount(@Param("bilTchKeyId") String accountId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("biaRcdTypeCd") String biaRcdTypeCd,
            @Param("bilDueDt") ZonedDateTime bilDueDt);

    @Query(value = "select count(b) from BilInvTtyAct b where b.bilInvTtyActId.bilTchKeyId = :bilTchKeyId and b.bilInvTtyActId.bilTchKeyTypCd = :bilTchKeyTypCd  and "
            + "b.bilInvTtyActId.bilAccountId = :bilAccountId and b.bilInvTtyActId.bilInvDt = :bilInvDt")
    public Integer getIfInvoicedToday(@Param("bilTchKeyId") String accountId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilAccountId") String bilAccountId,
            @Param("bilInvDt") ZonedDateTime bilInvDt);
    
    @Query(value = "select b from BilInvTtyAct b where b.bilInvTtyActId.bilTchKeyId = :bilTchKeyId and b.bilInvTtyActId.bilTchKeyTypCd = :bilTchKeyTypCd and "
            + "b.bilInvTtyActId.bilAccountId = :bilAccountId and b.bilDueDt = :bilDueDt and b.bilInvTtyActId.biaRcdTypeCd = :biaRcdTypeCd"
            + " and b.bilSttReconInd NOT IN :reconciliationIndicatorList")
    public BilInvTtyAct getRowBuDuDate(@Param("bilTchKeyId") String bilTchKeyId, @Param("bilTchKeyTypCd") char bilTchKeyTypCd,
            @Param("bilAccountId") String bilAccountId, @Param("bilDueDt") ZonedDateTime bilDueDt, @Param("biaRcdTypeCd") String biaRcdTypeCd,
            @Param("reconciliationIndicatorList") List<Character> reconciliationIndicatorList);
    
    @Query(value = "select b from BilInvTtyAct b where b.bilInvTtyActId.bilTchKeyId = :bilTchKeyId and b.bilInvTtyActId.bilTchKeyTypCd = :bilTchKeyTypCd and b.bilDueDt = "
            + ":bilDueDt and b.bilInvTtyActId.biaRcdTypeCd = :biaRcdTypeCd")
    public BilInvTtyAct findByTechnicalKeyAndDueDate(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilDueDt") ZonedDateTime bilDueDt,
            @Param("biaRcdTypeCd") String biaRcdTypeCd);
    
    @Query(value = "select SUM(b.bilMinNetAmt), SUM(b.bilSttReconAmt) from BilInvTtyAct b where b.bilInvTtyActId.bilTchKeyId = :bilTchKeyId and b.bilInvTtyActId.bilTchKeyTypCd = :bilTchKeyTypCd and "
            + "b.bilInvTtyActId.biaRcdTypeCd = :biaRcdTypeCd")
    public List<Object[]> findMinNetAndReconciliationAmount(@Param("bilTchKeyId") String accountId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("biaRcdTypeCd") String biaRcdTypeCd);
}
