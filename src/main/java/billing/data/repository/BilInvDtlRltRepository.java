package billing.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilInvDtlRlt;
import billing.data.entity.id.BilInvDtlRltId;

public interface BilInvDtlRltRepository extends JpaRepository<BilInvDtlRlt, BilInvDtlRltId> {

    @Query(value = "select b from BilInvDtlRlt b where b.bilInvDtlRltId.bilAccountId = :bilAccountId "
            + "and b.bilInvDtlRltId.bilInvEntryDt = :invoiceEntryDate and b.policyId = :policyId "
            + "and b.bilSeqNbr = :bilSequenceNumber")
    public List<BilInvDtlRlt> fetchCommissionPersent(@Param("bilAccountId") String bilAccountId,
            @Param("invoiceEntryDate") ZonedDateTime invoiceEntryDate, @Param("policyId") String policyId,
            @Param("bilSequenceNumber") short bilSequenceNumber);

    @Query(value = "select b from BilInvDtlRlt b where b.bilInvDtlRltId.bilTchKeyId = :bilTchKeyId "
            + " and b.bilInvDtlRltId.bilTchKeyTypCd = :bilTchKeyTypCd and b.bilInvDtlRltId.bilAccountId = :bilAccountId "
            + " and b.bilInvDtlRltId.bilInvEntryDt = :bilInvEntryDt and b.bilInvDtlRltId.bilInvSeqNbr <> :bilInvSeqNbr "
            + " and b.policyId = :policyId and b.polEffectiveDt = :polEffectiveDt "
            + " and b.bilSeqNbr = :bilSequenceNumber")
    public List<BilInvDtlRlt> fetchBilInvDtlRltRows(@Param("bilTchKeyId") String bilTchKeyId,
            @Param("bilTchKeyTypCd") char bilTchKeyTypCd, @Param("bilAccountId") String bilAccountId,
            @Param("bilInvEntryDt") ZonedDateTime bilInvEntryDt, @Param("bilInvSeqNbr") short bilInvSeqNbr,
            @Param("policyId") String policyId, @Param("polEffectiveDt") ZonedDateTime polEffectiveDt,
            @Param("bilSequenceNumber") short bilSequenceNumber);

}
