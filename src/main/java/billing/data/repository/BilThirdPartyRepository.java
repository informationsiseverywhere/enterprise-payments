package billing.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import billing.data.entity.BilThirdParty;

public interface BilThirdPartyRepository extends JpaRepository<BilThirdParty, String> {

    public BilThirdParty findByBilTtyNbr(String bilTtyNbr);

    public BilThirdParty findByBillThirdPartyId(String billThirdPartyId);

}
