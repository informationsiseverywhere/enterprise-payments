package billing.data.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import billing.data.entity.BilCrcrdArspDes;
import billing.data.entity.id.BilCrcrdArspDesId;

public interface BilCrcrdArspDesRepository extends CrudRepository<BilCrcrdArspDes, BilCrcrdArspDesId> {

    @Query(value = "select COUNT(b) from BilCrcrdArspDes b where b.bilCrcrdArspDesId.bilCrcrdArspCd = :bilCrcrdArspCd AND b.bilCrcrdArspDesId.bilCrcrdArspTyc <> '  '")
    public Integer getByBilCrcrdArspCd(@Param("bilCrcrdArspCd") char bilCrcrdArspCd);

}
