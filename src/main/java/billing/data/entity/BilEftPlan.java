package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "BIL_EFT_PLAN")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "bilEftPlan")
public class BilEftPlan implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "BIL_COLLECTION_PLN", length = 4, nullable = false)
    private String bilCollectionPln = SEPARATOR_BLANK;

    @Column(name = "BEP_PLAN_NM", length = 30, nullable = false)
    private String bepPlanNm = SEPARATOR_BLANK;

    @Column(name = "BEP_INV_DPL_NBR", length = 5, nullable = false)
    private short bepInvDplNbr;

    @Column(name = "BEP_TAPE_DPL_NBR", length = 5, nullable = false)
    private short bepTapeDplNbr;

    @Column(name = "BEP_PMT_DPL_NBR", length = 5, nullable = false)
    private short bepPmtDplNbr;

    @Column(name = "BEP_NOTICE_IND", nullable = false)
    private char bepNoticeInd;

    @Column(name = "BEP_PEND_NBR", length = 5, nullable = false)
    private short bepPendNbr;

    @Column(name = "BEP_ADV_NOTICE_IND", nullable = false)
    private char bepAdvNoticeInd;

    @Column(name = "BEP_TOL_AMT", precision = 14, scale = 2, nullable = false)
    private double bepTolAmt;

    @Column(name = "BEP_RPS_MTH", length = 4, nullable = false)
    private String bepRpsMth = SEPARATOR_BLANK;

    @Column(name = "BEP_RPS_NBR", length = 5, nullable = false)
    private short bepRpsNbr;

    @Column(name = "BEP_NSF_NBR", length = 5, nullable = false)
    private short bepNsfNbr;

    @Column(name = "BEP_NSF_MO", length = 5, nullable = false)
    private short bepNsfMo;

    @Column(name = "BEP_ALT_COL_MTH", length = 3, nullable = false)
    private String bepAltColMth = SEPARATOR_BLANK;

    @Column(name = "BIL_FILE_TYPE_CD", length = 5, nullable = false)
    private short bilFileTypeCd;

    @Column(name = "BIL_BANK_CD", length = 3, nullable = false)
    private String bilBankCd = SEPARATOR_BLANK;

    @Column(name = "BEP_EFT_SCG_IND", nullable = false)
    private char bepEftScgInd;

    @Column(name = "BEP_EFT_SCG_AMT", precision = 14, scale = 2, nullable = false)
    private double bepEftScgAmt;

    @Column(name = "BIL_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEffDt;

    @Column(name = "BIL_EXP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilExpDt;

    @Column(name = "BIL_NEXT_PLAN_CD", length = 3, nullable = false)
    private String bilNextPlanCd = SEPARATOR_BLANK;

    public String getBilCollectionPln() {
        return bilCollectionPln;
    }

    public void setBilCollectionPln(String bilCollectionPln) {
        this.bilCollectionPln = bilCollectionPln;
    }

    public String getBepPlanNm() {
        return bepPlanNm;
    }

    public void setBepPlanNm(String bepPlanNm) {
        this.bepPlanNm = bepPlanNm;
    }

    public short getBepInvDplNbr() {
        return bepInvDplNbr;
    }

    public void setBepInvDplNbr(short bepInvDplNbr) {
        this.bepInvDplNbr = bepInvDplNbr;
    }

    public short getBepTapeDplNbr() {
        return bepTapeDplNbr;
    }

    public void setBepTapeDplNbr(short bepTapeDplNbr) {
        this.bepTapeDplNbr = bepTapeDplNbr;
    }

    public short getBepPmtDplNbr() {
        return bepPmtDplNbr;
    }

    public void setBepPmtDplNbr(short bepPmtDplNbr) {
        this.bepPmtDplNbr = bepPmtDplNbr;
    }

    public char getBepNoticeInd() {
        return bepNoticeInd;
    }

    public void setBepNoticeInd(char bepNoticeInd) {
        this.bepNoticeInd = bepNoticeInd;
    }

    public short getBepPendNbr() {
        return bepPendNbr;
    }

    public void setBepPendNbr(short bepPendNbr) {
        this.bepPendNbr = bepPendNbr;
    }

    public char getBepAdvNoticeInd() {
        return bepAdvNoticeInd;
    }

    public void setBepAdvNoticeInd(char bepAdvNoticeInd) {
        this.bepAdvNoticeInd = bepAdvNoticeInd;
    }

    public double getBepTolAmt() {
        return bepTolAmt;
    }

    public void setBepTolAmt(double bepTolAmt) {
        this.bepTolAmt = bepTolAmt;
    }

    public String getBepRpsMth() {
        return bepRpsMth;
    }

    public void setBepRpsMth(String bepRpsMth) {
        this.bepRpsMth = bepRpsMth;
    }

    public short getBepRpsNbr() {
        return bepRpsNbr;
    }

    public void setBepRpsNbr(short bepRpsNbr) {
        this.bepRpsNbr = bepRpsNbr;
    }

    public short getBepNsfNbr() {
        return bepNsfNbr;
    }

    public void setBepNsfNbr(short bepNsfNbr) {
        this.bepNsfNbr = bepNsfNbr;
    }

    public short getBepNsfMo() {
        return bepNsfMo;
    }

    public void setBepNsfMo(short bepNsfMo) {
        this.bepNsfMo = bepNsfMo;
    }

    public String getBepAltColMth() {
        return bepAltColMth;
    }

    public void setBepAltColMth(String bepAltColMth) {
        this.bepAltColMth = bepAltColMth;
    }

    public short getBilFileTypeCd() {
        return bilFileTypeCd;
    }

    public void setBilFileTypeCd(short bilFileTypeCd) {
        this.bilFileTypeCd = bilFileTypeCd;
    }

    public String getBilBankCd() {
        return bilBankCd;
    }

    public void setBilBankCd(String bilBankCd) {
        this.bilBankCd = bilBankCd;
    }

    public char getBepEftScgInd() {
        return bepEftScgInd;
    }

    public void setBepEftScgInd(char bepEftScgInd) {
        this.bepEftScgInd = bepEftScgInd;
    }

    public double getBepEftScgAmt() {
        return bepEftScgAmt;
    }

    public void setBepEftScgAmt(double bepEftScgAmt) {
        this.bepEftScgAmt = bepEftScgAmt;
    }

    public ZonedDateTime getBilEffDt() {
        return bilEffDt;
    }

    public void setBilEffDt(ZonedDateTime bilEffDt) {
        this.bilEffDt = bilEffDt;
    }

    public ZonedDateTime getBilExpDt() {
        return bilExpDt;
    }

    public void setBilExpDt(ZonedDateTime bilExpDt) {
        this.bilExpDt = bilExpDt;
    }

    public String getBilNextPlanCd() {
        return bilNextPlanCd;
    }

    public void setBilNextPlanCd(String bilNextPlanCd) {
        this.bilNextPlanCd = bilNextPlanCd;
    }

}
