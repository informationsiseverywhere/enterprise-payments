package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilInvPolId;

@Entity
@Table(name = "BIL_INV_POL")
public class BilInvPol implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilInvPolId bilInvPolId;

    @Column(name = "BIL_ADDITIONAL_ID", length = 20, nullable = false)
    private String bilAdditionalId = SEPARATOR_BLANK;

    @Column(name = "POL_NBR", length = 25, nullable = false)
    private String polNbr = SEPARATOR_BLANK;

    @Column(name = "POL_SYMBOL_CD", length = 3)
    private String polSymbolCd = SEPARATOR_BLANK;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNbr = SEPARATOR_BLANK;

    @Column(name = "POL_EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime polEffectiveDt;

    @Column(name = "BIL_PLAN_CD", length = 4, nullable = false)
    private String bilPlanCd = SEPARATOR_BLANK;

    @Column(name = "BIL_POL_STATUS_CD", nullable = false)
    private char bilPolStatusCd = BLANK_CHAR;

    @Column(name = "BIL_PREV_DUE_AMT", precision = 14, scale = 2, nullable = false)
    private double bilPrevDueAmt = DECIMAL_ZERO;

    @Column(name = "BIL_MIN_DUE_AMT", precision = 14, scale = 2, nullable = false)
    private double bilMinDueAmt = DECIMAL_ZERO;

    @Column(name = "BIL_NEW_BAL_AMT", precision = 14, scale = 2, nullable = false)
    private double bilNewBalAmt = DECIMAL_ZERO;

    @Column(name = "BIL_STT_RECON_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttReconAmt = DECIMAL_ZERO;

    @Column(name = "BIP_AGT_NBR", length = 20, nullable = false)
    private String bipAgtNbr = SEPARATOR_BLANK;

    @Column(name = "BIP_AGT_SUB_CD", length = 2, nullable = false)
    private String bipAgtSubCd = SEPARATOR_BLANK;

    @Column(name = "BIP_MARKET_ARM_CD", length = 4, nullable = false)
    private String bipMarketArmCd = SEPARATOR_BLANK;

    @Column(name = "BIP_POL_FQY_DES", length = 22, nullable = false)
    private String bipPolFqyDes = SEPARATOR_BLANK;

    @Column(name = "BIP_INS_TYPE_CD", length = 3, nullable = false)
    private String bipInsTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIP_ADDITIONAL_DES", length = 45, nullable = false)
    private String bipAdditionalDes = SEPARATOR_BLANK;

    public BilInvPolId getBilInvPolId() {
        return bilInvPolId;
    }

    public void setBilInvPolId(BilInvPolId bilInvPolId) {
        this.bilInvPolId = bilInvPolId;
    }

    public String getBilAdditionalId() {
        return bilAdditionalId;
    }

    public void setBilAdditionalId(String bilAdditionalId) {
        this.bilAdditionalId = bilAdditionalId;
    }

    public String getPolNbr() {
        return polNbr;
    }

    public void setPolNbr(String polNbr) {
        this.polNbr = polNbr;
    }

    public String getPolSymbolCd() {
        return polSymbolCd;
    }

    public void setPolSymbolCd(String polSymbolCd) {
        this.polSymbolCd = polSymbolCd;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public ZonedDateTime getPolEffectiveDt() {
        return polEffectiveDt;
    }

    public void setPolEffectiveDt(ZonedDateTime polEffectiveDt) {
        this.polEffectiveDt = polEffectiveDt;
    }

    public String getBilPlanCd() {
        return bilPlanCd;
    }

    public void setBilPlanCd(String bilPlanCd) {
        this.bilPlanCd = bilPlanCd;
    }

    public char getBilPolStatusCd() {
        return bilPolStatusCd;
    }

    public void setBilPolStatusCd(char bilPolStatusCd) {
        this.bilPolStatusCd = bilPolStatusCd;
    }

    public double getBilPrevDueAmt() {
        return bilPrevDueAmt;
    }

    public void setBilPrevDueAmt(double bilPrevDueAmt) {
        this.bilPrevDueAmt = bilPrevDueAmt;
    }

    public double getBilMinDueAmt() {
        return bilMinDueAmt;
    }

    public void setBilMinDueAmt(double bilMinDueAmt) {
        this.bilMinDueAmt = bilMinDueAmt;
    }

    public double getBilNewBalAmt() {
        return bilNewBalAmt;
    }

    public void setBilNewBalAmt(double bilNewBalAmt) {
        this.bilNewBalAmt = bilNewBalAmt;
    }

    public double getBilSttReconAmt() {
        return bilSttReconAmt;
    }

    public void setBilSttReconAmt(double bilSttReconAmt) {
        this.bilSttReconAmt = bilSttReconAmt;
    }

    public String getBipAgtNbr() {
        return bipAgtNbr;
    }

    public void setBipAgtNbr(String bipAgtNbr) {
        this.bipAgtNbr = bipAgtNbr;
    }

    public String getBipAgtSubCd() {
        return bipAgtSubCd;
    }

    public void setBipAgtSubCd(String bipAgtSubCd) {
        this.bipAgtSubCd = bipAgtSubCd;
    }

    public String getBipMarketArmCd() {
        return bipMarketArmCd;
    }

    public void setBipMarketArmCd(String bipMarketArmCd) {
        this.bipMarketArmCd = bipMarketArmCd;
    }

    public String getBipPolFqyDes() {
        return bipPolFqyDes;
    }

    public void setBipPolFqyDes(String bipPolFqyDes) {
        this.bipPolFqyDes = bipPolFqyDes;
    }

    public String getBipInsTypeCd() {
        return bipInsTypeCd;
    }

    public void setBipInsTypeCd(String bipInsTypeCd) {
        this.bipInsTypeCd = bipInsTypeCd;
    }

    public String getBipAdditionalDes() {
        return bipAdditionalDes;
    }

    public void setBipAdditionalDes(String bipAdditionalDes) {
        this.bipAdditionalDes = bipAdditionalDes;
    }

}
