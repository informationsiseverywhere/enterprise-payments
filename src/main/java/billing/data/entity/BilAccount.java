package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "BIL_ACCOUNT")
public class BilAccount implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String accountId = SEPARATOR_BLANK;

    @Column(name = "BIL_ADDITIONAL_ID", length = 20, nullable = false)
    private String additionalId = SEPARATOR_BLANK;

    @Column(name = "BIL_TYPE_CD", length = 2, nullable = false)
    private String billTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_CLASS_CD", length = 3, nullable = false)
    private String billClassCd = SEPARATOR_BLANK;

    @Column(name = "BAT_STATUS_CD", nullable = false)
    private char status = BLANK_CHAR;

    @Column(name = "BAT_PAY_CLT_ID", length = 20, nullable = false)
    private String payorClientId = SEPARATOR_BLANK;

    @Column(name = "BAT_PAY_ADR_SEQ")
    private short addressSequenceNumber = SHORT_ZERO;

    @Column(name = "BAT_START_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime startDueDate;

    @Column(name = "BAT_LAST_DAY_IND", nullable = false)
    private char lastDayIndicator = BLANK_CHAR;

    @Column(name = "BIL_START_RFR_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime startReferenceDate;

    @Column(name = "BIL_RFR_LST_DAY", nullable = false)
    private char referenceLastDay = BLANK_CHAR;

    @Column(name = "BIL_SUS_FU_REA_CD", length = 3, nullable = false)
    private String suspendedFullReasonCd = SEPARATOR_BLANK;

    @Column(name = "BAT_PREV_BAL_AMT", precision = 14, scale = 2, nullable = false)
    private double previousBalanceAmount = DECIMAL_ZERO;

    @Column(name = "BIL_COLLECTION_MTH", length = 3, nullable = false)
    private String collectionMethod = SEPARATOR_BLANK;

    @Column(name = "BIL_COLLECTION_PLN", length = 4, nullable = false)
    private String collectionPlan = SEPARATOR_BLANK;

    @Column(name = "BIL_DUE_DT_DPL_NBR")
    private short dueDateDplNumber = SHORT_ZERO;

    @Column(name = "BIL_REPORT_LVL_CD", length = 8, nullable = false)
    private String reportingLevelCode = SEPARATOR_BLANK;

    @Column(name = "BIL_SUB_GROUP_CD", length = 20, nullable = false)
    private String subGroupCd = SEPARATOR_BLANK;

    @Column(name = "BAT_RESCHEDULE_IND", nullable = false)
    private char rescheduleIndicator = BLANK_CHAR;

    @Column(name = "CURRENCY_CD", length = 4)
    private String currencyCode = SEPARATOR_BLANK;

    @Column(name = "BIL_ACCOUNT_NBR", length = 30, nullable = false)
    private String accountNumber = SEPARATOR_BLANK;

    @Column(name = "BIL_TTY_NBR", length = 20, nullable = false)
    private String thirdPartyIdentifier = SEPARATOR_BLANK;

    @Column(name = "BIL_AGT_ACT_NBR", length = 20, nullable = false)
    private String agentAccountNumber = SEPARATOR_BLANK;

    @Column(name = "BIL_LOK_USER_ID", length = 8, nullable = false)
    private String lockUserId = SEPARATOR_BLANK;

    @Column(name = "ISSUE_NODE_ID", length = 15, nullable = false)
    private String issueNodeId = SEPARATOR_BLANK;

    @Column(name = "CLIENT_ID", length = 20, nullable = false)
    private String clientId = SEPARATOR_BLANK;

    @Column(name = "BIL_FUNCTION_NM", length = 8, nullable = false)
    private String lockFunctionName = SEPARATOR_BLANK;

    @Column(name = "BIL_IN_FLIGHT_IND", nullable = false)
    private char lockInFlightIndicator = BLANK_CHAR;

    @Column(name = "BIL_USER_LOK_NBR")
    private short userLockNbr = SHORT_ZERO;

    @Column(name = "BIL_LOK_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime lockTimeStamp;

    @Column(name = "BAT_CASH_STATUS_CD", nullable = false)
    private char cashStatusCd = BLANK_CHAR;

    @Column(name = "BAT_DSB_PYE_CD", nullable = false)
    private char disbPayeeCd = BLANK_CHAR;

    @Column(name = "BIL_PRESENTMENT_CD", length = 3, nullable = false)
    private String presentmentMethod = SEPARATOR_BLANK;

    @Column(name = "BIL_SUS_DSB_REA_CD", length = 3, nullable = false)
    private String suspendDisbReasonCd = SEPARATOR_BLANK;

    @Column(name = "APP_MIGRATION_CD", length = 4, nullable = false)
    private String appMigrationCd = SEPARATOR_BLANK;

    @Column(name = "BIL_START_DED_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime startDeductionDate;

    @Column(name = "BIL_DED_LST_DAY_IND", nullable = false)
    private char deductionLastDayIndicator = BLANK_CHAR;

    @Column(name = "BIL_START_DED_RFR_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime startDeductionReferenceDate;

    @Column(name = "BIL_DED_RFR_LST_DAY_IND", nullable = false)
    private char deductionReferenceLastDayIndicator = BLANK_CHAR;

    @Column(name = "BIL_SEA_XCL_CD", length = 10, nullable = false)
    private String seasionalExclusionCode = SEPARATOR_BLANK;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAdditionalId() {
        return additionalId;
    }

    public void setAdditionalId(String additionalId) {
        this.additionalId = additionalId;
    }

    public String getBillTypeCd() {
        return billTypeCd;
    }

    public void setBillTypeCd(String billTypeCd) {
        this.billTypeCd = billTypeCd;
    }

    public String getBillClassCd() {
        return billClassCd;
    }

    public void setBillClassCd(String billClassCd) {
        this.billClassCd = billClassCd;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public String getPayorClientId() {
        return payorClientId;
    }

    public void setPayorClientId(String payorClientId) {
        this.payorClientId = payorClientId;
    }

    public short getAddressSequenceNumber() {
        return addressSequenceNumber;
    }

    public void setAddressSequenceNumber(short addressSequenceNumber) {
        this.addressSequenceNumber = addressSequenceNumber;
    }

    public ZonedDateTime getStartDueDate() {
        return startDueDate;
    }

    public void setStartDueDate(ZonedDateTime startDueDate) {
        this.startDueDate = startDueDate;
    }

    public char getLastDayIndicator() {
        return lastDayIndicator;
    }

    public void setLastDayIndicator(char lastDayIndicator) {
        this.lastDayIndicator = lastDayIndicator;
    }

    public ZonedDateTime getStartReferenceDate() {
        return startReferenceDate;
    }

    public void setStartReferenceDate(ZonedDateTime startReferenceDate) {
        this.startReferenceDate = startReferenceDate;
    }

    public char getReferenceLastDay() {
        return referenceLastDay;
    }

    public void setReferenceLastDay(char referenceLastDay) {
        this.referenceLastDay = referenceLastDay;
    }

    public String getSuspendedFullReasonCd() {
        return suspendedFullReasonCd;
    }

    public void setSuspendedFullReasonCd(String suspendedFullReasonCd) {
        this.suspendedFullReasonCd = suspendedFullReasonCd;
    }

    public double getPreviousBalanceAmount() {
        return previousBalanceAmount;
    }

    public void setPreviousBalanceAmount(double previousBalanceAmount) {
        this.previousBalanceAmount = previousBalanceAmount;
    }

    public String getCollectionMethod() {
        return collectionMethod;
    }

    public void setCollectionMethod(String collectionMethod) {
        this.collectionMethod = collectionMethod;
    }

    public String getCollectionPlan() {
        return collectionPlan;
    }

    public void setCollectionPlan(String collectionPlan) {
        this.collectionPlan = collectionPlan;
    }

    public short getDueDateDplNumber() {
        return dueDateDplNumber;
    }

    public void setDueDateDplNumber(short dueDateDplNumber) {
        this.dueDateDplNumber = dueDateDplNumber;
    }

    public String getReportingLevelCode() {
        return reportingLevelCode;
    }

    public void setReportingLevelCode(String reportingLevelCode) {
        this.reportingLevelCode = reportingLevelCode;
    }

    public String getSubGroupCd() {
        return subGroupCd;
    }

    public void setSubGroupCd(String subGroupCd) {
        this.subGroupCd = subGroupCd;
    }

    public char getRescheduleIndicator() {
        return rescheduleIndicator;
    }

    public void setRescheduleIndicator(char rescheduleIndicator) {
        this.rescheduleIndicator = rescheduleIndicator;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getThirdPartyIdentifier() {
        return thirdPartyIdentifier;
    }

    public void setThirdPartyIdentifier(String thirdPartyIdentifier) {
        this.thirdPartyIdentifier = thirdPartyIdentifier;
    }

    public String getAgentAccountNumber() {
        return agentAccountNumber;
    }

    public void setAgentAccountNumber(String agentAccountNumber) {
        this.agentAccountNumber = agentAccountNumber;
    }

    public String getLockUserId() {
        return lockUserId;
    }

    public void setLockUserId(String lockUserId) {
        this.lockUserId = lockUserId;
    }

    public String getIssueNodeId() {
        return issueNodeId;
    }

    public void setIssueNodeId(String issueNodeId) {
        this.issueNodeId = issueNodeId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getLockFunctionName() {
        return lockFunctionName;
    }

    public void setLockFunctionName(String lockFunctionName) {
        this.lockFunctionName = lockFunctionName;
    }

    public char getLockInFlightIndicator() {
        return lockInFlightIndicator;
    }

    public void setLockInFlightIndicator(char lockInFlightIndicator) {
        this.lockInFlightIndicator = lockInFlightIndicator;
    }

    public short getUserLockNbr() {
        return userLockNbr;
    }

    public void setUserLockNbr(short userLockNbr) {
        this.userLockNbr = userLockNbr;
    }

    public ZonedDateTime getLockTimeStamp() {
        return lockTimeStamp;
    }

    public void setLockTimeStamp(ZonedDateTime lockTimeStamp) {
        this.lockTimeStamp = lockTimeStamp;
    }

    public char getCashStatusCd() {
        return cashStatusCd;
    }

    public void setCashStatusCd(char cashStatusCd) {
        this.cashStatusCd = cashStatusCd;
    }

    public char getDisbPayeeCd() {
        return disbPayeeCd;
    }

    public void setDisbPayeeCd(char disbPayeeCd) {
        this.disbPayeeCd = disbPayeeCd;
    }

    public String getPresentmentMethod() {
        return presentmentMethod;
    }

    public void setPresentmentMethod(String presentmentMethod) {
        this.presentmentMethod = presentmentMethod;
    }

    public String getSuspendDisbReasonCd() {
        return suspendDisbReasonCd;
    }

    public void setSuspendDisbReasonCd(String suspendDisbReasonCd) {
        this.suspendDisbReasonCd = suspendDisbReasonCd;
    }

    public String getAppMigrationCd() {
        return appMigrationCd;
    }

    public void setAppMigrationCd(String appMigrationCd) {
        this.appMigrationCd = appMigrationCd;
    }

    public ZonedDateTime getStartDeductionDate() {
        return startDeductionDate;
    }

    public void setStartDeductionDate(ZonedDateTime startDeductionDate) {
        this.startDeductionDate = startDeductionDate;
    }

    public char getDeductionLastDayIndicator() {
        return deductionLastDayIndicator;
    }

    public void setDeductionLastDayIndicator(char deductionLastDayIndicator) {
        this.deductionLastDayIndicator = deductionLastDayIndicator;
    }

    public ZonedDateTime getStartDeductionReferenceDate() {
        return startDeductionReferenceDate;
    }

    public void setStartDeductionReferenceDate(ZonedDateTime startDeductionReferenceDate) {
        this.startDeductionReferenceDate = startDeductionReferenceDate;
    }

    public char getDeductionReferenceLastDayIndicator() {
        return deductionReferenceLastDayIndicator;
    }

    public void setDeductionReferenceLastDayIndicator(char deductionReferenceLastDayIndicator) {
        this.deductionReferenceLastDayIndicator = deductionReferenceLastDayIndicator;
    }

    public String getSeasionalExclusionCode() {
        return seasionalExclusionCode;
    }

    public void setSeasionalExclusionCode(String seasionalExclusionCode) {
        this.seasionalExclusionCode = seasionalExclusionCode;
    }

}