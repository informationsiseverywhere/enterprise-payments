package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "BIL_AGENT")
public class BilAgent implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "BIL_AGT_ACT_ID", length = 8, nullable = false)
    private String bilAgtActId = SEPARATOR_BLANK;

    @Column(name = "BIL_AGT_ACT_NBR", length = 20, nullable = false)
    private String bilAgtActNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_TYPE_CD", length = 2, nullable = false)
    private String bilTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_CLASS_CD", length = 8, nullable = false)
    private String bilClassCd = SEPARATOR_BLANK;

    @Column(name = "BAG_STATUS_CD", nullable = false)
    private char bagStatusCd;

    @Column(name = "BAG_AGT_PAY_CLT_ID", length = 20, nullable = false)
    private String bagAgtPayCltId = SEPARATOR_BLANK;

    @Column(name = "BAG_AGT_PAY_ADR", length = 5, nullable = false)
    private short bagAgtPayAdr;

    @Column(name = "BAG_START_ACT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bagStartActDt;

    @Column(name = "BAG_ACT_LST_DAY", nullable = false)
    private char bagActLstDay;

    @Column(name = "BAG_START_STT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bagStartSttDt;

    @Column(name = "BAG_STT_LST_DAY", nullable = false)
    private char bagSttLstDay;

    @Column(name = "BIL_START_RFR_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilStartRfrDt;

    @Column(name = "BIL_RFR_LST_DAY", nullable = false)
    private char bilRfrLstDay;

    @Column(name = "BIL_COLLECTION_MTH", length = 3, nullable = false)
    private String bilCollectionMth = SEPARATOR_BLANK;

    @Column(name = "BIL_COLLECTION_PLN", length = 4, nullable = false)
    private String bilCollectionPln = SEPARATOR_BLANK;

    @Column(name = "BIL_SUS_FU_REA_CD", length = 3, nullable = false)
    private String bilSusFuReaCd = SEPARATOR_BLANK;

    @Column(name = "BIL_LST_RECON_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilLstReconDt;

    @Column(name = "BIL_REPORT_LVL_CD", length = 8, nullable = false)
    private String bilReportLvlCd = SEPARATOR_BLANK;

    @Column(name = "CURRENCY_CD", length = 4, nullable = true)
    private String currencyCd;

    @Column(name = "BIL_LOK_USER_ID", length = 8, nullable = false)
    private String bilLokUserId = SEPARATOR_BLANK;

    @Column(name = "ISSUE_NODE_ID", length = 15, nullable = false)
    private String issueNodeId = SEPARATOR_BLANK;

    @Column(name = "CLIENT_ID", length = 20, nullable = false)
    private String clientId = SEPARATOR_BLANK;

    @Column(name = "BIL_FUNCTION_NM", length = 8, nullable = false)
    private String bilFunctionNm = SEPARATOR_BLANK;

    @Column(name = "BIL_IN_FLIGHT_IND", nullable = false)
    private char bilInFlightInd;

    @Column(name = "BIL_USER_LOK_NBR", length = 5, nullable = false)
    private short bilUserLokNbr;

    @Column(name = "BIL_LOK_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bilLokTs;

    @Column(name = "BIL_PRESENTMENT_CD", length = 3, nullable = false)
    private String bilPresentmentCd = SEPARATOR_BLANK;

    @Column(name = "APP_MIGRATION_CD", length = 4, nullable = false)
    private String appMigrationCd = SEPARATOR_BLANK;

    public String getBilAgtActId() {
        return bilAgtActId;
    }

    public void setBilAgtActId(String bilAgtActId) {
        this.bilAgtActId = bilAgtActId;
    }

    public String getBilAgtActNbr() {
        return bilAgtActNbr;
    }

    public void setBilAgtActNbr(String bilAgtActNbr) {
        this.bilAgtActNbr = bilAgtActNbr;
    }

    public String getBilTypeCd() {
        return bilTypeCd;
    }

    public void setBilTypeCd(String bilTypeCd) {
        this.bilTypeCd = bilTypeCd;
    }

    public String getBilClassCd() {
        return bilClassCd;
    }

    public void setBilClassCd(String bilClassCd) {
        this.bilClassCd = bilClassCd;
    }

    public char getBagStatusCd() {
        return bagStatusCd;
    }

    public void setBagStatusCd(char bagStatusCd) {
        this.bagStatusCd = bagStatusCd;
    }

    public String getBagAgtPayCltId() {
        return bagAgtPayCltId;
    }

    public void setBagAgtPayCltId(String bagAgtPayCltId) {
        this.bagAgtPayCltId = bagAgtPayCltId;
    }

    public short getBagAgtPayAdr() {
        return bagAgtPayAdr;
    }

    public void setBagAgtPayAdr(short bagAgtPayAdr) {
        this.bagAgtPayAdr = bagAgtPayAdr;
    }

    public ZonedDateTime getBagStartActDt() {
        return bagStartActDt;
    }

    public void setBagStartActDt(ZonedDateTime bagStartActDt) {
        this.bagStartActDt = bagStartActDt;
    }

    public char getBagActLstDay() {
        return bagActLstDay;
    }

    public void setBagActLstDay(char bagActLstDay) {
        this.bagActLstDay = bagActLstDay;
    }

    public ZonedDateTime getBagStartSttDt() {
        return bagStartSttDt;
    }

    public void setBagStartSttDt(ZonedDateTime bagStartSttDt) {
        this.bagStartSttDt = bagStartSttDt;
    }

    public char getBagSttLstDay() {
        return bagSttLstDay;
    }

    public void setBagSttLstDay(char bagSttLstDay) {
        this.bagSttLstDay = bagSttLstDay;
    }

    public ZonedDateTime getBilStartRfrDt() {
        return bilStartRfrDt;
    }

    public void setBilStartRfrDt(ZonedDateTime bilStartRfrDt) {
        this.bilStartRfrDt = bilStartRfrDt;
    }

    public char getBilRfrLstDay() {
        return bilRfrLstDay;
    }

    public void setBilRfrLstDay(char bilRfrLstDay) {
        this.bilRfrLstDay = bilRfrLstDay;
    }

    public String getBilCollectionMth() {
        return bilCollectionMth;
    }

    public void setBilCollectionMth(String bilCollectionMth) {
        this.bilCollectionMth = bilCollectionMth;
    }

    public String getBilCollectionPln() {
        return bilCollectionPln;
    }

    public void setBilCollectionPln(String bilCollectionPln) {
        this.bilCollectionPln = bilCollectionPln;
    }

    public String getBilSusFuReaCd() {
        return bilSusFuReaCd;
    }

    public void setBilSusFuReaCd(String bilSusFuReaCd) {
        this.bilSusFuReaCd = bilSusFuReaCd;
    }

    public ZonedDateTime getBilLstReconDt() {
        return bilLstReconDt;
    }

    public void setBilLstReconDt(ZonedDateTime bilLstReconDt) {
        this.bilLstReconDt = bilLstReconDt;
    }

    public String getBilReportLvlCd() {
        return bilReportLvlCd;
    }

    public void setBilReportLvlCd(String bilReportLvlCd) {
        this.bilReportLvlCd = bilReportLvlCd;
    }

    public String getCurrencyCd() {
        return currencyCd;
    }

    public void setCurrencyCd(String currencyCd) {
        this.currencyCd = currencyCd;
    }

    public String getBilLokUserId() {
        return bilLokUserId;
    }

    public void setBilLokUserId(String bilLokUserId) {
        this.bilLokUserId = bilLokUserId;
    }

    public String getIssueNodeId() {
        return issueNodeId;
    }

    public void setIssueNodeId(String issueNodeId) {
        this.issueNodeId = issueNodeId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getBilFunctionNm() {
        return bilFunctionNm;
    }

    public void setBilFunctionNm(String bilFunctionNm) {
        this.bilFunctionNm = bilFunctionNm;
    }

    public char getBilInFlightInd() {
        return bilInFlightInd;
    }

    public void setBilInFlightInd(char bilInFlightInd) {
        this.bilInFlightInd = bilInFlightInd;
    }

    public short getBilUserLokNbr() {
        return bilUserLokNbr;
    }

    public void setBilUserLokNbr(short bilUserLokNbr) {
        this.bilUserLokNbr = bilUserLokNbr;
    }

    public ZonedDateTime getBilLokTs() {
        return bilLokTs;
    }

    public void setBilLokTs(ZonedDateTime bilLokTs) {
        this.bilLokTs = bilLokTs;
    }

    public String getBilPresentmentCd() {
        return bilPresentmentCd;
    }

    public void setBilPresentmentCd(String bilPresentmentCd) {
        this.bilPresentmentCd = bilPresentmentCd;
    }

    public String getAppMigrationCd() {
        return appMigrationCd;
    }

    public void setAppMigrationCd(String appMigrationCd) {
        this.appMigrationCd = appMigrationCd;
    }

}
