package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilCashDspId;

@Entity
@Table(name = "BIL_CASH_DSP")
public final class BilCashDsp implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilCashDspId bilCashDspId;

    @Column(name = "POLICY_ID", length = 16, nullable = false)
    private String policyId = SEPARATOR_BLANK;

    @Column(name = "POL_SYMBOL_CD", length = 3, nullable = true)
    private String policySymbolCd = SEPARATOR_BLANK;

    @Column(name = "POL_NBR", length = 25, nullable = false)
    private String policyNumber = SEPARATOR_BLANK;

    @Column(name = "POL_EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime policyEffectiveDate;

    @Column(name = "BIL_PBL_ITEM_CD", length = 3, nullable = false)
    private String payableItemCd = SEPARATOR_BLANK;

    @Column(name = "BIL_SEQ_NBR")
    private short bilSequenceNumber;

    @Column(name = "BIL_SYS_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime systemDueDate;

    @Column(name = "BIL_ADJ_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime adjustmentDueDate;

    @Column(name = "BIL_INV_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime invoiceDate;

    @Column(name = "BIL_CRG_TYPE_CD", nullable = false)
    private char chargeTypeCd = BLANK_CHAR;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "BIL_DSP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime dspDate;

    @Column(name = "BIL_DSP_REASON_CD", length = 3, nullable = false)
    private String dspReasonCd = SEPARATOR_BLANK;

    @Column(name = "BIL_DSP_TYPE_CD", length = 2, nullable = false)
    private String dspTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_DSP_AMT", nullable = false)
    private double dspAmount;

    @Column(name = "BIL_PAYEE_CLT_ID", length = 20, nullable = false)
    private String payeeClientId = SEPARATOR_BLANK;

    @Column(name = "BIL_PAYEE_ADR_SEQ")
    private short payeeAddressSequenceNumber;

    @Column(name = "BIL_MANUAL_SUS_IND", nullable = false)
    private char manualSuspenseIndicator = BLANK_CHAR;

    @Column(name = "DWS_CK_DRF_NBR")
    private int draftNbr;

    @Column(name = "BCD_CREDIT_IND", nullable = false)
    private char creditIndicator = BLANK_CHAR;

    @Column(name = "BCD_CRE_POL_ID", length = 16, nullable = false)
    private String creditPolicyId = SEPARATOR_BLANK;

    @Column(name = "BCD_CRE_AMT_TYPE", length = 3, nullable = false)
    private String creditAmountType = SEPARATOR_BLANK;

    @Column(name = "BIL_REVS_RSUS_IND", nullable = false)
    private char revsRsusIndicator = BLANK_CHAR;

    @Column(name = "BIL_TO_FRO_TRF_NBR", length = 30, nullable = false)
    private String toFromTransferNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_TRF_TYPE_CD", nullable = false)
    private char transferTypeCd = BLANK_CHAR;

    @Column(name = "DDS_DSB_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime disbursementDate;

    @Column(name = "DWS_STATUS_CD", nullable = false)
    private char statusCd = BLANK_CHAR;

    @Column(name = "BIL_DSB_ID", length = 20, nullable = false)
    private String disbursementId = SEPARATOR_BLANK;

    @Column(name = "BIL_CHK_PRD_MTH_CD", nullable = false)
    private char checkProdMethodCd = BLANK_CHAR;

    @Column(name = "BIL_STT_DTL_TYP_CD", length = 3, nullable = false)
    private String statementDetailTypeCd = SEPARATOR_BLANK;

    public BilCashDspId getBilCashDspId() {
        return bilCashDspId;
    }

    public void setBilCashDspId(BilCashDspId bilCashDspId) {
        this.bilCashDspId = bilCashDspId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicySymbolCd() {
        return policySymbolCd;
    }

    public void setPolicySymbolCd(String policySymbolCd) {
        this.policySymbolCd = policySymbolCd;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public ZonedDateTime getPolicyEffectiveDate() {
        return policyEffectiveDate;
    }

    public void setPolicyEffectiveDate(ZonedDateTime policyEffectiveDate) {
        this.policyEffectiveDate = policyEffectiveDate;
    }

    public String getPayableItemCd() {
        return payableItemCd;
    }

    public void setPayableItemCd(String payableItemCd) {
        this.payableItemCd = payableItemCd;
    }

    public short getBilSequenceNumber() {
        return bilSequenceNumber;
    }

    public void setBilSequenceNumber(short bilSequenceNumber) {
        this.bilSequenceNumber = bilSequenceNumber;
    }

    public ZonedDateTime getSystemDueDate() {
        return systemDueDate;
    }

    public void setSystemDueDate(ZonedDateTime systemDueDate) {
        this.systemDueDate = systemDueDate;
    }

    public ZonedDateTime getAdjustmentDueDate() {
        return adjustmentDueDate;
    }

    public void setAdjustmentDueDate(ZonedDateTime adjustmentDueDate) {
        this.adjustmentDueDate = adjustmentDueDate;
    }

    public ZonedDateTime getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(ZonedDateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public char getChargeTypeCd() {
        return chargeTypeCd;
    }

    public void setChargeTypeCd(char chargeTypeCd) {
        this.chargeTypeCd = chargeTypeCd;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ZonedDateTime getDspDate() {
        return dspDate;
    }

    public void setDspDate(ZonedDateTime dspDate) {
        this.dspDate = dspDate;
    }

    public String getDspReasonCd() {
        return dspReasonCd;
    }

    public void setDspReasonCd(String dspReasonCd) {
        this.dspReasonCd = dspReasonCd;
    }

    public String getDspTypeCd() {
        return dspTypeCd;
    }

    public void setDspTypeCd(String dspTypeCd) {
        this.dspTypeCd = dspTypeCd;
    }

    public double getDspAmount() {
        return dspAmount;
    }

    public void setDspAmount(double dspAmount) {
        this.dspAmount = dspAmount;
    }

    public String getPayeeClientId() {
        return payeeClientId;
    }

    public void setPayeeClientId(String payeeClientId) {
        this.payeeClientId = payeeClientId;
    }

    public short getPayeeAddressSequenceNumber() {
        return payeeAddressSequenceNumber;
    }

    public void setPayeeAddressSequenceNumber(short payeeAddressSequenceNumber) {
        this.payeeAddressSequenceNumber = payeeAddressSequenceNumber;
    }

    public char getManualSuspenseIndicator() {
        return manualSuspenseIndicator;
    }

    public void setManualSuspenseIndicator(char manualSuspenseIndicator) {
        this.manualSuspenseIndicator = manualSuspenseIndicator;
    }

    public int getDraftNbr() {
        return draftNbr;
    }

    public void setDraftNbr(int draftNbr) {
        this.draftNbr = draftNbr;
    }

    public char getCreditIndicator() {
        return creditIndicator;
    }

    public void setCreditIndicator(char creditIndicator) {
        this.creditIndicator = creditIndicator;
    }

    public String getCreditPolicyId() {
        return creditPolicyId;
    }

    public void setCreditPolicyId(String creditPolicyId) {
        this.creditPolicyId = creditPolicyId;
    }

    public String getCreditAmountType() {
        return creditAmountType;
    }

    public void setCreditAmountType(String creditAmountType) {
        this.creditAmountType = creditAmountType;
    }

    public char getRevsRsusIndicator() {
        return revsRsusIndicator;
    }

    public void setRevsRsusIndicator(char revsRsusIndicator) {
        this.revsRsusIndicator = revsRsusIndicator;
    }

    public String getToFromTransferNbr() {
        return toFromTransferNbr;
    }

    public void setToFromTransferNbr(String toFromTransferNbr) {
        this.toFromTransferNbr = toFromTransferNbr;
    }

    public char getTransferTypeCd() {
        return transferTypeCd;
    }

    public void setTransferTypeCd(char transferTypeCd) {
        this.transferTypeCd = transferTypeCd;
    }

    public ZonedDateTime getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(ZonedDateTime disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public char getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(char statusCd) {
        this.statusCd = statusCd;
    }

    public String getDisbursementId() {
        return disbursementId;
    }

    public void setDisbursementId(String disbursementId) {
        this.disbursementId = disbursementId;
    }

    public char getCheckProdMethodCd() {
        return checkProdMethodCd;
    }

    public void setCheckProdMethodCd(char checkProdMethodCd) {
        this.checkProdMethodCd = checkProdMethodCd;
    }

    public String getStatementDetailTypeCd() {
        return statementDetailTypeCd;
    }

    public void setStatementDetailTypeCd(String statementDetailTypeCd) {
        this.statementDetailTypeCd = statementDetailTypeCd;
    }

}
