package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilUnIdCashId;

@Entity
@Table(name = "BIL_UN_ID_CASH")
public class BilUnIdCash implements Serializable {
	/**
	 *
	 */

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private BilUnIdCashId bilUnIdCashId;

	@Column(name = "BIL_ENTRY_SEQ_NBR", length = 5, nullable = false)
	private short bilEntrySeqNbr = SHORT_ZERO;

	@Column(name = "BIL_ACCOUNT_NBR", length = 30, nullable = false)
	private String bilAccountNbr = SEPARATOR_BLANK;

	@Column(name = "BIL_TTY_NBR", length = 20, nullable = false)
	private String bilTtyNbr = SEPARATOR_BLANK;

	@Column(name = "BIL_AGT_ACT_NBR", length = 20, nullable = false)
	private String bilAgtActNbr = SEPARATOR_BLANK;

	@Column(name = "BIL_ADDITIONAL_ID", length = 20, nullable = false)
	private String bilAdditionalId = SEPARATOR_BLANK;

	@Column(name = "POL_SYMBOL_CD", length = 3, nullable = false)
	private String polSymbolCd = SEPARATOR_BLANK;

	@Column(name = "POL_NBR", length = 25, nullable = false)
	private String polNbr = SEPARATOR_BLANK;

	@Column(name = "BIL_ADJ_DUE_DT", nullable = false)
	@Type(type = "org.hibernate.type.ZonedDateTimeType")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private ZonedDateTime bilAdjDueDt;

	@Column(name = "BIL_PBL_ITEM_CD", length = 3, nullable = false)
	private String bilPblItemCd = SEPARATOR_BLANK;

	@Column(name = "BIL_RCT_AMT", precision = 14, scale = 2, nullable = false)
	private double bilRctAmt;

	@Column(name = "BIL_RCT_TYPE_CD", length = 2, nullable = false)
	private String bilRctTypeCd = SEPARATOR_BLANK;

	@Column(name = "BIL_RCT_ID", length = 10, nullable = false)
	private String bilRctId = SEPARATOR_BLANK;

	@Column(name = "BIL_RCT_CLT_ID", length = 20, nullable = false)
	private String bilRctCltId = SEPARATOR_BLANK;

	@Column(name = "BIL_RCT_ADR_SEQ", length = 5, nullable = false)
	private short bilRctAdrSeq = SHORT_ZERO;

	@Column(name = "BIL_RCT_RECEIVE_DT", nullable = false)
	@Type(type = "org.hibernate.type.ZonedDateTimeType")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private ZonedDateTime bilRctReceiveDt;

	@Column(name = "BIL_TAX_YEAR_NBR", length = 5, nullable = false)
	private short bilTaxYearNbr = SHORT_ZERO;

	@Column(name = "BIL_DSP_DT", nullable = false)
	@Type(type = "org.hibernate.type.ZonedDateTimeType")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private ZonedDateTime bilDspDt;

	@Column(name = "BIL_DSP_TYPE_CD", length = 2, nullable = false)
	private String bilDspTypeCd = SEPARATOR_BLANK;

	@Column(name = "BIL_DSP_REASON_CD", length = 3, nullable = false)
	private String bilDspReasonCd = SEPARATOR_BLANK;

	@Column(name = "BIL_PAYEE_CLT_ID", length = 20, nullable = false)
	private String bilPayeeCltId = SEPARATOR_BLANK;

	@Column(name = "BIL_PAYEE_ADR_SEQ", length = 5, nullable = false)
	private short bilPayeeAdrSeq = SHORT_ZERO;

	@Column(name = "BIL_MANUAL_SUS_IND", nullable = false)
	private char bilManualSusInd;

	@Column(name = "DWS_CK_DRF_NBR", length = 10, nullable = false)
	private int dwsCkDrfNbr;

	@Column(name = "BIL_BANK_CD", length = 3, nullable = false)
	private String bilBankCd = SEPARATOR_BLANK;

	@Column(name = "BIL_DEPOSIT_DT", nullable = false)
	@Type(type = "org.hibernate.type.ZonedDateTimeType")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private ZonedDateTime bilDepositDt;

	@Column(name = "BIL_ACY_TS", nullable = false)
	@Type(type = "org.hibernate.type.ZonedDateTimeType")
	private ZonedDateTime bilAcyTs;

	@Column(name = "BIL_CSH_ETR_MTH_CD", nullable = false)
	private char bilCshEtrMthCd = BLANK_CHAR;

	@Column(name = "BIL_CRCRD_TYPE_CD", nullable = false)
	private char bilCrcrdTypeCd = BLANK_CHAR;

	@Column(name = "BIL_CRCRD_ARSP_CD", nullable = false)
	private char bilCrcrdArspCd = BLANK_CHAR;

	@Column(name = "BIL_CRCRD_ACT_NBR", length = 25, nullable = false)
	private String bilCrcrdActNbr = SEPARATOR_BLANK;

	@Column(name = "BIL_CRCRD_EXP_DT", length = 4, nullable = false)
	private String bilCrcrdExpDt = SEPARATOR_BLANK;

	@Column(name = "BIL_CRCRD_AUT_APV", length = 6, nullable = false)
	private String bilCrcrdAutApv = SEPARATOR_BLANK;

	@Column(name = "BUC_REVS_DSB_IND", nullable = false)
	private char bucRevsDsbInd = BLANK_CHAR;

	@Column(name = "BIL_CWA_ID", length = 10, nullable = false)
	private String bilCwaId = SEPARATOR_BLANK;

	@Column(name = "DDS_DSB_DT")
	@Type(type = "org.hibernate.type.ZonedDateTimeType")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private ZonedDateTime ddsDsbDt;

	@Column(name = "DWS_STATUS_CD", nullable = false)
	private char dwsStatusCd = BLANK_CHAR;

	@Column(name = "BIL_DSB_ID", length = 20, nullable = false)
	private String bilDsbId = SEPARATOR_BLANK;

	@Column(name = "BIL_CHK_PRD_MTH_CD", nullable = false)
	private char bilChkPrdMthCd = BLANK_CHAR;

	@Column(name = "BUC_ACY_USER_ID", length = 8, nullable = false)
	private String activityUserId = SEPARATOR_BLANK;

	@Column(name = "BIL_PSTMRK_DT", nullable = false)
	@Type(type = "org.hibernate.type.ZonedDateTimeType")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private ZonedDateTime bilPstmrkDt;

	@Column(name = "BIL_PSTMRK_CD", nullable = false)
	private char bilPstmrkCd = BLANK_CHAR;

	@Column(name = "BIL_RCT_CMT", length = 80, nullable = false)
	private String bilRctCmt = SEPARATOR_BLANK;

	@Column(name = "BIL_UNID_CASH_ID", length = 8, nullable = false)
	private String billUnidCashId = SEPARATOR_BLANK;

	@Column(name = "AGENCY_NBR", length = 12, nullable = false)
	private String agencyNbr = SEPARATOR_BLANK;

	@Column(name = "BIL_CRCRD_ARSP_TYC", length = 3, nullable = false)
	private String bilCrcrdArspTyc = SEPARATOR_BLANK;

	@Column(name = "BIL_BUS_GRP_CD", length = 3, nullable = false)
	private String bilBusGrpCd = SEPARATOR_BLANK;
	
	@Column(name = "PMT_PFL_ID", length = 512, nullable = true)
    private String paymentProfileId;

	public BilUnIdCashId getBilUnIdCashId() {
		return bilUnIdCashId;
	}

	public void setBilUnIdCashId(BilUnIdCashId bilUnIdCashId) {
		this.bilUnIdCashId = bilUnIdCashId;
	}

	public short getBilEntrySeqNbr() {
		return bilEntrySeqNbr;
	}

	public void setBilEntrySeqNbr(short bilEntrySeqNbr) {
		this.bilEntrySeqNbr = bilEntrySeqNbr;
	}

	public String getBilAccountNbr() {
		return bilAccountNbr;
	}

	public void setBilAccountNbr(String bilAccountNbr) {
		this.bilAccountNbr = bilAccountNbr;
	}

	public String getBilTtyNbr() {
		return bilTtyNbr;
	}

	public void setBilTtyNbr(String bilTtyNbr) {
		this.bilTtyNbr = bilTtyNbr;
	}

	public String getBilAgtActNbr() {
		return bilAgtActNbr;
	}

	public void setBilAgtActNbr(String bilAgtActNbr) {
		this.bilAgtActNbr = bilAgtActNbr;
	}

	public String getBilAdditionalId() {
		return bilAdditionalId;
	}

	public void setBilAdditionalId(String bilAdditionalId) {
		this.bilAdditionalId = bilAdditionalId;
	}

	public String getPolSymbolCd() {
		return polSymbolCd;
	}

	public void setPolSymbolCd(String polSymbolCd) {
		this.polSymbolCd = polSymbolCd;
	}

	public String getPolNbr() {
		return polNbr;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = polNbr;
	}

	public ZonedDateTime getBilAdjDueDt() {
		return bilAdjDueDt;
	}

	public void setBilAdjDueDt(ZonedDateTime bilAdjDueDt) {
		this.bilAdjDueDt = bilAdjDueDt;
	}

	public String getBilPblItemCd() {
		return bilPblItemCd;
	}

	public void setBilPblItemCd(String bilPblItemCd) {
		this.bilPblItemCd = bilPblItemCd;
	}

	public double getBilRctAmt() {
		return bilRctAmt;
	}

	public void setBilRctAmt(double bilRctAmt) {
		this.bilRctAmt = bilRctAmt;
	}

	public String getBilRctTypeCd() {
		return bilRctTypeCd;
	}

	public void setBilRctTypeCd(String bilRctTypeCd) {
		this.bilRctTypeCd = bilRctTypeCd;
	}

	public String getBilRctId() {
		return bilRctId;
	}

	public void setBilRctId(String bilRctId) {
		this.bilRctId = bilRctId;
	}

	public String getBilRctCltId() {
		return bilRctCltId;
	}

	public void setBilRctCltId(String bilRctCltId) {
		this.bilRctCltId = bilRctCltId;
	}

	public short getBilRctAdrSeq() {
		return bilRctAdrSeq;
	}

	public void setBilRctAdrSeq(short bilRctAdrSeq) {
		this.bilRctAdrSeq = bilRctAdrSeq;
	}

	public ZonedDateTime getBilRctReceiveDt() {
		return bilRctReceiveDt;
	}

	public void setBilRctReceiveDt(ZonedDateTime bilRctReceiveDt) {
		this.bilRctReceiveDt = bilRctReceiveDt;
	}

	public short getBilTaxYearNbr() {
		return bilTaxYearNbr;
	}

	public void setBilTaxYearNbr(short bilTaxYearNbr) {
		this.bilTaxYearNbr = bilTaxYearNbr;
	}

	public ZonedDateTime getBilDspDt() {
		return bilDspDt;
	}

	public void setBilDspDt(ZonedDateTime bilDspDt) {
		this.bilDspDt = bilDspDt;
	}

	public String getBilDspTypeCd() {
		return bilDspTypeCd;
	}

	public void setBilDspTypeCd(String bilDspTypeCd) {
		this.bilDspTypeCd = bilDspTypeCd;
	}

	public String getBilDspReasonCd() {
		return bilDspReasonCd;
	}

	public void setBilDspReasonCd(String bilDspReasonCd) {
		this.bilDspReasonCd = bilDspReasonCd;
	}

	public String getBilPayeeCltId() {
		return bilPayeeCltId;
	}

	public void setBilPayeeCltId(String bilPayeeCltId) {
		this.bilPayeeCltId = bilPayeeCltId;
	}

	public short getBilPayeeAdrSeq() {
		return bilPayeeAdrSeq;
	}

	public void setBilPayeeAdrSeq(short bilPayeeAdrSeq) {
		this.bilPayeeAdrSeq = bilPayeeAdrSeq;
	}

	public char getBilManualSusInd() {
		return bilManualSusInd;
	}

	public void setBilManualSusInd(char bilManualSusInd) {
		this.bilManualSusInd = bilManualSusInd;
	}

	public int getDwsCkDrfNbr() {
		return dwsCkDrfNbr;
	}

	public void setDwsCkDrfNbr(int dwsCkDrfNbr) {
		this.dwsCkDrfNbr = dwsCkDrfNbr;
	}

	public String getBilBankCd() {
		return bilBankCd;
	}

	public void setBilBankCd(String bilBankCd) {
		this.bilBankCd = bilBankCd;
	}

	public ZonedDateTime getBilDepositDt() {
		return bilDepositDt;
	}

	public void setBilDepositDt(ZonedDateTime bilDepositDt) {
		this.bilDepositDt = bilDepositDt;
	}

	public ZonedDateTime getBilAcyTs() {
		return bilAcyTs;
	}

	public void setBilAcyTs(ZonedDateTime bilAcyTs) {
		this.bilAcyTs = bilAcyTs;
	}

	public char getBilCshEtrMthCd() {
		return bilCshEtrMthCd;
	}

	public void setBilCshEtrMthCd(char bilCshEtrMthCd) {
		this.bilCshEtrMthCd = bilCshEtrMthCd;
	}

	public char getBilCrcrdTypeCd() {
		return bilCrcrdTypeCd;
	}

	public void setBilCrcrdTypeCd(char bilCrcrdTypeCd) {
		this.bilCrcrdTypeCd = bilCrcrdTypeCd;
	}

	public char getBilCrcrdArspCd() {
		return bilCrcrdArspCd;
	}

	public void setBilCrcrdArspCd(char bilCrcrdArspCd) {
		this.bilCrcrdArspCd = bilCrcrdArspCd;
	}

	public String getBilCrcrdActNbr() {
		return bilCrcrdActNbr;
	}

	public void setBilCrcrdActNbr(String bilCrcrdActNbr) {
		this.bilCrcrdActNbr = bilCrcrdActNbr;
	}

	public String getBilCrcrdExpDt() {
		return bilCrcrdExpDt;
	}

	public void setBilCrcrdExpDt(String bilCrcrdExpDt) {
		this.bilCrcrdExpDt = bilCrcrdExpDt;
	}

	public String getBilCrcrdAutApv() {
		return bilCrcrdAutApv;
	}

	public void setBilCrcrdAutApv(String bilCrcrdAutApv) {
		this.bilCrcrdAutApv = bilCrcrdAutApv;
	}

	public char getBucRevsDsbInd() {
		return bucRevsDsbInd;
	}

	public void setBucRevsDsbInd(char bucRevsDsbInd) {
		this.bucRevsDsbInd = bucRevsDsbInd;
	}

	public String getBilCwaId() {
		return bilCwaId;
	}

	public void setBilCwaId(String bilCwaId) {
		this.bilCwaId = bilCwaId;
	}

	public ZonedDateTime getDdsDsbDt() {
		return ddsDsbDt;
	}

	public void setDdsDsbDt(ZonedDateTime ddsDsbDt) {
		this.ddsDsbDt = ddsDsbDt;
	}

	public char getDwsStatusCd() {
		return dwsStatusCd;
	}

	public void setDwsStatusCd(char dwsStatusCd) {
		this.dwsStatusCd = dwsStatusCd;
	}

	public String getBilDsbId() {
		return bilDsbId;
	}

	public void setBilDsbId(String bilDsbId) {
		this.bilDsbId = bilDsbId;
	}

	public char getBilChkPrdMthCd() {
		return bilChkPrdMthCd;
	}

	public void setBilChkPrdMthCd(char bilChkPrdMthCd) {
		this.bilChkPrdMthCd = bilChkPrdMthCd;
	}

	public String getActivityUserId() {
		return activityUserId;
	}

	public void setActivityUserId(String activityUserId) {
		this.activityUserId = activityUserId;
	}

	public ZonedDateTime getBilPstmrkDt() {
		return bilPstmrkDt;
	}

	public void setBilPstmrkDt(ZonedDateTime bilPstmrkDt) {
		this.bilPstmrkDt = bilPstmrkDt;
	}

	public char getBilPstmrkCd() {
		return bilPstmrkCd;
	}

	public void setBilPstmrkCd(char bilPstmrkCd) {
		this.bilPstmrkCd = bilPstmrkCd;
	}

	public String getBilRctCmt() {
		return bilRctCmt;
	}

	public void setBilRctCmt(String bilRctCmt) {
		this.bilRctCmt = bilRctCmt;
	}

	public String getBillUnidCashId() {
		return billUnidCashId;
	}

	public void setBillUnidCashId(String billUnidCashId) {
		this.billUnidCashId = billUnidCashId;
	}

	public String getAgencyNbr() {
		return agencyNbr;
	}

	public void setAgencyNbr(String agencyNbr) {
		this.agencyNbr = agencyNbr;
	}

	public String getBilCrcrdArspTyc() {
		return bilCrcrdArspTyc;
	}

	public void setBilCrcrdArspTyc(String bilCrcrdArspTyc) {
		this.bilCrcrdArspTyc = bilCrcrdArspTyc;
	}

	public String getBilBusGrpCd() {
		return bilBusGrpCd;
	}

	public void setBilBusGrpCd(String bilBusGrpCd) {
		this.bilBusGrpCd = bilBusGrpCd;
	}

    public String getPaymentProfileId() {
        return paymentProfileId;
    }

    public void setPaymentProfileId(String paymentProfileId) {
        this.paymentProfileId = paymentProfileId;
    }
	
}
