package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilEftPendingTapeId;

@Entity
@Table(name = "BIL_EFT_PND_TAPE")
public final class BilEftPendingTape implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilEftPendingTapeId bilEftPendingTapeId;

    @Column(name = "BIL_EFT_DR_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime eftDrDate;

    @Column(name = "BIL_EFT_DR_AMT", precision = 14, scale = 2, nullable = false)
    private double eftDrAmount;

    @Column(name = "BIL_EFT_PST_PMT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime eftPostPaymentDate;

    @Column(name = "BIL_EFT_NOT_IND", nullable = false)
    private char eftNotIndicator;

    @Column(name = "BIL_EFT_RCD_TYC", nullable = false)
    private char eftReceivedType;

    @Column(name = "BIL_FILE_TYPE_CD", length = 5, nullable = false)
    private short fileTypeCd;

    @Column(name = "BIL_DSB_ID", length = 20, nullable = false)
    private String disbursementId = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_SEQ_NBR", length = 5, nullable = false)
    private short receiptSequenceNumber;

    @Column(name = "WEB_AUT_IND", nullable = false)
    private char webAutomatedIndicator;

    @Column(name = "AGENCY_NBR", length = 12, nullable = false)
    private String agencyNbr = SEPARATOR_BLANK;

    @Column(name = "INSERTED_ROW_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime insertedRowTime;

    @Column(name = "BIL_PMT_ORDER_ID", length = 22, nullable = false)
    private String paymentOrderId = SEPARATOR_BLANK;

    @Column(name = "BIL_BUS_ACT_CD", nullable = false)
    private char businessActivityCd;

    @Column(name = "BIL_BUS_GRP_CD", length = 3, nullable = false)
    private String bilBusGrpCd = SEPARATOR_BLANK;

    public BilEftPendingTape() {
        super();
    }

    public BilEftPendingTape(BilEftPendingTape bilEftPendingTape) {
        super();
        this.bilEftPendingTapeId = bilEftPendingTape.getBilEftPendingTapeId();
        this.eftDrDate = bilEftPendingTape.getEftDrDate();
        this.eftDrAmount = bilEftPendingTape.getEftDrAmount();
        this.eftPostPaymentDate = bilEftPendingTape.getEftPostPaymentDate();
        this.eftNotIndicator = bilEftPendingTape.getEftNotIndicator();
        this.eftReceivedType = bilEftPendingTape.getEftReceivedType();
        this.fileTypeCd = bilEftPendingTape.getFileTypeCd();
        this.disbursementId = bilEftPendingTape.getDisbursementId();
        this.receiptSequenceNumber = bilEftPendingTape.getReceiptSequenceNumber();
        this.webAutomatedIndicator = bilEftPendingTape.getWebAutomatedIndicator();
        this.agencyNbr = bilEftPendingTape.getAgencyNbr();
        this.insertedRowTime = bilEftPendingTape.getInsertedRowTime();
        this.paymentOrderId = bilEftPendingTape.getPaymentOrderId();
        this.businessActivityCd = bilEftPendingTape.getBusinessActivityCd();
        this.bilBusGrpCd = bilEftPendingTape.getBilBusGrpCd();
    }

    public BilEftPendingTapeId getBilEftPendingTapeId() {
        return bilEftPendingTapeId;
    }

    public void setBilEftPendingTapeId(BilEftPendingTapeId bilEftPendingTapeId) {
        this.bilEftPendingTapeId = bilEftPendingTapeId;
    }

    public ZonedDateTime getEftDrDate() {
        return eftDrDate;
    }

    public void setEftDrDate(ZonedDateTime eftDrDate) {
        this.eftDrDate = eftDrDate;
    }

    public double getEftDrAmount() {
        return eftDrAmount;
    }

    public void setEftDrAmount(double eftDrAmount) {
        this.eftDrAmount = eftDrAmount;
    }

    public ZonedDateTime getEftPostPaymentDate() {
        return eftPostPaymentDate;
    }

    public void setEftPostPaymentDate(ZonedDateTime eftPostPaymentDate) {
        this.eftPostPaymentDate = eftPostPaymentDate;
    }

    public char getEftNotIndicator() {
        return eftNotIndicator;
    }

    public void setEftNotIndicator(char eftNotIndicator) {
        this.eftNotIndicator = eftNotIndicator;
    }

    public char getEftReceivedType() {
        return eftReceivedType;
    }

    public void setEftReceivedType(char eftReceivedType) {
        this.eftReceivedType = eftReceivedType;
    }

    public short getFileTypeCd() {
        return fileTypeCd;
    }

    public void setFileTypeCd(short fileTypeCd) {
        this.fileTypeCd = fileTypeCd;
    }

    public String getDisbursementId() {
        return disbursementId;
    }

    public void setDisbursementId(String disbursementId) {
        this.disbursementId = disbursementId;
    }

    public short getReceiptSequenceNumber() {
        return receiptSequenceNumber;
    }

    public void setReceiptSequenceNumber(short receiptSequenceNumber) {
        this.receiptSequenceNumber = receiptSequenceNumber;
    }

    public char getWebAutomatedIndicator() {
        return webAutomatedIndicator;
    }

    public void setWebAutomatedIndicator(char webAutomatedIndicator) {
        this.webAutomatedIndicator = webAutomatedIndicator;
    }

    public String getAgencyNbr() {
        return agencyNbr;
    }

    public void setAgencyNbr(String agencyNbr) {
        this.agencyNbr = agencyNbr;
    }

    public ZonedDateTime getInsertedRowTime() {
        return insertedRowTime;
    }

    public void setInsertedRowTime(ZonedDateTime insertedRowTime) {
        this.insertedRowTime = insertedRowTime;
    }

    public String getPaymentOrderId() {
        return paymentOrderId;
    }

    public void setPaymentOrderId(String paymentOrderId) {
        this.paymentOrderId = paymentOrderId;
    }

    public char getBusinessActivityCd() {
        return businessActivityCd;
    }

    public void setBusinessActivityCd(char businessActivityCd) {
        this.businessActivityCd = businessActivityCd;
    }

    public String getBilBusGrpCd() {
        return bilBusGrpCd;
    }

    public void setBilBusGrpCd(String bilBusGrpCd) {
        this.bilBusGrpCd = bilBusGrpCd;
    }

}
