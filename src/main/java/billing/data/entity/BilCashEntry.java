package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilCashEntryId;

@Entity
@Table(name = "BIL_CASH_ENTRY")
public class BilCashEntry implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilCashEntryId bilCashEntryId;

    @Column(name = "BIL_ACCOUNT_NBR", length = 30, nullable = false)
    private String bilAccountNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_TTY_NBR", length = 20, nullable = false)
    private String bilTtyNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_AGT_ACT_NBR", length = 20, nullable = false)
    private String bilAgtActNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_ADDITIONAL_ID", length = 20, nullable = false)
    private String bilAdditionalId = SEPARATOR_BLANK;

    @Column(name = "POL_SYMBOL_CD", length = 3, nullable = true)
    private String polSymbolCd = SEPARATOR_BLANK;

    @Column(name = "POL_NBR", length = 25, nullable = false)
    private String polNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_PBL_ITEM_CD", length = 3, nullable = false)
    private String bilPblItemCd = SEPARATOR_BLANK;

    @Column(name = "BIL_ADJ_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAdjDueDt;

    @Column(name = "BIL_RCT_AMT", precision = 14, scale = 2, nullable = false)
    private double bilRctAmt;

    @Column(name = "BIL_RCT_TYPE_CD", length = 2, nullable = false)
    private String bilRctTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_SUS_REASON_CD", length = 3, nullable = false)
    private String bilSusReasonCd = SEPARATOR_BLANK;

    @Column(name = "BIL_MANUAL_SUS_IND", nullable = false)
    private char bilManualSusInd;

    @Column(name = "BIL_RCT_ID", length = 10, nullable = false)
    private String bilRctId = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_CLT_ID", length = 20, nullable = false)
    private String bilRctCltId = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_ADR_SEQ", nullable = false)
    private short bilRctAdrSeq;

    @Column(name = "BIL_RCT_RECEIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilRctReceiveDt;

    @Column(name = "BIL_TAX_YEAR_NBR", nullable = false)
    private short bilTaxYearNbr;

    @Column(name = "BIL_CRCRD_TYPE_CD", nullable = false)
    private char bilCrcrdTypeCd;

    @Column(name = "BIL_CRCRD_ARSP_CD", nullable = false)
    private char bilCrcrdArspCd;

    @Column(name = "BIL_CRCRD_ACT_NBR", length = 25, nullable = false)
    private String bilCrcrdActNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_EXP_DT", length = 4, nullable = false)
    private String bilCrcrdExpDt = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_AUT_APV", length = 6, nullable = false)
    private String bilCrcrdAutApv = SEPARATOR_BLANK;

    @Column(name = "BIL_CWA_ID", length = 10, nullable = false)
    private String bilCwaId = SEPARATOR_BLANK;

    @Column(name = "BIL_PSTMRK_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilPstMrkDt;

    @Column(name = "BIL_PSTMRK_CD", nullable = false)
    private char bilPstMrkCd;

    @Column(name = "BIL_RCT_CMT", length = 80, nullable = false)
    private String bilRctCmt = SEPARATOR_BLANK;

    @Column(name = "BCE_CHG_USER_ID", length = 8, nullable = false)
    private String becChgUserId = SEPARATOR_BLANK;

    @Column(name = "AGENCY_NBR", length = 12, nullable = false)
    private String agencyNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_PST_CD", length = 13, nullable = false)
    private String bilCrcrdPstCd = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_ARSP_TYC", length = 3, nullable = false)
    private String bilCrcrdArspTyc = SEPARATOR_BLANK;

    @Column(name = "INSERTED_ROW_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "%Y-%0o-%0d-%0t.%0m.%0s.%0u")
    private ZonedDateTime insertedRowTs;

    @Column(name = "BIL_PMT_ORDER_ID", length = 22, nullable = false)
    private String bilPmtOrderId = SEPARATOR_BLANK;
    
    @Column(name = "BIL_BUS_GRP_CD", length = 3, nullable = false)
    private String bilBusGrpCd = SEPARATOR_BLANK;
    
    public BilCashEntryId getBilCashEntryId() {
        return bilCashEntryId;
    }

    public void setBilCashEntryId(BilCashEntryId bilCashEntryId) {
        this.bilCashEntryId = bilCashEntryId;
    }

    public String getBilAccountNbr() {
        return bilAccountNbr;
    }

    public void setBilAccountNbr(String bilAccountNbr) {
        this.bilAccountNbr = bilAccountNbr;
    }

    public String getBilTtyNbr() {
        return bilTtyNbr;
    }

    public void setBilTtyNbr(String bilTtyNbr) {
        this.bilTtyNbr = bilTtyNbr;
    }

    public String getBilAgtActNbr() {
        return bilAgtActNbr;
    }

    public void setBilAgtActNbr(String bilAgtActNbr) {
        this.bilAgtActNbr = bilAgtActNbr;
    }

    public String getBilAdditionalId() {
        return bilAdditionalId;
    }

    public void setBilAdditionalId(String bilAdditionalId) {
        this.bilAdditionalId = bilAdditionalId;
    }

    public String getPolSymbolCd() {
        return polSymbolCd;
    }

    public void setPolSymbolCd(String polSymbolCd) {
        this.polSymbolCd = polSymbolCd;
    }

    public String getPolNbr() {
        return polNbr;
    }

    public void setPolNbr(String polNbr) {
        this.polNbr = polNbr;
    }

    public String getBilPblItemCd() {
        return bilPblItemCd;
    }

    public void setBilPblItemCd(String bilPblItemCd) {
        this.bilPblItemCd = bilPblItemCd;
    }

    public ZonedDateTime getBilAdjDueDt() {
        return bilAdjDueDt;
    }

    public void setBilAdjDueDt(ZonedDateTime bilAdjDueDt) {
        this.bilAdjDueDt = bilAdjDueDt;
    }

    public double getBilRctAmt() {
        return bilRctAmt;
    }

    public void setBilRctAmt(double bilRctAmt) {
        this.bilRctAmt = bilRctAmt;
    }

    public String getBilRctTypeCd() {
        return bilRctTypeCd;
    }

    public void setBilRctTypeCd(String bilRctTypeCd) {
        this.bilRctTypeCd = bilRctTypeCd;
    }

    public String getBilSusReasonCd() {
        return bilSusReasonCd;
    }

    public void setBilSusReasonCd(String bilSusReasonCd) {
        this.bilSusReasonCd = bilSusReasonCd;
    }

    public char getBilManualSusInd() {
        return bilManualSusInd;
    }

    public void setBilManualSusInd(char bilManualSusInd) {
        this.bilManualSusInd = bilManualSusInd;
    }

    public String getBilRctId() {
        return bilRctId;
    }

    public void setBilRctId(String bilRctId) {
        this.bilRctId = bilRctId;
    }

    public String getBilRctCltId() {
        return bilRctCltId;
    }

    public void setBilRctCltId(String bilRctCltId) {
        this.bilRctCltId = bilRctCltId;
    }

    public short getBilRctAdrSeq() {
        return bilRctAdrSeq;
    }

    public void setBilRctAdrSeq(short bilRctAdrSeq) {
        this.bilRctAdrSeq = bilRctAdrSeq;
    }

    public ZonedDateTime getBilRctReceiveDt() {
        return bilRctReceiveDt;
    }

    public void setBilRctReceiveDt(ZonedDateTime bilRctReceiveDt) {
        this.bilRctReceiveDt = bilRctReceiveDt;
    }

    public short getBilTaxYearNbr() {
        return bilTaxYearNbr;
    }

    public void setBilTaxYearNbr(short bilTaxYearNbr) {
        this.bilTaxYearNbr = bilTaxYearNbr;
    }

    public char getBilCrcrdTypeCd() {
        return bilCrcrdTypeCd;
    }

    public void setBilCrcrdTypeCd(char bilCrcrdTypeCd) {
        this.bilCrcrdTypeCd = bilCrcrdTypeCd;
    }

    public char getBilCrcrdArspCd() {
        return bilCrcrdArspCd;
    }

    public void setBilCrcrdArspCd(char bilCrcrdArspCd) {
        this.bilCrcrdArspCd = bilCrcrdArspCd;
    }

    public String getBilCrcrdActNbr() {
        return bilCrcrdActNbr;
    }

    public void setBilCrcrdActNbr(String bilCrcrdActNbr) {
        this.bilCrcrdActNbr = bilCrcrdActNbr;
    }

    public String getBilCrcrdExpDt() {
        return bilCrcrdExpDt;
    }

    public void setBilCrcrdExpDt(String bilCrcrdExpDt) {
        this.bilCrcrdExpDt = bilCrcrdExpDt;
    }

    public String getBilCrcrdAutApv() {
        return bilCrcrdAutApv;
    }

    public void setBilCrcrdAutApv(String bilCrcrdAutApv) {
        this.bilCrcrdAutApv = bilCrcrdAutApv;
    }

    public String getBilCwaId() {
        return bilCwaId;
    }

    public void setBilCwaId(String bilCwaId) {
        this.bilCwaId = bilCwaId;
    }

    public ZonedDateTime getBilPstMrkDt() {
        return bilPstMrkDt;
    }

    public void setBilPstMrkDt(ZonedDateTime bilPstMrkDt) {
        this.bilPstMrkDt = bilPstMrkDt;
    }

    public char getBilPstMrkCd() {
        return bilPstMrkCd;
    }

    public void setBilPstMrkCd(char bilPstMrkCd) {
        this.bilPstMrkCd = bilPstMrkCd;
    }

    public String getBilRctCmt() {
        return bilRctCmt;
    }

    public void setBilRctCmt(String bilRctCmt) {
        this.bilRctCmt = bilRctCmt;
    }

    public String getBecChgUserId() {
        return becChgUserId;
    }

    public void setBecChgUserId(String becChgUserId) {
        this.becChgUserId = becChgUserId;
    }

    public String getAgencyNbr() {
        return agencyNbr;
    }

    public void setAgencyNbr(String agencyNbr) {
        this.agencyNbr = agencyNbr;
    }

    public String getBilCrcrdPstCd() {
        return bilCrcrdPstCd;
    }

    public void setBilCrcrdPstCd(String bilCrcrdPstCd) {
        this.bilCrcrdPstCd = bilCrcrdPstCd;
    }

    public String getBilCrcrdArspTyc() {
        return bilCrcrdArspTyc;
    }

    public void setBilCrcrdArspTyc(String bilCrcrdArspTyc) {
        this.bilCrcrdArspTyc = bilCrcrdArspTyc;
    }

    public ZonedDateTime getInsertedRowTs() {
        return insertedRowTs;
    }

    public void setInsertedRowTs(ZonedDateTime insertedRowTs) {
        this.insertedRowTs = insertedRowTs;
    }

    public String getBilPmtOrderId() {
        return bilPmtOrderId;
    }

    public void setBilPmtOrderId(String bilPmtOrderId) {
        this.bilPmtOrderId = bilPmtOrderId;
    }

	public String getBilBusGrpCd() {
		return bilBusGrpCd;
	}

	public void setBilBusGrpCd(String bilBusGrpCd) {
		this.bilBusGrpCd = bilBusGrpCd;
	}
    
}
