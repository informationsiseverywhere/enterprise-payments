package billing.data.entity;

import static core.utils.CommonConstants.BLANK_STRING;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilAgtSummaryId;

@Entity
@Table(name = "BIL_AGT_SUMMARY")
public class BilAgtSummary implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilAgtSummaryId bilAgtSummaryId;

    @Column(name = "BIL_ACY_DES_CD", length = 3, nullable = false)
    private String bilAcyDesCd = BLANK_STRING;

    @Column(name = "BIL_DES_REA_TYP", length = 3, nullable = false)
    private String bilDesReaTyp = BLANK_STRING;

    @Column(name = "BIL_ACY_DES1_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAcyDes1Dt;

    @Column(name = "BIL_ACY_DES2_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAcyDes2Dt;

    @Column(name = "BIL_ACY_AMT", precision = 14, scale = 2, nullable = false)
    private double bilAcyAmt;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = BLANK_STRING;

    @Column(name = "BAS_ADD_DATA_TXT", length = 100, nullable = false)
    private String basAddDataTxt = BLANK_STRING;

    @Column(name = "BIL_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAcyTs;

    public BilAgtSummary() {
        super();
    }

    public BilAgtSummary(BilAgtSummaryId bilAgtSummaryId, String bilAcyDesCd, String bilDesReaTyp,
            ZonedDateTime bilAcyDes1Dt, ZonedDateTime bilAcyDes2Dt, double bilAcyAmt, String userId,
            String basAddDataTxt, ZonedDateTime bilAcyTs) {
        super();
        this.bilAgtSummaryId = bilAgtSummaryId;
        this.bilAcyDesCd = bilAcyDesCd;
        this.bilDesReaTyp = bilDesReaTyp;
        this.bilAcyDes1Dt = bilAcyDes1Dt;
        this.bilAcyDes2Dt = bilAcyDes2Dt;
        this.bilAcyAmt = bilAcyAmt;
        this.userId = userId;
        this.basAddDataTxt = basAddDataTxt;
        this.bilAcyTs = bilAcyTs;
    }

    public BilAgtSummaryId getBilAgtSummaryId() {
        return bilAgtSummaryId;
    }

    public void setBilAgtSummaryId(BilAgtSummaryId bilAgtSummaryId) {
        this.bilAgtSummaryId = bilAgtSummaryId;
    }

    public String getBilAcyDesCd() {
        return bilAcyDesCd;
    }

    public void setBilAcyDesCd(String bilAcyDesCd) {
        this.bilAcyDesCd = bilAcyDesCd;
    }

    public String getBilDesReaTyp() {
        return bilDesReaTyp;
    }

    public void setBilDesReaTyp(String bilDesReaTyp) {
        this.bilDesReaTyp = bilDesReaTyp;
    }

    public ZonedDateTime getBilAcyDes1Dt() {
        return bilAcyDes1Dt;
    }

    public void setBilAcyDes1Dt(ZonedDateTime bilAcyDes1Dt) {
        this.bilAcyDes1Dt = bilAcyDes1Dt;
    }

    public ZonedDateTime getBilAcyDes2Dt() {
        return bilAcyDes2Dt;
    }

    public void setBilAcyDes2Dt(ZonedDateTime bilAcyDes2Dt) {
        this.bilAcyDes2Dt = bilAcyDes2Dt;
    }

    public double getBilAcyAmt() {
        return bilAcyAmt;
    }

    public void setBilAcyAmt(double bilAcyAmt) {
        this.bilAcyAmt = bilAcyAmt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBasAddDataTxt() {
        return basAddDataTxt;
    }

    public void setBasAddDataTxt(String basAddDataTxt) {
        this.basAddDataTxt = basAddDataTxt;
    }

    public ZonedDateTime getBilAcyTs() {
        return bilAcyTs;
    }

    public void setBilAcyTs(ZonedDateTime bilAcyTs) {
        this.bilAcyTs = bilAcyTs;
    }
}
