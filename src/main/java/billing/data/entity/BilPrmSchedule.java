package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilPrmScheduleId;

/**
 * The persistent class for the BIL_PRM_SCHEDULE database table.
 *
 */
@Entity
@Table(name = "BIL_PRM_SCHEDULE")
public class BilPrmSchedule implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilPrmScheduleId bilPrmScheduleId;

    @Column(name = "BPS_PARM_AREA", length = 750, nullable = false)
    private String bpsParmArea = SEPARATOR_BLANK;

    @Column(name = "BUS_CASE_ID", length = 21, nullable = false)
    private String busCaseId = SEPARATOR_BLANK;

    @Column(name = "PROCESS_IND", nullable = false)
    private char processInd = BLANK_CHAR;

    public BilPrmScheduleId getBilPrmScheduleId() {
        return bilPrmScheduleId;
    }

    public void setBilPrmScheduleId(BilPrmScheduleId bilPrmScheduleId) {
        this.bilPrmScheduleId = bilPrmScheduleId;
    }

    public String getBpsParmArea() {
        return bpsParmArea;
    }

    public void setBpsParmArea(String bpsParmArea) {
        this.bpsParmArea = bpsParmArea;
    }

    public String getBusCaseId() {
        return busCaseId;
    }

    public void setBusCaseId(String busCaseId) {
        this.busCaseId = busCaseId;
    }

    public char getProcessInd() {
        return processInd;
    }

    public void setProcessInd(char processInd) {
        this.processInd = processInd;
    }

}