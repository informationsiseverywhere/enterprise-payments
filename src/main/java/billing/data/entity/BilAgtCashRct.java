package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilAgtCashRctId;

@Entity
@Table(name = "BIL_AGT_CASH_RCT")
public class BilAgtCashRct implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilAgtCashRctId bilAgtCashRctId;

    @Column(name = "BIL_ENTRY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEntryDt;

    @Column(name = "BIL_ENTRY_NBR", length = 4, nullable = false)
    private String bilEntryNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_ENTRY_SEQ_NBR", length = 5, nullable = false)
    private short bilEntrySeqNbr = SHORT_ZERO;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_AMT", precision = 14, scale = 2, nullable = false)
    private double bilRctAmt = DECIMAL_ZERO;

    @Column(name = "BIL_RCT_ID", length = 10, nullable = false)
    private String bilRctId = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_TYPE_CD", length = 2, nullable = false)
    private String bilRctTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_RECEIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilRctReceive;

    @Column(name = "BIL_DEPOSIT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilDepositDt;

    @Column(name = "BIL_BANK_CD", length = 3, nullable = false)
    private String bilBankCd = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_TYPE_CD", nullable = false)
    private char bilCrcrdTypeCd = BLANK_CHAR;

    @Column(name = "BIL_CRCRD_ACT_NBR", length = 25, nullable = false)
    private String bilCrcrdActNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_EXP_DT", length = 4, nullable = false)
    private String bilCrcrdExpDt = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_AUT_APV", length = 6, nullable = false)
    private String bilCrcrdAutApv = SEPARATOR_BLANK;

    @Column(name = "BIL_CSH_ETR_MTH_CD", nullable = false)
    private char bilCshEtrMthCd = BLANK_CHAR;

    @Column(name = "BIL_TAX_YEAR_NBR", length = 5, nullable = false)
    private short bilTaxYearNbr = SHORT_ZERO;

    @Column(name = "BIL_TO_FRO_TRF_NBR", length = 30, nullable = false)
    private String bilToFroTrfNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_TRF_TYPE_CD", nullable = false)
    private char bilTrfTypeCd = BLANK_CHAR;

    @Column(name = "BIL_PSTMRK_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilPstmrkDt;

    @Column(name = "BIL_PSTMRK_CD", nullable = false)
    private char bilPstmrkCd = BLANK_CHAR;

    @Column(name = "BIL_RCT_CMT", length = 80, nullable = false)
    private String bilRctCmt = SEPARATOR_BLANK;

    public BilAgtCashRctId getBilAgtCashRctId() {
        return bilAgtCashRctId;
    }

    public void setBilAgtCashRctId(BilAgtCashRctId bilAgtCashRctId) {
        this.bilAgtCashRctId = bilAgtCashRctId;
    }

    public ZonedDateTime getBilEntryDt() {
        return bilEntryDt;
    }

    public void setBilEntryDt(ZonedDateTime bilEntryDt) {
        this.bilEntryDt = bilEntryDt;
    }

    public String getBilEntryNbr() {
        return bilEntryNbr;
    }

    public void setBilEntryNbr(String bilEntryNbr) {
        this.bilEntryNbr = bilEntryNbr;
    }

    public short getBilEntrySeqNbr() {
        return bilEntrySeqNbr;
    }

    public void setBilEntrySeqNbr(short bilEntrySeqNbr) {
        this.bilEntrySeqNbr = bilEntrySeqNbr;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getBilRctAmt() {
        return bilRctAmt;
    }

    public void setBilRctAmt(double bilRctAmt) {
        this.bilRctAmt = bilRctAmt;
    }

    public String getBilRctId() {
        return bilRctId;
    }

    public void setBilRctId(String bilRctId) {
        this.bilRctId = bilRctId;
    }

    public String getBilRctTypeCd() {
        return bilRctTypeCd;
    }

    public void setBilRctTypeCd(String bilRctTypeCd) {
        this.bilRctTypeCd = bilRctTypeCd;
    }

    public ZonedDateTime getBilRctReceive() {
        return bilRctReceive;
    }

    public void setBilRctReceive(ZonedDateTime bilRctReceive) {
        this.bilRctReceive = bilRctReceive;
    }

    public ZonedDateTime getBilDepositDt() {
        return bilDepositDt;
    }

    public void setBilDepositDt(ZonedDateTime bilDepositDt) {
        this.bilDepositDt = bilDepositDt;
    }

    public String getBilBankCd() {
        return bilBankCd;
    }

    public void setBilBankCd(String bilBankCd) {
        this.bilBankCd = bilBankCd;
    }

    public char getBilCrcrdTypeCd() {
        return bilCrcrdTypeCd;
    }

    public void setBilCrcrdTypeCd(char bilCrcrdTypeCd) {
        this.bilCrcrdTypeCd = bilCrcrdTypeCd;
    }

    public String getBilCrcrdActNbr() {
        return bilCrcrdActNbr;
    }

    public void setBilCrcrdActNbr(String bilCrcrdActNbr) {
        this.bilCrcrdActNbr = bilCrcrdActNbr;
    }

    public String getBilCrcrdExpDt() {
        return bilCrcrdExpDt;
    }

    public void setBilCrcrdExpDt(String bilCrcrdExpDt) {
        this.bilCrcrdExpDt = bilCrcrdExpDt;
    }

    public String getBilCrcrdAutApv() {
        return bilCrcrdAutApv;
    }

    public void setBilCrcrdAutApv(String bilCrcrdAutApv) {
        this.bilCrcrdAutApv = bilCrcrdAutApv;
    }

    public char getBilCshEtrMthCd() {
        return bilCshEtrMthCd;
    }

    public void setBilCshEtrMthCd(char bilCshEtrMthCd) {
        this.bilCshEtrMthCd = bilCshEtrMthCd;
    }

    public short getBilTaxYearNbr() {
        return bilTaxYearNbr;
    }

    public void setBilTaxYearNbr(short bilTaxYearNbr) {
        this.bilTaxYearNbr = bilTaxYearNbr;
    }

    public String getBilToFroTrfNbr() {
        return bilToFroTrfNbr;
    }

    public void setBilToFroTrfNbr(String bilToFroTrfNbr) {
        this.bilToFroTrfNbr = bilToFroTrfNbr;
    }

    public char getBilTrfTypeCd() {
        return bilTrfTypeCd;
    }

    public void setBilTrfTypeCd(char bilTrfTypeCd) {
        this.bilTrfTypeCd = bilTrfTypeCd;
    }

    public ZonedDateTime getBilPstmrkDt() {
        return bilPstmrkDt;
    }

    public void setBilPstmrkDt(ZonedDateTime bilPstmrkDt) {
        this.bilPstmrkDt = bilPstmrkDt;
    }

    public char getBilPstmrkCd() {
        return bilPstmrkCd;
    }

    public void setBilPstmrkCd(char bilPstmrkCd) {
        this.bilPstmrkCd = bilPstmrkCd;
    }

    public String getBilRctCmt() {
        return bilRctCmt;
    }

    public void setBilRctCmt(String bilRctCmt) {
        this.bilRctCmt = bilRctCmt;
    }

}
