package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilMstCoStId;

@Entity
@Table(name = "BIL_MST_CO_ST")
public class BilMstCoSt implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilMstCoStId bilMstCoStId;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_STATE_PVN_CD", length = 3, nullable = false)
    private String bilStatePvnCd = SEPARATOR_BLANK;

    public BilMstCoStId getBilMstCoStId() {
        return bilMstCoStId;
    }

    public void setBilMstCoStId(BilMstCoStId bilMstCoStId) {
        this.bilMstCoStId = bilMstCoStId;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public String getBilStatePvnCd() {
        return bilStatePvnCd;
    }

    public void setBilStatePvnCd(String bilStatePvnCd) {
        this.bilStatePvnCd = bilStatePvnCd;
    }

}
