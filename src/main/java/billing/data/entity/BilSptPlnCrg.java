package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import billing.data.entity.id.BilSptPlnCrgId;

@Entity
@Table(name = "BIL_SPT_PLN_CRG")
public class BilSptPlnCrg implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilSptPlnCrgId bilSptPlnCrgId;

    @Column(name = "BIL_SPT_SCG_IND", nullable = false)
    private char bilSptScgInd = BLANK_CHAR;

    @Column(name = "BIL_SCG_TYPE_CD", length = 2, nullable = false)
    private String bilScgTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_SCG_ACTION_CD", nullable = false)
    private char bilScgActionCd = BLANK_CHAR;

    @Column(name = "BIL_SVC_CRG_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSvcCrgAmt = DECIMAL_ZERO;

    @Column(name = "BIL_LATE_CRG_AMT", precision = 14, scale = 2, nullable = false)
    private double bilLateCrgAmt = DECIMAL_ZERO;

    @Column(name = "BIL_LATE_CRG_PCT", precision = 5, scale = 2, nullable = false)
    private double bilLateCrgPct = DECIMAL_ZERO;

    @Column(name = "BIL_NSF_PNY_AMT", precision = 14, scale = 2, nullable = false)
    private double bilNsfPnyAmt = DECIMAL_ZERO;

    @Column(name = "BIL_PRM_AMT", precision = 14, scale = 2, nullable = false)
    private double bilPrmAmt = DECIMAL_ZERO;

    @Column(name = "BIL_SCG_ICR_AMT", precision = 14, scale = 2, nullable = false)
    private double bilScgIcrAmt = DECIMAL_ZERO;

    @Column(name = "BIL_PRM_ICR_AMT", precision = 14, scale = 2, nullable = false)
    private double bilPrmIcrAmt = DECIMAL_ZERO;

    @Column(name = "BIL_MAX_SCG_AMT", precision = 14, scale = 2, nullable = false)
    private double bilMaxScgAmt = DECIMAL_ZERO;

    @Column(name = "LAST_MDF_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime lastMdfAcyTs;

    public BilSptPlnCrgId getBilSptPlnCrgId() {
        return bilSptPlnCrgId;
    }

    public void setBilSptPlnCrgId(BilSptPlnCrgId bilSptPlnCrgId) {
        this.bilSptPlnCrgId = bilSptPlnCrgId;
    }

    public char getBilSptScgInd() {
        return bilSptScgInd;
    }

    public void setBilSptScgInd(char bilSptScgInd) {
        this.bilSptScgInd = bilSptScgInd;
    }

    public String getBilScgTypeCd() {
        return bilScgTypeCd;
    }

    public void setBilScgTypeCd(String bilScgTypeCd) {
        this.bilScgTypeCd = bilScgTypeCd;
    }

    public char getBilScgActionCd() {
        return bilScgActionCd;
    }

    public void setBilScgActionCd(char bilScgActionCd) {
        this.bilScgActionCd = bilScgActionCd;
    }

    public double getBilSvcCrgAmt() {
        return bilSvcCrgAmt;
    }

    public void setBilSvcCrgAmt(double bilSvcCrgAmt) {
        this.bilSvcCrgAmt = bilSvcCrgAmt;
    }

    public double getBilLateCrgAmt() {
        return bilLateCrgAmt;
    }

    public void setBilLateCrgAmt(double bilLateCrgAmt) {
        this.bilLateCrgAmt = bilLateCrgAmt;
    }

    public double getBilLateCrgPct() {
        return bilLateCrgPct;
    }

    public void setBilLateCrgPct(double bilLateCrgPct) {
        this.bilLateCrgPct = bilLateCrgPct;
    }

    public double getBilNsfPnyAmt() {
        return bilNsfPnyAmt;
    }

    public void setBilNsfPnyAmt(double bilNsfPnyAmt) {
        this.bilNsfPnyAmt = bilNsfPnyAmt;
    }

    public double getBilPrmAmt() {
        return bilPrmAmt;
    }

    public void setBilPrmAmt(double bilPrmAmt) {
        this.bilPrmAmt = bilPrmAmt;
    }

    public double getBilScgIcrAmt() {
        return bilScgIcrAmt;
    }

    public void setBilScgIcrAmt(double bilScgIcrAmt) {
        this.bilScgIcrAmt = bilScgIcrAmt;
    }

    public double getBilPrmIcrAmt() {
        return bilPrmIcrAmt;
    }

    public void setBilPrmIcrAmt(double bilPrmIcrAmt) {
        this.bilPrmIcrAmt = bilPrmIcrAmt;
    }

    public double getBilMaxScgAmt() {
        return bilMaxScgAmt;
    }

    public void setBilMaxScgAmt(double bilMaxScgAmt) {
        this.bilMaxScgAmt = bilMaxScgAmt;
    }

    public ZonedDateTime getLastMdfAcyTs() {
        return lastMdfAcyTs;
    }

    public void setLastMdfAcyTs(ZonedDateTime lastMdfAcyTs) {
        this.lastMdfAcyTs = lastMdfAcyTs;
    }

}
