package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilTtySeasExclId;

@Entity
@Table(name = "BIL_TTY_SEAS_EXCL")
public class BilTtySeasExcl implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilTtySeasExclId bilTtySeasExclId;

    @Column(name = "BIL_SEA_END_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilSeaEndDt;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "LAST_MDF_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime lastMdfAcyTs;

    public BilTtySeasExcl(BilTtySeasExclId bilTtySeasExclId, ZonedDateTime bilSeaEndDt, String userId,
            ZonedDateTime lastMdfAcyTs) {
        super();
        this.bilTtySeasExclId = bilTtySeasExclId;
        this.bilSeaEndDt = bilSeaEndDt;
        this.userId = userId;
        this.lastMdfAcyTs = lastMdfAcyTs;
    }

    public BilTtySeasExcl() {
        super();
    }

    public BilTtySeasExclId getBilTtySeasExclId() {
        return bilTtySeasExclId;
    }

    public void setBilTtySeasExclId(BilTtySeasExclId bilTtySeasExclId) {
        this.bilTtySeasExclId = bilTtySeasExclId;
    }

    public ZonedDateTime getBilSeaEndDt() {
        return bilSeaEndDt;
    }

    public void setBilSeaEndDt(ZonedDateTime bilSeaEndDt) {
        this.bilSeaEndDt = bilSeaEndDt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ZonedDateTime getLastMdfAcyTs() {
        return lastMdfAcyTs;
    }

    public void setLastMdfAcyTs(ZonedDateTime lastMdfAcyTs) {
        this.lastMdfAcyTs = lastMdfAcyTs;
    }

}
