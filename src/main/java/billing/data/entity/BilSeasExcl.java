package billing.data.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilSeasExclId;

@Entity
@Table(name = "BIL_SEAS_EXCL")
public class BilSeasExcl implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilSeasExclId bilSeasExclId;

    @Column(name = "BIL_SEA_END_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilSeaEndDt;

    public BilSeasExclId getBilSeasExclId() {
        return bilSeasExclId;
    }

    public void setBilSeasExclId(BilSeasExclId bilSeasExclId) {
        this.bilSeasExclId = bilSeasExclId;
    }

    public ZonedDateTime getBilSeaEndDt() {
        return bilSeaEndDt;
    }

    public void setBilSeaEndDt(ZonedDateTime bilSeaEndDt) {
        this.bilSeaEndDt = bilSeaEndDt;
    }

}
