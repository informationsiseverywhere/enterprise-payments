package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilStRulesUctId;

@Entity
@Table(name = "BIL_ST_RULES_UCT")
public class BilStRulesUct implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilStRulesUctId bilStRulesUctId;

    @Column(name = "EXPIRATION_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime expirationDt;

    @Column(name = "BIL_RULE_CD", nullable = false)
    private char bilRuleCd;

    @Column(name = "BIL_RULE_DES", length = 100, nullable = false)
    private String bilRuleDes = SEPARATOR_BLANK;

    @Column(name = "BIL_PARM_LIST_TXT", length = 100, nullable = false)
    private String bilParmListTxt = SEPARATOR_BLANK;

    public BilStRulesUct() {
        super();
    }

    public BilStRulesUct(BilStRulesUctId bilStRulesUctId, ZonedDateTime expirationDt, char bilRuleCd, String bilRuleDes,
            String bilParmListTxt) {
        super();
        this.bilStRulesUctId = bilStRulesUctId;
        this.expirationDt = expirationDt;
        this.bilRuleCd = bilRuleCd;
        this.bilRuleDes = bilRuleDes;
        this.bilParmListTxt = bilParmListTxt;
    }

    public BilStRulesUctId getBilStRulesUctId() {
        return bilStRulesUctId;
    }

    public void setBilStRulesUctId(BilStRulesUctId bilStRulesUctId) {
        this.bilStRulesUctId = bilStRulesUctId;
    }

    public ZonedDateTime getExpirationDt() {
        return expirationDt;
    }

    public void setExpirationDt(ZonedDateTime expirationDt) {
        this.expirationDt = expirationDt;
    }

    public char getBilRuleCd() {
        return bilRuleCd;
    }

    public void setBilRuleCd(char bilRuleCd) {
        this.bilRuleCd = bilRuleCd;
    }

    public String getBilRuleDes() {
        return bilRuleDes;
    }

    public void setBilRuleDes(String bilRuleDes) {
        this.bilRuleDes = bilRuleDes;
    }

    public String getBilParmListTxt() {
        return bilParmListTxt;
    }

    public void setBilParmListTxt(String bilParmListTxt) {
        this.bilParmListTxt = bilParmListTxt;
    }

}
