package billing.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilAmtSchRltId;

@Entity
@Table(name = "BIL_AMT_SCH_RLT")
public class BilAmtSchRlt implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilAmtSchRltId bilAmtSchRltId;

    @Column(name = "BIL_PRM_IST_AMT", precision = 14, scale = 2, nullable = false)
    private Double bilPrmIstAmt;

    @Column(name = "BIL_PRM_IST_NET", precision = 14, scale = 2, nullable = false)
    private Double bilPrmIstNet;

}
