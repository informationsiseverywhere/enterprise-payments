package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilTtyCashDspId;

@Entity
@Table(name = "BIL_TTY_CASH_DSP")
public class BilTtyCashDsp implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilTtyCashDspId bilTtyCashDspId;

    @Column(name = "BIL_ADJ_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAdjDueDt;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "BIL_DSP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilDspDt;

    @Column(name = "BIL_DSP_TYPE_CD", length = 2, nullable = false)
    private String bilDspTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_DSP_REASON_CD", length = 3, nullable = false)
    private String bilDspReasonCd = SEPARATOR_BLANK;

    @Column(name = "BIL_DSP_AMT", precision = 14, scale = 2, nullable = false)
    private double bilDspAmt;

    @Column(name = "BIL_PAYEE_CLT_ID", length = 20, nullable = false)
    private String bilPayeeCltId = SEPARATOR_BLANK;

    @Column(name = "BIL_PAYEE_ADR_SEQ", length = 5, nullable = false)
    private short bilPayeeAdrSeq = SHORT_ZERO;

    @Column(name = "BIL_MANUAL_SUS_IND", nullable = false)
    private char bilManualSusInd = BLANK_CHAR;

    @Column(name = "DWS_CK_DRF_NBR", length = 10, nullable = false)
    private int dwsCkDrfNbr = 0;

    @Column(name = "BIL_REVS_RSUS_IND", nullable = false)
    private char bilRevsRsusInd = BLANK_CHAR;

    @Column(name = "BIL_TO_FRO_TRF_NBR", length = 20, nullable = false)
    private String bilToFroTrfNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_TRF_TYPE_CD", nullable = false)
    private char bilTrfTypeCd = BLANK_CHAR;

    @Column(name = "DDS_DSB_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ddsDsbDt;

    @Column(name = "DWS_STATUS_CD", nullable = false)
    private char dwsStatusCd = BLANK_CHAR;

    @Column(name = "BIL_DSB_ID", length = 20, nullable = false)
    private String bilDsbId = SEPARATOR_BLANK;

    @Column(name = "BIL_CHK_PRD_MTH_CD", nullable = false)
    private char bilChkPrdMthCd = BLANK_CHAR;

    public BilTtyCashDspId getBilTtyCashDspId() {
        return bilTtyCashDspId;
    }

    public void setBilTtyCashDspId(BilTtyCashDspId bilTtyCashDspId) {
        this.bilTtyCashDspId = bilTtyCashDspId;
    }

    public ZonedDateTime getBilAdjDueDt() {
        return bilAdjDueDt;
    }

    public void setBilAdjDueDt(ZonedDateTime bilAdjDueDt) {
        this.bilAdjDueDt = bilAdjDueDt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ZonedDateTime getBilDspDt() {
        return bilDspDt;
    }

    public void setBilDspDt(ZonedDateTime bilDspDt) {
        this.bilDspDt = bilDspDt;
    }

    public String getBilDspTypeCd() {
        return bilDspTypeCd;
    }

    public void setBilDspTypeCd(String bilDspTypeCd) {
        this.bilDspTypeCd = bilDspTypeCd;
    }

    public String getBilDspReasonCd() {
        return bilDspReasonCd;
    }

    public void setBilDspReasonCd(String bilDspReasonCd) {
        this.bilDspReasonCd = bilDspReasonCd;
    }

    public double getBilDspAmt() {
        return bilDspAmt;
    }

    public void setBilDspAmt(double bilDspAmt) {
        this.bilDspAmt = bilDspAmt;
    }

    public String getBilPayeeCltId() {
        return bilPayeeCltId;
    }

    public void setBilPayeeCltId(String bilPayeeCltId) {
        this.bilPayeeCltId = bilPayeeCltId;
    }

    public short getBilPayeeAdrSeq() {
        return bilPayeeAdrSeq;
    }

    public void setBilPayeeAdrSeq(short bilPayeeAdrSeq) {
        this.bilPayeeAdrSeq = bilPayeeAdrSeq;
    }

    public char getBilManualSusInd() {
        return bilManualSusInd;
    }

    public void setBilManualSusInd(char bilManualSusInd) {
        this.bilManualSusInd = bilManualSusInd;
    }

    public int getDwsCkDrfNbr() {
        return dwsCkDrfNbr;
    }

    public void setDwsCkDrfNbr(int dwsCkDrfNbr) {
        this.dwsCkDrfNbr = dwsCkDrfNbr;
    }

    public char getBilRevsRsusInd() {
        return bilRevsRsusInd;
    }

    public void setBilRevsRsusInd(char bilRevsRsusInd) {
        this.bilRevsRsusInd = bilRevsRsusInd;
    }

    public String getBilToFroTrfNbr() {
        return bilToFroTrfNbr;
    }

    public void setBilToFroTrfNbr(String bilToFroTrfNbr) {
        this.bilToFroTrfNbr = bilToFroTrfNbr;
    }

    public char getBilTrfTypeCd() {
        return bilTrfTypeCd;
    }

    public void setBilTrfTypeCd(char bilTrfTypeCd) {
        this.bilTrfTypeCd = bilTrfTypeCd;
    }

    public ZonedDateTime getDdsDsbDt() {
        return ddsDsbDt;
    }

    public void setDdsDsbDt(ZonedDateTime ddsDsbDt) {
        this.ddsDsbDt = ddsDsbDt;
    }

    public char getDwsStatusCd() {
        return dwsStatusCd;
    }

    public void setDwsStatusCd(char dwsStatusCd) {
        this.dwsStatusCd = dwsStatusCd;
    }

    public String getBilDsbId() {
        return bilDsbId;
    }

    public void setBilDsbId(String bilDsbId) {
        this.bilDsbId = bilDsbId;
    }

    public char getBilChkPrdMthCd() {
        return bilChkPrdMthCd;
    }

    public void setBilChkPrdMthCd(char bilChkPrdMthCd) {
        this.bilChkPrdMthCd = bilChkPrdMthCd;
    }

}
