package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilEftActivityId;

@Entity
@Table(name = "BIL_EFT_ACTIVITY")
public class BilEftActivity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilEftActivityId bilEftActivityId;

    @Column(name = "BIL_EFT_TAPE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEftTapeDt;

    @Column(name = "BIL_EFT_DR_AMT", precision = 14, scale = 2, nullable = false)
    private double bilEftDrAmt;

    @Column(name = "BIL_EFT_NOT_IND", nullable = false)
    private char bilEftNotInd;

    @Column(name = "BEA_TRS_STA", nullable = false)
    private char beaTrsSta;

    @Column(name = "BIL_EFT_ACY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEftAcyDt;

    @Column(name = "BIL_EFT_RCD_TYC", nullable = false)
    private char bilEftRcdTyc;

    @Column(name = "BIL_FILE_TYPE_CD", length = 5, nullable = false)
    private short bilFileTypeCd;

    @Column(name = "BIL_DSB_ID", length = 20, nullable = false)
    private String bilDsbId = SEPARATOR_BLANK;

    @Column(name = "WEB_AUT_IND", nullable = false)
    private char webAutInd;

    @Column(name = "AGENCY_NBR", length = 12, nullable = false)
    private String agencyNbr = SEPARATOR_BLANK;

    @Column(name = "INSERTED_ROW_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime insertedRowTs;

    @Column(name = "BIL_PMT_ORDER_ID", length = 22, nullable = false)
    private String bilPmtOrderId = SEPARATOR_BLANK;

    @Column(name = "BIL_BUS_ACT_CD", nullable = false)
    private char bilBusActCd;
    
    @Column(name = "BIL_BUS_GRP_CD", length = 3, nullable = false)
    private String bilBusGrpCd = SEPARATOR_BLANK;

    public BilEftActivityId getBilEftActivityId() {
        return bilEftActivityId;
    }

    public void setBilEftActivityId(BilEftActivityId bilEftActivityId) {
        this.bilEftActivityId = bilEftActivityId;
    }

    public ZonedDateTime getBilEftTapeDt() {
        return bilEftTapeDt;
    }

    public void setBilEftTapeDt(ZonedDateTime bilEftTapeDt) {
        this.bilEftTapeDt = bilEftTapeDt;
    }

    public double getBilEftDrAmt() {
        return bilEftDrAmt;
    }

    public void setBilEftDrAmt(double bilEftDrAmt) {
        this.bilEftDrAmt = bilEftDrAmt;
    }

    public char getBilEftNotInd() {
        return bilEftNotInd;
    }

    public void setBilEftNotInd(char bilEftNotInd) {
        this.bilEftNotInd = bilEftNotInd;
    }

    public char getBeaTrsSta() {
        return beaTrsSta;
    }

    public void setBeaTrsSta(char beaTrsSta) {
        this.beaTrsSta = beaTrsSta;
    }

    public ZonedDateTime getBilEftAcyDt() {
        return bilEftAcyDt;
    }

    public void setBilEftAcyDt(ZonedDateTime bilEftAcyDt) {
        this.bilEftAcyDt = bilEftAcyDt;
    }

    public char getBilEftRcdTyc() {
        return bilEftRcdTyc;
    }

    public void setBilEftRcdTyc(char bilEftRcdTyc) {
        this.bilEftRcdTyc = bilEftRcdTyc;
    }

    public short getBilFileTypeCd() {
        return bilFileTypeCd;
    }

    public void setBilFileTypeCd(short bilFileTypeCd) {
        this.bilFileTypeCd = bilFileTypeCd;
    }

    public String getBilDsbId() {
        return bilDsbId;
    }

    public void setBilDsbId(String bilDsbId) {
        this.bilDsbId = bilDsbId;
    }

    public char getWebAutInd() {
        return webAutInd;
    }

    public void setWebAutInd(char webAutInd) {
        this.webAutInd = webAutInd;
    }

    public String getAgencyNbr() {
        return agencyNbr;
    }

    public void setAgencyNbr(String agencyNbr) {
        this.agencyNbr = agencyNbr;
    }

    public ZonedDateTime getInsertedRowTs() {
        return insertedRowTs;
    }

    public void setInsertedRowTs(ZonedDateTime insertedRowTs) {
        this.insertedRowTs = insertedRowTs;
    }

    public String getBilPmtOrderId() {
        return bilPmtOrderId;
    }

    public void setBilPmtOrderId(String bilPmtOrderId) {
        this.bilPmtOrderId = bilPmtOrderId;
    }

    public char getBilBusActCd() {
        return bilBusActCd;
    }

    public void setBilBusActCd(char bilBusActCd) {
        this.bilBusActCd = bilBusActCd;
    }
    
    public String getBilBusGrpCd() {
        return bilBusGrpCd;
    }

    public void setBilBusGrpCd(String bilBusGrpCd) {
        this.bilBusGrpCd = bilBusGrpCd;
    }

}
