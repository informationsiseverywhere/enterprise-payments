package billing.data.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilScheduleCshId;

/**
 * The persistent class for the BIL_SCHEDULE_CSH database table.
 *
 */
@Entity
@Table(name = "BIL_SCHEDULE_CSH")
public class BilScheduleCsh implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilScheduleCshId bilScheduleCshId;

    @Column(name = "BIL_ENTRY_DT")
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEntryDt;

    @Column(name = "BIL_ENTRY_NBR", length = 4, nullable = false)
    private String bilEntryNbr;

    @Column(name = "BIL_ENTRY_SEQ_NBR", nullable = false)
    private Short bilEntrySeqNbr;

    @Column(name = "PROCESS_IND", nullable = false)
    private Character processInd;

    @Column(name = "REFERENCE_ID", length = 10, nullable = false)
    private String referenceId;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId;

    public BilScheduleCsh() {
    	super();
    }

    public BilScheduleCshId getBilScheduleCshIdId() {
        return this.bilScheduleCshId;
    }

    public void setBilScheduleCshId(BilScheduleCshId bilScheduleCshId) {
        this.bilScheduleCshId = bilScheduleCshId;
    }

    public ZonedDateTime getBilEntryDt() {
        return this.bilEntryDt;
    }

    public void setBilEntryDt(ZonedDateTime bilEntryDt) {
        this.bilEntryDt = bilEntryDt;
    }

    public String getBilEntryNbr() {
        return this.bilEntryNbr;
    }

    public void setBilEntryNbr(String bilEntryNbr) {
        this.bilEntryNbr = bilEntryNbr;
    }

    public Short getBilEntrySeqNbr() {
        return this.bilEntrySeqNbr;
    }

    public void setBilEntrySeqNbr(Short bilEntrySeqNbr) {
        this.bilEntrySeqNbr = bilEntrySeqNbr;
    }

    public Character getProcessInd() {
        return this.processInd;
    }

    public void setProcessInd(Character processInd) {
        this.processInd = processInd;
    }

    public String getReferenceId() {
        return this.referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}