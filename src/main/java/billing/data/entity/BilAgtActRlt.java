package billing.data.entity;

import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilAgtActRltId;

@Entity
@Table(name = "BIL_AGT_ACT_RLT")
public class BilAgtActRlt implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilAgtActRltId bilAgtActRltId;

    @Column(name = "BIL_ADDITIONAL_ID", length = 20, nullable = false)
    private String additionalId = SEPARATOR_BLANK;

    @Column(name = "BIL_TOT_CREDIT_AMT", precision = 14, scale = 2, nullable = false)
    private double bilTotCreditAmt = DECIMAL_ZERO;

    @Column(name = "BIL_AVL_CREDIT_AMT", precision = 14, scale = 2, nullable = false)
    private double bilAvlCreditAmt = DECIMAL_ZERO;

    public BilAgtActRlt() {

    }

    public BilAgtActRlt(BilAgtActRltId bilAgtActRltId, String additionalId, double bilTotCreditAmt,
            double bilAvlCreditAmt) {
        super();
        this.bilAgtActRltId = bilAgtActRltId;
        this.additionalId = additionalId;
        this.bilTotCreditAmt = bilTotCreditAmt;
        this.bilAvlCreditAmt = bilAvlCreditAmt;
    }

    public BilAgtActRltId getBilAgtActRltId() {
        return bilAgtActRltId;
    }

    public void setBilAgtActRltId(BilAgtActRltId bilAgtActRltId) {
        this.bilAgtActRltId = bilAgtActRltId;
    }

    public String getAdditionalId() {
        return additionalId;
    }

    public void setAdditionalId(String additionalId) {
        this.additionalId = additionalId;
    }

    public double getBilTotCreditAmt() {
        return bilTotCreditAmt;
    }

    public void setBilTotCreditAmt(double bilTotCreditAmt) {
        this.bilTotCreditAmt = bilTotCreditAmt;
    }

    public double getBilAvlCreditAmt() {
        return bilAvlCreditAmt;
    }

    public void setBilAvlCreditAmt(double bilAvlCreditAmt) {
        this.bilAvlCreditAmt = bilAvlCreditAmt;
    }

}