package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilOrgSupportId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_FILE_TYPE_CD", length = 5, nullable = false)
    private Short bilFileTypeCd;

    @Column(name = "PRI_LGG_CD", length = 3, nullable = false)
    private String priLggCd = SEPARATOR_BLANK;

    public BilOrgSupportId() {
        super();
    }

    public BilOrgSupportId(Short bilFileTypeCd, String priLggCd) {
        super();
        this.bilFileTypeCd = bilFileTypeCd;
        this.priLggCd = priLggCd;
    }

    public Short getBilFileTypeCd() {
        return bilFileTypeCd;
    }

    public void setBilFileTypeCd(Short bilFileTypeCd) {
        this.bilFileTypeCd = bilFileTypeCd;
    }

    public String getPriLggCd() {
        return priLggCd;
    }

    public void setPriLggCd(String priLggCd) {
        this.priLggCd = priLggCd;
    }

}
