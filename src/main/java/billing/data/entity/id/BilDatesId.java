package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilDatesId implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ACCOUNT_ID", nullable = false)
    private String bilAccountId = SEPARATOR_BLANK;

    @Column(name = "BIL_REFERENCE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilReferenceDt;

    public BilDatesId() {
        super();
    }

    public BilDatesId(String bilAccountId, ZonedDateTime bilReferenceDt) {
        super();
        this.bilAccountId = bilAccountId;
        this.bilReferenceDt = bilReferenceDt;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public ZonedDateTime getBilReferenceDt() {
        return bilReferenceDt;
    }

    public void setBilReferenceDt(ZonedDateTime bilReferenceDt) {
        this.bilReferenceDt = bilReferenceDt;
    }

}
