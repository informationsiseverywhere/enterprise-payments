package billing.data.entity.id;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilEftRejReaId implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Column(name = "BIL_EFT_REJ_CD", nullable = false)
    private String bilEftRejCd;
    
    @Column(name = "BIL_COLLECTION_PLN", nullable = false)
    private String bilCollectionPln;
    
    @Column(name = "PRI_LGG_CD", nullable = false)
    private String priLggCd;

    public String getBilEftRejCd() {
        return bilEftRejCd;
    }

    public void setBilEftRejCd(String bilEftRejCd) {
        this.bilEftRejCd = bilEftRejCd;
    }

    public String getBilCollectionPln() {
        return bilCollectionPln;
    }

    public void setBilCollectionPln(String bilCollectionPln) {
        this.bilCollectionPln = bilCollectionPln;
    }

    public String getPriLggCd() {
        return priLggCd;
    }

    public void setPriLggCd(String priLggCd) {
        this.priLggCd = priLggCd;
    }
    
    
}
