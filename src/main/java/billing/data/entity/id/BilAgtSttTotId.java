package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilAgtSttTotId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_AGT_ACT_ID", length = 8, nullable = false)
    private String bilAgtActId = SEPARATOR_BLANK;

    @Column(name = "BIL_ACCOUNT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAccountDt;

    @Column(name = "BIL_STT_SEQ_NBR", length = 5, nullable = false)
    private short bilDtbSeqNbr = SHORT_ZERO;

    public BilAgtSttTotId() {
        super();
    }

    public BilAgtSttTotId(String bilAgtActId, ZonedDateTime bilAccountDt, short bilDtbSeqNbr) {
        super();
        this.bilAgtActId = bilAgtActId;
        this.bilAccountDt = bilAccountDt;
        this.bilDtbSeqNbr = bilDtbSeqNbr;
    }

    public String getBilAgtActId() {
        return bilAgtActId;
    }

    public void setBilAgtActId(String bilAgtActId) {
        this.bilAgtActId = bilAgtActId;
    }

    public ZonedDateTime getBilAccountDt() {
        return bilAccountDt;
    }

    public void setBilAccountDt(ZonedDateTime bilAccountDt) {
        this.bilAccountDt = bilAccountDt;
    }

    public short getBilDtbSeqNbr() {
        return bilDtbSeqNbr;
    }

    public void setBilDtbSeqNbr(short bilDtbSeqNbr) {
        this.bilDtbSeqNbr = bilDtbSeqNbr;
    }

}
