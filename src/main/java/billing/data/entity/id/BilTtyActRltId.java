package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilTtyActRltId implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_THIRD_PARTY_ID", length = 8, nullable = false)
    private String bilThirdPartyId = SEPARATOR_BLANK;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String bilAccountId = SEPARATOR_BLANK;

    public BilTtyActRltId() {
        super();
    }

    public BilTtyActRltId(String bilThirdPartyId, String bilAccountId) {
        super();
        this.bilThirdPartyId = bilThirdPartyId;
        this.bilAccountId = bilAccountId;
    }

    public String getBilThirdPartyId() {
        return bilThirdPartyId;
    }

    public void setBilThirdPartyId(String bilThirdPartyId) {
        this.bilThirdPartyId = bilThirdPartyId;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

}
