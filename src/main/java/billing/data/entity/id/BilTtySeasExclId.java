package billing.data.entity.id;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilTtySeasExclId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_TCH_KEY_ID", length = 8, nullable = false)
    private String bilTchKeyId = SEPARATOR_BLANK;

    @Column(name = "BIL_TCH_KEY_TYP_CD", nullable = false)
    private char bilTchKeyTypCd = BLANK_CHAR;

    @Column(name = "BIL_SEA_XCL_CD", length = 10, nullable = false)
    private String bilSeaXclCd = SEPARATOR_BLANK;

    @Column(name = "HISTORY_VLD_NBR")
    private short historyVldNbr = SHORT_ZERO;

    @Column(name = "BIL_SEA_START_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilSeaStartDt;

    public BilTtySeasExclId(String bilTchKeyId, char bilTchKeyTypCd, String bilSeaXclCd, ZonedDateTime bilSeaStartDt) {
        super();
        this.bilTchKeyId = bilTchKeyId;
        this.bilTchKeyTypCd = bilTchKeyTypCd;
        this.bilSeaXclCd = bilSeaXclCd;
        this.bilSeaStartDt = bilSeaStartDt;
    }

    public BilTtySeasExclId() {
        super();
    }

    public String getBilTchKeyId() {
        return bilTchKeyId;
    }

    public void setBilTchKeyId(String bilTchKeyId) {
        this.bilTchKeyId = bilTchKeyId;
    }

    public char getBilTchKeyTypCd() {
        return bilTchKeyTypCd;
    }

    public void setBilTchKeyTypCd(char bilTchKeyTypCd) {
        this.bilTchKeyTypCd = bilTchKeyTypCd;
    }

    public String getBilSeaXclCd() {
        return bilSeaXclCd;
    }

    public void setBilSeaXclCd(String bilSeaXclCd) {
        this.bilSeaXclCd = bilSeaXclCd;
    }

    public short getHistoryVldNbr() {
        return historyVldNbr;
    }

    public void setHistoryVldNbr(short historyVldNbr) {
        this.historyVldNbr = historyVldNbr;
    }

    public ZonedDateTime getBilSeaStartDt() {
        return bilSeaStartDt;
    }

    public void setBilSeaStartDt(ZonedDateTime bilSeaStartDt) {
        this.bilSeaStartDt = bilSeaStartDt;
    }

}
