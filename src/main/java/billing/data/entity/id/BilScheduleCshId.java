package billing.data.entity.id;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the BIL_SCHEDULE_CSH database table.
 *
 */
@Embeddable
public class BilScheduleCshId implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String bilAccountId;

    @Column(name = "BCH_CSH_PRC_CD", nullable = false)
    private Character bchCshPrcCd;

    @Column(name = "BIL_SEQ_NBR", nullable = false)
    private Short bilSeqNbr;

    public BilScheduleCshId() {
        super();
    }

    public String getBilAccountId() {
        return this.bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public Character getBchCshPrcCd() {
        return this.bchCshPrcCd;
    }

    public void setBchCshPrcCd(Character bchCshPrcCd) {
        this.bchCshPrcCd = bchCshPrcCd;
    }

    public short getBilSeqNbr() {
        return this.bilSeqNbr;
    }

    public void setBilSeqNbr(short bilSeqNbr) {
        this.bilSeqNbr = bilSeqNbr;
    }

    public BilScheduleCshId(String bilAccountId, Character bchCshPrcCd, Short bilSeqNbr) {
        super();
        this.bilAccountId = bilAccountId;
        this.bchCshPrcCd = bchCshPrcCd;
        this.bilSeqNbr = bilSeqNbr;
    }
}