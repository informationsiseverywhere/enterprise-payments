package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilPolicyTermId implements Serializable {

    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String bilAccountId = SEPARATOR_BLANK;

    @Column(name = "POLICY_ID", length = 16, nullable = false)
    private String policyId = SEPARATOR_BLANK;

    @Column(name = "POL_EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime polEffectiveDt;

    public BilPolicyTermId() {
        super();
    }

    public BilPolicyTermId(String bilAccountId, String policyId, ZonedDateTime polEffectiveDt) {
        super();
        this.bilAccountId = bilAccountId;
        this.policyId = policyId;
        this.polEffectiveDt = polEffectiveDt;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public ZonedDateTime getPolEffectiveDt() {
        return polEffectiveDt;
    }

    public void setPolEffectiveDt(ZonedDateTime polEffectiveDt) {
        this.polEffectiveDt = polEffectiveDt;
    }

}
