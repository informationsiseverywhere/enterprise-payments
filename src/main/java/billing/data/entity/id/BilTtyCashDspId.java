package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilTtyCashDspId implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_THIRD_PARTY_ID", length = 8, nullable = false)
    private String bilThirdPartyId = SEPARATOR_BLANK;

    @Column(name = "BIL_DTB_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilDtbDt;

    @Column(name = "BIL_DTB_SEQ_NBR", length = 5, nullable = false)
    private short bilDtbSeqNbr;

    @Column(name = "BIL_DSP_SEQ_NBR", length = 5, nullable = false)
    private short bilDspSeqNbr;

    public BilTtyCashDspId() {
        super();
    }

    public BilTtyCashDspId(String bilThirdPartyId, ZonedDateTime bilDtbDt, short bilDtbSeqNbr, short bilDspSeqNbr) {
        super();
        this.bilThirdPartyId = bilThirdPartyId;
        this.bilDtbDt = bilDtbDt;
        this.bilDtbSeqNbr = bilDtbSeqNbr;
        this.bilDspSeqNbr = bilDspSeqNbr;
    }

    public String getBilThirdPartyId() {
        return bilThirdPartyId;
    }

    public void setBilThirdPartyId(String bilThirdPartyId) {
        this.bilThirdPartyId = bilThirdPartyId;
    }

    public ZonedDateTime getBilDtbDt() {
        return bilDtbDt;
    }

    public void setBilDtbDt(ZonedDateTime bilDtbDt) {
        this.bilDtbDt = bilDtbDt;
    }

    public short getBilDtbSeqNbr() {
        return bilDtbSeqNbr;
    }

    public void setBilDtbSeqNbr(short bilDtbSeqNbr) {
        this.bilDtbSeqNbr = bilDtbSeqNbr;
    }

    public short getBilDspSeqNbr() {
        return bilDspSeqNbr;
    }

    public void setBilDspSeqNbr(short bilDspSeqNbr) {
        this.bilDspSeqNbr = bilDspSeqNbr;
    }
}
