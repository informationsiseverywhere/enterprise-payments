package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilEftActivityId implements Serializable {

    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_TCH_KEY_ID", length = 8, nullable = false)
    private String bilTchKeyId = SEPARATOR_BLANK;

    @Column(name = "BIL_TCH_KEY_TYP_CD", nullable = false)
    private char bilTchKeyTypCd;

    @Column(name = "BIL_EFT_DR_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEftDrDt;

    @Column(name = "BIL_EFT_TRACE_NBR", length = 22, nullable = false)
    private String bilEftTraceNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_EFT_PST_PMT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEftPstPmtDt;

    @Column(name = "BIL_DTB_SEQ_NBR", length = 5, nullable = false)
    private short bilDtbSeqNbr;

    public BilEftActivityId() {
        super();
    }

    public BilEftActivityId(String bilTchKeyId, char bilTchKeyTypCd, ZonedDateTime bilEftDrDt, String bilEftTraceNbr,
            ZonedDateTime bilEftPstPmtDt, short bilDtbSeqNbr) {
        super();
        this.bilTchKeyId = bilTchKeyId;
        this.bilTchKeyTypCd = bilTchKeyTypCd;
        this.bilEftDrDt = bilEftDrDt;
        this.bilEftTraceNbr = bilEftTraceNbr;
        this.bilEftPstPmtDt = bilEftPstPmtDt;
        this.bilDtbSeqNbr = bilDtbSeqNbr;
    }

    public String getBilTchKeyId() {
        return bilTchKeyId;
    }

    public void setBilTchKeyId(String bilTchKeyId) {
        this.bilTchKeyId = bilTchKeyId;
    }

    public char getBilTchKeyTypCd() {
        return bilTchKeyTypCd;
    }

    public void setBilTchKeyTypCd(char bilTchKeyTypCd) {
        this.bilTchKeyTypCd = bilTchKeyTypCd;
    }

    public ZonedDateTime getBilEftDrDt() {
        return bilEftDrDt;
    }

    public void setBilEftDrDt(ZonedDateTime bilEftDrDt) {
        this.bilEftDrDt = bilEftDrDt;
    }

    public String getBilEftTraceNbr() {
        return bilEftTraceNbr;
    }

    public void setBilEftTraceNbr(String bilEftTraceNbr) {
        this.bilEftTraceNbr = bilEftTraceNbr;
    }

    public ZonedDateTime getBilEftPstPmtDt() {
        return bilEftPstPmtDt;
    }

    public void setBilEftPstPmtDt(ZonedDateTime bilEftPstPmtDt) {
        this.bilEftPstPmtDt = bilEftPstPmtDt;
    }

    public short getBilDtbSeqNbr() {
        return bilDtbSeqNbr;
    }

    public void setBilDtbSeqNbr(short bilDtbSeqNbr) {
        this.bilDtbSeqNbr = bilDtbSeqNbr;
    }

}
