package billing.data.entity.id;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class LegalDaysNoticeId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "LOB_CD", length = 3, nullable = false)
    private String lineOfBusiness = SEPARATOR_BLANK;

    @Column(name = "POL_SYMBOL_CD", length = 3, nullable = false)
    private String policySymbol;

    @Column(name = "RISK_STATE_CD", length = 3, nullable = false)
    private String riskState;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNumber;

    @Column(name = "EFFECTIVE_TYPE_CD", nullable = false)
    private char effectiveTypeCode = BLANK_CHAR;

    @Column(name = "LDN_PROCESS_CD", length = 3, nullable = false)
    private String processCode;

    @Column(name = "EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime effectiveDt;

    public LegalDaysNoticeId() {
        super();
    }

    public LegalDaysNoticeId(String lineOfBusiness, String policySymbol, String riskState, String masterCompanyNumber,
            char effectiveTypeCode, String processCode, ZonedDateTime effectiveDt) {
        super();
        this.lineOfBusiness = lineOfBusiness;
        this.policySymbol = policySymbol;
        this.riskState = riskState;
        this.masterCompanyNumber = masterCompanyNumber;
        this.effectiveTypeCode = effectiveTypeCode;
        this.processCode = processCode;
        this.effectiveDt = effectiveDt;
    }

    public String getLineOfBusiness() {
        return lineOfBusiness;
    }

    public void setLineOfBusiness(String lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }

    public String getPolicySymbol() {
        return policySymbol;
    }

    public void setPolicySymbol(String policySymbol) {
        this.policySymbol = policySymbol;
    }

    public String getRiskState() {
        return riskState;
    }

    public void setRiskState(String riskState) {
        this.riskState = riskState;
    }

    public String getMasterCompanyNumber() {
        return masterCompanyNumber;
    }

    public void setMasterCompanyNumber(String masterCompanyNumber) {
        this.masterCompanyNumber = masterCompanyNumber;
    }

    public char getEffectiveTypeCode() {
        return effectiveTypeCode;
    }

    public void setEffectiveTypeCode(char effectiveTypeCode) {
        this.effectiveTypeCode = effectiveTypeCode;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public ZonedDateTime getEffectiveDt() {
        return effectiveDt;
    }

    public void setEffectiveDt(ZonedDateTime effectiveDt) {
        this.effectiveDt = effectiveDt;
    }

}
