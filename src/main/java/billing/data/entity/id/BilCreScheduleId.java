package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilCreScheduleId implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String bilAccountId = SEPARATOR_BLANK;

    @Column(name = "POLICY_ID", length = 16, nullable = false)
    private String policyId = SEPARATOR_BLANK;

    @Column(name = "BIL_SEQ_NBR", nullable = false)
    private short bilSeqNbr;

    @Column(name = "BIL_CRE_SEQ_NBR", nullable = false)
    private short bilCreSeqNbr;
    
    public BilCreScheduleId() {
        super();
    }

    public BilCreScheduleId(String bilAccountId, String policyId, short bilSeqNbr, short bilCreSeqNbr) {
        super();
        this.bilAccountId = bilAccountId;
        this.policyId = policyId;
        this.bilSeqNbr = bilSeqNbr;
        this.bilCreSeqNbr = bilCreSeqNbr;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public short getBilSeqNbr() {
        return bilSeqNbr;
    }

    public void setBilSeqNbr(short bilSeqNbr) {
        this.bilSeqNbr = bilSeqNbr;
    }

    public short getBilCreSeqNbr() {
        return bilCreSeqNbr;
    }

    public void setBilCreSeqNbr(short bilCreSeqNbr) {
        this.bilCreSeqNbr = bilCreSeqNbr;
    }

}
