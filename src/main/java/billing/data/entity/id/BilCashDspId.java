package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilCashDspId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String accountId = SEPARATOR_BLANK;

    @Column(name = "BIL_DTB_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilDtbDate;

    @Column(name = "BIL_DTB_SEQ_NBR")
    private short dtbSequenceNbr;

    @Column(name = "BIL_DSP_SEQ_NBR")
    private short dspSequenceNbr;

    public BilCashDspId(String accountId, ZonedDateTime bilDtbDate, short dtbSequenceNbr, short dspSequenceNbr) {
        super();
        this.accountId = accountId;
        this.bilDtbDate = bilDtbDate;
        this.dtbSequenceNbr = dtbSequenceNbr;
        this.dspSequenceNbr = dspSequenceNbr;
    }

    public BilCashDspId() {
        super();
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public ZonedDateTime getBilDtbDate() {
        return bilDtbDate;
    }

    public void setBilDtbDate(ZonedDateTime bilDtbDate) {
        this.bilDtbDate = bilDtbDate;
    }

    public short getDtbSequenceNbr() {
        return dtbSequenceNbr;
    }

    public void setDtbSequenceNbr(short dtbSequenceNbr) {
        this.dtbSequenceNbr = dtbSequenceNbr;
    }

    public short getDspSequenceNbr() {
        return dspSequenceNbr;
    }

    public void setDspSequenceNbr(short dspSequenceNbr) {
        this.dspSequenceNbr = dspSequenceNbr;
    }

}
