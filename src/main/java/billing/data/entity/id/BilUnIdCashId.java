package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilUnIdCashId implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_DTB_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilDtbDt;

    @Column(name = "BIL_DTB_SEQ_NBR", length = 5, nullable = false)
    private Short bilDtbSeqNbr;

    @Column(name = "BIL_ENTRY_NBR", length = 4, nullable = false)
    private String bilEntryNbr = SEPARATOR_BLANK;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "BIL_ENTRY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEntryDt;

    public BilUnIdCashId() {
        super();
    }

    public BilUnIdCashId(ZonedDateTime bilDtbDt, short bilDtbSeqNbr, String bilEntryNbr, String userId,
            ZonedDateTime bilEntryDt) {
        super();
        this.bilDtbDt = bilDtbDt;
        this.bilDtbSeqNbr = bilDtbSeqNbr;
        this.bilEntryNbr = bilEntryNbr;
        this.userId = userId;
        this.bilEntryDt = bilEntryDt;
    }

    public ZonedDateTime getBilDtbDt() {
        return bilDtbDt;
    }

    public void setBilDtbDt(ZonedDateTime bilDtbDt) {
        this.bilDtbDt = bilDtbDt;
    }

    public short getBilDtbSeqNbr() {
        return bilDtbSeqNbr;
    }

    public void setBilDtbSeqNbr(short bilDtbSeqNbr) {
        this.bilDtbSeqNbr = bilDtbSeqNbr;
    }

    public String getBilEntryNbr() {
        return bilEntryNbr;
    }

    public void setBilEntryNbr(String bilEntryNbr) {
        this.bilEntryNbr = bilEntryNbr;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ZonedDateTime getBilEntryDt() {
        return bilEntryDt;
    }

    public void setBilEntryDt(ZonedDateTime bilEntryDt) {
        this.bilEntryDt = bilEntryDt;
    }

}
