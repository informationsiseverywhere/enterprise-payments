package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilActInquiryId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String bilAccountId = SEPARATOR_BLANK;

    @Column(name = "BIL_ACY_TYPE_CD", length = 3, nullable = false)
    private String bilAcyTypeCd = SEPARATOR_BLANK;

    public BilActInquiryId() {
        super();
    }

    public BilActInquiryId(String bilAccountId, String bilAcyTypeCd) {
        super();
        this.bilAccountId = bilAccountId;
        this.bilAcyTypeCd = bilAcyTypeCd;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public String getBilAcyTypeCd() {
        return bilAcyTypeCd;
    }

    public void setBilAcyTypeCd(String bilAcyTypeCd) {
        this.bilAcyTypeCd = bilAcyTypeCd;
    }

}
