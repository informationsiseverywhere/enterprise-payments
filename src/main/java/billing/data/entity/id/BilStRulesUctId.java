package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilStRulesUctId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_RULE_ID", length = 4, nullable = false)
    private String bilRuleId = SEPARATOR_BLANK;

    @Column(name = "RISK_STATE_CD", length = 3, nullable = false)
    private String riskStateCd = SEPARATOR_BLANK;

    @Column(name = "LOB_CD", length = 3, nullable = false)
    private String lobCd = SEPARATOR_BLANK;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNbr = SEPARATOR_BLANK;

    @Column(name = "EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime effectiveDt;

    public BilStRulesUctId() {
        super();
    }

    public BilStRulesUctId(String bilRuleId, String riskStateCd, String lobCd, String masterCompanyNbr,
            ZonedDateTime effectiveDt) {
        super();
        this.bilRuleId = bilRuleId;
        this.riskStateCd = riskStateCd;
        this.lobCd = lobCd;
        this.masterCompanyNbr = masterCompanyNbr;
        this.effectiveDt = effectiveDt;
    }

    public String getBilRuleId() {
        return bilRuleId;
    }

    public void setBilRuleId(String bilRuleId) {
        this.bilRuleId = bilRuleId;
    }

    public String getRiskStateCd() {
        return riskStateCd;
    }

    public void setRiskStateCd(String riskStateCd) {
        this.riskStateCd = riskStateCd;
    }

    public String getLobCd() {
        return lobCd;
    }

    public void setLobCd(String lobCd) {
        this.lobCd = lobCd;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public ZonedDateTime getEffectiveDt() {
        return effectiveDt;
    }

    public void setEffectiveDt(ZonedDateTime effectiveDt) {
        this.effectiveDt = effectiveDt;
    }

}
