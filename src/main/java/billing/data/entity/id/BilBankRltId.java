package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilBankRltId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_BANK_CD", length = 3, nullable = false)
    private String bilBankCd = SEPARATOR_BLANK;

    @Column(name = "BBR_BANK_TYPE_CD", nullable = false)
    private char bbrBankTypeCd;

    @Column(name = "BIL_COUNTRY_CD", length = 4, nullable = false)
    private String bilCountryCd = SEPARATOR_BLANK;

    @Column(name = "BIL_STATE_PVN_CD", length = 3, nullable = false)
    private String bilStatePvnCd = SEPARATOR_BLANK;

    @Column(name = "BBR_BANK_LOC_CD", length = 8, nullable = false)
    private String bbrBankLocCd = SEPARATOR_BLANK;

    public BilBankRltId() {
        super();
    }

    public BilBankRltId(String bilBankCd, char bbrBankTypeCd, String bilCountryCd, String bilStatePvnCd,
            String bbrBankLocCd) {
        super();
        this.bilBankCd = bilBankCd;
        this.bbrBankTypeCd = bbrBankTypeCd;
        this.bilCountryCd = bilCountryCd;
        this.bilStatePvnCd = bilStatePvnCd;
        this.bbrBankLocCd = bbrBankLocCd;
    }

    public String getBilBankCd() {
        return bilBankCd;
    }

    public void setBilBankCd(String bilBankCd) {
        this.bilBankCd = bilBankCd;
    }

    public char getBbrBankTypeCd() {
        return bbrBankTypeCd;
    }

    public void setBbrBankTypeCd(char bbrBankTypeCd) {
        this.bbrBankTypeCd = bbrBankTypeCd;
    }

    public String getBilCountryCd() {
        return bilCountryCd;
    }

    public void setBilCountryCd(String bilCountryCd) {
        this.bilCountryCd = bilCountryCd;
    }

    public String getBilStatePvnCd() {
        return bilStatePvnCd;
    }

    public void setBilStatePvnCd(String bilStatePvnCd) {
        this.bilStatePvnCd = bilStatePvnCd;
    }

    public String getBbrBankLocCd() {
        return bbrBankLocCd;
    }

    public void setBbrBankLocCd(String bbrBankLocCd) {
        this.bbrBankLocCd = bbrBankLocCd;
    }

}
