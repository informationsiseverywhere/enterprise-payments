package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilObjectCfgId implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "OBJECT_ID", length = 8, nullable = false)
    private String objectId = SEPARATOR_BLANK;

    @Column(name = "BOC_OBJ_TYPE_CD", nullable = false)
    private char bocObjTypeCd;

    public BilObjectCfgId() {
        super();
    }

    public BilObjectCfgId(String objectId, char bocObjTypeCd) {
        super();
        this.objectId = objectId;
        this.bocObjTypeCd = bocObjTypeCd;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public char getBocObjTypeCd() {
        return bocObjTypeCd;
    }

    public void setBocObjTypeCd(char bocObjTypeCd) {
        this.bocObjTypeCd = bocObjTypeCd;
    }
}
