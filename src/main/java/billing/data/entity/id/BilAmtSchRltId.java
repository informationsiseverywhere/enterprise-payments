package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilAmtSchRltId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String bilAccountId = SEPARATOR_BLANK;

    @Column(name = "POLICY_ID", length = 16, nullable = false)
    private String policyId = SEPARATOR_BLANK;

    @Column(name = "BRS_BAM_SEQ_NBR", length = 5, nullable = false)
    private short brsBamSeqNbr;

    @Column(name = "BRS_BIS_SEQ_NBR", length = 5, nullable = false)
    private short brsBisSeqNbr;
}
