package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilAgtActRltId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_AGT_ACT_ID", length = 8, nullable = true)
    private String bilAgtActId = SEPARATOR_BLANK;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = true)
    private String bilAccountId = SEPARATOR_BLANK;

    public BilAgtActRltId() {

    }

    public BilAgtActRltId(String bilAgtActId, String bilAccountId) {
        super();
        this.bilAgtActId = bilAgtActId;
        this.bilAccountId = bilAccountId;
    }

    public String getBilAgtActId() {
        return bilAgtActId;
    }

    public void setBilAgtActId(String bilAgtActId) {
        this.bilAgtActId = bilAgtActId;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

}
