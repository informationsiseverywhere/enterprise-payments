package billing.data.entity.id;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilSptPlnCrgId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_TYPE_CD", length = 2, nullable = false)
    private String bilTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_CLASS_CD", length = 3, nullable = false)
    private String bilClassCd = SEPARATOR_BLANK;

    @Column(name = "EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime effectiveDt;

    @Column(name = "BIL_CRG_EFF_TYP_CD", nullable = false)
    private char bilCrgEffTypCd = BLANK_CHAR;

    @Column(name = "HISTORY_VLD_NBR", length = 5, nullable = false)
    private short historyVldNbr = SHORT_ZERO;

    public BilSptPlnCrgId() {
        super();
    }

    public BilSptPlnCrgId(String bilTypeCd, String bilClassCd, ZonedDateTime effectiveDt, char bilCrgEffTypCd,
            short historyVldNbr) {
        super();
        this.bilTypeCd = bilTypeCd;
        this.bilClassCd = bilClassCd;
        this.effectiveDt = effectiveDt;
        this.bilCrgEffTypCd = bilCrgEffTypCd;
        this.historyVldNbr = historyVldNbr;
    }

    public String getBilTypeCd() {
        return bilTypeCd;
    }

    public void setBilTypeCd(String bilTypeCd) {
        this.bilTypeCd = bilTypeCd;
    }

    public String getBilClassCd() {
        return bilClassCd;
    }

    public void setBilClassCd(String bilClassCd) {
        this.bilClassCd = bilClassCd;
    }

    public ZonedDateTime getEffectiveDt() {
        return effectiveDt;
    }

    public void setEffectiveDt(ZonedDateTime effectiveDt) {
        this.effectiveDt = effectiveDt;
    }

    public char getBilCrgEffTypCd() {
        return bilCrgEffTypCd;
    }

    public void setBilCrgEffTypCd(char bilCrgEffTypCd) {
        this.bilCrgEffTypCd = bilCrgEffTypCd;
    }

    public short getHistoryVldNbr() {
        return historyVldNbr;
    }

    public void setHistoryVldNbr(short historyVldNbr) {
        this.historyVldNbr = historyVldNbr;
    }

}
