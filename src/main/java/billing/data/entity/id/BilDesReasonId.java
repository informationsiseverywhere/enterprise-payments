package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilDesReasonId implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_DES_REA_CD", length = 3, nullable = false)
    private String bilDesReasonCd = SEPARATOR_BLANK;

    @Column(name = "BIL_DES_REA_TYP", length = 3, nullable = false)
    private String bilDesReasonType = SEPARATOR_BLANK;

    @Column(name = "PRI_LGG_CD", length = 3, nullable = false)
    private String primaryLanguageCd = SEPARATOR_BLANK;

    public BilDesReasonId() {
        super();
    }

    public BilDesReasonId(String bilDesReasonCd, String bilDesReasonType, String primaryLanguageCd) {
        super();
        this.bilDesReasonCd = bilDesReasonCd;
        this.bilDesReasonType = bilDesReasonType;
        this.primaryLanguageCd = primaryLanguageCd;
    }

    public String getBilDesReasonCd() {
        return bilDesReasonCd;
    }

    public void setBilDesReasonCd(String bilDesReasonCd) {
        this.bilDesReasonCd = bilDesReasonCd;
    }

    public String getBilDesReasonType() {
        return bilDesReasonType;
    }

    public void setBilDesReasonType(String bilDesReasonType) {
        this.bilDesReasonType = bilDesReasonType;
    }

    public String getPrimaryLanguageCd() {
        return primaryLanguageCd;
    }

    public void setPrimaryLanguageCd(String primaryLanguageCd) {
        this.primaryLanguageCd = primaryLanguageCd;
    }

}
