package billing.data.entity.id;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilSttReconDtlId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_TCH_KEY_ID", length = 8, nullable = false)
    private String bilTchKeyId = SEPARATOR_BLANK;

    @Column(name = "BIL_TCH_KEY_TYP_CD", nullable = false)
    private char bilTchKeyTypCd = BLANK_CHAR;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String bilAccountId = SEPARATOR_BLANK;

    @Column(name = "BIL_INV_ENTRY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilInvEntryDt;

    @Column(name = "BIL_INV_SEQ_NBR", length = 5, nullable = false)
    private short bilInvSeqNbr = SHORT_ZERO;

    @Column(name = "BIL_STT_TYPE_CD", nullable = false)
    private char bilSttTypeCd = BLANK_CHAR;

    @Column(name = "BIL_STT_DTL_SEQ", length = 10, nullable = false)
    private short bilSttDtlSeq = SHORT_ZERO;

    @Column(name = "BIL_ACY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAcyDt;

    public BilSttReconDtlId() {
        super();
    }

    public BilSttReconDtlId(String bilTchKeyId, char bilTchKeyTypCd, String bilAccountId, ZonedDateTime bilInvEntryDt,
            short bilInvSeqNbr, char bilSttTypeCd, short bilSttDtlSeq, ZonedDateTime bilAcyDt) {
        super();
        this.bilTchKeyId = bilTchKeyId;
        this.bilTchKeyTypCd = bilTchKeyTypCd;
        this.bilAccountId = bilAccountId;
        this.bilInvEntryDt = bilInvEntryDt;
        this.bilInvSeqNbr = bilInvSeqNbr;
        this.bilSttTypeCd = bilSttTypeCd;
        this.bilSttDtlSeq = bilSttDtlSeq;
        this.bilAcyDt = bilAcyDt;
    }

    public String getBilTchKeyId() {
        return bilTchKeyId;
    }

    public void setBilTchKeyId(String bilTchKeyId) {
        this.bilTchKeyId = bilTchKeyId;
    }

    public char getBilTchKeyTypCd() {
        return bilTchKeyTypCd;
    }

    public void setBilTchKeyTypCd(char bilTchKeyTypCd) {
        this.bilTchKeyTypCd = bilTchKeyTypCd;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public ZonedDateTime getBilInvEntryDt() {
        return bilInvEntryDt;
    }

    public void setBilInvEntryDt(ZonedDateTime bilInvEntryDt) {
        this.bilInvEntryDt = bilInvEntryDt;
    }

    public short getBilInvSeqNbr() {
        return bilInvSeqNbr;
    }

    public void setBilInvSeqNbr(short bilInvSeqNbr) {
        this.bilInvSeqNbr = bilInvSeqNbr;
    }

    public char getBilSttTypeCd() {
        return bilSttTypeCd;
    }

    public void setBilSttTypeCd(char bilSttTypeCd) {
        this.bilSttTypeCd = bilSttTypeCd;
    }

    public short getBilSttDtlSeq() {
        return bilSttDtlSeq;
    }

    public void setBilSttDtlSeq(short bilSttDtlSeq) {
        this.bilSttDtlSeq = bilSttDtlSeq;
    }

    public ZonedDateTime getBilAcyDt() {
        return bilAcyDt;
    }

    public void setBilAcyDt(ZonedDateTime bilAcyDt) {
        this.bilAcyDt = bilAcyDt;
    }

}
