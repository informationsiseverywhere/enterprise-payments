package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class TransRateHstId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ENTRY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEntryDt;

    @Column(name = "BIL_ENTRY_NBR", length = 4, nullable = false)
    private String bilEntryNbr = SEPARATOR_BLANK;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "BIL_ENTRY_SEQ_NBR", nullable = false)
    private short bilEntrySeqNbr;

    public TransRateHstId() {
        super();
    }

    public TransRateHstId(ZonedDateTime bilEntryDt, String bilEntryNbr, String userId, short bilEntrySeqNbr) {
        super();
        this.bilEntryDt = bilEntryDt;
        this.bilEntryNbr = bilEntryNbr;
        this.userId = userId;
        this.bilEntrySeqNbr = bilEntrySeqNbr;
    }

    public ZonedDateTime getBilEntryDt() {
        return bilEntryDt;
    }

    public void setBilEntryDt(ZonedDateTime bilEntryDt) {
        this.bilEntryDt = bilEntryDt;
    }

    public String getBilEntryNbr() {
        return bilEntryNbr;
    }

    public void setBilEntryNbr(String bilEntryNbr) {
        this.bilEntryNbr = bilEntryNbr;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public short getBilEntrySeqNbr() {
        return bilEntrySeqNbr;
    }

    public void setBilEntrySeqNbr(short bilEntrySeqNbr) {
        this.bilEntrySeqNbr = bilEntrySeqNbr;
    }

}