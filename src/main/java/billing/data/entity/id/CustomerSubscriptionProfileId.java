package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CustomerSubscriptionProfileId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "MERCHANT_CSR_ID", nullable = false)
    private String merchantCustomerId = SEPARATOR_BLANK;

    @Column(name = "APP_ID", nullable = false)
    private String appId = SEPARATOR_BLANK;

    public CustomerSubscriptionProfileId() {
        super();
    }

    public CustomerSubscriptionProfileId(String merchantCustomerId, String appId) {
        super();
        this.merchantCustomerId = merchantCustomerId;
        this.appId = appId;
    }

    public String getMerchantcustomerid() {
        return merchantCustomerId;
    }

    public void setMerchantcustomerid(String merchantcustomerid) {
        this.merchantCustomerId = merchantcustomerid;
    }

    public String getAppid() {
        return appId;
    }

    public void setAppid(String appid) {
        this.appId = appid;
    }

}
