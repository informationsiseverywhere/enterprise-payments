package billing.data.entity.id;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilEftPlnCrgId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_COLLECTION_PLN", length = 4, nullable = false)
    private String bilCollectionPln = SEPARATOR_BLANK;

    @Column(name = "EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime effectiveDt;

    @Column(name = "BIL_CRG_EFF_TYP_CD", nullable = false)
    private char bilCrgEffTypCd = BLANK_CHAR;

    @Column(name = "HISTORY_VLD_NBR", length = 5, nullable = false)
    private short historyVldNbr = SHORT_ZERO;

    public BilEftPlnCrgId() {
        super();
    }

    public BilEftPlnCrgId(String bilCollectionPln, ZonedDateTime effectiveDt, char bilCrgEffTypCd,
            short historyVldNbr) {
        super();
        this.bilCollectionPln = bilCollectionPln;
        this.effectiveDt = effectiveDt;
        this.bilCrgEffTypCd = bilCrgEffTypCd;
        this.historyVldNbr = historyVldNbr;
    }

    public String getBilCollectionPln() {
        return bilCollectionPln;
    }

    public void setBilCollectionPln(String bilCollectionPln) {
        this.bilCollectionPln = bilCollectionPln;
    }

    public ZonedDateTime getEffectiveDt() {
        return effectiveDt;
    }

    public void setEffectiveDt(ZonedDateTime effectiveDt) {
        this.effectiveDt = effectiveDt;
    }

    public char getBilCrgEffTypCd() {
        return bilCrgEffTypCd;
    }

    public void setBilCrgEffTypCd(char bilCrgEffTypCd) {
        this.bilCrgEffTypCd = bilCrgEffTypCd;
    }

    public short getHistoryVldNbr() {
        return historyVldNbr;
    }

    public void setHistoryVldNbr(short historyVldNbr) {
        this.historyVldNbr = historyVldNbr;
    }

}
