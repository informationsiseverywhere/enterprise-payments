package billing.data.entity.id;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilSystemCfgId implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "BSC_COMMIT_FQY_NBR", length = 5, nullable = false)
    private short bscCommitFqyNbr;

    @Column(name = "BSC_REKICK_NBR", length = 5, nullable = false)
    private short bscRekickNbr;

    @Column(name = "BSC_PARTITION_NBR", length = 5, nullable = false)
    private short bscPartitionNbr;

    @Column(name = "BSC_BCH_RNL_IND", nullable = false)
    private char bscBchRnlInd;

    @Column(name = "BSC_QUEUE_WRT_IND", nullable = false)
    private char bscQueueWrtInd;

    @Column(name = "BSC_DROP_LOK_NBR", length = 5, nullable = false)
    private short bscDropLokNbr;

    public BilSystemCfgId() {
        super();
    }

    public BilSystemCfgId(short bscCommitFqyNbr, short bscRekickNbr, short bscPartitionNbr, char bscBchRnlInd,
            char bscQueueWrtInd, short bscDropLokNbr) {
        super();
        this.bscCommitFqyNbr = bscCommitFqyNbr;
        this.bscRekickNbr = bscRekickNbr;
        this.bscPartitionNbr = bscPartitionNbr;
        this.bscBchRnlInd = bscBchRnlInd;
        this.bscQueueWrtInd = bscQueueWrtInd;
        this.bscDropLokNbr = bscDropLokNbr;
    }

    public short getBscCommitFqyNbr() {
        return bscCommitFqyNbr;
    }

    public void setBscCommitFqyNbr(short bscCommitFqyNbr) {
        this.bscCommitFqyNbr = bscCommitFqyNbr;
    }

    public short getBscRekickNbr() {
        return bscRekickNbr;
    }

    public void setBscRekickNbr(short bscRekickNbr) {
        this.bscRekickNbr = bscRekickNbr;
    }

    public short getBscPartitionNbr() {
        return bscPartitionNbr;
    }

    public void setBscPartitionNbr(short bscPartitionNbr) {
        this.bscPartitionNbr = bscPartitionNbr;
    }

    public char getBscBchRnlInd() {
        return bscBchRnlInd;
    }

    public void setBscBchRnlInd(char bscBchRnlInd) {
        this.bscBchRnlInd = bscBchRnlInd;
    }

    public char getBscQueueWrtInd() {
        return bscQueueWrtInd;
    }

    public void setBscQueueWrtInd(char bscQueueWrtInd) {
        this.bscQueueWrtInd = bscQueueWrtInd;
    }

    public short getBscDropLokNbr() {
        return bscDropLokNbr;
    }

    public void setBscDropLokNbr(short bscDropLokNbr) {
        this.bscDropLokNbr = bscDropLokNbr;
    }

}
