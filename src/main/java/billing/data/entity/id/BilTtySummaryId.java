package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilTtySummaryId implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_THIRD_PARTY_ID", length = 8, nullable = false)
    private String bilThirdPartyId = SEPARATOR_BLANK;

    @Column(name = "BIL_ACY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAcyDt;

    @Column(name = "BIL_ACY_SEQ", length = 5, nullable = false)
    private short bilAcySeq = SHORT_ZERO;

    public BilTtySummaryId() {
        super();
    }

    public BilTtySummaryId(String bilThirdPartyId, ZonedDateTime bilAcyDt, short bilAcySeq) {
        super();
        this.bilThirdPartyId = bilThirdPartyId;
        this.bilAcyDt = bilAcyDt;
        this.bilAcySeq = bilAcySeq;
    }

    public String getBilThirdPartyId() {
        return bilThirdPartyId;
    }

    public void setBilThirdPartyId(String bilThirdPartyId) {
        this.bilThirdPartyId = bilThirdPartyId;
    }

    public ZonedDateTime getBilAcyDt() {
        return bilAcyDt;
    }

    public void setBilAcyDt(ZonedDateTime bilAcyDt) {
        this.bilAcyDt = bilAcyDt;
            }

    public short getBilAcySeq() {
        return bilAcySeq;
    }

    public void setBilAcySeq(short bilAcySeq) {
        this.bilAcySeq = bilAcySeq;
    }

}