package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilEftExtPytId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_TCH_KEY_ID", length = 8, nullable = false)
    private String bilTchKeyId = SEPARATOR_BLANK;

    @Column(name = "BIL_TCH_KEY_TYP_CD", nullable = false)
    private char bilTchKeyTypCd;

    @Column(name = "BIL_EFT_TAPE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEftTapeDt;

    @Column(name = "BIL_EFT_ACY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEftAcyDt;

    @Column(name = "BIL_SEQ_NBR", nullable = false)
    private short bilSeqNbr;
    
    public BilEftExtPytId() {
        super();
    }

    public BilEftExtPytId(String bilTchKeyId, char bilTchKeyTypCd, ZonedDateTime bilEftTapeDt,
            ZonedDateTime bilEftAcyDt, short bilSeqNbr) {
        super();
        this.bilTchKeyId = bilTchKeyId;
        this.bilTchKeyTypCd = bilTchKeyTypCd;
        this.bilEftTapeDt = bilEftTapeDt;
        this.bilEftAcyDt = bilEftAcyDt;
        this.bilSeqNbr = bilSeqNbr;
    }

    public String getBilTchKeyId() {
        return bilTchKeyId;
    }

    public void setBilTchKeyId(String bilTchKeyId) {
        this.bilTchKeyId = bilTchKeyId;
    }

    public char getBilTchKeyTypCd() {
        return bilTchKeyTypCd;
    }

    public void setBilTchKeyTypCd(char bilTchKeyTypCd) {
        this.bilTchKeyTypCd = bilTchKeyTypCd;
    }

    public ZonedDateTime getBilEftTapeDt() {
        return bilEftTapeDt;
    }

    public void setBilEftTapeDt(ZonedDateTime bilEftTapeDt) {
        this.bilEftTapeDt = bilEftTapeDt;
    }

    public ZonedDateTime getBilEftAcyDt() {
        return bilEftAcyDt;
    }

    public void setBilEftAcyDt(ZonedDateTime bilEftAcyDt) {
        this.bilEftAcyDt = bilEftAcyDt;
    }

    public short getBilSeqNbr() {
        return bilSeqNbr;
    }

    public void setBilSeqNbr(short bilSeqNbr) {
        this.bilSeqNbr = bilSeqNbr;
    }
}
