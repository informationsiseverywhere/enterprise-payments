package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilCrcrdArspDesId implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_CRCRD_ARSP_CD", nullable = false)
    private char bilCrcrdArspCd;

    @Column(name = "BIL_CRCRD_ARSP_TYC", length = 3, nullable = false)
    private String bilCrcrdArspTyc = SEPARATOR_BLANK;

    @Column(name = "PRI_LGG_CD", length = 3, nullable = false)
    private String priLggCd = SEPARATOR_BLANK;

    public BilCrcrdArspDesId() {
        super();
    }

    public BilCrcrdArspDesId(char bilCrcrdArspCd, String bilCrcrdArspTyc, String priLggCd) {
        super();
        this.bilCrcrdArspCd = bilCrcrdArspCd;
        this.bilCrcrdArspTyc = bilCrcrdArspTyc;
        this.priLggCd = priLggCd;
    }

    public char getBilCrcrdArspCd() {
        return bilCrcrdArspCd;
    }

    public void setBilCrcrdArspCd(char bilCrcrdArspCd) {
        this.bilCrcrdArspCd = bilCrcrdArspCd;
    }

    public String getBilCrcrdArspTyc() {
        return bilCrcrdArspTyc;
    }

    public void setBilCrcrdArspTyc(String bilCrcrdArspTyc) {
        this.bilCrcrdArspTyc = bilCrcrdArspTyc;
    }

    public String getPriLggCd() {
        return priLggCd;
    }

    public void setPriLggCd(String priLggCd) {
        this.priLggCd = priLggCd;
    }

}
