package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilActRulesId implements Serializable {

    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_TYPE_CD", length = 2, nullable = false)
    private String bilTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_CLASS_CD", length = 3, nullable = false)
    private String bilClassCd = SEPARATOR_BLANK;

    public BilActRulesId() {
        super();
    }

    public BilActRulesId(String bilTypeCd, String bilClassCd) {
        super();
        this.bilTypeCd = bilTypeCd;
        this.bilClassCd = bilClassCd;
    }

    public String getBilTypeCd() {
        return bilTypeCd;
    }

    public void setBilTypeCd(String bilTypeCd) {
        this.bilTypeCd = bilTypeCd;
    }

    public String getBilClassCd() {
        return bilClassCd;
    }

    public void setBilClassCd(String bilClassCd) {
        this.bilClassCd = bilClassCd;
    }

}
