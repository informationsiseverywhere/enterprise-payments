package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;

@Embeddable
public class BilAccountBalanceId implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String billAccountId = SEPARATOR_BLANK;

    @Column(name = "BIL_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime billAcyTs;

    public BilAccountBalanceId() {
        super();
    }

    public BilAccountBalanceId(String billAccountId, ZonedDateTime billAcyTs) {
        super();
        this.billAccountId = billAccountId;
        this.billAcyTs = billAcyTs;
    }

    public String getBillAccountId() {
        return billAccountId;
    }

    public void setBillAccountId(String billAccountId) {
        this.billAccountId = billAccountId;
    }

    public ZonedDateTime getBillAcyTs() {
        return billAcyTs;
    }

    public void setBillAcyTs(ZonedDateTime billAcyTs) {
        this.billAcyTs = billAcyTs;
    }
}
