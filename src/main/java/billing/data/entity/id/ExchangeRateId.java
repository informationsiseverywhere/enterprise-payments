package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class ExchangeRateId implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "EXCHANGE_DATE", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime exchangeDate;

    @Column(name = "FROM_CURRENCY", length = 15, nullable = false)
    private String fromCurrency = SEPARATOR_BLANK;

    @Column(name = "TO_CURRENCY", length = 15, nullable = false)
    private String toCurrency = SEPARATOR_BLANK;

    public ExchangeRateId() {
        super();
    }

    public ExchangeRateId(ZonedDateTime exchangeDate, String fromCurrency, String toCurrency) {
        super();
        this.exchangeDate = exchangeDate;
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
    }

    public ZonedDateTime getExchangeDate() {
        return exchangeDate;
    }

    public void setExchangeDate(ZonedDateTime exchangeDate) {
        this.exchangeDate = exchangeDate;
    }

    public String getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(String fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public String getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(String toCurrency) {
        this.toCurrency = toCurrency;
    }

}
