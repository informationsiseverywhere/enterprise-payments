package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilIstScheduleId implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String billAccountId = SEPARATOR_BLANK;

    @Column(name = "POLICY_ID", length = 16, nullable = false)
    private String policyId = SEPARATOR_BLANK;

    @Column(name = "BIL_SEQ_NBR", nullable = false)
    private short billSeqNbr;

    public BilIstScheduleId(String billAccountId, String policyId, short billSeqNbr) {
        super();
        this.billAccountId = billAccountId;
        this.policyId = policyId;
        this.billSeqNbr = billSeqNbr;
    }

    public BilIstScheduleId() {
        super();
    }

    public String getBillAccountId() {
        return billAccountId;
    }

    public void setBillAccountId(String billAccountId) {
        this.billAccountId = billAccountId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public short getBillSeqNbr() {
        return billSeqNbr;
    }

    public void setBillSeqNbr(short billSeqNbr) {
        this.billSeqNbr = billSeqNbr;
    }

}
