package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilFullPmtDscId implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_STATE_PVN_CD", length = 3, nullable = false)
    private String bilStatePvnCd = SEPARATOR_BLANK;

    @Column(name = "LOB_CD", length = 3, nullable = false)
    private String lobCd = SEPARATOR_BLANK;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNbr;

    @Column(name = "POL_TERM_TYPE_CD", length = 1, nullable = false)
    private char polTermTypeCd;

    @Column(name = "EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime effectiveDt;

    public BilFullPmtDscId() {
        super();
    }

    public BilFullPmtDscId(String bilStatePvnCd, String lobCd, String masterCompanyNbr, char polTermTypeCd,
            ZonedDateTime effectiveDt) {
        super();
        this.bilStatePvnCd = bilStatePvnCd;
        this.lobCd = lobCd;
        this.masterCompanyNbr = masterCompanyNbr;
        this.polTermTypeCd = polTermTypeCd;
        this.effectiveDt = effectiveDt;
    }

    public String getBilStatePvnCd() {
        return bilStatePvnCd;
    }

    public void setBilStatePvnCd(String bilStatePvnCd) {
        this.bilStatePvnCd = bilStatePvnCd;
    }

    public String getLobCd() {
        return lobCd;
    }

    public void setLobCd(String lobCd) {
        this.lobCd = lobCd;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public char getPolTermTypeCd() {
        return polTermTypeCd;
    }

    public void setPolTermTypeCd(char polTermTypeCd) {
        this.polTermTypeCd = polTermTypeCd;
    }

    public ZonedDateTime getEffectiveDt() {
        return effectiveDt;
    }

    public void setEffectiveDt(ZonedDateTime effectiveDt) {
        this.effectiveDt = effectiveDt;
    }

}
