package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilAgtCashRctId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_AGT_ACT_ID", length = 8, nullable = false)
    private String bilAgtActId = SEPARATOR_BLANK;

    @Column(name = "BIL_DTB_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilDtbDt;

    @Column(name = "BIL_DTB_SEQ_NBR", length = 5, nullable = false)
    private short bilDtbSeqNbr = SHORT_ZERO;

    public BilAgtCashRctId() {
        super();
    }

    public BilAgtCashRctId(String bilAgtActId, ZonedDateTime bilDtbDt, short bilDtbSeqNbr) {
        super();
        this.bilAgtActId = bilAgtActId;
        this.bilDtbDt = bilDtbDt;
        this.bilDtbSeqNbr = bilDtbSeqNbr;
    }

    public String getBilAgtActId() {
        return bilAgtActId;
    }

    public void setBilAgtActId(String bilAgtActId) {
        this.bilAgtActId = bilAgtActId;
    }

    public ZonedDateTime getBilDtbDt() {
        return bilDtbDt;
    }

    public void setBilDtbDt(ZonedDateTime bilDtbDt) {
        this.bilDtbDt = bilDtbDt;
    }

    public short getBilDtbSeqNbr() {
        return bilDtbSeqNbr;
    }

    public void setBilDtbSeqNbr(short bilDtbSeqNbr) {
        this.bilDtbSeqNbr = bilDtbSeqNbr;
    }

}
