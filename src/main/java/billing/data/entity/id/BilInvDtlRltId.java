package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilInvDtlRltId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_TCH_KEY_ID", length = 8, nullable = false)
    private String bilTchKeyId = SEPARATOR_BLANK;

    @Column(name = "BIL_TCH_KEY_TYP_CD", nullable = false)
    private char bilTchKeyTypCd;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String bilAccountId = SEPARATOR_BLANK;

    @Column(name = "BIL_INV_ENTRY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilInvEntryDt;

    @Column(name = "BIL_INV_SEQ_NBR", nullable = false)
    private short bilInvSeqNbr;

    public BilInvDtlRltId() {
        super();
    }

    public BilInvDtlRltId(String bilTchKeyId, char bilTchKeyTypCd, String bilAccountId, ZonedDateTime bilInvEntryDt,
            short bilInvSeqNbr) {
        super();
        this.bilTchKeyId = bilTchKeyId;
        this.bilTchKeyTypCd = bilTchKeyTypCd;
        this.bilAccountId = bilAccountId;
        this.bilInvEntryDt = bilInvEntryDt;
        this.bilInvSeqNbr = bilInvSeqNbr;
    }

    public String getBilTchKeyId() {
        return bilTchKeyId;
    }

    public void setBilTchKeyId(String bilTchKeyId) {
        this.bilTchKeyId = bilTchKeyId;
    }

    public char getBilTchKeyTypCd() {
        return bilTchKeyTypCd;
    }

    public void setBilTchKeyTypCd(char bilTchKeyTypCd) {
        this.bilTchKeyTypCd = bilTchKeyTypCd;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public ZonedDateTime getBilInvEntryDt() {
        return bilInvEntryDt;
    }

    public void setBilInvEntryDt(ZonedDateTime bilInvEntryDt) {
        this.bilInvEntryDt = bilInvEntryDt;
    }

    public short getBilInvSeqNbr() {
        return bilInvSeqNbr;
    }

    public void setBilInvSeqNbr(short bilInvSeqNbr) {
        this.bilInvSeqNbr = bilInvSeqNbr;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
