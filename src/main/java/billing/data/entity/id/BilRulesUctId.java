package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilRulesUctId implements Serializable {

    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @Column(name = "BRT_RULE_ID", length = 4, nullable = false)
    private String brtRuleId = SEPARATOR_BLANK;

    @Column(name = "BIL_TYPE_CD", length = 2, nullable = false)
    private String bilTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_CLASS_CD", length = 3, nullable = false)
    private String bilClassCd = SEPARATOR_BLANK;

    @Column(name = "BIL_PLAN_CD", length = 4, nullable = false)
    private String bilPlanCd = SEPARATOR_BLANK;

    @Column(name = "EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime effectiveDt;

    public BilRulesUctId() {
        super();
    }

    public BilRulesUctId(String brtRuleId, String bilTypeCd, String bilClassCd, String bilPlanCd,
            ZonedDateTime effectiveDt) {
        super();
        this.brtRuleId = brtRuleId;
        this.bilTypeCd = bilTypeCd;
        this.bilClassCd = bilClassCd;
        this.bilPlanCd = bilPlanCd;
        this.effectiveDt = effectiveDt;
    }

    public String getBrtRuleId() {
        return brtRuleId;
    }

    public void setBrtRuleId(String brtRuleId) {
        this.brtRuleId = brtRuleId;
    }

    public String getBilTypeCd() {
        return bilTypeCd;
    }

    public void setBilTypeCd(String bilTypeCd) {
        this.bilTypeCd = bilTypeCd;
    }

    public String getBilClassCd() {
        return bilClassCd;
    }

    public void setBilClassCd(String bilClassCd) {
        this.bilClassCd = bilClassCd;
    }

    public String getBilPlanCd() {
        return bilPlanCd;
    }

    public void setBilPlanCd(String bilPlanCd) {
        this.bilPlanCd = bilPlanCd;
    }

    public ZonedDateTime getEffectiveDt() {
        return effectiveDt;
    }

    public void setEffectiveDt(ZonedDateTime effectiveDt) {
        this.effectiveDt = effectiveDt;
    }

}
