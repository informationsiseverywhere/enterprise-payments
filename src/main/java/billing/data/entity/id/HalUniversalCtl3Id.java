package billing.data.entity.id;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable

public class HalUniversalCtl3Id implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "ENTRY_KEY_CD", unique = true, nullable = false, length = 150)
    private String entryKeyCd;

    @Column(name = "TABLE_LABEL_TXT", unique = true, nullable = false, length = 32)
    private String tableLabelTxt;

    @Column(name = "CTR_NBR_CD", length = 5, nullable = false)
    private short ctrNbrCd;

    @Column(name = "SEQUENCE_NBR", length = 5, nullable = false)
    private short sequenceNbr;

    public HalUniversalCtl3Id() {
        super();
    }

    public HalUniversalCtl3Id(String entryKeyCd, String tableLabelTxt, short ctrNbrCd, short sequenceNbr) {
        super();
        this.entryKeyCd = entryKeyCd;
        this.tableLabelTxt = tableLabelTxt;
        this.ctrNbrCd = ctrNbrCd;
        this.sequenceNbr = sequenceNbr;
    }

    public String getEntryKeyCd() {
        return entryKeyCd;
    }

    public String getTableLabelTxt() {
        return tableLabelTxt;
    }

    public short getCtrNbrCd() {
        return ctrNbrCd;
    }

    public short getSequenceNbr() {
        return sequenceNbr;
    }

    public void setEntryKeyCd(String entryKeyCd) {
        this.entryKeyCd = entryKeyCd;
    }

    public void setTableLabelTxt(String tableLabelTxt) {
        this.tableLabelTxt = tableLabelTxt;
    }

    public void setCtrNbrCd(short ctrNbrCd) {
        this.ctrNbrCd = ctrNbrCd;
    }

    public void setSequenceNbr(short sequenceNbr) {
        this.sequenceNbr = sequenceNbr;
    }

}
