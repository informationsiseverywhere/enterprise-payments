package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;

@Embeddable
public class HalEventDtlId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "HAL_EVENT_KEY_ID", length = 50, nullable = false)
    private String halEventKeyId = SEPARATOR_BLANK;

    @Column(name = "HAL_EVT_KEY_TYP_CD", length = 2, nullable = false)
    private String halEvtKeyTypCd = SEPARATOR_BLANK;

    @Column(name = "BUS_OBJ_NM", length = 32, nullable = false)
    private String busObjNm = SEPARATOR_BLANK;

    @Column(name = "HAL_EVENT_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime halEventTs;

    public HalEventDtlId() {
        super();
    }

    public HalEventDtlId(String halEventKeyId, String halEvtKeyTypCd, String busObjNm, ZonedDateTime halEventTs) {
        super();
        this.halEventKeyId = halEventKeyId;
        this.halEvtKeyTypCd = halEvtKeyTypCd;
        this.busObjNm = busObjNm;
        this.halEventTs = halEventTs;
    }

    public String getHalEventKeyId() {
        return halEventKeyId;
    }

    public void setHalEventKeyId(String halEventKeyId) {
        this.halEventKeyId = halEventKeyId;
    }

    public String getHalEvtKeyTypCd() {
        return halEvtKeyTypCd;
    }

    public void setHalEvtKeyTypCd(String halEvtKeyTypCd) {
        this.halEvtKeyTypCd = halEvtKeyTypCd;
    }

    public String getBusObjNm() {
        return busObjNm;
    }

    public void setBusObjNm(String busObjNm) {
        this.busObjNm = busObjNm;
    }

    public ZonedDateTime getHalEventTs() {
        return halEventTs;
    }

    public void setHalEventTs(ZonedDateTime halEventTs) {
        this.halEventTs = halEventTs;
    }

}
