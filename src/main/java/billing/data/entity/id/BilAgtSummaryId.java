package billing.data.entity.id;

import static core.utils.CommonConstants.BLANK_STRING;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilAgtSummaryId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_AGT_ACT_ID", length = 8, nullable = false)
    private String bilAgtActId = BLANK_STRING;

    @Column(name = "BIL_ACY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAcyDt;

    @Column(name = "BIL_ACY_SEQ", length = 5, nullable = false)
    private short bilAcySeq;

    public BilAgtSummaryId() {
        super();
    }

    public BilAgtSummaryId(String bilAgtActId, ZonedDateTime bilAcyDt, short bilAcySeq) {
        super();
        this.bilAgtActId = bilAgtActId;
        this.bilAcyDt = bilAcyDt;
        this.bilAcySeq = bilAcySeq;
    }

    public String getBilAgtActId() {
        return bilAgtActId;
    }

    public void setBilAgtActId(String bilAgtActId) {
        this.bilAgtActId = bilAgtActId;
    }

    public ZonedDateTime getBilAcyDt() {
        return bilAcyDt;
    }

    public void setBilAcyDt(ZonedDateTime bilAcyDt) {
        this.bilAcyDt = bilAcyDt;
    }

    public short getBilAcySeq() {
        return bilAcySeq;
    }

    public void setBilAcySeq(short bilAcySeq) {
        this.bilAcySeq = bilAcySeq;
    }

}
