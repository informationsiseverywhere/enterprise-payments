package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

public class BilFunctionCtlId implements Serializable {

    private static final long serialVersionUID = 1;

    @Column(name = "BIL_FUN_TYP_CD", length = 3, nullable = false)
    private String bilFunTypCd = SEPARATOR_BLANK;

    @Column(name = "BIL_FUN_CD", length = 3, nullable = false)
    private String bilFunCd = SEPARATOR_BLANK;

    @Column(name = "RISK_STATE_CD", length = 3, nullable = false)
    private String riskStateCd = SEPARATOR_BLANK;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNbr = SEPARATOR_BLANK;

    @Column(name = "LOB_CD", length = 3, nullable = false)
    private String lobCd = SEPARATOR_BLANK;

    @Column(name = "EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime effDt;

    public String getBilFunTypCd() {
        return bilFunTypCd;
    }

    public void setBilFunTypCd(String bilFunTypCd) {
        this.bilFunTypCd = bilFunTypCd;
    }

    public String getBilFunCd() {
        return bilFunCd;
    }

    public void setBilFunCd(String bilFunCd) {
        this.bilFunCd = bilFunCd;
    }

    public String getRiskStateCd() {
        return riskStateCd;
    }

    public void setRiskStateCd(String riskStateCd) {
        this.riskStateCd = riskStateCd;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public String getLobCd() {
        return lobCd;
    }

    public void setLobCd(String lobCd) {
        this.lobCd = lobCd;
    }

    public ZonedDateTime getEffDt() {
        return effDt;
    }

    public void setEffDt(ZonedDateTime effDt) {
        this.effDt = effDt;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
