package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilActSummaryId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String bilAccountId = SEPARATOR_BLANK;

    @Column(name = "BIL_ACY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAcyDt;

    @Column(name = "BIL_ACY_SEQ", length = 8, nullable = false)
    private short bilAcySeq;

    public BilActSummaryId() {
        super();
    }

    public BilActSummaryId(String bilAccountId, ZonedDateTime bilAcyDt, short bilAcySeq) {
        super();
        this.bilAccountId = bilAccountId;
        this.bilAcyDt = bilAcyDt;
        this.bilAcySeq = bilAcySeq;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public ZonedDateTime getBilAcyDt() {
        return bilAcyDt;
    }

    public void setBilAcyDt(ZonedDateTime bilAcyDt) {
        this.bilAcyDt = bilAcyDt;
    }

    public short getBilAcySeq() {
        return bilAcySeq;
    }

    public void setBilAcySeq(short bilAcySeq) {
        this.bilAcySeq = bilAcySeq;
    }

}
