package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilSeasExclId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_SEA_XCL_CD", length = 10, nullable = false)
    private String bilSeaXclCd = SEPARATOR_BLANK;

    @Column(name = "BIL_SEA_START_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilSeaStartDt;

    public BilSeasExclId() {
        super();
    }

    public BilSeasExclId(String bilSeaXclCd, ZonedDateTime bilSeaStartDt) {
        super();
        this.bilSeaXclCd = bilSeaXclCd;
        this.bilSeaStartDt = bilSeaStartDt;
    }

    public String getBilSeaXclCd() {
        return bilSeaXclCd;
    }

    public void setBilSeaXclCd(String bilSeaXclCd) {
        this.bilSeaXclCd = bilSeaXclCd;
    }

    public ZonedDateTime getBilSeaStartDt() {
        return bilSeaStartDt;
    }

    public void setBilSeaStartDt(ZonedDateTime bilSeaStartDt) {
        this.bilSeaStartDt = bilSeaStartDt;
    }

}
