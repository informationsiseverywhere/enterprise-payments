package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class HolidayPrcId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "HOLIDAY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime holidayDt;

    @Column(name = "CTR_CD", length = 4, nullable = false)
    private String ctrCd = SEPARATOR_BLANK;

    @Column(name = "STATE_CD", length = 3, nullable = false)
    private String stateCd = SEPARATOR_BLANK;

    public HolidayPrcId() {
        super();
    }

    public HolidayPrcId(ZonedDateTime holidayDt, String ctrCd, String stateCd) {
        super();
        this.holidayDt = holidayDt;
        this.ctrCd = ctrCd;
        this.stateCd = stateCd;
    }

    public ZonedDateTime getHolidayDt() {
        return holidayDt;
    }

    public void setHolidayDt(ZonedDateTime holidayDt) {
        this.holidayDt = holidayDt;
    }

    public String getCtrCd() {
        return ctrCd;
    }

    public void setCtrCd(String ctrCd) {
        this.ctrCd = ctrCd;
    }

    public String getStateCd() {
        return stateCd;
    }

    public void setStateCd(String stateCd) {
        this.stateCd = stateCd;
    }

}
