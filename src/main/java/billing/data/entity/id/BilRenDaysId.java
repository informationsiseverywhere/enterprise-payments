package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilRenDaysId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_STATE_PVN_CD", length = 3, nullable = false)
    private String bilStatePvnCd = SEPARATOR_BLANK;

    @Column(name = "LOB_CD", length = 3, nullable = false)
    private String lobCd = SEPARATOR_BLANK;

    @Column(name = "MASTER_COMPANY_NBR", length = 3, nullable = false)
    private String masterCompanyNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_ISSUE_SYS_ID", length = 2, nullable = false)
    private String bilIssueSysId = SEPARATOR_BLANK;

    public BilRenDaysId() {
        super();
    }

    public BilRenDaysId(String bilStatePvnCd, String lobCd, String masterCompanyNbr, String bilIssueSysId) {
        super();
        this.bilStatePvnCd = bilStatePvnCd;
        this.lobCd = lobCd;
        this.masterCompanyNbr = masterCompanyNbr;
        this.bilIssueSysId = bilIssueSysId;
    }

    public String getBilStatePvnCd() {
        return bilStatePvnCd;
    }

    public void setBilStatePvnCd(String bilStatePvnCd) {
        this.bilStatePvnCd = bilStatePvnCd;
    }

    public String getLobCd() {
        return lobCd;
    }

    public void setLobCd(String lobCd) {
        this.lobCd = lobCd;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public String getBilIssueSysId() {
        return bilIssueSysId;
    }

    public void setBilIssueSysId(String bilIssueSysId) {
        this.bilIssueSysId = bilIssueSysId;
    }

}
