package billing.data.entity.id;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilEftPendingTapeId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_TCH_KEY_ID", length = 8, nullable = false)
    private String technicalKey = SEPARATOR_BLANK;

    @Column(name = "BIL_TCH_KEY_TYP_CD", nullable = false)
    private char technicalKeyType = BLANK_CHAR;

    @Column(name = "BIL_EFT_TAPE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime eftTapeDate;

    @Column(name = "BIL_EFT_ACY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime eftActivityDate;

    @Column(name = "BIL_SEQ_NBR")
    private short sequenceNbr;

    public BilEftPendingTapeId(String technicalKey, char technicalKeyType, ZonedDateTime eftTapeDate,
            ZonedDateTime eftActivityDate, short sequenceNbr) {
        super();
        this.technicalKey = technicalKey;
        this.technicalKeyType = technicalKeyType;
        this.eftTapeDate = eftTapeDate;
        this.eftActivityDate = eftActivityDate;
        this.sequenceNbr = sequenceNbr;
    }

    public BilEftPendingTapeId() {
        super();
    }

    public String getTechnicalKey() {
        return technicalKey;
    }

    public void setTechnicalKey(String technicalKey) {
        this.technicalKey = technicalKey;
    }

    public char getTechnicalKeyType() {
        return technicalKeyType;
    }

    public void setTechnicalKeyType(char technicalKeyType) {
        this.technicalKeyType = technicalKeyType;
    }

    public ZonedDateTime getEftTapeDate() {
        return eftTapeDate;
    }

    public void setEftTapeDate(ZonedDateTime eftTapeDate) {
        this.eftTapeDate = eftTapeDate;
    }

    public ZonedDateTime getEftActivityDate() {
        return eftActivityDate;
    }

    public void setEftActivityDate(ZonedDateTime eftActivityDate) {
        this.eftActivityDate = eftActivityDate;
    }

    public short getSequenceNbr() {
        return sequenceNbr;
    }

    public void setSequenceNbr(short sequenceNbr) {
        this.sequenceNbr = sequenceNbr;
    }

}