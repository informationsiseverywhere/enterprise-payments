package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the BIL_PRM_SCHEDULE database table.
 *
 */
@Embeddable
public class BilPrmScheduleId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String bilAccountId = SEPARATOR_BLANK;

    @Column(name = "POLICY_ID", length = 16, nullable = false)
    private String policyId = SEPARATOR_BLANK;

    @Column(name = "BIL_SEQ_NBR", length = 5, nullable = false)
    private short bilSeqNbr = SHORT_ZERO;

    public BilPrmScheduleId() {
        super();
    }

    public BilPrmScheduleId(String bilAccountId, String policyId, short bilSeqNbr) {
        super();
        this.bilAccountId = bilAccountId;
        this.policyId = policyId;
        this.bilSeqNbr = bilSeqNbr;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public short getBilSeqNbr() {
        return bilSeqNbr;
    }

    public void setBilSeqNbr(short bilSeqNbr) {
        this.bilSeqNbr = bilSeqNbr;
    }

}