package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BilSchAutoReconCshId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_TCH_KEY_ID", length = 8, nullable = false)
    private String bilTchKeyId = SEPARATOR_BLANK;

    @Column(name = "BIL_TCH_KEY_TYP_CD", nullable = false)
    private char bilTchKeyTypCd;

    @Column(name = "BIL_SEQ_NBR", length = 5, nullable = false)
    private short bilSeqNbr = SHORT_ZERO;

    public BilSchAutoReconCshId() {
        super();
    }

    public BilSchAutoReconCshId(String bilTchKeyId, char bilTchKeyTypCd, short bilInvSeqNbr) {
        super();
        this.bilTchKeyId = bilTchKeyId;
        this.bilTchKeyTypCd = bilTchKeyTypCd;
        this.bilSeqNbr = bilInvSeqNbr;
    }

    public String getBilTchKeyId() {
        return bilTchKeyId;
    }

    public void setBilTchKeyId(String bilTchKeyId) {
        this.bilTchKeyId = bilTchKeyId;
    }

    public char getBilTchKeyTypCd() {
        return bilTchKeyTypCd;
    }

    public void setBilTchKeyTypCd(char bilTchKeyTypCd) {
        this.bilTchKeyTypCd = bilTchKeyTypCd;
    }

    public short getBilSeqNbr() {
        return bilSeqNbr;
    }

    public void setBilSeqNbr(short bilSeqNbr) {
        this.bilSeqNbr = bilSeqNbr;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
