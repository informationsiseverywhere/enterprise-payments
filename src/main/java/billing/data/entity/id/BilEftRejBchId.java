package billing.data.entity.id;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilEftRejBchId implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Column(name = "BRB_ENTRY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime brbEntryDt;
    
    @Column(name = "BRB_BCH_NBR", nullable = false)
    private String brbBchNbr;
    
    @Column(name = "USER_ID", nullable = false)
    private String userId;
    
    @Column(name = "BRB_ENTRY_SEQ_NBR", nullable = false)
    private short brbEntrySeqNbr;
    

    public BilEftRejBchId() {
        super();
           }

    public BilEftRejBchId(ZonedDateTime brbEntryDt, String brbBchNbr, String userId, short brbEntrySeqNbr) {
        super();
        this.brbEntryDt = brbEntryDt;
        this.brbBchNbr = brbBchNbr;
        this.userId = userId;
        this.brbEntrySeqNbr = brbEntrySeqNbr;
    }

    public ZonedDateTime getBrbEntryDt() {
        return brbEntryDt;
    }

    public void setBrbEntryDt(ZonedDateTime brbEntryDt) {
        this.brbEntryDt = brbEntryDt;
    }

    public String getBrbBchNbr() {
        return brbBchNbr;
    }

    public void setBrbBchNbr(String brbBchNbr) {
        this.brbBchNbr = brbBchNbr;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public short getBrbEntrySeqNbr() {
        return brbEntrySeqNbr;
    }

    public void setBrbEntrySeqNbr(short brbEntrySeqNbr) {
        this.brbEntrySeqNbr = brbEntrySeqNbr;
    }
    
    
    
}
