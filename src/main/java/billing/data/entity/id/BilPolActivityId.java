package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilPolActivityId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ACCOUNT_ID", length = 8, nullable = false)
    private String bilAccountId = SEPARATOR_BLANK;

    @Column(name = "POLICY_ID", length = 16, nullable = false)
    private String policyId = SEPARATOR_BLANK;

    @Column(name = "POL_EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime polEffectiveDt;

    @Column(name = "BIL_ACY_TYPE_CD", length = 3, nullable = false)
    private String bilAcyTypeCd = SEPARATOR_BLANK;

    public BilPolActivityId() {
        super();
    }

    public BilPolActivityId(String bilAccountId, String policyId, ZonedDateTime polEffectiveDt, String bilAcyTypeCd) {
        super();
        this.bilAccountId = bilAccountId;
        this.policyId = policyId;
        this.polEffectiveDt = polEffectiveDt;
        this.bilAcyTypeCd = bilAcyTypeCd;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public ZonedDateTime getPolEffectiveDt() {
        return polEffectiveDt;
    }

    public void setPolEffectiveDt(ZonedDateTime polEffectiveDt) {
        this.polEffectiveDt = polEffectiveDt;
    }

    public String getBilAcyTypeCd() {
        return bilAcyTypeCd;
    }

    public void setBilAcyTypeCd(String bilAcyTypeCd) {
        this.bilAcyTypeCd = bilAcyTypeCd;
    }

}
