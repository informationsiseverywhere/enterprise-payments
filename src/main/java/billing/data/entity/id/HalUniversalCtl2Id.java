package billing.data.entity.id;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * The primary key class for the HAL_UNIVERSAL_CTL2 database table.
 *
 */
@Embeddable
public class HalUniversalCtl2Id implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "HUC2_ENTRY_KEY_CD", unique = true, nullable = false, length = 150)
    private String entryKey;

    @Column(name = "HUC2_TBL_LBL_TXT", unique = true, nullable = false, length = 32)
    private String label;

    @Column(name = "EFFECTIVE_DT", unique = true, nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime effectiveDate;

    public HalUniversalCtl2Id() {
        super();
    }

    public HalUniversalCtl2Id(String entryKey, String label, ZonedDateTime effectiveDate) {
        super();
        this.entryKey = entryKey;
        this.label = label;
        this.effectiveDate = effectiveDate;
    }

    public String getEntryKey() {
        return entryKey != null ? entryKey.trim() : entryKey;
    }

    public void setEntryKey(String entryKey) {
        this.entryKey = entryKey;
    }

    public String getLabel() {
        return label != null ? label.trim() : label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public ZonedDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof HalUniversalCtl2Id)) {
            return false;
        }
        HalUniversalCtl2Id castOther = (HalUniversalCtl2Id) other;
        return this.entryKey.equals(castOther.entryKey) && this.label.equals(castOther.label)
                && this.effectiveDate.equals(castOther.effectiveDate);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + this.entryKey.hashCode();
        hash = hash * prime + this.label.hashCode();
        hash = hash * prime + this.effectiveDate.hashCode();

        return hash;
    }
}