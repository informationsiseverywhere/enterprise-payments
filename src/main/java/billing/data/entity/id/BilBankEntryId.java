package billing.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilBankEntryId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_ENTRY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime entryDt;

    @Column(name = "BIL_ENTRY_NBR", length = 4, nullable = false)
    private String entryNbr = SEPARATOR_BLANK;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "BIL_ENTRY_SEQ_NBR", nullable = false)
    private short entySeqNbr;

    public BilBankEntryId(ZonedDateTime entryDt, String entryNbr, String userId, short entySeqNbr) {
        super();
        this.entryDt = entryDt;
        this.entryNbr = entryNbr;
        this.userId = userId;
        this.entySeqNbr = entySeqNbr;

    }

    public BilBankEntryId() {
        super();
    }

    public ZonedDateTime getEntryDt() {
        return entryDt;
    }

    public void setEntryDt(ZonedDateTime entryDt) {
        this.entryDt = entryDt;
    }

    public String getEntryNbr() {
        return entryNbr;
    }

    public void setEntryNbr(String entryNbr) {
        this.entryNbr = entryNbr;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public short getEntySeqNbr() {
        return entySeqNbr;
    }

    public void setEntySeqNbr(short entySeqNbr) {
        this.entySeqNbr = entySeqNbr;
    }

}