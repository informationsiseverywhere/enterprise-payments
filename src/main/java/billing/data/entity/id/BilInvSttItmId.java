package billing.data.entity.id;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class BilInvSttItmId implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BIL_TCH_KEY_ID", length = 8, nullable = false)
    private String bilTchKeyId = SEPARATOR_BLANK;

    @Column(name = "BIL_TCH_KEY_TYP_CD", nullable = false)
    private char bilTchKeyTypCd = BLANK_CHAR;

    @Column(name = "BIL_INV_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilInvDt;

    @Column(name = "BIL_INV_SEQ_NBR", length = 5, nullable = false)
    private short bilInvSeqNbr = SHORT_ZERO;

    public BilInvSttItmId() {
        super();
    }

    public BilInvSttItmId(String bilTchKeyId, char bilTchKeyTypCd, ZonedDateTime bilInvDt, short bilInvSeqNbr) {
        super();
        this.bilTchKeyId = bilTchKeyId;
        this.bilTchKeyTypCd = bilTchKeyTypCd;
        this.bilInvDt = bilInvDt;
        this.bilInvSeqNbr = bilInvSeqNbr;
    }

    public String getBilTchKeyId() {
        return bilTchKeyId;
    }

    public void setBilTchKeyId(String bilTchKeyId) {
        this.bilTchKeyId = bilTchKeyId;
    }

    public char getBilTchKeyTypCd() {
        return bilTchKeyTypCd;
    }

    public void setBilTchKeyTypCd(char bilTchKeyTypCd) {
        this.bilTchKeyTypCd = bilTchKeyTypCd;
    }

    public ZonedDateTime getBilInvDt() {
        return bilInvDt;
    }

    public void setBilInvDt(ZonedDateTime bilInvDt) {
        this.bilInvDt = bilInvDt;
    }

    public short getBilInvSeqNbr() {
        return bilInvSeqNbr;
    }

    public void setBilInvSeqNbr(short bilInvSeqNbr) {
        this.bilInvSeqNbr = bilInvSeqNbr;
    }

}
