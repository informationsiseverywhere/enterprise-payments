package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "BIL_EFT_BANK_ED")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "bilEftBankEd")
public class BilEftBankEd implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "BIL_EFT_BANK_ID", length = 10, nullable = false)
    private Integer eftBankId;

    @Column(name = "BBE_PRF_TRA_NBR", length = 10, nullable = false)
    private String preferredRoutingTransitNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_RTE_TRA_NBR", length = 10, nullable = false)
    private String routingTransitNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_BANK_NM", length = 512, nullable = false)
    private String bankName = SEPARATOR_BLANK;

    @Column(name = "BBE_BANK_ADR_1", length = 64, nullable = false)
    private String bankAddress1 = SEPARATOR_BLANK;

    @Column(name = "BBE_BANK_ADR_2", length = 64, nullable = false)
    private String bankAddress2 = SEPARATOR_BLANK;

    @Column(name = "BBE_BANK_ADR_3", length = 64, nullable = false)
    private String bankAddress3 = SEPARATOR_BLANK;

    @Column(name = "BBE_BANK_ADR_4", length = 64, nullable = false)
    private String bankAddress4 = SEPARATOR_BLANK;

    @Column(name = "BBE_BANK_PHONE_NBR", length = 21, nullable = false)
    private String bankPhoneNbr = SEPARATOR_BLANK;

    public Integer getEftBankId() {
        return eftBankId;
    }

    public void setEftBankId(Integer eftBankId) {
        this.eftBankId = eftBankId;
    }

    public String getPreferredRoutingTransitNbr() {
        return preferredRoutingTransitNbr;
    }

    public void setPreferredRoutingTransitNbr(String preferredRoutingTransitNbr) {
        this.preferredRoutingTransitNbr = preferredRoutingTransitNbr;
    }

    public String getRoutingTransitNbr() {
        return routingTransitNbr;
    }

    public void setRoutingTransitNbr(String routingTransitNbr) {
        this.routingTransitNbr = routingTransitNbr;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAddress1() {
        return bankAddress1;
    }

    public void setBankAddress1(String bankAddress1) {
        this.bankAddress1 = bankAddress1;
    }

    public String getBankAddress2() {
        return bankAddress2;
    }

    public void setBankAddress2(String bankAddress2) {
        this.bankAddress2 = bankAddress2;
    }

    public String getBankAddress3() {
        return bankAddress3;
    }

    public void setBankAddress3(String bankAddress3) {
        this.bankAddress3 = bankAddress3;
    }

    public String getBankAddress4() {
        return bankAddress4;
    }

    public void setBankAddress4(String bankAddress4) {
        this.bankAddress4 = bankAddress4;
    }

    public String getBankPhoneNbr() {
        return bankPhoneNbr;
    }

    public void setBankPhoneNbr(String bankPhoneNbr) {
        this.bankPhoneNbr = bankPhoneNbr;
    }

}