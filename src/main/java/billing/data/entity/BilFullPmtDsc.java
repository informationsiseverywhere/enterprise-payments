package billing.data.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilFullPmtDscId;

@Entity
@Table(name = "BIL_FULL_PMT_DSC")
public class BilFullPmtDsc implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilFullPmtDscId bilFullPmtDscId;

    @Column(name = "EXPIRATION_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime expirationDt;

    @Column(name = "BIL_DSC_PAID_PCT", precision = 5, scale = 2, nullable = false)
    private double bilDscPaidPct;

    @Column(name = "BIL_FLP_DSC_AMT_CD", nullable = false)
    private char bilFlpDscAmtCd;

    @Column(name = "BIL_PMT_QFY_CD", nullable = false)
    private char bilPmtQfyCd;

    @Column(name = "BRD_PSTMRK_DT_IND", nullable = false)
    private char brdPstMrkDtInd;

    @Column(name = "FLP_DSC_EXP1_NBR", length = 5, nullable = false)
    private short flpDscExp1Nbr;

    @Column(name = "FLP_DSC_EXP2_NBR", length = 5, nullable = false)
    private short flpDscExp2Nbr;

    public BilFullPmtDscId getBilFullPmtDscId() {
        return bilFullPmtDscId;
    }

    public void setBilFullPmtDscId(BilFullPmtDscId bilFullPmtDscId) {
        this.bilFullPmtDscId = bilFullPmtDscId;
    }

    public ZonedDateTime getExpirationDt() {
        return expirationDt;
    }

    public void setExpirationDt(ZonedDateTime expirationDt) {
        this.expirationDt = expirationDt;
    }

    public double getBilDscPaidPct() {
        return bilDscPaidPct;
    }

    public void setBilDscPaidPct(double bilDscPaidPct) {
        this.bilDscPaidPct = bilDscPaidPct;
    }

    public char getBilFlpDscAmtCd() {
        return bilFlpDscAmtCd;
    }

    public void setBilFlpDscAmtCd(char bilFlpDscAmtCd) {
        this.bilFlpDscAmtCd = bilFlpDscAmtCd;
    }

    public char getBilPmtQfyCd() {
        return bilPmtQfyCd;
    }

    public void setBilPmtQfyCd(char bilPmtQfyCd) {
        this.bilPmtQfyCd = bilPmtQfyCd;
    }

    public char getBrdPstMrkDtInd() {
        return brdPstMrkDtInd;
    }

    public void setBrdPstMrkDtInd(char brdPstMrkDtInd) {
        this.brdPstMrkDtInd = brdPstMrkDtInd;
    }

    public short getFlpDscExp1Nbr() {
        return flpDscExp1Nbr;
    }

    public void setFlpDscExp1Nbr(short flpDscExp1Nbr) {
        this.flpDscExp1Nbr = flpDscExp1Nbr;
    }

    public short getFlpDscExp2Nbr() {
        return flpDscExp2Nbr;
    }

    public void setFlpDscExp2Nbr(short flpDscExp2Nbr) {
        this.flpDscExp2Nbr = flpDscExp2Nbr;
    }

}
