package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilCashEntryTotId;

@Entity
@Table(name = "BIL_CASH_ENTRY_TOT")
public class BilCashEntryTot implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilCashEntryTotId bilCashEntryTotId;

    @Column(name = "BCT_TOT_CASH_AMT", precision = 14, scale = 2, nullable = false)
    private double bctTotCashAmt;

    @Column(name = "BCT_DEPOSIT_IND", nullable = false)
    private char bctDepositInd;

    @Column(name = "BCT_ACCEPT_IND", nullable = false)
    private char bctAcceptInd;

    @Column(name = "BIL_BANK_CD", length = 3, nullable = false)
    private String bilBankCd = SEPARATOR_BLANK;

    @Column(name = "BCT_ACCEPT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bctAcceptDt;

    @Column(name = "BIL_CSH_ETR_MTH_CD", nullable = false)
    private char bilCshEtrMthCd;

    @Column(name = "BIL_DEPOSIT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilDepositDt;

    @Column(name = "CURRENCY_CD", length = 4)
    private String currencyCd = SEPARATOR_BLANK;

    @Column(name = "BCT_DEP_BANK_CD", length = 3, nullable = false)
    private String bctDepBankCd = SEPARATOR_BLANK;

    public BilCashEntryTotId getBilCashEntryTotId() {
        return bilCashEntryTotId;
    }

    public void setBilCashEntryTotId(BilCashEntryTotId bilCashEntryTotId) {
        this.bilCashEntryTotId = bilCashEntryTotId;
    }

    public double getBctTotCashAmt() {
        return bctTotCashAmt;
    }

    public void setBctTotCashAmt(double bctTotCashAmt) {
        this.bctTotCashAmt = bctTotCashAmt;
    }

    public char getBctDepositInd() {
        return bctDepositInd;
    }

    public void setBctDepositInd(char bctDepositInd) {
        this.bctDepositInd = bctDepositInd;
    }

    public char getBctAcceptInd() {
        return bctAcceptInd;
    }

    public void setBctAcceptInd(char bctAcceptInd) {
        this.bctAcceptInd = bctAcceptInd;
    }

    public String getBilBankCd() {
        return bilBankCd;
    }

    public void setBilBankCd(String bilBankCd) {
        this.bilBankCd = bilBankCd;
    }

    public ZonedDateTime getBctAcceptDt() {
        return bctAcceptDt;
    }

    public void setBctAcceptDt(ZonedDateTime bctAcceptDt) {
        this.bctAcceptDt = bctAcceptDt;
    }

    public char getBilCshEtrMthCd() {
        return bilCshEtrMthCd;
    }

    public void setBilCshEtrMthCd(char bilCshEtrMthCd) {
        this.bilCshEtrMthCd = bilCshEtrMthCd;
    }

    public ZonedDateTime getBilDepositDt() {
        return bilDepositDt;
    }

    public void setBilDepositDt(ZonedDateTime bilDepositDt) {
        this.bilDepositDt = bilDepositDt;
    }

    public String getCurrencyCd() {
        return currencyCd;
    }

    public void setCurrencyCd(String currencyCd) {
        this.currencyCd = currencyCd;
    }

    public String getBctDepBankCd() {
        return bctDepBankCd;
    }

    public void setBctDepBankCd(String bctDepBankCd) {
        this.bctDepBankCd = bctDepBankCd;
    }

}