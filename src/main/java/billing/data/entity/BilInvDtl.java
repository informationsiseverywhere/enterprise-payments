package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilInvDtlId;

@Entity
@Table(name = "BIL_INV_DTL")
public class BilInvDtl implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilInvDtlId bilInvDtlId;

    @Column(name = "POL_NBR", length = 25, nullable = false)
    private String polNbr = SEPARATOR_BLANK;

    @Column(name = "POL_SYMBOL_CD", length = 3)
    private String polSymbolCd = SEPARATOR_BLANK;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNbr = SEPARATOR_BLANK;

    @Column(name = "BID_TRS_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bidTrsDt;

    @Column(name = "BID_TRS_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bidTrsEffDt;

    @Column(name = "BID_LH_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bidLhDueDt;

    @Column(name = "BIL_ACY_DES_CD", length = 3, nullable = false)
    private String bilAcyDesCd = SEPARATOR_BLANK;

    @Column(name = "BIL_DES_REA_TYP", length = 3, nullable = false)
    private String bilDesReaTyp = SEPARATOR_BLANK;

    @Column(name = "BIL_DES_REA_DES", length = 22, nullable = false)
    private String bilDesReaDes = SEPARATOR_BLANK;

    @Column(name = "BID_LOAN_BAL_AMT", precision = 14, scale = 2, nullable = false)
    private double bidLoanBalAmt = DECIMAL_ZERO;

    @Column(name = "BID_GRS_DETAIL_AMT", precision = 14, scale = 2, nullable = false)
    private double bidGrsDetailAmt = DECIMAL_ZERO;

    @Column(name = "BID_NET_DETAIL_AMT", precision = 14, scale = 2, nullable = false)
    private double bidNetDetailAmt = DECIMAL_ZERO;

    @Column(name = "BIL_COMMISSION_PCT", precision = 9, scale = 6, nullable = false)
    private double bilCommissionPct = DECIMAL_ZERO;

    @Column(name = "BIL_STT_SVG_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttSvgAmt = DECIMAL_ZERO;

    @Column(name = "BIL_STT_RECON_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttReconAmt = DECIMAL_ZERO;

    @Column(name = "BIL_STT_RECON_IND", nullable = false)
    private char bilSttReconInd = BLANK_CHAR;

    @Column(name = "BIL_SUS_REASON_CD", length = 3, nullable = false)
    private String bilSusReasonCd = SEPARATOR_BLANK;

    @Column(name = "BIL_EXC_REASON_CD", length = 3, nullable = false)
    private String bilExcReasonCd = SEPARATOR_BLANK;

    @Column(name = "BID_OPT_REQ_IND", nullable = false)
    private char bidOptReqInd = BLANK_CHAR;

    @Column(name = "BID_BACK_BILL_IND", nullable = false)
    private char bidBackBillInd = BLANK_CHAR;

    @Column(name = "BID_PBL_ITEM_QFY", length = 13, nullable = false)
    private String bidPblItemQfy = SEPARATOR_BLANK;

    @Column(name = "BID_LOAN_IRT_PCT", precision = 5, scale = 2, nullable = false)
    private double bidLoanIrtPct = DECIMAL_ZERO;

    @Column(name = "BID_UNP_SPREAD_CD", nullable = false)
    private char bidUnpSpreadCd = BLANK_CHAR;

    @Column(name = "BIL_AMT_CHG_CD", nullable = false)
    private char bilAmtChgCd = BLANK_CHAR;

    @Column(name = "BID_IST_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bidIstDueDt;

    @Column(name = "BIL_STT_DTL_TYP_CD", length = 3, nullable = false)
    private String bilSttDtlTypCd = SEPARATOR_BLANK;

    public BilInvDtlId getBilInvDtlId() {
        return bilInvDtlId;
    }

    public void setBilInvDtlId(BilInvDtlId bilInvDtlId) {
        this.bilInvDtlId = bilInvDtlId;
    }

    public String getPolNbr() {
        return polNbr;
    }

    public void setPolNbr(String polNbr) {
        this.polNbr = polNbr;
    }

    public String getPolSymbolCd() {
        return polSymbolCd;
    }

    public void setPolSymbolCd(String polSymbolCd) {
        this.polSymbolCd = polSymbolCd;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public ZonedDateTime getBidTrsDt() {
        return bidTrsDt;
    }

    public void setBidTrsDt(ZonedDateTime bidTrsDt) {
        this.bidTrsDt = bidTrsDt;
    }

    public ZonedDateTime getBidTrsEffDt() {
        return bidTrsEffDt;
    }

    public void setBidTrsEffDt(ZonedDateTime bidTrsEffDt) {
        this.bidTrsEffDt = bidTrsEffDt;
    }

    public ZonedDateTime getBidLhDueDt() {
        return bidLhDueDt;
    }

    public void setBidLhDueDt(ZonedDateTime bidLhDueDt) {
        this.bidLhDueDt = bidLhDueDt;
    }

    public String getBilAcyDesCd() {
        return bilAcyDesCd;
    }

    public void setBilAcyDesCd(String bilAcyDesCd) {
        this.bilAcyDesCd = bilAcyDesCd;
    }

    public String getBilDesReaTyp() {
        return bilDesReaTyp;
    }

    public void setBilDesReaTyp(String bilDesReaTyp) {
        this.bilDesReaTyp = bilDesReaTyp;
    }

    public String getBilDesReaDes() {
        return bilDesReaDes;
    }

    public void setBilDesReaDes(String bilDesReaDes) {
        this.bilDesReaDes = bilDesReaDes;
    }

    public double getBidLoanBalAmt() {
        return bidLoanBalAmt;
    }

    public void setBidLoanBalAmt(double bidLoanBalAmt) {
        this.bidLoanBalAmt = bidLoanBalAmt;
    }

    public double getBidGrsDetailAmt() {
        return bidGrsDetailAmt;
    }

    public void setBidGrsDetailAmt(double bidGrsDetailAmt) {
        this.bidGrsDetailAmt = bidGrsDetailAmt;
    }

    public double getBidNetDetailAmt() {
        return bidNetDetailAmt;
    }

    public void setBidNetDetailAmt(double bidNetDetailAmt) {
        this.bidNetDetailAmt = bidNetDetailAmt;
    }

    public double getBilCommissionPct() {
        return bilCommissionPct;
    }

    public void setBilCommissionPct(double bilCommissionPct) {
        this.bilCommissionPct = bilCommissionPct;
    }

    public double getBilSttSvgAmt() {
        return bilSttSvgAmt;
    }

    public void setBilSttSvgAmt(double bilSttSvgAmt) {
        this.bilSttSvgAmt = bilSttSvgAmt;
    }

    public double getBilSttReconAmt() {
        return bilSttReconAmt;
    }

    public void setBilSttReconAmt(double bilSttReconAmt) {
        this.bilSttReconAmt = bilSttReconAmt;
    }

    public char getBilSttReconInd() {
        return bilSttReconInd;
    }

    public void setBilSttReconInd(char bilSttReconInd) {
        this.bilSttReconInd = bilSttReconInd;
    }

    public String getBilSusReasonCd() {
        return bilSusReasonCd;
    }

    public void setBilSusReasonCd(String bilSusReasonCd) {
        this.bilSusReasonCd = bilSusReasonCd;
    }

    public String getBilExcReasonCd() {
        return bilExcReasonCd;
    }

    public void setBilExcReasonCd(String bilExcReasonCd) {
        this.bilExcReasonCd = bilExcReasonCd;
    }

    public char getBidOptReqInd() {
        return bidOptReqInd;
    }

    public void setBidOptReqInd(char bidOptReqInd) {
        this.bidOptReqInd = bidOptReqInd;
    }

    public char getBidBackBillInd() {
        return bidBackBillInd;
    }

    public void setBidBackBillInd(char bidBackBillInd) {
        this.bidBackBillInd = bidBackBillInd;
    }

    public String getBidPblItemQfy() {
        return bidPblItemQfy;
    }

    public void setBidPblItemQfy(String bidPblItemQfy) {
        this.bidPblItemQfy = bidPblItemQfy;
    }

    public double getBidLoanIrtPct() {
        return bidLoanIrtPct;
    }

    public void setBidLoanIrtPct(double bidLoanIrtPct) {
        this.bidLoanIrtPct = bidLoanIrtPct;
    }

    public char getBidUnpSpreadCd() {
        return bidUnpSpreadCd;
    }

    public void setBidUnpSpreadCd(char bidUnpSpreadCd) {
        this.bidUnpSpreadCd = bidUnpSpreadCd;
    }

    public char getBilAmtChgCd() {
        return bilAmtChgCd;
    }

    public void setBilAmtChgCd(char bilAmtChgCd) {
        this.bilAmtChgCd = bilAmtChgCd;
    }

    public ZonedDateTime getBidIstDueDt() {
        return bidIstDueDt;
    }

    public void setBidIstDueDt(ZonedDateTime bidIstDueDt) {
        this.bidIstDueDt = bidIstDueDt;
    }

    public String getBilSttDtlTypCd() {
        return bilSttDtlTypCd;
    }

    public void setBilSttDtlTypCd(String bilSttDtlTypCd) {
        this.bilSttDtlTypCd = bilSttDtlTypCd;
    }
}