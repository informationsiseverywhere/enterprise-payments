package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import billing.data.entity.id.BilCreScheduleId;

@Entity
@Table(name = "BIL_CRE_SCHEDULE")
public class BilCreSchedule implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilCreScheduleId bilCreScheduleId;

    @Column(name = "POL_EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime polEffectiveDt;

    @Column(name = "BCS_CRE_PBL_ITEM", length = 3, nullable = false)
    private String bcsCrePblItem = SEPARATOR_BLANK;

    @Column(name = "BCS_CRE_AMT_DSP", nullable = false)
    private char bcsCreAmtDsp;

    @Column(name = "BIL_PRM_IST_AMT", precision = 14, scale = 2, nullable = false)
    private double bilPrmIstAmt;

    @Column(name = "BIL_PRM_IST_NET", precision = 14, scale = 2, nullable = false)
    private double bilPrmIstNet;

    @Column(name = "BIL_COMMISSION_PCT", precision = 9, scale = 6, nullable = false)
    private double bilCommissionPct;

    @Column(name = "BIL_SYS_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bilSysDueDt;

    @Column(name = "BIL_ADJ_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bilAdjDueDt;

    @Column(name = "BIL_INV_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bilInvDt;

    public BilCreScheduleId getBilCreScheduleId() {
        return bilCreScheduleId;
    }

    public void setBilCreScheduleId(BilCreScheduleId bilCreScheduleId) {
        this.bilCreScheduleId = bilCreScheduleId;
    }

    public ZonedDateTime getPolEffectiveDt() {
        return polEffectiveDt;
    }

    public void setPolEffectiveDt(ZonedDateTime polEffectiveDt) {
        this.polEffectiveDt = polEffectiveDt;
    }

    public String getBcsCrePblItem() {
        return bcsCrePblItem;
    }

    public void setBcsCrePblItem(String bcsCrePblItem) {
        this.bcsCrePblItem = bcsCrePblItem;
    }

    public char getBcsCreAmtDsp() {
        return bcsCreAmtDsp;
    }

    public void setBcsCreAmtDsp(char bcsCreAmtDsp) {
        this.bcsCreAmtDsp = bcsCreAmtDsp;
    }

    public double getBilPrmIstAmt() {
        return bilPrmIstAmt;
    }

    public void setBilPrmIstAmt(double bilPrmIstAmt) {
        this.bilPrmIstAmt = bilPrmIstAmt;
    }

    public double getBilPrmIstNet() {
        return bilPrmIstNet;
    }

    public void setBilPrmIstNet(double bilPrmIstNet) {
        this.bilPrmIstNet = bilPrmIstNet;
    }

    public double getBilCommissionPct() {
        return bilCommissionPct;
    }

    public void setBilCommissionPct(double bilCommissionPct) {
        this.bilCommissionPct = bilCommissionPct;
    }

    public ZonedDateTime getBilSysDueDt() {
        return bilSysDueDt;
    }

    public void setBilSysDueDt(ZonedDateTime bilSysDueDt) {
        this.bilSysDueDt = bilSysDueDt;
    }

    public ZonedDateTime getBilAdjDueDt() {
        return bilAdjDueDt;
    }

    public void setBilAdjDueDt(ZonedDateTime bilAdjDueDt) {
        this.bilAdjDueDt = bilAdjDueDt;
    }

    public ZonedDateTime getBilInvDt() {
        return bilInvDt;
    }

    public void setBilInvDt(ZonedDateTime bilInvDt) {
        this.bilInvDt = bilInvDt;
    }

}