package billing.data.entity;

import static core.utils.CommonConstants.BLANK_STRING;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "HAL_BO_MDU_XRF")
public class HalBoMduXrf implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "BUS_OBJ_NM", nullable = false, length = 32)
    private String busObjectName = BLANK_STRING;

    @Column(name = "HBMX_DP_DFL_MDU_NM", nullable = false, length = 32)
    private String hbmxDpDflMduNm = BLANK_STRING;

    @Column(name = "HBMX_BOBJ_MDU_NM", nullable = false, length = 32)
    private String hbmxBobjMduNm = BLANK_STRING;

    @Column(name = "HBMX_CPX_ED_MDU_NM", nullable = false, length = 32)
    private String hbmxCpxEdMduNm = BLANK_STRING;

    public HalBoMduXrf() {
        super();
    }

    public HalBoMduXrf(String busObjectName, String hbmxDpDflMduNm, String hbmxBobjMduNm, String hbmxCpxEdMduNm) {
        super();
        this.busObjectName = busObjectName;
        this.hbmxDpDflMduNm = hbmxDpDflMduNm;
        this.hbmxBobjMduNm = hbmxBobjMduNm;
        this.hbmxCpxEdMduNm = hbmxCpxEdMduNm;
    }

    public String getBusObjectName() {
        return busObjectName;
    }

    public void setBusObjectName(String busObjectName) {
        this.busObjectName = busObjectName;
    }

    public String getHbmxDpDflMduNm() {
        return hbmxDpDflMduNm;
    }

    public void setHbmxDpDflMduNm(String hbmxDpDflMduNm) {
        this.hbmxDpDflMduNm = hbmxDpDflMduNm;
    }

    public String getHbmxBobjMduNm() {
        return hbmxBobjMduNm;
    }

    public void setHbmxBobjMduNm(String hbmxBobjMduNm) {
        this.hbmxBobjMduNm = hbmxBobjMduNm;
    }

    public String getHbmxCpxEdMduNm() {
        return hbmxCpxEdMduNm;
    }

    public void setHbmxCpxEdMduNm(String hbmxCpxEdMduNm) {
        this.hbmxCpxEdMduNm = hbmxCpxEdMduNm;
    }

}
