package billing.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilEftRejReaId;

@Entity
@Table(name = "BIL_EFT_REJ_REA")
public class BilEftRejRea implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilEftRejReaId bilEftRejReaId;
    
    @Column(name = "BRR_WIP_IND", nullable = false)
    private char brrWipInd;
    
    @Column(name = "BRR_REJ_TXT", nullable = false)
    private String brrRejTxt;
    
    @Column(name = "BRR_NSF_IND", nullable = false)
    private char brrNsfInd;
    
    @Column(name = "BRR_FORM_ID", nullable = false)
    private String brrFormId;
    
    @Column(name = "BRR_ACY_IND", nullable = false)
    private char brrAcyInd;

    public BilEftRejReaId getBilEftRejReaId() {
        return bilEftRejReaId;
    }

    public void setBilEftRejReaId(BilEftRejReaId bilEftRejReaId) {
        this.bilEftRejReaId = bilEftRejReaId;
    }

    public char getBrrWipInd() {
        return brrWipInd;
    }

    public void setBrrWipInd(char brrWipInd) {
        this.brrWipInd = brrWipInd;
    }

    public String getBrrRejTxt() {
        return brrRejTxt;
    }

    public void setBrrRejTxt(String brrRejTxt) {
        this.brrRejTxt = brrRejTxt;
    }

    public char getBrrNsfInd() {
        return brrNsfInd;
    }

    public void setBrrNsfInd(char brrNsfInd) {
        this.brrNsfInd = brrNsfInd;
    }

    public String getBrrFormId() {
        return brrFormId;
    }

    public void setBrrFormId(String brrFormId) {
        this.brrFormId = brrFormId;
    }

    public char getBrrAcyInd() {
        return brrAcyInd;
    }

    public void setBrrAcyInd(char brrAcyInd) {
        this.brrAcyInd = brrAcyInd;
    }

    
    
}
