package billing.data.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilEftRejBchId;

@Entity
@Table(name = "BIL_EFT_REJ_BCH")
public class BilEftRejBch implements Serializable {

    
    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    private BilEftRejBchId bilEftRejBchId;
    
    @Column(name = "BIL_ACCOUNT_ID", nullable = false)
    private String bilAccountId;
    
    @Column(name = "BIL_ACCOUNT_NBR", nullable = false)
    private String bilAccountNumber;
    
    @Column(name = "BIL_THIRD_PARTY_ID", nullable = false)
    private String bilThirdPartyId;
    
    @Column(name = "BIL_TTY_NBR", nullable = false)
    private String bilTtyNbr;
    
    @Column(name = "BIL_EFT_REJ_CD", nullable = false)
    private String bilEftRejCd;
    
    @Column(name = "BIL_EFT_DR_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEftDrDt;
    
    @Column(name = "BIL_EFT_TRACE_NBR", nullable = false)
    private String bilEftTraceNbr;
    
    @Column(name = "BIL_BANK_ACCT_NBR", nullable = false)
    private String bilBankAccNbr;
    
    @Column(name = "BRB_ACE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime brbAceDt;
    
    @Column(name = "BIL_EFT_DR_AMT",precision = 14, scale = 2, nullable = false)
    private double bilEftDrAmt;
    
    @Column(name = "BRB_REVIEW_CD", nullable = false)
    private String brbReviewCd;
    
    @Column(name = "BRB_REJ_STA_IND", nullable = false)
    private char brbRejStaInd;
    
    @Column(name = "BIL_CRCRD_TYPE_CD", nullable = false)
    private char bilCrCrdTypeCd;
    
    @Column(name = "BIL_PAY_LNG_NM", nullable = false)
    private String bilPayLngNm;
    
    @Column(name = "BIL_UNID_CASH_ID", nullable = false)
    private String bilUnidCashId;
    
    @Column(name = "BIL_UNID_CASH_DIR", nullable = false)
    private String bilUnidCashDir;
    
    @Column(name = "AGENCY_NBR", nullable = false)
    private String agencyNbr;

    public BilEftRejBchId getBilEftRejBchId() {
        return bilEftRejBchId;
    }

    public void setBilEftRejBchId(BilEftRejBchId bilEftRejBchId) {
        this.bilEftRejBchId = bilEftRejBchId;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public String getBilAccountNumber() {
        return bilAccountNumber;
    }

    public void setBilAccountNumber(String bilAccountNumber) {
        this.bilAccountNumber = bilAccountNumber;
    }

    public String getBilThirdPartyId() {
        return bilThirdPartyId;
    }

    public void setBilThirdPartyId(String bilThirdPartyId) {
        this.bilThirdPartyId = bilThirdPartyId;
    }

    public String getBilTtyNbr() {
        return bilTtyNbr;
    }

    public void setBilTtyNbr(String bilTtyNbr) {
        this.bilTtyNbr = bilTtyNbr;
    }

    public String getBilEftRejCd() {
        return bilEftRejCd;
    }

    public void setBilEftRejCd(String bilEftRejCd) {
        this.bilEftRejCd = bilEftRejCd;
    }

    public ZonedDateTime getBilEftDrDt() {
        return bilEftDrDt;
    }

    public void setBilEftDrDt(ZonedDateTime bilEftDrDt) {
        this.bilEftDrDt = bilEftDrDt;
    }

    public String getBilEftTraceNbr() {
        return bilEftTraceNbr;
    }

    public void setBilEftTraceNbr(String bilEftTraceNbr) {
        this.bilEftTraceNbr = bilEftTraceNbr;
    }

    public String getBilBankAccNbr() {
        return bilBankAccNbr;
    }

    public void setBilBankAccNbr(String bilBankAccNbr) {
        this.bilBankAccNbr = bilBankAccNbr;
    }

    public ZonedDateTime getBrbAceDt() {
        return brbAceDt;
    }

    public void setBrbAceDt(ZonedDateTime brbAceDt) {
        this.brbAceDt = brbAceDt;
    }

    public double getBilEftDrAmt() {
        return bilEftDrAmt;
    }

    public void setBilEftDrAmt(double bilEftDrAmt) {
        this.bilEftDrAmt = bilEftDrAmt;
    }

    public String getBrbReviewCd() {
        return brbReviewCd;
    }

    public void setBrbReviewCd(String brbReviewCd) {
        this.brbReviewCd = brbReviewCd;
    }

    public char getBrbRejStaInd() {
        return brbRejStaInd;
    }

    public void setBrbRejStaInd(char brbRejStaInd) {
        this.brbRejStaInd = brbRejStaInd;
    }

    public char getBilCrCrdTypeCd() {
        return bilCrCrdTypeCd;
    }

    public void setBilCrCrdTypeCd(char bilCrCrdTypeCd) {
        this.bilCrCrdTypeCd = bilCrCrdTypeCd;
    }

    public String getBilPayLngNm() {
        return bilPayLngNm;
    }

    public void setBilPayLngNm(String bilPayLngNm) {
        this.bilPayLngNm = bilPayLngNm;
    }

    public String getBilUnidCashId() {
        return bilUnidCashId;
    }

    public void setBilUnidCashId(String bilUnidCashId) {
        this.bilUnidCashId = bilUnidCashId;
    }

    public String getBilUnidCashDir() {
        return bilUnidCashDir;
    }

    public void setBilUnidCashDir(String bilUnidCashDir) {
        this.bilUnidCashDir = bilUnidCashDir;
    }

    public String getAgencyNbr() {
        return agencyNbr;
    }

    public void setAgencyNbr(String agencyNbr) {
        this.agencyNbr = agencyNbr;
    }
    

}

