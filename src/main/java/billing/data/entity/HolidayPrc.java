package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.HolidayPrcId;

@Entity
@Table(name = "HOLIDAY_PRC")
public class HolidayPrc implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private HolidayPrcId holidayPrcId;

    @Column(name = "APP_01_XCL_CD", length = 3, nullable = false)
    private String app01XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_02_XCL_CD", length = 3, nullable = false)
    private String app02XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_03_XCL_CD", length = 3, nullable = false)
    private String app03XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_04_XCL_CD", length = 3, nullable = false)
    private String app04XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_05_XCL_CD", length = 3, nullable = false)
    private String app05XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_06_XCL_CD", length = 3, nullable = false)
    private String app06XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_07_XCL_CD", length = 3, nullable = false)
    private String app07XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_08_XCL_CD", length = 3, nullable = false)
    private String app08XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_09_XCL_CD", length = 3, nullable = false)
    private String app09XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_10_XCL_CD", length = 3, nullable = false)
    private String app10XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_11_XCL_CD", length = 3, nullable = false)
    private String app11XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_12_XCL_CD", length = 3, nullable = false)
    private String app12XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_13_XCL_CD", length = 3, nullable = false)
    private String app13XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_14_XCL_CD", length = 3, nullable = false)
    private String app14XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_15_XCL_CD", length = 3, nullable = false)
    private String app15XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_16_XCL_CD", length = 3, nullable = false)
    private String app16XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_17_XCL_CD", length = 3, nullable = false)
    private String app17XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_18_XCL_CD", length = 3, nullable = false)
    private String app18XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_19_XCL_CD", length = 3, nullable = false)
    private String app19XclCd = SEPARATOR_BLANK;

    @Column(name = "APP_20_XCL_CD", length = 3, nullable = false)
    private String app20XclCd = SEPARATOR_BLANK;

    @Column(name = "HOLIDAY_DES", length = 40, nullable = false)
    private String holidayDes = SEPARATOR_BLANK;

    public HolidayPrcId getHolidayPrcId() {
        return holidayPrcId;
    }

    public void setHolidayPrcId(HolidayPrcId holidayPrcId) {
        this.holidayPrcId = holidayPrcId;
    }

    public String getApp01XclCd() {
        return app01XclCd;
    }

    public void setApp01XclCd(String app01XclCd) {
        this.app01XclCd = app01XclCd;
    }

    public String getApp02XclCd() {
        return app02XclCd;
    }

    public void setApp02XclCd(String app02XclCd) {
        this.app02XclCd = app02XclCd;
    }

    public String getApp03XclCd() {
        return app03XclCd;
    }

    public void setApp03XclCd(String app03XclCd) {
        this.app03XclCd = app03XclCd;
    }

    public String getApp04XclCd() {
        return app04XclCd;
    }

    public void setApp04XclCd(String app04XclCd) {
        this.app04XclCd = app04XclCd;
    }

    public String getApp05XclCd() {
        return app05XclCd;
    }

    public void setApp05XclCd(String app05XclCd) {
        this.app05XclCd = app05XclCd;
    }

    public String getApp06XclCd() {
        return app06XclCd;
    }

    public void setApp06XclCd(String app06XclCd) {
        this.app06XclCd = app06XclCd;
    }

    public String getApp07XclCd() {
        return app07XclCd;
    }

    public void setApp07XclCd(String app07XclCd) {
        this.app07XclCd = app07XclCd;
    }

    public String getApp08XclCd() {
        return app08XclCd;
    }

    public void setApp08XclCd(String app08XclCd) {
        this.app08XclCd = app08XclCd;
    }

    public String getApp09XclCd() {
        return app09XclCd;
    }

    public void setApp09XclCd(String app09XclCd) {
        this.app09XclCd = app09XclCd;
    }

    public String getApp10XclCd() {
        return app10XclCd;
    }

    public void setApp10XclCd(String app10XclCd) {
        this.app10XclCd = app10XclCd;
    }

    public String getApp11XclCd() {
        return app11XclCd;
    }

    public void setApp11XclCd(String app11XclCd) {
        this.app11XclCd = app11XclCd;
    }

    public String getApp12XclCd() {
        return app12XclCd;
    }

    public void setApp12XclCd(String app12XclCd) {
        this.app12XclCd = app12XclCd;
    }

    public String getApp13XclCd() {
        return app13XclCd;
    }

    public void setApp13XclCd(String app13XclCd) {
        this.app13XclCd = app13XclCd;
    }

    public String getApp14XclCd() {
        return app14XclCd;
    }

    public void setApp14XclCd(String app14XclCd) {
        this.app14XclCd = app14XclCd;
    }

    public String getApp15XclCd() {
        return app15XclCd;
    }

    public void setApp15XclCd(String app15XclCd) {
        this.app15XclCd = app15XclCd;
    }

    public String getApp16XclCd() {
        return app16XclCd;
    }

    public void setApp16XclCd(String app16XclCd) {
        this.app16XclCd = app16XclCd;
    }

    public String getApp17XclCd() {
        return app17XclCd;
    }

    public void setApp17XclCd(String app17XclCd) {
        this.app17XclCd = app17XclCd;
    }

    public String getApp18XclCd() {
        return app18XclCd;
    }

    public void setApp18XclCd(String app18XclCd) {
        this.app18XclCd = app18XclCd;
    }

    public String getApp19XclCd() {
        return app19XclCd;
    }

    public void setApp19XclCd(String app19XclCd) {
        this.app19XclCd = app19XclCd;
    }

    public String getApp20XclCd() {
        return app20XclCd;
    }

    public void setApp20XclCd(String app20XclCd) {
        this.app20XclCd = app20XclCd;
    }

    public String getHolidayDes() {
        return holidayDes;
    }

    public void setHolidayDes(String holidayDes) {
        this.holidayDes = holidayDes;
    }

}
