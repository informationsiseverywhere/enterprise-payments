package billing.data.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilAmtTrmRltId;

@Entity
@Table(name = "BIL_AMT_TRM_RLT")
public class BilAmtTrmRlt implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilAmtTrmRltId bilAmtTrmRltId;

    @Column(name = "POL_EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime polEffectiveDt;

    @Column(name = "BAM_AMT_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bamAmtTs;

    @Column(name = "RLT_POL_SYM_CD", length = 3, nullable = true)
    private String rltPolSymCd;

    @Column(name = "RLT_POL_NBR", length = 25, nullable = true)
    private String rltPolNbr;

    @Column(name = "RLT_POL_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime rltPolEffDt;

    @Column(name = "BIL_RLT_TYPE_CD", length = 3, nullable = true)
    private String bilRltTypeCd;

    @Column(name = "RLT_GNRC01_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime rltGnrc01Dt;

    @Column(name = "RLT_GNRC02_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime rltGnrc02Dt;

    @Column(name = "RLT_GNRC03_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime rltGnrc03Dt;

    @Column(name = "RLT_GNRC04_NBR", nullable = false)
    private Short rltGnrc04Nbr;

    @Column(name = "RLT_GNRC05_NBR", nullable = false)
    private Short rltGnrc05Nbr;

    @Column(name = "RLT_GNRC06_TXT", length = 25, nullable = true)
    private String rltGnrc06Txt;

    @Column(name = "RLT_GNRC07_TXT", length = 25, nullable = true)
    private String rltGnrc07Txt;

    @Column(name = "RLT_GNRC08_TXT", length = 50, nullable = true)
    private String rltGnrc08Txt;

    @Column(name = "RLT_GNRC09_TXT", length = 50, nullable = true)
    private String rltGnrc09Txt;

    @Column(name = "RLT_GNRC10_AMT", precision = 14, scale = 2, nullable = false)
    private Double rltGnrc10Amt;

    @Column(name = "RLT_GNRC11_AMT", precision = 14, scale = 2, nullable = false)
    private Double rltGnrc11Amt;

    public BilAmtTrmRltId getBilAmtTrmRltId() {
        return bilAmtTrmRltId;
    }

    public ZonedDateTime getPolEffectiveDt() {
        return polEffectiveDt;
    }

    public ZonedDateTime getBamAmtTs() {
        return bamAmtTs;
    }

    public String getRltPolSymCd() {
        return rltPolSymCd;
    }

    public String getRltPolNbr() {
        return rltPolNbr;
    }

    public ZonedDateTime getRltPolEffDt() {
        return rltPolEffDt;
    }

    public String getBilRltTypeCd() {
        return bilRltTypeCd;
    }

    public ZonedDateTime getRltGnrc01Dt() {
        return rltGnrc01Dt;
    }

    public ZonedDateTime getRltGnrc02Dt() {
        return rltGnrc02Dt;
    }

    public ZonedDateTime getRltGnrc03Dt() {
        return rltGnrc03Dt;
    }

    public Short getRltGnrc04Nbr() {
        return rltGnrc04Nbr;
    }

    public Short getRltGnrc05Nbr() {
        return rltGnrc05Nbr;
    }

    public String getRltGnrc06Txt() {
        return rltGnrc06Txt;
    }

    public String getRltGnrc07Txt() {
        return rltGnrc07Txt;
    }

    public String getRltGnrc08Txt() {
        return rltGnrc08Txt;
    }

    public String getRltGnrc09Txt() {
        return rltGnrc09Txt;
    }

    public Double getRltGnrc10Amt() {
        return rltGnrc10Amt;
    }

    public Double getRltGnrc11Amt() {
        return rltGnrc11Amt;
    }

    public void setBilAmtTrmRltId(BilAmtTrmRltId bilAmtTrmRltId) {
        this.bilAmtTrmRltId = bilAmtTrmRltId;
    }

    public void setPolEffectiveDt(ZonedDateTime polEffectiveDt) {
        this.polEffectiveDt = polEffectiveDt;
    }

    public void setBamAmtTs(ZonedDateTime bamAmtTs) {
        this.bamAmtTs = bamAmtTs;
    }

    public void setRltPolSymCd(String rltPolSymCd) {
        this.rltPolSymCd = rltPolSymCd;
    }

    public void setRltPolNbr(String rltPolNbr) {
        this.rltPolNbr = rltPolNbr;
    }

    public void setRltPolEffDt(ZonedDateTime rltPolEffDt) {
        this.rltPolEffDt = rltPolEffDt;
    }

    public void setBilRltTypeCd(String bilRltTypeCd) {
        this.bilRltTypeCd = bilRltTypeCd;
    }

    public void setRltGnrc01Dt(ZonedDateTime rltGnrc01Dt) {
        this.rltGnrc01Dt = rltGnrc01Dt;
    }

    public void setRltGnrc02Dt(ZonedDateTime rltGnrc02Dt) {
        this.rltGnrc02Dt = rltGnrc02Dt;
    }

    public void setRltGnrc03Dt(ZonedDateTime rltGnrc03Dt) {
        this.rltGnrc03Dt = rltGnrc03Dt;
    }

    public void setRltGnrc04Nbr(Short rltGnrc04Nbr) {
        this.rltGnrc04Nbr = rltGnrc04Nbr;
    }

    public void setRltGnrc05Nbr(Short rltGnrc05Nbr) {
        this.rltGnrc05Nbr = rltGnrc05Nbr;
    }

    public void setRltGnrc06Txt(String rltGnrc06Txt) {
        this.rltGnrc06Txt = rltGnrc06Txt;
    }

    public void setRltGnrc07Txt(String rltGnrc07Txt) {
        this.rltGnrc07Txt = rltGnrc07Txt;
    }

    public void setRltGnrc08Txt(String rltGnrc08Txt) {
        this.rltGnrc08Txt = rltGnrc08Txt;
    }

    public void setRltGnrc09Txt(String rltGnrc09Txt) {
        this.rltGnrc09Txt = rltGnrc09Txt;
    }

    public void setRltGnrc10Amt(Double rltGnrc10Amt) {
        this.rltGnrc10Amt = rltGnrc10Amt;
    }

    public void setRltGnrc11Amt(Double rltGnrc11Amt) {
        this.rltGnrc11Amt = rltGnrc11Amt;
    }

}
