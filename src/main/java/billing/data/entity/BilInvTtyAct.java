package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilInvTtyActId;

@Entity
@Table(name = "BIL_INV_TTY_ACT")
public class BilInvTtyAct implements Serializable{
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilInvTtyActId bilInvTtyActId;

    @Column(name = "BIL_TTY_NBR", length = 20, nullable = false)
    private String bilTtyNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_AGT_ACT_NBR", length = 20, nullable = false)
    private String bilAgtActNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_ACCOUNT_NBR", length = 30, nullable = false)
    private String bilAccountNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_ADDITIONAL_ID", length = 20, nullable = false)
    private String bilAdditionalId = SEPARATOR_BLANK;

    @Column(name = "BIL_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilDueDt;

    @Column(name = "BIL_REFERENCE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilReferenceDt;

    @Column(name = "BIA_LAST_ACY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime biaLastAcyDt;

    @Column(name = "BIL_TYPE_CD", length = 2, nullable = false)
    private String bilTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_CLASS_CD", length = 3, nullable = false)
    private String bilClassCd = SEPARATOR_BLANK;

    @Column(name = "BIL_RVW_FQY_CD", length = 2, nullable = false)
    private String bilRvwFqyCd = SEPARATOR_BLANK;

    @Column(name = "BIL_PREV_DUE_AMT", precision = 14, scale = 2, nullable = false)
    private double bilPrevDueAmt;

    @Column(name = "BIL_MIN_DUE_AMT", precision = 14, scale = 2, nullable = false)
    private double bilMinDueAmt;

    @Column(name = "BIL_NEW_BAL_AMT", precision = 14, scale = 2, nullable = false)
    private double bilNewBalAmt;

    @Column(name = "BIA_PRE_SCG_AMT", precision = 14, scale = 2, nullable = false)
    private double biaPreScgAmt;

    @Column(name = "BIA_PRE_PNY_AMT", precision = 14, scale = 2, nullable = false)
    private double biaPrePnyAmt;

    @Column(name = "BIA_PRE_LTE_AMT", precision = 14, scale = 2, nullable = false)
    private double biaPreLteAmt;

    @Column(name = "BIA_BILL_SCG_AMT", precision = 14, scale = 2, nullable = false)
    private double biaBillScgAmt;

    @Column(name = "BIL_STT_SVG_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttSvgAmt;

    @Column(name = "BIL_STT_RECON_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttReconAmt;

    @Column(name = "BIA_SUSPENSE_AMT", precision = 14, scale = 2, nullable = false)
    private double biaSuspenseAmt;

    @Column(name = "BIL_STT_RECON_IND", nullable = false)
    private char bilSttReconInd;

    @Column(name = "BIL_SUS_REASON_CD", length = 3, nullable = false)
    private String bilSusReasonCd = SEPARATOR_BLANK;

    @Column(name = "BIL_EXC_REASON_CD", length = 3, nullable = false)
    private String bilExcReasonCd = SEPARATOR_BLANK;

    @Column(name = "BIL_INV_REPRINT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilInvReprintDt;

    @Column(name = "BIL_STT_SR_SEQ_CD", length = 2, nullable = false)
    private String bilSttSrSeqCd = SEPARATOR_BLANK;

    @Column(name = "BIL_PRT_FORMAT_CD", length = 4, nullable = false)
    private String bilPrtFormatCd = SEPARATOR_BLANK;

    @Column(name = "BIL_COLLECTION_MTH", length = 3, nullable = false)
    private String bilCollectionMth = SEPARATOR_BLANK;

    @Column(name = "BIA_NO_PMT_IND", nullable = false)
    private char biaNoPmtInd;

    @Column(name = "BIL_PAY_LNG_NM", length = 512, nullable = false)
    private String bilPayLngNm;

    @Column(name = "BIA_MSG_AREA_TXT", length = 140, nullable = false)
    private String biaMsgAreaTxt;

    @Column(name = "BIA_CCR_REASON_IND", nullable = false)
    private char biaCcrReasonInd;

    @Column(name = "BIL_MIN_NET_AMT", precision = 14, scale = 2, nullable = false)
    private double bilMinNetAmt;

    @Column(name = "BIL_SUB_GROUP_CD", length = 20, nullable = false)
    private String bilSubGroupCd = SEPARATOR_BLANK;

    @Column(name = "BIL_AMT_CHG_CD", nullable = false)
    private char bilAmtChgCd;

    @Column(name = "BIL_DIR_SUS_AMT", precision = 14, scale = 2, nullable = false)
    private double bilDirSusAmt;

    @Column(name = "BIL_UN_DIR_SUS_AMT", precision = 14, scale = 2, nullable = false)
    private double bilUnDirSusAmt;

    @Column(name = "BIL_TAKE_SUS_AMT", precision = 14, scale = 2, nullable = false)
    private double bilTakeSusAmt;

    public BilInvTtyActId getBilInvTtyActId() {
        return bilInvTtyActId;
    }

    public void setBilInvTtyActId(BilInvTtyActId bilInvTtyActId) {
        this.bilInvTtyActId = bilInvTtyActId;
    }

    public String getBilTtyNbr() {
        return bilTtyNbr;
    }

    public void setBilTtyNbr(String bilTtyNbr) {
        this.bilTtyNbr = bilTtyNbr;
    }

    public String getBilAgtActNbr() {
        return bilAgtActNbr;
    }

    public void setBilAgtActNbr(String bilAgtActNbr) {
        this.bilAgtActNbr = bilAgtActNbr;
    }

    public String getBilAccountNbr() {
        return bilAccountNbr;
    }

    public void setBilAccountNbr(String bilAccountNbr) {
        this.bilAccountNbr = bilAccountNbr;
    }

    public String getBilAdditionalId() {
        return bilAdditionalId;
    }

    public void setBilAdditionalId(String bilAdditionalId) {
        this.bilAdditionalId = bilAdditionalId;
    }

    public ZonedDateTime getBilDueDt() {
        return bilDueDt;
    }

    public void setBilDueDt(ZonedDateTime bilDueDt) {
        this.bilDueDt = bilDueDt;
    }

    public ZonedDateTime getBilReferenceDt() {
        return bilReferenceDt;
    }

    public void setBilReferenceDt(ZonedDateTime bilReferenceDt) {
        this.bilReferenceDt = bilReferenceDt;
    }

    public ZonedDateTime getBiaLastAcyDt() {
        return biaLastAcyDt;
    }

    public void setBiaLastAcyDt(ZonedDateTime biaLastAcyDt) {
        this.biaLastAcyDt = biaLastAcyDt;
    }

    public String getBilTypeCd() {
        return bilTypeCd;
    }

    public void setBilTypeCd(String bilTypeCd) {
        this.bilTypeCd = bilTypeCd;
    }

    public String getBilClassCd() {
        return bilClassCd;
    }

    public void setBilClassCd(String bilClassCd) {
        this.bilClassCd = bilClassCd;
    }

    public String getBilRvwFqyCd() {
        return bilRvwFqyCd;
    }

    public void setBilRvwFqyCd(String bilRvwFqyCd) {
        this.bilRvwFqyCd = bilRvwFqyCd;
    }

    public double getBilPrevDueAmt() {
        return bilPrevDueAmt;
    }

    public void setBilPrevDueAmt(double bilPrevDueAmt) {
        this.bilPrevDueAmt = bilPrevDueAmt;
    }

    public double getBilMinDueAmt() {
        return bilMinDueAmt;
    }

    public void setBilMinDueAmt(double bilMinDueAmt) {
        this.bilMinDueAmt = bilMinDueAmt;
    }

    public double getBilNewBalAmt() {
        return bilNewBalAmt;
    }

    public void setBilNewBalAmt(double bilNewBalAmt) {
        this.bilNewBalAmt = bilNewBalAmt;
    }

    public double getBiaPreScgAmt() {
        return biaPreScgAmt;
    }

    public void setBiaPreScgAmt(double biaPreScgAmt) {
        this.biaPreScgAmt = biaPreScgAmt;
    }

    public double getBiaPrePnyAmt() {
        return biaPrePnyAmt;
    }

    public void setBiaPrePnyAmt(double biaPrePnyAmt) {
        this.biaPrePnyAmt = biaPrePnyAmt;
    }

    public double getBiaPreLteAmt() {
        return biaPreLteAmt;
    }

    public void setBiaPreLteAmt(double biaPreLteAmt) {
        this.biaPreLteAmt = biaPreLteAmt;
    }

    public double getBiaBillScgAmt() {
        return biaBillScgAmt;
    }

    public void setBiaBillScgAmt(double biaBillScgAmt) {
        this.biaBillScgAmt = biaBillScgAmt;
    }

    public double getBilSttSvgAmt() {
        return bilSttSvgAmt;
    }

    public void setBilSttSvgAmt(double bilSttSvgAmt) {
        this.bilSttSvgAmt = bilSttSvgAmt;
    }

    public double getBilSttReconAmt() {
        return bilSttReconAmt;
    }

    public void setBilSttReconAmt(double bilSttReconAmt) {
        this.bilSttReconAmt = bilSttReconAmt;
    }

    public double getBiaSuspenseAmt() {
        return biaSuspenseAmt;
    }

    public void setBiaSuspenseAmt(double biaSuspenseAmt) {
        this.biaSuspenseAmt = biaSuspenseAmt;
    }

    public char getBilSttReconInd() {
        return bilSttReconInd;
    }

    public void setBilSttReconInd(char bilSttReconInd) {
        this.bilSttReconInd = bilSttReconInd;
    }

    public String getBilSusReasonCd() {
        return bilSusReasonCd;
    }

    public void setBilSusReasonCd(String bilSusReasonCd) {
        this.bilSusReasonCd = bilSusReasonCd;
    }

    public String getBilExcReasonCd() {
        return bilExcReasonCd;
    }

    public void setBilExcReasonCd(String bilExcReasonCd) {
        this.bilExcReasonCd = bilExcReasonCd;
    }

    public ZonedDateTime getBilInvReprintDt() {
        return bilInvReprintDt;
    }

    public void setBilInvReprintDt(ZonedDateTime bilInvReprintDt) {
        this.bilInvReprintDt = bilInvReprintDt;
    }

    public String getBilSttSrSeqCd() {
        return bilSttSrSeqCd;
    }

    public void setBilSttSrSeqCd(String bilSttSrSeqCd) {
        this.bilSttSrSeqCd = bilSttSrSeqCd;
    }

    public String getBilPrtFormatCd() {
        return bilPrtFormatCd;
    }

    public void setBilPrtFormatCd(String bilPrtFormatCd) {
        this.bilPrtFormatCd = bilPrtFormatCd;
    }

    public String getBilCollectionMth() {
        return bilCollectionMth;
    }

    public void setBilCollectionMth(String bilCollectionMth) {
        this.bilCollectionMth = bilCollectionMth;
    }

    public char getBiaNoPmtInd() {
        return biaNoPmtInd;
    }

    public void setBiaNoPmtInd(char biaNoPmtInd) {
        this.biaNoPmtInd = biaNoPmtInd;
    }

    public String getBilPayLngNm() {
        return bilPayLngNm;
    }

    public void setBilPayLngNm(String bilPayLngNm) {
        this.bilPayLngNm = bilPayLngNm;
    }

    public String getBiaMsgAreaTxt() {
        return biaMsgAreaTxt;
    }

    public void setBiaMsgAreaTxt(String biaMsgAreaTxt) {
        this.biaMsgAreaTxt = biaMsgAreaTxt;
    }

    public char getBiaCcrReasonInd() {
        return biaCcrReasonInd;
    }

    public void setBiaCcrReasonInd(char biaCcrReasonInd) {
        this.biaCcrReasonInd = biaCcrReasonInd;
    }

    public double getBilMinNetAmt() {
        return bilMinNetAmt;
    }

    public void setBilMinNetAmt(double bilMinNetAmt) {
        this.bilMinNetAmt = bilMinNetAmt;
    }

    public String getBilSubGroupCd() {
        return bilSubGroupCd;
    }

    public void setBilSubGroupCd(String bilSubGroupCd) {
        this.bilSubGroupCd = bilSubGroupCd;
    }

    public char getBilAmtChgCd() {
        return bilAmtChgCd;
    }

    public void setBilAmtChgCd(char bilAmtChgCd) {
        this.bilAmtChgCd = bilAmtChgCd;
    }

    public double getBilDirSusAmt() {
        return bilDirSusAmt;
    }

    public void setBilDirSusAmt(double bilDirSusAmt) {
        this.bilDirSusAmt = bilDirSusAmt;
    }

    public double getBilUnDirSusAmt() {
        return bilUnDirSusAmt;
    }

    public void setBilUnDirSusAmt(double bilUnDirSusAmt) {
        this.bilUnDirSusAmt = bilUnDirSusAmt;
    }

    public double getBilTakeSusAmt() {
        return bilTakeSusAmt;
    }

    public void setBilTakeSusAmt(double bilTakeSusAmt) {
        this.bilTakeSusAmt = bilTakeSusAmt;
    }

    

}
