package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilPolicyId;

@Entity
@Table(name = "BIL_POLICY")
public class BilPolicy implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilPolicyId bilPolicyId;

    @Column(name = "POL_NBR", length = 25, nullable = false)
    private String polNbr = SEPARATOR_BLANK;

    @Column(name = "POL_SYMBOL_CD", length = 3, nullable = false)
    private String polSymbolCd = SEPARATOR_BLANK;

    @Column(name = "BIL_ACCOUNT_NBR", length = 30, nullable = false)
    private String bilAccountNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_TYPE_CD", length = 2, nullable = false)
    private String bilTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_CLASS_CD", length = 3, nullable = false)
    private String bilClassCd = SEPARATOR_BLANK;

    @Column(name = "BIL_PLAN_CD", length = 4, nullable = false)
    private String bilPlanCd = SEPARATOR_BLANK;

    @Column(name = "BIL_PAY_CD", nullable = false)
    private char bilPayCd;

    @Column(name = "BIL_OTH_CLT_ID", length = 20, nullable = false)
    private String bilOthCltId = SEPARATOR_BLANK;

    @Column(name = "BIL_OTH_ADR_SEQ", length = 5, nullable = false)
    private short bilOthAdrSeq;

    @Column(name = "BIL_RNL_BIL_PLAN", length = 4, nullable = false)
    private String bilRnlBilPlan = SEPARATOR_BLANK;

    @Column(name = "BIL_RNL_PAY_CD", nullable = false)
    private char bilRnlPayCd;

    @Column(name = "BIL_RNL_OTH_CLT_ID", length = 20, nullable = false)
    private String bilRnlOthCltId = SEPARATOR_BLANK;

    @Column(name = "BIL_RNL_OTHADR_SEQ", length = 5, nullable = false)
    private short bilRnlOthadrSeq;

    @Column(name = "BIL_RNL_CLASS_CD", length = 3, nullable = false)
    private String bilRnlClassCd = SEPARATOR_BLANK;

    @Column(name = "BIL_ADDITIONAL_ID", length = 20, nullable = false)
    private String bilAdditionalId = SEPARATOR_BLANK;

    @Column(name = "BIL_REPORT_LVL_CD", length = 8, nullable = false)
    private String bilReportLvlCd = SEPARATOR_BLANK;

    @Column(name = "BIL_SUB_GROUP_CD", length = 20, nullable = false)
    private String bilSubGroupCd = SEPARATOR_BLANK;

    @Column(name = "BIL_ISSUE_IND", nullable = false)
    private char bilIssueInd;

    public BilPolicyId getBillPolicyId() {
        return bilPolicyId;
    }

    public void setBillPolicyId(BilPolicyId bilPolicyId) {
        this.bilPolicyId = bilPolicyId;
    }

    public String getPolNbr() {
        return polNbr;
    }

    public void setPolNbr(String polNbr) {
        this.polNbr = polNbr;
    }

    public String getPolSymbolCd() {
        return polSymbolCd;
    }

    public void setPolSymbolCd(String polSymbolCd) {
        this.polSymbolCd = polSymbolCd;
    }

    public String getBilAccountNbr() {
        return bilAccountNbr;
    }

    public void setBilAccountNbr(String bilAccountNbr) {
        this.bilAccountNbr = bilAccountNbr;
    }

    public String getBilTypeCd() {
        return bilTypeCd;
    }

    public void setBilTypeCd(String bilTypeCd) {
        this.bilTypeCd = bilTypeCd;
    }

    public String getBilClassCd() {
        return bilClassCd;
    }

    public void setBilClassCd(String bilClassCd) {
        this.bilClassCd = bilClassCd;
    }

    public String getBilPlanCd() {
        return bilPlanCd;
    }

    public void setBilPlanCd(String bilPlanCd) {
        this.bilPlanCd = bilPlanCd;
    }

    public char getBilPayCd() {
        return bilPayCd;
    }

    public void setBilPayCd(char bilPayCd) {
        this.bilPayCd = bilPayCd;
    }

    public String getBilOthCltId() {
        return bilOthCltId;
    }

    public void setBilOthCltId(String bilOthCltId) {
        this.bilOthCltId = bilOthCltId;
    }

    public short getBilOthAdrSeq() {
        return bilOthAdrSeq;
    }

    public void setBilOthAdrSeq(short bilOthAdrSeq) {
        this.bilOthAdrSeq = bilOthAdrSeq;
    }

    public String getBilRnlBilPlan() {
        return bilRnlBilPlan;
    }

    public void setBilRnlBilPlan(String bilRnlBilPlan) {
        this.bilRnlBilPlan = bilRnlBilPlan;
    }

    public char getBilRnlPayCd() {
        return bilRnlPayCd;
    }

    public void setBilRnlPayCd(char bilRnlPayCd) {
        this.bilRnlPayCd = bilRnlPayCd;
    }

    public String getBilRnlOthCltId() {
        return bilRnlOthCltId;
    }

    public void setBilRnlOthCltId(String bilRnlOthCltId) {
        this.bilRnlOthCltId = bilRnlOthCltId;
    }

    public short getBilRnlOthadrSeq() {
        return bilRnlOthadrSeq;
    }

    public void setBilRnlOthadrSeq(short bilRnlOthadrSeq) {
        this.bilRnlOthadrSeq = bilRnlOthadrSeq;
    }

    public String getBilRnlClassCd() {
        return bilRnlClassCd;
    }

    public void setBilRnlClassCd(String bilRnlClassCd) {
        this.bilRnlClassCd = bilRnlClassCd;
    }

    public String getBilAdditionalId() {
        return bilAdditionalId;
    }

    public void setBilAdditionalId(String bilAdditionalId) {
        this.bilAdditionalId = bilAdditionalId;
    }

    public String getBilReportLvlCd() {
        return bilReportLvlCd;
    }

    public void setBilReportLvlCd(String bilReportLvlCd) {
        this.bilReportLvlCd = bilReportLvlCd;
    }

    public String getBilSubGroupCd() {
        return bilSubGroupCd;
    }

    public void setBilSubGroupCd(String bilSubGroupCd) {
        this.bilSubGroupCd = bilSubGroupCd;
    }

    public char getBilIssueInd() {
        return bilIssueInd;
    }

    public void setBilIssueInd(char bilIssueInd) {
        this.bilIssueInd = bilIssueInd;
    }

}
