package billing.data.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilFunctionCtlId;

@Entity
@Table(name = "BIL_FUNCTION_CTL")
public class BilFunctionCtl implements Serializable {

    private static final long serialVersionUID = 1;

    @EmbeddedId
    private BilFunctionCtlId bilFunctionCtlId;

    @Column(name = "EXP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime expDt;

    public BilFunctionCtlId getBilFunctionCtlId() {
        return bilFunctionCtlId;
    }

    public void setBilFunctionCtlId(BilFunctionCtlId bilFunctionCtlId) {
        this.bilFunctionCtlId = bilFunctionCtlId;
    }

    public ZonedDateTime getExpDt() {
        return expDt;
    }

    public void setExpDt(ZonedDateTime expDt) {
        this.expDt = expDt;
    }

}
