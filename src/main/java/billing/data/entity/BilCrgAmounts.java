package billing.data.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import billing.data.entity.id.BilCrgAmountsId;

@Entity
@Table(name = "BIL_CRG_AMOUNTS")
public class BilCrgAmounts implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilCrgAmountsId bilCrgAmountsId;

    @Column(name = "BIL_ADJ_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bilAdjDueDt;

    @Column(name = "BIL_INV_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bilInvDt;

    @Column(name = "BIL_CRG_TYPE_CD", nullable = false)
    private char bilCrgTypeCd;

    @Column(name = "BCA_CRG_AMT", precision = 14, scale = 2, nullable = false)
    private double bcaCrgAmt;

    @Column(name = "BCA_CRG_PAID_AMT", precision = 14, scale = 2, nullable = false)
    private double bcaCrgPaidAmt;

    @Column(name = "BCA_WRO_CRG_AMT", precision = 14, scale = 2, nullable = false)
    private double bcaWroCrgAmt;

    @Column(name = "BIL_INVOICE_CD", nullable = false)
    private char bilInvoiceCd;

    public BilCrgAmountsId getBilCrgAmountsId() {
        return bilCrgAmountsId;
    }

    public void setBilCrgAmountsId(BilCrgAmountsId bilCrgAmountsId) {
        this.bilCrgAmountsId = bilCrgAmountsId;
    }

    public ZonedDateTime getBilAdjDueDt() {
        return bilAdjDueDt;
    }

    public void setBilAdjDueDt(ZonedDateTime bilAdjDueDt) {
        this.bilAdjDueDt = bilAdjDueDt;
    }

    public ZonedDateTime getBilInvDt() {
        return bilInvDt;
    }

    public void setBilInvDt(ZonedDateTime bilInvDt) {
        this.bilInvDt = bilInvDt;
    }

    public char getBilCrgTypeCd() {
        return bilCrgTypeCd;
    }

    public void setBilCrgTypeCd(char bilCrgTypeCd) {
        this.bilCrgTypeCd = bilCrgTypeCd;
    }

    public double getBcaCrgAmt() {
        return bcaCrgAmt;
    }

    public void setBcaCrgAmt(double bcaCrgAmt) {
        this.bcaCrgAmt = bcaCrgAmt;
    }

    public double getBcaCrgPaidAmt() {
        return bcaCrgPaidAmt;
    }

    public void setBcaCrgPaidAmt(double bcaCrgPaidAmt) {
        this.bcaCrgPaidAmt = bcaCrgPaidAmt;
    }

    public double getBcaWroCrgAmt() {
        return bcaWroCrgAmt;
    }

    public void setBcaWroCrgAmt(double bcaWroCrgAmt) {
        this.bcaWroCrgAmt = bcaWroCrgAmt;
    }

    public char getBilInvoiceCd() {
        return bilInvoiceCd;
    }

    public void setBilInvoiceCd(char bilInvoiceCd) {
        this.bilInvoiceCd = bilInvoiceCd;
    }

}
