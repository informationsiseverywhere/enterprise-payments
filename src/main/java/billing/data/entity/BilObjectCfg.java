package billing.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilObjectCfgId;

@Entity
@Table(name = "BIL_OBJECT_CFG")
public class BilObjectCfg implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilObjectCfgId bilObjectCfgId;

    @Column(name = "BOC_OBJ_REKICK_CNT", length = 5, nullable = false)
    private short bocObjRekickCnt;

    @Column(name = "BOC_COMMIT_CNT", length = 5, nullable = false)
    private short bocCommitCnt;

    @Column(name = "BOC_NBR_OBJ_TRNS", length = 5, nullable = false)
    private short bocNbrObjTrns;

    @Column(name = "BOC_NBR_REGIONS", length = 5, nullable = false)
    private short bocNbrRegions;

    @Column(name = "BOC_NBR_ACY_MIN", length = 10, nullable = false)
    private int bocNbrAcyMin;

    @Column(name = "BOC_PRC_DURATION", length = 10, nullable = false)
    private int bocPrcDuration;

    @Column(name = "BOC_OBJ_PRC_CD", nullable = false)
    private char bocObjPrcCd;

    @Column(name = "BOC_OBJ_DFR_TM", length = 10, nullable = false)
    private int bocObjDfrTm;

    public BilObjectCfgId getBilObjectCfgId() {
        return bilObjectCfgId;
    }

    public void setBilObjectCfgId(BilObjectCfgId bilObjectCfgId) {
        this.bilObjectCfgId = bilObjectCfgId;
    }

    public short getBocObjRekickCnt() {
        return bocObjRekickCnt;
    }

    public void setBocObjRekickCnt(short bocObjRekickCnt) {
        this.bocObjRekickCnt = bocObjRekickCnt;
    }

    public short getBocCommitCnt() {
        return bocCommitCnt;
    }

    public void setBocCommitCnt(short bocCommitCnt) {
        this.bocCommitCnt = bocCommitCnt;
    }

    public short getBocNbrObjTrns() {
        return bocNbrObjTrns;
    }

    public void setBocNbrObjTrns(short bocNbrObjTrns) {
        this.bocNbrObjTrns = bocNbrObjTrns;
    }

    public short getBocNbrRegions() {
        return bocNbrRegions;
    }

    public void setBocNbrRegions(short bocNbrRegions) {
        this.bocNbrRegions = bocNbrRegions;
    }

    public int getBocNbrAcyMin() {
        return bocNbrAcyMin;
    }

    public void setBocNbrAcyMin(int bocNbrAcyMin) {
        this.bocNbrAcyMin = bocNbrAcyMin;
    }

    public int getBocPrcDuration() {
        return bocPrcDuration;
    }

    public void setBocPrcDuration(int bocPrcDuration) {
        this.bocPrcDuration = bocPrcDuration;
    }

    public char getBocObjPrcCd() {
        return bocObjPrcCd;
    }

    public void setBocObjPrcCd(char bocObjPrcCd) {
        this.bocObjPrcCd = bocObjPrcCd;
    }

    public int getBocObjDfrTm() {
        return bocObjDfrTm;
    }

    public void setBocObjDfrTm(int bocObjDfrTm) {
        this.bocObjDfrTm = bocObjDfrTm;
    }
}
