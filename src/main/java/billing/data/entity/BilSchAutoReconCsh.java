package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilSchAutoReconCshId;

@Entity
@Table(name = "BIL_SCH_AUTO_RECON_CSH")
public class BilSchAutoReconCsh {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilSchAutoReconCshId bilSchAutoReconCshId;

    @Column(name = "BIL_ENTRY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEntryDt;

    @Column(name = "BIL_ENTRY_NBR", length = 4, nullable = false)
    private String bilEntryNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_ENTRY_SEQ_NBR", length = 5, nullable = false)
    private short bilEntrySeqNbr = SHORT_ZERO;

    @Column(name = "BIL_DTB_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilDtbDt;

    @Column(name = "BIL_DTB_SEQ_NBR", length = 5, nullable = false)
    private short bilDtbSeqNbr = SHORT_ZERO;

    @Column(name = "PROCESS_IND", nullable = false)
    private char processInd;

    public BilSchAutoReconCshId getBilSchAutoReconCshId() {
        return bilSchAutoReconCshId;
    }

    public void setBilSchAutoReconCshId(BilSchAutoReconCshId bilSchAutoReconCshId) {
        this.bilSchAutoReconCshId = bilSchAutoReconCshId;
    }

    public ZonedDateTime getBilEntryDt() {
        return bilEntryDt;
    }

    public void setBilEntryDt(ZonedDateTime bilEntryDt) {
        this.bilEntryDt = bilEntryDt;
    }

    public String getBilEntryNbr() {
        return bilEntryNbr;
    }

    public void setBilEntryNbr(String bilEntryNbr) {
        this.bilEntryNbr = bilEntryNbr;
    }

    public short getBilEntrySeqNbr() {
        return bilEntrySeqNbr;
    }

    public void setBilEntrySeqNbr(short bilEntrySeqNbr) {
        this.bilEntrySeqNbr = bilEntrySeqNbr;
    }

    public ZonedDateTime getBilDtbDt() {
        return bilDtbDt;
    }

    public void setBilDtbDt(ZonedDateTime bilDtbDt) {
        this.bilDtbDt = bilDtbDt;
    }

    public short getBilDtbSeqNbr() {
        return bilDtbSeqNbr;
    }

    public void setBilDtbSeqNbr(short bilDtbSeqNbr) {
        this.bilDtbSeqNbr = bilDtbSeqNbr;
    }

    public char getProcessInd() {
        return processInd;
    }

    public void setProcessInd(char processInd) {
        this.processInd = processInd;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
