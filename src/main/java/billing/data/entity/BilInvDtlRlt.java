package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilInvDtlRltId;

@Entity
@Table(name = "BIL_INV_DTL_RLT")
public class BilInvDtlRlt implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilInvDtlRltId bilInvDtlRltId;

    @Column(name = "POLICY_ID", length = 16, nullable = false)
    private String policyId = SEPARATOR_BLANK;

    @Column(name = "POL_EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime polEffectiveDt;

    @Column(name = "BIL_SEQ_NBR", length = 5, nullable = false)
    private short bilSeqNbr = SHORT_ZERO;

    public BilInvDtlRltId getBilInvDtlRltId() {
        return bilInvDtlRltId;
    }

    public void setBilInvDtlRltId(BilInvDtlRltId bilInvDtlRltId) {
        this.bilInvDtlRltId = bilInvDtlRltId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public ZonedDateTime getPolEffectiveDt() {
        return polEffectiveDt;
    }

    public void setPolEffectiveDt(ZonedDateTime polEffectiveDt) {
        this.polEffectiveDt = polEffectiveDt;
    }

    public short getBilSeqNbr() {
        return bilSeqNbr;
    }

    public void setBilSeqNbr(short bilSeqNbr) {
        this.bilSeqNbr = bilSeqNbr;
    }

}
