package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.TransRateHstId;

@Entity
@Table(name = "TRANS_RATE_HST")
public class TransRateHst implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private TransRateHstId transRateHstId;

    @Column(name = "RECEIVED_CURRENCY", length = 15, nullable = false)
    private String receivedCurrency = SEPARATOR_BLANK;

    @Column(name = "PAYMENT_CURRENCY", length = 15, nullable = false)
    private String paymentCurrency = SEPARATOR_BLANK;

    @Column(name = "RECEIVED_AMOUNT", precision = 14, scale = 2, nullable = false)
    private double receivedAmount;

    @Column(name = "PAYMENT_AMOUNT", precision = 14, scale = 2, nullable = false)
    private double paymentAmount;

    @Column(name = "RATE", precision = 14, scale = 2, nullable = false)
    private double rate;

    public TransRateHst(TransRateHstId transRateHstId, String receivedCurrency, String paymentCurrency,
            double receivedAmount, double paymentAmount, double rate) {
        super();
        this.transRateHstId = transRateHstId;
        this.receivedCurrency = receivedCurrency;
        this.paymentCurrency = paymentCurrency;
        this.receivedAmount = receivedAmount;
        this.paymentAmount = paymentAmount;
        this.rate = rate;
    }

    public TransRateHst() {
    }

    public TransRateHstId getTransRateHstId() {
        return transRateHstId;
    }

    public void setTransRateHstId(TransRateHstId transRateHstId) {
        this.transRateHstId = transRateHstId;
    }

    public String getReceivedCurrency() {
        return receivedCurrency;
    }

    public void setReceivedCurrency(String receivedCurrency) {
        this.receivedCurrency = receivedCurrency;
    }

    public double getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(double receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

}
