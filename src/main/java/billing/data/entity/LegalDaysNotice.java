package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.LegalDaysNoticeId;

@Entity
@Table(name = "LEGAL_DAYS_NOTICE")
public class LegalDaysNotice implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private LegalDaysNoticeId legalDaysNoticeId;

    @Column(name = "LDN_LEG_DAYS_NBR", length = 5, nullable = false)
    private short legalDaysNumber;

    @Column(name = "LDN_MAL_DAYS_NBR", length = 5, nullable = false)
    private short mailLegalDaysNumber;

    @Column(name = "LDN_LEG_NOT_IND", nullable = false)
    private char legalNoticeIndicator = BLANK_CHAR;

    @Column(name = "LDN_MAX_DAYS_NBR", length = 5, nullable = false)
    private short maximumLegalDaysNumber;

    public LegalDaysNoticeId getLegalDaysNoticeId() {
        return legalDaysNoticeId;
    }

    public void setLegalDaysNoticeId(LegalDaysNoticeId legalDaysNoticeId) {
        this.legalDaysNoticeId = legalDaysNoticeId;
    }

    public short getLegalDaysNumber() {
        return legalDaysNumber;
    }

    public void setLegalDaysNumber(short legalDaysNumber) {
        this.legalDaysNumber = legalDaysNumber;
    }

    public short getMailLegalDaysNumber() {
        return mailLegalDaysNumber;
    }

    public void setMailLegalDaysNumber(short mailLegalDaysNumber) {
        this.mailLegalDaysNumber = mailLegalDaysNumber;
    }

    public char getLegalNoticeIndicator() {
        return legalNoticeIndicator;
    }

    public void setLegalNoticeIndicator(char legalNoticeIndicator) {
        this.legalNoticeIndicator = legalNoticeIndicator;
    }

    public short getMaximumLegalDaysNumber() {
        return maximumLegalDaysNumber;
    }

    public void setMaximumLegalDaysNumber(short maximumLegalDaysNumber) {
        this.maximumLegalDaysNumber = maximumLegalDaysNumber;
    }

}
