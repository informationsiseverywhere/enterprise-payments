package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "BIL_SYSTEM_RULES")
public class BilSystemRules implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "BIL_TYPE_CD", length = 2, nullable = false)
    private String bilTypeCd = SEPARATOR_BLANK;

    @Column(name = "BSR_PAY_TO_LNG_NM", length = 512, nullable = false)
    private String bsrPayToLngNm = SEPARATOR_BLANK;

    @Column(name = "BSR_PAY_TO_ADR_1", length = 64, nullable = false)
    private String bsrPayToAdr1 = SEPARATOR_BLANK;

    @Column(name = "BSR_PAY_TO_ADR_2", length = 64, nullable = false)
    private String bsrPayToAdr2 = SEPARATOR_BLANK;

    @Column(name = "BSR_PAY_TO_CITY_NM", length = 64, nullable = false)
    private String bsrPayToCityNm = SEPARATOR_BLANK;

    @Column(name = "BSR_PAY_TO_ST_CD", length = 3, nullable = false)
    private String bsrPayToStCd = SEPARATOR_BLANK;

    @Column(name = "BSR_PAY_TO_PST_CD", length = 20, nullable = false)
    private String bsrPayToPstCd = SEPARATOR_BLANK;

    @Column(name = "BSR_RET_TO_LNG_NM", length = 512, nullable = false)
    private String bsrRetToLngNm = SEPARATOR_BLANK;

    @Column(name = "BSR_RET_TO_ADR_1", length = 64, nullable = false)
    private String bsrRetToAdr1 = SEPARATOR_BLANK;

    @Column(name = "BSR_RET_TO_ADR_2", length = 64, nullable = false)
    private String bsrRetToAdr2 = SEPARATOR_BLANK;

    @Column(name = "BSR_RET_TO_CITY_NM", length = 64, nullable = false)
    private String bsrRetToCityNm = SEPARATOR_BLANK;

    @Column(name = "BSR_RET_TO_ST_CD", length = 3, nullable = false)
    private String bsrRetToStCd = SEPARATOR_BLANK;

    @Column(name = "BSR_RET_TO_PST_CD", length = 20, nullable = false)
    private String bsrRetToPstCd = SEPARATOR_BLANK;

    @Column(name = "BSR_SUS_WIP_NBR", length = 5, nullable = false)
    private Short bsrSusWipNbr;

    @Column(name = "BSR_LST_SUS_WIP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bsrLstSusWipDt;

    @Column(name = "BSR_UN_ID_WIP_NBR", length = 5, nullable = false)
    private Short bsrUnIdWipNbr;

    @Column(name = "BSR_LST_UN_ID_WIP", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bsrLstUnIdWipDt;

    @Column(name = "BSR_NSF_WIP_NBR", length = 5, nullable = false)
    private Short bsrNsfWipNbr;

    @Column(name = "BSR_PCN_WIP_NBR", length = 5, nullable = false)
    private Short bsrPcnWipNbr;

    @Column(name = "BSR_DSB_WIP_AMT", precision = 14, scale = 2, nullable = false)
    private double bsrDsbWipAmt;

    @Column(name = "BSR_STT_WIP_NBR", length = 5, nullable = false)
    private Short bsrSttWipNbr;

    @Column(name = "BSR_LST_STT_WIP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bsrLstSttWipDt;

    @Column(name = "BSR_WRO_WIP_IND", nullable = false)
    private char bsrWroWipInd;

    public String getBilTypeCd() {
        return bilTypeCd;
    }

    public void setBilTypeCd(String bilTypeCd) {
        this.bilTypeCd = bilTypeCd;
    }

    public String getBsrPayToLngNm() {
        return bsrPayToLngNm;
    }

    public void setBsrPayToLngNm(String bsrPayToLngNm) {
        this.bsrPayToLngNm = bsrPayToLngNm;
    }

    public String getBsrPayToAdr1() {
        return bsrPayToAdr1;
    }

    public void setBsrPayToAdr1(String bsrPayToAdr1) {
        this.bsrPayToAdr1 = bsrPayToAdr1;
    }

    public String getBsrPayToAdr2() {
        return bsrPayToAdr2;
    }

    public void setBsrPayToAdr2(String bsrPayToAdr2) {
        this.bsrPayToAdr2 = bsrPayToAdr2;
    }

    public String getBsrPayToCityNm() {
        return bsrPayToCityNm;
    }

    public void setBsrPayToCityNm(String bsrPayToCityNm) {
        this.bsrPayToCityNm = bsrPayToCityNm;
    }

    public String getBsrPayToStCd() {
        return bsrPayToStCd;
    }

    public void setBsrPayToStCd(String bsrPayToStCd) {
        this.bsrPayToStCd = bsrPayToStCd;
    }

    public String getBsrPayToPstCd() {
        return bsrPayToPstCd;
    }

    public void setBsrPayToPstCd(String bsrPayToPstCd) {
        this.bsrPayToPstCd = bsrPayToPstCd;
    }

    public String getBsrRetToLngNm() {
        return bsrRetToLngNm;
    }

    public void setBsrRetToLngNm(String bsrRetToLngNm) {
        this.bsrRetToLngNm = bsrRetToLngNm;
    }

    public String getBsrRetToAdr1() {
        return bsrRetToAdr1;
    }

    public void setBsrRetToAdr1(String bsrRetToAdr1) {
        this.bsrRetToAdr1 = bsrRetToAdr1;
    }

    public String getBsrRetToAdr2() {
        return bsrRetToAdr2;
    }

    public void setBsrRetToAdr2(String bsrRetToAdr2) {
        this.bsrRetToAdr2 = bsrRetToAdr2;
    }

    public String getBsrRetToCityNm() {
        return bsrRetToCityNm;
    }

    public void setBsrRetToCityNm(String bsrRetToCityNm) {
        this.bsrRetToCityNm = bsrRetToCityNm;
    }

    public String getBsrRetToStCd() {
        return bsrRetToStCd;
    }

    public void setBsrRetToStCd(String bsrRetToStCd) {
        this.bsrRetToStCd = bsrRetToStCd;
    }

    public String getBsrRetToPstCd() {
        return bsrRetToPstCd;
    }

    public void setBsrRetToPstCd(String bsrRetToPstCd) {
        this.bsrRetToPstCd = bsrRetToPstCd;
    }

    public Short getBsrSusWipNbr() {
        return bsrSusWipNbr;
    }

    public void setBsrSusWipNbr(Short bsrSusWipNbr) {
        this.bsrSusWipNbr = bsrSusWipNbr;
    }

    public ZonedDateTime getBsrLstSusWipDt() {
        return bsrLstSusWipDt;
    }

    public void setBsrLstSusWipDt(ZonedDateTime bsrLstSusWipDt) {
        this.bsrLstSusWipDt = bsrLstSusWipDt;
    }

    public Short getBsrUnIdWipNbr() {
        return bsrUnIdWipNbr;
    }

    public void setBsrUnIdWipNbr(Short bsrUnIdWipNbr) {
        this.bsrUnIdWipNbr = bsrUnIdWipNbr;
    }

    public ZonedDateTime getBsrLstUnIdWipDt() {
        return bsrLstUnIdWipDt;
    }

    public void setBsrLstUnIdWipDt(ZonedDateTime bsrLstUnIdWipDt) {
        this.bsrLstUnIdWipDt = bsrLstUnIdWipDt;
    }

    public Short getBsrNsfWipNbr() {
        return bsrNsfWipNbr;
    }

    public void setBsrNsfWipNbr(Short bsrNsfWipNbr) {
        this.bsrNsfWipNbr = bsrNsfWipNbr;
    }

    public Short getBsrPcnWipNbr() {
        return bsrPcnWipNbr;
    }

    public void setBsrPcnWipNbr(Short bsrPcnWipNbr) {
        this.bsrPcnWipNbr = bsrPcnWipNbr;
    }

    public double getBsrDsbWipAmt() {
        return bsrDsbWipAmt;
    }

    public void setBsrDsbWipAmt(double bsrDsbWipAmt) {
        this.bsrDsbWipAmt = bsrDsbWipAmt;
    }

    public Short getBsrSttWipNbr() {
        return bsrSttWipNbr;
    }

    public void setBsrSttWipNbr(Short bsrSttWipNbr) {
        this.bsrSttWipNbr = bsrSttWipNbr;
    }

    public ZonedDateTime getBsrLstSttWipDt() {
        return bsrLstSttWipDt;
    }

    public void setBsrLstSttWipDt(ZonedDateTime bsrLstSttWipDt) {
        this.bsrLstSttWipDt = bsrLstSttWipDt;
    }

    public char getBsrWroWipInd() {
        return bsrWroWipInd;
    }

    public void setBsrWroWipInd(char bsrWroWipInd) {
        this.bsrWroWipInd = bsrWroWipInd;
    }

}
