package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import billing.data.entity.id.BilPolicyTermId;

@Entity
@Table(name = "BIL_POLICY_TRM")
public class BilPolicyTerm implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilPolicyTermId bilPolicyTermId;

    @Column(name = "MASTER_COMPANY_NBR", length = 8, nullable = false)
    private String masterCompanyNbr = SEPARATOR_BLANK;

    @Column(name = "PLN_EXP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime plnExpDt;

    @Column(name = "BPT_ISU_CLT_ID", length = 8, nullable = false)
    private String bptIsuCltId = SEPARATOR_BLANK;

    @Column(name = "BPT_ISU_ADR_SEQ", length = 8, nullable = false)
    private short bptIsuAdrSeq;

    @Column(name = "BPT_AGT_CLT_ID", length = 8, nullable = false)
    private String bptAgtCltId = SEPARATOR_BLANK;

    @Column(name = "BPT_AGT_ADR_SEQ", length = 8, nullable = false)
    private short bptAgtAdrSeq;

    @Column(name = "LOB_CD", length = 8, nullable = false)
    private String lobCd = SEPARATOR_BLANK;

    @Column(name = "BIL_STATE_PVN_CD", length = 8, nullable = false)
    private String billStatePvnCd = SEPARATOR_BLANK;

    @Column(name = "BIL_COUNTRY_CD", length = 8, nullable = false)
    private String billCountryCd = SEPARATOR_BLANK;

    @Column(name = "BPT_WRO_PRC_IND", nullable = false)
    private char bptWroPrcInd;

    @Column(name = "BIL_PLAN_CD", length = 8, nullable = false)
    private String billPlanCd = SEPARATOR_BLANK;

    @Column(name = "BIL_POL_STATUS_CD", nullable = false)
    private char billPolStatusCd;

    @Column(name = "BIL_SUS_FU_REA_CD", length = 8, nullable = false)
    private String billSusFuReaCd = SEPARATOR_BLANK;

    @Column(name = "BPT_STATUS_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bptStatusEffDt;

    @Column(name = "BPT_AGREEMENT_IND", nullable = false)
    private char bptAgreementInd;

    @Column(name = "BPT_ISSUE_SYS_ID", length = 8, nullable = false)
    private String bptIssueSysId = SEPARATOR_BLANK;

    @Column(name = "BIL_TO_FRO_TRF_NBR", length = 8, nullable = false)
    private String billToFroTrfNbr = SEPARATOR_BLANK;

    @Column(name = "BPT_AUDIT_CD", nullable = false)
    private char bptAuditCd;

    @Column(name = "BPT_RE_DTB_CD", nullable = false)
    private char bptReDtbCd;

    @Column(name = "BPT_CNR_POL_SYM_CD", length = 8, nullable = false)
    private String bptCnrPolSymCd = SEPARATOR_BLANK;

    @Column(name = "BPT_CNR_POL_NBR", length = 8, nullable = false)
    private String bptCnrPolNbr = SEPARATOR_BLANK;

    @Column(name = "BPT_CNR_POL_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bptCnrPolEffDt;

    @Column(name = "BPT_TERM_SPLIT_IND", nullable = false)
    private char bptTermSplitInd;

    @Column(name = "BPT_PCN_CAUSAL_IND", nullable = false)
    private char bptPcnCausalInd;

    @Column(name = "BPT_TRF_BILL_PLAN", length = 8, nullable = false)
    private String bptTrfBillPlan = SEPARATOR_BLANK;

    @Column(name = "BPT_SGD_PLAN_IND", nullable = false)
    private char bptSgdPlanInd;

    @Column(name = "BPT_BELOW_MIN_CNT", length = 8, nullable = false)
    private short bptBelowMinCnt;

    @Column(name = "BPT_POL_UW_STA_CD", nullable = false)
    private char bptPolUwStaCd;

    @Column(name = "BPT_UW_STA_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bptUwStaEffDt;

    @Column(name = "BPT_POL_COL_AMT", precision = 14, scale = 2, nullable = false)
    private double bptPolColAmt;

    @Column(name = "BPT_ACT_COL_CD", nullable = false)
    private char bptActColCd;

    @Column(name = "FULL_PMT_DSC_CD", nullable = false)
    private char fullPmtDscCd;

    @Column(name = "BIL_CUR_DSC_AMT", precision = 14, scale = 2, nullable = false)
    private double billCurDscAmt;

    @Column(name = "BIL_INV_DSC_AMT", precision = 14, scale = 2, nullable = false)
    private double billInvDscAmt;

    @Column(name = "BPT_BUS_UNIT_CD", length = 8, nullable = false)
    private String bptBusUnitCd = SEPARATOR_BLANK;

    public BilPolicyTermId getBillPolicyTermId() {
        return bilPolicyTermId;
    }

    public void setBillPolicyTermId(BilPolicyTermId bilPolicyTermId) {
        this.bilPolicyTermId = bilPolicyTermId;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public ZonedDateTime getPlnExpDt() {
        return plnExpDt;
    }

    public void setPlnExpDt(ZonedDateTime plnExpDt) {
        this.plnExpDt = plnExpDt;
    }

    public String getBptIsuCltId() {
        return bptIsuCltId;
    }

    public void setBptIsuCltId(String bptIsuCltId) {
        this.bptIsuCltId = bptIsuCltId;
    }

    public short getBptIsuAdrSeq() {
        return bptIsuAdrSeq;
    }

    public void setBptIsuAdrSeq(short bptIsuAdrSeq) {
        this.bptIsuAdrSeq = bptIsuAdrSeq;
    }

    public String getBptAgtCltId() {
        return bptAgtCltId;
    }

    public void setBptAgtCltId(String bptAgtCltId) {
        this.bptAgtCltId = bptAgtCltId;
    }

    public short getBptAgtAdrSeq() {
        return bptAgtAdrSeq;
    }

    public void setBptAgtAdrSeq(short bptAgtAdrSeq) {
        this.bptAgtAdrSeq = bptAgtAdrSeq;
    }

    public String getLobCd() {
        return lobCd;
    }

    public void setLobCd(String lobCd) {
        this.lobCd = lobCd;
    }

    public String getBillStatePvnCd() {
        return billStatePvnCd;
    }

    public void setBillStatePvnCd(String billStatePvnCd) {
        this.billStatePvnCd = billStatePvnCd;
    }

    public String getBillCountryCd() {
        return billCountryCd;
    }

    public void setBillCountryCd(String billCountryCd) {
        this.billCountryCd = billCountryCd;
    }

    public char getBptWroPrcInd() {
        return bptWroPrcInd;
    }

    public void setBptWroPrcInd(char bptWroPrcInd) {
        this.bptWroPrcInd = bptWroPrcInd;
    }

    public String getBillPlanCd() {
        return billPlanCd;
    }

    public void setBillPlanCd(String billPlanCd) {
        this.billPlanCd = billPlanCd;
    }

    public char getBillPolStatusCd() {
        return billPolStatusCd;
    }

    public void setBillPolStatusCd(char billPolStatusCd) {
        this.billPolStatusCd = billPolStatusCd;
    }

    public String getBillSusFuReaCd() {
        return billSusFuReaCd;
    }

    public void setBillSusFuReaCd(String billSusFuReaCd) {
        this.billSusFuReaCd = billSusFuReaCd;
    }

    public ZonedDateTime getBptStatusEffDt() {
        return bptStatusEffDt;
    }

    public void setBptStatusEffDt(ZonedDateTime bptStatusEffDt) {
        this.bptStatusEffDt = bptStatusEffDt;
    }

    public char getBptAgreementInd() {
        return bptAgreementInd;
    }

    public void setBptAgreementInd(char bptAgreementInd) {
        this.bptAgreementInd = bptAgreementInd;
    }

    public String getBptIssueSysId() {
        return bptIssueSysId;
    }

    public void setBptIssueSysId(String bptIssueSysId) {
        this.bptIssueSysId = bptIssueSysId;
    }

    public String getBillToFroTrfNbr() {
        return billToFroTrfNbr;
    }

    public void setBillToFroTrfNbr(String billToFroTrfNbr) {
        this.billToFroTrfNbr = billToFroTrfNbr;
    }

    public char getBptAuditCd() {
        return bptAuditCd;
    }

    public void setBptAuditCd(char bptAuditCd) {
        this.bptAuditCd = bptAuditCd;
    }

    public char getBptReDtbCd() {
        return bptReDtbCd;
    }

    public void setBptReDtbCd(char bptReDtbCd) {
        this.bptReDtbCd = bptReDtbCd;
    }

    public String getBptCnrPolSymCd() {
        return bptCnrPolSymCd;
    }

    public void setBptCnrPolSymCd(String bptCnrPolSymCd) {
        this.bptCnrPolSymCd = bptCnrPolSymCd;
    }

    public String getBptCnrPolNbr() {
        return bptCnrPolNbr;
    }

    public void setBptCnrPolNbr(String bptCnrPolNbr) {
        this.bptCnrPolNbr = bptCnrPolNbr;
    }

    public ZonedDateTime getBptCnrPolEffDt() {
        return bptCnrPolEffDt;
    }

    public void setBptCnrPolEffDt(ZonedDateTime bptCnrPolEffDt) {
        this.bptCnrPolEffDt = bptCnrPolEffDt;
    }

    public char getBptTermSplitInd() {
        return bptTermSplitInd;
    }

    public void setBptTermSplitInd(char bptTermSplitInd) {
        this.bptTermSplitInd = bptTermSplitInd;
    }

    public char getBptPcnCausalInd() {
        return bptPcnCausalInd;
    }

    public void setBptPcnCausalInd(char bptPcnCausalInd) {
        this.bptPcnCausalInd = bptPcnCausalInd;
    }

    public String getBptTrfBillPlan() {
        return bptTrfBillPlan;
    }

    public void setBptTrfBillPlan(String bptTrfBillPlan) {
        this.bptTrfBillPlan = bptTrfBillPlan;
    }

    public char getBptSgdPlanInd() {
        return bptSgdPlanInd;
    }

    public void setBptSgdPlanInd(char bptSgdPlanInd) {
        this.bptSgdPlanInd = bptSgdPlanInd;
    }

    public short getBptBelowMinCnt() {
        return bptBelowMinCnt;
    }

    public void setBptBelowMinCnt(short bptBelowMinCnt) {
        this.bptBelowMinCnt = bptBelowMinCnt;
    }

    public char getBptPolUwStaCd() {
        return bptPolUwStaCd;
    }

    public void setBptPolUwStaCd(char bptPolUwStaCd) {
        this.bptPolUwStaCd = bptPolUwStaCd;
    }

    public ZonedDateTime getBptUwStaEffDt() {
        return bptUwStaEffDt;
    }

    public void setBptUwStaEffDt(ZonedDateTime bptUwStaEffDt) {
        this.bptUwStaEffDt = bptUwStaEffDt;
    }

    public double getBptPolColAmt() {
        return bptPolColAmt;
    }

    public void setBptPolColAmt(double bptPolColAmt) {
        this.bptPolColAmt = bptPolColAmt;
    }

    public char getBptActColCd() {
        return bptActColCd;
    }

    public void setBptActColCd(char bptActColCd) {
        this.bptActColCd = bptActColCd;
    }

    public char getFullPmtDscCd() {
        return fullPmtDscCd;
    }

    public void setFullPmtDscCd(char fullPmtDscCd) {
        this.fullPmtDscCd = fullPmtDscCd;
    }

    public double getBillCurDscAmt() {
        return billCurDscAmt;
    }

    public void setBillCurDscAmt(double billCurDscAmt) {
        this.billCurDscAmt = billCurDscAmt;
    }

    public double getBillInvDscAmt() {
        return billInvDscAmt;
    }

    public void setBillInvDscAmt(double billInvDscAmt) {
        this.billInvDscAmt = billInvDscAmt;
    }

    public String getBptBusUnitCd() {
        return bptBusUnitCd;
    }

    public void setBptBusUnitCd(String bptBusUnitCd) {
        this.bptBusUnitCd = bptBusUnitCd;
    }

}
