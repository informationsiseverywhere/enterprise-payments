package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilAccountBalanceId;

@Entity
@Table(name = "BIL_ACT_BALANCE")
public class BilAccountBalance implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilAccountBalanceId bilAccountBalanceId;

    @Column(name = "BIL_ACCOUNT_NBR", length = 30, nullable = false)
    private String billAccountNumber = SEPARATOR_BLANK;

    @Column(name = "BIL_TYPE_CD", length = 2, nullable = false)
    private String billTypeCode = SEPARATOR_BLANK;

    @Column(name = "BAT_STATUS_CD", nullable = false)
    private char batStatusCode;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "BIL_ACT_BAL_AMT", precision = 14, scale = 2, nullable = false)
    private double billActBalAmt;

    @Column(name = "TOT_PRM_AMT", precision = 14, scale = 2, nullable = false)
    private double totPrmAmt;

    @Column(name = "TOT_PRM_PAID_AMT", precision = 14, scale = 2, nullable = false)
    private double totPrmPaidAmt;

    @Column(name = "TOT_PRM_WRO_AMT", precision = 14, scale = 2, nullable = false)
    private double totPrmWroAmt;

    @Column(name = "TOT_PRM_BAL_AMT", precision = 14, scale = 2, nullable = false)
    private double totPrmBalAmt;

    @Column(name = "INV_PRM_AMT", precision = 14, scale = 2, nullable = false)
    private double invPrmAmt;

    @Column(name = "INV_PRM_PAID_AMT", precision = 14, scale = 2, nullable = false)
    private double invPrmPaidAmt;

    @Column(name = "INV_PRM_WRO_AMT", precision = 14, scale = 2, nullable = false)
    private double invPrmWroAmt;

    @Column(name = "INV_PRM_BAL_AMT", precision = 14, scale = 2, nullable = false)
    private double invPrmBalAmt;

    @Column(name = "TOT_CRG_AMT", precision = 14, scale = 2, nullable = false)
    private double totCrgAmt;

    @Column(name = "TOT_CRG_PAID_AMT", precision = 14, scale = 2, nullable = false)
    private double totCrgPaidAmt;

    @Column(name = "TOT_CRG_WRO_AMT", precision = 14, scale = 2, nullable = false)
    private double totCrgWroAmt;

    @Column(name = "TOT_CRG_BAL_AMT", precision = 14, scale = 2, nullable = false)
    private double totCrgBalAmt;

    @Column(name = "INV_CRG_AMT", precision = 14, scale = 2, nullable = false)
    private double invCrgAmt;

    @Column(name = "INV_CRG_PAID_AMT", precision = 14, scale = 2, nullable = false)
    private double invCrgPaidAmt;

    @Column(name = "INV_CRG_WRO_AMT", precision = 14, scale = 2, nullable = false)
    private double invCrgWroAmt;

    @Column(name = "INV_CRG_BAL_AMT", precision = 14, scale = 2, nullable = false)
    private double invCrgBalAmt;

    @Column(name = "TOT_CASH_SUS_AMT", precision = 14, scale = 2, nullable = false)
    private double totCashSusAmt;

    @Column(name = "TOT_CASH_DSB_AMT", precision = 14, scale = 2, nullable = false)
    private double totCashDsbAmt;

    @Column(name = "TOT_CASH_WRO_AMT", precision = 14, scale = 2, nullable = false)
    private double totCashWroAmt;

    @Column(name = "TOT_CASH_REV_AMT", precision = 14, scale = 2, nullable = false)
    private double totCashRevAmt;

    @Column(name = "TOT_CASH_TRF_AMT", precision = 14, scale = 2, nullable = false)
    private double totCashTrfAmt;

    @Column(name = "TOT_SVC_CRG_AMT", precision = 14, scale = 2, nullable = false)
    private double totSvcCrgAmt;

    @Column(name = "TOT_LATE_CRG_AMT", precision = 14, scale = 2, nullable = false)
    private double totLateCrgAmt;

    @Column(name = "TOT_PNY_CRG_AMT", precision = 14, scale = 2, nullable = false)
    private double totPnyCrgAmt;

    @Column(name = "TOT_DPAY_CRG_AMT", precision = 14, scale = 2, nullable = false)
    private double totDpayCrgAmt;

    public BilAccountBalanceId getBillAccountBalanceId() {
        return bilAccountBalanceId;
    }

    public void setBillAccountBalanceId(BilAccountBalanceId bilAccountBalanceId) {
        this.bilAccountBalanceId = bilAccountBalanceId;
    }

    public String getBillAccountNumber() {
        return billAccountNumber;
    }

    public void setBillAccountNumber(String billAccountNumber) {
        this.billAccountNumber = billAccountNumber;
    }

    public String getBillTypeCode() {
        return billTypeCode;
    }

    public void setBillTypeCode(String billTypeCode) {
        this.billTypeCode = billTypeCode;
    }

    public char getBatStatusCode() {
        return batStatusCode;
    }

    public void setBatStatusCode(char batStatusCode) {
        this.batStatusCode = batStatusCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getBillActBalAmt() {
        return billActBalAmt;
    }

    public void setBillActBalAmt(double billActBalAmt) {
        this.billActBalAmt = billActBalAmt;
    }

    public double getTotPrmAmt() {
        return totPrmAmt;
    }

    public void setTotPrmAmt(double totPrmAmt) {
        this.totPrmAmt = totPrmAmt;
    }

    public double getTotPrmPaidAmt() {
        return totPrmPaidAmt;
    }

    public void setTotPrmPaidAmt(double totPrmPaidAmt) {
        this.totPrmPaidAmt = totPrmPaidAmt;
    }

    public double getTotPrmWroAmt() {
        return totPrmWroAmt;
    }

    public void setTotPrmWroAmt(double totPrmWroAmt) {
        this.totPrmWroAmt = totPrmWroAmt;
    }

    public double getTotPrmBalAmt() {
        return totPrmBalAmt;
    }

    public void setTotPrmBalAmt(double totPrmBalAmt) {
        this.totPrmBalAmt = totPrmBalAmt;
    }

    public double getInvPrmAmt() {
        return invPrmAmt;
    }

    public void setInvPrmAmt(double invPrmAmt) {
        this.invPrmAmt = invPrmAmt;
    }

    public double getInvPrmPaidAmt() {
        return invPrmPaidAmt;
    }

    public void setInvPrmPaidAmt(double invPrmPaidAmt) {
        this.invPrmPaidAmt = invPrmPaidAmt;
    }

    public double getInvPrmWroAmt() {
        return invPrmWroAmt;
    }

    public void setInvPrmWroAmt(double invPrmWroAmt) {
        this.invPrmWroAmt = invPrmWroAmt;
    }

    public double getInvPrmBalAmt() {
        return invPrmBalAmt;
    }

    public void setInvPrmBalAmt(double invPrmBalAmt) {
        this.invPrmBalAmt = invPrmBalAmt;
    }

    public double getTotCrgAmt() {
        return totCrgAmt;
    }

    public void setTotCrgAmt(double totCrgAmt) {
        this.totCrgAmt = totCrgAmt;
    }

    public double getTotCrgPaidAmt() {
        return totCrgPaidAmt;
    }

    public void setTotCrgPaidAmt(double totCrgPaidAmt) {
        this.totCrgPaidAmt = totCrgPaidAmt;
    }

    public double getTotCrgWroAmt() {
        return totCrgWroAmt;
    }

    public void setTotCrgWroAmt(double totCrgWroAmt) {
        this.totCrgWroAmt = totCrgWroAmt;
    }

    public double getTotCrgBalAmt() {
        return totCrgBalAmt;
    }

    public void setTotCrgBalAmt(double totCrgBalAmt) {
        this.totCrgBalAmt = totCrgBalAmt;
    }

    public double getInvCrgAmt() {
        return invCrgAmt;
    }

    public void setInvCrgAmt(double invCrgAmt) {
        this.invCrgAmt = invCrgAmt;
    }

    public double getInvCrgPaidAmt() {
        return invCrgPaidAmt;
    }

    public void setInvCrgPaidAmt(double invCrgPaidAmt) {
        this.invCrgPaidAmt = invCrgPaidAmt;
    }

    public double getInvCrgWroAmt() {
        return invCrgWroAmt;
    }

    public void setInvCrgWroAmt(double invCrgWroAmt) {
        this.invCrgWroAmt = invCrgWroAmt;
    }

    public double getInvCrgBalAmt() {
        return invCrgBalAmt;
    }

    public void setInvCrgBalAmt(double invCrgBalAmt) {
        this.invCrgBalAmt = invCrgBalAmt;
    }

    public double getTotCashSusAmt() {
        return totCashSusAmt;
    }

    public void setTotCashSusAmt(double totCashSusAmt) {
        this.totCashSusAmt = totCashSusAmt;
    }

    public double getTotCashDsbAmt() {
        return totCashDsbAmt;
    }

    public void setTotCashDsbAmt(double totCashDsbAmt) {
        this.totCashDsbAmt = totCashDsbAmt;
    }

    public double getTotCashWroAmt() {
        return totCashWroAmt;
    }

    public void setTotCashWroAmt(double totCashWroAmt) {
        this.totCashWroAmt = totCashWroAmt;
    }

    public double getTotCashRevAmt() {
        return totCashRevAmt;
    }

    public void setTotCashRevAmt(double totCashRevAmt) {
        this.totCashRevAmt = totCashRevAmt;
    }

    public double getTotCashTrfAmt() {
        return totCashTrfAmt;
    }

    public void setTotCashTrfAmt(double totCashTrfAmt) {
        this.totCashTrfAmt = totCashTrfAmt;
    }

    public double getTotSvcCrgAmt() {
        return totSvcCrgAmt;
    }

    public void setTotSvcCrgAmt(double totSvcCrgAmt) {
        this.totSvcCrgAmt = totSvcCrgAmt;
    }

    public double getTotLateCrgAmt() {
        return totLateCrgAmt;
    }

    public void setTotLateCrgAmt(double totLateCrgAmt) {
        this.totLateCrgAmt = totLateCrgAmt;
    }

    public double getTotPnyCrgAmt() {
        return totPnyCrgAmt;
    }

    public void setTotPnyCrgAmt(double totPnyCrgAmt) {
        this.totPnyCrgAmt = totPnyCrgAmt;
    }

    public double getTotDpayCrgAmt() {
        return totDpayCrgAmt;
    }

    public void setTotDpayCrgAmt(double totDpayCrgAmt) {
        this.totDpayCrgAmt = totDpayCrgAmt;
    }

}
