package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import billing.data.entity.id.BilIstScheduleId;

@Entity
@Table(name = "BIL_IST_SCHEDULE")
public class BilIstSchedule implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilIstScheduleId bilIstScheduleId;

    @Column(name = "POL_EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime policyEffectiveDt;

    @Column(name = "BIL_PBL_ITEM_CD", length = 3, nullable = false)
    private String billPblItemCd = SEPARATOR_BLANK;

    @Column(name = "BIL_IST_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime billIstDueDt;

    @Column(name = "BIL_PAY_PTY_CD", nullable = false)
    private char billPayPtyCd;

    @Column(name = "BIL_SYS_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime billSysDueDt;

    @Column(name = "BIL_REFERENCE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime billReferenceDt;

    @Column(name = "BIL_ADJ_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime billAdjDueDt;

    @Column(name = "BIL_INV_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime billInvDt;

    @Column(name = "BIL_PRM_IST_AMT", precision = 14, scale = 2, nullable = false)
    private double billPrmIstAmt;

    @Column(name = "BIL_PRM_IST_NET", precision = 14, scale = 2, nullable = false)
    private double billPrmIstNet;

    @Column(name = "BIS_CRE_FUT_AMT", precision = 14, scale = 2, nullable = false)
    private double bisCreFutAmt;

    @Column(name = "BIS_CRE_OPN_AMT", precision = 14, scale = 2, nullable = false)
    private double bisCreOpnAmt;

    @Column(name = "BIS_CRE_PRM_NET", precision = 14, scale = 2, nullable = false)
    private double bisCrePrmAmt;

    @Column(name = "BIS_CNC_FUT_AMT", precision = 14, scale = 2, nullable = false)
    private double bisCncFutAmt;

    @Column(name = "BIS_CNC_OPN_AMT", precision = 14, scale = 2, nullable = false)
    private double bisCncOpnAmt;

    @Column(name = "BIS_CNC_PRM_NET", precision = 14, scale = 2, nullable = false)
    private double bisCncPrmAmt;

    @Column(name = "BIS_PRM_PAID_AMT", precision = 14, scale = 2, nullable = false)
    private double bisPrmPaidAmt;

    @Column(name = "BIS_WRO_PRM_AMT", precision = 14, scale = 2, nullable = false)
    private double bisWroPrmAmt;

    @Column(name = "BIS_CRE_PAID_AMT", precision = 14, scale = 2, nullable = false)
    private double bisCrePaidAmt;

    @Column(name = "BIL_COMMISSION_PCT", precision = 9, scale = 6, nullable = false)
    private double billCommissionPct;

    @Column(name = "BIL_INVOICE_CD", nullable = false)
    private char billInvoiceCd;

    @Column(name = "BIS_NON_TRM_DT_IND", nullable = false)
    private char bisNonTrmDtInd;

    @Column(name = "BIS_TYPE_WHEN_INV", nullable = false)
    private char bisTypeWhenInv;

    @Column(name = "BIL_DISCREPANCY_CD", nullable = false)
    private char billDiscrepancyCd;

    @Column(name = "BIL_DED_CD", nullable = false)
    private char billDedCd;

    @Column(name = "BIL_STT_DTL_TYP_CD", length = 3, nullable = false)
    private String billSttDtlTypeCd = SEPARATOR_BLANK;

    public BilIstScheduleId getBillIstScheduleId() {
        return bilIstScheduleId;
    }

    public void setBillIstScheduleId(BilIstScheduleId bilIstScheduleId) {
        this.bilIstScheduleId = bilIstScheduleId;
    }

    public ZonedDateTime getPolicyEffectiveDt() {
        return policyEffectiveDt;
    }

    public void setPolicyEffectiveDt(ZonedDateTime policyEffectiveDt) {
        this.policyEffectiveDt = policyEffectiveDt;
    }

    public String getBillPblItemCd() {
        return billPblItemCd;
    }

    public void setBillPblItemCd(String billPblItemCd) {
        this.billPblItemCd = billPblItemCd;
    }

    public ZonedDateTime getBillIstDueDt() {
        return billIstDueDt;
    }

    public void setBillIstDueDt(ZonedDateTime billIstDueDt) {
        this.billIstDueDt = billIstDueDt;
    }

    public char getBillPayPtyCd() {
        return billPayPtyCd;
    }

    public void setBillPayPtyCd(char billPayPtyCd) {
        this.billPayPtyCd = billPayPtyCd;
    }

    public ZonedDateTime getBillSysDueDt() {
        return billSysDueDt;
    }

    public void setBillSysDueDt(ZonedDateTime billSysDueDt) {
        this.billSysDueDt = billSysDueDt;
    }

    public ZonedDateTime getBillReferenceDt() {
        return billReferenceDt;
    }

    public void setBillReferenceDt(ZonedDateTime billReferenceDt) {
        this.billReferenceDt = billReferenceDt;
    }

    public ZonedDateTime getBillAdjDueDt() {
        return billAdjDueDt;
    }

    public void setBillAdjDueDt(ZonedDateTime billAdjDueDt) {
        this.billAdjDueDt = billAdjDueDt;
    }

    public ZonedDateTime getBillInvDt() {
        return billInvDt;
    }

    public void setBillInvDt(ZonedDateTime billInvDt) {
        this.billInvDt = billInvDt;
    }

    public double getBillPrmIstAmt() {
        return billPrmIstAmt;
    }

    public void setBillPrmIstAmt(double billPrmIstAmt) {
        this.billPrmIstAmt = billPrmIstAmt;
    }

    public double getBillPrmIstNet() {
        return billPrmIstNet;
    }

    public void setBillPrmIstNet(double billPrmIstNet) {
        this.billPrmIstNet = billPrmIstNet;
    }

    public double getBisCreFutAmt() {
        return bisCreFutAmt;
    }

    public void setBisCreFutAmt(double bisCreFutAmt) {
        this.bisCreFutAmt = bisCreFutAmt;
    }

    public double getBisCreOpnAmt() {
        return bisCreOpnAmt;
    }

    public void setBisCreOpnAmt(double bisCreOpnAmt) {
        this.bisCreOpnAmt = bisCreOpnAmt;
    }

    public double getBisCrePrmAmt() {
        return bisCrePrmAmt;
    }

    public void setBisCrePrmAmt(double bisCrePrmAmt) {
        this.bisCrePrmAmt = bisCrePrmAmt;
    }

    public double getBisCncFutAmt() {
        return bisCncFutAmt;
    }

    public void setBisCncFutAmt(double bisCncFutAmt) {
        this.bisCncFutAmt = bisCncFutAmt;
    }

    public double getBisCncOpnAmt() {
        return bisCncOpnAmt;
    }

    public void setBisCncOpnAmt(double bisCncOpnAmt) {
        this.bisCncOpnAmt = bisCncOpnAmt;
    }

    public double getBisCncPrmAmt() {
        return bisCncPrmAmt;
    }

    public void setBisCncPrmAmt(double bisCncPrmAmt) {
        this.bisCncPrmAmt = bisCncPrmAmt;
    }

    public double getBisPrmPaidAmt() {
        return bisPrmPaidAmt;
    }

    public void setBisPrmPaidAmt(double bisPrmPaidAmt) {
        this.bisPrmPaidAmt = bisPrmPaidAmt;
    }

    public double getBisWroPrmAmt() {
        return bisWroPrmAmt;
    }

    public void setBisWroPrmAmt(double bisWroPrmAmt) {
        this.bisWroPrmAmt = bisWroPrmAmt;
    }

    public double getBisCrePaidAmt() {
        return bisCrePaidAmt;
    }

    public void setBisCrePaidAmt(double bisCrePaidAmt) {
        this.bisCrePaidAmt = bisCrePaidAmt;
    }

    public double getBillCommissionPct() {
        return billCommissionPct;
    }

    public void setBillCommissionPct(double billCommissionPct) {
        this.billCommissionPct = billCommissionPct;
    }

    public char getBillInvoiceCd() {
        return billInvoiceCd;
    }

    public void setBillInvoiceCd(char billInvoiceCd) {
        this.billInvoiceCd = billInvoiceCd;
    }

    public char getBisNonTrmDtInd() {
        return bisNonTrmDtInd;
    }

    public void setBisNonTrmDtInd(char bisNonTrmDtInd) {
        this.bisNonTrmDtInd = bisNonTrmDtInd;
    }

    public char getBisTypeWhenInv() {
        return bisTypeWhenInv;
    }

    public void setBisTypeWhenInv(char bisTypeWhenInv) {
        this.bisTypeWhenInv = bisTypeWhenInv;
    }

    public char getBillDiscrepancyCd() {
        return billDiscrepancyCd;
    }

    public void setBillDiscrepancyCd(char billDiscrepancyCd) {
        this.billDiscrepancyCd = billDiscrepancyCd;
    }

    public char getBillDedCd() {
        return billDedCd;
    }

    public void setBillDedCd(char billDedCd) {
        this.billDedCd = billDedCd;
    }

    public String getBillSttDtlTypeCd() {
        return billSttDtlTypeCd;
    }

    public void setBillSttDtlTypeCd(String billSttDtlTypeCd) {
        this.billSttDtlTypeCd = billSttDtlTypeCd;
    }

}
