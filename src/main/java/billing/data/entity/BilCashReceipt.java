package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilCashReceiptId;

@Entity
@Table(name = "BIL_CASH_RECEIPT")
public final class BilCashReceipt implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilCashReceiptId bilCashReceiptId;

    @Column(name = "BIL_ENTRY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime entryDate;

    @Column(name = "BIL_ENTRY_NBR", length = 4, nullable = false)
    private String entryNumber = SEPARATOR_BLANK;

    @Column(name = "BIL_ENTRY_SEQ_NBR")
    private short entrySequenceNumber;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_AMT", nullable = false)
    private double receiptAmount;

    @Column(name = "BIL_RCT_ID", length = 10, nullable = false)
    private String receiptId = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_TYPE_CD", length = 2, nullable = false)
    private String receiptTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_RECEIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime receiptReceiveDate;

    @Column(name = "BIL_DEPOSIT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime depositeDate;

    @Column(name = "BIL_BANK_CD", length = 3, nullable = false)
    private String bankCd = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_TYPE_CD", nullable = false)
    private char creditCardTypeCd;

    @Column(name = "BIL_CRCRD_ACT_NBR", length = 25, nullable = false)
    private String creditCardAccounttNumber = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_EXP_DT", length = 4, nullable = false)
    private String creditCardExpirationDate = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_AUT_APV", length = 6, nullable = false)
    private String credutCardAuthroizationApproval = SEPARATOR_BLANK;

    @Column(name = "BIL_CSH_ETR_MTH_CD", nullable = false)
    private char cashEntryMethodCd;

    @Column(name = "BIL_TAX_YEAR_NBR")
    private short taxYearNumber;

    @Column(name = "BCR_TTY_CASH_IND", nullable = false)
    private char thirdPartyCashIdentifier = BLANK_CHAR;

    @Column(name = "BIL_TO_FRO_TRF_NBR", length = 30, nullable = false)
    private String toFroTransferNumber = SEPARATOR_BLANK;

    @Column(name = "BIL_TRF_TYPE_CD", nullable = false)
    private char transferTypeCd = BLANK_CHAR;

    @Column(name = "BIL_PSTMRK_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime postMarkDate;

    @Column(name = "BIL_PSTMRK_CD", nullable = false)
    private char postMarkCd;

    @Column(name = "BIL_RCT_CMT", length = 80, nullable = false)
    private String receiptComment = SEPARATOR_BLANK;

    @Column(name = "AGENCY_NBR", length = 12, nullable = false)
    private String agencyNumber;

    @Column(name = "PMT_PFL_ID", length = 512, nullable = true)
    private String paymentProfileId;

    public ZonedDateTime getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(ZonedDateTime entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryNumber() {
        return entryNumber;
    }

    public void setEntryNumber(String entryNumber) {
        this.entryNumber = entryNumber;
    }

    public short getEntrySequenceNumber() {
        return entrySequenceNumber;
    }

    public void setEntrySequenceNumber(short entrySequenceNumber) {
        this.entrySequenceNumber = entrySequenceNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getReceiptAmount() {
        return receiptAmount;
    }

    public void setReceiptAmount(double receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public String getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId;
    }

    public String getReceiptTypeCd() {
        return receiptTypeCd;
    }

    public void setReceiptTypeCd(String receiptTypeCd) {
        this.receiptTypeCd = receiptTypeCd;
    }

    public ZonedDateTime getReceiptReceiveDate() {
        return receiptReceiveDate;
    }

    public void setReceiptReceiveDate(ZonedDateTime receiptReceiveDate) {
        this.receiptReceiveDate = receiptReceiveDate;
    }

    public ZonedDateTime getDepositeDate() {
        return depositeDate;
    }

    public void setDepositeDate(ZonedDateTime depositeDate) {
        this.depositeDate = depositeDate;
    }

    public String getBankCd() {
        return bankCd;
    }

    public void setBankCd(String bankCd) {
        this.bankCd = bankCd;
    }

    public char getCreditCardTypeCd() {
        return creditCardTypeCd;
    }

    public void setCreditCardTypeCd(char creditCardTypeCd) {
        this.creditCardTypeCd = creditCardTypeCd;
    }

    public String getCreditCardAccounttNumber() {
        return creditCardAccounttNumber;
    }

    public void setCreditCardAccounttNumber(String creditCardAccounttNumber) {
        this.creditCardAccounttNumber = creditCardAccounttNumber;
    }

    public String getCreditCardExpirationDate() {
        return creditCardExpirationDate;
    }

    public void setCreditCardExpirationDate(String creditCardExpirationDate) {
        this.creditCardExpirationDate = creditCardExpirationDate;
    }

    public String getCredutCardAuthroizationApproval() {
        return credutCardAuthroizationApproval;
    }

    public void setCredutCardAuthroizationApproval(String credutCardAuthroizationApproval) {
        this.credutCardAuthroizationApproval = credutCardAuthroizationApproval;
    }

    public char getCashEntryMethodCd() {
        return cashEntryMethodCd;
    }

    public void setCashEntryMethodCd(char cashEntryMethodCd) {
        this.cashEntryMethodCd = cashEntryMethodCd;
    }

    public short getTaxYearNumber() {
        return taxYearNumber;
    }

    public void setTaxYearNumber(short taxYearNumber) {
        this.taxYearNumber = taxYearNumber;
    }

    public char getThirdPartyCashIdentifier() {
        return thirdPartyCashIdentifier;
    }

    public void setThirdPartyCashIdentifier(char thirdPartyCashIdentifier) {
        this.thirdPartyCashIdentifier = thirdPartyCashIdentifier;
    }

    public String getToFroTransferNumber() {
        return toFroTransferNumber;
    }

    public void setToFroTransferNumber(String toFroTransferNumber) {
        this.toFroTransferNumber = toFroTransferNumber;
    }

    public char getTransferTypeCd() {
        return transferTypeCd;
    }

    public void setTransferTypeCd(char transferTypeCd) {
        this.transferTypeCd = transferTypeCd;
    }

    public ZonedDateTime getPostMarkDate() {
        return postMarkDate;
    }

    public void setPostMarkDate(ZonedDateTime postMarkDate) {
        this.postMarkDate = postMarkDate;
    }

    public char getPostMarkCd() {
        return postMarkCd;
    }

    public void setPostMarkCd(char postMarkCd) {
        this.postMarkCd = postMarkCd;
    }

    public String getReceiptComment() {
        return receiptComment;
    }

    public void setReceiptComment(String receiptComment) {
        this.receiptComment = receiptComment;
    }

    public String getAgencyNumber() {
        return agencyNumber;
    }

    public void setAgencyNumber(String agencyNumber) {
        this.agencyNumber = agencyNumber;
    }

    public BilCashReceiptId getBilCashReceiptId() {
        return bilCashReceiptId;
    }

    public void setBilCashReceiptId(BilCashReceiptId bilCashReceiptId) {
        this.bilCashReceiptId = bilCashReceiptId;
    }

    public String getPaymentProfileId() {
        return paymentProfileId;
    }

    public void setPaymentProfileId(String paymentProfileId) {
        this.paymentProfileId = paymentProfileId;
    }
}
