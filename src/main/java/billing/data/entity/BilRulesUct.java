package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilRulesUctId;

@Entity
@Table(name = "BIL_RULES_UCT")
public class BilRulesUct implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilRulesUctId bilRulesUctId;

    @Column(name = "EXPIRATION_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime expirationDt;

    @Column(name = "BRT_RULE_CD", nullable = false)
    private char brtRuleCd;

    @Column(name = "BRT_RULE_DES", length = 100, nullable = false)
    private String brtRuleDes = SEPARATOR_BLANK;

    @Column(name = "BRT_PARM_LIST_TXT", length = 100, nullable = false)
    private String brtParmListTxt = SEPARATOR_BLANK;

    public BilRulesUctId getBilRulesUctId() {
        return bilRulesUctId;
    }

    public void setBilRulesUctId(BilRulesUctId bilRulesUctId) {
        this.bilRulesUctId = bilRulesUctId;
    }

    public ZonedDateTime getExpirationDt() {
        return expirationDt;
    }

    public void setExpirationDt(ZonedDateTime expirationDt) {
        this.expirationDt = expirationDt;
    }

    public char getBrtRuleCd() {
        return brtRuleCd;
    }

    public void setBrtRuleCd(char brtRuleCd) {
        this.brtRuleCd = brtRuleCd;
    }

    public String getBrtRuleDes() {
        return brtRuleDes;
    }

    public void setBrtRuleDes(String brtRuleDes) {
        this.brtRuleDes = brtRuleDes;
    }

    public String getBrtParmListTxt() {
        return brtParmListTxt;
    }

    public void setBrtParmListTxt(String brtParmListTxt) {
        this.brtParmListTxt = brtParmListTxt;
    }

}
