package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilBankEntryId;

@Entity
@Table(name = "BIL_BANK_ENTRY")
public class BilBankEntry implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilBankEntryId bilBankEntryId;

    @Column(name = "BIL_BANK_ACCT_NBR", length = 25, nullable = false)
    private String bankAccountNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_RTE_TRA_NBR", length = 10, nullable = false)
    private String routingTransitNbr = SEPARATOR_BLANK;

    @Column(name = "BEK_ACT_TYPE_CD", length = 3, nullable = false)
    private String accountTypCd = SEPARATOR_BLANK;

    @Column(name = "BEK_EFT_PAY_LNG_NM", length = 512, nullable = false)
    private String eftPayorLongName = SEPARATOR_BLANK;

    @Column(name = "WEB_AUT_IND", nullable = false)
    private char webAuthInd;

    @Column(name = "CONFIRMATION_NBR", length = 20, nullable = false)
    private String confirmationNbr = SEPARATOR_BLANK;

    public BilBankEntryId getBilBankEntryId() {
        return bilBankEntryId;
    }

    public void setBilBankEntryId(BilBankEntryId bilBankEntryId) {
        this.bilBankEntryId = bilBankEntryId;
    }

    public String getBankAccountNbr() {
        return bankAccountNbr;
    }

    public void setBankAccountNbr(String bankAccountNbr) {
        this.bankAccountNbr = bankAccountNbr;
    }

    public String getRoutingTransitNbr() {
        return routingTransitNbr;
    }

    public void setRoutingTransitNbr(String routingTransitNbr) {
        this.routingTransitNbr = routingTransitNbr;
    }

    public String getAccountTypCd() {
        return accountTypCd;
    }

    public void setAccountTypCd(String accountTypCd) {
        this.accountTypCd = accountTypCd;
    }

    public String getEftPayorLongName() {
        return eftPayorLongName;
    }

    public void setEftPayorLongName(String eftPayorLongName) {
        this.eftPayorLongName = eftPayorLongName;
    }

    public char getWebAuthInd() {
        return webAuthInd;
    }

    public void setWebAuthInd(char webAuthInd) {
        this.webAuthInd = webAuthInd;
    }

    public String getConfirmationNbr() {
        return confirmationNbr;
    }

    public void setConfirmationNbr(String confirmationNbr) {
        this.confirmationNbr = confirmationNbr;
    }

}