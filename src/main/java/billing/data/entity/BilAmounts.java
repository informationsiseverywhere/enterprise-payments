package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilAmountsId;

@Entity
@Table(name = "BIL_AMOUNTS")
public class BilAmounts implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilAmountsId bilAmountsId;

    @Column(name = "BIL_PBL_ITEM_CD", length = 3, nullable = false)
    private String bilPblItemCd = SEPARATOR_BLANK;

    @Column(name = "POL_REA_AMD_CD", length = 3, nullable = true)
    private String polReaAmdCd;

    @Column(name = "BAM_AMT_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bamAmtEffDt;

    @Column(name = "BAM_AMT_EXP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bamAmtExpDt;

    @Column(name = "BIL_AMT_REC_ID", length = 3, nullable = false)
    private String bilAmtRecId = SEPARATOR_BLANK;

    @Column(name = "BAM_AMT_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bamAmtTs;

    @Column(name = "BIL_AMT_EFF_IND", nullable = false)
    private char bilAmtEffInd;

    @Column(name = "BIL_DES_REA_TYP", length = 3, nullable = false)
    private String bilDesReaTyp = SEPARATOR_BLANK;

    @Column(name = "PTF_TAX_FEE_SUR_CD", nullable = true)
    private char ptfTaxFeeSurCd;

    @Column(name = "BIL_FULL_BILL_IND", nullable = false)
    private char bilFullBillInd;

    @Column(name = "BIL_PAY_PTY_CD", nullable = false)
    private char bilPayPtyCd;

    @Column(name = "BAM_AMT", precision = 14, scale = 2, nullable = false)
    private double bamAmt;

    @Column(name = "BIL_COMMISSION_PCT", precision = 9, scale = 6, nullable = false)
    private double bilCommissionPct;

    @Column(name = "BIL_COMMISSION_AMT", precision = 14, scale = 2, nullable = false)
    private double bilCommissionAmt;

    @Column(name = "BAM_AMT_PRC_IND", nullable = false)
    private char bamAmtPrcInd;

    @Column(name = "BAM_COV_SEQ_NBR", length = 10, nullable = true)
    private int bamCovSeqNbr;

    @Column(name = "BAM_COM_PCT_TYPE", nullable = true)
    private char bamComPctType;

    @Column(name = "JURISDICTION_CD", length = 3, nullable = false)
    private String jurisdictionCd = SEPARATOR_BLANK;

    @Column(name = "PTF_TAX_CLASS_CD", length = 5, nullable = false)
    private String ptfTaxClassCd = SEPARATOR_BLANK;

    @Column(name = "BAM_UNIT_NBR", length = 6, nullable = false)
    private String bamUnitNbr = SEPARATOR_BLANK;

    @Column(name = "BAM_SPE_PRC_CD", nullable = false)
    private char bamSpePrcCd;

    public BilAmountsId getBilAmountsId() {
        return bilAmountsId;
    }

    public void setBilAmountsId(BilAmountsId bilAmountsId) {
        this.bilAmountsId = bilAmountsId;
    }

    public String getBilPblItemCd() {
        return bilPblItemCd;
    }

    public void setBilPblItemCd(String bilPblItemCd) {
        this.bilPblItemCd = bilPblItemCd;
    }

    public String getPolReaAmdCd() {
        return polReaAmdCd;
    }

    public void setPolReaAmdCd(String polReaAmdCd) {
        this.polReaAmdCd = polReaAmdCd;
    }

    public ZonedDateTime getBamAmtEffDt() {
        return bamAmtEffDt;
    }

    public void setBamAmtEffDt(ZonedDateTime bamAmtEffDt) {
        this.bamAmtEffDt = bamAmtEffDt;
    }

    public ZonedDateTime getBamAmtExpDt() {
        return bamAmtExpDt;
    }

    public void setBamAmtExpDt(ZonedDateTime bamAmtExpDt) {
        this.bamAmtExpDt = bamAmtExpDt;
    }

    public String getBilAmtRecId() {
        return bilAmtRecId;
    }

    public void setBilAmtRecId(String bilAmtRecId) {
        this.bilAmtRecId = bilAmtRecId;
    }

    public ZonedDateTime getBamAmtTs() {
        return bamAmtTs;
    }

    public void setBamAmtTs(ZonedDateTime bamAmtTs) {
        this.bamAmtTs = bamAmtTs;
    }

    public char getBilAmtEffInd() {
        return bilAmtEffInd;
    }

    public void setBilAmtEffInd(char bilAmtEffInd) {
        this.bilAmtEffInd = bilAmtEffInd;
    }

    public String getBilDesReaTyp() {
        return bilDesReaTyp;
    }

    public void setBilDesReaTyp(String bilDesReaTyp) {
        this.bilDesReaTyp = bilDesReaTyp;
    }

    public char getPtfTaxFeeSurCd() {
        return ptfTaxFeeSurCd;
    }

    public void setPtfTaxFeeSurCd(char ptfTaxFeeSurCd) {
        this.ptfTaxFeeSurCd = ptfTaxFeeSurCd;
    }

    public char getBilFullBillInd() {
        return bilFullBillInd;
    }

    public void setBilFullBillInd(char bilFullBillInd) {
        this.bilFullBillInd = bilFullBillInd;
    }

    public char getBilPayPtyCd() {
        return bilPayPtyCd;
    }

    public void setBilPayPtyCd(char bilPayPtyCd) {
        this.bilPayPtyCd = bilPayPtyCd;
    }

    public double getBamAmt() {
        return bamAmt;
    }

    public void setBamAmt(double bamAmt) {
        this.bamAmt = bamAmt;
    }

    public double getBilCommissionPct() {
        return bilCommissionPct;
    }

    public void setBilCommissionPct(double bilCommissionPct) {
        this.bilCommissionPct = bilCommissionPct;
    }

    public double getBilCommissionAmt() {
        return bilCommissionAmt;
    }

    public void setBilCommissionAmt(double bilCommissionAmt) {
        this.bilCommissionAmt = bilCommissionAmt;
    }

    public char getBamAmtPrcInd() {
        return bamAmtPrcInd;
    }

    public void setBamAmtPrcInd(char bamAmtPrcInd) {
        this.bamAmtPrcInd = bamAmtPrcInd;
    }

    public int getBamCovSeqNbr() {
        return bamCovSeqNbr;
    }

    public void setBamCovSeqNbr(int bamCovSeqNbr) {
        this.bamCovSeqNbr = bamCovSeqNbr;
    }

    public char getBamComPctType() {
        return bamComPctType;
    }

    public void setBamComPctType(char bamComPctType) {
        this.bamComPctType = bamComPctType;
    }

    public String getJurisdictionCd() {
        return jurisdictionCd;
    }

    public void setJurisdictionCd(String jurisdictionCd) {
        this.jurisdictionCd = jurisdictionCd;
    }

    public String getPtfTaxClassCd() {
        return ptfTaxClassCd;
    }

    public void setPtfTaxClassCd(String ptfTaxClassCd) {
        this.ptfTaxClassCd = ptfTaxClassCd;
    }

    public String getBamUnitNbr() {
        return bamUnitNbr;
    }

    public void setBamUnitNbr(String bamUnitNbr) {
        this.bamUnitNbr = bamUnitNbr;
    }

    public char getBamSpePrcCd() {
        return bamSpePrcCd;
    }

    public void setBamSpePrcCd(char bamSpePrcCd) {
        this.bamSpePrcCd = bamSpePrcCd;
    }

}
