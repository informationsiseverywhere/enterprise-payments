package billing.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import billing.data.entity.id.BilRenDaysId;

@Entity
@Table(name = "BIL_REN_DAYS")
public class BilRenDays implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private BilRenDaysId bilRenDaysId;

    @Column(name = "BRD_REN_DAYS", nullable = false)
    private short brdRenDays;

    @Column(name = "BRD_REN_TYPE_CD", nullable = false)
    private char brdRenTypeCd;

    @Column(name = "BRD_REN_PRT_CD", nullable = false)
    private char brdRenPrtCd;

    @Column(name = "BRD_RESC_PRT_CD", nullable = false)
    private char brdRescPrtCd;

    @Column(name = "BRD_PSTMRK_DT_IND", nullable = false)
    private char brdPstmrkDtInd;

    @Column(name = "BRD_PSTMRK_DT_NBR", nullable = false)
    private short brdPstmrkDtNbr;

    @Column(name = "BRD_REN_EXP_DT_IND", nullable = false)
    private char brdRenExpDtInd;

    @Column(name = "BRD_NON_REN_PRT_CD", nullable = false)
    private char brdNonRenPrtCd;

    @Column(name = "BRD_RNP_DAYS_NBR", nullable = false)
    private short brdRnpDaysNbr;

    @Column(name = "BRD_RNP_PST_DT_NBR", nullable = false)
    private short brdRnpPstDtNbr;

    public BilRenDays() {
        super();
    }

    public BilRenDays(BilRenDaysId bilRenDaysId, short brdRenDays, char brdRenTypeCd, char brdRenPrtCd,
            char brdRescPrtCd, char brdPstmrkDtInd, short brdPstmrkDtNbr, char brdRenExpDtInd, char brdNonRenPrtCd,
            short brdRnpDaysNbr, short brdRnpPstDtNbr) {
        super();
        this.bilRenDaysId = bilRenDaysId;
        this.brdRenDays = brdRenDays;
        this.brdRenTypeCd = brdRenTypeCd;
        this.brdRenPrtCd = brdRenPrtCd;
        this.brdRescPrtCd = brdRescPrtCd;
        this.brdPstmrkDtInd = brdPstmrkDtInd;
        this.brdPstmrkDtNbr = brdPstmrkDtNbr;
        this.brdRenExpDtInd = brdRenExpDtInd;
        this.brdNonRenPrtCd = brdNonRenPrtCd;
        this.brdRnpDaysNbr = brdRnpDaysNbr;
        this.brdRnpPstDtNbr = brdRnpPstDtNbr;
    }

    public BilRenDaysId getBilRenDaysId() {
        return bilRenDaysId;
    }

    public void setBilRenDaysId(BilRenDaysId bilRenDaysId) {
        this.bilRenDaysId = bilRenDaysId;
    }

    public short getBrdRenDays() {
        return brdRenDays;
    }

    public void setBrdRenDays(short brdRenDays) {
        this.brdRenDays = brdRenDays;
    }

    public char getBrdRenTypeCd() {
        return brdRenTypeCd;
    }

    public void setBrdRenTypeCd(char brdRenTypeCd) {
        this.brdRenTypeCd = brdRenTypeCd;
    }

    public char getBrdRenPrtCd() {
        return brdRenPrtCd;
    }

    public void setBrdRenPrtCd(char brdRenPrtCd) {
        this.brdRenPrtCd = brdRenPrtCd;
    }

    public char getBrdRescPrtCd() {
        return brdRescPrtCd;
    }

    public void setBrdRescPrtCd(char brdRescPrtCd) {
        this.brdRescPrtCd = brdRescPrtCd;
    }

    public char getBrdPstmrkDtInd() {
        return brdPstmrkDtInd;
    }

    public void setBrdPstmrkDtInd(char brdPstmrkDtInd) {
        this.brdPstmrkDtInd = brdPstmrkDtInd;
    }

    public short getBrdPstmrkDtNbr() {
        return brdPstmrkDtNbr;
    }

    public void setBrdPstmrkDtNbr(short brdPstmrkDtNbr) {
        this.brdPstmrkDtNbr = brdPstmrkDtNbr;
    }

    public char getBrdRenExpDtInd() {
        return brdRenExpDtInd;
    }

    public void setBrdRenExpDtInd(char brdRenExpDtInd) {
        this.brdRenExpDtInd = brdRenExpDtInd;
    }

    public char getBrdNonRenPrtCd() {
        return brdNonRenPrtCd;
    }

    public void setBrdNonRenPrtCd(char brdNonRenPrtCd) {
        this.brdNonRenPrtCd = brdNonRenPrtCd;
    }

    public short getBrdRnpDaysNbr() {
        return brdRnpDaysNbr;
    }

    public void setBrdRnpDaysNbr(short brdRnpDaysNbr) {
        this.brdRnpDaysNbr = brdRnpDaysNbr;
    }

    public short getBrdRnpPstDtNbr() {
        return brdRnpPstDtNbr;
    }

    public void setBrdRnpPstDtNbr(short brdRnpPstDtNbr) {
        this.brdRnpPstDtNbr = brdRnpPstDtNbr;
    }

}
