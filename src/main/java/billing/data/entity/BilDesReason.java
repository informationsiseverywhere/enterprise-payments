package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import billing.data.entity.id.BilDesReasonId;

@Entity
@Table(name = "BIL_DES_REASON")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "bilDesReason")
public class BilDesReason implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilDesReasonId bilDesReasonId;

    @Column(name = "BIL_DES_REA_DES", length = 22, nullable = false)
    private String bilDesReasonDescription = SEPARATOR_BLANK;

    @Column(name = "BDR_LONG_DES", length = 160, nullable = false)
    private String longDescription = SEPARATOR_BLANK;

    @Column(name = "BIL_ACT_BAL_SUM_CD", nullable = false)
    private char accountBalanceSumCd;

    public BilDesReasonId getBilDesReasonId() {
        return bilDesReasonId;
    }

    public void setBilDesReasonId(BilDesReasonId bilDesReasonId) {
        this.bilDesReasonId = bilDesReasonId;
    }

    public String getBilDesReasonDescription() {
        return bilDesReasonDescription;
    }

    public void setBilDesReasonDescription(String bilDesReasonDescription) {
        this.bilDesReasonDescription = bilDesReasonDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public char getAccountBalanceSumCd() {
        return accountBalanceSumCd;
    }

    public void setAccountBalanceSumCd(char accountBalanceSumCd) {
        this.accountBalanceSumCd = accountBalanceSumCd;
    }

}
