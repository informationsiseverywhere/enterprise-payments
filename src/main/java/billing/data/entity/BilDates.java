package billing.data.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilDatesId;

@Entity
@Table(name = "BIL_DATES")
public class BilDates implements Serializable {
    /**
    *
    */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilDatesId bilDatesId;

    @Column(name = "BIL_SYS_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilSysDueDt;

    @Column(name = "BIL_ADJ_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAdjDueDt;

    @Column(name = "BIL_INV_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilInvDt;

    @Column(name = "BDT_DATE_TYPE", nullable = false)
    private char bdtDateType;

    @Column(name = "BDT_DATE_IND", nullable = false)
    private char bdtDateInd;

    @Column(name = "BIL_INVOICE_CD", nullable = false)
    private char bilInvoiceCd;

    @Column(name = "BDT_NONCPI_IND", nullable = false)
    private char bdtNoncpiInd;

    @Column(name = "BDT_LATE_CRG_IND", nullable = false)
    private char bdtLateCrgInd;

    @Column(name = "BDT_FULL_PAY_IND", nullable = false)
    private char bdtFullPayInd;

    @Column(name = "BIL_EXC_IND", nullable = false)
    private char bilExcInd;

    public BilDatesId getBilDatesId() {
        return bilDatesId;
    }

    public void setBilDatesId(BilDatesId bilDatesId) {
        this.bilDatesId = bilDatesId;
    }

    public ZonedDateTime getBilSysDueDt() {
        return bilSysDueDt;
    }

    public void setBilSysDueDt(ZonedDateTime bilSysDueDt) {
        this.bilSysDueDt = bilSysDueDt;
    }

    public ZonedDateTime getBilAdjDueDt() {
        return bilAdjDueDt;
    }

    public void setBilAdjDueDt(ZonedDateTime bilAdjDueDt) {
        this.bilAdjDueDt = bilAdjDueDt;
    }

    public ZonedDateTime getBilInvDt() {
        return bilInvDt;
    }

    public void setBilInvDt(ZonedDateTime bilInvDt) {
        this.bilInvDt = bilInvDt;
    }

    public char getBdtDateType() {
        return bdtDateType;
    }

    public void setBdtDateType(char bdtDateType) {
        this.bdtDateType = bdtDateType;
    }

    public char getBdtDateInd() {
        return bdtDateInd;
    }

    public void setBdtDateInd(char bdtDateInd) {
        this.bdtDateInd = bdtDateInd;
    }

    public char getBilInvoiceCd() {
        return bilInvoiceCd;
    }

    public void setBilInvoiceCd(char bilInvoiceCd) {
        this.bilInvoiceCd = bilInvoiceCd;
    }

    public char getBdtNoncpiInd() {
        return bdtNoncpiInd;
    }

    public void setBdtNoncpiInd(char bdtNoncpiInd) {
        this.bdtNoncpiInd = bdtNoncpiInd;
    }

    public char getBdtLateCrgInd() {
        return bdtLateCrgInd;
    }

    public void setBdtLateCrgInd(char bdtLateCrgInd) {
        this.bdtLateCrgInd = bdtLateCrgInd;
    }

    public char getBdtFullPayInd() {
        return bdtFullPayInd;
    }

    public void setBdtFullPayInd(char bdtFullPayInd) {
        this.bdtFullPayInd = bdtFullPayInd;
    }

    public char getBilExcInd() {
        return bilExcInd;
    }

    public void setBilExcInd(char bilExcInd) {
        this.bilExcInd = bilExcInd;
    }

}
