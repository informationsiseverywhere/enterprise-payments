package billing.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilActRulesId;

@Entity
@Table(name = "BIL_ACT_RULES")
public class BilActRules implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilActRulesId bilActRulesId;

    @Column(name = "BRU_NSF_CNC_IND", nullable = false)
    private char bruNsfCncInd;

    @Column(name = "BRU_DSB_RCT_DT_IND", nullable = false)
    private char bruDsbRctDtInd;

    @Column(name = "BRU_DSB_CK_DAYS", length = 5, nullable = false)
    private short bruDsbCkDays;

    @Column(name = "BRU_DSB_EFT_DAYS", length = 5, nullable = false)
    private short bruDsbEftDays;

    @Column(name = "BRU_DSB_CRCRD_DAYS", length = 5, nullable = false)
    private short bruDsbCrcrdDays;

    @Column(name = "BRU_MIN_DSB_AMT", precision = 14, scale = 2, nullable = false)
    private double bruMinDsbAmt;

    @Column(name = "BRU_MAX_DSB_AMT", precision = 14, scale = 2, nullable = false)
    private double bruMaxDsbAmt;

    @Column(name = "BRU_RNL_BILL_IND", nullable = false)
    private char bruRnlBillInd;

    @Column(name = "BRU_REN_BILL_IND", nullable = false)
    private char bruRenBillInd;

    @Column(name = "BRU_RSS_BILL_IND", nullable = false)
    private char bruRssBillInd;

    @Column(name = "BRU_RNL_QTE_IND", nullable = false)
    private char bruRnlQteInd;

    @Column(name = "BRU_RNL_IF_DAYS", length = 5, nullable = false)
    private short bruRnlIfDays;

    @Column(name = "BRU_RNL_IF_WIP_IND", nullable = false)
    private char bruRnlIfWipInd;

    @Column(name = "BRU_RNL_IF_STAT_CD", nullable = false)
    private char bruRnlIfStatCd;

    @Column(name = "BRU_FPDD_IND", nullable = false)
    private char bruFpddInd;

    @Column(name = "BRU_MIN_WRO_IND", nullable = false)
    private char bruMinWroInd;

    @Column(name = "BRU_MIN_WRO_NBR", length = 5, nullable = false)
    private short bruMinWroNbr;

    @Column(name = "BRU_CCR_CREDIT_IND", nullable = false)
    private char bruCcrCreditInd;

    @Column(name = "BRU_CCR_PART_IND", nullable = false)
    private char bruCcrPartInd;

    @Column(name = "BRU_CCR_NBR_DAYS", length = 5, nullable = false)
    private short bruCcrNbrDays;

    @Column(name = "BRU_WRO_LTE_IND", nullable = false)
    private char bruWroLteInd;

    @Column(name = "BRU_PSTMRK_REQ_IND", nullable = false)
    private char bruPstmrkReqInd;

    @Column(name = "BRU_INC_WKN_IND", nullable = false)
    private char bruIncWknInd;

    @Column(name = "BRU_DSB_PACS_DAYS", length = 5, nullable = false)
    private short bruDsbPacsDays;

    @Column(name = "BRU_FLP_SCG_WRO_CD", nullable = false)
    private char bruFlpScgWroCd;

    public BilActRulesId getBilActRulesId() {
        return bilActRulesId;
    }

    public void setBilActRulesId(BilActRulesId bilActRulesId) {
        this.bilActRulesId = bilActRulesId;
    }

    public char getBruNsfCncInd() {
        return bruNsfCncInd;
    }

    public void setBruNsfCncInd(char bruNsfCncInd) {
        this.bruNsfCncInd = bruNsfCncInd;
    }

    public char getBruDsbRctDtInd() {
        return bruDsbRctDtInd;
    }

    public void setBruDsbRctDtInd(char bruDsbRctDtInd) {
        this.bruDsbRctDtInd = bruDsbRctDtInd;
    }

    public short getBruDsbCkDays() {
        return bruDsbCkDays;
    }

    public void setBruDsbCkDays(short bruDsbCkDays) {
        this.bruDsbCkDays = bruDsbCkDays;
    }

    public short getBruDsbEftDays() {
        return bruDsbEftDays;
    }

    public void setBruDsbEftDays(short bruDsbEftDays) {
        this.bruDsbEftDays = bruDsbEftDays;
    }

    public short getBruDsbCrcrdDays() {
        return bruDsbCrcrdDays;
    }

    public void setBruDsbCrcrdDays(short bruDsbCrcrdDays) {
        this.bruDsbCrcrdDays = bruDsbCrcrdDays;
    }

    public double getBruMinDsbAmt() {
        return bruMinDsbAmt;
    }

    public void setBruMinDsbAmt(double bruMinDsbAmt) {
        this.bruMinDsbAmt = bruMinDsbAmt;
    }

    public double getBruMaxDsbAmt() {
        return bruMaxDsbAmt;
    }

    public void setBruMaxDsbAmt(double bruMaxDsbAmt) {
        this.bruMaxDsbAmt = bruMaxDsbAmt;
    }

    public char getBruRnlBillInd() {
        return bruRnlBillInd;
    }

    public void setBruRnlBillInd(char bruRnlBillInd) {
        this.bruRnlBillInd = bruRnlBillInd;
    }

    public char getBruRenBillInd() {
        return bruRenBillInd;
    }

    public void setBruRenBillInd(char bruRenBillInd) {
        this.bruRenBillInd = bruRenBillInd;
    }

    public char getBruRssBillInd() {
        return bruRssBillInd;
    }

    public void setBruRssBillInd(char bruRssBillInd) {
        this.bruRssBillInd = bruRssBillInd;
    }

    public char getBruRnlQteInd() {
        return bruRnlQteInd;
    }

    public void setBruRnlQteInd(char bruRnlQteInd) {
        this.bruRnlQteInd = bruRnlQteInd;
    }

    public short getBruRnlIfDays() {
        return bruRnlIfDays;
    }

    public void setBruRnlIfDays(short bruRnlIfDays) {
        this.bruRnlIfDays = bruRnlIfDays;
    }

    public char getBruRnlIfWipInd() {
        return bruRnlIfWipInd;
    }

    public void setBruRnlIfWipInd(char bruRnlIfWipInd) {
        this.bruRnlIfWipInd = bruRnlIfWipInd;
    }

    public char getBruRnlIfStatCd() {
        return bruRnlIfStatCd;
    }

    public void setBruRnlIfStatCd(char bruRnlIfStatCd) {
        this.bruRnlIfStatCd = bruRnlIfStatCd;
    }

    public char getBruFpddInd() {
        return bruFpddInd;
    }

    public void setBruFpddInd(char bruFpddInd) {
        this.bruFpddInd = bruFpddInd;
    }

    public char getBruMinWroInd() {
        return bruMinWroInd;
    }

    public void setBruMinWroInd(char bruMinWroInd) {
        this.bruMinWroInd = bruMinWroInd;
    }

    public short getBruMinWroNbr() {
        return bruMinWroNbr;
    }

    public void setBruMinWroNbr(short bruMinWroNbr) {
        this.bruMinWroNbr = bruMinWroNbr;
    }

    public char getBruCcrCreditInd() {
        return bruCcrCreditInd;
    }

    public void setBruCcrCreditInd(char bruCcrCreditInd) {
        this.bruCcrCreditInd = bruCcrCreditInd;
    }

    public char getBruCcrPartInd() {
        return bruCcrPartInd;
    }

    public void setBruCcrPartInd(char bruCcrPartInd) {
        this.bruCcrPartInd = bruCcrPartInd;
    }

    public short getBruCcrNbrDays() {
        return bruCcrNbrDays;
    }

    public void setBruCcrNbrDays(short bruCcrNbrDays) {
        this.bruCcrNbrDays = bruCcrNbrDays;
    }

    public char getBruWroLteInd() {
        return bruWroLteInd;
    }

    public void setBruWroLteInd(char bruWroLteInd) {
        this.bruWroLteInd = bruWroLteInd;
    }

    public char getBruPstmrkReqInd() {
        return bruPstmrkReqInd;
    }

    public void setBruPstmrkReqInd(char bruPstmrkReqInd) {
        this.bruPstmrkReqInd = bruPstmrkReqInd;
    }

    public char getBruIncWknInd() {
        return bruIncWknInd;
    }

    public void setBruIncWknInd(char bruIncWknInd) {
        this.bruIncWknInd = bruIncWknInd;
    }

    public short getBruDsbPacsDays() {
        return bruDsbPacsDays;
    }

    public void setBruDsbPacsDays(short bruDsbPacsDays) {
        this.bruDsbPacsDays = bruDsbPacsDays;
    }

    public char getBruFlpScgWroCd() {
        return bruFlpScgWroCd;
    }

    public void setBruFlpScgWroCd(char bruFlpScgWroCd) {
        this.bruFlpScgWroCd = bruFlpScgWroCd;
    }

}
