package billing.data.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilSystemCfgId;

@Entity
@Table(name = "BIL_SYSTEM_CFG")
public class BilSystemCfg implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilSystemCfgId bilSystemCfgId;

    public BilSystemCfgId getBilSystemCfgId() {
        return bilSystemCfgId;
    }

    public void setBilSystemCfgId(BilSystemCfgId bilSystemCfgId) {
        this.bilSystemCfgId = bilSystemCfgId;
    }
}
