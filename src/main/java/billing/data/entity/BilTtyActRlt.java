package billing.data.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilTtyActRltId;

@Entity
@Table(name = "BIL_TTY_ACT_RLT")
public class BilTtyActRlt implements Serializable {

    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilTtyActRltId bilTtyActRltId;

    public BilTtyActRltId getBilTtyActRltId() {
        return bilTtyActRltId;
    }

    public void setBilTtyActRltId(BilTtyActRltId bilTtyActRltId) {
        this.bilTtyActRltId = bilTtyActRltId;
    }

}
