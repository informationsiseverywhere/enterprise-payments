package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilEftExtPytId;


@Entity
@Table(name = "BIL_EFT_EXT_PYT")
public class BilEftExtPyt implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilEftExtPytId bilEftExtPytId;

    @Column(name = "BIL_WAL_TYPE_CD", length = 2, nullable = false)
    private String bilWalTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_WAL_IDENTIFIER", length = 512, nullable = false)
    private String bilWalIdentifier = SEPARATOR_BLANK;

    public BilEftExtPyt() {
        super();
    }

    public BilEftExtPyt(BilEftExtPytId bilEftExtPytId, String bilWalTypeCd, String bilWalIdentifier) {
        super();
        this.bilEftExtPytId = bilEftExtPytId;
        this.bilWalTypeCd = bilWalTypeCd;
        this.bilWalIdentifier = bilWalIdentifier;
    }

    public BilEftExtPytId getBilEftExtPytId() {
        return bilEftExtPytId;
    }

    public void setBilEftExtPytId(BilEftExtPytId bilEftExtPytId) {
        this.bilEftExtPytId = bilEftExtPytId;
    }

    public String getBilWalTypeCd() {
        return bilWalTypeCd;
    }

    public void setBilWalTypeCd(String bilWalTypeCd) {
        this.bilWalTypeCd = bilWalTypeCd;
    }

    public String getBilWalIdentifier() {
        return bilWalIdentifier;
    }

    public void setBilWalIdentifier(String bilWalIdentifier) {
        this.bilWalIdentifier = bilWalIdentifier;
    }

}
