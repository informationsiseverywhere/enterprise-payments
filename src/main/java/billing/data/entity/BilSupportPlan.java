package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.INVALID_INT_VAL;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilSupportPlanId;

@Entity
@Table(name = "BIL_SUPPORT_PLAN")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "bilSupportPlan")
public class BilSupportPlan implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilSupportPlanId bilSupportPlanId;

    @Column(name = "BIL_RVW_FQY_CD", length = 2, nullable = false)
    private String bilRvwFqyCd = SEPARATOR_BLANK;

    @Column(name = "BSP_MIN_INV_AMT", precision = 14, scale = 2, nullable = false)
    private double bspMinInvAmt;

    @Column(name = "BIL_DUE_DT_DPL_NBR", length = 5, nullable = false)
    private short bilDueDtDplNbr;

    @Column(name = "BSP_PND_CNC_NBR", length = 5, nullable = false)
    private short bspPndCncNbr;

    @Column(name = "BSP_SCG_AMT", precision = 14, scale = 2, nullable = false)
    private double bspScgAmt;

    @Column(name = "BSP_LATE_CRG_NBR", length = 5, nullable = false)
    private short bspLateCrgNbr;

    @Column(name = "BSP_LATE_CRG_AMT", precision = 14, scale = 2, nullable = false)
    private double bspLateCrgAmt;

    @Column(name = "BSP_LATE_CRG_PCT", precision = 14, scale = 2, nullable = false)
    private double bspLateCrgPct;

    @Column(name = "BSP_PNY_AMT", precision = 14, scale = 2, nullable = false)
    private double bspPnyAmt;

    @Column(name = "BSP_NONCPI_LET_IND", nullable = false)
    private char bspNoncpiLetInd;

    @Column(name = "BSP_NONCPI_LET_NBR", length = 5, nullable = false)
    private short bspNoncpiLetNbr;

    @Column(name = "BSP_NONCPI_LET_PCT", precision = 5, scale = 2, nullable = false)
    private double bspNoncpiLetPct;

    @Column(name = "BSP_NONCPI_LET_AMT", precision = 14, scale = 2, nullable = false)
    private double bspNoncpiLetAmt;

    @Column(name = "BSP_2ND_NOT_IND", nullable = false)
    private char bsp2ndNotInd;

    @Column(name = "BSP_2ND_NOT_NBR", length = 5, nullable = false)
    private short bsp2ndNotNbr;

    @Column(name = "BSP_2ND_NOT_PCT", precision = 5, scale = 2, nullable = false)
    private double bsp2ndNotPct;

    @Column(name = "BSP_2ND_NOT_AMT", precision = 14, scale = 2, nullable = false)
    private double bsp2ndNotAmt;

    @Column(name = "BSP_OCB_IND", nullable = false)
    private char bspOcbInd;

    @Column(name = "BSP_OCB_BFR_DAYS", length = 5, nullable = false)
    private short bspOcbBfrDays;

    @Column(name = "BSP_OCB_AFT_DAYS", length = 5, nullable = false)
    private short bspOcbAftDays;

    @Column(name = "BSP_OCB_TOL_AMT", precision = 14, scale = 2, nullable = false)
    private double bspOcbTolAmt;

    @Column(name = "BSP_PMT_PTY_IND", nullable = false)
    private char bspPmtPtyInd;

    @Column(name = "BSP_CSH_OVP_IND", nullable = false)
    private char bspCshOvpInd;

    @Column(name = "BSP_OVP_MIN_DSB", precision = 14, scale = 2, nullable = false)
    private double bspOvpMinDsb;

    @Column(name = "BSP_OVP_MAX_DSB", precision = 14, scale = 2, nullable = false)
    private double bspOvpMaxDsb;

    @Column(name = "BSP_OVP_HOLD_NBR", length = 5, nullable = false)
    private short bspOvpHoldNbr;

    @Column(name = "BSP_UPY_WRO_AMT", precision = 14, scale = 2, nullable = false)
    private double bspUpyWroAmt;

    @Column(name = "BSP_OVP_WRO_AMT", precision = 14, scale = 2, nullable = false)
    private double bspOvpWroAmt;

    @Column(name = "BSP_AGT_PAY_DUE", length = 5, nullable = false)
    private short bspAgtPayDue;

    @Column(name = "BSP_AGT_RET_DUE", length = 5, nullable = false)
    private short bspAgtRetDue;

    @Column(name = "BSP_AGT_MCH_ID_CD", nullable = false)
    private char bspAgtMchIdCd;

    @Column(name = "BSP_AGT_MCH_TOL", precision = 14, scale = 2, nullable = false)
    private double bspAgtMchTol;

    @Column(name = "BSP_AGT_DTL_MCH", precision = 14, scale = 2, nullable = false)
    private double bspAgtDtlMch;

    @Column(name = "BSP_STT_RM_IND", nullable = false)
    private char bspSttRmInd;

    @Column(name = "BSP_STT_RM_DAY", length = 5, nullable = false)
    private short bspSttRmDay;

    @Column(name = "BSP_STT_PAY_RM_IND", nullable = false)
    private char bspSttPayRmInd;

    @Column(name = "BSP_STT_PAY_RM_DAY", length = 5, nullable = false)
    private short bspSttPayRmDay;

    @Column(name = "BSP_SCG_CHG_IND", nullable = false)
    private char bspScgChgInd;

    @Column(name = "BSP_SCGCHG_TYP_CD", length = 2, nullable = false)
    private String bspScgchgTypCd = SEPARATOR_BLANK;

    @Column(name = "BSP_PRM_AMT", precision = 14, scale = 2, nullable = false)
    private double bspPrmAmt;

    @Column(name = "BSP_SCG_INC_AMT", precision = 14, scale = 2, nullable = false)
    private double bspScgIncAmt;

    @Column(name = "BSP_PRM_INC_AMT", precision = 14, scale = 2, nullable = false)
    private double bspPrmIncAmt;

    @Column(name = "BSP_MAX_SCG_AMT", precision = 14, scale = 2, nullable = false)
    private double bspMaxScgAmt;

    @Column(name = "BSP_SVC_CD", nullable = false)
    private char bspSvcCd;

    @Column(name = "BSP_PND_CNC_CD", nullable = false)
    private char bspPndCncCd;

    @Column(name = "BIL_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEffDt;

    @Column(name = "BIL_EXP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilExpDt;

    @Column(name = "BIL_NEXT_CLASS_CD", length = 3, nullable = false)
    private String bilNextClassCd = SEPARATOR_BLANK;

    @Column(name = "BSP_DED_IND", nullable = false)
    private char bspDedInd;

    @Column(name = "BSP_DED_DT_DPL_NBR", length = 5, nullable = false)
    private short bspDedDtDplNbr;

    @Column(name = "BIL_DLQ_IND", nullable = false)
    private char bilDlqInd = BLANK_CHAR;

    @Column(name = "BIL_DLQ_NBR", length = 5, nullable = false)
    private int bilDlqNbr = INVALID_INT_VAL;

    @Column(name = "BIL_DLQ_PCT", precision = 14, scale = 2, nullable = false)
    private double bilDlqPct;

    public BilSupportPlanId getBilSupportPlanId() {
        return bilSupportPlanId;
    }

    public void setBilSupportPlanId(BilSupportPlanId bilSupportPlanId) {
        this.bilSupportPlanId = bilSupportPlanId;
    }

    public String getBilRvwFqyCd() {
        return bilRvwFqyCd;
    }

    public void setBilRvwFqyCd(String bilRvwFqyCd) {
        this.bilRvwFqyCd = bilRvwFqyCd;
    }

    public double getBspMinInvAmt() {
        return bspMinInvAmt;
    }

    public void setBspMinInvAmt(double bspMinInvAmt) {
        this.bspMinInvAmt = bspMinInvAmt;
    }

    public short getBilDueDtDplNbr() {
        return bilDueDtDplNbr;
    }

    public void setBilDueDtDplNbr(short bilDueDtDplNbr) {
        this.bilDueDtDplNbr = bilDueDtDplNbr;
    }

    public short getBspPndCncNbr() {
        return bspPndCncNbr;
    }

    public void setBspPndCncNbr(short bspPndCncNbr) {
        this.bspPndCncNbr = bspPndCncNbr;
    }

    public double getBspScgAmt() {
        return bspScgAmt;
    }

    public void setBspScgAmt(double bspScgAmt) {
        this.bspScgAmt = bspScgAmt;
    }

    public short getBspLateCrgNbr() {
        return bspLateCrgNbr;
    }

    public void setBspLateCrgNbr(short bspLateCrgNbr) {
        this.bspLateCrgNbr = bspLateCrgNbr;
    }

    public double getBspLateCrgAmt() {
        return bspLateCrgAmt;
    }

    public void setBspLateCrgAmt(double bspLateCrgAmt) {
        this.bspLateCrgAmt = bspLateCrgAmt;
    }

    public double getBspLateCrgPct() {
        return bspLateCrgPct;
    }

    public void setBspLateCrgPct(double bspLateCrgPct) {
        this.bspLateCrgPct = bspLateCrgPct;
    }

    public double getBspPnyAmt() {
        return bspPnyAmt;
    }

    public void setBspPnyAmt(double bspPnyAmt) {
        this.bspPnyAmt = bspPnyAmt;
    }

    public char getBspNoncpiLetInd() {
        return bspNoncpiLetInd;
    }

    public void setBspNoncpiLetInd(char bspNoncpiLetInd) {
        this.bspNoncpiLetInd = bspNoncpiLetInd;
    }

    public short getBspNoncpiLetNbr() {
        return bspNoncpiLetNbr;
    }

    public void setBspNoncpiLetNbr(short bspNoncpiLetNbr) {
        this.bspNoncpiLetNbr = bspNoncpiLetNbr;
    }

    public double getBspNoncpiLetPct() {
        return bspNoncpiLetPct;
    }

    public void setBspNoncpiLetPct(double bspNoncpiLetPct) {
        this.bspNoncpiLetPct = bspNoncpiLetPct;
    }

    public double getBspNoncpiLetAmt() {
        return bspNoncpiLetAmt;
    }

    public void setBspNoncpiLetAmt(double bspNoncpiLetAmt) {
        this.bspNoncpiLetAmt = bspNoncpiLetAmt;
    }

    public char getBsp2ndNotInd() {
        return bsp2ndNotInd;
    }

    public void setBsp2ndNotInd(char bsp2ndNotInd) {
        this.bsp2ndNotInd = bsp2ndNotInd;
    }

    public short getBsp2ndNotNbr() {
        return bsp2ndNotNbr;
    }

    public void setBsp2ndNotNbr(short bsp2ndNotNbr) {
        this.bsp2ndNotNbr = bsp2ndNotNbr;
    }

    public double getBsp2ndNotPct() {
        return bsp2ndNotPct;
    }

    public void setBsp2ndNotPct(double bsp2ndNotPct) {
        this.bsp2ndNotPct = bsp2ndNotPct;
    }

    public double getBsp2ndNotAmt() {
        return bsp2ndNotAmt;
    }

    public void setBsp2ndNotAmt(double bsp2ndNotAmt) {
        this.bsp2ndNotAmt = bsp2ndNotAmt;
    }

    public char getBspOcbInd() {
        return bspOcbInd;
    }

    public void setBspOcbInd(char bspOcbInd) {
        this.bspOcbInd = bspOcbInd;
    }

    public short getBspOcbBfrDays() {
        return bspOcbBfrDays;
    }

    public void setBspOcbBfrDays(short bspOcbBfrDays) {
        this.bspOcbBfrDays = bspOcbBfrDays;
    }

    public short getBspOcbAftDays() {
        return bspOcbAftDays;
    }

    public void setBspOcbAftDays(short bspOcbAftDays) {
        this.bspOcbAftDays = bspOcbAftDays;
    }

    public double getBspOcbTolAmt() {
        return bspOcbTolAmt;
    }

    public void setBspOcbTolAmt(double bspOcbTolAmt) {
        this.bspOcbTolAmt = bspOcbTolAmt;
    }

    public char getBspPmtPtyInd() {
        return bspPmtPtyInd;
    }

    public void setBspPmtPtyInd(char bspPmtPtyInd) {
        this.bspPmtPtyInd = bspPmtPtyInd;
    }

    public char getBspCshOvpInd() {
        return bspCshOvpInd;
    }

    public void setBspCshOvpInd(char bspCshOvpInd) {
        this.bspCshOvpInd = bspCshOvpInd;
    }

    public double getBspOvpMinDsb() {
        return bspOvpMinDsb;
    }

    public void setBspOvpMinDsb(double bspOvpMinDsb) {
        this.bspOvpMinDsb = bspOvpMinDsb;
    }

    public double getBspOvpMaxDsb() {
        return bspOvpMaxDsb;
    }

    public void setBspOvpMaxDsb(double bspOvpMaxDsb) {
        this.bspOvpMaxDsb = bspOvpMaxDsb;
    }

    public short getBspOvpHoldNbr() {
        return bspOvpHoldNbr;
    }

    public void setBspOvpHoldNbr(short bspOvpHoldNbr) {
        this.bspOvpHoldNbr = bspOvpHoldNbr;
    }

    public double getBspUpyWroAmt() {
        return bspUpyWroAmt;
    }

    public void setBspUpyWroAmt(double bspUpyWroAmt) {
        this.bspUpyWroAmt = bspUpyWroAmt;
    }

    public double getBspOvpWroAmt() {
        return bspOvpWroAmt;
    }

    public void setBspOvpWroAmt(double bspOvpWroAmt) {
        this.bspOvpWroAmt = bspOvpWroAmt;
    }

    public short getBspAgtPayDue() {
        return bspAgtPayDue;
    }

    public void setBspAgtPayDue(short bspAgtPayDue) {
        this.bspAgtPayDue = bspAgtPayDue;
    }

    public short getBspAgtRetDue() {
        return bspAgtRetDue;
    }

    public void setBspAgtRetDue(short bspAgtRetDue) {
        this.bspAgtRetDue = bspAgtRetDue;
    }

    public char getBspAgtMchIdCd() {
        return bspAgtMchIdCd;
    }

    public void setBspAgtMchIdCd(char bspAgtMchIdCd) {
        this.bspAgtMchIdCd = bspAgtMchIdCd;
    }

    public double getBspAgtMchTol() {
        return bspAgtMchTol;
    }

    public void setBspAgtMchTol(double bspAgtMchTol) {
        this.bspAgtMchTol = bspAgtMchTol;
    }

    public double getBspAgtDtlMch() {
        return bspAgtDtlMch;
    }

    public void setBspAgtDtlMch(double bspAgtDtlMch) {
        this.bspAgtDtlMch = bspAgtDtlMch;
    }

    public char getBspSttRmInd() {
        return bspSttRmInd;
    }

    public void setBspSttRmInd(char bspSttRmInd) {
        this.bspSttRmInd = bspSttRmInd;
    }

    public short getBspSttRmDay() {
        return bspSttRmDay;
    }

    public void setBspSttRmDay(short bspSttRmDay) {
        this.bspSttRmDay = bspSttRmDay;
    }

    public char getBspSttPayRmInd() {
        return bspSttPayRmInd;
    }

    public void setBspSttPayRmInd(char bspSttPayRmInd) {
        this.bspSttPayRmInd = bspSttPayRmInd;
    }

    public short getBspSttPayRmDay() {
        return bspSttPayRmDay;
    }

    public void setBspSttPayRmDay(short bspSttPayRmDay) {
        this.bspSttPayRmDay = bspSttPayRmDay;
    }

    public char getBspScgChgInd() {
        return bspScgChgInd;
    }

    public void setBspScgChgInd(char bspScgChgInd) {
        this.bspScgChgInd = bspScgChgInd;
    }

    public String getBspScgchgTypCd() {
        return bspScgchgTypCd;
    }

    public void setBspScgchgTypCd(String bspScgchgTypCd) {
        this.bspScgchgTypCd = bspScgchgTypCd;
    }

    public double getBspPrmAmt() {
        return bspPrmAmt;
    }

    public void setBspPrmAmt(double bspPrmAmt) {
        this.bspPrmAmt = bspPrmAmt;
    }

    public double getBspScgIncAmt() {
        return bspScgIncAmt;
    }

    public void setBspScgIncAmt(double bspScgIncAmt) {
        this.bspScgIncAmt = bspScgIncAmt;
    }

    public double getBspPrmIncAmt() {
        return bspPrmIncAmt;
    }

    public void setBspPrmIncAmt(double bspPrmIncAmt) {
        this.bspPrmIncAmt = bspPrmIncAmt;
    }

    public double getBspMaxScgAmt() {
        return bspMaxScgAmt;
    }

    public void setBspMaxScgAmt(double bspMaxScgAmt) {
        this.bspMaxScgAmt = bspMaxScgAmt;
    }

    public char getBspSvcCd() {
        return bspSvcCd;
    }

    public void setBspSvcCd(char bspSvcCd) {
        this.bspSvcCd = bspSvcCd;
    }

    public char getBspPndCncCd() {
        return bspPndCncCd;
    }

    public void setBspPndCncCd(char bspPndCncCd) {
        this.bspPndCncCd = bspPndCncCd;
    }

    public ZonedDateTime getBilEffDt() {
        return bilEffDt;
    }

    public void setBilEffDt(ZonedDateTime bilEffDt) {
        this.bilEffDt = bilEffDt;
    }

    public ZonedDateTime getBilExpDt() {
        return bilExpDt;
    }

    public void setBilExpDt(ZonedDateTime bilExpDt) {
        this.bilExpDt = bilExpDt;
    }

    public String getBilNextClassCd() {
        return bilNextClassCd;
    }

    public void setBilNextClassCd(String bilNextClassCd) {
        this.bilNextClassCd = bilNextClassCd;
    }

    public char getBspDedInd() {
        return bspDedInd;
    }

    public void setBspDedInd(char bspDedInd) {
        this.bspDedInd = bspDedInd;
    }

    public short getBspDedDtDplNbr() {
        return bspDedDtDplNbr;
    }

    public void setBspDedDtDplNbr(short bspDedDtDplNbr) {
        this.bspDedDtDplNbr = bspDedDtDplNbr;
    }

    public char getBilDlqInd() {
        return bilDlqInd;
    }

    public void setBilDlqInd(char bilDlqInd) {
        this.bilDlqInd = bilDlqInd;
    }

    public int getBilDlqNbr() {
        return bilDlqNbr;
    }

    public void setBilDlqNbr(int bilDlqNbr) {
        this.bilDlqNbr = bilDlqNbr;
    }

    public double getBilDlqPct() {
        return bilDlqPct;
    }

    public void setBilDlqPct(double bilDlqPct) {
        this.bilDlqPct = bilDlqPct;
    }

}
