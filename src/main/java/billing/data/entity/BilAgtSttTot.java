package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilAgtSttTotId;

@Entity
@Table(name = "BIL_AGT_STT_TOT")
public class BilAgtSttTot implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilAgtSttTotId bilAgtSttTotId;

    @Column(name = "BST_ACCEPT_IND", nullable = false)
    private char bstAcceptInd = BLANK_CHAR;

    @Column(name = "BST_STT_REC_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bstSttRecDt;

    @Column(name = "BST_STT_TYPE_CD", nullable = false)
    private char bstSttTypeCd = BLANK_CHAR;

    @Column(name = "BIL_STT_GROSS_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttGrossAmt = DECIMAL_ZERO;

    @Column(name = "BIL_STT_NET_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttNetAmt = DECIMAL_ZERO;

    @Column(name = "BIL_STT_COM_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttComAmt = DECIMAL_ZERO;

    @Column(name = "BST_STT_WRO_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttWroAmt = DECIMAL_ZERO;

    @Column(name = "BIL_STT_PAID_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttPaidAmt = DECIMAL_ZERO;

    @Column(name = "BST_BAL_WRO_REA_CD", length = 3, nullable = false)
    private String bstBalWroReaCd = SEPARATOR_BLANK;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "BIL_COM_PAID_AMT", precision = 14, scale = 2, nullable = false)
    private double bilComPaidAmt = DECIMAL_ZERO;

    public char getBstAcceptInd() {
        return bstAcceptInd;
    }

    public void setBstAcceptInd(char bstAcceptInd) {
        this.bstAcceptInd = bstAcceptInd;
    }

    public ZonedDateTime getBstSttRecDt() {
        return bstSttRecDt;
    }

    public void setBstSttRecDt(ZonedDateTime bstSttRecDt) {
        this.bstSttRecDt = bstSttRecDt;
    }

    public char getBstSttTypeCd() {
        return bstSttTypeCd;
    }

    public void setBstSttTypeCd(char bstSttTypeCd) {
        this.bstSttTypeCd = bstSttTypeCd;
    }

    public double getBilSttGrossAmt() {
        return bilSttGrossAmt;
    }

    public void setBilSttGrossAmt(double bilSttGrossAmt) {
        this.bilSttGrossAmt = bilSttGrossAmt;
    }

    public double getBilSttNetAmt() {
        return bilSttNetAmt;
    }

    public void setBilSttNetAmt(double bilSttNetAmt) {
        this.bilSttNetAmt = bilSttNetAmt;
    }

    public double getBilSttComAmt() {
        return bilSttComAmt;
    }

    public void setBilSttComAmt(double bilSttComAmt) {
        this.bilSttComAmt = bilSttComAmt;
    }

    public double getBilSttWroAmt() {
        return bilSttWroAmt;
    }

    public void setBilSttWroAmt(double bilSttWroAmt) {
        this.bilSttWroAmt = bilSttWroAmt;
    }

    public double getBilSttPaidAmt() {
        return bilSttPaidAmt;
    }

    public void setBilSttPaidAmt(double bilSttPaidAmt) {
        this.bilSttPaidAmt = bilSttPaidAmt;
    }

    public String getBstBalWroReaCd() {
        return bstBalWroReaCd;
    }

    public void setBstBalWroReaCd(String bstBalWroReaCd) {
        this.bstBalWroReaCd = bstBalWroReaCd;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getBilComPaidAmt() {
        return bilComPaidAmt;
    }

    public void setBilComPaidAmt(double bilComPaidAmt) {
        this.bilComPaidAmt = bilComPaidAmt;
    }

}
