package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilBankRltId;

@Entity
@Table(name = "BIL_BANK_RLT")
public class BilBankRlt implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilBankRltId bilBankRltId;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNbr = SEPARATOR_BLANK;

    @Column(name = "BBR_PARENT_BANK_CD", length = 8, nullable = false)
    private String bbrParentBankCd = SEPARATOR_BLANK;

    public BilBankRltId getBilBankRltId() {
        return bilBankRltId;
    }

    public void setBilBankRltId(BilBankRltId bilBankRltId) {
        this.bilBankRltId = bilBankRltId;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public String getBbrParentBankCd() {
        return bbrParentBankCd;
    }

    public void setBbrParentBankCd(String bbrParentBankCd) {
        this.bbrParentBankCd = bbrParentBankCd;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
