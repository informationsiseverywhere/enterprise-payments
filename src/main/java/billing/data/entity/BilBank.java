package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BIL_BANK")
public class BilBank implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "BIL_BANK_CD", length = 3, nullable = false)
    private String bilBankCd = SEPARATOR_BLANK;

    @Column(name = "BBK_ACCOUNT_NM", length = 40, nullable = false)
    private String bbkAccountNm = SEPARATOR_BLANK;

    @Column(name = "BBK_ACCOUNT_NBR", length = 25, nullable = false)
    private String bbkAccountNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_RTE_TRA_NBR", length = 10, nullable = false)
    private String bilRteTraNbr = SEPARATOR_BLANK;

    @Column(name = "BBK_NO_DISPLAY_IND", nullable = false)
    private char bbkNoDisplayInd;

    public String getBilBankCd() {
        return bilBankCd;
    }

    public void setBilBankCd(String bilBankCd) {
        this.bilBankCd = bilBankCd;
    }

    public String getBbkAccountNm() {
        return bbkAccountNm;
    }

    public void setBbkAccountNm(String bbkAccountNm) {
        this.bbkAccountNm = bbkAccountNm;
    }

    public String getBbkAccountNbr() {
        return bbkAccountNbr;
    }

    public void setBbkAccountNbr(String bbkAccountNbr) {
        this.bbkAccountNbr = bbkAccountNbr;
    }

    public String getBilRteTraNbr() {
        return bilRteTraNbr;
    }

    public void setBilRteTraNbr(String bilRteTraNbr) {
        this.bilRteTraNbr = bilRteTraNbr;
    }

    public char getBbkNoDisplayInd() {
        return bbkNoDisplayInd;
    }

    public void setBbkNoDisplayInd(char bbkNoDisplayInd) {
        this.bbkNoDisplayInd = bbkNoDisplayInd;
    }

}
