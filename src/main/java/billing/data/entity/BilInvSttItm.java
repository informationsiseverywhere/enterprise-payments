package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilInvSttItmId;

@Entity
@Table(name = "BIL_INV_STT_ITM")
public class BilInvSttItm implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilInvSttItmId bilInvSttItmId;

    @Column(name = "BIL_ACCOUNT_NBR", length = 30, nullable = false)
    private String bilAccountNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_ADDITIONAL_ID", length = 20, nullable = false)
    private String bilAdditionalId = SEPARATOR_BLANK;

    @Column(name = "POL_SYMBOL_CD", length = 3)
    private String polSymbolCd = SEPARATOR_BLANK;

    @Column(name = "POL_NBR", length = 25, nullable = false)
    private String polNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_PBL_ITEM_CD", length = 3, nullable = false)
    private String bilPblItemCd = SEPARATOR_BLANK;

    @Column(name = "BIL_STT_GROSS_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttGrossAmt = DECIMAL_ZERO;

    @Column(name = "BIL_STT_NET_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttNetAmt = DECIMAL_ZERO;

    @Column(name = "BIL_STT_COM_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSttComAmt = DECIMAL_ZERO;

    @Column(name = "BSD_STT_COM_PCT", precision = 9, scale = 6, nullable = false)
    private double bilSttComPct = DECIMAL_ZERO;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "BIL_STT_RECON_IND", nullable = false)
    private char bilSttReconInd = BLANK_CHAR;

    @Column(name = "BIL_RECON_ACY_CD", length = 3, nullable = false)
    private String bilReconAcyCd = SEPARATOR_BLANK;

    @Column(name = "BIL_STT_RECON_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilSttReconDt;

    @Column(name = "BIL_OGN_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bilOgnTs;

    @Column(name = "BIL_STT_DTL_TXT", length = 140, nullable = false)
    private String bilSttDtlTxt = SEPARATOR_BLANK;

    public BilInvSttItmId getBilInvSttItmId() {
        return bilInvSttItmId;
    }

    public void setBilInvSttItmId(BilInvSttItmId bilInvSttItmId) {
        this.bilInvSttItmId = bilInvSttItmId;
    }

    public String getBilAccountNbr() {
        return bilAccountNbr;
    }

    public void setBilAccountNbr(String bilAccountNbr) {
        this.bilAccountNbr = bilAccountNbr;
    }

    public String getBilAdditionalId() {
        return bilAdditionalId;
    }

    public void setBilAdditionalId(String bilAdditionalId) {
        this.bilAdditionalId = bilAdditionalId;
    }

    public String getPolSymbolCd() {
        return polSymbolCd;
    }

    public void setPolSymbolCd(String polSymbolCd) {
        this.polSymbolCd = polSymbolCd;
    }

    public String getPolNbr() {
        return polNbr;
    }

    public void setPolNbr(String polNbr) {
        this.polNbr = polNbr;
    }

    public String getBilPblItemCd() {
        return bilPblItemCd;
    }

    public void setBilPblItemCd(String bilPblItemCd) {
        this.bilPblItemCd = bilPblItemCd;
    }

    public double getBilSttGrossAmt() {
        return bilSttGrossAmt;
    }

    public void setBilSttGrossAmt(double bilSttGrossAmt) {
        this.bilSttGrossAmt = bilSttGrossAmt;
    }

    public double getBilSttNetAmt() {
        return bilSttNetAmt;
    }

    public void setBilSttNetAmt(double bilSttNetAmt) {
        this.bilSttNetAmt = bilSttNetAmt;
    }

    public double getBilSttComAmt() {
        return bilSttComAmt;
    }

    public void setBilSttComAmt(double bilSttComAmt) {
        this.bilSttComAmt = bilSttComAmt;
    }

    public double getBilSttComPct() {
        return bilSttComPct;
    }

    public void setBilSttComPct(double bilSttComPct) {
        this.bilSttComPct = bilSttComPct;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public char getBilSttReconInd() {
        return bilSttReconInd;
    }

    public void setBilSttReconInd(char bilSttReconInd) {
        this.bilSttReconInd = bilSttReconInd;
    }

    public String getBilReconAcyCd() {
        return bilReconAcyCd;
    }

    public void setBilReconAcyCd(String bilReconAcyCd) {
        this.bilReconAcyCd = bilReconAcyCd;
    }

    public ZonedDateTime getBilSttReconDt() {
        return bilSttReconDt;
    }

    public void setBilSttReconDt(ZonedDateTime bilSttReconDt) {
        this.bilSttReconDt = bilSttReconDt;
    }

    public ZonedDateTime getBilOgnTs() {
        return bilOgnTs;
    }

    public void setBilOgnTs(ZonedDateTime bilOgnTs) {
        this.bilOgnTs = bilOgnTs;
    }

    public String getBilSttDtlTxt() {
        return bilSttDtlTxt;
    }

    public void setBilSttDtlTxt(String bilSttDtlTxt) {
        this.bilSttDtlTxt = bilSttDtlTxt;
    }

	
   

}
