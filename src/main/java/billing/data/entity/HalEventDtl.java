package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.HalEventDtlId;

@Entity
@Table(name = "HAL_EVENT_DTL")
public class HalEventDtl implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private HalEventDtlId halEventDtlId;

    @Column(name = "HAL_EVT_STATUS_CD", length = 50, nullable = false)
    private char halEvtStatusCd = BLANK_CHAR;

    @Column(name = "HAL_EVT_PROCESS_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime halEvtProcessTs;

    @Column(name = "EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime effectiveDt;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "HAL_EVENT_MDU_NM", length = 8, nullable = false)
    private String halEventMduNm = SEPARATOR_BLANK;

    @Column(name = "ISSUE_SYS_CD", length = 2, nullable = false)
    private String issueSysCd = SEPARATOR_BLANK;

    @Column(name = "HAL_EVENT_CPL_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime halEventCplTs;

    @Column(name = "HAL_EVENT_DTL_TXT", length = 3000, nullable = false)
    private String halEventDtlTxt;

    public HalEventDtl() {
        super();
    }

    public HalEventDtl(HalEventDtlId halEventDtlId, char halEvtStatusCd, ZonedDateTime halEvtProcessTs,
            ZonedDateTime effectiveDt, String userId, String halEventMduNm, String issueSysCd,
            ZonedDateTime halEventCplTs, String halEventDtlTxt) {
        super();
        this.halEventDtlId = halEventDtlId;
        this.halEvtStatusCd = halEvtStatusCd;
        this.halEvtProcessTs = halEvtProcessTs;
        this.effectiveDt = effectiveDt;
        this.userId = userId;
        this.halEventMduNm = halEventMduNm;
        this.issueSysCd = issueSysCd;
        this.halEventCplTs = halEventCplTs;
        this.halEventDtlTxt = halEventDtlTxt;
    }

    public HalEventDtlId getHalEventDtlId() {
        return halEventDtlId;
    }

    public void setHalEventDtlId(HalEventDtlId halEventDtlId) {
        this.halEventDtlId = halEventDtlId;
    }

    public char getHalEvtStatusCd() {
        return halEvtStatusCd;
    }

    public void setHalEvtStatusCd(char halEvtStatusCd) {
        this.halEvtStatusCd = halEvtStatusCd;
    }

    public ZonedDateTime getHalEvtProcessTs() {
        return halEvtProcessTs;
    }

    public void setHalEvtProcessTs(ZonedDateTime halEvtProcessTs) {
        this.halEvtProcessTs = halEvtProcessTs;
    }

    public ZonedDateTime getEffectiveDt() {
        return effectiveDt;
    }

    public void setEffectiveDt(ZonedDateTime effectiveDt) {
        this.effectiveDt = effectiveDt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getHalEventMduNm() {
        return halEventMduNm;
    }

    public void setHalEventMduNm(String halEventMduNm) {
        this.halEventMduNm = halEventMduNm;
    }

    public String getIssueSysCd() {
        return issueSysCd;
    }

    public void setIssueSysCd(String issueSysCd) {
        this.issueSysCd = issueSysCd;
    }

    public ZonedDateTime getHalEventCplTs() {
        return halEventCplTs;
    }

    public void setHalEventCplTs(ZonedDateTime halEventCplTs) {
        this.halEventCplTs = halEventCplTs;
    }

    public String getHalEventDtlTxt() {
        return halEventDtlTxt;
    }

    public void setHalEventDtlTxt(String halEventDtlTxt) {
        this.halEventDtlTxt = halEventDtlTxt;
    }

}
