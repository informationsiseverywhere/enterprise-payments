package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "SEC_USRS")
public class SecUsrs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SEC_USR_ID", length = 8, nullable = false)
    private String secUserId = SEPARATOR_BLANK;

    @Column(name = "SEC_USR_TYPE_CD", nullable = false)
    private char secUsrTypeCd = BLANK_CHAR;

    @Column(name = "SEC_LST_LOGON_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime secLastLoginDate;

    @Column(name = "SEC_LST_LOGON_TM", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "hh:mm:ss")
    private ZonedDateTime secLastLoginTime;

    @Column(name = "SEC_USR_EFF_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime secUsrEffectiveDate;

    @Column(name = "SEC_USR_EXP_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime secUsrExpirationDate;

    @Column(name = "SEC_USR_CLT_ID", length = 20, nullable = false)
    private String secUsrClientId = SEPARATOR_BLANK;

    @Column(name = "SEC_LONG_USR_ID", length = 255, nullable = false)
    private String secLongUserId = SEPARATOR_BLANK;

    public String getSecUserId() {
        return secUserId;
    }

    public void setSecUserId(String secUserId) {
        this.secUserId = secUserId;
    }

    public char getSecUsrTypeCd() {
        return secUsrTypeCd;
    }

    public void setSecUsrTypeCd(char secUsrTypeCd) {
        this.secUsrTypeCd = secUsrTypeCd;
    }

    public ZonedDateTime getSecLastLoginDate() {
        return secLastLoginDate;
    }

    public void setSecLastLoginDate(ZonedDateTime secLastLoginDate) {
        this.secLastLoginDate = secLastLoginDate;
    }

    public ZonedDateTime getSecLastLoginTime() {
        return secLastLoginTime;
    }

    public void setSecLastLoginTime(ZonedDateTime secLastLoginTime) {
        this.secLastLoginTime = secLastLoginTime;
    }

    public ZonedDateTime getSecUsrEffectiveDate() {
        return secUsrEffectiveDate;
    }

    public void setSecUsrEffectiveDate(ZonedDateTime secUsrEffectiveDate) {
        this.secUsrEffectiveDate = secUsrEffectiveDate;
    }

    public ZonedDateTime getSecUsrExpirationDate() {
        return secUsrExpirationDate;
    }

    public void setSecUsrExpirationDate(ZonedDateTime secUsrExpirationDate) {
        this.secUsrExpirationDate = secUsrExpirationDate;
    }

    public String getSecUsrClientId() {
        return secUsrClientId;
    }

    public void setSecUsrClientId(String secUsrClientId) {
        this.secUsrClientId = secUsrClientId;
    }

    public String getSecLongUserId() {
        return secLongUserId;
    }

    public void setSecLongUserId(String secLongUserId) {
        this.secLongUserId = secLongUserId;
    }
}