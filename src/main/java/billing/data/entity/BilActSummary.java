package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilActSummaryId;

@Entity
@Table(name = "BIL_ACT_SUMMARY")
public class BilActSummary implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilActSummaryId bilActSummaryId;

    @Column(name = "POL_SYMBOL_CD", length = 3, nullable = false)
    private String polSymbolCd = SEPARATOR_BLANK;

    @Column(name = "POL_NBR", length = 25, nullable = false)
    private String polNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_ACY_DES_CD", length = 3, nullable = false)
    private String bilAcyDesCd = SEPARATOR_BLANK;

    @Column(name = "BIL_DES_REA_TYP", length = 3, nullable = false)
    private String bilDesReaTyp = SEPARATOR_BLANK;

    @Column(name = "BIL_ACY_DES1_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAcyDes1Dt;

    @Column(name = "BIL_ACY_DES2_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAcyDes2Dt;

    @Column(name = "BIL_ACY_AMT", precision = 14, scale = 2, nullable = false)
    private double bilAcyAmt;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "BIL_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bilAcyTs;

    @Column(name = "BAS_ADD_DATA_TXT", length = 100, nullable = false)
    private String basAddDataTxt = SEPARATOR_BLANK;

    public BilActSummaryId getBillActSummaryId() {
        return bilActSummaryId;
    }

    public void setBillActSummaryId(BilActSummaryId bilActSummaryId) {
        this.bilActSummaryId = bilActSummaryId;
    }

    public String getPolSymbolCd() {
        return polSymbolCd;
    }

    public void setPolSymbolCd(String polSymbolCd) {
        this.polSymbolCd = polSymbolCd;
    }

    public String getPolNbr() {
        return polNbr;
    }

    public void setPolNbr(String polNbr) {
        this.polNbr = polNbr;
    }

    public String getBilAcyDesCd() {
        return bilAcyDesCd;
    }

    public void setBilAcyDesCd(String bilAcyDesCd) {
        this.bilAcyDesCd = bilAcyDesCd;
    }

    public String getBilDesReaTyp() {
        return bilDesReaTyp;
    }

    public void setBilDesReaTyp(String bilDesReaTyp) {
        this.bilDesReaTyp = bilDesReaTyp;
    }

    public ZonedDateTime getBilAcyDes1Dt() {
        return bilAcyDes1Dt;
    }

    public void setBilAcyDes1Dt(ZonedDateTime bilAcyDes1Dt) {
        this.bilAcyDes1Dt = bilAcyDes1Dt;
    }

    public ZonedDateTime getBilAcyDes2Dt() {
        return bilAcyDes2Dt;
    }

    public void setBilAcyDes2Dt(ZonedDateTime bilAcyDes2Dt) {
        this.bilAcyDes2Dt = bilAcyDes2Dt;
    }

    public double getBilAcyAmt() {
        return bilAcyAmt;
    }

    public void setBilAcyAmt(double bilAcyAmt) {
        this.bilAcyAmt = bilAcyAmt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ZonedDateTime getBilAcyTs() {
        return bilAcyTs;
    }

    public void setBilAcyTs(ZonedDateTime bilAcyTs) {
        this.bilAcyTs = bilAcyTs;
    }

    public String getBasAddDataTxt() {
        return basAddDataTxt;
    }

    public void setBasAddDataTxt(String basAddDataTxt) {
        this.basAddDataTxt = basAddDataTxt;
    }

}
