package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilTtyCashRctId;

@Entity
@Table(name = "BIL_TTY_CASH_RCT")
public class BilTtyCashRct implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilTtyCashRctId bilTtyCashRctId;

    @Column(name = "BIL_ENTRY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEntryDt;

    @Column(name = "BIL_ENTRY_NBR", length = 4, nullable = false)
    private String bilEntryNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_ENTRY_SEQ_NBR", length = 5, nullable = false)
    private short bilEntrySeqNbr;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_AMT", precision = 14, scale = 2, nullable = false)
    private double bilRctAmt;

    @Column(name = "BIL_RCT_ID", length = 10, nullable = false)
    private String bilRctId = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_TYPE_CD", length = 2, nullable = false)
    private String bilRctTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_RECEIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilRctReceiveDt;

    @Column(name = "BIL_DEPOSIT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilDepositDt;

    @Column(name = "BIL_BANK_CD", length = 3, nullable = false)
    private String bilBankCd = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_TYPE_CD", nullable = false)
    private char bilCrcrdTypeCd;

    @Column(name = "BIL_CRCRD_ACT_NBR", length = 25, nullable = false)
    private String bilCrcrdActNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_EXP_DT", length = 4, nullable = false)
    private String bilCrcrdExpDt = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_AUT_APV", length = 6, nullable = false)
    private String bilCrcrdAutApv = SEPARATOR_BLANK;

    @Column(name = "BIL_CSH_ETR_MTH_CD", nullable = false)
    private char bilCshEtrMthCd;

    @Column(name = "BIL_TAX_YEAR_NBR", length = 5, nullable = false)
    private short bilTaxYearNbr;

    @Column(name = "BIL_TO_FRO_TRF_NBR", length = 30, nullable = false)
    private String bilToFroTrfNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_TRF_TYPE_CD", nullable = false)
    private char bilTrfTypeCd;

    @Column(name = "BIL_PSTMRK_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilPstmrkDt;

    @Column(name = "BIL_PSTMRK_CD", nullable = false)
    private char bilPstmrkCd;

    @Column(name = "BIL_RCT_CMT", length = 80, nullable = false)
    private String bilRctCmt = SEPARATOR_BLANK;

    public BilTtyCashRctId getBilTtyCashRctId() {
        return bilTtyCashRctId;
    }

    public void setBilTtyCashRctId(BilTtyCashRctId bilTtyCashRctId) {
        this.bilTtyCashRctId = bilTtyCashRctId;
    }

    public ZonedDateTime getBilEntryDt() {
        return bilEntryDt;
    }

    public void setBilEntryDt(ZonedDateTime bilEntryDt) {
        this.bilEntryDt = bilEntryDt;
    }

    public String getBilEntryNbr() {
        return bilEntryNbr;
    }

    public void setBilEntryNbr(String bilEntryNbr) {
        this.bilEntryNbr = bilEntryNbr;
    }

    public short getBilEntrySeqNbr() {
        return bilEntrySeqNbr;
    }

    public void setBilEntrySeqNbr(short bilEntrySeqNbr) {
        this.bilEntrySeqNbr = bilEntrySeqNbr;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getBilRctAmt() {
        return bilRctAmt;
    }

    public void setBilRctAmt(double bilRctAmt) {
        this.bilRctAmt = bilRctAmt;
    }

    public String getBilRctId() {
        return bilRctId;
    }

    public void setBilRctId(String bilRctId) {
        this.bilRctId = bilRctId;
    }

    public String getBilRctTypeCd() {
        return bilRctTypeCd;
    }

    public void setBilRctTypeCd(String bilRctTypeCd) {
        this.bilRctTypeCd = bilRctTypeCd;
    }

    public ZonedDateTime getBilRctReceiveDt() {
        return bilRctReceiveDt;
    }

    public void setBilRctReceiveDt(ZonedDateTime bilRctReceiveDt) {
        this.bilRctReceiveDt = bilRctReceiveDt;
    }

    public ZonedDateTime getBilDepositDt() {
        return bilDepositDt;
    }

    public void setBilDepositDt(ZonedDateTime bilDepositDt) {
        this.bilDepositDt = bilDepositDt;
    }

    public String getBilBankCd() {
        return bilBankCd;
    }

    public void setBilBankCd(String bilBankCd) {
        this.bilBankCd = bilBankCd;
    }

    public char getBilCrcrdTypeCd() {
        return bilCrcrdTypeCd;
    }

    public void setBilCrcrdTypeCd(char bilCrcrdTypeCd) {
        this.bilCrcrdTypeCd = bilCrcrdTypeCd;
    }

    public String getBilCrcrdActNbr() {
        return bilCrcrdActNbr;
    }

    public void setBilCrcrdActNbr(String bilCrcrdActNbr) {
        this.bilCrcrdActNbr = bilCrcrdActNbr;
    }

    public String getBilCrcrdExpDt() {
        return bilCrcrdExpDt;
    }

    public void setBilCrcrdExpDt(String bilCrcrdExpDt) {
        this.bilCrcrdExpDt = bilCrcrdExpDt;
    }

    public String getBilCrcrdAutApv() {
        return bilCrcrdAutApv;
    }

    public void setBilCrcrdAutApv(String bilCrcrdAutApv) {
        this.bilCrcrdAutApv = bilCrcrdAutApv;
    }

    public char getBilCshEtrMthCd() {
        return bilCshEtrMthCd;
    }

    public void setBilCshEtrMthCd(char bilCshEtrMthCd) {
        this.bilCshEtrMthCd = bilCshEtrMthCd;
    }

    public short getBilTaxYearNbr() {
        return bilTaxYearNbr;
    }

    public void setBilTaxYearNbr(short bilTaxYearNbr) {
        this.bilTaxYearNbr = bilTaxYearNbr;
    }

    public String getBilToFroTrfNbr() {
        return bilToFroTrfNbr;
    }

    public void setBilToFroTrfNbr(String bilToFroTrfNbr) {
        this.bilToFroTrfNbr = bilToFroTrfNbr;
    }

    public char getBilTrfTypeCd() {
        return bilTrfTypeCd;
    }

    public void setBilTrfTypeCd(char bilTrfTypeCd) {
        this.bilTrfTypeCd = bilTrfTypeCd;
    }

    public ZonedDateTime getBilPstmrkDt() {
        return bilPstmrkDt;
    }

    public void setBilPstmrkDt(ZonedDateTime bilPstmrkDt) {
        this.bilPstmrkDt = bilPstmrkDt;
    }

    public char getBilPstmrkCd() {
        return bilPstmrkCd;
    }

    public void setBilPstmrkCd(char bilPstmrkCd) {
        this.bilPstmrkCd = bilPstmrkCd;
    }

    public String getBilRctCmt() {
        return bilRctCmt;
    }

    public void setBilRctCmt(String bilRctCmt) {
        this.bilRctCmt = bilRctCmt;
    }

}
