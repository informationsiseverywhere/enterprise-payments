package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilOrgSupportId;

@Entity
@Table(name = "BIL_ORG_SUPPORT")
public class BilOrgSupport implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilOrgSupportId bilOrgSupportId;

    @Column(name = "BOS_CPA_DSN_CEN", length = 5, nullable = false)
    private String bosCpaDsnCen = SEPARATOR_BLANK;

    @Column(name = "BOS_CPA_CRN_NBR", length = 4, nullable = false)
    private String bosCpaCrnNbr = SEPARATOR_BLANK;

    @Column(name = "BOS_MERCHANT_ID", length = 17, nullable = false)
    private String bosMerchantId = SEPARATOR_BLANK;

    @Column(name = "BIL_RTE_TRA_NBR", length = 10, nullable = false)
    private String bilRteTraNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_BANK_NM", length = 512, nullable = false)
    private String bilBankNm = SEPARATOR_BLANK;

    @Column(name = "BOS_TAPE_FRM_CD", length = 4, nullable = false)
    private String bosTapeFrmCd = SEPARATOR_BLANK;

    @Column(name = "BOS_COMPANY_ID", length = 25, nullable = false)
    private String bosCompanyId = SEPARATOR_BLANK;

    @Column(name = "BOS_COMPANY_NM", length = 512, nullable = false)
    private String bosCompanyNm = SEPARATOR_BLANK;

    @Column(name = "BOS_PHY_TAPE_IND", length = 2, nullable = false)
    private String bosPhyTapeInd = SEPARATOR_BLANK;

    @Column(name = "BOS_DISC_DATA", length = 20, nullable = false)
    private String bosDiscData = SEPARATOR_BLANK;

    @Column(name = "BOS_FILE_DES_TXT", length = 40, nullable = false)
    private String bosFileDesTxt = SEPARATOR_BLANK;

    @Column(name = "BIL_BANK_CD", length = 3, nullable = false)
    private String bilBankCd = SEPARATOR_BLANK;

    public BilOrgSupportId getBilOrgSupportId() {
        return bilOrgSupportId;
    }

    public void setBilOrgSupportId(BilOrgSupportId bilOrgSupportId) {
        this.bilOrgSupportId = bilOrgSupportId;
    }

    public String getBosCpaDsnCen() {
        return bosCpaDsnCen;
    }

    public void setBosCpaDsnCen(String bosCpaDsnCen) {
        this.bosCpaDsnCen = bosCpaDsnCen;
    }

    public String getBosCpaCrnNbr() {
        return bosCpaCrnNbr;
    }

    public void setBosCpaCrnNbr(String bosCpaCrnNbr) {
        this.bosCpaCrnNbr = bosCpaCrnNbr;
    }

    public String getBosMerchantId() {
        return bosMerchantId;
    }

    public void setBosMerchantId(String bosMerchantId) {
        this.bosMerchantId = bosMerchantId;
    }

    public String getBilRteTraNbr() {
        return bilRteTraNbr;
    }

    public void setBilRteTraNbr(String bilRteTraNbr) {
        this.bilRteTraNbr = bilRteTraNbr;
    }

    public String getBilBankNm() {
        return bilBankNm;
    }

    public void setBilBankNm(String bilBankNm) {
        this.bilBankNm = bilBankNm;
    }

    public String getBosTapeFrmCd() {
        return bosTapeFrmCd;
    }

    public void setBosTapeFrmCd(String bosTapeFrmCd) {
        this.bosTapeFrmCd = bosTapeFrmCd;
    }

    public String getBosCompanyId() {
        return bosCompanyId;
    }

    public void setBosCompanyId(String bosCompanyId) {
        this.bosCompanyId = bosCompanyId;
    }

    public String getBosCompanyNm() {
        return bosCompanyNm;
    }

    public void setBosCompanyNm(String bosCompanyNm) {
        this.bosCompanyNm = bosCompanyNm;
    }

    public String getBosPhyTapeInd() {
        return bosPhyTapeInd;
    }

    public void setBosPhyTapeInd(String bosPhyTapeInd) {
        this.bosPhyTapeInd = bosPhyTapeInd;
    }

    public String getBosDiscData() {
        return bosDiscData;
    }

    public void setBosDiscData(String bosDiscData) {
        this.bosDiscData = bosDiscData;
    }

    public String getBosFileDesTxt() {
        return bosFileDesTxt;
    }

    public void setBosFileDesTxt(String bosFileDesTxt) {
        this.bosFileDesTxt = bosFileDesTxt;
    }

    public String getBilBankCd() {
        return bilBankCd;
    }

    public void setBilBankCd(String bilBankCd) {
        this.bilBankCd = bilBankCd;
    }

}
