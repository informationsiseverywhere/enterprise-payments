package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.HalUniversalCtl3Id;

@Entity
@Table(name = "HAL_UNIVERSAL_CTL3")
public class HalUniversalCtl3 implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private HalUniversalCtl3Id halUniversalCtl3Id;

    @Column(name = "ENTRY_DATA_TAG", length = 150, nullable = false)
    private String entryDataTag = SEPARATOR_BLANK;

    @Column(name = "ENTRY_DATA_TXT", length = 200, nullable = false)
    private String entryDataTxt = SEPARATOR_BLANK;

    public HalUniversalCtl3() {
        super();
    }

    public HalUniversalCtl3(HalUniversalCtl3Id halUniversalCtl3Id, String entryDataTag, String entryDataTxt) {
        super();
        this.halUniversalCtl3Id = halUniversalCtl3Id;
        this.entryDataTag = entryDataTag;
        this.entryDataTxt = entryDataTxt;
    }

    public HalUniversalCtl3Id getHalUniversalCtl3Id() {
        return halUniversalCtl3Id;
    }

    public String getEntryDataTag() {
        return entryDataTag;
    }

    public String getEntryDataTxt() {
        return entryDataTxt;
    }

    public void setHalUniversalCtl3Id(HalUniversalCtl3Id halUniversalCtl3Id) {
        this.halUniversalCtl3Id = halUniversalCtl3Id;
    }

    public void setEntryDataTag(String entryDataTag) {
        this.entryDataTag = entryDataTag;
    }

    public void setEntryDataTxt(String entryDataTxt) {
        this.entryDataTxt = entryDataTxt;
    }

}
