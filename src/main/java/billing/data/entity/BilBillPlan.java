package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "BIL_BILL_PLAN")
public class BilBillPlan implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "BIL_PLAN_CD", length = 4, nullable = false)
    private String bilPlanCd = SEPARATOR_BLANK;

    @Column(name = "BBP_PLAN_DES", length = 30, nullable = false)
    private String bbpPlanDes = SEPARATOR_BLANK;

    @Column(name = "BBP_BILL_FQY_CD", length = 2, nullable = false)
    private String bbpBillFqyCd = SEPARATOR_BLANK;

    @Column(name = "BBP_DOWNPAY_IND", nullable = false)
    private char bbpDownpayInd;

    @Column(name = "BBP_DOWNPAY_PCT", precision = 14, scale = 2, nullable = false)
    private double bbpDownpayPct;

    @Column(name = "BBP_IST_NBR_IND", nullable = false)
    private char bbpIstNbrInd;

    @Column(name = "BBP_IST_NBR", length = 5, nullable = false)
    private short bbpIstNbr;

    @Column(name = "BBP_PND_CNC_IND", nullable = false)
    private char bbpPndCncInd;

    @Column(name = "BBP_PND_CNC_PCT", precision = 5, scale = 2, nullable = false)
    private double bbpPndCncPct;

    @Column(name = "BBP_PND_CNC_AMT", precision = 14, scale = 2, nullable = false)
    private double bbpPndCncAmt;

    @Column(name = "BBP_COFF_CNC_PCT", precision = 5, scale = 2, nullable = false)
    private double bbpCoffCncPct;

    @Column(name = "BBP_COFF_CNC_AMT", precision = 14, scale = 2, nullable = false)
    private double bbpCoffCncAmt;

    @Column(name = "BBP_WRO_IND", nullable = false)
    private char bbpWroInd;

    @Column(name = "BBP_WRO_NBR", length = 5, nullable = false)
    private short bbpWroNbr;

    @Column(name = "BBP_WRO_PCT", precision = 5, scale = 2, nullable = false)
    private double bbpWroPct;

    @Column(name = "BBP_WRO_AMT", precision = 14, scale = 2, nullable = false)
    private double bbpWroAmt;

    @Column(name = "BBP_CNC_DSB_PYE", nullable = false)
    private char bbpCncDsbPye;

    @Column(name = "BBP_CNC_MIN_DSB", precision = 14, scale = 2, nullable = false)
    private double bbpCncMinDsb;

    @Column(name = "BBP_CNC_MAX_DSB", precision = 14, scale = 2, nullable = false)
    private double bbpCncMaxDsb;

    @Column(name = "BBP_CNC_HOLD_NBR", length = 5, nullable = false)
    private short bbpCncHoldNbr;

    @Column(name = "BBP_CRE_DSB_PYE", nullable = false)
    private char bbpCreDsbPye;

    @Column(name = "BBP_CRE_MIN_DSB", precision = 14, scale = 2, nullable = false)
    private double bbpCreMinDsb;

    @Column(name = "BBP_CRE_MAX_DSB", precision = 14, scale = 2, nullable = false)
    private double bbpCreMaxDsb;

    @Column(name = "BBP_CRE_HOLD_NBR", length = 5, nullable = false)
    private short bbpCreHoldNbr;

    @Column(name = "BBP_RNL_FIST_NBR", length = 5, nullable = false)
    private short bbpRnlFistNbr;

    @Column(name = "BBP_SPREAD_IND", nullable = false)
    private char bbpSpreadInd;

    @Column(name = "BBP_CANCEL_IND", nullable = false)
    private char bbpCancelInd;

    @Column(name = "BBP_SGD_MIN_DAYS", length = 5, nullable = false)
    private short bbpSgdMinDays;

    @Column(name = "BBP_RNL_DP_BILL", nullable = false)
    private char bbpRnlDpBill;

    @Column(name = "BBP_POL_TRM_NBR", length = 5, nullable = false)
    private short bbpPolTrmNbr;

    @Column(name = "BBP_FLT_CAN_PP_IND", nullable = false)
    private char bbpFltCanPpInd;

    @Column(name = "BBP_PEN_CAN_AMT", precision = 14, scale = 2, nullable = false)
    private double bbpPenCanAmt;

    @Column(name = "BBP_PEN_CAN_PCT", precision = 5, scale = 2, nullable = false)
    private double bbpPenCanPct;

    @Column(name = "BBP_RES_CAN_AMT", precision = 14, scale = 2, nullable = false)
    private double bbpResCanAmt;

    @Column(name = "BBP_RES_CAN_PCT", precision = 5, scale = 2, nullable = false)
    private double bbpResCanPct;

    @Column(name = "BBP_ADJ_DT_IND", nullable = false)
    private char bbpAdjDtInd;

    @Column(name = "BIL_EQY_RESC_CD", nullable = false)
    private char bilEqyRescCd;

    @Column(name = "BIL_EQY_RESC_NBR", length = 5, nullable = false)
    private short bilEqyRescNbr;

    @Column(name = "BIL_ROU_OPT_CD", nullable = false)
    private char bilRouOptCd;

    @Column(name = "BIL_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilEffDt;

    @Column(name = "BIL_EXP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilExpDt;

    @Column(name = "BIL_NEXT_PLAN_CD", length = 2, nullable = false)
    private String bilNextPlanCd = SEPARATOR_BLANK;

    @Column(name = "BBP_INV_DURING_PCN_IND", nullable = false)
    private char bbpInvDuringPcnInd;

    @Column(name = "BBP_ALT_LGL_DAYS", length = 5, nullable = false)
    private Short bbpAltLglDays;

    public String getBilPlanCd() {
        return bilPlanCd;
    }

    public void setBilPlanCd(String bilPlanCd) {
        this.bilPlanCd = bilPlanCd;
    }

    public String getBbpPlanDes() {
        return bbpPlanDes;
    }

    public void setBbpPlanDes(String bbpPlanDes) {
        this.bbpPlanDes = bbpPlanDes;
    }

    public String getBbpBillFqyCd() {
        return bbpBillFqyCd;
    }

    public void setBbpBillFqyCd(String bbpBillFqyCd) {
        this.bbpBillFqyCd = bbpBillFqyCd;
    }

    public char getBbpDownpayInd() {
        return bbpDownpayInd;
    }

    public void setBbpDownpayInd(char bbpDownpayInd) {
        this.bbpDownpayInd = bbpDownpayInd;
    }

    public double getBbpDownpayPct() {
        return bbpDownpayPct;
    }

    public void setBbpDownpayPct(double bbpDownpayPct) {
        this.bbpDownpayPct = bbpDownpayPct;
    }

    public char getBbpIstNbrInd() {
        return bbpIstNbrInd;
    }

    public void setBbpIstNbrInd(char bbpIstNbrInd) {
        this.bbpIstNbrInd = bbpIstNbrInd;
    }

    public short getBbpIstNbr() {
        return bbpIstNbr;
    }

    public void setBbpIstNbr(short bbpIstNbr) {
        this.bbpIstNbr = bbpIstNbr;
    }

    public char getBbpPndCncInd() {
        return bbpPndCncInd;
    }

    public void setBbpPndCncInd(char bbpPndCncInd) {
        this.bbpPndCncInd = bbpPndCncInd;
    }

    public double getBbpPndCncPct() {
        return bbpPndCncPct;
    }

    public void setBbpPndCncPct(double bbpPndCncPct) {
        this.bbpPndCncPct = bbpPndCncPct;
    }

    public double getBbpPndCncAmt() {
        return bbpPndCncAmt;
    }

    public void setBbpPndCncAmt(double bbpPndCncAmt) {
        this.bbpPndCncAmt = bbpPndCncAmt;
    }

    public double getBbpCoffCncPct() {
        return bbpCoffCncPct;
    }

    public void setBbpCoffCncPct(double bbpCoffCncPct) {
        this.bbpCoffCncPct = bbpCoffCncPct;
    }

    public double getBbpCoffCncAmt() {
        return bbpCoffCncAmt;
    }

    public void setBbpCoffCncAmt(double bbpCoffCncAmt) {
        this.bbpCoffCncAmt = bbpCoffCncAmt;
    }

    public char getBbpWroInd() {
        return bbpWroInd;
    }

    public void setBbpWroInd(char bbpWroInd) {
        this.bbpWroInd = bbpWroInd;
    }

    public short getBbpWroNbr() {
        return bbpWroNbr;
    }

    public void setBbpWroNbr(short bbpWroNbr) {
        this.bbpWroNbr = bbpWroNbr;
    }

    public double getBbpWroPct() {
        return bbpWroPct;
    }

    public void setBbpWroPct(double bbpWroPct) {
        this.bbpWroPct = bbpWroPct;
    }

    public double getBbpWroAmt() {
        return bbpWroAmt;
    }

    public void setBbpWroAmt(double bbpWroAmt) {
        this.bbpWroAmt = bbpWroAmt;
    }

    public char getBbpCncDsbPye() {
        return bbpCncDsbPye;
    }

    public void setBbpCncDsbPye(char bbpCncDsbPye) {
        this.bbpCncDsbPye = bbpCncDsbPye;
    }

    public double getBbpCncMinDsb() {
        return bbpCncMinDsb;
    }

    public void setBbpCncMinDsb(double bbpCncMinDsb) {
        this.bbpCncMinDsb = bbpCncMinDsb;
    }

    public double getBbpCncMaxDsb() {
        return bbpCncMaxDsb;
    }

    public void setBbpCncMaxDsb(double bbpCncMaxDsb) {
        this.bbpCncMaxDsb = bbpCncMaxDsb;
    }

    public short getBbpCncHoldNbr() {
        return bbpCncHoldNbr;
    }

    public void setBbpCncHoldNbr(short bbpCncHoldNbr) {
        this.bbpCncHoldNbr = bbpCncHoldNbr;
    }

    public char getBbpCreDsbPye() {
        return bbpCreDsbPye;
    }

    public void setBbpCreDsbPye(char bbpCreDsbPye) {
        this.bbpCreDsbPye = bbpCreDsbPye;
    }

    public double getBbpCreMinDsb() {
        return bbpCreMinDsb;
    }

    public void setBbpCreMinDsb(double bbpCreMinDsb) {
        this.bbpCreMinDsb = bbpCreMinDsb;
    }

    public double getBbpCreMaxDsb() {
        return bbpCreMaxDsb;
    }

    public void setBbpCreMaxDsb(double bbpCreMaxDsb) {
        this.bbpCreMaxDsb = bbpCreMaxDsb;
    }

    public short getBbpCreHoldNbr() {
        return bbpCreHoldNbr;
    }

    public void setBbpCreHoldNbr(short bbpCreHoldNbr) {
        this.bbpCreHoldNbr = bbpCreHoldNbr;
    }

    public short getBbpRnlFistNbr() {
        return bbpRnlFistNbr;
    }

    public void setBbpRnlFistNbr(short bbpRnlFistNbr) {
        this.bbpRnlFistNbr = bbpRnlFistNbr;
    }

    public char getBbpSpreadInd() {
        return bbpSpreadInd;
    }

    public void setBbpSpreadInd(char bbpSpreadInd) {
        this.bbpSpreadInd = bbpSpreadInd;
    }

    public char getBbpCancelInd() {
        return bbpCancelInd;
    }

    public void setBbpCancelInd(char bbpCancelInd) {
        this.bbpCancelInd = bbpCancelInd;
    }

    public short getBbpSgdMinDays() {
        return bbpSgdMinDays;
    }

    public void setBbpSgdMinDays(short bbpSgdMinDays) {
        this.bbpSgdMinDays = bbpSgdMinDays;
    }

    public char getBbpRnlDpBill() {
        return bbpRnlDpBill;
    }

    public void setBbpRnlDpBill(char bbpRnlDpBill) {
        this.bbpRnlDpBill = bbpRnlDpBill;
    }

    public short getBbpPolTrmNbr() {
        return bbpPolTrmNbr;
    }

    public void setBbpPolTrmNbr(short bbpPolTrmNbr) {
        this.bbpPolTrmNbr = bbpPolTrmNbr;
    }

    public char getBbpFltCanPpInd() {
        return bbpFltCanPpInd;
    }

    public void setBbpFltCanPpInd(char bbpFltCanPpInd) {
        this.bbpFltCanPpInd = bbpFltCanPpInd;
    }

    public double getBbpPenCanAmt() {
        return bbpPenCanAmt;
    }

    public void setBbpPenCanAmt(double bbpPenCanAmt) {
        this.bbpPenCanAmt = bbpPenCanAmt;
    }

    public double getBbpPenCanPct() {
        return bbpPenCanPct;
    }

    public void setBbpPenCanPct(double bbpPenCanPct) {
        this.bbpPenCanPct = bbpPenCanPct;
    }

    public double getBbpResCanAmt() {
        return bbpResCanAmt;
    }

    public void setBbpResCanAmt(double bbpResCanAmt) {
        this.bbpResCanAmt = bbpResCanAmt;
    }

    public double getBbpResCanPct() {
        return bbpResCanPct;
    }

    public void setBbpResCanPct(double bbpResCanPct) {
        this.bbpResCanPct = bbpResCanPct;
    }

    public char getBbpAdjDtInd() {
        return bbpAdjDtInd;
    }

    public void setBbpAdjDtInd(char bbpAdjDtInd) {
        this.bbpAdjDtInd = bbpAdjDtInd;
    }

    public char getBilEqyRescCd() {
        return bilEqyRescCd;
    }

    public void setBilEqyRescCd(char bilEqyRescCd) {
        this.bilEqyRescCd = bilEqyRescCd;
    }

    public short getBilEqyRescNbr() {
        return bilEqyRescNbr;
    }

    public void setBilEqyRescNbr(short bilEqyRescNbr) {
        this.bilEqyRescNbr = bilEqyRescNbr;
    }

    public char getBilRouOptCd() {
        return bilRouOptCd;
    }

    public void setBilRouOptCd(char bilRouOptCd) {
        this.bilRouOptCd = bilRouOptCd;
    }

    public ZonedDateTime getBilEffDt() {
        return bilEffDt;
    }

    public void setBilEffDt(ZonedDateTime bilEffDt) {
        this.bilEffDt = bilEffDt;
    }

    public ZonedDateTime getBilExpDt() {
        return bilExpDt;
    }

    public void setBilExpDt(ZonedDateTime bilExpDt) {
        this.bilExpDt = bilExpDt;
    }

    public String getBilNextPlanCd() {
        return bilNextPlanCd;
    }

    public void setBilNextPlanCd(String bilNextPlanCd) {
        this.bilNextPlanCd = bilNextPlanCd;
    }

    public char getBbpInvDuringPcnInd() {
        return bbpInvDuringPcnInd;
    }

    public void setBbpInvDuringPcnInd(char bbpInvDuringPcnInd) {
        this.bbpInvDuringPcnInd = bbpInvDuringPcnInd;
    }

    public Short getBbpAltLglDays() {
        return bbpAltLglDays;
    }

    public void setBbpAltLglDays(Short bbpAltLglDays) {
        this.bbpAltLglDays = bbpAltLglDays;
    }

}
