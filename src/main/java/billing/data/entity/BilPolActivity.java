package billing.data.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilPolActivityId;

@Entity
@Table(name = "BIL_POL_ACTIVITY")
public class BilPolActivity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilPolActivityId bilPolActivityId;

    @Column(name = "BIL_NXT_ACY_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilNxtAcyDt;

    public BilPolActivityId getBillPolActivityId() {
        return bilPolActivityId;
    }

    public void setBillPolActivityId(BilPolActivityId bilPolActivityId) {
        this.bilPolActivityId = bilPolActivityId;
    }

    public ZonedDateTime getBilNxtAcyDt() {
        return bilNxtAcyDt;
    }

    public void setBilNxtAcyDt(ZonedDateTime bilNxtAcyDt) {
        this.bilNxtAcyDt = bilNxtAcyDt;
    }

}
