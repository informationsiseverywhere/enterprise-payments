package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "BIL_THIRD_PARTY")
public class BilThirdParty implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "BIL_THIRD_PARTY_ID", length = 8, nullable = false)
    private String billThirdPartyId = SEPARATOR_BLANK;

    @Column(name = "BIL_TTY_NBR", length = 20, nullable = false)
    private String bilTtyNbr = SEPARATOR_BLANK;

    @Column(name = "BTP_TTY_CLIENT_ID", length = 20, nullable = false)
    private String btpTtyClientId = SEPARATOR_BLANK;

    @Column(name = "BTP_TTY_ADR_SEQ", length = 5, nullable = false)
    private short btpTtyAdrSeq;

    @Column(name = "BIL_START_RFR_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilStartRfrDt;

    @Column(name = "BIL_RFR_LST_DAY", nullable = false)
    private char bilRfrLstDay;

    @Column(name = "BTP_STT_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime btpSttDueDt;

    @Column(name = "BTP_STT_LST_IND", nullable = false)
    private char btpSttLstInd;

    @Column(name = "BIL_CLASS_CD", length = 3, nullable = false)
    private String bilClassCd = SEPARATOR_BLANK;

    @Column(name = "BIL_STT_SR_SEQ_CD", length = 2, nullable = false)
    private String bilSttSrSeqCd = SEPARATOR_BLANK;

    @Column(name = "BIL_PRT_FORMAT_CD", length = 4, nullable = false)
    private String bilPrtFormatCd = SEPARATOR_BLANK;

    @Column(name = "BTP_STATUS_CD", nullable = false)
    private char btpStatusCd;

    @Column(name = "BIL_TYPE_CD", length = 2, nullable = false)
    private String bilTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_COLLECTION_MTH", length = 3, nullable = false)
    private String bilCollectionMth = SEPARATOR_BLANK;

    @Column(name = "BIL_COLLECTION_PLN", length = 4, nullable = false)
    private String bilCollectionPln = SEPARATOR_BLANK;

    @Column(name = "BIL_SUS_FU_REA_CD", length = 3, nullable = false)
    private String bilSusFuReaCd = SEPARATOR_BLANK;

    @Column(name = "BIL_DUE_DT_DPL_NBR", length = 5, nullable = false)
    private short bilDueDtDplNbr;

    @Column(name = "BIL_REPORT_LVL_CD", length = 8, nullable = false)
    private String bilReportLvlCd = SEPARATOR_BLANK;

    @Column(name = "CURRENCY_CD", length = 4, nullable = true)
    private String currencyCd;

    @Column(name = "BIL_LOK_USER_ID", length = 8, nullable = false)
    private String bilLokUserId = SEPARATOR_BLANK;

    @Column(name = "ISSUE_NODE_ID", length = 15, nullable = false)
    private String issueNodeId = SEPARATOR_BLANK;

    @Column(name = "CLIENT_ID", length = 20, nullable = false)
    private String clientId = SEPARATOR_BLANK;

    @Column(name = "BIL_FUNCTION_NM", length = 8, nullable = false)
    private String bilFunctionNm = SEPARATOR_BLANK;

    @Column(name = "BIL_IN_FLIGHT_IND", nullable = false)
    private char bilInFlightInd;

    @Column(name = "BIL_USER_LOK_NBR", length = 5, nullable = false)
    private short bilUserLokNbr;

    @Column(name = "BIL_LOK_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime bilLokTs;

    @Column(name = "BTP_NXT_STT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime btpNxtSttDt;

    @Column(name = "BIL_PRESENTMENT_CD", length = 3, nullable = false)
    private String bilPresentmentCd = SEPARATOR_BLANK;

    @Column(name = "APP_MIGRATION_CD", length = 4, nullable = false)
    private String appMigrationCd = SEPARATOR_BLANK;

    @Column(name = "BIL_STT_CD", length = 10, nullable = false)
    private String bilSttCd = SEPARATOR_BLANK;

    @Column(name = "BTP_TAKE_SUS_CD", length = 3, nullable = false)
    private String btpTakeSusCd = SEPARATOR_BLANK;

    public String getBillThirdPartyId() {
        return billThirdPartyId;
    }

    public void setBillThirdPartyId(String billThirdPartyId) {
        this.billThirdPartyId = billThirdPartyId;
    }

    public String getBilTtyNbr() {
        return bilTtyNbr;
    }

    public void setBilTtyNbr(String bilTtyNbr) {
        this.bilTtyNbr = bilTtyNbr;
    }

    public String getBtpTtyClientId() {
        return btpTtyClientId;
    }

    public void setBtpTtyClientId(String btpTtyClientId) {
        this.btpTtyClientId = btpTtyClientId;
    }

    public short getBtpTtyAdrSeq() {
        return btpTtyAdrSeq;
    }

    public void setBtpTtyAdrSeq(short btpTtyAdrSeq) {
        this.btpTtyAdrSeq = btpTtyAdrSeq;
    }

    public ZonedDateTime getBilStartRfrDt() {
        return bilStartRfrDt;
    }

    public void setBilStartRfrDt(ZonedDateTime bilStartRfrDt) {
        this.bilStartRfrDt = bilStartRfrDt;
    }

    public char getBilRfrLstDay() {
        return bilRfrLstDay;
    }

    public void setBilRfrLstDay(char bilRfrLstDay) {
        this.bilRfrLstDay = bilRfrLstDay;
    }

    public ZonedDateTime getBtpSttDueDt() {
        return btpSttDueDt;
    }

    public void setBtpSttDueDt(ZonedDateTime btpSttDueDt) {
        this.btpSttDueDt = btpSttDueDt;
    }

    public char getBtpSttLstInd() {
        return btpSttLstInd;
    }

    public void setBtpSttLstInd(char btpSttLstInd) {
        this.btpSttLstInd = btpSttLstInd;
    }

    public String getBilClassCd() {
        return bilClassCd;
    }

    public void setBilClassCd(String bilClassCd) {
        this.bilClassCd = bilClassCd;
    }

    public String getBilSttSrSeqCd() {
        return bilSttSrSeqCd;
    }

    public void setBilSttSrSeqCd(String bilSttSrSeqCd) {
        this.bilSttSrSeqCd = bilSttSrSeqCd;
    }

    public String getBilPrtFormatCd() {
        return bilPrtFormatCd;
    }

    public void setBilPrtFormatCd(String bilPrtFormatCd) {
        this.bilPrtFormatCd = bilPrtFormatCd;
    }

    public char getBtpStatusCd() {
        return btpStatusCd;
    }

    public void setBtpStatusCd(char btpStatusCd) {
        this.btpStatusCd = btpStatusCd;
    }

    public String getBilTypeCd() {
        return bilTypeCd;
    }

    public void setBilTypeCd(String bilTypeCd) {
        this.bilTypeCd = bilTypeCd;
    }

    public String getBilCollectionMth() {
        return bilCollectionMth;
    }

    public void setBilCollectionMth(String bilCollectionMth) {
        this.bilCollectionMth = bilCollectionMth;
    }

    public String getBilCollectionPln() {
        return bilCollectionPln;
    }

    public void setBilCollectionPln(String bilCollectionPln) {
        this.bilCollectionPln = bilCollectionPln;
    }

    public String getBilSusFuReaCd() {
        return bilSusFuReaCd;
    }

    public void setBilSusFuReaCd(String bilSusFuReaCd) {
        this.bilSusFuReaCd = bilSusFuReaCd;
    }

    public short getBilDueDtDplNbr() {
        return bilDueDtDplNbr;
    }

    public void setBilDueDtDplNbr(short bilDueDtDplNbr) {
        this.bilDueDtDplNbr = bilDueDtDplNbr;
    }

    public String getBilReportLvlCd() {
        return bilReportLvlCd;
    }

    public void setBilReportLvlCd(String bilReportLvlCd) {
        this.bilReportLvlCd = bilReportLvlCd;
    }

    public String getCurrencyCd() {
        return currencyCd;
    }

    public void setCurrencyCd(String currencyCd) {
        this.currencyCd = currencyCd;
    }

    public String getBilLokUserId() {
        return bilLokUserId;
    }

    public void setBilLokUserId(String bilLokUserId) {
        this.bilLokUserId = bilLokUserId;
    }

    public String getIssueNodeId() {
        return issueNodeId;
    }

    public void setIssueNodeId(String issueNodeId) {
        this.issueNodeId = issueNodeId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getBilFunctionNm() {
        return bilFunctionNm;
    }

    public void setBilFunctionNm(String bilFunctionNm) {
        this.bilFunctionNm = bilFunctionNm;
    }

    public char getBilInFlightInd() {
        return bilInFlightInd;
    }

    public void setBilInFlightInd(char bilInFlightInd) {
        this.bilInFlightInd = bilInFlightInd;
    }

    public short getBilUserLokNbr() {
        return bilUserLokNbr;
    }

    public void setBilUserLokNbr(short bilUserLokNbr) {
        this.bilUserLokNbr = bilUserLokNbr;
    }

    public ZonedDateTime getBilLokTs() {
        return bilLokTs;
    }

    public void setBilLokTs(ZonedDateTime bilLokTs) {
        this.bilLokTs = bilLokTs;
    }

    public ZonedDateTime getBtpNxtSttDt() {
        return btpNxtSttDt;
    }

    public void setBtpNxtSttDt(ZonedDateTime btpNxtSttDt) {
        this.btpNxtSttDt = btpNxtSttDt;
    }

    public String getBilPresentmentCd() {
        return bilPresentmentCd;
    }

    public void setBilPresentmentCd(String bilPresentmentCd) {
        this.bilPresentmentCd = bilPresentmentCd;
    }

    public String getAppMigrationCd() {
        return appMigrationCd;
    }

    public void setAppMigrationCd(String appMigrationCd) {
        this.appMigrationCd = appMigrationCd;
    }

    public String getBilSttCd() {
        return bilSttCd;
    }

    public void setBilSttCd(String bilSttCd) {
        this.bilSttCd = bilSttCd;
    }

    public String getBtpTakeSusCd() {
        return btpTakeSusCd;
    }

    public void setBtpTakeSusCd(String btpTakeSusCd) {
        this.btpTakeSusCd = btpTakeSusCd;
    }

}
