package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import billing.data.entity.id.BilUnIdCshHstId;

@Entity
@Table(name = "BIL_UNID_CSH_HST")
public class BilUnidCshHst implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilUnIdCshHstId bilUnIdCshHstId;

    @Column(name = "BIL_ENTRY_SEQ_NBR", length = 5, nullable = false)
    private short bilEntrySeqNbr = SHORT_ZERO;

    @Column(name = "BIL_ACCOUNT_NBR", length = 30, nullable = false)
    private String bilAccountNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_TTY_NBR", length = 20, nullable = false)
    private String bilTtyNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_AGT_ACT_NBR", length = 20, nullable = false)
    private String bilAgtActNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_ADDITIONAL_ID", length = 20, nullable = false)
    private String bilAdditionalId = SEPARATOR_BLANK;

    @Column(name = "POL_SYMBOL_CD", length = 3, nullable = true)
    private String polSymbolCd = SEPARATOR_BLANK;

    @Column(name = "POL_NBR", length = 25, nullable = false)
    private String polNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_ADJ_DUE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilAdjDueDt;

    @Column(name = "BIL_PBL_ITEM_CD", length = 3, nullable = false)
    private String bilPblItemCd = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_AMT", precision = 14, scale = 2, nullable = false)
    private double bilRctAmt;

    @Column(name = "BIL_RCT_TYPE_CD", length = 2, nullable = false)
    private String bilRctTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_ID", length = 10, nullable = false)
    private String bilRctId = SEPARATOR_BLANK;

    @Column(name = "BIL_DSP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilDspDt;

    @Column(name = "BIL_DSP_TYPE_CD", length = 2, nullable = false)
    private String bilDspTypeCd = SEPARATOR_BLANK;

    @Column(name = "BIL_DSP_REASON_CD", length = 3, nullable = false)
    private String bilDspReasonCd = SEPARATOR_BLANK;

    @Column(name = "BIL_PAYEE_CLT_ID", length = 20, nullable = false)
    private String bilPayeeCltId = SEPARATOR_BLANK;

    @Column(name = "BIL_PAYEE_ADR_SEQ", length = 5, nullable = false)
    private short bilPayeeAdrSeq = SHORT_ZERO;

    @Column(name = "BIL_MANUAL_SUS_IND", nullable = false)
    private char bilManualSusInd;

    @Column(name = "DWS_CK_DRF_NBR", length = 10, nullable = true)
    private int dwsCkDrfNbr;

    @Column(name = "BIL_DEPOSIT_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bilDepositDt;

    @Column(name = "BIL_CWA_ID", length = 10, nullable = false)
    private String bilCwaId = SEPARATOR_BLANK;

    @Column(name = "DDS_DSB_DT")
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ddsDsbDt;

    @Column(name = "DWS_STATUS_CD", nullable = false)
    private char dwsStatusCd = BLANK_CHAR;

    @Column(name = "BIL_DSB_ID", length = 20, nullable = false)
    private String bilDsbId = SEPARATOR_BLANK;

    @Column(name = "BIL_CHK_PRD_MTH_CD", nullable = false)
    private char bilChkPrdMthCd = BLANK_CHAR;

    @Column(name = "BIL_ACY_USER_ID", length = 8, nullable = false)
    private String activityUserId = SEPARATOR_BLANK;

    @Column(name = "BIL_RCT_CMT", length = 80, nullable = false)
    private String bilRctCmt = SEPARATOR_BLANK;

    public BilUnIdCshHstId getBilUnIdCshHstId() {
        return bilUnIdCshHstId;
    }

    public void setBilUnIdCshHstId(BilUnIdCshHstId bilUnIdCshHstId) {
        this.bilUnIdCshHstId = bilUnIdCshHstId;
    }

    public short getBilEntrySeqNbr() {
        return bilEntrySeqNbr;
    }

    public void setBilEntrySeqNbr(short bilEntrySeqNbr) {
        this.bilEntrySeqNbr = bilEntrySeqNbr;
    }

    public String getBilAccountNbr() {
        return bilAccountNbr;
    }

    public void setBilAccountNbr(String bilAccountNbr) {
        this.bilAccountNbr = bilAccountNbr;
    }

    public String getBilTtyNbr() {
        return bilTtyNbr;
    }

    public void setBilTtyNbr(String bilTtyNbr) {
        this.bilTtyNbr = bilTtyNbr;
    }

    public String getBilAgtActNbr() {
        return bilAgtActNbr;
    }

    public void setBilAgtActNbr(String bilAgtActNbr) {
        this.bilAgtActNbr = bilAgtActNbr;
    }

    public String getBilAdditionalId() {
        return bilAdditionalId;
    }

    public void setBilAdditionalId(String bilAdditionalId) {
        this.bilAdditionalId = bilAdditionalId;
    }

    public String getPolSymbolCd() {
        return polSymbolCd;
    }

    public void setPolSymbolCd(String polSymbolCd) {
        this.polSymbolCd = polSymbolCd;
    }

    public String getPolNbr() {
        return polNbr;
    }

    public void setPolNbr(String polNbr) {
        this.polNbr = polNbr;
    }

    public ZonedDateTime getBilAdjDueDt() {
        return bilAdjDueDt;
    }

    public void setBilAdjDueDt(ZonedDateTime bilAdjDueDt) {
        this.bilAdjDueDt = bilAdjDueDt;
    }

    public String getBilPblItemCd() {
        return bilPblItemCd;
    }

    public void setBilPblItemCd(String bilPblItemCd) {
        this.bilPblItemCd = bilPblItemCd;
    }

    public double getBilRctAmt() {
        return bilRctAmt;
    }

    public void setBilRctAmt(double bilRctAmt) {
        this.bilRctAmt = bilRctAmt;
    }

    public String getBilRctTypeCd() {
        return bilRctTypeCd;
    }

    public void setBilRctTypeCd(String bilRctTypeCd) {
        this.bilRctTypeCd = bilRctTypeCd;
    }

    public String getBilRctId() {
        return bilRctId;
    }

    public void setBilRctId(String bilRctId) {
        this.bilRctId = bilRctId;
    }

    public ZonedDateTime getBilDspDt() {
        return bilDspDt;
    }

    public void setBilDspDt(ZonedDateTime bilDspDt) {
        this.bilDspDt = bilDspDt;
    }

    public String getBilDspTypeCd() {
        return bilDspTypeCd;
    }

    public void setBilDspTypeCd(String bilDspTypeCd) {
        this.bilDspTypeCd = bilDspTypeCd;
    }

    public String getBilDspReasonCd() {
        return bilDspReasonCd;
    }

    public void setBilDspReasonCd(String bilDspReasonCd) {
        this.bilDspReasonCd = bilDspReasonCd;
    }

    public String getBilPayeeCltId() {
        return bilPayeeCltId;
    }

    public void setBilPayeeCltId(String bilPayeeCltId) {
        this.bilPayeeCltId = bilPayeeCltId;
    }

    public short getBilPayeeAdrSeq() {
        return bilPayeeAdrSeq;
    }

    public void setBilPayeeAdrSeq(short bilPayeeAdrSeq) {
        this.bilPayeeAdrSeq = bilPayeeAdrSeq;
    }

    public char getBilManualSusInd() {
        return bilManualSusInd;
    }

    public void setBilManualSusInd(char bilManualSusInd) {
        this.bilManualSusInd = bilManualSusInd;
    }

    public int getDwsCkDrfNbr() {
        return dwsCkDrfNbr;
    }

    public void setDwsCkDrfNbr(int dwsCkDrfNbr) {
        this.dwsCkDrfNbr = dwsCkDrfNbr;
    }

    public ZonedDateTime getBilDepositDt() {
        return bilDepositDt;
    }

    public void setBilDepositDt(ZonedDateTime bilDepositDt) {
        this.bilDepositDt = bilDepositDt;
    }

    public String getBilCwaId() {
        return bilCwaId;
    }

    public void setBilCwaId(String bilCwaId) {
        this.bilCwaId = bilCwaId;
    }

    public ZonedDateTime getDdsDsbDt() {
        return ddsDsbDt;
    }

    public void setDdsDsbDt(ZonedDateTime ddsDsbDt) {
        this.ddsDsbDt = ddsDsbDt;
    }

    public char getDwsStatusCd() {
        return dwsStatusCd;
    }

    public void setDwsStatusCd(char dwsStatusCd) {
        this.dwsStatusCd = dwsStatusCd;
    }

    public String getBilDsbId() {
        return bilDsbId;
    }

    public void setBilDsbId(String bilDsbId) {
        this.bilDsbId = bilDsbId;
    }

    public char getBilChkPrdMthCd() {
        return bilChkPrdMthCd;
    }

    public void setBilChkPrdMthCd(char bilChkPrdMthCd) {
        this.bilChkPrdMthCd = bilChkPrdMthCd;
    }

    public String getActivityUserId() {
        return activityUserId;
    }

    public void setActivityUserId(String activityUserId) {
        this.activityUserId = activityUserId;
    }

    public String getBilRctCmt() {
        return bilRctCmt;
    }

    public void setBilRctCmt(String bilRctCmt) {
        this.bilRctCmt = bilRctCmt;
    }
}
