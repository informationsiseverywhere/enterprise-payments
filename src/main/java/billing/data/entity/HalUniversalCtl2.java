package billing.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;

import billing.data.entity.id.HalUniversalCtl2Id;

/**
 * The persistent class for the HAL_UNIVERSAL_CTL2 database table.
 *
 */
@Entity
@Table(name = "HAL_UNIVERSAL_CTL2")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HalUniversalCtl2 extends ResourceSupport implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private HalUniversalCtl2Id halUniversalCtl2Id;

    @Column(name = "HUC2_ENTRY_DTA_TXT", nullable = false, length = 200)
    private String entryData;

    public HalUniversalCtl2() {
        super();
    }

    public HalUniversalCtl2(HalUniversalCtl2Id halUniversalCtl2Id, String entryData) {
        super();
        this.halUniversalCtl2Id = halUniversalCtl2Id;
        this.entryData = entryData;
    }

    public HalUniversalCtl2Id getHalUniversalCtl2Id() {
        return halUniversalCtl2Id;
    }

    public void setHalUniversalCtl2Id(HalUniversalCtl2Id halUniversalCtl2Id) {
        this.halUniversalCtl2Id = halUniversalCtl2Id;
    }

    public String getEntryData() {
        return entryData != null ? entryData.trim() : entryData;
    }

    public void setEntryData(String entryData) {
        this.entryData = entryData;
    }

}