package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.DECIMAL_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import billing.data.entity.id.BilEftPlnCrgId;

@Entity
@Table(name = "BIL_EFT_PLN_CRG")
public class BilEftPlnCrg implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilEftPlnCrgId bilEftPlnCrgId;

    @Column(name = "BIL_EFT_SCG_IND", nullable = false)
    private char bilEftScgInd = BLANK_CHAR;

    @Column(name = "BIL_SVC_CRG_AMT", precision = 14, scale = 2, nullable = false)
    private double bilSvcCrgAmt = DECIMAL_ZERO;

    @Column(name = "LAST_MDF_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime lastMdfAcyTs;

    public BilEftPlnCrgId getBilEftPlnCrgId() {
        return bilEftPlnCrgId;
    }

    public void setBilEftPlnCrgId(BilEftPlnCrgId bilEftPlnCrgId) {
        this.bilEftPlnCrgId = bilEftPlnCrgId;
    }

    public char getBilEftScgInd() {
        return bilEftScgInd;
    }

    public void setBilEftScgInd(char bilEftScgInd) {
        this.bilEftScgInd = bilEftScgInd;
    }

    public double getBilSvcCrgAmt() {
        return bilSvcCrgAmt;
    }

    public void setBilSvcCrgAmt(double bilSvcCrgAmt) {
        this.bilSvcCrgAmt = bilSvcCrgAmt;
    }

    public ZonedDateTime getLastMdfAcyTs() {
        return lastMdfAcyTs;
    }

    public void setLastMdfAcyTs(ZonedDateTime lastMdfAcyTs) {
        this.lastMdfAcyTs = lastMdfAcyTs;
    }

}
