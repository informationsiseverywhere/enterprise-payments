package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilCrcrdArspDesId;

@Entity
@Table(name = "BIL_CRCRD_ARSP_DES")
public class BilCrcrdArspDes implements Serializable {

    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilCrcrdArspDesId bilCrcrdArspDesId;

    @Column(name = "BAR_CRCRD_ARSP_DSY", length = 3, nullable = false)
    private String barCrcrdArspDsy = SEPARATOR_BLANK;

    @Column(name = "BAR_CRCRD_ARSP_DES", length = 22, nullable = false)
    private String barCrcrdArspDes = SEPARATOR_BLANK;

    public BilCrcrdArspDesId getBilCrcrdArspDesId() {
        return bilCrcrdArspDesId;
    }

    public void setBilCrcrdArspDesId(BilCrcrdArspDesId bilCrcrdArspDesId) {
        this.bilCrcrdArspDesId = bilCrcrdArspDesId;
    }

    public String getBarCrcrdArspDsy() {
        return barCrcrdArspDsy;
    }

    public void setBarCrcrdArspDsy(String barCrcrdArspDsy) {
        this.barCrcrdArspDsy = barCrcrdArspDsy;
    }

    public String getBarCrcrdArspDes() {
        return barCrcrdArspDes;
    }

    public void setBarCrcrdArspDes(String barCrcrdArspDes) {
        this.barCrcrdArspDes = barCrcrdArspDes;
    }

}
