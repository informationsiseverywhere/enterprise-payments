package billing.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.ExchangeRateId;

@Entity
@Table(name = "EXCHANGE_RATE")
public class ExchangeRate implements Serializable {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ExchangeRateId exchangeRateId;

    @Column(name = "RATE", precision = 14, scale = 8, nullable = false)
    private double rate;

    public ExchangeRateId getExchangeRateId() {
        return exchangeRateId;
    }

    public void setExchangeRateId(ExchangeRateId exchangeRateId) {
        this.exchangeRateId = exchangeRateId;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

}
