package billing.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.CustomerSubscriptionProfileId;


@Entity
@Table(name = "CSR_SCR_PFL")
public class CustomerSubscriptionProfile implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private CustomerSubscriptionProfileId customerSubscriptionProfileId;

    @Column(name = "CSR_ID", length = 255, nullable = false)
    private String customerId = SEPARATOR_BLANK;

    @Column(name = "PMT_PFL_ID", length = 512, nullable = false)
    private String paymentProfileId = SEPARATOR_BLANK;

    public CustomerSubscriptionProfileId getCustomerSubscriptionProfileId() {
        return customerSubscriptionProfileId;
    }

    public void setCustomerSubscriptionProfileId(CustomerSubscriptionProfileId customerSubscriptionProfileId) {
        this.customerSubscriptionProfileId = customerSubscriptionProfileId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPaymentProfileId() {
        return paymentProfileId;
    }

    public void setPaymentProfileId(String paymentProfileId) {
        this.paymentProfileId = paymentProfileId;
    }

}
