package billing.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import billing.data.entity.id.BilEftBankId;

@Entity
@Table(name = "BIL_EFT_BANK")
public class BilEftBank implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BilEftBankId bilEftBankId;

    @Column(name = "BIL_BANK_ACCT_NBR", length = 25, nullable = false)
    private String bilBankAcctNbr = SEPARATOR_BLANK;

    @Column(name = "BIL_EFT_BANK_ID", length = 10, nullable = false)
    private int bilEftBankIdNbr = 0;

    @Column(name = "BEB_ACCOUNT_TYC", length = 3, nullable = false)
    private String bebAccountTyc = SEPARATOR_BLANK;

    @Column(name = "BEB_ACCOUNT_STA_CD", nullable = false)
    private char bebAccountStaCd = BLANK_CHAR;

    @Column(name = "BEB_AUT_IND", nullable = false)
    private char bebAutInd = BLANK_CHAR;

    @Column(name = "BEB_AUT_AMT", precision = 14, scale = 2, nullable = false)
    private double bebAutAmt = DECIMAL_ZERO;

    @Column(name = "BEB_WEB_AUT_IND", nullable = false)
    private char bebWebAutInd = BLANK_CHAR;

    @Column(name = "BIL_CRCRD_TYPE_CD", nullable = false)
    private char bilCrcrdTypeCd = BLANK_CHAR;

    @Column(name = "BIL_CRCRD_EXP_DT", length = 4, nullable = false)
    private String bilCrcrdExpDt = SEPARATOR_BLANK;

    @Column(name = "BEB_EFT_PAY_LNG_NM", length = 512, nullable = false)
    private String bebEftPayLngNm = SEPARATOR_BLANK;

    @Column(name = "BIL_CRCRD_PST_CD", length = 13, nullable = false)
    private String bilCrcrdPstCd = SEPARATOR_BLANK;

    public BilEftBankId getBilEftBankId() {
        return bilEftBankId;
    }

    public void setBilEftBankId(BilEftBankId bilEftBankId) {
        this.bilEftBankId = bilEftBankId;
    }

    public String getBilBankAcctNbr() {
        return bilBankAcctNbr;
    }

    public void setBilBankAcctNbr(String bilBankAcctNbr) {
        this.bilBankAcctNbr = bilBankAcctNbr;
    }

    public int getBilEftBankIdNbr() {
        return bilEftBankIdNbr;
    }

    public void setBilEftBankIdNbr(int bilEftBankIdNbr) {
        this.bilEftBankIdNbr = bilEftBankIdNbr;
    }

    public String getBebAccountTyc() {
        return bebAccountTyc;
    }

    public void setBebAccountTyc(String bebAccountTyc) {
        this.bebAccountTyc = bebAccountTyc;
    }

    public char getBebAccountStaCd() {
        return bebAccountStaCd;
    }

    public void setBebAccountStaCd(char bebAccountStaCd) {
        this.bebAccountStaCd = bebAccountStaCd;
    }

    public char getBebAutInd() {
        return bebAutInd;
    }

    public void setBebAutInd(char bebAutInd) {
        this.bebAutInd = bebAutInd;
    }

    public double getBebAutAmt() {
        return bebAutAmt;
    }

    public void setBebAutAmt(double bebAutAmt) {
        this.bebAutAmt = bebAutAmt;
    }

    public char getBebWebAutInd() {
        return bebWebAutInd;
    }

    public void setBebWebAutInd(char bebWebAutInd) {
        this.bebWebAutInd = bebWebAutInd;
    }

    public char getBilCrcrdTypeCd() {
        return bilCrcrdTypeCd;
    }

    public void setBilCrcrdTypeCd(char bilCrcrdTypeCd) {
        this.bilCrcrdTypeCd = bilCrcrdTypeCd;
    }

    public String getBilCrcrdExpDt() {
        return bilCrcrdExpDt;
    }

    public void setBilCrcrdExpDt(String bilCrcrdExpDt) {
        this.bilCrcrdExpDt = bilCrcrdExpDt;
    }

    public String getBebEftPayLngNm() {
        return bebEftPayLngNm;
    }

    public void setBebEftPayLngNm(String bebEftPayLngNm) {
        this.bebEftPayLngNm = bebEftPayLngNm;
    }

    public String getBilCrcrdPstCd() {
        return bilCrcrdPstCd;
    }

    public void setBilCrcrdPstCd(String bilCrcrdPstCd) {
        this.bilCrcrdPstCd = bilCrcrdPstCd;
    }

}
