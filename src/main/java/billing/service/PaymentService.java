package billing.service;

import java.time.ZonedDateTime;

import billing.model.Payment;
import billing.model.PaymentList;

public interface PaymentService {

    public PaymentList findPayments(int page, int size, String filters);

    public Payment findPayment(String batchId, short sequenceNumber, String postedDate, String user);

    public Payment getCurrency(Payment payment);

    public Payment makeAPayment(Payment payment);

    public String getExternalReferenceLink(String type, String accountId);

    public void insertAccountBalance(String accountId, String accountNumber, String bilTypeCode, char status,
            ZonedDateTime paymentDate);
}