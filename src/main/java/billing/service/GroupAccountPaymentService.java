package billing.service;

import billing.data.entity.BilThirdParty;

public interface GroupAccountPaymentService {

	public void applyUnidentifiedCash(BilThirdParty bilThirdParty);
}
