package billing.service;

public interface ContractResumeService {

    public void resumeContract(String accountId, String policyId, String polEffDt);

}
