package billing.service;

import billing.data.entity.BilRulesUct;

public interface BilRulesUctService {

    public BilRulesUct readBilRulesUct(String ruleCode, String billTypeCd, String billClassCd, String billPlanCd);
}
