package billing.service;

import core.options.model.SupportData;

public interface SupportDataService {

    public SupportData getSupportData(String parentCode);
}
