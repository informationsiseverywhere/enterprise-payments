package billing.service;

import java.time.ZonedDateTime;

import billing.data.entity.HalEventDtl;
import billing.model.Event;

public interface EventPostingService {

    public HalEventDtl postHalEventDtl(Event event);

    public void postDfrAcyQue(String halEventKeyId, String halEvtKeyTypCd, ZonedDateTime halEvtProcessTs);

    public void postEvent(Event event);
}
