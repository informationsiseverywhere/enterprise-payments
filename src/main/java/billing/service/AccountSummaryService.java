package billing.service;

import java.util.List;

import billing.model.AccountHistory;

public interface AccountSummaryService {

    public void writeAccountSummary(String accountId, List<AccountHistory> accountHistoryList);

}
