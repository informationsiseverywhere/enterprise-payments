package billing.service.impl;

import static billing.utils.BillingConstants.ERROR_CODE;
import static billing.utils.BillingConstants.STATEMENT_ROW;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_M;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_20;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_4;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.text.DecimalFormat;
import java.time.ZonedDateTime;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilBankEntry;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilEftExtPyt;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.BilInvTtyAct;
import billing.data.entity.BilMstCoSt;
import billing.data.entity.BilOrgSupport;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSchAutoReconCsh;
import billing.data.entity.BilThirdParty;
import billing.data.entity.BilTtyCashDsp;
import billing.data.entity.BilTtyCashRct;
import billing.data.entity.BilTtySummary;
import billing.data.entity.id.BilBankEntryId;
import billing.data.entity.id.BilCashEntryId;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilEftExtPytId;
import billing.data.entity.id.BilEftPendingTapeId;
import billing.data.entity.id.BilInvTtyActId;
import billing.data.entity.id.BilSchAutoReconCshId;
import billing.data.entity.id.BilTtyCashDspId;
import billing.data.entity.id.BilTtyCashRctId;
import billing.data.entity.id.BilTtySummaryId;
import billing.data.repository.BilBankEntryRepository;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilEftExtPytRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilInvTtyActRepository;
import billing.data.repository.BilMstCoStRepository;
import billing.data.repository.BilOrgSupportRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilSchAutoReconCshRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.data.repository.BilTtyCashDspRepository;
import billing.data.repository.BilTtyCashRctRepository;
import billing.data.repository.BilTtySummaryRepository;
import billing.model.Payment;
import billing.service.CredicardService;
import billing.service.PaymentService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.BilCashEntryMethodCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.PayableItemCode;
import billing.utils.BillingConstants.PaymentType;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import core.utils.IdGenerator;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;

@Service("groupPaymentService")
public class GroupPaymentServiceImpl {

    @Autowired
    private BilOrgSupportRepository bilOrgSupportRepository;

    @Autowired
    private BilTtyCashDspRepository bilTtyCashDspRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    @Autowired
    private BilTtyCashRctRepository bilTtyCashRctRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilTtySummaryRepository bilTtySummaryRepository;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private BilInvTtyActRepository bilInvTtyActRepository;

    @Autowired
    private BilBankEntryRepository bilBankEntryRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilMstCoStRepository bilMstCoStRepository;

    @Autowired
    private BilSchAutoReconCshRepository bilSchAutoReconCshRepository;

    @Autowired
    private DateService dateService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private CredicardService credicardService;

    @Autowired
    private BilEftExtPytRepository bilEftExtPytRepository;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    @Value("${onetimepayment.integration}")
    String oneTimePaymentIntegration;

    @Value("${paymentIntegration.provider}")
    String providerIntegration;

    public Payment makeAPayment(Payment payment) {

        boolean isDueDateDefaulted = false;
        boolean isPaymentIntegration = false;
        boolean isOneTimePaymentIntegration = false;

        if (StringUtils.isNotBlank(providerIntegration)) {
            isPaymentIntegration = true;
            isOneTimePaymentIntegration = BooleanUtils.toBoolean(oneTimePaymentIntegration);
        }

        if (payment.getDueDate() == null || payment.getDueDate().equals(DateRoutine.defaultDateTime())) {
            payment.setDueDate(DateRoutine.defaultDateTime());
            isDueDateDefaulted = true;
        }

        ZonedDateTime se3DateTime = dateService.currentDate();
        short fileTypeCode = 0;

        String bilBankCode;
        String bilEntryNumber;
        Short bilDtbSeqNbr;
        char arspCode;
        String autApv;
        String crcrdArspTyc;
        Integer bilCashEntrySeqNbr = 0;
        String orderId;

        bilEntryNumber = getUniqueBilEntryNumber(se3DateTime);
        payment.setUserId(execContext.getUserSeqeunceId());
        bilCashEntrySeqNbr = getBilEntrySequenceNumber(bilEntryNumber, se3DateTime, payment.getUserId());
        payment.setBatchId(bilEntryNumber);
        payment.setPaymentSequenceNumber(bilCashEntrySeqNbr.shortValue());

        BilThirdParty bilThirdParty = bilThirdPartyRepository.findById(payment.getAccountId()).orElse(null);
        String currencyCode = multiCurrencyService.getCurrencyCode(bilThirdParty.getCurrencyCd().trim());
        if (payment.getTransactionId() == null) {
            orderId = generateOrderId(bilThirdParty.getBilTtyNbr().trim(), payment.getPaymentType());
        } else {
            orderId = payment.getTransactionId();
        }

        if (payment.getAuthorization() == null) {
            autApv = BLANK_STRING;
        } else {
            autApv = payment.getAuthorization();
        }
        if (payment.getAuthorizationResponse() == null) {
            arspCode = BLANK_CHAR;
        } else {
            arspCode = payment.getAuthorizationResponse().charAt(SHORT_ZERO);
        }

        if (payment.getAuthorizationResponseType() == null) {
            crcrdArspTyc = BLANK_STRING;
        } else {
            crcrdArspTyc = payment.getAuthorizationResponseType();
        }

        bilDtbSeqNbr = getBilDtbSeqNbr(payment.getAccountId(), se3DateTime);
        if (!isDueDateDefaulted) {
            BilInvTtyAct bilInvTtyAct = bilInvTtyActRepository.getRow(payment.getAccountId(), CHAR_T,
                    payment.getDueDate(), STATEMENT_ROW);
            if (bilInvTtyAct != null) {
                boolean isStatementPaid = isStatementPaid(payment.getAccountId(), CHAR_T, SEPARATOR_BLANK,
                        bilInvTtyAct.getBilInvTtyActId().getBilInvDt(), STATEMENT_ROW);
                if (isStatementPaid) {
                    throw new InvalidDataException("reconciliation.statement.already.paid");
                }
            }
        }

        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {

            fileTypeCode = determineFileTypeCode(isOneTimePaymentIntegration ? payment.getPaymentType() : BLANK_STRING,
                    BilDesReasonType.ONE_XA.getValue());

            bilBankCode = getBankCode(BLANK_STRING, "ACH", fileTypeCode);

            insertBilTtyCashReceipt(payment, se3DateTime, bilDtbSeqNbr, bilCashEntrySeqNbr, bilEntryNumber, bilBankCode,
                    autApv);

            insertBilTtyCashDsp(payment, se3DateTime, bilDtbSeqNbr);

            insertBilSchAutoReconCsh(payment, se3DateTime, bilThirdParty.getBilTypeCd().trim(),
                    bilThirdParty.getBilClassCd().trim(), bilDtbSeqNbr);

            insertBilTtySummary(payment, se3DateTime, bilDtbSeqNbr);

            insertBilEFtPendingTape(payment, bilDtbSeqNbr, se3DateTime, fileTypeCode, crcrdArspTyc, orderId);

            updateBilInvTtyAct(payment);

            insertBilBankEntry(payment, bilCashEntrySeqNbr, bilEntryNumber, se3DateTime);

            insertBilCashEntryTotal(payment, se3DateTime, bilEntryNumber, bilBankCode, currencyCode);

            insertBilCashEntry(payment, se3DateTime, bilEntryNumber, bilCashEntrySeqNbr,
                    bilThirdParty.getBilTtyNbr().trim(), arspCode, autApv, crcrdArspTyc, orderId);

            paymentService.insertAccountBalance(bilThirdParty.getBillThirdPartyId().trim(),
                    bilThirdParty.getBilTtyNbr().trim(), bilThirdParty.getBilTypeCd().trim(),
                    bilThirdParty.getBtpStatusCd(), se3DateTime);
        } else {
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
                if (!isPaymentIntegration) {
                    credicardService.processCreditRespone(payment);
                }
                fileTypeCode = getFileTypeCodeFromBilRuleUct(se3DateTime);
                bilBankCode = getBankCode(payment.getPaymentType(), BilDesReasonType.ECH.getValue(), fileTypeCode);
            } else {
                fileTypeCode = determineFileTypeCode(payment.getPaymentType(), null);
                bilBankCode = getBankCode(BLANK_STRING, BilDesReasonType.DIGITAL_WALLET_HOUSE.getValue(), fileTypeCode);
            }

            insertBilCashEntryTotal(payment, se3DateTime, bilEntryNumber, bilBankCode, currencyCode);

            insertBilCashEntry(payment, se3DateTime, bilEntryNumber, bilCashEntrySeqNbr,
                    bilThirdParty.getBilTtyNbr().trim(), arspCode, autApv, crcrdArspTyc, orderId);

            insertBilTtyCashReceipt(payment, se3DateTime, bilDtbSeqNbr, bilCashEntrySeqNbr, bilEntryNumber, bilBankCode,
                    autApv);

            insertBilTtyCashDsp(payment, se3DateTime, bilDtbSeqNbr);

            insertBilEFtPendingTape(payment, bilDtbSeqNbr, se3DateTime, fileTypeCode, crcrdArspTyc, orderId);

            insertBilTtySummary(payment, se3DateTime, bilDtbSeqNbr);

            paymentService.insertAccountBalance(bilThirdParty.getBillThirdPartyId().trim(),
                    bilThirdParty.getBilTtyNbr().trim(), bilThirdParty.getBilTypeCd().trim(),
                    bilThirdParty.getBtpStatusCd(), se3DateTime);

            if (!isDueDateDefaulted) {
                updateBilInvTtyAct(payment);
            }

            updateBilCashEntryTotal(se3DateTime, bilEntryNumber, payment.getUserId());

        }

        callFinancialApi(payment.getPaymentAmount(), dateService.currentDate(), bilThirdParty.getBillThirdPartyId(),
                payment.getPaymentSequenceNumber(), bilBankCode, se3DateTime, bilEntryNumber, payment.getUserId(),
                payment.getIdentifier(), BilCashEntryMethodCode.CASH.getValue(), PayableItemCode.SUS.getValue(),
                BLANK_STRING, payment.getPaymentMethod(), payment.getPaymentType(), payment.getSuspenseReason());

        payment.setPostedDate(se3DateTime);
        payment.setUserId(execContext.getUserId());
        payment.setIsUnidentified(false);
        return payment;

    }

    private void insertBilEftExtPayment(Payment payment, BilEftPendingTape bilEftPendingTape) {
        BilEftExtPyt bilEftExtPyt = new BilEftExtPyt();
        BilEftExtPytId bilEftExtPytId = new BilEftExtPytId();
        bilEftExtPytId.setBilTchKeyId(payment.getAccountId());
        bilEftExtPytId.setBilTchKeyTypCd(bilEftPendingTape.getBilEftPendingTapeId().getTechnicalKeyType());
        bilEftExtPytId.setBilEftTapeDt(bilEftPendingTape.getBilEftPendingTapeId().getEftTapeDate());
        bilEftExtPytId.setBilEftAcyDt(bilEftPendingTape.getBilEftPendingTapeId().getEftActivityDate());
        bilEftExtPytId.setBilSeqNbr(bilEftPendingTape.getBilEftPendingTapeId().getSequenceNbr());
        bilEftExtPyt.setBilWalTypeCd(payment.getWalletType());
        bilEftExtPyt.setBilWalIdentifier(payment.getWalletIdentifier());
        bilEftExtPyt.setBilEftExtPytId(bilEftExtPytId);
        bilEftExtPytRepository.saveAndFlush(bilEftExtPyt);
    }

    private String getUniqueBilEntryNumber(ZonedDateTime se3DateTime) {
        String bilEntryNumber;
        do {
            bilEntryNumber = generateUniqueId(GENERATED_ID_LENGTH_4);
        } while (bilCashEntryTotRepository.getBilEntryNumber(se3DateTime, bilEntryNumber,
                execContext.getUserSeqeunceId()) != 0);
        return bilEntryNumber;
    }

    private String generateUniqueId(int generatedIdLength) {
        return IdGenerator.generateUniqueIdUpperCase(generatedIdLength);
    }

    private Integer getBilEntrySequenceNumber(String bilEntryNumber, ZonedDateTime se3DateTime, String userId) {
        Integer bilEntrySequenceNumber = bilCashEntryRepository.getMaxBilEntrySeq(bilEntryNumber, se3DateTime, userId);
        if (bilEntrySequenceNumber == null) {
            bilEntrySequenceNumber = 0;
        } else {
            bilEntrySequenceNumber = bilEntrySequenceNumber + 1;
        }
        return bilEntrySequenceNumber;
    }

    private short determineFileTypeCode(String paymentType, String bilDesReasonCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(paymentType,
                BilDesReasonType.FTC.getValue(), execContext.getLanguage());
        if (busCdTranslation == null) {
            busCdTranslation = busCdTranslationRepository.findByCodeAndType(bilDesReasonCode,
                    BilDesReasonType.FTC.getValue(), execContext.getLanguage());
        }
        if (busCdTranslation == null) {
            throw new InvalidDataException("bankCode.invalid");
        }
        return Short.parseShort(busCdTranslation.getBusDescription().trim());
    }

    private void insertBilCashEntry(Payment payment, ZonedDateTime se3DateTime, String bilEntryNumber,
            Integer bilEntrySequenceNumber, String bilAccountNumber, char arspCode, String autApv, String crcrdArspTyc,
            String orderId) {
        BilCashEntry bilCashEntry = new BilCashEntry();
        bilCashEntry.setBilCashEntryId(new BilCashEntryId(se3DateTime, bilEntryNumber, payment.getUserId(),
                bilEntrySequenceNumber.shortValue()));
        bilCashEntry.setBilAccountNbr(SEPARATOR_BLANK);
        bilCashEntry.setBilTtyNbr(bilAccountNumber);

        bilCashEntry.setBilAgtActNbr(SEPARATOR_BLANK);
        bilCashEntry.setBilAdditionalId(payment.getAdditionalId());
        bilCashEntry.setPolSymbolCd(payment.getPolicySymbol());
        bilCashEntry.setPolNbr(payment.getPolicyNumber());
        bilCashEntry.setBilPblItemCd(payment.getPayableItem());
        bilCashEntry.setBilAdjDueDt(payment.getDueDate());
        bilCashEntry.setBilRctAmt(payment.getPaymentAmount());
        bilCashEntry.setBilRctTypeCd(payment.getPaymentType());
        bilCashEntry.setBilSusReasonCd(SEPARATOR_BLANK);
        bilCashEntry.setBilManualSusInd(CHAR_N);
        bilCashEntry.setBilRctId(payment.getPaymentId());
        bilCashEntry.setBilRctCltId(SEPARATOR_BLANK);
        bilCashEntry.setBilRctAdrSeq(SHORT_ZERO);
        bilCashEntry.setBilRctReceiveDt(se3DateTime);
        bilCashEntry.setBilTaxYearNbr((short) se3DateTime.getYear());
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            bilCashEntry.setBilCrcrdTypeCd(BLANK_CHAR);
            bilCashEntry.setBilCrcrdArspCd(BLANK_CHAR);
            bilCashEntry.setBilCrcrdActNbr(SEPARATOR_BLANK);
            bilCashEntry.setBilCrcrdExpDt(SEPARATOR_BLANK);
            bilCashEntry.setBilCrcrdAutApv(SEPARATOR_BLANK);
            bilCashEntry.setBilCrcrdPstCd(SEPARATOR_BLANK);
        } else {
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
                bilCashEntry.setBilCrcrdTypeCd(payment.getCreditCardType().charAt(SHORT_ZERO));
                bilCashEntry.setBilCrcrdActNbr(payment.getCreditCardNumber());

            } else {
                bilCashEntry.setBilCrcrdTypeCd(payment.getWalletType().charAt(SHORT_ZERO));
                bilCashEntry.setBilCrcrdActNbr(payment.getWalletNumber());
            }
            bilCashEntry.setBilCrcrdArspCd(arspCode);
            bilCashEntry.setBilCrcrdExpDt(payment.getCreditCardExpiryDate());
            bilCashEntry.setBilCrcrdAutApv(autApv);
            bilCashEntry.setBilCrcrdPstCd(payment.getPostalCode());
        }

        bilCashEntry.setBilCrcrdArspTyc(crcrdArspTyc);
        bilCashEntry.setBilCwaId(SEPARATOR_BLANK);
        bilCashEntry.setBilPstMrkDt(se3DateTime);
        bilCashEntry.setBilPstMrkCd(BLANK_CHAR);
        bilCashEntry.setBilRctCmt(SEPARATOR_BLANK);
        bilCashEntry.setBecChgUserId(payment.getUserId());
        bilCashEntry.setAgencyNbr(SEPARATOR_BLANK);

        bilCashEntry.setInsertedRowTs(dateService.currentDateTime());
        bilCashEntry.setBilPmtOrderId(orderId);
        bilCashEntryRepository.save(bilCashEntry);
    }

    private Short getBilDtbSeqNbr(String accountId, ZonedDateTime se3DateTime) {
        Short bilDtbSeqNbr = 0;
        bilDtbSeqNbr = bilTtyCashRctRepository.getMaxBilDtbSequenceNumber(accountId, se3DateTime);
        if (bilDtbSeqNbr == null) {
            bilDtbSeqNbr = 0;
        } else {
            bilDtbSeqNbr = (short) (bilDtbSeqNbr + 1);
        }
        return bilDtbSeqNbr;
    }

    private String getBankCode(String paymentType, String bilDesReasonType, Short fileType) {

        short fileTypeCode = 0;

        if (!paymentType.equals(BLANK_STRING)) {
            fileTypeCode = determineFileTypeCode(paymentType, "CPY");
        } else {
            fileTypeCode = fileType;
        }

        BilOrgSupport bilOrgSupport = bilOrgSupportRepository.getByBilFileTypeCd(fileTypeCode, bilDesReasonType);
        if (bilOrgSupport != null) {
            return bilOrgSupport.getBilBankCd().trim();
        }
        return SEPARATOR_BLANK;
    }

    private void insertBilCashEntryTotal(Payment payment, ZonedDateTime se3DateTime, String bilEntryNumber,
            String bilBankCode, String currencyCode) {
        BilCashEntryTot bilCashEntryTot = new BilCashEntryTot();
        bilCashEntryTot.setBilCashEntryTotId(new BilCashEntryTotId(se3DateTime, bilEntryNumber, payment.getUserId()));
        bilCashEntryTot.setBctTotCashAmt(payment.getPaymentAmount());
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            bilCashEntryTot.setBctDepositInd(CHAR_Y);
            bilCashEntryTot.setBctAcceptInd(CHAR_Y);
            bilCashEntryTot.setBilBankCd(bilBankCode);
            bilCashEntryTot.setBctDepBankCd(bilCashEntryTot.getBilBankCd());
            bilCashEntryTot.setBilDepositDt(se3DateTime);
        } else {
            bilCashEntryTot.setBctDepositInd(CHAR_N);
            bilCashEntryTot.setBctAcceptInd(CHAR_Y);
            bilCashEntryTot.setBilBankCd(bilBankCode);
            bilCashEntryTot.setBctDepBankCd(SEPARATOR_BLANK);
            bilCashEntryTot.setBilDepositDt(DateRoutine.defaultDateTime());
        }
        bilCashEntryTot.setBctAcceptDt(se3DateTime);
        bilCashEntryTot.setBilCshEtrMthCd(payment.getPaymentMethod().charAt(0));
        bilCashEntryTot.setCurrencyCd(currencyCode == null ? SEPARATOR_BLANK : currencyCode);
        bilCashEntryTotRepository.save(bilCashEntryTot);
    }

    private void insertBilTtyCashReceipt(Payment payment, ZonedDateTime se3DateTime, Short bilDtbSeqNbr,
            Integer bilTtyCashRctSeqNbr, String bilEntryNumber, String bilBanckCode, String autApv) {
        BilTtyCashRct bilTtyCashRct = new BilTtyCashRct();
        bilTtyCashRct.setBilTtyCashRctId(new BilTtyCashRctId(payment.getAccountId(), se3DateTime, bilDtbSeqNbr));
        bilTtyCashRct.setBilPstmrkCd(BLANK_CHAR);
        bilTtyCashRct.setBilEntryDt(se3DateTime);
        bilTtyCashRct.setBilEntryNbr(bilEntryNumber);
        bilTtyCashRct.setBilEntrySeqNbr(bilTtyCashRctSeqNbr.shortValue());
        bilTtyCashRct.setUserId(payment.getUserId());
        bilTtyCashRct.setBilRctAmt(payment.getPaymentAmount());
        bilTtyCashRct.setBilRctTypeCd(payment.getPaymentType());
        bilTtyCashRct.setBilRctId(payment.getPaymentId());
        bilTtyCashRct.setBilRctCmt(payment.getPaymentComments());
        bilTtyCashRct.setBilToFroTrfNbr(SEPARATOR_BLANK);
        bilTtyCashRct.setBilTrfTypeCd(BLANK_CHAR);
        bilTtyCashRct.setBilRctReceiveDt(se3DateTime);
        bilTtyCashRct.setBilBankCd(bilBanckCode);
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            bilTtyCashRct.setBilCrcrdTypeCd(BLANK_CHAR);
            bilTtyCashRct.setBilCrcrdActNbr(SEPARATOR_BLANK);
            bilTtyCashRct.setBilCrcrdExpDt(SEPARATOR_BLANK);
            bilTtyCashRct.setBilCrcrdAutApv(SEPARATOR_BLANK);
        } else {
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
                bilTtyCashRct.setBilCrcrdTypeCd(payment.getCreditCardType().charAt(0));
                bilTtyCashRct.setBilCrcrdActNbr(payment.getCreditCardNumber());
                bilTtyCashRct.setBilCrcrdExpDt(payment.getCreditCardExpiryDate().substring(0, 4));

            } else {
                bilTtyCashRct.setBilCrcrdTypeCd(payment.getWalletType().charAt(0));
                bilTtyCashRct.setBilCrcrdActNbr(payment.getWalletNumber());
                bilTtyCashRct.setBilCrcrdExpDt(payment.getCreditCardExpiryDate());
            }
            bilTtyCashRct.setBilCrcrdAutApv(autApv);
        }
        bilTtyCashRct.setBilCshEtrMthCd(payment.getPaymentMethod().charAt(SHORT_ZERO));
        bilTtyCashRct.setBilTaxYearNbr((short) DateRoutine.defaultDateTime().getYear());
        bilTtyCashRct.setBilPstmrkDt(se3DateTime);
        bilTtyCashRct.setBilPstmrkCd(BLANK_CHAR);
        bilTtyCashRct.setBilDepositDt(DateRoutine.defaultDateTime());
        bilTtyCashRctRepository.save(bilTtyCashRct);
    }

    private void insertBilTtyCashDsp(Payment payment, ZonedDateTime se3DateTime, Short bilDtbSeqNbr) {
        BilTtyCashDsp bilTtyCashDsp = new BilTtyCashDsp();
        bilTtyCashDsp
                .setBilTtyCashDspId(new BilTtyCashDspId(payment.getAccountId(), se3DateTime, bilDtbSeqNbr, SHORT_ZERO));
        bilTtyCashDsp.setBilAdjDueDt(payment.getDueDate());
        bilTtyCashDsp.setUserId(payment.getUserId());
        bilTtyCashDsp.setBilDspDt(se3DateTime);
        bilTtyCashDsp.setBilDspTypeCd("SP");
        bilTtyCashDsp.setBilDspReasonCd(SEPARATOR_BLANK);
        bilTtyCashDsp.setBilDspAmt(payment.getPaymentAmount());
        bilTtyCashDsp.setBilPayeeCltId(SEPARATOR_BLANK);
        bilTtyCashDsp.setBilPayeeAdrSeq(SHORT_ZERO);
        bilTtyCashDsp.setBilManualSusInd(CHAR_N);
        bilTtyCashDsp.setDwsCkDrfNbr(SHORT_ZERO);
        bilTtyCashDsp.setBilRevsRsusInd(BLANK_CHAR);
        bilTtyCashDsp.setBilToFroTrfNbr(SEPARATOR_BLANK);
        bilTtyCashDsp.setBilTrfTypeCd(BLANK_CHAR);
        bilTtyCashDsp.setDwsStatusCd(BLANK_CHAR);
        bilTtyCashDsp.setBilDsbId(SEPARATOR_BLANK);
        bilTtyCashDsp.setBilChkPrdMthCd(BLANK_CHAR);
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            bilTtyCashDsp.setDdsDsbDt(DateRoutine.defaultDateTime());
        } else {
            bilTtyCashDsp.setDdsDsbDt(DateRoutine.dateTimeAsYYYYMMDD("0001-01-01"));
        }

        bilTtyCashDspRepository.save(bilTtyCashDsp);

    }

    private void insertBilEFtPendingTape(Payment payment, Short bilDtbSeqNbr, ZonedDateTime se3DateTime,
            short fileTypeCode, String crcrdArspTyc, String orderId) {

        Short bilSequenceNumber = 0;
        BilEftPendingTape bilEftPendingTape = new BilEftPendingTape();
        BilEftPendingTapeId bilEftPendingTapeId = null;
        bilSequenceNumber = getBilSequenceNumber(payment.getAccountId(), CHAR_T, se3DateTime);
        bilEftPendingTapeId = new BilEftPendingTapeId(payment.getAccountId(), CHAR_T, se3DateTime, se3DateTime,
                bilSequenceNumber);

        bilEftPendingTape.setBilEftPendingTapeId(bilEftPendingTapeId);
        bilEftPendingTape.setEftDrDate(se3DateTime);
        bilEftPendingTape.setEftDrAmount(payment.getPaymentAmount());
        bilEftPendingTape.setEftPostPaymentDate(se3DateTime);
        bilEftPendingTape.setEftNotIndicator(CHAR_N);
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
            bilEftPendingTape.setEftReceivedType(EftRecordType.RECONCILED_CASH.getValue());
            bilEftPendingTape.setBusinessActivityCd(getBusinessActivityCode(crcrdArspTyc));
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            bilEftPendingTape.setEftReceivedType(EftRecordType.ONE_TIME_ACH.getValue());
            bilEftPendingTape.setBusinessActivityCd(getBusinessActivityCode(payment.getAccountType()));
        } else {
            bilEftPendingTape.setEftReceivedType(EftRecordType.DIGITAL_WALLET.getValue());
            bilEftPendingTape.setBusinessActivityCd(getBusinessActivityCode(crcrdArspTyc));
        }
        bilEftPendingTape.setFileTypeCd(fileTypeCode);
        bilEftPendingTape.setDisbursementId(SEPARATOR_BLANK);
        bilEftPendingTape.setReceiptSequenceNumber(bilDtbSeqNbr);
        bilEftPendingTape.setWebAutomatedIndicator(CHAR_N);
        bilEftPendingTape.setAgencyNbr(SEPARATOR_BLANK);
        bilEftPendingTape.setInsertedRowTime(dateService.currentDateTime());
        bilEftPendingTape.setPaymentOrderId(orderId);
        bilEftPendingTapeRepository.save(bilEftPendingTape);

        if (StringUtils.isNotBlank(payment.getWalletIdentifier())) {
            insertBilEftExtPayment(payment, bilEftPendingTape);
        }

    }

    private Short getBilSequenceNumber(String accountId, char techKeyTypCd, ZonedDateTime se3DateTime) {
        Short bilSequenceNumber = bilEftPendingTapeRepository.getMaximunSequenceNumber(accountId, techKeyTypCd,
                se3DateTime, se3DateTime);
        if (bilSequenceNumber == null) {
            bilSequenceNumber = 0;
        } else {
            bilSequenceNumber = (short) (bilSequenceNumber + 1);
        }
        return bilSequenceNumber;
    }

    private void insertBilTtySummary(Payment payment, ZonedDateTime se3DateTime, Short bilDtbSeqNbr) {

        String xcce = "1XCCE";
        String pending = "Pending";

        Short bilAcySeq = getBilAcySeq(payment.getAccountId(), se3DateTime);
        BilTtySummary bilTtySummary = new BilTtySummary();
        BilTtySummaryId bilTtySummaryId = new BilTtySummaryId(payment.getAccountId(), se3DateTime, bilAcySeq);
        bilTtySummary.setBilTtySummaryId(bilTtySummaryId);
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
            bilTtySummary.setBilAcyDesCd(payment.getPaymentType());
            bilTtySummary.setBilDesReaTyp(BilDesReasonType.CPY.getValue());
            bilTtySummary.setBasAddDataTxt(xcce + SEPARATOR_BLANK + pending + SEPARATOR_BLANK + "#" + bilDtbSeqNbr + "#"
                    + DateRoutine.dateTimeAsYYYYMMDDString(se3DateTime) + payment.getBatchId()
                    + StringUtils.rightPad(payment.getUserId(), 8)
                    + StringUtils.leftPad(String.valueOf(payment.getPaymentSequenceNumber()), 4, CHAR_ZERO));
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            bilTtySummary.setBilAcyDesCd(payment.getPaymentType());
            bilTtySummary.setBilDesReaTyp(BilDesReasonType.ONE_XA.getValue());
            bilTtySummary.setBasAddDataTxt(pending);
        } else {
            bilTtySummary.setBilAcyDesCd(payment.getPaymentType());
            bilTtySummary.setBilDesReaTyp(BilDesReasonType.WALLET_PAYMENT_TYPE.getValue());
            bilTtySummary.setBasAddDataTxt(BilDesReasonType.WALLET_PAYMENT_TYPE.getValue() + SEPARATOR_BLANK + pending
                    + SEPARATOR_BLANK + "#" + bilDtbSeqNbr + "#" + DateRoutine.dateTimeAsYYYYMMDDString(se3DateTime)
                    + payment.getBatchId() + StringUtils.rightPad(payment.getUserId(), 8)
                    + StringUtils.leftPad(String.valueOf(payment.getPaymentSequenceNumber()), 4, CHAR_ZERO));
        }
        bilTtySummary.setBilAcyDes1Dt(DateRoutine.defaultDateTime());
        bilTtySummary.setBilAcyDes2Dt(DateRoutine.defaultDateTime());
        bilTtySummary.setBilAcyAmt(payment.getPaymentAmount());
        bilTtySummary.setUserId(payment.getUserId());
        bilTtySummary.setBilAcyTs(dateService.currentDateTime());

        bilTtySummaryRepository.saveAndFlush(bilTtySummary);

    }

    private Short getBilAcySeq(String accountId, ZonedDateTime se3DateTime) {
        Short bilAcySeq;

        bilAcySeq = bilTtySummaryRepository.getMaxSeqNumber(accountId, se3DateTime);

        if (bilAcySeq == null) {
            bilAcySeq = SHORT_ZERO;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        return bilAcySeq;
    }

    private void updateBilInvTtyAct(Payment payment) {
        BilInvTtyAct bilInvTtyAct = bilInvTtyActRepository.getRow(payment.getAccountId(), CHAR_T, payment.getDueDate(),
                STATEMENT_ROW);
        if (bilInvTtyAct != null && bilInvTtyAct.getBilSttReconInd() == CHAR_O) {
            bilInvTtyActRepository.updateRow(CHAR_M, payment.getAccountId(), CHAR_T, payment.getDueDate(),
                    STATEMENT_ROW);
        }

    }

    private void insertBilBankEntry(Payment payment, Integer cashRctSeqNbr, String bilEntryNumber,
            ZonedDateTime se3DateTime) {
        BilBankEntry bilBankEntry = new BilBankEntry();
        bilBankEntry.setBilBankEntryId(
                new BilBankEntryId(se3DateTime, bilEntryNumber, payment.getUserId(), cashRctSeqNbr.shortValue()));

        bilBankEntry.setBankAccountNbr(payment.getBankAccountNumber());
        bilBankEntry.setRoutingTransitNbr(payment.getRoutingNumber());
        bilBankEntry.setAccountTypCd(payment.getAccountType());
        bilBankEntry.setWebAuthInd(CHAR_N);
        bilBankEntry.setConfirmationNbr(generateConfirmationNumber());
        bilBankEntryRepository.save(bilBankEntry);

    }

    private String generateConfirmationNumber() {
        String uniqueId;
        do {
            uniqueId = generateUniqueId(GENERATED_ID_LENGTH_20);
        } while (bilBankEntryRepository.findByConfirmationNbr(uniqueId) != null);
        return uniqueId;
    }

    private void updateBilCashEntryTotal(ZonedDateTime se3Date, String bilEntryNumber, String userId) {
        bilCashEntryTotRepository.updateRow(se3Date, bilEntryNumber, userId);
    }

    private char getBusinessActivityCode(String crcrdArspTyc) {
        if (busCdTranslationRepository.findByCodeAndType(crcrdArspTyc, BusCodeTranslationType.CCD.getValue(),
                execContext.getLanguage()) != null) {
            return CHAR_Y;
        }
        return CHAR_N;
    }

    private short getFileTypeCodeFromBilRuleUct(ZonedDateTime se3DateTime) {
        final String STRING_1XCC = "1XCC";
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(STRING_1XCC, SEPARATOR_BLANK, SEPARATOR_BLANK,
                SEPARATOR_BLANK, se3DateTime, se3DateTime);
        if (bilRulesUct != null) {
            return new Short(bilRulesUct.getBrtParmListTxt());
        }
        return 0;
    }

    private String generateOrderId(String bilAccountNumber, String paymentType) {
        StringBuilder orderId = new StringBuilder();
        ZonedDateTime currentDateTime = dateService.currentDateTime();
        DecimalFormat df = new DecimalFormat("00");
        if (bilAccountNumber.length() < 8) {
            orderId.append(StringUtils.rightPad(bilAccountNumber, 8, "0"));
        } else {
            orderId.append(bilAccountNumber.substring(SHORT_ZERO, 8));
        }
        orderId.append(paymentType.substring(0, 1)).append(df.format(currentDateTime.getMonthValue()))
                .append(df.format(currentDateTime.getDayOfMonth())).append(df.format(currentDateTime.getHour()))
                .append(df.format(currentDateTime.getMinute())).append(df.format(currentDateTime.getSecond()))
                .append("X");
        return orderId.toString();
    }

    private boolean isStatementPaid(String accountId, char bilTechKeyTypeCode, String bilAccountId,
            ZonedDateTime invoiceDate, String statementRow) {
        BilInvTtyAct bilInvTtyAct = bilInvTtyActRepository
                .findById(new BilInvTtyActId(accountId, bilTechKeyTypeCode, bilAccountId, invoiceDate, statementRow))
                .orElse(null);
        return bilInvTtyAct != null && bilInvTtyAct.getBilSttReconInd() == CHAR_R;
    }

    private void callFinancialApi(Double amount, ZonedDateTime applicationDate, String accountId,
            short entrySequenceNumber, String bankCode, ZonedDateTime entryDate, String entryNumber, String userId,
            String accountNumber, String transactionObjectCode, String transactionActionCode, String paybleItemCode,
            String sourceCode, String paymentType, String bilReasonCode) {

        String masterCompany = BillingConstants.MASTER_COMPANY_NUMBER;
        String riskState = BLANK_STRING;
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        BilThirdParty bilThirdParty = bilThirdPartyRepository.findById(accountId).orElse(null);
        if (bilThirdParty == null) {
            throw new DataNotFoundException("no.data.found");
        }
        BilMstCoSt bilMstCoSt = bilMstCoStRepository.findRow(bilThirdParty.getBilClassCd(),
                bilThirdParty.getBilTypeCd(), BLANK_STRING);
        if (bilMstCoSt != null) {
            masterCompany = bilMstCoSt.getMasterCompanyNbr();
            riskState = bilMstCoSt.getBilStatePvnCd();
        }
        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(BLANK_STRING);
        financialApiActivity.setApplicationName(ApplicationName.BACPCCP.toString());
        financialApiActivity.setUserId(userId);
        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setAppProgramId(ApplicationProgramIdentifier.BACPTCCP.toString());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilTypeCode(bilThirdParty.getBilTypeCd());
        financialApiActivity.setBilClassCode(bilThirdParty.getBilClassCd());
        financialApiActivity.setPayableItemCode(paybleItemCode);
        financialApiActivity.setAgentTtyId(accountNumber);
        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setBilSourceCode(sourceCode);
        financialApiActivity.setMasterCompanyNbr(masterCompany);
        financialApiActivity.setBilReceiptTypeCode(paymentType);
        financialApiActivity.setStateCode(riskState);
        financialApiActivity.setBilDatabaseKey(getBilDatabaseKey(entryDate, entryNumber, userId, entrySequenceNumber));
        financialApiActivity.setStatusCode(BLANK_CHAR);
        financialApiActivity.setBilReasonCode(bilReasonCode);
        financialApiActivity.setSummaryEffectiveDate(applicationDate);
        financialApiActivity.setOperatorId(userId);
        financialApiActivity.setCurrencyCode(bilThirdParty.getCurrencyCd());

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {

            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

    private void insertBilSchAutoReconCsh(Payment payment, ZonedDateTime se3DateTime, String bilTypeCode,
            String bilClassCode, Short bilDtbSeqNbr) {
        BilRulesUct bilRulesUct = null;
        if (!bilClassCode.equals(BLANK_STRING)) {
            bilRulesUct = bilRulesUctRepository.getBilRulesRow("SPAR", bilTypeCode, bilClassCode, SEPARATOR_BLANK,
                    se3DateTime, se3DateTime);
        }
        if (bilRulesUct == null) {
            bilRulesUct = bilRulesUctRepository.getBilRulesRow("SPAR", bilTypeCode, SEPARATOR_BLANK, SEPARATOR_BLANK,
                    se3DateTime, se3DateTime);
        }

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            BilSchAutoReconCsh bilSchAutoReconCsh = new BilSchAutoReconCsh();
            Short sequenceNbr = bilSchAutoReconCshRepository.findMaxSequenceNumber(payment.getAccountId(), CHAR_T);
            if (sequenceNbr == null) {
                sequenceNbr = 0;
            } else {
                sequenceNbr = (short) (sequenceNbr + 1);
            }
            BilSchAutoReconCshId bilSchAutoReconCshId = new BilSchAutoReconCshId(payment.getAccountId(), CHAR_T,
                    sequenceNbr);
            bilSchAutoReconCsh.setBilSchAutoReconCshId(bilSchAutoReconCshId);
            bilSchAutoReconCsh.setBilEntryDt(se3DateTime);
            bilSchAutoReconCsh.setBilEntryNbr(payment.getBatchId());
            bilSchAutoReconCsh.setBilEntrySeqNbr(payment.getPaymentSequenceNumber());
            bilSchAutoReconCsh.setBilDtbDt(se3DateTime);
            bilSchAutoReconCsh.setProcessInd(CHAR_N);
            bilSchAutoReconCsh.setBilDtbSeqNbr(bilDtbSeqNbr);
            bilSchAutoReconCshRepository.save(bilSchAutoReconCsh);
        }
    }

    private String getBilDatabaseKey(ZonedDateTime entryDate, String entryNumber, String operatorId,
            short bilDtbSequenceNumber) {
        String seqNumberSign = "+";

        String dataString = DateRoutine.dateTimeAsYYYYMMDDString(entryDate);
        dataString += entryNumber;
        dataString += StringUtils.rightPad(operatorId, 8, BLANK_CHAR);
        dataString += seqNumberSign;
        dataString += StringUtils.leftPad(String.valueOf(bilDtbSequenceNumber), 5, CHAR_ZERO);

        return dataString;
    }

}
