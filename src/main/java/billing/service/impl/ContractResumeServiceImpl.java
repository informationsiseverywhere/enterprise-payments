package billing.service.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_C;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_F;
import static core.utils.CommonConstants.CHAR_H;
import static core.utils.CommonConstants.CHAR_I;
import static core.utils.CommonConstants.CHAR_K;
import static core.utils.CommonConstants.CHAR_L;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_V;
import static core.utils.CommonConstants.CHAR_W;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_Z;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActRules;
import billing.data.entity.BilPolActivity;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.id.BilActRulesId;
import billing.data.entity.id.BilPolActivityId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActRulesRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilAmountsRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.model.AccountHistory;
import billing.service.AccountSummaryService;
import billing.service.BilRulesUctService;
import billing.service.ContractResumeService;
import billing.utils.AccountOperationsWrapper;
import billing.utils.BillingConstants.ActivityType;
import billing.utils.BillingConstants.RuleId;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.schedule.service.ScheduleService;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;

@Service
public class ContractResumeServiceImpl implements ContractResumeService {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private DateService dateService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private AccountSummaryService accountSummaryService;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private BilAmountsRepository bilAmountsRepository;

    @Autowired
    private BilActRulesRepository bilActRulesRepository;

    private static final char CHAR_3 = '3';
    private static final char CHAR_4 = '4';
    private static final char CHAR_5 = '5';
    private static final char CHAR_6 = '6';
    private static final char CHAR_7 = '7';
    private static final String SINGLE_POLICY = "SP";
    private static final String REMAIN_WRO_CRG = "remainWriteOfAmountCharge";
    private static final String REMAIN_WRO_PRM = "remainWriteOfAmountPremium";
    private static final String WRO_PRM_AMT = "writeOffPremiumAmount";
    private static final String WRO_CRG_AMT = "writeOffChargeAmount";
    private static final String REV_WRO_PRM_AMT = "revWriteOffPremiumAmount";
    private static final String REV_WRO_CRG_AMT = "revWriteOffChargeAmount";
    private static final String INI_DATE = "iniBilAcyDate";
    private static final String INI_SEQUENCE = "initBilSequence";
    private static final String RNT_BAL_AMT = "policyRntBalanceAmount";
    private static final String POL_BAL_AMT = "policyRntBalanceAmount";
    private static final String SCG_BAL_AMT = "scgBalanceAmount";
    private static final String NO_DATA_FOUND = "no.data.found";
    private static final String SYSTEM = "SYSTEM";

    @Override
    public void resumeContract(String accountId, String policyId, String polEffDt) {
        BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
        if (bilAccount == null) {
            throw new DataNotFoundException("no.bil.account.exists", new Object[] { accountId });
        }
        BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository
                .findById(new BilPolicyTermId(accountId, policyId, DateRoutine.dateTimeAsYYYYMMDD(polEffDt)))
                .orElse(null);
        if (bilPolicyTerm == null) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }
        BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(policyId, accountId)).orElse(null);
        if (bilPolicy == null) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }

        if (bilPolicyTerm.getBillPolStatusCd() == CHAR_H) {
            scheduleBillingApi(bilAccount, bilPolicyTerm, bilPolicy, "COF", BLANK_STRING, BLANK_STRING, BLANK_STRING);
        } else if (bilPolicyTerm.getBillPolStatusCd() == CHAR_I) {
            double policyCollectionAmount = getPolicyCollectionAmount(accountId, policyId);
            if (policyCollectionAmount == DECIMAL_ZERO) {
                deleteBilPolActivityTriger(accountId, policyId, "BCL");
            }
            scheduleBillingApi(bilAccount, bilPolicyTerm, bilPolicy, "COF", "Y", BLANK_STRING, BLANK_STRING);
        }
        updateResumeNextDate(accountId, policyId, polEffDt);
        resumeAccount(accountId, polEffDt, bilPolicyTerm, bilPolicy);
        processPolicyTermResume(accountId, policyId, polEffDt);
    }

    private void processPolicyTermResume(String accountId, String policyId, String polEffDt) {
        BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository.findSuspendActivityData(accountId, policyId,
                DateRoutine.dateTimeAsYYYYMMDD(polEffDt), getSuspendStatusCode());
        if (bilPolicyTerm != null) {
            Double policyBalance = bilIstScheduleRepository.getLastPolicyBalance(accountId, policyId,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            if (policyBalance == null) {
                policyBalance = DECIMAL_ZERO;
            }
            char newStatus = getNewPolicyStatus(accountId, policyId, bilPolicyTerm, policyBalance);
            Integer quoteExisting = bilAmountsRepository.getMaxQuoteAmountEftInd(accountId, policyId,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilPolicyTerm.getPlnExpDt(),
                    dateService.currentDateTime(), CHAR_Y, CHAR_R);

            BilActRules bilActRules = bilActRulesRepository.findById(new BilActRulesId(BLANK_STRING, BLANK_STRING))
                    .orElse(null);
            if (bilActRules != null && bilActRules.getBruRnlQteInd() == CHAR_Y) {
                List<BilPolActivity> bilPolActivities = bilPolActivityRepository.fetchBilPolActivityTriger(accountId,
                        policyId, "RIF");
                if (bilPolActivities != null && !bilPolActivities.isEmpty() && quoteExisting != null
                        && quoteExisting > 0) {
                    newStatus = CHAR_Q;
                }
            }
            BilPolicyTerm bilPolicyTermUpdate = bilPolicyTermRepository.findById(
                    new BilPolicyTermId(accountId, policyId, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()))
                    .orElse(null);
            if (bilPolicyTermUpdate != null) {
                bilPolicyTermUpdate.setBillPolStatusCd(newStatus);
                bilPolicyTermUpdate.setBillSusFuReaCd(BLANK_STRING);
                bilPolicyTermUpdate.setBptStatusEffDt(dateService.currentDate());
                bilPolicyTermRepository.save(bilPolicyTermUpdate);
            }
        }
    }

    private Character getNewPolicyStatus(String accountId, String policyId, BilPolicyTerm bilPolicyTerm,
            Double policyBalance) {
        Character eftIndicator = BLANK_CHAR;
        char newStatus = BLANK_CHAR;
        ZonedDateTime bamAmountTimestamp = bilAmountsRepository.getmaxAmtTs(accountId, policyId,
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilPolicyTerm.getPlnExpDt());
        if (bamAmountTimestamp != null) {
            List<String> bilDesReasonTypes = new ArrayList<>(
                    Arrays.asList("BAM", "AMT", "BAS", "BPS", "BAA", "BPA", "BEQ"));
            ZonedDateTime reintallBamTimestamp = bilAmountsRepository.getMaxReInstallmentAmtTs(accountId, policyId,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilPolicyTerm.getPlnExpDt(),
                    bamAmountTimestamp, CHAR_Y,
                    new ArrayList<>(Arrays.asList(CHAR_K, CHAR_L, CHAR_O, CHAR_T, CHAR_Y, CHAR_Z, CHAR_4, CHAR_6)),
                    bilDesReasonTypes);
            if (reintallBamTimestamp == null) {
                eftIndicator = bilAmountsRepository.getMaxAmountEftInd(accountId, policyId,
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilPolicyTerm.getPlnExpDt(),
                        bamAmountTimestamp, CHAR_Y, CHAR_F,
                        new ArrayList<>(Arrays.asList("BAM", "AMT", "BAS", "BPS", "BEQ")),
                        new ArrayList<>(Arrays.asList(CHAR_P, CHAR_S, CHAR_7, CHAR_3, CHAR_5)), bilDesReasonTypes);
            }
        }
        if (eftIndicator != null) {
            if (eftIndicator == CHAR_F) {
                newStatus = eftIndicator;
            } else if (eftIndicator == CHAR_3 || eftIndicator == CHAR_5) {
                newStatus = CHAR_A;
            } else if (eftIndicator == CHAR_7) {
                newStatus = CHAR_W;
            } else if (eftIndicator != BLANK_CHAR) {
                newStatus = CHAR_C;
            } else if (policyBalance == 0) {
                newStatus = CHAR_X;
            } else {
                newStatus = CHAR_O;
            }
        }
        return newStatus;
    }

    private void resumeAccount(String accountId, String polEffDt, BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy) {
        String bilDesReasonType = BLANK_STRING;
        if (bilPolicyTerm.getBillPolStatusCd() == CHAR_S) {
            bilDesReasonType = "RSM";
        } else if (bilPolicyTerm.getBillPolStatusCd() == CHAR_L) {
            bilDesReasonType = "RFM";
        } else if (bilPolicyTerm.getBillPolStatusCd() == CHAR_B) {
            bilDesReasonType = "RBM";
        } else if (bilPolicyTerm.getBillPolStatusCd() == CHAR_I) {
            bilDesReasonType = "RCM";
        } else if (bilPolicyTerm.getBillPolStatusCd() == CHAR_H) {
            bilDesReasonType = "RPM";
        }

        String acyDesCode = bilPolicyTerm.getBillSusFuReaCd().substring(0, 2).concat("R");
        List<AccountHistory> accountHistoryList = new ArrayList<>();
        accountHistoryList.add(AccountOperationsWrapper.createActivity(acyDesCode, bilDesReasonType,
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), DateRoutine.dateTimeAsYYYYMMDD(polEffDt),
                DateRoutine.defaultDateTime(), 0, BLANK_STRING, execContext.getUserSeqeunceId()));
        accountSummaryService.writeAccountSummary(accountId, accountHistoryList);
    }

    private void updateResumeNextDate(String accountId, String policyId, String polEffDt) {
        BilPolActivity bilPolActivity = bilPolActivityRepository.findById(new BilPolActivityId(accountId, policyId,
                DateRoutine.dateTimeAsYYYYMMDD(polEffDt), ActivityType.POLICY_RESUME.getValue())).orElse(null);
        if (bilPolActivity != null) {
            bilPolActivity.setBilNxtAcyDt(DateRoutine.defaultDateTime());
            bilPolActivityRepository.save(bilPolActivity);
        }
    }

    private double getPolicyCollectionAmount(String accountId, String policyId) {
        double policyCollectionAmount = DECIMAL_ZERO;
        ZonedDateTime maxPlanExpiredDate = bilPolicyTermRepository.getMaxCollectionPolicyExpiDate(accountId, policyId,
                CHAR_I);
        if (maxPlanExpiredDate != null) {
            BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository.getPolicyCollectionAmount(accountId, policyId,
                    maxPlanExpiredDate);
            if (bilPolicyTerm != null) {
                policyCollectionAmount = bilPolicyTerm.getBptPolColAmt();
            }
        }
        return policyCollectionAmount;
    }

    private void scheduleBillingApi(BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy,
            String apiType, String stopManStart, String manualSuspend, String classChange) {
        StringBuilder userData = new StringBuilder();
        userData.append(StringUtils.rightPad(apiType, 3, BLANK_CHAR));
        userData.append(StringUtils.rightPad(bilAccount.getAccountId(), 8, BLANK_CHAR));
        userData.append(StringUtils.rightPad(bilPolicy.getBillPolicyId().getPolicyId(), 16, BLANK_CHAR));
        userData.append(StringUtils.rightPad(bilPolicy.getPolSymbolCd(), 3, BLANK_CHAR));
        userData.append(StringUtils.rightPad(bilPolicy.getPolNbr(), 25, BLANK_CHAR));
        userData.append(
                StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()), 10, BLANK_CHAR));
        userData.append(StringUtils.rightPad(bilAccount.getBillTypeCd(), 2, BLANK_CHAR));
        userData.append(StringUtils.rightPad(bilAccount.getBillClassCd(), 3, BLANK_CHAR));
        userData.append(StringUtils.rightPad(bilAccount.getAccountNumber(), 30, BLANK_CHAR));
        userData.append(StringUtils.rightPad(execContext.getUserId(), 8, BLANK_CHAR));
        userData.append(StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()), 10,
                BLANK_CHAR));
        userData.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(bilPolicyTerm.getPlnExpDt()), 10,
                BLANK_CHAR));
        userData.append(StringUtils.rightPad("F", 1));
        userData.append(StringUtils.rightPad(classChange, 1, BLANK_CHAR));
        userData.append(StringUtils.rightPad("P", 1));
        userData.append(StringUtils.rightPad(stopManStart, 1, BLANK_CHAR));
        userData.append(StringUtils.rightPad("N", 1, BLANK_CHAR));
        userData.append(StringUtils.rightPad(BLANK_STRING, 8, BLANK_CHAR));
        userData.append(StringUtils.rightPad(BLANK_STRING, 30, BLANK_CHAR));
        userData.append(StringUtils.rightPad(BLANK_STRING, 8, '2'));
        userData.append(StringUtils.rightPad(BLANK_STRING, 70, BLANK_CHAR));
        userData.append(StringUtils.rightPad(BLANK_STRING, 30, BLANK_CHAR));
        userData.append(StringUtils.rightPad("0", 14, '0'));
        userData.append(StringUtils.rightPad(BLANK_STRING, 1, BLANK_CHAR));
        userData.append(StringUtils.rightPad("N", 1, BLANK_CHAR));
        userData.append(StringUtils.rightPad(manualSuspend, 1, BLANK_CHAR));
        scheduleService.scheduleDeferActivity("BCMOAPI", userData.toString());
        // Call BCMOAPI from PM rest APi if possible

        if ((apiType.equalsIgnoreCase("COF") || apiType.equalsIgnoreCase("STP"))
                && !bilAccount.getBillTypeCd().trim().equalsIgnoreCase(SINGLE_POLICY)) {
            checkingAfterScheduleBillingApi(bilAccount, bilPolicy.getBillPolicyId().getPolicyId());
        }
    }

    private void checkingAfterScheduleBillingApi(BilAccount bilAccount, String policyId) {
        String accountId = bilAccount.getAccountId();
        List<Character> chargeStatus = new ArrayList<>(Arrays.asList('H', 'I'));
        String chargePolicyId = bilPolicyTermRepository.getChargePolicyId(accountId, policyId, CHAR_Y, chargeStatus);
        if (chargePolicyId != null) {
            BilPolicyTerm bilPolicyTerm = null;
            ZonedDateTime maxChargeEffDate = bilPolicyTermRepository.getMaxChargePolicyEffDate(accountId, policyId,
                    chargeStatus);
            if (maxChargeEffDate != null) {
                bilPolicyTerm = bilPolicyTermRepository
                        .findById(new BilPolicyTermId(accountId, chargePolicyId, maxChargeEffDate)).orElse(null);
            }
            BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct(RuleId.COLLECTIONS_RULE.toString(),
                    bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(), BLANK_STRING);
            if (bilRulesUct == null || bilRulesUct.getBrtRuleCd() != CHAR_Y) {
                throw new InvalidDataException("bil.account.not.suspend");
            }
            boolean woCollAmountInd = false;
            if (bilRulesUct.getBrtParmListTxt() != null && !bilRulesUct.getBrtParmListTxt().isEmpty()
                    && bilRulesUct.getBrtParmListTxt().length() >= 18) {
                woCollAmountInd = bilRulesUct.getBrtParmListTxt().charAt(17) == 'Y';
            }
            BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(chargePolicyId, accountId)).orElse(null);
            Map<String, Object> collectionsMap = new HashMap<>();
            double collectionBalanceAmount = fetchPolicyBalanceAmounts(accountId, chargePolicyId, false, CHAR_N,
                    collectionsMap);
            Integer existenceBLCTrigger = bilPolActivityRepository.getPolicyAmountsCount(accountId, chargePolicyId,
                    "BLC");
            if (bilPolicyTerm != null) {
                if (bilPolicyTerm.getBillPolStatusCd() == CHAR_I) {
                    checkCollectionBalanceChange(bilAccount, chargePolicyId, maxChargeEffDate, collectionBalanceAmount,
                            existenceBLCTrigger, bilPolicyTerm, collectionsMap, woCollAmountInd);
                } else {
                    checkPrevCollectionBalanceChange(bilAccount, chargePolicyId, maxChargeEffDate, bilPolicy,
                            collectionBalanceAmount, bilPolicyTerm, existenceBLCTrigger, collectionsMap,
                            woCollAmountInd);
                }
                updateAccountCollectionCode(bilAccount, chargePolicyId, bilPolicyTerm.getBillPolStatusCd());
            }
        }
    }

    private void updateAccountCollectionCode(BilAccount bilAccount, String chargePolicyId, char policyStatusCode) {
        List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.findPolicyDetails(bilAccount.getAccountId(),
                chargePolicyId);
        if (bilPolicyTermList != null && !bilPolicyTermList.isEmpty()) {
            for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
                if (bilPolicyTerm.getBptActColCd() != CHAR_N) {
                    bilPolicyTerm.setBptActColCd(CHAR_N);
                    bilPolicyTermRepository.save(bilPolicyTerm);
                }
            }
            collectionResumeBilAccount(bilAccount, policyStatusCode);
        }
    }

    private void collectionResumeBilAccount(BilAccount bilAccount, char policyStatusCode) {
        Double collectionAccountBalance = bilIstScheduleRepository.getCollectionAccountBalance(
                bilAccount.getAccountId(), new ArrayList<>(Arrays.asList('S', 'B', 'H', 'I', 'P', 'N', 'E', 'M')));
        if (collectionAccountBalance == null) {
            collectionAccountBalance = DECIMAL_ZERO;
        }
        char accountStatus = CHAR_A;
        if (collectionAccountBalance == DECIMAL_ZERO) {
            accountStatus = CHAR_I;
        }
        if ((bilAccount.getSuspendDisbReasonCd().equalsIgnoreCase("AC")
                || bilAccount.getSuspendDisbReasonCd().equalsIgnoreCase("AP")) && bilAccount.getStatus() == CHAR_B) {
            bilAccount.setSuspendDisbReasonCd(BLANK_STRING);
            bilAccount.setStatus(accountStatus);
            bilAccountRepository.save(bilAccount);

            String userId = execContext.getUserSeqeunceId();
            String activityDesType;
            String activityDesCode = "ACR";
            if (policyStatusCode == CHAR_H) {
                activityDesCode = "APR";
                if (execContext.getUserSeqeunceId().trim().isEmpty()
                        || execContext.getUserSeqeunceId().trim().equalsIgnoreCase(SYSTEM)) {
                    userId = SYSTEM;
                    activityDesType = "RPA";
                } else {
                    activityDesType = "RPM";
                }
            } else {
                if (execContext.getUserSeqeunceId().trim().isEmpty()
                        || execContext.getUserSeqeunceId().trim().equalsIgnoreCase(SYSTEM)) {
                    userId = SYSTEM;
                    activityDesType = "RCA";
                } else {
                    activityDesType = "RCM";
                }
            }

            List<AccountHistory> accountHistoryList = new ArrayList<>();
            accountHistoryList.add(AccountOperationsWrapper.createActivity(activityDesCode, activityDesType,
                    BLANK_STRING, BLANK_STRING, DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime(),
                    SHORT_ZERO, BLANK_STRING, userId));
            accountSummaryService.writeAccountSummary(bilAccount.getAccountId(), accountHistoryList);
        }
    }

    private void checkPrevCollectionBalanceChange(BilAccount bilAccount, String chargePolicyId,
            ZonedDateTime chargeEffDate, BilPolicy bilPolicy, double collectionBalanceAmount,
            BilPolicyTerm bilPolicyTerm, Integer existenceBLCTrigger, Map<String, Object> collectionsMap,
            boolean woCollAmountInd) {
        Double policyBalanceAmount = (Double) collectionsMap.get(POL_BAL_AMT);
        Double scgBalance = (Double) collectionsMap.get(SCG_BAL_AMT);
        if (existenceBLCTrigger == null || existenceBLCTrigger == 0) {
            double collMinAmount = getCollectionMinAmount(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd());
            if (collectionBalanceAmount < collMinAmount || collectionBalanceAmount <= 0 && collMinAmount == 0) {
                scheduleBillingApi(bilAccount, bilPolicyTerm, bilPolicy, "COF", BLANK_STRING, BLANK_STRING, "N");
                return;
            } else if (collectionBalanceAmount != bilPolicyTerm.getBptPolColAmt()
                    && (policyBalanceAmount > 0 || scgBalance > 0) && woCollAmountInd) {
                updateCWOTrigger(bilAccount.getAccountId(), chargePolicyId);
            }
        }
        if (collectionBalanceAmount != bilPolicyTerm.getBptPolColAmt()) {
            double balanceChangeAmount = collectionBalanceAmount - bilPolicyTerm.getBptPolColAmt();
            String transObjectCode = "COL";
            if (existenceBLCTrigger != null && existenceBLCTrigger > 0) {
                transObjectCode = "PCL";
            }
            writeCollectionFinancial(bilAccount, chargePolicyId, chargeEffDate, balanceChangeAmount, bilPolicyTerm,
                    transObjectCode);
            writeCollectionFinancialComplete(bilAccount.getAccountId(), bilAccount.getCurrencyCode());
            updateCollectionAmount(bilAccount.getAccountId(), chargePolicyId, chargeEffDate, collectionBalanceAmount);
        }
    }

    private void checkCollectionBalanceChange(BilAccount bilAccount, String chargePolicyId, ZonedDateTime chargeEffDate,
            double collectionBalanceAmount, Integer blcTrigger, BilPolicyTerm bilPolicyTerm,
            Map<String, Object> collectionsMap, boolean woCollAmountInd) {
        if (collectionBalanceAmount != bilPolicyTerm.getBptPolColAmt()) {
            if (blcTrigger == null || blcTrigger == 0) {
                double collectionBalChange = collectionBalanceAmount - bilPolicyTerm.getBptPolColAmt();
                writeCollectionFinancial(bilAccount, chargePolicyId, chargeEffDate, collectionBalChange, bilPolicyTerm,
                        "COL");
                writeCollectionFinancialComplete(bilAccount.getAccountId(), bilAccount.getCurrencyCode());
                Double policyBalanceAmount = (Double) collectionsMap.get(POL_BAL_AMT);
                Double scgBalance = (Double) collectionsMap.get(SCG_BAL_AMT);
                if (woCollAmountInd && (policyBalanceAmount != null && policyBalanceAmount > 0
                        || scgBalance != null && scgBalance > 0)) {
                    updateCWOTrigger(bilAccount.getAccountId(), chargePolicyId);
                }
                insertCCLTrigger(bilAccount.getAccountId(), chargePolicyId);
            }
            updateCollectionAmount(bilAccount.getAccountId(), chargePolicyId, chargeEffDate, collectionBalanceAmount);
        }
    }

    private void insertCCLTrigger(String accountId, String chargePolicyId) {
        BilPolActivity bilPolActivity = new BilPolActivity();
        bilPolActivity.setBillPolActivityId(
                new BilPolActivityId(accountId, chargePolicyId, DateRoutine.defaultDateTime(), "CCL"));
        bilPolActivity.setBilNxtAcyDt(dateService.currentDate());
        bilPolActivityRepository.save(bilPolActivity);
    }

    private void updateCWOTrigger(String accountId, String chargePolicyId) {
        BilPolActivity bilPolActivityExisted = bilPolActivityRepository
                .findById(new BilPolActivityId(accountId, chargePolicyId, DateRoutine.defaultDateTime(), "CWO"))
                .orElse(null);
        if (bilPolActivityExisted == null) {
            bilPolActivityExisted = new BilPolActivity();
            bilPolActivityExisted.setBillPolActivityId(
                    new BilPolActivityId(accountId, chargePolicyId, DateRoutine.defaultDateTime(), "CWO"));
        }
        bilPolActivityExisted.setBilNxtAcyDt(dateService.currentDate());
        bilPolActivityRepository.save(bilPolActivityExisted);
    }

    private void writeCollectionFinancialComplete(String accountId, String currencyCode) {
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setFunctionCode(CHAR_S);
        financialApiActivity.setFolderId(accountId);
        financialApiActivity.setApplicationName("BACPPSR");
        financialApiActivity.setCurrencyCode(currencyCode);
        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                execContext.getApplicationDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
    }

    private void writeCollectionFinancial(BilAccount bilAccount, String policyId, ZonedDateTime effDate,
            double collectionBalChange, BilPolicyTerm bilPolicyTerm, String transObjectCode) {
        String se3Date = DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate());
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setBilReasonCode("BCG");
        financialApiActivity.setTransactionObjectCode(transObjectCode);
        financialApiActivity.setTransactionActionCode("BAL");
        financialApiActivity.setActivityAmount(collectionBalChange);
        financialApiActivity.setActivityNetAmount(collectionBalChange);
        financialApiActivity.setFunctionCode(CHAR_A);
        financialApiActivity.setFolderId(bilAccount.getAccountId());
        financialApiActivity.setApplicationName("BACPPSR");
        financialApiActivity.setApplicationProductIdentifier("BCMS");
        financialApiActivity.setMasterCompanyNbr(bilPolicyTerm.getMasterCompanyNbr());
        financialApiActivity.setCountryCode(bilPolicyTerm.getBillCountryCd());
        financialApiActivity.setStateCode(bilPolicyTerm.getBillStatePvnCd());
        financialApiActivity.setLineOfBusCode(bilPolicyTerm.getLobCd());
        financialApiActivity.setCountyCode(bilPolicyTerm.getBillCountryCd());
        financialApiActivity.setPolicyId(policyId);
        financialApiActivity.setOriginalEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(effDate));
        financialApiActivity.setFinancialIndicator(CHAR_Y);
        financialApiActivity.setCompanyLocationNbr("00");
        financialApiActivity.setEffectiveDate(se3Date);
        financialApiActivity.setReferenceDate(se3Date);
        financialApiActivity.setBilPostDate(se3Date);
        financialApiActivity.setBilAccountId(bilAccount.getAccountNumber());
        financialApiActivity.setBilTypeCode(bilAccount.getBillTypeCd());
        financialApiActivity.setBilClassCode(bilAccount.getBillClassCd());
        financialApiActivity.setCurrencyCode(bilAccount.getCurrencyCode());

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                execContext.getApplicationDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
    }

    private void updateCollectionAmount(String accountId, String chargePolicyId, ZonedDateTime chargeEffDate,
            double collectionBalanceAmount) {
        BilPolicyTerm bilPolicyTermExist = bilPolicyTermRepository
                .findById(new BilPolicyTermId(accountId, chargePolicyId, chargeEffDate)).orElse(null);
        if (bilPolicyTermExist != null) {
            bilPolicyTermExist.setBptPolColAmt(collectionBalanceAmount);
            bilPolicyTermRepository.save(bilPolicyTermExist);
        }
    }

    private void deleteBilPolActivityTriger(String accountId, String policyId, String bilAcyType) {
        List<BilPolActivity> bilPolActivities = bilPolActivityRepository.fetchBilPolActivityTriger(accountId, policyId,
                bilAcyType);
        if (bilPolActivities != null && !bilPolActivities.isEmpty()) {
            bilPolActivityRepository.deleteAll(bilPolActivities);
        }
    }

    private double fetchPolicyBalanceAmounts(String accountId, String policyId, boolean accountCollectionCode,
            char batchCollectionInd, Map<String, Object> collectionsMap) {
        double collectionBalanceAmount;
        double collectionScgBalance = DECIMAL_ZERO;
        Double lastPolicyBalance = DECIMAL_ZERO;
        List<BilPolicyTerm> bilPolicyTerms = bilPolicyTermRepository.getPolicyBalance(accountId, policyId,
                new ArrayList<>(Arrays.asList(CHAR_W, CHAR_T, CHAR_Z, '1', '2')));
        if (bilPolicyTerms != null && !bilPolicyTerms.isEmpty()) {
            BilPolicyTermId bilPolicyTermId = bilPolicyTerms.get(0).getBillPolicyTermId();
            lastPolicyBalance = bilIstScheduleRepository.getLastPolicyBalance(accountId, bilPolicyTermId.getPolicyId(),
                    bilPolicyTermId.getPolEffectiveDt());
            if (lastPolicyBalance == null) {
                lastPolicyBalance = DECIMAL_ZERO;
            }
        }
        collectionsMap.put(POL_BAL_AMT, lastPolicyBalance);
        BilPolicy bilPolicyExisting = bilPolicyRepository.findById(new BilPolicyId(policyId, accountId)).orElse(null);
        if (bilPolicyExisting == null) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }
        calculateRntBalance(accountId, policyId, bilPolicyExisting, collectionsMap);
        calculatePremiumWriteOffBalance(accountId, bilPolicyExisting, collectionsMap);
        if (accountCollectionCode) {
            collectionScgBalance = calculateBilChargesAmounts(accountId, batchCollectionInd);
            calculateChargeWriteOffBalance(accountId, collectionsMap, bilPolicyExisting);
        } else {
            collectionsMap.put(WRO_CRG_AMT, DECIMAL_ZERO);
            collectionsMap.put(REV_WRO_CRG_AMT, DECIMAL_ZERO);
        }
        collectionsMap.put(SCG_BAL_AMT, collectionScgBalance);
        calculateCollectBalance(collectionsMap);
        collectionBalanceAmount = (double) collectionsMap.get(RNT_BAL_AMT) + collectionScgBalance
                + (double) collectionsMap.get(REMAIN_WRO_PRM) + (double) collectionsMap.get(REMAIN_WRO_CRG);
        return collectionBalanceAmount;
    }

    private void calculateCollectBalance(Map<String, Object> collectionsMap) {
        double remainWriteOfAmountPremium = DECIMAL_ZERO;
        double remainWriteOfAmountCharge = DECIMAL_ZERO;
        double writeOffPremiumAmount = (double) collectionsMap.get(WRO_PRM_AMT);
        double writeOffChargeAmount = (double) collectionsMap.get(WRO_CRG_AMT);
        if (writeOffChargeAmount != writeOffPremiumAmount) {
            double revWriteOffPremiumAmount = (double) collectionsMap.get(REV_WRO_PRM_AMT);
            if (writeOffChargeAmount - revWriteOffPremiumAmount < 0) {
                collectionsMap.put(WRO_PRM_AMT, DECIMAL_ZERO);
                collectionsMap.put(REV_WRO_PRM_AMT, DECIMAL_ZERO);
            }
            double revWriteOffChargeAmount = (double) collectionsMap.get(REV_WRO_CRG_AMT);
            if (writeOffPremiumAmount - revWriteOffChargeAmount < 0) {
                collectionsMap.put(WRO_CRG_AMT, DECIMAL_ZERO);
                collectionsMap.put(REV_WRO_CRG_AMT, DECIMAL_ZERO);
            }

            remainWriteOfAmountPremium = writeOffChargeAmount - revWriteOffPremiumAmount;
            remainWriteOfAmountCharge = writeOffPremiumAmount - revWriteOffChargeAmount;
        }
        collectionsMap.put(REMAIN_WRO_PRM, remainWriteOfAmountPremium);
        collectionsMap.put(REMAIN_WRO_CRG, remainWriteOfAmountCharge);
    }

    private void calculateChargeWriteOffBalance(String accountId, Map<String, Object> collectionsMap,
            BilPolicy bilPolicyExisting) {
        if (collectionsMap.get(INI_DATE) != null) {
            ZonedDateTime iniBilAcyDate = (ZonedDateTime) collectionsMap.get(INI_DATE);
            short iniBilSequence = (short) collectionsMap.get(INI_SEQUENCE);
            Double writeOffChargeAmount = bilActSummaryRepository.getWriteOffChargeAmounts(accountId, iniBilAcyDate,
                    iniBilSequence, iniBilAcyDate, bilPolicyExisting.getPolNbr(), bilPolicyExisting.getPolSymbolCd(),
                    new ArrayList<>(Arrays.asList("WPA", "W01")));
            if (writeOffChargeAmount == null) {
                writeOffChargeAmount = DECIMAL_ZERO;
            }
            Double revWriteOffChargeAmount = bilActSummaryRepository.getRevertManWriteOffAmounts(accountId,
                    iniBilAcyDate, iniBilSequence, iniBilAcyDate, bilPolicyExisting.getPolNbr(),
                    bilPolicyExisting.getPolSymbolCd(), "WRR");
            if (revWriteOffChargeAmount == null) {
                revWriteOffChargeAmount = DECIMAL_ZERO;
            }
            Double revAutWriteOffAmount = bilActSummaryRepository.getRevertManWriteOffAmounts(accountId, iniBilAcyDate,
                    iniBilSequence, iniBilAcyDate, bilPolicyExisting.getPolNbr(), bilPolicyExisting.getPolSymbolCd(),
                    "RW1");
            if (revAutWriteOffAmount == null) {
                revAutWriteOffAmount = DECIMAL_ZERO;
            }
            revWriteOffChargeAmount += revAutWriteOffAmount;
            collectionsMap.put(REV_WRO_CRG_AMT, revWriteOffChargeAmount);
            collectionsMap.put(WRO_CRG_AMT, writeOffChargeAmount);
        } else {
            collectionsMap.put(REV_WRO_CRG_AMT, DECIMAL_ZERO);
        }
    }

    private double calculateBilChargesAmounts(String accountId, char batchCollectionsInd) {
        List<Character> chargeTypes = new ArrayList<>(Arrays.asList(CHAR_P, CHAR_L, CHAR_D));
        List<Character> invoiceCodes = new ArrayList<>(Arrays.asList(CHAR_Y, CHAR_L, CHAR_B));
        Double collectionScgBalanceAmount = null;
        if (batchCollectionsInd == CHAR_I) {
            collectionScgBalanceAmount = bilCrgAmountsRepository.getCollectionScgBalanceAmountWithInvoiceDate(accountId,
                    chargeTypes, CHAR_S, invoiceCodes, CHAR_S, BLANK_CHAR, dateService.currentDate());
        } else {
            collectionScgBalanceAmount = bilCrgAmountsRepository.getCollectionScgBalanceAmount(accountId, chargeTypes,
                    CHAR_S, invoiceCodes);
        }
        if (collectionScgBalanceAmount == null) {
            collectionScgBalanceAmount = DECIMAL_ZERO;
        }
        return collectionScgBalanceAmount;
    }

    private void calculatePremiumWriteOffBalance(String accountId, BilPolicy bilPolicyExisting,
            Map<String, Object> collectionsMap) {
        ZonedDateTime bilAcyDate = DateRoutine.defaultSinceDateTime();
        short sequenceNumber = SHORT_ZERO;
        double revWriteOffPremiumAmount = DECIMAL_ZERO;
        List<String> bilAcyDesCodes = new ArrayList<>(Arrays.asList("STP", "COF"));
        ZonedDateTime maxAcyDate = bilActSummaryRepository.findMaxBilActivityDateWithPolicySymBol(accountId,
                bilPolicyExisting.getPolNbr(), bilPolicyExisting.getPolSymbolCd(), bilAcyDesCodes);
        if (maxAcyDate != null) {
            List<Object[]> bilAcyDateAndSequence = bilActSummaryRepository.getMaxAcyDateAndSequenceWithPolicy(accountId,
                    bilAcyDesCodes, maxAcyDate);
            if (bilAcyDateAndSequence != null && !bilAcyDateAndSequence.isEmpty()) {
                bilAcyDate = (ZonedDateTime) bilAcyDateAndSequence.get(0)[0];
                sequenceNumber = (short) bilAcyDateAndSequence.get(0)[1];
            }
        }

        ZonedDateTime iniBilAcyDate = bilActSummaryRepository.findMaxIniBilActivityDate(accountId,
                bilPolicyExisting.getPolNbr(), bilPolicyExisting.getPolSymbolCd(), "INI", bilAcyDate, sequenceNumber,
                bilAcyDate);
        if (iniBilAcyDate != null) {
            collectionsMap.put(INI_DATE, iniBilAcyDate);
            Double premiumWriteOffAmount = null;
            Short initBilSequence = bilActSummaryRepository.findMaxIniBilSequence(accountId,
                    bilPolicyExisting.getPolNbr(), bilPolicyExisting.getPolSymbolCd(), "INI", iniBilAcyDate);
            if (initBilSequence != null) {
                collectionsMap.put(INI_SEQUENCE, initBilSequence);
                premiumWriteOffAmount = bilActSummaryRepository.getPrimeumWriteOffAmounts(accountId, iniBilAcyDate,
                        initBilSequence, iniBilAcyDate, bilPolicyExisting.getPolNbr(),
                        bilPolicyExisting.getPolSymbolCd(), new ArrayList<>(Arrays.asList("WC", "WE", "WU")), "WPA");
            }
            if (premiumWriteOffAmount == null) {
                premiumWriteOffAmount = DECIMAL_ZERO;
            }
            Double revManWroPrmAmount = bilActSummaryRepository.getRevertManWriteOffAmounts(accountId, iniBilAcyDate,
                    sequenceNumber, iniBilAcyDate, bilPolicyExisting.getPolNbr(), bilPolicyExisting.getPolSymbolCd(),
                    "WRR");
            if (revManWroPrmAmount == null) {
                revManWroPrmAmount = DECIMAL_ZERO;
            }
            Double revAutWroPrmAmount = bilActSummaryRepository.getRevertManWriteOffAmounts(accountId, iniBilAcyDate,
                    sequenceNumber, iniBilAcyDate, bilPolicyExisting.getPolNbr(), bilPolicyExisting.getPolSymbolCd(),
                    "RW1");
            if (revAutWroPrmAmount == null) {
                revAutWroPrmAmount = DECIMAL_ZERO;
            }
            revWriteOffPremiumAmount = revManWroPrmAmount + revAutWroPrmAmount;
            collectionsMap.put(WRO_PRM_AMT, premiumWriteOffAmount);
        }
        collectionsMap.put(REV_WRO_PRM_AMT, revWriteOffPremiumAmount);
    }

    private void calculateRntBalance(String accountId, String policyId, BilPolicy bilPolicyExisting,
            Map<String, Object> collectionsMap) {
        Double policyBalanceAmount = (Double) collectionsMap.get(POL_BAL_AMT);
        if (policyBalanceAmount == null) {
            policyBalanceAmount = DECIMAL_ZERO;
        }
        if (bilPolicyExisting != null) {
            Double policyRntBalanceAmount = DECIMAL_ZERO;
            List<BilPolicy> bilPolicies = bilPolicyRepository.getRntBalance(accountId, policyId,
                    bilPolicyExisting.getPolNbr(), bilPolicyExisting.getPolSymbolCd(), CHAR_K);
            for (BilPolicy bilPolicy : bilPolicies) {
                policyRntBalanceAmount = bilIstScheduleRepository.getSumPolicyBalance(accountId,
                        bilPolicy.getBillPolicyId().getPolicyId());
                if (policyRntBalanceAmount == null) {
                    policyRntBalanceAmount = DECIMAL_ZERO;
                }
                policyRntBalanceAmount = policyBalanceAmount + policyRntBalanceAmount;
            }
            collectionsMap.put(RNT_BAL_AMT, policyRntBalanceAmount);
        }
        collectionsMap.put(POL_BAL_AMT, policyBalanceAmount);
    }

    private double getCollectionMinAmount(String billTypeCode, String billClassCode) {
        Double collectionAmount = DECIMAL_ZERO;
        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct(RuleId.COLLECTIONS_RULE.toString(), billTypeCode,
                billClassCode, BLANK_STRING);
        if (bilRulesUct == null || bilRulesUct.getBrtRuleCd() != CHAR_Y) {
            throw new InvalidDataException("bil.account.not.suspend");
        }
        try {
            if (bilRulesUct.getBrtParmListTxt() != null && !bilRulesUct.getBrtParmListTxt().trim().isEmpty()
                    && bilRulesUct.getBrtParmListTxt().length() >= 16) {
                collectionAmount = new BigDecimal(Integer.parseInt(bilRulesUct.getBrtParmListTxt().substring(0, 16)))
                        .doubleValue();
            }
        } catch (Exception e) {
            throw new InvalidDataException("invalid.collection.param");
        }
        return collectionAmount;
    }

    private List<Character> getSuspendStatusCode() {
        return new ArrayList<>(Arrays.asList(CHAR_T, CHAR_W, CHAR_V, CHAR_4));
    }

}
