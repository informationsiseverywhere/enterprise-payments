package billing.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import billing.data.entity.ExchangeRate;
import billing.data.entity.id.ExchangeRateId;
import billing.data.repository.ExchangeRateRepository;
import billing.model.CurrencyExchange;
import billing.service.ExchangeRateService;
import core.model.Paging;
import core.utils.DateRoutine;
import core.utils.MultiFiltersSearch;
import core.utils.PageNavigation;

@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

    @Autowired
    private ExchangeRateRepository exchangeRateRepository;

    @Value("${currency.code}")
    String defaultCurrency;

    @Override
    public List<CurrencyExchange> findAllExchangeRates(String filters, int size, int page, Paging paging) {
        List<CurrencyExchange> currencyExchangeList = new ArrayList<>();
        Page<ExchangeRate> exchangeRateList = null;
        if (filters != null && !filters.isEmpty()) {
            exchangeRateList = filterCurrencyExchange(filters, size, page - 1);
        } else {
            exchangeRateList = exchangeRateRepository.findAll(PageRequest.of(page - 1, size));
        }
        if (exchangeRateList != null && exchangeRateList.getTotalElements() != 0) {
            int totalRows = (int) exchangeRateList.getTotalElements();
            for (ExchangeRate exchangeRate : exchangeRateList) {
                CurrencyExchange currencyExchange = mapCurrencyExchange(exchangeRate);
                currencyExchangeList.add(currencyExchange);
            }
            PageNavigation.getPagingAttributes(totalRows, size, page, paging);
        }
        return currencyExchangeList;
    }

    private Page<ExchangeRate> filterCurrencyExchange(String filters, int size, int page) {
        ZonedDateTime exchangeDate = null;
        String fromCurrency = null;
        String toCurrency = null;
        Page<ExchangeRate> exchangeRateList = null;
        Map<String, String> filtersMap = new HashMap<>();
        filtersMap = MultiFiltersSearch.getFilters(filters, filtersMap);
        for (Map.Entry<String, String> entry : filtersMap.entrySet()) {
            String filter = entry.getKey();
            String match = entry.getValue();
            if (match != null && match.equalsIgnoreCase("default")) {
                match = defaultCurrency;
            }
            if (filter.equalsIgnoreCase("exchangeDate")) {
                exchangeDate = DateRoutine.dateTimeAsYYYYMMDD(match);
            } else if (filter.equalsIgnoreCase("fromCurrency")) {
                fromCurrency = match;
            } else if (filter.equalsIgnoreCase("toCurrency")) {
                toCurrency = match;
            }
        }
        if (exchangeDate != null) {
            if (fromCurrency == null && toCurrency == null) {
                exchangeRateList = exchangeRateRepository.findByExchangeRateIdExchangeDate(exchangeDate,
                        PageRequest.of(page, size));
            } else if (fromCurrency != null && toCurrency == null) {
                exchangeRateList = exchangeRateRepository.findByExchangeRateIdExchangeDateAndExchangeRateIdFromCurrency(
                        exchangeDate, fromCurrency, PageRequest.of(page, size));
            } else if (fromCurrency == null) {
                exchangeRateList = exchangeRateRepository.findByExchangeRateIdExchangeDateAndExchangeRateIdToCurrency(
                        exchangeDate, toCurrency, PageRequest.of(page, size));
            } else {
                ExchangeRate exchangeRate = exchangeRateRepository
                        .findById(new ExchangeRateId(exchangeDate, fromCurrency, toCurrency)).orElse(null);
                if (exchangeRate != null) {
                    return new PageImpl<>(Arrays.asList(exchangeRate));
                }
            }
        } else {
            if (fromCurrency != null && toCurrency == null) {
                exchangeRateList = exchangeRateRepository.findByExchangeRateIdFromCurrency(fromCurrency,
                        PageRequest.of(page, size));
            } else if (fromCurrency == null && toCurrency != null) {
                exchangeRateList = exchangeRateRepository.findByExchangeRateIdToCurrency(toCurrency,
                        PageRequest.of(page, size));
            } else {
                exchangeRateList = exchangeRateRepository.findByExchangeRateIdFromCurrencyAndExchangeRateIdToCurrency(
                        fromCurrency, toCurrency, PageRequest.of(page, size));
            }
        }

        return exchangeRateList;
    }

    private CurrencyExchange mapCurrencyExchange(ExchangeRate exchangeRate) {
        CurrencyExchange currencyExchange = new CurrencyExchange();
        currencyExchange.setExchangeDate(exchangeRate.getExchangeRateId().getExchangeDate());
        currencyExchange.setFromCurrency(exchangeRate.getExchangeRateId().getFromCurrency().trim());
        currencyExchange.setToCurrency(exchangeRate.getExchangeRateId().getToCurrency().trim());
        currencyExchange.setRate(exchangeRate.getRate());
        return currencyExchange;
    }

    @Override
    public CurrencyExchange findExchangeRate(String exchangeDate, String fromCurrency, String toCurrency,
            Double fromAmount) {
        CurrencyExchange currencyExchange = new CurrencyExchange();
        if (fromCurrency.equals(toCurrency)) {
            currencyExchange.setRate(Double.valueOf(1));
        } else if (defaultCurrency.equals(fromCurrency) || defaultCurrency.equals(toCurrency)) {
            ExchangeRate exchangeRate = exchangeRateRepository
                    .findById(
                            new ExchangeRateId(DateRoutine.dateTimeAsYYYYMMDD(exchangeDate), fromCurrency, toCurrency))
                    .orElse(null);
            if (exchangeRate != null) {
                currencyExchange.setRate(exchangeRate.getRate());
            } else {
                currencyExchange.setRate(Double.valueOf(1));
            }
        } else {
            BigDecimal fromRate = null;
            BigDecimal toRate = null;
            ExchangeRate fromCurrencyRate = exchangeRateRepository.findById(
                    new ExchangeRateId(DateRoutine.dateTimeAsYYYYMMDD(exchangeDate), fromCurrency, defaultCurrency))
                    .orElse(null);
            if (fromCurrencyRate != null) {
                fromRate = BigDecimal.valueOf(fromCurrencyRate.getRate());
            }
            ExchangeRate toCurrencyRate = exchangeRateRepository.findById(
                    new ExchangeRateId(DateRoutine.dateTimeAsYYYYMMDD(exchangeDate), defaultCurrency, toCurrency))
                    .orElse(null);
            if (toCurrencyRate != null) {
                toRate = BigDecimal.valueOf(toCurrencyRate.getRate());
            }
            if (fromRate != null && toRate != null) {
                currencyExchange.setRate((fromRate.multiply(toRate).setScale(2, RoundingMode.HALF_EVEN)).doubleValue());
            }
        }
        if (fromAmount != null) {
            BigDecimal fromAmountValue = BigDecimal.valueOf(fromAmount);
            BigDecimal rate = BigDecimal.valueOf(currencyExchange.getRate());
            if (fromAmount != 0) {
                currencyExchange.setFromAmount(fromAmount);
                currencyExchange.setToAmount(
                        (fromAmountValue.multiply(rate).setScale(2, RoundingMode.HALF_EVEN)).doubleValue());
            }
        }
        return currencyExchange;
    }

    @Override
    public void patchExchangeRate(String exchangeDate, String fromCurrency, String toCurrency,
            CurrencyExchange currencyExchange) {
        if (currencyExchange != null && currencyExchange.getRate() != null) {
            ExchangeRate exchangeRate = exchangeRateRepository
                    .findById(
                            new ExchangeRateId(DateRoutine.dateTimeAsYYYYMMDD(exchangeDate), fromCurrency, toCurrency))
                    .orElse(null);
            if (exchangeRate != null && exchangeRate.getRate() != currencyExchange.getRate()) {
                exchangeRate.setRate(currencyExchange.getRate());
                exchangeRateRepository.save(exchangeRate);
            }
        }
    }
}
