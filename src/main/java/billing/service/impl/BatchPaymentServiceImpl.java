package billing.service.impl;

import static billing.utils.BillingConstants.STATEMENT_ROW;
import static billing.utils.BillingConstants.TECH_KEY_TYPE_CODE;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_L;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilAgent;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilInvTtyAct;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilThirdParty;
import billing.data.entity.TransRateHst;
import billing.data.entity.id.BilCashEntryId;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.TransRateHstId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilAgentRepository;
import billing.data.repository.BilAgtActRltRepository;
import billing.data.repository.BilAgtCashDspRepository;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilInvSttItmRepository;
import billing.data.repository.BilInvTtyActRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilSttReconDtlRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.data.repository.BilTtyCashDspRepository;
import billing.data.repository.TransRateHstRepository;
import billing.model.Payment;
import billing.model.embedded.Address;
import billing.model.embedded.Party;
import billing.service.BatchPaymentService;
import billing.service.BusinessGroupService;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilFunctionCtl;
import billing.utils.BillingConstants.BilRules;
import billing.utils.BillingConstants.BusCodeTranslationParentCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.ManualSuspendIndicator;
import billing.utils.BillingConstants.ReconGroupProcessType;
import billing.utils.BillingConstants.ReconciliationProcessType;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import billing.utils.BillingConstants.StatementLevelType;
import billing.utils.BillingConstants.StatementReconciliationIndicator;
import billing.utils.BillingConstants.TechnicalKeyTypeCode;
import core.api.ExecContext;
import core.exception.application.AppAuthenticationException;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.model.Paging;
import core.security.service.SecurityService;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import core.utils.MultiFiltersSearch;
import core.utils.PageNavigation;
import party.service.impl.PartyElasticSearchServiceImpl;

@Service
public class BatchPaymentServiceImpl implements BatchPaymentService {

    @Autowired
    private SecurityService securityService;

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private PartyElasticSearchServiceImpl partyElasticSearchServiceImpl;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    private BilAgentRepository bilAgentRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilInvTtyActRepository bilInvTtyActRepository;

    @Autowired
    private BilAgtActRltRepository bilAgtActRltRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilSttReconDtlRepository bilSttReconDtlRepository;

    @Autowired
    private BilAgtCashDspRepository bilAgtCashDspRepository;

    @Autowired
    private BilInvSttItmRepository bilInvSttItmRepository;

    @Autowired
    private BilTtyCashDspRepository bilTtyCashDspRepository;

    @Autowired
    private BusinessGroupService businessGroupService;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private TransRateHstRepository transRateHstRepository;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    @Value("${multi.currency}")
    String multiCurrency;

    private static final String IDENTIFIER = "identifier";
    private static final String PAYMENT_TYPE = "paymentType";
    private static final String DEFAULT_DATE = "9999-12-31";
    private static final String POLICY_IDENTIFIER = "Policy";
    private static final String BATCH_PAYMENT_NOT_FOUND = "batchpayment.notfound";
    private static final String BATCH_ADMIN = "batchAdmin";

    List<Character> statementTypeList = Arrays.asList(StatementLevelType.ACCOUNT_LEVEL.getValue(),
            StatementLevelType.DETAIL_LEVEL.getValue());

    private static final Logger logger = LogManager.getLogger(BatchPaymentServiceImpl.class);

    @Override
    public List<Payment> findAllBatchPayments(String postedDate, String batchId, String query, String filters,
            String sort, int size, int page, Paging paging, String userId) {
        List<Payment> batchPaymentList = new LinkedList<>();
        int totalRows = 0;
        Page<BilCashEntry> bilCashEntryPage = null;
        List<BilCashEntry> bilCashEntryRows = new ArrayList<>();
        boolean batchAdmin = true;

        try {
            securityService.authenticateFunction(BATCH_ADMIN);
        } catch (AppAuthenticationException e) {
            batchAdmin = false;
        }

        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(postedDate);

        if (filters != null && !filters.isEmpty()) {
            bilCashEntryPage = batchPaymentsRowsByParams(bilEntryDt, batchId, userId, page, size, filters);
        } else {
            bilCashEntryPage = bilCashEntryRepository.getBilCashEntryRowsByPaging(bilEntryDt, batchId, userId,
                    PageRequest.of(page - 1, size));
        }

        if (bilCashEntryPage != null && bilCashEntryPage.getTotalElements() != 0) {
            totalRows = (int) bilCashEntryPage.getTotalElements();
            bilCashEntryRows = bilCashEntryPage.getContent();
        }

        if (bilCashEntryRows.isEmpty()) {
            return batchPaymentList;
        }
        String currency = null;
        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(bilEntryDt, batchId, userId)).orElse(null);

        if (bilCashEntryTot != null) {
            currency = bilCashEntryTot.getCurrencyCd().trim();
        }
        for (BilCashEntry bilCashEntryRow : bilCashEntryRows) {
            batchPaymentList.add(
                    mapBilCashEntryToBatchPayment(bilCashEntryRow, BLANK_STRING, currency, false, batchAdmin, userId, bilCashEntryTot));
        }

        PageNavigation.getPagingAttributes(totalRows, size, page, paging);
        return batchPaymentList;
    }

    private Page<BilCashEntry> batchPaymentsRowsByParams(ZonedDateTime bilEntryDt, String batchId, String userId,
            int page, int size, String filters) {
        Map<String, String> matchFilterMap = new HashMap<>();

        matchFilterMap = MultiFiltersSearch.getFilters(filters, matchFilterMap);
        String identifier = BLANK_STRING;
        String paymentType = BLANK_STRING;

        for (Map.Entry<String, String> entry : matchFilterMap.entrySet()) {
            String filter = entry.getKey();
            String match = entry.getValue();
            if (match != null) {
                if (filter.equalsIgnoreCase(IDENTIFIER)) {
                    identifier = match;
                } else if (filter.equalsIgnoreCase(PAYMENT_TYPE)) {
                    paymentType = match;
                }
            }
        }

        String paymentCd = BLANK_STRING;

        if (!paymentType.isEmpty()) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType.trim(),
                    BusCodeTranslationType.PAYMENT_TYPE_CODE.getValue(), execContext.getLanguage());
            if (busCdTranslation != null) {
                paymentCd = busCdTranslation.getBusCdTranslationId().getBusCd().trim();
            }
        }
        return getBatchPaymentsWithFilters(bilEntryDt, batchId, userId, paymentCd, identifier, page, size);

    }

    private Page<BilCashEntry> getBatchPaymentsWithFilters(ZonedDateTime bilEntryDt, String batchId, String userId,
            String paymentCd, String identifier, int page, int size) {
        Page<BilCashEntry> bilCashEntryPage = null;

        if (!identifier.isEmpty() && !paymentCd.isEmpty()) {
            bilCashEntryPage = bilCashEntryRepository.getBilCashEntryRowsByFilters(bilEntryDt, batchId, userId,
                    paymentCd, identifier, PageRequest.of(page - 1, size));
        } else {

            if (!identifier.isEmpty()) {
                bilCashEntryPage = bilCashEntryRepository.getBilCashEntryRowsByIdentifier(bilEntryDt, batchId, userId,
                        identifier, PageRequest.of(page - 1, size));
            }

            if (!paymentCd.isEmpty()) {
                bilCashEntryPage = bilCashEntryRepository.getBilCashEntryRowsByPaymentType(bilEntryDt, batchId, userId,
                        paymentCd, PageRequest.of(page - 1, size));
            }
        }

        return bilCashEntryPage;
    }

    private Payment mapBilCashEntryToBatchPayment(BilCashEntry bilCashEntry, String userName, String currency,
            boolean isDetailSw, boolean batchAdmin, String userId, BilCashEntryTot bilCashEntryTot) {
        Payment batchPayment = new Payment();
        boolean currencyMatch = true;
        ZonedDateTime entryDt = bilCashEntry.getBilCashEntryId().getBilEntryDt();
        String entryNbr = bilCashEntry.getBilCashEntryId().getBilEntryNbr();
        batchPayment.setPostedDate(entryDt);
        batchPayment.setBatchId(entryNbr);
        batchPayment.setUserId(userName);
        batchPayment.setPaymentSequenceNumber(bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
        batchPayment.setPaymentAmount(bilCashEntry.getBilRctAmt());
        batchPayment.setPaymentComments(bilCashEntry.getBilRctCmt().trim());
        batchPayment.setPostmarkDate(bilCashEntry.getBilPstMrkDt().compareTo(DateRoutine.defaultDateTime()) == 0 ? null
                : bilCashEntry.getBilPstMrkDt());
        batchPayment
                .setPaymentDate(bilCashEntry.getBilRctReceiveDt().compareTo(DateRoutine.defaultDateTime()) == 0 ? null
                        : bilCashEntry.getBilRctReceiveDt());
        batchPayment.setDueDate(bilCashEntry.getBilAdjDueDt().compareTo(DateRoutine.defaultDateTime()) == 0 ? null
                : bilCashEntry.getBilAdjDueDt());
        batchPayment
                .setPaymentType(busCdTranslationRepository
                        .findAllByCodeAndTypeList(bilCashEntry.getBilRctTypeCd(),
                                Arrays.asList("CPY", "PYT", "PYC", "COL"), execContext.getLanguage())
                        .get(0).getBusDescription());
        batchPayment
                .setSuspenseReason(!bilCashEntry.getBilSusReasonCd().trim().isEmpty() ? busCdTranslationRepository
                        .findByCodeAndType(bilCashEntry.getBilSusReasonCd(),
                                BusCodeTranslationType.DIRECT_TYPE.getValue(), execContext.getLanguage())
                        .getBusDescription() : BLANK_STRING);
        batchPayment.setPaymentOnHold(bilCashEntry.getBilManualSusInd() == CHAR_Y);
        batchPayment.setPaymentId(bilCashEntry.getBilRctId().trim());
        batchPayment.setPayableItem(bilCashEntry.getBilPblItemCd().trim());
        batchPayment.setAdditionalId(bilCashEntry.getBilAdditionalId().trim());
        if (!bilCashEntry.getBilRctCltId().trim().isEmpty()) {
            batchPayment.setRemitterId(bilCashEntry.getBilRctCltId().trim());
            Party remitterClient = getPayorName(bilCashEntry.getBilRctCltId().trim());
            if (remitterClient != null) {
                batchPayment.setRemitterName(remitterClient.getName());
            } else {
                batchPayment.setRemitterName(BLANK_STRING);
            }

        }

        batchPayment.setPolicySymbol(bilCashEntry.getPolSymbolCd().trim());
        batchPayment.setPolicyNumber(bilCashEntry.getPolNbr().trim());
        batchPayment.setIsUnidentified(false);
        batchPayment.setReviewCode(BLANK_STRING);
        batchPayment.setCurrentDue(DECIMAL_ZERO);
        batchPayment.setTotalDue(DECIMAL_ZERO);
        batchPayment.setIdentifier(BLANK_STRING);
        batchPayment.setIdentifierType(BLANK_STRING);

        if (bilCashEntry.getBilAccountNbr().trim().compareTo(BLANK_STRING) != 0) {
            BilAccount bilAccount = bilAccountRepository.findByAccountNumber(bilCashEntry.getBilAccountNbr().trim());

            batchPayment.setIdentifier(bilCashEntry.getBilAccountNbr().trim());
            batchPayment.setIdentifierType(getIdentifierTypeDescription(AccountType.BILLACCOUNT.toString()));

            if (bilAccount != null) {
                batchPayment.setAccountId(bilAccount.getAccountId());
                batchPayment.setType(AccountType.DIRECTBILL.toString());
                Party payor = getPayorName(bilAccount.getPayorClientId());
                if (payor != null) {
                    batchPayment.setPayorName(payor.getName());
                    batchPayment
                            .setPayorAddress(getAddress(payor.getAddresses(), bilAccount.getAddressSequenceNumber()));
                } else {
                    batchPayment.setPayorName(BLANK_STRING);
                    batchPayment.setPayorAddress(BLANK_STRING);
                }
                verifyAccountData(batchPayment, bilAccount, bilCashEntry.getBilAccountNbr().trim());
                if (!batchPayment.getPolicyNumber().trim().isEmpty()) {
                    getPolicyRow(batchPayment, bilCashEntry.getBilAdjDueDt());
                    if (!isDetailSw && !batchPayment.getPolicyNumber().trim().isEmpty()) {
                        batchPayment.setIdentifier(bilCashEntry.getPolSymbolCd().trim().concat(SEPARATOR_BLANK)
                                .concat(bilCashEntry.getPolNbr().trim()));
                        batchPayment.setIdentifierType(POLICY_IDENTIFIER);
                    }
                } else {
                    getAccountData(batchPayment, bilCashEntry.getBilAdjDueDt(), false, false);
                }
                currencyMatch = multiCurrencyService.compareCurrency(currency, bilAccount.getCurrencyCode().trim());
            } else {
                if (!batchPayment.getPolicyNumber().trim().isEmpty()) {
                    batchPayment.setReviewCode("PNF");
                } else {
                    batchPayment.setReviewCode("ANP");
                }
                batchPayment.setIsUnidentified(true);
                batchPayment.setCurrentDue(DECIMAL_ZERO);
                batchPayment.setTotalDue(DECIMAL_ZERO);
            }

        } else if (bilCashEntry.getBilTtyNbr().trim().compareTo(BLANK_STRING) != 0) {
            BilThirdParty bilThirdParty = bilThirdPartyRepository.findByBilTtyNbr(bilCashEntry.getBilTtyNbr().trim());
            batchPayment.setIdentifier(bilCashEntry.getBilTtyNbr().trim());
            if (bilThirdParty != null) {
                batchPayment.setAccountId(bilThirdParty.getBillThirdPartyId());
                batchPayment.setType(AccountType.GROUPBILL.toString());
                Party payor = getPayorName(bilThirdParty.getBtpTtyClientId());
                if (payor != null) {
                    batchPayment.setPayorName(payor.getName());
                    batchPayment.setPayorAddress(getAddress(payor.getAddresses(), bilThirdParty.getBtpTtyAdrSeq()));
                } else {
                    batchPayment.setPayorName(BLANK_STRING);
                    batchPayment.setPayorAddress(BLANK_STRING);
                }
                batchPayment.setIdentifierType(getIdentifierTypeDescription(AccountType.GROUP_ACCOUNT.toString()));
                getThirdPartyData(batchPayment, bilCashEntry.getBilAdjDueDt());

                currencyMatch = multiCurrencyService.compareCurrency(currency, bilThirdParty.getCurrencyCd().trim());
            } else {
                batchPayment.setIdentifierType(getIdentifierTypeDescription(AccountType.GROUP_ACCOUNT.toString()));
                batchPayment.setReviewCode("N3F");
                batchPayment.setCurrentDue(DECIMAL_ZERO);
                batchPayment.setTotalDue(DECIMAL_ZERO);
                batchPayment.setIsUnidentified(true);
            }
        } else if (bilCashEntry.getBilAgtActNbr().trim().compareTo(BLANK_STRING) != 0) {
            BilAgent bilAgent = bilAgentRepository.findByBilAgtActNbr(bilCashEntry.getBilAgtActNbr().trim());
            batchPayment.setIdentifier(bilCashEntry.getBilAgtActNbr().trim());
            if (bilAgent != null) {
                batchPayment.setAccountId(bilAgent.getBilAgtActId());
                batchPayment.setType(AccountType.AGENCYBILL.toString());
                Party payor = getPayorName(bilAgent.getBagAgtPayCltId());
                if (payor != null) {
                    batchPayment.setPayorName(payor.getName());
                    batchPayment.setPayorAddress(getAddress(payor.getAddresses(), bilAgent.getBagAgtPayAdr()));
                } else {
                    batchPayment.setPayorName(BLANK_STRING);
                    batchPayment.setPayorAddress(BLANK_STRING);
                }
                batchPayment.setIdentifierType(getIdentifierTypeDescription(AccountType.AGENCY_ACCOUNT.toString()));
                getAgentData(batchPayment, bilCashEntry.getBilAdjDueDt());

                currencyMatch = multiCurrencyService.compareCurrency(currency, bilAgent.getCurrencyCd().trim());
            } else {
                batchPayment.setIdentifierType(getIdentifierTypeDescription(AccountType.AGENCY_ACCOUNT.toString()));
                batchPayment.setReviewCode("GNP");
                batchPayment.setCurrentDue(DECIMAL_ZERO);
                batchPayment.setTotalDue(DECIMAL_ZERO);
                batchPayment.setIsUnidentified(true);
            }
        } else if (bilCashEntry.getPolNbr().trim().compareTo(BLANK_STRING) != 0) {
            getPolicyRow(batchPayment, bilCashEntry.getBilAdjDueDt());
            if (batchPayment.getAccountId() != null && !batchPayment.getAccountId().isEmpty()) {
                BilAccount bilAccount = bilAccountRepository.findById(batchPayment.getAccountId()).orElse(null);
                if (bilAccount != null) {
                    Party payor = getPayorName(bilAccount.getPayorClientId());
                    if (payor != null) {
                        batchPayment.setPayorName(payor.getName());
                        batchPayment.setPayorAddress(
                                getAddress(payor.getAddresses(), bilAccount.getAddressSequenceNumber()));
                    } else {
                        batchPayment.setPayorName(BLANK_STRING);
                        batchPayment.setPayorAddress(BLANK_STRING);
                    }

                    currencyMatch = multiCurrencyService.compareCurrency(currency, bilAccount.getCurrencyCode().trim());
                }
            }

            if (!isDetailSw) {
                batchPayment.setIdentifier(bilCashEntry.getPolSymbolCd().trim().concat(SEPARATOR_BLANK)
                        .concat(bilCashEntry.getPolNbr().trim()));
                batchPayment.setIdentifierType(POLICY_IDENTIFIER);
            }

            BusCdTranslation busCdTranslation = busCdTranslationRepository
                    .findByCodeAndType(batchPayment.getPolicySymbol().trim(), "SYM", execContext.getLanguage());
            if (busCdTranslation != null) {
                batchPayment.setPolicySymbol(busCdTranslation.getBusDescription());
            }
        }

        batchPayment.setPayableItem(formatPayableItemCode(bilCashEntry.getBilPblItemCd().trim()));
        if (batchPayment.getReviewCode().isEmpty()) {
            opyUpyCheck(batchPayment, bilCashEntry);
        }

        if (!currencyMatch) {
            batchPayment.setReviewCode("CNM");
        }
        if (!batchPayment.getReviewCode().isEmpty()) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository
                    .findByCodeAndType(batchPayment.getReviewCode(), "REV", execContext.getLanguage());
            if (busCdTranslation != null) {
                batchPayment.setReviewCode(busCdTranslation.getBusDescription().trim());
            }
        }
        if (Boolean.TRUE.equals(Boolean.valueOf(multiCurrency))) {
            TransRateHst transRateHst = transRateHstRepository.findById(new TransRateHstId(entryDt, entryNbr,
                    bilCashEntry.getBilCashEntryId().getUserId(), bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr()))
                    .orElse(null);
            if (transRateHst != null) {
                batchPayment.setReceivedCurrency(transRateHst.getReceivedCurrency().trim());
                batchPayment.setReceivedAmount(transRateHst.getReceivedAmount());
                batchPayment.setRate(transRateHst.getRate());
            }
        }
        batchPayment.setIsEdit(false);
        batchPayment.setIsDelete(false);
        if ((bilCashEntryTot.getBctAcceptInd() != CHAR_Y && bilCashEntryTot.getBctAcceptInd() != CHAR_D)
                && (userId.equals(execContext.getUserSequenceId()) || batchAdmin)) {
            batchPayment.setIsEdit(true);
            batchPayment.setIsDelete(true);
        }
        return batchPayment;
    }

    private String getAddress(List<Address> addresses, short addressSequenceNumber) {
        if (addresses != null && !addresses.isEmpty()) {
            for (Address address : addresses) {
                if (address.getAddressSequence() != null
                        && address.getAddressSequence() == (int) addressSequenceNumber) {
                    return address.getAddress();
                }
            }
        }

        return BLANK_STRING;
    }

    private String formatPayableItemCode(String payableItemCode) {
        if (!payableItemCode.trim().isEmpty()) {
            List<String> payableItemList = new ArrayList<>();
            payableItemList.add(BusCodeTranslationType.BILLING_ITEM.getValue());
            payableItemList.add(BusCodeTranslationType.AMOUNT.getValue());
            payableItemList.add(BusCodeTranslationType.BPA.getValue());
            payableItemList.add(BusCodeTranslationType.COMMISION.getValue());
            payableItemList.add(BusCodeTranslationType.EIT.getValue());
            payableItemList.add(BusCodeTranslationType.BPS.getValue());

            List<BusCdTranslation> busCodeTranslationList = busCdTranslationRepository
                    .findAllByCodeAndTypeListAndParentCode(payableItemCode, payableItemList,
                            BusCodeTranslationParentCode.PAYABLE_ITEM.getValue(), execContext.getLanguage());
            if (busCodeTranslationList != null && !busCodeTranslationList.isEmpty()) {
                return busCodeTranslationList.get(0).getBusDescription().trim();
            } else {
                throw new DataNotFoundException("support.data.missing", new Object[] { "Payable Item" });
            }
        } else {
            return BLANK_STRING;
        }
    }

    private Payment opyUpyCheck(Payment payment, BilCashEntry bilCashEntry) {
        if (bilCashEntry.getBilAgtActNbr().trim().compareTo(BLANK_STRING) != 0) {
            if (bilCashEntry.getBilRctAmt() > payment.getTotalDue()) {
                payment.setReviewCode("OPY");
            } else {
                if (bilCashEntry.getBilRctAmt() < payment.getTotalDue()) {
                    payment.setReviewCode("UPY");
                } else {
                    payment.setReviewCode(BLANK_STRING);
                }
            }
        } else {
            if (bilCashEntry.getBilRctAmt() > payment.getCurrentDue()) {
                payment.setReviewCode("OPY");
            } else {
                if (bilCashEntry.getBilRctAmt() < payment.getCurrentDue()) {
                    payment.setReviewCode("UPY");
                } else {
                    payment.setReviewCode(BLANK_STRING);
                }
            }
        }
        return payment;
    }

    private Payment verifyAccountData(Payment payment, BilAccount bilAccount, String entryAccountNumber) {
        if (bilAccount.getBillTypeCd().trim().equals("SP")
                && !entryAccountNumber.equals(bilAccount.getAccountNumber().trim())) {
            payment.setReviewCode("ACR");
        }
        List<String> agentId = bilAgtActRltRepository.getBilAgtActIdByAccountId(bilAccount.getAccountId());
        if (agentId != null && !agentId.isEmpty()) {
            payment.setReviewCode("CAG");
            payment.setCurrentDue(DECIMAL_ZERO);
            payment.setTotalDue(DECIMAL_ZERO);
        }
        return payment;
    }

    private Payment getAgentData(Payment payment, ZonedDateTime dueDate) {
        Double totalDue = null;
        if (dueDate.compareTo(DateRoutine.defaultDateTime()) == 0) {
            List<Object[]> minNetAndReconciliationAmount = bilInvTtyActRepository
                    .findMinNetAndReconciliationAmount(payment.getAccountId(), TECH_KEY_TYPE_CODE, STATEMENT_ROW);
            Double minDueAmount = checkForNull((Double) minNetAndReconciliationAmount.get(0)[0]);
            Double sttReconciliationAmount = checkForNull((Double) minNetAndReconciliationAmount.get(0)[1]);
            totalDue = minDueAmount - sttReconciliationAmount;
        } else {
            totalDue = calculateDueAmount(payment.getAccountId(), dueDate);
        }
        if (totalDue == null || totalDue == DECIMAL_ZERO) {
            payment.setReviewCode("NSD");
            totalDue = DECIMAL_ZERO;
        }

        payment.setTotalDue(totalDue);
        payment.setCurrentDue(totalDue);
        return payment;
    }

    private Double calculateDueAmount(String accountId, ZonedDateTime dueDate) {

        ZonedDateTime statementDate = null;
        BilInvTtyAct bilInvTtyAct = bilInvTtyActRepository.getRowBuDuDate(accountId, TECH_KEY_TYPE_CODE, BLANK_STRING,
                dueDate, STATEMENT_ROW, Arrays.asList(StatementReconciliationIndicator.INCOMPLETE.getValue()));
        if (bilInvTtyAct != null) {
            statementDate = bilInvTtyAct.getBilInvTtyActId().getBilInvDt();
        }

        Double dueAmount = checkForNull(
                bilInvTtyActRepository.getAgencyStatementDueAmount(accountId, TECH_KEY_TYPE_CODE, "00", dueDate));

        Double receivedAmount = getAmountReceived(accountId, dueDate);
        Double pendingActivityAmount = checkForNull(bilSttReconDtlRepository.getStatementAmount(accountId,
                TECH_KEY_TYPE_CODE, statementDate,
                Arrays.asList(ReconciliationProcessType.WRITEOFF.getValue(), ReconciliationProcessType.DEFER.getValue(),
                        ReconciliationProcessType.EXCLUDE.getValue()),
                StatementReconciliationIndicator.PENDING.getValue(), statementTypeList));

        Double completedActivityAmount = checkForNull(
                bilSttReconDtlRepository.getStatementAmount(accountId, TECH_KEY_TYPE_CODE, statementDate,
                        Arrays.asList(ReconciliationProcessType.DEFER.getValue(),
                                ReconciliationProcessType.EXCLUDE.getValue()),
                        StatementReconciliationIndicator.COMPLETE.getValue(), statementTypeList));

        Double receivableAmount = checkForNull(
                bilSttReconDtlRepository.getStatementAmount(accountId, TECH_KEY_TYPE_CODE, statementDate,
                        Arrays.asList(ReconciliationProcessType.ADD_RECEIVABLE.getValue()), statementTypeList));

        return BigDecimal.valueOf(dueAmount).subtract(BigDecimal.valueOf(pendingActivityAmount))
                .subtract(BigDecimal.valueOf(completedActivityAmount)).add(BigDecimal.valueOf(receivableAmount))
                .subtract(BigDecimal.valueOf(receivedAmount)).doubleValue();
    }

    private Double getAmountReceived(String bilAccountId, ZonedDateTime dueDate) {
        Double receivedAmount = bilAgtCashDspRepository.findDspAmountByDspTypeCode(bilAccountId, dueDate,
                BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                ManualSuspendIndicator.ENABLED.getValue());
        if (receivedAmount != null) {
            return receivedAmount;
        }
        return DECIMAL_ZERO;
    }

    private Double checkForNull(Double amount) {
        if (amount == null) {
            return DECIMAL_ZERO;
        } else {
            return amount;
        }
    }

    private Payment getThirdPartyData(Payment payment, ZonedDateTime dueDate) {
        Double sttDueAmt = null;

        if (dueDate.compareTo(DateRoutine.defaultDateTime()) == 0) {
            sttDueAmt = calclateOutstandingBlanceAmount(payment.getAccountId());
        } else {
            sttDueAmt = calculateGroupDueAmount(payment.getAccountId(), dueDate);
        }

        if (sttDueAmt == null || sttDueAmt == DECIMAL_ZERO) {
            payment.setReviewCode("N3C");
            sttDueAmt = DECIMAL_ZERO;
        }

        payment.setCurrentDue(sttDueAmt);
        return payment;
    }

    private Double calclateOutstandingBlanceAmount(String accountId) {
        Double dueAmount = checkForNull(bilInvTtyActRepository.getStatementDueAmount(accountId, CHAR_T, "00"));

        Double writeOffAndWaiveSumAmount = checkForNull(
                bilSttReconDtlRepository.getStatementsAmount(accountId, TechnicalKeyTypeCode.THIRD_PARTY.getValue(),
                        Arrays.asList(ReconGroupProcessType.WRITEOFF.getValue(),
                                ReconGroupProcessType.WAIVE.getValue()),
                        StatementReconciliationIndicator.PENDING.getValue(), statementTypeList));

        Double deferAndWaiveAmount = checkForNull(
                bilSttReconDtlRepository.getStatementsAmount(accountId, TechnicalKeyTypeCode.THIRD_PARTY.getValue(),
                        Arrays.asList(ReconGroupProcessType.DEFER.getValue(), ReconGroupProcessType.WAIVE.getValue()),
                        StatementReconciliationIndicator.ACCEPTED.getValue(), statementTypeList));

        Double receivableAmount = checkForNull(
                bilSttReconDtlRepository.getStatementsAmount(accountId, TechnicalKeyTypeCode.THIRD_PARTY.getValue(),
                        Arrays.asList(ReconGroupProcessType.ADD_RECEIVABLE.getValue()),
                        StatementReconciliationIndicator.PENDING.getValue(), statementTypeList));
        Double unidReceivableAmount = checkForNull(bilInvSttItmRepository.getStatementsReceivableAmount(accountId,
                TechnicalKeyTypeCode.THIRD_PARTY.getValue(), StatementReconciliationIndicator.PENDING.getValue()));

        BigDecimal totalOutstandingAmount = BigDecimal.valueOf(dueAmount)
                .subtract(BigDecimal.valueOf(writeOffAndWaiveSumAmount))
                .subtract(BigDecimal.valueOf(deferAndWaiveAmount)).add(BigDecimal.valueOf(receivableAmount))
                .add(BigDecimal.valueOf(unidReceivableAmount));

        return totalOutstandingAmount.doubleValue();
    }

    private Payment getAccountData(Payment payment, ZonedDateTime dueDate, boolean isParty, boolean isValidPolicy) {
        Double acctCurrDue = null;
        Integer rowCount = accountRowsCount(payment.getAccountId(), dueDate);
        if (rowCount == null || rowCount == 0) {
            rowCount = 0;
            acctCurrDue = DECIMAL_ZERO;
        } else {
            acctCurrDue = getAccountCurrentDue(payment.getAccountId(), dueDate);
            if (acctCurrDue == null) {
                acctCurrDue = DECIMAL_ZERO;
                payment.setReviewCode("ERR");
                payment.setTotalDue(DECIMAL_ZERO);
                payment.setCurrentDue(DECIMAL_ZERO);
            }
        }

        List<Object> scgAmountObj = getScgAmount(payment.getAccountId(), dueDate);
        Integer scgRowCnt = (Integer) scgAmountObj.get(0);
        Double scgAmount = (Double) scgAmountObj.get(1);

        if (rowCount == 0 && scgRowCnt == 0) {
            payment.setReviewCode("OPY");
        } else {
            acctCurrDue = scgAmount + acctCurrDue;
            if (!isValidPolicy) {
                if (acctCurrDue == DECIMAL_ZERO) {
                    if (payment.getReviewCode().isEmpty()) {
                        payment.setReviewCode("NAC");
                    }
                    payment.setCurrentDue(DECIMAL_ZERO);
                } else {
                    payment.setCurrentDue(acctCurrDue);
                }
            }
        }

        Double totalDueAmt = getTotalDueAmount(payment.getAccountId(), dueDate, isParty);
        if (totalDueAmt == null) {
            totalDueAmt = DECIMAL_ZERO;
            if (payment.getReviewCode().isEmpty()) {
                payment.setReviewCode("NAT");
            }
        }

        totalDueAmt = getTotalCrgAmount1(payment.getAccountId(), isParty) + totalDueAmt;
        totalDueAmt = getTotalCrgAmount2(payment.getAccountId(), isParty) + totalDueAmt;
        payment.setTotalDue(totalDueAmt);
        return payment;
    }

    private Integer getScgRowCount(String accountId, ZonedDateTime bilAdjDueDt) {
        return bilCrgAmountsRepository.getRowCountNumber(Arrays.asList(CHAR_Y, CHAR_L, CHAR_B), accountId, bilAdjDueDt,
                DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt), DEFAULT_DATE);
    }

    private List<Object> getScgAmount(String accountId, ZonedDateTime bilAdjDueDt) {
        List<Object> scgAmountObject = new ArrayList<>();
        Integer scgRowCnt = null;
        Double scgAmount = null;

        scgRowCnt = getScgRowCount(accountId, bilAdjDueDt);
        if (scgRowCnt == null || scgRowCnt == 0) {
            scgRowCnt = 0;
        } else {
            scgAmount = bilCrgAmountsRepository.getServiceChargeAmount(accountId, Arrays.asList(CHAR_Y, CHAR_L, CHAR_B),
                    bilAdjDueDt, DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt), DEFAULT_DATE);
        }
        scgAmountObject.add(scgRowCnt);
        scgAmountObject.add(scgAmount == null ? DECIMAL_ZERO : scgAmount);
        return scgAmountObject;
    }

    private Double getAccountCurrentDue(String accountId, ZonedDateTime bilAdjDueDt) {
        Double currDueAmt = null;
        currDueAmt = bilIstScheduleRepository.getCurrentlDueAmountByAccount(accountId,
                Arrays.asList(CHAR_Y, CHAR_L, CHAR_B), bilAdjDueDt, DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt),
                DEFAULT_DATE);
        if (currDueAmt == null) {
            return DECIMAL_ZERO;
        }
        return currDueAmt;
    }

    private Integer accountRowsCount(String accountId, ZonedDateTime dueDate) {
        return bilIstScheduleRepository.getRowCountNumber(accountId, Arrays.asList(CHAR_Y, CHAR_L, CHAR_B), dueDate,
                DateRoutine.dateTimeAsYYYYMMDDString(dueDate), DEFAULT_DATE);
    }

    private Double getTotalCrgAmount1(String accountId, boolean isParty) {
        Double totalCrgAmount1 = null;
        if (!isParty) {
            totalCrgAmount1 = bilCrgAmountsRepository.getTotalCrgAmount1(Arrays.asList(CHAR_Y, CHAR_L), accountId,
                    CHAR_S);
        }
        if (totalCrgAmount1 == null) {
            totalCrgAmount1 = DECIMAL_ZERO;
        }
        return totalCrgAmount1;
    }

    private Double getTotalCrgAmount2(String accountId, boolean isParty) {
        Double totalCrgAmount2 = null;
        if (!isParty) {
            totalCrgAmount2 = bilCrgAmountsRepository.getTotalCrgAmount2(Arrays.asList(CHAR_P, CHAR_L, CHAR_D),
                    accountId);
        }
        if (totalCrgAmount2 == null) {
            totalCrgAmount2 = DECIMAL_ZERO;
        }
        return totalCrgAmount2;
    }

    private Double getTotalDueAmount(String accountId, ZonedDateTime bilAdjDueDt, boolean isParty) {
        Double totalDueAmt = null;
        if (isParty) {
            totalDueAmt = bilIstScheduleRepository.getTotalDueAmount(accountId, bilAdjDueDt,
                    DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt), DEFAULT_DATE);
        } else {
            totalDueAmt = bilIstScheduleRepository.getTotalDueAmountByAccountId(accountId);
        }

        return totalDueAmt;
    }

    private Party getPayorName(String clientId) {

        try {
            Party partySearch = partyElasticSearchServiceImpl.fetchPartyDocument(clientId.trim());
            if (partySearch != null) {
                return partySearch;
            }
        } catch (IOException e) {
            logger.error("Error in fetching the Party Document");
        }
        return null;
    }

    private Payment getPolicyRow(Payment payment, ZonedDateTime dueDate) {
        payment.setCurrentDue(DECIMAL_ZERO);
        payment.setTotalDue(DECIMAL_ZERO);
        int cnrCount = 0;
        List<BilPolicy> bilPolicyList = null;

        if (!payment.getPolicyNumber().trim().isEmpty() && !payment.getIdentifier().trim().isEmpty()) {
            bilPolicyList = bilPolicyRepository.findAccountAndPolicyNumber(payment.getIdentifier(),
                    payment.getPolicyNumber(), payment.getPolicySymbol(), Arrays.asList('W', 'Y', 'Z'));
        } else {
            bilPolicyList = bilPolicyRepository.findPolicyIdByPolicyNumber(payment.getPolicyNumber(),
                    payment.getPolicySymbol(), Arrays.asList('W', 'Y', 'Z'));
        }

        Integer rowCnt = null;
        String cnrPolId = BLANK_STRING;
        String accountId = BLANK_STRING;
        String policyId = BLANK_STRING;
        String holdPolicyId = BLANK_STRING;
        String holdAccountId = BLANK_STRING;
        String prevPolicyId = BLANK_STRING;
        String preAccountId = BLANK_STRING;
        int policyCount = 0;
        int issuesCount = 0;
        int notIssuesCount = 0;
        boolean isValidPolicy = false;
        boolean isSplit = false;
        boolean isMultiAccts = false;
        boolean isMultiCnrPol = false;
        for (BilPolicy bilPolicyTO : bilPolicyList) {
            accountId = bilPolicyTO.getBillPolicyId().getBilAccountId();
            policyId = bilPolicyTO.getBillPolicyId().getPolicyId();

            if (bilPolicyTO.getBilIssueInd() != 'W') {
                rowCnt = bilPolicyTermRepository.getPolicyRowCount(bilPolicyTO.getBillPolicyId().getBilAccountId(),
                        bilPolicyTO.getBillPolicyId().getPolicyId(), 'T');
                if (rowCnt == null || rowCnt == 0) {
                    accountId = preAccountId;
                    policyId = prevPolicyId;
                }
            }
            prevPolicyId = accountId;
            preAccountId = policyId;
            ++policyCount;

            if (bilPolicyTO.getBilIssueInd() == 'W') {
                ++issuesCount;
                if (!cnrPolId.isEmpty() && cnrPolId.compareTo(policyId) != 0) {
                    isMultiCnrPol = true;
                }
                cnrPolId = policyId;
            } else {
                ++notIssuesCount;
                if (policyId.compareTo(holdPolicyId) == 0) {
                    isSplit = true;
                }
                if (!holdAccountId.isEmpty() && holdAccountId.compareTo(accountId) != 0) {
                    isMultiAccts = true;
                }
                holdPolicyId = policyId;
                holdAccountId = accountId;
            }
        }

        if (policyCount == 0) {
            payment.setReviewCode("PNF");
            payment.setCurrentDue(DECIMAL_ZERO);
            payment.setTotalDue(DECIMAL_ZERO);
            payment.setIsUnidentified(true);
            return payment;
        }

        if (policyCount - issuesCount > 1) {
            if (isSplit) {
                payment.setReviewCode("SPF");
                if (payment.getIdentifier().trim().isEmpty()) {
                    payment.setIsUnidentified(true);
                }

            } else {
                if (isMultiAccts) {
                    payment.setReviewCode("MAF");
                    if (payment.getIdentifier().trim().isEmpty()) {
                        payment.setIsUnidentified(true);
                    }
                } else {
                    payment.setReviewCode("DPF");
                }
            }
            payment.setCurrentDue(DECIMAL_ZERO);
            payment.setTotalDue(DECIMAL_ZERO);
            return payment;
        }

        if (policyCount > 1 && policyCount - issuesCount == 1) {
            isValidPolicy = true;
            if (cnrCount > 0) {
                payment.setReviewCode("PCR");
            }
        } else {
            if (policyCount == issuesCount) {
                if (isMultiCnrPol) {
                    payment.setReviewCode("DPF");
                    payment.setCurrentDue(DECIMAL_ZERO);
                    payment.setTotalDue(DECIMAL_ZERO);
                    if (payment.getIdentifier().trim().isEmpty()) {
                        payment.setIsUnidentified(true);
                    }
                    return payment;
                } else {
                    checkBptPolNbr(accountId, policyId, payment);
                    payment.setReviewCode("PCR");
                }
            } else {
                if (policyCount == notIssuesCount) {
                    isValidPolicy = true;
                    if (cnrCount > 0) {
                        payment.setReviewCode("PCR");
                    }
                }
            }
        }

        if (payment.getReviewCode().compareTo("PCR") == 0 && !isValidPolicy) {
            if (cnrCount > 3) {
                payment.setCurrentDue(DECIMAL_ZERO);
                payment.setTotalDue(DECIMAL_ZERO);
            } else {
                payment.setReviewCode(BLANK_STRING);
                ++cnrCount;
                getPolicyRow(payment, dueDate);
            }
        }

        List<String> agtId = bilAgtActRltRepository.getBilAgtActIdByAccountId(accountId);
        if (agtId != null && !agtId.isEmpty()) {
            payment.setReviewCode("CAG");
            payment.setCurrentDue(DECIMAL_ZERO);
            payment.setTotalDue(DECIMAL_ZERO);
        }

        boolean isValidAmtType = false;
        if (payment.getPayableItem() != null && !payment.getPayableItem().trim().isEmpty() && isValidPolicy) {
            isValidAmtType = true;
            Short maxSeq = bilIstScheduleRepository.getMaxSeqNbrByBilPayableItem(accountId, policyId,
                    payment.getPayableItem());
            if (maxSeq == null) {
                payment.setReviewCode("TNF");
                payment.setCurrentDue(DECIMAL_ZERO);
                payment.setTotalDue(DECIMAL_ZERO);
                isValidAmtType = false;
            }

            if (isValidAmtType) {
                getPolicyPayableItem(payment, accountId, policyId, payment.getPayableItem(), dueDate);
            }
        }
        payment.setAccountId(accountId);

        if (payment.getCurrentDue() == 0 && payment.getTotalDue() == 0 && isValidPolicy) {
            getPolicyData(payment, accountId, policyId, dueDate, isValidPolicy);
        }

        if (payment.getCurrentDue() == 0 && payment.getTotalDue() == 0) {
            getAccountData(payment, dueDate, false, isValidPolicy);
        }

        return payment;
    }

    private Payment checkBptPolNbr(String bilAccountId, String policyId, Payment payment) {
        ZonedDateTime bptPolEffectiveDt = bilPolicyTermRepository.getMaxPolicyEffectiveDate(bilAccountId, policyId);
        if (bptPolEffectiveDt != null) {
            BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository.getPolicyRow(bilAccountId, policyId,
                    bptPolEffectiveDt);
            if (bilPolicyTerm != null) {
                payment.setPolicySymbol(bilPolicyTerm.getBptCnrPolSymCd());
                payment.setPolicyNumber(bilPolicyTerm.getBptCnrPolNbr());
            }
        }
        return payment;
    }

    private Payment getPolicyPayableItem(Payment payment, String accountId, String policyId, String payableItem,
            ZonedDateTime dueDate) {
        Double payableItemCurrDue = null;
        Integer payableItemRowCount = payableItemRowsCount(accountId, policyId, payableItem, dueDate);
        if (payableItemRowCount == null) {
            payment.setReviewCode("OPY");
            payment.setCurrentDue(DECIMAL_ZERO);
            payment.setTotalDue(DECIMAL_ZERO);
        } else {
            payableItemCurrDue = getpayableItemCurrentDue(accountId, policyId, payableItem, dueDate);
            if (payableItemCurrDue != null) {
                if (payableItemCurrDue == 0) {
                    if (payment.getReviewCode().isEmpty()) {
                        payment.setReviewCode("NTC");
                    }
                    payment.setCurrentDue(DECIMAL_ZERO);
                } else {
                    List<Object> scgAmountObj = getScgAmount(accountId, dueDate);
                    Double scgAmount = (Double) scgAmountObj.get(1);
                    payment.setCurrentDue(payableItemCurrDue + scgAmount);
                }
            }
        }

        Double payableItemTotDueAmt = getPayableItemTotalDue(accountId, policyId, payableItem, dueDate);
        if (payableItemTotDueAmt == null || payableItemTotDueAmt == 0) {
            payment.setReviewCode("NTT");
            payment.setTotalDue(DECIMAL_ZERO);
        }

        return payment;
    }

    private Double getPayableItemTotalDue(String accountId, String policyId, String payableItem,
            ZonedDateTime bilAdjDueDt) {
        Double totalDueAmt = null;
        totalDueAmt = bilIstScheduleRepository.getTotalDueAmountByPayableItem(accountId, policyId, payableItem,
                bilAdjDueDt, DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt), DEFAULT_DATE);
        return totalDueAmt;
    }

    private Double getpayableItemCurrentDue(String accountId, String policyId, String payableItem,
            ZonedDateTime bilAdjDueDt) {
        Double currDueAmt = null;
        currDueAmt = bilIstScheduleRepository.getCurrentDueAmountByPayableItem(accountId, policyId, payableItem,
                Arrays.asList(CHAR_Y, CHAR_L), bilAdjDueDt, DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt),
                DEFAULT_DATE);
        return currDueAmt;
    }

    private Integer payableItemRowsCount(String accountId, String policyId, String payableItem, ZonedDateTime dueDate) {
        return bilIstScheduleRepository.getRowCountNumberByPayableItem(accountId, policyId, payableItem,
                Arrays.asList(CHAR_Y, CHAR_L), dueDate, DateRoutine.dateTimeAsYYYYMMDDString(dueDate), DEFAULT_DATE);
    }

    private Payment getPolicyData(Payment payment, String accountId, String policyId, ZonedDateTime dueDate,
            boolean isValidPolicy) {

        boolean isCharge = countPolicies(payment.getCurrentDue(), payment.getTotalDue(), isValidPolicy, accountId,
                policyId);

        Double polCurrDue = null;
        Integer polRowsCnt = policyRowsCount(accountId, policyId, dueDate);
        if (polRowsCnt == null || polRowsCnt == 0) {
            if (payment.getReviewCode().isEmpty()) {
                payment.setReviewCode("OPY");
            }
            payment.setCurrentDue(DECIMAL_ZERO);
        } else {
            polCurrDue = getPolicyCurrentDue(accountId, policyId, dueDate);
            if (polCurrDue != null) {
                payment.setCurrentDue(polCurrDue);
                if (isCharge) {
                    List<Object> scgAmountObj = getScgAmount(payment.getAccountId(), dueDate);
                    Double scgAmount = (Double) scgAmountObj.get(1);
                    payment.setCurrentDue(scgAmount + payment.getCurrentDue());
                }
                if (payment.getCurrentDue() == 0 && payment.getReviewCode().isEmpty()) {
                    payment.setReviewCode("NPC");
                }
            }
        }

        Double polTotDueAmt = getPolicyTotalDue(accountId, policyId, dueDate);
        if (polTotDueAmt == null || polTotDueAmt == 0) {
            payment.setReviewCode("NPT");
            payment.setTotalDue(DECIMAL_ZERO);
            polTotDueAmt = DECIMAL_ZERO;
        }

        if (isCharge) {
            polTotDueAmt = getTotalCrgAmount1(accountId, false) + polTotDueAmt;
            polTotDueAmt = getTotalCrgAmount2(accountId, false) + polTotDueAmt;
        }
        payment.setTotalDue(polTotDueAmt);
        return payment;
    }

    private Double getPolicyTotalDue(String accountId, String policyId, ZonedDateTime bilAdjDueDt) {
        Double totalDueAmt = null;
        totalDueAmt = bilIstScheduleRepository.getTotalDueAmountByPolicyId(accountId, policyId, bilAdjDueDt,
                DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt), DEFAULT_DATE);
        return totalDueAmt;
    }

    private Double getPolicyCurrentDue(String accountId, String policyId, ZonedDateTime bilAdjDueDt) {
        Double currDueAmt = null;
        currDueAmt = bilIstScheduleRepository.getCurrentDueAmountByPolicyId(accountId, policyId,
                Arrays.asList(CHAR_Y, CHAR_L), bilAdjDueDt, DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt),
                DEFAULT_DATE);
        return currDueAmt;
    }

    private Integer policyRowsCount(String accountId, String policyId, ZonedDateTime dueDate) {
        return bilIstScheduleRepository.getRowCountNumberByPolicyId(accountId, policyId, Arrays.asList(CHAR_Y, CHAR_L),
                dueDate, DateRoutine.dateTimeAsYYYYMMDDString(dueDate), DEFAULT_DATE);
    }

    private boolean countPolicies(Double currentDue, Double totalDue, boolean isValidPolicy, String accountId,
            String policyId) {
        boolean isCharge = false;
        if (currentDue == 0 && totalDue == 0 && isValidPolicy) {
            BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
            if (bilAccount != null && bilAccount.getBillTypeCd().compareTo("SP") == 0) {
                isCharge = true;

            }
        }
        BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(policyId, accountId)).orElse(null);
        if (bilPolicy == null) {
            isCharge = true;
        }
        return isCharge;
    }

    private String getIdentifierTypeDescription(String accountType) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(accountType,
                BusCodeTranslationType.BILLACCOUNTTYPE.getValue(), execContext.getLanguage());
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusDescription();
        }
        return null;
    }

    @Override
    public Payment findBatchPaymentDetails(String batchId, String postedDate, short paymentSeqNumber, String userId) {
        Payment batchPayment;
        boolean batchAdmin = true;
        try {
            securityService.authenticateFunction(BATCH_ADMIN);
        } catch (AppAuthenticationException e) {
            batchAdmin = false;
        }
        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(postedDate);
        String currency = null;
        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(bilEntryDt, batchId, userId)).orElse(null);
        if (bilCashEntryTot != null) {
            currency = bilCashEntryTot.getCurrencyCd().trim();
        }
        BilCashEntry bilCashEntryRow = bilCashEntryRepository
                .findById(new BilCashEntryId(bilEntryDt, batchId, userId, paymentSeqNumber)).orElse(null);
        if (bilCashEntryRow == null) {
            throw new DataNotFoundException(BATCH_PAYMENT_NOT_FOUND, new Object[] { batchId });
        }

        String userName = securityService
                .getUserNameByUserSeqId(bilCashEntryRow.getBilCashEntryId().getUserId().trim());
        batchPayment = mapBilCashEntryToBatchPayment(bilCashEntryRow, userName, currency, true, batchAdmin, userId, bilCashEntryTot);
        if (batchPayment.getIdentifierType().equals(POLICY_IDENTIFIER)) {
            batchPayment.setIdentifier(BLANK_STRING);
            batchPayment.setIdentifierType(BLANK_STRING);
        }
        return batchPayment;
    }

    @Override
    public void deleteBatchPayment(String batchId, String postedDate, short paymentSeqNumber, String userId) {
        if (!userId.equals(execContext.getUserSeqeunceId())) {
            try {
                securityService.authenticateFunction(BATCH_ADMIN);
            } catch (AppAuthenticationException e) {
                throw new AppAuthenticationException("userId.unauthorized.batchPayment");
            }
        }

        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(postedDate);
        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(bilEntryDt, batchId, userId)).orElse(null);
        if (bilCashEntryTot == null) {
            throw new DataNotFoundException("batch.notfound", new Object[] { batchId });
        }
        if (bilCashEntryTot.getBctAcceptInd() != CHAR_N && bilCashEntryTot.getBctDepositInd() != CHAR_N) {
            throw new InvalidDataException("payment.delete.not.valid");
        }

        BilCashEntry bilCashEntryRow = bilCashEntryRepository.findById(
                new BilCashEntryId(DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId, userId, paymentSeqNumber))
                .orElse(null);
        if (bilCashEntryRow == null) {
            throw new DataNotFoundException(BATCH_PAYMENT_NOT_FOUND, new Object[] { batchId });
        }

        bilCashEntryRepository.delete(bilCashEntryRow);

        if (Boolean.TRUE.equals(Boolean.valueOf(multiCurrency))) {
            TransRateHst transRateHst = transRateHstRepository
                    .findById(new TransRateHstId(bilEntryDt, batchId, userId, paymentSeqNumber)).orElse(null);
            if (transRateHst != null) {
                transRateHstRepository.delete(transRateHst);
            }
        }

    }

    @Override
    public void patchBatchPayment(String batchId, String postedDate, short paymentSeqNumber, Payment batchPayment,
            String userId) {
        if (!userId.equals(execContext.getUserSeqeunceId())) {
            try {
                securityService.authenticateFunction(BATCH_ADMIN);
            } catch (AppAuthenticationException e) {
                throw new AppAuthenticationException("userId.unauthorized.batchPayment");
            }
        }
        BilCashEntry bilCashEntry = applyBatchPayment(batchId, postedDate, batchPayment, paymentSeqNumber, userId);
        bilCashEntryRepository.save(bilCashEntry);

    }

    private BilCashEntry applyBatchPayment(String batchId, String postedDate, Payment batchPayment,
            short paymentSeqNumber, String userId) {

        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(postedDate);
        BilCashEntry existingRow = bilCashEntryRepository
                .findById(new BilCashEntryId(bilEntryDt, batchId, userId, paymentSeqNumber)).orElse(null);
        if (existingRow == null) {
            throw new DataNotFoundException(BATCH_PAYMENT_NOT_FOUND, new Object[] { batchId });
        }

        if (batchPayment.getPaymentAmount() != null
                && !batchPayment.getPaymentAmount().equals(existingRow.getBilRctAmt())) {
            existingRow.setBilRctAmt(batchPayment.getPaymentAmount());
        }

        if (batchPayment.getPaymentDate() != null
                && batchPayment.getPaymentDate().compareTo(existingRow.getBilRctReceiveDt()) != 0) {
            existingRow.setBilRctReceiveDt(batchPayment.getPaymentDate());
        } else if (batchPayment.getPaymentDate() == null) {
            existingRow.setBilRctReceiveDt(DateRoutine.defaultDateTime());
        }

        if (batchPayment.getDueDate() != null
                && batchPayment.getDueDate().compareTo(existingRow.getBilAdjDueDt()) != 0) {
            existingRow.setBilAdjDueDt(batchPayment.getDueDate());
        } else if (batchPayment.getDueDate() == null) {
            existingRow.setBilAdjDueDt(DateRoutine.defaultDateTime());
        }

        if (batchPayment.getPostmarkDate() != null
                && batchPayment.getPostmarkDate().compareTo(existingRow.getBilPstMrkDt()) != 0) {
            existingRow.setBilPstMrkDt(batchPayment.getPostmarkDate());
        } else if (batchPayment.getPostmarkDate() == null) {
            existingRow.setBilPstMrkDt(DateRoutine.defaultDateTime());
        }

        if (batchPayment.getAdditionalId() != null && !batchPayment.getAdditionalId().trim().isEmpty()
                && !batchPayment.getAdditionalId().trim().equalsIgnoreCase(existingRow.getBilAdditionalId().trim())) {
            existingRow.setBilAdditionalId(batchPayment.getAdditionalId().trim());
        }
        if (batchPayment.getPaymentId() != null && !batchPayment.getPaymentId().trim().isEmpty()
                && !batchPayment.getPaymentId().trim().equalsIgnoreCase(existingRow.getBilRctId().trim())) {
            existingRow.setBilRctId(batchPayment.getPaymentId().trim());
        }
        if (batchPayment.getPolicySymbol() != null && !batchPayment.getPolicySymbol().trim().isEmpty()
                && !batchPayment.getPolicySymbol().trim().equalsIgnoreCase(existingRow.getPolSymbolCd().trim())) {
            existingRow.setPolSymbolCd(batchPayment.getPolicySymbol().trim());
        }

        if (batchPayment.getPolicyNumber() != null && !batchPayment.getPolicyNumber().trim().isEmpty()
                && !batchPayment.getPolicyNumber().trim().equalsIgnoreCase(existingRow.getPolNbr().trim())) {
            existingRow.setPolNbr(batchPayment.getPolicyNumber().trim());
        }

        if (batchPayment.getPayableItem() != null && !batchPayment.getPayableItem().trim().isEmpty()
                && !batchPayment.getPayableItem().trim().equalsIgnoreCase(existingRow.getBilPblItemCd().trim())) {
            existingRow.setBilPblItemCd(batchPayment.getPayableItem().trim());
        }

        if (batchPayment.getPaymentOnHold() != null && batchPayment.getPaymentOnHold()) {
            existingRow.setBilManualSusInd(CHAR_Y);
        } else {
            existingRow.setBilManualSusInd(CHAR_N);
            existingRow.setBilSusReasonCd(BLANK_STRING);
        }

        if (batchPayment.getSuspenseReason() != null && !batchPayment.getSuspenseReason().trim().isEmpty()
                && !batchPayment.getSuspenseReason().trim().equalsIgnoreCase(existingRow.getBilSusReasonCd().trim())) {
            existingRow.setBilSusReasonCd(batchPayment.getSuspenseReason().trim());
        }

        if (batchPayment.getPaymentType() != null && !batchPayment.getPaymentType().trim().isEmpty()
                && !batchPayment.getPaymentType().trim().equalsIgnoreCase(existingRow.getBilRctTypeCd().trim())) {
            existingRow.setBilRctTypeCd(batchPayment.getPaymentType().trim());
        }

        if (batchPayment.getPaymentComments() != null && !batchPayment.getPaymentComments().trim().isEmpty()
                && !batchPayment.getPaymentComments().trim().equalsIgnoreCase(existingRow.getBilRctCmt().trim())) {
            existingRow.setBilRctCmt(batchPayment.getPaymentComments().trim());
        }

        if (!batchPayment.getIdentifierType().isEmpty()) {

            switch (AccountType.getEnumKey(batchPayment.getIdentifierType().trim())) {
            case BILLACCOUNT:
                existingRow.setBilAccountNbr(batchPayment.getIdentifier().trim());
                existingRow.setBilAgtActNbr(BLANK_STRING);
                existingRow.setBilTtyNbr(BLANK_STRING);
                break;
            case AGENCY_ACCOUNT:
                existingRow.setBilAgtActNbr(batchPayment.getIdentifier().trim());
                existingRow.setBilAccountNbr(BLANK_STRING);
                existingRow.setBilTtyNbr(BLANK_STRING);
                break;
            case GROUP_ACCOUNT:
                existingRow.setBilTtyNbr(batchPayment.getIdentifier().trim());
                existingRow.setBilAccountNbr(BLANK_STRING);
                existingRow.setBilAgtActNbr(BLANK_STRING);
                break;
            default:
                break;
            }
        }

        return existingRow;
    }

    @Override
    public Short saveBatchPayment(String batchId, String postedDate, Payment batchPayment) {
        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(postedDate);
        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(bilEntryDt, batchId, execContext.getUserSeqeunceId())).orElse(null);
        Short paymentSeq = null;
        if (bilCashEntryTot != null) {
            if (checkRuleEnabled(BilRules.BUSINESS_GROUP.getValue(), BLANK_STRING, BLANK_STRING, BLANK_STRING,
                    execContext.getApplicationDate(), execContext.getApplicationDate())) {
                if (batchPayment.getIdentifierType() != null && !batchPayment.getIdentifierType().isEmpty()) {

                    String bilTypeCode = "";

                    switch (AccountType.getEnumKey(batchPayment.getIdentifierType())) {
                    case BILLACCOUNT:

                        BilAccount bilAccount = bilAccountRepository
                                .findByAccountNumber(batchPayment.getIdentifier().trim());
                        if (bilAccount != null) {
                            bilTypeCode = bilAccount.getBillTypeCd();
                        }
                        break;
                    case AGENCY_ACCOUNT:

                        BilAgent bilAgent = bilAgentRepository.findByBilAgtActNbr(batchPayment.getIdentifier().trim());

                        if (bilAgent != null) {
                            bilTypeCode = bilAgent.getBilTypeCd();
                        }

                        break;
                    case GROUP_ACCOUNT:

                        BilThirdParty bilThirdParty = bilThirdPartyRepository
                                .findByBilTtyNbr(batchPayment.getIdentifier().trim());

                        if (bilThirdParty != null) {
                            bilTypeCode = bilThirdParty.getBilTypeCd();
                        }

                        break;
                    default:
                        break;
                    }
                    if (bilTypeCode != null && !bilTypeCode.isEmpty()) {
                        String businesGroupCodeOfBank = businessGroupService
                                .getBusinessGroup(BilFunctionCtl.BANK.toString(), bilCashEntryTot.getBilBankCd());
                        String businessGroupOfAccount = businessGroupService
                                .getBusinessGroup(BilFunctionCtl.BUSINESSGROUP.toString(), bilTypeCode);

                        if (businesGroupCodeOfBank == null || businessGroupOfAccount == null
                                || !businesGroupCodeOfBank.equals(businessGroupOfAccount)) {
                            throw new InvalidDataException("deposite.bank.not.valid");
                        }
                    }

                }
                if ((batchPayment.getIdentifierType() == null || batchPayment.getIdentifierType().trim().isEmpty())
                        && batchPayment.getPolicyNumber() != null && !batchPayment.getPolicyNumber().trim().isEmpty()) {

                    String bilTypeCode = "";

                    List<BilPolicy> bilPolicyList = bilPolicyRepository
                            .findByPolicyNumber(batchPayment.getPolicyNumber().trim());
                    if (bilPolicyList != null && !bilPolicyList.isEmpty()) {
                        bilTypeCode = bilPolicyList.get(0).getBilTypeCd();
                    }
                    if (bilTypeCode != null && !bilTypeCode.isEmpty()) {
                        String businesGroupCodeOfBank = businessGroupService
                                .getBusinessGroup(BilFunctionCtl.BANK.toString(), bilCashEntryTot.getBilBankCd());
                        String businessGroupOfAccount = businessGroupService
                                .getBusinessGroup(BilFunctionCtl.BUSINESSGROUP.toString(), bilTypeCode);

                        if (businesGroupCodeOfBank == null || businessGroupOfAccount == null
                                || !businesGroupCodeOfBank.equals(businessGroupOfAccount)) {
                            throw new InvalidDataException("deposite.bank.not.valid");
                        }
                    }

                }

            }

            BilCashEntry bilCashEntry = mapPaymentToBilCashEntry(batchId, bilEntryDt, batchPayment);
            bilCashEntryRepository.save(bilCashEntry);
            paymentSeq = bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr();

        }

        return paymentSeq;
    }

    private BilCashEntry mapPaymentToBilCashEntry(String batchId, ZonedDateTime postedDate, Payment batchPayment) {
        BilCashEntry bilCashEntry = new BilCashEntry();
        bilCashEntry.setBilCashEntryId(new BilCashEntryId(postedDate, batchId, execContext.getUserSeqeunceId(),
                getMaxbilEntrySeqNbr(postedDate, batchId)));
        bilCashEntry.setInsertedRowTs(execContext.getApplicationDateTime());
        bilCashEntry.setBilPmtOrderId(BLANK_STRING);
        bilCashEntry.setBilCrcrdArspTyc(BLANK_STRING);
        bilCashEntry.setBilCrcrdPstCd(BLANK_STRING);
        bilCashEntry.setAgencyNbr(BLANK_STRING);
        bilCashEntry.setBecChgUserId(execContext.getUserSeqeunceId());
        bilCashEntry.setBilRctCmt(batchPayment.getPaymentComments().trim());
        bilCashEntry.setBilPstMrkCd(BLANK_CHAR);
        bilCashEntry.setBilPstMrkDt(batchPayment.getPostmarkDate());
        bilCashEntry.setBilCwaId(BLANK_STRING);
        bilCashEntry.setBilCrcrdAutApv(BLANK_STRING);
        bilCashEntry.setBilCrcrdExpDt(BLANK_STRING);
        bilCashEntry.setBilCrcrdActNbr(BLANK_STRING);
        bilCashEntry.setBilCrcrdArspCd(BLANK_CHAR);
        bilCashEntry.setBilCrcrdTypeCd(BLANK_CHAR);
        bilCashEntry.setBilTaxYearNbr((short) execContext.getApplicationDate().getYear());
        bilCashEntry.setBilRctReceiveDt(batchPayment.getPaymentDate());
        bilCashEntry.setBilRctAdrSeq(SHORT_ZERO);
        bilCashEntry.setBilRctCltId(batchPayment.getRemitterId());
        bilCashEntry.setBilRctId(batchPayment.getPaymentId());
        bilCashEntry.setBilSusReasonCd(batchPayment.getSuspenseReason());
        if (batchPayment.getPaymentOnHold() != null && batchPayment.getPaymentOnHold()) {
            bilCashEntry.setBilManualSusInd(CHAR_Y);
        } else {
            bilCashEntry.setBilManualSusInd(CHAR_N);
        }
        bilCashEntry.setBilRctTypeCd(batchPayment.getPaymentType());
        bilCashEntry.setBilRctAmt(batchPayment.getPaymentAmount());
        bilCashEntry.setBilAdjDueDt(batchPayment.getDueDate());
        bilCashEntry.setPolNbr(batchPayment.getPolicyNumber());
        bilCashEntry.setPolSymbolCd(batchPayment.getPolicySymbol());
        if (!batchPayment.getPolicyNumber().isEmpty()) {
            bilCashEntry.setBilPblItemCd(batchPayment.getPayableItem());
        }
        bilCashEntry.setBilAdditionalId(batchPayment.getAdditionalId());

        if (!batchPayment.getIdentifierType().isEmpty()) {

            switch (AccountType.getEnumKey(batchPayment.getIdentifierType())) {
            case BILLACCOUNT:
                bilCashEntry.setBilAccountNbr(batchPayment.getIdentifier());
                bilCashEntry.setBilAgtActNbr(BLANK_STRING);
                bilCashEntry.setBilTtyNbr(BLANK_STRING);
                break;
            case AGENCY_ACCOUNT:
                bilCashEntry.setBilAgtActNbr(batchPayment.getIdentifier());
                bilCashEntry.setBilAccountNbr(BLANK_STRING);
                bilCashEntry.setBilTtyNbr(BLANK_STRING);
                break;
            case GROUP_ACCOUNT:
                bilCashEntry.setBilTtyNbr(batchPayment.getIdentifier());
                bilCashEntry.setBilAccountNbr(BLANK_STRING);
                bilCashEntry.setBilAgtActNbr(BLANK_STRING);
                break;
            default:
                break;
            }
        }
        return bilCashEntry;
    }

    private short getMaxbilEntrySeqNbr(ZonedDateTime postedDate, String batchId) {
        Short maxSeqNbr = bilCashEntryRepository.getMaxBilEntrySeqByBilEntry(postedDate, batchId,
                execContext.getUserSeqeunceId());
        return (maxSeqNbr != null ? (short) (maxSeqNbr + 1) : SHORT_ZERO);
    }

    private Double calculateGroupDueAmount(String accountId, ZonedDateTime dueDate) {

        ZonedDateTime statementDate = null;
        BilInvTtyAct bilInvTtyAct = bilInvTtyActRepository.getRowBuDuDate(accountId, TECH_KEY_TYPE_CODE, BLANK_STRING,
                dueDate, STATEMENT_ROW, Arrays.asList(StatementReconciliationIndicator.INCOMPLETE.getValue(),
                        StatementReconciliationIndicator.PENDING_REPROCESS.getValue()));
        if (bilInvTtyAct != null) {
            statementDate = bilInvTtyAct.getBilInvTtyActId().getBilInvDt();
        }

        Double statementAmountReceived = checkForNull(bilTtyCashDspRepository.getStatementAmountReceived(accountId,
                BilDspTypeCode.SUSPENDED.getValue(), dueDate, CHAR_Y, BLANK_CHAR));

        Double statementDueAmount = checkForNull(
                bilInvTtyActRepository.getStatementDueAmount(accountId, CHAR_T, "00", dueDate));

        Double reversalAmount = checkForNull(
                bilInvTtyActRepository.getReverseDueAmount(accountId, CHAR_T, "02", dueDate));

        Double pendingActivityAmount = checkForNull(bilSttReconDtlRepository.getStatementAmount(accountId, CHAR_T,
                statementDate,
                Arrays.asList(ReconGroupProcessType.WRITEOFF.getValue(), ReconGroupProcessType.WAIVE.getValue(),
                        ReconGroupProcessType.DEFER.getValue(), ReconGroupProcessType.EXCLUDE.getValue()),
                StatementReconciliationIndicator.PENDING.getValue(), Arrays.asList(
                        StatementLevelType.ACCOUNT_LEVEL.getValue(), StatementLevelType.DETAIL_LEVEL.getValue())));

        Double completedActivityAmount = checkForNull(bilSttReconDtlRepository.getStatementAmount(accountId, CHAR_T,
                statementDate,
                Arrays.asList(ReconGroupProcessType.DEFER.getValue(), ReconGroupProcessType.WAIVE.getValue(),
                        ReconGroupProcessType.EXCLUDE.getValue()),
                StatementReconciliationIndicator.ACCEPTED.getValue(), Arrays.asList(
                        StatementLevelType.ACCOUNT_LEVEL.getValue(), StatementLevelType.DETAIL_LEVEL.getValue())));

        Double receivableAmount = checkForNull(bilSttReconDtlRepository.getStatementAmount(accountId, CHAR_T,
                statementDate, Arrays.asList(ReconGroupProcessType.ADD_RECEIVABLE.getValue()), Arrays.asList(
                        StatementLevelType.ACCOUNT_LEVEL.getValue(), StatementLevelType.DETAIL_LEVEL.getValue())));

        Double unidReceivableAmount = checkForNull(
                bilInvSttItmRepository.getSumForStatementReceivable(accountId, CHAR_T, statementDate));

        BigDecimal totalDueAmount = BigDecimal.valueOf(statementDueAmount).add(BigDecimal.valueOf(reversalAmount))
                .subtract(BigDecimal.valueOf(pendingActivityAmount))
                .subtract(BigDecimal.valueOf(completedActivityAmount)).add(BigDecimal.valueOf(receivableAmount))
                .add(BigDecimal.valueOf(unidReceivableAmount)).subtract(BigDecimal.valueOf(statementAmountReceived));
        return totalDueAmount.doubleValue();
    }

    private boolean checkRuleEnabled(String ruleId, String bilTypeCd, String bilClassCd, String bilPlanCd,
            ZonedDateTime effectiveDt, ZonedDateTime expirationDt) {
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(ruleId, bilTypeCd, bilClassCd, bilPlanCd,
                effectiveDt, expirationDt);
        return bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y;
    }

}
