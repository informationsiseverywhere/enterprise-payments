package billing.service.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_G;
import static core.utils.CommonConstants.CHAR_L;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import application.security.service.impl.AppSecurityServiceImpl;
import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilAccountBalance;
import billing.data.entity.BilAgent;
import billing.data.entity.BilAgtCashRct;
import billing.data.entity.BilBankEntry;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilEftActivity;
import billing.data.entity.BilEftExtPyt;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilThirdParty;
import billing.data.entity.BilTtyCashRct;
import billing.data.entity.TransRateHst;
import billing.data.entity.id.BilAccountBalanceId;
import billing.data.entity.id.BilBankEntryId;
import billing.data.entity.id.BilCashEntryId;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilEftExtPytId;
import billing.data.entity.id.TransRateHstId;
import billing.data.repository.BilAccountBalanceRepository;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilAgentRepository;
import billing.data.repository.BilAgtCashDspRepository;
import billing.data.repository.BilAgtCashRctRepository;
import billing.data.repository.BilBankEntryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilEftActivityRepository;
import billing.data.repository.BilEftExtPytRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilScheduleCashRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.data.repository.BilTtyCashDspRepository;
import billing.data.repository.BilTtyCashRctRepository;
import billing.data.repository.TransRateHstRepository;
import billing.model.Payment;
import billing.model.PaymentList;
import billing.model.embedded.Party;
import billing.service.PaymentService;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilDesReasonCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilTechKeyTypeCode;
import billing.utils.BillingConstants.BillingEftTransactionStatus;
import billing.utils.BillingConstants.BusCodeTranslationParentCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CashEntryMethod;
import billing.utils.BillingConstants.CashProcessingCode;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.ManualSuspendIndicator;
import billing.utils.BillingConstants.PaymentType;
import billing.utils.BillingConstants.ProcessIndicator;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import billing.utils.BillingConstants.TechnicalKeyTypeCode;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import core.utils.Formatter;
import core.utils.MultiFiltersSearch;
import core.utils.PageNavigation;
import party.service.impl.PartyElasticSearchServiceImpl;

@Service("paymentService")
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    private BilAgentRepository bilAgentRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private PartyElasticSearchServiceImpl partyElasticSearchServiceImpl;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilTtyCashRctRepository bilTtyCashRctRepository;

    @Autowired
    private BilAgtCashRctRepository bilAgtCashRctRepository;

    @Autowired
    private BilBankEntryRepository bilBankEntryRepository;

    @Autowired
    private GroupPaymentServiceImpl groupPaymentServiceImpl;

    @Autowired
    private BillAccountPaymentServiceImpl billAccountPaymentServiceImpl;

    @Autowired
    private UnIdentifiedAccountPaymentServiceImpl unIdentifiedAccountPaymentServiceImpl;

    @Autowired
    private BilTtyCashDspRepository bilTtyCashDspRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilEftActivityRepository bilEftActivityRepository;

    @Autowired
    private BilScheduleCashRepository bilScheduleCashRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilAgtCashDspRepository bilAgtCashDspRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilAccountBalanceRepository bilAccountBalanceRepository;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    @Autowired
    private TransRateHstRepository transRateHstRepository;

    @Autowired
    private BilEftExtPytRepository bilEftExtPytRepository;

    @Value("${multi.currency}")
    String multiCurrency;

    @Value("${billing.url}")
    private String externalBillingService;

    @Value("${group.url}")
    private String externalGroupService;

    @Value("${agency.url}")
    private String externalAgencyService;

    private static final Logger logger = LogManager.getLogger(PaymentServiceImpl.class);
    private static final String SINCE_DATE = "since";
    private static final String UNTIL_DATE = "until";
    private static final String TO_AMOUNT = "toAmount";
    private static final String FROM_AMOUNT = "fromAmount";
    private static final String PAYMENT_METHOD = "paymentMethod";
    private static final String PENDING_REVERSAL = "Pending Reversal";
    private static final String IDENTIFIER_TYPE = "identifierType";
    private static final String PAYMENTS_LIT = "/payments";
    private static final String USER_ID = "userId";
    private static final String PAYMENT_ID = "paymentId";
    private static final String CURRENCY = "currency";
    private static final String PAYMENT_DESCRPTION_INVALID = "paymentDescription.invalid";

    @Override
    public Payment findPayment(String batchId, short sequenceNumber, String postedDate, String user) {

        String userId = appSecurityServiceImpl.authenticateUser(user.trim()).getUserSequenceId();
        BilCashEntry bilCashEntry = bilCashEntryRepository
                .findById(
                        new BilCashEntryId(DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId, userId, sequenceNumber))
                .orElse(null);

        if (bilCashEntry == null) {
            throw new DataNotFoundException("payment.not.found", new Object[] { batchId,
                    DateRoutine.dateTimeAsMMDDYYYYAsString(DateRoutine.dateTimeAsYYYYMMDD(postedDate)), user });
        }

        return getPaymentDetails(bilCashEntry);
    }

    @Override
    public PaymentList findPayments(int page, int size, String filters) {
        Page<BilCashEntry> bilCashEntryPage = null;
        int totalRows = 0;
        Map<String, String> matchFilterMap = new HashMap<>();
        if (filters != null) {
            matchFilterMap = MultiFiltersSearch.getFilters(filters, matchFilterMap);
        }
        ZonedDateTime sinceDate = DateRoutine.defaultSinceDateTime();
        ZonedDateTime untilDate = DateRoutine.defaultDateTime();
        Double fromAmount = 0d;
        Double toAmount = 999999999999d;
        String fromUserId = BLANK_STRING;
        String toUserId = Formatter.generateField(BilCashEntryId.class, USER_ID);
        String fromPaymentId = BLANK_STRING;
        String toPaymentId = Formatter.generateField(BilCashEntry.class, "bilRctId");
        String identifierType = null;
        String currency = null;
        List<Character> cashEntryMethodList = getCashEntryMethodList();
        if (matchFilterMap.get(SINCE_DATE) != null) {
            sinceDate = DateRoutine.dateTimeAsYYYYMMDD(matchFilterMap.get(SINCE_DATE));
        }
        if (matchFilterMap.get(UNTIL_DATE) != null) {
            untilDate = DateRoutine.dateTimeAsYYYYMMDD(matchFilterMap.get(UNTIL_DATE));
        }
        if (matchFilterMap.get(FROM_AMOUNT) != null) {
            fromAmount = Double.parseDouble(matchFilterMap.get(FROM_AMOUNT));
        }
        if (matchFilterMap.get(TO_AMOUNT) != null) {
            toAmount = Double.parseDouble(matchFilterMap.get(TO_AMOUNT));
        }
        if (matchFilterMap.get(CURRENCY) != null) {
            currency = matchFilterMap.get(CURRENCY);
        }
        if (matchFilterMap.get(PAYMENT_METHOD) != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    matchFilterMap.get(PAYMENT_METHOD).trim(), BusCodeTranslationType.PAYMENTMETHOD.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                Character cashEntryMethod = busCdTranslation.getBusCdTranslationId().getBusCd().charAt(SHORT_ZERO);
                cashEntryMethodList.clear();
                cashEntryMethodList.add(cashEntryMethod);
            } else {
                return null;
            }
        }
        if (matchFilterMap.get(USER_ID) != null) {
            if (!matchFilterMap.get(USER_ID.trim()).equals(execContext.getUserId().trim())) {
                try {
                    fromUserId = appSecurityServiceImpl.authenticateUser(matchFilterMap.get(USER_ID))
                            .getUserSequenceId();
                } catch (NullPointerException exception) {
                    logger.error("userId:{} does not exit. Setting it to blank", matchFilterMap.get(USER_ID.trim()));
                    fromUserId = BLANK_STRING;
                }
            } else {
                fromUserId = execContext.getUserSeqeunceId();
            }
            if (fromUserId.length() > 8) {
                return null;
            }
            toUserId = fromUserId;
        }
        if (matchFilterMap.get(PAYMENT_ID) != null) {
            fromPaymentId = matchFilterMap.get(PAYMENT_ID);
            toPaymentId = fromPaymentId;
        }
        if (matchFilterMap.get(IDENTIFIER_TYPE) != null) {
            identifierType = matchFilterMap.get(IDENTIFIER_TYPE);
        }

        bilCashEntryPage = getPaymentWithFilters(cashEntryMethodList, toAmount, fromAmount, sinceDate, untilDate, page,
                size, identifierType, fromUserId, toUserId, fromPaymentId, toPaymentId, currency);

        if (bilCashEntryPage != null && bilCashEntryPage.getTotalElements() != 0) {
            totalRows = (int) bilCashEntryPage.getTotalElements();
        }
        if (totalRows == 0) {
            return null;
        }
        List<Payment> paymentList = new LinkedList<>();

        if (bilCashEntryPage != null) {
            for (BilCashEntry bilCashEntryRow : bilCashEntryPage) {
                paymentList.add(getPaymentRows(bilCashEntryRow));
            }
        }
        PaymentList payments = new PaymentList();
        payments.setPayments(paymentList);
        payments.setPage(PageNavigation.derivePageAttributes(totalRows, size, page));
        return payments;
    }

    @Transactional
    @Override
    public Payment makeAPayment(Payment payment) {
        String identifierType = BLANK_STRING;
        if (Boolean.TRUE.equals(payment.getIsUnidentified())) {
            identifierType = payment.getIdentifierType();
            payment.setIdentifierType(AccountType.UNIDENTIFIED.toString());
        }
        switch (AccountType.getEnumKey(payment.getIdentifierType())) {

        case GROUP_ACCOUNT:
            payment = groupPaymentServiceImpl.makeAPayment(payment);
            break;
        case BILLACCOUNT:
            payment = billAccountPaymentServiceImpl.makeAPayment(payment);
            break;
        case AGENCY_ACCOUNT:
            break;
        default:
            payment.setIdentifierType(identifierType);
            payment = unIdentifiedAccountPaymentServiceImpl.makeAPayment(payment);
            break;

        }

        return payment;
    }

    private Page<BilCashEntry> getPaymentWithFilters(List<Character> cashEntryMethodList, Double toAmount,
            Double fromAmount, ZonedDateTime sinceDate, ZonedDateTime untilDate, int page, int size,
            String identifierType, String fromUserId, String toUserId, String fromPaymentId, String toPaymentId,
            String currency) {

        Page<BilCashEntry> bilCashEntryPage = null;
        if (identifierType != null && !identifierType.trim().isEmpty()) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(identifierType,
                    BusCodeTranslationType.BILLACCOUNTTYPE.getValue(), execContext.getLanguage());
            identifierType = busCdTranslation != null ? busCdTranslation.getBusCdTranslationId().getBusCd().trim()
                    : BLANK_STRING;

            switch (AccountType.getEnumKey(identifierType)) {
            case GROUP_ACCOUNT:
                if (currency != null && !currency.trim().isEmpty()) {
                    bilCashEntryPage = bilCashEntryRepository.getAllPaymentsByGroupWithCurrency(sinceDate, untilDate,
                            fromAmount, toAmount, cashEntryMethodList, BLANK_STRING, fromUserId, toUserId,
                            fromPaymentId, toPaymentId, currency, PageRequest.of(page - 1, size));
                } else {
                    bilCashEntryPage = bilCashEntryRepository.getAllPaymentsByGroupWithReceiptDate(sinceDate, untilDate,
                            fromAmount, toAmount, cashEntryMethodList, BLANK_STRING, fromUserId, toUserId,
                            fromPaymentId, toPaymentId, PageRequest.of(page - 1, size));
                }
                break;
            case BILLACCOUNT:
                if (currency != null && !currency.trim().isEmpty()) {
                    bilCashEntryPage = bilCashEntryRepository.getAllPaymentsByBillAccountWithCurrency(sinceDate,
                            untilDate, fromAmount, toAmount, cashEntryMethodList, BLANK_STRING, fromUserId, toUserId,
                            fromPaymentId, toPaymentId, currency, PageRequest.of(page - 1, size));
                } else {
                    bilCashEntryPage = bilCashEntryRepository.getAllPaymentsByBillAccountWithReceiptDate(sinceDate,
                            untilDate, fromAmount, toAmount, cashEntryMethodList, BLANK_STRING, fromUserId, toUserId,
                            fromPaymentId, toPaymentId, PageRequest.of(page - 1, size));
                }
                break;

            case AGENCY_ACCOUNT:
                if (currency != null && !currency.trim().isEmpty()) {
                    bilCashEntryPage = bilCashEntryRepository.getAllPaymentsByAgencyWithCurrency(sinceDate, untilDate,
                            fromAmount, toAmount, cashEntryMethodList, BLANK_STRING, fromUserId, toUserId,
                            fromPaymentId, toPaymentId, currency, PageRequest.of(page - 1, size));
                } else {
                    bilCashEntryPage = bilCashEntryRepository.getAllPaymentsByAgencyWithReceiptDate(sinceDate,
                            untilDate, fromAmount, toAmount, cashEntryMethodList, BLANK_STRING, fromUserId, toUserId,
                            fromPaymentId, toPaymentId, PageRequest.of(page - 1, size));
                }
                break;
            default:
                break;
            }
        } else {
            if (currency != null && !currency.trim().isEmpty()) {
                bilCashEntryPage = bilCashEntryRepository.getAllPaymentsByReceiptDateAndCurrency(sinceDate, untilDate,
                        fromAmount, toAmount, cashEntryMethodList, CHAR_Y, fromUserId, toUserId, fromPaymentId,
                        toPaymentId, currency, PageRequest.of(page - 1, size));
            } else {
                bilCashEntryPage = bilCashEntryRepository.getAllPaymentsByReceiptDate(sinceDate, untilDate, fromAmount,
                        toAmount, cashEntryMethodList, CHAR_Y, fromUserId, toUserId, fromPaymentId, toPaymentId,
                        PageRequest.of(page - 1, size));
            }
        }

        return bilCashEntryPage;

    }

    private Payment getPaymentDetails(BilCashEntry bilCashEntryRow) {

        Payment payment = new Payment();
        ZonedDateTime entryDate = bilCashEntryRow.getBilCashEntryId().getBilEntryDt();
        String userId = bilCashEntryRow.getBilCashEntryId().getUserId();
        String batchId = bilCashEntryRow.getBilCashEntryId().getBilEntryNbr();

        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(entryDate, batchId, userId)).orElse(null);

        if (bilCashEntryTot == null) {
            throw new DataNotFoundException("no.data.found");
        }
        payment.setCurrency(multiCurrencyService.getCurrencyCode(bilCashEntryTot.getCurrencyCd()));
        payment.setPaymentSequenceNumber(bilCashEntryRow.getBilCashEntryId().getBilEntrySeqNbr());
        payment.setPaymentAmount(bilCashEntryRow.getBilRctAmt());
        payment.setPaymentType(
                getPaymentTypeDescription(bilCashEntryRow.getBilRctTypeCd(), bilCashEntryTot.getBilCshEtrMthCd()));
        payment.setPaymentMethod(getPaymentMethodDescription(String.valueOf(bilCashEntryTot.getBilCshEtrMthCd())));
        payment.setPostedDate(bilCashEntryRow.getBilCashEntryId().getBilEntryDt());
        payment.setPaymentDate(bilCashEntryRow.getBilRctReceiveDt());
        if (!bilCashEntryRow.getBilPstMrkDt().equals(DateRoutine.defaultDateTime())) {
            payment.setPostmarkDate(bilCashEntryRow.getBilPstMrkDt());
        }
        payment.setBatchId(bilCashEntryRow.getBilCashEntryId().getBilEntryNbr());
        payment.setDepositBank(
                getCodeDescription(bilCashEntryTot.getBctDepBankCd().trim(), BusCodeTranslationType.BANK.getValue()));
        payment.setControlBank(
                getCodeDescription(bilCashEntryTot.getBilBankCd().trim(), BusCodeTranslationType.BANK.getValue()));
        payment.setUserId(appSecurityServiceImpl
                .getUserIdByUserSeqId(bilCashEntryRow.getBilCashEntryId().getUserId().trim()).trim());
        payment.setUserName(appSecurityServiceImpl
                .getUserNameByUserSeqId(bilCashEntryRow.getBilCashEntryId().getUserId().trim()).trim());
        payment.setPaymentComments(bilCashEntryRow.getBilRctCmt().trim());
        payment.setPaymentId(bilCashEntryRow.getBilRctId().trim());
        payment.setAdditionalId(bilCashEntryRow.getBilAdditionalId().trim());
        payment.setPolicyNumber(bilCashEntryRow.getPolNbr().trim());
        payment.setPolicySymbol(bilCashEntryRow.getPolSymbolCd().trim());
        payment.setPayableItem(formatPayableItemCode(bilCashEntryRow.getBilPblItemCd().trim()));

        char methodCode = getPayorInformation(bilCashEntryRow, payment, true);
        if (StringUtils.isNotBlank(bilCashEntryRow.getBilPmtOrderId())) {
            payment.setPaymentId(bilCashEntryRow.getBilPmtOrderId().trim());
        }
        if (StringUtils.isNotBlank(bilCashEntryRow.getBilRctId())) {
            payment.setTransactionId(bilCashEntryRow.getBilRctId().trim());
        }
        if (StringUtils.isNotBlank(bilCashEntryRow.getBilCrcrdPstCd())) {
            payment.setPostalCode(bilCashEntryRow.getBilCrcrdPstCd().trim());
        }
        if (StringUtils.isNotBlank(bilCashEntryRow.getBilCrcrdAutApv())) {
            payment.setAuthorization(bilCashEntryRow.getBilCrcrdAutApv().trim());
        }
        if (methodCode == PaymentType.CREDIT_CARD.getValue().charAt(SHORT_ZERO)) {
            payment.setCreditCardNumber(bilCashEntryRow.getBilCrcrdActNbr().trim());
            payment.setCreditCardExpiryDate(bilCashEntryRow.getBilCrcrdExpDt().trim());
            payment.setCreditCardType(getCodeDescription(String.valueOf(bilCashEntryRow.getBilCrcrdTypeCd()),
                    BusCodeTranslationType.CREDIT_CARD_TYPE.getValue()));
        } else if (methodCode == PaymentType.BANK_ACCOUNT.getValue().charAt(SHORT_ZERO)) {
            payment.setTransactionId(bilCashEntryRow.getBilRctId().trim());
            BilBankEntry bilBankEntry = bilBankEntryRepository.findById(
                    new BilBankEntryId(bilCashEntryRow.getBilCashEntryId().getBilEntryDt(), payment.getBatchId(),
                            bilCashEntryRow.getBilCashEntryId().getUserId().trim(), payment.getPaymentSequenceNumber()))
                    .orElse(null);
            if (bilBankEntry == null) {
                throw new DataNotFoundException("no.data.found");
            }
            payment.setBankAccountNumber(bilBankEntry.getBankAccountNbr().trim());
            payment.setRoutingNumber(bilBankEntry.getRoutingTransitNbr().trim());
            payment.setAccountType(getCodeDescription(bilBankEntry.getAccountTypCd(),
                    BusCodeTranslationType.EFT_ACCOUNT_TYPE.getValue()).trim());

        } else if (StringUtils.isNotBlank(String.valueOf(bilCashEntryRow.getBilCrcrdTypeCd()))) {
            payment.setWalletType(getCodeDescription(String.valueOf(bilCashEntryRow.getBilCrcrdTypeCd()),
                    BusCodeTranslationType.DIGITAL_WALLET_TYPE.getValue()));
            payment.setCreditCardNumber(BLANK_STRING);
            payment.setWalletNumber(bilCashEntryRow.getBilCrcrdActNbr().trim());
        }
        if (Boolean.TRUE.equals(Boolean.valueOf(multiCurrency))) {
            TransRateHst transRateHst = transRateHstRepository
                    .findById(new TransRateHstId(bilCashEntryRow.getBilCashEntryId().getBilEntryDt(),
                            payment.getBatchId(), bilCashEntryRow.getBilCashEntryId().getUserId(),
                            bilCashEntryRow.getBilCashEntryId().getBilEntrySeqNbr()))
                    .orElse(null);
            if (transRateHst != null) {
                payment.setReceivedCurrency(transRateHst.getReceivedCurrency().trim());
                payment.setReceivedAmount(transRateHst.getReceivedAmount());
                payment.setRate(transRateHst.getRate());
            }
        }
        return payment;
    }

    private Payment getPaymentRows(BilCashEntry bilCashEntryRow) {

        Payment payment = new Payment();
        ZonedDateTime entryDate = bilCashEntryRow.getBilCashEntryId().getBilEntryDt();
        String userId = bilCashEntryRow.getBilCashEntryId().getUserId();
        String batchId = bilCashEntryRow.getBilCashEntryId().getBilEntryNbr();

        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(entryDate, batchId, userId)).orElse(null);
        if (bilCashEntryTot != null) {
            payment.setCurrency(multiCurrencyService.getCurrencyCode(bilCashEntryTot.getCurrencyCd()));
        }
        payment.setPaymentSequenceNumber(bilCashEntryRow.getBilCashEntryId().getBilEntrySeqNbr());
        payment.setPaymentAmount(bilCashEntryRow.getBilRctAmt());
        payment.setPostedDate(bilCashEntryRow.getBilCashEntryId().getBilEntryDt());
        payment.setBatchId(bilCashEntryRow.getBilCashEntryId().getBilEntryNbr());
        payment.setUserId(appSecurityServiceImpl
                .getUserIdByUserSeqId(bilCashEntryRow.getBilCashEntryId().getUserId().trim()).trim());
        payment.setUserName(appSecurityServiceImpl
                .getUserNameByUserSeqId(bilCashEntryRow.getBilCashEntryId().getUserId().trim()).trim());

        payment.setPaymentId(bilCashEntryRow.getBilRctId());
        if (!bilCashEntryRow.getBilPmtOrderId().trim().isEmpty()) {
            payment.setPaymentId(bilCashEntryRow.getBilPmtOrderId().trim());
        }

        payment.setPaymentDate(bilCashEntryRow.getBilRctReceiveDt());
        getPayorInformation(bilCashEntryRow, payment, false);
        if (Boolean.TRUE.equals(Boolean.valueOf(multiCurrency))) {
            TransRateHst transRateHst = transRateHstRepository
                    .findById(new TransRateHstId(bilCashEntryRow.getBilCashEntryId().getBilEntryDt(),
                            payment.getBatchId(), bilCashEntryRow.getBilCashEntryId().getUserId(),
                            bilCashEntryRow.getBilCashEntryId().getBilEntrySeqNbr()))
                    .orElse(null);
            if (transRateHst != null) {
                payment.setReceivedCurrency(transRateHst.getReceivedCurrency().trim());
                payment.setReceivedAmount(transRateHst.getReceivedAmount());
                payment.setRate(transRateHst.getRate());
            }
        }

        return payment;
    }

    private char getPayorInformation(BilCashEntry bilCashEntryRow, Payment payment, Boolean detailSw) {
        ZonedDateTime entryDate = bilCashEntryRow.getBilCashEntryId().getBilEntryDt();
        String userId = bilCashEntryRow.getBilCashEntryId().getUserId();
        String batchId = bilCashEntryRow.getBilCashEntryId().getBilEntryNbr();
        short sequenceNbr = bilCashEntryRow.getBilCashEntryId().getBilEntrySeqNbr();

        if (!bilCashEntryRow.getBilAccountNbr().trim().equals(BLANK_STRING)) {
            payment.setIdentifier(bilCashEntryRow.getBilAccountNbr().trim());
            BilCashReceipt bilCashReceipt = getBilCashReceiptRow(entryDate, batchId, userId, sequenceNbr);
            if (bilCashReceipt != null) {
                return getCashReceiptData(detailSw, bilCashReceipt, payment);
            }
            BilTtyCashRct bilTtyCashRct = getTtyReceiptRow(entryDate, batchId, userId, sequenceNbr);
            if (bilTtyCashRct != null) {
                return getGroupCashReceiptData(detailSw, bilTtyCashRct, payment);
            }
            BilAgtCashRct bilAgtCashRct = getAgentReceiptRow(entryDate, batchId, userId, sequenceNbr);
            if (bilAgtCashRct != null) {
                return getAgentCashReciptData(detailSw, bilAgtCashRct, payment);
            }
        } else if (!bilCashEntryRow.getBilAgtActNbr().trim().equals(BLANK_STRING)) {
            payment.setIdentifier(bilCashEntryRow.getBilAgtActNbr().trim());
            BilAgtCashRct bilAgtCashRct = getAgentReceiptRow(entryDate, batchId, userId, sequenceNbr);
            if (bilAgtCashRct != null) {
                return getAgentCashReciptData(detailSw, bilAgtCashRct, payment);
            }
            BilCashReceipt bilCashReceipt = getBilCashReceiptRow(entryDate, batchId, userId, sequenceNbr);
            if (bilCashReceipt != null) {
                return getCashReceiptData(detailSw, bilCashReceipt, payment);
            }
            BilTtyCashRct bilTtyCashRct = getTtyReceiptRow(entryDate, batchId, userId, sequenceNbr);
            if (bilTtyCashRct != null) {
                return getGroupCashReceiptData(detailSw, bilTtyCashRct, payment);
            }
        } else if (!bilCashEntryRow.getBilTtyNbr().trim().equals(BLANK_STRING)) {
            payment.setIdentifier(bilCashEntryRow.getBilTtyNbr().trim());
            BilTtyCashRct bilTtyCashRct = getTtyReceiptRow(entryDate, batchId, userId, sequenceNbr);
            if (bilTtyCashRct != null) {
                return getGroupCashReceiptData(detailSw, bilTtyCashRct, payment);
            }
            BilAgtCashRct bilAgtCashRct = getAgentReceiptRow(entryDate, batchId, userId, sequenceNbr);
            if (bilAgtCashRct != null) {
                return getAgentCashReciptData(detailSw, bilAgtCashRct, payment);
            }
            BilCashReceipt bilCashReceipt = getBilCashReceiptRow(entryDate, batchId, userId, sequenceNbr);
            if (bilCashReceipt != null) {
                return getCashReceiptData(detailSw, bilCashReceipt, payment);
            }
        }

        if (!bilCashEntryRow.getPolNbr().trim().equals(BLANK_STRING)) {
            return getAccountDetails(entryDate, userId, batchId, sequenceNbr, payment, detailSw);
        }

        return BLANK_CHAR;
    }

    private char getGroupCashReceiptData(Boolean detailSw, BilTtyCashRct bilTtyCashRct, Payment payment) {
        payment.setIdentifierType(getIdentifierTypeDescription(AccountType.GROUP_ACCOUNT.toString()));
        payment.setType(AccountType.GROUP.toString());
        payment.setPaymentMethod(getPaymentMethodDescription(String.valueOf(bilTtyCashRct.getBilCshEtrMthCd())));
        payment.setStatus(getPaymentDescription(bilTtyCashRct.getBilTtyCashRctId().getBilThirdPartyId(),
                bilTtyCashRct.getBilTtyCashRctId().getBilDtbDt(), bilTtyCashRct.getBilTtyCashRctId().getBilDtbSeqNbr(),
                bilTtyCashRct.getBilCshEtrMthCd()));
        if (Boolean.TRUE.equals(detailSw)) {
            calculateGroupAccountAmounts(bilTtyCashRct, payment);
        }
        payment.setPaymentType(
                getPaymentTypeDescription(bilTtyCashRct.getBilRctTypeCd(), bilTtyCashRct.getBilCshEtrMthCd()));
        BilThirdParty bilThirdParty = bilThirdPartyRepository
                .findById(bilTtyCashRct.getBilTtyCashRctId().getBilThirdPartyId()).orElse(null);
        if (bilThirdParty != null) {
            payment.setAccountId(bilThirdParty.getBillThirdPartyId());
            payment.setIdentifier(bilThirdParty.getBilTtyNbr().trim());
            payment.setPayorId(bilThirdParty.getBtpTtyClientId());
            payment.setPayorName(getPayorName(bilThirdParty.getBtpTtyClientId()));
            payment.setIsUnidentified(false);
        }

        if (isDigitalWalletPayment(bilTtyCashRct.getBilCshEtrMthCd())) {
            getWallentIdentifier(bilTtyCashRct.getBilTtyCashRctId().getBilThirdPartyId(), CHAR_T,
                    bilTtyCashRct.getBilTtyCashRctId().getBilDtbDt(),
                    bilTtyCashRct.getBilTtyCashRctId().getBilDtbSeqNbr(), payment);
        }

        return bilTtyCashRct.getBilCshEtrMthCd();
    }

    private char getAgentCashReciptData(Boolean detailSw, BilAgtCashRct bilAgtCashRct, Payment payment) {
        payment.setIdentifierType(getIdentifierTypeDescription(AccountType.AGENCY_ACCOUNT.toString()));
        payment.setType(AccountType.AGENCY.toString());
        payment.setPaymentMethod(getPaymentMethodDescription(String.valueOf(bilAgtCashRct.getBilCshEtrMthCd())));
        payment.setStatus(getDescription(bilAgtCashRct.getBilAgtCashRctId().getBilAgtActId(),
                bilAgtCashRct.getBilAgtCashRctId().getBilDtbDt(), bilAgtCashRct.getBilAgtCashRctId().getBilDtbSeqNbr(),
                bilAgtCashRct.getBilCshEtrMthCd(), AccountType.AGENCY_ACCOUNT.toString()));
        if (Boolean.TRUE.equals(detailSw)) {
            calculateAgentAccountAmounts(bilAgtCashRct, payment);
        }
        payment.setPaymentType(
                getPaymentTypeDescription(bilAgtCashRct.getBilRctTypeCd(), bilAgtCashRct.getBilCshEtrMthCd()));
        BilAgent bilAgent = bilAgentRepository.findById(bilAgtCashRct.getBilAgtCashRctId().getBilAgtActId())
                .orElse(null);
        if (bilAgent != null) {
            payment.setAccountId(bilAgent.getBilAgtActId());
            payment.setIdentifier(bilAgent.getBilAgtActNbr().trim());
            payment.setPayorId(bilAgent.getBagAgtPayCltId());
            payment.setPayorName(getPayorName(bilAgent.getBagAgtPayCltId()));
            payment.setIsUnidentified(false);

        }
        return bilAgtCashRct.getBilCshEtrMthCd();
    }

    private char getCashReceiptData(Boolean detailSw, BilCashReceipt bilCashReceipt, Payment payment) {
        payment.setIdentifierType(getIdentifierTypeDescription(AccountType.BILLACCOUNT.toString()));
        payment.setType(AccountType.DIRECTBILL.toString());
        payment.setPaymentMethod(getPaymentMethodDescription(String.valueOf(bilCashReceipt.getCashEntryMethodCd())));
        payment.setStatus(getDescription(bilCashReceipt.getBilCashReceiptId().getAccountId(),
                bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), bilCashReceipt.getCashEntryMethodCd(),
                AccountType.BILLACCOUNT.toString()));
        if (Boolean.TRUE.equals(detailSw)) {
            calculateBillAccountAmounts(bilCashReceipt, payment);
        }

        payment.setPaymentType(
                getPaymentTypeDescription(bilCashReceipt.getReceiptTypeCd(), bilCashReceipt.getCashEntryMethodCd()));

        if (bilCashReceipt.getCashEntryMethodCd() == PaymentType.AGENCY_SWEEP.getValue().charAt(SHORT_ZERO)) {
            getpaymentIdForAgencySweep(bilCashReceipt, payment);
        }

        BilAccount bilAccount = bilAccountRepository.findById(bilCashReceipt.getBilCashReceiptId().getAccountId())
                .orElse(null);
        if (bilAccount != null) {
            payment.setAccountId(bilAccount.getAccountId());
            payment.setIdentifier(bilAccount.getAccountNumber().trim());
            payment.setPayorId(bilAccount.getPayorClientId());
            payment.setPayorName(getPayorName(bilAccount.getPayorClientId()));
            if (!bilAccount.getThirdPartyIdentifier().trim().isEmpty()) {
                payment.setType(AccountType.GROUPBILL.toString());
            } else if (!bilAccount.getAgentAccountNumber().trim().isEmpty()) {
                payment.setType(AccountType.AGENCYBILL.toString());
            }
            payment.setIsUnidentified(false);

        }
        if (isDigitalWalletPayment(bilCashReceipt.getCashEntryMethodCd())) {
            getWallentIdentifier(bilCashReceipt.getBilCashReceiptId().getAccountId(), 'A',
                    bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                    bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), payment);
        }
        return bilCashReceipt.getCashEntryMethodCd();

    }

    private void getWallentIdentifier(String accountId, char techKeyTypCd, ZonedDateTime bilDtbDate,
            short dtbSequenceNbr, Payment payment) {
        List<BilEftPendingTape> bilEftPendingTapeList = bilEftPendingTapeRepository.fetchDigitalPaymentRows(accountId,
                techKeyTypCd, bilDtbDate, dtbSequenceNbr, payment.getPaymentId());
        if (!bilEftPendingTapeList.isEmpty()) {
            BilEftExtPyt bilEftExtPyt = bilEftExtPytRepository
                    .findById(new BilEftExtPytId(
                            bilEftPendingTapeList.get(SHORT_ZERO).getBilEftPendingTapeId().getTechnicalKey(),
                            bilEftPendingTapeList.get(SHORT_ZERO).getBilEftPendingTapeId().getTechnicalKeyType(),
                            bilEftPendingTapeList.get(SHORT_ZERO).getBilEftPendingTapeId().getEftTapeDate(),
                            bilEftPendingTapeList.get(SHORT_ZERO).getBilEftPendingTapeId().getEftActivityDate(),
                            bilEftPendingTapeList.get(SHORT_ZERO).getBilEftPendingTapeId().getSequenceNbr()))
                    .orElse(null);
            payment.setWalletIdentifier(bilEftExtPyt != null ? bilEftExtPyt.getBilWalIdentifier() : BLANK_STRING);
        } else {
            BilEftExtPyt bilEftExtPyt = bilEftExtPytRepository
                    .findById(new BilEftExtPytId(accountId, techKeyTypCd, bilDtbDate, bilDtbDate, dtbSequenceNbr))
                    .orElse(null);
            payment.setWalletIdentifier(bilEftExtPyt != null ? bilEftExtPyt.getBilWalIdentifier() : BLANK_STRING);
        }

    }

    private char getAccountDetails(ZonedDateTime entryDate, String userId, String batchId, short sequenceNbr,
            Payment payment, Boolean detailSw) {
        BilCashReceipt bilCashReceipt = getBilCashReceiptRow(entryDate, batchId, userId, sequenceNbr);
        if (bilCashReceipt != null) {
            return getCashReceiptData(detailSw, bilCashReceipt, payment);

        } else {
            BilTtyCashRct bilTtyCashRct = getTtyReceiptRow(entryDate, batchId, userId, sequenceNbr);
            if (bilTtyCashRct != null) {
                return getGroupCashReceiptData(detailSw, bilTtyCashRct, payment);
            } else {
                BilAgtCashRct bilAgtCashRct = getAgentReceiptRow(entryDate, batchId, userId, sequenceNbr);

                if (bilAgtCashRct != null) {
                    payment.setPaymentMethod(
                            getPaymentMethodDescription(String.valueOf(bilAgtCashRct.getBilCshEtrMthCd())));
                    payment.setPaymentType(getPaymentTypeDescription(bilAgtCashRct.getBilRctTypeCd(),
                            bilAgtCashRct.getBilCshEtrMthCd()));
                    if (Boolean.TRUE.equals(detailSw)) {
                        calculateAgentAccountAmounts(bilAgtCashRct, payment);
                    }
                    if (isDigitalWalletPayment(bilAgtCashRct.getBilCshEtrMthCd())) {
                        getWallentIdentifier(bilAgtCashRct.getBilAgtCashRctId().getBilAgtActId(), CHAR_G,
                                bilAgtCashRct.getBilAgtCashRctId().getBilDtbDt(),
                                bilAgtCashRct.getBilAgtCashRctId().getBilDtbSeqNbr(), payment);
                    }
                    BilAgent bilAgent = bilAgentRepository.findById(bilAgtCashRct.getBilAgtCashRctId().getBilAgtActId())
                            .orElse(null);
                    if (bilAgent != null) {
                        payment.setAccountId(bilAgent.getBilAgtActId());
                        payment.setIdentifier(bilAgent.getBilAgtActNbr().trim());
                        payment.setPayorId(bilAgent.getBagAgtPayCltId());
                        payment.setPayorName(getPayorName(bilAgent.getBagAgtPayCltId()));
                        payment.setIdentifier(bilAgent.getBilAgtActNbr().trim());
                        payment.setIdentifierType(getIdentifierTypeDescription(AccountType.AGENCY_ACCOUNT.toString()));
                        payment.setType(AccountType.AGENCY.toString());
                        payment.setIsUnidentified(false);
                        return bilAgtCashRct.getBilCshEtrMthCd();
                    }

                }
            }

        }

        return BLANK_CHAR;
    }

    private void getpaymentIdForAgencySweep(BilCashReceipt bilCashReceipt, Payment payment) {
        List<BilEftActivity> bilEftActivityList = bilEftActivityRepository.getPaymentIdRow(
                bilCashReceipt.getBilCashReceiptId().getAccountId(), BilTechKeyTypeCode.BILL_ACCOUNT.getValue(),
                bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(),
                BillingEftTransactionStatus.APPLIED.getValue());
        if (bilEftActivityList != null && !bilEftActivityList.isEmpty()) {
            payment.setPaymentId(bilEftActivityList.get(SHORT_ZERO).getBilPmtOrderId());
        } else {
            payment.setPaymentId(BLANK_STRING);
        }
    }

    private BilAgtCashRct getAgentReceiptRow(ZonedDateTime entryDate, String batchId, String userId,
            short sequenceNbr) {
        List<BilAgtCashRct> bilAgtCashRctList = bilAgtCashRctRepository.getAgentCashReciptRow(entryDate, batchId,
                userId, sequenceNbr, SEPARATOR_BLANK, PageRequest.of(SHORT_ZERO, SHORT_ONE));
        if (bilAgtCashRctList != null && !bilAgtCashRctList.isEmpty()) {
            return bilAgtCashRctList.get(SHORT_ZERO);
        }
        return null;

    }

    private BilTtyCashRct getTtyReceiptRow(ZonedDateTime entryDate, String batchId, String userId, short sequenceNbr) {

        List<BilTtyCashRct> bilTtyCashRctList = bilTtyCashRctRepository.getTtyCashReciptRow(entryDate, batchId, userId,
                sequenceNbr, SEPARATOR_BLANK, PageRequest.of(SHORT_ZERO, SHORT_ONE));
        if (bilTtyCashRctList != null && !bilTtyCashRctList.isEmpty()) {
            return bilTtyCashRctList.get(SHORT_ZERO);
        }
        return null;
    }

    private BilCashReceipt getBilCashReceiptRow(ZonedDateTime entryDate, String batchId, String userId,
            short sequenceNbr) {
        List<BilCashReceipt> bilCashReceiptList = bilCashReceiptRepository.getBilCashReciptRow(entryDate, batchId,
                userId, sequenceNbr, SEPARATOR_BLANK, PageRequest.of(SHORT_ZERO, SHORT_ONE));
        if (bilCashReceiptList != null && !bilCashReceiptList.isEmpty()) {
            return bilCashReceiptList.get(SHORT_ZERO);
        }
        return null;

    }

    private String getPayorName(String clientId) {

        try {
            Party partySearch = partyElasticSearchServiceImpl.fetchPartyDocument(clientId.trim());
            if (partySearch != null) {
                return partySearch.getName();
            }
        } catch (IOException e) {
            logger.error("Error in fetching the Party Document");
        }
        return BLANK_STRING;
    }

    private List<Character> getCashEntryMethodList() {
        List<Character> bilCashEntryMethod = new ArrayList<>();
        bilCashEntryMethod.add(CashEntryMethod.EFT.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.CREDIT.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.TOLERANCE_WRITEOFF.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.EFT_CREDIT_CARD.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.LOCKBOX.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.ONLINE_ENTRY.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.CREDIT_CARD.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.OCR.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.STATEMENT.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.TAPE_FROM_CASH_RECONCILIATION.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.AGENT_ACH.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.INSURED.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.ONE_TIME_ACH.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.VENDOR_COLLECTIONS.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.AGENT_CREDIT_CARD.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod());
        bilCashEntryMethod.add(CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod());
        List<Character> entryMethodList = getDigitalEntryMethods();
        if (!entryMethodList.isEmpty()) {
            bilCashEntryMethod.addAll(entryMethodList);
        }
        return bilCashEntryMethod;
    }

    private List<Character> getDigitalEntryMethods() {
        List<Character> entryMethodList = new ArrayList<>();
        List<String> budCodeList = busCdTranslationRepository.findCodeListByTypeList(
                Arrays.asList(BusCodeTranslationType.DIGITAL_WALLET_TYPE.getValue()), execContext.getLanguage());
        if (!budCodeList.isEmpty()) {
            budCodeList.stream().forEach(busCode -> entryMethodList.add(busCode.charAt(SHORT_ZERO)));
        }
        return entryMethodList;
    }

    private String getPaymentMethodDescription(String cashEntryMethodCode) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(cashEntryMethodCode,
                BusCodeTranslationType.PAYMENTMETHOD.getValue(), execContext.getLanguage());
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusDescription().trim();
        }
        return cashEntryMethodCode;
    }

    private String getPaymentTypeDescription(String receiptTypeCode, char paymentMethod) {
        BusCdTranslation busCodeTranslation = null;
        if (paymentMethod == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(receiptTypeCode,
                    BusCodeTranslationType.CREDIT_CARD_PAYMENT.getValue(), execContext.getLanguage());
        } else if (paymentMethod == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(receiptTypeCode,
                    BusCodeTranslationType.BANK_ACCOUNT_PAYMENT.getValue(), execContext.getLanguage());
        } else if (isDigitalWalletPayment(paymentMethod)) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(receiptTypeCode,
                    BilDesReasonType.WALLET_PAYMENT_TYPE.getValue(), execContext.getLanguage());
        } else {
            List<BusCdTranslation> busCodeTranslationList = busCdTranslationRepository
                    .findAllByCodeAndTypeListAndParentCode(receiptTypeCode,
                            Arrays.asList(BusCodeTranslationType.PAYMENT_TYPE_CODE.getValue(),
                                    BusCodeTranslationType.CASH_PAYMENT.getValue(),
                                    BusCodeTranslationType.COLLECT_AGENCY_PAYMENT.getValue()),
                            BLANK_STRING, execContext.getLanguage());
            if (busCodeTranslationList != null && !busCodeTranslationList.isEmpty()) {
                busCodeTranslation = busCodeTranslationList.get(SHORT_ZERO);
            }
        }
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusDescription().trim();
        }
        return receiptTypeCode;
    }

    private String getCodeDescription(String busCode, String busTypeCode) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(busCode, busTypeCode,
                execContext.getLanguage());
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusDescription().trim();
        }
        return busCode;
    }

    private String getDescription(String accountId, ZonedDateTime distributionDate, short distributionSequenceNumber,
            char cashEntryMethodCode, String accountType) {
        Integer count = null;

        BusCdTranslation busCodeTranslation = null;
        if (accountType.equals(AccountType.BILLACCOUNT.toString())) {
            count = bilCashDspRepository.findCountForPaymentDescription(accountId, distributionDate,
                    distributionSequenceNumber, BilDspTypeCode.REVERSED.getValue());
        } else {
            count = bilAgtCashDspRepository.findCountForPaymentDescription(accountId, distributionDate,
                    distributionSequenceNumber, BilDspTypeCode.REVERSED.getValue());
        }

        if (count != null && count > SHORT_ZERO) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(BilDspTypeCode.REVERSED.getValue(),
                    BilDesReasonType.DSP.getValue(), execContext.getLanguage());
            if (busCodeTranslation != null) {
                return busCodeTranslation.getBusDescription().trim();
            } else {
                throw new InvalidDataException(PAYMENT_DESCRPTION_INVALID, new Object[] { "RV", "DSP" });
            }

        } else {
            String description = chkPndAchPaymnt(accountId, distributionDate, distributionSequenceNumber,
                    cashEntryMethodCode, accountType);
            if (description.isEmpty()) {
                count = bilCashDspRepository.findCountForPaymentDescriptions(accountId, distributionDate,
                        distributionSequenceNumber, BilDspTypeCode.SUSPENDED.getValue(),
                        ReverseReSuspendIndicator.BLANK.getValue(), CHAR_Y, getCreditIndicatorList());
                if (count != null && count > SHORT_ZERO) {
                    busCodeTranslation = busCdTranslationRepository.findByCodeAndType(BilDesReasonCode.DES.getValue(),
                            BilDesReasonType.RCT.getValue(), execContext.getLanguage());
                    if (busCodeTranslation != null) {
                        return busCodeTranslation.getBusDescription().trim();
                    } else {

                        throw new InvalidDataException("no.record.exists.in.bus.cd.translation",
                                new Object[] { BilDesReasonCode.DES.getValue(), BilDesReasonType.RCT.getValue() });
                    }
                }
            }
        }

        return "Payment Received";
    }

    private String chkPndAchPaymnt(String accountId, ZonedDateTime distributionDate, short distributionSequenceNumber,
            char cashEntryMethodCode, String accountType) {

        boolean isRowOf1XCC = false;
        boolean pendingAchExists = false;

        List<Character> bilEftRecordTypeList = new ArrayList<>();

        if (accountType.equals(AccountType.AGENCY_ACCOUNT.toString())) {
            bilEftRecordTypeList.add(EftRecordType.ONE_TIME_ACH.getValue());

            return BLANK_STRING;
        } else if (accountType.equals(AccountType.BILLACCOUNT.toString())) {

            bilEftRecordTypeList.add(EftRecordType.RECONCILED_CASH.getValue());
            bilEftRecordTypeList.add(EftRecordType.UNIDENTIFIED.getValue());
            bilEftRecordTypeList.add(EftRecordType.ONE_TIME_ACH.getValue());
            bilEftRecordTypeList.add(EftRecordType.AGENCY_SWEEP.getValue());
            bilEftRecordTypeList.add(EftRecordType.DIGITAL_WALLET.getValue());

            if (cashEntryMethodCode == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()
                    || isDigitalWalletPayment(cashEntryMethodCode)) {
                isRowOf1XCC = checkIfEFTRow(accountId);
            }

            if (cashEntryMethodCode == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()
                    || cashEntryMethodCode == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod() || isRowOf1XCC) {
                ZonedDateTime insertedTimeStamp = bilEftPendingTapeRepository.getTimeStamp(accountId, distributionDate,
                        distributionSequenceNumber, bilEftRecordTypeList, TechnicalKeyTypeCode.THIRD_PARTY.getValue());
                if (insertedTimeStamp != null) {
                    pendingAchExists = true;
                }
            }
        }

        if (pendingAchExists) {
            BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(
                    BilDesReasonCode.PAC.getValue(), BilDesReasonType.RCT.getValue(), execContext.getLanguage());
            if (busCodeTranslation != null) {
                return busCodeTranslation.getBusDescription().trim();
            } else {
                throw new InvalidDataException(PAYMENT_DESCRPTION_INVALID,
                        new Object[] { BilDesReasonCode.PAC.getValue(), BilDesReasonType.RCT.getValue() });
            }
        }

        return BLANK_STRING;

    }

    private List<Character> getCreditIndicatorList() {

        List<Character> creditIndicatorList = new ArrayList<>();
        creditIndicatorList.add(CHAR_B);
        creditIndicatorList.add(CHAR_D);
        return creditIndicatorList;
    }

    private String getPaymentDescription(String accountId, ZonedDateTime distributionDate,
            short distributionSequenceNumber, char cashEntryMethodCode) {

        Integer count = null;
        List<Character> bilEftRecordTypeList = new ArrayList<>();
        bilEftRecordTypeList.add(EftRecordType.RECONCILED_CASH.getValue());
        bilEftRecordTypeList.add(EftRecordType.UNIDENTIFIED.getValue());
        bilEftRecordTypeList.add(EftRecordType.ONE_TIME_ACH.getValue());
        bilEftRecordTypeList.add(EftRecordType.AGENCY_SWEEP.getValue());
        bilEftRecordTypeList.add(EftRecordType.DIGITAL_WALLET.getValue());

        count = bilTtyCashDspRepository.findCountByBilDspTypeCode(accountId, distributionDate,
                distributionSequenceNumber, BilDspTypeCode.REVERSED.getValue());
        BusCdTranslation busCodeTranslation = null;
        Integer pendingRowCount = bilScheduleCashRepository.getPendingReversalRow(accountId,
                CashProcessingCode.GROUP_REVERSAL.getValue(), distributionDate, distributionSequenceNumber,
                ProcessIndicator.NOT_PROCESSED.getValue());
        if (count != null && count > SHORT_ZERO) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(BilDspTypeCode.REVERSED.getValue(),
                    BilDesReasonType.DSP.getValue(), execContext.getLanguage());
            if (busCodeTranslation != null) {
                return busCodeTranslation.getBusDescription().trim();
            } else {
                throw new InvalidDataException(PAYMENT_DESCRPTION_INVALID,
                        new Object[] { BilDspTypeCode.REVERSED.getValue(), BilDesReasonType.DSP.getValue() });
            }
        } else if (pendingRowCount != null && pendingRowCount > 0) {
            return PENDING_REVERSAL;
        } else {
            boolean isRowOf1XCC = false;
            count = bilTtyCashDspRepository.findCountByTypeCodeAndReasonCode(accountId, distributionDate,
                    distributionSequenceNumber, BilDspTypeCode.SUSPENDED.getValue(), SEPARATOR_BLANK,
                    ReverseReSuspendIndicator.BLANK.getValue(), ManualSuspendIndicator.ENABLED.getValue());
            if (cashEntryMethodCode == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()
                    || isDigitalWalletPayment(cashEntryMethodCode)) {
                isRowOf1XCC = checkIfEFTRow(accountId);
            }
            boolean pendingAchExists = false;
            if (cashEntryMethodCode == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()
                    || cashEntryMethodCode == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod() || isRowOf1XCC) {
                ZonedDateTime insertedTimeStamp = bilEftPendingTapeRepository.getTimeStamp(accountId, distributionDate,
                        distributionSequenceNumber, bilEftRecordTypeList, TechnicalKeyTypeCode.THIRD_PARTY.getValue());
                if (insertedTimeStamp != null) {
                    pendingAchExists = true;
                }
            }
            if (pendingAchExists) {
                busCodeTranslation = busCdTranslationRepository.findByCodeAndType(BilDesReasonCode.PAC.getValue(),
                        BilDesReasonType.RCT.getValue(), execContext.getLanguage());
                if (busCodeTranslation != null) {
                    return busCodeTranslation.getBusDescription().trim();
                } else {
                    throw new InvalidDataException(PAYMENT_DESCRPTION_INVALID,
                            new Object[] { BilDesReasonCode.PAC.getValue(), BilDesReasonType.RCT.getValue() });
                }
            }
            if (count != null && count > SHORT_ZERO) {
                busCodeTranslation = busCdTranslationRepository.findByCodeAndType(BilDesReasonCode.DES.getValue(),
                        BilDesReasonType.RCT.getValue(), execContext.getLanguage());
                if (busCodeTranslation != null) {
                    return busCodeTranslation.getBusDescription().trim();
                } else {

                    throw new InvalidDataException("no.record.exists.in.bus.cd.translation",
                            new Object[] { BilDesReasonCode.DES.getValue(), BilDesReasonType.RCT.getValue() });
                }
            }
        }

        return "Payment Received";
    }

    private boolean checkIfEFTRow(String accountId) {
        List<Character> bilEftRecordTypeList = new ArrayList<>();
        bilEftRecordTypeList.add(EftRecordType.RECONCILED_CASH.getValue());
        bilEftRecordTypeList.add(EftRecordType.UNIDENTIFIED.getValue());
        bilEftRecordTypeList.add(EftRecordType.DIGITAL_WALLET.getValue());
        Integer rowCount = bilEftActivityRepository.checkIfExists(accountId, bilEftRecordTypeList);
        if (rowCount != null && rowCount > SHORT_ZERO) {
            return true;
        } else {
            rowCount = bilEftPendingTapeRepository.checkIfExists(accountId, bilEftRecordTypeList);
            if (rowCount != null && rowCount > SHORT_ZERO) {
                return true;
            }
        }
        return false;
    }

    private Double calculateAmount(String accountId, ZonedDateTime bilDtbDate, short dtbSequenceNbr, String dspTypeCode,
            String type) {
        Double amount = null;
        if (type.equals(AccountType.BILLACCOUNT.toString())) {
            amount = bilCashDspRepository.findSuspenseAmount(accountId, bilDtbDate, dtbSequenceNbr, dspTypeCode,
                    BLANK_CHAR);
        } else if (type.equals(AccountType.GROUP_ACCOUNT.toString())) {
            amount = bilTtyCashDspRepository.findPaymentSuspenseAmount(accountId, dtbSequenceNbr, bilDtbDate,
                    dspTypeCode, BLANK_CHAR);
        } else if (type.equals(AccountType.AGENCY_ACCOUNT.toString())) {
            amount = bilAgtCashDspRepository.findDspAmountByDspTypeCodeRevsRsusInd(accountId, bilDtbDate,
                    dtbSequenceNbr, dspTypeCode, BLANK_CHAR);
        }

        if (amount == null) {
            return DECIMAL_ZERO;
        }
        return amount;

    }

    private void calculateGroupAccountAmounts(BilTtyCashRct bilTtyCashRct, Payment payment) {
        payment.setAppliedAmount(calculateAmount(bilTtyCashRct.getBilTtyCashRctId().getBilThirdPartyId(),
                bilTtyCashRct.getBilTtyCashRctId().getBilDtbDt(), bilTtyCashRct.getBilTtyCashRctId().getBilDtbSeqNbr(),
                BilDspTypeCode.APPLIED.getValue(), AccountType.GROUP_ACCOUNT.toString()));
        payment.setDisbursedAmount(calculateAmount(bilTtyCashRct.getBilTtyCashRctId().getBilThirdPartyId(),
                bilTtyCashRct.getBilTtyCashRctId().getBilDtbDt(), bilTtyCashRct.getBilTtyCashRctId().getBilDtbSeqNbr(),
                BilDspTypeCode.DISBURSED.getValue(), AccountType.GROUP_ACCOUNT.toString()));
        payment.setWriteOffAmount(calculateWriteOffAmount(bilTtyCashRct.getBilTtyCashRctId().getBilThirdPartyId(),
                bilTtyCashRct.getBilTtyCashRctId().getBilDtbDt(), bilTtyCashRct.getBilTtyCashRctId().getBilDtbSeqNbr(),
                AccountType.GROUP_ACCOUNT.toString()));
        payment.setSuspenseAmount(calculateAmount(bilTtyCashRct.getBilTtyCashRctId().getBilThirdPartyId(),
                bilTtyCashRct.getBilTtyCashRctId().getBilDtbDt(), bilTtyCashRct.getBilTtyCashRctId().getBilDtbSeqNbr(),
                BilDspTypeCode.SUSPENDED.getValue(), AccountType.GROUP_ACCOUNT.toString()));

    }

    private void calculateAgentAccountAmounts(BilAgtCashRct bilAgtCashRct, Payment payment) {
        payment.setAppliedAmount(calculateAmount(bilAgtCashRct.getBilAgtCashRctId().getBilAgtActId(),
                bilAgtCashRct.getBilAgtCashRctId().getBilDtbDt(), bilAgtCashRct.getBilAgtCashRctId().getBilDtbSeqNbr(),
                BilDspTypeCode.APPLIED.getValue(), AccountType.AGENCY_ACCOUNT.toString()));
        payment.setDisbursedAmount(calculateAmount(bilAgtCashRct.getBilAgtCashRctId().getBilAgtActId(),
                bilAgtCashRct.getBilAgtCashRctId().getBilDtbDt(), bilAgtCashRct.getBilAgtCashRctId().getBilDtbSeqNbr(),
                BilDspTypeCode.DISBURSED.getValue(), AccountType.AGENCY_ACCOUNT.toString()));
        payment.setWriteOffAmount(calculateWriteOffAmount(bilAgtCashRct.getBilAgtCashRctId().getBilAgtActId(),
                bilAgtCashRct.getBilAgtCashRctId().getBilDtbDt(), bilAgtCashRct.getBilAgtCashRctId().getBilDtbSeqNbr(),
                AccountType.AGENCY_ACCOUNT.toString()));
        payment.setSuspenseAmount(calculateAmount(bilAgtCashRct.getBilAgtCashRctId().getBilAgtActId(),
                bilAgtCashRct.getBilAgtCashRctId().getBilDtbDt(), bilAgtCashRct.getBilAgtCashRctId().getBilDtbSeqNbr(),
                BilDspTypeCode.SUSPENDED.getValue(), AccountType.AGENCY_ACCOUNT.toString()));

    }

    private void calculateBillAccountAmounts(BilCashReceipt bilCashReceipt, Payment payment) {

        payment.setAppliedAmount(calculateAmount(bilCashReceipt.getBilCashReceiptId().getAccountId(),
                bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), BilDspTypeCode.APPLIED.getValue(),
                AccountType.BILLACCOUNT.toString()));
        payment.setDisbursedAmount(calculateAmount(bilCashReceipt.getBilCashReceiptId().getAccountId(),
                bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), BilDspTypeCode.DISBURSED.getValue(),
                AccountType.BILLACCOUNT.toString()));
        payment.setWriteOffAmount(calculateWriteOffAmount(bilCashReceipt.getBilCashReceiptId().getAccountId(),
                bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), AccountType.BILLACCOUNT.toString()));
        payment.setSuspenseAmount(calculateAmount(bilCashReceipt.getBilCashReceiptId().getAccountId(),
                bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), BilDspTypeCode.SUSPENDED.getValue(),
                AccountType.BILLACCOUNT.toString()));

    }

    private Double calculateWriteOffAmount(String accountId, ZonedDateTime bilDtbDate, short dtbSequenceNbr,
            String type) {
        Double amount = null;
        List<String> dsyTypeCodeList = new ArrayList<>();
        dsyTypeCodeList.add(BilDspTypeCode.WRITE_OFF.getValue());
        dsyTypeCodeList.add(BilDspTypeCode.WRITE_OFF_FOR_RECONCILIATION.getValue());

        if (type.equals(AccountType.BILLACCOUNT.toString())) {
            amount = bilCashDspRepository.findWriteOffAmount(accountId, bilDtbDate, dtbSequenceNbr, dsyTypeCodeList,
                    ReverseReSuspendIndicator.BLANK.getValue());
        } else if (type.equals(AccountType.GROUP_ACCOUNT.toString())) {
            amount = bilTtyCashDspRepository.findPaymentWriteOffAmount(accountId, dtbSequenceNbr, bilDtbDate,
                    dsyTypeCodeList, ReverseReSuspendIndicator.BLANK.getValue());
        } else if (type.equals(AccountType.AGENCY_ACCOUNT.toString())) {
            amount = bilAgtCashDspRepository.findPaymentWriteOffAmount(accountId, bilDtbDate, dtbSequenceNbr,
                    dsyTypeCodeList, ReverseReSuspendIndicator.BLANK.getValue());
        }

        if (null == amount) {
            return DECIMAL_ZERO;
        }
        return amount;

    }

    private String getIdentifierTypeDescription(String accountType) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(accountType,
                BusCodeTranslationType.BILLACCOUNTTYPE.getValue(), execContext.getLanguage());
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusDescription();
        }
        return accountType;
    }

    @Override
    public String getExternalReferenceLink(String type, String accountId) {
        String externalUrl;
        if (type.trim().equals(AccountType.DIRECTBILL.toString())
                || type.trim().equals(AccountType.GROUPBILL.toString())
                || type.trim().equals(AccountType.AGENCYBILL.toString())) {
            externalUrl = externalBillingService + accountId + PAYMENTS_LIT;
        } else if (type.trim().equals(AccountType.GROUP_ACCOUNT.toString())) {
            externalUrl = externalGroupService + accountId + PAYMENTS_LIT;
        } else {
            externalUrl = externalAgencyService + accountId + PAYMENTS_LIT;
        }
        return externalUrl;
    }

    @Override
    public void insertAccountBalance(String bilaccountId, String bilAccountNumber, String bilTypeCode,
            char btpStatusCode, ZonedDateTime se3DateTime) {
        List<Character> revIndicatorList = new ArrayList<>();
        revIndicatorList.add(CHAR_N);
        revIndicatorList.add(BLANK_CHAR);
        List<Character> bilInvoiceCdList = new ArrayList<>();
        bilInvoiceCdList.add(CHAR_Y);
        bilInvoiceCdList.add(CHAR_L);
        boolean isInvoice = false;
        BilAccountBalance bilAccountBalance = new BilAccountBalance();
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow("BABL", SEPARATOR_BLANK, SEPARATOR_BLANK,
                SEPARATOR_BLANK, se3DateTime, se3DateTime);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            bilAccountBalance = getPremiumAmounts(bilaccountId, bilAccountBalance, isInvoice);
            isInvoice = true;
            bilAccountBalance = getPremiumAmounts(bilaccountId, bilAccountBalance, isInvoice);
            isInvoice = false;
            bilAccountBalance = getChargeAmounts(bilaccountId, bilAccountBalance, isInvoice);
            isInvoice = true;
            bilAccountBalance = getChargeAmounts(bilaccountId, bilAccountBalance, isInvoice);
            Double totSvcCrgAmt = bilCrgAmountsRepository.getServiceChargeAmountForAccountBalance(bilaccountId, CHAR_S,
                    bilInvoiceCdList);
            totSvcCrgAmt = defaultAmount(totSvcCrgAmt);
            bilAccountBalance.setTotSvcCrgAmt(totSvcCrgAmt);
            Double totLateCrgAmt = bilCrgAmountsRepository.getLateChargeAmountForAccountBalance(bilaccountId, CHAR_L);
            totLateCrgAmt = defaultAmount(totLateCrgAmt);
            bilAccountBalance.setTotLateCrgAmt(totLateCrgAmt);
            Double totPnyCrgAmt = bilCrgAmountsRepository.getLateChargeAmountForAccountBalance(bilaccountId, CHAR_P);
            totPnyCrgAmt = defaultAmount(totPnyCrgAmt);
            bilAccountBalance.setTotPnyCrgAmt(totPnyCrgAmt);
            Double totDpayCrgAmt = bilCrgAmountsRepository.getLateChargeAmountForAccountBalance(bilaccountId, CHAR_D);
            totDpayCrgAmt = defaultAmount(totDpayCrgAmt);
            bilAccountBalance.setTotDpayCrgAmt(totDpayCrgAmt);
            BigDecimal totCashSusAmt = bilCashDspRepository.getSusAmountForAccountBalance(bilaccountId,
                    BilDspTypeCode.SUSPENDED.getValue(), revIndicatorList);
            totCashSusAmt = defaultAmount(totCashSusAmt);
            bilAccountBalance.setTotCashSusAmt(totCashSusAmt.doubleValue());
            BigDecimal totCashDsbAmt = bilCashDspRepository.getAmountForAccountBalance(bilaccountId,
                    BilDspTypeCode.DISBURSED.getValue(), BLANK_CHAR);
            totCashDsbAmt = defaultAmount(totCashDsbAmt);
            bilAccountBalance.setTotCashDsbAmt(totCashDsbAmt.doubleValue());
            BigDecimal totCashWroAmt = bilCashDspRepository.getAmountForAccountBalance(bilaccountId,
                    BilDspTypeCode.WRITE_OFF.getValue(), BLANK_CHAR);
            totCashWroAmt = defaultAmount(totCashWroAmt);
            bilAccountBalance.setTotCashWroAmt(totCashWroAmt.doubleValue());
            BigDecimal totCashRevAmt = bilCashDspRepository.getSusAmountForAccountBalance(bilaccountId,
                    BilDesReasonCode.RV.getValue(), revIndicatorList);
            totCashRevAmt = defaultAmount(totCashRevAmt);
            bilAccountBalance.setTotCashRevAmt(totCashRevAmt.doubleValue());
            BigDecimal totCashTrfAmt = bilCashDspRepository.getAmountForAccountBalance(bilaccountId,
                    BilDspTypeCode.TRANSFERRED.getValue(), BLANK_CHAR);
            totCashTrfAmt = defaultAmount(totCashTrfAmt);
            bilAccountBalance.setTotCashTrfAmt(totCashTrfAmt.doubleValue());
            bilAccountBalance
                    .setBillActBalAmt(bilAccountBalance.getTotPrmBalAmt() + bilAccountBalance.getTotCrgBalAmt());
            bilAccountBalance
                    .setBillAccountBalanceId(new BilAccountBalanceId(bilaccountId, execContext.getApplicationDateTime()));
            bilAccountBalance.setBillAccountNumber(bilAccountNumber);
            bilAccountBalance.setBillTypeCode(bilTypeCode);
            bilAccountBalance.setBatStatusCode(btpStatusCode);
            bilAccountBalance.setUserId(execContext.getUserSeqeunceId());
            bilAccountBalanceRepository.saveAndFlush(bilAccountBalance);
        }

    }

    private BilAccountBalance getPremiumAmounts(String accountId, BilAccountBalance bilAccountBalance,
            boolean isInvoice) {
        List<Object[]> accountBalanceSum = null;
        List<Character> bilInvoiceCdList = new ArrayList<>();
        bilInvoiceCdList.add(CHAR_Y);
        bilInvoiceCdList.add(CHAR_L);
        if (!isInvoice) {
            accountBalanceSum = bilIstScheduleRepository.getAccountBalanceSum(accountId);
        } else {
            accountBalanceSum = bilIstScheduleRepository.getInvoicedAccountBalanceSum(accountId, bilInvoiceCdList);
        }
        if (accountBalanceSum != null && !accountBalanceSum.isEmpty()) {
            Double bilPrmIstAmt = (Double) accountBalanceSum.get(0)[0];
            Double bisCreFutAmt = (Double) accountBalanceSum.get(0)[1];
            Double bisCreOpnAmt = (Double) accountBalanceSum.get(0)[2];
            Double bisCncFutAmt = (Double) accountBalanceSum.get(0)[3];
            Double bisCncOpnAmt = (Double) accountBalanceSum.get(0)[4];
            Double bisPrmPaidAmt = (Double) accountBalanceSum.get(0)[5];
            Double bisWroPrmAmt = (Double) accountBalanceSum.get(0)[6];
            Double bisCrePaidAmt = (Double) accountBalanceSum.get(0)[7];
            bilPrmIstAmt = defaultAmount(bilPrmIstAmt);
            bisCreFutAmt = defaultAmount(bisCreFutAmt);
            bisCreOpnAmt = defaultAmount(bisCreOpnAmt);
            bisCncFutAmt = defaultAmount(bisCncFutAmt);
            bisCncOpnAmt = defaultAmount(bisCncOpnAmt);
            bisPrmPaidAmt = defaultAmount(bisPrmPaidAmt);
            bisWroPrmAmt = defaultAmount(bisWroPrmAmt);
            bisCrePaidAmt = defaultAmount(bisCrePaidAmt);
            if (!isInvoice) {
                bilAccountBalance
                        .setTotPrmAmt(bilPrmIstAmt + bisCreFutAmt + bisCreOpnAmt + bisCncFutAmt + bisCncOpnAmt);
                bilAccountBalance.setTotPrmPaidAmt(bisPrmPaidAmt + bisCrePaidAmt);
                bilAccountBalance.setTotPrmWroAmt(bisWroPrmAmt);
                bilAccountBalance.setTotPrmBalAmt(bilAccountBalance.getTotPrmAmt()
                        - bilAccountBalance.getTotPrmPaidAmt() - bilAccountBalance.getTotPrmWroAmt());
            } else {
                bilAccountBalance
                        .setInvPrmAmt(bilPrmIstAmt + bisCreFutAmt + bisCreOpnAmt + bisCncFutAmt + bisCncOpnAmt);
                bilAccountBalance.setInvPrmPaidAmt(bisPrmPaidAmt + bisCrePaidAmt);
                bilAccountBalance.setInvPrmWroAmt(bisWroPrmAmt);
                bilAccountBalance.setInvPrmBalAmt(bilAccountBalance.getInvPrmAmt()
                        - bilAccountBalance.getInvPrmPaidAmt() - bilAccountBalance.getInvPrmWroAmt());
            }
        }
        return bilAccountBalance;
    }

    private BilAccountBalance getChargeAmounts(String bilaccountId, BilAccountBalance bilAccountBalance,
            boolean isInvoice) {
        List<Object[]> accountChargesSum = null;
        List<Character> bilCrgTypeCdList = new ArrayList<>();
        bilCrgTypeCdList.add(CHAR_P);
        bilCrgTypeCdList.add(CHAR_L);
        bilCrgTypeCdList.add(CHAR_D);
        List<Character> bilInvoiceCdList = new ArrayList<>();
        bilInvoiceCdList.add(CHAR_Y);
        bilInvoiceCdList.add(CHAR_L);
        if (!isInvoice) {
            accountChargesSum = bilCrgAmountsRepository.getChargeAmountForAccountBalance(bilaccountId, bilCrgTypeCdList,
                    CHAR_S, bilInvoiceCdList);
        } else {
            accountChargesSum = bilCrgAmountsRepository.getInvoicedChargeAmountForAccountBalance(bilaccountId,
                    bilInvoiceCdList);
        }

        if (accountChargesSum != null && !accountChargesSum.isEmpty()) {
            Double crgAmt = (Double) accountChargesSum.get(0)[0];
            Double crgPaidAmt = (Double) accountChargesSum.get(0)[1];
            Double crgWroAmt = (Double) accountChargesSum.get(0)[2];
            crgAmt = defaultAmount(crgAmt);
            crgPaidAmt = defaultAmount(crgPaidAmt);
            crgWroAmt = defaultAmount(crgWroAmt);
            if (!isInvoice) {
                bilAccountBalance.setTotCrgAmt(crgAmt);
                bilAccountBalance.setTotCrgPaidAmt(crgPaidAmt);
                bilAccountBalance.setTotCrgWroAmt(crgWroAmt);
                bilAccountBalance.setTotCrgBalAmt(bilAccountBalance.getTotCrgAmt()
                        - bilAccountBalance.getTotCrgPaidAmt() - bilAccountBalance.getTotCrgWroAmt());
            } else {
                bilAccountBalance.setInvCrgAmt(crgAmt);
                bilAccountBalance.setInvCrgPaidAmt(crgPaidAmt);
                bilAccountBalance.setInvCrgWroAmt(crgWroAmt);
                bilAccountBalance.setInvCrgBalAmt(bilAccountBalance.getInvCrgAmt()
                        - bilAccountBalance.getInvCrgPaidAmt() - bilAccountBalance.getInvCrgWroAmt());
            }
        }
        return bilAccountBalance;
    }

    private Double defaultAmount(Double amount) {
        if (amount == null) {
            amount = Double.valueOf(0.00);
        }
        return amount;
    }

    private BigDecimal defaultAmount(BigDecimal amount) {
        if (amount == null) {
            amount = BigDecimal.valueOf(0.00);
        }
        return amount;
    }

    private String formatPayableItemCode(String payableItemCode) {
        if (!payableItemCode.trim().isEmpty()) {
            List<String> payableItemList = new ArrayList<>();
            payableItemList.add(BusCodeTranslationType.BILLING_ITEM.getValue());
            payableItemList.add(BusCodeTranslationType.AMOUNT.getValue());
            payableItemList.add(BusCodeTranslationType.BPA.getValue());
            payableItemList.add(BusCodeTranslationType.COMMISION.getValue());
            payableItemList.add(BusCodeTranslationType.EIT.getValue());
            payableItemList.add(BusCodeTranslationType.BPS.getValue());

            List<BusCdTranslation> busCodeTranslationList = busCdTranslationRepository
                    .findAllByCodeAndTypeListAndParentCode(payableItemCode, payableItemList,
                            BusCodeTranslationParentCode.PAYABLE_ITEM.getValue(), execContext.getLanguage());
            if (busCodeTranslationList != null && !busCodeTranslationList.isEmpty()) {
                return busCodeTranslationList.get(0).getBusDescription().trim();
            } else {
                throw new DataNotFoundException("support.data.missing", new Object[] { "Payable Item" });
            }
        } else {
            return BLANK_STRING;
        }
    }

    @Override
    public Payment getCurrency(Payment payment) {
        payment.setCurrency(getCurrencyFormatByCode(payment.getCurrency()));
        return payment;
    }

    private String getCurrencyFormatByCode(String currencyCode) {
        String currencySymbol = currencyCode;
        if (currencyCode == null || currencyCode.trim().isEmpty()) {
            currencyCode = "USD";
        }
        Locale locale = LOCALE_CURRENCY_MAP.get(currencyCode.trim());
        if (locale != null) {
            Currency currency = Currency.getInstance(locale);
            currencySymbol = currency.getSymbol(locale);
        }

        return currencySymbol;
    }

    public static final Map<String, Locale> LOCALE_CURRENCY_MAP;
    static {
        final Map<String, Locale> m = new HashMap<>();

        m.put("USD", Locale.US);
        m.put("EUR", Locale.GERMANY);
        m.put("GBP", Locale.UK);
        m.put("JPY", Locale.JAPAN);

        LOCALE_CURRENCY_MAP = Collections.unmodifiableMap(m);
    }

    private boolean isDigitalWalletPayment(Character cashEntryMethodCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(
                String.valueOf(cashEntryMethodCode), BusCodeTranslationType.DIGITAL_WALLET_TYPE.getValue(),
                execContext.getLanguage());

        return busCdTranslation != null ? Boolean.TRUE : Boolean.FALSE;
    }

}