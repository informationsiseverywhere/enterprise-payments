package billing.service.impl;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.time.ZonedDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import billing.data.entity.BilRulesUct;
import billing.data.repository.BilRulesUctRepository;
import billing.service.BilRulesUctService;
import core.datetime.service.DateService;

@Service
public class BilRulesUctServiceImpl implements BilRulesUctService {
    private static final Logger logger = LoggerFactory.getLogger(BilRulesUctServiceImpl.class);

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private DateService dateService;

    @Override
    public BilRulesUct readBilRulesUct(String ruleCode, String billTypeCd, String billClassCd, String billPlanCd) {
        ZonedDateTime se3DateTime = dateService.currentDate();
        BilRulesUct bilRulesUct = null;
        if (billPlanCd != null) {
            logger.debug("get bilRulesUct by policyPlan code combinations");
            bilRulesUct = bilRulesUctRepository.getBilRulesRow(ruleCode, billTypeCd, billClassCd, billPlanCd,
                    se3DateTime, se3DateTime);
            if (bilRulesUct == null) {
                bilRulesUct = bilRulesUctRepository.getBilRulesRow(ruleCode, SEPARATOR_BLANK, billClassCd, billPlanCd,
                        se3DateTime, se3DateTime);
                if (bilRulesUct == null) {
                    bilRulesUct = bilRulesUctRepository.getBilRulesRow(ruleCode, billTypeCd, SEPARATOR_BLANK,
                            billPlanCd, se3DateTime, se3DateTime);
                    if (bilRulesUct == null) {
                        bilRulesUct = bilRulesUctRepository.getBilRulesRow(ruleCode, SEPARATOR_BLANK, SEPARATOR_BLANK,
                                billPlanCd, se3DateTime, se3DateTime);
                    }
                }
            }
        } else {
            logger.debug("get bilRulesUct by other combinations");
            bilRulesUct = getBilRulesUctByOtherCombination(ruleCode, billTypeCd, billClassCd, se3DateTime);
        }
        return bilRulesUct;
    }

    private BilRulesUct getBilRulesUctByOtherCombination(String ruleCode, String billTypeCd, String billClassCd,
            ZonedDateTime se3DateTime) {
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(ruleCode, billTypeCd, billClassCd,
                SEPARATOR_BLANK, se3DateTime, se3DateTime);
        if (bilRulesUct == null) {
            bilRulesUct = bilRulesUctRepository.getBilRulesRow(ruleCode, billTypeCd, SEPARATOR_BLANK, SEPARATOR_BLANK,
                    se3DateTime, se3DateTime);
            if (bilRulesUct == null) {
                bilRulesUct = bilRulesUctRepository.getBilRulesRow(ruleCode, SEPARATOR_BLANK, SEPARATOR_BLANK,
                        SEPARATOR_BLANK, se3DateTime, se3DateTime);
            }
        }
        return bilRulesUct;
    }
}