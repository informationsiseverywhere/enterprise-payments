package billing.service.impl;

import static billing.utils.BillingConstants.DEFAULT_DATE;
import static billing.utils.BillingConstants.ERROR_CODE;
import static core.utils.CommonConstants.BIGDECIMAL_ZERO;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_G;
import static core.utils.CommonConstants.CHAR_H;
import static core.utils.CommonConstants.CHAR_I;
import static core.utils.CommonConstants.CHAR_L;
import static core.utils.CommonConstants.CHAR_M;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_U;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import application.utils.model.AuthorizeOption;
import application.utils.service.CommissionsRestService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilActInquiry;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilAmounts;
import billing.data.entity.BilAmtTrmRlt;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilCrgAmounts;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilFullPmtDsc;
import billing.data.entity.BilInvDtl;
import billing.data.entity.BilInvDtlRlt;
import billing.data.entity.BilIstSchedule;
import billing.data.entity.BilMstCoSt;
import billing.data.entity.BilObjectCfg;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSttReconDtl;
import billing.data.entity.HalBoMduXrf;
import billing.data.entity.id.BilActInquiryId;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilCashDspId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.entity.id.BilInvDtlId;
import billing.data.entity.id.BilInvDtlRltId;
import billing.data.entity.id.BilIstScheduleId;
import billing.data.entity.id.BilObjectCfgId;
import billing.data.entity.id.BilPolActivityId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilAgtActRltRepository;
import billing.data.repository.BilAmountsRepository;
import billing.data.repository.BilAmtTrmRltRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilEftBankRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilFullPmtDscRepository;
import billing.data.repository.BilInvDtlRepository;
import billing.data.repository.BilInvDtlRltRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilMstCoStRepository;
import billing.data.repository.BilObjectCfgRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilSttReconDtlRepository;
import billing.data.repository.BilTtyActRltRepository;
import billing.data.repository.HalBoMduXrfRepository;
import billing.handler.impl.DownPaymentCashApplyServiceImpl;
import billing.model.CashApply;
import billing.model.Event;
import billing.service.BilRulesUctService;
import billing.service.CashApplyService;
import billing.service.EventPostingService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.AccountTypeCode;
import billing.utils.BillingConstants.BilAmountsEftIndicator;
import billing.utils.BillingConstants.BilCashEntryMethodCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BilReceiptTypeCode;
import billing.utils.BillingConstants.CashOverPayIndicators;
import billing.utils.BillingConstants.ChargeTypeCodes;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.PolicyIssueIndicators;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.id.BusCdTranslationId;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class CashApplyServiceImpl implements CashApplyService {

    @Autowired
    private DateService dateService;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilInvDtlRepository bilInvDtlRepository;

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BilInvDtlRltRepository bilInvDtlRltRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private BilMstCoStRepository bilMstCoStRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilTtyActRltRepository bilTtyActRltRepository;

    @Autowired
    private BilAgtActRltRepository bilAgtActRltRepository;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private BilAmountsRepository bilAmountsRepository;

    @Autowired
    private BilFullPmtDscRepository bilFullPmtDscRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilSttReconDtlRepository bilSttReconDtlRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private DownPaymentCashApplyServiceImpl downPaymentCashApplyServiceImpl;

    @Autowired
    private EventPostingService eventPostingService;

    @Autowired
    private BilAmtTrmRltRepository bilAmtTrmRltRepository;

    @Autowired
    private HalBoMduXrfRepository halBoMduXrfRepository;

    @Autowired
    private CommissionsRestService commissionsRestService;

    @Autowired
    private BilObjectCfgRepository bilObjectCfgRepository;
    
    @Value("${language}")
    private String language;

    public static final Logger logger = LogManager.getLogger(CashApplyServiceImpl.class);

    private static final char QTE_INV_CD_1 = '1';
    private static final char QTE_INV_CD_2 = '2';
    private static final char QTE_INV_CD_3 = '3';
    private static final char QTE_INV_CD_4 = '4';
    private static final char QTE_INV_CD_5 = '5';
    private static final String CCMOBCM = "CCMOBCM";
    private static final String NO_DATA_FOUND = "no.data.found";

    private static final List<Character> FUTURE_INVOICE_CODES = Arrays.asList(BLANK_CHAR,
            InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue());

    private static final List<Character> CURRENT_INVOICE_CODES = Arrays.asList(
            InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
            InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());

    private static final List<Character> INVOICE_CODES = Arrays.asList(BLANK_CHAR,
            InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue(),
            InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue(),
            InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue());

    private static final List<Character> CATCHUP_INVOICE_CODES = Arrays.asList(BLANK_CHAR,
            InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue(),
            InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue());

    @Override
    @Transactional
    public CashApply cashApply(CashApply cashApply, boolean firtstTimeSw) {

        boolean quoteTermHiddenInd = false;
        char alcaRuleCd = CHAR_N;
        String alcaRuleParam = SEPARATOR_BLANK;

        BilAccount bilAccount = bilAccountRepository.findById(cashApply.getAccountId()).orElse(null);
        if (bilAccount == null) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }
        if (!cashApply.getCreditPolicyId().isEmpty()) {
            quoteTermHiddenInd = findQuoteHiddenRows(cashApply.getAccountId(), quoteTermHiddenInd);
        }

        if (cashApply.getPaymentPriorityInd() == CHAR_P) {
            BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("ALCA", bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd(), BLANK_STRING);
            if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                alcaRuleCd = bilRulesUct.getBrtRuleCd();
                alcaRuleParam = bilRulesUct.getBrtParmListTxt().trim();
            }
        }

        processCashTables(bilAccount, alcaRuleCd, alcaRuleParam, cashApply, firtstTimeSw);

        if (quoteTermHiddenInd) {
            findQuoteHiddenRows(cashApply.getAccountId(), quoteTermHiddenInd);
        }

        return cashApply;
    }

    private Boolean findQuoteHiddenRows(String accountId, boolean quoteTermHiddenInd) {
        boolean quoteTermHide = false;
        List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.findContractsByStatus(accountId,
                BilPolicyStatusCode.QUOTE.getValue());
        if (bilPolicyTermList != null && !bilPolicyTermList.isEmpty()) {
            if (quoteTermHiddenInd) {
                quoteTermHide = false;
            } else {
                quoteTermHide = true;
            }
            quoteTermHiddenInd = checkQuoteTermProcess(bilPolicyTermList, quoteTermHide);
        }

        return quoteTermHiddenInd;

    }

    private void deleteOrSuspendCash(double originalCashAmount, String accountId, ZonedDateTime bilDtbDate,
            short bilDtbSeqNbr, short bilDspSeqNbr, double dspAmount) {
        BilCashDsp bilCashDsp = bilCashDspRepository
                .findById(new BilCashDspId(accountId, bilDtbDate, bilDtbSeqNbr, bilDspSeqNbr)).orElse(null);
        if (bilCashDsp == null) {
            return;
        }
        if (originalCashAmount == 0) {
            bilCashDspRepository.delete(bilCashDsp);
            bilCashDspRepository.flush();
            return;
        }

        if (originalCashAmount == dspAmount) {
            return;
        }
        bilCashDsp.setDspAmount(originalCashAmount);
        bilCashDspRepository.saveAndFlush(bilCashDsp);
    }

    private double processCashTables(BilAccount bilAccount, char alcaRuleCd, String alcaRuleParam, CashApply cashApply,
            boolean firtstTimeSw) {

        BilPolicyTerm bilPolicyTerm = null;
        double originalCashAmount = DECIMAL_ZERO;
        double invoiceCommissionPersent = DECIMAL_ZERO;
        boolean equityPymtTypeInd = false;
        boolean creditReceipt = false;
        String typeSpace = SEPARATOR_BLANK;
        String accountId = cashApply.getAccountId();
        String policyId = cashApply.getPolicyId();
        ZonedDateTime bilDtbDate = cashApply.getBilDtbDate();
        short bilDtbSeqNbr = cashApply.getBilDtbSequenceNumber();
        short bilDspSeqNbr = cashApply.getBilDspSequenceNumber();
        char cashOverpayInd = cashApply.getCashOverpaymentInd();
        boolean dpProgramSw = false;
        char billTypeCode = BLANK_CHAR;
        int updatedRows = SHORT_ZERO;

        BilCashDsp bilCashDsp = bilCashDspRepository
                .findById(new BilCashDspId(accountId, bilDtbDate, bilDtbSeqNbr, bilDspSeqNbr)).orElse(null);
        if (bilCashDsp != null) {
            originalCashAmount = bilCashDsp.getDspAmount();
            BilCashReceipt bilCashReceipt = bilCashReceiptRepository
                    .findById(new BilCashReceiptId(accountId, bilDtbDate, bilDtbSeqNbr)).orElse(null);
            if (bilCashReceipt != null) {
                if (bilCashReceipt.getReceiptTypeCd().equalsIgnoreCase(BilReceiptTypeCode.CREDIT_CASH.getValue())
                        || bilCashReceipt.getReceiptTypeCd()
                                .equalsIgnoreCase(BilReceiptTypeCode.COMMISSION_PAYMENT_CASH.getValue())) {
                    creditReceipt = true;
                }
                if (validateIfEquityPayment(bilCashReceipt.getReceiptTypeCd())) {
                    equityPymtTypeInd = true;
                }
                if (bilCashDsp.getManualSuspenseIndicator() == CHAR_Y
                        && bilCashDsp.getDspReasonCd().equalsIgnoreCase("DPR")) {
                    Object[] cashDownPayment = checkDownPayment(cashOverpayInd, cashApply.isDownPaymentDateSw(),
                            bilCashReceipt.getEntryDate(), cashApply.getCashApplyDate());
                    dpProgramSw = (boolean) cashDownPayment[0];
                    cashOverpayInd = (char) cashDownPayment[1];
                }
                if (creditReceipt && bilCashDsp.getPayableItemCd().trim().equalsIgnoreCase(typeSpace) && firtstTimeSw) {
                    typeSpace = "999";
                }
                if (dpProgramSw) {
                    downPaymentCashApplyServiceImpl.cashApply(bilCashDsp, cashApply.getQuoteIndicator(),
                            cashApply.getCashApplyDate(), cashApply.getUserSequenceId());
                    return 0;
                }

                updatedRows = hideExcludePremium(bilCashDsp, cashApply.getCashApplyDate(), cashApply.getUserSequenceId());

                if (!bilCashDsp.getPolicyNumber().trim().isEmpty()) {
                    bilPolicyTerm = getPolicyRow(bilCashDsp, cashApply, equityPymtTypeInd, bilCashReceipt, BLANK_STRING,
                            billTypeCode);
                } else {
                    bilPolicyTerm = new BilPolicyTerm();
                    bilPolicyTerm.setBillPolicyTermId(
                            new BilPolicyTermId(BLANK_STRING, BLANK_STRING, DateRoutine.defaultDateTime()));
                }

                if (bilCashReceipt.getReceiptTypeCd().equalsIgnoreCase(BilReceiptTypeCode.GROSS_PAYMENT.getValue())
                        || bilCashReceipt.getReceiptTypeCd()
                                .equalsIgnoreCase(BilReceiptTypeCode.GROSS_WRITEOFF.getValue())
                        || bilCashReceipt.getReceiptTypeCd()
                                .equalsIgnoreCase(BilReceiptTypeCode.GROSS_OVERPAY.getValue())) {
                    invoiceCommissionPersent = loadCommissonPercent(bilCashDsp, policyId);
                }

                originalCashAmount = processCash(policyId, bilAccount, bilCashDsp, bilCashReceipt, equityPymtTypeInd,
                        originalCashAmount, alcaRuleCd, alcaRuleParam, invoiceCommissionPersent, cashOverpayInd,
                        typeSpace, cashApply, bilPolicyTerm, creditReceipt);
            }
        }
        deleteOrSuspendCash(originalCashAmount, accountId, bilDtbDate, bilDtbSeqNbr, bilDspSeqNbr,
                bilCashDsp != null ? bilCashDsp.getDspAmount() : DECIMAL_ZERO);
        if (updatedRows > SHORT_ZERO) {
            unHideExcludePremium(bilCashDsp.getBilCashDspId().getAccountId(), cashApply.getCashApplyDate(), cashApply.getUserSequenceId());
        }
        return originalCashAmount;
    }

    private void unHideExcludePremium(String accountId, ZonedDateTime cashApplyDate, String userSequenceId) {
        List<BilCashDsp> bilCashDspList = bilCashDspRepository.findHideExcludePremiumRow(accountId, cashApplyDate, "XC",
                SHORT_ZERO, userSequenceId);
        if (bilCashDspList != null && !bilCashDspList.isEmpty()) {
            for (BilCashDsp bilCashDsp : bilCashDspList) {
                BilIstSchedule bilIstSchedule = bilIstScheduleRepository
                        .findById(new BilIstScheduleId(bilCashDsp.getBilCashDspId().getAccountId(),
                                bilCashDsp.getPolicyId(), bilCashDsp.getBilSequenceNumber()))
                        .orElse(null);
                if (bilIstSchedule == null) {
                    throw new DataNotFoundException(NO_DATA_FOUND);
                }
                bilIstSchedule.setBisPrmPaidAmt(bilCashDsp.getDspAmount());
                bilIstScheduleRepository.saveAndFlush(bilIstSchedule);
                bilCashDspRepository.deleteById(bilCashDsp.getBilCashDspId());
            }
        }
    }

    private int hideExcludePremium(BilCashDsp bilCashDsp, ZonedDateTime applyDate, String userSequenceId) {
        String accountId = bilCashDsp.getBilCashDspId().getAccountId();

        List<String> parentIdList = null;

        int updatedRows = SHORT_ZERO;
        String parentId;
        char bilTypeCode = BLANK_CHAR;
        parentIdList = bilTtyActRltRepository.getBilThirdPartyIdByBilAccountId(accountId);
        if (parentIdList != null && !parentIdList.isEmpty()) {
            bilTypeCode = CHAR_T;
        } else {
            parentIdList = bilAgtActRltRepository.getBilAgtActIdByAccountId(accountId);
            if (parentIdList != null && !parentIdList.isEmpty()) {
                bilTypeCode = CHAR_G;
            }
        }
        if (parentIdList != null && !parentIdList.isEmpty()) {
            parentId = parentIdList.get(SHORT_ZERO);
            List<BilSttReconDtl> bilSttReconDtlList = bilSttReconDtlRepository.findExcluteItemRows(parentId,
                    bilTypeCode, accountId, new ArrayList<>(Arrays.asList("EXC", "DFR")), CHAR_P, DECIMAL_ZERO);
            if (bilSttReconDtlList != null && !bilSttReconDtlList.isEmpty()) {
                for (BilSttReconDtl bilSttReconDtl : bilSttReconDtlList) {
                    BilInvDtlRlt bilInvDtlRlt = bilInvDtlRltRepository.findById(new BilInvDtlRltId(parentId,
                            bilTypeCode, accountId, bilSttReconDtl.getBilSttReconDtlId().getBilInvEntryDt(),
                            bilSttReconDtl.getBilSttReconDtlId().getBilInvSeqNbr())).orElse(null);
                    if (bilInvDtlRlt == null) {
                        throw new InvalidDataException("bil.inv.dtl.rlt.rows.not.found ");
                    }

                    if (bilTypeCode == CHAR_G) {
                        List<BilInvDtlRlt> bilInvDtlRltList = bilInvDtlRltRepository.fetchBilInvDtlRltRows(parentId,
                                bilTypeCode, accountId, bilSttReconDtl.getBilSttReconDtlId().getBilInvEntryDt(),
                                bilSttReconDtl.getBilSttReconDtlId().getBilInvSeqNbr(), bilInvDtlRlt.getPolicyId(),
                                bilInvDtlRlt.getPolEffectiveDt(), bilInvDtlRlt.getBilSeqNbr());
                        if (bilInvDtlRltList != null && !bilInvDtlRltList.isEmpty()) {
                            short invSquenceNbr = bilInvDtlRltList.get(SHORT_ZERO).getBilInvDtlRltId()
                                    .getBilInvSeqNbr();
                            List<String> reconAcyCdList = bilSttReconDtlRepository.findReconAcyCd(parentId, bilTypeCode,
                                    accountId, bilSttReconDtl.getBilSttReconDtlId().getBilInvEntryDt(), invSquenceNbr,
                                    new ArrayList<>(Arrays.asList("EXC", "DFR", "IMP")));
                            if (reconAcyCdList != null && !reconAcyCdList.isEmpty()
                                    && reconAcyCdList.get(SHORT_ZERO).equals("IMP")) {
                                continue;
                            }
                        }
                    }

                    updatedRows = processExludeScheduleRows(updatedRows, accountId, bilTypeCode, bilInvDtlRlt,
                            bilSttReconDtl, bilCashDsp, applyDate, userSequenceId);

                }

            }
        }

        return updatedRows;
    }

    private int processExludeScheduleRows(int updatedRows, String accountId, char bilTypeCode,
            BilInvDtlRlt bilInvDtlRlt, BilSttReconDtl bilSttReconDtl, BilCashDsp bilCashDsp, ZonedDateTime applyDate, String userSequenceId) {

        Double dispositionAmount = null;
        Double differenceAmount = null;
        Double premPaidAmount = null;
        Double acyAmount = null;

        List<BilIstSchedule> bilIstScheduleList = bilIstScheduleRepository.getBilIstSchedulRowsByAccountId(accountId,
                bilInvDtlRlt.getPolicyId(), bilInvDtlRlt.getBilSeqNbr(),
                new ArrayList<>(Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                        InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue())));
        if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
            BilIstSchedule bilIstSchedule = bilIstScheduleList.get(SHORT_ZERO);

            Double balanceAmount = bilIstSchedule.getBisCncFutAmt() + bilIstSchedule.getBisCreFutAmt()
                    + bilIstSchedule.getBisCncOpnAmt() + bilIstSchedule.getBillPrmIstAmt()
                    + bilIstSchedule.getBisCreOpnAmt();
            Double paidAmount = bilIstSchedule.getBisWroPrmAmt() + bilIstSchedule.getBisPrmPaidAmt()
                    + bilIstSchedule.getBisCrePaidAmt();
            balanceAmount = balanceAmount - paidAmount;
            acyAmount = bilSttReconDtl.getBilAcyAmt();
            if (balanceAmount > DECIMAL_ZERO) {
                if (bilTypeCode == CHAR_G) {
                    BilInvDtl bilInvDtl = bilInvDtlRepository
                            .findById(new BilInvDtlId(bilSttReconDtl.getBilSttReconDtlId().getBilTchKeyId(),
                                    bilTypeCode, accountId, bilSttReconDtl.getBilSttReconDtlId().getBilInvEntryDt(),
                                    bilSttReconDtl.getBilSttReconDtlId().getBilInvSeqNbr()))
                            .orElse(null);
                    if (bilInvDtl == null) {
                        throw new DataNotFoundException(NO_DATA_FOUND);
                    }
                    differenceAmount = bilInvDtl.getBidGrsDetailAmt() - bilInvDtl.getBidNetDetailAmt();
                    if (differenceAmount > DECIMAL_ZERO) {
                        if (bilSttReconDtl.getBilAcyAmt() == bilInvDtl.getBidNetDetailAmt()) {
                            acyAmount = balanceAmount;
                        } else {
                            acyAmount = bilSttReconDtl.getBilAcyAmt();
                        }
                    }
                }
                if (balanceAmount <= acyAmount) {
                    premPaidAmount = bilIstSchedule.getBisPrmPaidAmt() + balanceAmount;
                    dispositionAmount = balanceAmount;
                } else {
                    premPaidAmount = bilIstSchedule.getBisPrmPaidAmt() + bilSttReconDtl.getBilAcyAmt();
                    dispositionAmount = bilSttReconDtl.getBilAcyAmt();
                }

                bilIstSchedule.setBisPrmPaidAmt(premPaidAmount);
                bilIstScheduleRepository.saveAndFlush(bilIstSchedule);
                updatedRows++;
                insertCash1(accountId, bilInvDtlRlt.getBilSeqNbr(), bilIstSchedule.getBillInvDt(), applyDate,
                        dispositionAmount, bilInvDtlRlt.getPolicyId(), bilCashDsp.getPolicyNumber(),
                        bilCashDsp.getPolicySymbolCd(), bilInvDtlRlt.getPolEffectiveDt(),
                        bilIstSchedule.getBillPblItemCd(), userSequenceId);
            }
        }
        return updatedRows;
    }

    private BilPolicyTerm getPolicyRow(BilCashDsp bilCashDsp, CashApply cashApply, boolean equityPymtTypeInd,
            BilCashReceipt bilCashReceipt, String policyId, char billTypeCode) {

        BilPolicyTerm bilPolicyTerm = null;
        ZonedDateTime policyEffectiveDate = null;

        if (!cashApply.getPolicyId().trim().isEmpty()) {
            policyId = cashApply.getPolicyId().trim();
        } else if ((bilCashReceipt.getReceiptTypeCd().equals(BilReceiptTypeCode.GROSS_WRITEOFF.getValue())
                || bilCashReceipt.getReceiptTypeCd().equals(BilReceiptTypeCode.GROSS_PAYMENT.getValue())
                || bilCashReceipt.getReceiptTypeCd().equals(BilReceiptTypeCode.GROSS_OVERPAY.getValue()))
                && policyId.equals(BLANK_STRING)) {
            policyId = bilPolicyRepository.findMaxPolicyId(bilCashDsp.getBilCashDspId().getAccountId(),
                    bilCashDsp.getPolicyNumber(), bilCashDsp.getPolicySymbolCd(), getIssueIndicatorList());
        } else if (bilCashReceipt.getReceiptTypeCd().equals(BilReceiptTypeCode.GROSS_WRITEOFF.getValue())
                || bilCashReceipt.getReceiptTypeCd().equals(BilReceiptTypeCode.GROSS_PAYMENT.getValue())
                || bilCashReceipt.getReceiptTypeCd().equals(BilReceiptTypeCode.GROSS_OVERPAY.getValue())
                || billTypeCode == CHAR_T && !policyId.equals(BLANK_STRING)) {

        } else if (!bilCashDsp.getPolicySymbolCd().trim().isEmpty()) {
            policyId = bilPolicyRepository.findMaxPolicyId(bilCashDsp.getBilCashDspId().getAccountId(),
                    bilCashDsp.getPolicyNumber(), bilCashDsp.getPolicySymbolCd(), getIssueIndicatorList());
        } else {
            policyId = bilPolicyRepository.findMaxPolicyId(bilCashDsp.getBilCashDspId().getAccountId(),
                    bilCashDsp.getPolicyNumber(), BLANK_STRING, getIssueIndicatorList());
            if (policyId == null) {
                policyId = bilPolicyRepository.findMaxPolicyId1(bilCashDsp.getBilCashDspId().getAccountId(),
                        bilCashDsp.getPolicyNumber(), getIssueIndicatorList());
            }
        }
        if (policyId == null) {
            return null;
        }

        if (equityPymtTypeInd) {
            bilPolicyTerm = getEquityPolicyRow(bilCashDsp.getBilCashDspId().getAccountId(), policyId,
                    cashApply.getCashApplyDate());
        } else {
            if (cashApply.getPolicyEffectiveDate() != null
                    && cashApply.getPolicyEffectiveDate() != DateRoutine.defaultDateTime()) {
                policyEffectiveDate = cashApply.getPolicyEffectiveDate();
            }
            bilPolicyTerm = getPolicyEffectiveRow(policyEffectiveDate, bilCashDsp.getBilCashDspId().getAccountId(),
                    policyId);
        }
        return bilPolicyTerm;
    }

    private List<Character> getIssueIndicatorList() {
        List<Character> issueIndicatorList = new ArrayList<>();
        issueIndicatorList.add(CHAR_N);
        issueIndicatorList.add(PolicyIssueIndicators.NOT_ISSUED.getValue());
        issueIndicatorList.add(PolicyIssueIndicators.CANCELLED_REWRITE.getValue());
        issueIndicatorList.add(PolicyIssueIndicators.CANCELLED_REISSUE.getValue());
        issueIndicatorList.add(PolicyIssueIndicators.VOID.getValue());

        return issueIndicatorList;
    }

    private BilPolicyTerm getPolicyEffectiveRow(ZonedDateTime policyEffectiveDate, String accountId, String policyId) {

        ZonedDateTime bptPolEffectiveDt = null;
        if (policyEffectiveDate == null
                || policyEffectiveDate.compareTo(DateRoutine.defaultSinceDateTime()) == SHORT_ZERO) {
            bptPolEffectiveDt = bilPolicyTermRepository.getMaxPolicyEffectiveDate(accountId, policyId);
            if (bptPolEffectiveDt == null) {
                return null;
            }
        } else {
            bptPolEffectiveDt = policyEffectiveDate;
        }

        return bilPolicyTermRepository.findById(new BilPolicyTermId(accountId, policyId, bptPolEffectiveDt))
                .orElse(null);

    }

    private BilPolicyTerm getEquityPolicyRow(String accountId, String policyId, ZonedDateTime cahApplyDate) {

        List<BilPolicyTerm> bilPolicyTermList = null;
        bilPolicyTermList = bilPolicyTermRepository.getMaxPolicyEffectiveRow(accountId, policyId, cahApplyDate,
                PageRequest.of(SHORT_ZERO, SHORT_ONE));

        if (bilPolicyTermList != null && !bilPolicyTermList.isEmpty()) {
            double equityAmount = calculateSumAmount(accountId, policyId,
                    bilPolicyTermList.get(SHORT_ZERO).getBillPolicyTermId().getPolEffectiveDt());
            if (equityAmount == DECIMAL_ZERO) {
                List<BilPolicyTerm> bilPolicyTermList1 = null;
                bilPolicyTermList1 = bilPolicyTermRepository.getMinPolicyEffectiveRow(accountId, policyId, cahApplyDate,
                        PageRequest.of(SHORT_ZERO, SHORT_ONE));
                return bilPolicyTermList1.get(SHORT_ZERO);
            } else {
                return bilPolicyTermList.get(SHORT_ZERO);
            }

        } else {
            bilPolicyTermList = bilPolicyTermRepository.getMinPolicyEffectiveRow(accountId, policyId, cahApplyDate,
                    PageRequest.of(SHORT_ZERO, SHORT_ONE));
            if (bilPolicyTermList != null && !bilPolicyTermList.isEmpty()) {
                return bilPolicyTermList.get(SHORT_ZERO);
            }
        }

        return null;
    }

    private Double calculateSumAmount(String accountId, String policyId, ZonedDateTime polEffectiveDt) {

        Double equityAmount = bilIstScheduleRepository.calculateBalanceAmount(accountId, policyId, polEffectiveDt);
        if (equityAmount == null || equityAmount == DECIMAL_ZERO) {
            return DECIMAL_ZERO;
        }
        return equityAmount;

    }

    private double loadCommissonPercent(BilCashDsp bilCashDsp, String policyId) {
        double commissionPersent = DECIMAL_ZERO;
        if (bilCashDsp.getInvoiceDate().compareTo(DateRoutine.defaultDateTime()) != 0) {
            List<BilInvDtlRlt> bilInvDtlRltList = bilInvDtlRltRepository.fetchCommissionPersent(
                    bilCashDsp.getBilCashDspId().getAccountId(), bilCashDsp.getInvoiceDate(), policyId,
                    bilCashDsp.getBilSequenceNumber());

            if (bilInvDtlRltList != null && !bilInvDtlRltList.isEmpty()) {

                for (BilInvDtlRlt bilInvDtlRlt : bilInvDtlRltList) {
                    BilInvDtl bilInvDtl = bilInvDtlRepository
                            .findById(new BilInvDtlId(bilInvDtlRlt.getBilInvDtlRltId().getBilTchKeyId(),
                                    bilInvDtlRlt.getBilInvDtlRltId().getBilTchKeyTypCd(),
                                    bilInvDtlRlt.getBilInvDtlRltId().getBilAccountId(),
                                    bilInvDtlRlt.getBilInvDtlRltId().getBilInvEntryDt(),
                                    bilInvDtlRlt.getBilInvDtlRltId().getBilInvSeqNbr()))
                            .orElse(null);
                    if (bilInvDtl != null) {
                        commissionPersent = bilInvDtl.getBilCommissionPct();
                        break;
                    }
                }
            }
            BilCashDsp bilCashDspUpdate = bilCashDspRepository.findSuspendedSchedule(
                    bilCashDsp.getBilCashDspId().getAccountId(), bilCashDsp.getBilCashDspId().getBilDtbDate(),
                    bilCashDsp.getBilCashDspId().getDtbSequenceNbr(), bilCashDsp.getBilCashDspId().getDspSequenceNbr(),
                    BilDspTypeCode.SUSPENDED.getValue());
            if (bilCashDspUpdate != null) {
                bilCashDspUpdate.setInvoiceDate(DateRoutine.defaultDateTime());
                bilCashDspRepository.saveAndFlush(bilCashDspUpdate);
            }
        } else {
            BilIstSchedule bilIstSchedule = bilIstScheduleRepository
                    .findById(new BilIstScheduleId(bilCashDsp.getBilCashDspId().getAccountId(),
                            bilCashDsp.getPolicyId(), bilCashDsp.getBilSequenceNumber()))
                    .orElse(null);
            if (bilIstSchedule != null) {
                commissionPersent = bilIstSchedule.getBillCommissionPct();
            }
        }
        return commissionPersent;
    }

    private Object[] checkDownPayment(char cashOverpayInd, boolean downPayDateSw, ZonedDateTime entryDate,
            ZonedDateTime cashApplyDate) {
        boolean dpProgramSw = true;
        if (downPayDateSw) {
            BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("DPCA", BLANK_STRING, BLANK_STRING,
                    BLANK_STRING);
            if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y
                    && Integer.getInteger(bilRulesUct.getBrtParmListTxt().substring(0, 3).trim()) != null
                    && Integer.getInteger(bilRulesUct.getBrtParmListTxt().substring(0, 3).trim()) > SHORT_ZERO) {
                ZonedDateTime differenceDate = DateRoutine.adjustDateWithOffset(cashApplyDate, false,
                        Integer.getInteger(bilRulesUct.getBrtParmListTxt().trim()), '-', null);
                if (entryDate.compareTo(differenceDate) <= 0) {
                    dpProgramSw = false;
                    cashOverpayInd = CashOverPayIndicators.SATISFY_OLDEST_DUE_FIRST.getValue();
                }
            }
        }

        return new Object[] { dpProgramSw, cashOverpayInd };
    }

    private boolean fetchDesReasonCode(String busCd, String busType) {
        return busCdTranslationRepository
                .existsById(new BusCdTranslationId(busCd, busType, language, BLANK_STRING));
    }

    private double processCash(String policyId, BilAccount bilAccount, BilCashDsp bilCashDsp,
            BilCashReceipt bilCashReceipt, boolean equityPymtTypeInd, double originalCashAmount, char alcaRuleCd,
            String alcaRuleParam, double invoiceCommissionPersent, char cashOverpayInd, String typeSpace,
            CashApply cashApply, BilPolicyTerm bilPolicyTerm, boolean creditReceipt) {

        if (equityPymtTypeInd) {
            if (bilCashDsp.getPolicyNumber().trim().isEmpty()) {
                return originalCashAmount;
            }

            originalCashAmount = processEquityPayment(bilPolicyTerm, bilCashDsp, bilAccount, bilCashReceipt, cashApply,
                    originalCashAmount);

            if (originalCashAmount <= DECIMAL_ZERO) {
                return originalCashAmount;
            }

        }
        Map<Character, Character> auditPremiumHidden = null;
        boolean auditPremiumHiddenSw = false;
        if (alcaRuleCd == CHAR_Y && alcaRuleParam.charAt(1) == CHAR_Y) {
            auditPremiumHidden = hideAuditPremium(policyId, bilCashDsp, typeSpace, cashApply.getCashApplyDate());
            if (auditPremiumHidden != null && !auditPremiumHidden.isEmpty()) {
                auditPremiumHiddenSw = true;
            }
        }

        if (!bilCashReceipt.getReceiptTypeCd().trim().equalsIgnoreCase(BilReceiptTypeCode.EVEN_ADJUSTMENT.getValue())) {
            originalCashAmount = processOpensAmount(policyId, bilAccount, bilCashDsp, invoiceCommissionPersent,
                    typeSpace, cashApply, originalCashAmount, bilCashReceipt, bilPolicyTerm, alcaRuleParam,
                    creditReceipt, auditPremiumHiddenSw);
        }

        if (auditPremiumHidden != null && !auditPremiumHidden.isEmpty()) {
            restoreAuditPremium(bilCashDsp.getBilCashDspId().getAccountId(), auditPremiumHidden);
            if (originalCashAmount > 0 && !bilCashReceipt.getReceiptTypeCd().trim()
                    .equalsIgnoreCase(BilReceiptTypeCode.EVEN_ADJUSTMENT.getValue())) {
                originalCashAmount = processOpensAmount(policyId, bilAccount, bilCashDsp, invoiceCommissionPersent,
                        typeSpace, cashApply, originalCashAmount, bilCashReceipt, bilPolicyTerm, alcaRuleParam,
                        creditReceipt, auditPremiumHiddenSw);
            }
        }

        checkPendingTape(bilAccount, cashApply.getCashApplyDate());
        if (!bilCashDsp.getAdjustmentDueDate().equals(DateRoutine.defaultDateTime())
                && bilCashReceipt.getReceiptTypeCd().trim().equals(BilReceiptTypeCode.EVEN_ADJUSTMENT.getValue())) {
            originalCashAmount = catchupFutures(true, bilCashDsp, bilPolicyTerm, invoiceCommissionPersent, typeSpace,
                    originalCashAmount, bilCashDsp.getAdjustmentDueDate(), cashApply, bilCashReceipt, bilAccount,
                    creditReceipt);

        }
        if (originalCashAmount > DECIMAL_ZERO) {
            originalCashAmount = processFutures(bilAccount, bilCashDsp, policyId, invoiceCommissionPersent, typeSpace,
                    originalCashAmount, cashApply, bilCashReceipt, bilPolicyTerm, creditReceipt, cashOverpayInd);
        }

        return originalCashAmount;

    }

    private double processEquityPayment(BilPolicyTerm bilPolicyTerm, BilCashDsp bilCashDsp, BilAccount bilAccount,
            BilCashReceipt bilCashReceipt, CashApply cashApply, double originalCashAmount) {

        List<BilIstSchedule> bilIstScheduleList = null;
        String prevPolId = BLANK_STRING;
        ZonedDateTime prevPolEffDt = null;

        if (bilPolicyTerm != null) {

            if (bilPolicyTerm.getBptAgreementInd() != CHAR_Y) {

                List<String> payableItemList = busCdTranslationRepository.findCodeListByTypeList(
                        new ArrayList<>(Arrays.asList("BIT", "BEQ")), language);
                bilIstScheduleList = bilIstScheduleRepository.findPaymentByPolicy(
                        bilCashDsp.getBilCashDspId().getAccountId(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        payableItemList, FUTURE_INVOICE_CODES, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            } else {

                bilIstScheduleList = bilIstScheduleRepository.findPaymentByAgreement(
                        bilCashDsp.getBilCashDspId().getAccountId(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        FUTURE_INVOICE_CODES, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            }
            if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
                for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                    Object[] equityFuture = processEquityFutureRows(originalCashAmount, bilIstSchedule, bilAccount,
                            bilCashDsp, cashApply, bilCashReceipt, bilPolicyTerm, prevPolId, prevPolEffDt);
                    originalCashAmount = (double) equityFuture[0];
                    prevPolId = (String) equityFuture[1];
                    prevPolEffDt = (ZonedDateTime) equityFuture[2];

                    if (originalCashAmount == DECIMAL_ZERO) {
                        break;
                    }
                }
            }
        }

        return originalCashAmount;

    }

    private void restoreAuditPremium(String accountId, Map<Character, Character> auditPremiumHidden) {
        for (Entry<Character, Character> entrySet : auditPremiumHidden.entrySet()) {
            Character invoiceCode = entrySet.getKey();
            if (invoiceCode != null) {
                bilIstScheduleRepository.restoreHideAuditPremium(invoiceCode, accountId,
                        auditPremiumHidden.get(invoiceCode));
            }
        }
    }

    private Map<Character, Character> hideAuditPremium(String policyId, BilCashDsp bilCashDsp, String typeSpace,
            ZonedDateTime applyDate) {
        Map<Character, Character> auditPremiumHidden = null;
        boolean rowExistSw = busCdTranslationRepository.existsById(
                new BusCdTranslationId(bilCashDsp.getPayableItemCd(), "XCA", language, BLANK_STRING));
        if (rowExistSw) {
            List<BilIstSchedule> bilIstScheduleList = bilIstScheduleRepository.fetchHideAuditPremium(
                    bilCashDsp.getBilCashDspId().getAccountId(), CURRENT_INVOICE_CODES, policyId, BLANK_STRING,
                    bilCashDsp.getPayableItemCd(), typeSpace, applyDate);
            if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
                auditPremiumHidden = processHideAuditPremium(bilIstScheduleList);
            }
        }

        return auditPremiumHidden;
    }

    private Map<Character, Character> processHideAuditPremium(List<BilIstSchedule> bilIstScheduleList) {
        Map<Character, Character> invoiceCodeHideMap = new HashMap<>();
        for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
            char invoiceCode = bilIstSchedule.getBillInvoiceCd();
            if (invoiceCode == InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()) {
                bilIstSchedule.setBillInvoiceCd('7');
                invoiceCodeHideMap.put(invoiceCode, '7');
            } else if (invoiceCode == InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()) {
                bilIstSchedule.setBillInvoiceCd('8');
                invoiceCodeHideMap.put(invoiceCode, '8');
            } else {
                bilIstSchedule.setBillInvoiceCd('9');
                invoiceCodeHideMap.put(invoiceCode, '9');
            }
            bilIstScheduleRepository.restoreHideAuditPremium(bilIstSchedule.getBillInvoiceCd(),
                    bilIstSchedule.getBillIstScheduleId().getBillAccountId(), invoiceCode);
        }
        return invoiceCodeHideMap;
    }

    private double processFutures(BilAccount bilAccount, BilCashDsp bilCashDsp, String policyId,
            double invoiceCommissionPersent, String typeSpace, double originalCashAmount, CashApply cashApply,
            BilCashReceipt bilCashReceipt, BilPolicyTerm bilPolicyTerm, boolean creditReceipt, char cashOverpayInd) {

        if (cashOverpayInd != CashOverPayIndicators.SUSPEND.getValue()) {
            if (cashApply.getPaymentPriorityInd() == CHAR_S) {
                originalCashAmount = checkFutureChargeOnly(bilAccount, bilCashDsp, originalCashAmount, cashApply,
                        bilPolicyTerm, bilCashReceipt);
            }
            Double futuresAmount = bilIstScheduleRepository.fetchFuturesAmounts(
                    bilCashDsp.getBilCashDspId().getAccountId(), FUTURE_INVOICE_CODES, policyId, BLANK_STRING,
                    bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, DECIMAL_ZERO,
                    bilCashDsp.getStatementDetailTypeCd(), typeSpace);

            if (futuresAmount == null || futuresAmount == 0) {
                originalCashAmount = checkFutureChargeOnly(bilAccount, bilCashDsp, originalCashAmount, cashApply,
                        bilPolicyTerm, bilCashReceipt);
            } else if (originalCashAmount >= futuresAmount) {
                originalCashAmount = openAllFuturesCsr(bilCashDsp, invoiceCommissionPersent, BLANK_STRING, typeSpace,
                        cashApply, bilPolicyTerm, originalCashAmount, bilCashReceipt, bilAccount, creditReceipt);

                originalCashAmount = checkFutureChargeOnly(bilAccount, bilCashDsp, originalCashAmount, cashApply,
                        bilPolicyTerm, bilCashReceipt);
                cashApply.setApplyReference("AD");
                return originalCashAmount;
            }

            if (cashOverpayInd == CashOverPayIndicators.SPREAD_OVER_INSTALLMENTS.getValue()) {
                if (originalCashAmount != DECIMAL_ZERO) {
                    originalCashAmount = spreadFutures(bilCashDsp, bilPolicyTerm, invoiceCommissionPersent, typeSpace,
                            originalCashAmount, DateRoutine.defaultDateTime(), cashApply, bilCashReceipt, bilAccount,
                            creditReceipt);
                }

            } else if (cashOverpayInd == CashOverPayIndicators.SATISFY_OLDEST_DUE_FIRST.getValue()
                    && originalCashAmount != DECIMAL_ZERO) {
                originalCashAmount = catchupFutures(false, bilCashDsp, bilPolicyTerm, invoiceCommissionPersent,
                        typeSpace, originalCashAmount, DateRoutine.defaultDateTime(), cashApply, bilCashReceipt,
                        bilAccount, creditReceipt);
            }
            if (originalCashAmount > DECIMAL_ZERO) {
                originalCashAmount = checkFutureChargeOnly(bilAccount, bilCashDsp, originalCashAmount, cashApply,
                        bilPolicyTerm, bilCashReceipt);
            }
        }
        cashApply.setApplyReference("AD");
        return originalCashAmount;
    }

    private double checkFutureChargeOnly(BilAccount bilAccount, BilCashDsp bilCashDsp, double originalCashAmount,
            CashApply cashApply, BilPolicyTerm bilPolicyTerm, BilCashReceipt bilCashReceipt) {
        if (bilCashDsp.getPolicyNumber().trim().compareTo(BLANK_STRING) > SHORT_ZERO
                && !bilAccount.getBillTypeCd().trim().equals(AccountTypeCode.SINGLE_POLICY.getValue())
                || bilCashDsp.getPayableItemCd().trim().compareTo(BLANK_STRING) > SHORT_ZERO) {
            return originalCashAmount;
        }
        originalCashAmount = processFutureCharge(bilAccount, bilCashDsp, originalCashAmount,
                ChargeTypeCodes.PENALTY_CHARGE.getValue(), cashApply, bilPolicyTerm, bilCashReceipt);
        if (originalCashAmount != DECIMAL_ZERO) {
            originalCashAmount = processFutureCharge(bilAccount, bilCashDsp, originalCashAmount,
                    ChargeTypeCodes.LATE_CHARGE.getValue(), cashApply, bilPolicyTerm, bilCashReceipt);
            if (originalCashAmount != DECIMAL_ZERO) {
                originalCashAmount = processFutureCharge(bilAccount, bilCashDsp, originalCashAmount,
                        ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue(), cashApply, bilPolicyTerm, bilCashReceipt);
            }
        }

        return originalCashAmount;
    }

    private double openAllFuturesCsr(BilCashDsp bilCashDsp, double invoiceCommissionPersent, String policySpace,
            String typeSpace, CashApply cashApply, BilPolicyTerm bilPolicyTerm, double originalCashAmount,
            BilCashReceipt bilCashReceipt, BilAccount bilAccount, boolean creditReceipt) {

        List<BilIstSchedule> bilIstScheduleList = null;
        String prevPolId = BLANK_STRING;
        ZonedDateTime prevPolEffDt = null;

        if (cashApply.getCreditPolicyId().trim().isEmpty()) {
            bilIstScheduleList = bilIstScheduleRepository.fetchAllFuturesAmounts(
                    bilCashDsp.getBilCashDspId().getAccountId(), FUTURE_INVOICE_CODES,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), policySpace, bilCashDsp.getPayableItemCd(),
                    typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(),
                    typeSpace);
        } else {
            bilIstScheduleList = bilIstScheduleRepository.findPaymentFutureRowsByPolicy(
                    bilCashDsp.getBilCashDspId().getAccountId(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    policySpace, bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO,
                    bilCashDsp.getStatementDetailTypeCd(), typeSpace, FUTURE_INVOICE_CODES,
                    BilPolicyStatusCode.QUOTE.getValue());
        }
        cashApply.setApplyReference("AD");

        if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
            for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                Object[] payAllFutures = processPayAllFutures(bilIstSchedule, bilCashDsp, originalCashAmount, cashApply,
                        bilCashReceipt, bilAccount, bilPolicyTerm, creditReceipt, prevPolId, prevPolEffDt);
                originalCashAmount = (double) payAllFutures[0];
                prevPolId = (String) payAllFutures[1];
                prevPolEffDt = (ZonedDateTime) payAllFutures[2];
                if (originalCashAmount == DECIMAL_ZERO) {
                    break;
                }

            }

        }

        return originalCashAmount;
    }

    private Object[] processPayAllFutures(BilIstSchedule bilIstSchedule, BilCashDsp bilCashDsp,
            double originalCashAmount, CashApply cashApply, BilCashReceipt bilCashReceipt, BilAccount bilAccount,
            BilPolicyTerm bilPolicyTerm, boolean creditReceipt, String prevPolId, ZonedDateTime prevPolEffDt) {

        double dispositionAmount;
        double premiumPaidAmount;
        double creditPaidAmount;
        double openGrossAmount = DECIMAL_ZERO;
        double openCashPaid = DECIMAL_ZERO;
        double openFutureAmount = DECIMAL_ZERO;

        Double openAmount = bilIstSchedule.getBisCncFutAmt() + bilIstSchedule.getBisCreFutAmt()
                + bilIstSchedule.getBisCncOpnAmt() + bilIstSchedule.getBillPrmIstAmt()
                + bilIstSchedule.getBisCreOpnAmt();
        Double paidAmount = bilIstSchedule.getBisWroPrmAmt() + bilIstSchedule.getBisPrmPaidAmt()
                + bilIstSchedule.getBisCrePaidAmt();
        openAmount = openAmount - paidAmount;

        if (creditReceipt) {
            creditPaidAmount = openAmount + bilIstSchedule.getBisCrePaidAmt();
            premiumPaidAmount = bilIstSchedule.getBisPrmPaidAmt();
        } else {
            creditPaidAmount = bilIstSchedule.getBisCrePaidAmt();
            premiumPaidAmount = openAmount + bilIstSchedule.getBisPrmPaidAmt();
        }
        openCashPaid = openCashPaid + openAmount;
        openFutureAmount = openFutureAmount + openAmount;
        originalCashAmount = originalCashAmount - openAmount;
        dispositionAmount = openAmount;
        if (dispositionAmount != DECIMAL_ZERO) {
            Object[] previousObject = updatePremAndCash(bilCashDsp, bilIstSchedule, dispositionAmount, creditPaidAmount,
                    premiumPaidAmount, openGrossAmount, openCashPaid, openFutureAmount, bilCashReceipt, cashApply,
                    bilAccount, bilPolicyTerm, prevPolId, prevPolEffDt);
            prevPolId = (String) previousObject[0];
            prevPolEffDt = (ZonedDateTime) previousObject[1];
        }

        return new Object[] { originalCashAmount, prevPolId, prevPolEffDt };
    }

    private double spreadFutures(BilCashDsp bilCashDsp, BilPolicyTerm bilPolicyTerm, double invoiceCommissionPersent,
            String typeSpace, double originalCashAmount, ZonedDateTime billIstDueDate, CashApply cashApply,
            BilCashReceipt bilCashReceipt, BilAccount bilAccount, boolean creditReceipt) {

        Integer spreadNo = bilIstScheduleRepository.spreadCountByAdjustDate(bilCashDsp.getBilCashDspId().getAccountId(),
                FUTURE_INVOICE_CODES, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING,
                bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO,
                bilCashDsp.getStatementDetailTypeCd(), typeSpace);
        if (spreadNo == null || spreadNo == SHORT_ZERO) {
            return originalCashAmount;
        }
        ZonedDateTime adjustDate = readFutureMinAdjustDateRow(bilCashDsp, bilPolicyTerm, typeSpace, billIstDueDate,
                invoiceCommissionPersent);
        ZonedDateTime systemDate = getCatchupSystemDate(adjustDate, bilCashDsp, bilPolicyTerm, invoiceCommissionPersent,
                typeSpace, billIstDueDate);

        double product = originalCashAmount / spreadNo;
        int roundSepRec = (int) product;
        double fraction = product - roundSepRec;
        BigDecimal fourDecimalFraction = BigDecimal.valueOf(fraction).setScale(4, RoundingMode.UP);
        BigDecimal twoDecimalFraction = BigDecimal.valueOf(fraction).setScale(2, RoundingMode.DOWN);
        BigDecimal twoLastDecimalFraction = fourDecimalFraction.subtract(twoDecimalFraction);
        if (twoLastDecimalFraction.movePointRight(4).doubleValue() > DECIMAL_ZERO) {
            fraction = fourDecimalFraction.doubleValue() + 0.01;
            twoDecimalFraction = BigDecimal.valueOf(fraction).setScale(2, RoundingMode.DOWN);
            product = roundSepRec + twoDecimalFraction.doubleValue();
        }

        double amountSpread = product;
        double amountSpreadHold = product;
        boolean endOfScheduleSw = false;

        while (!(endOfScheduleSw || originalCashAmount == DECIMAL_ZERO)) {

            Object[] spreadObject = spreadOverAdjustDate(amountSpread, originalCashAmount, bilCashDsp, bilPolicyTerm,
                    typeSpace, billIstDueDate, invoiceCommissionPersent, cashApply, bilCashReceipt, bilAccount,
                    creditReceipt, adjustDate, systemDate);

            originalCashAmount = (double) spreadObject[0];
            adjustDate = (ZonedDateTime) spreadObject[1];

            if (originalCashAmount != DECIMAL_ZERO) {

                ZonedDateTime preAdjustDate = getSpreadAdjustDate(adjustDate, bilCashDsp, bilPolicyTerm,
                        invoiceCommissionPersent, typeSpace);

                if (preAdjustDate == null) {
                    endOfScheduleSw = true;
                } else {
                    adjustDate = preAdjustDate;
                    systemDate = getCatchupSystemDate(adjustDate, bilCashDsp, bilPolicyTerm, invoiceCommissionPersent,
                            typeSpace, billIstDueDate);
                    amountSpread = amountSpreadHold;
                }

            }
        }

        return originalCashAmount;
    }

    private Object[] spreadOverAdjustDate(double amountSpread, double originalCashAmount, BilCashDsp bilCashDsp,
            BilPolicyTerm bilPolicyTerm, String typeSpace, ZonedDateTime billIstDueDate,
            double invoiceCommissionPersent, CashApply cashApply, BilCashReceipt bilCashReceipt, BilAccount bilAccount,
            boolean creditReceipt, ZonedDateTime adjustDate, ZonedDateTime systemDate) {

        Double adjustAmount = readFutureOpensAdjustAmounts(bilCashDsp, bilPolicyTerm, typeSpace, billIstDueDate,
                invoiceCommissionPersent, adjustDate, systemDate);

        if (amountSpread > originalCashAmount) {
            amountSpread = originalCashAmount;
        }

        if (amountSpread > adjustAmount) {
            originalCashAmount = spreadFutureFullPayment(adjustDate, systemDate, bilCashDsp, bilPolicyTerm, typeSpace,
                    invoiceCommissionPersent, billIstDueDate, originalCashAmount, cashApply, bilCashReceipt, bilAccount,
                    creditReceipt, amountSpread);
        } else {
            double originalPayment = amountSpread;
            double applicationAmt = amountSpread;
            originalCashAmount = calculateLowSpreadPriority(bilCashDsp, originalCashAmount, adjustDate, systemDate,
                    typeSpace, invoiceCommissionPersent, bilPolicyTerm, cashApply, bilCashReceipt, bilAccount,
                    billIstDueDate, creditReceipt, amountSpread, true, originalPayment, applicationAmt, true);
        }

        return new Object[] { originalCashAmount, adjustDate };
    }

    private Double readFutureOpensAdjustAmounts(BilCashDsp bilCashDsp, BilPolicyTerm bilPolicyTerm, String typeSpace,
            ZonedDateTime billIstDueDate, double invoiceCommissionPersent, ZonedDateTime adjustDate,
            ZonedDateTime systemDate) {
        return bilIstScheduleRepository.futureOpensAdjustAmounts(bilCashDsp.getBilCashDspId().getAccountId(),
                FUTURE_INVOICE_CODES, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING,
                bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, adjustDate,
                systemDate, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace, billIstDueDate,
                DateRoutine.defaultDateTime(), billIstDueDate);
    }

    private double catchupFutures(boolean futrDueDtDir, BilCashDsp bilCashDsp, BilPolicyTerm bilPolicyTerm,
            double invoiceCommissionPersent, String typeSpace, double originalCashAmount, ZonedDateTime billIstDueDate,
            CashApply cashApply, BilCashReceipt bilCashReceipt, BilAccount bilAccount, boolean creditReceipt) {

        ZonedDateTime adjustDate = null;
        if (!futrDueDtDir) {
            adjustDate = readFutureMinAdjustDateRow(bilCashDsp, bilPolicyTerm, typeSpace, billIstDueDate,
                    invoiceCommissionPersent);
            if (adjustDate == null) {
                return originalCashAmount;
            }
        } else {
            adjustDate = readFutureIEAdjustDateRow(bilCashDsp, bilPolicyTerm, typeSpace, billIstDueDate,
                    invoiceCommissionPersent);
            if (adjustDate == null) {
                adjustDate = bilCashDsp.getAdjustmentDueDate();
            }
        }

        ZonedDateTime systemDate = getCatchupSystemDate(adjustDate, bilCashDsp, bilPolicyTerm, invoiceCommissionPersent,
                typeSpace, billIstDueDate);

        if (systemDate == null && futrDueDtDir) {
            return originalCashAmount;
        }

        ZonedDateTime holdSystemDate = systemDate;
        ZonedDateTime holdAdjustDate = adjustDate;
        boolean endOfScheduleSw = false;

        while (!(endOfScheduleSw || originalCashAmount == DECIMAL_ZERO)) {

            Object[] catchupFutureData = catchupFutureAdjustDates(adjustDate, systemDate, bilCashDsp, bilPolicyTerm,
                    typeSpace, invoiceCommissionPersent, billIstDueDate, originalCashAmount, cashApply, bilCashReceipt,
                    bilAccount, creditReceipt, endOfScheduleSw, holdAdjustDate, holdSystemDate);

            originalCashAmount = (double) catchupFutureData[0];
            adjustDate = (ZonedDateTime) catchupFutureData[1];
            endOfScheduleSw = (boolean) catchupFutureData[2];

            if (originalCashAmount != DECIMAL_ZERO) {

                ZonedDateTime preAdjustDate = getCatchupAdjustDate(adjustDate, bilCashDsp, bilPolicyTerm,
                        invoiceCommissionPersent, typeSpace, billIstDueDate);

                if (preAdjustDate == null || adjustDate != preAdjustDate && futrDueDtDir) {
                    endOfScheduleSw = true;
                } else {
                    adjustDate = preAdjustDate;
                    systemDate = getCatchupSystemDate(adjustDate, bilCashDsp, bilPolicyTerm, invoiceCommissionPersent,
                            typeSpace, billIstDueDate);
                }

            }
        }

        return originalCashAmount;
    }

    private ZonedDateTime readFutureMinAdjustDateRow(BilCashDsp bilCashDsp, BilPolicyTerm bilPolicyTerm,
            String typeSpace, ZonedDateTime billIstDueDate, double invoiceCommissionPersent) {

        return bilIstScheduleRepository.futureMinAdjustDateRow(bilCashDsp.getBilCashDspId().getAccountId(),
                FUTURE_INVOICE_CODES, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING,
                bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO,
                bilCashDsp.getStatementDetailTypeCd(), typeSpace, billIstDueDate, DateRoutine.defaultDateTime(),
                billIstDueDate);
    }

    private ZonedDateTime readFutureIEAdjustDateRow(BilCashDsp bilCashDsp, BilPolicyTerm bilPolicyTerm,
            String typeSpace, ZonedDateTime billIstDueDate, double invoiceCommissionPersent) {

        return bilIstScheduleRepository.futureIEAdjustDateRow(bilCashDsp.getBilCashDspId().getAccountId(),
                FUTURE_INVOICE_CODES, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING,
                bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO,
                bilCashDsp.getStatementDetailTypeCd(), typeSpace, billIstDueDate);
    }

    private ZonedDateTime getCatchupAdjustDate(ZonedDateTime adjustDate, BilCashDsp bilCashDsp,
            BilPolicyTerm bilPolicyTerm, double invoiceCommissionPersent, String typeSpace,
            ZonedDateTime billIstDueDate) {

        return bilIstScheduleRepository.catchupfutureMinAdjustDate(adjustDate,
                bilCashDsp.getBilCashDspId().getAccountId(), FUTURE_INVOICE_CODES,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                billIstDueDate, DateRoutine.defaultDateTime(), billIstDueDate);

    }

    private ZonedDateTime getSpreadAdjustDate(ZonedDateTime adjustDate, BilCashDsp bilCashDsp,
            BilPolicyTerm bilPolicyTerm, double invoiceCommissionPersent, String typeSpace) {

        return bilIstScheduleRepository.spreadfutureMinAdjustDate(adjustDate,
                bilCashDsp.getBilCashDspId().getAccountId(), FUTURE_INVOICE_CODES,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace);

    }

    private ZonedDateTime getCatchupSystemDate(ZonedDateTime adjustDate, BilCashDsp bilCashDsp,
            BilPolicyTerm bilPolicyTerm, double invoiceCommissionPersent, String typeSpace,
            ZonedDateTime billIstDueDate) {

        return bilIstScheduleRepository.futureSystemDueDateRange(bilCashDsp.getBilCashDspId().getAccountId(),
                FUTURE_INVOICE_CODES, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING,
                bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO,
                bilCashDsp.getStatementDetailTypeCd(), typeSpace, adjustDate, billIstDueDate,
                DateRoutine.defaultDateTime(), billIstDueDate);
    }

    private Object[] catchupFutureAdjustDates(ZonedDateTime adjustDate, ZonedDateTime systemDate, BilCashDsp bilCashDsp,
            BilPolicyTerm bilPolicyTerm, String typeSpace, double invoiceCommissionPersent,
            ZonedDateTime billIstDueDate, double originalCashAmount, CashApply cashApply, BilCashReceipt bilCashReceipt,
            BilAccount bilAccount, boolean creditReceipt, boolean endOfScheduleSw, ZonedDateTime holdAdjustDate,
            ZonedDateTime holdSystemDate) {

        Double adjustAmount = readFutureOpensAdjustAmounts(bilCashDsp, bilPolicyTerm, typeSpace, billIstDueDate,
                invoiceCommissionPersent, adjustDate, systemDate);

        if (originalCashAmount >= adjustAmount) {
            originalCashAmount = catchupFutureFullPayment(adjustDate, systemDate, bilCashDsp, bilPolicyTerm, typeSpace,
                    invoiceCommissionPersent, billIstDueDate, originalCashAmount, cashApply, bilCashReceipt, bilAccount,
                    creditReceipt);

        } else {
            boolean secondTermExcludeSw = determineExcludeRows(bilCashDsp.getBilCashDspId().getAccountId(), adjustDate,
                    systemDate, bilCashDsp.getPayableItemCd(), typeSpace, false, true);

            Object[] lowCatchupPriority = calculateLowCatchupPriority(bilCashDsp, originalCashAmount, adjustDate,
                    systemDate, typeSpace, invoiceCommissionPersent, bilPolicyTerm, cashApply, bilCashReceipt,
                    bilAccount, billIstDueDate, creditReceipt);
            originalCashAmount = (double) lowCatchupPriority[0];
            endOfScheduleSw = (boolean) lowCatchupPriority[1];

            if (secondTermExcludeSw) {
                unhideMultipleTermRows(bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate, true);
                if (originalCashAmount > DECIMAL_ZERO) {

                    ZonedDateTime dueDate = bilIstScheduleRepository.fetchCatchupMinAdjustDateRow(
                            bilCashDsp.getBilCashDspId().getAccountId(), FUTURE_INVOICE_CODES,
                            bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING,
                            bilCashDsp.getPayableItemCd(), typeSpace, holdAdjustDate, holdSystemDate,
                            invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                            billIstDueDate, DateRoutine.defaultDateTime(), billIstDueDate);

                    if (dueDate != null) {
                        adjustDate = dueDate;
                        lowCatchupPriority = calculateLowCatchupPriority(bilCashDsp, originalCashAmount, adjustDate,
                                systemDate, typeSpace, invoiceCommissionPersent, bilPolicyTerm, cashApply,
                                bilCashReceipt, bilAccount, billIstDueDate, creditReceipt);
                        originalCashAmount = (double) lowCatchupPriority[0];
                        endOfScheduleSw = (boolean) lowCatchupPriority[1];
                    }
                }

            }
        }
        return new Object[] { originalCashAmount, adjustDate, endOfScheduleSw };
    }

    private double catchupFutureFullPayment(ZonedDateTime adjustDate, ZonedDateTime systemDate, BilCashDsp bilCashDsp,
            BilPolicyTerm bilPolicyTerm, String typeSpace, double invoiceCommissionPersent,
            ZonedDateTime billIstDueDate, double originalCashAmount, CashApply cashApply, BilCashReceipt bilCashReceipt,
            BilAccount bilAccount, boolean creditReceipt) {

        List<BilIstSchedule> bilIstScheduleList = null;
        String prevPolId = BLANK_STRING;
        ZonedDateTime prevPolEffDt = null;

        if (cashApply.getCreditPolicyId().trim().isEmpty()) {
            bilIstScheduleList = bilIstScheduleRepository.futureFullPaymentRows(
                    bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                    typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                    FUTURE_INVOICE_CODES, DateRoutine.defaultDateTime(), billIstDueDate, systemDate, billIstDueDate);
        } else {
            bilIstScheduleList = bilIstScheduleRepository.futureFullPaymentRowsByPolicy(
                    bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                    typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                    FUTURE_INVOICE_CODES, DateRoutine.defaultDateTime(), BilPolicyStatusCode.QUOTE.getValue(),
                    billIstDueDate, systemDate, billIstDueDate);
        }
        if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
            for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                Object[] fullPayCatchup = processFullPayCatchup(bilIstSchedule, bilCashDsp, bilPolicyTerm,
                        originalCashAmount, cashApply, bilCashReceipt, bilAccount, creditReceipt, prevPolId,
                        prevPolEffDt);

                originalCashAmount = (double) fullPayCatchup[0];
                prevPolId = (String) fullPayCatchup[1];
                prevPolEffDt = (ZonedDateTime) fullPayCatchup[2];

                if (originalCashAmount == DECIMAL_ZERO) {
                    break;
                }
            }

        }
        return originalCashAmount;
    }

    private double spreadFutureFullPayment(ZonedDateTime adjustDate, ZonedDateTime systemDate, BilCashDsp bilCashDsp,
            BilPolicyTerm bilPolicyTerm, String typeSpace, double invoiceCommissionPersent,
            ZonedDateTime billIstDueDate, double originalCashAmount, CashApply cashApply, BilCashReceipt bilCashReceipt,
            BilAccount bilAccount, boolean creditReceipt, double amountSpread) {

        List<BilIstSchedule> bilIstScheduleList = null;
        String prevPolId = BLANK_STRING;
        ZonedDateTime prevPolEffDt = null;

        if (cashApply.getCreditPolicyId().trim().isEmpty()) {
            bilIstScheduleList = bilIstScheduleRepository.futureFullPaymentRows(
                    bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                    typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                    FUTURE_INVOICE_CODES, DateRoutine.defaultDateTime(), billIstDueDate, systemDate, billIstDueDate);
        } else {
            bilIstScheduleList = bilIstScheduleRepository.futureFullPaymentRowsByPolicy(
                    bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                    typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                    FUTURE_INVOICE_CODES, DateRoutine.defaultDateTime(), BilPolicyStatusCode.QUOTE.getValue(),
                    billIstDueDate, systemDate, billIstDueDate);
        }
        if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
            for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                Object[] spreadAdjustAmounts = processFullPaySpread(bilIstSchedule, bilCashDsp, bilPolicyTerm,
                        originalCashAmount, cashApply, bilCashReceipt, bilAccount, creditReceipt, amountSpread,
                        prevPolId, prevPolEffDt);

                originalCashAmount = (double) spreadAdjustAmounts[0];
                amountSpread = (double) spreadAdjustAmounts[1];
                prevPolId = (String) spreadAdjustAmounts[2];
                prevPolEffDt = (ZonedDateTime) spreadAdjustAmounts[3];

                if (originalCashAmount == DECIMAL_ZERO || amountSpread == DECIMAL_ZERO) {
                    break;
                }
            }

        }
        return originalCashAmount;
    }

    private Object[] processFullPaySpread(BilIstSchedule bilIstSchedule, BilCashDsp bilCashDsp,
            BilPolicyTerm bilPolicyTerm, double originalCashAmount, CashApply cashApply, BilCashReceipt bilCashReceipt,
            BilAccount bilAccount, boolean creditReceipt, double amountSpread, String prevPolId,
            ZonedDateTime prevPolEffDt) {

        double dispositionAmount;
        double premiumPaidAmount;
        double creditPaidAmount;
        double openGrossAmount = DECIMAL_ZERO;
        double openCashPaid = DECIMAL_ZERO;
        double openFutureAmount = DECIMAL_ZERO;

        Double openAmount = bilIstSchedule.getBisCncFutAmt() + bilIstSchedule.getBisCreFutAmt()
                + bilIstSchedule.getBisCncOpnAmt() + bilIstSchedule.getBillPrmIstAmt()
                + bilIstSchedule.getBisCreOpnAmt();
        Double paidAmount = bilIstSchedule.getBisWroPrmAmt() + bilIstSchedule.getBisPrmPaidAmt()
                + bilIstSchedule.getBisCrePaidAmt();

        openAmount = openAmount - paidAmount;
        if (openAmount > amountSpread) {
            openAmount = amountSpread;
            amountSpread = DECIMAL_ZERO;
        } else {
            amountSpread = amountSpread - openAmount;
        }

        if (openAmount > originalCashAmount) {
            openAmount = originalCashAmount;
            originalCashAmount = DECIMAL_ZERO;
        }

        if (creditReceipt) {
            creditPaidAmount = openAmount + bilIstSchedule.getBisCrePaidAmt();
            premiumPaidAmount = bilIstSchedule.getBisPrmPaidAmt();
        } else {
            creditPaidAmount = bilIstSchedule.getBisCrePaidAmt();
            premiumPaidAmount = openAmount + bilIstSchedule.getBisPrmPaidAmt();
        }
        openCashPaid = openCashPaid + openAmount;
        openFutureAmount = openFutureAmount + openAmount;

        dispositionAmount = openAmount;

        if (originalCashAmount != DECIMAL_ZERO) {
            originalCashAmount = originalCashAmount - openAmount;
        }
        if (dispositionAmount != DECIMAL_ZERO) {
            Object[] previousObject = updatePremAndCash(bilCashDsp, bilIstSchedule, dispositionAmount, creditPaidAmount,
                    premiumPaidAmount, openGrossAmount, openCashPaid, openFutureAmount, bilCashReceipt, cashApply,
                    bilAccount, bilPolicyTerm, prevPolId, prevPolEffDt);
            prevPolId = (String) previousObject[0];
            prevPolEffDt = (ZonedDateTime) previousObject[1];
        }

        return new Object[] { originalCashAmount, amountSpread, prevPolId, prevPolEffDt };
    }

    private Object[] processFullPayCatchup(BilIstSchedule bilIstSchedule, BilCashDsp bilCashDsp,
            BilPolicyTerm bilPolicyTerm, double originalCashAmount, CashApply cashApply, BilCashReceipt bilCashReceipt,
            BilAccount bilAccount, boolean creditReceipt, String prevPolId, ZonedDateTime prevPolEffDt) {

        double dispositionAmount;
        double premiumPaidAmount;
        double creditPaidAmount;
        double openGrossAmount = DECIMAL_ZERO;
        double openCashPaid = DECIMAL_ZERO;
        double openFutureAmount = DECIMAL_ZERO;

        Double openAmount = bilIstSchedule.getBisCncFutAmt() + bilIstSchedule.getBisCreFutAmt()
                + bilIstSchedule.getBisCncOpnAmt() + bilIstSchedule.getBillPrmIstAmt()
                + bilIstSchedule.getBisCreOpnAmt();
        Double paidAmount = bilIstSchedule.getBisWroPrmAmt() + bilIstSchedule.getBisPrmPaidAmt()
                + bilIstSchedule.getBisCrePaidAmt();

        openAmount = openAmount - paidAmount;

        if (openAmount > originalCashAmount) {
            openAmount = originalCashAmount;
            originalCashAmount = DECIMAL_ZERO;
        } else {
            originalCashAmount = originalCashAmount - openAmount;
        }

        if (creditReceipt) {
            creditPaidAmount = openAmount + bilIstSchedule.getBisCrePaidAmt();
            premiumPaidAmount = bilIstSchedule.getBisPrmPaidAmt();
        } else {
            creditPaidAmount = bilIstSchedule.getBisCrePaidAmt();
            premiumPaidAmount = openAmount + bilIstSchedule.getBisPrmPaidAmt();
        }
        openCashPaid = openCashPaid + openAmount;
        openFutureAmount = openFutureAmount + openAmount;

        dispositionAmount = openAmount;
        if (dispositionAmount != DECIMAL_ZERO) {
            Object[] previousObject = updatePremAndCash(bilCashDsp, bilIstSchedule, dispositionAmount, creditPaidAmount,
                    premiumPaidAmount, openGrossAmount, openCashPaid, openFutureAmount, bilCashReceipt, cashApply,
                    bilAccount, bilPolicyTerm, prevPolId, prevPolEffDt);
            prevPolId = (String) previousObject[0];
            prevPolEffDt = (ZonedDateTime) previousObject[1];
        }

        return new Object[] { originalCashAmount, prevPolId, prevPolEffDt };
    }

    private Object[] calculateLowCatchupPriority(BilCashDsp bilCashDsp, double originalCashAmount,
            ZonedDateTime adjustDate, ZonedDateTime systemDate, String typeSpace, double invoiceCommissionPersent,
            BilPolicyTerm bilPolicyTerm, CashApply cashApply, BilCashReceipt bilCashReceipt, BilAccount bilAccount,
            ZonedDateTime billIstDueDate, boolean creditReceipt) {

        List<Character> priorityIndicatorList = bilIstScheduleRepository.futurePaymentPriorityRows(
                bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                FUTURE_INVOICE_CODES, billIstDueDate, DateRoutine.defaultDateTime(), billIstDueDate);

        if (priorityIndicatorList != null && !priorityIndicatorList.isEmpty()) {
            for (Character priorityIndicator : priorityIndicatorList) {

                Double priorityAmount = bilIstScheduleRepository.futurePriorityAmount(
                        bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                        typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(),
                        typeSpace, FUTURE_INVOICE_CODES, priorityIndicator, billIstDueDate,
                        DateRoutine.defaultDateTime(), billIstDueDate);

                if (priorityAmount == null) {
                    priorityAmount = DECIMAL_ZERO;
                }

                if (originalCashAmount >= priorityAmount) {

                    originalCashAmount = processPartialFullCatchup(bilCashDsp, originalCashAmount, adjustDate,
                            systemDate, typeSpace, invoiceCommissionPersent, bilPolicyTerm, cashApply, bilCashReceipt,
                            bilAccount, billIstDueDate, creditReceipt, priorityIndicator);

                    if (originalCashAmount == DECIMAL_ZERO) {
                        break;
                    }
                } else {
                    if (originalCashAmount != DECIMAL_ZERO) {
                        originalCashAmount = processFullPayCatchupRows(bilCashDsp, originalCashAmount, adjustDate,
                                systemDate, typeSpace, invoiceCommissionPersent, bilPolicyTerm, true, priorityIndicator,
                                cashApply, bilCashReceipt, bilAccount, billIstDueDate, creditReceipt);
                    }
                }
            }
        } else {
            return new Object[] { originalCashAmount, true };
        }

        return new Object[] { originalCashAmount, false };
    }

    private double calculateLowSpreadPriority(BilCashDsp bilCashDsp, double originalCashAmount,
            ZonedDateTime adjustDate, ZonedDateTime systemDate, String typeSpace, double invoiceCommissionPersent,
            BilPolicyTerm bilPolicyTerm, CashApply cashApply, BilCashReceipt bilCashReceipt, BilAccount bilAccount,
            ZonedDateTime billIstDueDate, boolean creditReceipt, double amountSpread, boolean futureSwitch,
            double originalPayment, double applicationAmt, boolean presentOrFuture) {

        List<Character> invoiceCodeList = null;
        String prevPolId = BLANK_STRING;
        ZonedDateTime prevPolEffDt = null;

        List<Character> priorityIndicatorList = bilIstScheduleRepository.futurePaymentPriorityRows(
                bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                FUTURE_INVOICE_CODES, billIstDueDate, DateRoutine.defaultDateTime(), billIstDueDate);

        if (priorityIndicatorList != null && !priorityIndicatorList.isEmpty()) {
            for (Character priorityIndicator : priorityIndicatorList) {

                Double priorityAmount = bilIstScheduleRepository.futurePriorityAmount(
                        bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                        typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(),
                        typeSpace, FUTURE_INVOICE_CODES, priorityIndicator, billIstDueDate,
                        DateRoutine.defaultDateTime(), billIstDueDate);

                if (priorityAmount == null) {
                    priorityAmount = DECIMAL_ZERO;
                }

                if (originalCashAmount == DECIMAL_ZERO) {
                    break;
                }

                if (amountSpread >= priorityAmount) {

                    List<BilIstSchedule> bilIstScheduleList = processPartialFullRows(bilCashDsp, adjustDate, systemDate,
                            typeSpace, invoiceCommissionPersent, bilPolicyTerm, cashApply, billIstDueDate,
                            priorityIndicator);
                    if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
                        for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                            Object[] partialPaySpread = processPartialFullPaySpread(bilIstSchedule, bilCashDsp,
                                    originalCashAmount, cashApply, bilCashReceipt, bilAccount, bilPolicyTerm,
                                    creditReceipt, amountSpread, prevPolId, prevPolEffDt);
                            originalCashAmount = (double) partialPaySpread[0];
                            prevPolId = (String) partialPaySpread[1];
                            prevPolEffDt = (ZonedDateTime) partialPaySpread[2];

                            if (originalCashAmount == DECIMAL_ZERO) {
                                break;
                            }
                        }
                    }

                } else {
                    if (originalCashAmount != DECIMAL_ZERO) {
                        List<BilIstSchedule> bilIstScheduleList = processPartialFullRows(bilCashDsp, adjustDate,
                                systemDate, typeSpace, invoiceCommissionPersent, bilPolicyTerm, cashApply,
                                billIstDueDate, priorityIndicator);
                        if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
                            if (!cashApply.getCreditPolicyId().trim().isEmpty()) {
                                for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                                    Object[] partialPaySpread = processPartialFullPaySpread(bilIstSchedule, bilCashDsp,
                                            originalCashAmount, cashApply, bilCashReceipt, bilAccount, bilPolicyTerm,
                                            creditReceipt, amountSpread, prevPolId, prevPolEffDt);
                                    originalCashAmount = (double) partialPaySpread[0];
                                    prevPolId = (String) partialPaySpread[1];
                                    prevPolEffDt = (ZonedDateTime) partialPaySpread[2];

                                    if (originalCashAmount == DECIMAL_ZERO) {
                                        break;
                                    }
                                }
                            } else {
                                for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                                    double istDuePaidAmt = DECIMAL_ZERO;
                                    if (futureSwitch) {
                                        invoiceCodeList = FUTURE_INVOICE_CODES;
                                    } else {
                                        invoiceCodeList = CURRENT_INVOICE_CODES;
                                    }
                                    Double itemAmount = bilIstScheduleRepository.getPriorityAmountByPolicyId(
                                            bilIstSchedule.getBillIstScheduleId().getBillAccountId(),
                                            bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                                            bilIstSchedule.getBillIstScheduleId().getBillSeqNbr(),
                                            bilIstSchedule.getBillSysDueDt(), bilIstSchedule.getBillAdjDueDt(),
                                            bilIstSchedule.getBillIstDueDt(), bilIstSchedule.getBillPblItemCd(),
                                            bilIstSchedule.getBillPayPtyCd(), invoiceCodeList);
                                    Double openAmount = bilIstSchedule.getBisCncFutAmt()
                                            + bilIstSchedule.getBisCreFutAmt() + bilIstSchedule.getBisCncOpnAmt()
                                            + bilIstSchedule.getBillPrmIstAmt() + bilIstSchedule.getBisCreOpnAmt();
                                    Double paidAmount = bilIstSchedule.getBisWroPrmAmt()
                                            + bilIstSchedule.getBisPrmPaidAmt() + bilIstSchedule.getBisCrePaidAmt();

                                    openAmount = openAmount - paidAmount;
                                    double quotient = itemAmount / priorityAmount;
                                    if (quotient <= DECIMAL_ZERO) {
                                        continue;
                                    }

                                    Object[] partialAmounts = applyCash(bilIstSchedule, quotient, originalPayment,
                                            presentOrFuture, cashApply, creditReceipt, bilCashDsp, bilCashReceipt,
                                            bilAccount, bilPolicyTerm, applicationAmt, originalCashAmount,
                                            istDuePaidAmt, prevPolId, prevPolEffDt);

                                    originalCashAmount = (double) partialAmounts[0];
                                    applicationAmt = (double) partialAmounts[1];
                                    prevPolId = (String) partialAmounts[3];
                                    prevPolEffDt = (ZonedDateTime) partialAmounts[4];

                                    if (originalCashAmount == DECIMAL_ZERO) {
                                        break;
                                    }
                                }
                            }

                            amountSpread = applicationAmt;
                        }
                    }
                }

                if (originalCashAmount == DECIMAL_ZERO || amountSpread == DECIMAL_ZERO) {
                    break;
                }
            }
        }

        return originalCashAmount;
    }

    private double processPartialFullCatchup(BilCashDsp bilCashDsp, double originalCashAmount, ZonedDateTime adjustDate,
            ZonedDateTime systemDate, String typeSpace, double invoiceCommissionPersent, BilPolicyTerm bilPolicyTerm,
            CashApply cashApply, BilCashReceipt bilCashReceipt, BilAccount bilAccount, ZonedDateTime billIstDueDate,
            boolean creditReceipt, Character priorityIndicator) {

        String prevPolId = BLANK_STRING;
        ZonedDateTime prevPolEffDt = null;

        List<BilIstSchedule> bilIstScheduleList = processPartialFullRows(bilCashDsp, adjustDate, systemDate, typeSpace,
                invoiceCommissionPersent, bilPolicyTerm, cashApply, billIstDueDate, priorityIndicator);

        if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
            for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                Object[] partialPayCatchup = processPartialFullPayCatchup(bilIstSchedule, bilCashDsp,
                        originalCashAmount, cashApply, bilCashReceipt, bilAccount, bilPolicyTerm, creditReceipt,
                        prevPolId, prevPolEffDt);
                originalCashAmount = (double) partialPayCatchup[0];
                prevPolId = (String) partialPayCatchup[1];
                prevPolEffDt = (ZonedDateTime) partialPayCatchup[2];

                if (originalCashAmount == DECIMAL_ZERO) {
                    break;
                }
            }
        }
        return originalCashAmount;
    }

    private List<BilIstSchedule> processPartialFullRows(BilCashDsp bilCashDsp, ZonedDateTime adjustDate,
            ZonedDateTime systemDate, String typeSpace, double invoiceCommissionPersent, BilPolicyTerm bilPolicyTerm,
            CashApply cashApply, ZonedDateTime billIstDueDate, Character priorityIndicator) {

        List<BilIstSchedule> bilIstScheduleList = null;

        if (cashApply.getCreditPolicyId().isEmpty()) {
            bilIstScheduleList = bilIstScheduleRepository.futurePaymentPriorityRows(
                    bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                    typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                    priorityIndicator, FUTURE_INVOICE_CODES, DateRoutine.defaultDateTime(), billIstDueDate,
                    billIstDueDate, systemDate);
        } else {
            bilIstScheduleList = bilIstScheduleRepository.futurePaymentPriorityRowsByPolicy(
                    bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                    typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                    FUTURE_INVOICE_CODES, BilPolicyStatusCode.QUOTE.getValue(), DateRoutine.defaultDateTime(),
                    billIstDueDate, priorityIndicator, billIstDueDate, systemDate);
        }

        return bilIstScheduleList;
    }

    private Object[] processPartialFullPayCatchup(BilIstSchedule bilIstSchedule, BilCashDsp bilCashDsp,
            double originalCashAmount, CashApply cashApply, BilCashReceipt bilCashReceipt, BilAccount bilAccount,
            BilPolicyTerm bilPolicyTerm, boolean creditReceipt, String prevPolId, ZonedDateTime prevPolEffDt) {

        double dispositionAmount;
        double premiumPaidAmount;
        double creditPaidAmount;
        double openGrossAmount = DECIMAL_ZERO;
        double openCashPaid = DECIMAL_ZERO;
        double openFutureAmount = DECIMAL_ZERO;

        Double openAmount = bilIstSchedule.getBisCncFutAmt() + bilIstSchedule.getBisCreFutAmt()
                + bilIstSchedule.getBisCncOpnAmt() + bilIstSchedule.getBillPrmIstAmt()
                + bilIstSchedule.getBisCreOpnAmt();
        Double paidAmount = bilIstSchedule.getBisWroPrmAmt() + bilIstSchedule.getBisPrmPaidAmt()
                + bilIstSchedule.getBisCrePaidAmt();

        openAmount = openAmount - paidAmount;

        if (openAmount > originalCashAmount) {
            openAmount = originalCashAmount;
            originalCashAmount = DECIMAL_ZERO;
        }

        if (creditReceipt) {
            creditPaidAmount = openAmount + bilIstSchedule.getBisCrePaidAmt();
            premiumPaidAmount = bilIstSchedule.getBisPrmPaidAmt();
        } else {
            creditPaidAmount = bilIstSchedule.getBisCrePaidAmt();
            premiumPaidAmount = openAmount + bilIstSchedule.getBisPrmPaidAmt();
        }
        openCashPaid = openCashPaid + openAmount;
        openFutureAmount = openFutureAmount + openAmount;
        if (originalCashAmount != DECIMAL_ZERO) {
            originalCashAmount = originalCashAmount - openAmount;
        }
        dispositionAmount = openAmount;

        if (dispositionAmount != DECIMAL_ZERO) {
            Object[] previousObject = updatePremAndCash(bilCashDsp, bilIstSchedule, dispositionAmount, creditPaidAmount,
                    premiumPaidAmount, openGrossAmount, openCashPaid, openFutureAmount, bilCashReceipt, cashApply,
                    bilAccount, bilPolicyTerm, prevPolId, prevPolEffDt);
            prevPolId = (String) previousObject[0];
            prevPolEffDt = (ZonedDateTime) previousObject[1];
        }

        return new Object[] { originalCashAmount, prevPolId, prevPolEffDt };
    }

    private Object[] processPartialFullPaySpread(BilIstSchedule bilIstSchedule, BilCashDsp bilCashDsp,
            double originalCashAmount, CashApply cashApply, BilCashReceipt bilCashReceipt, BilAccount bilAccount,
            BilPolicyTerm bilPolicyTerm, boolean creditReceipt, double amountSpread, String prevPolId,
            ZonedDateTime prevPolEffDt) {

        double dispositionAmount;
        double premiumPaidAmount;
        double creditPaidAmount;
        double openGrossAmount = DECIMAL_ZERO;
        double openCashPaid = DECIMAL_ZERO;
        double openFutureAmount = DECIMAL_ZERO;

        Double openAmount = bilIstSchedule.getBisCncFutAmt() + bilIstSchedule.getBisCreFutAmt()
                + bilIstSchedule.getBisCncOpnAmt() + bilIstSchedule.getBillPrmIstAmt()
                + bilIstSchedule.getBisCreOpnAmt();
        Double paidAmount = bilIstSchedule.getBisWroPrmAmt() + bilIstSchedule.getBisPrmPaidAmt()
                + bilIstSchedule.getBisCrePaidAmt();

        openAmount = openAmount - paidAmount;

        if (openAmount > originalCashAmount) {
            openAmount = originalCashAmount;
            originalCashAmount = DECIMAL_ZERO;
        }

        if (creditReceipt) {
            creditPaidAmount = openAmount + bilIstSchedule.getBisCrePaidAmt();
            premiumPaidAmount = bilIstSchedule.getBisPrmPaidAmt();
        } else {
            creditPaidAmount = bilIstSchedule.getBisCrePaidAmt();
            premiumPaidAmount = openAmount + bilIstSchedule.getBisPrmPaidAmt();
        }
        openCashPaid = openCashPaid + openAmount;
        openFutureAmount = openFutureAmount + openAmount;
        if (originalCashAmount != DECIMAL_ZERO) {
            originalCashAmount = originalCashAmount - openAmount;
            amountSpread = amountSpread - openAmount;
        }
        dispositionAmount = openAmount;

        if (dispositionAmount != DECIMAL_ZERO) {
            Object[] previousObject = updatePremAndCash(bilCashDsp, bilIstSchedule, dispositionAmount, creditPaidAmount,
                    premiumPaidAmount, openGrossAmount, openCashPaid, openFutureAmount, bilCashReceipt, cashApply,
                    bilAccount, bilPolicyTerm, prevPolId, prevPolEffDt);
            prevPolId = (String) previousObject[0];
            prevPolEffDt = (ZonedDateTime) previousObject[1];
        }

        return new Object[] { originalCashAmount, prevPolId, prevPolEffDt };
    }

    private double processFullPayCatchupRows(BilCashDsp bilCashDsp, double originalCashAmount, ZonedDateTime adjustDate,
            ZonedDateTime systemDate, String typeSpace, double invoiceCommissionPersent, BilPolicyTerm bilPolicyTerm,
            boolean presentOrFuture, Character priorityIndicator, CashApply cashApply, BilCashReceipt bilCashReceipt,
            BilAccount bilAccount, ZonedDateTime billIstDueDate, boolean creditReceipt) {

        List<Object[]> bilScheduleObjectList = null;
        String prevPolId = BLANK_STRING;
        ZonedDateTime prevPolEffDt = null;

        if (!cashApply.getCreditPolicyId().trim().isEmpty()) {
            bilScheduleObjectList = bilIstScheduleRepository.fetchCatchupPaymentRowsByPolicy(
                    bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                    typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                    CATCHUP_INVOICE_CODES, BilPolicyStatusCode.QUOTE.getValue(), DateRoutine.defaultDateTime(),
                    billIstDueDate, priorityIndicator, systemDate);
        } else {
            bilScheduleObjectList = bilIstScheduleRepository.futurePaymentPartialRows(
                    bilCashDsp.getBilCashDspId().getAccountId(), adjustDate, systemDate,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                    typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                    CATCHUP_INVOICE_CODES, DateRoutine.defaultDateTime(), billIstDueDate, priorityIndicator,
                    billIstDueDate);
        }

        if (bilScheduleObjectList != null && !bilScheduleObjectList.isEmpty()) {
            double originalPaymAmt = originalCashAmount;
            double applicationAmt = originalCashAmount;

            for (Object[] bilScheduleObject : bilScheduleObjectList) {
                ZonedDateTime bilIsuDueDate = (ZonedDateTime) bilScheduleObject[0];
                double balanceAmount = (double) bilScheduleObject[1];
                long count = (long) bilScheduleObject[2];
                String maxPolicyId = (String) bilScheduleObject[3];
                short maxSequenceNbr = (short) bilScheduleObject[4];
                double istDuePaidAmt = DECIMAL_ZERO;
                if (count <= SHORT_ZERO) {
                    continue;
                } else {
                    if (count > SHORT_ONE || count == SHORT_ONE && maxPolicyId.trim().isEmpty()) {
                        Double[] partialAmounts = processGroupDetails(balanceAmount, applicationAmt, originalCashAmount,
                                istDuePaidAmt, bilCashDsp, adjustDate, systemDate, typeSpace, invoiceCommissionPersent,
                                bilPolicyTerm, presentOrFuture, priorityIndicator, cashApply, bilCashReceipt,
                                bilAccount, creditReceipt, bilIsuDueDate, originalPaymAmt);

                        originalCashAmount = partialAmounts[0];
                        applicationAmt = partialAmounts[1];
                        istDuePaidAmt = partialAmounts[2];

                        originalPaymAmt = originalPaymAmt - istDuePaidAmt;
                        istDuePaidAmt = DECIMAL_ZERO;
                        if (applicationAmt == DECIMAL_ZERO || originalCashAmount == DECIMAL_ZERO) {
                            break;
                        }
                        continue;
                    }
                }

                BilIstSchedule bilIstSchedule = bilIstScheduleRepository.findById(
                        new BilIstScheduleId(bilCashDsp.getBilCashDspId().getAccountId(), maxPolicyId, maxSequenceNbr))
                        .orElse(null);
                if (bilIstSchedule == null) {
                    throw new DataNotFoundException(NO_DATA_FOUND);
                }
                Object[] partialAmounts = applyCash(bilIstSchedule, 1, originalPaymAmt, presentOrFuture, cashApply,
                        creditReceipt, bilCashDsp, bilCashReceipt, bilAccount, bilPolicyTerm, applicationAmt,
                        originalCashAmount, istDuePaidAmt, prevPolId, prevPolEffDt);

                originalCashAmount = (double) partialAmounts[0];
                applicationAmt = (double) partialAmounts[1];
                istDuePaidAmt = (double) partialAmounts[2];
                prevPolId = (String) partialAmounts[3];
                prevPolEffDt = (ZonedDateTime) partialAmounts[4];

                originalPaymAmt = originalPaymAmt - istDuePaidAmt;

                if (applicationAmt == DECIMAL_ZERO || originalCashAmount == DECIMAL_ZERO) {
                    break;
                }
            }
        }

        return originalCashAmount;
    }

    private Double[] processGroupDetails(double balanceAmount, double applicationAmt, double originalCashAmount,
            double istDuePaidAmt, BilCashDsp bilCashDsp, ZonedDateTime adjustDate, ZonedDateTime systemDate,
            String typeSpace, double invoiceCommissionPersent, BilPolicyTerm bilPolicyTerm, boolean presentOrFuture,
            Character priorityIndicator, CashApply cashApply, BilCashReceipt bilCashReceipt, BilAccount bilAccount,
            boolean creditReceipt, ZonedDateTime bilIsuDueDate, double originalPaymAmt) {

        boolean payGrpInFullSw = false;
        String prevPolId = BLANK_STRING;
        ZonedDateTime prevPolEffDt = null;

        if (originalCashAmount >= balanceAmount) {
            payGrpInFullSw = true;
        }

        List<BilIstSchedule> bilIstScheduleList = catchupGroupDetails(bilCashDsp, bilPolicyTerm, priorityIndicator,
                bilIsuDueDate, adjustDate, systemDate, typeSpace, cashApply, invoiceCommissionPersent);
        if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
            for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                Double openAmount = bilIstSchedule.getBisCncFutAmt() + bilIstSchedule.getBisCreFutAmt()
                        + bilIstSchedule.getBisCncOpnAmt() + bilIstSchedule.getBillPrmIstAmt()
                        + bilIstSchedule.getBisCreOpnAmt();
                Double paidAmount = bilIstSchedule.getBisWroPrmAmt() + bilIstSchedule.getBisPrmPaidAmt()
                        + bilIstSchedule.getBisCrePaidAmt();
                openAmount = openAmount - paidAmount;
                if (balanceAmount == DECIMAL_ZERO || openAmount == DECIMAL_ZERO) {
                    continue;
                }
                double quotient = DECIMAL_ZERO;
                if (payGrpInFullSw) {
                    quotient = 1;
                } else {
                    quotient = openAmount / balanceAmount;
                }
                Object[] partialAmounts = applyCash(bilIstSchedule, quotient, originalPaymAmt, presentOrFuture,
                        cashApply, creditReceipt, bilCashDsp, bilCashReceipt, bilAccount, bilPolicyTerm, applicationAmt,
                        originalCashAmount, istDuePaidAmt, prevPolId, prevPolEffDt);

                originalCashAmount = (double) partialAmounts[0];
                applicationAmt = (double) partialAmounts[1];
                istDuePaidAmt = (double) partialAmounts[2];
                prevPolId = (String) partialAmounts[3];
                prevPolEffDt = (ZonedDateTime) partialAmounts[4];

                if (applicationAmt == DECIMAL_ZERO || originalCashAmount == DECIMAL_ZERO) {
                    break;
                }
            }

        }
        return new Double[] { originalCashAmount, applicationAmt, istDuePaidAmt };
    }

    private List<BilIstSchedule> catchupGroupDetails(BilCashDsp bilCashDsp, BilPolicyTerm bilPolicyTerm,
            Character priorityIndicator, ZonedDateTime bilIsuDueDate, ZonedDateTime adjustDate,
            ZonedDateTime systemDate, String typeSpace, CashApply cashApply, double invoiceCommissionPersent) {

        if (!cashApply.getCreditPolicyId().trim().isEmpty()) {
            return bilIstScheduleRepository.groupCatchupRowsByPolicy(bilCashDsp.getBilCashDspId().getAccountId(),
                    adjustDate, systemDate, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING,
                    bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO,
                    bilCashDsp.getStatementDetailTypeCd(), typeSpace, CATCHUP_INVOICE_CODES,
                    BilPolicyStatusCode.QUOTE.getValue(), bilIsuDueDate, priorityIndicator);
        } else {
            return bilIstScheduleRepository.groupCatchupRows(bilCashDsp.getBilCashDspId().getAccountId(), adjustDate,
                    systemDate, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING,
                    bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO,
                    bilCashDsp.getStatementDetailTypeCd(), typeSpace, CATCHUP_INVOICE_CODES, bilIsuDueDate,
                    priorityIndicator);
        }

    }

    private Object[] applyCash(BilIstSchedule bilIstSchedule, double quotient, double originalPayment,
            boolean presentOrFuture, CashApply cashApply, boolean creditReceipt, BilCashDsp bilCashDsp,
            BilCashReceipt bilCashReceipt, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, double applicationAmt,
            double originalCashAmount, double istDuePaidAmt, String prevPolId, ZonedDateTime prevPolEffDt) {

        double dispositionAmount;
        double premiumPaidAmount = DECIMAL_ZERO;
        double creditPaidAmount = DECIMAL_ZERO;
        double openGrossAmount = DECIMAL_ZERO;
        double openCashPaid = DECIMAL_ZERO;
        double openFutureAmount = DECIMAL_ZERO;

        Double openAmount = bilIstSchedule.getBisCncFutAmt() + bilIstSchedule.getBisCreFutAmt()
                + bilIstSchedule.getBisCncOpnAmt() + bilIstSchedule.getBillPrmIstAmt()
                + bilIstSchedule.getBisCreOpnAmt();
        Double paidAmount = bilIstSchedule.getBisWroPrmAmt() + bilIstSchedule.getBisPrmPaidAmt()
                + bilIstSchedule.getBisCrePaidAmt();
        openAmount = openAmount - paidAmount;

        if (quotient <= DECIMAL_ZERO) {
            return new Object[] { originalCashAmount, applicationAmt, istDuePaidAmt, prevPolId, prevPolEffDt };
        }

        double product = quotient * originalPayment;

        int roundSepRec = (int) product;
        double fraction = product - roundSepRec;
        BigDecimal fourDecimalFraction = BigDecimal.valueOf(fraction).setScale(4, RoundingMode.UP);
        BigDecimal twoDecimalFraction = BigDecimal.valueOf(fraction).setScale(2, RoundingMode.DOWN);
        BigDecimal twoLastDecimalFraction = fourDecimalFraction.subtract(twoDecimalFraction);
        if (twoLastDecimalFraction.movePointRight(4).doubleValue() > DECIMAL_ZERO) {
            fraction = fourDecimalFraction.doubleValue() + 0.01;
            twoDecimalFraction = BigDecimal.valueOf(fraction).setScale(2, RoundingMode.DOWN);
            product = roundSepRec + twoDecimalFraction.doubleValue();
        }

        if (product == DECIMAL_ZERO) {
            return new Object[] { originalCashAmount, applicationAmt, istDuePaidAmt, prevPolId, prevPolEffDt };
        }
        if (product < openAmount) {
            openAmount = product;
        }
        if (openAmount > applicationAmt) {
            openAmount = applicationAmt;
        }
        applicationAmt = applicationAmt - openAmount;
        originalCashAmount = originalCashAmount - openAmount;
        if (originalPayment == DECIMAL_ZERO) {
            return new Object[] { originalCashAmount, applicationAmt, istDuePaidAmt, prevPolId, prevPolEffDt };
        }
        if (creditReceipt && presentOrFuture) {
            creditPaidAmount = openAmount + bilIstSchedule.getBisCrePaidAmt();
            premiumPaidAmount = bilIstSchedule.getBisPrmPaidAmt();
            openCashPaid = openCashPaid + openAmount;
            openFutureAmount = openFutureAmount + openAmount;

        } else {
            if (presentOrFuture) {
                creditPaidAmount = bilIstSchedule.getBisCrePaidAmt();
                premiumPaidAmount = openAmount + bilIstSchedule.getBisPrmPaidAmt();
                openCashPaid = openCashPaid + openAmount;
                openFutureAmount = openFutureAmount + openAmount;
            }
        }
        if (creditReceipt && !presentOrFuture) {
            creditPaidAmount = openAmount + bilIstSchedule.getBisCrePaidAmt();
            premiumPaidAmount = bilIstSchedule.getBisPrmPaidAmt();
            openCashPaid = openCashPaid + openAmount;
            openGrossAmount = openGrossAmount + openAmount;
        } else {
            if (!presentOrFuture) {
                creditPaidAmount = bilIstSchedule.getBisCrePaidAmt();
                premiumPaidAmount = openAmount + bilIstSchedule.getBisPrmPaidAmt();
                openCashPaid = openCashPaid + openAmount;
                openGrossAmount = openGrossAmount + openAmount;
            }
        }
        dispositionAmount = openAmount;
        istDuePaidAmt = istDuePaidAmt + openAmount;

        if (dispositionAmount != DECIMAL_ZERO) {
            Object[] previousObject = updatePremAndCash(bilCashDsp, bilIstSchedule, dispositionAmount, creditPaidAmount,
                    premiumPaidAmount, openGrossAmount, openCashPaid, openFutureAmount, bilCashReceipt, cashApply,
                    bilAccount, bilPolicyTerm, prevPolId, prevPolEffDt);
            prevPolId = (String) previousObject[0];
            prevPolEffDt = (ZonedDateTime) previousObject[1];
        }

        return new Object[] { originalCashAmount, applicationAmt, istDuePaidAmt, prevPolId, prevPolEffDt };
    }

    private void insertCash(BilCashDsp bilCashDsp, short bilSequenceNumber, ZonedDateTime dueDate,
            ZonedDateTime invoiceDate, char bilCrgTypeCd, ZonedDateTime applyDate, double dispositionAmount,
            ZonedDateTime systemDueDate, String policyId, String policyNumber, String policySymbol,
            ZonedDateTime effectiveDate, String payableItem, String userSequenceId) {
        Short maxSequence = bilCashDspRepository.findMaxDispositionNumber(bilCashDsp.getBilCashDspId().getAccountId(),
                bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr());
        if (maxSequence == null) {
            maxSequence = 0;
        } else {
            maxSequence = (short) (maxSequence + 1);
        }

        BilCashDsp bilCashDspSave = new BilCashDsp();
        BilCashDspId bilCashDspId = new BilCashDspId(bilCashDsp.getBilCashDspId().getAccountId(),
                bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr(),
                maxSequence);
        bilCashDspSave.setPolicyId(policyId);
        bilCashDspSave.setPolicyNumber(policyNumber);
        bilCashDspSave.setPolicySymbolCd(policySymbol);
        bilCashDspSave.setPolicyEffectiveDate(effectiveDate);
        bilCashDspSave.setBilSequenceNumber(bilSequenceNumber);
        bilCashDspSave.setSystemDueDate(systemDueDate);
        bilCashDspSave.setAdjustmentDueDate(dueDate);
        bilCashDspSave.setInvoiceDate(invoiceDate);
        bilCashDspSave.setChargeTypeCd(bilCrgTypeCd);
        bilCashDspSave.setUserId(userSequenceId);
        bilCashDspSave.setDspDate(applyDate);
        bilCashDspSave.setDspTypeCd(BilDspTypeCode.APPLIED.getValue());
        bilCashDspSave
                .setDspAmount(BigDecimal.valueOf(dispositionAmount).setScale(2, RoundingMode.HALF_EVEN).doubleValue());
        bilCashDspSave.setPayeeClientId(bilCashDsp.getPayeeClientId());
        bilCashDspSave.setPayeeAddressSequenceNumber(bilCashDsp.getPayeeAddressSequenceNumber());
        bilCashDspSave.setManualSuspenseIndicator(bilCashDsp.getManualSuspenseIndicator());
        bilCashDspSave.setDraftNbr(SHORT_ZERO);

        bilCashDspSave.setCreditPolicyId(BLANK_STRING);
        if (bilCashDsp.getCreditIndicator() != CHAR_P || bilCashDsp.getCreditIndicator() != CHAR_H) {
            bilCashDspSave.setCreditAmountType(BLANK_STRING);
            bilCashDspSave.setCreditIndicator(BLANK_CHAR);
        } else {
            bilCashDspSave.setCreditAmountType(bilCashDsp.getCreditAmountType());
            bilCashDspSave.setCreditIndicator(bilCashDsp.getCreditIndicator());
        }

        bilCashDspSave.setRevsRsusIndicator(BLANK_CHAR);
        bilCashDspSave.setToFromTransferNbr(bilCashDsp.getToFromTransferNbr());
        bilCashDspSave.setTransferTypeCd(BLANK_CHAR);
        bilCashDspSave.setDisbursementDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setStatusCd(BLANK_CHAR);
        bilCashDspSave.setDisbursementId(BLANK_STRING);
        bilCashDspSave.setCheckProdMethodCd(BLANK_CHAR);
        bilCashDspSave.setStatementDetailTypeCd(bilCashDsp.getStatementDetailTypeCd());
        bilCashDspSave.setBilCashDspId(bilCashDspId);
        bilCashDspSave.setPayableItemCd(payableItem);
        bilCashDspRepository.saveAndFlush(bilCashDspSave);

    }

    private void insertCash1(String accountId, short bilSequenceNumber, ZonedDateTime invoiceDate,
            ZonedDateTime applyDate, double dispositionAmount, String policyId, String policyNumber,
            String policySymbol, ZonedDateTime effectiveDate, String payableItem, String userSequenceId) {
        Short maxSequence = bilCashDspRepository.findMaxDistributionNumber(accountId, applyDate);
        if (maxSequence == null) {
            maxSequence = 0;
        } else {
            maxSequence = (short) (maxSequence + 1);
        }

        BilCashDsp bilCashDspSave = new BilCashDsp();
        BilCashDspId bilCashDspId = new BilCashDspId(accountId, applyDate, maxSequence, SHORT_ZERO);
        bilCashDspSave.setPolicyId(policyId);
        bilCashDspSave.setPolicyNumber(policyNumber);
        bilCashDspSave.setPolicySymbolCd(policySymbol);
        bilCashDspSave.setPolicyEffectiveDate(effectiveDate);
        bilCashDspSave.setBilSequenceNumber(bilSequenceNumber);
        bilCashDspSave.setSystemDueDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setAdjustmentDueDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setInvoiceDate(invoiceDate);
        bilCashDspSave.setChargeTypeCd(BLANK_CHAR);
        bilCashDspSave.setUserId(userSequenceId);
        bilCashDspSave.setDspDate(applyDate);
        bilCashDspSave.setDspTypeCd(BilDspTypeCode.EXCLUDE.getValue());
        bilCashDspSave.setDspAmount(dispositionAmount);
        bilCashDspSave.setPayeeClientId(BLANK_STRING);
        bilCashDspSave.setPayeeAddressSequenceNumber(SHORT_ZERO);
        bilCashDspSave.setManualSuspenseIndicator(CHAR_N);
        bilCashDspSave.setDraftNbr(SHORT_ZERO);
        bilCashDspSave.setCreditPolicyId(BLANK_STRING);
        bilCashDspSave.setCreditAmountType(BLANK_STRING);
        bilCashDspSave.setCreditIndicator(CHAR_N);

        bilCashDspSave.setRevsRsusIndicator(BLANK_CHAR);
        bilCashDspSave.setToFromTransferNbr(BLANK_STRING);
        bilCashDspSave.setTransferTypeCd(BLANK_CHAR);
        bilCashDspSave.setDisbursementDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setStatusCd(BLANK_CHAR);
        bilCashDspSave.setDisbursementId(BLANK_STRING);
        bilCashDspSave.setCheckProdMethodCd(BLANK_CHAR);
        bilCashDspSave.setStatementDetailTypeCd(BLANK_STRING);
        bilCashDspSave.setBilCashDspId(bilCashDspId);
        bilCashDspSave.setPayableItemCd(payableItem);
        bilCashDspRepository.saveAndFlush(bilCashDspSave);

    }

    private String[] calculateChargeActions(char chargeTypeCd, char invoiceCode, double openChargeAmount,
            boolean futureSwitch) {
        String objectCode = BLANK_STRING;
        String actionCode;

        if (chargeTypeCd == BLANK_CHAR) {
            objectCode = ObjectCode.PREMIUM.toString();
        } else if (chargeTypeCd == ChargeTypeCodes.PENALTY_CHARGE.getValue()) {
            objectCode = ObjectCode.PENALTY_CHARGE.toString();
        } else if (chargeTypeCd == ChargeTypeCodes.LATE_CHARGE.getValue()) {
            objectCode = ObjectCode.LATE_CHARGE.toString();
        } else if (chargeTypeCd == ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()) {
            objectCode = ObjectCode.DOWN_PAYMENT_FEE.toString();
        } else if (chargeTypeCd == ChargeTypeCodes.SERVICE_CHARGE.getValue()) {
            objectCode = ObjectCode.SERVICE_CHARGE.toString();
            actionCode = ActionCode.APPLY_SERVICE_CHARGE.toString();
            return new String[] { objectCode, actionCode };
        }

        if (futureSwitch) {
            actionCode = ActionCode.APPLY_TO_UNBILLED.toString();
        } else {
            if (openChargeAmount > DECIMAL_ZERO
                    && (invoiceCode == InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue()
                            || invoiceCode == BLANK_CHAR
                            || invoiceCode == InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING
                                    .getValue())) {
                actionCode = ActionCode.APPLY_TO_UNBILLED.toString();
            } else {
                actionCode = ActionCode.APPLY_TO_INSTALL.toString();
            }
        }

        return new String[] { objectCode, actionCode };
    }

    private double processFutureCharge(BilAccount bilAccount, BilCashDsp bilCashDsp, double originalCashAmount,
            char chargeType, CashApply cashApply, BilPolicyTerm bilPolicyTerm, BilCashReceipt bilCashReceipt) {

        char quoteIndicator = cashApply.getQuoteIndicator().charAt(SHORT_ZERO);

        List<BilCrgAmounts> bilCrgAmountList = bilCrgAmountsRepository.fetchFuturesCharge(bilAccount.getAccountId(),
                chargeType, FUTURE_INVOICE_CODES);
        for (BilCrgAmounts bilCrgAmounts : bilCrgAmountList) {
            double chargeAmount1 = DECIMAL_ZERO;
            double chargeAmount2 = bilCrgAmounts.getBcaCrgAmt();
            double newChargeAmount;
            double dispositionAmount;
            double openCashAmount = DECIMAL_ZERO;
            double openFutureAmount = DECIMAL_ZERO;

            cashApply.setApplyReference("AD");
            chargeAmount1 = chargeAmount1 + bilCrgAmounts.getBcaCrgPaidAmt() + bilCrgAmounts.getBcaWroCrgAmt();
            chargeAmount2 = chargeAmount2 - chargeAmount1;
            if (originalCashAmount >= chargeAmount2) {
                originalCashAmount = originalCashAmount - chargeAmount2;
                newChargeAmount = chargeAmount2 + bilCrgAmounts.getBcaCrgPaidAmt();
                dispositionAmount = chargeAmount2;
                openCashAmount = openCashAmount + chargeAmount2;
                openFutureAmount = openFutureAmount + chargeAmount2;
            } else {
                newChargeAmount = originalCashAmount + bilCrgAmounts.getBcaCrgPaidAmt();
                dispositionAmount = originalCashAmount;
                openCashAmount = openCashAmount + originalCashAmount;
                openFutureAmount = openFutureAmount + originalCashAmount;
                originalCashAmount = DECIMAL_ZERO;
            }
            if (dispositionAmount != DECIMAL_ZERO) {
                updateSvcAndCash(bilCashDsp, bilCrgAmounts, newChargeAmount, chargeType, cashApply.getCashApplyDate(),
                        dispositionAmount, cashApply.getUserSequenceId());

                String orginalEfftiveDate;
                String referenceDate;
                String bilDatabaseKey;

                if (openCashAmount > DECIMAL_ZERO && quoteIndicator != BilPolicyStatusCode.QUOTE.getValue()) {
                    if (bilPolicyTerm.getBillPolicyTermId().getPolicyId().trim().isEmpty()) {
                        orginalEfftiveDate = BLANK_STRING;
                        referenceDate = DateRoutine.dateTimeAsYYYYMMDDString(cashApply.getCashApplyDate());
                    } else {
                        orginalEfftiveDate = "9999-12-31";
                        referenceDate = DateRoutine
                                .dateTimeAsYYYYMMDDString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                    }

                    bilDatabaseKey = getBilDatabaseKey(bilCashReceipt.getEntryDate(), bilCashReceipt.getEntryNumber(),
                            bilCashReceipt.getUserId(), bilCashReceipt.getEntrySequenceNumber());

                    callFinancialApi(openCashAmount, openCashAmount, cashApply.getCashApplyDate(),
                            bilCashReceipt.getBankCd(), bilAccount.getAccountNumber(),
                            BilCashEntryMethodCode.CASH.getValue(), BilCashEntryMethodCode.APPLY.getValue(),
                            BLANK_STRING, String.valueOf(bilCashReceipt.getCashEntryMethodCd()),
                            bilCashReceipt.getReceiptTypeCd(), BLANK_STRING, bilAccount,
                            bilPolicyTerm.getBillCountryCd(), bilPolicyTerm.getLobCd(), orginalEfftiveDate,
                            bilPolicyTerm, bilDatabaseKey, referenceDate, cashApply.getUserSequenceId());
                }

                if (openFutureAmount > DECIMAL_ZERO && quoteIndicator != BilPolicyStatusCode.QUOTE.getValue()) {
                    orginalEfftiveDate = DateRoutine.dateTimeAsYYYYMMDDString(cashApply.getCashApplyDate());
                    referenceDate = DateRoutine.dateTimeAsYYYYMMDDString(cashApply.getCashApplyDate());

                    String[] fwsCodes = calculateChargeActions(chargeType, bilCrgAmounts.getBilInvoiceCd(),
                            openFutureAmount, true);
                    if (fwsCodes[1].compareTo(ActionCode.APPLY_TO_UNBILLED.toString()) == 0
                            && (fwsCodes[0].compareTo(ObjectCode.LATE_CHARGE.toString()) == 0
                                    || fwsCodes[0].compareTo(ObjectCode.DOWN_PAYMENT_FEE.toString()) == 0
                                    || fwsCodes[0].compareTo(ObjectCode.PENALTY_CHARGE.toString()) == 0)) {
                        bilDatabaseKey = BLANK_STRING;
                    } else {
                        bilDatabaseKey = getBilDatabaseKey(bilCashReceipt.getEntryDate(),
                                bilCashReceipt.getEntryNumber(), bilCashReceipt.getUserId(),
                                bilCashReceipt.getEntrySequenceNumber());
                    }
                    callFinancialApi(openFutureAmount, openFutureAmount, cashApply.getCashApplyDate(),
                            bilCashReceipt.getBankCd(), bilAccount.getAccountNumber(), fwsCodes[0], fwsCodes[1],
                            BLANK_STRING, String.valueOf(bilCashReceipt.getCashEntryMethodCd()),
                            bilCashReceipt.getReceiptTypeCd(), BLANK_STRING, bilAccount,
                            bilPolicyTerm.getBillCountryCd(), bilPolicyTerm.getLobCd(), orginalEfftiveDate,
                            bilPolicyTerm, bilDatabaseKey, referenceDate, cashApply.getUserSequenceId());
                }
            }

        }

        return originalCashAmount;
    }

    private void updateSvcAndCash(BilCashDsp bilCashDsp, BilCrgAmounts bilCrgAmounts, double newChargeAmount,
            char bilCrgTypeCd, ZonedDateTime applyDate, double dispositionAmount, String userSequenceId) {
        bilCrgAmounts.setBcaCrgPaidAmt(newChargeAmount);
        bilCrgAmountsRepository.saveAndFlush(bilCrgAmounts);
        insertCash(bilCashDsp, bilCrgAmounts.getBilCrgAmountsId().getBilSeqNbr(), bilCrgAmounts.getBilAdjDueDt(),
                bilCrgAmounts.getBilInvDt(), bilCrgTypeCd, applyDate, dispositionAmount, DateRoutine.defaultDateTime(),
                BLANK_STRING, BLANK_STRING, BLANK_STRING, DateRoutine.defaultDateTime(), BLANK_STRING, userSequenceId);

    }

    private void checkPendingTape(BilAccount bilAccount, ZonedDateTime currentDate) {
        final String EFT_COLLECTION_METHOD = "CET";
        final char ACTIVE_STATUS = CHAR_A;
        boolean rowExists = fetchDesReasonCode(bilAccount.getCollectionMethod(), EFT_COLLECTION_METHOD);
        if (rowExists) {
            List<BilEftBank> bilEftBankList = bilEftBankRepository.findBilEftBankEntry(bilAccount.getAccountId(),
                    CHAR_A, currentDate, ACTIVE_STATUS, PageRequest.of(SHORT_ZERO, SHORT_ONE));
            if (bilEftBankList != null && !bilEftBankList.isEmpty()) {
                ZonedDateTime minEftTapeDate = bilEftPendingTapeRepository.getMinEftTapeDate(bilAccount.getAccountId(),
                        CHAR_A, CHAR_O, BLANK_STRING);
                if (minEftTapeDate != null && minEftTapeDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
                    BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("EADJ", bilAccount.getBillTypeCd(),
                            bilAccount.getBillClassCd(), BLANK_STRING);
                    if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                        insertBilActInquiry(bilAccount.getAccountId(), minEftTapeDate);
                    }
                }
            }
        }

    }

    private void insertBilActInquiry(String accountId, ZonedDateTime minEftTapeDate) {
        BilActInquiry bilActInquiry = new BilActInquiry();
        BilActInquiryId bilActInquiryId = new BilActInquiryId();
        bilActInquiryId.setBilAccountId(accountId);
        bilActInquiryId.setBilAcyTypeCd("EPA");
        bilActInquiry.setBilActInquiryId(bilActInquiryId);
        bilActInquiry.setBilNxtAcyDt(minEftTapeDate);
        bilActInquiryRepository.save(bilActInquiry);
    }

    private double processOpensAmount(String policyId, BilAccount bilAccount, BilCashDsp bilCashDsp,
            double invoiceCommissionPersent, String typeSpace, CashApply cashApply, double originalCashAmount,
            BilCashReceipt bilCashReceipt, BilPolicyTerm bilPolicyTerm, String alcaRuleParam, boolean creditReceipt,
            boolean auditPremiumHiddenSw) {

        Double opensAmount = bilIstScheduleRepository.fetchOpensAmounts(bilCashDsp.getBilCashDspId().getAccountId(),
                CURRENT_INVOICE_CODES, policyId, BLANK_STRING, bilCashDsp.getPayableItemCd().trim(), typeSpace.trim(),
                invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getAdjustmentDueDate(),
                bilCashDsp.getAdjustmentDueDate(), DateRoutine.defaultDateTime(), DECIMAL_ZERO,
                bilCashDsp.getStatementDetailTypeCd().trim(), typeSpace.trim());
        if (opensAmount != null && opensAmount > DECIMAL_ZERO) {

            originalCashAmount = processAdjustDueDate(policyId, bilAccount, bilCashDsp, invoiceCommissionPersent,
                    typeSpace, cashApply, originalCashAmount, bilCashReceipt, bilPolicyTerm, creditReceipt,
                    alcaRuleParam, auditPremiumHiddenSw);

            if (originalCashAmount > DECIMAL_ZERO && !auditPremiumHiddenSw) {
                originalCashAmount = checkChargeOnly(bilAccount, bilCashDsp, originalCashAmount, cashApply,
                        bilCashReceipt, bilPolicyTerm, false, null);
            }

        } else {
            if ((opensAmount == null || opensAmount == DECIMAL_ZERO) && (!bilCashReceipt.getReceiptTypeCd().trim()
                    .equalsIgnoreCase(BilReceiptTypeCode.EVEN_ADJUSTMENT.getValue()))) {
                if ((bilCashDsp.getPolicyNumber().trim().compareTo(BLANK_STRING) > SHORT_ZERO
                        || bilCashDsp.getPayableItemCd().trim().compareTo(BLANK_STRING) > SHORT_ZERO)
                        && !bilAccount.getBillTypeCd().trim().equals(AccountTypeCode.SINGLE_POLICY.getValue())) {
                    return originalCashAmount;
                }
                if ((auditPremiumHiddenSw && alcaRuleParam.charAt(SHORT_ZERO) != CHAR_Y) || !auditPremiumHiddenSw) {
                    originalCashAmount = checkChargeOnly(bilAccount, bilCashDsp, originalCashAmount, cashApply,
                            bilCashReceipt, bilPolicyTerm, false, null);
                }
            }

        }

        return originalCashAmount;
    }

    private double processAdjustDueDate(String policyId, BilAccount bilAccount, BilCashDsp bilCashDsp,
            double invoiceCommissionPersent, String typeSpace, CashApply cashApply, double originalCashAmount,
            BilCashReceipt bilCashReceipt, BilPolicyTerm bilPolicyTerm, boolean creditReceipt, String alcaRuleParam,
            boolean auditPremiumHiddenSw) {

        ZonedDateTime adjustDueDate = null;
        ZonedDateTime minSystemDate = null;
        ZonedDateTime maxSystemDate = null;

        if (DateRoutine.dateTimeAsMMDDYYYYAsString(bilCashDsp.getAdjustmentDueDate())
                .equals(DateRoutine.dateTimeAsMMDDYYYYAsString(DateRoutine.defaultDateTime()))) {
            adjustDueDate = bilIstScheduleRepository.fetchMinAdjustDate(bilCashDsp.getBilCashDspId().getAccountId(),
                    CURRENT_INVOICE_CODES, policyId, BLANK_STRING, bilCashDsp.getPayableItemCd(), typeSpace,
                    invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace);
        } else {
            adjustDueDate = bilCashDsp.getAdjustmentDueDate();
        }

        Object[] systemDueRange = getSystemDate(adjustDueDate, bilCashDsp, bilPolicyTerm, invoiceCommissionPersent,
                typeSpace);
        if (systemDueRange != null && systemDueRange.length > 1) {
            minSystemDate = (ZonedDateTime) systemDueRange[SHORT_ZERO];
            maxSystemDate = (ZonedDateTime) systemDueRange[SHORT_ONE];
        }
        if (minSystemDate == null || maxSystemDate == null) {
            throw new DataNotFoundException("min.max.dates.not.found");
        }

        if (!auditPremiumHiddenSw && alcaRuleParam.charAt(SHORT_ZERO) != CHAR_Y) {
            originalCashAmount = checkOldChargeDue(bilAccount, bilCashDsp, originalCashAmount, cashApply,
                    bilCashReceipt, bilPolicyTerm, adjustDueDate);
        }

        boolean endOfScheduleSw = false;
        ZonedDateTime systemDate = minSystemDate;
        ZonedDateTime holdAdjDt = adjustDueDate;
        ZonedDateTime holdSysDt = minSystemDate;

        while (!(endOfScheduleSw || originalCashAmount == DECIMAL_ZERO)) {
            Object[] adjustRows = processAdjustDates(policyId, bilAccount, bilCashDsp, invoiceCommissionPersent,
                    typeSpace, cashApply, originalCashAmount, bilCashReceipt, bilPolicyTerm, adjustDueDate, systemDate,
                    creditReceipt, holdAdjDt, holdSysDt);

            originalCashAmount = (double) adjustRows[SHORT_ZERO];
            adjustDueDate = (ZonedDateTime) adjustRows[SHORT_ONE];
            if (originalCashAmount == DECIMAL_ZERO) {
                break;
            }
            if (originalCashAmount > DECIMAL_ZERO) {
                ZonedDateTime nextSystemDate = bilIstScheduleRepository.fetchMinSystemDueDate(
                        bilCashDsp.getBilCashDspId().getAccountId(), CURRENT_INVOICE_CODES, policyId, BLANK_STRING,
                        bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO,
                        bilCashDsp.getStatementDetailTypeCd(), typeSpace, adjustDueDate, systemDate);

                if (nextSystemDate == null) {
                    if ((bilCashDsp.getPolicyNumber().trim().compareTo(BLANK_STRING) > SHORT_ZERO
                            || bilCashDsp.getPayableItemCd().trim().compareTo(BLANK_STRING) > SHORT_ZERO)
                            && !bilAccount.getBillTypeCd().trim().equals(BilDspTypeCode.SUSPENDED.getValue())) {

                    } else {
                        if (cashApply.getPaymentPriorityInd() == CHAR_P && originalCashAmount > DECIMAL_ZERO
                                && (systemDate.compareTo(maxSystemDate) == 0)
                                && alcaRuleParam.charAt(SHORT_ZERO) != CHAR_Y) {
                            if (auditPremiumHiddenSw) {
                                originalCashAmount = checkOldChargeDue(bilAccount, bilCashDsp, originalCashAmount,
                                        cashApply, bilCashReceipt, bilPolicyTerm, adjustDueDate);
                            }
                            originalCashAmount = processCharges(originalCashAmount, adjustDueDate, bilCashDsp,
                                    cashApply, bilCashReceipt, bilPolicyTerm, bilAccount);
                        }
                    }
                } else {
                    systemDate = nextSystemDate;
                }
                if (nextSystemDate == null) {
                    if (!bilCashDsp.getAdjustmentDueDate().equals(DateRoutine.defaultDateTime())) {
                        endOfScheduleSw = true;
                    }
                    ZonedDateTime preAdjustDate = getNextAdjustDate(adjustDueDate, bilCashDsp, bilPolicyTerm,
                            invoiceCommissionPersent, typeSpace);

                    if (preAdjustDate == null) {
                        endOfScheduleSw = true;
                    } else {
                        adjustDueDate = preAdjustDate;
                        systemDueRange = getSystemDate(adjustDueDate, bilCashDsp, bilPolicyTerm,
                                invoiceCommissionPersent, typeSpace);
                        if (systemDueRange != null && systemDueRange.length > 1) {
                            minSystemDate = (ZonedDateTime) systemDueRange[SHORT_ZERO];
                            maxSystemDate = (ZonedDateTime) systemDueRange[SHORT_ONE];
                        }
                    }
                }
            }
        }

        return originalCashAmount;

    }

    private Object[] getSystemDate(ZonedDateTime adjustDueDate, BilCashDsp bilCashDsp, BilPolicyTerm bilPolicyTerm,
            double invoiceCommissionPersent, String typeSpace) {

        List<Object[]> dateTimeList = bilIstScheduleRepository.fetchSystemDueDateRange(
                bilCashDsp.getBilCashDspId().getAccountId(), CURRENT_INVOICE_CODES,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING, bilCashDsp.getPayableItemCd(),
                typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace,
                adjustDueDate);
        if (dateTimeList != null && !dateTimeList.isEmpty()) {
            return dateTimeList.get(SHORT_ZERO);
        }
        return new Object[] {};

    }

    private ZonedDateTime getNextAdjustDate(ZonedDateTime adjustDueDate, BilCashDsp bilCashDsp,
            BilPolicyTerm bilPolicyTerm, double invoiceCommissionPersent, String typeSpace) {

        return bilIstScheduleRepository.fetchNextMinAdjustDate(bilCashDsp.getBilCashDspId().getAccountId(),
                CURRENT_INVOICE_CODES, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), BLANK_STRING,
                bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO,
                bilCashDsp.getStatementDetailTypeCd(), typeSpace, adjustDueDate);
    }

    private Object[] processAdjustDates(String policyId, BilAccount bilAccount, BilCashDsp bilCashDsp,
            double invoiceCommissionPersent, String typeSpace, CashApply cashApply, double originalCashAmount,
            BilCashReceipt bilCashReceipt, BilPolicyTerm bilPolicyTerm, ZonedDateTime adjustDueDate,
            ZonedDateTime minSystemDate, boolean creditReceipt, ZonedDateTime holdAdjDt, ZonedDateTime holdSysDt) {

        if (cashApply.getPaymentPriorityInd() == CHAR_S) {
            if (!bilCashDsp.getAdjustmentDueDate().equals(DateRoutine.defaultDateTime())) {
                originalCashAmount = checkChargeOnly(bilAccount, bilCashDsp, originalCashAmount, cashApply,
                        bilCashReceipt, bilPolicyTerm, false, null);
            } else {
                originalCashAmount = processCharges(originalCashAmount, adjustDueDate, bilCashDsp, cashApply,
                        bilCashReceipt, bilPolicyTerm, bilAccount);
            }
        }

        if (originalCashAmount == DECIMAL_ZERO) {
            return new Object[] { originalCashAmount, adjustDueDate };
        }

        return processOpenAdjustRows(policyId, bilAccount, bilCashDsp, invoiceCommissionPersent, typeSpace, cashApply,
                originalCashAmount, bilCashReceipt, bilPolicyTerm, adjustDueDate, minSystemDate, creditReceipt,
                holdAdjDt, holdSysDt);
    }

    private double processCharges(double originalCashAmount, ZonedDateTime adjustDueDate, BilCashDsp bilCashDsp,
            CashApply cashApply, BilCashReceipt bilCashReceipt, BilPolicyTerm bilPolicyTerm, BilAccount bilAccount) {

        originalCashAmount = processChargesAmounts(bilAccount.getAccountId(), originalCashAmount, adjustDueDate,
                ChargeTypeCodes.PENALTY_CHARGE.getValue(), bilCashDsp, cashApply, bilCashReceipt, bilPolicyTerm,
                bilAccount);
        if (originalCashAmount != DECIMAL_ZERO) {
            originalCashAmount = processChargesAmounts(bilAccount.getAccountId(), originalCashAmount, adjustDueDate,
                    ChargeTypeCodes.LATE_CHARGE.getValue(), bilCashDsp, cashApply, bilCashReceipt, bilPolicyTerm,
                    bilAccount);
        }
        if (originalCashAmount != DECIMAL_ZERO
                && cashApply.getPremiumPayIndicator() != ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()) {
            originalCashAmount = processChargesAmounts(bilAccount.getAccountId(), originalCashAmount, adjustDueDate,
                    ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue(), bilCashDsp, cashApply, bilCashReceipt, bilPolicyTerm,
                    bilAccount);
        }

        if (originalCashAmount != DECIMAL_ZERO
                && cashApply.getPremiumPayIndicator() != ChargeTypeCodes.SERVICE_CHARGE.getValue()) {
            originalCashAmount = processChargesAmounts(bilAccount.getAccountId(), originalCashAmount, adjustDueDate,
                    ChargeTypeCodes.SERVICE_CHARGE.getValue(), bilCashDsp, cashApply, bilCashReceipt, bilPolicyTerm,
                    bilAccount);
        }
        return originalCashAmount;
    }

    private Object[] processOpenAdjustRows(String policyId, BilAccount bilAccount, BilCashDsp bilCashDsp,
            double invoiceCommissionPersent, String typeSpace, CashApply cashApply, double originalCashAmount,
            BilCashReceipt bilCashReceipt, BilPolicyTerm bilPolicyTerm, ZonedDateTime adjustDueDate,
            ZonedDateTime billSysDueDt, boolean creditReceipt, ZonedDateTime holdAdjDt, ZonedDateTime holdSysDt) {

        List<BilIstSchedule> bilIstScheduleList = null;
        String prevPolId = BLANK_STRING;
        ZonedDateTime prevPolEffDt = null;

        Double opensAdjustAmount = bilIstScheduleRepository.fetchOpensAdjustAmounts(
                bilCashDsp.getBilCashDspId().getAccountId(), CURRENT_INVOICE_CODES, policyId, BLANK_STRING,
                bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO, adjustDueDate,
                billSysDueDt, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace);

        if (opensAdjustAmount != null) {
            if (originalCashAmount >= opensAdjustAmount) {
                if (cashApply.getCreditPolicyId().isEmpty()) {
                    bilIstScheduleList = bilIstScheduleRepository.findPaymentAdjustRows(
                            bilCashDsp.getBilCashDspId().getAccountId(), adjustDueDate, billSysDueDt, policyId,
                            BLANK_STRING, bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent,
                            DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace, CURRENT_INVOICE_CODES,
                            DateRoutine.defaultDateTime(), billSysDueDt);

                } else {
                    bilIstScheduleList = bilIstScheduleRepository.findPaymentAdjustRowsByPolicy(
                            bilCashDsp.getBilCashDspId().getAccountId(), adjustDueDate, billSysDueDt, policyId,
                            BLANK_STRING, bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent,
                            DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace, CURRENT_INVOICE_CODES,
                            DateRoutine.defaultDateTime(), BilPolicyStatusCode.QUOTE.getValue(), billSysDueDt);
                }

                if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
                    for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                        Object[] fullpayEachRow = processFullpayEachRow(originalCashAmount, bilIstSchedule, bilAccount,
                                bilCashDsp, cashApply, bilCashReceipt, bilPolicyTerm, creditReceipt, prevPolId,
                                prevPolEffDt);
                        originalCashAmount = (double) fullpayEachRow[0];
                        prevPolId = (String) fullpayEachRow[1];
                        prevPolEffDt = (ZonedDateTime) fullpayEachRow[2];
                        if (originalCashAmount == DECIMAL_ZERO) {
                            break;
                        }
                    }

                }

            } else {
                boolean secondTermExcludeSw = determineExcludeRows(bilCashDsp.getBilCashDspId().getAccountId(),
                        adjustDueDate, billSysDueDt, bilCashDsp.getPayableItemCd(), typeSpace, false, false);
                if (originalCashAmount != DECIMAL_ZERO) {
                    originalCashAmount = calculateLowPriorityAmount(bilCashDsp, originalCashAmount, adjustDueDate,
                            billSysDueDt, typeSpace, invoiceCommissionPersent, policyId, cashApply, bilCashReceipt,
                            bilAccount, bilPolicyTerm, creditReceipt);
                }
                if (secondTermExcludeSw) {
                    unhideMultipleTermRows(bilCashDsp.getBilCashDspId().getAccountId(), adjustDueDate, billSysDueDt,
                            false);
                    secondTermExcludeSw = false;
                    if (originalCashAmount > DECIMAL_ZERO) {

                        ZonedDateTime dueDate = bilIstScheduleRepository.fetchMinAdjustDateRow(
                                bilCashDsp.getBilCashDspId().getAccountId(), CURRENT_INVOICE_CODES, policyId,
                                BLANK_STRING, bilCashDsp.getPayableItemCd(), typeSpace, holdAdjDt, holdSysDt,
                                invoiceCommissionPersent, DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(),
                                typeSpace);
                        if (dueDate != null) {
                            adjustDueDate = dueDate;
                            if (originalCashAmount != DECIMAL_ZERO) {
                                originalCashAmount = calculateLowPriorityAmount(bilCashDsp, originalCashAmount, dueDate,
                                        billSysDueDt, typeSpace, invoiceCommissionPersent, policyId, cashApply,
                                        bilCashReceipt, bilAccount, bilPolicyTerm, creditReceipt);
                            }
                        }
                    }
                }

            }

        }

        return new Object[] { originalCashAmount, adjustDueDate };
    }

    private double calculateLowPriorityAmount(BilCashDsp bilCashDsp, double originalCashAmount,
            ZonedDateTime adjustDueDate, ZonedDateTime billSysDueDt, String typeSpace, double invoiceCommissionPersent,
            String policyId, CashApply cashApply, BilCashReceipt bilCashReceipt, BilAccount bilAccount,
            BilPolicyTerm bilPolicyTerm, boolean creditReceipt) {

        String accountId = bilCashDsp.getBilCashDspId().getAccountId();
        String bilSttDtlTypCd = bilCashDsp.getStatementDetailTypeCd();
        String payableItemCd = bilCashDsp.getPayableItemCd();
        String prevPolId = BLANK_STRING;
        ZonedDateTime prevPolEffDt = null;

        List<Character> priorityIndicatorList = bilIstScheduleRepository.getPaymentPriorityRows(accountId,
                adjustDueDate, billSysDueDt, policyId, BLANK_STRING, payableItemCd, typeSpace, invoiceCommissionPersent,
                DECIMAL_ZERO, bilSttDtlTypCd, typeSpace, CURRENT_INVOICE_CODES);

        if (priorityIndicatorList != null && !priorityIndicatorList.isEmpty()) {
            for (Character priorityIndicator : priorityIndicatorList) {
                List<BilIstSchedule> bilIstScheduleList = null;
                Double priorityAmount = bilIstScheduleRepository.getPriorityAmount(accountId, adjustDueDate,
                        billSysDueDt, policyId, BLANK_STRING, payableItemCd, typeSpace, invoiceCommissionPersent,
                        DECIMAL_ZERO, bilSttDtlTypCd, typeSpace, CURRENT_INVOICE_CODES, priorityIndicator);
                if (priorityAmount == null) {
                    priorityAmount = DECIMAL_ZERO;
                }
                if (originalCashAmount >= priorityAmount) {
                    if (cashApply.getCreditPolicyId().trim().isEmpty()) {
                        bilIstScheduleList = bilIstScheduleRepository.findPaymentPriorityRows(
                                bilCashDsp.getBilCashDspId().getAccountId(), adjustDueDate, billSysDueDt, policyId,
                                BLANK_STRING, bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent,
                                DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace, priorityIndicator,
                                CURRENT_INVOICE_CODES, DateRoutine.defaultDateTime(), billSysDueDt);
                    } else {
                        bilIstScheduleList = bilIstScheduleRepository.findPartialAdjustRowsByPolicy(
                                bilCashDsp.getBilCashDspId().getAccountId(), adjustDueDate, billSysDueDt, policyId,
                                BLANK_STRING, bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent,
                                DECIMAL_ZERO, bilCashDsp.getStatementDetailTypeCd(), typeSpace, CURRENT_INVOICE_CODES,
                                DateRoutine.defaultDateTime(), BilPolicyStatusCode.QUOTE.getValue(), priorityIndicator,
                                billSysDueDt);
                    }

                    if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
                        for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                            Object[] fullpayEachRow = processFullpayEachRow(originalCashAmount, bilIstSchedule,
                                    bilAccount, bilCashDsp, cashApply, bilCashReceipt, bilPolicyTerm, creditReceipt,
                                    prevPolId, prevPolEffDt);
                            originalCashAmount = (double) fullpayEachRow[0];
                            prevPolId = (String) fullpayEachRow[1];
                            prevPolEffDt = (ZonedDateTime) fullpayEachRow[2];
                            if (originalCashAmount == DECIMAL_ZERO) {
                                break;
                            }
                        }
                    }

                } else {
                    double originalPayment = originalCashAmount;
                    double applicationAmt = originalCashAmount;
                    applicationAmt = processPaymentPriorityRows(bilCashDsp, originalCashAmount, adjustDueDate,
                            billSysDueDt, typeSpace, invoiceCommissionPersent, policyId, false, priorityIndicator,
                            priorityAmount, cashApply, bilCashReceipt, bilAccount, bilPolicyTerm, creditReceipt, false,
                            originalPayment, applicationAmt);
                    originalCashAmount = applicationAmt;
                    if (originalCashAmount == DECIMAL_ZERO) {
                        break;
                    }
                }
            }
        }

        return originalCashAmount;

    }

    private double processPaymentPriorityRows(BilCashDsp bilCashDsp, double originalCashAmount,
            ZonedDateTime adjustDueDate, ZonedDateTime billSysDueDt, String typeSpace, double invoiceCommissionPersent,
            String policyId, boolean presentOrFuture, char priorityIndicator, Double priorityAmount,
            CashApply cashApply, BilCashReceipt bilCashReceipt, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm,
            boolean creditReceipt, boolean futureSwitch, double originalPayment, double applicationAmt) {

        List<Character> invoiceCodeList = null;
        List<BilIstSchedule> bilIstScheduleList = null;
        String prevPolId = BLANK_STRING;
        ZonedDateTime prevPolEffDt = null;

        if (cashApply.getCreditPolicyId().trim().isEmpty()) {
            bilIstScheduleList = bilIstScheduleRepository.findPaymentPriorityRows(
                    bilCashDsp.getBilCashDspId().getAccountId(), adjustDueDate, billSysDueDt, policyId, BLANK_STRING,
                    bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO,
                    bilCashDsp.getStatementDetailTypeCd(), typeSpace, priorityIndicator, CURRENT_INVOICE_CODES,
                    DateRoutine.defaultDateTime(), billSysDueDt);
        } else {
            bilIstScheduleList = bilIstScheduleRepository.findPartialAdjustRowsByPolicy(
                    bilCashDsp.getBilCashDspId().getAccountId(), adjustDueDate, billSysDueDt, policyId, BLANK_STRING,
                    bilCashDsp.getPayableItemCd(), typeSpace, invoiceCommissionPersent, DECIMAL_ZERO,
                    bilCashDsp.getStatementDetailTypeCd(), typeSpace, CURRENT_INVOICE_CODES,
                    DateRoutine.defaultDateTime(), BilPolicyStatusCode.QUOTE.getValue(), priorityIndicator,
                    billSysDueDt);
        }

        if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
            for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {

                double istDuePaidAmt = DECIMAL_ZERO;
                if (futureSwitch) {
                    invoiceCodeList = FUTURE_INVOICE_CODES;
                } else {
                    invoiceCodeList = CURRENT_INVOICE_CODES;
                }
                Double itemAmount = bilIstScheduleRepository.getPriorityAmountByPolicyId(
                        bilIstSchedule.getBillIstScheduleId().getBillAccountId(),
                        bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                        bilIstSchedule.getBillIstScheduleId().getBillSeqNbr(), bilIstSchedule.getBillSysDueDt(),
                        bilIstSchedule.getBillAdjDueDt(), bilIstSchedule.getBillIstDueDt(),
                        bilIstSchedule.getBillPblItemCd(), bilIstSchedule.getBillPayPtyCd(), invoiceCodeList);
                Double openAmount = bilIstSchedule.getBisCncFutAmt() + bilIstSchedule.getBisCreFutAmt()
                        + bilIstSchedule.getBisCncOpnAmt() + bilIstSchedule.getBillPrmIstAmt()
                        + bilIstSchedule.getBisCreOpnAmt();
                Double paidAmount = bilIstSchedule.getBisWroPrmAmt() + bilIstSchedule.getBisPrmPaidAmt()
                        + bilIstSchedule.getBisCrePaidAmt();

                openAmount = openAmount - paidAmount;
                double quotient = itemAmount / priorityAmount;
                if (quotient <= DECIMAL_ZERO) {
                    continue;
                }

                Object[] partialAmounts = applyCash(bilIstSchedule, quotient, originalPayment, presentOrFuture,
                        cashApply, creditReceipt, bilCashDsp, bilCashReceipt, bilAccount, bilPolicyTerm, applicationAmt,
                        originalCashAmount, istDuePaidAmt, prevPolId, prevPolEffDt);

                originalCashAmount = (double) partialAmounts[0];
                applicationAmt = (double) partialAmounts[1];
                prevPolId = (String) partialAmounts[3];
                prevPolEffDt = (ZonedDateTime) partialAmounts[4];

                if (originalCashAmount == DECIMAL_ZERO) {
                    break;
                }
            }
        }

        return applicationAmt;

    }

    private Object[] processEquityFutureRows(double originalCashAmount, BilIstSchedule bilIstSchedule,
            BilAccount bilAccount, BilCashDsp bilCashDsp, CashApply cashApply, BilCashReceipt bilCashReceipt,
            BilPolicyTerm bilPolicyTerm, String prevPolId, ZonedDateTime prevPolEffDt) {

        double dispositionAmount;
        double premiumPaidAmount;
        double creditPaidAmount;
        double openCashPaid = DECIMAL_ZERO;
        double openGrossAmount = DECIMAL_ZERO;
        double openFutureAmount = DECIMAL_ZERO;

        Double openAmount = bilIstSchedule.getBisCncFutAmt() + bilIstSchedule.getBisCreFutAmt()
                + bilIstSchedule.getBisCncOpnAmt() + bilIstSchedule.getBillPrmIstAmt()
                + bilIstSchedule.getBisCreOpnAmt();
        Double paidAmount = bilIstSchedule.getBisWroPrmAmt() + bilIstSchedule.getBisPrmPaidAmt()
                + bilIstSchedule.getBisCrePaidAmt();
        openAmount = openAmount - paidAmount;

        if (openAmount > originalCashAmount) {
            openAmount = originalCashAmount;
            originalCashAmount = DECIMAL_ZERO;
        } else {
            originalCashAmount = originalCashAmount - openAmount;
        }
        creditPaidAmount = bilIstSchedule.getBisCrePaidAmt();
        premiumPaidAmount = openAmount + bilIstSchedule.getBisPrmPaidAmt();

        openCashPaid = openCashPaid + openAmount;
        openFutureAmount = openFutureAmount + openAmount;
        dispositionAmount = openAmount;
        if (dispositionAmount != DECIMAL_ZERO) {
            Object[] previousObject = updatePremAndCash(bilCashDsp, bilIstSchedule, dispositionAmount, creditPaidAmount,
                    premiumPaidAmount, openGrossAmount, openCashPaid, openFutureAmount, bilCashReceipt, cashApply,
                    bilAccount, bilPolicyTerm, prevPolId, prevPolEffDt);
            prevPolId = (String) previousObject[0];
            prevPolEffDt = (ZonedDateTime) previousObject[1];
        }
        Object[] equityFuture = { originalCashAmount, prevPolId, prevPolEffDt };
        cashApply.setApplyReference("AD");
        return equityFuture;
    }

    private Object[] processFullpayEachRow(double originalCashAmount, BilIstSchedule bilIstSchedule,
            BilAccount bilAccount, BilCashDsp bilCashDsp, CashApply cashApply, BilCashReceipt bilCashReceipt,
            BilPolicyTerm bilPolicyTerm, boolean creditReceipt, String prevPolId, ZonedDateTime prevPolEffDt) {

        BigDecimal dispositionAmount;
        BigDecimal premiumPaidAmount;
        BigDecimal creditPaidAmount;
        BigDecimal openGrossAmount = BigDecimal.valueOf(0.00);
        BigDecimal openCashPaid = BigDecimal.valueOf(0.00);
        BigDecimal openFutureAmount = BigDecimal.valueOf(0.00);
        BigDecimal cashAmount = BigDecimal.valueOf(originalCashAmount);
        Object[] previousObject = null;

        BigDecimal openAmount = BigDecimal.valueOf(
                bilIstSchedule.getBisCncFutAmt() + bilIstSchedule.getBisCreFutAmt() + bilIstSchedule.getBisCncOpnAmt()
                        + bilIstSchedule.getBillPrmIstAmt() + bilIstSchedule.getBisCreOpnAmt());
        BigDecimal paidAmount = BigDecimal.valueOf(bilIstSchedule.getBisWroPrmAmt() + bilIstSchedule.getBisPrmPaidAmt()
                + bilIstSchedule.getBisCrePaidAmt());
        openAmount = openAmount.subtract(paidAmount);

        if (openAmount.compareTo(cashAmount) > 0) {
            openAmount = cashAmount;
            cashAmount = BIGDECIMAL_ZERO;
        }

        if (creditReceipt) {
            creditPaidAmount = openAmount.add(BigDecimal.valueOf(bilIstSchedule.getBisCrePaidAmt()));
            premiumPaidAmount = BigDecimal.valueOf(bilIstSchedule.getBisPrmPaidAmt());
        } else {
            creditPaidAmount = BigDecimal.valueOf(bilIstSchedule.getBisCrePaidAmt());
            premiumPaidAmount = openAmount.add(BigDecimal.valueOf(bilIstSchedule.getBisPrmPaidAmt()));
        }

        openGrossAmount = openGrossAmount.add(openAmount);
        openCashPaid = openCashPaid.add(openAmount);

        if (cashAmount != BIGDECIMAL_ZERO) {
            cashAmount = cashAmount.subtract(openAmount);
        }

        dispositionAmount = openAmount;
        if (dispositionAmount != BIGDECIMAL_ZERO) {
            previousObject = updatePremAndCash(bilCashDsp, bilIstSchedule, dispositionAmount.doubleValue(),
                    creditPaidAmount.doubleValue(), premiumPaidAmount.doubleValue(), openGrossAmount.doubleValue(),
                    openCashPaid.doubleValue(), openFutureAmount.doubleValue(), bilCashReceipt, cashApply, bilAccount,
                    bilPolicyTerm, prevPolId, prevPolEffDt);
            prevPolId = (String) previousObject[0];
            prevPolEffDt = (ZonedDateTime) previousObject[1];
        }
        originalCashAmount = cashAmount.doubleValue();
        return new Object[] { originalCashAmount, prevPolId, prevPolEffDt };
    }

    private Object[] updatePremAndCash(BilCashDsp bilCashDsp, BilIstSchedule bilIstSchedule, double dispositionAmount,
            double creditPaidAmount, double premiumPaidAmount, double openGrossAmount, double openCashPaid,
            double openFutureAmount, BilCashReceipt bilCashReceipt, CashApply cashApply, BilAccount bilAccount,
            BilPolicyTerm bilPolicyTerm, String prevPolId, ZonedDateTime prevPolEffDt) {

        bilIstSchedule.setBisPrmPaidAmt(
                BigDecimal.valueOf(premiumPaidAmount).setScale(2, RoundingMode.HALF_EVEN).doubleValue());
        bilIstSchedule.setBisCrePaidAmt(
                BigDecimal.valueOf(creditPaidAmount).setScale(2, RoundingMode.HALF_EVEN).doubleValue());
        bilIstScheduleRepository.saveAndFlush(bilIstSchedule);
        BilPolicy bilPolicy = null;

        if (bilPolicyTerm.getBillPolicyTermId().getPolicyId().trim().isEmpty()
                || bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt() != bilIstSchedule.getPolicyEffectiveDt()) {
            if (!bilIstSchedule.getBillIstScheduleId().getPolicyId().isEmpty()) {
                bilPolicyTerm = bilPolicyTermRepository
                        .findById(new BilPolicyTermId(bilCashDsp.getBilCashDspId().getAccountId(),
                                bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                                bilIstSchedule.getPolicyEffectiveDt()))
                        .orElse(null);
                bilPolicy = bilPolicyRepository
                        .findById(new BilPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                bilPolicyTerm.getBillPolicyTermId().getBilAccountId()))
                        .orElse(null);
            } else {
                bilPolicy = new BilPolicy();
            }

        } else {
            bilPolicy = bilPolicyRepository.findById(new BilPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getBilAccountId())).orElse(null);
        }

        insertCash(bilCashDsp, bilIstSchedule.getBillIstScheduleId().getBillSeqNbr(), bilIstSchedule.getBillAdjDueDt(),
                bilIstSchedule.getBillInvDt(), BLANK_CHAR, cashApply.getCashApplyDate(), dispositionAmount,
                bilIstSchedule.getBillSysDueDt(), bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), bilIstSchedule.getPolicyEffectiveDt(),
                bilIstSchedule.getBillPblItemCd(), cashApply.getUserSequenceId());

        String orginalEfftiveDate;
        String bilDatabaseKey = BLANK_STRING;
        String referenceDate;

        if (bilPolicyTerm.getBillPolicyTermId().getPolicyId().trim().isEmpty()) {
            orginalEfftiveDate = BLANK_STRING;
            referenceDate = DateRoutine.dateTimeAsYYYYMMDDString(cashApply.getCashApplyDate());
        } else {
            orginalEfftiveDate = DateRoutine.dateTimeAsYYYYMMDDString(bilIstSchedule.getPolicyEffectiveDt());
            referenceDate = DateRoutine
                    .dateTimeAsYYYYMMDDString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
        }

        char quoteIndicator = cashApply.getQuoteIndicator().charAt(SHORT_ZERO);

        if (openCashPaid > DECIMAL_ZERO && quoteIndicator != BilPolicyStatusCode.QUOTE.getValue()) {

            bilDatabaseKey = getBilDatabaseKey(bilCashReceipt.getEntryDate(), bilCashReceipt.getEntryNumber(),
                    bilCashReceipt.getUserId(), bilCashReceipt.getEntrySequenceNumber());

            callFinancialApi(openCashPaid, openCashPaid, cashApply.getCashApplyDate(), bilCashReceipt.getBankCd(),
                    bilAccount.getAccountNumber(), BilCashEntryMethodCode.CASH.getValue(),
                    BilCashEntryMethodCode.APPLY.getValue(), bilIstSchedule.getBillPblItemCd(),
                    String.valueOf(bilCashReceipt.getCashEntryMethodCd()), bilCashReceipt.getReceiptTypeCd(),
                    BLANK_STRING, bilAccount, bilPolicyTerm.getBillCountryCd(), bilPolicyTerm.getLobCd(),
                    orginalEfftiveDate, bilPolicyTerm, bilDatabaseKey, referenceDate, cashApply.getUserSequenceId());
        }

        if (openGrossAmount > DECIMAL_ZERO && quoteIndicator != BilPolicyStatusCode.QUOTE.getValue()) {
            String[] fwsCodes = calculateChargeActions(BLANK_CHAR, bilIstSchedule.getBillInvoiceCd(), openGrossAmount,
                    false);
            double netAmount = openGrossAmount - bilIstSchedule.getBillCommissionPct() / 100 * openGrossAmount;
            if (bilCashReceipt.getReceiptTypeCd().trim().equals(BilReceiptTypeCode.GROSS_PAYMENT.getValue())
                    || bilCashReceipt.getReceiptTypeCd().trim().equals(BilReceiptTypeCode.GROSS_WRITEOFF.getValue())
                    || bilCashReceipt.getReceiptTypeCd().trim().equals(BilReceiptTypeCode.GROSS_OVERPAY.getValue())) {
                netAmount = openGrossAmount;
            }
            if (fwsCodes[0].compareTo(ObjectCode.PREMIUM.toString()) == 0
                    && (fwsCodes[1].compareTo(ActionCode.APPLY_TO_INSTALL.toString()) == 0
                            || fwsCodes[1].compareTo(ActionCode.APPLY_TO_UNBILLED.toString()) == 0)) {
                bilDatabaseKey = getBilDatabaseKey(bilCashReceipt.getEntryDate(), bilCashReceipt.getEntryNumber(),
                        bilCashReceipt.getUserId(), bilCashReceipt.getEntrySequenceNumber());
            }

            callFinancialApi(openGrossAmount, netAmount, cashApply.getCashApplyDate(), bilCashReceipt.getBankCd(),
                    bilAccount.getAccountNumber(), fwsCodes[0], fwsCodes[1], bilIstSchedule.getBillPblItemCd(),
                    String.valueOf(bilCashReceipt.getCashEntryMethodCd()), bilCashReceipt.getReceiptTypeCd(),
                    BLANK_STRING, bilAccount, bilPolicyTerm.getBillCountryCd(), bilPolicyTerm.getLobCd(),
                    orginalEfftiveDate, bilPolicyTerm, bilDatabaseKey, referenceDate, cashApply.getUserSequenceId());

            if (checkCommissionAuthActive(fwsCodes[0], fwsCodes[1])) {
                AuthorizeOption authorizeOption = new AuthorizeOption();
                authorizeOption.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
                authorizeOption
                        .setActionCode(ObjectCode.PREMIUM.toString().concat(ActionCode.APPLY_TO_UNBILLED.toString()));
                authorizeOption.setAmount(openGrossAmount);
                authorizeOption.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDD(orginalEfftiveDate));
                authorizeOption.setCommissionPercentage(BigDecimal.valueOf(bilIstSchedule.getBillCommissionPct())
                        .divide(new BigDecimal(100)).doubleValue());
                commissionsRestService.save(authorizeOption);
            }
        }

        if (openFutureAmount > DECIMAL_ZERO && quoteIndicator != BilPolicyStatusCode.QUOTE.getValue()) {
            String[] fwsCodes = calculateChargeActions(BLANK_CHAR, bilIstSchedule.getBillInvoiceCd(), openFutureAmount,
                    false);
            double netAmount = openFutureAmount - bilIstSchedule.getBillCommissionPct() / 100 * openFutureAmount;
            if (fwsCodes[0].compareTo(ObjectCode.PREMIUM.toString()) == 0
                    && (fwsCodes[1].compareTo(ActionCode.APPLY_TO_INSTALL.toString()) == 0
                            || fwsCodes[1].compareTo(ActionCode.APPLY_TO_UNBILLED.toString()) == 0)) {
                bilDatabaseKey = getBilDatabaseKey(bilCashReceipt.getEntryDate(), bilCashReceipt.getEntryNumber(),
                        bilCashReceipt.getUserId(), bilCashReceipt.getEntrySequenceNumber());
                callFinancialApi(openFutureAmount, netAmount, cashApply.getCashApplyDate(), bilCashReceipt.getBankCd(),
                        bilAccount.getAccountNumber(), fwsCodes[0], fwsCodes[1], bilIstSchedule.getBillPblItemCd(),
                        String.valueOf(bilCashReceipt.getCashEntryMethodCd()), bilCashReceipt.getReceiptTypeCd(),
                        BLANK_STRING, bilAccount, bilPolicyTerm.getBillCountryCd(), bilPolicyTerm.getLobCd(),
                        orginalEfftiveDate, bilPolicyTerm, bilDatabaseKey, referenceDate, cashApply.getUserSequenceId());
                if (checkCommissionAuthActive(fwsCodes[0], fwsCodes[1])) {
                    AuthorizeOption authorizeOption = new AuthorizeOption();
                    authorizeOption.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
                    authorizeOption.setActionCode(
                            ObjectCode.PREMIUM.toString().concat(ActionCode.APPLY_TO_UNBILLED.toString()));
                    authorizeOption.setAmount(openGrossAmount);
                    authorizeOption.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDD(orginalEfftiveDate));
                    authorizeOption.setCommissionPercentage(BigDecimal.valueOf(bilIstSchedule.getBillCommissionPct())
                            .divide(new BigDecimal(100)).doubleValue());
                    commissionsRestService.save(authorizeOption);
                }
            }
        }

        char fullPmtDscCd;
        double appliedAmount;
        double pifDscTolAmt = DECIMAL_ZERO;

        postEventRecord(bilPolicyTerm, bilCashDsp, bilCashReceipt, bilAccount, bilIstSchedule, bilPolicy.getPolNbr(),
                bilPolicy.getPolSymbolCd(), cashApply.getCashApplyDate());

        if (bilPolicyTerm.getFullPmtDscCd() == CHAR_O && bilPolicyTerm.getBillCurDscAmt() > DECIMAL_ZERO) {
            fullPmtDscCd = bilPolicyTerm.getFullPmtDscCd();
            if (!prevPolId.equals(bilIstSchedule.getBillIstScheduleId().getPolicyId())
                    && prevPolEffDt != bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()) {

                if (bilPolicyTerm.getBptTermSplitInd() != CHAR_Y) {
                    Object[] pifDsc = evalPifDsc(bilCashDsp, bilPolicyTerm, bilIstSchedule,
                            cashApply.getCashApplyDate(), bilCashReceipt);
                    fullPmtDscCd = (char) pifDsc[SHORT_ZERO];
                    pifDscTolAmt = (double) pifDsc[SHORT_ONE];

                } else {
                    if (bilPolicyTerm.getBptTermSplitInd() == CHAR_Y) {
                        fullPmtDscCd = CHAR_N;
                    }
                }
            }

            if (fullPmtDscCd == CHAR_O) {
                appliedAmount = calcCashApplied(bilCashDsp, bilPolicyTerm, bilIstSchedule);
                if (appliedAmount >= pifDscTolAmt) {
                    fullPmtDscCd = CHAR_Y;
                }
            }
            if (fullPmtDscCd == CHAR_Y || fullPmtDscCd == CHAR_N) {
                uptPifDsc(bilPolicyTerm, fullPmtDscCd, bilCashDsp, bilIstSchedule, cashApply, bilPolicy);
            }
        }

        prevPolId = bilIstSchedule.getBillIstScheduleId().getPolicyId();
        prevPolEffDt = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
        Object[] previousObject = { prevPolId, prevPolEffDt };

        bilPolicyTermRepository.updateWroPrcIndicator(new BilPolicyTermId(bilCashDsp.getBilCashDspId().getAccountId(),
                bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()), CHAR_Y, BLANK_CHAR);

        return previousObject;

    }

    private void postEventRecord(BilPolicyTerm bilPolicyTermRow, BilCashDsp bilCashDspRow,
            BilCashReceipt bilCashReceiptRow, BilAccount bilAccountRow, BilIstSchedule bilIstSchedule,
            String policyNumber, String policySymbol, ZonedDateTime currentDate) {
        final String postEventBusObjCash = "BIL-POST-EVENT-CASH-";
        final String invokeProgram = "BCMOBE01";
        Event event = new Event();
        event.setIssueSystemCode(
                bilPolicyTermRow.getBptIssueSysId().isEmpty() ? BillingConstants.EventIssueSystemCode.UWS.getValue()
                        : bilPolicyTermRow.getBptIssueSysId());
        String postEventBusObjName = postEventBusObjCash.concat(event.getIssueSystemCode());
        event.setEventCode(BillingConstants.EventCode.EVENT_PREM_PYMT.getValue());
        event.setPolicyId(bilIstSchedule.getBillIstScheduleId().getPolicyId());
        event.setPolicyEffectiveDate(bilPolicyTermRow.getBillPolicyTermId().getPolEffectiveDt());
        event.setPlannedExpirationDate(bilPolicyTermRow.getPlnExpDt());
        event.setPolicySymbolCode(policySymbol.trim());
        event.setPolicyNumber(policyNumber.trim());
        event.setLineOfBusinessCode(bilPolicyTermRow.getLobCd());
        event.setMasterCompanyNumber(bilPolicyTermRow.getMasterCompanyNbr());
        event.setStateProvinceCode(bilPolicyTermRow.getBillStatePvnCd());
        event.setCountryCode(bilPolicyTermRow.getBillCountryCd());
        event.setPolicyStatusCode(String.valueOf(bilPolicyTermRow.getBillPolStatusCd()));
        event.setBilPlanCode(bilPolicyTermRow.getBillPlanCd());
        event.setPaymentReceiveCode(bilCashDspRow.getPayableItemCd());
        event.setBilAccountId(bilAccountRow.getAccountId());
        event.setBilTechKeyId(bilAccountRow.getAccountId());
        event.setBilTechKeyTypeCode(CHAR_A);
        event.setBilAccountNumber(bilAccountRow.getAccountNumber());
        event.setBilTypeCode(bilAccountRow.getBillTypeCd());
        event.setBilClassCode(bilAccountRow.getBillClassCd());
        event.setCollectionMethod(bilAccountRow.getCollectionMethod());
        event.setCollectionPlan(bilAccountRow.getCollectionPlan());
        event.setBilThirdPartyNumber(bilAccountRow.getThirdPartyIdentifier());
        event.setBilAdditionalId(bilAccountRow.getAdditionalId());
        event.setPaymentInstallmentDueDate(bilIstSchedule.getBillIstDueDt());
        event.setPaymentDepositTypeCode(BilDspTypeCode.APPLIED.getValue());
        event.setPaymentReceiveCode(bilIstSchedule.getBillPblItemCd());
        event.setPaymentDepositAmount(bilCashDspRow.getDspAmount());
        event.setPaymentEntryDate(bilCashReceiptRow.getEntryDate());
        event.setPaymentLoanDate(DateRoutine.defaultDateTime());
        if (!bilCashDspRow.getPayableItemCd().isEmpty()) {
            BilAmtTrmRlt bilAmtTrmRlt = bilAmtTrmRltRepository.getLoanInceptionDate(bilAccountRow.getAccountId(),
                    bilIstSchedule.getBillIstScheduleId().getPolicyId(), bilCashDspRow.getPayableItemCd(),
                    bilCashDspRow.getBilSequenceNumber());
            if (bilAmtTrmRlt != null) {
                event.setPaymentLoanDate(bilAmtTrmRlt.getRltGnrc01Dt());
            }
        }
        event.setProgramName(ApplicationName.BCMOCX.toString());
        event.setReceivableCode(bilIstSchedule.getBillPblItemCd());
        event.setReceiptTypeCode(bilCashReceiptRow.getReceiptTypeCd());
        event.setBilDepositDate(bilCashDspRow.getDspDate());
        event.setBussinessObjectName(postEventBusObjName);
        event.setEventKeyId(event.getBilAccountId());
        event.setEventKeyTypeCode(BillingConstants.EventKeyTypeCode.BILLACCOUNT.getValue());
        event.setEventStatusCode(CHAR_P);
        event.setEffectiveDate(currentDate);
        event.setEventMduNm(ApplicationName.BCMOCX.toString());
        event.setRequestProgramName(ApplicationName.BCMOCX.toString());

        HalBoMduXrf halBoMduXrf = halBoMduXrfRepository.findById(postEventBusObjName).orElse(null);
        if (halBoMduXrf != null) {
            String programName = halBoMduXrf.getHbmxBobjMduNm().trim();
            if (programName.isEmpty()) {
                logger.error("Program name is not found in HAL_BO_MDU_XRF. {}", postEventBusObjName);
            } else if (programName.equals(invokeProgram)) {
                eventPostingService.postEvent(event);
            }
        } else {
            logger.error("Program name is not found in HAL_BO_MDU_XRF. {}", postEventBusObjName);
        }
    }

    private Object[] evalPifDsc(BilCashDsp bilCashDsp, BilPolicyTerm bilPolicyTerm, BilIstSchedule bilIstSchedule,
            ZonedDateTime applyDate, BilCashReceipt bilCashReceipt) {
        Pageable pageable = PageRequest.of(SHORT_ZERO, SHORT_ONE);
        char polTermTypeCd;
        char fullPmtDscCd = bilPolicyTerm.getFullPmtDscCd();
        ZonedDateTime pifPmtDt = null;
        ZonedDateTime pifQualifyDt = null;
        double pifDscTolAmt = DECIMAL_ZERO;

        List<BilAmounts> bilAmountsList = bilAmountsRepository.findMaxAmountRow(
                bilCashDsp.getBilCashDspId().getAccountId(), bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), getBilAmtEffIndList(), CHAR_Y, pageable);

        if (bilAmountsList != null && !bilAmountsList.isEmpty()) {

            polTermTypeCd = bilAmountsList.get(SHORT_ZERO).getBilAmtEffInd();
            if (polTermTypeCd == CHAR_N || polTermTypeCd == CHAR_M) {
                polTermTypeCd = CHAR_N;
            } else {
                polTermTypeCd = CHAR_R;
            }

            BilFullPmtDsc bilFullPmtDsc = readBilFullPmtDsc(bilPolicyTerm, polTermTypeCd, applyDate);
            if (bilFullPmtDsc == null) {
                fullPmtDscCd = CHAR_N;
                return new Object[] { fullPmtDscCd, pifDscTolAmt };
            }

            List<Object[]> adjDueDateRange = bilIstScheduleRepository.fetchAdjDueDateRange(
                    bilCashDsp.getBilCashDspId().getAccountId(), CURRENT_INVOICE_CODES,
                    bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());

            if (adjDueDateRange != null && !adjDueDateRange.isEmpty()
                    && adjDueDateRange.get(0)[0] != adjDueDateRange.get(0)[1]) {
                fullPmtDscCd = CHAR_N;
                return new Object[] { fullPmtDscCd, pifDscTolAmt };
            }

            if (bilFullPmtDsc.getBrdPstMrkDtInd() == CHAR_Y) {
                pifPmtDt = bilCashReceipt.getPostMarkDate();
            } else {
                pifPmtDt = bilCashReceipt.getReceiptReceiveDate();
            }

            if (bilFullPmtDsc.getBilPmtQfyCd() == CHAR_A) {
                pifQualifyDt = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
            } else {
                if (bilFullPmtDsc.getBilPmtQfyCd() == CHAR_B) {
                    if (adjDueDateRange == null) {
                        pifQualifyDt = bilIstScheduleRepository.fetchMinAdjDueDate(
                                bilCashDsp.getBilCashDspId().getAccountId(),
                                bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                    }

                } else if (adjDueDateRange != null && !adjDueDateRange.isEmpty()) {
                    pifQualifyDt = (ZonedDateTime) adjDueDateRange.get(0)[0];
                }
            }

            if (pifPmtDt.compareTo(pifQualifyDt) > 0) {
                fullPmtDscCd = CHAR_N;
                return new Object[] { fullPmtDscCd, pifDscTolAmt };
            }

            if (bilFullPmtDsc.getBilFlpDscAmtCd() == CHAR_A) {
                pifDscTolAmt = bilPolicyTerm.getBillCurDscAmt() * bilFullPmtDsc.getBilDscPaidPct() / 100;
            } else if (bilFullPmtDsc.getBilFlpDscAmtCd() == CHAR_B) {
                if (bilPolicyTerm.getBillCurDscAmt() > bilPolicyTerm.getBillInvDscAmt()
                        && bilPolicyTerm.getBillInvDscAmt() > DECIMAL_ZERO) {
                    pifDscTolAmt = bilPolicyTerm.getBillInvDscAmt() * bilFullPmtDsc.getBilDscPaidPct() / 100;
                } else {
                    pifDscTolAmt = bilPolicyTerm.getBillCurDscAmt() * bilFullPmtDsc.getBilDscPaidPct() / 100;
                }
            }

        }

        return new Object[] { fullPmtDscCd, pifDscTolAmt };
    }

    private BilFullPmtDsc readBilFullPmtDsc(BilPolicyTerm bilPolicyTerm, char polTermTypeCd, ZonedDateTime applyDate) {

        List<BilFullPmtDsc> bilFullPmtDscList = null;

        bilFullPmtDscList = bilFullPmtDscRepository.findBilFullPmtDscRow(bilPolicyTerm.getBillStatePvnCd(),
                bilPolicyTerm.getLobCd(), bilPolicyTerm.getMasterCompanyNbr(), polTermTypeCd,
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), applyDate);

        if (bilFullPmtDscList != null && !bilFullPmtDscList.isEmpty()) {
            return bilFullPmtDscList.get(SHORT_ZERO);
        }
        bilFullPmtDscList = bilFullPmtDscRepository.findBilFullPmtDscRow(bilPolicyTerm.getBillStatePvnCd(),
                bilPolicyTerm.getLobCd(), "99", polTermTypeCd, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                applyDate);
        if (bilFullPmtDscList != null && !bilFullPmtDscList.isEmpty()) {
            return bilFullPmtDscList.get(SHORT_ZERO);
        }

        bilFullPmtDscList = bilFullPmtDscRepository.findBilFullPmtDscRow(bilPolicyTerm.getBillStatePvnCd(),
                SEPARATOR_BLANK, "99", polTermTypeCd, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                applyDate);

        if (bilFullPmtDscList != null && !bilFullPmtDscList.isEmpty()) {
            return bilFullPmtDscList.get(SHORT_ZERO);
        }

        bilFullPmtDscList = bilFullPmtDscRepository.findBilFullPmtDscRow(SEPARATOR_BLANK, SEPARATOR_BLANK, "99",
                polTermTypeCd, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), applyDate);

        if (bilFullPmtDscList != null && !bilFullPmtDscList.isEmpty()) {
            return bilFullPmtDscList.get(SHORT_ZERO);
        }

        return null;
    }

    private List<Character> getBilAmtEffIndList() {
        List<Character> bilAmtEffIndList = new ArrayList<>();
        bilAmtEffIndList.add(BilAmountsEftIndicator.NEW_BUSINESS.getValue());
        bilAmtEffIndList.add(BilAmountsEftIndicator.NEWBUSINESS.getValue());
        bilAmtEffIndList.add(BilAmountsEftIndicator.REVERSE_AUDIT_OPERATOR.getValue());
        bilAmtEffIndList.add(BilAmountsEftIndicator.RENEWAL.getValue());
        bilAmtEffIndList.add(BilAmountsEftIndicator.RENEWAL_UNDER.getValue());
        bilAmtEffIndList.add(BilAmountsEftIndicator.RENEWALS.getValue());
        bilAmtEffIndList.add(BilAmountsEftIndicator.REISSUE_NEW_TERM.getValue());

        return bilAmtEffIndList;
    }

    private double calcCashApplied(BilCashDsp bilCashDsp, BilPolicyTerm bilPolicyTerm, BilIstSchedule bilIstSchedule) {
        Double appliedAmount = bilCashDspRepository.getDspAmountForApplied(bilCashDsp.getBilCashDspId().getAccountId(),
                BilDspTypeCode.APPLIED.getValue(), bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), BLANK_CHAR);
        if (appliedAmount == null) {
            return DECIMAL_ZERO;
        } else {
            return appliedAmount;
        }

    }

    private void uptPifDsc(BilPolicyTerm bilPolicyTerm, char fullPmtDscCd, BilCashDsp bilCashDsp,
            BilIstSchedule bilIstSchedule, CashApply cashApply, BilPolicy bilPolicy) {
        bilPolicyTermRepository.updateFullPmtDscCd(bilPolicyTerm.getBillPolicyTermId(), fullPmtDscCd);
        if (fullPmtDscCd == CHAR_Y) {
            insertBilActSummary(bilPolicyTerm, cashApply.getCashApplyDate(), bilPolicy, cashApply.getUserSequenceId());
        }
        bilPolActivityRepository.deleteActivityRow(new BilPolActivityId(bilCashDsp.getBilCashDspId().getAccountId(),
                bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "PDX"));

    }

    private boolean determineExcludeRows(String accountId, ZonedDateTime adjustDueDate, ZonedDateTime billSysDueDt,
            String payableItemCd, String typeSpace, boolean secondTermExcludeSw, boolean dueDateInvoiceSw) {
        Integer count = bilIstScheduleRepository.findExludeRows(accountId, payableItemCd, typeSpace, adjustDueDate,
                billSysDueDt);
        if (count == null || count == SHORT_ZERO) {
            return secondTermExcludeSw;
        }

        List<Object[]> excludePolicyList = bilIstScheduleRepository.getExcludePolicyRows(accountId, payableItemCd,
                typeSpace, adjustDueDate, billSysDueDt, INVOICE_CODES);

        boolean multipleRowsSw = false;
        String excludePolicyId = BLANK_STRING;

        for (Object[] excludePolicyObject : excludePolicyList) {
            if (excludePolicyId.equals(excludePolicyObject[0])) {
                if (!multipleRowsSw) {
                    multipleRowsSw = true;
                    secondTermExcludeSw = excludeOtherTerms(secondTermExcludeSw, excludePolicyId, accountId,
                            payableItemCd, typeSpace, adjustDueDate, billSysDueDt, dueDateInvoiceSw);
                    secondTermExcludeSw = checkForRNT(excludePolicyId, accountId, payableItemCd, typeSpace,
                            adjustDueDate, billSysDueDt, secondTermExcludeSw, dueDateInvoiceSw);
                    secondTermExcludeSw = checkForCRW(excludePolicyId, accountId, payableItemCd, typeSpace,
                            adjustDueDate, billSysDueDt, secondTermExcludeSw, dueDateInvoiceSw);
                }
            } else {
                if (!excludePolicyId.isEmpty() && !multipleRowsSw) {

                    secondTermExcludeSw = checkForRNT(excludePolicyId, accountId, payableItemCd, typeSpace,
                            adjustDueDate, billSysDueDt, secondTermExcludeSw, dueDateInvoiceSw);
                    secondTermExcludeSw = checkForCRW(excludePolicyId, accountId, payableItemCd, typeSpace,
                            adjustDueDate, billSysDueDt, secondTermExcludeSw, dueDateInvoiceSw);
                    excludePolicyId = (String) excludePolicyObject[0];
                    multipleRowsSw = false;
                } else {
                    excludePolicyId = (String) excludePolicyObject[0];
                    multipleRowsSw = false;
                }
            }
        }

        return secondTermExcludeSw;
    }

    private boolean excludeOtherTerms(boolean secondTermExcludeSw, String excludePolicyId, String accountId,
            String payableItemCd, String typeSpace, ZonedDateTime adjustDueDate, ZonedDateTime billSysDueDt,
            boolean dueDateInvoiceSw) {

        ZonedDateTime minEffectiveDate = bilIstScheduleRepository.getMinPolicyEffectiveDt(accountId, excludePolicyId,
                adjustDueDate, billSysDueDt, payableItemCd, typeSpace);
        if (minEffectiveDate != null) {
            secondTermExcludeSw = hideMultipleTermRows(accountId, excludePolicyId, adjustDueDate, billSysDueDt,
                    payableItemCd, typeSpace, secondTermExcludeSw, minEffectiveDate, dueDateInvoiceSw);
        }

        return secondTermExcludeSw;
    }

    private boolean checkForCRW(String excludePolicyId, String accountId, String payableItemCd, String typeSpace,
            ZonedDateTime adjustDueDate, ZonedDateTime billSysDueDt, boolean secondTermExcludeSw,
            boolean dueDateInvoiceSw) {

        Integer count = bilIstScheduleRepository.findCrwRowByPolicyId(accountId, excludePolicyId, BLANK_STRING,
                BLANK_STRING, payableItemCd, typeSpace, adjustDueDate, billSysDueDt,
                PolicyIssueIndicators.CANCELLED_REWRITE.getValue(), INVOICE_CODES);
        if (count != null && count != 0) {
            secondTermExcludeSw = hideScheduleRows(excludePolicyId, accountId, adjustDueDate, billSysDueDt,
                    dueDateInvoiceSw, secondTermExcludeSw);
        }
        return secondTermExcludeSw;
    }

    private boolean checkForRNT(String excludePolicyId, String accountId, String payableItemCd, String typeSpace,
            ZonedDateTime adjustDueDate, ZonedDateTime billSysDueDt, boolean secondTermExcludeSw,
            boolean dueDateInvoiceSw) {

        BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(excludePolicyId, accountId)).orElse(null);

        if (bilPolicy == null) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }

        Integer count = bilIstScheduleRepository.findRNTRowByPolicyId(accountId, excludePolicyId, payableItemCd,
                typeSpace, adjustDueDate, billSysDueDt, PolicyIssueIndicators.CANCELLED_REISSUE.getValue(),
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), INVOICE_CODES);
        if (count != null && count != 0) {
            secondTermExcludeSw = hideScheduleRows(excludePolicyId, accountId, adjustDueDate, billSysDueDt,
                    dueDateInvoiceSw, secondTermExcludeSw);
        }
        return secondTermExcludeSw;
    }

    private Boolean hideScheduleRows(String excludePolicyId, String accountId, ZonedDateTime adjustDueDate,
            ZonedDateTime billSysDueDt, boolean dueDateInvoiceSw, boolean secondTermExcludeSw) {
        Integer count = null;
        if (!dueDateInvoiceSw) {
            count = bilIstScheduleRepository.updateInvoiceRow(accountId, excludePolicyId, adjustDueDate, billSysDueDt,
                    CHAR_L, CHAR_T);
            if (count == null || count == 0) {
                count = bilIstScheduleRepository.updateInvoiceRow(accountId, excludePolicyId, adjustDueDate,
                        billSysDueDt, CHAR_Y, CHAR_U);
                if (count == null || count == 0) {
                    count = bilIstScheduleRepository.updateInvoiceRow(accountId, excludePolicyId, adjustDueDate,
                            billSysDueDt, CHAR_B, CHAR_S);
                    if (count == null || count == 0) {
                        return secondTermExcludeSw;
                    }
                }
            }
        } else {
            count = bilIstScheduleRepository.updateInvoiceRow(accountId, excludePolicyId, adjustDueDate, billSysDueDt,
                    BLANK_CHAR, CHAR_Q);
            if (count == null || count == 0) {
                count = bilIstScheduleRepository.updateInvoiceRow(accountId, excludePolicyId, adjustDueDate,
                        billSysDueDt, CHAR_A, CHAR_R);
                if (count == null || count == 0) {
                    return secondTermExcludeSw;
                }
            }
        }
        if (count > SHORT_ZERO) {
            return true;
        }

        return secondTermExcludeSw;

    }

    private boolean hideMultipleTermRows(String accountId, String excludePolicyId, ZonedDateTime adjustDueDate,
            ZonedDateTime billSysDueDt, String payableItemCd, String typeSpace, boolean secondTermExcludeSw,
            ZonedDateTime minEffectiveDate, boolean dueDateInvoiceSw) {

        Integer count = null;
        if (!dueDateInvoiceSw) {
            count = bilIstScheduleRepository.updateInvoiceRow(accountId, excludePolicyId, adjustDueDate, billSysDueDt,
                    minEffectiveDate, CHAR_L, payableItemCd, typeSpace, CHAR_T);
            if (count == null || count == 0) {
                count = bilIstScheduleRepository.updateInvoiceRow(accountId, excludePolicyId, adjustDueDate,
                        billSysDueDt, minEffectiveDate, CHAR_Y, payableItemCd, typeSpace, CHAR_U);
                if (count == null || count == 0) {
                    count = bilIstScheduleRepository.updateInvoiceRow(accountId, excludePolicyId, adjustDueDate,
                            billSysDueDt, minEffectiveDate, CHAR_B, payableItemCd, typeSpace, CHAR_S);
                    if (count == null || count == 0) {
                        return secondTermExcludeSw;
                    }
                }
            }
        } else {
            count = bilIstScheduleRepository.updateInvoiceRow(accountId, excludePolicyId, adjustDueDate, billSysDueDt,
                    minEffectiveDate, BLANK_CHAR, payableItemCd, typeSpace, CHAR_Q);
            if (count == null || count == 0) {
                count = bilIstScheduleRepository.updateInvoiceRow(accountId, excludePolicyId, adjustDueDate,
                        billSysDueDt, minEffectiveDate, CHAR_A, payableItemCd, typeSpace, CHAR_R);
                if (count == null || count == 0) {
                    return secondTermExcludeSw;
                }
            }
        }

        if (count > SHORT_ZERO) {
            return true;
        }

        return secondTermExcludeSw;
    }

    private void unhideMultipleTermRows(String accountId, ZonedDateTime adjustDueDate, ZonedDateTime billSysDueDt,
            boolean dueDateInvoiceSw) {

        if (!dueDateInvoiceSw) {
            bilIstScheduleRepository.updateInvoiceRow(accountId, adjustDueDate, billSysDueDt, CHAR_T, CHAR_L);
            bilIstScheduleRepository.updateInvoiceRow(accountId, adjustDueDate, billSysDueDt, CHAR_U, CHAR_Y);
            bilIstScheduleRepository.updateInvoiceRow(accountId, adjustDueDate, billSysDueDt, CHAR_S, CHAR_B);

        } else {
            bilIstScheduleRepository.updateInvoiceRow(accountId, adjustDueDate, billSysDueDt, CHAR_Q, BLANK_CHAR);
            bilIstScheduleRepository.updateInvoiceRow(accountId, adjustDueDate, billSysDueDt, CHAR_R, CHAR_A);
        }

    }

    private double checkOldChargeDue(BilAccount bilAccount, BilCashDsp bilCashDsp, double originalCashAmount,
            CashApply cashApply, BilCashReceipt bilCashReceipt, BilPolicyTerm bilPolicyTerm, ZonedDateTime adjustDate) {

        if ((!bilCashReceipt.getReceiptTypeCd().trim().equalsIgnoreCase(BilReceiptTypeCode.EVEN_ADJUSTMENT.getValue()))
                && ((bilCashDsp.getPolicyNumber().trim().compareTo(BLANK_STRING) > SHORT_ZERO
                        || bilCashDsp.getPayableItemCd().trim().compareTo(BLANK_STRING) > SHORT_ZERO)
                        && !bilAccount.getBillTypeCd().trim().equals(BilDspTypeCode.SUSPENDED.getValue()))) {
            return originalCashAmount;

        }
        if (originalCashAmount != DECIMAL_ZERO) {
            originalCashAmount = checkChargeOnly(bilAccount, bilCashDsp, originalCashAmount, cashApply, bilCashReceipt,
                    bilPolicyTerm, true, adjustDate);
        }
        return originalCashAmount;

    }

    private double checkChargeOnly(BilAccount bilAccount, BilCashDsp bilCashDsp, double originalCashAmount,
            CashApply cashApply, BilCashReceipt bilCashReceipt, BilPolicyTerm bilPolicyTerm, boolean oldChargeSw,
            ZonedDateTime adjustDate) {

        List<Object[]> accountChargesTypeCodeList = null;

        if (bilCashReceipt.getReceiptTypeCd().trim().equals(BilReceiptTypeCode.EVEN_ADJUSTMENT.getValue())) {
            return originalCashAmount;
        }
        if ((bilCashDsp.getPolicyNumber().trim().compareTo(BLANK_STRING) > SHORT_ZERO
                || bilCashDsp.getPayableItemCd().trim().compareTo(BLANK_STRING) > SHORT_ZERO)
                && !bilAccount.getBillTypeCd().trim().equals(BilDspTypeCode.SUSPENDED.getValue())) {
            return originalCashAmount;
        }
        if (oldChargeSw) {
            accountChargesTypeCodeList = bilCrgAmountsRepository.findOldServiceChargeCodes(bilAccount.getAccountId(),
                    adjustDate, CURRENT_INVOICE_CODES);
        } else {
            accountChargesTypeCodeList = bilCrgAmountsRepository.findServiceChargeCodes(bilAccount.getAccountId(),
                    bilCashDsp.getAdjustmentDueDate(),
                    DateRoutine.dateTimeAsYYYYMMDDString(bilCashDsp.getAdjustmentDueDate()), DEFAULT_DATE,
                    CURRENT_INVOICE_CODES);

        }

        if (accountChargesTypeCodeList != null && !accountChargesTypeCodeList.isEmpty()) {
            for (Object[] accountChargesTypeCode : accountChargesTypeCodeList) {
                ZonedDateTime dueDate = (ZonedDateTime) accountChargesTypeCode[0];
                char pExist = accountChargesTypeCode[1].toString().charAt(SHORT_ZERO);
                char sExist = accountChargesTypeCode[2].toString().charAt(SHORT_ZERO);
                char lExist = accountChargesTypeCode[3].toString().charAt(SHORT_ZERO);
                char dExist = accountChargesTypeCode[4].toString().charAt(SHORT_ZERO);
                if (pExist == CHAR_Y || sExist == CHAR_Y || lExist == CHAR_Y || dExist == CHAR_Y) {
                    if (pExist == CHAR_Y) {
                        originalCashAmount = processChargesAmounts(bilAccount.getAccountId(), originalCashAmount,
                                dueDate, ChargeTypeCodes.PENALTY_CHARGE.getValue(), bilCashDsp, cashApply,
                                bilCashReceipt, bilPolicyTerm, bilAccount);
                    }

                    if (lExist == CHAR_Y) {
                        originalCashAmount = processChargesAmounts(bilAccount.getAccountId(), originalCashAmount,
                                dueDate, ChargeTypeCodes.LATE_CHARGE.getValue(), bilCashDsp, cashApply, bilCashReceipt,
                                bilPolicyTerm, bilAccount);
                    }

                    if (dExist == CHAR_Y && cashApply.getPremiumPayIndicator() != CHAR_Y) {
                        originalCashAmount = processChargesAmounts(bilAccount.getAccountId(), originalCashAmount,
                                dueDate, ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue(), bilCashDsp, cashApply,
                                bilCashReceipt, bilPolicyTerm, bilAccount);
                    }

                    if (sExist == CHAR_Y && cashApply.getPremiumPayIndicator() != CHAR_Y) {
                        originalCashAmount = processChargesAmounts(bilAccount.getAccountId(), originalCashAmount,
                                dueDate, ChargeTypeCodes.SERVICE_CHARGE.getValue(), bilCashDsp, cashApply,
                                bilCashReceipt, bilPolicyTerm, bilAccount);
                    }

                    if (originalCashAmount == DECIMAL_ZERO) {
                        break;
                    }
                }
            }
        }

        return originalCashAmount;
    }

    private double processChargesAmounts(String accountId, double originalCashAmount, ZonedDateTime dueDate,
            char bilCrgTypeCd, BilCashDsp bilCashDsp, CashApply cashApply, BilCashReceipt bilCashReceipt,
            BilPolicyTerm bilPolicyTerm, BilAccount bilAccount) {

        ZonedDateTime applyDate = cashApply.getCashApplyDate();
        char quoteIndicator = cashApply.getQuoteIndicator().charAt(SHORT_ZERO);

        List<BilCrgAmounts> bilCrgAmountsList = bilCrgAmountsRepository.readChargeRowsByDueDate(accountId, dueDate,
                bilCrgTypeCd, CURRENT_INVOICE_CODES);
        if (bilCrgAmountsList != null && !bilCrgAmountsList.isEmpty()) {
            for (BilCrgAmounts bilCrgAmounts : bilCrgAmountsList) {
                double openChargeAmount = DECIMAL_ZERO;
                double openCashPaid = DECIMAL_ZERO;
                double dispositionAmount;
                double newChargeAmount;
                double chargeAmount = bilCrgAmounts.getBcaCrgAmt();
                double paidAmount = bilCrgAmounts.getBcaCrgPaidAmt() + bilCrgAmounts.getBcaWroCrgAmt();
                chargeAmount = chargeAmount - paidAmount;
                if (originalCashAmount >= chargeAmount) {
                    originalCashAmount = originalCashAmount - chargeAmount;
                    openChargeAmount = openChargeAmount + chargeAmount;
                    openCashPaid = openCashPaid + chargeAmount;
                    dispositionAmount = chargeAmount;
                    newChargeAmount = chargeAmount + bilCrgAmounts.getBcaCrgPaidAmt();
                } else {
                    newChargeAmount = originalCashAmount + bilCrgAmounts.getBcaCrgPaidAmt();
                    openChargeAmount = openChargeAmount + originalCashAmount;
                    openCashPaid = openCashPaid + originalCashAmount;
                    dispositionAmount = originalCashAmount;
                    originalCashAmount = DECIMAL_ZERO;
                }

                if (dispositionAmount != DECIMAL_ZERO) {
                    updateSvcAndCash(bilCashDsp, bilCrgAmounts, newChargeAmount, bilCrgTypeCd, applyDate,
                            dispositionAmount, cashApply.getUserSequenceId());
                    String orginalEfftiveDate;
                    String referenceDate;
                    String bilDatabaseKey;

                    if (openCashPaid > DECIMAL_ZERO && quoteIndicator != BilPolicyStatusCode.QUOTE.getValue()) {

                        if (bilPolicyTerm.getBillPolicyTermId().getPolicyId().trim().isEmpty()) {
                            orginalEfftiveDate = BLANK_STRING;
                            referenceDate = DateRoutine.dateTimeAsYYYYMMDDString(cashApply.getCashApplyDate());
                        } else {
                            orginalEfftiveDate = "9999-12-31";
                            referenceDate = DateRoutine
                                    .dateTimeAsYYYYMMDDString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                        }
                        bilDatabaseKey = getBilDatabaseKey(bilCashReceipt.getEntryDate(),
                                bilCashReceipt.getEntryNumber(), bilCashReceipt.getUserId(),
                                bilCashReceipt.getEntrySequenceNumber());

                        callFinancialApi(openCashPaid, openCashPaid, cashApply.getCashApplyDate(),
                                bilCashReceipt.getBankCd(), bilAccount.getAccountNumber(),
                                BilCashEntryMethodCode.CASH.getValue(), BilCashEntryMethodCode.APPLY.getValue(),
                                BLANK_STRING, String.valueOf(bilCashReceipt.getCashEntryMethodCd()),
                                bilCashReceipt.getReceiptTypeCd(), BLANK_STRING, bilAccount,
                                bilPolicyTerm.getBillCountryCd(), bilPolicyTerm.getLobCd(), orginalEfftiveDate,
                                bilPolicyTerm, bilDatabaseKey, referenceDate, cashApply.getUserSequenceId());
                    }

                    if (openChargeAmount > DECIMAL_ZERO && quoteIndicator != BilPolicyStatusCode.QUOTE.getValue()) {
                        orginalEfftiveDate = DateRoutine.dateTimeAsYYYYMMDDString(cashApply.getCashApplyDate());
                        referenceDate = DateRoutine.dateTimeAsYYYYMMDDString(cashApply.getCashApplyDate());

                        String[] fwsCodes = calculateChargeActions(bilCrgTypeCd, bilCrgAmounts.getBilInvoiceCd(),
                                openChargeAmount, false);
                        if (fwsCodes[1].compareTo(ActionCode.APPLY_TO_UNBILLED.toString()) == 0
                                && (fwsCodes[0].compareTo(ObjectCode.LATE_CHARGE.toString()) == 0
                                        || fwsCodes[0].compareTo(ObjectCode.DOWN_PAYMENT_FEE.toString()) == 0
                                        || fwsCodes[0].compareTo(ObjectCode.PENALTY_CHARGE.toString()) == 0)) {
                            bilDatabaseKey = BLANK_STRING;
                        } else {
                            bilDatabaseKey = getBilDatabaseKey(bilCashReceipt.getEntryDate(),
                                    bilCashReceipt.getEntryNumber(), bilCashReceipt.getUserId(),
                                    bilCashReceipt.getEntrySequenceNumber());
                        }
                        callFinancialApi(openChargeAmount, openChargeAmount, cashApply.getCashApplyDate(),
                                bilCashReceipt.getBankCd(), bilAccount.getAccountNumber(), fwsCodes[0], fwsCodes[1],
                                BLANK_STRING, String.valueOf(bilCashReceipt.getCashEntryMethodCd()),
                                bilCashReceipt.getReceiptTypeCd(), BLANK_STRING, bilAccount,
                                bilPolicyTerm.getBillCountryCd(), bilPolicyTerm.getLobCd(), orginalEfftiveDate,
                                bilPolicyTerm, bilDatabaseKey, referenceDate, cashApply.getUserSequenceId());
                    }

                }

            }

        }

        return originalCashAmount;

    }

    private boolean checkQuoteTermProcess(List<BilPolicyTerm> bilPolicyTermList, boolean quoteTermHideReset) {
        boolean quoteTermHiddenInd = false;
        List<Character> invoiceList = null;
        if (quoteTermHideReset) {
            invoiceList = INVOICE_CODES;
        } else {
            invoiceList = getResetInvoiceCdList();
        }
        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
            List<Character> invoiceCodeList = bilIstScheduleRepository.fetchInvoiceCode(
                    bilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), invoiceList);
            if (invoiceCodeList != null && !invoiceCodeList.isEmpty()) {
                char quoteInvoiceCode = invoiceCodeList.get(0);
                quoteTermHiddenInd = processBisIstScheduleQuote(quoteInvoiceCode, quoteTermHideReset, bilPolicyTerm);
            }
        }
        return quoteTermHiddenInd;
    }

    private List<Character> getResetInvoiceCdList() {
        List<Character> invoiceCodeList = new ArrayList<>();
        invoiceCodeList.add(QTE_INV_CD_1);
        invoiceCodeList.add(QTE_INV_CD_2);
        invoiceCodeList.add(QTE_INV_CD_3);
        invoiceCodeList.add(QTE_INV_CD_4);
        invoiceCodeList.add(QTE_INV_CD_5);
        return invoiceCodeList;

    }

    private boolean processBisIstScheduleQuote(char quoteInvoiceCode, boolean quoteTermHideReset,
            BilPolicyTerm bilPolicyTerm) {
        boolean quoteTermHiddenInd = false;
        char newQuoteInvoiceCode = BLANK_CHAR;
        if (quoteTermHideReset) {
            switch (quoteInvoiceCode) {
            case BLANK_CHAR:
                newQuoteInvoiceCode = QTE_INV_CD_1;
                break;
            case CHAR_A:
                newQuoteInvoiceCode = QTE_INV_CD_2;
                break;
            case CHAR_B:
                newQuoteInvoiceCode = QTE_INV_CD_3;
                break;
            case CHAR_L:
                newQuoteInvoiceCode = QTE_INV_CD_4;
                break;
            case CHAR_Y:
                newQuoteInvoiceCode = QTE_INV_CD_5;
                break;
            default:
                break;

            }
        } else {
            switch (quoteInvoiceCode) {
            case QTE_INV_CD_2:
                newQuoteInvoiceCode = InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue();
                break;
            case QTE_INV_CD_3:
                newQuoteInvoiceCode = InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue();
                break;
            case QTE_INV_CD_4:
                newQuoteInvoiceCode = InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue();
                break;
            case QTE_INV_CD_5:
                newQuoteInvoiceCode = InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue();
                break;
            default:
                break;
            }
        }
        if (newQuoteInvoiceCode != BLANK_CHAR) {
            if (quoteTermHideReset) {
                quoteTermHiddenInd = true;
            }
            bilIstScheduleRepository.updateRow(bilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), quoteInvoiceCode, newQuoteInvoiceCode);

        }
        return quoteTermHiddenInd;
    }

    private void callFinancialApi(Double amount, Double netAmount, ZonedDateTime applicationDate, String bankCode,
            String accountNumber, String transactionObjectCode, String transactionActionCode, String paybleItemCode,
            String sourceCode, String paymentType, String bilReasonCode, BilAccount bilAccount, String countryCode,
            String lineOfBusCode, String originalEffectiveDate, BilPolicyTerm bilPolicyTerm, String bilDatabaseKey,
            String referenceDate, String userSequenceId) {

        String masterCompany = bilPolicyTerm.getMasterCompanyNbr().trim();
        String riskState = bilPolicyTerm.getBillStatePvnCd().trim();
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        if (bilPolicyTerm.getBillPolicyTermId().getPolicyId().trim().isEmpty()
                || transactionObjectCode.equals(ObjectCode.SERVICE_CHARGE.toString())
                || transactionObjectCode.equals(ObjectCode.LATE_CHARGE.toString())
                || transactionObjectCode.equals(ObjectCode.PENALTY_CHARGE.toString())
                || transactionObjectCode.equals(ObjectCode.DOWN_PAYMENT_FEE.toString())
                || !bilPolicyTerm.getBillPolicyTermId().getPolicyId().isEmpty()
                        && bilPolicyTerm.getBptAgreementInd() == CHAR_Y
                        && bilPolicyTerm.getBillStatePvnCd().trim().isEmpty()
                        && (bilPolicyTerm.getMasterCompanyNbr().trim().equals("99")
                                || bilPolicyTerm.getMasterCompanyNbr().trim().isEmpty())) {
            BilMstCoSt bilMstCoSt = bilMstCoStRepository.findRow(bilAccount.getBillClassCd(),
                    bilAccount.getBillTypeCd(), BLANK_STRING);
            if (bilMstCoSt != null) {
                masterCompany = bilMstCoSt.getMasterCompanyNbr();
                riskState = bilMstCoSt.getBilStatePvnCd();
            }
        }

        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(BLANK_STRING);
        financialApiActivity.setApplicationName(ApplicationName.BCMOCX.toString());
        financialApiActivity.setUserId(userSequenceId);
        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setAppProgramId(ApplicationName.BCMOCX.toString());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(netAmount);
        financialApiActivity.setReferenceDate(referenceDate);
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilTypeCode(bilAccount.getBillTypeCd());
        financialApiActivity.setBilClassCode(bilAccount.getBillClassCd());
        financialApiActivity.setPayableItemCode(paybleItemCode);
        financialApiActivity.setBilAccountId(accountNumber);
        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setBilSourceCode(sourceCode);
        financialApiActivity.setMasterCompanyNbr(masterCompany);
        financialApiActivity.setStateCode(riskState);

        financialApiActivity.setStatusCode(BLANK_CHAR);
        financialApiActivity.setBilReceiptTypeCode(paymentType);
        financialApiActivity.setBilReasonCode(bilReasonCode);
        financialApiActivity.setSummaryEffectiveDate(applicationDate);
        financialApiActivity.setOperatorId(userSequenceId);
        financialApiActivity.setCountryCode(countryCode);
        financialApiActivity.setLineOfBusCode(lineOfBusCode);
        financialApiActivity.setOriginalEffectiveDate(originalEffectiveDate);
        financialApiActivity.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId().trim());
        financialApiActivity.setCurrencyCode(bilAccount.getCurrencyCode());
        if (paymentType.trim().equals(BilReceiptTypeCode.GROSS_PAYMENT.getValue())
                || paymentType.trim().equals(BilReceiptTypeCode.GROSS_WRITEOFF.getValue())
                || paymentType.trim().equals(BilReceiptTypeCode.GROSS_OVERPAY.getValue())) {
            if (transactionActionCode.equals(ActionCode.APPLY_TO_INSTALL.toString())) {
                financialApiActivity.setAgentTtyId(bilAccount.getAgentAccountNumber());
            } else {
                financialApiActivity.setAgentTtyId(accountNumber);
            }

        }
        financialApiActivity.setBilDatabaseKey(bilDatabaseKey);

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {

            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

    private String getBilDatabaseKey(ZonedDateTime entryDate, String entryNumber, String operatorId,
            short bilDtbSequenceNumber) {
        String seqNumberSign = "+";

        String dataString = DateRoutine.dateTimeAsYYYYMMDDString(entryDate);
        dataString += entryNumber;
        dataString += StringUtils.rightPad(operatorId, 8, BLANK_CHAR);
        dataString += seqNumberSign;
        dataString += StringUtils.leftPad(String.valueOf(bilDtbSequenceNumber), 5, CHAR_ZERO);

        return dataString;
    }

    private void insertBilActSummary(BilPolicyTerm bilPolicyTerm, ZonedDateTime se3DateTime, BilPolicy bilPolicy, String userSequenceId) {
        short bilAcySeq = getBilAcySeq(bilPolicyTerm.getBillPolicyTermId().getBilAccountId(), se3DateTime);

        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(bilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                se3DateTime, bilAcySeq);
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(bilPolicy.getPolSymbolCd());
        bilActSummary.setPolNbr(bilPolicy.getPolNbr());
        bilActSummary.setBilAcyDesCd(String.valueOf(CHAR_D));

        bilActSummary.setBilDesReaTyp(BLANK_STRING);
        bilActSummary.setBilAcyDes1Dt(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
        bilActSummary.setBilAcyDes2Dt(DateRoutine.defaultDateTime());
        bilActSummary.setBilAcyAmt(DECIMAL_ZERO);
        bilActSummary.setUserId(userSequenceId);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());
        bilActSummary.setBasAddDataTxt(BLANK_STRING);

        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

    private Short getBilAcySeq(String accountId, ZonedDateTime se3DateTime) {
        Short bilAcySeq = 0;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, se3DateTime.with(LocalTime.MIN));
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        return bilAcySeq;
    }

    private boolean validateIfEquityPayment(String paymentType) {
        return busCdTranslationRepository.findByCodeAndType(paymentType, BilDesReasonType.EQT.getValue(),
                language) != null;
    }

    private char getObjectCfg() {
        char objectPrcCode = BLANK_CHAR;
        BilObjectCfg bilObjectCfg = bilObjectCfgRepository.findById(new BilObjectCfgId(CCMOBCM, CHAR_I)).orElse(null);
        if (bilObjectCfg != null) {
            objectPrcCode = bilObjectCfg.getBocObjPrcCd();
        }
        return objectPrcCode;
    }

    private boolean checkCommissionAuthActive(String objectCode, String actionCode) {
        char objectPrcCode = getObjectCfg();
        return objectCode.compareTo(ObjectCode.PREMIUM.toString()) == 0
                && (actionCode.compareTo(ActionCode.APPLY_TO_INSTALL.toString()) == 0
                        || actionCode.compareTo(ActionCode.APPLY_TO_UNBILLED.toString()) == 0
                        || actionCode.compareTo(ActionCode.WRITE_OFF_INSTALL.toString()) == 0
                        || actionCode.compareTo(ActionCode.WRITE_OFF_UNBILLED.toString()) == 0
                        || actionCode.compareTo(ActionCode.CLOSED_BILLED.toString()) == 0
                        || actionCode.compareTo(ActionCode.CLOSED_UNBILLED.toString()) == 0
                        || actionCode.compareTo(ActionCode.CREDIT_AGST_DEBIT.toString()) == 0
                        || actionCode.compareTo(ActionCode.CREDIT_SCHEDULE_TO_BILL.toString()) == 0)
                && objectPrcCode != CHAR_N;
    }

}
