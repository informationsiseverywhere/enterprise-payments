package billing.service.impl;

import static billing.utils.BillingConstants.ENTRY_NUMBER;
import static billing.utils.BillingConstants.MASTER_COMPANY_NUMBER;
import static billing.utils.BillingConstants.PLUS_SIGN;
import static billing.utils.BillingConstants.STATEMENT_ROW;
import static billing.utils.BillingConstants.ZERO_VALUE;
import static core.utils.CommonConstants.BIGDECIMAL_ZERO;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilAgent;
import billing.data.entity.BilAgtCashDsp;
import billing.data.entity.BilInvTtyAct;
import billing.data.entity.BilMstCoSt;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSchAutoReconCsh;
import billing.data.entity.BilThirdParty;
import billing.data.entity.BilTtyCashDsp;
import billing.data.entity.BilTtySummary;
import billing.data.entity.id.BilAgtCashDspId;
import billing.data.entity.id.BilSchAutoReconCshId;
import billing.data.entity.id.BilTtyCashDspId;
import billing.data.entity.id.BilTtySummaryId;
import billing.data.repository.BilAgtCashDspRepository;
import billing.data.repository.BilInvTtyActRepository;
import billing.data.repository.BilMstCoStRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilSchAutoReconCshRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.data.repository.BilTtyCashDspRepository;
import billing.data.repository.BilTtySummaryRepository;
import billing.service.ReplacementPaymentService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilCashEntryMethodCode;
import billing.utils.BillingConstants.BilDesReasonCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.PayableItemCode;
import billing.utils.BillingConstants.ProcessIndicator;
import billing.utils.BillingConstants.StatementReconciliationIndicator;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class ReplacementPaymentServiceImpl implements ReplacementPaymentService {

    public static final Logger logger = LogManager.getLogger(ReplacementPaymentServiceImpl.class);

    @Autowired
    ExecContext execContext;

    @Autowired
    BilTtyCashDspRepository bilTtyCashDspRepository;

    @Autowired
    BilInvTtyActRepository bilInvTtyActRepository;

    @Autowired
    BilTtySummaryRepository bilTtySummaryRepository;

    @Autowired
    BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    FinancialApiService financialApiService;

    @Autowired
    BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    BilSchAutoReconCshRepository bilSchAutoReconCshRepository;

    @Autowired
    BilAgtCashDspRepository bilAgtCashDspRepository;

    @Autowired
    BilMstCoStRepository bilMstCoStRepository;

    private static final String DISPOSITION_NUMBER = "Disposition number to be used for AP row= {}";

    @Override
    @Transactional
    public void reapplyPaymentForThirdParty(String accountId, ZonedDateTime dueDate, BigDecimal amount, String reason,
            String accountNumber, BilInvTtyAct invoiceActivityRow, ZonedDateTime entryDate, String entryNumber,
            short entrySequenceNumber, ZonedDateTime distributionDate, short distributionSequenceNumber, String userId,
            char cashEntryMethodCode, String bankCode, Short dispositionSequenceNumber, String accountType) {
        logger.debug("Replacement Payment service start for accountId {}", accountId);
        BigDecimal differenceAmount;

        if (invoiceActivityRow.getBilSttReconInd() == StatementReconciliationIndicator.REOPEN.getValue()) {
            differenceAmount = BigDecimal.valueOf(invoiceActivityRow.getBilMinDueAmt())
                    .subtract(BigDecimal.valueOf(invoiceActivityRow.getBilSttSvgAmt()));
        } else {
            differenceAmount = BigDecimal.valueOf(invoiceActivityRow.getBilMinDueAmt())
                    .subtract(BigDecimal.valueOf(invoiceActivityRow.getBilSttReconAmt()));
        }
        BilTtyCashDsp ttyCashDisposition = bilTtyCashDspRepository.findById(
                new BilTtyCashDspId(accountId, distributionDate, distributionSequenceNumber, dispositionSequenceNumber))
                .orElse(null);
        BigDecimal orignalDispositionAmount = BIGDECIMAL_ZERO;
        if (ttyCashDisposition != null) {
            orignalDispositionAmount = BigDecimal.valueOf(ttyCashDisposition.getBilDspAmt());
        }
        if (differenceAmount.equals(new BigDecimal(ZERO_VALUE))
                || invoiceActivityRow.getBilSttReconInd() == StatementReconciliationIndicator.RECONCILED.getValue()) {
            logger.debug("Statement already reconciled hence no updates from replacement service.");
            return;
        } else {
            BigDecimal applyAmount;
            if (amount.compareTo(differenceAmount) >= 0) {
                applyAmount = differenceAmount;
            } else {
                applyAmount = amount;
            }
            applyAmountToStatement(accountId, dueDate, distributionSequenceNumber, distributionDate, applyAmount,
                    accountNumber, invoiceActivityRow, entrySequenceNumber, entryDate, entryNumber, userId,
                    cashEntryMethodCode, bankCode);
            BigDecimal insertAmount = amount.subtract(applyAmount);
            BigDecimal updateAmount = orignalDispositionAmount.subtract(amount);
            if (orignalDispositionAmount.compareTo(amount) > 0) {
                if (amount.compareTo(applyAmount) > 0) {
                    logger.debug(
                            "Receipt amount is greater than transferred amount and transferred amount is greater than applyAmount.");
                    updateCurrentDisposition(updateAmount, accountId, distributionDate, distributionSequenceNumber,
                            accountType, entryDate, entryNumber, entrySequenceNumber, ttyCashDisposition);
                    insertBilTtyCashDisposition(ttyCashDisposition, accountId, distributionDate,
                            distributionSequenceNumber, insertAmount.doubleValue(), dueDate);
                    callFinancialApi(insertAmount.doubleValue(), execContext.getApplicationDate(), accountId,
                            entrySequenceNumber, bankCode, entryDate, entryNumber, userId, accountNumber,
                            ObjectCode.CASH.toString(), BilCashEntryMethodCode.SUSPENDED.getValue(), BLANK_STRING,
                            String.valueOf(cashEntryMethodCode));
                } else {
                    logger.debug(
                            "Receipt amount is greater than transferred amount and transferred amount is equal or less than applyAmount.");
                    updateCurrentDisposition(updateAmount, accountId, distributionDate, distributionSequenceNumber,
                            accountType, entryDate, entryNumber, entrySequenceNumber, ttyCashDisposition);
                }
            } else if (orignalDispositionAmount.equals(amount)) {
                if (amount.compareTo(applyAmount) > 0 && ttyCashDisposition != null) {
                    logger.debug(
                            "Receipt amount is equal to transferred amount and transferred amount is greater than applyAmount.");
                    insertBilTtyCashDisposition(ttyCashDisposition, accountId, distributionDate,
                            distributionSequenceNumber, insertAmount.doubleValue(), dueDate);
                    callFinancialApi(insertAmount.doubleValue(), execContext.getApplicationDate(), accountId,
                            entrySequenceNumber, bankCode, entryDate, entryNumber, userId, accountNumber,
                            ObjectCode.CASH.toString(), BilCashEntryMethodCode.SUSPENDED.getValue(), BLANK_STRING,
                            String.valueOf(cashEntryMethodCode));
                    bilTtyCashDspRepository.delete(ttyCashDisposition);
                } else {
                    logger.debug(
                            "Receipt amount is equal to transferred amount and transferred amount is equal or less than applyAmount.");
                    logger.debug("Delete current disposition row.");
                    if (ttyCashDisposition != null) {
                        bilTtyCashDspRepository.delete(ttyCashDisposition);
                    }
                }
            }
        }
        logger.debug("Replacement Payment service end for accountId {}", accountId);
    }

    private void applyAmountToStatement(String accountId, ZonedDateTime dueDate, short distributionSequenceNumber,
            ZonedDateTime distributionDate, BigDecimal amount, String accountNumber, BilInvTtyAct invoiceActivityRow,
            short entrySequenceNumber, ZonedDateTime entryDate, String entryNumber, String userId,
            char cashEntryMethodCode, String bankCode) {
        logger.debug("applyAmountToStatement function start.");
        insertAppliedDisposition(accountId, dueDate, distributionSequenceNumber, distributionDate, amount);
        boolean isCompletelyPaid = updateInvoice02Row(invoiceActivityRow, amount);
        if (isCompletelyPaid) {
            logger.debug("Statement is completely re-paid from replacement service for accountId {}", accountId);
            insertIntoBilTtySummary(accountId, execContext.getApplicationDate(), amount.doubleValue(),
                    BilDesReasonCode.REOPEN_TO_PAID.getValue(), BilDesReasonType.STATEMENT_STATUS.getValue(),
                    DateRoutine.defaultDateTime());
        }
        callFinancialApi(amount.doubleValue(), execContext.getApplicationDateTime(), accountId, entrySequenceNumber,
                bankCode, entryDate, entryNumber, userId, accountNumber, ObjectCode.PREMIUM.toString(),
                BilCashEntryMethodCode.APPLIED.getValue(), PayableItemCode.TRR.getValue(),
                String.valueOf(cashEntryMethodCode));
        callFinancialApi(amount.doubleValue(), execContext.getApplicationDateTime(), accountId, entrySequenceNumber,
                bankCode, entryDate, entryNumber, userId, accountNumber, ObjectCode.CASH.toString(),
                BilCashEntryMethodCode.APPLY.getValue(), BLANK_STRING, String.valueOf(cashEntryMethodCode));
        logger.debug("applyAmountToStatement function end.");
    }

    private void insertBilTtyCashDisposition(BilTtyCashDsp ttyCashDisposition, String accountId,
            ZonedDateTime distributionDate, Short sequenceNumber, Double amount, ZonedDateTime dueDate) {
        logger.debug("insertBilTtyCashDisposition function start.");
        BilTtyCashDsp ttyCashDispositionRow = new BilTtyCashDsp();
        Short dispositionNumber = findMaxSequenceNumberTtyCashDisposition(accountId, distributionDate, sequenceNumber);
        logger.debug(DISPOSITION_NUMBER, dispositionNumber);
        ttyCashDispositionRow.setBilTtyCashDspId(
                new BilTtyCashDspId(accountId, distributionDate, sequenceNumber, dispositionNumber));
        ttyCashDispositionRow.setBilDspAmt(amount);
        ttyCashDispositionRow.setDdsDsbDt(DateRoutine.defaultDateTime());
        ttyCashDispositionRow.setBilDspDt(execContext.getApplicationDate());
        ttyCashDispositionRow.setUserId(execContext.getUserSeqeunceId());
        ttyCashDispositionRow.setBilAdjDueDt(dueDate);
        ttyCashDispositionRow.setBilManualSusInd(ttyCashDisposition.getBilManualSusInd());
        ttyCashDispositionRow.setBilDspReasonCd(ttyCashDisposition.getBilDspReasonCd());
        ttyCashDispositionRow.setBilToFroTrfNbr(ttyCashDisposition.getBilToFroTrfNbr());
        ttyCashDispositionRow.setBilDspTypeCd(ttyCashDisposition.getBilDspTypeCd());
        ttyCashDispositionRow.setBilPayeeCltId(ttyCashDisposition.getBilPayeeCltId());
        ttyCashDispositionRow.setBilPayeeAdrSeq(ttyCashDisposition.getBilPayeeAdrSeq());
        ttyCashDispositionRow.setDwsCkDrfNbr(ttyCashDisposition.getDwsCkDrfNbr());
        ttyCashDispositionRow.setBilRevsRsusInd(ttyCashDisposition.getBilRevsRsusInd());
        ttyCashDispositionRow.setBilTrfTypeCd(ttyCashDisposition.getBilTrfTypeCd());
        ttyCashDispositionRow.setBilChkPrdMthCd(ttyCashDisposition.getBilChkPrdMthCd());
        logger.debug("Intended insert operation on ttyCashDisposition.");
        bilTtyCashDspRepository.saveAndFlush(ttyCashDispositionRow);
        logger.debug("insertBilTtyCashDisposition function end.");
    }

    private void callFinancialApi(Double amount, ZonedDateTime applicationDate, String accountId,
            short entrySequenceNumber, String bankCode, ZonedDateTime entryDate, String entryNumber, String userId,
            String accountNumber, String transactionObjectCode, String transactionActionCode, String paybleItemCode,
            String sourceCode) {
        logger.debug("callFinancialApi function start.");
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        BilThirdParty bilThirdParty = bilThirdPartyRepository.findByBillThirdPartyId(accountId);
        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(accountId);
        financialApiActivity.setApplicationName(ApplicationName.BACPTDIR.toString());
        financialApiActivity.setUserId(execContext.getUserId());
        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setErrorCode(ENTRY_NUMBER);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setAppProgramId(ApplicationProgramIdentifier.BCMORC.toString());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilTypeCode(bilThirdParty.getBilTypeCd());
        financialApiActivity.setBilClassCode(bilThirdParty.getBilClassCd());
        financialApiActivity.setPayableItemCode(paybleItemCode);
        financialApiActivity.setAgentTtyId(accountNumber);
        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setBilSourceCode(sourceCode);
        financialApiActivity.setMasterCompanyNbr(BillingConstants.MASTER_COMPANY_NUMBER);
        financialApiActivity.setBilDatabaseKey(getBilDatabaseKey(entryDate, entryNumber, userId, entrySequenceNumber));
        financialApiActivity.setStatusCode(BLANK_CHAR);
        financialApiActivity.setCurrencyCode(bilThirdParty.getCurrencyCd());
        logger.debug("Calling Financial API.");
        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            logger.error("Call to Financial API failed.");
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
        logger.debug("callFinancialApi function end.");
    }

    private String fillRightByChar(String param, int length, char padChar) {
        return StringUtils.rightPad(param, length, padChar);
    }

    private String fillLeftByZero(String param, int length) {
        return StringUtils.leftPad(param, length, CHAR_ZERO);
    }

    private String getBilDatabaseKey(ZonedDateTime entryDate, String entryNumber, String operatorId,
            short bilDtbSequenceNumber) {
        String seqNumberSign = PLUS_SIGN;
        StringBuilder sb = new StringBuilder(28);
        sb.append(entryDate.toLocalDate());
        sb.append(entryNumber);
        sb.append(fillRightByChar(operatorId, 8, BLANK_CHAR));
        sb.append(seqNumberSign);
        sb.append(fillLeftByZero(String.valueOf(bilDtbSequenceNumber), 5));
        return sb.toString();
    }

    private void insertIntoBilTtySummary(String accountId, ZonedDateTime applicationDate, Double activityAmount,
            String accountDescriptionCode, String descriptionReasonType, ZonedDateTime infinityDate) {
        BilTtySummary bilTtySummary = new BilTtySummary();
        bilTtySummary.setBilTtySummaryId(new BilTtySummaryId(accountId, applicationDate,
                getIncrementalValue(bilTtySummaryRepository.getMaxSeqNumber(accountId, applicationDate))));
        bilTtySummary.setUserId(execContext.getUserSeqeunceId());
        bilTtySummary.setBilAcyAmt(activityAmount);
        bilTtySummary.setBilAcyDesCd(accountDescriptionCode);
        bilTtySummary.setBilDesReaTyp(descriptionReasonType);
        bilTtySummary.setBilAcyDes1Dt(infinityDate);
        bilTtySummary.setBilAcyDes2Dt(infinityDate);
        bilTtySummary.setBilAcyTs(execContext.getApplicationDateTime());
        bilTtySummaryRepository.saveAndFlush(bilTtySummary);
    }

    private Short getIncrementalValue(Short sequenceNumber) {
        if (sequenceNumber == null) {
            return 0;
        } else {
            return (short) (sequenceNumber + 1);
        }
    }

    private boolean updateInvoice02Row(BilInvTtyAct invoiceActivityRow, BigDecimal amount) {
        logger.debug("Update BilInvTtyAct row function start.");
        BigDecimal statementAmount = BigDecimal.valueOf(invoiceActivityRow.getBilSttSvgAmt());
        invoiceActivityRow.setBilSttSvgAmt(statementAmount.add(amount).doubleValue());
        boolean isCompletelyPaid = false;
        if (invoiceActivityRow.getBilSttSvgAmt() >= invoiceActivityRow.getBilSttReconAmt()) {
            invoiceActivityRow.setBilSttReconInd(StatementReconciliationIndicator.RECONCILED.getValue());
            bilInvTtyActRepository.updateStatementLevelRow(invoiceActivityRow.getBilInvTtyActId().getBilTchKeyId(),
                    invoiceActivityRow.getBilInvTtyActId().getBilTchKeyTypCd(),
                    invoiceActivityRow.getBilInvTtyActId().getBilInvDt(), STATEMENT_ROW,
                    invoiceActivityRow.getBilSttReconInd(), execContext.getApplicationDate());
            isCompletelyPaid = true;
        }
        bilInvTtyActRepository.save(invoiceActivityRow);
        logger.debug("Update BilInvTtyAct row function end.");
        return isCompletelyPaid;
    }

    private void updateCurrentDisposition(BigDecimal updateAmount, String accountId, ZonedDateTime distributionDate,
            short distributionSequenceNumber, String accountType, ZonedDateTime entryDate, String entryNumber,
            short entrySequenceNumber, BilTtyCashDsp ttyCashDisposition) {
        logger.debug("updateCurrentDisposition function start.");
        ttyCashDisposition.setBilDspAmt(updateAmount.doubleValue());
        bilTtyCashDspRepository.save(ttyCashDisposition);

        if (accountType.equals(AccountType.GROUP_ACCOUNT.toString())) {
            String statementAutoReconcile = "SPAR";
            String bilTypeCode = SEPARATOR_BLANK;
            String bilClassCode = SEPARATOR_BLANK;
            String bilPlanCode = SEPARATOR_BLANK;

            BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(statementAutoReconcile, bilTypeCode,
                    bilClassCode, bilPlanCode, execContext.getApplicationDate(), execContext.getApplicationDate());
            if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                logger.info("SPAR rule is ON.");
                checkAndInsertAutoReconciliationRow(accountId, getAccountTypeCode(accountType), entryDate, entryNumber,
                        entrySequenceNumber, distributionDate, distributionSequenceNumber);
            }
        }
        logger.debug("updateCurrentDisposition function end.");
    }

    private char getAccountTypeCode(String accountType) {
        char bilTechKeyTypeCode;
        if (AccountType.GROUP_ACCOUNT.name().equalsIgnoreCase(accountType)) {
            bilTechKeyTypeCode = CHAR_T;
        } else {
            logger.error("Account type is invalid.");
            throw new InvalidDataException("bad.request.data.invalid", new Object[] { accountType });
        }
        logger.debug("bilTechKeyTypeCode : {}", bilTechKeyTypeCode);
        return bilTechKeyTypeCode;
    }

    private void checkAndInsertAutoReconciliationRow(String accountId, char bilTechKeyTypeCode, ZonedDateTime entryDate,
            String entryNumber, short entrySequenceNumber, ZonedDateTime distributionDate,
            short distributionSequenceNumber) {
        logger.debug("insertAutoReconciliationRow function start.");
        List<Character> processIndicatorList = new ArrayList<>();
        processIndicatorList.add(ProcessIndicator.RE_KICK.getValue());
        processIndicatorList.add(ProcessIndicator.NOT_PROCESSED.getValue());
        BilSchAutoReconCsh bilScheduleAutoReconciliationCashRow = bilSchAutoReconCshRepository.findRow(accountId,
                bilTechKeyTypeCode, entryDate, entryNumber, entrySequenceNumber, distributionDate,
                distributionSequenceNumber, processIndicatorList);
        if (bilScheduleAutoReconciliationCashRow == null) {
            logger.debug("old bilScheduleAutoReconciliationCashRow not found.");
            BilSchAutoReconCsh bilScheduleAutomaticReconciliationCashRow = new BilSchAutoReconCsh();
            Short bilSequenceNumber = findMaxSequenceNumberAutoReconciliationCash(accountId, bilTechKeyTypeCode);
            bilScheduleAutomaticReconciliationCashRow.setBilSchAutoReconCshId(
                    new BilSchAutoReconCshId(accountId, bilTechKeyTypeCode, bilSequenceNumber));
            bilScheduleAutomaticReconciliationCashRow.setBilDtbDt(distributionDate);
            bilScheduleAutomaticReconciliationCashRow.setBilDtbSeqNbr(distributionSequenceNumber);
            bilScheduleAutomaticReconciliationCashRow.setBilEntryDt(entryDate);
            bilScheduleAutomaticReconciliationCashRow.setBilEntryNbr(entryNumber);
            bilScheduleAutomaticReconciliationCashRow.setBilEntrySeqNbr(entrySequenceNumber);
            bilScheduleAutomaticReconciliationCashRow.setProcessInd(ProcessIndicator.NOT_PROCESSED.getValue());
            logger.debug("Intended insert operation on bilScheduleAutomaticReconciliationCashRow.");
            bilSchAutoReconCshRepository.save(bilScheduleAutomaticReconciliationCashRow);
        }
        logger.debug("insertAutoReconciliationRow function end.");
    }

    private void insertAppliedDisposition(String accountId, ZonedDateTime dueDate, Short distributionSequenceNumber,
            ZonedDateTime distributionDate, BigDecimal differenceAmount) {
        logger.debug("insertAppliedDisposition function start.");
        BilTtyCashDsp bilTtyCashDispositionRow = new BilTtyCashDsp();
        Short dispositionNumber = findMaxSequenceNumberTtyCashDisposition(accountId, distributionDate,
                distributionSequenceNumber);
        logger.debug(DISPOSITION_NUMBER, dispositionNumber);
        bilTtyCashDispositionRow.setBilTtyCashDspId(
                new BilTtyCashDspId(accountId, distributionDate, distributionSequenceNumber, dispositionNumber));
        bilTtyCashDispositionRow.setBilDspAmt(differenceAmount.doubleValue());
        bilTtyCashDispositionRow.setDdsDsbDt(DateRoutine.defaultDateTime());
        bilTtyCashDispositionRow.setBilDspDt(execContext.getApplicationDate());
        bilTtyCashDispositionRow.setUserId(execContext.getUserSeqeunceId());
        bilTtyCashDispositionRow.setBilAdjDueDt(dueDate);
        bilTtyCashDispositionRow.setBilDspTypeCd(BilDspTypeCode.APPLIED.getValue());
        bilTtyCashDspRepository.saveAndFlush(bilTtyCashDispositionRow);
        logger.debug("insertAppliedDisposition function end.");
    }

    private Short findMaxSequenceNumberAutoReconciliationCash(String accountId, char bilTechKeyTypeCode) {
        Short sequenceNumber = bilSchAutoReconCshRepository.findMaxSequenceNumber(accountId, bilTechKeyTypeCode);
        if (sequenceNumber != null) {
            logger.debug("Maximum sequence number for BIL_SCH_AUTO_RECON_CSH: {}", (sequenceNumber + 1));
            return ++sequenceNumber;
        } else {
            logger.debug("Maximum sequence number for BIL_SCH_AUTO_RECON_CSH: {}", ZERO_VALUE);
            return SHORT_ZERO;
        }
    }

    private Short findMaxSequenceNumberTtyCashDisposition(String accountId, ZonedDateTime bilDistributionDate,
            Short sequenceNumber) {
        Short maxSequenceNumber = bilTtyCashDspRepository.findMaxDispositionNumber(accountId, bilDistributionDate,
                sequenceNumber);
        if (maxSequenceNumber != null) {
            logger.debug("Maximum sequence number for BIL_TTY_CASH_DSP: {}", (sequenceNumber + 1));
            return ++maxSequenceNumber;
        } else {
            logger.debug("Maximum sequence number for BIL_TTY_CASH_DSP: {}", ZERO_VALUE);
            return SHORT_ZERO;
        }
    }

    @Override
    public void reapplyPaymentForAgent(String accountId, ZonedDateTime dueDate, BigDecimal amount, String reason,
            String accountNumber, BilInvTtyAct invoiceActivityRow, ZonedDateTime entryDate, String entryNumber,
            short entrySequenceNumber, ZonedDateTime distributionDate, short distributionSequenceNumber, String userId,
            char cashEntryMethodCode, String bankCode, Short dispositionSequenceNumber, String accountType,
            BilAgent bilAgent) {

        logger.debug("Replacement Payment service start for accountId {}", accountId);

        BigDecimal differenceAmount = BigDecimal.valueOf(invoiceActivityRow.getBilMinDueAmt())
                .subtract(BigDecimal.valueOf(invoiceActivityRow.getBilSttReconAmt()));

        BilAgtCashDsp agentCashDisposition = bilAgtCashDspRepository.findById(
                new BilAgtCashDspId(accountId, distributionDate, distributionSequenceNumber, dispositionSequenceNumber))
                .orElse(null);
        if (agentCashDisposition == null) {
            throw new DataNotFoundException("payment.activity.does.not.exists");
        }
        BigDecimal orignalDispositionAmount = BigDecimal.valueOf(agentCashDisposition.getBilDspAmt());
        if (differenceAmount.compareTo(BigDecimal.valueOf(ZERO_VALUE)) <= 0) {
            logger.debug("Statement already reconciled hence no updates from replacement service.");
            return;
        } else {
            updateBilAgentCashDSp(accountId, dueDate, amount, accountNumber, invoiceActivityRow, entryDate, entryNumber,
                    entrySequenceNumber, distributionDate, distributionSequenceNumber, userId, cashEntryMethodCode,
                    bankCode, bilAgent, differenceAmount, orignalDispositionAmount, agentCashDisposition);
        }
        logger.debug("Replacement Payment service end for accountId {}", accountId);

    }

    private void updateBilAgentCashDSp(String accountId, ZonedDateTime dueDate, BigDecimal amount, String accountNumber,
            BilInvTtyAct invoiceActivityRow, ZonedDateTime entryDate, String entryNumber, short entrySequenceNumber,
            ZonedDateTime distributionDate, short distributionSequenceNumber, String userId, char cashEntryMethodCode,
            String bankCode, BilAgent bilAgent, BigDecimal differenceAmount, BigDecimal orignalDispositionAmount,
            BilAgtCashDsp agentCashDisposition) {

        BigDecimal applyAmount;
        if (amount.compareTo(differenceAmount) >= 0) {
            applyAmount = differenceAmount;
        } else {
            applyAmount = amount;
        }
        applyAmountToStatement(accountId, dueDate, distributionSequenceNumber, distributionDate, applyAmount,
                accountNumber, invoiceActivityRow, entrySequenceNumber, entryDate, entryNumber, userId,
                cashEntryMethodCode, bankCode, bilAgent);
        BigDecimal insertAmount = amount.subtract(applyAmount);
        BigDecimal updateAmount = orignalDispositionAmount.subtract(amount);
        if (orignalDispositionAmount.compareTo(amount) > 0) {
            if (amount.compareTo(applyAmount) > 0) {
                logger.debug(
                        "Receipt amount is greater than transferred amount and transferred amount is greater than applyAmount.");
                updateCurrentDisposition(updateAmount, agentCashDisposition);
                insertBilagentCashDisposition(agentCashDisposition, accountId, distributionDate,
                        distributionSequenceNumber, insertAmount.doubleValue(), dueDate);
                callFinancialApi(insertAmount.doubleValue(), execContext.getApplicationDateTime(), accountId,
                        entrySequenceNumber, bankCode, entryDate, entryNumber, userId, accountNumber,
                        ObjectCode.CASH.toString(), BilCashEntryMethodCode.SUSPENDED.getValue(), BLANK_STRING,
                        String.valueOf(cashEntryMethodCode), bilAgent);
            } else {
                logger.debug(
                        "Receipt amount is greater than transferred amount and transferred amount is equal or less than applyAmount.");
                updateCurrentDisposition(updateAmount, agentCashDisposition);
            }
        } else if (orignalDispositionAmount.equals(amount)) {
            if (amount.compareTo(applyAmount) > 0) {
                logger.debug(
                        "Receipt amount is equal to transferred amount and transferred amount is greater than applyAmount.");
                insertBilagentCashDisposition(agentCashDisposition, accountId, distributionDate,
                        distributionSequenceNumber, insertAmount.doubleValue(), dueDate);
                callFinancialApi(insertAmount.doubleValue(), execContext.getApplicationDate(), accountId,
                        entrySequenceNumber, bankCode, entryDate, entryNumber, userId, accountNumber,
                        ObjectCode.CASH.toString(), BilCashEntryMethodCode.SUSPENDED.getValue(), BLANK_STRING,
                        String.valueOf(cashEntryMethodCode), bilAgent);
                bilAgtCashDspRepository.delete(agentCashDisposition);
            } else {
                logger.debug(
                        "Receipt amount is equal to transferred amount and transferred amount is equal or less than applyAmount.");
                logger.debug("Delete current disposition row.");
                bilAgtCashDspRepository.delete(agentCashDisposition);
            }
        }

    }

    private void applyAmountToStatement(String accountId, ZonedDateTime dueDate, short distributionSequenceNumber,
            ZonedDateTime distributionDate, BigDecimal amount, String accountNumber, BilInvTtyAct invoiceActivityRow,
            short entrySequenceNumber, ZonedDateTime entryDate, String entryNumber, String userId,
            char cashEntryMethodCode, String bankCode, BilAgent bilAgent) {
        logger.debug("applyAmountToStatement function start.");
        insertAppliedDispositionForAgent(accountId, dueDate, distributionSequenceNumber, distributionDate, amount);
        updateInvoice02RowForAgent(invoiceActivityRow, amount);
        callFinancialApi(amount.doubleValue(), execContext.getApplicationDateTime(), accountId, entrySequenceNumber,
                bankCode, entryDate, entryNumber, userId, accountNumber, ObjectCode.PREMIUM.toString(),
                BilCashEntryMethodCode.APPLIED.getValue(), PayableItemCode.TRR.getValue(),
                String.valueOf(cashEntryMethodCode), bilAgent);
        callFinancialApi(amount.doubleValue(), execContext.getApplicationDateTime(), accountId, entrySequenceNumber,
                bankCode, entryDate, entryNumber, userId, accountNumber, ObjectCode.CASH.toString(),
                BilCashEntryMethodCode.APPLY.getValue(), BLANK_STRING, String.valueOf(cashEntryMethodCode), bilAgent);
        logger.debug("applyAmountToStatement function end.");
    }

    private void insertBilagentCashDisposition(BilAgtCashDsp agentCashDisposition, String accountId,
            ZonedDateTime distributionDate, Short sequenceNumber, Double amount, ZonedDateTime dueDate) {
        logger.debug("insertBilagentCashDisposition function start.");
        BilAgtCashDsp agentCashDispositionRow = new BilAgtCashDsp();
        Short dispositionNumber = findMaxSequenceNumberAgentCashDisposition(accountId, distributionDate,
                sequenceNumber);
        logger.debug(DISPOSITION_NUMBER, dispositionNumber);
        agentCashDispositionRow.setBilAgtCashDspId(
                new BilAgtCashDspId(accountId, distributionDate, sequenceNumber, dispositionNumber));
        agentCashDispositionRow.setBilDspAmt(amount);
        agentCashDispositionRow.setBilDsbDt(DateRoutine.defaultDateTime());
        agentCashDispositionRow.setBilDspDt(execContext.getApplicationDate());
        agentCashDispositionRow.setUserId(execContext.getUserSeqeunceId());
        agentCashDispositionRow.setBilAdjDueDt(dueDate);
        agentCashDispositionRow.setBilManualSusInd(agentCashDisposition.getBilManualSusInd());
        agentCashDispositionRow.setBilDspReasonCd(agentCashDisposition.getBilDspReasonCd());
        agentCashDispositionRow.setBilToFroTrfNbr(agentCashDisposition.getBilToFroTrfNbr());
        agentCashDispositionRow.setBilDspTypeCd(agentCashDisposition.getBilDspTypeCd());
        agentCashDispositionRow.setBilPayeeCltId(agentCashDisposition.getBilPayeeCltId());
        agentCashDispositionRow.setBilPayeeAdrSeq(agentCashDisposition.getBilPayeeAdrSeq());
        agentCashDispositionRow.setDwsCkDrfNbr(agentCashDisposition.getDwsCkDrfNbr());
        agentCashDispositionRow.setBilRevsRsusInd(agentCashDisposition.getBilRevsRsusInd());
        agentCashDispositionRow.setBilTrfTypeCd(agentCashDisposition.getBilTrfTypeCd());
        agentCashDispositionRow.setBilChkPrdMthCd(agentCashDisposition.getBilChkPrdMthCd());
        logger.debug("Intended insert operation on agentCashDisposition.");
        bilAgtCashDspRepository.saveAndFlush(agentCashDispositionRow);
        logger.debug("insertBilagentCashDisposition function end.");
    }

    private void callFinancialApi(Double amount, ZonedDateTime applicationDate, String accountId,
            short entrySequenceNumber, String bankCode, ZonedDateTime entryDate, String entryNumber, String userId,
            String accountNumber, String transactionObjectCode, String transactionActionCode, String paybleItemCode,
            String sourceCode, BilAgent bilAgent) {
        logger.debug("callFinancialApi function start.");
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(accountId);
        financialApiActivity.setApplicationName(ApplicationName.BACPGDIR.toString());
        financialApiActivity.setUserId(execContext.getUserId());
        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setErrorCode(ENTRY_NUMBER);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setAppProgramId(ApplicationProgramIdentifier.BCMORC.toString());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilTypeCode(bilAgent.getBilTypeCd());
        financialApiActivity.setBilClassCode(bilAgent.getBilClassCd());
        financialApiActivity.setAgentTtyId(bilAgent.getBilAgtActNbr());
        financialApiActivity.setPayableItemCode(paybleItemCode);
        financialApiActivity.setAgentTtyId(accountNumber);
        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setBilSourceCode(sourceCode);
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setStateCode(BLANK_STRING);
        financialApiActivity.setCurrencyCode(bilAgent.getCurrencyCd());
        BilMstCoSt bilMstCoSt = getMasterCompanyNumber(bilAgent.getBilClassCd(), bilAgent.getBilTypeCd());
        if (bilMstCoSt != null) {
            financialApiActivity.setMasterCompanyNbr(bilMstCoSt.getMasterCompanyNbr());
            financialApiActivity.setStateCode(bilMstCoSt.getBilStatePvnCd());
        }
        financialApiActivity.setBilDatabaseKey(getBilDatabaseKey(entryDate, entryNumber, userId, entrySequenceNumber));
        financialApiActivity.setStatusCode(BLANK_CHAR);
        logger.debug("Calling Financial API.");
        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            logger.error("Call to Financial API failed.");
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
        logger.debug("callFinancialApi function end.");
    }

    private void updateInvoice02RowForAgent(BilInvTtyAct invoiceActivityRow, BigDecimal amount) {
        logger.debug("Update BilInvTtyAct row function start.");
        BigDecimal statementAmount = BigDecimal.valueOf(invoiceActivityRow.getBilSttSvgAmt());
        invoiceActivityRow.setBilSttReconAmt(statementAmount.add(amount).doubleValue());
        bilInvTtyActRepository.save(invoiceActivityRow);
    }

    private void updateCurrentDisposition(BigDecimal updateAmount, BilAgtCashDsp agentCashDisposition) {
        logger.debug("updateCurrentDisposition function start.");
        agentCashDisposition.setBilDspAmt(updateAmount.doubleValue());
        bilAgtCashDspRepository.save(agentCashDisposition);
        logger.debug("updateCurrentDisposition function end.");
    }

    private void insertAppliedDispositionForAgent(String accountId, ZonedDateTime dueDate,
            Short distributionSequenceNumber, ZonedDateTime distributionDate, BigDecimal differenceAmount) {
        logger.debug("insertAppliedDisposition function start.");
        BilAgtCashDsp bilagentCashDispositionRow = new BilAgtCashDsp();
        Short dispositionNumber = findMaxSequenceNumberAgentCashDisposition(accountId, distributionDate,
                distributionSequenceNumber);
        logger.debug(DISPOSITION_NUMBER, dispositionNumber);
        bilagentCashDispositionRow.setBilAgtCashDspId(
                new BilAgtCashDspId(accountId, distributionDate, distributionSequenceNumber, dispositionNumber));
        bilagentCashDispositionRow.setBilDspAmt(differenceAmount.doubleValue());
        bilagentCashDispositionRow.setBilDsbDt(DateRoutine.defaultDateTime());
        bilagentCashDispositionRow.setBilDspDt(execContext.getApplicationDate());
        bilagentCashDispositionRow.setUserId(execContext.getUserSeqeunceId());
        bilagentCashDispositionRow.setBilAdjDueDt(dueDate);
        bilagentCashDispositionRow.setBilDspTypeCd(BilDspTypeCode.APPLIED.getValue());
        bilAgtCashDspRepository.saveAndFlush(bilagentCashDispositionRow);
        logger.debug("insertAppliedDisposition function end.");
    }

    private Short findMaxSequenceNumberAgentCashDisposition(String accountId, ZonedDateTime bilDistributionDate,
            Short sequenceNumber) {
        Short maxSequenceNumber = bilAgtCashDspRepository.findMaxDispositionNumber(accountId, bilDistributionDate,
                sequenceNumber);
        if (maxSequenceNumber != null) {
            logger.debug("Maximum sequence number for BIL_TTY_CASH_DSP: {} ", sequenceNumber + 1);
            return ++maxSequenceNumber;
        } else {
            logger.debug("Maximum sequence number for BIL_TTY_CASH_DSP {}: ", ZERO_VALUE);
            return SHORT_ZERO;
        }
    }

    private BilMstCoSt getMasterCompanyNumber(String bilClassCd, String bilTypeCd) {

        return bilMstCoStRepository.findRow(bilClassCd, bilTypeCd, BLANK_STRING);
    }
}
