package billing.service.impl;

import static billing.utils.BillingConstants.PLUS_SIGN;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_I;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_U;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_10;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;
import static disbursement.utils.DisbursementConstants.BIL_BANK_CODE;
import static disbursement.utils.DisbursementConstants.COMPANY_LOCATION_NUMBER;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import application.security.service.impl.AppSecurityServiceImpl;
import application.utils.model.PaymentProfile;
import application.utils.service.ActivityService;
import application.utils.service.PaymentIntegrationService;
import billing.data.entity.BilBankEntry;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilEftActivity;
import billing.data.entity.BilEftBankEd;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.BilObjectCfg;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSystemRules;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.BilUnidCshHst;
import billing.data.entity.id.BilBankEntryId;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilEftPendingTapeId;
import billing.data.entity.id.BilObjectCfgId;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.entity.id.BilUnIdCshHstId;
import billing.data.repository.BilBankEntryRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilEftActivityRepository;
import billing.data.repository.BilEftBankEdRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilObjectCfgRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilSystemRulesRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.data.repository.BilUnidCshHstRepository;
import billing.model.PayableOption;
import billing.service.BillingAccountBalanceService;
import billing.service.DisbursementSuspendPaymentService;
import billing.utils.BillingConstants.BankAccountType;
import billing.utils.BillingConstants.BankAccountTypeCd;
import billing.utils.BillingConstants.BilDesReasonCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilRules;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CashEntryMethod;
import billing.utils.BillingConstants.EftNoticeIndicator;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.WebAutomatedIndicator;
import billing.utils.BillingConstants.WipAcitivityId;
import billing.utils.BillingConstants.WipObjectCode;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.CommonConstants;
import core.utils.DateRoutine;
import core.utils.IdGenerator;
import disbursement.service.DisbursementAPIService;
import disbursement.utils.DisbursementConstants.DisbursementIdType;
import disbursement.utils.DisbursementConstants.DisbursementMediumCode;
import disbursement.utils.DisbursementConstants.DisbursementStatusCode;
import disbursement.utils.DisbursementConstants.DisbursementType;
import disbursement.utils.DisbursementConstants.ElectronicInformation;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.ObjectCode;
import party.data.entity.CcmAgency;
import party.data.entity.ClientReference;
import party.data.entity.ClientTab;
import party.data.entity.CltBankAccount;
import party.data.entity.CltFinancialItt;
import party.data.repository.CcmAgencyRepository;
import party.data.repository.ClientReferenceRepository;
import party.data.repository.ClientTabRepository;
import party.data.repository.CltBankAccountRepository;
import party.data.repository.CltFinancialIttRepository;

@Service
public class DisburseSuspendPaymentServiceImpl implements DisbursementSuspendPaymentService {

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private DisbursementAPIService disbursementService;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilObjectCfgRepository bilObjectCfgRepository;

    @Autowired
    private BilSystemRulesRepository bilSystemRulesRepository;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private BilBankEntryRepository bilBankEntryRepository;

    @Autowired
    private BilEftBankEdRepository bilEftBankEdRepository;

    @Autowired
    private BilEftActivityRepository bilEftActivityRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private CltBankAccountRepository cltBankAccountRepository;

    @Autowired
    private CltFinancialIttRepository cltFinancialIttRepository;

    @Autowired
    private ClientTabRepository clientTabRepository;

    @Autowired
    private CcmAgencyRepository ccmAgencyRepository;

    @Autowired
    private ClientReferenceRepository clientReferenceRepository;

    @Autowired
    private BilUnidCshHstRepository bilUnidCshHstRepository;

    @Autowired
    private BillingAccountBalanceService billingAccountBalanceService;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private PaymentIntegrationService paymentIntegrationService;

    private static final Logger logger = LogManager.getLogger(DisburseSuspendPaymentServiceImpl.class);

    private static final String MASTER_COMPANY = "99";
    private static final String ERROR_CODE = "0000";
    private static final String WIPOPST = "WIPOPST";
    private static final String DSBTYPECODE = String.valueOf(CHAR_U);
    private static final String AGENCY_SWEEP_CANNOT_BE_PROCESSED = "agency.sweep.cannot.be.processed";
    private static final String NO_RECORD_FOUND = "no.record.found";

    @Override
    @Transactional
    public void save(Short sequenceNumber, ZonedDateTime postedDate, String user, String batchId,
            ZonedDateTime distributionDate, PayableOption payableOption) {
        logger.debug("disbursement service : Start");
        String currencyCode = BLANK_STRING;

        String userId = appSecurityServiceImpl.authenticateUser(user).getUserSequenceId();
        logger.info("fetch row from bilUnidCash");
        BilUnIdCash bilUnidentifiedCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(distributionDate, sequenceNumber, batchId, userId, postedDate))
                .orElse(null);

        if (bilUnidentifiedCash == null) {
            logger.debug("Payment not found in BIL_UNID_CASH");
            throw new DataNotFoundException("payment.does.not.exists");
        }
        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId()))
                .orElse(null);
        if (bilCashEntryTot != null) {
            currencyCode = bilCashEntryTot.getCurrencyCd();
        }

        String fileTypeCode = BLANK_STRING;
        String agencyClient = BLANK_STRING;
        if (bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()) {
            String ruleText = checkAgencySweepProcessing();
            if (ruleText.isEmpty()) {
                throw new InvalidDataException(AGENCY_SWEEP_CANNOT_BE_PROCESSED);
            }
            logger.info("agency sweep data gathering.");
            fileTypeCode = ruleText.substring(0, 3);
            Boolean isContract = ruleText.charAt(6) == CHAR_Y;
            agencyClient = validateAgency(payableOption.getPayeeId(), payableOption.getReason().trim(),
                    bilUnidentifiedCash.getAgencyNbr(), isContract);
        }
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(BilRules.UNIDENTIFIED_CASH_HISTORY.getValue(),
                BLANK_STRING, BLANK_STRING, BLANK_STRING, execContext.getApplicationDate(),
                execContext.getApplicationDate());
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            logger.info("insert into unidentified cash");
            saveUnidentifiedCashHistory(bilUnidentifiedCash);
        }

        switch (DisbursementType.getEnumValue(payableOption.getRefundMethod().trim())) {
        case MANUAL:
            processManualDisbursement(bilUnidentifiedCash, payableOption, currencyCode);
            break;
        case SYSTEM:
            processSystemDisbursement(bilUnidentifiedCash, payableOption, currencyCode);
            break;
        case ELECTRONIC:
            processElectronicDisbursement(bilUnidentifiedCash, payableOption, fileTypeCode, agencyClient, currencyCode);
            break;
        default:
            logger.warn("Refund Method Entered is incorrect");
            throw new InvalidDataException("bad.request.data.invalid", new Object[] { "refund method" });
        }

        bilRulesUct = bilRulesUctRepository.getBilRulesRow(BilRules.ACCOUNT_BALANCE.getValue(), BLANK_STRING,
                BLANK_STRING, BLANK_STRING, execContext.getApplicationDate(), execContext.getApplicationDate());

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            billingAccountBalanceService.saveUnidentifiedBalance();
        }
        logger.debug("disbursement service : end");
    }

    private void processElectronicDisbursement(BilUnIdCash bilUnidentifiedCash, PayableOption payableOption,
            String fileTypeCode, String agencyClient, String currencyCode) {
        String processingLocationCode = findProcessingLocationCode(bilUnidentifiedCash.getBilBankCd());
        String folderId = generateTechnicalObjectKey(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getUserId());

        String technicalDisbursementId = getTechnicalDisbursementId(bilUnidentifiedCash.getBilCshEtrMthCd(),
                bilUnidentifiedCash.getBillUnidCashId(), bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt());

        String disbursementId = getElectronicPaymentInformation(bilUnidentifiedCash, payableOption,
                processingLocationCode, DSBTYPECODE, folderId, technicalDisbursementId, fileTypeCode, agencyClient,
                currencyCode);

        if (!payableOption.getTransactionId().trim().isEmpty()
                || bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()
                || bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()
                || bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()
                        && checkIfEftPayment(bilUnidentifiedCash.getBillUnidCashId(),
                                bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr())) {

            updateUnidentifiedCash(DisbursementStatusCode.DISBURSED.toChar(), payableOption.getRefundMethod(),
                    payableOption.getReason(), payableOption.getCheckNumber(), disbursementId,
                    bilUnidentifiedCash.getBilRctAmt(), bilUnidentifiedCash, payableOption.getPayeeId(),
                    payableOption.getAddressSequenceNumber());

            saveFinancialActivity(folderId, bilUnidentifiedCash, execContext.getApplicationDateTime(),
                    ObjectCode.UNIDENTIFIED_CASH.toString(), ActionCode.AUTHOR_DISBURSEMENT.toString(), CHAR_Y,
                    ApplicationName.BCWSUNID.toString(), CHAR_A, DisbursementIdType.BCMS.getDisbursementIdType(),
                    ApplicationProgramIdentifier.BACPUDS.toString(), disbursementId, payableOption.getReason(),
                    currencyCode);

            saveFinancialActivity(folderId, bilUnidentifiedCash, execContext.getApplicationDateTime(),
                    ObjectCode.UNIDENTIFIED_CASH.toString(), ActionCode.UPDATED.toString(), CHAR_Y,
                    ApplicationName.BCWSUNID.toString(), CHAR_A, DisbursementIdType.BCMS.getDisbursementIdType(),
                    ApplicationProgramIdentifier.BACPUDS.toString(), disbursementId, payableOption.getReason(),
                    currencyCode);

        } else if (bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()
                || bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.ONE_TIME_ACH.getCashEntryMethod()) {

            updateUnidentifiedCash(DisbursementStatusCode.PENDING.toChar(), payableOption.getRefundMethod(),
                    payableOption.getReason(), payableOption.getCheckNumber(), disbursementId,
                    bilUnidentifiedCash.getBilRctAmt(), bilUnidentifiedCash, payableOption.getPayeeId(),
                    payableOption.getAddressSequenceNumber());

            saveFinancialActivity(folderId, bilUnidentifiedCash, execContext.getApplicationDateTime(),
                    ObjectCode.UNIDENTIFIED_CASH.toString(), ActionCode.AUTHOR_DISBURSEMENT.toString(), CHAR_Y,
                    ApplicationName.BCWSUNID.toString(), CHAR_A, DisbursementIdType.BCMS.getDisbursementIdType(),
                    ApplicationProgramIdentifier.BACPUDS.toString(), disbursementId, payableOption.getReason(),
                    currencyCode);
        }

        BilObjectCfg bilObjectCfg = bilObjectCfgRepository.findById(new BilObjectCfgId(WIPOPST, CHAR_I)).orElse(null);
        if (bilObjectCfg != null && bilObjectCfg.getBocObjPrcCd() == CHAR_Y) {
            processWipActivities(bilUnidentifiedCash.getBilAccountNbr(), bilUnidentifiedCash.getBilRctAmt(),
                    bilUnidentifiedCash.getPolNbr(), bilUnidentifiedCash.getBilAgtActNbr(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                    bilUnidentifiedCash.getBilUnIdCashId().getUserId());
        }
    }

    private String getElectronicPaymentInformation(BilUnIdCash bilUnidentifiedCash, PayableOption payableOption,
            String processingLocationCode, String dsbTypeCode, String folderId, String technicalDisbursementId,
            String fileTypeCode, String agencyClient, String currencyCode) {
        String disbursementId = null;
        switch (CashEntryMethod.getEnumValue(bilUnidentifiedCash.getBilCshEtrMthCd())) {
        case BANK_ACCOUNT:
            disbursementId = loadBankInformation(DisbursementMediumCode.BANKACCOUNT.toString(), DisbursementIdType.BACM,
                    bilUnidentifiedCash, payableOption, processingLocationCode, dsbTypeCode,
                    DisbursementStatusCode.DISBURSED.toChar(), folderId, technicalDisbursementId, currencyCode);
            break;
        case CREDIT_CARD:
            if (checkIfEftPayment(bilUnidentifiedCash.getBillUnidCashId(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr())) {
                disbursementId = loadCreditCardInformation(DisbursementMediumCode.CREDITCARD.toString(),
                        DisbursementIdType.BEFM, bilUnidentifiedCash, payableOption, processingLocationCode,
                        dsbTypeCode, DisbursementStatusCode.DISBURSED.toChar(), folderId, technicalDisbursementId,
                        currencyCode);
            } else {
                disbursementId = loadCreditCardInformation(DisbursementMediumCode.CREDITCARD.toString(),
                        DisbursementIdType.BCCM, bilUnidentifiedCash, payableOption, processingLocationCode,
                        dsbTypeCode, DisbursementStatusCode.PENDING.toChar(), folderId, technicalDisbursementId,
                        currencyCode);
            }
            break;
        case ONE_TIME_ACH:
            disbursementId = loadBankInformation(DisbursementMediumCode.AUTOMATEDCLEARINGHOUSE.toString(),
                    DisbursementIdType.BPCM, bilUnidentifiedCash, payableOption, processingLocationCode, dsbTypeCode,
                    DisbursementStatusCode.PENDING.toChar(), folderId, technicalDisbursementId, currencyCode);
            break;
        case AGENCY_SWEEP:
            disbursementId = loadAgencySweepInformation(DisbursementMediumCode.BANKACCOUNT.toString(),
                    DisbursementIdType.BASM, bilUnidentifiedCash, payableOption, processingLocationCode, dsbTypeCode,
                    folderId, fileTypeCode, agencyClient, currencyCode);
            break;
        default:
            logger.error("Invalid Payment type for electronic payment");
            throw new InvalidDataException("invalid.electronic.type",
                    new Object[] { bilUnidentifiedCash.getBilCshEtrMthCd() });

        }
        return disbursementId;
    }

    private String loadAgencySweepInformation(String mediumCode, DisbursementIdType disbursementIdType,
            BilUnIdCash bilUnidentifiedCash, PayableOption payableOption, String processingLocationCode,
            String dsbTypeCode, String folderId, String fileTypeCode, String agencyClient, String currencyCode) {

        String longName;
        char accountType;
        String bankAccountType;
        String routingNumber;
        String bankAccountNumber;

        List<BilEftActivity> biEftActivityList = bilEftActivityRepository.getPaymentIdRow(
                bilUnidentifiedCash.getBillUnidCashId(), CHAR_U, bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(), CHAR_A);

        if (biEftActivityList == null || biEftActivityList.isEmpty()
                || biEftActivityList.get(SHORT_ZERO).getBilPmtOrderId().trim().isEmpty()) {
            List<Object[]> objectData = cltBankAccountRepository.getMaxEffectiveDateAndAccountSequenceNumber(
                    agencyClient, SHORT_ZERO, execContext.getApplicationDate());
            if (objectData.isEmpty() || objectData.get(0)[0] == null || objectData.get(0)[1] == null) {
                throw new DataNotFoundException(NO_RECORD_FOUND, new Object[] { agencyClient });
            }
            CltBankAccount clientBankAccount = cltBankAccountRepository.findBankTapeData(agencyClient, SHORT_ZERO,
                    DateRoutine.dateTimeAsYYYYMMDD(objectData.get(0)[0].toString()),
                    Short.valueOf(objectData.get(0)[1].toString()));
            if (clientBankAccount == null) {
                throw new DataNotFoundException(NO_RECORD_FOUND, new Object[] { agencyClient });
            }
            accountType = findAccountType(clientBankAccount.getCibaBkActTypCd());
            routingNumber = clientBankAccount.getCibaBkRteNbr();
            bankAccountNumber = clientBankAccount.getCibaBkActNbr();
            CltFinancialItt clientFinancialItt = cltFinancialIttRepository
                    .findByCltFinancialIttIdTchObjectKey(clientBankAccount.getCibaBkRteNbr());
            if (clientFinancialItt == null) {
                throw new DataNotFoundException(NO_RECORD_FOUND, new Object[] { clientBankAccount.getCibaBkRteNbr() });
            }
            ClientTab clientTab = clientTabRepository.findClient(agencyClient,
                    clientFinancialItt.getCltFinancialIttId().getHistoryVldNbr(), clientFinancialItt.getExpirationDt());
            if (clientTab == null) {
                throw new DataNotFoundException(NO_RECORD_FOUND, new Object[] { agencyClient });
            }
            longName = clientTab.getCiclLngNm().trim();

        } else {
            PaymentProfile paymentProfie = paymentIntegrationService
                    .getPaymentProfile(biEftActivityList.get(SHORT_ZERO).getBilPmtOrderId());
            longName = paymentProfie.getBankAccount().getNameOnAccount();
            bankAccountType = paymentProfie.getBankAccount().getAccountType();
            routingNumber = paymentProfie.getBankAccount().getRoutingNumber();
            bankAccountNumber = paymentProfie.getBankAccount().getAccountNumber();
            accountType = bankAccountType.charAt(SHORT_ZERO);
        }

        String memoText = findMemoText(payableOption.getReason());
        String userMemoText = generateMemoTextForAgencySweep(accountType, longName, routingNumber, bankAccountNumber,
                ElectronicInformation.BAS.getElectronicInformation(), bilUnidentifiedCash.getBillUnidCashId(),
                DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt()),
                String.valueOf(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr()));

        String disbursementId = SEPARATOR_BLANK;
        if (payableOption.getTransactionId().isEmpty()) {
            disbursementId = disbursementService.saveDisbursementActivity(folderId, payableOption.getRefundMethod(),
                    DisbursementStatusCode.DISBURSED.toChar(), processingLocationCode, payableOption.getCheckNumber(),
                    payableOption.getReason(), mediumCode, disbursementIdType,
                    DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt()),
                    payableOption.getPayeeId(), bilUnidentifiedCash.getBilRctAmt(), memoText, userMemoText, dsbTypeCode,
                    payableOption.getAddressSequenceNumber(), bilUnidentifiedCash.getBillUnidCashId(), currencyCode);
        }

        saveEftPendingTape(bilUnidentifiedCash.getBillUnidCashId(), bilUnidentifiedCash.getBilRctAmt(),
                bilUnidentifiedCash, BLANK_STRING, BLANK_CHAR, disbursementId, fileTypeCode,
                payableOption.getTransactionId());
        return disbursementId;
    }

    private String generateMemoTextForAgencySweep(Character accountTypeCode, String clientBankName,
            String parmRoutingTransitNumber, String bankAccountNumber, String electronicInformation, String accountId,
            String date, String sequenceNumber) {
        StringBuilder sb = new StringBuilder(103);
        sb.append(electronicInformation);
        sb.append(accountTypeCode);
        sb.append(StringUtils.rightPad(bankAccountNumber, 25, BLANK_STRING));
        sb.append(StringUtils.rightPad(parmRoutingTransitNumber, 10));
        sb.append(StringUtils.rightPad(clientBankName, 40));
        sb.append(StringUtils.rightPad(accountId, 8));
        sb.append(StringUtils.rightPad(date, 10));
        sb.append(StringUtils.rightPad(sequenceNumber, 6));
        return sb.toString();
    }

    private String validateAgency(String payeeId, String reason, String agencyNumber, Boolean isContract) {
        String agencyClient = BLANK_STRING;

        if (Boolean.TRUE.equals(isContract)) {
            CcmAgency ccmAgency = ccmAgencyRepository.findCagAgcCltIdByCagAgencyNbr(agencyNumber);
            agencyClient = ccmAgency.getCagAgcCltId();
        } else {
            List<ClientReference> clientReference = clientReferenceRepository
                    .findClientReferenceIdClientIdByCirfRefIdAndRefTypCd(agencyNumber, getReferenceTypeCodeList());
            if (!clientReference.isEmpty()) {
                agencyClient = clientReference.get(SHORT_ZERO).getClientReferenceId().getClientId();
            }
        }
        if (!agencyClient.isEmpty()) {
            if (agencyClient.equals(payeeId) && !(reason.equals(BilDesReasonCode.AGENY_REFUND_CHECK.getValue())
                    || reason.equals(BilDesReasonCode.REFUND_TO_AGENCY.getValue()))) {
                throw new InvalidDataException("invalid.reason");
            }
            if (!agencyClient.equals(payeeId) && (reason.equals(BilDesReasonCode.AGENY_REFUND_CHECK.getValue())
                    || reason.equals(BilDesReasonCode.REFUND_TO_AGENCY.getValue()))) {
                throw new InvalidDataException("invalid.reason");
            }
        } else {
            throw new InvalidDataException(AGENCY_SWEEP_CANNOT_BE_PROCESSED);
        }
        return agencyClient;
    }

    public String checkAgencySweepProcessing() {
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(BilRules.AGENCY_SWEEP_PROCESSING.getValue(),
                BLANK_STRING, BLANK_STRING, BLANK_STRING, execContext.getApplicationDate(),
                execContext.getApplicationDate());
        if (bilRulesUct == null || bilRulesUct.getBrtRuleCd() != CHAR_Y
                || bilRulesUct.getBrtParmListTxt().trim().isEmpty()) {
            throw new InvalidDataException(AGENCY_SWEEP_CANNOT_BE_PROCESSED);
        }
        return bilRulesUct.getBrtParmListTxt().trim();
    }

    private String loadCreditCardInformation(String mediumCode, DisbursementIdType disbursementIdType,
            BilUnIdCash bilUnidentifiedCash, PayableOption payableOption, String processingLocationCode,
            String dsbTypeCode, char statusCode, String folderId, String technicalDisbursementId, String currencyCode) {
        String memoText = findMemoText(payableOption.getReason());
        String userMemoText = generateMemoTextForCreditCard(bilUnidentifiedCash.getBilCrcrdTypeCd(),
                bilUnidentifiedCash.getBilCrcrdActNbr(), bilUnidentifiedCash.getBilCrcrdExpDt(),
                DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt()),
                bilUnidentifiedCash.getBillUnidCashId(),
                String.valueOf(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr()),
                ElectronicInformation.BCC.getElectronicInformation());
        String disbursementId = SEPARATOR_BLANK;
        if (payableOption.getTransactionId().isEmpty()) {
            disbursementId = disbursementService.saveDisbursementActivity(folderId, payableOption.getRefundMethod(),
                    statusCode, processingLocationCode, payableOption.getCheckNumber(), payableOption.getReason(),
                    mediumCode, disbursementIdType,
                    DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt()),
                    payableOption.getPayeeId(), bilUnidentifiedCash.getBilRctAmt(), memoText, userMemoText, dsbTypeCode,
                    payableOption.getAddressSequenceNumber(), technicalDisbursementId, currencyCode);
        }

        if (statusCode == DisbursementStatusCode.DISBURSED.toChar()) {
            saveEftPendingTape(bilUnidentifiedCash.getBillUnidCashId(), bilUnidentifiedCash.getBilRctAmt(),
                    bilUnidentifiedCash, BLANK_STRING, BLANK_CHAR, disbursementId, BLANK_STRING,
                    payableOption.getTransactionId());
        }

        return disbursementId;
    }

    private void saveEftPendingTape(String accountId, Double amount, BilUnIdCash bilUnidentifiedCash,
            String accountTypeCode, Character webAuthIndicator, String disbursementId, String fileType,
            String transactionId) {
        BilEftPendingTape eftPendingTape = new BilEftPendingTape();
        Short sequenceNumber = getIncrementalValue(bilEftPendingTapeRepository.getMaxSequenceNumber(accountId,
                EftRecordType.UNIDENTIFIED.getValue(), execContext.getApplicationDate()));
        eftPendingTape.setBilEftPendingTapeId(new BilEftPendingTapeId(accountId, EftRecordType.UNIDENTIFIED.getValue(),
                execContext.getApplicationDate(), execContext.getApplicationDate(), sequenceNumber));
        eftPendingTape.setEftNotIndicator(EftNoticeIndicator.NO_NOTICE.getValue());
        eftPendingTape.setEftReceivedType(EftRecordType.ONE_TIME_ACH.getValue());
        eftPendingTape.setReceiptSequenceNumber(SHORT_ZERO);
        eftPendingTape.setEftDrAmount(amount * -1);
        eftPendingTape.setBusinessActivityCd(CHAR_N);
        eftPendingTape.setEftDrDate(execContext.getApplicationDate());
        eftPendingTape.setEftPostPaymentDate(execContext.getApplicationDate());
        eftPendingTape.setInsertedRowTime(execContext.getApplicationDateTime());
        eftPendingTape.setDisbursementId(disbursementId);
        eftPendingTape.setWebAutomatedIndicator(webAuthIndicator);
        if (bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(accountTypeCode,
                    BilDesReasonCode.ACCOUNT_TYPE.getValue(), execContext.getLanguage());
            if (busCdTranslation != null) {
                eftPendingTape.setBusinessActivityCd(CHAR_Y);
            }
            BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(
                    BilDesReasonType.ONE_XA.getValue(), BilDesReasonCode.FTC.getValue(), execContext.getLanguage());
            if (busCodeTranslation != null) {
                eftPendingTape.setFileTypeCd(Short.valueOf(busCodeTranslation.getBusDescription()));
            }
        } else if (bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()
                && checkIfEftPayment(accountId, bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr())) {
            eftPendingTape.setFileTypeCd(SHORT_ZERO);
            BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(BilRules.ONE_TIME_CREDIT_CARD.getValue(),
                    BLANK_STRING, BLANK_STRING, BLANK_STRING, execContext.getApplicationDate(),
                    execContext.getApplicationDate());
            if (bilRulesUct != null) {
                eftPendingTape.setFileTypeCd(Short.valueOf(bilRulesUct.getBrtParmListTxt()));
            }
            eftPendingTape.setEftReceivedType(EftRecordType.UNIDENTIFIED.getValue());
        } else if (bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()) {

            eftPendingTape.setFileTypeCd(Short.valueOf(fileType));
            eftPendingTape.setWebAutomatedIndicator(WebAutomatedIndicator.NOT_AUTHORIZED.getValue());
            eftPendingTape.setEftReceivedType(EftRecordType.AGENCY_SWEEP.getValue());
            eftPendingTape.setAgencyNbr(bilUnidentifiedCash.getAgencyNbr());
        }
        eftPendingTape.setPaymentOrderId(transactionId);
        bilEftPendingTapeRepository.save(eftPendingTape);
    }

    private Short getIncrementalValue(Short sequenceNumber) {
        if (sequenceNumber == null) {
            return CommonConstants.SHORT_ZERO;
        } else {
            return (short) (sequenceNumber + 1);
        }
    }

    private String generateMemoTextForCreditCard(Character creditCardType, String creditCardNumber,
            String creditCardExpiryDate, String date, String accountId, String sequenceNumber,
            String electronicInformation) {
        StringBuilder sb = new StringBuilder(103);
        sb.append(electronicInformation);
        sb.append(creditCardType);
        sb.append(StringUtils.rightPad(creditCardNumber.trim(), 16));
        sb.append(StringUtils.rightPad(creditCardExpiryDate, 59));
        sb.append(StringUtils.rightPad(accountId, 8));
        sb.append(StringUtils.rightPad(date, 10));
        sb.append(StringUtils.rightPad(sequenceNumber, 6));
        return sb.toString();
    }

    private boolean checkIfEftPayment(String accountId, Short sequenceNumber) {
        Short count = bilEftActivityRepository.checkIfExists(accountId, EftRecordType.UNIDENTIFIED.getValue(),
                sequenceNumber, EftRecordType.UNIDENTIFIED.getValue());
        return count != null && count > SHORT_ZERO;
    }

    private String loadBankInformation(String mediumCode, DisbursementIdType disbursementIdType,
            BilUnIdCash bilUnidentifiedCash, PayableOption payableOption, String processingLocationCode,
            String dsbTypeCode, char statusCode, String folderId, String technicalDisbursementId, String currencyCode) {
        BilBankEntry bilBankEntry = bilBankEntryRepository
                .findById(new BilBankEntryId(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(), bilUnidentifiedCash.getBilEntrySeqNbr()))
                .orElse(null);
        if (bilBankEntry == null) {
            throw new DataNotFoundException("entry.number.not.found",
                    new Object[] { bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr() });
        }
        String userMemoText;
        if (bilBankEntry.getRoutingTransitNbr().trim().isEmpty()
                || bilBankEntry.getRoutingTransitNbr().trim().matches("XXX(.*)")) {
            userMemoText = generateMemoText(findAccountType(bilBankEntry.getAccountTypCd()),
                    bilBankEntry.getBankAccountNbr(), bilBankEntry.getRoutingTransitNbr(), BLANK_STRING,
                    ElectronicInformation.BAC.getElectronicInformation(), bilUnidentifiedCash.getBillUnidCashId(),
                    BLANK_STRING, BLANK_STRING);
        } else {
            BilEftBankEd bilEftBankEd = bilEftBankEdRepository
                    .findByRoutingTransitNbr(bilBankEntry.getRoutingTransitNbr());
            if (bilEftBankEd.getPreferredRoutingTransitNbr().trim().isEmpty()) {
                bilEftBankEd.setPreferredRoutingTransitNbr(bilEftBankEd.getRoutingTransitNbr());
            }
            userMemoText = generateMemoText(findAccountType(bilBankEntry.getAccountTypCd()),
                    bilBankEntry.getBankAccountNbr(), bilEftBankEd.getPreferredRoutingTransitNbr(),
                    bilEftBankEd.getBankName(), ElectronicInformation.BAC.getElectronicInformation(),
                    bilUnidentifiedCash.getBillUnidCashId(), BLANK_STRING, BLANK_STRING);
        }

        String memoText = findMemoText(payableOption.getReason());
        String disbursementId = SEPARATOR_BLANK;
        if (payableOption.getTransactionId().isEmpty()) {
            disbursementId = disbursementService.saveDisbursementActivity(folderId, payableOption.getRefundMethod(),
                    statusCode, processingLocationCode, payableOption.getCheckNumber(), payableOption.getReason(),
                    mediumCode, disbursementIdType,
                    DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt()),
                    payableOption.getPayeeId(), bilUnidentifiedCash.getBilRctAmt(), memoText, userMemoText, dsbTypeCode,
                    payableOption.getAddressSequenceNumber(), technicalDisbursementId, currencyCode);
        }

        if (statusCode == DisbursementStatusCode.DISBURSED.toChar()) {
            saveEftPendingTape(bilUnidentifiedCash.getBillUnidCashId(), bilUnidentifiedCash.getBilRctAmt(),
                    bilUnidentifiedCash, bilBankEntry.getAccountTypCd(), bilBankEntry.getWebAuthInd(), disbursementId,
                    BLANK_STRING, payableOption.getTransactionId());
        }
        return disbursementId;
    }

    private String generateMemoText(Character accountType, String bankAccountNumber, String parmRoutingTransitNumber,
            String bankName, String electronicInformation, String accountId, String date, String sequenceNumber) {
        StringBuilder sb = new StringBuilder(103);
        sb.append(electronicInformation);
        sb.append(accountType);
        sb.append(StringUtils.rightPad(bankAccountNumber, 25, BLANK_STRING));
        sb.append(StringUtils.rightPad(parmRoutingTransitNumber, 10));
        sb.append(StringUtils.rightPad(bankName, 40));
        sb.append(StringUtils.rightPad(accountId, 8));
        sb.append(StringUtils.rightPad(date, 10));
        sb.append(StringUtils.rightPad(sequenceNumber, 6));
        return sb.toString();
    }

    private Character findAccountType(String accountTypeCode) {
        if (accountTypeCode.equalsIgnoreCase(BankAccountTypeCd.BUSINESS_SAVINGS.toString())
                || accountTypeCode.equalsIgnoreCase(BankAccountTypeCd.PERSONAL_SAVINGS.toString())
                || accountTypeCode.equalsIgnoreCase(BankAccountTypeCd.SAVINGS.toString())
                || accountTypeCode.equalsIgnoreCase(BankAccountTypeCd.BUSINESS_SAVE.toString())) {
            return BankAccountType.SAVING.toChar();
        } else if (accountTypeCode.equalsIgnoreCase(BankAccountTypeCd.BUSINESS_CHECKING.toString())
                || accountTypeCode.equalsIgnoreCase(BankAccountTypeCd.PERSONAL_CHECKING.toString())
                || accountTypeCode.equalsIgnoreCase(BankAccountTypeCd.CHECK.toString())
                || accountTypeCode.equalsIgnoreCase(BankAccountTypeCd.BUSINESS_CHECK.toString())) {
            return BankAccountType.CHECKING.toChar();
        } else if (accountTypeCode.equalsIgnoreCase(BankAccountTypeCd.MONEY_MARKET.toString())) {
            return BankAccountType.MONEY_MARKET.toChar();
        }
        return BLANK_CHAR;
    }

    private void processSystemDisbursement(BilUnIdCash bilUnidentifiedCash, PayableOption payableOption,
            String currencyCode) {
        String processingLocationCode = findProcessingLocationCode(bilUnidentifiedCash.getBilBankCd());
        String memoText = findMemoText(payableOption.getReason());
        logger.info("folder id for fms call.");
        String folderId = generateTechnicalObjectKey(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getUserId());

        String technicalDisbursementId = getTechnicalDisbursementId(bilUnidentifiedCash.getBilCshEtrMthCd(),
                bilUnidentifiedCash.getBillUnidCashId(), bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt());

        String disbursementId = disbursementService.saveDisbursementActivity(folderId, payableOption.getRefundMethod(),
                DisbursementStatusCode.PENDING.toChar(), processingLocationCode, payableOption.getCheckNumber(),
                payableOption.getReason(), DisbursementMediumCode.CHECK.toString(), DisbursementIdType.BCMS,
                DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt()),
                payableOption.getPayeeId(), bilUnidentifiedCash.getBilRctAmt(), memoText, memoText, DSBTYPECODE,
                payableOption.getAddressSequenceNumber(), technicalDisbursementId, currencyCode);

        updateUnidentifiedCash(DisbursementStatusCode.PENDING.toChar(), payableOption.getRefundMethod(),
                payableOption.getReason(), payableOption.getCheckNumber(), disbursementId,
                bilUnidentifiedCash.getBilRctAmt(), bilUnidentifiedCash, payableOption.getPayeeId(),
                payableOption.getAddressSequenceNumber());

        saveFinancialActivity(folderId, bilUnidentifiedCash, execContext.getApplicationDateTime(),
                ObjectCode.UNIDENTIFIED_CASH.toString(), ActionCode.AUTHOR_DISBURSEMENT.toString(), CHAR_Y,
                ApplicationName.BCWSUNID.toString(), CHAR_A, DisbursementIdType.BCMS.getDisbursementIdType(),
                ApplicationProgramIdentifier.BACPUDS.toString(), disbursementId, payableOption.getReason(),
                currencyCode);

        BilObjectCfg bilObjectCfg = bilObjectCfgRepository.findById(new BilObjectCfgId(WIPOPST, CHAR_I)).orElse(null);
        if (bilObjectCfg != null && bilObjectCfg.getBocObjPrcCd() == CHAR_Y) {
            processWipActivities(bilUnidentifiedCash.getBilAccountNbr(), bilUnidentifiedCash.getBilRctAmt(),
                    bilUnidentifiedCash.getPolNbr(), bilUnidentifiedCash.getBilAgtActNbr(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                    bilUnidentifiedCash.getBilUnIdCashId().getUserId());
        }

    }

    private String getTechnicalDisbursementId(char cashEntryMethodeCode, String unidentifiedCashId,
            ZonedDateTime entryDate) {
        switch (CashEntryMethod.getEnumValue(cashEntryMethodeCode)) {
        case BANK_ACCOUNT:
        case AGENCY_SWEEP:
            return unidentifiedCashId;
        default:
            StringBuilder date = new StringBuilder();
            date.append(DateRoutine.dateTimeAsYYYYMMDDString(entryDate).substring(0, 4));
            date.append(DateRoutine.dateTimeAsYYYYMMDDString(entryDate).substring(5, 7));
            date.append(DateRoutine.dateTimeAsYYYYMMDDString(entryDate).substring(8, 10));
            return date.toString();
        }
    }

    private void processManualDisbursement(BilUnIdCash bilUnidentifiedCash, PayableOption payableOption,
            String currencyCode) {
        String processingLocationCode = findProcessingLocationCode(bilUnidentifiedCash.getBilBankCd());
        String memoText = findMemoText(payableOption.getReason());
        String folderId = generateTechnicalObjectKey(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getUserId());

        String technicalDisbursementId = getTechnicalDisbursementId(bilUnidentifiedCash.getBilCshEtrMthCd(),
                bilUnidentifiedCash.getBillUnidCashId(), bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt());

        String disbursementId = disbursementService.saveDisbursementActivity(folderId, payableOption.getRefundMethod(),
                DisbursementStatusCode.DISBURSED.toChar(), processingLocationCode, payableOption.getCheckNumber(),
                payableOption.getReason(), DisbursementMediumCode.MANUALCHECK.toString(), DisbursementIdType.BCMS,
                DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt()),
                payableOption.getPayeeId(), bilUnidentifiedCash.getBilRctAmt(), memoText, memoText, DSBTYPECODE,
                payableOption.getAddressSequenceNumber(), technicalDisbursementId, currencyCode);

        updateUnidentifiedCash(DisbursementStatusCode.DISBURSED.toChar(), payableOption.getRefundMethod(),
                payableOption.getReason(), payableOption.getCheckNumber(), disbursementId,
                bilUnidentifiedCash.getBilRctAmt(), bilUnidentifiedCash, payableOption.getPayeeId(),
                payableOption.getAddressSequenceNumber());

        saveFinancialActivity(folderId, bilUnidentifiedCash, execContext.getApplicationDateTime(),
                ObjectCode.UNIDENTIFIED_CASH.toString(), ActionCode.AUTHOR_DISBURSEMENT.toString(), CHAR_Y,
                ApplicationName.BCWSUNID.toString(), CHAR_A, DisbursementIdType.BCMS.getDisbursementIdType(),
                ApplicationProgramIdentifier.BACPUDS.toString(), disbursementId, payableOption.getReason(),
                currencyCode);

        saveFinancialActivity(folderId, bilUnidentifiedCash, execContext.getApplicationDateTime(),
                ObjectCode.UNIDENTIFIED_CASH.toString(), ActionCode.UPDATED.toString(), CHAR_Y,
                ApplicationName.BCWSUNID.toString(), CHAR_A, DisbursementIdType.BCMS.getDisbursementIdType(),
                ApplicationProgramIdentifier.BACPUDS.toString(), disbursementId, payableOption.getReason(),
                currencyCode);

        BilObjectCfg bilObjectCfg = bilObjectCfgRepository.findById(new BilObjectCfgId(WIPOPST, CHAR_I)).orElse(null);
        if (bilObjectCfg != null && bilObjectCfg.getBocObjPrcCd() == CHAR_Y) {
            processWipActivities(bilUnidentifiedCash.getBilAccountNbr(), bilUnidentifiedCash.getBilRctAmt(),
                    bilUnidentifiedCash.getPolNbr(), bilUnidentifiedCash.getBilAgtActNbr(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                    bilUnidentifiedCash.getBilUnIdCashId().getUserId());
        }
    }

    private void saveUnidentifiedCashHistory(BilUnIdCash bilUnidentifiedCash) {
        BilUnidCshHst unidentifiedCashHistory = new BilUnidCshHst();
        unidentifiedCashHistory
                .setBilUnIdCshHstId(new BilUnIdCshHstId(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(), bilUnidentifiedCash.getBilAcyTs()));
        unidentifiedCashHistory.setBilAccountNbr(bilUnidentifiedCash.getBilAccountNbr());
        unidentifiedCashHistory.setActivityUserId(bilUnidentifiedCash.getActivityUserId());
        unidentifiedCashHistory.setBilAdditionalId(bilUnidentifiedCash.getBilAdditionalId());
        unidentifiedCashHistory.setBilAdjDueDt(bilUnidentifiedCash.getBilAdjDueDt());
        unidentifiedCashHistory.setBilAgtActNbr(bilUnidentifiedCash.getBilAgtActNbr());
        unidentifiedCashHistory.setBilChkPrdMthCd(bilUnidentifiedCash.getBilChkPrdMthCd());
        unidentifiedCashHistory.setBilCwaId(bilUnidentifiedCash.getBilCwaId());
        unidentifiedCashHistory.setBilDepositDt(bilUnidentifiedCash.getBilDepositDt());
        unidentifiedCashHistory.setBilDsbId(BLANK_STRING);
        unidentifiedCashHistory.setBilDspDt(bilUnidentifiedCash.getBilDspDt());
        unidentifiedCashHistory.setBilDspReasonCd(bilUnidentifiedCash.getBilDspReasonCd());
        unidentifiedCashHistory.setBilDspTypeCd(bilUnidentifiedCash.getBilDspTypeCd());
        unidentifiedCashHistory.setBilEntrySeqNbr(bilUnidentifiedCash.getBilEntrySeqNbr());
        unidentifiedCashHistory.setBilManualSusInd(bilUnidentifiedCash.getBilManualSusInd());
        unidentifiedCashHistory.setBilPayeeAdrSeq(bilUnidentifiedCash.getBilPayeeAdrSeq());
        unidentifiedCashHistory.setBilPayeeCltId(bilUnidentifiedCash.getBilPayeeCltId());
        unidentifiedCashHistory.setBilPblItemCd(bilUnidentifiedCash.getBilPblItemCd());
        unidentifiedCashHistory.setBilRctAmt(bilUnidentifiedCash.getBilRctAmt());
        unidentifiedCashHistory.setBilRctCmt(bilUnidentifiedCash.getBilRctCmt());
        unidentifiedCashHistory.setBilRctId(bilUnidentifiedCash.getBilRctId());
        unidentifiedCashHistory.setBilRctTypeCd(bilUnidentifiedCash.getBilRctTypeCd());
        unidentifiedCashHistory.setBilTtyNbr(bilUnidentifiedCash.getBilTtyNbr());
        unidentifiedCashHistory.setDdsDsbDt(bilUnidentifiedCash.getDdsDsbDt());
        unidentifiedCashHistory.setDwsCkDrfNbr(bilUnidentifiedCash.getDwsCkDrfNbr());
        unidentifiedCashHistory.setDwsStatusCd(BLANK_CHAR);
        unidentifiedCashHistory.setPolNbr(bilUnidentifiedCash.getPolNbr());
        unidentifiedCashHistory.setPolSymbolCd(bilUnidentifiedCash.getPolSymbolCd());
        bilUnidCshHstRepository.save(unidentifiedCashHistory);

    }

    private void processWipActivities(String accountNumber, Double amount, String policyNumber,
            String agencyAccountNumber, ZonedDateTime distributionDate, Short sequenceNumber, String entryNumber,
            ZonedDateTime entryDate, String userId) {
        BilSystemRules bilSystemRules = bilSystemRulesRepository.findById(BLANK_STRING).orElse(null);
        String technicalObjectKey = generateTechnicalObjectKey(distributionDate, sequenceNumber, entryNumber, entryDate,
                userId);
        if (bilSystemRules != null && bilSystemRules.getBsrDsbWipAmt() < amount) {
            if (accountNumber != null && !accountNumber.trim().isEmpty()) {
                activityService.postActivity(WipAcitivityId.DISBURSE_AUTHENTICATION.getValue(),
                        WipObjectCode.UNIDENTIFIED_CASH.getValue(), accountNumber, technicalObjectKey);
            } else if (policyNumber != null && !policyNumber.trim().isEmpty()) {
                activityService.postActivity(WipAcitivityId.DISBURSE_AUTHENTICATION.getValue(),
                        WipObjectCode.UNIDENTIFIED_CASH.getValue(), policyNumber, technicalObjectKey);
            } else if (agencyAccountNumber != null && !agencyAccountNumber.trim().isEmpty()) {
                activityService.postActivity(WipAcitivityId.DISBURSE_AUTHENTICATION.getValue(),
                        WipObjectCode.UNIDENTIFIED_CASH.getValue(), agencyAccountNumber, technicalObjectKey);
            } else {
                activityService.postActivity(WipAcitivityId.DISBURSE_AUTHENTICATION.getValue(),
                        WipObjectCode.UNIDENTIFIED_CASH.getValue(),
                        DateRoutine.dateTimeAsYYYYMMDDString(distributionDate),
                        DateRoutine.dateTimeAsYYYYMMDDString(distributionDate));
            }
        }
    }

    private String generateTechnicalObjectKey(ZonedDateTime distributionDate, Short sequenceNumber, String entryNumber,
            ZonedDateTime entryDate, String userId) {
        StringBuilder technicalObjectKey = new StringBuilder(36);
        technicalObjectKey.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(distributionDate), 10));
        technicalObjectKey.append(StringUtils.leftPad(sequenceNumber.toString(), 4, CHAR_ZERO));
        technicalObjectKey.append(BLANK_CHAR);
        technicalObjectKey.append(StringUtils.rightPad(entryNumber, 4));
        technicalObjectKey.append(StringUtils.rightPad(userId, 8));
        technicalObjectKey.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        return technicalObjectKey.toString();
    }

    private void saveFinancialActivity(String folderId, BilUnIdCash bilUnidentifiedCash, ZonedDateTime applicationDate,
            String transactionObjectCode, String transactionActionCode, Character financialIndicator,
            String applicationName, Character functionCode, String productIdentifier, String programId,
            String disbursementId, String reasonCode, String currencyCode) {
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setActivityAmount(bilUnidentifiedCash.getBilRctAmt());
        financialApiActivity.setActivityNetAmount(bilUnidentifiedCash.getBilRctAmt());
        financialApiActivity.setFinancialIndicator(financialIndicator);
        financialApiActivity.setDisbursementId(disbursementId);
        financialApiActivity.setFunctionCode(functionCode);
        financialApiActivity.setFolderId(folderId);
        financialApiActivity.setApplicationName(applicationName);
        financialApiActivity.setUserId(execContext.getUserSeqeunceId());
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setApplicationProductIdentifier(productIdentifier);
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY);
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setOperatorId(execContext.getUserSeqeunceId());
        financialApiActivity.setAppProgramId(programId);
        financialApiActivity.setReferenceDate(
                DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilAccountId(bilUnidentifiedCash.getBilAccountNbr());
        financialApiActivity.setBilReceiptTypeCode(bilUnidentifiedCash.getBilRctTypeCd());
        financialApiActivity.setBilBankCode(bilUnidentifiedCash.getBilBankCd());
        financialApiActivity.setBilSourceCode(String.valueOf(bilUnidentifiedCash.getBilCshEtrMthCd()));
        financialApiActivity.setBilReasonCode(reasonCode);
        financialApiActivity
                .setBilDatabaseKey(generateDatabaseKey(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(), bilUnidentifiedCash.getBilEntrySeqNbr()));
        financialApiActivity.setSummaryEffectiveDate(DateRoutine.defaultDateTime());
        financialApiActivity.setPolicyId(bilUnidentifiedCash.getPolNbr());
        financialApiActivity.setBusinessCaseId(generateBusinessCaseId());
        financialApiActivity.setCurrencyCode(currencyCode);
        if (!bilUnidentifiedCash.getAgencyNbr().equals(BLANK_STRING)) {
            financialApiActivity.setAgentTtyId(bilUnidentifiedCash.getBilAgtActNbr());
        }
        if (!bilUnidentifiedCash.getBilTtyNbr().equals(BLANK_STRING)) {
            financialApiActivity.setAgentTtyId(bilUnidentifiedCash.getBilTtyNbr());
        }
        String[] fwsReturnMessage = null;
        fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

    private String generateBusinessCaseId() {
        String businessCaseId = IdGenerator.generateUniqueIdUpperCase(GENERATED_ID_LENGTH_10);
        StringBuilder sb = new StringBuilder(20);
        sb.append(businessCaseId);
        sb.append(CommonConstants.SEPARATOR_BLANK);
        sb.append(CommonConstants.SEPARATOR_BLANK);
        sb.append(FinancialIntegratorConstants.ApplicationName.BCMSDISB.toString());
        return sb.toString();
    }

    private String generateDatabaseKey(ZonedDateTime entryDate, String entryNumber, String userId,
            Short entrySequenceNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        sb.append(StringUtils.rightPad(entryNumber, 4));
        sb.append(StringUtils.rightPad(userId, 8));
        sb.append(PLUS_SIGN);
        sb.append(StringUtils.rightPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
        return sb.toString();
    }

    private void updateUnidentifiedCash(Character disbursementStatusCode, String refundMethod, String reason,
            Integer checkNumber, String disbursementId, Double amount, BilUnIdCash bilUnidentifiedCash, String clientId,
            Short addressSequenceNumber) {
        bilUnidentifiedCash.setBilDspTypeCd(BilDspTypeCode.DISBURSED.getValue());
        bilUnidentifiedCash.setBilDspDt(execContext.getApplicationDate());
        bilUnidentifiedCash.setDdsDsbDt(execContext.getApplicationDate());
        bilUnidentifiedCash.setActivityUserId(execContext.getUserSeqeunceId());
        bilUnidentifiedCash.setDwsCkDrfNbr(checkNumber);
        bilUnidentifiedCash.setBilChkPrdMthCd(refundMethod.charAt(0));
        bilUnidentifiedCash.setBilDspReasonCd(reason);
        bilUnidentifiedCash.setBilPayeeCltId(clientId);
        bilUnidentifiedCash.setBilPayeeAdrSeq(addressSequenceNumber);
        bilUnidentifiedCash.setBilDsbId(disbursementId);
        bilUnidentifiedCash.setDwsStatusCd(disbursementStatusCode);
        bilUnidentifiedCash.setBilRctAmt(amount);
        bilUnIdCashRepository.saveAndFlush(bilUnidentifiedCash);

    }

    private String findProcessingLocationCode(String bankCode) {
        logger.info("fetch processing location code based on bank code");
        if (bankCode != null) {
            return bankCode;
        } else {
            return BIL_BANK_CODE;
        }
    }

    public String findMemoText(String reason) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(reason,
                BilDesReasonType.DISBURSE_MANUAL.getValue(), execContext.getLanguage());
        if (busCodeTranslation == null) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(reason,
                    BusCodeTranslationType.DISBURSE_SUSPENSE_TYPE.getValue(), execContext.getLanguage());
            if (busCodeTranslation == null) {
                busCodeTranslation = busCdTranslationRepository.findByCodeAndType(reason,
                        BusCodeTranslationType.DISBURSE_AGENCY_SWEEP.getValue(), execContext.getLanguage());
                if (busCodeTranslation == null) {
                    throw new InvalidDataException("support.data.not.found",
                            new Object[] { reason, BilDesReasonType.DISBURSE_MANUAL.getValue() });
                }
            }
        }
        return busCodeTranslation.getBusDescription();
    }

    private ArrayList<String> getReferenceTypeCodeList() {
        ArrayList<String> referenceTypeCdList = new ArrayList<>();
        referenceTypeCdList.add("ANBR");
        referenceTypeCdList.add("AGYN");
        return referenceTypeCdList;
    }
}
