package billing.service.impl;

import static billing.utils.BillingConstants.ERROR_CODE;
import static billing.utils.BillingConstants.MASTER_COMPANY_NUMBER;
import static billing.utils.BillingConstants.PLUS_SIGN;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static disbursement.utils.DisbursementConstants.COMPANY_LOCATION_NUMBER;

import java.time.ZonedDateTime;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import application.security.service.impl.AppSecurityServiceImpl;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.BilUnidCshHst;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.entity.id.BilUnIdCshHstId;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.data.repository.BilUnidCshHstRepository;
import billing.model.WriteOffOption;
import billing.service.BillingAccountBalanceService;
import billing.service.WriteOffSuspendPaymentService;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilRules;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.utils.DateRoutine;
import disbursement.utils.DisbursementConstants.DisbursementIdType;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class WriteOffSuspendPaymentServiceImpl implements WriteOffSuspendPaymentService {

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private BilUnidCshHstRepository bilUnidCshHstRepository;

    @Autowired
    private BillingAccountBalanceService billingAccountBalanceService;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    private static final Logger logger = LogManager.getLogger(WriteOffSuspendPaymentServiceImpl.class);

    @Override
    @Transactional
    public void save(Short sequenceNumber, ZonedDateTime postedDate, String user, String batchId,
            ZonedDateTime distributionDate, WriteOffOption writeOffOption) {
        logger.debug("write off processing: start");

        String userId = appSecurityServiceImpl.authenticateUser(user).getUserSequenceId();
        String currencyCode = BLANK_STRING;
        logger.info("fetch row from bilUnidCash");
        BilUnIdCash bilUnidentifiedCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(distributionDate, sequenceNumber, batchId, userId, postedDate))
                .orElse(null);

        if (bilUnidentifiedCash == null) {
            logger.debug("Payment not found in BIL_UNID_CASH");
            throw new DataNotFoundException("payment.does.not.exists");
        }
        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId()))
                .orElse(null);
        if (bilCashEntryTot != null) {
            currencyCode = bilCashEntryTot.getCurrencyCd();
        }

        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(BilRules.UNIDENTIFIED_CASH_HISTORY.getValue(),
                BLANK_STRING, BLANK_STRING, BLANK_STRING, execContext.getApplicationDate(),
                execContext.getApplicationDate());
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            logger.info("if UCSH rule is ON insert record in unidentified cash history");
            saveUnidentifiedCashHistory(bilUnidentifiedCash);
        }

        updateBilUnidentifiedCash(bilUnidentifiedCash, writeOffOption.getReason());
        saveFinancialActivity(bilUnidentifiedCash, currencyCode);

        bilRulesUct = bilRulesUctRepository.getBilRulesRow(BilRules.ACCOUNT_BALANCE.getValue(), BLANK_STRING,
                BLANK_STRING, BLANK_STRING, execContext.getApplicationDate(), execContext.getApplicationDate());

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            logger.info("update total unidientified balance in the system.");
            billingAccountBalanceService.saveUnidentifiedBalance();
        }
        logger.debug("write off processing: end");
    }

    private void saveFinancialActivity(BilUnIdCash bilUnidentifiedCash, String currencyCode) {
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setTransactionObjectCode(ObjectCode.UNIDENTIFIED_CASH.toString());
        financialApiActivity.setTransactionActionCode(ActionCode.WRITE_OFF.toString());
        financialApiActivity.setActivityAmount(bilUnidentifiedCash.getBilRctAmt());
        financialApiActivity.setActivityNetAmount(bilUnidentifiedCash.getBilRctAmt());
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(generateFolderId(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getUserId()));
        financialApiActivity.setApplicationName(ApplicationName.BCWSUNID.toString());
        financialApiActivity.setUserId(execContext.getUserSeqeunceId());
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setApplicationProductIdentifier(DisbursementIdType.BCMS.getDisbursementIdType());
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setOperatorId(execContext.getUserSeqeunceId());
        financialApiActivity.setAppProgramId(ApplicationProgramIdentifier.BACPUCP.toString());
        financialApiActivity.setReferenceDate(
                DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setBilAccountId(bilUnidentifiedCash.getBilAccountNbr());
        financialApiActivity.setBilReceiptTypeCode(bilUnidentifiedCash.getBilRctTypeCd());
        financialApiActivity.setBilBankCode(bilUnidentifiedCash.getBilBankCd());
        financialApiActivity.setBilSourceCode(String.valueOf(bilUnidentifiedCash.getBilCshEtrMthCd()));
        financialApiActivity.setBilReasonCode(bilUnidentifiedCash.getBilDspReasonCd());
        financialApiActivity
                .setBilDatabaseKey(generateDatabaseKey(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(), bilUnidentifiedCash.getBilEntrySeqNbr()));
        financialApiActivity.setSummaryEffectiveDate(DateRoutine.defaultDateTime());
        financialApiActivity.setPolicyId(bilUnidentifiedCash.getPolNbr());
        financialApiActivity.setUserId(bilUnidentifiedCash.getActivityUserId());
        financialApiActivity.setCurrencyCode(currencyCode);
        if (!bilUnidentifiedCash.getBilAgtActNbr().trim().equals(BLANK_STRING)) {
            financialApiActivity.setAgentTtyId(bilUnidentifiedCash.getBilAgtActNbr());
        }
        if (!bilUnidentifiedCash.getBilTtyNbr().trim().equals(BLANK_STRING)) {
            financialApiActivity.setAgentTtyId(bilUnidentifiedCash.getBilTtyNbr());
        }
        String[] fwsReturnMessage = null;
        fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                execContext.getApplicationDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

    private void updateBilUnidentifiedCash(BilUnIdCash bilUnidentifiedCash, String reason) {
        bilUnidentifiedCash.setBilDspTypeCd(BilDspTypeCode.WRITE_OFF.getValue());
        bilUnidentifiedCash.setBilDspReasonCd(reason);
        bilUnidentifiedCash.setBilAcyTs(execContext.getApplicationDateTime());
        bilUnidentifiedCash.setActivityUserId(execContext.getUserSeqeunceId());
        bilUnidentifiedCash.setBilDspDt(execContext.getApplicationDate());
        bilUnIdCashRepository.saveAndFlush(bilUnidentifiedCash);
    }

    private void saveUnidentifiedCashHistory(BilUnIdCash bilUnidentifiedCash) {
        BilUnidCshHst unidentifiedCashHistory = new BilUnidCshHst();

        unidentifiedCashHistory
                .setBilUnIdCshHstId(new BilUnIdCshHstId(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(), execContext.getApplicationDateTime()));
        unidentifiedCashHistory.setBilAccountNbr(bilUnidentifiedCash.getBilAccountNbr());
        unidentifiedCashHistory.setActivityUserId(bilUnidentifiedCash.getActivityUserId());
        unidentifiedCashHistory.setBilAdditionalId(bilUnidentifiedCash.getBilAdditionalId());
        unidentifiedCashHistory.setBilAdjDueDt(bilUnidentifiedCash.getBilAdjDueDt());
        unidentifiedCashHistory.setBilAgtActNbr(bilUnidentifiedCash.getBilAgtActNbr());
        unidentifiedCashHistory.setBilChkPrdMthCd(bilUnidentifiedCash.getBilChkPrdMthCd());
        unidentifiedCashHistory.setBilCwaId(bilUnidentifiedCash.getBilCwaId());
        unidentifiedCashHistory.setBilDepositDt(bilUnidentifiedCash.getBilDepositDt());
        unidentifiedCashHistory.setBilDsbId(BLANK_STRING);
        unidentifiedCashHistory.setBilDspDt(bilUnidentifiedCash.getBilDspDt());
        unidentifiedCashHistory.setBilDspReasonCd(bilUnidentifiedCash.getBilDspReasonCd());
        unidentifiedCashHistory.setBilDspTypeCd(bilUnidentifiedCash.getBilDspTypeCd());
        unidentifiedCashHistory.setBilEntrySeqNbr(bilUnidentifiedCash.getBilEntrySeqNbr());
        unidentifiedCashHistory.setBilManualSusInd(bilUnidentifiedCash.getBilManualSusInd());
        unidentifiedCashHistory.setBilPayeeAdrSeq(bilUnidentifiedCash.getBilPayeeAdrSeq());
        unidentifiedCashHistory.setBilPayeeCltId(bilUnidentifiedCash.getBilPayeeCltId());
        unidentifiedCashHistory.setBilPblItemCd(bilUnidentifiedCash.getBilPblItemCd());
        unidentifiedCashHistory.setBilRctAmt(bilUnidentifiedCash.getBilRctAmt());
        unidentifiedCashHistory.setBilRctCmt(bilUnidentifiedCash.getBilRctCmt());
        unidentifiedCashHistory.setBilRctId(bilUnidentifiedCash.getBilRctId());
        unidentifiedCashHistory.setBilRctTypeCd(bilUnidentifiedCash.getBilRctTypeCd());
        unidentifiedCashHistory.setBilTtyNbr(bilUnidentifiedCash.getBilTtyNbr());
        unidentifiedCashHistory.setDdsDsbDt(bilUnidentifiedCash.getDdsDsbDt());
        unidentifiedCashHistory.setDwsCkDrfNbr(bilUnidentifiedCash.getDwsCkDrfNbr());
        unidentifiedCashHistory.setDwsStatusCd(BLANK_CHAR);
        unidentifiedCashHistory.setPolNbr(bilUnidentifiedCash.getPolNbr());
        unidentifiedCashHistory.setPolSymbolCd(bilUnidentifiedCash.getPolSymbolCd());
        bilUnidCshHstRepository.save(unidentifiedCashHistory);
    }

    private String generateFolderId(ZonedDateTime distributionDate, Short sequenceNumber, String entryNumber,
            ZonedDateTime entryDate, String userId) {
        StringBuilder technicalObjectKey = new StringBuilder(36);
        technicalObjectKey.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(distributionDate), 10));
        technicalObjectKey.append(StringUtils.leftPad(sequenceNumber.toString(), 4, CHAR_ZERO));
        technicalObjectKey.append(BLANK_CHAR);
        technicalObjectKey.append(StringUtils.rightPad(entryNumber, 4));
        technicalObjectKey.append(StringUtils.rightPad(userId, 8));
        technicalObjectKey.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        return technicalObjectKey.toString();
    }

    private String generateDatabaseKey(ZonedDateTime entryDate, String entryNumber, String userId,
            Short entrySequenceNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        sb.append(StringUtils.rightPad(entryNumber, 4));
        sb.append(StringUtils.rightPad(userId, 8));
        sb.append(PLUS_SIGN);
        sb.append(StringUtils.leftPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
        return sb.toString();
    }
}
