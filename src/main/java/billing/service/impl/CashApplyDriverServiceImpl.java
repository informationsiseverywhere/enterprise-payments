package billing.service.impl;

import static billing.utils.BillingConstants.ERROR_CODE;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_C;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_F;
import static core.utils.CommonConstants.CHAR_H;
import static core.utils.CommonConstants.CHAR_I;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActInquiry;
import billing.data.entity.BilActRules;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilBillPlan;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.BilInvPol;
import billing.data.entity.BilPolActivity;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.id.BilActInquiryId;
import billing.data.entity.id.BilActRulesId;
import billing.data.entity.id.BilCashDspId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.entity.id.BilPolActivityId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.entity.id.BilSupportPlanId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActRulesRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilBillPlanRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilDatesRepository;
import billing.data.repository.BilEftBankRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilInvPolRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilSupportPlanRepository;
import billing.data.repository.BilTtyActRltRepository;
import billing.handler.impl.BillingApiService;
import billing.handler.impl.DownPaymentCashApplyServiceImpl;
import billing.handler.impl.ReverseWriteOffToPaymentServiceImpl;
import billing.handler.impl.WriteOffDueToOverPaymentServiceImpl;
import billing.handler.impl.WriteOffDueToUnderPaymentServiceImpl;
import billing.handler.impl.WriteOffFeeChargesServiceImpl;
import billing.handler.model.AccountAdjustDates;
import billing.handler.model.AccountProcessingAndRecovery;
import billing.handler.model.CollectionVendorTapeProcessing;
import billing.handler.model.ReverseWriteOffPayment;
import billing.handler.model.WriteOffFeeCharges;
import billing.handler.model.WriteOffPayment;
import billing.handler.service.AccountProcessingAndRecoveryService;
import billing.handler.service.ChargesAccountAdjustDatesService;
import billing.model.CashApply;
import billing.model.CashApplyDriver;
import billing.service.BilRulesUctService;
import billing.service.CashApplyDriverService;
import billing.service.CashApplyService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.AccountStatus;
import billing.utils.BillingConstants.AccountTypeCode;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BilReceiptTypeCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CashOverPayIndicators;
import billing.utils.BillingConstants.ChargeTypeCodes;
import billing.utils.BillingConstants.CollectionLayout;
import billing.utils.BillingConstants.CreditIndicator;
import billing.utils.BillingConstants.DedudctionDate;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.FullPayServiceChargeWriteOff;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.ManualSuspendIndicator;
import billing.utils.BillingConstants.PolicyIssueIndicators;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.data.entity.id.BusCdTranslationId;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class CashApplyDriverServiceImpl implements CashApplyDriverService {

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilTtyActRltRepository bilTtyActRltRepository;

    @Autowired
    private BilActRulesRepository bilActRulesRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilSupportPlanRepository bilSupportPlanRepository;

    @Autowired
    private BilDatesRepository bilDatesRepository;

    @Autowired
    private DateService dateService;

    @Autowired
    private BilInvPolRepository bilInvPolRepository;

    @Autowired
    private BilBillPlanRepository bilBillPlanRepository;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private CashApplyService cashApplyService;

    @Autowired
    private DownPaymentCashApplyServiceImpl downPaymentCashApplyServiceImpl;

    @Autowired
    private WriteOffDueToUnderPaymentServiceImpl writeOffDueToUnderPaymentServiceImpl;

    @Autowired
    private WriteOffDueToOverPaymentServiceImpl writeOffDueToOverPaymentServiceImpl;

    @Autowired
    private WriteOffFeeChargesServiceImpl writeOffFeeChargesServiceImpl;

    @Autowired
    private ReverseWriteOffToPaymentServiceImpl reverseWriteOffToPaymentServiceImpl;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private BillingApiService billingApiService;

    @Autowired
    private AccountProcessingAndRecoveryService accountProcessingAndRecoveryService;

    @Autowired
    private ChargesAccountAdjustDatesService chargesAccountAdjustDatesService;
    
    @Value("${language}")
    private String language;

    private static final String DIRECT = "BTY";
    private static final String THIRD_PARTY = "BTP";
    private static final String NO_DATA_FOUND = "no.data.found";
    private static final String BCMOCA = "BCMOCA";
    private static final String CORRECTED_INVOICE = "correctedInvoice";
    private static final String CIV_PREMIUM_PAID_AMOUNT = "civPremiumPaidAmount";
    private static final String CIV_CHARGE_PAID_AMOUNT = "civChargePaidAmount";
    private static final String CIV_AMOUNT_EXIST = "civAmountsExist";
    private static final String CIV_PREMIUM_BALANCE_AMOUNT = "civPremiumBalanceAmount";
    private static final String CIV_CHARGE_BALANCE_AMOUNT = "civChargeBalanceAmount";
    private static final String UNABLE_TO_UPDATE_BIL_POL_TRM = "unable.to.update.bil.pol.trm";

    @Override
    @Transactional
    public void processCashApply(CashApplyDriver cashApplyDriver) {
        BilActRules bilActRules = null;
        boolean eftCorrectedInvoice = false;
        String accountId = cashApplyDriver.getAccountId();
        BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
        if (bilAccount == null) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }
        String accountType = getAccountTypeCode(bilAccount.getBillTypeCd());
        boolean eftCollectionMethod = getBilCollectionMethod(accountId, bilAccount.getCollectionMethod(), cashApplyDriver.getCashApplyDate());

        List<BilCashDsp> downPaymentList = evaluateDownPaymentCash(accountType, accountId, bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), true, cashApplyDriver.getCashApplyDate());

        boolean vendorPayment = false;
        List<String> thirdPartyIdList = null;
        Double totalCashAmount = sumCashAmount(accountId);

        if (totalCashAmount > 0 || downPaymentList != null && !downPaymentList.isEmpty()) {
            if (accountType.equals(DIRECT)) {
                bilActRules = fetchBilActRules(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd());
                if (bilActRules == null) {
                    bilActRules = defaultBilActRules();
                }
                if (eftCollectionMethod) {
                    eftCorrectedInvoice = readAccountData(cashApplyDriver, bilAccount, eftCollectionMethod,
                            bilActRules);
                }

                if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())) {
                    checkRevAutWriteOffDuePayment(cashApplyDriver, bilAccount, totalCashAmount, BLANK_STRING);
                }
            } else {
                thirdPartyIdList = readThirdPartyTable(accountType, bilAccount.getAccountId());
                bilActRules = defaultBilActRules();
            }

            vendorPayment = processCashFlows(cashApplyDriver, bilAccount, totalCashAmount, downPaymentList,
                    eftCorrectedInvoice, thirdPartyIdList, bilActRules, accountType);
        }

        processCollectionTriger(vendorPayment, bilAccount, cashApplyDriver, accountType);

        if (bilActRules != null && bilActRules.getBruRnlQteInd() == CHAR_Y) {
            renewalInforceCashEvent();
        }
    }

    private List<String> readThirdPartyTable(String accountType, String accountId) {
        List<String> thirdPartyIdList = null;
        if (accountType.equals(THIRD_PARTY)) {
            thirdPartyIdList = bilTtyActRltRepository.getBilThirdPartyIdByBilAccountId(accountId);
            if (thirdPartyIdList == null || thirdPartyIdList.isEmpty()) {
                throw new DataNotFoundException("select.bil.tty.act.rlt.not.found");
            }
        }

        return thirdPartyIdList;
    }

    private void processCollectionTriger(boolean vendorPayment, BilAccount bilAccount, CashApplyDriver cashApplyDriver,
            String accountType) {
        double collectionMinAmount = DECIMAL_ZERO;
        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("COLL", bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), BLANK_STRING);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            if (bilRulesUct.getBrtParmListTxt() != null && !bilRulesUct.getBrtParmListTxt().isEmpty()
                    && bilRulesUct.getBrtParmListTxt().length() >= 14) {
                collectionMinAmount = Double.parseDouble(bilRulesUct.getBrtParmListTxt().substring(0, 14));
            }
            processCollection(cashApplyDriver, vendorPayment, bilRulesUct, bilAccount);
            processPreCollection(cashApplyDriver, collectionMinAmount, bilAccount, bilRulesUct, accountType);
        }
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_A) {
            if (bilRulesUct.getBrtParmListTxt() != null && !bilRulesUct.getBrtParmListTxt().isEmpty()
                    && bilRulesUct.getBrtParmListTxt().length() >= 14) {
                collectionMinAmount = Double.parseDouble(bilRulesUct.getBrtParmListTxt().substring(0, 14));
            }
            processAccountPreCollection(cashApplyDriver, vendorPayment, bilRulesUct, bilAccount,collectionMinAmount);
            processAccountCollection(cashApplyDriver, collectionMinAmount, bilAccount, bilRulesUct, accountType,vendorPayment);
        }
    }
    private void processAccountPreCollection(CashApplyDriver cashApplyDriver, boolean vendorPayment, BilRulesUct collectionRules,
            BilAccount bilAccount,Double collectionMinAmount) {
        String previousPolicyId = BLANK_STRING;
        boolean policyChange = false;
        Double[] deterCollectionBalance = null;
        char accountCollPreColl = BLANK_CHAR;
        Double outstandingBalance = DECIMAL_ZERO;
        Double outstandingCollAmt = DECIMAL_ZERO;
        
        Integer existence =bilActInquiryRepository.existenceCheck(cashApplyDriver.getAccountId(),BusCodeTranslationType.COLLECTION_LETTER.getValue(), BusCodeTranslationType.LAST_COLLECTION_LETTER.getValue(),BusCodeTranslationType.ACCOUNTPLAN.getValue());
        
        if(existence>0) {
        	accountCollPreColl = AccountStatus.PRE_COLLECTION.toChar();
        }
        else {
        	accountCollPreColl = AccountStatus.COLLECTIONS.toChar();
        }
        
        List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.fetchCollectionProcess(
                cashApplyDriver.getAccountId(), BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue(), DECIMAL_ZERO);
        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
            if (!previousPolicyId.equals(bilPolicyTerm.getBillPolicyTermId().getPolicyId())) {
                previousPolicyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
                policyChange = true;
            }

            if (policyChange) {
                BilPolicy bilPolicy = bilPolicyRepository
                        .findById(new BilPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                cashApplyDriver.getAccountId()))
                        .orElse(null);
                if (bilPolicy == null) {
                    throw new DataNotFoundException(NO_DATA_FOUND);
                }
                boolean includeCharge = bilPolicyTerm.getBptActColCd() == CHAR_Y;
                deterCollectionBalance = deterCollectionBalance(cashApplyDriver.getAccountId(), bilPolicyTerm,
                        bilPolicy, includeCharge);
            }

            double collectionBalance = (deterCollectionBalance == null || deterCollectionBalance.length == 0)
                    ? DECIMAL_ZERO
                    : deterCollectionBalance[0];
            outstandingBalance = collectionBalance + outstandingBalance;
            outstandingCollAmt = bilPolicyTerm.getBptPolColAmt()+outstandingBalance;
            
            if(collectionBalance<=DECIMAL_ZERO) {
            	if(bilPolicyTerm.getBptPolUwStaCd() ==BilPolicyStatusCode.FLAT_CANCELLATION.getValue() || bilPolicyTerm.getBptPolUwStaCd() ==BilPolicyStatusCode.CANCELLED.getValue()) {
            		Integer count = bilPolicyTermRepository.updatePolicyStatus(bilPolicyTerm.getBillPolicyTermId().getBilAccountId(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
            				bilPolicyTerm.getBptStatusEffDt(), bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                            DECIMAL_ZERO, SEPARATOR_BLANK,
                            BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue(),
                            bilPolicyTerm.getBptPolUwStaCd(), getPolicyStatusList());
           		if (count == null) {
                        throw new DataNotFoundException(UNABLE_TO_UPDATE_BIL_POL_TRM, new Object[] { bilPolicyTerm.getBillPolicyTermId().getBilAccountId().trim(),
                        		bilPolicyTerm.getBillPolicyTermId().getPolicyId().trim(), DateRoutine.dateTimeAsMMDDYYYYAsString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()) });
                  }
            	}
            	else if (bilPolicyTerm.getPlnExpDt().compareTo(cashApplyDriver.getCashApplyDate())<=DECIMAL_ZERO) {
            		Integer count = bilPolicyTermRepository.updatePolicyStatus1(bilPolicyTerm.getBillPolicyTermId().getBilAccountId(), 
            				bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
            				cashApplyDriver.getCashApplyDate(),
            				bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), DECIMAL_ZERO, SEPARATOR_BLANK,
                            BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue(),BilPolicyStatusCode.CLOSED.getValue());

                    if (count == null) {
                           throw new DataNotFoundException(UNABLE_TO_UPDATE_BIL_POL_TRM, new Object[] { bilPolicyTerm.getBillPolicyTermId().getBilAccountId().trim(),
                        		bilPolicyTerm.getBillPolicyTermId().getPolicyId(), DateRoutine.dateTimeAsMMDDYYYYAsString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()) });
                    }
            	}
            	 double balanceCharge = collectionBalance - bilPolicyTerm.getBptPolColAmt();
                 writeFms("BCG", BusCodeTranslationType.COLLECT_AGENCY_PAYMENT.getValue(), "BAL", balanceCharge, cashApplyDriver, bilPolicyTerm, bilAccount);
            	
            }
           if(collectionBalance< bilPolicyTerm.getBptPolColAmt()) {
        	   double balanceCharge = collectionBalance - bilPolicyTerm.getBptPolColAmt();
        	   String transactionObjCd;
        	   if(accountCollPreColl == AccountStatus.COLLECTIONS.toChar()) {
        		   transactionObjCd = BusCodeTranslationType.COLLECT_AGENCY_PAYMENT.getValue();
				} else {
					 transactionObjCd ="PCL";
				}
               writeFms("BCG", transactionObjCd, "BAL", balanceCharge, cashApplyDriver, bilPolicyTerm, bilAccount);
               updateCollectionAmount(bilPolicyTerm.getBillPolicyTermId().getBilAccountId(), 
            				bilPolicyTerm.getBillPolicyTermId().getPolicyId(),collectionBalance);
          	
           }
        }
        if(accountCollPreColl == AccountStatus.COLLECTIONS.toChar()) {
			if(outstandingBalance.compareTo(collectionMinAmount)<0 || outstandingBalance <= 0 || 
					outstandingCollAmt == 0) {
				 CollectionVendorTapeProcessing collectionVendorTapeProcessing = buildAcoountCollectionBillingApiParam(
			                cashApplyDriver.getAccountId(), bilAccount, "ACF", cashApplyDriver.getCashApplyDate());
			        billingApiService.getCollectionVendor(collectionVendorTapeProcessing);
			}

		}
    } 
	private List<Character> getPolicyStatusList() {
        List<Character> policyStatusList = new ArrayList<>();
        policyStatusList.add(BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue());
        policyStatusList.add(BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue());
        return policyStatusList;
    }
    private void processAccountCollection(CashApplyDriver cashApplyDriver, double collectionMinAmount,
            BilAccount bilAccount, BilRulesUct collectionRules, String accountType, boolean vendorPayment) {
        Double outstandingBalance = DECIMAL_ZERO;
        Double outstandingCollAmt = DECIMAL_ZERO;
        
        boolean policyChange = false;
        String previousPolicyId = BLANK_STRING;
        Double[] deterCollections = null;
        BilPolicy bilPolicy = null;

        List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.fetchCollectionProcess(
                cashApplyDriver.getAccountId(), BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue(), DECIMAL_ZERO);
        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
            if (!previousPolicyId.equals(bilPolicyTerm.getBillPolicyTermId().getPolicyId())) {
                previousPolicyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
                policyChange = true;
            }
            if (policyChange) {
                bilPolicy = bilPolicyRepository
                        .findById(new BilPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                bilPolicyTerm.getBillPolicyTermId().getBilAccountId()))
                        .orElse(null);
                if (bilPolicy == null) {
                    throw new DataNotFoundException(NO_DATA_FOUND);
                }
                boolean includeCharge = bilPolicyTerm.getBptActColCd() == CHAR_Y;
                deterCollections = deterCollectionBalance(cashApplyDriver.getAccountId(), bilPolicyTerm, bilPolicy,
                        includeCharge);
            }

            double collectionBalance = deterCollections[0] == null ? DECIMAL_ZERO : deterCollections[0];
            outstandingBalance = collectionBalance + outstandingBalance;
            outstandingCollAmt = bilPolicyTerm.getBptPolColAmt()+outstandingBalance;
            if(collectionBalance< bilPolicyTerm.getBptPolColAmt()) {
         	   double balanceChange = collectionBalance - bilPolicyTerm.getBptPolColAmt();
         	   
                writeFms("BCG", "COL", "BAL", balanceChange, cashApplyDriver, bilPolicyTerm, bilAccount);
                updateCollectionAmount(bilPolicyTerm.getBillPolicyTermId().getBilAccountId(), 
             				bilPolicyTerm.getBillPolicyTermId().getPolicyId(),collectionBalance);
            }
        }
        if(bilAccount.getStatus() ==AccountStatus.COLLECTIONS.toChar()) {
        	Integer bclExistance = bilActInquiryRepository.checkExistenceByTypeCd(bilAccount.getAccountId(), "BCL");
        	if((outstandingBalance<outstandingCollAmt) && (!vendorPayment && bclExistance<=0)) {
        		BilActInquiry  bilActInquiry = new BilActInquiry();
        		BilActInquiryId bilActInquiryId = new BilActInquiryId(bilAccount.getAccountId(), "CCL");
                bilActInquiry.setBilActInquiryId(bilActInquiryId);
                bilActInquiry.setBilNxtAcyDt(cashApplyDriver.getCashApplyDate());
                bilActInquiryRepository.saveAndFlush(bilActInquiry);
        	}
        }
    }

    private String getAccountTypeCode(String billTypeCd) {
        String accountType;
        List<BusCdTranslation> busCdTranslations = busCdTranslationRepository.findAllByCodeAndTypeList(billTypeCd,
                Arrays.asList(DIRECT, THIRD_PARTY), language);
        if (busCdTranslations != null && !busCdTranslations.isEmpty()) {
            accountType = busCdTranslations.get(0).getBusCdTranslationId().getBusType().trim();
        } else {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }
        return accountType;
    }

    private void renewalInforceCashEvent() {
        // Call to RenewalInforceCashEvent module

    }

    private void processPreCollection(CashApplyDriver cashApplyDriver, double collectionMinAmount,
            BilAccount bilAccount, BilRulesUct collectionRules, String accountType) {
        boolean writeOffCollection = false;
        if (collectionRules.getBrtParmListTxt() != null && !collectionRules.getBrtParmListTxt().isEmpty()
                && collectionRules.getBrtParmListTxt().length() >= 18) {
            writeOffCollection = collectionRules.getBrtParmListTxt().charAt(17) == CHAR_Y;
        }
        boolean preCollection = false;
        boolean policyChange = false;
        String previousPolicyId = BLANK_STRING;
        Double[] deterCollections = null;
        BilPolicy bilPolicy = null;

        List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.fetchCollectionProcess(
                cashApplyDriver.getAccountId(), BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue(), DECIMAL_ZERO);
        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
            if (!previousPolicyId.equals(bilPolicyTerm.getBillPolicyTermId().getPolicyId())) {
                previousPolicyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
                policyChange = true;
            }
            if (policyChange) {
                Integer count = bilPolActivityRepository.checkPolicyPreCollection(
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(), cashApplyDriver.getAccountId(),
                        DateRoutine.defaultDateTime(), "BCL", "CL ", "CL9");

                if (count != null && count > SHORT_ZERO) {
                    preCollection = true;
                }
                bilPolicy = bilPolicyRepository
                        .findById(new BilPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                bilPolicyTerm.getBillPolicyTermId().getBilAccountId()))
                        .orElse(null);
                if (bilPolicy == null) {
                    throw new DataNotFoundException(NO_DATA_FOUND);
                }
                boolean includeCharge = bilPolicyTerm.getBptActColCd() == CHAR_Y;
                deterCollections = deterCollectionBalance(cashApplyDriver.getAccountId(), bilPolicyTerm, bilPolicy,
                        includeCharge);
            }

            double collectionBalance = deterCollections[0] == null ? DECIMAL_ZERO : deterCollections[0];
            if (!preCollection) {
                double policyBalance = deterCollections[1];
                double serviceChargeBalance = deterCollections[2];
                if (collectionBalance < collectionMinAmount
                        || collectionBalance <= DECIMAL_ZERO && collectionMinAmount == 0) {
                    processBillingApi(cashApplyDriver, bilAccount, bilPolicyTerm, bilPolicy.getPolSymbolCd(),
                            bilPolicy.getPolNbr(), accountType, writeOffCollection, collectionMinAmount);
                } else if (collectionBalance != bilPolicyTerm.getBptPolColAmt()) {
                    double balanceChange = collectionBalance - bilPolicyTerm.getBptPolColAmt();
                    writeFms("BCG", "PCL", "BAL", balanceChange, cashApplyDriver, bilPolicyTerm, bilAccount);
                    if (policyChange) {
                        if ((policyBalance > 0 || serviceChargeBalance > 0) && writeOffCollection) {
                            updateCWOTriger(cashApplyDriver.getAccountId(),
                                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), cashApplyDriver.getCashApplyDate());
                        }
                        updateCollectionAmount(cashApplyDriver.getAccountId(),
                                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), collectionBalance);
                    }

                }
            } else {
                double balanceChange = collectionBalance - bilPolicyTerm.getBptPolColAmt();
                writeFms("BCG", "PCL", "BAL", balanceChange, cashApplyDriver, bilPolicyTerm, bilAccount);
                if (policyChange) {
                    updateCollectionAmount(cashApplyDriver.getAccountId(),
                            bilPolicyTerm.getBillPolicyTermId().getPolicyId(), collectionBalance);
                }

            }
        }
    }

    private void processBillingApi(CashApplyDriver cashApplyDriver, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm,
            String symbolCd, String polNumber, String accountType, boolean writeOffCollection,
            double collectionMinAmount) {
        CollectionVendorTapeProcessing collectionVendorTapeProcessing = buildBillingApiParam(
                cashApplyDriver.getAccountId(), bilAccount, bilPolicyTerm, symbolCd, polNumber, cashApplyDriver.getCashApplyDate());
        billingApiService.getCollectionVendor(collectionVendorTapeProcessing);
        if (accountType.equals(DIRECT)
                && !bilAccount.getBillTypeCd().equals(AccountTypeCode.SINGLE_POLICY.getValue())) {
            checkIfAb(bilAccount, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), writeOffCollection,
                    collectionMinAmount, cashApplyDriver);
        }
    }
    private CollectionVendorTapeProcessing buildAcoountCollectionBillingApiParam(String accountId, BilAccount bilAccount,
            String symbolCd, ZonedDateTime currentDate) {

        CollectionVendorTapeProcessing collectionVendorTapeProcessing = new CollectionVendorTapeProcessing();
        collectionVendorTapeProcessing.setApiType(symbolCd);
        collectionVendorTapeProcessing.setUserId(BillingConstants.SYSTEM_USER_ID);
        collectionVendorTapeProcessing.setBilAccountId(accountId);
        collectionVendorTapeProcessing.setActivityDt(currentDate);
        collectionVendorTapeProcessing.setCollectionType(CollectionLayout.COLLECTION_FULL.getValue());
        collectionVendorTapeProcessing.setClassChgSw(CHAR_N);
        collectionVendorTapeProcessing.setBilTypeCd(bilAccount.getBillTypeCd());
        collectionVendorTapeProcessing.setBilClassCd(bilAccount.getBillClassCd());
        collectionVendorTapeProcessing.setBilAccountNbr(bilAccount.getAccountNumber());
        collectionVendorTapeProcessing.setCollectionQuote(CHAR_N);
        collectionVendorTapeProcessing.setPolSymbolCd(symbolCd);

        return collectionVendorTapeProcessing;
    }

    private CollectionVendorTapeProcessing buildBillingApiParam(String accountId, BilAccount bilAccount,
            BilPolicyTerm bilPolicyTerm, String symbolCd, String polNumber, ZonedDateTime currentDate) {

        CollectionVendorTapeProcessing collectionVendorTapeProcessing = new CollectionVendorTapeProcessing();
        collectionVendorTapeProcessing.setApiType("COF");
        collectionVendorTapeProcessing.setUserId(BillingConstants.SYSTEM_USER_ID);
        collectionVendorTapeProcessing.setBilAccountId(accountId);
        collectionVendorTapeProcessing.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
        collectionVendorTapeProcessing.setPolEffDate(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
        collectionVendorTapeProcessing.setPolExpDate(bilPolicyTerm.getPlnExpDt());
        collectionVendorTapeProcessing.setActivityDt(currentDate);
        collectionVendorTapeProcessing.setCollectionType(CollectionLayout.COLLECTION_FULL.getValue());
        collectionVendorTapeProcessing.setClassChgSw(CHAR_N);
        collectionVendorTapeProcessing.setBilTypeCd(bilAccount.getBillTypeCd());
        collectionVendorTapeProcessing.setBilClassCd(bilAccount.getBillClassCd());
        collectionVendorTapeProcessing.setBilAccountNbr(bilAccount.getAccountNumber());
        collectionVendorTapeProcessing.setCollectionQuote(CHAR_N);
        collectionVendorTapeProcessing.setPolSymbolCd(symbolCd);
        collectionVendorTapeProcessing.setPolNbr(polNumber);

        return collectionVendorTapeProcessing;
    }

    private void processCollection(CashApplyDriver cashApplyDriver, boolean vendorPayment, BilRulesUct collectionRules,
            BilAccount bilAccount) {
        String previousPolicyId = BLANK_STRING;
        boolean policyChange = false;
        Double[] deterCollectionBalance = null;
        List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.fetchCollectionProcess(
                cashApplyDriver.getAccountId(), BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue(), DECIMAL_ZERO);
        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
            if (!previousPolicyId.equals(bilPolicyTerm.getBillPolicyTermId().getPolicyId())) {
                previousPolicyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
                policyChange = true;
            }

            if (policyChange) {
                BilPolicy bilPolicy = bilPolicyRepository
                        .findById(new BilPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                cashApplyDriver.getAccountId()))
                        .orElse(null);
                if (bilPolicy == null) {
                    throw new DataNotFoundException(NO_DATA_FOUND);
                }
                boolean includeCharge = bilPolicyTerm.getBptActColCd() == CHAR_Y;
                deterCollectionBalance = deterCollectionBalance(cashApplyDriver.getAccountId(), bilPolicyTerm,
                        bilPolicy, includeCharge);
            }

            double collectionBalance = (deterCollectionBalance == null || deterCollectionBalance.length == 0)
                    ? DECIMAL_ZERO
                    : deterCollectionBalance[0];
            if (collectionBalance < bilPolicyTerm.getBptPolColAmt()) {
                evaluateCollection(cashApplyDriver, bilPolicyTerm, vendorPayment, collectionBalance,
                        deterCollectionBalance, collectionRules, bilAccount, policyChange);
            }
        }
    }

    private void evaluateCollection(CashApplyDriver cashApplyDriver, BilPolicyTerm bilPolicyTerm, boolean vendorPayment,
            double collectionBalance, Double[] deterCollectionBalance, BilRulesUct collectionRules,
            BilAccount bilAccount, boolean policyChange) {
        boolean collWoCollAmt = false;
        if (collectionRules.getBrtParmListTxt() != null && !collectionRules.getBrtParmListTxt().isEmpty()
                && collectionRules.getBrtParmListTxt().length() >= 18) {
            collWoCollAmt = collectionRules.getBrtParmListTxt().charAt(17) == CHAR_Y;
        }
        double policyBalance = deterCollectionBalance[1];
        double serviceChargeBalance = deterCollectionBalance[2];
        String policyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
        ZonedDateTime polEffDate = DateRoutine.defaultDateTime();
        if (policyChange) {
            if (!bilPolActivityRepository.existsById(new BilPolActivityId(cashApplyDriver.getAccountId(), policyId,
                    DateRoutine.defaultDateTime(), "BCL"))) {
                if (!vendorPayment) {
                    BilPolActivity bilPolActivity = new BilPolActivity();
                    BilPolActivityId bilPolActivityId = new BilPolActivityId(cashApplyDriver.getAccountId(), policyId,
                            polEffDate, "CCL");
                    bilPolActivity.setBillPolActivityId(bilPolActivityId);
                    bilPolActivity.setBilNxtAcyDt(cashApplyDriver.getCashApplyDate());
                    bilPolActivityRepository.save(bilPolActivity);
                }

                if ((policyBalance > DECIMAL_ZERO || serviceChargeBalance > DECIMAL_ZERO) && collWoCollAmt) {
                    updateCWOTriger(cashApplyDriver.getAccountId(), policyId, cashApplyDriver.getCashApplyDate());
                }
            }

            updateCollectionAmount(cashApplyDriver.getAccountId(), policyId, collectionBalance);
        }
        double balanceCharge = collectionBalance - bilPolicyTerm.getBptPolColAmt();
        writeFms("BCG", "COL", "BAL", balanceCharge, cashApplyDriver, bilPolicyTerm, bilAccount);
    }

    private void updateCWOTriger(String accountId, String policyId, ZonedDateTime currentDate) {
        ZonedDateTime polEffDate = DateRoutine.defaultDateTime();
        BilPolActivityId bilPolActivityId = new BilPolActivityId(accountId, policyId, polEffDate, "CWO");
        BilPolActivity bilPolActivity = bilPolActivityRepository.findById(bilPolActivityId).orElse(null);
        if (bilPolActivity == null) {
            bilPolActivity = new BilPolActivity();
            bilPolActivity.setBillPolActivityId(bilPolActivityId);
        }
        bilPolActivity.setBilNxtAcyDt(currentDate);
        bilPolActivityRepository.save(bilPolActivity);
    }

    private void writeFms(String reason, String objectCd, String actionCd, double balanceChange,
            CashApplyDriver cashApplyDriver, BilPolicyTerm bilPolicyTerm, BilAccount bilAccount) {
        String se3Date = DateRoutine.dateTimeAsYYYYMMDDString(cashApplyDriver.getCashApplyDate());
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(cashApplyDriver.getAccountId());
        financialApiActivity.setApplicationName(BCMOCA);
        financialApiActivity.setBilReasonCode(reason);
        financialApiActivity.setTransactionObjectCode(objectCd);
        financialApiActivity.setTransactionActionCode(actionCd);
        financialApiActivity.setUserId(BLANK_STRING);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setMasterCompanyNbr(bilPolicyTerm.getMasterCompanyNbr());
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setEffectiveDate(se3Date);
        financialApiActivity.setOperatorId(BLANK_STRING);
        financialApiActivity.setAppProgramId(BCMOCA);
        financialApiActivity.setCountryCode(bilPolicyTerm.getBillCountryCd());
        financialApiActivity.setStateCode(bilPolicyTerm.getBillStatePvnCd());
        financialApiActivity.setCountyCode(bilPolicyTerm.getBillCountryCd());
        financialApiActivity.setLineOfBusCode(bilPolicyTerm.getLobCd());
        financialApiActivity.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setCurrencyCode(bilAccount.getCurrencyCode());
        financialApiActivity.setOriginalEffectiveDate(
                DateRoutine.dateTimeAsYYYYMMDDString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()));
        financialApiActivity.setBilBankCode(BLANK_STRING);
        financialApiActivity.setReferenceDate(se3Date);
        financialApiActivity.setBilPostDate(se3Date);
        financialApiActivity.setBilAccountId(bilAccount.getAccountNumber());
        financialApiActivity.setBilReceiptTypeCode(BLANK_STRING);
        financialApiActivity.setBilTypeCode(bilAccount.getBillTypeCd());
        financialApiActivity.setBilClassCode(bilAccount.getBillClassCd());
        financialApiActivity.setPayableItemCode(BLANK_STRING);
        financialApiActivity.setBilDatabaseKey(BLANK_STRING);
        financialApiActivity.setAgentTtyId(BLANK_STRING);
        financialApiActivity.setBilSourceCode(BLANK_STRING);
        financialApiActivity.setActivityAmount(balanceChange);
        financialApiActivity.setActivityNetAmount(balanceChange);

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                dateService.currentDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
    }

    private void updateCollectionAmount(String accountId, String policyId, double collectionBalance) {
        bilPolicyTermRepository.updateCollectionAmounts(collectionBalance, accountId, policyId, DECIMAL_ZERO);
    }

    private Double[] deterCollectionBalance(String accountId, BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy,
            boolean includeCharge) {
        double collectionBalance;
        double writeOffCharge = DECIMAL_ZERO;
        double revWriteOffCharge = DECIMAL_ZERO;
        double serviceChargeBalance = DECIMAL_ZERO;

        Double policyBalance = bilIstScheduleRepository.getPolicyBalance(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                Arrays.asList(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                        BilPolicyStatusCode.TRANSFERRED.getValue(), BilPolicyStatusCode.PENDING_TRANSFER.getValue(),
                        BilPolicyStatusCode.PENDING_TRANSFER_APPLYPAYMENTS.getValue(),
                        BilPolicyStatusCode.PENDING_TRANSFER_HOLDPAYMENTS.getValue()));
        if (policyBalance == null) {
            policyBalance = DECIMAL_ZERO;
        }
        policyBalance = calculateReissueNewTermBalance(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                policyBalance);
        Object[] premiumWriteOffObject = calculatePremiumWriteOffBalance(accountId, bilPolicy.getPolNbr(),
                bilPolicy.getPolSymbolCd());
        ZonedDateTime iniDate = (ZonedDateTime) premiumWriteOffObject[0];
        Short iniSequenceNumber = (Short) premiumWriteOffObject[1];
        double writeOffPremium = (double) premiumWriteOffObject[2];
        double revWriteOffPremiumAmount = (double) premiumWriteOffObject[3];

        if (includeCharge) {
            serviceChargeBalance = calculateChargeAmounts(accountId);
            if (iniDate != null) {
                Object[] chargeAmountsObject = calculateChargeWriteOffBalance(accountId, iniDate, iniSequenceNumber);
                writeOffCharge = (double) chargeAmountsObject[0];
                revWriteOffCharge = (double) chargeAmountsObject[1];
            }
        }

        double remainWroAmtPrm = DECIMAL_ZERO;
        double remainWroAmtCrg = DECIMAL_ZERO;
        if (writeOffPremium + writeOffCharge != DECIMAL_ZERO) {
            if (writeOffPremium - revWriteOffPremiumAmount < DECIMAL_ZERO) {
                writeOffPremium = DECIMAL_ZERO;
                revWriteOffPremiumAmount = DECIMAL_ZERO;
            }
            if (writeOffCharge - revWriteOffCharge < 0) {
                writeOffCharge = DECIMAL_ZERO;
                revWriteOffCharge = DECIMAL_ZERO;
            }
            remainWroAmtPrm = writeOffPremium - revWriteOffPremiumAmount;
            remainWroAmtCrg = writeOffCharge - revWriteOffCharge;
        }
        collectionBalance = policyBalance + serviceChargeBalance + remainWroAmtPrm + remainWroAmtCrg;
        return new Double[] { collectionBalance, policyBalance, serviceChargeBalance };
    }

    private Object[] calculateChargeWriteOffBalance(String accountId, ZonedDateTime iniDate, Short iniSequenceNumber) {
        Double writeOffChargeAmount = bilActSummaryRepository.getRevManWriteOffPremiumAmount(accountId, iniDate,
                iniSequenceNumber, iniDate, BLANK_STRING, BLANK_STRING, Arrays.asList("WPA", "W01"));
        if (writeOffChargeAmount == null) {
            writeOffChargeAmount = DECIMAL_ZERO;
        }
        Double revWriteOffChargeAmount = bilActSummaryRepository.getRevManWriteOffPremiumAmount(accountId, iniDate,
                iniSequenceNumber, iniDate, BLANK_STRING, BLANK_STRING, Arrays.asList("WRR"));
        if (revWriteOffChargeAmount == null) {
            revWriteOffChargeAmount = DECIMAL_ZERO;
        }
        Double revAutWriteOffCharge = bilActSummaryRepository.getRevManWriteOffPremiumAmount(accountId, iniDate,
                iniSequenceNumber, iniDate, BLANK_STRING, BLANK_STRING, Arrays.asList("RW1"));
        if (revAutWriteOffCharge == null) {
            revAutWriteOffCharge = DECIMAL_ZERO;
        }
        if (revAutWriteOffCharge < 0) {
            revAutWriteOffCharge = revAutWriteOffCharge * -1;
        }
        revWriteOffChargeAmount = revWriteOffChargeAmount + revAutWriteOffCharge;
        return new Object[] { writeOffChargeAmount, revWriteOffChargeAmount };
    }

    private Double calculateChargeAmounts(String accountId) {
        Double serviceChargeBalance = null;
        final List<Character> bilCrgTypeCdList = Arrays.asList(ChargeTypeCodes.PENALTY_CHARGE.getValue(),
                ChargeTypeCodes.LATE_CHARGE.getValue(), ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue());
        final List<Character> bilInvoiceCdList = Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());
        serviceChargeBalance = bilCrgAmountsRepository.getTotalCrgAmount(bilInvoiceCdList, accountId, bilCrgTypeCdList,
                ChargeTypeCodes.SERVICE_CHARGE.getValue());

        if (serviceChargeBalance == null) {
            serviceChargeBalance = DECIMAL_ZERO;
        }
        return serviceChargeBalance;
    }

    private Object[] calculatePremiumWriteOffBalance(String accountId, String polNumber, String polSymbolCd) {
        Double revWriteOffPremiumAmount = DECIMAL_ZERO;
        ZonedDateTime bilAcyDate = DateRoutine.dateTimeAsYYYYMMDD(0);
        Short sequenceNumber = SHORT_ZERO;
        List<String> bilAcyDesCdList = Arrays.asList("STP", "COF");
        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.getMaxBilAcyDate(accountId, polNumber,
                polSymbolCd, bilAcyDesCdList);
        if (bilActSummaryList != null && !bilActSummaryList.isEmpty()) {
            bilAcyDate = bilActSummaryList.get(SHORT_ZERO).getBillActSummaryId().getBilAcyDt();
            sequenceNumber = bilActSummaryList.get(SHORT_ZERO).getBillActSummaryId().getBilAcySeq();
        }

        ZonedDateTime iniAcyDate = bilActSummaryRepository.findMaxIniBilActivityDate(accountId, polNumber, polSymbolCd,
                "INI", bilAcyDate, sequenceNumber, bilAcyDate);
        Short iniSequenceNumber = null;
        Double writeOffPremium = DECIMAL_ZERO;
        if (iniAcyDate != null) {
            iniSequenceNumber = bilActSummaryRepository.findMaxIniBilSequence(accountId, polNumber, polSymbolCd, "INI",
                    iniAcyDate);
            if (iniSequenceNumber != null) {
                writeOffPremium = bilActSummaryRepository.getPrimeumWriteOffAmounts(accountId, iniAcyDate,
                		iniSequenceNumber, iniAcyDate, polNumber, polSymbolCd, Arrays.asList("WC", "WE", "WU"), "WPA");
                if (writeOffPremium == null) {
                    writeOffPremium = DECIMAL_ZERO;
                }
                Double revManWriteOffPPremium = bilActSummaryRepository.getRevManWriteOffPremiumAmount(accountId,
                        iniAcyDate, iniSequenceNumber, iniAcyDate, polNumber, polSymbolCd, Arrays.asList("WRR"));
                if (revManWriteOffPPremium == null) {
                    revManWriteOffPPremium = DECIMAL_ZERO;
                }
                Double revAutWriteOffPPremium = bilActSummaryRepository.getRevManWriteOffPremiumAmount(accountId,
                        iniAcyDate, iniSequenceNumber, iniAcyDate, polNumber, polSymbolCd, Arrays.asList("RW1"));
                if (revAutWriteOffPPremium == null) {
                    revAutWriteOffPPremium = DECIMAL_ZERO;
                }
                revWriteOffPremiumAmount = revManWriteOffPPremium + revAutWriteOffPPremium;
            }
        }
        return new Object[] { iniAcyDate, iniSequenceNumber, writeOffPremium, revWriteOffPremiumAmount };
    }

    private Double calculateReissueNewTermBalance(String accountId, String policyId, Double policyBalance) {
        BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(policyId, accountId)).orElse(null);
        if (bilPolicy != null) {
            List<String> policyIdList = bilPolicyRepository.findReissuePolicyId(policyId, accountId,
                    bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                    PolicyIssueIndicators.CANCELLED_REISSUE.getValue());
            if (policyIdList != null && !policyIdList.isEmpty()) {
                for (String oldPolicyId : policyIdList) {
                    Double reissueBalance = bilIstScheduleRepository.getUnpaidPremAmount(accountId, oldPolicyId);
                    if (reissueBalance == null) {
                        reissueBalance = DECIMAL_ZERO;
                    }
                    policyBalance = policyBalance + reissueBalance;
                }
            }

        }
        return policyBalance;
    }

    private boolean processCashFlows(CashApplyDriver cashApplyDriver, BilAccount bilAccount, Double totalCashAmount,
            List<BilCashDsp> downPaymentList, boolean civEftSw, List<String> thirdPartyIdList, BilActRules bilActRules,
            String accountType) {
        char correctedInvoive = BLANK_CHAR;
        ZonedDateTime distributionDate = null;
        short distributionSequence = SHORT_ZERO;
        if (cashApplyDriver.isCallBcmoax() && accountType.equalsIgnoreCase(DIRECT)) {
            if (bilAccount.getStatus() == CHAR_I) {
                callBcmoax(cashApplyDriver, correctedInvoive, civEftSw, false, null, SHORT_ZERO);
                return false;
            } else {
                callBcmoax(cashApplyDriver, correctedInvoive, civEftSw, true, null, SHORT_ZERO);
            }
        }

        BilRulesUct rpauRules = null;
        BilSupportPlan bilSupportPlan = bilSupportPlanRepository
                .findById(new BilSupportPlanId(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd())).orElse(null);
        if (bilSupportPlan == null) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }
        if (bilSupportPlan.getBspCshOvpInd() != CashOverPayIndicators.SUSPEND.getValue()) {
            rpauRules = bilRulesUctService.readBilRulesUct("RPAU", bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd(), SEPARATOR_BLANK);
        }

        Map<String, Object> civCollectionMap = null;
        if (bilActRules.getBruCcrPartInd() == CHAR_Y && accountType.equalsIgnoreCase(DIRECT)) {
            civCollectionMap = sumCivAmounts(true, cashApplyDriver.getAccountId(), bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd());
            correctedInvoive = (char) civCollectionMap.get(CORRECTED_INVOICE);
        }

        Object[] processCashData = processCash(cashApplyDriver, bilAccount, bilActRules, downPaymentList,
                bilSupportPlan, totalCashAmount, rpauRules, accountType, true, distributionDate, distributionSequence);
        boolean vendorPayment = (boolean) processCashData[SHORT_ZERO];
        boolean noDownPaymentToPay = (boolean) processCashData[SHORT_ONE];
        distributionDate = (ZonedDateTime) processCashData[2];
        distributionSequence = (short) processCashData[3];

        if (bilActRules.getBruCcrPartInd() == CHAR_Y && correctedInvoive == BLANK_CHAR) {
            correctedInvoive = adjustCorrectedInvoice(cashApplyDriver.getAccountId(), civCollectionMap,
                    bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(), civEftSw,
                    bilSupportPlan.getBspMinInvAmt(), cashApplyDriver.getCashApplyDate());
        }

        if (thirdPartyIdList == null || thirdPartyIdList.isEmpty()) {
            ZonedDateTime maxPolicyEffDate = bilPolicyTermRepository
                    .getMaxPolicyEffectiveDateByAccountId(cashApplyDriver.getAccountId());

            char invoiceIndicator = processAccountAdjustDate(cashApplyDriver.getAccountId(), maxPolicyEffDate,
                    cashApplyDriver.getQuoteIndicator(), cashApplyDriver.getCashApplyDate());

            if (invoiceIndicator == CHAR_F) {
                if (!noDownPaymentToPay) {
                    downPaymentList = evaluateDownPaymentCash(accountType, cashApplyDriver.getAccountId(),
                            bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(), false, cashApplyDriver.getCashApplyDate());
                }

                processCashData = processCash(cashApplyDriver, bilAccount, bilActRules, downPaymentList, bilSupportPlan,
                        totalCashAmount, rpauRules, accountType, false, distributionDate, distributionSequence);
                vendorPayment = (boolean) processCashData[SHORT_ZERO];
                distributionDate = (ZonedDateTime) processCashData[2];
                distributionSequence = (short) processCashData[3];
                processAccountAdjustDate(cashApplyDriver.getAccountId(), maxPolicyEffDate,
                        cashApplyDriver.getQuoteIndicator(), cashApplyDriver.getCashApplyDate());
            }
        }
        if (cashApplyDriver.isCallBcmoax()) {
            callBcmoax(cashApplyDriver, correctedInvoive, civEftSw, false, distributionDate, distributionSequence);
        }
        if (thirdPartyIdList != null && !thirdPartyIdList.isEmpty() && accountType.equalsIgnoreCase(THIRD_PARTY)) {
            checkAccountStatus(bilAccount.getAccountId());
        }
        return vendorPayment;
    }

    private char adjustCorrectedInvoice(String accountId, Map<String, Object> civCollectionMap, String billTypeCd,
            String billClassCd, boolean civEftSw, double minInvoiceAmount, ZonedDateTime currentDate) {
        boolean civAmountsExist = false;
        char correctedInvoive = BLANK_CHAR;
        double civPaymentBAmount = DECIMAL_ZERO;
        if (civCollectionMap.get(CIV_PREMIUM_PAID_AMOUNT) != null) {
            civPaymentBAmount = civPaymentBAmount + (Double) civCollectionMap.get(CIV_PREMIUM_PAID_AMOUNT);
        }
        if (civCollectionMap.get(CIV_CHARGE_PAID_AMOUNT) != null) {
            civPaymentBAmount = civPaymentBAmount + (Double) civCollectionMap.get(CIV_CHARGE_PAID_AMOUNT);
        }
        if (civCollectionMap.get(CIV_AMOUNT_EXIST) != null) {
            civAmountsExist = (boolean) civCollectionMap.get(CIV_AMOUNT_EXIST);
        }

        if (civCollectionMap.get(CORRECTED_INVOICE) != null) {
            correctedInvoive = (char) civCollectionMap.get(CORRECTED_INVOICE);
        }

        ZonedDateTime civInvoiceDate = (ZonedDateTime) civCollectionMap.get("civInvoiceDate");

        double civPaymentAAmount = DECIMAL_ZERO;
        double civBalanceAAmount = DECIMAL_ZERO;
        civCollectionMap = sumCivAmounts(civAmountsExist, accountId, billTypeCd, billClassCd);

        if (civCollectionMap.get(CIV_PREMIUM_PAID_AMOUNT) != null) {
            civPaymentAAmount = civPaymentAAmount + (Double) civCollectionMap.get(CIV_PREMIUM_PAID_AMOUNT);
        }
        if (civCollectionMap.get(CIV_CHARGE_PAID_AMOUNT) != null) {
            civPaymentAAmount = civPaymentAAmount + (Double) civCollectionMap.get(CIV_CHARGE_PAID_AMOUNT);
        }
        if (civCollectionMap.get(CIV_PREMIUM_BALANCE_AMOUNT) != null) {
            civBalanceAAmount = civBalanceAAmount + (Double) civCollectionMap.get(CIV_PREMIUM_BALANCE_AMOUNT);
        }
        if (civCollectionMap.get(CIV_CHARGE_BALANCE_AMOUNT) != null) {
            civBalanceAAmount = civBalanceAAmount + (Double) civCollectionMap.get(CIV_CHARGE_BALANCE_AMOUNT);
        }

        double cimtCivMinAmount = DECIMAL_ZERO;
        char cimtCivTolCode = BLANK_CHAR;
        BilRulesUct cimtRules = bilRulesUctService.readBilRulesUct("CIMT", billTypeCd, billClassCd, BLANK_STRING);
        if (cimtRules != null && cimtRules.getBrtRuleCd() == CHAR_Y && cimtRules.getBrtParmListTxt() != null
                && !cimtRules.getBrtParmListTxt().isEmpty()) {
            if (cimtRules.getBrtParmListTxt().length() >= 1) {
                cimtCivTolCode = cimtRules.getBrtParmListTxt().substring(0, 1).charAt(0);
            }
            if (cimtRules.getBrtParmListTxt().length() >= 14) {
                cimtCivMinAmount = Double.parseDouble(cimtRules.getBrtParmListTxt().substring(1, 14));
            }
        }

        if ((civPaymentAAmount != civPaymentBAmount) && (cimtCivTolCode != CHAR_A && cimtCivTolCode != CHAR_P
                || cimtCivTolCode == CHAR_A && civBalanceAAmount > 0 && civBalanceAAmount >= minInvoiceAmount
                || cimtCivTolCode == CHAR_P && civBalanceAAmount >= cimtCivMinAmount)) {
            ZonedDateTime bilDtbDate = bilCashDspRepository.getMaxBilDistributionDate(accountId,
                    BilDspTypeCode.APPLIED.getValue(), ManualSuspendIndicator.BLANK.getValue(),
                    currentDate);
            if (bilDtbDate == null || bilDtbDate != null && bilDtbDate.compareTo(civInvoiceDate) < 0) {
                correctedInvoive = CHAR_X;
            }
            if (correctedInvoive != CHAR_X && correctedInvoive != CHAR_N) {
                if (civEftSw) {
                    correctedInvoive = '2';
                } else {
                    correctedInvoive = CHAR_C;
                }
            }
        }
        return correctedInvoive;
    }

    private void checkAccountStatus(String accountId) {
        final char ACTIVE_STATUS = CHAR_A;
        final char INACTIVE_STATUS = CHAR_I;
        char status = ACTIVE_STATUS;
        char statusUpdate = INACTIVE_STATUS;

        Double accountBalance = bilIstScheduleRepository.getAccountBalance(accountId, Arrays.asList(
                BilPolicyStatusCode.SUSPEND_BILLING.getValue(), BilPolicyStatusCode.SUSPENDBILLING_FOLLOWUP.getValue(),
                BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue(),
                BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue(), BilPolicyStatusCode.PENDING_CANCELLATION.getValue(),
                BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue(),
                BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue(),
                BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue()));
        if (accountBalance != null && accountBalance != DECIMAL_ZERO) {
            status = INACTIVE_STATUS;
            statusUpdate = ACTIVE_STATUS;
        }
        updateBilAccountStatus(accountId, status, statusUpdate);
    }

    private void updateBilAccountStatus(String accountId, char status, char statusUpdate) {
        BilAccount bilAccountUpdate = bilAccountRepository.findById(accountId).orElse(null);
        if (bilAccountUpdate != null && bilAccountUpdate.getStatus() == status) {
            bilAccountUpdate.setStatus(statusUpdate);
            bilAccountRepository.save(bilAccountUpdate);
        }
    }

    private void callBcmoax(CashApplyDriver cashApplyDriver, char correctedInvoive, boolean civEftSw,
            boolean isRecovery, ZonedDateTime distributionDate, short distributionSequence) {

        AccountProcessingAndRecovery accountProcessingAndRecovery = new AccountProcessingAndRecovery();
        accountProcessingAndRecovery.setAccountId(cashApplyDriver.getAccountId());
        accountProcessingAndRecovery.setDriverIndicator(CHAR_C);
        accountProcessingAndRecovery.setQuoteIndicator(cashApplyDriver.getQuoteIndicator().charAt(SHORT_ZERO));
        accountProcessingAndRecovery.setRecoveryDate(cashApplyDriver.getCashApplyDate());
        accountProcessingAndRecovery.setDistributionDate(distributionDate);
        accountProcessingAndRecovery.setDistributionSequenceNumber(distributionSequence);
        accountProcessingAndRecovery.setUserSequenceId(cashApplyDriver.getUserSequenceId());
        if (!isRecovery) {
            if (!cashApplyDriver.isNoReinstatement()) {
                accountProcessingAndRecovery.setCashAcyCallIndicator(CHAR_R);
            } else {
                accountProcessingAndRecovery.setCashAcyCallIndicator(CHAR_Y);
            }
        } else {
            accountProcessingAndRecovery.setRecoverCheckIndicator(CHAR_Y);
        }

        if (correctedInvoive == CHAR_C || correctedInvoive == '2') {
            if (civEftSw) {
                accountProcessingAndRecovery.setCorrectedInvocie('2');
            } else {
                accountProcessingAndRecovery.setCorrectedInvocie(CHAR_C);
            }
        }

        accountProcessingAndRecoveryService.accountAndRecoveryProcess(accountProcessingAndRecovery);

    }

    private char processAccountAdjustDate(String accountId, ZonedDateTime maxPolicyEffDate, String quoteInd, ZonedDateTime currentDate) {
        char addChargeInd = BLANK_CHAR;
        if (maxPolicyEffDate == null) {
            addChargeInd = CHAR_C;
        }

        AccountAdjustDates accountAdjustDates = new AccountAdjustDates();
        accountAdjustDates.setAccountId(accountId);
        accountAdjustDates.setApplicationName(BCMOCA);
        accountAdjustDates.setQuoteIndicator(quoteInd);
        accountAdjustDates.setAddChargeIndicator(addChargeInd);
        accountAdjustDates.setProcessDate(currentDate);

        chargesAccountAdjustDatesService.processChargesAccountAdjustDates(accountAdjustDates);

        return accountAdjustDates.getInvoiceIndicator();
    }

    private Object[] processCash(CashApplyDriver cashApplyDriver, BilAccount bilAccount, BilActRules bilActRules,
            List<BilCashDsp> downPaymentList, BilSupportPlan bilSupportPlan, Double totalCashAmount,
            BilRulesUct rpauRules, String accountType, boolean firstTime, ZonedDateTime distributionDate,
            short distributionSequence) {
        boolean vendorPayment = false;
        boolean cancelRewriteProcess = false;
        boolean policyTransferCash = false;
        boolean transAcctCash = false;
        boolean noDownPaymentToPay = false;

        if (firstTime && cashApplyDriver.getTranferCashPolicyNumber() != null
                && !cashApplyDriver.getTranferCashPolicyNumber().trim().isEmpty()) {
            policyTransferCash = true;
        }

        if (bilActRules.getBruWroLteInd() == CHAR_Y) {
            checkForLateChargeWriteOff(cashApplyDriver, bilAccount);
        }
        if (downPaymentList != null && !downPaymentList.isEmpty()) {
            distributionDate = downPaymentList.get(downPaymentList.size() - 1).getBilCashDspId().getBilDtbDate();
            distributionSequence = downPaymentList.get(downPaymentList.size() - 1).getBilCashDspId()
                    .getDtbSequenceNbr();
            noDownPaymentToPay = applyDownpaymentCash(cashApplyDriver, downPaymentList);
        }

        Integer cancelRewriteRowsCount = bilCashDspRepository.cancelRewriteCountByAccountId(
                BilDspTypeCode.SUSPENDED.getValue(), CHAR_Y, BLANK_CHAR, cashApplyDriver.getAccountId(),
                DateRoutine.defaultDateTime(), CreditIndicator.CANCELLATION_FOR_REWRITE.getValue(), BLANK_STRING);
        if (cancelRewriteRowsCount != null && cancelRewriteRowsCount != SHORT_ZERO) {
            cancelRewriteProcess = true;
        }

        char apcrRules = fetchApcrRule(cashApplyDriver.getAccountId(), bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd());
        if (apcrRules == CHAR_A || apcrRules == CHAR_P) {
            Object[] distributionInfo = processCreditCashFirst(cashApplyDriver, bilAccount, bilSupportPlan, bilActRules,
                    totalCashAmount, rpauRules, policyTransferCash, distributionDate, distributionSequence);
            distributionDate = (ZonedDateTime) distributionInfo[SHORT_ZERO];
            distributionSequence = (short) distributionInfo[SHORT_ONE];
            totalCashAmount = sumCashAmount(cashApplyDriver.getAccountId());
        }

        boolean removeSdtIndicator = false;

        Object[] suspendObject = performCashLoop(cashApplyDriver, cancelRewriteProcess, bilSupportPlan, bilAccount,
                totalCashAmount, rpauRules, bilActRules, removeSdtIndicator, accountType, policyTransferCash,
                distributionDate, distributionSequence);

        boolean suspenseCash = (boolean) suspendObject[0];
        boolean suspendSdtFound = (boolean) suspendObject[1];
        ZonedDateTime dueDateHold = (ZonedDateTime) suspendObject[2];
        distributionDate = (ZonedDateTime) suspendObject[4];
        distributionSequence = (short) suspendObject[5];
        if (cashApplyDriver.getTranferCashPolicyNumber() != null
                && !cashApplyDriver.getTranferCashPolicyNumber().trim().isEmpty()) {
            transAcctCash = true;
        }
        if (suspenseCash) {
            suspendSdtFound = false;
            removeSdtIndicator = true;
            suspendObject = performCashData(cashApplyDriver, cancelRewriteProcess, bilSupportPlan, bilAccount,
                    totalCashAmount, rpauRules, bilActRules, removeSdtIndicator, accountType, policyTransferCash,
                    transAcctCash, distributionDate, distributionSequence);
            distributionDate = (ZonedDateTime) suspendObject[4];
            distributionSequence = (short) suspendObject[5];
        }
        if (suspendSdtFound) {
            suspendObject = performCashData(cashApplyDriver, cancelRewriteProcess, bilSupportPlan, bilAccount,
                    totalCashAmount, rpauRules, bilActRules, removeSdtIndicator, accountType, policyTransferCash,
                    transAcctCash, distributionDate, distributionSequence);
            distributionDate = (ZonedDateTime) suspendObject[4];
            distributionSequence = (short) suspendObject[5];
        }
        vendorPayment = (boolean) suspendObject[3];

        if (dueDateHold != null) {
            BilCashDsp bilCashDsp = invoiceWriteOffProcess(cashApplyDriver, dueDateHold, bilSupportPlan, bilAccount,
                    vendorPayment, rpauRules, totalCashAmount, bilActRules);
            if (bilCashDsp != null) {
                distributionDate = bilCashDsp.getBilCashDspId().getBilDtbDate();
                distributionSequence = bilCashDsp.getBilCashDspId().getDtbSequenceNbr();
            }
        }

        return new Object[] { vendorPayment, noDownPaymentToPay, distributionDate, distributionSequence };
    }

    private Object[] performCashLoop(CashApplyDriver cashApplyDriver, boolean cancelRewriteProcess,
            BilSupportPlan bilSupportPlan, BilAccount bilAccount, Double totalCashAmount, BilRulesUct rpauRules,
            BilActRules bilActRules, boolean removeSdtIndicator, String accountType, boolean policyTransferCash,
            ZonedDateTime distributionDate, short distributionSequence) {

        Object[] suspendObject = null;
        boolean transAcctCash = false;
        if (cancelRewriteProcess) {
            suspendObject = performCashData(cashApplyDriver, cancelRewriteProcess, bilSupportPlan, bilAccount,
                    totalCashAmount, rpauRules, bilActRules, removeSdtIndicator, accountType, policyTransferCash,
                    transAcctCash, distributionDate, distributionSequence);
            cancelRewriteProcess = false;
            suspendObject = performCashData(cashApplyDriver, cancelRewriteProcess, bilSupportPlan, bilAccount,
                    totalCashAmount, rpauRules, bilActRules, removeSdtIndicator, accountType, policyTransferCash,
                    transAcctCash, distributionDate, distributionSequence);
        } else if (policyTransferCash) {

            char holdOverPaymentIndicator = bilSupportPlan.getBspCshOvpInd();
            bilSupportPlan.setBspCshOvpInd(CashOverPayIndicators.SATISFY_OLDEST_DUE_FIRST.getValue());

            suspendObject = performCashData(cashApplyDriver, cancelRewriteProcess, bilSupportPlan, bilAccount,
                    totalCashAmount, rpauRules, bilActRules, removeSdtIndicator, accountType, policyTransferCash,
                    transAcctCash, distributionDate, distributionSequence);

            bilSupportPlan.setBspCshOvpInd(holdOverPaymentIndicator);
            policyTransferCash = false;
            transAcctCash = true;
            suspendObject = performCashData(cashApplyDriver, cancelRewriteProcess, bilSupportPlan, bilAccount,
                    totalCashAmount, rpauRules, bilActRules, removeSdtIndicator, accountType, policyTransferCash,
                    transAcctCash, distributionDate, distributionSequence);
        } else {
            suspendObject = performCashData(cashApplyDriver, cancelRewriteProcess, bilSupportPlan, bilAccount,
                    totalCashAmount, rpauRules, bilActRules, removeSdtIndicator, accountType, policyTransferCash,
                    transAcctCash, distributionDate, distributionSequence);
        }

        return suspendObject;
    }

    private List<BilCashDsp> fetchCallCashData(CashApplyDriver cashApplyDriver, boolean cancelRewriteProcess,
            boolean transAcctCash, boolean policyTransferCash) {
        List<BilCashDsp> bilCashDspList;
        if (cancelRewriteProcess) {
            bilCashDspList = bilCashDspRepository.fetchCancelForRewriteCash(cashApplyDriver.getAccountId(),
                    BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                    ManualSuspendIndicator.ENABLED.getValue(), DateRoutine.defaultDateTime(),
                    CreditIndicator.CANCELLATION_FOR_REWRITE.getValue(), BLANK_STRING);
        } else if (transAcctCash) {
            bilCashDspList = bilCashDspRepository.fetchBilTranferCash(cashApplyDriver.getAccountId(),
                    BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                    ManualSuspendIndicator.ENABLED.getValue(), cashApplyDriver.getTranferCashPolicyNumber());
        } else if (policyTransferCash) {
            bilCashDspList = bilCashDspRepository.fetchBilTranferCash(cashApplyDriver.getAccountId(),
                    BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                    ManualSuspendIndicator.ENABLED.getValue(), BLANK_STRING);
        } else {
            bilCashDspList = bilCashDspRepository.fetchCashBySpreadCashOver(cashApplyDriver.getAccountId(),
                    BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                    ManualSuspendIndicator.ENABLED.getValue());
        }
        return bilCashDspList;
    }

    private BilCashDsp invoiceWriteOffProcess(CashApplyDriver cashApplyDriver, ZonedDateTime dueDateHold,
            BilSupportPlan bilSupportPlan, BilAccount bilAccount, boolean vendorPayment, BilRulesUct rpauRules,
            Double totalCashAmount, BilActRules bilActRules) {

        Integer count = bilCashDspRepository.checkInvoiceWriteOff(cashApplyDriver.getAccountId(), dueDateHold,
                BLANK_STRING, BLANK_STRING, "SYSTEMIW", BilDspTypeCode.APPLIED.getValue(),
                ReverseReSuspendIndicator.BLANK.getValue(), ManualSuspendIndicator.ENABLED.getValue());
        if (count != null && count > SHORT_ZERO) {
            return null;
        }

        double totalSuspendWriteOff = fetchTotalWriteOffs(cashApplyDriver.getAccountId(), dueDateHold);
        Double totalScheduleDueAmount = bilIstScheduleRepository.getScheduleDueAmount(cashApplyDriver.getAccountId(),
                getInvoiceCodeList(), dueDateHold);
        if (totalScheduleDueAmount != null) {
            Double totalChargeAmountDue = bilCrgAmountsRepository.getChargeAmountDue(cashApplyDriver.getAccountId(),
                    getInvoiceCodeList(), dueDateHold);
            if (totalChargeAmountDue == null) {
                totalChargeAmountDue = DECIMAL_ZERO;
            }
            totalScheduleDueAmount = totalScheduleDueAmount + totalChargeAmountDue;
            if (totalScheduleDueAmount != DECIMAL_ZERO || totalSuspendWriteOff != DECIMAL_ZERO) {

                if (totalScheduleDueAmount > totalSuspendWriteOff) {
                    totalScheduleDueAmount = totalScheduleDueAmount - totalSuspendWriteOff;
                    if (totalScheduleDueAmount <= bilSupportPlan.getBspUpyWroAmt()) {
                        BilCashDsp bilCashDsp = processInvoiceWriteOff('U', totalScheduleDueAmount, dueDateHold,
                                cashApplyDriver.getCashApplyDate(), cashApplyDriver.getQuoteIndicator(), bilAccount);

                        processCashApply(cashApplyDriver, bilAccount, bilCashDsp, bilSupportPlan, bilActRules,
                                totalCashAmount, vendorPayment, "IW", rpauRules, DECIMAL_ZERO, false, false, false);
                        return bilCashDsp;
                    }

                } else if (totalScheduleDueAmount < totalSuspendWriteOff) {
                    totalSuspendWriteOff = totalSuspendWriteOff - totalScheduleDueAmount;
                    if (totalSuspendWriteOff <= bilSupportPlan.getBspOvpWroAmt()) {
                        processInvoiceWriteOff('O', DECIMAL_ZERO, dueDateHold, cashApplyDriver.getCashApplyDate(),
                                cashApplyDriver.getQuoteIndicator(), bilAccount);
                    }
                }
            }
        }

        return null;
    }

    private BilCashDsp processInvoiceWriteOff(char processIndicator, Double paymentAmount, ZonedDateTime adjustDueDate,
            ZonedDateTime applicationDate, String quoteIndicator, BilAccount bilAccount) {

        BilCashDsp bilCashDsp = null;
        WriteOffPayment writeOffPayment = new WriteOffPayment();
        writeOffPayment.setProcessIndicator(processIndicator);
        writeOffPayment.setPaymentAmount(paymentAmount);
        writeOffPayment.setAdjustDueDate(adjustDueDate);
        writeOffPayment.setApplicationDate(applicationDate);
        writeOffPayment.setQuoteIndicator(quoteIndicator);
        if (processIndicator == 'U') {
            bilCashDsp = writeOffDueToUnderPaymentServiceImpl.writeOffDueToUnderPayment(bilAccount, writeOffPayment);
        } else {
            bilCashDsp = writeOffDueToOverPaymentServiceImpl.writeOffDueToOverPayment(bilAccount, writeOffPayment);
        }

        return bilCashDsp;

    }

    private double fetchTotalWriteOffs(String accountId, ZonedDateTime dueDateHold) {
        double totalSuspendWriteOff = DECIMAL_ZERO;

        List<BilCashReceipt> bilCashReceiptList = bilCashReceiptRepository.fetchWriteOffReceipt(accountId, CHAR_Y);
        for (BilCashReceipt bilCashReceipt : bilCashReceiptList) {
            Double sumSuspendWriteOff = bilCashDspRepository.fetchSuspendWriteOff(dueDateHold, accountId,
                    bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                    bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), BilDspTypeCode.SUSPENDED.getValue(),
                    BLANK_CHAR, CHAR_Y, BLANK_STRING, BLANK_STRING);
            if (sumSuspendWriteOff == null) {
                sumSuspendWriteOff = DECIMAL_ZERO;
            }
            totalSuspendWriteOff = totalSuspendWriteOff + sumSuspendWriteOff;
        }

        return totalSuspendWriteOff;
    }

    private Object[] performCashData(CashApplyDriver cashApplyDriver, boolean cancelRewriteProcess,
            BilSupportPlan bilSupportPlan, BilAccount bilAccount, Double totalCashAmount, BilRulesUct rpauRules,
            BilActRules bilActRules, boolean removeSdtIndicator, String accountType, boolean policyTransferCash,
            boolean transAcctCash, ZonedDateTime distributionDate, short distributionSequence) {
        boolean suspendCash = false;
        boolean suspendSDTInd = false;
        boolean vendorPayment = false;
        ZonedDateTime dueDateHold = null;
        Object[] suspendObject;
        List<BilCashDsp> bilCashDspList = fetchCallCashData(cashApplyDriver, cancelRewriteProcess, transAcctCash,
                policyTransferCash);
        for (BilCashDsp bilCashDsp : bilCashDspList) {
            distributionDate = bilCashDsp.getBilCashDspId().getBilDtbDate();
            distributionSequence = bilCashDsp.getBilCashDspId().getDtbSequenceNbr();
            if (!bilCashDsp.getPolicyNumber().trim().isEmpty() && accountType.equalsIgnoreCase(DIRECT)
                    && !bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())) {
                evaluateRevAutoWriteOff(cashApplyDriver, bilCashDsp, bilAccount, totalCashAmount);
            }
            BilCashReceipt bilCashReceipt = bilCashReceiptRepository
                    .findById(new BilCashReceiptId(bilCashDsp.getBilCashDspId().getAccountId(),
                            bilCashDsp.getBilCashDspId().getBilDtbDate(),
                            bilCashDsp.getBilCashDspId().getDtbSequenceNbr()))
                    .orElse(null);
            if (bilCashReceipt == null) {
                throw new DataNotFoundException("select.bil.cash.receipt.failed");
            }
            vendorPayment = checkVendorPayment(bilCashReceipt.getReceiptTypeCd());
            if ((bilCashDsp.getPolicyNumber().trim().isEmpty() && bilCashDsp.getPayableItemCd().trim().isEmpty()
                    && bilCashDsp.getAdjustmentDueDate().compareTo(DateRoutine.defaultDateTime()) != 0
                    && bilCashReceipt.getThirdPartyCashIdentifier() != CHAR_Y)
                    && (dueDateHold == null || bilCashDsp.getAdjustmentDueDate().compareTo(dueDateHold) > 0)) {
                dueDateHold = bilCashDsp.getAdjustmentDueDate();
            }
            boolean processTtyCash = false;
            if (bilCashReceipt.getThirdPartyCashIdentifier() == CHAR_Y
                    && bilCashDsp.getAdjustmentDueDate().compareTo(DateRoutine.defaultDateTime()) != SHORT_ZERO) {
                processTtyCash = true;
            } else if (bilCashReceipt.getThirdPartyCashIdentifier() != CHAR_Y
                    && bilCashDsp.getAdjustmentDueDate().compareTo(DateRoutine.defaultDateTime()) != SHORT_ZERO
                    && bilSupportPlan.getBspOvpWroAmt() == DECIMAL_ZERO
                    && bilSupportPlan.getBspUpyWroAmt() == DECIMAL_ZERO) {
                Integer isThirdPartyCash = bilDatesRepository.checkThirdPartyCash(cashApplyDriver.getAccountId(),
                        InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                        bilCashDsp.getAdjustmentDueDate(), CHAR_X);
                if (isThirdPartyCash != null && isThirdPartyCash > 0) {
                    processTtyCash = true;
                }
            }
            if (bilCashReceipt.getReceiptTypeCd().equalsIgnoreCase(BilReceiptTypeCode.EVEN_ADJUSTMENT.getValue())) {
                updateBilPolicyStatus(cashApplyDriver.getAccountId(), bilCashDsp.getPolicyNumber(), '#',
                        BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue());
            }
            boolean overrideOverPayment = false;
            boolean firstTimeCall = true;
            Double balanceDueAmount = DECIMAL_ZERO;
            if (rpauRules != null && rpauRules.getBrtRuleCd() == CHAR_Y
                    && !bilCashDsp.getPayableItemCd().trim().isEmpty() && !bilCashDsp.getPolicyNumber().trim().isEmpty()
                    && !bilCashDsp.getPolicySymbolCd().trim().isEmpty() && bilCashDsp.getCreditIndicator() == CHAR_H
                    && !bilCashDsp.getCreditPolicyId().trim().isEmpty()) {
                balanceDueAmount = bilIstScheduleRepository.getBalanceDueAmount(
                        bilCashDsp.getBilCashDspId().getAccountId(), bilCashDsp.getCreditPolicyId(),
                        bilCashDsp.getPayableItemCd(), getInvoiceCodeList());
                if (balanceDueAmount == null) {
                    balanceDueAmount = DECIMAL_ZERO;
                }
            }

            processCashApply(cashApplyDriver, bilAccount, bilCashDsp, bilSupportPlan, bilActRules, totalCashAmount,
                    vendorPayment, bilCashReceipt.getReceiptTypeCd(), rpauRules, balanceDueAmount, firstTimeCall,
                    overrideOverPayment, policyTransferCash);

            if (bilCashReceipt.getReceiptTypeCd().equalsIgnoreCase(BilReceiptTypeCode.EVEN_ADJUSTMENT.getValue())) {
                updateBilPolicyStatus(cashApplyDriver.getAccountId(), bilCashDsp.getPolicyNumber(),
                        BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(), '#');
            }
            BilCashDsp bilCashDspUpdate = bilCashDspRepository.findById(bilCashDsp.getBilCashDspId()).orElse(null);
            if (bilCashDspUpdate != null && rpauRules != null && rpauRules.getBrtRuleCd() == CHAR_Y
                    && bilCashDsp.getCreditIndicator() == CHAR_H
                    && bilCashDsp.getDspTypeCd().equalsIgnoreCase(BilDspTypeCode.SUSPENDED.getValue())) {
                overrideOverPayment = true;
                if (!bilCashDsp.getPolicyNumber().trim().isEmpty() && !bilCashDsp.getPayableItemCd().trim().isEmpty()
                        && !bilCashDsp.getPolicySymbolCd().trim().isEmpty()) {
                    bilCashDspUpdate.setPayableItemCd(BLANK_STRING);
                    bilCashDspUpdate.setPolicySymbolCd(BLANK_STRING);
                    bilCashDspUpdate.setPolicyNumber(BLANK_STRING);
                    firstTimeCall = false;
                    prepareCashApply(cashApplyDriver, bilAccount, bilCashDsp, bilSupportPlan, bilActRules,
                            totalCashAmount, vendorPayment, bilCashReceipt.getReceiptTypeCd(), null, rpauRules,
                            balanceDueAmount, firstTimeCall, overrideOverPayment, policyTransferCash);
                }

                if (processTtyCash) {

                    if (removeSdtIndicator && updateSDTCash(bilCashDspUpdate)) {
                        suspendObject = new Object[] { true, true, dueDateHold, vendorPayment };
                        bilCashDspRepository.saveAndFlush(bilCashDspUpdate);
                        return suspendObject;
                    }
                    suspendCash = updateAdjustDueDate(bilCashDspUpdate);

                }

                bilCashDspRepository.saveAndFlush(bilCashDspUpdate);
            }

        }
        suspendObject = new Object[] { suspendCash, suspendSDTInd, dueDateHold, vendorPayment, distributionDate,
                distributionSequence };
        return suspendObject;
    }

    private boolean updateAdjustDueDate(BilCashDsp bilCashDsp) {

        if (bilCashDsp != null && bilCashDsp.getDspTypeCd().equalsIgnoreCase(BilDspTypeCode.SUSPENDED.getValue())) {
            bilCashDsp.setAdjustmentDueDate(DateRoutine.defaultDateTime());

            return true;
        }
        return false;
    }

    private boolean updateSDTCash(BilCashDsp bilCashDsp) {
        if (bilCashDsp != null && !bilCashDsp.getStatementDetailTypeCd().trim().isEmpty()
                && bilCashDsp.getDspTypeCd().equalsIgnoreCase(BilDspTypeCode.SUSPENDED.getValue())) {
            bilCashDsp.setStatementDetailTypeCd(BLANK_STRING);
            return true;
        }
        return false;
    }

    private void updateBilPolicyStatus(String accountId, String policyNumber, char statusUpdate, char status) {
        bilPolicyTermRepository.updatePolicyCancelForRewrite(statusUpdate, accountId, status, policyNumber);
    }

    private boolean checkVendorPayment(String receiptTypeCd) {
        boolean vendorPayment = false;
        BusCdTranslation busCdTranslation = busCdTranslationRepository
                .findById(new BusCdTranslationId(receiptTypeCd, "COL", language, BLANK_STRING))
                .orElse(null);
        if (busCdTranslation != null) {
            vendorPayment = true;
        }
        return vendorPayment;
    }

    private void evaluateRevAutoWriteOff(CashApplyDriver cashApplyDriver, BilCashDsp bilCashDsp, BilAccount bilAccount,
            Double totalCashAmount) {
        List<String> policyIdList = bilPolicyRepository.findPolicyData(cashApplyDriver.getAccountId(),
                bilCashDsp.getPolicyNumber(), bilCashDsp.getPolicySymbolCd(), getIssueIndList(), SEPARATOR_BLANK);
        if (policyIdList != null && !policyIdList.isEmpty() && policyIdList.size() == SHORT_ONE) {
            checkRevAutWriteOffDuePayment(cashApplyDriver, bilAccount, totalCashAmount, policyIdList.get(SHORT_ZERO));
        }
    }

    private Object[] processCreditCashFirst(CashApplyDriver cashApplyDriver, BilAccount bilAccount,
            BilSupportPlan bilSupportPlan, BilActRules bilActRules, Double totalCashAmount, BilRulesUct rpauRules,
            boolean policyTransferCash, ZonedDateTime distributionDate, short distributionSequence) {
        String accountId = cashApplyDriver.getAccountId();
        boolean vendorPayment = false;
        List<BilCashReceipt> bilCashReceiptList = bilCashReceiptRepository.getCreditCommissionCash(accountId,
                Arrays.asList(BilReceiptTypeCode.CREDIT_CASH.getValue(),
                        BilReceiptTypeCode.COMMISSION_PAYMENT_CASH.getValue()));
        for (BilCashReceipt bilCashReceipt : bilCashReceiptList) {
            BilCashDsp bilCashDsp = bilCashDspRepository.fetchCreditCashFirst(
                    bilCashReceipt.getBilCashReceiptId().getAccountId(),
                    bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                    bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), BilDspTypeCode.SUSPENDED.getValue(),
                    ReverseReSuspendIndicator.BLANK.getValue(), ManualSuspendIndicator.ENABLED.getValue());
            if (bilCashDsp != null) {
                distributionDate = bilCashDsp.getBilCashDspId().getBilDtbDate();
                distributionSequence = bilCashDsp.getBilCashDspId().getDtbSequenceNbr();
                boolean overrideOverPayment = false;
                processCashApply(cashApplyDriver, bilAccount, bilCashDsp, bilSupportPlan, bilActRules, totalCashAmount,
                        vendorPayment, bilCashReceipt.getReceiptTypeCd(), rpauRules, DECIMAL_ZERO, false,
                        overrideOverPayment, policyTransferCash);

                BilCashDsp bilCashDspUpdate = bilCashDspRepository.findById(bilCashDsp.getBilCashDspId()).orElse(null);
                if (bilCashDspUpdate != null
                        && bilCashDspUpdate.getDspTypeCd().equalsIgnoreCase(BilDspTypeCode.SUSPENDED.getValue())
                        && (bilCashDspUpdate.getRevsRsusIndicator() == ReverseReSuspendIndicator.BLANK.getValue()
                                || bilCashDspUpdate
                                        .getRevsRsusIndicator() == ReverseReSuspendIndicator.RECONCILIATION_NOT_COMPLETE
                                                .getValue())) {
                    if (!bilCashDspUpdate.getPayableItemCd().trim().isEmpty()) {
                        bilCashDspUpdate.setPayableItemCd(BLANK_STRING);
                        bilCashDspRepository.saveAndFlush(bilCashDspUpdate);
                    }
                    distributionDate = bilCashDspUpdate.getBilCashDspId().getBilDtbDate();
                    distributionSequence = bilCashDspUpdate.getBilCashDspId().getDtbSequenceNbr();
                    processCashApply(cashApplyDriver, bilAccount, bilCashDspUpdate, bilSupportPlan, bilActRules,
                            totalCashAmount, vendorPayment, bilCashReceipt.getReceiptTypeCd(), rpauRules, DECIMAL_ZERO,
                            false, overrideOverPayment, policyTransferCash);
                }
            }
        }
        return new Object[] { distributionDate, distributionSequence };
    }

    private void processCashApply(CashApplyDriver cashApplyDriver, BilAccount bilAccount, BilCashDsp bilCashDsp,
            BilSupportPlan bilSupportPlan, BilActRules bilActRules, Double totalCashAmount, boolean vendorPayment,
            String receiptTypeCd, BilRulesUct rpauRules, Double balanceAmountDue, boolean firstTimeCall,
            boolean overrideOverPayment, boolean policyTransferCash) {
        if (!bilCashDsp.getPolicyNumber().trim().isEmpty()
                && bilCashDsp.getCreditIndicator() != CreditIndicator.CANCELLATION_FOR_REWRITE.getValue()) {
            processBilPolicyTrm(cashApplyDriver, bilAccount, bilCashDsp, bilSupportPlan, bilActRules, totalCashAmount,
                    vendorPayment, receiptTypeCd, rpauRules, balanceAmountDue, firstTimeCall, overrideOverPayment,
                    policyTransferCash);
        } else {
            prepareCashApply(cashApplyDriver, bilAccount, bilCashDsp, bilSupportPlan, bilActRules, totalCashAmount,
                    vendorPayment, receiptTypeCd, null, rpauRules, balanceAmountDue, firstTimeCall, overrideOverPayment,
                    policyTransferCash);
        }
    }

    private void prepareCashApply(CashApplyDriver cashApplyDriver, BilAccount bilAccount, BilCashDsp bilCashDsp,
            BilSupportPlan bilSupportPlan, BilActRules bilActRules, Double totalCashAmount, boolean vendorPayment,
            String receiptTypeCode, BilPolicyTerm bilPolicyTerm, BilRulesUct rpauRules, Double balanceAmountDue,
            boolean firstTimeCall, boolean overrideOverPayment, boolean policyTransferCash) {
        Integer overRidePriority = null;
        String accountId = cashApplyDriver.getAccountId();
        String cashPolicId = BLANK_STRING;
        ZonedDateTime cashPolEffDate = DateRoutine.defaultDateTime();
        char cashOverInd = bilSupportPlan.getBspCshOvpInd();
        char payPremium1stInd = BLANK_CHAR;

        boolean paymentPremiumPriority = false;
        final char PREMIUM = CHAR_P;
        if (bilSupportPlan.getBspPmtPtyInd() == PREMIUM) {
            paymentPremiumPriority = true;
        }

        if (!paymentPremiumPriority && bilSupportPlan.getBspCshOvpInd() == CashOverPayIndicators.SUSPEND.getValue()) {
            overRidePriority = bilIstScheduleRepository.checkOverRidePenalty(accountId, BLANK_CHAR,
                    cashApplyDriver.getCashApplyDate());
            if (overRidePriority == null || overRidePriority == SHORT_ZERO) {
                List<Double> lastQualServiceChargeAmounts = new ArrayList<>();
                List<Double> downpaymentAmounts = new ArrayList<>();
                List<ZonedDateTime> fullpayDatesArrays = new ArrayList<>();
                boolean fullPayment = checkFullPayServiceWriteOff(bilActRules, totalCashAmount, accountId, bilAccount,
                        lastQualServiceChargeAmounts, downpaymentAmounts, fullpayDatesArrays);
                if (fullPayment || !downpaymentAmounts.isEmpty()) {
                    paymentPremiumPriority = true;
                    if (!downpaymentAmounts.isEmpty() || lastQualServiceChargeAmounts.get(2) > DECIMAL_ZERO) {
                        payPremium1stInd = CHAR_D;
                    } else {
                        payPremium1stInd = CHAR_S;
                    }
                }
            }
        }

        if (vendorPayment && bilCashDsp.getPolicyNumber().trim().isEmpty() && !paymentPremiumPriority) {
            paymentPremiumPriority = false;
        }
        if (receiptTypeCode.equalsIgnoreCase("GA") || isCashApplyOrDownPayment(receiptTypeCode)) {
            cashOverInd = CashOverPayIndicators.SATISFY_OLDEST_DUE_FIRST.getValue();
        }
        char bilSpreadInd = BLANK_CHAR;
        if (!bilCashDsp.getPolicyNumber().trim().isEmpty()
                && bilCashDsp.getCreditIndicator() == CreditIndicator.CANCELLATION_FOR_REWRITE.getValue()) {
            cashPolEffDate = DateRoutine.dateTimeAsYYYYMMDD(0);
            List<BilPolicy> bilPolicies = bilPolicyRepository.findBilPolicyId(accountId, bilCashDsp.getPolicyNumber(),
                    bilCashDsp.getPolicySymbolCd(),
                    Arrays.asList(PolicyIssueIndicators.CANCELLED_REWRITE.getValue(),
                            PolicyIssueIndicators.CANCELLED_REISSUE.getValue(), PolicyIssueIndicators.VOID.getValue()),
                    BLANK_STRING);
            if (bilPolicies == null || bilPolicies.isEmpty()) {
                return;
            }
            String policyId = bilPolicies.get(0).getBillPolicyId().getPolicyId();
            List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.getPolicyByMaxPolEffectiveDate(accountId,
                    policyId,
                    Arrays.asList(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                            BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                            BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(),
                            BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue()));
            if (bilPolicyTermList == null || bilPolicyTermList.isEmpty()) {
                return;
            } else {
                BilPolicyTerm creditTerm = bilPolicyTermList.get(SHORT_ZERO);

                BilBillPlan bilBillPlan = bilBillPlanRepository.findById(creditTerm.getBillPlanCd()).orElse(null);
                if (bilBillPlan != null) {
                    bilSpreadInd = bilBillPlan.getBbpSpreadInd();
                }
            }

            if (bilCashDsp.getInvoiceDate().compareTo(DateRoutine.defaultDateTime()) != 0) {
                if (cashOverInd == CashOverPayIndicators.SPREAD_OVER_INSTALLMENTS.getValue()
                        || cashOverInd == CashOverPayIndicators.SUSPEND.getValue()) {
                    if (bilSpreadInd == CHAR_N) {
                        cashOverInd = CashOverPayIndicators.SATISFY_OLDEST_DUE_FIRST.getValue();
                    } else if (bilSpreadInd == CHAR_Y) {
                        cashOverInd = CashOverPayIndicators.SPREAD_OVER_INSTALLMENTS.getValue();
                    }
                } else if (cashOverInd == CashOverPayIndicators.SATISFY_OLDEST_DUE_FIRST.getValue()
                        && bilSpreadInd == CHAR_Y) {
                    cashOverInd = CashOverPayIndicators.SPREAD_OVER_INSTALLMENTS.getValue();
                }
            }
        }

        if (bilSupportPlan.getBspCshOvpInd() == CashOverPayIndicators.SPREAD_OVER_INSTALLMENTS.getValue()
                || bilSupportPlan.getBspCshOvpInd() == CashOverPayIndicators.SATISFY_OLDEST_DUE_FIRST.getValue()) {
            List<BilPolActivity> bilPolActivities = bilPolActivityRepository.fetchBilPolActivityTriger(accountId,
                    "RPC");
            if (bilPolActivities != null && !bilPolActivities.isEmpty()) {
                cashOverInd = CashOverPayIndicators.SUSPEND.getValue();
            }
        }
        if (policyTransferCash) {
            cashOverInd = CashOverPayIndicators.SATISFY_OLDEST_DUE_FIRST.getValue();
        }

        if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())) {
            cashPolicId = BLANK_STRING;
            cashPolEffDate = null;
        } else {
            if (bilPolicyTerm != null) {
                cashPolicId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
                cashPolEffDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
            }

        }
        if (rpauRules != null && rpauRules.getBrtRuleCd() == CHAR_Y && bilCashDsp.getCreditIndicator() == CHAR_H) {
            if (firstTimeCall && balanceAmountDue != null && balanceAmountDue == DECIMAL_ZERO) {
                String itemCode = BLANK_STRING;
                if (rpauRules.getBrtParmListTxt() != null && !rpauRules.getBrtParmListTxt().isEmpty()
                        && rpauRules.getBrtParmListTxt().length() >= 27) {
                    itemCode = rpauRules.getBrtParmListTxt().substring(27);
                }
                updatePayableItemCode(itemCode, bilCashDsp.getBilCashDspId());
            } else if (overrideOverPayment) {
                cashOverInd = CashOverPayIndicators.SUSPEND.getValue();
            }
        }

        processCashApplyService(cashApplyDriver, cashPolicId, cashPolEffDate, cashOverInd, payPremium1stInd,
                paymentPremiumPriority, bilCashDsp, bilActRules);
    }

    private void processCashApplyService(CashApplyDriver cashApplyDriver, String cashPolicId,
            ZonedDateTime cashPolEffDate, char cashOverInd, char payPremium1stInd, boolean paymentPremiumPriority,
            BilCashDsp bilCashDsp, BilActRules bilActRules) {
        CashApply cashApply = new CashApply();
        cashApply.setAccountId(cashApplyDriver.getAccountId());
        cashApply.setPolicyId(cashPolicId);
        cashApply.setCashOverpaymentInd(cashOverInd);
        cashApply.setQuoteIndicator(cashApplyDriver.getQuoteIndicator());
        cashApply.setPremiumPayIndicator(payPremium1stInd);
        cashApply.setPaymentPriorityInd(paymentPremiumPriority ? CHAR_P : CHAR_S);
        cashApply.setCashApplyDate(cashApplyDriver.getCashApplyDate());
        cashApply.setUserSequenceId(cashApplyDriver.getUserSequenceId());
        if (bilActRules.getBruRnlQteInd() == CHAR_Y) {
            cashApply.setCreditPolicyId(bilCashDsp.getCreditPolicyId());
        }
        if (cashPolEffDate != null) {
            cashApply.setPolicyEffectiveDate(cashPolEffDate);
        }

        if (bilCashDsp != null) {
            cashApply.setBilDtbSequenceNumber(bilCashDsp.getBilCashDspId().getDtbSequenceNbr());
            cashApply.setBilDspSequenceNumber(bilCashDsp.getBilCashDspId().getDspSequenceNbr());
            cashApply.setBilDtbDate(bilCashDsp.getBilCashDspId().getBilDtbDate());
        }

        cashApplyService.cashApply(cashApply, false);
    }

    private void updatePayableItemCode(String itemCode, BilCashDspId bilCashDspId) {
        BilCashDsp bilCashDsp = bilCashDspRepository.findById(bilCashDspId).orElse(null);
        if (bilCashDsp != null) {
            bilCashDsp.setPayableItemCd(itemCode);
            bilCashDspRepository.save(bilCashDsp);
        }
    }

    @Override
    public boolean checkFullPayServiceWriteOff(BilActRules bilActRules, Double totalCashAmount, String accountId,
            BilAccount bilAccount, List<Double> lastQualServiceChargeAmounts, List<Double> downpaymentAmounts,
            List<ZonedDateTime> fullpayDatesArrays) {
        boolean fullPayment = false;

        if (bilActRules.getBruFlpScgWroCd() == FullPayServiceChargeWriteOff.ACTUAL_INVOICE.getValue()
                || bilActRules.getBruFlpScgWroCd() == FullPayServiceChargeWriteOff.SCHEDULE_INVOICE.getValue()
                || bilActRules.getBruFlpScgWroCd() == FullPayServiceChargeWriteOff.USER_EXIT.getValue()) {
            fullPayment = processFullPayTest(accountId, totalCashAmount, fullpayDatesArrays,
                    lastQualServiceChargeAmounts, downpaymentAmounts);
        }

        if ((fullPayment) && (bilActRules.getBruFlpScgWroCd() == FullPayServiceChargeWriteOff.ACTUAL_INVOICE.getValue()
                || bilActRules.getBruFlpScgWroCd() == FullPayServiceChargeWriteOff.SCHEDULE_INVOICE.getValue())) {
            fullPayment = determineServiceChargeWriteOff(accountId, fullpayDatesArrays, bilActRules.getBruFlpScgWroCd(),
                    fullPayment);
            if (!fullPayment) {
                lastQualServiceChargeAmounts.add(0, DECIMAL_ZERO);
                lastQualServiceChargeAmounts.add(2, DECIMAL_ZERO);
            }
        }

        return fullPayment;
    }

    private boolean determineServiceChargeWriteOff(String accountId, List<ZonedDateTime> fullpayDatesArrays,
            char fullpayServiceWriteOffCode, boolean fullPayment) {
        ZonedDateTime fullpayInvoiceDate = fullpayDatesArrays.get(0);
        List<Object[]> schedulePoliciesList = bilIstScheduleRepository.fetchDistinctSchedulePolicy(accountId,
                fullpayInvoiceDate);
        boolean evaluateTerm = true;
        if (schedulePoliciesList != null && !schedulePoliciesList.isEmpty()) {
            for (Object[] schedulePolicies : schedulePoliciesList) {
                String policyId = (String) schedulePolicies[0];
                ZonedDateTime polEffectiveDate = (ZonedDateTime) schedulePolicies[1];
                BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository
                        .findById(new BilPolicyTermId(accountId, policyId, polEffectiveDate)).orElse(null);
                if (bilPolicyTerm != null) {
                    char polStatus = bilPolicyTerm.getBillPolStatusCd();
                    short belowMinCount = bilPolicyTerm.getBptBelowMinCnt();
                    if (polStatus == BilPolicyStatusCode.TRANSFERRED.getValue()) {
                        throw new InvalidDataException("status.is.transferred.payment.not.apply",
                                new Object[] { policyId });
                    }
                    ZonedDateTime workInvocieDate = null;
                    if (polStatus == BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue()
                            || polStatus == BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue()
                            || polStatus == BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue()) {
                        Integer workInvoiceSchedule = bilIstScheduleRepository.checkWorkInvoiceDate(accountId, policyId,
                                polEffectiveDate, fullpayInvoiceDate,
                                InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue());
                        if (workInvoiceSchedule != null && workInvoiceSchedule > 0) {
                            evaluateTerm = checkBilInvoicePolicy(accountId, policyId, fullpayInvoiceDate,
                                    polEffectiveDate, evaluateTerm);
                            workInvocieDate = fullpayInvoiceDate;
                        } else {
                            continue;
                        }
                    } else {
                        Integer isMoreTerms = bilIstScheduleRepository.checkMoreTerms(accountId, policyId,
                                polEffectiveDate, fullpayInvoiceDate);
                        if (isMoreTerms == null || isMoreTerms == 0) {
                            evaluateTerm = checkBilInvoicePolicy(accountId, policyId, fullpayInvoiceDate,
                                    polEffectiveDate, evaluateTerm);
                        }
                    }
                    short invoiceCounts = 0;
                    if (evaluateTerm) {

                        if (fullpayServiceWriteOffCode == FullPayServiceChargeWriteOff.SCHEDULE_INVOICE.getValue()
                                && belowMinCount > 1) {
                            fullPayment = false;
                            break;
                        }

                        List<ZonedDateTime> workInvoiceDateList = bilIstScheduleRepository.getWorkInvoiceDate(accountId,
                                policyId, polEffectiveDate, CHAR_Y);
                        if (workInvoiceDateList != null && !workInvoiceDateList.isEmpty()) {
                            workInvocieDate = workInvoiceDateList.get(0);
                            if (workInvoiceDateList.size() > 1) {
                                invoiceCounts = (short) workInvoiceDateList.size();
                            } else {
                                invoiceCounts = 1;
                            }
                        }
                        if (workInvocieDate != null && invoiceCounts == 1
                                && workInvocieDate.compareTo(fullpayInvoiceDate) != 0) {
                            fullPayment = false;
                        }

                        if (fullpayServiceWriteOffCode == FullPayServiceChargeWriteOff.ACTUAL_INVOICE.getValue()
                                && invoiceCounts > 1) {
                            fullPayment = false;
                            break;
                        }
                        if (fullpayServiceWriteOffCode == FullPayServiceChargeWriteOff.SCHEDULE_INVOICE.getValue()
                                && invoiceCounts + belowMinCount > 1) {
                            fullPayment = false;
                            break;
                        }
                    }

                }
            }
        }

        return fullPayment;
    }

    private boolean checkBilInvoicePolicy(String accountId, String policyId, ZonedDateTime fullpayInvoiceDate,
            ZonedDateTime polEffectiveDate, boolean evaluateTerm) {

        BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(policyId, accountId)).orElse(null);
        if (bilPolicy != null) {
            Integer termInIncoice = bilInvPolRepository.checkTermParticipatedInvoice(accountId, CHAR_A, accountId,
                    fullpayInvoiceDate, bilPolicy.getPolSymbolCd(), bilPolicy.getPolNbr(), polEffectiveDate,
                    DECIMAL_ZERO);
            if (termInIncoice != null && termInIncoice > 0) {
                evaluateTerm = true;
            }
        }
        return evaluateTerm;
    }

    private boolean processFullPayTest(String accountId, Double totalCashAmount, List<ZonedDateTime> fullpayDatesArrays,
            List<Double> lastQualServiceChargeAmounts, List<Double> downpaymentAmounts) {

        Double totalPremiumAmount;
        Double totalPaidAmount;
        Double totalChargeAmount = DECIMAL_ZERO;
        Double totalChargePaidAmount = DECIMAL_ZERO;
        Double totalServiceCharge = DECIMAL_ZERO;
        Double totalServiceChargePaid = DECIMAL_ZERO;

        Boolean fullPayment = getLastQualServiceCharge(accountId, downpaymentAmounts, lastQualServiceChargeAmounts,
                fullpayDatesArrays);

        if (fullPayment != null && !fullPayment && downpaymentAmounts.isEmpty()) {
            return fullPayment;
        }
        List<Object[]> fullpaymentTotalAmounts = bilIstScheduleRepository.getFullpayTotalAmounts(accountId);
        if (fullpaymentTotalAmounts != null && !fullpaymentTotalAmounts.isEmpty()
                && fullpaymentTotalAmounts.get(0)[0] != null && fullpaymentTotalAmounts.get(0)[1] != null) {
            totalPremiumAmount = (Double) fullpaymentTotalAmounts.get(0)[0];
            totalPaidAmount = (Double) fullpaymentTotalAmounts.get(0)[1];

            List<Object[]> fullpayChargeAmounts = bilCrgAmountsRepository.getFullpayChargeAmounts(accountId,
                    Arrays.asList(ChargeTypeCodes.PENALTY_CHARGE.getValue(), ChargeTypeCodes.LATE_CHARGE.getValue(),
                            ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()));
            if (fullpayChargeAmounts != null && !fullpayChargeAmounts.isEmpty()
                    && fullpayChargeAmounts.get(0)[0] != null && fullpayChargeAmounts.get(0)[1] != null) {
                totalChargeAmount = (Double) fullpayChargeAmounts.get(0)[0];
                totalChargePaidAmount = (Double) fullpayChargeAmounts.get(0)[1];
            }
            if (downpaymentAmounts != null && !downpaymentAmounts.isEmpty()) {
                totalChargeAmount = totalChargeAmount - downpaymentAmounts.get(0) - downpaymentAmounts.get(1)
                        - downpaymentAmounts.get(2);
            }
            List<Object[]> fullpayServiceChargeAmounts = bilCrgAmountsRepository.getFullpayServiceChargeAmounts(
                    accountId, ChargeTypeCodes.SERVICE_CHARGE.getValue(),
                    InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue());
            if (fullpayServiceChargeAmounts != null && !fullpayServiceChargeAmounts.isEmpty()
                    && fullpayServiceChargeAmounts.get(0)[0] != null && fullpayServiceChargeAmounts.get(0)[1] != null) {
                totalServiceCharge = (Double) fullpayServiceChargeAmounts.get(0)[0];
                totalServiceChargePaid = (Double) fullpayServiceChargeAmounts.get(0)[1];
            }
            totalPaidAmount = totalPaidAmount + totalChargePaidAmount + totalServiceChargePaid;

            if (lastQualServiceChargeAmounts != null && !lastQualServiceChargeAmounts.isEmpty()) {
                Double lastInvScgAmt = lastQualServiceChargeAmounts.get(1);
                Double lastInvDpfAmt = lastQualServiceChargeAmounts.get(3);

                if (lastInvDpfAmt != null && lastInvDpfAmt > 0) {
                    totalPaidAmount = totalPaidAmount + lastQualServiceChargeAmounts.get(0);
                } else if (lastInvScgAmt != null && lastInvScgAmt > 0 && lastInvDpfAmt == 0) {
                    totalPaidAmount = totalPaidAmount + lastQualServiceChargeAmounts.get(2);
                }
            }

            totalPremiumAmount = totalPremiumAmount + totalServiceCharge + totalChargeAmount;
            if (fullPayment != null && !fullPayment && downpaymentAmounts != null && !downpaymentAmounts.isEmpty()) {
                if (totalCashAmount > 0 && totalCashAmount >= totalPremiumAmount - totalPaidAmount) {
                    return fullPayment;
                } else {
                    downpaymentAmounts.clear();
                    return fullPayment;
                }

            }
            if (totalCashAmount > 0 && totalCashAmount >= totalPremiumAmount - totalPaidAmount
                    || totalPremiumAmount <= totalPaidAmount) {
                fullPayment = true;
            } else {
                fullPayment = false;
            }
        } else {
            fullPayment = false;
        }
        return fullPayment;
    }

    private Boolean getLastQualServiceCharge(String accountId, List<Double> downPaymentAmounts,
            List<Double> lastQualServiceChargeAmounts, List<ZonedDateTime> fullpayDatesArrays) {
        Boolean fullPayment = null;
        Double fmsScgwos = DECIMAL_ZERO;
        Double lastInvScgAmt = DECIMAL_ZERO;
        Double fmsDpfwos = DECIMAL_ZERO;
        Double lastInvDpfAmt = DECIMAL_ZERO;
        ZonedDateTime fullpayInvoiceDate = null;
        ZonedDateTime fullpayAdjustDate = null;
        List<Object[]> maxDates = bilDatesRepository.getMaxDate(accountId,
                InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue());
        if (maxDates != null) {
            fullpayInvoiceDate = (ZonedDateTime) maxDates.get(SHORT_ZERO)[0];
            fullpayAdjustDate = (ZonedDateTime) maxDates.get(SHORT_ZERO)[1];
        }

        if (fullpayInvoiceDate != null && fullpayAdjustDate != null) {
            fullpayDatesArrays.add(0, fullpayInvoiceDate);
            fullpayDatesArrays.add(1, fullpayAdjustDate);

            List<Object[]> fullpayChargeAmounts = bilCrgAmountsRepository.getFullPaymentAmounts(accountId,
                    fullpayInvoiceDate, getInvoiceCodeList(), Arrays.asList(ChargeTypeCodes.SERVICE_CHARGE.getValue(),
                            ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()));
            if (fullpayChargeAmounts != null && !fullpayChargeAmounts.isEmpty()) {
                fmsScgwos = (Double) fullpayChargeAmounts.get(0)[0] == null ? DECIMAL_ZERO
                        : (Double) fullpayChargeAmounts.get(0)[0];
                lastInvScgAmt = (Double) fullpayChargeAmounts.get(0)[1] == null ? DECIMAL_ZERO
                        : (Double) fullpayChargeAmounts.get(0)[1];
                fmsDpfwos = (Double) fullpayChargeAmounts.get(0)[2] == null ? DECIMAL_ZERO
                        : (Double) fullpayChargeAmounts.get(0)[2];
                lastInvDpfAmt = (Double) fullpayChargeAmounts.get(0)[3] == null ? DECIMAL_ZERO
                        : (Double) fullpayChargeAmounts.get(0)[3];
                if (lastInvScgAmt != null && lastInvScgAmt <= DECIMAL_ZERO) {
                    fmsScgwos = DECIMAL_ZERO;
                }
                if (lastInvDpfAmt != null && lastInvDpfAmt <= DECIMAL_ZERO
                        || lastInvScgAmt != null && lastInvScgAmt > DECIMAL_ZERO) {
                    fmsDpfwos = DECIMAL_ZERO;
                }

            } else {
                fullPayment = false;
            }

            lastQualServiceChargeAmounts.add(0, fmsScgwos);
            lastQualServiceChargeAmounts.add(1, lastInvScgAmt);
            lastQualServiceChargeAmounts.add(2, fmsDpfwos);
            lastQualServiceChargeAmounts.add(3, lastInvDpfAmt);
            if (fmsScgwos == DECIMAL_ZERO && fmsDpfwos == DECIMAL_ZERO) {
                fullPayment = false;
                return fullPayment;
            }
            BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("FPWX", BLANK_STRING, BLANK_STRING,
                    BLANK_STRING);
            if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y
                    && bilRulesUct.getExpirationDt() == DateRoutine.defaultDateTime()) {
                ZonedDateTime fpwxEffectiveDate = bilRulesUct.getBilRulesUctId().getEffectiveDt();
                if (fullpayInvoiceDate.compareTo(fpwxEffectiveDate) < 0
                        && fullpayAdjustDate.compareTo(fpwxEffectiveDate) < 0) {
                    fullPayment = false;
                }
            }
        } else {
            List<Double> paymentAmounts = findDownpayment(accountId);
            if (downPaymentAmounts == null || !downPaymentAmounts.isEmpty()) {
                downPaymentAmounts = new ArrayList<>();
            }
            downPaymentAmounts.addAll(paymentAmounts);
            fullPayment = false;
        }

        return fullPayment;
    }

    private List<Double> findDownpayment(String accountId) {
        List<Double> downpaymentAmounts = new ArrayList<>();
        ZonedDateTime minAdjustDueDate = bilDatesRepository.getMinAdjustDueDate(accountId,
                Arrays.asList(BLANK_CHAR, InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue()));
        if (minAdjustDueDate != null) {
            List<Object[]> downpaymentAmountsList = bilCrgAmountsRepository.getDownpaymentAmounts(accountId,
                    minAdjustDueDate, ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue(),
                    Arrays.asList(BLANK_CHAR, InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue()));
            if (downpaymentAmountsList != null && !downpaymentAmountsList.isEmpty()) {
                if (downpaymentAmountsList.get(0)[0] != null) {
                    downpaymentAmounts.add(0, (Double) downpaymentAmountsList.get(0)[0]);
                }

                if (downpaymentAmountsList.get(0)[1] != null) {
                    downpaymentAmounts.add(1, (Double) downpaymentAmountsList.get(0)[1]);
                }

                if (downpaymentAmountsList.get(0)[2] != null) {
                    downpaymentAmounts.add(2, (Double) downpaymentAmountsList.get(0)[2]);
                }

            }
        }
        return downpaymentAmounts;
    }

    private void processBilPolicyTrm(CashApplyDriver cashApplyDriver, BilAccount bilAccount, BilCashDsp bilCashDsp,
            BilSupportPlan bilSupportPlan, BilActRules bilActRules, Double totalCashAmount, boolean vendorPayment,
            String receiptTypeCode, BilRulesUct rpauRules, Double balanceAmountDue, boolean firstTimeCall,
            boolean overrideOverPayment, boolean policyTransferCash) {
        List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.findBilPolicyByPolicyStatus(
                cashApplyDriver.getAccountId(), Arrays.asList(BilPolicyStatusCode.TRANSFERRED.getValue(),
                        BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue()));
        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {

            BilPolicy bilPolicy = bilPolicyRepository.findPolicyByPolicyNumber(
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getBilAccountId(), bilCashDsp.getPolicyNumber(),
                    bilCashDsp.getPolicySymbolCd(), BLANK_STRING);
            if (bilPolicy != null) {
                Double balanceAmount = bilIstScheduleRepository.calculateBalanceAmount(
                        bilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                if (balanceAmount != null && balanceAmount > 0) {
                    prepareCashApply(cashApplyDriver, bilAccount, bilCashDsp, bilSupportPlan, bilActRules,
                            totalCashAmount, vendorPayment, receiptTypeCode, bilPolicyTerm, rpauRules, balanceAmountDue,
                            firstTimeCall, overrideOverPayment, policyTransferCash);
                }
            }
        }

    }

    private char fetchApcrRule(String accountId, String billTypeCd, String billClassCd) {
        String policyNumber;
        String creditPolicyId;
        String policySymbol;
        String bilPlanCode = BLANK_STRING;
        char apcrRulesCode = BLANK_CHAR;

        List<Object[]> maxPolicyData = bilCashDspRepository.fetchMaxPolicyData(accountId,
                BilDspTypeCode.SUSPENDED.getValue(), BLANK_CHAR, CHAR_Y,
                Arrays.asList(BilReceiptTypeCode.CREDIT_CASH.getValue(),
                        BilReceiptTypeCode.COMMISSION_PAYMENT_CASH.getValue()));
        if (maxPolicyData != null && !maxPolicyData.isEmpty()) {
            policyNumber = (String) maxPolicyData.get(0)[0];
            creditPolicyId = (String) maxPolicyData.get(0)[1];
            policySymbol = bilCashDspRepository.fetchMaxPolicySymbolCode(accountId, BilDspTypeCode.SUSPENDED.getValue(),
                    BLANK_CHAR, CHAR_Y, policyNumber);

            if (policyNumber != null && !policyNumber.trim().isEmpty()) {
                List<String> bilPlanCodeList = bilPolicyRepository.findBilPlanCode(accountId, policyNumber,
                        policySymbol, getIssueIndList(), BLANK_STRING);
                if (bilPlanCodeList != null && !bilPlanCodeList.isEmpty()) {
                    bilPlanCode = bilPlanCodeList.get(0);
                }
            } else {
                BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(creditPolicyId, accountId))
                        .orElse(null);
                if (bilPolicy != null) {
                    bilPlanCode = bilPolicy.getBilPlanCd();
                }
            }

            apcrRulesCode = readAPCRRule(billTypeCd, billClassCd, bilPlanCode);
        }

        return apcrRulesCode;
    }

    private char readAPCRRule(String billTypeCd, String billClassCd, String bilPlanCode) {
        char apcrRulesCode = BLANK_CHAR;
        BilRulesUct bilRulesUct = null;

        bilRulesUct = bilRulesUctService.readBilRulesUct("APCR", billTypeCd, billClassCd, bilPlanCode);
        if (bilRulesUct == null) {
            bilRulesUct = bilRulesUctService.readBilRulesUct("APCR", billTypeCd, billClassCd, SEPARATOR_BLANK);
        }
        if (bilRulesUct != null) {
            apcrRulesCode = bilRulesUct.getBrtRuleCd();
        }
        return apcrRulesCode;
    }

    private boolean applyDownpaymentCash(CashApplyDriver cashApplyDriver, List<BilCashDsp> downPaymentList) {

        boolean noDownPaymentToPay = false;
        for (BilCashDsp bilCashDsp : downPaymentList) {
            boolean isDownPaymentRowsExist = downPaymentCashApplyServiceImpl.cashApply(bilCashDsp,
                    cashApplyDriver.getQuoteIndicator(), cashApplyDriver.getCashApplyDate(),
                    cashApplyDriver.getUserSequenceId());
            if (isDownPaymentRowsExist) {
                noDownPaymentToPay = true;
            }
        }

        return noDownPaymentToPay;
    }

    private void checkForLateChargeWriteOff(CashApplyDriver cashApplyDriver, BilAccount bilAccount) {
        String accountId = cashApplyDriver.getAccountId();
        BilActInquiry bilActInquiry = bilActInquiryRepository.findById(new BilActInquiryId(accountId, "LFA"))
                .orElse(null);
        if (bilActInquiry != null) {
            List<BilCashReceipt> bilCashReceiptList = bilCashReceiptRepository.getAvailableCashReceipt(accountId,
                    bilActInquiry.getBilNxtAcyDt(), bilActInquiry.getBilNxtAcyDt());
            if (bilCashReceiptList != null && !bilCashReceiptList.isEmpty()) {
                for (BilCashReceipt bilCashReceipt : bilCashReceiptList) {
                    Double dispostionAmount = bilCashDspRepository.fetchAvailableAmountDsp(accountId,
                            bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                            bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(),
                            BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                            ManualSuspendIndicator.ENABLED.getValue(), Arrays.asList(BLANK_CHAR, CHAR_N), BLANK_STRING);
                    if (dispostionAmount != null) {
                        Double outstandPremiumBalance = bilIstScheduleRepository.getOutstandPremiumBalance(accountId,
                                bilActInquiry.getBilNxtAcyDt(), getInvoiceCodeList());
                        if (outstandPremiumBalance == null) {
                            outstandPremiumBalance = DECIMAL_ZERO;
                        }
                        Double outstandChargeBalanceAmount = bilCrgAmountsRepository.getOutstandChargeBalanceAmount(
                                accountId,
                                Arrays.asList(
                                        InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                                        InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue(),
                                        InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()),
                                Arrays.asList(ChargeTypeCodes.PENALTY_CHARGE.getValue(),
                                        ChargeTypeCodes.LATE_CHARGE.getValue(),
                                        ChargeTypeCodes.SERVICE_CHARGE.getValue(),
                                        ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()),
                                bilActInquiry.getBilNxtAcyDt());
                        if (outstandChargeBalanceAmount == null) {
                            outstandChargeBalanceAmount = DECIMAL_ZERO;
                        }
                        double totalOutStanding = outstandPremiumBalance + outstandChargeBalanceAmount;
                        if (totalOutStanding <= dispostionAmount) {
                            ZonedDateTime bilAdjustDueDate = bilCrgAmountsRepository.getAdjustDueDate(accountId,
                                    ChargeTypeCodes.LATE_CHARGE.getValue());
                            if (bilAdjustDueDate != null) {
                                WriteOffFeeCharges writeOffFeeCharges = new WriteOffFeeCharges();
                                writeOffFeeCharges.setChargeTypeCode(ChargeTypeCodes.LATE_CHARGE.getValue());
                                writeOffFeeCharges.setDueDate(bilAdjustDueDate);
                                writeOffFeeCharges.setAmount(DECIMAL_ZERO);
                                writeOffFeeCharges.setReasonCode("PD");
                                writeOffFeeCharges.setReasonTypeCode("WPA");
                                writeOffFeeCharges.setQuoteIndicator(cashApplyDriver.getQuoteIndicator());
                                writeOffFeeCharges.setApplicationDate(cashApplyDriver.getCashApplyDate());
                                writeOffFeeChargesServiceImpl.processWriteOffFeeCharges(writeOffFeeCharges, bilAccount);
                                bilActInquiryRepository.delete(bilActInquiry);
                            }
                        }
                    }
                }
            }

        }
    }

    private Map<String, Object> sumCivAmounts(boolean civAmountsExist, String accountId, String billTypeCd,
            String billClassCd) {
        Map<String, Object> civCollectionMap = new HashMap<>();
        Double civChargeBalanceAmount = DECIMAL_ZERO;
        Double civChargePaidAmount = DECIMAL_ZERO;
        char correctedInvoice = BLANK_CHAR;
        List<BilInvPol> bilInvPolList = null;
        List<Object[]> civChargeAmounts = bilCrgAmountsRepository.getCivChargeAmounts(getInvoiceCodeList(), accountId);
        if (civChargeAmounts != null && !civChargeAmounts.isEmpty()) {
            civChargeBalanceAmount = civChargeAmounts.get(0)[0] != null ? (Double) civChargeAmounts.get(0)[0]
                    : DECIMAL_ZERO;
            civChargePaidAmount = civChargeAmounts.get(0)[0] != null ? (Double) civChargeAmounts.get(0)[1]
                    : DECIMAL_ZERO;
        }
        if (civAmountsExist) {
            ZonedDateTime civDueDate = bilDatesRepository.getMaxCivAdjDueDate(accountId, getInvoiceCodeList(),
                    DedudctionDate.EXCLUDE.getValue());
            if (civDueDate != null) {
                ZonedDateTime civInvoiceDate = bilDatesRepository.getMaxCivInvoiceDate(accountId, getInvoiceCodeList(),
                        civDueDate, DedudctionDate.EXCLUDE.getValue());
                if (civInvoiceDate != null) {
                    civAmountsExist = false;
                    civCollectionMap.put("civInvoiceDate", civInvoiceDate);
                }
                BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("ZINV", billTypeCd, billClassCd,
                        SEPARATOR_BLANK);

                if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                    bilInvPolList = bilInvPolRepository.fetchBilInvPol(accountId, CHAR_A, accountId, civInvoiceDate);
                } else {
                    bilInvPolList = bilInvPolRepository.fetchBilInvWithDueAmount(accountId, CHAR_A, accountId,
                            civInvoiceDate, DECIMAL_ZERO, DECIMAL_ZERO);
                }

                getCivPremiumAmounts(accountId, civCollectionMap, bilInvPolList, civInvoiceDate);
            } else {
                civAmountsExist = false;
                correctedInvoice = CHAR_N;
            }
        }
        civCollectionMap.put(CIV_CHARGE_BALANCE_AMOUNT, civChargeBalanceAmount);
        civCollectionMap.put(CIV_CHARGE_PAID_AMOUNT, civChargePaidAmount);
        civCollectionMap.put(CIV_AMOUNT_EXIST, civAmountsExist);
        civCollectionMap.put(CORRECTED_INVOICE, correctedInvoice);
        return civCollectionMap;
    }

    private void getCivPremiumAmounts(String accountId, Map<String, Object> civCollectionMap,
            List<BilInvPol> bilInvPolList, ZonedDateTime civInvoiceDate) {
        double civPremiumPaidAmount = DECIMAL_ZERO;
        double civPremiumBalanceAmount = DECIMAL_ZERO;
        for (BilInvPol bilInvPol : bilInvPolList) {
            List<BilPolicy> bilPolicyList = bilPolicyRepository.findCivPolicyId(bilInvPol.getPolNbr(),
                    bilInvPol.getPolSymbolCd(), accountId, getIssueIndList());
            if (bilPolicyList != null && !bilPolicyList.isEmpty()) {
                BilPolicy bilPolicy = bilPolicyList.get(SHORT_ZERO);
                Double civPolPaidAmount = DECIMAL_ZERO;
                Double civPolBalanceAmount = DECIMAL_ZERO;
                List<Object[]> civAmounts = null;
                if (bilInvPol.getBilPrevDueAmt() != 0) {
                    civAmounts = bilIstScheduleRepository.getCivAmounts(accountId,
                            bilPolicy.getBillPolicyId().getPolicyId(), bilInvPol.getPolEffectiveDt(),
                            getInvoiceCodeList());
                } else {
                    civAmounts = bilIstScheduleRepository.getCivAmountsWithInvoiceDate(accountId,
                            bilPolicy.getBillPolicyId().getPolicyId(), bilInvPol.getPolEffectiveDt(),
                            InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(), civInvoiceDate,
                            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());
                }
                if (civAmounts != null && !civAmounts.isEmpty()) {
                    civPolBalanceAmount = (Double) civAmounts.get(0)[0];
                    civPolPaidAmount = (Double) civAmounts.get(0)[1];
                }
                if (civPolBalanceAmount != null && civPolPaidAmount != null) {
                    civPremiumPaidAmount = civPolPaidAmount + civPremiumPaidAmount;
                    civPremiumBalanceAmount = civPolBalanceAmount + civPremiumBalanceAmount;
                }
            }
        }
        civCollectionMap.put(CIV_PREMIUM_PAID_AMOUNT, civPremiumPaidAmount);
        civCollectionMap.put(CIV_PREMIUM_BALANCE_AMOUNT, civPremiumBalanceAmount);
    }

    private boolean readAccountData(CashApplyDriver cashApplyDriver, BilAccount bilAccount, boolean eftCollectionMethod,
            BilActRules bilActRules) {
        String accountId = cashApplyDriver.getAccountId();
        boolean eftCorrectedInvoice = false;

        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("CICM", bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), SEPARATOR_BLANK);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            if (eftCollectionMethod) {
                List<BilEftPendingTape> bilEftPendingTapes = bilEftPendingTapeRepository.fetchEftDrAmount(accountId,
                        CHAR_A, Arrays.asList(EftRecordType.ORIGINAL.getValue()), BLANK_STRING);
                if (bilEftPendingTapes != null && !bilEftPendingTapes.isEmpty()) {
                    eftCorrectedInvoice = true;
                } else {
                    bilActRules.setBruCcrCreditInd(CHAR_N);
                    bilActRules.setBruCcrPartInd(CHAR_N);
                }
            } else if (!bilAccount.getCollectionPlan().trim().isEmpty()) {
                List<BilActInquiry> bilActInquiries = bilActInquiryRepository.fetchBilActInquiry(accountId,
                        Arrays.asList("ERP", "EEC", "EBC", "ECC"));
                if (bilActInquiries != null && !bilActInquiries.isEmpty()) {
                    eftCorrectedInvoice = true;
                }
            }
        } else {
            bilActRules.setBruCcrCreditInd(CHAR_N);
            bilActRules.setBruCcrPartInd(CHAR_N);
        }

        return eftCorrectedInvoice;
    }

    private void checkRevAutWriteOffDuePayment(CashApplyDriver cashApplyDriver, BilAccount bilAccount,
            Double totalCashAmount, String policyId) {

        List<BilPolicyTerm> bilPolicyTermList = null;
        String accountId = cashApplyDriver.getAccountId();
        char accountCollection = BLANK_CHAR;

        List<Character> bilIssueIndList = Arrays.asList(PolicyIssueIndicators.CANCELLED_REWRITE.getValue(),
                PolicyIssueIndicators.CANCELLED_REISSUE.getValue());

        List<Character> billPolStatusCdList = Arrays.asList(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(), BilPolicyStatusCode.TRANSFERRED.getValue(),
                BilPolicyStatusCode.PENDING_TRANSFER.getValue(),
                BilPolicyStatusCode.PENDING_TRANSFER_APPLYPAYMENTS.getValue(),
                BilPolicyStatusCode.PENDING_TRANSFER_HOLDPAYMENTS.getValue(), '4');

        if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())) {
            bilPolicyTermList = bilPolicyTermRepository.getWriteOffPolicyRows(accountId,
                    cashApplyDriver.getCashApplyDate(), billPolStatusCdList, bilIssueIndList);

            if (bilPolicyTermList == null || bilPolicyTermList.isEmpty()) {
                return;
            }
            policyId = bilPolicyTermList.get(SHORT_ZERO).getBillPolicyTermId().getPolicyId();
        } else {
            ZonedDateTime maxWriteOffEffDate = bilPolicyTermRepository.getMaxWriteOffEffectiveDate(accountId,
                    cashApplyDriver.getCashApplyDate(), billPolStatusCdList, bilIssueIndList);
            if (maxWriteOffEffDate == null) {
                return;
            }
            bilPolicyTermList = bilPolicyTermRepository.getCollectionCode(accountId, policyId, DECIMAL_ZERO);
            if (bilPolicyTermList == null || bilPolicyTermList.isEmpty()) {
                accountCollection = CHAR_N;
            } else {
                accountCollection = bilPolicyTermList.get(SHORT_ZERO).getBptActColCd();
            }
        }

        BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(policyId, accountId)).orElse(null);
        if (bilPolicy == null) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }
        double unPaidOpens = calculateOpensAmount(accountId, policyId, bilAccount.getBillTypeCd(), accountCollection);
        double newCashSuspend = calculateCashSuspended(accountId, bilPolicy, totalCashAmount,
                bilAccount.getBillTypeCd());
        if (unPaidOpens < newCashSuspend) {
            calculateWriteOffAmounts(cashApplyDriver, bilPolicy, unPaidOpens, newCashSuspend, bilAccount,
                    accountCollection);
        }
    }

    private void calculateWriteOffAmounts(CashApplyDriver cashApplyDriver, BilPolicy bilPolicy, double unPaidOpens,
            double newCashSuspend, BilAccount bilAccount, char accountCollection) {

        String bilDesReaTyp = "WRR";
        String bilDesReaTyp1 = "WPA";

        String accountId = cashApplyDriver.getAccountId();
        double totalPreWcAmount = getActivityAmounts(accountId, Arrays.asList("WC"), bilDesReaTyp1,
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd());

        double totalPreWcRevAmount = getActivityAmounts(accountId, Arrays.asList("WCR"), bilDesReaTyp,
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd());

        double totalPreWeAmount = getActivityAmounts(accountId, Arrays.asList("WE"), bilDesReaTyp1,
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd());

        double totalPreWeRevAmount = getActivityAmounts(accountId, Arrays.asList("WER"), bilDesReaTyp,
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd());

        Double totalRw1RevAmount = bilActSummaryRepository.getPremiumActivityAmount(accountId,
                Arrays.asList("RPB", "RWD"), "RW1", bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd());
        if (totalRw1RevAmount == null) {
            totalRw1RevAmount = DECIMAL_ZERO;
        }
        if (totalRw1RevAmount < 0) {
            totalRw1RevAmount = totalRw1RevAmount * -1;
        }
        Double totalPenaltyWPAmount = DECIMAL_ZERO;
        Double totalPenaltyWPRevAmount = DECIMAL_ZERO;
        Double totalLateWLAmount = DECIMAL_ZERO;
        Double totalLateWLRevAmount = DECIMAL_ZERO;
        Double totalScgWAAmount = DECIMAL_ZERO;
        Double totalScgWARevAmount = DECIMAL_ZERO;
        Double totalDownpaymentWDAmount = DECIMAL_ZERO;
        Double totalDownpaymentWDRevAmount = DECIMAL_ZERO;
        if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                || accountCollection == CHAR_Y) {
            totalPenaltyWPAmount = getBalaceAmounts(accountId, "WP", bilDesReaTyp1);
            totalPenaltyWPRevAmount = getBalaceAmounts(accountId, "WPR", bilDesReaTyp);
            totalLateWLAmount = getBalaceAmounts(accountId, "WL", bilDesReaTyp1);
            totalLateWLRevAmount = getBalaceAmounts(accountId, "WLR", bilDesReaTyp);
            totalScgWAAmount = getBalaceAmounts(accountId, "WA", bilDesReaTyp1);
            totalScgWARevAmount = getBalaceAmounts(accountId, "WAR", bilDesReaTyp);
            totalDownpaymentWDAmount = getBalaceAmounts(accountId, "WD", bilDesReaTyp1);
            totalDownpaymentWDRevAmount = getBalaceAmounts(accountId, "WDR", bilDesReaTyp);
        }

        if (totalPreWcAmount > DECIMAL_ZERO && totalPreWeAmount == DECIMAL_ZERO) {
            totalPreWcAmount = totalPreWeAmount + totalPreWcAmount;
            totalPreWcRevAmount = totalPreWcRevAmount + totalPreWeRevAmount + totalRw1RevAmount;
            totalPreWeRevAmount = DECIMAL_ZERO;
        } else {
            totalPreWeAmount = totalPreWeAmount + totalPreWcAmount;
            totalPreWeRevAmount = totalPreWcRevAmount + totalPreWeRevAmount + totalRw1RevAmount;
            totalPreWcAmount = DECIMAL_ZERO;
            totalPreWcRevAmount = DECIMAL_ZERO;
        }
        if (totalPreWcAmount - totalPreWcRevAmount > 0 || totalPreWeAmount - totalPreWeRevAmount > 0
                || totalPenaltyWPAmount - totalPenaltyWPRevAmount > 0 || totalLateWLAmount - totalLateWLRevAmount > 0
                || totalScgWAAmount - totalScgWARevAmount > 0
                || totalDownpaymentWDAmount - totalDownpaymentWDRevAmount > 0) {
            reverseAutomaticWriteoff(cashApplyDriver, bilPolicy, newCashSuspend - unPaidOpens,
                    totalPreWcAmount - totalPreWcRevAmount, totalPreWeAmount - totalPreWeRevAmount,
                    totalPenaltyWPAmount - totalPenaltyWPRevAmount, totalLateWLAmount - totalLateWLRevAmount,
                    totalScgWAAmount - totalScgWARevAmount, totalDownpaymentWDAmount - totalDownpaymentWDRevAmount,
                    bilAccount);
        }
    }

    private double getBalaceAmounts(String accountId, String descriptionCode, String reasonType) {
        Double amount = bilActSummaryRepository.getActivityAmount(accountId, descriptionCode, reasonType);
        if (amount == null) {
            amount = DECIMAL_ZERO;
        }

        return amount;
    }

    private double getActivityAmounts(String accountId, List<String> descriptionCode, String reasonType,
            String policyNumber, String policySymbol) {
        Double amount = bilActSummaryRepository.getPremiumActivityAmount(accountId, descriptionCode, reasonType,
                policyNumber, policySymbol);
        if (amount == null) {
            amount = DECIMAL_ZERO;
        }

        return amount;
    }

    private void reverseAutomaticWriteoff(CashApplyDriver cashApplyDriver, BilPolicy bilPolicy, double cashInSuspend,
            double prmWpaWcAmount, double prmWpaWeAmount, double penaltyWpaAmount, double lateWpaAmount,
            double scgWpaAmount, double downpaymentWpaAmount, BilAccount bilAccount) {
        ReverseWriteOffPayment reverseWriteOffPayment = new ReverseWriteOffPayment();
        reverseWriteOffPayment.setCashInSuspenseAmount(cashInSuspend);
        reverseWriteOffPayment.setPremiumWriteOffcAmount(prmWpaWcAmount);
        reverseWriteOffPayment.setPremiumWriteOffeAmount(prmWpaWeAmount);
        reverseWriteOffPayment.setPenaltyWriteOffAmount(penaltyWpaAmount);
        reverseWriteOffPayment.setLateWriteOffAmount(lateWpaAmount);
        reverseWriteOffPayment.setServiceChargeWriteOffAmount(scgWpaAmount);
        reverseWriteOffPayment.setDownPaymentWriteOffAmount(downpaymentWpaAmount);
        reverseWriteOffPayment.setQuoteIndicator(cashApplyDriver.getQuoteIndicator());
        reverseWriteOffPayment.setApplicationDate(cashApplyDriver.getCashApplyDate());
        reverseWriteOffPayment.setPolicyId(bilPolicy.getBillPolicyId().getPolicyId());
        reverseWriteOffToPaymentServiceImpl.reverseWriteOffToPayment(reverseWriteOffPayment, bilAccount);
    }

    private double calculateCashSuspended(String accountId, BilPolicy bilPolicy, Double totalCashAmount,
            String bilTypeCode) {
        double newCashSuspend = DECIMAL_ZERO;
        Integer isCashSupend = bilActSummaryRepository.getCashSuspendActivity(accountId, "WPA");
        if (isCashSupend == null || isCashSupend == 0) {
            newCashSuspend = totalCashAmount;
        } else {
            Double cashSuspendAmount;
            List<Character> revsRsusIndicatorList = Arrays.asList(ReverseReSuspendIndicator.BLANK.getValue(),
                    ReverseReSuspendIndicator.PAYMENTS_REAPPLIED.getValue());
            if (bilTypeCode.equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())) {
                cashSuspendAmount = bilCashDspRepository.readTotalCashAmount(accountId,
                        BilDspTypeCode.SUSPENDED.getValue(), revsRsusIndicatorList,
                        ManualSuspendIndicator.ENABLED.getValue());
            } else {
                cashSuspendAmount = bilCashDspRepository.readTotalCashAmountByPolicy(accountId,
                        bilPolicy.getPolSymbolCd(), bilPolicy.getPolNbr(), BilDspTypeCode.SUSPENDED.getValue(),
                        revsRsusIndicatorList, ManualSuspendIndicator.ENABLED.getValue());
            }
            if (cashSuspendAmount != null) {
                newCashSuspend = cashSuspendAmount;
            }
        }
        return newCashSuspend;
    }

    private double calculateOpensAmount(String accountId, String policyId, String bilTypeCode, char accountCollection) {
        double unpaidOpens = DECIMAL_ZERO;
        Double unPaidPremAmount = bilIstScheduleRepository.getUnpaidPremAmount(accountId, policyId);
        if (unPaidPremAmount == null) {
            unPaidPremAmount = DECIMAL_ZERO;
        }
        if (bilTypeCode.equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue()) || accountCollection == CHAR_Y) {
            Double unPaidScgAmount = bilCrgAmountsRepository.getTotalCrgAmount1(
                    Arrays.asList(InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue(),
                            InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()),
                    accountId, ChargeTypeCodes.SERVICE_CHARGE.getValue());
            if (unPaidScgAmount == null) {
                unPaidScgAmount = DECIMAL_ZERO;
            }
            Double unPaidLateAmount = bilCrgAmountsRepository
                    .getTotalCrgAmount2(Arrays.asList(ChargeTypeCodes.LATE_CHARGE.getValue()), accountId);
            if (unPaidLateAmount == null) {
                unPaidLateAmount = DECIMAL_ZERO;
            }
            Double unPaidDownPayment = bilCrgAmountsRepository
                    .getTotalCrgAmount2(Arrays.asList(ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()), accountId);
            if (unPaidDownPayment == null) {
                unPaidDownPayment = DECIMAL_ZERO;
            }
            Double unPaidPenalty = bilCrgAmountsRepository
                    .getTotalCrgAmount2(Arrays.asList(ChargeTypeCodes.PENALTY_CHARGE.getValue()), accountId);
            if (unPaidPenalty == null) {
                unPaidPenalty = DECIMAL_ZERO;
            }
            unpaidOpens = unPaidScgAmount + unPaidLateAmount + unPaidDownPayment + unPaidPenalty;
        }
        unpaidOpens = unpaidOpens + unPaidPremAmount;
        return unpaidOpens;
    }

    private Double sumCashAmount(String accountId) {
        List<Character> revsRsusIndicatorList = Arrays.asList(BLANK_CHAR);

        Double totalAmount = bilCashDspRepository.readTotalCashAmount(accountId, BilDspTypeCode.SUSPENDED.getValue(),
                revsRsusIndicatorList, CHAR_Y);
        if (totalAmount == null) {
            totalAmount = DECIMAL_ZERO;
        }
        return totalAmount;
    }

    private List<BilCashDsp> evaluateDownPaymentCash(String accountType, String accountId, String bilTypeCode,
            String bilClassCode, boolean isProcessAllDownPayments, ZonedDateTime currentDate) {
        ZonedDateTime displaceDate = null;
        List<BilCashDsp> bilCashDspList = null;
        BilRulesUct bilRulesUct = null;
        List<BilCashDsp> downPaymentList = new ArrayList<>();
        if (accountType.equals(DIRECT)) {
            if (isProcessAllDownPayments) {
                bilRulesUct = bilRulesUctService.readBilRulesUct("DPCA", bilTypeCode, bilClassCode, SEPARATOR_BLANK);
                if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                    try {
                        int downPaymentDay = 0;
                        if (bilRulesUct.getBrtParmListTxt() != null && !bilRulesUct.getBrtParmListTxt().isEmpty()
                                && bilRulesUct.getBrtParmListTxt().length() >= 3) {
                            downPaymentDay = Integer.parseInt(bilRulesUct.getBrtParmListTxt().substring(0, 3));
                        }
                        displaceDate = DateRoutine.adjustDateWithOffset(currentDate, false,
                                downPaymentDay, '-', null);

                    } catch (Exception exception) {
                        return downPaymentList;
                    }

                }
            }

            if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                bilCashDspList = bilCashDspRepository.fetchDownPaymentCashRows(accountId,
                        BilDspTypeCode.SUSPENDED.getValue(), CHAR_Y, "DPR", getRevsRsusIndicatorList());
            } else {
                bilCashDspList = bilCashDspRepository.fetchDownPaymentCashRows(accountId,
                        BilDspTypeCode.SUSPENDED.getValue(), CHAR_Y, "DPR",
                        Arrays.asList(ReverseReSuspendIndicator.BLANK.getValue()));
            }

            if (bilCashDspList != null && !bilCashDspList.isEmpty()) {
                for (BilCashDsp bilCashDsp : bilCashDspList) {
                    if (isProcessAllDownPayments) {
                        BilCashReceipt bilCashReceipt = bilCashReceiptRepository
                                .findById(new BilCashReceiptId(bilCashDsp.getBilCashDspId().getAccountId(),
                                        bilCashDsp.getBilCashDspId().getBilDtbDate(),
                                        bilCashDsp.getBilCashDspId().getDtbSequenceNbr()))
                                .orElse(null);
                        if (bilCashReceipt != null) {
                            if (displaceDate != null && bilCashReceipt.getEntryDate().compareTo(displaceDate) <= 0) {
                                bilCashDsp.setDspReasonCd(BLANK_STRING);
                                bilCashDsp.setManualSuspenseIndicator(BLANK_CHAR);
                                bilCashDspRepository.saveAndFlush(bilCashDsp);
                            } else {
                                downPaymentList.add(bilCashDsp);
                            }
                        }
                    } else {
                        downPaymentList.add(bilCashDsp);
                    }

                }
            }
        }

        return downPaymentList;
    }

    private boolean getBilCollectionMethod(String accountId, String collectionMethod, ZonedDateTime currentDate) {
        boolean eftCollectionMethod = false;
        final char ACTIVE_STATUS = CHAR_A;
        final String EFT_COLLECTION_METHOD = "CET";
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(collectionMethod,
                EFT_COLLECTION_METHOD, language);
        if (busCdTranslation != null) {
            List<BilEftBank> bilEftBankList = bilEftBankRepository.findBilEftBankEntry(accountId, CHAR_A,
                    currentDate, ACTIVE_STATUS, PageRequest.of(SHORT_ZERO, SHORT_ONE));
            if (bilEftBankList != null && !bilEftBankList.isEmpty()) {
                eftCollectionMethod = true;
            }
        }
        return eftCollectionMethod;
    }

    private BilActRules fetchBilActRules(String billTypeCd, String billClassCd) {
        BilActRules bilActRules = null;

        bilActRules = bilActRulesRepository.findById(new BilActRulesId(billTypeCd, billClassCd)).orElse(null);
        if (bilActRules == null) {
            bilActRules = bilActRulesRepository.findById(new BilActRulesId(billTypeCd, BLANK_STRING)).orElse(null);
            if (bilActRules == null) {
                return bilActRules;
            }
        }
        if (bilActRules.getBruFlpScgWroCd() != FullPayServiceChargeWriteOff.ACTUAL_INVOICE.getValue()
                && bilActRules.getBruFlpScgWroCd() != FullPayServiceChargeWriteOff.SCHEDULE_INVOICE.getValue()
                && bilActRules.getBruFlpScgWroCd() != FullPayServiceChargeWriteOff.USER_EXIT.getValue()
                && bilActRules.getBruFlpScgWroCd() != FullPayServiceChargeWriteOff.DISABLED.getValue()) {
            bilActRules.setBruFlpScgWroCd(CHAR_A);
        }

        return bilActRules;

    }

    private List<Character> getIssueIndList() {
        return Arrays.asList(PolicyIssueIndicators.ISSUED_EDIT.getValue(),
                PolicyIssueIndicators.ISSUED_NOT_EDIT.getValue());
    }

    private List<Character> getInvoiceCodeList() {
        return Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());
    }

    private BilActRules defaultBilActRules() {
        BilActRules bilActRules = new BilActRules();
        bilActRules.setBruDsbCkDays(SHORT_ZERO);
        bilActRules.setBruDsbEftDays(SHORT_ZERO);
        bilActRules.setBruDsbCrcrdDays(SHORT_ZERO);
        bilActRules.setBruMinDsbAmt(DECIMAL_ZERO);
        bilActRules.setBruMaxDsbAmt(DECIMAL_ZERO);
        bilActRules.setBruRnlIfDays(SHORT_ZERO);
        bilActRules.setBruMinWroNbr(SHORT_ZERO);
        bilActRules.setBruCcrNbrDays(SHORT_ZERO);
        bilActRules.setBruRnlIfStatCd(BLANK_CHAR);
        bilActRules.setBruNsfCncInd(CHAR_N);
        bilActRules.setBruDsbRctDtInd(CHAR_N);
        bilActRules.setBruRnlBillInd(CHAR_N);
        bilActRules.setBruRenBillInd(CHAR_N);
        bilActRules.setBruRssBillInd(CHAR_N);
        bilActRules.setBruRnlQteInd(CHAR_N);
        bilActRules.setBruRnlIfWipInd(CHAR_N);
        bilActRules.setBruCcrCreditInd(CHAR_N);
        bilActRules.setBruCcrPartInd(CHAR_N);
        bilActRules.setBruWroLteInd(CHAR_N);
        bilActRules.setBruPstmrkReqInd(CHAR_N);
        bilActRules.setBruIncWknInd(CHAR_N);
        bilActRules.setBruDsbPacsDays(SHORT_ZERO);
        bilActRules.setBruFlpScgWroCd(CHAR_A);
        return bilActRules;
    }

    private List<Character> getRevsRsusIndicatorList() {
        List<Character> revsRsusIndicatorList = new ArrayList<>();
        revsRsusIndicatorList.add(ReverseReSuspendIndicator.PAYMENTS_REAPPLIED.getValue());
        revsRsusIndicatorList.add(ReverseReSuspendIndicator.BLANK.getValue());
        return revsRsusIndicatorList;
    }

    private boolean isCashApplyOrDownPayment(String receiptTypeCode) {
        List<BusCdTranslation> busCdTranslationList = busCdTranslationRepository.findAllByCodeAndTypeList(
                receiptTypeCode, Arrays.asList("CAP", BusCodeTranslationType.DOWN_PAYMENT.getValue()),
                language);
        return busCdTranslationList != null && !busCdTranslationList.isEmpty();
    }

    private void checkIfAb(BilAccount bilAccount, String policyId, boolean writeOffCollection, double collMinimumAmount,
            CashApplyDriver cashApplyDriver) {

        String accountId = bilAccount.getAccountId();

        List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.getLastCollectionTerm(accountId, policyId,
                Arrays.asList(BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue(),
                        BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue()),
                CHAR_Y, PageRequest.of(SHORT_ZERO, SHORT_ONE));

        if (bilPolicyTermList != null && !bilPolicyTermList.isEmpty()) {
            String lastPolicyId = bilPolicyTermList.get(SHORT_ZERO).getBillPolicyTermId().getPolicyId();
            List<BilPolicyTerm> bilPolicyTermList1 = bilPolicyTermRepository.findPolicyData1(accountId, lastPolicyId,
                    Arrays.asList(BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue(),
                            BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue()));

            if (bilPolicyTermList1 == null || bilPolicyTermList1.isEmpty()) {
                throw new DataNotFoundException("bil.policy.trm.not.found",
                        new Object[] { accountId.trim(), lastPolicyId });
            }

            BilPolicyTerm bilPolicyTerm = bilPolicyTermList1.get(SHORT_ZERO);

            BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(lastPolicyId, accountId)).orElse(null);
            if (bilPolicy == null) {
                throw new DataNotFoundException("bil.policy.not.found",
                        new Object[] { accountId.trim(), lastPolicyId });
            }

            Double[] collectionBalaceData = deterCollectionBalance(accountId, bilPolicyTerm, bilPolicy, false);

            boolean rowExist = bilPolActivityRepository
                    .existsById(new BilPolActivityId(accountId, lastPolicyId, DateRoutine.defaultDateTime(), "BCL"));
            if (bilPolicyTerm.getBillPolStatusCd() == BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue()) {
                checkCollectionBalanceChange(collectionBalaceData, rowExist, accountId, lastPolicyId, bilAccount,
                        bilPolicyTerm, writeOffCollection, cashApplyDriver);
            } else {
                checkPreCollectionBalanceChange(collectionBalaceData, rowExist, accountId, lastPolicyId, bilAccount,
                        bilPolicyTerm, writeOffCollection, collMinimumAmount, bilPolicy, cashApplyDriver);
            }

            updateAccountCollection(accountId, lastPolicyId);
        }

    }

    private void checkCollectionBalanceChange(Double[] collectionBalaceData, boolean rowExist, String accountId,
            String lastPolicyId, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, boolean writeOffCollection,
            CashApplyDriver cashApplyDriver) {

        Double collectionBalance = collectionBalaceData[0];
        Double policyBalance = collectionBalaceData[1];
        Double surchargeBalance = collectionBalaceData[2];

        if (collectionBalance.compareTo(bilPolicyTerm.getBptPolColAmt()) != SHORT_ZERO) {
            if (!rowExist) {
                double balanceChange = collectionBalance - bilPolicyTerm.getBptPolColAmt();

                callFinancialApi(balanceChange, cashApplyDriver.getCashApplyDate(), BLANK_STRING,
                        bilAccount.getAccountNumber(), ObjectCode.COLLECTIONS.toString(), ActionCode.BALANCE.toString(),
                        BLANK_STRING, BLANK_STRING, BLANK_STRING, "BCG", bilAccount.getBillTypeCd(),
                        bilAccount.getBillClassCd(), cashApplyDriver.getCashApplyDate(), BLANK_STRING, BLANK_STRING,
                        DateRoutine.dateTimeAsYYYYMMDDString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()),
                        BCMOCA, lastPolicyId, bilPolicyTerm.getBillCountryCd(), bilPolicyTerm.getLobCd(),
                        bilPolicyTerm.getMasterCompanyNbr(), bilPolicyTerm.getBillStatePvnCd(),
                        bilAccount.getCurrencyCode());

                if (writeOffCollection && (policyBalance.compareTo(DECIMAL_ZERO) > SHORT_ZERO
                        || surchargeBalance.compareTo(DECIMAL_ZERO) > SHORT_ZERO)) {
                    insertBilPolActivity(accountId, lastPolicyId, "CWO", cashApplyDriver.getCashApplyDate());

                }
                insertBilPolActivity(accountId, lastPolicyId, "CCL", cashApplyDriver.getCashApplyDate());
            }
            bilPolicyTerm.setBptPolColAmt(collectionBalance);
            bilPolicyTermRepository.saveAndFlush(bilPolicyTerm);
        }

    }

    private void checkPreCollectionBalanceChange(Double[] collectionBalaceData, boolean rowExist, String accountId,
            String lastPolicyId, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, boolean writeOffCollection,
            Double collMinimumAmount, BilPolicy bilPolicy, CashApplyDriver cashApplyDriver) {
        Double collectionBalance = collectionBalaceData[0];
        Double policyBalance = collectionBalaceData[1];
        Double surchargeBalance = collectionBalaceData[2];
        if (!rowExist) {
            if (collectionBalance.compareTo(collMinimumAmount) < SHORT_ZERO
                    || collectionBalance.compareTo(DECIMAL_ZERO) <= SHORT_ZERO
                            && collMinimumAmount.compareTo(DECIMAL_ZERO) == SHORT_ZERO) {

                CollectionVendorTapeProcessing collectionVendorTapeProcessing = buildBillingApiParam(
                        cashApplyDriver.getAccountId(), bilAccount, bilPolicyTerm, bilPolicy.getPolSymbolCd(),
                        bilPolicy.getPolNbr(), cashApplyDriver.getCashApplyDate());
                billingApiService.getCollectionVendor(collectionVendorTapeProcessing);
                return;
            } else {
                if ((collectionBalance.compareTo(bilPolicyTerm.getBptPolColAmt()) != SHORT_ZERO)
                        && (writeOffCollection && (policyBalance.compareTo(DECIMAL_ZERO) > SHORT_ZERO
                                || surchargeBalance.compareTo(DECIMAL_ZERO) > SHORT_ZERO))) {
                    insertBilPolActivity(accountId, lastPolicyId, "CWO", cashApplyDriver.getCashApplyDate());
                }
            }
        }

        if (collectionBalance.compareTo(bilPolicyTerm.getBptPolColAmt()) != SHORT_ZERO)

        {
            FinancialApiActivity financialApiActivity = new FinancialApiActivity();
            double balanceChange = collectionBalance - bilPolicyTerm.getBptPolColAmt();
            String actionCode;
            String objectCode;
            financialApiActivity.setBilReasonCode("BCG");
            financialApiActivity.setActivityAmount(balanceChange);
            financialApiActivity.setActivityNetAmount(balanceChange);
            if (!rowExist) {
                objectCode = ObjectCode.COLLECTIONS.toString();
                actionCode = ActionCode.BALANCE.toString();
            } else {
                objectCode = ObjectCode.PRE_COLLECTIONS.toString();
                actionCode = ActionCode.BALANCE.toString();
            }

            callFinancialApi(balanceChange, cashApplyDriver.getCashApplyDate(), BLANK_STRING,
                    bilAccount.getAccountNumber(), objectCode, actionCode, BLANK_STRING, BLANK_STRING, BLANK_STRING,
                    "BCG", bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(), cashApplyDriver.getCashApplyDate(),
                    BLANK_STRING, BLANK_STRING,
                    DateRoutine.dateTimeAsYYYYMMDDString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()),
                    BCMOCA, lastPolicyId, bilPolicyTerm.getBillCountryCd(), bilPolicyTerm.getLobCd(),
                    bilPolicyTerm.getMasterCompanyNbr(), bilPolicyTerm.getBillStatePvnCd(),
                    bilAccount.getCurrencyCode());

            bilPolicyTerm.setBptPolColAmt(collectionBalance);
            bilPolicyTermRepository.saveAndFlush(bilPolicyTerm);
        }
    }

    private void insertBilPolActivity(String accountId, String lastPolicyId, String activityTypeCode,
            ZonedDateTime bilNxtAcyDt) {

        BilPolActivity bilPolActivity = new BilPolActivity();
        bilPolActivity.setBillPolActivityId(
                new BilPolActivityId(accountId, lastPolicyId, DateRoutine.defaultDateTime(), activityTypeCode));
        bilPolActivity.setBilNxtAcyDt(bilNxtAcyDt);
        bilPolActivityRepository.saveAndFlush(bilPolActivity);

    }

    private void updateAccountCollection(String accountId, String lastPolicyId) {

        Integer count = bilPolicyTermRepository.updateCollectionIndicator(accountId, lastPolicyId, CHAR_N);
        if (count == null || count == 0) {
            throw new DataNotFoundException("unable.to.update.data.bil.policy.trm",
                    new Object[] { accountId.trim(), lastPolicyId.trim() });
        }

    }

    private void callFinancialApi(Double amount, ZonedDateTime applicationDate, String bankCode, String accountNumber,
            String transactionObjectCode, String transactionActionCode, String paybleItemCode, String sourceCode,
            String paymentType, String bilReasonCode, String bilTypeCode, String bilClassCode,
            ZonedDateTime referenceDate, String databaseKey, String operatorId, String orginalEffectiveDate,
            String applicationProgram, String policyId, String countryCode, String lineOfBusiness, String masterCompany,
            String riskState, String currencyCode) {

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();

        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(BLANK_STRING);
        financialApiActivity.setApplicationName(ApplicationName.BCMOCA.toString());
        financialApiActivity.setUserId(BLANK_STRING);
        financialApiActivity.setErrorCode(ERROR_CODE);

        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setMasterCompanyNbr(masterCompany);
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setAgentTtyId(BLANK_STRING);
        financialApiActivity.setAppProgramId(applicationProgram);
        financialApiActivity.setBilAccountId(accountNumber);
        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setBilClassCode(bilClassCode);
        financialApiActivity.setBilDatabaseKey(databaseKey);
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilReceiptTypeCode(paymentType);
        financialApiActivity.setBilReasonCode(bilReasonCode);
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(referenceDate));
        financialApiActivity.setBilSourceCode(sourceCode);
        financialApiActivity.setBilTypeCode(bilTypeCode);
        financialApiActivity.setClientId(BLANK_STRING);
        financialApiActivity.setCountryCode(countryCode);
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setCurrencyCode(currencyCode);
        financialApiActivity.setDepositeBankCode(BLANK_STRING);
        financialApiActivity.setDisbursementId(BLANK_STRING);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setLineOfBusCode(lineOfBusiness);
        financialApiActivity.setMarketSectorCode(BLANK_STRING);
        financialApiActivity.setOriginalEffectiveDate(orginalEffectiveDate);
        financialApiActivity.setOperatorId(operatorId);
        financialApiActivity.setPayableItemCode(paybleItemCode);
        financialApiActivity.setPolicyId(policyId);
        financialApiActivity.setStateCode(riskState);
        financialApiActivity.setStatusCode(BLANK_CHAR);

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {

            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

}
