package billing.service.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_U;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilBankEntry;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilEftActivity;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilEftRejBch;
import billing.data.entity.BilEftRejRea;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilThirdParty;
import billing.data.entity.BilTtyCashRct;
import billing.data.entity.BilTtySummary;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilBankEntryId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.entity.id.BilEftRejBchId;
import billing.data.entity.id.BilTtyCashRctId;
import billing.data.entity.id.BilTtySummaryId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilBankEntryRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilEftActivityRepository;
import billing.data.repository.BilEftBankRepository;
import billing.data.repository.BilEftRejBchRepository;
import billing.data.repository.BilEftRejReaRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.data.repository.BilTtyCashRctRepository;
import billing.data.repository.BilTtySummaryRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.model.RejectionPayment;
import billing.model.embedded.Party;
import billing.service.BatchRejectionPaymentService;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilTechKeyTypeCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.StatementLevelType;
import core.api.ExecContext;
import core.exception.validation.InvalidDataException;
import core.model.Paging;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import core.utils.MultiFiltersSearch;
import core.utils.PageNavigation;
import party.data.entity.CcmAgency;
import party.data.entity.CltBankAccount;
import party.data.repository.CcmAgencyRepository;
import party.data.repository.CltBankAccountRepository;
import party.service.UserPartySecurityService;
import party.service.impl.PartyElasticSearchServiceImpl;

@Service
public class BatchRejectionPaymentServiceImpl implements BatchRejectionPaymentService {

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private CltBankAccountRepository cltBankAccountRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    private BilEftRejReaRepository bilEftRejReaRepository;

    @Autowired
    private BilBankEntryRepository bilBankEntryRepository;

    @Autowired
    private CcmAgencyRepository ccmAgencyRepository;

    @Autowired
    private BilEftRejBchRepository bilEftRejBchRepository;

    @Autowired
    private BilEftActivityRepository bilEftActivityRepository;

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private UserPartySecurityService userPartySecurityService;

    @Autowired
    private PartyElasticSearchServiceImpl partyElasticSearchServiceImpl;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilTtyCashRctRepository bilTtyCashRctRepository;

    @Autowired
    private BilTtySummaryRepository bilTtySummaryRepository;

    private static final String REJECTION_REASON = "rejectionReason";
    private static final String NO_DATA_FOUND = "no.data.found.table";
    private String unidCashDir = BLANK_STRING;

    List<Character> statementTypeList = Arrays.asList(StatementLevelType.ACCOUNT_LEVEL.getValue(),
            StatementLevelType.DETAIL_LEVEL.getValue());

    private static final Logger logger = LogManager.getLogger(BatchRejectionPaymentServiceImpl.class);

    @Override
    public List<RejectionPayment> findAllBatchPayments(String entryDate, String batchId, String query, String filters,
            String sort, int size, int page, Paging paging) {
        List<RejectionPayment> rejectionPaymentList = new LinkedList<>();
        int totalRows = 0;
        Page<BilEftRejBch> bilEftRejBchPage = null;
        List<BilEftRejBch> bilEftRejBchRows = new ArrayList<>();

        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(entryDate);

        if (filters != null && !filters.isEmpty()) {
            bilEftRejBchPage = batchPaymentsRowsByParams(bilEntryDt, batchId, execContext.getUserSeqeunceId(), page,
                    size, filters);
        } else {
            bilEftRejBchPage = bilEftRejBchRepository.getRejectionsByBatchId(batchId, bilEntryDt,
                    execContext.getUserSeqeunceId(), PageRequest.of(page - 1, size));
        }

        if (bilEftRejBchPage != null && bilEftRejBchPage.getTotalElements() != 0) {
            totalRows = (int) bilEftRejBchPage.getTotalElements();
            bilEftRejBchRows = bilEftRejBchPage.getContent();
        }

        if (bilEftRejBchRows.isEmpty()) {
            return rejectionPaymentList;
        }
        for (BilEftRejBch bilEftRejBchRow : bilEftRejBchRows) {
            if (!bilEftRejBchRow.getBilEftTraceNbr().trim().isEmpty()) {
                rejectionPaymentList.add(mapBilEfRejBchToRejectionPayment(bilEftRejBchRow));
            }

        }

        PageNavigation.getPagingAttributes(totalRows, size, page, paging);
        return rejectionPaymentList;
    }

    private Page<BilEftRejBch> batchPaymentsRowsByParams(ZonedDateTime bilEntryDt, String batchId, String userId,
            int page, int size, String filters) {
        Map<String, String> matchFilterMap = new HashMap<>();

        matchFilterMap = MultiFiltersSearch.getFilters(filters, matchFilterMap);
        String rejectionReason = BLANK_STRING;

        for (Map.Entry<String, String> entry : matchFilterMap.entrySet()) {
            String filter = entry.getKey();
            String match = entry.getValue();
            if (match != null && filter.equalsIgnoreCase(REJECTION_REASON)) {
                rejectionReason = match;
            }
        }

        String rejectionCode = BLANK_STRING;

        if (!rejectionReason.isEmpty()) {
            String eftRejReaCode = bilEftRejReaRepository.findEftRejCd(rejectionReason.trim());
            if (eftRejReaCode != null && !eftRejReaCode.isEmpty()) {
                rejectionCode = eftRejReaCode.trim();
            } else {
                rejectionCode = rejectionReason.trim();
            }
        }
        return getBatchPaymentsWithFilters(bilEntryDt, batchId, userId, rejectionCode, page, size);

    }

    private Page<BilEftRejBch> getBatchPaymentsWithFilters(ZonedDateTime bilEntryDt, String batchId, String userId,
            String rejectionCode, int page, int size) {
        Page<BilEftRejBch> bilEftRejBchPage = null;

        if (!rejectionCode.isEmpty()) {
            bilEftRejBchPage = bilEftRejBchRepository.getBilEftRejBchRowsByRejectioncode(batchId, bilEntryDt, userId,
                    rejectionCode, PageRequest.of(page - 1, size));
        }

        return bilEftRejBchPage;
    }

    private RejectionPayment mapBilEfRejBchToRejectionPayment(BilEftRejBch bilEftRejBch) {
        RejectionPayment batchPayment = new RejectionPayment();
        ZonedDateTime entryDt = bilEftRejBch.getBilEftRejBchId().getBrbEntryDt();
        String entryNbr = bilEftRejBch.getBilEftRejBchId().getBrbBchNbr();
        batchPayment.setEntryDate(entryDt);
        batchPayment.setBatchId(entryNbr);
        batchPayment.setPaymentSequenceNumber(bilEftRejBch.getBilEftRejBchId().getBrbEntrySeqNbr());
        batchPayment.setSettlementAmount(bilEftRejBch.getBilEftDrAmt());
        batchPayment.setTransactionNumber(bilEftRejBch.getBilEftTraceNbr().trim());
        batchPayment.setRejectionReason(getRejectionReason(bilEftRejBch.getBilEftRejCd().trim()));
        batchPayment.setSettlementDate(bilEftRejBch.getBilEftDrDt());
        batchPayment.setPayorName(bilEftRejBch.getBilPayLngNm().trim());
        batchPayment.setRejectionStatus(BLANK_STRING);
        List<BilEftActivity> bilEftActivityList = bilEftActivityRepository.checkTraceNumber(
                bilEftRejBch.getBilEftDrDt(), bilEftRejBch.getBilEftTraceNbr().trim(),
                PageRequest.of(SHORT_ZERO, SHORT_ONE));

        if (bilEftActivityList != null && !bilEftActivityList.isEmpty()) {
            if (bilEftActivityList.get(SHORT_ZERO).getBeaTrsSta() == CHAR_X
                    || bilEftActivityList.get(SHORT_ZERO).getBeaTrsSta() == CHAR_D
                    || bilEftActivityList.get(SHORT_ZERO).getBeaTrsSta() == CHAR_R) {
                batchPayment.setRejectionStatus("Rejected");
            }
            batchPayment.setReferenceNumber(bilEftActivityList.get(SHORT_ZERO).getBilPmtOrderId().trim());
        }

        return batchPayment;
    }

    private String getRejectionReason(String reasonCode) {
        BilEftRejRea bilEftRejRea = bilEftRejReaRepository.findEftRejCdDescription(reasonCode);
        if (bilEftRejRea != null) {
            return bilEftRejRea.getBrrRejTxt().trim();
        }
        return reasonCode;
    }

    private String getCodeDescription(String busCode, String busTypeCode) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(busCode, busTypeCode,
                execContext.getLanguage());
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusDescription().trim();
        }
        return busCode;
    }

    @Override
    public RejectionPayment findBatchPaymentDetails(String batchId, String entryDate, short paymentSeqNumber) {
        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(entryDate);

        BilEftRejBch bilEftRejBchRow = bilEftRejBchRepository
                .findById(new BilEftRejBchId(bilEntryDt, batchId, execContext.getUserSeqeunceId(), paymentSeqNumber))
                .orElse(null);
        if (bilEftRejBchRow == null || bilEftRejBchRow.getBilEftTraceNbr().trim().isEmpty()) {
            throw new InvalidDataException("batchpayment.notfound", new Object[] { batchId });
        }

        return mapBilEfRejBchDetail(bilEftRejBchRow);

    }

    private RejectionPayment mapBilEfRejBchDetail(BilEftRejBch bilEftRejBch) {

        RejectionPayment batchPayment = new RejectionPayment();
        ZonedDateTime entryDt = bilEftRejBch.getBilEftRejBchId().getBrbEntryDt();
        String entryNbr = bilEftRejBch.getBilEftRejBchId().getBrbBchNbr();

        batchPayment.setEntryDate(entryDt);
        batchPayment.setBatchId(entryNbr);
        batchPayment.setPaymentSequenceNumber(bilEftRejBch.getBilEftRejBchId().getBrbEntrySeqNbr());

        batchPayment.setSettlementAmount(bilEftRejBch.getBilEftDrAmt());
        batchPayment.setTransactionNumber(bilEftRejBch.getBilEftTraceNbr().trim());
        batchPayment.setSettlementDate(bilEftRejBch.getBilEftDrDt());
        batchPayment.setRejectionReason(getRejectionReason(bilEftRejBch.getBilEftRejCd().trim()));
        batchPayment.setPayorName(bilEftRejBch.getBilPayLngNm().trim());

        if (!bilEftRejBch.getBilAccountNumber().trim().isEmpty()) {
            batchPayment.setIdentifier(bilEftRejBch.getBilAccountNumber().trim());
            BilAccount bilAccount = bilAccountRepository.findById(bilEftRejBch.getBilAccountId()).orElse(null);
            if (bilAccount != null) {
                if (!bilAccount.getThirdPartyIdentifier().trim().isEmpty()) {
                    batchPayment.setIdentifierType(AccountType.GROUPBILL.toString());
                } else if (!bilAccount.getAgentAccountNumber().trim().isEmpty()) {
                    batchPayment.setIdentifierType(AccountType.AGENCYBILL.toString());
                } else {
                    batchPayment.setIdentifierType(AccountType.DIRECTBILL.toString());
                }
            } else {
                batchPayment.setIsUnidentified(true);
            }

        } else if (!bilEftRejBch.getBilTtyNbr().trim().isEmpty()) {
            batchPayment.setIdentifier(bilEftRejBch.getBilTtyNbr().trim());
            batchPayment.setIdentifierType(getIdentifierTypeDescription(AccountType.GROUP_ACCOUNT.toString()));
        } else {
            batchPayment.setIdentifier(bilEftRejBch.getBilUnidCashId().trim());
            batchPayment.setIsUnidentified(true);
        }
        if (!bilEftRejBch.getBilUnidCashDir().trim().isEmpty()) {
            List<BilUnIdCash> bilUnIdCashList = bilUnIdCashRepository
                    .findByBillUnidCashId(bilEftRejBch.getBilUnidCashId());
            if (bilUnIdCashList != null && !bilUnIdCashList.isEmpty()) {
                if (!bilUnIdCashList.get(SHORT_ZERO).getBilAccountNbr().trim().isEmpty()) {
                    batchPayment.setIdentifier(bilUnIdCashList.get(SHORT_ZERO).getBilAccountNbr().trim());
                    batchPayment.setIdentifierType(getIdentifierTypeDescription(AccountType.BILLACCOUNT.toString()));
                } else if (!bilUnIdCashList.get(SHORT_ZERO).getBilTtyNbr().trim().isEmpty()) {
                    batchPayment.setIdentifier(bilUnIdCashList.get(SHORT_ZERO).getBilTtyNbr().trim());
                    batchPayment.setIdentifierType(getIdentifierTypeDescription(AccountType.GROUP_ACCOUNT.toString()));
                }

                if (!bilUnIdCashList.get(SHORT_ZERO).getPolNbr().trim().isEmpty()) {
                    batchPayment.setPolicySymbol(bilUnIdCashList.get(SHORT_ZERO).getPolSymbolCd().trim());
                    batchPayment.setPolicyNumber(bilUnIdCashList.get(SHORT_ZERO).getPolNbr().trim());
                }

            }
        }

        batchPayment.setAgencyNumber(bilEftRejBch.getAgencyNbr().trim());

        if (bilEftRejBch.getBilCrCrdTypeCd() != BLANK_CHAR) {
            batchPayment.setCreditCardNumber(bilEftRejBch.getBilBankAccNbr().trim());
            batchPayment.setCreditCardType(getCodeDescription(String.valueOf(bilEftRejBch.getBilCrCrdTypeCd()),
                    BusCodeTranslationType.CREDIT_CARD_TYPE.getValue()));
        } else {
            batchPayment.setBankAccountNumber(bilEftRejBch.getBilBankAccNbr().trim());
        }

        List<BilEftActivity> bilEftActivityList = bilEftActivityRepository.checkTraceNumber(
                bilEftRejBch.getBilEftDrDt(), bilEftRejBch.getBilEftTraceNbr().trim(),
                PageRequest.of(SHORT_ZERO, SHORT_ONE));
        if (bilEftActivityList != null && !bilEftActivityList.isEmpty()) {
            batchPayment.setReferenceNumber(bilEftActivityList.get(SHORT_ZERO).getBilPmtOrderId().trim());
            if (bilEftActivityList.get(SHORT_ZERO).getBeaTrsSta() == CHAR_X
                    || bilEftActivityList.get(SHORT_ZERO).getBeaTrsSta() == CHAR_D
                    || bilEftActivityList.get(SHORT_ZERO).getBeaTrsSta() == CHAR_R) {
                batchPayment.setRejectionStatus("Rejected");
            }
        } else {
            batchPayment.setRejectionStatus(BLANK_STRING);
        }

        return batchPayment;
    }

    @Override
    public void deleteBatchPayment(String batchId, String entryDate, short paymentSeqNumber) {

        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(entryDate);
        BilEftRejBch bilEftRejBch = bilEftRejBchRepository
                .findById(new BilEftRejBchId(bilEntryDt, batchId, execContext.getUserSeqeunceId(), paymentSeqNumber))
                .orElse(null);
        if (bilEftRejBch == null) {
            throw new InvalidDataException("batchpayment.notfound", new Object[] { batchId });
        } else if (bilEftRejBch.getBrbRejStaInd() != CHAR_O) {
            throw new InvalidDataException("rejection.delete.not.valid");
        }
        Integer rejectionPaymentsCount = bilEftRejBchRepository.countbatchExits(batchId, bilEntryDt,
                execContext.getUserSeqeunceId());
        if (rejectionPaymentsCount == 1) {
            resetBilEftRejBchData(bilEftRejBch);
        } else {
            bilEftRejBchRepository.delete(bilEftRejBch);
        }

    }

    private void resetBilEftRejBchData(BilEftRejBch bilEftRejBch) {
        bilEftRejBch.getBilEftRejBchId().setBrbEntrySeqNbr(SHORT_ZERO);
        bilEftRejBch.setBilAccountId(BLANK_STRING);
        bilEftRejBch.setBilAccountNumber(BLANK_STRING);
        bilEftRejBch.setBilThirdPartyId(BLANK_STRING);
        bilEftRejBch.setBilTtyNbr(BLANK_STRING);
        bilEftRejBch.setBilEftRejCd(BLANK_STRING);
        bilEftRejBch.setBilEftDrDt(DateRoutine.defaultDateTime());
        bilEftRejBch.setBilEftTraceNbr(BLANK_STRING);
        bilEftRejBch.setBilBankAccNbr(BLANK_STRING);
        bilEftRejBch.setBrbAceDt(DateRoutine.defaultDateTime());
        bilEftRejBch.setBilEftDrAmt(DECIMAL_ZERO);
        bilEftRejBch.setBrbReviewCd(BLANK_STRING);
        bilEftRejBch.setBrbRejStaInd(CHAR_O);
        bilEftRejBch.setBilCrCrdTypeCd(BLANK_CHAR);
        bilEftRejBch.setBilPayLngNm(BLANK_STRING);
        bilEftRejBch.setBilUnidCashId(BLANK_STRING);
        bilEftRejBch.setBilUnidCashDir(BLANK_STRING);
        bilEftRejBch.setAgencyNbr(BLANK_STRING);
        bilEftRejBchRepository.save(bilEftRejBch);
    }

    @Override
    public void patchBatchPayment(String batchId, String entryDate, short paymentSeqNumber,
            RejectionPayment rejectionPayment) {

        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(entryDate);
        BilEftRejBch bilEftRejBch = processRejectionData(rejectionPayment);
        bilEftRejBch.setBilEftRejBchId(
                new BilEftRejBchId(bilEntryDt, batchId, execContext.getUserSeqeunceId(), paymentSeqNumber));

        bilEftRejBchRepository.save(bilEftRejBch);

    }

    @Override
    public Short saveBatchPayment(String batchId, String entryDate, RejectionPayment rejectionPayment) {
        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(entryDate);

        Short paymentSeq = null;

        BilEftRejBch bilEftRejBch = processRejectionData(rejectionPayment);

        Short entrySequence = getMaxSequenceNbr(batchId, bilEntryDt);
        bilEftRejBch.setBilEftRejBchId(
                new BilEftRejBchId(bilEntryDt, batchId, execContext.getUserSeqeunceId(), entrySequence));

        bilEftRejBchRepository.save(bilEftRejBch);
        paymentSeq = bilEftRejBch.getBilEftRejBchId().getBrbEntrySeqNbr();

        return paymentSeq;
    }

    private Short getMaxSequenceNbr(String batchId, ZonedDateTime bilEntryDt) {
        Integer bilEntrySequenceNumber = bilEftRejBchRepository.getMaxBilEntrySeq(batchId, bilEntryDt,
                execContext.getUserSeqeunceId());
        if (bilEntrySequenceNumber == null) {
            bilEntrySequenceNumber = 0;
        } else if (bilEntrySequenceNumber == 0) {
            BilEftRejBch bilEftRejBch = bilEftRejBchRepository.findById(new BilEftRejBchId(bilEntryDt, batchId,
                    execContext.getUserSeqeunceId(), bilEntrySequenceNumber.shortValue())).orElse(null);
            if (bilEftRejBch != null && !bilEftRejBch.getBilEftTraceNbr().trim().isEmpty()) {
                bilEntrySequenceNumber = bilEntrySequenceNumber + 1;
            }
        } else {
            bilEntrySequenceNumber = bilEntrySequenceNumber + 1;
        }
        return bilEntrySequenceNumber.shortValue();
    }

    private BilEftRejBch processRejectionData(RejectionPayment rejectionPayment) {

        ZonedDateTime settlementDate = rejectionPayment.getSettlementDate();
        String traceNumber = rejectionPayment.getTransactionNumber();
        BilEftActivity bilEftActivity = getActivity(rejectionPayment.getTransactionNumber(),
                rejectionPayment.getSettlementDate());

        Double dueAmount = bilEftActivity.getBilEftDrAmt();
        if (bilEftActivity.getBilEftRcdTyc() == EftRecordType.AGENCY_SWEEP.getValue()
                && bilEftActivity.getBilEftDrAmt() > DECIMAL_ZERO) {
            Double amount = bilEftActivityRepository.findAmountByTraceNbr(settlementDate, traceNumber);
            if (amount != null) {
                dueAmount = amount;
            }

        }

        String[] bankData = getAccount(bilEftActivity);
        return insertBilEftRejBch(bankData[0], unidCashDir, dueAmount, bankData[1], traceNumber, bilEftActivity,
                rejectionPayment, bankData[2]);
    }

    private BilEftActivity getActivity(String traceNumber, ZonedDateTime settlementDate) {

        List<BilEftActivity> bilEftActivityList = bilEftActivityRepository.checkTraceNumber(settlementDate, traceNumber,
                PageRequest.of(SHORT_ZERO, SHORT_ONE));
        if (bilEftActivityList != null && !bilEftActivityList.isEmpty()) {

            if (bilEftActivityList.get(SHORT_ZERO).getBilEftRcdTyc() == CHAR_T
                    || bilEftActivityList.get(SHORT_ZERO).getBilEftRcdTyc() == CHAR_U) {
                throw new InvalidDataException("payment.creditcard");
            } else if (bilEftActivityList.get(SHORT_ZERO).getBeaTrsSta() == CHAR_X
                    || bilEftActivityList.get(SHORT_ZERO).getBeaTrsSta() == CHAR_D
                    || bilEftActivityList.get(SHORT_ZERO).getBeaTrsSta() == CHAR_R) {
                throw new InvalidDataException("transaction.rejected",
                        new Object[] { traceNumber, DateRoutine.dateTimeAsMMDDYYYYAsString(settlementDate) });
            }

            return bilEftActivityList.get(SHORT_ZERO);
        } else {
            throw new InvalidDataException("transaction.not.found",
                    new Object[] { traceNumber, DateRoutine.dateTimeAsMMDDYYYYAsString(settlementDate) });
        }

    }

    private String[] getAccount(BilEftActivity bilEftActivity) {

        if (bilEftActivity.getBilEftRcdTyc() == EftRecordType.ONE_TIME_ACH.getValue()) {
            BilBankEntry bilBankEntry = getBankInformation(bilEftActivity);
            if (bilBankEntry == null) {
                throw new InvalidDataException(NO_DATA_FOUND);
            }
            return new String[] { bilBankEntry.getBankAccountNbr(), SEPARATOR_BLANK,
                    bilBankEntry.getEftPayorLongName() };
        } else if (bilEftActivity.getBilEftRcdTyc() == EftRecordType.AGENCY_SWEEP.getValue()) {
            boolean excdContracts = checkAgencySweepConfig();
            String clientId = getAgencyBank(bilEftActivity.getBilEftAcyDt(), bilEftActivity.getAgencyNbr(),
                    excdContracts);
            CltBankAccount cltBankAccount = getPolicyDataRetrieveAgent(clientId, bilEftActivity.getBilEftAcyDt());
            if (cltBankAccount == null) {
                throw new InvalidDataException(NO_DATA_FOUND);
            }
            return new String[] { cltBankAccount.getCibaBkActNbr(), SEPARATOR_BLANK, SEPARATOR_BLANK };
        } else if (bilEftActivity.getBilEftActivityId().getBilTchKeyTypCd() == BilTechKeyTypeCode.THIRD_PARTY
                .getValue()) {
            BilEftBank bilEftBank = getEftBankDetails(BilTechKeyTypeCode.THIRD_PARTY.getValue(), bilEftActivity);
            if (bilEftBank == null) {
                throw new InvalidDataException(NO_DATA_FOUND, new Object[] { "BIL_EFT_BANK" });
            }
            return new String[] { bilEftBank.getBilBankAcctNbr(), String.valueOf(bilEftBank.getBilCrcrdTypeCd()),
                    SEPARATOR_BLANK };
        } else {
            BilEftBank bilEftBank = getEftBankDetails(BilTechKeyTypeCode.BILL_ACCOUNT.getValue(), bilEftActivity);
            if (bilEftBank == null) {
                throw new InvalidDataException(NO_DATA_FOUND, new Object[] { "BIL_EFT_BANK" });
            }
            return new String[] { bilEftBank.getBilBankAcctNbr(), String.valueOf(bilEftBank.getBilCrcrdTypeCd()),
                    SEPARATOR_BLANK };
        }

    }

    private BilEftBank getEftBankDetails(char bilTypeCode, BilEftActivity bilEftActivity) {
        List<BilEftBank> bilEftBankList = bilEftBankRepository.findEftBankEntry(
                bilEftActivity.getBilEftActivityId().getBilTchKeyId(), bilTypeCode, bilEftActivity.getBilEftAcyDt());
        if (bilEftBankList == null || bilEftBankList.isEmpty()) {
            bilEftBankList = bilEftBankRepository.findEftBankEntry(
                    bilEftActivity.getBilEftActivityId().getBilTchKeyId(), bilTypeCode,
                    bilEftActivity.getBilEftTapeDt());
            if (bilEftBankList == null || bilEftBankList.isEmpty()) {
                return null;
            }
        }
        return bilEftBankList.get(SHORT_ZERO);
    }

    private BilBankEntry getBankInformation(BilEftActivity bilEftActivity) {

        String entryNumber = null;
        short entrySequenceNbr = SHORT_ZERO;
        ZonedDateTime entryDate = null;
        String userId = null;

        if (bilEftActivity.getBilEftActivityId().getBilTchKeyTypCd() == BilTechKeyTypeCode.BILL_ACCOUNT.getValue()) {
            if (bilEftActivity.getBilEftDrAmt() < DECIMAL_ZERO) {
                BilActSummary bilActSummary = bilActSummaryRepository
                        .findById(new BilActSummaryId(bilEftActivity.getBilEftActivityId().getBilTchKeyId(),
                                bilEftActivity.getBilEftAcyDt(),
                                bilEftActivity.getBilEftActivityId().getBilDtbSeqNbr()))
                        .orElse(null);
                if (bilActSummary != null && !bilActSummary.getBasAddDataTxt().trim().isEmpty()) {
                    entryDate = DateRoutine.dateTimeAsYYYYMMDD(bilActSummary.getBasAddDataTxt().substring(0, 10));
                    entryNumber = bilActSummary.getBasAddDataTxt().substring(10, 14).trim();
                    userId = bilActSummary.getBasAddDataTxt().substring(14, 22).trim();
                    entrySequenceNbr = Short.valueOf(bilActSummary.getBasAddDataTxt().substring(22, 26));
                } else {
                    throw new InvalidDataException(NO_DATA_FOUND, new Object[] { "BIL_ACT_SUMMARY" });
                }
            } else {
                BilCashReceipt bilCashReceipt = bilCashReceiptRepository
                        .findById(new BilCashReceiptId(bilEftActivity.getBilEftActivityId().getBilTchKeyId(),
                                bilEftActivity.getBilEftActivityId().getBilEftPstPmtDt(),
                                bilEftActivity.getBilEftActivityId().getBilDtbSeqNbr()))
                        .orElse(null);
                if (bilCashReceipt != null) {
                    entryNumber = bilCashReceipt.getEntryNumber();
                    entrySequenceNbr = bilCashReceipt.getEntrySequenceNumber();
                    entryDate = bilCashReceipt.getEntryDate();
                    userId = bilCashReceipt.getUserId();
                } else {
                    throw new InvalidDataException(NO_DATA_FOUND, new Object[] { "BIL_CASH_RECEIPT" });

                }
            }

        } else if (bilEftActivity.getBilEftActivityId().getBilTchKeyTypCd() == BilTechKeyTypeCode.UNIDENTIFIED
                .getValue()) {
            BilUnIdCash bilUnIdCash = getBilUnIdCash(bilEftActivity.getBilEftActivityId().getBilTchKeyId());

            if (bilUnIdCash != null) {
                entryNumber = bilUnIdCash.getBilUnIdCashId().getBilEntryNbr();
                entrySequenceNbr = bilUnIdCash.getBilEntrySeqNbr();
                entryDate = bilUnIdCash.getBilUnIdCashId().getBilEntryDt();
                userId = bilUnIdCash.getBilUnIdCashId().getUserId();
                getIdentifier(bilUnIdCash);
            } else {
                throw new InvalidDataException(NO_DATA_FOUND, new Object[] { "BIL_UN_ID_CASH" });
            }
        } else if (bilEftActivity.getBilEftActivityId().getBilTchKeyTypCd() == BilTechKeyTypeCode.THIRD_PARTY
                .getValue()) {
            if (bilEftActivity.getBilEftDrAmt() < DECIMAL_ZERO) {
                BilTtySummary bilTtySummary = bilTtySummaryRepository
                        .findById(new BilTtySummaryId(bilEftActivity.getBilEftActivityId().getBilTchKeyId(),
                                bilEftActivity.getBilEftAcyDt(),
                                bilEftActivity.getBilEftActivityId().getBilDtbSeqNbr()))
                        .orElse(null);
                if (bilTtySummary != null && !bilTtySummary.getBasAddDataTxt().trim().isEmpty()) {
                    entryDate = DateRoutine.dateTimeAsYYYYMMDD(bilTtySummary.getBasAddDataTxt().substring(0, 10));
                    entryNumber = bilTtySummary.getBasAddDataTxt().substring(10, 14).trim();
                    userId = bilTtySummary.getBasAddDataTxt().substring(14, 22).trim();
                    entrySequenceNbr = Short.valueOf(bilTtySummary.getBasAddDataTxt().substring(22, 26));
                } else {
                    throw new InvalidDataException(NO_DATA_FOUND, new Object[] { "BIL_ACT_SUMMARY" });
                }
            } else {
                BilTtyCashRct bilTtyCashRct = bilTtyCashRctRepository
                        .findById(new BilTtyCashRctId(bilEftActivity.getBilEftActivityId().getBilTchKeyId(),
                                bilEftActivity.getBilEftActivityId().getBilEftPstPmtDt(),
                                bilEftActivity.getBilEftActivityId().getBilDtbSeqNbr()))
                        .orElse(null);
                if (bilTtyCashRct != null) {
                    entryNumber = bilTtyCashRct.getBilEntryNbr();
                    entrySequenceNbr = bilTtyCashRct.getBilEntrySeqNbr();
                    entryDate = bilTtyCashRct.getBilEntryDt();
                    userId = bilTtyCashRct.getUserId();
                } else {
                    throw new InvalidDataException(NO_DATA_FOUND, new Object[] { "BIL_TTY_CASH_RCT" });

                }
            }
        }

        return bilBankEntryRepository.findById(new BilBankEntryId(entryDate, entryNumber, userId, entrySequenceNbr))
                .orElse(null);

    }

    private BilUnIdCash getBilUnIdCash(String bilTchKeyId) {
        List<BilUnIdCash> bilUnIdCashList = bilUnIdCashRepository.findByBillUnidCashId(bilTchKeyId);
        if (bilUnIdCashList != null && !bilUnIdCashList.isEmpty()) {
            return bilUnIdCashList.get(SHORT_ZERO);
        }
        return null;
    }

    private void getIdentifier(BilUnIdCash bilUnIdCash) {

        if (!bilUnIdCash.getPolNbr().trim().isEmpty()) {
            unidCashDir = bilUnIdCash.getPolNbr().trim();
        } else if (!bilUnIdCash.getBilAccountNbr().trim().isEmpty()) {
            unidCashDir = bilUnIdCash.getBilAccountNbr().trim();
        } else if (!bilUnIdCash.getBilRctId().trim().isEmpty()) {
            unidCashDir = bilUnIdCash.getBilRctId().trim();
        }
    }

    private boolean checkAgencySweepConfig() {

        logger.debug("Enter checkAgencySweepConfig method");

        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("AGSW", SEPARATOR_BLANK, SEPARATOR_BLANK,
                SEPARATOR_BLANK);

        boolean excdContracts = false;
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y && !bilRulesUct.getBrtParmListTxt().isEmpty()
                && bilRulesUct.getBrtParmListTxt().length() > 6) {
            char excludeContracts = bilRulesUct.getBrtParmListTxt().charAt(6);
            if (excludeContracts == CHAR_Y) {
                excdContracts = true;
            }

        }
        logger.debug("Exit checkAgencySweepConfig method");
        return excdContracts;

    }

    private String getAgencyBank(ZonedDateTime eftAcyDate, String agencyNumber, boolean excdContracts) {

        String agencyClientId = null;

        if (excdContracts) {
            agencyClientId = getAgencyClientIdFromCcm(agencyNumber);
        } else {
            agencyClientId = userPartySecurityService.getAgencyClientIdFromClienReference(eftAcyDate, agencyNumber,
                    agencyClientId);
        }

        return agencyClientId;
    }

    private String getAgencyClientIdFromCcm(String agencyNumber) {

        String clientId = SEPARATOR_BLANK;

        List<CcmAgency> ccmAgencyList = ccmAgencyRepository.findByAgencyNumber(agencyNumber);
        if (ccmAgencyList != null && !ccmAgencyList.isEmpty()) {
            clientId = ccmAgencyList.get(SHORT_ZERO).getCagAgcCltId();
        }

        return clientId;
    }

    private CltBankAccount getPolicyDataRetrieveAgent(String agencyClientId, ZonedDateTime bilEftAcyDt) {

        logger.debug("Enter getPolicyDataRetrieveAgent method");

        CltBankAccount cltBankAccount = null;

        List<Object[]> effectiveDateAndSequenceNumberList = cltBankAccountRepository
                .getMaxEffectiveDateAndAccountSequenceNumber(agencyClientId, SHORT_ZERO, bilEftAcyDt);

        if (effectiveDateAndSequenceNumberList != null && !effectiveDateAndSequenceNumberList.isEmpty()
                && effectiveDateAndSequenceNumberList.get(0)[0] != null
                && effectiveDateAndSequenceNumberList.get(0)[1] != null) {
            ZonedDateTime effectiveDate = (ZonedDateTime) effectiveDateAndSequenceNumberList.get(0)[0];
            short accountSequenceNumber = (short) effectiveDateAndSequenceNumberList.get(0)[1];

            List<CltBankAccount> cltBankAccountList = cltBankAccountRepository.fetchPolicyDataRetrieve(agencyClientId,
                    SHORT_ZERO, effectiveDate, accountSequenceNumber, PageRequest.of(SHORT_ZERO, SHORT_ONE));

            if (cltBankAccountList == null || cltBankAccountList.isEmpty()) {
                throw new InvalidDataException(NO_DATA_FOUND, new Object[] { "Clt Bank Account" });

            } else {
                cltBankAccount = cltBankAccountList.get(SHORT_ZERO);
            }
        }

        logger.debug("Exit getPolicyDataRetrieveAgent method");
        return cltBankAccount;
    }

    private BilEftRejBch insertBilEftRejBch(String bankAccountNumber, String unidCashDir, Double dueAmount,
            String cardTypeCode, String traceNumber, BilEftActivity bilEftActivity, RejectionPayment rejectionPayment,
            String payorName) {

        BilEftRejBch bilEftRejBch = new BilEftRejBch();
        bilEftRejBch.setBilTtyNbr(BLANK_STRING);
        bilEftRejBch.setBilThirdPartyId(BLANK_STRING);
        bilEftRejBch.setBilAccountNumber(BLANK_STRING);
        bilEftRejBch.setBilAccountId(BLANK_STRING);
        bilEftRejBch.setBilUnidCashId(BLANK_STRING);
        if (bilEftActivity.getBilEftActivityId().getBilTchKeyTypCd() == BilTechKeyTypeCode.BILL_ACCOUNT.getValue()) {
            BilAccount bilAccount = bilAccountRepository.findById(bilEftActivity.getBilEftActivityId().getBilTchKeyId())
                    .orElse(null);
            if (bilAccount != null) {
                bilEftRejBch.setBilAccountNumber(bilAccount.getAccountNumber());
                bilEftRejBch.setBilAccountId(bilEftActivity.getBilEftActivityId().getBilTchKeyId());
                if (payorName.trim().isEmpty()) {
                    bilEftRejBch.setBilPayLngNm(getPayorName(bilAccount.getPayorClientId().trim()));
                } else {
                    bilEftRejBch.setBilPayLngNm(payorName.trim());
                }

            } else {
                throw new InvalidDataException(NO_DATA_FOUND, new Object[] { "BIL_ACCOUNT" });
            }
        } else if (bilEftActivity.getBilEftActivityId().getBilTchKeyTypCd() == BilTechKeyTypeCode.THIRD_PARTY
                .getValue()) {
            BilThirdParty bilThirdParty = bilThirdPartyRepository
                    .findById(bilEftActivity.getBilEftActivityId().getBilTchKeyId()).orElse(null);
            if (bilThirdParty != null) {
                bilEftRejBch.setBilTtyNbr(bilThirdParty.getBilTtyNbr());
                bilEftRejBch.setBilThirdPartyId(bilEftActivity.getBilEftActivityId().getBilTchKeyId());
                if (payorName.trim().isEmpty()) {
                    bilEftRejBch.setBilPayLngNm(getPayorName(bilThirdParty.getBtpTtyClientId().trim()));
                } else {
                    bilEftRejBch.setBilPayLngNm(payorName.trim());
                }

            } else {
                throw new InvalidDataException(NO_DATA_FOUND, new Object[] { "BIL_THIRD_PARTY" });
            }
        } else if (bilEftActivity.getBilEftActivityId().getBilTchKeyTypCd() == BilTechKeyTypeCode.UNIDENTIFIED
                .getValue()) {
            bilEftRejBch.setBilUnidCashId(bilEftActivity.getBilEftActivityId().getBilTchKeyId());
            bilEftRejBch.setBilPayLngNm(payorName.trim());
        }

        bilEftRejBch.setBilEftDrDt(rejectionPayment.getSettlementDate());
        bilEftRejBch.setBilEftTraceNbr(traceNumber);
        bilEftRejBch.setBilEftRejCd(rejectionPayment.getRejectionReason());
        bilEftRejBch.setBrbAceDt(DateRoutine.defaultDateTime());
        bilEftRejBch.setBrbReviewCd(BLANK_STRING);
        bilEftRejBch.setBrbRejStaInd(CHAR_O);
        bilEftRejBch.setBilCrCrdTypeCd(cardTypeCode.charAt(SHORT_ZERO));
        bilEftRejBch.setBilUnidCashDir(unidCashDir);
        bilEftRejBch.setAgencyNbr(BLANK_STRING);
        bilEftRejBch.setBilEftDrAmt(dueAmount);
        bilEftRejBch.setBilBankAccNbr(bankAccountNumber);
        bilEftRejBch.setAgencyNbr(bilEftActivity.getAgencyNbr());

        return bilEftRejBch;
    }

    private String getPayorName(String clientId) {
        try {
            Party partySearch = partyElasticSearchServiceImpl.fetchPartyDocument(clientId.trim());
            if (partySearch != null) {
                return partySearch.getName();
            }
        } catch (IOException e) {
            logger.error("Error in fetching the Party Document");
        }
        return null;
    }

    private String getIdentifierTypeDescription(String accountType) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(accountType,
                BusCodeTranslationType.BILLACCOUNTTYPE.getValue(), execContext.getLanguage());
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusDescription();
        }
        return null;
    }

}
