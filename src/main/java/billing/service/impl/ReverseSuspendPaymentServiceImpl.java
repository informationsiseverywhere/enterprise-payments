package billing.service.impl;

import static billing.utils.BillingConstants.COMPANY_LOCATION_NUMBER;
import static billing.utils.BillingConstants.ERROR_CODE;
import static billing.utils.BillingConstants.MASTER_COMPANY_NUMBER;
import static billing.utils.BillingConstants.PLUS_SIGN;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_I;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_U;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_10;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import application.security.service.impl.AppSecurityServiceImpl;
import application.utils.service.ActivityService;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilEftActivity;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.BilObjectCfg;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.BilUnidCshHst;
import billing.data.entity.id.BilCashEntryId;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilObjectCfgId;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.entity.id.BilUnIdCshHstId;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilEftActivityRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilObjectCfgRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.data.repository.BilUnidCshHstRepository;
import billing.model.ReverseOption;
import billing.service.BilRulesUctService;
import billing.service.BillingAccountBalanceService;
import billing.service.ReverseSuspendPaymentService;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilRules;
import billing.utils.BillingConstants.BillingEftTransactionStatus;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CashEntryMethod;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.TechnicalKeyTypeCode;
import billing.utils.BillingConstants.WipAcitivityId;
import billing.utils.BillingConstants.WipObjectCode;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.schedule.service.ScheduleService;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.CommonConstants;
import core.utils.DateRoutine;
import core.utils.IdGenerator;
import disbursement.data.entity.DwsDisbursement;
import disbursement.service.DisbursementAPIService;
import disbursement.utils.DisbursementConstants.DisbursementStatusCode;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class ReverseSuspendPaymentServiceImpl implements ReverseSuspendPaymentService {

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private BilUnidCshHstRepository bilUnidCshHstRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private DateService dateService;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private BillingAccountBalanceService billingAccountBalanceService;

    @Autowired
    private DisbursementAPIService disbursementAPIService;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilObjectCfgRepository bilObjectCfgRepository;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilEftActivityRepository bilEftActivityRepository;

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    private static final Logger logger = LogManager.getLogger(ReverseSuspendPaymentServiceImpl.class);
    private static final String CASH_TRANSFER_REVERSAL = "BCMOTRP";
    private static final String WIP_POST = "WIPOPST";
    private static final List<Character> EFT_RECORDTYPE_CODES = Arrays.asList(EftRecordType.ONE_TIME_ACH.getValue(),
            EftRecordType.AGENCY_SWEEP.getValue(), EftRecordType.RECONCILED_CASH.getValue(),
            EftRecordType.UNIDENTIFIED.getValue());

    private static final List<Character> ECC_RECORDTYPE_CODES = Arrays.asList(EftRecordType.RECONCILED_CASH.getValue(),
            EftRecordType.UNIDENTIFIED.getValue());

    @Override
    @Transactional
    public void save(Short sequenceNumber, ZonedDateTime postedDate, String userId, String batchId,
            ZonedDateTime distributionDate, ReverseOption reverseOption) {
        logger.info("reversal service : Start");

        String disbursementId = null;
        String currencyCode = BLANK_STRING;

        String userSequenceId = appSecurityServiceImpl.authenticateUser(userId).getUserSequenceId();
        BilUnIdCash bilUnidentifiedCash = getUnidentifiedCash(distributionDate, sequenceNumber, batchId, userSequenceId,
                postedDate);
        if (bilUnidentifiedCash == null) {
            throw new DataNotFoundException("no.data.found");
        }

        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId()))
                .orElse(null);
        if (bilCashEntryTot != null) {
            currencyCode = bilCashEntryTot.getCurrencyCd();
        }

        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct(BilRules.UNIDENTIFIED_CASH_HISTORY.getValue(),
                BLANK_STRING, BLANK_STRING, BLANK_STRING);

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            disbursementId = saveUnidentifiedCashHistory(bilUnidentifiedCash);
        }

        char statusCode = bilUnidentifiedCash.getDwsStatusCd();
        BigDecimal activityAmount = BigDecimal.valueOf(bilUnidentifiedCash.getBilRctAmt());
        String agentThirdPartyId = getAgentThirdPartyId(bilUnidentifiedCash.getAgencyNbr().trim(),
                bilUnidentifiedCash.getBilTtyNbr().trim());

        logger.debug(
                "If receipt is in status that can be reversed then insert negative Unidentified-DisburseAuthorized FIS row");
        saveFinancialActivityByStatus(bilUnidentifiedCash, activityAmount, agentThirdPartyId, disbursementId,
                reverseOption, currencyCode);

        switch (BilDspTypeCode.getEnumKey(bilUnidentifiedCash.getBilDspTypeCd())) {
        case SUSPENDED:

            logger.debug("insert UnidentifiedCash-ReverseSuspense FIS row");
            saveFinancialActivity(bilUnidentifiedCash, ObjectCode.UNIDENTIFIED_CASH.toString(),
                    ActionCode.REVERSE_SUSPENSE.toString(), activityAmount, bilUnidentifiedCash.getPolNbr(),
                    bilUnidentifiedCash.getBilAccountNbr(), bilUnidentifiedCash.getBilRctTypeCd(), agentThirdPartyId,
                    BLANK_STRING, bilUnidentifiedCash.getBilBankCd(), reverseOption, currencyCode);
            break;

        case DISBURSED:
            String actionCode;

            if (statusCode == DisbursementStatusCode.DISBURSED.toChar()
                    || statusCode == DisbursementStatusCode.STOPPAYREQUEST.toChar()
                    || statusCode == DisbursementStatusCode.HONORED.toChar()
                    || statusCode == DisbursementStatusCode.ESCHEATWAIT.toChar()
                    || statusCode == DisbursementStatusCode.ESCHEATED.toChar()) {

                actionCode = ActionCode.REVERSE_DISBURSEMENT.toString();
                activityAmount = activityAmount.negate();
                disbursementRebillWarning(bilUnidentifiedCash);

            } else if (statusCode == DisbursementStatusCode.PENDING.toChar()
                    || statusCode == DisbursementStatusCode.SELECTCOMBINED.toChar()
                    || statusCode == DisbursementStatusCode.COMBINEDPAYMENT.toChar()
                    || statusCode == DisbursementStatusCode.HOLD.toChar()) {

                actionCode = ActionCode.REVERSE_SUSPENSE.toString();
                logger.debug("call disbursement api service to process disbursement tables");
                processDisbursement(bilUnidentifiedCash);

            } else {
                actionCode = ActionCode.REVERSE_DISBURSEMENT.toString();
                activityAmount = activityAmount.negate();
            }

            logger.debug("insert UnidentifiedCash-ReverseSuspense/ReverseDisburseAuthor FIS row");
            saveFinancialActivity(bilUnidentifiedCash, ObjectCode.UNIDENTIFIED_CASH.toString(), actionCode,
                    activityAmount, bilUnidentifiedCash.getPolNbr(), bilUnidentifiedCash.getBilAccountNbr(),
                    bilUnidentifiedCash.getBilRctTypeCd(), agentThirdPartyId, BLANK_STRING,
                    bilUnidentifiedCash.getBilBankCd(), reverseOption, currencyCode);
            break;

        case WRITE_OFF:
            logger.debug("insert UnidentifiedCash-ReverseWriteOff FIS row");
            saveFinancialActivity(bilUnidentifiedCash, ObjectCode.UNIDENTIFIED_CASH.toString(),
                    ActionCode.WRITE_OFF.toString(), activityAmount.negate(), bilUnidentifiedCash.getPolNbr(),
                    bilUnidentifiedCash.getBilAccountNbr(), bilUnidentifiedCash.getBilRctTypeCd(), agentThirdPartyId,
                    BLANK_STRING, bilUnidentifiedCash.getBilBankCd(), reverseOption, currencyCode);

            logger.debug("insert UnidentifiedCash-ReverseSuspense FIS row");
            saveFinancialActivity(bilUnidentifiedCash, ObjectCode.UNIDENTIFIED_CASH.toString(),
                    ActionCode.REVERSE_SUSPENSE.toString(), activityAmount, bilUnidentifiedCash.getPolNbr(),
                    bilUnidentifiedCash.getBilAccountNbr(), bilUnidentifiedCash.getBilRctTypeCd(), agentThirdPartyId,
                    BLANK_STRING, bilUnidentifiedCash.getBilBankCd(), reverseOption, currencyCode);
            break;

        default:
            break;

        }

        boolean isPendingAch = checkForPendingAchRow(bilUnidentifiedCash);

        if (!isPendingAch) {
            updateBilEftActivityStatus(bilUnidentifiedCash.getBilCshEtrMthCd(), bilUnidentifiedCash.getBillUnidCashId(),
                    bilUnidentifiedCash.getBilDsbId());

            logger.debug("insert Cash-Deposited FIS row");

            String bankCode = getBankCode(bilUnidentifiedCash);
            saveFinancialActivity(bilUnidentifiedCash, ObjectCode.CASH.toString(), ActionCode.DEPOSIT.toString(),
                    activityAmount.negate(), bilUnidentifiedCash.getPolNbr(), BLANK_STRING,
                    bilUnidentifiedCash.getBilRctTypeCd(), BLANK_STRING, BLANK_STRING, bankCode, reverseOption,
                    currencyCode);
        }

        updateUnidentifiedCash(reverseOption, bilUnidentifiedCash);

        logger.info("schedule defer activity for BCMOTRP");
        processCashTransferReversal(batchId, userSequenceId, postedDate, bilUnidentifiedCash.getBilEntrySeqNbr(),
                reverseOption);

        bilRulesUct = bilRulesUctService.readBilRulesUct(BilRules.ACCOUNT_BALANCE.getValue(), BLANK_STRING,
                BLANK_STRING, BLANK_STRING);

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            logger.info("update total unidientified balance in the system.");
            billingAccountBalanceService.saveUnidentifiedBalance();
        }

        logger.info("reversal service : End");

    }

    private void updateBilEftActivityStatus(char cashEntryMethodCode, String technicalKeyId, String disbursementId) {

        if (cashEntryMethodCode == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()) {
            boolean creditCardRow = checkForEftCreditCardPayment(technicalKeyId);
            if (creditCardRow) {
                updateBilEftActivityRows(technicalKeyId, disbursementId);

            }
        } else if (isDigitalWalletPayment(cashEntryMethodCode)) {
            boolean digitalPaymentRow = checkForEftWalletPayment(technicalKeyId);
            if (digitalPaymentRow) {
                updateBilEftActivityRows(technicalKeyId, disbursementId);

            }
        }

    }

    private void updateBilEftActivityRows(String technicalKeyId, String disbursementId) {
        int index = SHORT_ZERO;
        List<BilEftActivity> bilEftActivityList = bilEftActivityRepository.findByIdTechKeyTypeCodeDisburseId(
                technicalKeyId, TechnicalKeyTypeCode.UNIDENTIFIED.getValue(), disbursementId);
        List<BilEftActivity> bilEftActvities = new CopyOnWriteArrayList<>(bilEftActivityList);

        if (!bilEftActvities.isEmpty()) {
            for (BilEftActivity bilEftActivity : bilEftActvities) {
                bilEftActivity.setBeaTrsSta(BillingEftTransactionStatus.REJECTED.getValue());
                bilEftActvities.set(index, bilEftActivity);
                index = index + SHORT_ONE;
            }
            logger.debug("update BilEftAcvitiyStatus");
            bilEftActivityRepository.saveAll(bilEftActvities);
        }
    }

    private boolean checkForEftCreditCardPayment(String technicalKeyId) {

        Integer rowCount = bilEftActivityRepository.checkIfExists(technicalKeyId, ECC_RECORDTYPE_CODES);
        if (rowCount != null && rowCount > CommonConstants.SHORT_ZERO) {
            return true;
        } else {
            rowCount = bilEftPendingTapeRepository.checkIfExists(technicalKeyId, ECC_RECORDTYPE_CODES);
            if (rowCount != null && rowCount > CommonConstants.SHORT_ZERO) {
                return true;
            }
        }
        return false;

    }

    private boolean checkForEftWalletPayment(String technicalKeyId) {

        Integer rowCount = bilEftActivityRepository.checkIfExists(technicalKeyId,
                Arrays.asList(EftRecordType.DIGITAL_WALLET.getValue()));
        if (rowCount != null && rowCount > CommonConstants.SHORT_ZERO) {
            return true;
        } else {
            rowCount = bilEftPendingTapeRepository.checkIfExists(technicalKeyId,
                    Arrays.asList(EftRecordType.DIGITAL_WALLET.getValue()));
            if (rowCount != null && rowCount > CommonConstants.SHORT_ZERO) {
                return true;
            }
        }
        return false;

    }

    private String getBankCode(BilUnIdCash bilUnidentifiedCash) {
        char cashEntryMethodCode = bilUnidentifiedCash.getBilCshEtrMthCd();
        if (cashEntryMethodCode != CashEntryMethod.EFT.getCashEntryMethod()
                && cashEntryMethodCode != CashEntryMethod.CREDIT.getCashEntryMethod()
                && cashEntryMethodCode != CashEntryMethod.EFT_CREDIT_CARD.getCashEntryMethod()
                && cashEntryMethodCode != CashEntryMethod.CREDIT_CARD.getCashEntryMethod()
                && !isDigitalWalletPayment(cashEntryMethodCode)
                && cashEntryMethodCode != CashEntryMethod.TOLERANCE_WRITEOFF.getCashEntryMethod()
                && cashEntryMethodCode != CashEntryMethod.AGENT_ACH.getCashEntryMethod()
                && cashEntryMethodCode != CashEntryMethod.INSURED.getCashEntryMethod()
                && cashEntryMethodCode != CashEntryMethod.ONE_TIME_ACH.getCashEntryMethod()
                && cashEntryMethodCode != CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()) {
            BilCashEntryTot bilCashEntryTotal = bilCashEntryTotRepository
                    .findById(new BilCashEntryTotId(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                            bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                            bilUnidentifiedCash.getBilUnIdCashId().getUserId()))
                    .orElse(null);
            if (bilCashEntryTotal != null) {
                return bilCashEntryTotal.getBilBankCd();
            }
        }

        return bilUnidentifiedCash.getBilBankCd();
    }

    private boolean checkForPendingAchRow(BilUnIdCash bilUnidentifiedCash) {
        char cashEntryMethodCode = bilUnidentifiedCash.getBilCshEtrMthCd();
        if (cashEntryMethodCode == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()
                || cashEntryMethodCode == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()
                || isDigitalWalletPayment(cashEntryMethodCode)
                || cashEntryMethodCode == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()) {

            logger.debug("find pending ach rows");
            List<BilEftPendingTape> bilEftPendingTapeList = bilEftPendingTapeRepository.findUnidentifiedPendingAchRows(
                    bilUnidentifiedCash.getBillUnidCashId(), TechnicalKeyTypeCode.UNIDENTIFIED.getValue(),
                    EFT_RECORDTYPE_CODES, DECIMAL_ZERO);
            if (bilEftPendingTapeList != null && !bilEftPendingTapeList.isEmpty()) {
                logger.debug("delete pending ach rows");
                bilEftPendingTapeRepository.deleteAllInBatch(bilEftPendingTapeList);

                if (bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()) {
                    BilCashEntry bilCashEntry = bilCashEntryRepository
                            .findById(new BilCashEntryId(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                                    bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                                    bilUnidentifiedCash.getBilUnIdCashId().getUserId(),
                                    bilUnidentifiedCash.getBilEntrySeqNbr()))
                            .orElse(null);
                    if (bilCashEntry != null) {
                        callCreditCardReversalUserExit();
                    }
                }
                return true;
            }
        }
        return false;
    }

    private void callCreditCardReversalUserExit() {
        // user exit to be implemented in future as per customer
        // requirement

    }

    private void disbursementRebillWarning(BilUnIdCash bilUnidentifiedCash) {

        BilObjectCfg bilObjectCfg = bilObjectCfgRepository.findById(new BilObjectCfgId(WIP_POST, CHAR_I)).orElse(null);
        if (bilObjectCfg == null) {
            throw new DataNotFoundException("no.data.found");
        }
        if (bilObjectCfg.getBocObjPrcCd() == CHAR_Y) {
            processWipActivities(bilUnidentifiedCash.getBilAccountNbr(), bilUnidentifiedCash.getPolNbr(),
                    bilUnidentifiedCash.getBilAgtActNbr(), bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                    bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                    bilUnidentifiedCash.getBilUnIdCashId().getUserId());
        }

    }

    private void processWipActivities(String accountNumber, String policyNumber, String agencyAccountNumber,
            ZonedDateTime distributionDate, short sequenceNumber, String batchId, ZonedDateTime postedDate,
            String userSequenceId) {

        String technicalObjectKey = generateTechnicalObjectKey(distributionDate, sequenceNumber, batchId, postedDate,
                userSequenceId);
        String activityId = WipAcitivityId.DISBUSRE_REBILL.getValue();

        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct(
                BilRules.AUTOGENERATE_DISBURSEMENT_REBILL.getValue(), BLANK_STRING, BLANK_STRING, BLANK_STRING);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            activityId = WipAcitivityId.AUTO_DISBURSE_REBILL.getValue();
        }

        logger.info("post disburse/autoDisburse Rebill warning to WIP");
        if (accountNumber != null && !accountNumber.trim().isEmpty()) {
            activityService.postActivity(activityId, WipObjectCode.UNIDENTIFIED_CASH.getValue(), accountNumber,
                    technicalObjectKey);
        } else if (policyNumber != null && !policyNumber.trim().isEmpty()) {
            activityService.postActivity(activityId, WipObjectCode.UNIDENTIFIED_CASH.getValue(), policyNumber,
                    technicalObjectKey);
        } else if (agencyAccountNumber != null && !agencyAccountNumber.trim().isEmpty()) {
            activityService.postActivity(activityId, WipObjectCode.UNIDENTIFIED_CASH.getValue(), agencyAccountNumber,
                    technicalObjectKey);
        } else {
            String date = DateRoutine.dateTimeAsYYYYMMDDString(distributionDate);
            activityService.postActivity(activityId, WipObjectCode.UNIDENTIFIED_CASH.getValue(), date, date);
        }

    }

    private void processDisbursement(BilUnIdCash bilUnidentifiedCash) {

        logger.info("Method to check the current status of payment activity.");
        String disbursementReason = bilUnidentifiedCash.getBilDspReasonCd();
        DwsDisbursement dwsDisbusrement = disbursementAPIService.checkDisbursement(bilUnidentifiedCash.getBilDsbId());
        String reason = getDescriptionByCode(disbursementReason, BilDesReasonType.DISBURSE_MANUAL.getValue());
        if (reason != null) {
            disbursementReason = reason;
        }

        logger.info("Method to cancel the disbursement");
        disbursementAPIService.cancelDisbursement(BLANK_STRING, bilUnidentifiedCash.getBilPayeeCltId(),
                disbursementReason, dwsDisbusrement.getDwsMediumCd(), dwsDisbusrement.getDwsPrcLocCd(),
                dwsDisbusrement.getDdsDspAmt(), dwsDisbusrement.getDwsCkDrfNbr(), bilUnidentifiedCash.getBilDsbId(),
                Character.toString(CHAR_R), bilUnidentifiedCash.getBilPayeeAdrSeq());

    }

    private void processCashTransferReversal(String batchId, String userSequenceId, ZonedDateTime postedDate,
            Short sequenceNumber, ReverseOption reverseOption) {
        StringBuilder userData = new StringBuilder();
        userData.append(StringUtils.rightPad("00", 2, BLANK_CHAR));
        userData.append(StringUtils.rightPad(BLANK_STRING, 10, BLANK_CHAR));
        userData.append(CHAR_U);
        userData.append(StringUtils.rightPad(BLANK_STRING, 8, BLANK_CHAR));
        userData.append(
                StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()), 10));
        userData.append(StringUtils.rightPad(batchId, 4, BLANK_CHAR));
        userData.append(StringUtils.rightPad(userSequenceId, 8, BLANK_CHAR));
        userData.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(postedDate), 10, BLANK_CHAR));
        userData.append(StringUtils.leftPad(String.valueOf(sequenceNumber.intValue()), 2, CHAR_ZERO));
        userData.append(StringUtils.rightPad(execContext.getUserSeqeunceId(), 8, BLANK_CHAR));
        userData.append(StringUtils.rightPad(reverseOption.getReason(), 3, BLANK_CHAR));
        userData.append(StringUtils.rightPad(reverseOption.getDepositDate(), 10, BLANK_CHAR));
        scheduleService.scheduleDeferActivity(CASH_TRANSFER_REVERSAL, userData.toString());

    }

    private BilUnIdCash getUnidentifiedCash(ZonedDateTime distributionDate, Short sequenceNumber, String batchId,
            String userSequenceId, ZonedDateTime postedDate) {
        logger.debug("fetch row from bilUnidCash");
        return bilUnIdCashRepository
                .findById(new BilUnIdCashId(distributionDate, sequenceNumber, batchId, userSequenceId, postedDate))
                .orElse(null);
    }

    private void saveFinancialActivityByStatus(BilUnIdCash bilUnidentifiedCash, BigDecimal activityAmount,
            String agentThirdPartyId, String disbursementId, ReverseOption reverseOption, String currencyCode) {
        if (bilUnidentifiedCash.getDwsStatusCd() == DisbursementStatusCode.PENDING.toChar()
                || bilUnidentifiedCash.getDwsStatusCd() == DisbursementStatusCode.SELECTCOMBINED.toChar()
                || bilUnidentifiedCash.getDwsStatusCd() == DisbursementStatusCode.COMBINEDPAYMENT.toChar()
                || bilUnidentifiedCash.getDwsStatusCd() == DisbursementStatusCode.HOLD.toChar()) {

            String actionCode = ActionCode.AUTHOR_DISBURSEMENT.toString();
            if (bilUnidentifiedCash.getBilDspTypeCd().equals(BilDspTypeCode.SUSPENDED.getValue())) {
                actionCode = ActionCode.WRITE_OFF.toString();
            }
            saveFinancialActivity(bilUnidentifiedCash, ObjectCode.UNIDENTIFIED_CASH.toString(), actionCode,
                    activityAmount.negate(), bilUnidentifiedCash.getPolNbr(), bilUnidentifiedCash.getBilAccountNbr(),
                    bilUnidentifiedCash.getBilRctTypeCd(), agentThirdPartyId, disbursementId,
                    bilUnidentifiedCash.getBilBankCd(), reverseOption, currencyCode);

        }

    }

    private String getAgentThirdPartyId(String agencyNbr, String thirdPartyNumber) {
        if (!agencyNbr.isEmpty()) {
            return agencyNbr;
        } else if (!thirdPartyNumber.isEmpty()) {
            return thirdPartyNumber;
        }
        return BLANK_STRING;
    }

    private void saveFinancialActivity(BilUnIdCash bilUnidentifiedCash, String objectCode, String actionCode,
            BigDecimal activityAmount, String policyId, String accountId, String receiptTypeCode,
            String agentThirdPartyId, String disbursementId, String bankCode, ReverseOption reverseOption,
            String currencyCode) {
        logger.info("save finacial activity start");

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setFunctionCode(CHAR_A);
        financialApiActivity.setApplicationName("BCWSUNID");
        financialApiActivity.setUserId(bilUnidentifiedCash.getActivityUserId());
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setTransactionObjectCode(objectCode);
        financialApiActivity.setActivityAmount(activityAmount.doubleValue());
        financialApiActivity.setActivityNetAmount(activityAmount.doubleValue());
        financialApiActivity.setTransactionActionCode(actionCode);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(CHAR_Y);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setOperatorId(bilUnidentifiedCash.getActivityUserId());
        financialApiActivity.setAppProgramId(ApplicationProgramIdentifier.BACPURR.toString());
        if (policyId != null && policyId.length() > 16) {
            financialApiActivity.setPolicyId(policyId.substring(0, 16));
        } else {
            financialApiActivity.setPolicyId(policyId);
        }
        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setReferenceDate(reverseOption.getDepositDate());
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setBilAccountId(accountId);
        financialApiActivity.setBilReasonCode(reverseOption.getReason());
        financialApiActivity.setBilReceiptTypeCode(receiptTypeCode);
        financialApiActivity.setCurrencyCode(currencyCode);

        financialApiActivity
                .setBilDatabaseKey(generateDatabaseKey(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(), bilUnidentifiedCash.getBilEntrySeqNbr()));
        financialApiActivity.setAgentTtyId(agentThirdPartyId);
        financialApiActivity.setBilSourceCode(String.valueOf(bilUnidentifiedCash.getBilCshEtrMthCd()));
        financialApiActivity.setDisbursementId(disbursementId);

        String[] fwsReturnMessage = null;
        fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                execContext.getApplicationDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
        logger.info("save finacial activity end");

    }

    private String saveUnidentifiedCashHistory(BilUnIdCash bilUnidentifiedCash) {
        logger.debug("insert into unidentified cash history");
        String disbursementId = BLANK_STRING;
        if (bilUnidentifiedCash.getBilDspTypeCd().equals(BilDspTypeCode.DISBURSED.getValue())) {
            disbursementId = bilUnidentifiedCash.getBilDsbId();
        }
        BilUnidCshHst unidentifiedCashHistory = new BilUnidCshHst();
        unidentifiedCashHistory
                .setBilUnIdCshHstId(new BilUnIdCshHstId(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(), bilUnidentifiedCash.getBilAcyTs()));
        unidentifiedCashHistory.setBilAccountNbr(bilUnidentifiedCash.getBilAccountNbr());
        unidentifiedCashHistory.setActivityUserId(bilUnidentifiedCash.getActivityUserId());
        unidentifiedCashHistory.setBilAdditionalId(bilUnidentifiedCash.getBilAdditionalId());
        unidentifiedCashHistory.setBilAdjDueDt(bilUnidentifiedCash.getBilAdjDueDt());
        unidentifiedCashHistory.setBilAgtActNbr(bilUnidentifiedCash.getBilAgtActNbr());
        unidentifiedCashHistory.setBilChkPrdMthCd(bilUnidentifiedCash.getBilChkPrdMthCd());
        unidentifiedCashHistory.setBilCwaId(bilUnidentifiedCash.getBilCwaId());
        unidentifiedCashHistory.setBilDepositDt(bilUnidentifiedCash.getBilDepositDt());
        unidentifiedCashHistory.setBilDspDt(bilUnidentifiedCash.getBilDspDt());
        unidentifiedCashHistory.setBilDspReasonCd(bilUnidentifiedCash.getBilDspReasonCd());
        unidentifiedCashHistory.setBilDspTypeCd(bilUnidentifiedCash.getBilDspTypeCd());
        unidentifiedCashHistory.setBilEntrySeqNbr(bilUnidentifiedCash.getBilEntrySeqNbr());
        unidentifiedCashHistory.setBilManualSusInd(bilUnidentifiedCash.getBilManualSusInd());
        unidentifiedCashHistory.setBilPayeeAdrSeq(bilUnidentifiedCash.getBilPayeeAdrSeq());
        unidentifiedCashHistory.setBilPayeeCltId(bilUnidentifiedCash.getBilPayeeCltId());
        unidentifiedCashHistory.setBilPblItemCd(bilUnidentifiedCash.getBilPblItemCd());
        unidentifiedCashHistory.setBilRctAmt(bilUnidentifiedCash.getBilRctAmt());
        unidentifiedCashHistory.setBilRctCmt(bilUnidentifiedCash.getBilRctCmt());
        unidentifiedCashHistory.setBilRctId(bilUnidentifiedCash.getBilRctId());
        unidentifiedCashHistory.setBilRctTypeCd(bilUnidentifiedCash.getBilRctTypeCd());
        unidentifiedCashHistory.setBilTtyNbr(bilUnidentifiedCash.getBilTtyNbr());
        unidentifiedCashHistory.setDdsDsbDt(bilUnidentifiedCash.getDdsDsbDt());
        unidentifiedCashHistory.setDwsCkDrfNbr(bilUnidentifiedCash.getDwsCkDrfNbr());
        unidentifiedCashHistory.setDwsStatusCd(BLANK_CHAR);
        unidentifiedCashHistory.setPolNbr(bilUnidentifiedCash.getPolNbr());
        unidentifiedCashHistory.setPolSymbolCd(bilUnidentifiedCash.getPolSymbolCd());
        unidentifiedCashHistory.setBilDsbId(disbursementId);
        bilUnidCshHstRepository.save(unidentifiedCashHistory);
        return disbursementId;
    }

    private void updateUnidentifiedCash(ReverseOption reverseOption, BilUnIdCash bilUnidentifiedCash) {
        logger.debug("update unidentified cash");
        bilUnidentifiedCash.setBilDspTypeCd(BilDspTypeCode.REVERSED.getValue());
        bilUnidentifiedCash.setBilDspReasonCd(reverseOption.getReason());
        bilUnidentifiedCash.setBilDepositDt(DateRoutine.dateTimeAsYYYYMMDD(reverseOption.getDepositDate()));
        bilUnidentifiedCash.setBilAcyTs(dateService.currentDateTime());
        bilUnidentifiedCash.setBilDspDt(execContext.getApplicationDate());
        bilUnidentifiedCash.setActivityUserId(execContext.getUserSeqeunceId());
        bilUnIdCashRepository.saveAndFlush(bilUnidentifiedCash);

    }

    private String generateBusinessCaseId() {
        String businessCaseId = IdGenerator.generateUniqueIdUpperCase(GENERATED_ID_LENGTH_10);
        StringBuilder sb = new StringBuilder(20);
        sb.append(businessCaseId);
        sb.append(CommonConstants.SEPARATOR_BLANK);
        sb.append(CommonConstants.SEPARATOR_BLANK);
        sb.append(FinancialIntegratorConstants.ApplicationName.BCMSDISB.toString());
        return sb.toString();
    }

    private String generateTechnicalObjectKey(ZonedDateTime distributionDate, Short sequenceNumber, String entryNumber,
            ZonedDateTime entryDate, String userSequenceId) {
        StringBuilder technicalObjectKey = new StringBuilder(36);
        technicalObjectKey.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(distributionDate), 10));
        technicalObjectKey.append(StringUtils.leftPad(sequenceNumber.toString(), 4, CHAR_ZERO));
        technicalObjectKey.append(BLANK_CHAR);
        technicalObjectKey.append(StringUtils.rightPad(entryNumber, 4));
        technicalObjectKey.append(StringUtils.rightPad(userSequenceId, 8));
        technicalObjectKey.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        return technicalObjectKey.toString();
    }

    private String generateDatabaseKey(ZonedDateTime entryDate, String entryNumber, String userSequenceId,
            Short entrySequenceNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        sb.append(StringUtils.rightPad(entryNumber, 4));
        sb.append(StringUtils.rightPad(userSequenceId, 8));
        sb.append(PLUS_SIGN);
        sb.append(StringUtils.rightPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
        return sb.toString();
    }

    private String getDescriptionByCode(String busCode, String busCodeType) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(busCode, busCodeType,
                execContext.getLanguage());
        if (busCdTranslation != null) {
            return busCdTranslation.getBusDescription().trim();
        }
        return null;
    }

    private boolean isDigitalWalletPayment(Character cashEntryMethodCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(
                String.valueOf(cashEntryMethodCode), BusCodeTranslationType.DIGITAL_WALLET_TYPE.getValue(),
                execContext.getLanguage());

        return busCdTranslation != null ? Boolean.TRUE : Boolean.FALSE;
    }

}
