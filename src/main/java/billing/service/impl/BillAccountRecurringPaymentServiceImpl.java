package billing.service.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActRules;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilAgent;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilEftBankEd;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.BilEftPlan;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.BilThirdParty;
import billing.data.entity.CustomerSubscriptionProfile;
import billing.data.entity.HalBoMduXrf;
import billing.data.entity.HolidayPrc;
import billing.data.entity.id.BilActRulesId;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilEftBankId;
import billing.data.entity.id.BilEftPendingTapeId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.BilSupportPlanId;
import billing.data.entity.id.CustomerSubscriptionProfileId;
import billing.data.entity.id.HolidayPrcId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActRulesRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilAgentRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilDatesRepository;
import billing.data.repository.BilEftBankEdRepository;
import billing.data.repository.BilEftBankRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilEftPlanRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilSupportPlanRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.data.repository.CustomerSubscriptionProfileRepository;
import billing.data.repository.HalBoMduXrfRepository;
import billing.data.repository.HolidayPrcRepository;
import billing.handler.model.AccountAdjustDates;
import billing.handler.model.AccountProcessingAndRecovery;
import billing.handler.service.AccountProcessingAndRecoveryService;
import billing.handler.service.AllDatesResetService;
import billing.model.CashApplyDriver;
import billing.model.Event;
import billing.model.Payment;
import billing.service.BilRulesUctService;
import billing.service.CashApplyDriverService;
import billing.service.EventPostingService;
import billing.service.PaymentService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.AccountStatus;
import billing.utils.BillingConstants.AccountTypeCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BillingMethod;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.EftNoticeIndicator;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.PaymentProviderType;
import billing.utils.BillingConstants.PaymentType;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.schedule.service.ScheduleService;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;

@Service("bilAccountRecurringPaymentService")
public class BillAccountRecurringPaymentServiceImpl {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilActRulesRepository bilActRulesRepository;

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BilEftPlanRepository bilEftPlanRepository;

    @Autowired
    private BilDatesRepository bilDatesRepository;

    @Autowired
    private HolidayPrcRepository holidayPrcRepository;

    @Autowired
    private BilSupportPlanRepository bilSupportPlanRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private EventPostingService eventPostingService;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private HalBoMduXrfRepository halBoMduXrfRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    private BilAgentRepository bilAgentRepository;

    @Autowired
    private CashApplyDriverService cashApplyDriverService;

    @Autowired
    private AccountProcessingAndRecoveryService accountProcessingAndRecoveryService;

    @Autowired
    private AllDatesResetService allResetService;

    @Autowired
    private BilEftBankEdRepository bilEftBankEdRepository;

    @Autowired
    private CustomerSubscriptionProfileRepository customerSubscriptionProfileRepository;

    @Value("${paymentIntegration.provider}")
    String providerIntegration;

    @Value("${autopayment.integration}")
    private String autoPaymentIntegration;

    private static final char ACTIVE_STATUS = CHAR_A;
    private static final char PENDING_STATUS = CHAR_P;
    private static final String EFT_COLLECTION_METHOD = "CET";
    private static final String COLLECTION_METHOD = "CHC";

    public static final Logger logger = LogManager.getLogger(BillAccountRecurringPaymentServiceImpl.class);

    private static final List<Character> INVOICE_CODES = Arrays.asList(
            InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());
    private short bilAcySeq;

    @Transactional
    public Payment processRecurringPayments(Payment payment) {

        boolean deftDate = false;
        boolean deftSameDay = false;
        boolean deftRule = false;
        ZonedDateTime invoiceDate = null;
        ZonedDateTime adjustDate = null;
        ZonedDateTime tapeDate = null;
        short fileTypeCode;
        bilAcySeq = SHORT_ZERO;
        BilAccount bilAccount = bilAccountRepository.findById(payment.getAccountId()).orElse(null);

        if (!bilAccount.getCollectionMethod().trim().equals(payment.getCollectionMethod())) {
            String oldCollectionType = getColllectionType(bilAccount.getCollectionMethod(), COLLECTION_METHOD);
            String newCollectionType = getColllectionType(payment.getCollectionMethod(), EFT_COLLECTION_METHOD);
            String additionalText = bilAccount.getCollectionMethod() + payment.getCollectionMethod();
            if (bilAccount.getStatus() == AccountStatus.PENDING.toChar() || bilAccount.getStatus() == 'C') {
                throw new DataNotFoundException("bad.account.status");
            }
            if (!(oldCollectionType.equals(EFT_COLLECTION_METHOD) && newCollectionType.equals(EFT_COLLECTION_METHOD))) {
                if (!bilAccount.getCollectionMethod().equals(payment.getCollectionMethod())) {
                    insertBilActSummary(payment.getAccountId(), additionalText, "BMC", DateRoutine.defaultDateTime(),
                            DateRoutine.defaultDateTime(), DECIMAL_ZERO, BLANK_STRING);
                }
                bilAccount.setCollectionMethod(payment.getCollectionMethod());
                bilAccount.setCollectionPlan(payment.getCollectionType());
                bilAccountRepository.saveAndFlush(bilAccount);

                BilEftPlan bilEftPlan = readBilEftPlan(payment);
                char statusCode = getAccountStatus(bilEftPlan);
                insertBilEftBank(payment, bilAccount, statusCode);
                if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
                    fileTypeCode = determineFileTypeCode(payment.getPaymentType(), "1XA");
                } else {
                    fileTypeCode = bilEftPlan.getBilFileTypeCd();
                }

                BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("DEFT", bilAccount.getBillTypeCd(),
                        bilAccount.getBillClassCd(), BLANK_STRING);
                if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                    deftRule = true;
                    if (bilRulesUct.getBrtParmListTxt().charAt(SHORT_ZERO) == CHAR_Y) {
                        deftDate = true;
                    }
                    Object[] maxDates = checkDates(deftDate, statusCode, payment, deftRule);
                    deftRule = (boolean) maxDates[0];
                    invoiceDate = (ZonedDateTime) maxDates[1];
                    adjustDate = (ZonedDateTime) maxDates[2];
                    if (invoiceDate != null && adjustDate != null) {
                        Object[] tapeDates = getTapeDate(deftDate, deftRule, bilEftPlan, bilAccount, adjustDate);
                        deftRule = (boolean) tapeDates[0];
                        deftSameDay = (boolean) tapeDates[1];
                        tapeDate = (ZonedDateTime) tapeDates[2];
                    }
                }

                if (!deftRule) {
                    ZonedDateTime[] debitTapeDate = getFirstDebitTapeDate(bilAccount.getAccountId(),
                            bilEftPlan.getBepTapeDplNbr());
                    if (debitTapeDate != null && debitTapeDate.length > 0) {
                        tapeDate = debitTapeDate[0];
                        adjustDate = debitTapeDate[1];
                    }
                }

                nachaCompliance(statusCode, payment, bilEftPlan, bilAccount, deftRule, invoiceDate, adjustDate,
                        tapeDate, deftSameDay, fileTypeCode);

                processCurrentInsert(payment, adjustDate, bilEftPlan, statusCode, fileTypeCode);

                processBilPolicyTerm(bilAccount);
                if (isPolicyTermexist(payment.getAccountId())) {
                    callAccountAdjustDatesForRecoveryDate(payment.getAccountId());
                }

                callBcmoax(payment.getAccountId());
                insertDeferActivityQueue(payment.getAccountId(), execContext.getApplicationDate());
                paymentService.insertAccountBalance(bilAccount.getAccountId().trim(),
                        bilAccount.getAccountNumber().trim(), bilAccount.getBillTypeCd().trim(), bilAccount.getStatus(),
                        execContext.getApplicationDate());
            }

            if (!bilAccount.getBillTypeCd().trim().equalsIgnoreCase(AccountTypeCode.COMMISSIONACCOUNTCD.getValue())
                    && StringUtils.isNotBlank(providerIntegration)
                    && providerIntegration.equalsIgnoreCase(PaymentProviderType.PAYMENTUS.getValue())
                    && BooleanUtils.toBoolean(autoPaymentIntegration)
                    && (payment.getPaymentAmount() == null || payment.getPaymentAmount() == SHORT_ZERO)) {
                saveCustomerSubscriptionProfile(payment, bilAccount);
            }
        }

        return payment;
    }

    private void saveCustomerSubscriptionProfile(Payment payment, BilAccount bilAccount) {
        CustomerSubscriptionProfile customerSubscriptionProfile = new CustomerSubscriptionProfile();
        customerSubscriptionProfile.setCustomerSubscriptionProfileId(
                new CustomerSubscriptionProfileId(bilAccount.getPayorClientId(), payment.getIdentifier()));
        customerSubscriptionProfile.setCustomerId(bilAccount.getPayorClientId());
        customerSubscriptionProfile.setPaymentProfileId(payment.getPaymentProfileId());
        if (StringUtils.isNotBlank(payment.getCustomerProfileId())) {
            customerSubscriptionProfile.setCustomerId(payment.getCustomerProfileId());
        }
        customerSubscriptionProfileRepository.save(customerSubscriptionProfile);

    }

    private String getAccountType(String busCd) {
        List<BusCdTranslation> busCdTranslationList = busCdTranslationRepository
                .findAllByCodeAndTypeList(
                        busCd, Arrays.asList(BusCodeTranslationType.BTG.getValue(),
                                BusCodeTranslationType.BTP.getValue(), BusCodeTranslationType.BTY.getValue()),
                        execContext.getLanguage());
        if (busCdTranslationList != null && !busCdTranslationList.isEmpty()) {
            return busCdTranslationList.get(SHORT_ZERO).getBusCdTranslationId().getBusType();
        } else {
            throw new InvalidDataException("bus.cd.rows.not.found");
        }
    }

    private void processBilPolicyTerm(BilAccount bilAccount) {
        NewTypeAcctIndEnum newTypeAcctInd;
        newTypeAcctInd = NewTypeAcctIndEnum.getNewTypeAcctIndEnum(getAccountType(bilAccount.getBillTypeCd()));
        List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.getPolicyRowByAccountId(
                bilAccount.getAccountId(),
                Arrays.asList(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                        BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                        BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(),
                        BilPolicyStatusCode.TRANSFERRED.getValue()));
        if (bilPolicyTermList != null && !bilPolicyTermList.isEmpty()) {
            String holdObjName = BLANK_STRING;
            String holdMduName = BLANK_STRING;
            for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
                List<BilPolicyTerm> bilPolicyTermRowList = bilPolicyTermRepository.getPolicyRowByAccountIdAndPolicyId(
                        bilAccount.getAccountId(), bilPolicyTerm.getBillPolicyTermId().getPolicyId());

                if (bilPolicyTermRowList != null && !bilPolicyTermRowList.isEmpty()) {
                    final String colMthChgBo = "BIL-INT-POST-EVENT-COL-MTH-";
                    String colMthChgIssId = bilPolicyTerm.getBptIssueSysId();
                    String filler0colMthChgBusObjNm = colMthChgBo.concat(colMthChgIssId);
                    Object[] eventObject = postEventRecord(newTypeAcctInd.toString(),
                            bilPolicyTermRowList.get(SHORT_ZERO), bilAccount, filler0colMthChgBusObjNm, holdObjName,
                            holdMduName);
                    holdObjName = (String) eventObject[0];
                    holdMduName = (String) eventObject[1];
                }

            }
        }
    }

    private Object[] postEventRecord(String bilAccountType, BilPolicyTerm bilPolicyTerm, BilAccount bilAccount,
            String interfaceObjName, String holdObjName, String holdMduName) {
        final String invokeProgram = "BCMOBE01";
        final String requestProgram = "BACPAAU";
        Event event = new Event();
        event.setEventCode(BillingConstants.EventCode.EVENT_BILL_CHG.getValue());
        event.setBilAccountType(bilAccountType);
        if (bilAccountType.equals(BillingConstants.BusCodeTranslationType.BTP.getValue())) {
            BilThirdParty bilThirdParty = bilThirdPartyRepository.findByBilTtyNbr(bilAccount.getThirdPartyIdentifier());
            if (bilThirdParty != null) {
                event.setBilTechKeyId(bilThirdParty.getBillThirdPartyId());
                event.setBilTechKeyTypeCode(BillingConstants.TechnicalKeyTypeCode.THIRD_PARTY.getValue());
            }
        } else if (bilAccountType.equals(BillingConstants.BusCodeTranslationType.BTG.getValue())) {
            BilAgent bilAgent = bilAgentRepository.findByBilAgtActNbr(bilAccount.getAgentAccountNumber());
            if (bilAgent != null) {
                event.setBilTechKeyId(bilAgent.getBilAgtActId());
                event.setBilTechKeyTypeCode(BillingConstants.TechnicalKeyTypeCode.AGENCY.getValue());
            }
        }
        getPolicyNumber(bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getBilAccountId(), event);
        event.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
        event.setPolicyEffectiveDate(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
        event.setPlannedExpirationDate(bilPolicyTerm.getPlnExpDt());
        event.setLineOfBusinessCode(bilPolicyTerm.getLobCd());
        event.setMasterCompanyNumber(bilPolicyTerm.getMasterCompanyNbr());
        event.setStateProvinceCode(bilPolicyTerm.getBillStatePvnCd());
        event.setCountryCode(bilPolicyTerm.getBillCountryCd());
        event.setPolicyStatusCode(String.valueOf(bilPolicyTerm.getBillPolStatusCd()));
        event.setBilPlanCode(bilPolicyTerm.getBillPlanCd());
        event.setBilThirdPartyNumber(bilAccount.getThirdPartyIdentifier());
        event.setBilAdditionalId(bilAccount.getAdditionalId());
        event.setBilAccountId(bilPolicyTerm.getBillPolicyTermId().getBilAccountId());
        event.setBilAccountNumber(bilAccount.getAccountNumber());
        event.setBilTypeCode(bilAccount.getBillTypeCd());
        event.setBilClassCode(bilAccount.getBillClassCd());
        event.setCollectionMethod(bilAccount.getCollectionMethod());
        event.setCollectionPlan(bilAccount.getCollectionPlan());

        event.setRequestProgramName(requestProgram);
        event.setBussinessObjectName(interfaceObjName);

        event.setIssueSystemCode(bilPolicyTerm.getBptIssueSysId());
        event.setEventKeyId(event.getBilAccountId());
        event.setEventKeyTypeCode(BillingConstants.EventKeyTypeCode.BILLACCOUNT.getValue());
        event.setEventStatusCode(CHAR_P);
        event.setEffectiveDate(execContext.getApplicationDate());
        event.setEventMduNm(requestProgram);
        Object[] eventObject = { holdMduName, holdObjName };
        if (holdMduName.isEmpty() && interfaceObjName.equals(holdObjName)) {
            return eventObject;
        }
        if (holdMduName.isEmpty() || !interfaceObjName.equals(holdObjName)) {
            HalBoMduXrf halBoMduXrf = halBoMduXrfRepository.findById(interfaceObjName).orElse(null);
            if (halBoMduXrf != null) {
                String programName = halBoMduXrf.getHbmxBobjMduNm().trim();
                if (programName.isEmpty()) {
                    holdMduName = programName;
                    holdObjName = interfaceObjName;
                    eventObject[0] = holdMduName;
                    eventObject[1] = holdObjName;
                    return eventObject;
                } else if (programName.equals(invokeProgram)) {
                    eventPostingService.postEvent(event);
                }
                holdMduName = programName;
            } else {
                logger.error("Program name is not found in HAL_BO_MDU_XRF. {}", interfaceObjName);
                return eventObject;
            }
        }
        holdObjName = interfaceObjName;
        eventObject[0] = holdMduName;
        eventObject[1] = holdObjName;
        return eventObject;
    }

    private Event getPolicyNumber(String policyId, String bilAccountId, Event event) {
        BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(policyId, bilAccountId)).orElse(null);
        if (bilPolicy != null) {
            event.setPolicyNumber(bilPolicy.getPolNbr());
            event.setPolicySymbolCode(bilPolicy.getPolSymbolCd());
        }
        return event;
    }

    private Object[] getTapeDate(boolean deftDate, boolean deftRule, BilEftPlan bilEftPlan, BilAccount bilAccount,
            ZonedDateTime adjustDate) {

        ZonedDateTime tapeDate = null;
        boolean deftSameDay = false;

        if (deftDate && adjustDate.isEqual(execContext.getApplicationDate())) {
            tapeDate = adjustDate;
            deftSameDay = true;
        } else {
            deftSameDay = false;
            if (!deftDate && bilEftPlan.getBepAdvNoticeInd() == CHAR_Y) {
                BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("ERCV", bilAccount.getBillTypeCd(),
                        bilAccount.getBillClassCd(), BLANK_STRING);
                if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                    int addDays = bilEftPlan.getBepInvDplNbr();
                    if (addDays > Integer.valueOf(bilRulesUct.getBrtParmListTxt().trim().substring(0, 3))) {
                        addDays = Integer.valueOf(bilRulesUct.getBrtParmListTxt().trim().substring(0, 3));
                    }
                    ZonedDateTime ercvEftDate = DateRoutine.getAdjustedDate(execContext.getApplicationDate(),
                            checkForIncludeWeekend(), addDays, null);

                    if (ercvEftDate.compareTo(adjustDate) > 0) {
                        return new Object[] { false, deftSameDay, tapeDate };
                    }
                }
            }
            tapeDate = getEftTapeDate(bilEftPlan.getBepTapeDplNbr(), adjustDate, checkForIncludeWeekend());
        }
        return new Object[] { deftRule, deftSameDay, tapeDate };
    }

    private ZonedDateTime[] getFirstDebitTapeDate(String accountId, short tapeDplNumber) {

        ZonedDateTime nextInvDueDt = null;
        ZonedDateTime adjustDate = null;

        nextInvDueDt = bilEftPendingTapeRepository.getMinimunEftDrDate(accountId, CHAR_A, DECIMAL_ZERO, new ArrayList<>(
                Arrays.asList(EftRecordType.PRENOTE.getValue(), EftRecordType.ONE_TIME_ACH.getValue())));

        if (nextInvDueDt == null) {
            nextInvDueDt = bilDatesRepository.getRevisedMinAdjDueDate(accountId, execContext.getApplicationDate(),
                    new ArrayList<>(Arrays.asList(BLANK_CHAR, CHAR_A)), CHAR_X);
        }
        if (nextInvDueDt == null) {
            return new ZonedDateTime[0];
        }
        adjustDate = nextInvDueDt;
        ZonedDateTime eftTapeDate = getEftTapeDate(tapeDplNumber, nextInvDueDt, !checkForIncludeWeekend());
        return new ZonedDateTime[] { eftTapeDate, adjustDate };
    }

    private void nachaCompliance(char statusCode, Payment payment, BilEftPlan bilEftPlan, BilAccount bilAccount,
            boolean deftRuleSw, ZonedDateTime invoiceDate, ZonedDateTime adjustDate, ZonedDateTime tapeDate,
            boolean deftSameDaySw, short fileTypeCode) {

        if ((bilEftPlan.getBepNoticeInd() == EftNoticeIndicator.DEBIT_NOTICE.getValue()
                || bilEftPlan.getBepNoticeInd() == EftNoticeIndicator.DEBIT_CREDIT_NOTICE.getValue())
                && statusCode == ACTIVE_STATUS) {
            statusCode = checkingNachaCompliance(bilAccount, statusCode, bilEftPlan, tapeDate, adjustDate,
                    fileTypeCode);
            if (statusCode == PENDING_STATUS) {
                updateBilEftBank(payment.getAccountId(), statusCode, execContext.getApplicationDate());
            }
        }
        if (deftRuleSw && statusCode == ACTIVE_STATUS) {
            createDeftRow(bilAccount, deftSameDaySw, tapeDate, adjustDate, invoiceDate, bilEftPlan,
                    execContext.getApplicationDate(), statusCode, fileTypeCode);
        }
    }

    private void updateBilEftBank(String accountId, char status, ZonedDateTime effectiveDate) {
        BilEftBank bilEftBank = bilEftBankRepository.findById(new BilEftBankId(accountId, CHAR_A, effectiveDate))
                .orElse(null);
        if (bilEftBank != null) {
            bilEftBank.setBebAccountStaCd(status);
            bilEftBankRepository.saveAndFlush(bilEftBank);
        }
    }

    private void processCurrentInsert(Payment payment, ZonedDateTime adjustDate, BilEftPlan bilEftPlan, char statusCode,
            short fileTypeCode) {
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
            insertBilActSummary(payment.getAccountId(), BLANK_STRING, "CCA", DateRoutine.defaultDateTime(),
                    DateRoutine.defaultDateTime(), DECIMAL_ZERO, BLANK_STRING);
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            insertBilActSummary(payment.getAccountId(), BLANK_STRING, "EIA", DateRoutine.defaultDateTime(),
                    DateRoutine.defaultDateTime(), DECIMAL_ZERO, BLANK_STRING);
        } else {
            insertBilActSummary(payment.getAccountId(), BLANK_STRING, "EWA", DateRoutine.defaultDateTime(),
                    DateRoutine.defaultDateTime(), DECIMAL_ZERO, payment.getPaymentType());
        }

        if ((bilEftPlan.getBepNoticeInd() == EftNoticeIndicator.DEBIT_NOTICE.getValue()
                || bilEftPlan.getBepNoticeInd() == EftNoticeIndicator.DEBIT_CREDIT_NOTICE.getValue())
                && statusCode == PENDING_STATUS) {
            insertPrenote(payment.getAccountId(), statusCode, adjustDate, bilEftPlan, fileTypeCode);
        }

        if (statusCode == ACTIVE_STATUS) {
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
                insertBilActSummary(payment.getAccountId(), BLANK_STRING, BillingMethod.ECC.toString(),
                        execContext.getApplicationDate(), DateRoutine.defaultDateTime(), DECIMAL_ZERO, BLANK_STRING);
            } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
                insertBilActSummary(payment.getAccountId(), BLANK_STRING, BillingMethod.EFT.name(),
                        execContext.getApplicationDate(), DateRoutine.defaultDateTime(), DECIMAL_ZERO, BLANK_STRING);
            } else {
                insertBilActSummary(payment.getAccountId(), BLANK_STRING, BillingMethod.ELECTRONIC_WALLET.toString(),
                        execContext.getApplicationDate(), DateRoutine.defaultDateTime(), DECIMAL_ZERO,
                        payment.getPaymentType());
            }

        }
    }

    private Object[] checkDates(boolean deftDateSw, char statusCode, Payment payment, boolean deftRuleSw) {

        ZonedDateTime adjustDate = null;
        ZonedDateTime invoiceDate = null;

        if (statusCode == ACTIVE_STATUS) {
            List<Object[]> maxDates = null;
            if (deftDateSw) {
                maxDates = bilDatesRepository.getMaxDate(payment.getAccountId(), execContext.getApplicationDate(),
                        CHAR_Y, CHAR_X);
            } else {
                maxDates = bilDatesRepository.getMaxDate1(payment.getAccountId(), execContext.getApplicationDate(),
                        CHAR_Y, CHAR_X);
            }

            if (maxDates != null && maxDates.get(SHORT_ZERO)[0] != null && maxDates.get(SHORT_ZERO)[1] != null) {
                invoiceDate = (ZonedDateTime) maxDates.get(SHORT_ZERO)[0];
                adjustDate = (ZonedDateTime) maxDates.get(SHORT_ZERO)[1];
                return new Object[] { true, invoiceDate, adjustDate };
            } else {
                return new Object[] { false, null, null };
            }
        }
        return new Object[] { deftRuleSw, null, null };
    }

    private char checkingNachaCompliance(BilAccount bilAccount, char statusCode, BilEftPlan bilEftPlan,
            ZonedDateTime tapeDate, ZonedDateTime adjustDate, short fileTypeCode) {
        boolean includeWeekend = checkForIncludeWeekend();

        ZonedDateTime nachaDate;
        char suppressPrenote = BLANK_CHAR;
        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("EPNC", bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), BLANK_STRING);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y
                && getEpncParam(bilRulesUct.getBrtParmListTxt().trim())) {
            nachaDate = DateRoutine.getAdjustedDate(execContext.getApplicationDate(), !includeWeekend,
                    Integer.parseInt(bilRulesUct.getBrtParmListTxt().trim()), null);
        } else {
            statusCode = PENDING_STATUS;
            return statusCode;
        }

        if (tapeDate == null || tapeDate.compareTo(nachaDate) < 0) {
            statusCode = PENDING_STATUS;
            return statusCode;
        } else {
            insertBilActSummary(bilAccount.getAccountId(), BLANK_STRING, "OSC", DateRoutine.defaultDateTime(),
                    DateRoutine.defaultDateTime(), DECIMAL_ZERO, BLANK_STRING);

            if (suppressPrenote != CHAR_Y) {
                checkExistingPrenote(bilAccount.getAccountId(), execContext.getApplicationDate());
                insertPrenote(bilAccount.getAccountId(), statusCode, adjustDate, bilEftPlan, fileTypeCode);
            }
        }

        return statusCode;
    }

    private void checkExistingPrenote(String accountId, ZonedDateTime eftAcyDate) {
        List<BilEftPendingTape> existingPrenotes = bilEftPendingTapeRepository.fetchFuturePrenote(accountId, CHAR_A,
                eftAcyDate, EftRecordType.PRENOTE.getValue());
        if (existingPrenotes != null && !existingPrenotes.isEmpty()) {
            deletePrenote(accountId, eftAcyDate);
        }
    }

    private void deletePrenote(String accountId, ZonedDateTime eftAcyDate) {
        bilEftPendingTapeRepository.deletePreNoteRows(accountId, CHAR_A, eftAcyDate,
                new ArrayList<>(Arrays.asList(EftRecordType.PRENOTE.getValue(), EftRecordType.COMPLETE.getValue())));

    }

    private void insertPrenote(String accountId, char statusCode, ZonedDateTime adjustDate, BilEftPlan bilEftPlan,
            short fileTypeCode) {
        ZonedDateTime tapeDate = getEftTapeDate(bilEftPlan.getBepTapeDplNbr(), adjustDate, checkForIncludeWeekend());
        ZonedDateTime se3Date = execContext.getApplicationDate();
        short sequenceNumber = generateEftPendingSequenceNumber(accountId, se3Date, se3Date);
        BilEftPendingTape bilEftPendingTape = new BilEftPendingTape();
        BilEftPendingTapeId bilEftPendingTapeId = new BilEftPendingTapeId(accountId, CHAR_A,
                execContext.getApplicationDate(), se3Date, sequenceNumber);
        bilEftPendingTape.setBilEftPendingTapeId(bilEftPendingTapeId);
        bilEftPendingTape.setAgencyNbr(BLANK_STRING);
        bilEftPendingTape.setEftDrAmount(DECIMAL_ZERO);
        bilEftPendingTape.setFileTypeCd(fileTypeCode);
        bilEftPendingTape.setEftDrDate(se3Date);
        bilEftPendingTape.setWebAutomatedIndicator(CHAR_N);
        bilEftPendingTape.setDisbursementId(BLANK_STRING);
        bilEftPendingTape.setReceiptSequenceNumber((short) 0);
        bilEftPendingTape.setBusinessActivityCd(fetchBusinessActivityTypeCode(String.valueOf(statusCode)));
        bilEftPendingTape.setEftReceivedType(EftRecordType.PRENOTE.getValue());
        bilEftPendingTape.setEftNotIndicator(CHAR_N);
        bilEftPendingTape.setInsertedRowTime(execContext.getApplicationDate());
        bilEftPendingTape.setEftPostPaymentDate(tapeDate);
        bilEftPendingTapeRepository.saveAndFlush(bilEftPendingTape);

        if (bilEftPlan.getBepNoticeInd() == EftNoticeIndicator.DEBIT_CREDIT_NOTICE.getValue()) {
            BilEftPendingTape bilEftPendingTapeExisting = new BilEftPendingTape(bilEftPendingTape);
            insertBetGeneric(bilEftPendingTapeExisting);
        }
    }

    private void insertBetGeneric(BilEftPendingTape bilEftPendingTapeExisting) {
        Short maxSequenceNumber = bilEftPendingTapeRepository.getMaximunSequenceNumber(
                bilEftPendingTapeExisting.getBilEftPendingTapeId().getTechnicalKey(),
                bilEftPendingTapeExisting.getBilEftPendingTapeId().getTechnicalKeyType(),
                bilEftPendingTapeExisting.getBilEftPendingTapeId().getEftTapeDate(),
                bilEftPendingTapeExisting.getBilEftPendingTapeId().getEftActivityDate());
        if (maxSequenceNumber == null) {
            maxSequenceNumber = 0;
        } else {
            maxSequenceNumber = (short) (maxSequenceNumber + 1);
        }
        BilEftPendingTape bilEftPendingTapeInserted = new BilEftPendingTape();
        BilEftPendingTapeId bilEftPendingTapeId = new BilEftPendingTapeId(
                bilEftPendingTapeExisting.getBilEftPendingTapeId().getTechnicalKey(),
                bilEftPendingTapeExisting.getBilEftPendingTapeId().getTechnicalKeyType(),
                bilEftPendingTapeExisting.getBilEftPendingTapeId().getEftTapeDate(),
                bilEftPendingTapeExisting.getBilEftPendingTapeId().getEftActivityDate(), maxSequenceNumber);
        bilEftPendingTapeInserted.setBilEftPendingTapeId(bilEftPendingTapeId);
        bilEftPendingTapeInserted.setEftReceivedType(EftRecordType.COMPLETE.getValue());
        bilEftPendingTapeInserted.setAgencyNbr(bilEftPendingTapeExisting.getAgencyNbr());
        bilEftPendingTapeInserted.setBusinessActivityCd(bilEftPendingTapeExisting.getBusinessActivityCd());
        bilEftPendingTapeInserted.setDisbursementId(bilEftPendingTapeExisting.getDisbursementId());
        bilEftPendingTapeInserted.setEftDrAmount(bilEftPendingTapeExisting.getEftDrAmount());
        bilEftPendingTapeInserted.setEftDrDate(bilEftPendingTapeExisting.getEftDrDate());
        bilEftPendingTapeInserted.setEftNotIndicator(bilEftPendingTapeExisting.getEftNotIndicator());
        bilEftPendingTapeInserted.setEftPostPaymentDate(bilEftPendingTapeExisting.getEftPostPaymentDate());
        bilEftPendingTapeInserted.setFileTypeCd(bilEftPendingTapeExisting.getFileTypeCd());
        bilEftPendingTapeInserted.setInsertedRowTime(bilEftPendingTapeExisting.getInsertedRowTime());
        bilEftPendingTapeInserted.setPaymentOrderId(bilEftPendingTapeExisting.getPaymentOrderId());
        bilEftPendingTapeInserted.setReceiptSequenceNumber(bilEftPendingTapeExisting.getReceiptSequenceNumber());
        bilEftPendingTapeInserted.setWebAutomatedIndicator(bilEftPendingTapeExisting.getWebAutomatedIndicator());
        bilEftPendingTapeRepository.saveAndFlush(bilEftPendingTapeInserted);
    }

    private short generateEftPendingSequenceNumber(String accountId, ZonedDateTime tapeDate, ZonedDateTime eftAcyDate) {
        Short maxSequenceNumber = bilEftPendingTapeRepository.getMaximunSequenceNumber(accountId, CHAR_A, tapeDate,
                eftAcyDate);
        if (maxSequenceNumber == null) {
            maxSequenceNumber = 0;
        } else {
            maxSequenceNumber = (short) (maxSequenceNumber + 1);
        }
        return maxSequenceNumber;
    }

    private boolean getEpncParam(String epncParam) {
        try {
            Integer.parseInt(epncParam);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    private ZonedDateTime getEftTapeDate(short tapeDplNumber, ZonedDateTime adjusDueDate, boolean includeWeekend) {

        ZonedDateTime eftTapeDate = DateRoutine.getAdjustedDate(execContext.getApplicationDate(), !includeWeekend,
                tapeDplNumber, adjusDueDate);
        eftTapeDate = adjustDateByHoliDayParam(eftTapeDate);
        return eftTapeDate;
    }

    private ZonedDateTime adjustDateByHoliDayParam(ZonedDateTime eftTapeDate) {
        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("XHOL", BLANK_STRING, BLANK_STRING, BLANK_STRING);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            do {
                switch (bilRulesUct.getBrtParmListTxt().trim()) {
                case "H8":
                    eftTapeDate = eftTapeDate.plusDays(1);
                    if (eftTapeDate.getDayOfWeek() == DayOfWeek.SATURDAY
                            || eftTapeDate.getDayOfWeek() == DayOfWeek.SUNDAY) {
                        if (eftTapeDate.getDayOfWeek() == DayOfWeek.SATURDAY) {
                            eftTapeDate = eftTapeDate.plusDays(2);
                        } else {
                            eftTapeDate = eftTapeDate.plusDays(1);
                        }
                    }
                    break;
                case "H6":
                    eftTapeDate = eftTapeDate.plusDays(1);
                    break;
                case "H5":
                case "H3":
                    eftTapeDate = eftTapeDate.plusDays(1);
                    if (eftTapeDate.getDayOfWeek() == DayOfWeek.SUNDAY) {
                        eftTapeDate = eftTapeDate.plusDays(1);
                    }
                    break;
                case "H1":
                    if (eftTapeDate.getDayOfWeek() == DayOfWeek.SATURDAY
                            || eftTapeDate.getDayOfWeek() == DayOfWeek.SUNDAY) {
                        if (eftTapeDate.getDayOfWeek() == DayOfWeek.SATURDAY) {
                            eftTapeDate = eftTapeDate.plusDays(2);
                        } else {
                            eftTapeDate = eftTapeDate.plusDays(1);
                        }
                    }
                    break;
                default:
                    break;
                }
            } while (checkIsHoliday(eftTapeDate) && !bilRulesUct.getBrtParmListTxt().trim().equalsIgnoreCase("H1"));
        }
        return eftTapeDate;
    }

    private boolean checkIsHoliday(ZonedDateTime inputDate) {
        HolidayPrc holidayPrc = holidayPrcRepository.findById(new HolidayPrcId(inputDate, "1", "ZZZ")).orElse(null);

        return holidayPrc != null;

    }

    private boolean checkForIncludeWeekend() {
        BilActRules bilActRules = bilActRulesRepository.findById(new BilActRulesId(BLANK_STRING, BLANK_STRING))
                .orElse(null);

        return (bilActRules != null && bilActRules.getBruIncWknInd() == CHAR_Y);

    }

    private char getAccountStatus(BilEftPlan bilEftPlan) {

        ZonedDateTime se3DateTime = execContext.getApplicationDate();
        char statusCode;

        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow("EPNC", SEPARATOR_BLANK, SEPARATOR_BLANK,
                SEPARATOR_BLANK, se3DateTime, se3DateTime);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            statusCode = ACTIVE_STATUS;
        } else if (bilEftPlan.getBepNoticeInd() == EftNoticeIndicator.NO_NOTICE.getValue()) {
            statusCode = ACTIVE_STATUS;
        } else {
            statusCode = PENDING_STATUS;
        }
        return statusCode;
    }

    private void createDeftRow(BilAccount bilAccount, boolean deftSameDay, ZonedDateTime eftTapeDate,
            ZonedDateTime adjustDate, ZonedDateTime invoiceDate, BilEftPlan bilEftPlan, ZonedDateTime currentEftDate,
            char statusCode, short fileTypeCode) {
        double currentDueAmount;
        double minInvoiceAmount = DECIMAL_ZERO;
        if (deftSameDay || eftTapeDate != null && eftTapeDate.compareTo(execContext.getApplicationDate()) >= 0) {
            currentDueAmount = getMaxDueDateBalance(bilAccount, adjustDate);
            if (currentDueAmount > 0) {
                BilSupportPlan bilSupportPlan = bilSupportPlanRepository
                        .findById(new BilSupportPlanId(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd()))
                        .orElse(null);
                if (bilSupportPlan != null) {
                    minInvoiceAmount = bilSupportPlan.getBspMinInvAmt();
                }
                if (currentDueAmount >= minInvoiceAmount) {
                    insertEftPndTape(bilAccount.getAccountId(), eftTapeDate, adjustDate, invoiceDate, currentDueAmount,
                            bilEftPlan.getBepPendNbr(), statusCode, fileTypeCode);
                }
            }

            if (eftTapeDate.compareTo(currentEftDate) >= 0 && bilEftPlan.getBepAdvNoticeInd() == CHAR_Y) {
            	BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("PEIE", bilAccount.getBillTypeCd(),
                        bilAccount.getBillClassCd(), BLANK_STRING);
            	if (bilRulesUct == null || bilRulesUct.getBrtRuleCd() != CHAR_Y) {
            		scheduleService.scheduleDeferActivity("BCMOEIA",
                            getAdvanNoteParam(bilAccount, currentDueAmount, adjustDate), "BACPAEU");
            	}
            	
            }
        }
    }

    private double getMaxDueDateBalance(BilAccount bilAccount, ZonedDateTime adjustDate) {
        Double currentDueAmount = bilIstScheduleRepository.getCurrentDueAmount(bilAccount.getAccountId(), INVOICE_CODES,
                adjustDate);
        if (currentDueAmount == null) {
            currentDueAmount = DECIMAL_ZERO;
        }
        Double balanceAmount = bilCrgAmountsRepository.getBalanceAmount(bilAccount.getAccountId(), INVOICE_CODES,
                adjustDate);
        if (balanceAmount == null) {
            balanceAmount = DECIMAL_ZERO;
        }
        currentDueAmount = currentDueAmount + balanceAmount;
        return currentDueAmount;
    }

    private void insertEftPndTape(String accountId, ZonedDateTime eftTapeDate, ZonedDateTime adjustDate,
            ZonedDateTime invoiceDate, double currentDueAmount, short pendingNumber, char statusCode, short fileType) {
        ZonedDateTime paymentPostDate = getPostPaymentDate(pendingNumber, adjustDate);
        short sequenceNumber = generateEftPendingSequenceNumber(accountId, eftTapeDate, invoiceDate);
        BilEftPendingTape bilEftPendingTape = new BilEftPendingTape();
        BilEftPendingTapeId bilEftPendingTapeId = new BilEftPendingTapeId(accountId, CHAR_A, eftTapeDate, invoiceDate,
                sequenceNumber);
        bilEftPendingTape.setEftPostPaymentDate(paymentPostDate);
        bilEftPendingTape.setBilEftPendingTapeId(bilEftPendingTapeId);
        bilEftPendingTape.setEftDrAmount(currentDueAmount);
        bilEftPendingTape.setEftDrDate(adjustDate);
        bilEftPendingTape.setDisbursementId(BLANK_STRING);
        bilEftPendingTape.setReceiptSequenceNumber((short) 0);
        bilEftPendingTape.setWebAutomatedIndicator(CHAR_N);
        bilEftPendingTape.setEftReceivedType(EftRecordType.ORIGINAL.getValue());
        bilEftPendingTape.setAgencyNbr(BLANK_STRING);
        bilEftPendingTape.setPaymentOrderId(BLANK_STRING);
        bilEftPendingTape.setBusinessActivityCd(fetchBusinessActivityTypeCode(String.valueOf(statusCode)));
        bilEftPendingTape.setInsertedRowTime(execContext.getApplicationDate());
        bilEftPendingTape.setFileTypeCd(fileType);
        bilEftPendingTapeRepository.save(bilEftPendingTape);

        String additionaText = DateRoutine.dateTimeAsYYYYMMDDString(bilEftPendingTapeId.getEftActivityDate());
        additionaText += DateRoutine.dateTimeAsYYYYMMDDString(bilEftPendingTapeId.getEftTapeDate());

        insertBilActSummary(accountId, additionaText, "EOV", bilEftPendingTapeId.getEftActivityDate(),
                bilEftPendingTapeId.getEftTapeDate(), bilEftPendingTape.getEftDrAmount(), BLANK_STRING);
        if (statusCode == PENDING_STATUS) {
            BilEftBank bilEftBank = bilEftBankRepository
                    .findById(new BilEftBankId(accountId, CHAR_A, execContext.getApplicationDate())).orElse(null);
            if (bilEftBank != null) {
                bilEftBank.setBebAccountStaCd(ACTIVE_STATUS);
                bilEftBankRepository.saveAndFlush(bilEftBank);
            }
        }
    }

    private ZonedDateTime getPostPaymentDate(short pendNumber, ZonedDateTime adjustDate) {
        return DateRoutine.getAdjustedDate(adjustDate, !checkForIncludeWeekend(), pendNumber, null);
    }

    private Short getBilAcySeq(String accountId, ZonedDateTime se3DateTime) {
        Short bilAcySeq = SHORT_ZERO;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, se3DateTime.with(LocalTime.MIN));
        if (bilAcySeq == null) {
            bilAcySeq = SHORT_ZERO;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        return bilAcySeq;
    }

    public void insertDeferActivityQueue(String accountId, ZonedDateTime se3DateTime) {
        Double amount = null;
        Integer policyCount = null;
        Double chargesAmount = null;

        amount = bilCashDspRepository.findSuspenseAmountByAccountId(accountId, "SP", BLANK_CHAR, CHAR_Y,
                getRevsRsusIndicatorList(), "DPR");
        if (amount == null || amount == DECIMAL_ZERO) {
            return;
        } else {
            policyCount = bilPolicyTermRepository.getPolicyRowCountByAccountId(accountId, getBillPolStatusCd());
            if (policyCount == null || policyCount == SHORT_ZERO) {
                policyCount = (int) SHORT_ZERO;
                chargesAmount = bilCrgAmountsRepository.getTotalCrgAmount(INVOICE_CODES, accountId,
                        Arrays.asList(CHAR_P), CHAR_S);
                if (chargesAmount == null) {
                    chargesAmount = DECIMAL_ZERO;
                }
            }
            if (policyCount == SHORT_ZERO && chargesAmount == DECIMAL_ZERO) {
                return;
            }
        }
        CashApplyDriver cashApplyDriver = new CashApplyDriver();
        cashApplyDriver.setAccountId(accountId);
        cashApplyDriver.setCashApplyDate(se3DateTime);
        cashApplyDriver.setNoReinstatement(false);
        cashApplyDriver.setUserSequenceId(execContext.getUserSeqeunceId());
        cashApplyDriverService.processCashApply(cashApplyDriver);
    }

    private void insertBilEftBank(Payment payment, BilAccount bilAccount, char statusCode) {

        BilEftBank bilEftBank = bilEftBankRepository
                .findById(new BilEftBankId(payment.getAccountId(), CHAR_A, execContext.getApplicationDate()))
                .orElse(null);
        if (bilEftBank == null) {
            bilEftBank = new BilEftBank();
            bilEftBank.setBilEftBankId(
                    new BilEftBankId(payment.getAccountId(), CHAR_A, execContext.getApplicationDate()));
        }

        bilEftBank.setBebAutAmt(DECIMAL_ZERO);
        bilEftBank.setBebAccountStaCd(statusCode);
        bilEftBank.setBebAutInd(BLANK_CHAR);

        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            bilEftBank.setBebAccountTyc(payment.getAccountType());
            bilEftBank.setBilBankAcctNbr(payment.getBankAccountNumber());
            bilEftBank.setBilCrcrdExpDt(BLANK_STRING);
            bilEftBank.setBilCrcrdPstCd(BLANK_STRING);
            bilEftBank.setBilCrcrdTypeCd(BLANK_CHAR);
            bilEftBank.setBebWebAutInd(CHAR_N);
            bilEftBank.setBebEftPayLngNm(payment.getBankHolderName());

            if (StringUtils.isNotBlank(providerIntegration) && BooleanUtils.toBoolean(autoPaymentIntegration)) {
                if (bilAccount.getBillTypeCd().trim()
                        .equalsIgnoreCase(AccountTypeCode.COMMISSIONACCOUNTCD.getValue())) {
                    bilEftBank.setBilEftBankIdNbr(getBankId(payment.getRoutingNumber()));
                }
            } else {
                bilEftBank.setBilEftBankIdNbr(getBankId(payment.getRoutingNumber()));
            }
        } else {
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
                bilEftBank.setBilBankAcctNbr(payment.getCreditCardNumber());
                bilEftBank.setBilCrcrdTypeCd(payment.getCreditCardType().charAt(SHORT_ZERO));
                bilEftBank.setBebEftPayLngNm(payment.getCreditCardHolderName());
                bilEftBank.setBilCrcrdExpDt(payment.getCreditCardExpiryDate());
                bilEftBank.setBilCrcrdPstCd(payment.getPostalCode());
            } else {
                bilEftBank.setBilBankAcctNbr(payment.getWalletNumber());
                bilEftBank.setBilCrcrdTypeCd(payment.getWalletType().charAt(SHORT_ZERO));
                bilEftBank.setBebEftPayLngNm(payment.getWalletIdentifier());
            }

            bilEftBank.setBebAccountTyc(BLANK_STRING);
            bilEftBank.setBebWebAutInd(BLANK_CHAR);

        }

        bilEftBankRepository.saveAndFlush(bilEftBank);

    }

    private int getBankId(String routingNumber) {
        BilEftBankEd bilEftBankEd = bilEftBankEdRepository.findByRoutingTransitNbr(routingNumber);
        if (bilEftBankEd == null) {
            throw new DataNotFoundException("no.data.found");
        }
        return bilEftBankEd.getEftBankId();
    }

    private List<Character> getBillPolStatusCd() {
        List<Character> polStatusCdList = new ArrayList<>();
        polStatusCdList.add(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue());
        polStatusCdList.add(BilPolicyStatusCode.TRANSFERRED.getValue());
        polStatusCdList.add(BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue());
        polStatusCdList.add(BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue());
        polStatusCdList.add(BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue());
        return polStatusCdList;
    }

    private List<Character> getRevsRsusIndicatorList() {
        List<Character> revsRsusIndicatorList = new ArrayList<>();
        revsRsusIndicatorList.add(CHAR_N);
        revsRsusIndicatorList.add(BLANK_CHAR);
        return revsRsusIndicatorList;
    }

    private BilEftPlan readBilEftPlan(Payment payment) {
        BilEftPlan bilEftPlan = bilEftPlanRepository.findById(payment.getCollectionType()).orElse(null);
        if (bilEftPlan == null) {
            throw new DataNotFoundException("no.data.found");
        }
        return bilEftPlan;

    }

    private String getColllectionType(String collectionMethod, String busType) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(collectionMethod, busType,
                execContext.getLanguage());

        if (busCdTranslation != null) {
            return busCdTranslation.getBusCdTranslationId().getBusType();
        } else {
            throw new InvalidDataException("bus.cd.rows.not.found");
        }

    }

    private void insertBilActSummary(String accountId, String additionalText, String bilAcyDesCd,
            ZonedDateTime acyDes1Date, ZonedDateTime acyDes2Date, double acyAmount, String bilDesReaTyp) {
        BilActSummary bilActSummary = new BilActSummary();
        if (bilAcySeq == 0) {
            bilAcySeq = getBilAcySeq(accountId, execContext.getApplicationDate());
        } else {
            bilAcySeq++;
        }

        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId, execContext.getApplicationDate(), bilAcySeq);
        bilActSummary.setBillActSummaryId(bilActSummaryId);

        bilActSummary.setBilAcyDesCd(bilAcyDesCd);
        bilActSummary.setBilDesReaTyp(bilDesReaTyp);
        bilActSummary.setBilAcyDes1Dt(acyDes1Date);
        bilActSummary.setBilAcyDes2Dt(acyDes2Date);
        bilActSummary.setBilAcyAmt(acyAmount);
        bilActSummary.setBasAddDataTxt(additionalText);
        bilActSummary.setPolSymbolCd(BLANK_STRING);
        bilActSummary.setPolNbr(BLANK_STRING);
        bilActSummary.setUserId(execContext.getUserSeqeunceId());
        bilActSummary.setBilAcyTs(execContext.getApplicationDateTime());
        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

    private char fetchBusinessActivityTypeCode(String accountType) {
        char businessActivityCode = CHAR_N;
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(accountType, "CCD",
                execContext.getLanguage());
        if (busCdTranslation != null) {
            businessActivityCode = CHAR_Y;
        }
        return businessActivityCode;
    }

    private String getAdvanNoteParam(BilAccount bilAccount, double currentDueAmount, ZonedDateTime bilAdjDueDt) {
        StringBuilder stringBuilder = new StringBuilder();
        String recordType = "00";
        if (bilAccount.getThirdPartyIdentifier().trim().isEmpty()) {
            recordType = "01";
        }
        stringBuilder.append(StringUtils.leftPad(recordType, 2, BLANK_CHAR));
        stringBuilder.append(StringUtils.leftPad("0", 2, '0'));
        stringBuilder.append(StringUtils
                .rightPad(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()), 10, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt), 10, BLANK_CHAR));
        stringBuilder.append(StringUtils.leftPad(String.valueOf(currentDueAmount).replace(".", ""), 14, '0'));
        stringBuilder.append(StringUtils.leftPad("0", 14, '0'));
        stringBuilder.append(StringUtils.rightPad(bilAccount.getAccountId(), 8, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 8, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad("BACPAEU", 10, BLANK_CHAR));
        return stringBuilder.toString();
    }

    private short determineFileTypeCode(String paymentType, String bilDesReasonCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(paymentType,
                BilDesReasonType.FTC.getValue(), execContext.getLanguage());
        if (busCdTranslation == null) {
            busCdTranslation = busCdTranslationRepository.findByCodeAndType(bilDesReasonCode,
                    BilDesReasonType.FTC.getValue(), execContext.getLanguage());
        }
        if (busCdTranslation == null) {
            throw new InvalidDataException("bankCode.invalid");
        }
        return Short.parseShort(busCdTranslation.getBusDescription().trim());
    }

    public void callBcmoax(String accountId) {
        AccountProcessingAndRecovery accountProcessingAndRecovery = new AccountProcessingAndRecovery();
        accountProcessingAndRecovery.setAccountId(accountId);
        accountProcessingAndRecovery.setDriverIndicator(CHAR_S);
        accountProcessingAndRecovery.setRecoveryDate(execContext.getApplicationDate());
        accountProcessingAndRecovery.setCashAcyCallIndicator(CHAR_Y);
        accountProcessingAndRecovery.setAccountForce(true);
        accountProcessingAndRecoveryService.accountAndRecoveryProcess(accountProcessingAndRecovery);

    }

    private enum NewTypeAcctIndEnum {
        NEW_AB("BTY"), NEW_TTY("BTP"), NEW_AGT("BTG"), BLANK(BLANK_STRING);

        public static NewTypeAcctIndEnum getNewTypeAcctIndEnum(final String data) {
            for (final NewTypeAcctIndEnum newTypeAcctIndData : NewTypeAcctIndEnum.values()) {
                if (newTypeAcctIndData.toString().equals(data)) {
                    return newTypeAcctIndData;
                }
            }
            return NewTypeAcctIndEnum.BLANK;
        }

        private String newTypeAcctIndData = BLANK_STRING;

        private NewTypeAcctIndEnum(String newTypeAcctIndData) {
            this.newTypeAcctIndData = newTypeAcctIndData;
        }

        @Override
        public String toString() {
            return newTypeAcctIndData;
        }
    }

    private void callAccountAdjustDatesForRecoveryDate(String accountId) {
        AccountAdjustDates accountAdjustDates = new AccountAdjustDates();
        accountAdjustDates.setAccountId(accountId);
        accountAdjustDates.setDateResetIndicator(CHAR_A);
        accountAdjustDates.setProcessDate(execContext.getApplicationDate());
        allResetService.processAllDatesReset(accountAdjustDates);
    }

    private boolean isPolicyTermexist(String accountId) {
        Integer policyCount = bilPolicyTermRepository.checkIfExistsByBilAccountId(accountId);
        return policyCount != null && policyCount > SHORT_ZERO ? Boolean.TRUE : Boolean.FALSE;

    }

}
