package billing.service.impl;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.model.Batch;
import billing.model.Payment;
import billing.mongo.data.model.ReportHeader;
import billing.service.BatchDetailReportService;
import billing.service.BatchPaymentService;
import billing.service.BatchService;
import billing.utils.XLSReportRenderer;
import core.model.Paging;
import core.utils.DateRoutine;

@Service
public class BatchDetailReportServiceImpl implements BatchDetailReportService {

    Paging paging = new Paging();
    private static final int INITIAL_PAGE = 1;
    private static final int INITIAL_PAGE_SIZE = 1000;

    @Autowired
    private BatchService batchService;

    @Autowired
    private BatchPaymentService batchPaymentService;

    @Autowired
    private XLSReportRenderer xlsReportRenderer;

    @Override
    public XSSFWorkbook buildExcelDocument(String batchId, String postedDate, String reportId, String userId) {

        Batch batch = batchService.findBatchDetails(batchId, postedDate, userId);

        String batchID = batch.getBatchId();
        ZonedDateTime entryDate = batch.getPostedDate();
        ZonedDateTime depositDate = batch.getDepositDate();
        Double batchTotal = batch.getBatchAmount();
        String depositBank = batch.getDepositBank();
        String currency = batch.getCurrency();

        List<Payment> batchPaymentList = batchPaymentService.findAllBatchPayments(postedDate, batchId, null, null, null,
                INITIAL_PAGE_SIZE, INITIAL_PAGE, paging, userId);
        List<ReportHeader> reportHeaderList = setReportHeaders(batchID, entryDate, depositDate, batchTotal, depositBank,
                currency);
        XSSFWorkbook workbook = xlsReportRenderer.createNewSheet(reportId, reportHeaderList);
        XSSFSheet sheet = workbook.getSheetAt(0);
        workbook = xlsReportRenderer.insertLogo(sheet, workbook);
        int count = sheet.getLastRowNum() + 1;
        if (batchPaymentList != null) {

            XSSFCellStyle dataStyle = xlsReportRenderer.createDataStyle(workbook);
            for (Payment payment : batchPaymentList) {
                int columnCount = 1;
                XSSFRow excelRow = xlsReportRenderer.createDataRow(sheet, count);
                excelRow = xlsReportRenderer.createStringCell(excelRow, columnCount++, dataStyle,
                        payment.getIdentifier());
                excelRow = xlsReportRenderer.createStringCell(excelRow, columnCount++, dataStyle,
                        payment.getIdentifierType());
                excelRow = xlsReportRenderer.createNumericCell(excelRow, columnCount++, dataStyle,
                        payment.getCurrentDue(), currency, workbook.createDataFormat());
                excelRow = xlsReportRenderer.createNumericCell(excelRow, columnCount++, dataStyle,
                        payment.getPaymentAmount(), currency, workbook.createDataFormat());
                excelRow = xlsReportRenderer.createStringCell(excelRow, columnCount++, dataStyle,
                        payment.getPaymentType());
                xlsReportRenderer.createStringCell(excelRow, columnCount, dataStyle, payment.getReviewCode());
                count = sheet.getLastRowNum() + 1;
            }
        } else {
            workbook = xlsReportRenderer.createNoDataRow(workbook,
                    sheet.getRow(sheet.getLastRowNum()).getLastCellNum());
        }

        return workbook;
    }

    private List<ReportHeader> setReportHeaders(String batchId, ZonedDateTime postedDate, ZonedDateTime depositDate,
            Double batchTotal, String depositBank, String currency) {
        List<ReportHeader> reportPropertyList = new ArrayList<>();

        ReportHeader reportHeader = new ReportHeader("batchId", batchId);
        reportPropertyList.add(reportHeader);

        reportHeader = new ReportHeader();
        reportHeader.setName("postedDate");
        reportHeader.setValue(DateRoutine.dateTimeAsMMDDYYYYAsString(postedDate));
        reportPropertyList.add(reportHeader);

        reportHeader = new ReportHeader();
        reportHeader.setName("depositDate");
        reportHeader.setValue(DateRoutine.dateTimeAsMMDDYYYYAsString(depositDate));
        reportPropertyList.add(reportHeader);

        reportHeader = new ReportHeader();
        reportHeader.setName("batchTotal");
        reportHeader.setValue(getCurrencySymbolByCode(currency) + String.valueOf(batchTotal));
        reportPropertyList.add(reportHeader);

        reportHeader = new ReportHeader("depositBank", depositBank);
        reportPropertyList.add(reportHeader);

        return reportPropertyList;

    }

    private String getCurrencySymbolByCode(String currencyCode) {
        String currencySymbol = currencyCode;
        if (currencyCode == null || currencyCode.trim().isEmpty()) {
            currencyCode = "USD";
        }
        Locale locale = LOCALE_CURRENCY_MAP.get(currencyCode.trim());
        if (locale != null) {
            Currency currency = Currency.getInstance(locale);
            currencySymbol = currency.getSymbol(locale);
        }
        return currencySymbol;
    }

    public static final Map<String, Locale> LOCALE_CURRENCY_MAP;
    static {
        final Map<String, Locale> m = new HashMap<>();

        m.put("USD", Locale.US);
        m.put("EUR", Locale.GERMANY);
        m.put("GBP", Locale.UK);
        m.put("JPY", Locale.JAPAN);

        LOCALE_CURRENCY_MAP = Collections.unmodifiableMap(m);
    }

}
