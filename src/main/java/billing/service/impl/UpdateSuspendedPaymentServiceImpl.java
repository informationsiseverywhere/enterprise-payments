package billing.service.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.time.ZonedDateTime;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import application.security.service.impl.AppSecurityServiceImpl;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.BilUnidCshHst;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.entity.id.BilUnIdCshHstId;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.data.repository.BilUnidCshHstRepository;
import billing.model.SuspendedPayment;
import billing.service.BillingAccountBalanceService;
import billing.service.UnidentifiedCashApplyService;
import billing.service.UpdateSuspendedPaymentService;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilRules;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;

@Service
public class UpdateSuspendedPaymentServiceImpl implements UpdateSuspendedPaymentService {

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private BilUnidCshHstRepository bilUnidCshHstRepository;

    @Autowired
    private UnidentifiedCashApplyService unidentifiedCashApplyService;

    @Autowired
    private BillingAccountBalanceService billingAccountBalanceService;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    private static final Logger logger = LogManager.getLogger(UpdateSuspendedPaymentServiceImpl.class);

    private static final String DOW_PAY_REASON = "DPR";

    @Override
    @Transactional
    public void save(Short sequenceNumber, ZonedDateTime postedDate, String userId, String batchId,
            ZonedDateTime distributionDate, SuspendedPayment suspendedPayment) {
        String userSequenceId = appSecurityServiceImpl.authenticateUser(userId).getUserSequenceId();
        Boolean isUpdate = true;
        String currencyCode = BLANK_STRING;
        logger.info("fetch unid payment from BIL_UNID_CASH.");
        BilUnIdCash bilUnidentifiedCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(distributionDate, sequenceNumber, batchId, userSequenceId, postedDate))
                .orElse(null);

        if (bilUnidentifiedCash == null) {
            logger.debug("Payment not found in BIL_UNID_CASH");
            throw new DataNotFoundException("payment.does.not.exists");
        }
        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId()))
                .orElse(null);
        if (bilCashEntryTot != null) {
            currencyCode = bilCashEntryTot.getCurrencyCd();
        }
        String previousIdentifier = bilUnidentifiedCash.getBilAccountNbr();

        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(BilRules.UNIDENTIFIED_CASH_HISTORY.getValue(),
                BLANK_STRING, BLANK_STRING, BLANK_STRING, execContext.getApplicationDate(),
                execContext.getApplicationDate());
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            logger.info("if UCSH rule is ON insert record in unidentified cash history");
            saveUnidentifiedCashHistory(bilUnidentifiedCash);
        }

        updateBilUnidentifiedCash(bilUnidentifiedCash, suspendedPayment);

        String folderId = generateFolderId(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getUserId());

        directUnidentifiedPayment(bilUnidentifiedCash, folderId,
                suspendedPayment.getOverrideBusinessGroupEditIndicator(), isUpdate, previousIdentifier, currencyCode);

        bilRulesUct = bilRulesUctRepository.getBilRulesRow(BilRules.ACCOUNT_BALANCE.getValue(), BLANK_STRING,
                BLANK_STRING, BLANK_STRING, execContext.getApplicationDate(), execContext.getApplicationDate());

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            logger.info("update total unidientified balance in the system.");
            billingAccountBalanceService.saveUnidentifiedBalance();
        }
    }

    private void directUnidentifiedPayment(BilUnIdCash bilUnidentifiedCash, String folderId, Boolean isBusGrpOverride,
            Boolean isUpdate, String previousIdentifier, String currencyCode) {
        if (!bilUnidentifiedCash.getBilAccountNbr().trim().isEmpty()) {
            unidentifiedCashApplyService.applyUnidentifiedCash(bilUnidentifiedCash.getBilAccountNbr().trim(),
                    bilUnidentifiedCash.getPolNbr(), bilUnidentifiedCash.getPolSymbolCd(), folderId,
                    bilUnidentifiedCash, AccountType.BILLACCOUNT.toString(), isBusGrpOverride, isUpdate,
                    previousIdentifier, currencyCode);
        } else if (!bilUnidentifiedCash.getBilTtyNbr().trim().isEmpty()) {
            unidentifiedCashApplyService.applyUnidentifiedCash(bilUnidentifiedCash.getBilTtyNbr().trim().trim(),
                    BLANK_STRING, BLANK_STRING, folderId, bilUnidentifiedCash, AccountType.GROUP_ACCOUNT.toString(),
                    isBusGrpOverride, isUpdate, previousIdentifier, currencyCode);
        } else if (!bilUnidentifiedCash.getPolNbr().trim().isEmpty()) {
            unidentifiedCashApplyService.applyUnidentifiedCash(bilUnidentifiedCash.getBilAccountNbr().trim(),
                    bilUnidentifiedCash.getPolNbr().trim(), bilUnidentifiedCash.getPolSymbolCd(), folderId,
                    bilUnidentifiedCash, BLANK_STRING, isBusGrpOverride, isUpdate, previousIdentifier, currencyCode);
        } else if (!bilUnidentifiedCash.getBilAgtActNbr().trim().isEmpty()) {
            unidentifiedCashApplyService.applyUnidentifiedCash(bilUnidentifiedCash.getBilAgtActNbr().trim(),
                    bilUnidentifiedCash.getPolNbr(), bilUnidentifiedCash.getPolSymbolCd(), folderId,
                    bilUnidentifiedCash, AccountType.AGENCY_ACCOUNT.toString(), isBusGrpOverride, isUpdate,
                    previousIdentifier, currencyCode);
        }
    }

    private String generateFolderId(ZonedDateTime distributionDate, Short sequenceNumber, String entryNumber,
            ZonedDateTime entryDate, String userId) {
        StringBuilder technicalObjectKey = new StringBuilder(36);
        technicalObjectKey.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(distributionDate), 10));
        technicalObjectKey.append(StringUtils.leftPad(sequenceNumber.toString(), 4, CHAR_ZERO));
        technicalObjectKey.append(BLANK_CHAR);
        technicalObjectKey.append(StringUtils.rightPad(entryNumber, 4));
        technicalObjectKey.append(StringUtils.rightPad(userId, 8));
        technicalObjectKey.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        return technicalObjectKey.toString();
    }

    private void updateBilUnidentifiedCash(BilUnIdCash bilUnidentifiedCash, SuspendedPayment suspendedPayment) {
        if (!suspendedPayment.getPolicyNumber().trim().isEmpty()) {
            bilUnidentifiedCash.setBilAccountNbr(SEPARATOR_BLANK);
            bilUnidentifiedCash.setBilTtyNbr(SEPARATOR_BLANK);
            bilUnidentifiedCash.setBilAgtActNbr(SEPARATOR_BLANK);
        }
        if (suspendedPayment.getIdentifierType().trim().equals(AccountType.BILLACCOUNT.toString())) {
            bilUnidentifiedCash.setBilAccountNbr(suspendedPayment.getIdentifier());
            bilUnidentifiedCash.setBilTtyNbr(SEPARATOR_BLANK);
            bilUnidentifiedCash.setBilAgtActNbr(SEPARATOR_BLANK);
        } else if (suspendedPayment.getIdentifierType().trim().equals(AccountType.GROUP_ACCOUNT.toString())) {
            bilUnidentifiedCash.setBilTtyNbr(suspendedPayment.getIdentifier());
            bilUnidentifiedCash.setBilAccountNbr(SEPARATOR_BLANK);
            bilUnidentifiedCash.setBilAgtActNbr(SEPARATOR_BLANK);
        } else if (suspendedPayment.getIdentifierType().trim().equals(AccountType.AGENCY_ACCOUNT.toString())) {
            bilUnidentifiedCash.setBilAgtActNbr(suspendedPayment.getIdentifier());
            bilUnidentifiedCash.setBilAccountNbr(SEPARATOR_BLANK);
            bilUnidentifiedCash.setBilTtyNbr(SEPARATOR_BLANK);
        }
        bilUnidentifiedCash.setPolNbr(suspendedPayment.getPolicyNumber().trim());
        bilUnidentifiedCash.setPolSymbolCd(suspendedPayment.getPolicySymbol());
        bilUnidentifiedCash.setBilPblItemCd(suspendedPayment.getPayableItem());
        bilUnidentifiedCash.setBilAdjDueDt(suspendedPayment.getDueDate());
        bilUnidentifiedCash.setActivityUserId(execContext.getUserSeqeunceId());
        bilUnidentifiedCash.setBilRctId(suspendedPayment.getPaymentId());
        bilUnidentifiedCash.setBilAdditionalId(suspendedPayment.getAdditionalId());
        bilUnidentifiedCash.setBilRctTypeCd(suspendedPayment.getPaymentType());
        if (suspendedPayment.getHoldSuspenseIndicator() != null) {
            bilUnidentifiedCash.setBilManualSusInd(
                    Boolean.TRUE.equals(suspendedPayment.getHoldSuspenseIndicator()) ? CHAR_Y : CHAR_N);
        }
        bilUnidentifiedCash.setBilDspReasonCd(suspendedPayment.getSuspenseReason());
        if (validateIfDownPayment(suspendedPayment.getPaymentType())) {
            bilUnidentifiedCash.setBilManualSusInd(CHAR_Y);
            bilUnidentifiedCash.setBilDspReasonCd(DOW_PAY_REASON);
        }
        bilUnidentifiedCash.setBilRctCmt(suspendedPayment.getPaymentComments());
        bilUnidentifiedCash.setBilDspDt(execContext.getApplicationDate());

        bilUnIdCashRepository.saveAndFlush(bilUnidentifiedCash);
    }

    private void saveUnidentifiedCashHistory(BilUnIdCash bilUnidentifiedCash) {
        BilUnidCshHst unidentifiedCashHistory = new BilUnidCshHst();

        unidentifiedCashHistory
                .setBilUnIdCshHstId(new BilUnIdCshHstId(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(), execContext.getApplicationDateTime()));
        unidentifiedCashHistory.setBilAccountNbr(bilUnidentifiedCash.getBilAccountNbr());
        unidentifiedCashHistory.setActivityUserId(bilUnidentifiedCash.getActivityUserId());
        unidentifiedCashHistory.setBilAdditionalId(bilUnidentifiedCash.getBilAdditionalId());
        unidentifiedCashHistory.setBilAdjDueDt(bilUnidentifiedCash.getBilAdjDueDt());
        unidentifiedCashHistory.setBilAgtActNbr(bilUnidentifiedCash.getBilAgtActNbr());
        unidentifiedCashHistory.setBilChkPrdMthCd(bilUnidentifiedCash.getBilChkPrdMthCd());
        unidentifiedCashHistory.setBilCwaId(bilUnidentifiedCash.getBilCwaId());
        unidentifiedCashHistory.setBilDepositDt(bilUnidentifiedCash.getBilDepositDt());
        unidentifiedCashHistory.setBilDsbId(BLANK_STRING);
        if (bilUnidentifiedCash.getBilDspTypeCd().trim().equals(BilDspTypeCode.DISBURSED.getValue())) {
            unidentifiedCashHistory.setBilDsbId(bilUnidentifiedCash.getBilDsbId());
        }
        unidentifiedCashHistory.setBilDspDt(bilUnidentifiedCash.getBilDspDt());
        unidentifiedCashHistory.setBilDspReasonCd(bilUnidentifiedCash.getBilDspReasonCd());
        unidentifiedCashHistory.setBilDspTypeCd(bilUnidentifiedCash.getBilDspTypeCd());
        unidentifiedCashHistory.setBilEntrySeqNbr(bilUnidentifiedCash.getBilEntrySeqNbr());
        unidentifiedCashHistory.setBilManualSusInd(bilUnidentifiedCash.getBilManualSusInd());
        unidentifiedCashHistory.setBilPayeeAdrSeq(bilUnidentifiedCash.getBilPayeeAdrSeq());
        unidentifiedCashHistory.setBilPayeeCltId(bilUnidentifiedCash.getBilPayeeCltId());
        unidentifiedCashHistory.setBilPblItemCd(bilUnidentifiedCash.getBilPblItemCd());
        unidentifiedCashHistory.setBilRctAmt(bilUnidentifiedCash.getBilRctAmt());
        unidentifiedCashHistory.setBilRctCmt(bilUnidentifiedCash.getBilRctCmt());
        unidentifiedCashHistory.setBilRctId(bilUnidentifiedCash.getBilRctId());
        unidentifiedCashHistory.setBilRctTypeCd(bilUnidentifiedCash.getBilRctTypeCd());
        unidentifiedCashHistory.setBilTtyNbr(bilUnidentifiedCash.getBilTtyNbr());
        unidentifiedCashHistory.setDdsDsbDt(bilUnidentifiedCash.getDdsDsbDt());
        unidentifiedCashHistory.setDwsCkDrfNbr(bilUnidentifiedCash.getDwsCkDrfNbr());
        unidentifiedCashHistory.setDwsStatusCd(bilUnidentifiedCash.getDwsStatusCd());
        unidentifiedCashHistory.setPolNbr(bilUnidentifiedCash.getPolNbr());
        unidentifiedCashHistory.setPolSymbolCd(bilUnidentifiedCash.getPolSymbolCd());
        bilUnidCshHstRepository.save(unidentifiedCashHistory);
    }

    private boolean validateIfDownPayment(String paymentType) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(paymentType,
                BusCodeTranslationType.DOWN_PAYMENT.getValue(), execContext.getLanguage());
        return busCodeTranslation != null;
    }
}
