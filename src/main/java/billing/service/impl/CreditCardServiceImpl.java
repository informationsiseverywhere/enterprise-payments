package billing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.repository.BilCrcrdArspDesRepository;
import billing.model.Payment;
import billing.service.CredicardService;
import core.utils.CommonConstants;

@Service
public class CreditCardServiceImpl implements CredicardService {

    @Autowired
    private BilCrcrdArspDesRepository bilCrcrdArspDesRepository;

    @Override
    public void processCreditRespone(Payment payment) {
        char fourthLastDigit = payment.getCreditCardNumber().charAt(payment.getCreditCardNumber().length() - 4);
        String lastThreeDigits = payment.getCreditCardNumber().substring(payment.getCreditCardNumber().length() - 3);
        getAuthorizationResponse(fourthLastDigit, payment);
        getAuthorizationResponseType(lastThreeDigits, payment);

    }

    private void getAuthorizationResponseType(String lastThreeDigits, Payment payment) {

        if (lastThreeDigits.equals("200")) {
            payment.setAuthorizationResponseType("200");
        } else if (lastThreeDigits.equals("300")) {
            payment.setAuthorizationResponseType("300");
        }
        if (lastThreeDigits.equals("400")) {
            payment.setAuthorizationResponseType("400");
        }
        if (lastThreeDigits.equals("987")) {
            payment.setAuthorizationResponseType("987");
            payment.setAuthorization(CommonConstants.BLANK_STRING);
            payment.setAuthorizationResponse(CommonConstants.SEPARATOR_BLANK);
        }
        if (lastThreeDigits.equals("765")) {
            payment.setAuthorizationResponseType(CommonConstants.BLANK_STRING);
            payment.setAuthorization(CommonConstants.BLANK_STRING);
            if (payment.getAuthorizationResponse().equals("A") || payment.getAuthorizationResponse().equals("S")) {
                payment.setAuthorization("123AB");
            }
        } else {
            payment.setAuthorizationResponseType("100");
        }

    }

    private void getAuthorizationResponse(char fourthLastDigit, Payment payment) {
        if (fourthLastDigit == '1') {
            payment.setAuthorization("123AB");
            payment.setAuthorizationResponse("A");
        } else if (fourthLastDigit == '2') {
            payment.setAuthorizationResponse("D");
        } else if (fourthLastDigit == '3') {
            payment.setAuthorizationResponse("U");
        } else if (fourthLastDigit == '4') {
            payment.setAuthorizationResponse("T");
        } else if (fourthLastDigit == '5') {
            payment.setAuthorizationResponse("M");
        } else if (fourthLastDigit == '6') {
            payment.setAuthorizationResponse("N");
        } else if (fourthLastDigit == '7') {
            payment.setAuthorizationResponse("Q");
        } else if (fourthLastDigit == '8') {
            payment.setAuthorizationResponse(CommonConstants.SEPARATOR_BLANK);
        } else if (fourthLastDigit == '9') {
            payment.setAuthorizationResponse("L");
        } else if (fourthLastDigit == '0') {
            payment.setAuthorizationResponse("R");
        }

        Integer count = bilCrcrdArspDesRepository.getByBilCrcrdArspCd(payment.getAuthorizationResponse().charAt(0));
        if (count == null) {
            payment.setAuthorizationResponse(CommonConstants.BLANK_STRING);
        }

    }

}
