package billing.service.impl;

import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_P;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilBankEntry;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilEftBankEd;
import billing.data.entity.id.BilBankEntryId;
import billing.data.entity.id.BilEftBankId;
import billing.data.repository.BilBankEntryRepository;
import billing.data.repository.BilEftBankEdRepository;
import billing.data.repository.BilEftBankRepository;
import billing.model.AddUpdateBankInformation;
import billing.service.AddUpdateBankInformationService;
import core.exception.database.DataNotFoundException;

@Service
public class AddUpdateBankInformationServiceImpl implements AddUpdateBankInformationService {

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BilBankEntryRepository bilBankEntryRepository;

    @Autowired
    private BilEftBankEdRepository bilEftBankEdRepository;

    @Override
    public void addBankInformation(AddUpdateBankInformation addUpdateBankInformation) {

        BilEftBank bilEftBnk = bilEftBankRepository.findEftBankEntry(addUpdateBankInformation.getBilAccountId(), CHAR_P,
                addUpdateBankInformation.getEffectiveDate(), CHAR_A);
        if (bilEftBnk != null && bilEftBnk.getBebAccountStaCd() == CHAR_A) {
            bilEftBankRepository.updateStatusCode(addUpdateBankInformation.getBilAccountId(), CHAR_P,
                    addUpdateBankInformation.getEffectiveDate(), CHAR_A, CHAR_O);
        }

        BilBankEntry bilBankEntry = bilBankEntryRepository
                .findById(new BilBankEntryId(addUpdateBankInformation.getBilEntryDate(),
                        addUpdateBankInformation.getBilEntryNumber(), addUpdateBankInformation.getUserId(),
                        addUpdateBankInformation.getBilEntrySeqNumber()))
                .orElse(null);
        if (bilBankEntry == null) {
            throw new DataNotFoundException("Bil Bank Entry not found");
        } else if (bilBankEntry.getWebAuthInd() == ' ') {
            bilBankEntry.setWebAuthInd('N');
        }

        BilEftBankEd bilEftBankEd = bilEftBankEdRepository.findByRoutingTransitNbr(bilBankEntry.getRoutingTransitNbr());
        if (bilEftBankEd == null) {
            bilEftBankEd = insertBilEftBankEd(bilBankEntry);
        }
        BilEftBank bilEftBank = new BilEftBank();
        bilEftBank.setBilEftBankId(new BilEftBankId(addUpdateBankInformation.getBilAccountId(), 'A',
                addUpdateBankInformation.getEffectiveDate()));
        bilEftBank.setBilBankAcctNbr(bilBankEntry.getBankAccountNbr());
        bilEftBank.setBilEftBankIdNbr(bilEftBankEd.getEftBankId());
        bilEftBank.setBebAccountTyc(bilBankEntry.getAccountTypCd());
        bilEftBank.setBebWebAutInd(bilBankEntry.getWebAuthInd());
        bilEftBank.setBebEftPayLngNm(bilBankEntry.getEftPayorLongName());
        bilEftBankRepository.save(bilEftBank);
    }

    private BilEftBankEd insertBilEftBankEd(BilBankEntry bilBankEntry) {
        Integer maxeftBankId = bilEftBankEdRepository.findMaxEftBankId();
        if (maxeftBankId == null) {
            maxeftBankId = 0;
        } else {
            maxeftBankId++;
        }
        BilEftBankEd bilEftBankEd = new BilEftBankEd();
        bilEftBankEd.setEftBankId(maxeftBankId);
        bilEftBankEd.setPreferredRoutingTransitNbr(BLANK_STRING);
        bilEftBankEd.setRoutingTransitNbr(bilBankEntry.getRoutingTransitNbr());
        bilEftBankEd.setBankName("PACS BANK");
        bilEftBankEd.setBankAddress1(BLANK_STRING);
        bilEftBankEd.setBankAddress2(BLANK_STRING);
        bilEftBankEd.setBankAddress3(BLANK_STRING);
        bilEftBankEd.setBankAddress4(BLANK_STRING);
        bilEftBankEd.setBankPhoneNbr(BLANK_STRING);
        bilEftBankEdRepository.saveAndFlush(bilEftBankEd);
        return bilEftBankEd;
    }

}
