package billing.service.impl;

import static billing.utils.BillingConstants.COMPANY_LOCATION_NUMBER;
import static billing.utils.BillingConstants.ERROR_CODE;
import static billing.utils.BillingConstants.MASTER_COMPANY_NUMBER;
import static billing.utils.BillingConstants.PLUS_SIGN;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilEftActivity;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.BilInvTtyAct;
import billing.data.entity.BilThirdParty;
import billing.data.entity.BilTtyCashDsp;
import billing.data.entity.BilTtyCashRct;
import billing.data.entity.BilTtySummary;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilEftActivityId;
import billing.data.entity.id.BilEftPendingTapeId;
import billing.data.entity.id.BilTtyCashDspId;
import billing.data.entity.id.BilTtyCashRctId;
import billing.data.entity.id.BilTtySummaryId;
import billing.data.repository.BilEftActivityRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilInvTtyActRepository;
import billing.data.repository.BilTtyCashDspRepository;
import billing.data.repository.BilTtyCashRctRepository;
import billing.data.repository.BilTtySummaryRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.service.GroupAccountPaymentService;
import billing.service.ReplacementPaymentService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CashEntryMethod;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.TechnicalKeyTypeCode;
import core.api.ExecContext;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.data.entity.id.BusCdTranslationId;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class GroupAccountPaymentServiceImpl implements GroupAccountPaymentService {

    @Autowired
    private BilTtyCashRctRepository bilTtyCashRctRepository;

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private BilTtyCashDspRepository bilTtyCashDspRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilEftActivityRepository bilEftActivityRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private BilTtySummaryRepository bilTtySummaryRepository;

    @Autowired
    private ReplacementPaymentService replacementPaymentService;

    @Autowired
    private BilInvTtyActRepository bilInvTtyActRepository;

    private static final List<Character> ECC_RECORDTYPE = Arrays.asList(EftRecordType.RECONCILED_CASH.getValue(),
            EftRecordType.UNIDENTIFIED.getValue(), EftRecordType.DIGITAL_WALLET.getValue());

    @Override
    @Transactional
    public void applyUnidentifiedCash(BilThirdParty bilThirdParty) {
        List<BilUnIdCash> unidentifiedCashList = bilUnIdCashRepository.findUnidentifiedPayment(
                bilThirdParty.getBilTtyNbr(), SEPARATOR_BLANK, SEPARATOR_BLANK, SEPARATOR_BLANK,
                BilDspTypeCode.SUSPENDED.getValue());
        if (unidentifiedCashList != null && !unidentifiedCashList.isEmpty()) {
            for (BilUnIdCash unidentifiedCash : unidentifiedCashList) {
                ZonedDateTime pendingTapedate = null;
                BilTtyCashRct bilTtyCashReceipt = saveGroupReceipt(bilThirdParty.getBillThirdPartyId(),
                        execContext.getApplicationDate(), unidentifiedCash);
                BilTtyCashDsp groupCashDisposition = saveGroupDisposition(bilThirdParty.getBillThirdPartyId(),
                        execContext.getApplicationDate(), unidentifiedCash,
                        bilTtyCashReceipt.getBilTtyCashRctId().getBilDtbSeqNbr());

                saveFinancialApi(bilThirdParty.getBillThirdPartyId(), execContext.getApplicationDate(),
                        unidentifiedCash, bilThirdParty.getBilTtyNbr(), BLANK_STRING, BLANK_STRING,
                        ObjectCode.UNIDENTIFIED_CASH.toString(), unidentifiedCash.getBilRctAmt(),
                        bilTtyCashReceipt.getBilEntryDt(), bilTtyCashReceipt.getBilEntryNbr(),
                        bilTtyCashReceipt.getUserId(), bilTtyCashReceipt.getBilEntrySeqNbr(),
                        bilThirdParty.getCurrencyCd());

                if (unidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()
                        || unidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()
                        || (isDigitalWalletPayment(unidentifiedCash.getBilCshEtrMthCd()) || unidentifiedCash
                                .getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod())
                                && checkIfEftCCPayment(unidentifiedCash.getBillUnidCashId())) {

                    pendingTapedate = updateEftDetails(unidentifiedCash, bilThirdParty.getBillThirdPartyId(),
                            bilTtyCashReceipt.getBilTtyCashRctId().getBilDtbSeqNbr());
                }
                if (!unidentifiedCash.getBilAdjDueDt().equals(DateRoutine.defaultDateTime())) {

                    BilInvTtyAct bilInvoiceThirdPartyAccount = bilInvTtyActRepository.getRow(
                            bilThirdParty.getBillThirdPartyId(), CHAR_T, groupCashDisposition.getBilAdjDueDt(),
                            BillingConstants.PAYMENT_REVERSED);

                    if (bilInvoiceThirdPartyAccount != null) {
                        replacementPaymentService.reapplyPaymentForThirdParty(bilThirdParty.getBillThirdPartyId(),
                                groupCashDisposition.getBilAdjDueDt(),
                                BigDecimal.valueOf(bilTtyCashReceipt.getBilRctAmt()),
                                groupCashDisposition.getBilDspReasonCd(), bilThirdParty.getBilTtyNbr(),
                                bilInvoiceThirdPartyAccount, bilTtyCashReceipt.getBilEntryDt(),
                                bilTtyCashReceipt.getBilEntryNbr(), bilTtyCashReceipt.getBilEntrySeqNbr(),
                                bilTtyCashReceipt.getBilTtyCashRctId().getBilDtbDt(),
                                bilTtyCashReceipt.getBilTtyCashRctId().getBilDtbSeqNbr(), bilTtyCashReceipt.getUserId(),
                                bilTtyCashReceipt.getBilCshEtrMthCd(), bilTtyCashReceipt.getBilBankCd(),
                                groupCashDisposition.getBilTtyCashDspId().getBilDspSeqNbr(),
                                AccountType.GROUP_ACCOUNT.toString());
                    }
                }
                saveFinancialApi(bilThirdParty.getBillThirdPartyId(), execContext.getApplicationDate(),
                        unidentifiedCash, bilThirdParty.getBilTtyNbr(), bilThirdParty.getBilTypeCd(),
                        bilThirdParty.getBilClassCd(), ObjectCode.CASH.toString(), unidentifiedCash.getBilRctAmt(),
                        bilTtyCashReceipt.getBilEntryDt(), bilTtyCashReceipt.getBilEntryNbr(),
                        bilTtyCashReceipt.getUserId(), bilTtyCashReceipt.getBilEntrySeqNbr(),
                        bilThirdParty.getCurrencyCd());

                saveThirdPartySummary(bilThirdParty.getBillThirdPartyId(), unidentifiedCash.getBilCshEtrMthCd(),
                        pendingTapedate, unidentifiedCash.getBilPstmrkDt(), unidentifiedCash.getBilRctTypeCd(),
                        unidentifiedCash.getBilRctAmt(), unidentifiedCash.getBilUnIdCashId().getUserId());

                bilUnIdCashRepository.delete(unidentifiedCash);
            }
        }
    }

    private boolean checkIfEftCCPayment(String technicalKeyId) {
        Integer rowCount = bilEftActivityRepository.checkIfExists(technicalKeyId, ECC_RECORDTYPE);
        if (rowCount != null && rowCount > SHORT_ZERO) {
            return true;
        } else {
            rowCount = bilEftPendingTapeRepository.checkIfExists(technicalKeyId, ECC_RECORDTYPE);
            if (rowCount != null && rowCount > SHORT_ZERO) {
                return true;
            }
        }
        return false;
    }

    private void saveFinancialApi(String accountId, ZonedDateTime distributionDate, BilUnIdCash unidentifiedCash,
            String accountNumber, String bilTypeCode, String bilClassCode, String transactionObjectCode, Double amount,
            ZonedDateTime entryDate, String entryNumber, String userId, Short entrySeqenceNumber, String currencyCode) {

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setFolderId(accountId);
        financialApiActivity.setApplicationName(ApplicationName.BACPDUC.toString());
        financialApiActivity.setFunctionCode(CHAR_A);
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setTransactionActionCode(ActionCode.SUSPENSE.toString());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity
                .setApplicationProductIdentifier(FinancialIntegratorConstants.ApplicationProduct.BCMS.toString());
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(distributionDate));
        financialApiActivity.setAppProgramId(ApplicationName.BACPDUC.toString());
        financialApiActivity.setBilBankCode(unidentifiedCash.getBilBankCd());
        financialApiActivity.setAgentTtyId(accountNumber);

        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setBilReceiptTypeCode(unidentifiedCash.getBilRctTypeCd());
        financialApiActivity.setBilTypeCode(bilTypeCode);
        financialApiActivity.setBilClassCode(bilClassCode);
        financialApiActivity.setBilDatabaseKey(generateDatabaseKey(entryDate, entryNumber, userId, entrySeqenceNumber));
        financialApiActivity.setBilSourceCode(String.valueOf(unidentifiedCash.getBilCshEtrMthCd()));
        financialApiActivity.setSummaryEffectiveDate(DateRoutine.defaultDateTime());
        financialApiActivity.setOriginalTimeStamp(execContext.getApplicationDateTime());
        financialApiActivity.setBilReasonCode(unidentifiedCash.getBilDspReasonCd());
        financialApiActivity.setCurrencyCode(currencyCode);

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                execContext.getApplicationDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

    private String generateDatabaseKey(ZonedDateTime entryDate, String entryNumber, String userId,
            Short entrySequenceNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        sb.append(StringUtils.rightPad(entryNumber, 4));
        sb.append(StringUtils.rightPad(userId, 8));
        sb.append(PLUS_SIGN);
        sb.append(StringUtils.rightPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
        return sb.toString();
    }

    private ZonedDateTime updateEftDetails(BilUnIdCash unidentifiedCash, String accountId,
            Short distributionSequenceNumber) {
        BilEftPendingTape bilEftPendingTape = bilEftPendingTapeRepository.findUnidentifiedPendingRow(
                unidentifiedCash.getBillUnidCashId(), EftRecordType.UNIDENTIFIED.getValue(), DECIMAL_ZERO);
        ZonedDateTime pendingTapeDate = DateRoutine.defaultDateTime();
        if (bilEftPendingTape != null) {
            pendingTapeDate = updateBilEftPendingTape(bilEftPendingTape, accountId,
                    unidentifiedCash.getBilCshEtrMthCd(), distributionSequenceNumber);
        } else {
            BilEftActivity bilEftActivity = bilEftActivityRepository.findUnidentifiedActivityRow(
                    unidentifiedCash.getBillUnidCashId(), EftRecordType.UNIDENTIFIED.getValue(), DECIMAL_ZERO);
            if (bilEftActivity != null) {
                updateBilEftActivity(bilEftActivity, accountId);
            }
        }
        return pendingTapeDate;
    }

    private BilTtyCashRct saveGroupReceipt(String accountId, ZonedDateTime distributionDate,
            BilUnIdCash unidentifiedCash) {
        BilTtyCashRct groupReceipt = new BilTtyCashRct();
        groupReceipt.setBilTtyCashRctId(new BilTtyCashRctId(accountId, distributionDate,
                getIncrementalValue(bilTtyCashRctRepository.getMaxBilDtbSequenceNumber(accountId, distributionDate))));
        groupReceipt.setBilPstmrkCd(unidentifiedCash.getBilPstmrkCd());
        groupReceipt.setBilEntryDt(unidentifiedCash.getBilUnIdCashId().getBilEntryDt());
        groupReceipt.setBilEntryNbr(unidentifiedCash.getBilUnIdCashId().getBilEntryNbr());
        groupReceipt.setBilEntrySeqNbr(unidentifiedCash.getBilEntrySeqNbr());
        groupReceipt.setUserId(unidentifiedCash.getBilUnIdCashId().getUserId());
        groupReceipt.setBilRctAmt(unidentifiedCash.getBilRctAmt());
        groupReceipt.setBilRctTypeCd(unidentifiedCash.getBilRctTypeCd());
        groupReceipt.setBilRctId(unidentifiedCash.getBilRctId());
        groupReceipt.setBilRctCmt(unidentifiedCash.getBilRctCmt());
        groupReceipt.setBilTrfTypeCd(BLANK_CHAR);
        groupReceipt.setBilRctReceiveDt(unidentifiedCash.getBilRctReceiveDt());
        groupReceipt.setBilBankCd(unidentifiedCash.getBilBankCd());
        groupReceipt.setBilCrcrdTypeCd(unidentifiedCash.getBilCrcrdTypeCd());
        groupReceipt.setBilCrcrdActNbr(unidentifiedCash.getBilCrcrdActNbr());
        groupReceipt.setBilCrcrdExpDt(unidentifiedCash.getBilCrcrdExpDt());
        groupReceipt.setBilCrcrdAutApv(unidentifiedCash.getBilCrcrdAutApv());
        groupReceipt.setBilCshEtrMthCd(unidentifiedCash.getBilCshEtrMthCd());
        groupReceipt.setBilTaxYearNbr(unidentifiedCash.getBilTaxYearNbr());
        groupReceipt.setBilPstmrkDt(unidentifiedCash.getBilPstmrkDt());
        groupReceipt.setBilDepositDt(unidentifiedCash.getBilDepositDt());
        groupReceipt.setBilRctCmt(unidentifiedCash.getBilRctCmt());
        bilTtyCashRctRepository.save(groupReceipt);
        return groupReceipt;
    }

    private BilTtyCashDsp saveGroupDisposition(String accountId, ZonedDateTime distributionDate,
            BilUnIdCash unidentifiedCash, Short distributionSequenceNumber) {
        BilTtyCashDsp groupCashDisposition = new BilTtyCashDsp();
        groupCashDisposition.setBilTtyCashDspId(new BilTtyCashDspId(accountId, distributionDate,
                distributionSequenceNumber, getIncrementalValue(bilTtyCashDspRepository
                        .findMaxDispositionNumber(accountId, distributionDate, distributionSequenceNumber))));
        groupCashDisposition.setBilAdjDueDt(unidentifiedCash.getBilAdjDueDt());
        groupCashDisposition.setUserId(unidentifiedCash.getBilUnIdCashId().getUserId());
        groupCashDisposition.setBilDspDt(unidentifiedCash.getBilDspDt());
        groupCashDisposition.setBilDspTypeCd(unidentifiedCash.getBilDspTypeCd());
        groupCashDisposition.setBilDspReasonCd(unidentifiedCash.getBilDspReasonCd());
        groupCashDisposition.setBilDspAmt(unidentifiedCash.getBilRctAmt());
        groupCashDisposition.setBilPayeeCltId(unidentifiedCash.getBilPayeeCltId());
        groupCashDisposition.setBilPayeeAdrSeq(unidentifiedCash.getBilPayeeAdrSeq());
        groupCashDisposition.setBilManualSusInd(unidentifiedCash.getBilManualSusInd());
        groupCashDisposition.setDwsStatusCd(unidentifiedCash.getDwsStatusCd());
        groupCashDisposition.setBilDsbId(unidentifiedCash.getBilDsbId());
        groupCashDisposition.setBilChkPrdMthCd(unidentifiedCash.getBilChkPrdMthCd());
        groupCashDisposition.setDdsDsbDt(DateRoutine.defaultDateTime());
        bilTtyCashDspRepository.saveAndFlush(groupCashDisposition);

        return groupCashDisposition;
    }

    private void saveThirdPartySummary(String accountId, Character cashEntryMethod, ZonedDateTime eftPendingTapeDate,
            ZonedDateTime postmarkDate, String receivedTypeCode, Double amount, String userId) {
        BilTtySummary bilThirdPartySummary = new BilTtySummary();
        Short sequenceNumber = getIncrementalValue(
                bilTtySummaryRepository.getMaxSeqNumber(accountId, execContext.getApplicationDate()));
        bilThirdPartySummary
                .setBilTtySummaryId(new BilTtySummaryId(accountId, execContext.getApplicationDate(), sequenceNumber));
        switch (CashEntryMethod.getEnumValue(cashEntryMethod)) {

        case CREDIT_CARD:
            bilThirdPartySummary.setBilDesReaTyp(BilDesReasonType.CPY.getValue());
            bilThirdPartySummary.setBilAcyDes1Dt(eftPendingTapeDate);
            break;

        case EFT_CREDIT_CARD:
        case EFT:
            bilThirdPartySummary.setBilDesReaTyp(isEftDigitalWallet(cashEntryMethod, receivedTypeCode)
                    ? BilDesReasonType.EFT_WALLET_PAYMENT_TYPE.getValue()
                    : BLANK_STRING);
            bilThirdPartySummary.setBilAcyDes1Dt(DateRoutine.defaultDateTime());
            break;

        case ONE_TIME_ACH:
            bilThirdPartySummary.setBilDesReaTyp(BilDesReasonType.ACH.getValue());
            bilThirdPartySummary.setBilAcyDes1Dt(DateRoutine.defaultDateTime());
            break;

        case BANK_ACCOUNT:
            bilThirdPartySummary.setBilDesReaTyp(BilDesReasonType.ONE_XA.getValue());
            bilThirdPartySummary.setBilAcyDes1Dt(eftPendingTapeDate);
            break;

        default:
            if (isDigitalWalletPayment(cashEntryMethod)) {
                bilThirdPartySummary.setBilDesReaTyp(BilDesReasonType.WALLET_PAYMENT_TYPE.getValue());
                bilThirdPartySummary.setBilAcyDes1Dt(DateRoutine.defaultDateTime());

            } else {
                bilThirdPartySummary.setBilDesReaTyp(findPaymentTypeDescription(receivedTypeCode));
                bilThirdPartySummary.setBilAcyDes1Dt(DateRoutine.defaultDateTime());

            }
            break;
        }
        bilThirdPartySummary.setBilAcyTs(execContext.getApplicationDateTime());
        bilThirdPartySummary.setBilAcyDesCd(receivedTypeCode);
        bilThirdPartySummary.setBilAcyDes2Dt(postmarkDate);
        bilThirdPartySummary.setBilAcyAmt(amount);
        bilThirdPartySummary.setUserId(userId);

        bilTtySummaryRepository.save(bilThirdPartySummary);

    }

    private String findPaymentTypeDescription(String receiptTypeCode) {
        List<BusCdTranslation> busCodeTranslation = busCdTranslationRepository.findAllByCodeAndTypeList(receiptTypeCode,
                Arrays.asList(BusCodeTranslationType.PAYMENT_TYPE_CODE.getValue(),
                        BusCodeTranslationType.CASH_PAYMENT.getValue(),
                        BusCodeTranslationType.COLLECT_AGENCY_PAYMENT.getValue(),
                        BusCodeTranslationType.EXTERNAL_BANK_PAYMENT.getValue(),
                        BilDesReasonType.DISPOSITION_REASON.getValue(), BilDesReasonType.ONE_XA.getValue(),
                        BusCodeTranslationType.OTHER_PAYMENT.getValue()),
                execContext.getLanguage());
        if (busCodeTranslation != null) {
            receiptTypeCode = busCodeTranslation.get(0).getBusCdTranslationId().getBusType().trim();
        }
        return receiptTypeCode;
    }

    private Short getIncrementalValue(Short sequenceNumber) {
        if (sequenceNumber == null) {
            return SHORT_ZERO;
        } else {
            return (short) (sequenceNumber + 1);
        }
    }

    private ZonedDateTime updateBilEftPendingTape(BilEftPendingTape bilEftPendingTapeRow, String accountId,
            Character cashEntryMethodCode, Short distributionSequenceNumber) {

        BilEftPendingTape bilEftPendingTape = new BilEftPendingTape();
        Short sequenceNumber = getIncrementalValue(bilEftPendingTapeRepository.getMaxSequenceNumber(accountId));
        bilEftPendingTape
                .setBilEftPendingTapeId(new BilEftPendingTapeId(accountId, TechnicalKeyTypeCode.THIRD_PARTY.getValue(),
                        bilEftPendingTapeRow.getBilEftPendingTapeId().getEftTapeDate(),
                        bilEftPendingTapeRow.getBilEftPendingTapeId().getEftActivityDate(), sequenceNumber));
        ZonedDateTime pendingTapedate = bilEftPendingTapeRow.getBilEftPendingTapeId().getEftTapeDate();
        bilEftPendingTape.setEftPostPaymentDate(execContext.getApplicationDate());
        bilEftPendingTape.setReceiptSequenceNumber(distributionSequenceNumber);
        bilEftPendingTape.setAgencyNbr(bilEftPendingTapeRow.getAgencyNbr());
        bilEftPendingTape.setBusinessActivityCd(bilEftPendingTapeRow.getBusinessActivityCd());
        bilEftPendingTape.setEftDrAmount(bilEftPendingTapeRow.getEftDrAmount());
        bilEftPendingTape.setEftDrDate(bilEftPendingTapeRow.getEftDrDate());
        bilEftPendingTape.setEftNotIndicator(bilEftPendingTapeRow.getEftNotIndicator());
        bilEftPendingTape.setEftReceivedType(bilEftPendingTapeRow.getEftReceivedType());
        bilEftPendingTape.setFileTypeCd(bilEftPendingTapeRow.getFileTypeCd());
        bilEftPendingTape.setInsertedRowTime(bilEftPendingTapeRow.getInsertedRowTime());
        bilEftPendingTape.setPaymentOrderId(bilEftPendingTapeRow.getPaymentOrderId());
        bilEftPendingTape.setWebAutomatedIndicator(bilEftPendingTapeRow.getWebAutomatedIndicator());
        if (cashEntryMethodCode.equals(CashEntryMethod.CREDIT_CARD.getCashEntryMethod())) {
            bilEftPendingTape.setEftReceivedType(CHAR_T);
        }
        bilEftPendingTapeRepository.delete(bilEftPendingTapeRow);
        bilEftPendingTapeRepository.save(bilEftPendingTape);

        return pendingTapedate;
    }

    private void updateBilEftActivity(BilEftActivity bilEftActivityRow, String accountId) {

        BilEftActivity bilEftActivity = new BilEftActivity();

        Short sequenceNumber = getIncrementalValue(bilEftActivityRepository.getMaxSequenceNumber(accountId));
        bilEftActivity.setBilEftActivityId(new BilEftActivityId(accountId, TechnicalKeyTypeCode.THIRD_PARTY.getValue(),
                bilEftActivityRow.getBilEftActivityId().getBilEftDrDt(),
                bilEftActivityRow.getBilEftActivityId().getBilEftTraceNbr(), execContext.getApplicationDate(),
                sequenceNumber));
        bilEftActivity.setAgencyNbr(bilEftActivityRow.getAgencyNbr());
        bilEftActivity.setBeaTrsSta(bilEftActivityRow.getBeaTrsSta());
        bilEftActivity.setBilBusActCd(bilEftActivityRow.getBilBusActCd());
        bilEftActivity.setBilDsbId(bilEftActivityRow.getBilDsbId());
        bilEftActivity.setBilEftAcyDt(bilEftActivityRow.getBilEftAcyDt());
        bilEftActivity.setBilEftDrAmt(bilEftActivityRow.getBilEftDrAmt());
        bilEftActivity.setBilEftNotInd(bilEftActivityRow.getBilEftNotInd());
        bilEftActivity.setBilEftTapeDt(bilEftActivityRow.getBilEftTapeDt());
        bilEftActivity.setBilFileTypeCd(bilEftActivityRow.getBilFileTypeCd());
        bilEftActivity.setBilPmtOrderId(bilEftActivityRow.getBilPmtOrderId());
        bilEftActivity.setInsertedRowTs(bilEftActivityRow.getInsertedRowTs());
        bilEftActivity.setWebAutInd(bilEftActivityRow.getWebAutInd());
        bilEftActivity.setBilEftRcdTyc(CHAR_T);

        bilEftActivityRepository.delete(bilEftActivityRow);
        bilEftActivityRepository.save(bilEftActivity);
    }

    private boolean isDigitalWalletPayment(Character cashEntryMethodCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(
                String.valueOf(cashEntryMethodCode), BusCodeTranslationType.DIGITAL_WALLET_TYPE.getValue(),
                execContext.getLanguage());

        return busCdTranslation != null ? Boolean.TRUE : Boolean.FALSE;
    }

    private boolean isEftDigitalWallet(Character cashEntryMethod, String receivedTypeCode) {
        return busCdTranslationRepository.existsById(new BusCdTranslationId(String.valueOf(cashEntryMethod),
                BilDesReasonType.PYS.getValue(), execContext.getLanguage(), receivedTypeCode)) ? Boolean.TRUE
                        : Boolean.FALSE;
    }
}
