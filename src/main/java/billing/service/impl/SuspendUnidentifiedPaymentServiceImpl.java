package billing.service.impl;

import static billing.utils.BillingConstants.ERROR_CODE;
import static billing.utils.BillingConstants.MASTER_COMPANY_NUMBER;
import static billing.utils.BillingConstants.PLUS_SIGN;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_U;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_10;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;
import static disbursement.utils.DisbursementConstants.COMPANY_LOCATION_NUMBER;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import application.security.service.impl.AppSecurityServiceImpl;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.BilUnidCshHst;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.entity.id.BilUnIdCshHstId;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.data.repository.BilUnidCshHstRepository;
import billing.service.BillingAccountBalanceService;
import billing.service.SuspendUnidentifiedPaymentService;
import billing.service.UnidentifiedCashApplyService;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilDesReasonCode;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilRules;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import core.utils.IdGenerator;
import disbursement.data.entity.DwsDisbursement;
import disbursement.service.DisbursementAPIService;
import disbursement.utils.DisbursementConstants.DisbursementIdType;
import disbursement.utils.DisbursementConstants.DisbursementStatusCode;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class SuspendUnidentifiedPaymentServiceImpl implements SuspendUnidentifiedPaymentService {

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilUnidCshHstRepository bilUnidCshHstRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private DisbursementAPIService disbursementAPIService;

    @Autowired
    private BillingAccountBalanceService billingAccountBalanceService;

    @Autowired
    private UnidentifiedCashApplyService unidentifiedCashApplyService;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    private static final Logger logger = LogManager.getLogger(SuspendUnidentifiedPaymentServiceImpl.class);

    @Override
    @Transactional
    public void save(Short sequenceNumber, ZonedDateTime postedDate, String userId, String batchId,
            ZonedDateTime distributionDate) {

        String userSequenceId = appSecurityServiceImpl.authenticateUser(userId).getUserSequenceId();
        Boolean isUpdate = false;
        String currencyCode = BLANK_STRING;
        logger.info("fetch unid payment from BIL_UNID_CASH.");
        BilUnIdCash bilUnidentifiedCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(distributionDate, sequenceNumber, batchId, userSequenceId, postedDate))
                .orElse(null);

        if (bilUnidentifiedCash == null) {
            logger.debug("Payment not found in BIL_UNID_CASH");
            throw new DataNotFoundException("payment.does.not.exists");
        }

        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId()))
                .orElse(null);
        if (bilCashEntryTot != null) {
            currencyCode = bilCashEntryTot.getCurrencyCd();
        }

        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(BilRules.UNIDENTIFIED_CASH_HISTORY.getValue(),
                BLANK_STRING, BLANK_STRING, BLANK_STRING, execContext.getApplicationDate(),
                execContext.getApplicationDate());
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            logger.info("if UCSH rule is ON insert record in unidentified cash history");
            saveUnidentifiedCashHistory(bilUnidentifiedCash);
        }
        String folderId;
        switch (BilDspTypeCode.getEnumKey(bilUnidentifiedCash.getBilDspTypeCd())) {
        case WRITE_OFF:
            folderId = suspendWriteOff(bilUnidentifiedCash, currencyCode);
            break;
        case DISBURSED:
            folderId = suspendDisburse(bilUnidentifiedCash, currencyCode);
            break;
        default:
            logger.debug("Only disbursed/WriteOff row can be suspended");
            throw new InvalidDataException("row.selected.does.not.qualify.for", new Object[] { "Suspend" });
        }
        if (bilUnidentifiedCash.getBilManualSusInd() != CHAR_Y) {
            directUnidentifiedPayment(bilUnidentifiedCash, folderId, false, isUpdate, currencyCode);
        }
        bilRulesUct = bilRulesUctRepository.getBilRulesRow(BilRules.ACCOUNT_BALANCE.getValue(), BLANK_STRING,
                BLANK_STRING, BLANK_STRING, execContext.getApplicationDate(), execContext.getApplicationDate());

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            logger.info("update total unidientified balance in the system.");
            billingAccountBalanceService.saveUnidentifiedBalance();
        }
    }

    private void directUnidentifiedPayment(BilUnIdCash bilUnidentifiedCash, String folderId, Boolean isBusGrpOverride,
            Boolean isUpdate, String currencyCode) {
        if (!bilUnidentifiedCash.getBilAccountNbr().trim().isEmpty()) {
            unidentifiedCashApplyService.applyUnidentifiedCash(bilUnidentifiedCash.getBilAccountNbr().trim(),
                    bilUnidentifiedCash.getPolNbr().trim(), bilUnidentifiedCash.getPolSymbolCd(), folderId,
                    bilUnidentifiedCash, AccountType.BILLACCOUNT.toString(), isBusGrpOverride, isUpdate, BLANK_STRING,
                    currencyCode);

        } else if (!bilUnidentifiedCash.getBilTtyNbr().trim().isEmpty()) {
            unidentifiedCashApplyService.applyUnidentifiedCash(bilUnidentifiedCash.getBilTtyNbr().trim().trim(),
                    BLANK_STRING, BLANK_STRING, folderId, bilUnidentifiedCash, AccountType.GROUP_ACCOUNT.toString(),
                    isBusGrpOverride, isUpdate, BLANK_STRING, currencyCode);

        } else if (!bilUnidentifiedCash.getPolNbr().trim().isEmpty()) {
            unidentifiedCashApplyService.applyUnidentifiedCash(bilUnidentifiedCash.getBilAccountNbr().trim(),
                    bilUnidentifiedCash.getPolNbr().trim(), bilUnidentifiedCash.getPolSymbolCd(), folderId,
                    bilUnidentifiedCash, BLANK_STRING, isBusGrpOverride, isUpdate, BLANK_STRING, currencyCode);

        } else if (!bilUnidentifiedCash.getBilAgtActNbr().trim().isEmpty()) {
            unidentifiedCashApplyService.applyUnidentifiedCash(bilUnidentifiedCash.getBilAgtActNbr().trim(),
                    bilUnidentifiedCash.getPolNbr(), bilUnidentifiedCash.getPolSymbolCd(), folderId,
                    bilUnidentifiedCash, AccountType.AGENCY_ACCOUNT.toString(), isBusGrpOverride, isUpdate,
                    BLANK_STRING, currencyCode);
        }
    }

    private String suspendDisburse(BilUnIdCash bilUnidentifiedCash, String currencyCode) {
        String folderId = generateFolderId(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getUserId());

        DwsDisbursement dwsDisbusrement = disbursementAPIService.checkDisbursement(bilUnidentifiedCash.getBilDsbId());
        disbursementAPIService.cancelDisbursement(folderId, bilUnidentifiedCash.getBilPayeeCltId(), BLANK_STRING,
                dwsDisbusrement.getDwsMediumCd(), dwsDisbusrement.getDwsPrcLocCd(),
                BigDecimal.valueOf(dwsDisbusrement.getDdsDspAmt()).negate().doubleValue(),
                dwsDisbusrement.getDwsCkDrfNbr(), bilUnidentifiedCash.getBilDsbId(), Character.toString(CHAR_U),
                bilUnidentifiedCash.getBilPayeeAdrSeq());

        saveFinancialActivity(folderId, bilUnidentifiedCash, ObjectCode.UNIDENTIFIED_CASH.toString(),
                ActionCode.AUTHOR_DISBURSEMENT.toString(),
                BigDecimal.valueOf(bilUnidentifiedCash.getBilRctAmt()).negate().doubleValue(), generateBusinessCaseId(),
                bilUnidentifiedCash.getBilDspReasonCd(), currencyCode, ApplicationProgramIdentifier.BACPUMS.toString());
        saveFinancialActivity(folderId, bilUnidentifiedCash, ObjectCode.UNIDENTIFIED_CASH.toString(),
                ActionCode.UPDATED.toString(), bilUnidentifiedCash.getBilRctAmt(), BLANK_STRING,
                DisbursementStatusCode.CANCEL.toString(), currencyCode,
                ApplicationProgramIdentifier.BACPUMS.toString());
        String reason = checkForDownPayment(bilUnidentifiedCash.getBilRctTypeCd().trim());
        updateBilUnidentifiedCash(bilUnidentifiedCash, reason);
        saveFinancialActivity(folderId, bilUnidentifiedCash, ObjectCode.UNIDENTIFIED_CASH.toString(),
                ActionCode.SUSPENSE.toString(),
                BigDecimal.valueOf(bilUnidentifiedCash.getBilRctAmt()).negate().doubleValue(), BLANK_STRING,
                bilUnidentifiedCash.getBilDspReasonCd(), currencyCode, ApplicationProgramIdentifier.BACPDUC.toString());
        saveFinancialActivity(folderId, bilUnidentifiedCash, ObjectCode.UNIDENTIFIED_CASH.toString(),
                ActionCode.SUSPENSE.toString(), bilUnidentifiedCash.getBilRctAmt(), BLANK_STRING,
                bilUnidentifiedCash.getBilDspReasonCd(), currencyCode, ApplicationProgramIdentifier.BACPDUC.toString());

        return folderId;

    }

    private String suspendWriteOff(BilUnIdCash bilUnidentifiedCash, String currencyCode) {

        String reason = checkForDownPayment(bilUnidentifiedCash.getBilRctTypeCd().trim());
        String folderId = generateFolderId(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                bilUnidentifiedCash.getBilUnIdCashId().getUserId());
        saveFinancialActivity(folderId, bilUnidentifiedCash, ObjectCode.UNIDENTIFIED_CASH.toString(),
                ActionCode.WRITE_OFF.toString(),
                BigDecimal.valueOf(bilUnidentifiedCash.getBilRctAmt()).negate().doubleValue(), generateBusinessCaseId(),
                bilUnidentifiedCash.getBilDspReasonCd(), currencyCode, ApplicationProgramIdentifier.BACPUMS.toString());

        updateBilUnidentifiedCash(bilUnidentifiedCash, reason);

        saveFinancialActivity(folderId, bilUnidentifiedCash, ObjectCode.UNIDENTIFIED_CASH.toString(),
                ActionCode.SUSPENSE.toString(),
                BigDecimal.valueOf(bilUnidentifiedCash.getBilRctAmt()).negate().doubleValue(), BLANK_STRING,
                bilUnidentifiedCash.getBilDspReasonCd(), currencyCode, ApplicationProgramIdentifier.BACPDUC.toString());
        saveFinancialActivity(folderId, bilUnidentifiedCash, ObjectCode.UNIDENTIFIED_CASH.toString(),
                ActionCode.SUSPENSE.toString(), bilUnidentifiedCash.getBilRctAmt(), BLANK_STRING,
                bilUnidentifiedCash.getBilDspReasonCd(), currencyCode, ApplicationProgramIdentifier.BACPDUC.toString());
        return folderId;
    }

    private void updateBilUnidentifiedCash(BilUnIdCash bilUnidentifiedCash, String reason) {
        bilUnidentifiedCash.setBilDspTypeCd(BilDspTypeCode.SUSPENDED.getValue());
        bilUnidentifiedCash.setBilDspReasonCd(reason);
        bilUnidentifiedCash.setBilAcyTs(execContext.getApplicationDateTime());
        bilUnidentifiedCash.setActivityUserId(execContext.getUserSeqeunceId());
        bilUnidentifiedCash.setBilDspDt(execContext.getApplicationDate());
        bilUnidentifiedCash.setBilPayeeAdrSeq(SHORT_ZERO);
        bilUnidentifiedCash.setBilPayeeCltId(BLANK_STRING);
        bilUnidentifiedCash.setBilDsbId(BLANK_STRING);
        bilUnidentifiedCash.setBilChkPrdMthCd(BLANK_CHAR);
        bilUnidentifiedCash.setDdsDsbDt(execContext.getApplicationDate());
        bilUnidentifiedCash.setDwsStatusCd(BLANK_CHAR);
        bilUnidentifiedCash.setDwsCkDrfNbr(SHORT_ZERO);
        bilUnIdCashRepository.saveAndFlush(bilUnidentifiedCash);
    }

    private String checkForDownPayment(String receiptTypeCode) {

        String reason;
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(receiptTypeCode,
                BusCodeTranslationType.DOWN_PAYMENT.getValue(), execContext.getLanguage());
        if (busCodeTranslation != null) {
            reason = BilDesReasonCode.DOWN_PAYMENT_RECEIPT.getValue();
        } else {
            reason = BilDesReasonCode.OPERATOR_SUSPEND.getValue();
        }
        return reason;
    }

    private void saveUnidentifiedCashHistory(BilUnIdCash bilUnidentifiedCash) {
        BilUnidCshHst unidentifiedCashHistory = new BilUnidCshHst();

        unidentifiedCashHistory
                .setBilUnIdCshHstId(new BilUnIdCshHstId(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(), execContext.getApplicationDateTime()));
        unidentifiedCashHistory.setBilAccountNbr(bilUnidentifiedCash.getBilAccountNbr());
        unidentifiedCashHistory.setActivityUserId(bilUnidentifiedCash.getActivityUserId());
        unidentifiedCashHistory.setBilAdditionalId(bilUnidentifiedCash.getBilAdditionalId());
        unidentifiedCashHistory.setBilAdjDueDt(bilUnidentifiedCash.getBilAdjDueDt());
        unidentifiedCashHistory.setBilAgtActNbr(bilUnidentifiedCash.getBilAgtActNbr());
        unidentifiedCashHistory.setBilChkPrdMthCd(bilUnidentifiedCash.getBilChkPrdMthCd());
        unidentifiedCashHistory.setBilCwaId(bilUnidentifiedCash.getBilCwaId());
        unidentifiedCashHistory.setBilDepositDt(bilUnidentifiedCash.getBilDepositDt());
        unidentifiedCashHistory.setBilDsbId(BLANK_STRING);
        if (bilUnidentifiedCash.getBilDspTypeCd().trim().equals(BilDspTypeCode.DISBURSED.getValue())) {
            unidentifiedCashHistory.setBilDsbId(bilUnidentifiedCash.getBilDsbId());
        }
        unidentifiedCashHistory.setBilDspDt(bilUnidentifiedCash.getBilDspDt());
        unidentifiedCashHistory.setBilDspReasonCd(bilUnidentifiedCash.getBilDspReasonCd());
        unidentifiedCashHistory.setBilDspTypeCd(bilUnidentifiedCash.getBilDspTypeCd());
        unidentifiedCashHistory.setBilEntrySeqNbr(bilUnidentifiedCash.getBilEntrySeqNbr());
        unidentifiedCashHistory.setBilManualSusInd(bilUnidentifiedCash.getBilManualSusInd());
        unidentifiedCashHistory.setBilPayeeAdrSeq(bilUnidentifiedCash.getBilPayeeAdrSeq());
        unidentifiedCashHistory.setBilPayeeCltId(bilUnidentifiedCash.getBilPayeeCltId());
        unidentifiedCashHistory.setBilPblItemCd(bilUnidentifiedCash.getBilPblItemCd());
        unidentifiedCashHistory.setBilRctAmt(bilUnidentifiedCash.getBilRctAmt());
        unidentifiedCashHistory.setBilRctCmt(bilUnidentifiedCash.getBilRctCmt());
        unidentifiedCashHistory.setBilRctId(bilUnidentifiedCash.getBilRctId());
        unidentifiedCashHistory.setBilRctTypeCd(bilUnidentifiedCash.getBilRctTypeCd());
        unidentifiedCashHistory.setBilTtyNbr(bilUnidentifiedCash.getBilTtyNbr());
        unidentifiedCashHistory.setDdsDsbDt(bilUnidentifiedCash.getDdsDsbDt());
        unidentifiedCashHistory.setDwsCkDrfNbr(bilUnidentifiedCash.getDwsCkDrfNbr());
        unidentifiedCashHistory.setDwsStatusCd(bilUnidentifiedCash.getDwsStatusCd());
        unidentifiedCashHistory.setPolNbr(bilUnidentifiedCash.getPolNbr());
        unidentifiedCashHistory.setPolSymbolCd(bilUnidentifiedCash.getPolSymbolCd());
        bilUnidCshHstRepository.save(unidentifiedCashHistory);
    }

    private void saveFinancialActivity(String folderId, BilUnIdCash bilUnidentifiedCash, String transactionObjectCode,
            String actionCode, Double amount, String businessCaseId, String reasonCode, String currencyCode,
            String appProgramId) {
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setTransactionActionCode(actionCode);
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(folderId);
        financialApiActivity.setApplicationName(ApplicationName.BCWSUNID.toString());
        financialApiActivity.setUserId(execContext.getUserSeqeunceId());
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setApplicationProductIdentifier(DisbursementIdType.BCMS.getDisbursementIdType());
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setOperatorId(execContext.getUserSeqeunceId());
        financialApiActivity.setAppProgramId(appProgramId);
        financialApiActivity.setReferenceDate(
                DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setBilAccountId(bilUnidentifiedCash.getBilAccountNbr());
        financialApiActivity.setBilReceiptTypeCode(bilUnidentifiedCash.getBilRctTypeCd());
        financialApiActivity.setBilBankCode(bilUnidentifiedCash.getBilBankCd());
        financialApiActivity.setBilSourceCode(String.valueOf(bilUnidentifiedCash.getBilCshEtrMthCd()));
        financialApiActivity.setBilReasonCode(reasonCode);
        financialApiActivity
                .setBilDatabaseKey(generateDatabaseKey(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(), bilUnidentifiedCash.getBilEntrySeqNbr()));
        financialApiActivity.setSummaryEffectiveDate(DateRoutine.defaultDateTime());
        financialApiActivity.setPolicyId(bilUnidentifiedCash.getPolNbr());
        financialApiActivity.setUserId(bilUnidentifiedCash.getActivityUserId());
        financialApiActivity.setBusinessCaseId(businessCaseId);
        financialApiActivity.setDisbursementId(bilUnidentifiedCash.getBilDsbId());
        financialApiActivity.setCurrencyCode(currencyCode);
        if (!bilUnidentifiedCash.getBilAgtActNbr().trim().equals(BLANK_STRING)) {
            financialApiActivity.setAgentTtyId(bilUnidentifiedCash.getBilAgtActNbr());
        }
        if (!bilUnidentifiedCash.getBilTtyNbr().trim().equals(BLANK_STRING)) {
            financialApiActivity.setAgentTtyId(bilUnidentifiedCash.getBilTtyNbr());
        }
        String[] fwsReturnMessage = null;
        fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                execContext.getApplicationDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
    }

    private String generateBusinessCaseId() {
        String businessCaseId = IdGenerator.generateUniqueIdUpperCase(GENERATED_ID_LENGTH_10);
        StringBuilder sb = new StringBuilder(20);
        sb.append(businessCaseId);
        sb.append(SEPARATOR_BLANK);
        sb.append(SEPARATOR_BLANK);
        sb.append(FinancialIntegratorConstants.ApplicationName.BCMSDISB.toString());
        return sb.toString();
    }

    private String generateFolderId(ZonedDateTime distributionDate, Short sequenceNumber, String entryNumber,
            ZonedDateTime entryDate, String userId) {
        StringBuilder technicalObjectKey = new StringBuilder(36);
        technicalObjectKey.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(distributionDate), 10));
        technicalObjectKey.append(StringUtils.leftPad(sequenceNumber.toString(), 4, CHAR_ZERO));
        technicalObjectKey.append(BLANK_CHAR);
        technicalObjectKey.append(StringUtils.rightPad(entryNumber, 4));
        technicalObjectKey.append(StringUtils.rightPad(userId, 8));
        technicalObjectKey.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        return technicalObjectKey.toString();
    }

    private String generateDatabaseKey(ZonedDateTime entryDate, String entryNumber, String userId,
            Short entrySequenceNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        sb.append(StringUtils.rightPad(entryNumber, 4));
        sb.append(StringUtils.rightPad(userId, 8));
        sb.append(PLUS_SIGN);
        sb.append(StringUtils.leftPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
        return sb.toString();
    }
}
