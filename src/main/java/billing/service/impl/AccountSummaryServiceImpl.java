package billing.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilActSummary;
import billing.data.entity.id.BilActSummaryId;
import billing.data.repository.BilActSummaryRepository;
import billing.model.AccountHistory;
import billing.service.AccountSummaryService;
import core.api.ExecContext;

@Service
public class AccountSummaryServiceImpl implements AccountSummaryService {

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private ExecContext execContext;

    @Override
    public void writeAccountSummary(String accountId, List<AccountHistory> accountHistoryList) {

        Short seqInteger = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId,
                execContext.getApplicationDate());
        if (seqInteger == null) {
            seqInteger = 0;
        } else {
            seqInteger = (short) (seqInteger + 1);
        }
        for (AccountHistory accountHistory : accountHistoryList) {
            BilActSummary bilActSummary = new BilActSummary();
            BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId, execContext.getApplicationDate(),
                    seqInteger);
            bilActSummary.setBillActSummaryId(bilActSummaryId);
            bilActSummary.setPolSymbolCd(accountHistory.getPolicySymbolCd());
            bilActSummary.setPolNbr(accountHistory.getPolicyNumber());
            bilActSummary.setBilAcyDesCd(accountHistory.getActivityCd());
            bilActSummary.setBilDesReaTyp(accountHistory.getActivityType());
            bilActSummary.setBilAcyDes1Dt(accountHistory.getActivityDesc1Dt());
            bilActSummary.setBilAcyDes2Dt(accountHistory.getActivityDesc2Dt());
            bilActSummary.setBilAcyAmt(accountHistory.getAmount());
            bilActSummary.setUserId(accountHistory.getUserName());
            bilActSummary.setBilAcyTs(execContext.getApplicationDateTime());
            bilActSummary.setBasAddDataTxt(accountHistory.getAdditionalDataTxt());
            bilActSummaryRepository.save(bilActSummary);
            seqInteger++;
        }

    }

}
