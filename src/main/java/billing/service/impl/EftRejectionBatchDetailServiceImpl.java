package billing.service.impl;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilAgent;
import billing.data.entity.BilEftRejBch;
import billing.data.entity.BilEftRejRea;
import billing.data.entity.BilThirdParty;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilAgentRepository;
import billing.data.repository.BilEftRejBchRepository;
import billing.data.repository.BilEftRejReaRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.model.EftRejectionDetail;
import billing.mongo.data.model.ReportHeader;
import billing.service.EftRejectionBatchDetailService;
import billing.utils.XLSReportRenderer;
import core.api.ExecContext;
import core.security.service.SecurityService;
import core.utils.DateRoutine;

@Service
public class EftRejectionBatchDetailServiceImpl implements EftRejectionBatchDetailService {

    @Autowired
    private BilEftRejBchRepository bilEftRejBchRepository;

    @Autowired
    private BilEftRejReaRepository bilEftRejReaRepository;

    @Autowired
    private XLSReportRenderer xlsReportRenderer;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilAgentRepository bilAgentRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Override
    public XSSFWorkbook buildExcelDocument(String batchId, String postedDate, String reportId) {
        String userId = securityService.getUserNameByUserSeqId(execContext.getUserSeqeunceId());
        List<EftRejectionDetail> eftRejectionDetailList = getEftRejectionDetailList(batchId, postedDate, userId);

        ZonedDateTime entryDate = DateRoutine.dateTimeAsYYYYMMDD(postedDate);

        List<ReportHeader> reportHeaderList = setReportHeaders(batchId, entryDate, userId);
        XSSFWorkbook workbook = xlsReportRenderer.createNewSheet(reportId, reportHeaderList);
        XSSFSheet sheet = workbook.getSheetAt(0);
        workbook = xlsReportRenderer.insertLogo(sheet, workbook);
        int count = sheet.getLastRowNum() + 1;
        if (!eftRejectionDetailList.isEmpty()) {

            XSSFCellStyle dataStyle = xlsReportRenderer.createDataStyle(workbook);
            for (EftRejectionDetail eftRejectionReport : eftRejectionDetailList) {
                int columnCount = 1;
                XSSFRow excelRow = xlsReportRenderer.createDataRow(sheet, count);

                excelRow = xlsReportRenderer.createStringCell(excelRow, columnCount++, dataStyle,
                        DateRoutine.dateTimeAsMMDDYYYYAsString(eftRejectionReport.getSettlementDate()));
                excelRow = xlsReportRenderer.createStringCell(excelRow, columnCount++, dataStyle,
                        eftRejectionReport.getTraceNumber());
                excelRow = xlsReportRenderer.createStringCell(excelRow, columnCount++, dataStyle,
                        eftRejectionReport.getAccountNumber());
                excelRow = xlsReportRenderer.createNumericCell(excelRow, columnCount++, dataStyle,
                        eftRejectionReport.getSettlementAmount(), eftRejectionReport.getCurrency(),
                        workbook.createDataFormat());
                xlsReportRenderer.createStringCell(excelRow, columnCount, dataStyle,
                        eftRejectionReport.getReturnReasonCode());
                count = sheet.getLastRowNum() + 1;
            }
        } else {
            workbook = xlsReportRenderer.createNoDataRow(workbook,
                    sheet.getRow(sheet.getLastRowNum()).getLastCellNum());
        }
        return workbook;
    }

    private List<EftRejectionDetail> getEftRejectionDetailList(String batchId, String postedDate, String userId) {

        List<EftRejectionDetail> eftRejectionDetailList = new ArrayList<>();

        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(postedDate);

        List<BilEftRejBch> bilEftRejBchList = bilEftRejBchRepository.getBilEftDataRows(batchId, bilEntryDt,
                execContext.getUserSeqeunceId());
        if (bilEftRejBchList.isEmpty()) {
            return eftRejectionDetailList;
        }

        for (BilEftRejBch bilEftRejBch : bilEftRejBchList) {
            BilEftRejRea bilEftRejRea = bilEftRejReaRepository.findEftRejCdDescription(bilEftRejBch.getBilEftRejCd());
            eftRejectionDetailList
                    .add(mapBilEftRejBchToEftRejectionDetail(bilEftRejBch, userId, bilEftRejRea.getBrrRejTxt()));
        }

        return eftRejectionDetailList;
    }

    private EftRejectionDetail mapBilEftRejBchToEftRejectionDetail(BilEftRejBch bilEftRejBch, String userName,
            String returnReason) {
        EftRejectionDetail eftRejectionDetail = new EftRejectionDetail();
        eftRejectionDetail.setPostedDate(bilEftRejBch.getBilEftRejBchId().getBrbEntryDt());
        eftRejectionDetail.setUserId(userName);
        eftRejectionDetail.setBatchId(bilEftRejBch.getBilEftRejBchId().getBrbBchNbr());
        eftRejectionDetail.setAccountNumber(bilEftRejBch.getBilAccountNumber());
        eftRejectionDetail.setTraceNumber(bilEftRejBch.getBilEftTraceNbr());
        eftRejectionDetail.setSettlementAmount(bilEftRejBch.getBilEftDrAmt());
        eftRejectionDetail.setSettlementDate(bilEftRejBch.getBilEftDrDt());
        eftRejectionDetail.setReturnReasonCode(returnReason);
        eftRejectionDetail.setReviewCode(bilEftRejBch.getBrbReviewCd());
        eftRejectionDetail.setCurrency(getCurrencyByAccountNumber(bilEftRejBch.getBilAccountNumber()));
        return eftRejectionDetail;
    }

    private String getCurrencyByAccountNumber(String accountNumber) {
        BilAccount bilAccount = bilAccountRepository.findByAccountNumber(accountNumber);
        if (bilAccount != null) {
            if (bilAccount.getThirdPartyIdentifier() != null
                    && !bilAccount.getThirdPartyIdentifier().trim().isEmpty()) {
                BilThirdParty bilThirdParty = bilThirdPartyRepository
                        .findByBilTtyNbr(bilAccount.getThirdPartyIdentifier());
                if (bilThirdParty != null) {
                    return multiCurrencyService.getCurrencyCode(bilThirdParty.getCurrencyCd());
                }
            } else if (bilAccount.getAgentAccountNumber() != null
                    && !bilAccount.getAgentAccountNumber().trim().isEmpty()) {
                BilAgent bilAgent = bilAgentRepository.findByBilAgtActNbr(bilAccount.getAgentAccountNumber());
                if (bilAgent != null) {
                    return multiCurrencyService.getCurrencyCode(bilAgent.getCurrencyCd());
                }
            } else {
                return multiCurrencyService.getCurrencyCode(bilAccount.getCurrencyCode());
            }
        }
        return null;
    }

    private List<ReportHeader> setReportHeaders(String batchId, ZonedDateTime entryDate, String userId) {
        List<ReportHeader> reportPropertyList = new ArrayList<>();

        ReportHeader reportHeader = new ReportHeader("batchId", batchId);
        reportPropertyList.add(reportHeader);

        reportHeader = new ReportHeader();
        reportHeader.setName("entryDate");
        reportHeader.setValue(DateRoutine.dateTimeAsMMDDYYYYAsString(entryDate));
        reportPropertyList.add(reportHeader);

        reportHeader = new ReportHeader("userId", userId);
        reportPropertyList.add(reportHeader);

        return reportPropertyList;

    }

}
