package billing.service.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_I;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilAccount;
import billing.data.entity.BilObjectCfg;
import billing.data.entity.BilSystemCfg;
import billing.data.entity.BilThirdParty;
import billing.data.entity.HalBoMduXrf;
import billing.data.entity.HalEventDtl;
import billing.data.entity.HalUniversalCtl2;
import billing.data.entity.HalUniversalCtl3;
import billing.data.entity.id.BilObjectCfgId;
import billing.data.entity.id.HalEventDtlId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilObjectCfgRepository;
import billing.data.repository.BilSystemCfgRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.data.repository.HalBoMduXrfRepository;
import billing.data.repository.HalEventDtlRepository;
import billing.data.repository.HalUniversalCtl2Repository;
import billing.data.repository.HalUniversalCtl3Repository;
import billing.model.Event;
import billing.service.EventPostingService;
import billing.utils.BillingConstants;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.schedule.service.ScheduleService;
import core.utils.CommonConstants;
import core.utils.DateRoutine;

@Service
public class EventPostingServiceImpl implements EventPostingService {

    @Autowired
    private DateService dateService;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    private HalEventDtlRepository halEventDtlRepository;

    @Autowired
    private BilSystemCfgRepository bilSystemCfgRepository;

    @Autowired
    private BilObjectCfgRepository bilObjectCfgRepository;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private HalUniversalCtl2Repository halUniversalCtl2Repository;

    @Autowired
    private HalBoMduXrfRepository halBoMduXrfRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private HalUniversalCtl3Repository halUniversalCtl3Repository;

    private static final String ROW_00_COUNT = "00";
    private static final String EVENT_OBJECT = "BIL-INT-SEND-EVENT-";

    public static final Logger logger = LogManager.getLogger(EventPostingServiceImpl.class);

    public HalEventDtl buildEventData(Event event) {
        checkUniversalControlConfiguration(event);
        processFields(event);
        return mapHalEventDtlRow(event);
    }

    @Override
    @Transactional
    public HalEventDtl postHalEventDtl(Event event) {
        logger.info("Insert into HAL_EVENT_DTL.");
        logger.debug("postHalEventDtl : START");
        HalEventDtl halEventDtlRow = buildEventData(event);
        halEventDtlRepository.save(halEventDtlRow);
        logger.debug("postHalEventDtl : END");
        return halEventDtlRow;
    }

    @Override
    @Transactional
    public void postDfrAcyQue(String halEventKeyId, String halEvtKeyTypCd, ZonedDateTime halEvtProcessTs) {
        logger.info("Schedule to Defer Queue.");
        logger.debug("postDfrAcyQue : START");
        checkPendingEvent(halEventKeyId, halEvtKeyTypCd, halEvtProcessTs);
        logger.debug("postDfrAcyQue : END");
    }

    @Override
    @Transactional
    public void postEvent(Event event) {
        logger.info("Insert into HAL_EVENT_DTL and DFR_ACY_QUE");
        logger.debug("postEvent : START");
        HalEventDtl halEventDtlRow = buildEventData(event);
        halEventDtlRepository.saveAndFlush(halEventDtlRow);
        processPendingEvent(halEventDtlRow.getIssueSysCd(), halEventDtlRow.getHalEventDtlId().getHalEventKeyId(),
                halEventDtlRow.getHalEventDtlId().getHalEvtKeyTypCd(), halEventDtlRow.getHalEvtProcessTs());
        logger.debug("postEvent : END");
    }

    private Event processFields(Event event) {
        if (event.getPaymentInstallmentDueDate() == null) {
            event.setPaymentInstallmentDueDate(DateRoutine.defaultDateTime());
        }
        if (!event.getBilAccountNumber().isEmpty()) {
            event.setBilTechKeyTypeCode(BillingConstants.BilTechKeyTypeCode.BILL_ACCOUNT.getValue());
            event.setBilTechKeyId(event.getBilAccountId());
            event.setBilAccountType(BillingConstants.BusCodeTranslationType.BILLING_TYPE.getValue());
        } else if (!event.getBilThirdPartyNumber().isEmpty()) {
            event.setBilTechKeyTypeCode(BillingConstants.BilTechKeyTypeCode.THIRD_PARTY.getValue());
            checkIfAccountExist(event.getBilTechKeyTypeCode(), event.getBilThirdPartyNumber(),
                    event.getBilAdditionalId(), event.getBilAccountNumber(), event.getBilAccountId());
            event.setBilAccountType(BillingConstants.BusCodeTranslationType.GROUP_BILL_TYPE.getValue());
        }

        if (event.getEventCode().equals(BillingConstants.EventCode.EVENT_PREM_PYMT.getValue())) {
            if (event.getPaymentDepositTypeCode().equals(BillingConstants.BilDspTypeCode.WRITE_OFF.getValue())
                    || event.getContext().equals(BillingConstants.EVENT_CONTEXT) || event.getEventKeyTypeCode()
                            .equals(BillingConstants.EventKeyTypeCode.UNIDENTIFIED_CASH.getValue())) {
                event.setPaymentAdjustedInstallmentAmount(DECIMAL_ZERO);
            } else {
                Double premium = calculateAdjustedAmount(event.getBilAccountId(), event.getPolicyId(),
                        event.getPaymentInstallmentDueDate(), event.getPaymentReceiveCode(),
                        event.getPolicyEffectiveDate(), event.getIssueSystemCode(), event.getMasterCompanyNumber());
                event.setPaymentAdjustedInstallmentAmount(premium);
            }
        }

        Iterator<BilSystemCfg> bilSystemCfgIterator = bilSystemCfgRepository.findAll().iterator();
        if (bilSystemCfgIterator != null && bilSystemCfgIterator.hasNext()) {
            event.setQueueWriteIndicator(bilSystemCfgIterator.next().getBilSystemCfgId().getBscQueueWrtInd());
        }
        return event;
    }

    private Double calculateAdjustedAmount(String billAccountId, String policyId, ZonedDateTime billIstDueDt,
            String billPblItemCd, ZonedDateTime polEffectiveDt, String issueSystem, String masterCompanyNumber) {
        Double premium = bilIstScheduleRepository.findAdjustAmount(billAccountId, policyId, billIstDueDt, billPblItemCd,
                polEffectiveDt);
        if (premium != null) {
            String issMco = issueSystem.concat(masterCompanyNumber);
            String iss99 = issueSystem.concat(BillingConstants.MASTER_COMPANY_NUMBER);
            String mco = BillingConstants.MASTER_COMPANY_NUMBER.concat(masterCompanyNumber);
            final String _9999 = "9999";
            premium = premium - getToleranceAmount(issMco, iss99, mco, _9999);
        } else {
            premium = DECIMAL_ZERO;
        }
        return premium;
    }

    private Double getToleranceAmount(String issMco, String iss99, String mco, String _9999) {
        final String fieldId = "BIL-PAYMENT-TOLERANCE-AMT";
        Double toleranceAmount = DECIMAL_ZERO;
        HalUniversalCtl3 halUniversalCtl3 = halUniversalCtl3Repository.findByEntryKeyCdAndEntryDataTag(fieldId,
                Arrays.asList(issMco, iss99, mco, _9999));
        if (halUniversalCtl3 != null) {
            String entryDataText = halUniversalCtl3.getEntryDataTxt();
            DecimalFormat formatter = new DecimalFormat(".##");
            formatter.setRoundingMode(RoundingMode.DOWN);
            String formattedAmount = formatter.format(Double.parseDouble(entryDataText.substring(0, 7)));

            toleranceAmount = Double.parseDouble(formattedAmount);
        }
        return toleranceAmount;
    }

    private String buildDetailTextParam(Event event) {
        StringBuilder sb = new StringBuilder(1839);
        sb.append(fillRightByChar(event.getEventCode(), 3, BLANK_CHAR));
        // Policy Area - total length: 183
        sb.append(fillRightByChar(event.getIssueSystemCode(), 2, BLANK_CHAR));
        sb.append(fillRightByChar(event.getPolicyId(), 16, BLANK_CHAR));
        sb.append(
                fillRightByChar(DateRoutine.dateTimeAsYYYYMMDDString(event.getPolicyEffectiveDate()), 10, BLANK_CHAR));
        sb.append(fillRightByChar(DateRoutine.dateTimeAsYYYYMMDDString(event.getPlannedExpirationDate()), 10,
                BLANK_CHAR));
        sb.append(fillRightByChar(event.getPolicySymbolCode(), 3, BLANK_CHAR));
        sb.append(fillRightByChar(event.getPolicyNumber(), 25, BLANK_CHAR));
        sb.append(fillRightByChar(event.getLineOfBusinessCode(), 3, BLANK_CHAR));
        sb.append(fillRightByChar(event.getMasterCompanyNumber(), 2, BLANK_CHAR));
        sb.append(fillRightByChar(event.getStateProvinceCode(), 3, BLANK_CHAR));
        sb.append(fillRightByChar(event.getCountryCode(), 4, BLANK_CHAR));
        sb.append(fillRightByChar(event.getPolicyStatusCode(), 1, BLANK_CHAR));
        sb.append(fillRightByChar(event.getBilPlanCode(), 4, BLANK_CHAR));
        sb.append(fillRightByChar(event.getFiller1(), 100, BLANK_CHAR));
        // Bill Account Area - total length: 222
        sb.append(fillRightByChar(event.getBilTechKeyId(), 8, BLANK_CHAR));
        sb.append(event.getBilTechKeyTypeCode());
        sb.append(fillRightByChar(event.getBilAccountId(), 8, BLANK_CHAR));
        sb.append(fillRightByChar(event.getBilAccountNumber(), 30, BLANK_CHAR));
        sb.append(fillRightByChar(event.getBilTypeCode(), 2, BLANK_CHAR));
        sb.append(fillRightByChar(event.getBilClassCode(), 3, BLANK_CHAR));
        sb.append(fillRightByChar(event.getBilAccountType(), 3, BLANK_CHAR));
        sb.append(fillRightByChar(event.getCollectionMethod(), 3, BLANK_CHAR));
        sb.append(fillRightByChar(event.getCollectionPlan(), 4, BLANK_CHAR));
        sb.append(fillRightByChar(event.getBilThirdPartyNumber(), 20, BLANK_CHAR));
        // Agent Number
        sb.append(fillRightByChar(BLANK_STRING, 20, BLANK_CHAR));
        sb.append(fillRightByChar(event.getBilAdditionalId(), 20, BLANK_CHAR));
        sb.append(fillRightByChar(event.getFiller2(), 100, BLANK_CHAR));

        // Additional Data - total length: 163
        sb.append(fillRightByChar(event.getUserSequenceId(), 8, BLANK_CHAR));
        sb.append(fillRightByChar(DateRoutine.dateTimeAsYYYYMMDDHHmmssSSSString(dateService.currentDateTime()),
                26, CommonConstants.CHAR_ZERO));
        sb.append(fillRightByChar(event.getRequestProgramName(), 8, BLANK_CHAR));
        sb.append(event.getQueueWriteIndicator());
        sb.append(fillRightByChar(event.getContext(), 20, BLANK_CHAR));
        sb.append(fillRightByChar(event.getFiller3(), 100, BLANK_CHAR));

        sb.append(fillRightByChar(event.getCustomerInput(), 200, BLANK_CHAR));
        sb.append(fillRightByChar(event.getCustomerOutput(), 200, BLANK_CHAR));
        sb.append(event.getErrorFlag());

        // Abend Text - total length: 367
        sb.append(fillRightByChar(event.getProgramName(), 8, BLANK_CHAR));
        sb.append(fillRightByChar(event.getParamName(), 30, BLANK_CHAR));
        sb.append(BLANK_CHAR);
        sb.append(fillLeftByChar(String.valueOf(Math.abs(event.getSqlCd())), 8, CommonConstants.CHAR_ZERO));
        sb.append(fillRightByChar(event.getErrorDescription(), 70, BLANK_CHAR));
        sb.append(fillRightByChar(event.getErrorKeyId(), 250, BLANK_CHAR));
        if (event.getEventCode().equals(BillingConstants.EventCode.EVENT_PREM_PYMT.getValue())) {
            sb.append(fillRightByChar(DateRoutine.dateTimeAsYYYYMMDDString(event.getPaymentInstallmentDueDate()), 10,
                    BLANK_CHAR));
            sb.append(fillRightByChar(event.getPaymentDepositTypeCode(), 2, BLANK_CHAR));
            sb.append(fillRightByChar(event.getPaymentReceiveCode(), 3, BLANK_CHAR));
            sb.append(checkNumberSign(event.getPaymentDepositAmount()));
            sb.append(fillLeftByChar(String.valueOf(Math.abs(event.getPaymentDepositAmount())), 11,
                    CommonConstants.CHAR_ZERO));
            sb.append(
                    fillRightByChar(DateRoutine.dateTimeAsYYYYMMDDString(event.getPaymentEntryDate()), 10, BLANK_CHAR));
            sb.append(
                    fillRightByChar(DateRoutine.dateTimeAsYYYYMMDDString(event.getPaymentLoanDate()), 10, BLANK_CHAR));
            sb.append(checkNumberSign(event.getPaymentAdjustedInstallmentAmount()));
            sb.append(fillLeftByChar(String.valueOf(Math.abs(event.getPaymentAdjustedInstallmentAmount())), 11,
                    CommonConstants.CHAR_ZERO));
            sb.append(fillRightByChar(event.getReceivableCode(), 3, BLANK_CHAR));
            sb.append(fillRightByChar(event.getReceiptTypeCode(), 2, BLANK_CHAR));
            sb.append(fillRightByChar(DateRoutine.dateTimeAsYYYYMMDDString(event.getBilDepositDate()), 10, BLANK_CHAR));
            sb.append(fillRightByChar(event.getPaymentFilter(), 426, BLANK_CHAR));
        }
        if (event.getEventCode().equals(BillingConstants.EventCode.EVENT_BILL_CHG.getValue())) {
            sb.append(fillRightByChar(event.getChgFiller(), 500, BLANK_CHAR));
        }
        if (event.getEventCode().equals(BillingConstants.EventCode.EVENT_RECN_REA.getValue())) {
            sb.append(fillRightByChar(event.getRcnThirdPartyNumber(), 20, BLANK_CHAR));
            sb.append(fillRightByChar(event.getRcnAdditionalId(), 20, BLANK_CHAR));
            sb.append(
                    fillRightByChar(DateRoutine.dateTimeAsYYYYMMDDString(event.getRcnInvEntryDate()), 10, BLANK_CHAR));
            sb.append(fillRightByChar(String.valueOf(event.getRcnInvSequenceNumber()), 4, BLANK_CHAR));
            sb.append(fillRightByChar(event.getRcnPayableItem(), 3, BLANK_CHAR));
            sb.append(fillRightByChar(event.getRcnAction(), 3, BLANK_CHAR));
            sb.append(event.getRcnReason());
            sb.append(
                    fillRightByChar(DateRoutine.dateTimeAsYYYYMMDDString(event.getRcnEffectiveDate()), 10, BLANK_CHAR));
            sb.append(fillRightByChar(event.getRcnComment(), 130, BLANK_CHAR));
            sb.append(checkNumberSign(event.getRcnBillAmount()));
            sb.append(
                    fillLeftByChar(String.valueOf(Math.abs(event.getRcnBillAmount())), 11, CommonConstants.CHAR_ZERO));
            sb.append(checkNumberSign(event.getRcnPaidAmount()));
            sb.append(
                    fillLeftByChar(String.valueOf(Math.abs(event.getRcnPaidAmount())), 11, CommonConstants.CHAR_ZERO));
            sb.append(fillRightByChar(event.getRcnFiller(), 275, BLANK_CHAR));
        }
        return sb.toString();
    }

    private void checkIfAccountExist(char techKeyTypeCode, String thirdPartyNumber, String additionalId,
            String accountNumber, String accountId) {

        if (techKeyTypeCode == BillingConstants.BilTechKeyTypeCode.THIRD_PARTY.getValue()) {
            BilThirdParty bilThirdParty = bilThirdPartyRepository.findByBilTtyNbr(thirdPartyNumber);
            if (bilThirdParty == null) {
                logger.error("third party not found. {}", thirdPartyNumber);
                throw new DataNotFoundException("no.bil.account.exists", new Object[] { thirdPartyNumber });
            }
        }
        if (techKeyTypeCode == BillingConstants.BilTechKeyTypeCode.THIRD_PARTY.getValue()
                && (additionalId.isEmpty() || accountNumber.isEmpty())) {
            BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
            if (bilAccount == null) {
                logger.error("account not found. {}", accountId);
                throw new DataNotFoundException("no.bil.account.exists", new Object[] { accountId });
            }
        }

    }

    private HalEventDtl mapHalEventDtlRow(Event event) {
        logger.info("Map data from Event to HalEventDtl");
        logger.debug("mapHalEventDtlRow : START");
        HalEventDtlId halEventDtlId = new HalEventDtlId();
        halEventDtlId.setBusObjNm(event.getBussinessObjectName());
        halEventDtlId.setHalEventKeyId(event.getEventKeyId());
        halEventDtlId.setHalEvtKeyTypCd(event.getEventKeyTypeCode());
        halEventDtlId.setHalEventTs(dateService.currentDateTime());
        HalEventDtl halEventDtl = new HalEventDtl(halEventDtlId, event.getEventStatusCode(),
                dateService.currentDateTime(), event.getEffectiveDate(), event.getUserSequenceId(),
                event.getEventMduNm(), event.getIssueSystemCode(), DateRoutine.defaultDateTime(),
                buildDetailTextParam(event));
        logger.debug("mapHalEventDtlRow : END");
        return halEventDtl;
    }

    private void checkUniversalControlConfiguration(Event event) {
        logger.debug("checkXmlPosting : START");
        ZonedDateTime maxEffectiveDate = halUniversalCtl2Repository.findMaxEffectiveDate(event.getBussinessObjectName(),
                BLANK_STRING, event.getEffectiveDate());
        if (maxEffectiveDate != null) {
            HalUniversalCtl2 halUniversalCtl2 = halUniversalCtl2Repository.findByEntryKeyAndLabelTextAndEffectiveDate(
                    event.getBussinessObjectName(), BLANK_STRING, maxEffectiveDate);
            if (halUniversalCtl2 != null) {
                event.setEventStatusCode(halUniversalCtl2.getEntryData().charAt(0));
            } else {
                logger.error("No configuration found in HAL_UNIVERSAL_CTL2 for {}", event.getBussinessObjectName());
                return;
            }
        } else {
            logger.error("Max effective date not found in HAL_UNIVERSAL_CTL2 for {}", event.getBussinessObjectName());
            return;
        }
        logger.debug("checkXmlPosting : END");
    }

    private void checkPendingEvent(String halEventKeyId, String halEvtKeyTypCd, ZonedDateTime halEvtProcessTs) {
        logger.info("Check pending event");
        logger.debug("checkPendingEvent : START");
        List<String> issuseSystemCdList = halEventDtlRepository.getIssueSystemList(halEventKeyId, halEvtKeyTypCd,
                CHAR_P);
        if (issuseSystemCdList != null && !issuseSystemCdList.isEmpty()) {
            for (String issueSysCd : issuseSystemCdList) {
                processPendingEvent(issueSysCd, halEventKeyId, halEvtKeyTypCd, halEvtProcessTs);
            }
        }
        logger.debug("checkPendingEvent : END");
    }

    private void processPendingEvent(String issueSysCd, String halEventKeyId, String halEvtKeyTypCd,
            ZonedDateTime halEvtProcessTs) {
        logger.info("process pending event");
        logger.debug("processPendingEvent : START");
        String interfaceObjName = EVENT_OBJECT.concat(issueSysCd).trim();
        String programName;
        HalBoMduXrf halBoMduXrf = halBoMduXrfRepository.findById(interfaceObjName).orElse(null);
        if (halBoMduXrf != null) {
            programName = halBoMduXrf.getHbmxBobjMduNm();
        } else {
            logger.error("No configuration found in HAL_BO_MDU_XRF for {}", interfaceObjName);
            return;
        }

        if (programName.trim().isEmpty()) {
            logger.error("No configuration found in HAL_BO_MDU_XRF for {}", interfaceObjName);
            return;
        }

        BilObjectCfg bilObjectCfg = bilObjectCfgRepository.findById(new BilObjectCfgId(programName, CHAR_P))
                .orElse(null);
        if (bilObjectCfg != null) {
            if (bilObjectCfg.getBocObjPrcCd() == CHAR_Y) {
                scheduleDeferRow(halEventKeyId, halEvtKeyTypCd, issueSysCd, halEvtProcessTs, programName.trim());
                updateHalEventDtlRow(halEventKeyId, halEvtKeyTypCd, issueSysCd);
            }
        } else {
            logger.error("No configuration found in BIL_OBJECT_CFG for {}", programName);
            return;
        }
        logger.debug("processPendingEvent : END");
    }

    private void updateHalEventDtlRow(String halEventKeyId, String halEvtKeyTypCd, String issueSysCd) {
        logger.info("Update HAL_EVENT_DTL");
        logger.debug("updateHalEventDtlRow : START");
        List<HalEventDtl> halEventDtlRows = halEventDtlRepository.getEventDetailRows(halEventKeyId, halEvtKeyTypCd,
                issueSysCd, CHAR_P);

        if (halEventDtlRows != null && !halEventDtlRows.isEmpty()) {
            for (HalEventDtl halEventDtl : halEventDtlRows) {
                halEventDtl.setHalEvtStatusCd(CHAR_I);
                halEventDtlRepository.save(halEventDtl);
            }
        }
        logger.debug("updateHalEventDtlRow : END");
    }

    private void scheduleDeferRow(String halEventKeyId, String halEvtKeyTypCd, String issueSysCd,
            ZonedDateTime halEvtProcessTs, String objectId) {
        logger.info("Call schedule service to Defer Queue");
        logger.debug("scheduleDeferRow : START");
        String programName = BLANK_STRING;
        String paraName = BLANK_STRING;
        String errKeyId = BLANK_STRING;
        String returnCode = BLANK_STRING;
        String sqlCode = BLANK_STRING;
        String errorDescription = BLANK_STRING;
        String acyParmListDta = getParameterListData(halEventKeyId, halEvtKeyTypCd, issueSysCd, halEvtProcessTs, false,
                programName, paraName, returnCode, sqlCode, errorDescription, errKeyId);
        scheduleService.scheduleDeferActivity(objectId, acyParmListDta, halEventKeyId);
        logger.debug("scheduleDeferRow : END");
    }

    private String getParameterListData(String halEventKeyId, String halEvtKeyTypCd, String issueSysCd,
            ZonedDateTime halEvtProcessTs, boolean isError, String programName, String paraName, String returnCode,
            String sqlCode, String errorDescription, String errKeyId) {
        StringBuilder sb = new StringBuilder(463);
        sb.append(ROW_00_COUNT);
        sb.append(fillRightByChar(ROW_00_COUNT, 2, BLANK_CHAR));
        sb.append(fillRightByChar(halEventKeyId, 50, BLANK_CHAR));
        sb.append(fillRightByChar(halEvtKeyTypCd, 2, BLANK_CHAR));
        sb.append(fillRightByChar(issueSysCd, 2, BLANK_CHAR));
        sb.append(fillRightByChar(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()), 10,
                CommonConstants.CHAR_ZERO));
        sb.append(fillRightByChar(DateRoutine.dateTimeAsYYYYMMDDHHmmssSSSString(halEvtProcessTs), 26,
                CommonConstants.CHAR_ZERO));
        sb.append(isError ? CHAR_Y : CHAR_N);
        sb.append(fillRightByChar(programName, 8, BLANK_CHAR));
        sb.append(fillRightByChar(paraName, 30, BLANK_CHAR));
        sb.append(fillRightByChar(returnCode, 2, BLANK_CHAR));
        sb.append(fillRightByChar(sqlCode, 8, CommonConstants.CHAR_ZERO));
        sb.append(fillRightByChar(errorDescription, 70, BLANK_CHAR));
        sb.append(fillRightByChar(errKeyId, 250, BLANK_CHAR));
        return sb.toString();
    }

    private String fillRightByChar(String parameter, int length, char padChar) {
        return StringUtils.rightPad(parameter, length, padChar);
    }

    private String fillLeftByChar(String parameter, int length, char padChar) {
        return StringUtils.leftPad(parameter.contains(".") ? parameter.replace(".", BLANK_STRING) : parameter, length,
                padChar);
    }

    public String checkNumberSign(Double amount) {
        if (amount == 0) {
            return CommonConstants.SEPARATOR_BLANK;
        }
        return amount < 0 ? BillingConstants.MINUS_SIGN : BillingConstants.PLUS_SIGN;
    }

    public String checkNumberSign(Integer amount) {
        if (amount == 0) {
            return BLANK_STRING;
        }
        return amount < 0 ? BillingConstants.MINUS_SIGN : BillingConstants.PLUS_SIGN;
    }

}
