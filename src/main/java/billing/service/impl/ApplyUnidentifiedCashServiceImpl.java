package billing.service.impl;

import static billing.utils.BillingConstants.COMPANY_LOCATION_NUMBER;
import static billing.utils.BillingConstants.ERROR_CODE;
import static billing.utils.BillingConstants.MASTER_COMPANY_NUMBER;
import static billing.utils.BillingConstants.PLUS_SIGN;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import application.utils.model.embedded.AccountSearch;
import application.utils.service.impl.AccountsElasticSearchServiceImpl;
import billing.data.entity.BilAccount;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilBankEntry;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilEftActivity;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilEftBankEd;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilBankEntryId;
import billing.data.entity.id.BilCashDspId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.entity.id.BilEftActivityId;
import billing.data.entity.id.BilEftBankId;
import billing.data.entity.id.BilEftPendingTapeId;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilBankEntryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilEftActivityRepository;
import billing.data.repository.BilEftBankEdRepository;
import billing.data.repository.BilEftBankRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.model.CashApplyDriver;
import billing.service.ApplyUnidentifiedCashService;
import billing.service.CashApplyDriverService;
import billing.service.PaymentService;
import billing.utils.BillingConstants.AccountStatus;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CashEntryMethod;
import billing.utils.BillingConstants.EftRecordType;
import core.api.ExecContext;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.data.entity.id.BusCdTranslationId;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class ApplyUnidentifiedCashServiceImpl implements ApplyUnidentifiedCashService {

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilEftActivityRepository bilEftActivityRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private CashApplyDriverService cashApplyDriverService;

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BilBankEntryRepository bilBankEntryRepository;

    @Autowired
    private BilEftBankEdRepository bilEftBankEdRepository;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private AccountsElasticSearchServiceImpl accountsElasticSearchServiceImpl;
    private static final String PENDING = "Pending";
    private static final String CREDITTEXT = "1XCCE";
    private static final String BANKNAME = "PACS BANK";
    private static final List<Character> ECC_RECORDTYPE = Arrays.asList(EftRecordType.RECONCILED_CASH.getValue(),
            EftRecordType.UNIDENTIFIED.getValue(), EftRecordType.DIGITAL_WALLET.getValue());
    private static final Logger logger = LoggerFactory.getLogger(ApplyUnidentifiedCashServiceImpl.class);

    @Override
    @Transactional
    public void applyUnidentifiedCash(BilAccount bilAccount, String policyNumber, String identifierType,
            Boolean isBusGrpOverride) {
        List<BilUnIdCash> unidentifiedCashList;
        if (identifierType.equals(AccountType.BILLACCOUNT.toString())) {
            unidentifiedCashList = bilUnIdCashRepository.findUnidentifiedPayment(SEPARATOR_BLANK,
                    bilAccount.getAccountNumber(), SEPARATOR_BLANK, policyNumber, BilDspTypeCode.SUSPENDED.getValue());
        } else {
            unidentifiedCashList = bilUnIdCashRepository.findUnidentifiedPayment(SEPARATOR_BLANK, SEPARATOR_BLANK,
                    SEPARATOR_BLANK, policyNumber, BilDspTypeCode.SUSPENDED.getValue());
        }
        if (unidentifiedCashList != null && !unidentifiedCashList.isEmpty()) {
            for (BilUnIdCash unidentifiedCash : unidentifiedCashList) {
                ZonedDateTime pendingTapedate = DateRoutine.defaultDateTime();
                BilCashReceipt bilCashReceipt = saveReceipt(bilAccount.getAccountId(), execContext.getApplicationDate(),
                        unidentifiedCash);
                saveDisposition(bilAccount.getAccountId(), execContext.getApplicationDate(), unidentifiedCash,
                        bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr());

                if (unidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.AGENT_ACH.getCashEntryMethod()
                        || unidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.INSURED.getCashEntryMethod()
                        || unidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.ONE_TIME_ACH.getCashEntryMethod()) {
                    addBankInformation(bilAccount.getAccountId(), unidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                            unidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                            unidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                            unidentifiedCash.getBilUnIdCashId().getUserId());
                }
                if (unidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()
                        || unidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()
                        || (isDigitalWalletPayment(unidentifiedCash.getBilCshEtrMthCd()) || unidentifiedCash
                                .getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod())
                                && checkIfEftCCPayment(unidentifiedCash.getBillUnidCashId())) {
                    if (Boolean.FALSE.equals(isBusGrpOverride)) {
                        String businessGroup = BLANK_STRING;
                        try {
                            AccountSearch accountSearch = accountsElasticSearchServiceImpl
                                    .fetchAccountDocument(bilAccount.getAccountId());
                            if (accountSearch != null && accountSearch.getAccountId() != null
                                    && !accountSearch.getAccountId().isEmpty()
                                    && accountSearch.getBusinessGroup() != null
                                    && !accountSearch.getBusinessGroup().trim().isEmpty()) {
                                businessGroup = accountSearch.getBusinessGroup();
                                if (unidentifiedCash.getBilBusGrpCd().compareTo(businessGroup) != 0) {
                                    throw new InvalidDataException("check.bitype.busgr.not.match");

                                }
                            }
                        } catch (IOException e) {
                            logger.error("Account not Found");
                        }
                    }
                    pendingTapedate = updateEftDetails(unidentifiedCash, bilAccount.getAccountId(),
                            bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr());
                }

                saveAccountSummary(bilAccount.getAccountId(), unidentifiedCash.getBilCshEtrMthCd(), pendingTapedate,
                        unidentifiedCash.getBilPstmrkDt(), unidentifiedCash.getBilRctTypeCd(),
                        unidentifiedCash.getBilRctAmt(), unidentifiedCash.getBilUnIdCashId().getUserId(),
                        unidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        unidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), bilCashReceipt.getAgencyNumber(),
                        bilCashReceipt.getEntrySequenceNumber());

                saveFinancialApi(bilAccount.getAccountId(), execContext.getApplicationDate(), unidentifiedCash,
                        bilAccount.getAccountNumber(), bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                        ObjectCode.UNIDENTIFIED_CASH.toString(), unidentifiedCash.getBilRctAmt() * -1,
                        bilCashReceipt.getEntryDate(), bilCashReceipt.getEntryNumber(), bilCashReceipt.getUserId(),
                        bilCashReceipt.getEntrySequenceNumber(), bilAccount.getCurrencyCode());

                saveFinancialApi(bilAccount.getAccountId(), execContext.getApplicationDate(), unidentifiedCash,
                        bilAccount.getAccountNumber(), bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                        ObjectCode.CASH.toString(), unidentifiedCash.getBilRctAmt(), bilCashReceipt.getEntryDate(),
                        bilCashReceipt.getEntryNumber(), bilCashReceipt.getUserId(),
                        bilCashReceipt.getEntrySequenceNumber(), bilAccount.getCurrencyCode());

                bilUnIdCashRepository.delete(unidentifiedCash);

                cashApply(bilAccount.getAccountId());

                paymentService.insertAccountBalance(bilAccount.getAccountId(), bilAccount.getAccountNumber(),
                        bilAccount.getBillTypeCd(), bilAccount.getStatus(), execContext.getApplicationDate());
            }
        }
    }

    private void addBankInformation(String accountId, short sequenceNumber, ZonedDateTime entryDate, String entryNumber,
            String userId) {
        List<BilEftBank> bilEftBank = bilEftBankRepository.findBilEftBankEntry(accountId, CHAR_P,
                execContext.getApplicationDate(), AccountStatus.ACTIVE.toChar(), PageRequest.of(SHORT_ZERO, SHORT_ONE));
        if (!bilEftBank.isEmpty() && bilEftBank.get(0) != null) {
            for (BilEftBank billEftBank : bilEftBank) {
                billEftBank.setBebAccountStaCd(AccountStatus.OLD.toChar());
            }
            bilEftBankRepository.saveAll(bilEftBank);
        }
        BilBankEntry bilBankEntry = bilBankEntryRepository
                .findById(new BilBankEntryId(entryDate, entryNumber, userId, sequenceNumber)).orElse(null);
        if (bilBankEntry != null && !bilBankEntry.getRoutingTransitNbr().trim().matches("XXX(.*)")) {
            if (bilBankEntry.getWebAuthInd() == BLANK_CHAR) {
                bilBankEntry.setWebAuthInd(CHAR_N);
            }
            BilEftBankEd bilEftBankEd = bilEftBankEdRepository
                    .findByRoutingTransitNbr(bilBankEntry.getRoutingTransitNbr());
            if (bilEftBankEd == null) {
                bilEftBankEd = saveBilEftBankEd(bilBankEntry.getRoutingTransitNbr());
            }
            saveBilEftBank(accountId, bilBankEntry.getBankAccountNbr(), bilEftBankEd.getEftBankId(),
                    bilBankEntry.getAccountTypCd(), bilBankEntry.getWebAuthInd(), bilBankEntry.getEftPayorLongName());

            bilBankEntryRepository.delete(bilBankEntry);
        }
    }

    private void saveBilEftBank(String accountId, String bankAccountNumber, Integer eftBankId, String accountTypeCode,
            char webAuthIndicator, String eftPayorLongName) {
        BilEftBank bilEftBank = new BilEftBank();
        bilEftBank.setBilEftBankId(new BilEftBankId(accountId, CHAR_P, execContext.getApplicationDate()));
        bilEftBank.setBebAccountTyc(accountTypeCode);
        bilEftBank.setBebEftPayLngNm(eftPayorLongName);
        bilEftBank.setBebAccountStaCd(AccountStatus.ACTIVE.toChar());
        bilEftBank.setBebAutInd(CHAR_N);
        bilEftBank.setBebWebAutInd(webAuthIndicator);
        bilEftBank.setBilBankAcctNbr(bankAccountNumber);
        bilEftBank.setBilEftBankIdNbr(eftBankId);
        bilEftBankRepository.save(bilEftBank);
    }

    private BilEftBankEd saveBilEftBankEd(String routingTransitNumber) {
        BilEftBankEd bilEftBankEd = new BilEftBankEd();
        BilEftBankEd eftBankEd = bilEftBankEdRepository.findTopByOrderByEftBankIdDesc();
        if (eftBankEd == null || eftBankEd.getEftBankId() == null) {
            bilEftBankEd.setEftBankId(0);
        } else {
            Short bankId = getIncrementalValue(eftBankEd.getEftBankId().shortValue());
            bilEftBankEd.setEftBankId(Integer.valueOf(bankId));
        }
        bilEftBankEd.setBankName(BANKNAME);
        bilEftBankEd.setRoutingTransitNbr(routingTransitNumber);
        bilEftBankEdRepository.save(bilEftBankEd);
        return bilEftBankEd;
    }

    private boolean checkIfEftCCPayment(String technicalKeyId) {
        Integer rowCount = bilEftActivityRepository.checkIfExists(technicalKeyId, ECC_RECORDTYPE);
        if (rowCount != null && rowCount > SHORT_ZERO) {
            return true;
        } else {
            rowCount = bilEftPendingTapeRepository.checkIfExists(technicalKeyId, ECC_RECORDTYPE);
            if (rowCount != null && rowCount > SHORT_ZERO) {
                return true;
            }
        }
        return false;
    }

    private void cashApply(String accountId) {

        CashApplyDriver cashApplyDriver = new CashApplyDriver();
        cashApplyDriver.setAccountId(accountId);
        cashApplyDriver.setCallBcmoax(false);
        cashApplyDriver.setCashApplyDate(execContext.getApplicationDate());
        cashApplyDriver.setUserSequenceId(execContext.getUserSeqeunceId());
        cashApplyDriverService.processCashApply(cashApplyDriver);
    }

    private void saveFinancialApi(String accountId, ZonedDateTime distributionDate, BilUnIdCash unidentifiedCash,
            String accountNumber, String bilTypeCode, String bilClassCode, String transactionObjectCode, Double amount,
            ZonedDateTime entryDate, String entryNumber, String userId, Short entrySeqenceNumber, String currencyCode) {

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setFolderId(accountId);
        financialApiActivity.setApplicationName(ApplicationName.BACPDUC.toString());
        financialApiActivity.setFunctionCode(CHAR_A);
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setTransactionActionCode(ActionCode.SUSPENSE.toString());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity
                .setApplicationProductIdentifier(FinancialIntegratorConstants.ApplicationProduct.BCMS.toString());
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(distributionDate));
        financialApiActivity.setOperatorId(userId);
        financialApiActivity.setAppProgramId(ApplicationName.BACPDUC.toString());
        financialApiActivity.setBilBankCode(unidentifiedCash.getBilBankCd());
        financialApiActivity.setBilAccountId(accountNumber);
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setBilReceiptTypeCode(unidentifiedCash.getBilRctTypeCd());
        financialApiActivity.setBilTypeCode(bilTypeCode);
        financialApiActivity.setBilClassCode(bilClassCode);
        financialApiActivity.setBilDatabaseKey(generateDatabaseKey(entryDate, entryNumber, userId, entrySeqenceNumber));
        financialApiActivity.setBilSourceCode(String.valueOf(unidentifiedCash.getBilCshEtrMthCd()));
        financialApiActivity.setSummaryEffectiveDate(DateRoutine.defaultDateTime());
        financialApiActivity.setOriginalTimeStamp(execContext.getApplicationDateTime());
        financialApiActivity.setBilReasonCode(unidentifiedCash.getBilDspReasonCd());
        financialApiActivity.setCurrencyCode(currencyCode);

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                execContext.getApplicationDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
    }

    private String generateDatabaseKey(ZonedDateTime entryDate, String entryNumber, String userId,
            Short entrySequenceNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        sb.append(StringUtils.rightPad(entryNumber, 4));
        sb.append(StringUtils.rightPad(userId, 8));
        sb.append(PLUS_SIGN);
        sb.append(StringUtils.rightPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
        return sb.toString();
    }

    private void saveDisposition(String accountId, ZonedDateTime distributionDate, BilUnIdCash unidentifiedCash,
            Short distributionSequenceNumber) {
        BilCashDsp bilCashDisposition = new BilCashDsp();
        bilCashDisposition.setBilCashDspId(new BilCashDspId(accountId, distributionDate, distributionSequenceNumber,
                getIncrementalValue(bilCashDspRepository.findMaxDispositionNumber(accountId, distributionDate,
                        distributionSequenceNumber))));
        bilCashDisposition.setAdjustmentDueDate(unidentifiedCash.getBilAdjDueDt());
        bilCashDisposition.setCheckProdMethodCd(unidentifiedCash.getBilChkPrdMthCd());
        bilCashDisposition.setDraftNbr(unidentifiedCash.getDwsCkDrfNbr());
        bilCashDisposition.setDspAmount(unidentifiedCash.getBilRctAmt());
        bilCashDisposition.setDspDate(unidentifiedCash.getBilDspDt());
        bilCashDisposition.setDspReasonCd(unidentifiedCash.getBilDspReasonCd());
        bilCashDisposition.setDspTypeCd(unidentifiedCash.getBilDspTypeCd());
        bilCashDisposition.setInvoiceDate(DateRoutine.defaultDateTime());
        bilCashDisposition.setManualSuspenseIndicator(unidentifiedCash.getBilManualSusInd());
        bilCashDisposition.setPayableItemCd(unidentifiedCash.getBilPblItemCd());
        bilCashDisposition.setPayeeAddressSequenceNumber(unidentifiedCash.getBilPayeeAdrSeq());
        bilCashDisposition.setPayeeClientId(unidentifiedCash.getBilPayeeCltId());
        bilCashDisposition.setPolicyEffectiveDate(DateRoutine.defaultDateTime());
        bilCashDisposition.setPolicyNumber(unidentifiedCash.getPolNbr());
        bilCashDisposition.setPolicySymbolCd(unidentifiedCash.getPolSymbolCd());
        bilCashDisposition.setSystemDueDate(DateRoutine.defaultDateTime());
        bilCashDisposition.setBilSequenceNumber(SHORT_ZERO);
        bilCashDisposition.setCreditIndicator(CHAR_N);
        bilCashDisposition.setUserId(unidentifiedCash.getBilUnIdCashId().getUserId());
        bilCashDisposition.setDisbursementDate(DateRoutine.defaultDateTime());
        bilCashDspRepository.save(bilCashDisposition);
    }

    private BilCashReceipt saveReceipt(String accountId, ZonedDateTime distributionDate, BilUnIdCash unidentifiedCash) {
        BilCashReceipt bilCashReceipt = new BilCashReceipt();
        Short sequenceNumber;

        sequenceNumber = getIncrementalValue(bilCashReceiptRepository.getMaxBilDtbSeqNbr(accountId, distributionDate));
        bilCashReceipt.setBilCashReceiptId(new BilCashReceiptId(accountId, distributionDate, sequenceNumber));
        bilCashReceipt.setAgencyNumber(unidentifiedCash.getAgencyNbr());
        bilCashReceipt.setBankCd(unidentifiedCash.getBilBankCd());
        bilCashReceipt.setCashEntryMethodCd(unidentifiedCash.getBilCshEtrMthCd());
        bilCashReceipt.setCreditCardAccounttNumber(unidentifiedCash.getBilCrcrdActNbr());
        bilCashReceipt.setCreditCardExpirationDate(unidentifiedCash.getBilCrcrdExpDt());
        bilCashReceipt.setCreditCardTypeCd(unidentifiedCash.getBilCrcrdTypeCd());
        bilCashReceipt.setCredutCardAuthroizationApproval(unidentifiedCash.getBilCrcrdAutApv());
        bilCashReceipt.setDepositeDate(unidentifiedCash.getBilDepositDt());
        bilCashReceipt.setEntryDate(unidentifiedCash.getBilUnIdCashId().getBilEntryDt());
        bilCashReceipt.setEntryNumber(unidentifiedCash.getBilUnIdCashId().getBilEntryNbr());
        bilCashReceipt.setEntrySequenceNumber(unidentifiedCash.getBilEntrySeqNbr());
        bilCashReceipt.setPostMarkCd(unidentifiedCash.getBilPstmrkCd());
        bilCashReceipt.setPostMarkDate(unidentifiedCash.getBilPstmrkDt());
        bilCashReceipt.setReceiptAmount(unidentifiedCash.getBilRctAmt());
        bilCashReceipt.setReceiptId(unidentifiedCash.getBilRctId());
        bilCashReceipt.setReceiptReceiveDate(unidentifiedCash.getBilRctReceiveDt());
        bilCashReceipt.setReceiptTypeCd(unidentifiedCash.getBilRctTypeCd());
        bilCashReceipt.setTaxYearNumber(unidentifiedCash.getBilTaxYearNbr());
        bilCashReceipt.setUserId(unidentifiedCash.getBilUnIdCashId().getUserId());
        bilCashReceipt.setReceiptComment(unidentifiedCash.getBilRctCmt());
        bilCashReceiptRepository.save(bilCashReceipt);
        return bilCashReceipt;
    }

    private ZonedDateTime updateEftDetails(BilUnIdCash unidentifiedCash, String accountId,
            Short distributionSequenceNumber) {
        BilEftPendingTape bilEftPendingTape = bilEftPendingTapeRepository.findUnidentifiedPendingRow(
                unidentifiedCash.getBillUnidCashId(), EftRecordType.UNIDENTIFIED.getValue(), DECIMAL_ZERO);
        ZonedDateTime pendingTapeDate = DateRoutine.defaultDateTime();
        if (bilEftPendingTape != null) {
            pendingTapeDate = updateBilEftPendingTape(bilEftPendingTape, accountId,
                    unidentifiedCash.getBilCshEtrMthCd(), distributionSequenceNumber);
        } else {
            BilEftActivity bilEftActivity = bilEftActivityRepository.findUnidentifiedActivityRow(
                    unidentifiedCash.getBillUnidCashId(), EftRecordType.UNIDENTIFIED.getValue(), DECIMAL_ZERO);
            if (bilEftActivity != null) {
                updateBilEftActivity(bilEftActivity, accountId, unidentifiedCash.getBilCshEtrMthCd());
            }
        }
        return pendingTapeDate;
    }

    private ZonedDateTime updateBilEftPendingTape(BilEftPendingTape bilEftPendingTapeRow, String accountId,
            Character cashEntryMethodCode, Short distributionSequenceNumber) {

        BilEftPendingTape bilEftPendingTape = new BilEftPendingTape();
        Short sequenceNumber = getIncrementalValue(bilEftPendingTapeRepository.getMaxSequenceNumber(accountId));
        bilEftPendingTape.setBilEftPendingTapeId(new BilEftPendingTapeId(accountId, CHAR_A,
                bilEftPendingTapeRow.getBilEftPendingTapeId().getEftTapeDate(),
                bilEftPendingTapeRow.getBilEftPendingTapeId().getEftActivityDate(), sequenceNumber));
        ZonedDateTime pendingTapedate = bilEftPendingTapeRow.getBilEftPendingTapeId().getEftTapeDate();
        bilEftPendingTape.setEftPostPaymentDate(execContext.getApplicationDate());
        bilEftPendingTape.setReceiptSequenceNumber(distributionSequenceNumber);
        bilEftPendingTape.setAgencyNbr(bilEftPendingTapeRow.getAgencyNbr());
        bilEftPendingTape.setBusinessActivityCd(bilEftPendingTapeRow.getBusinessActivityCd());
        bilEftPendingTape.setEftDrAmount(bilEftPendingTapeRow.getEftDrAmount());
        bilEftPendingTape.setEftDrDate(bilEftPendingTapeRow.getEftDrDate());
        bilEftPendingTape.setEftNotIndicator(bilEftPendingTapeRow.getEftNotIndicator());
        bilEftPendingTape.setEftReceivedType(bilEftPendingTapeRow.getEftReceivedType());
        bilEftPendingTape.setFileTypeCd(bilEftPendingTapeRow.getFileTypeCd());
        bilEftPendingTape.setInsertedRowTime(bilEftPendingTapeRow.getInsertedRowTime());
        bilEftPendingTape.setPaymentOrderId(bilEftPendingTapeRow.getPaymentOrderId());
        bilEftPendingTape.setWebAutomatedIndicator(bilEftPendingTapeRow.getWebAutomatedIndicator());
        bilEftPendingTape.setDisbursementId(BLANK_STRING);
        if (cashEntryMethodCode.equals(CashEntryMethod.CREDIT_CARD.getCashEntryMethod())) {
            bilEftPendingTape.setEftReceivedType(CHAR_T);
        }
        bilEftPendingTapeRepository.delete(bilEftPendingTapeRow);
        bilEftPendingTapeRepository.save(bilEftPendingTape);

        return pendingTapedate;
    }

    private void updateBilEftActivity(BilEftActivity bilEftActivityRow, String accountId,
            Character cashEntryMethodCode) {

        BilEftActivity bilEftActivity = new BilEftActivity();

        Short sequenceNumber = getIncrementalValue(bilEftActivityRepository.getMaxSequenceNumber(accountId));
        bilEftActivity.setBilEftActivityId(
                new BilEftActivityId(accountId, CHAR_A, bilEftActivityRow.getBilEftActivityId().getBilEftDrDt(),
                        bilEftActivityRow.getBilEftActivityId().getBilEftTraceNbr(), execContext.getApplicationDate(),
                        sequenceNumber));
        bilEftActivity.setAgencyNbr(bilEftActivityRow.getAgencyNbr());
        bilEftActivity.setBeaTrsSta(bilEftActivityRow.getBeaTrsSta());
        bilEftActivity.setBilBusActCd(bilEftActivityRow.getBilBusActCd());
        bilEftActivity.setBilDsbId(bilEftActivityRow.getBilDsbId());
        bilEftActivity.setBilEftAcyDt(bilEftActivityRow.getBilEftAcyDt());
        bilEftActivity.setBilEftDrAmt(bilEftActivityRow.getBilEftDrAmt());
        bilEftActivity.setBilEftNotInd(bilEftActivityRow.getBilEftNotInd());
        bilEftActivity.setBilEftTapeDt(bilEftActivityRow.getBilEftTapeDt());
        bilEftActivity.setBilFileTypeCd(bilEftActivityRow.getBilFileTypeCd());
        bilEftActivity.setBilPmtOrderId(bilEftActivityRow.getBilPmtOrderId());
        bilEftActivity.setInsertedRowTs(bilEftActivityRow.getInsertedRowTs());
        bilEftActivity.setWebAutInd(bilEftActivityRow.getWebAutInd());
        bilEftActivity.setBilEftRcdTyc(CHAR_A);
        if (cashEntryMethodCode.equals(CashEntryMethod.CREDIT_CARD.getCashEntryMethod())) {
            bilEftActivity.setBilEftRcdTyc(EftRecordType.RECONCILED_CASH.getValue());
        }

        bilEftActivityRepository.delete(bilEftActivityRow);
        bilEftActivityRepository.save(bilEftActivity);
    }

    private boolean isDigitalWalletPayment(Character cashEntryMethodCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(
                String.valueOf(cashEntryMethodCode), BusCodeTranslationType.DIGITAL_WALLET_TYPE.getValue(),
                execContext.getLanguage());

        return busCdTranslation != null ? Boolean.TRUE : Boolean.FALSE;
    }

    private void saveAccountSummary(String accountId, Character cashEntryMethod, ZonedDateTime eftPendingTapeDate,
            ZonedDateTime postmarkDate, String receivedTypeCode, Double amount, String userId, ZonedDateTime entryDate,
            String entryNumber, Short distributionSequenceNumber, String agencyNumber, Short entrySequenceNumber) {
        BilActSummary accountSummary = new BilActSummary();
        short sequenceNumber;

        sequenceNumber = getIncrementalValue(
                bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, execContext.getApplicationDate()));
        accountSummary
                .setBillActSummaryId(new BilActSummaryId(accountId, execContext.getApplicationDate(), sequenceNumber));
        switch (CashEntryMethod.getEnumValue(cashEntryMethod)) {

        case CREDIT_CARD:
            accountSummary.setBilDesReaTyp(BilDesReasonType.CPY.getValue());
            accountSummary.setBilAcyDes1Dt(eftPendingTapeDate);
            accountSummary.setBasAddDataTxt(generateAddDataText(entryDate, entryNumber, userId,
                    distributionSequenceNumber, cashEntryMethod, BLANK_STRING, entrySequenceNumber, false));
            break;

        case EFT_CREDIT_CARD:
        case EFT:

            accountSummary.setBilDesReaTyp(isEftDigitalWallet(cashEntryMethod, receivedTypeCode)
                    ? BilDesReasonType.EFT_WALLET_PAYMENT_TYPE.getValue()
                    : BLANK_STRING);
            accountSummary.setBilAcyDes1Dt(DateRoutine.defaultDateTime());
            accountSummary.setBasAddDataTxt(generateAddDataText(entryDate, entryNumber, userId,
                    distributionSequenceNumber, cashEntryMethod, BLANK_STRING, entrySequenceNumber, false));
            break;

        case ONE_TIME_ACH:
            accountSummary.setBilDesReaTyp(BilDesReasonType.ACH.getValue());
            accountSummary.setBilAcyDes1Dt(DateRoutine.defaultDateTime());
            accountSummary.setBasAddDataTxt(SEPARATOR_BLANK);
            break;

        case BANK_ACCOUNT:
            accountSummary.setBilDesReaTyp(BilDesReasonType.ONE_XA.getValue());
            accountSummary.setBilAcyDes1Dt(eftPendingTapeDate);
            accountSummary.setBasAddDataTxt(SEPARATOR_BLANK);

            break;

        case AGENCY_SWEEP:
            accountSummary.setBilDesReaTyp(BilDesReasonType.ASW.getValue());
            accountSummary.setBilAcyDes1Dt(eftPendingTapeDate);
            accountSummary.setBasAddDataTxt(generateAddDataText(entryDate, entryNumber, userId,
                    distributionSequenceNumber, cashEntryMethod, agencyNumber, entrySequenceNumber, false));
            break;

        default:
            if (isDigitalWalletPayment(cashEntryMethod)) {
                accountSummary.setBilDesReaTyp(BilDesReasonType.WALLET_PAYMENT_TYPE.getValue());
                accountSummary.setBilAcyDes1Dt(eftPendingTapeDate);
                accountSummary.setBasAddDataTxt(generateAddDataText(entryDate, entryNumber, userId,
                        distributionSequenceNumber, cashEntryMethod, BLANK_STRING, entrySequenceNumber, true));
            } else {
                accountSummary.setBilDesReaTyp(findPaymentTypeDescription(receivedTypeCode));
                accountSummary.setBilAcyDes1Dt(DateRoutine.defaultDateTime());
                accountSummary.setBasAddDataTxt(generateAddDataText(entryDate, entryNumber, userId,
                        distributionSequenceNumber, cashEntryMethod, BLANK_STRING, entrySequenceNumber, false));
            }

            break;
        }
        accountSummary.setBilAcyDesCd(receivedTypeCode);
        accountSummary.setBilAcyDes2Dt(postmarkDate);
        accountSummary.setBilAcyAmt(amount);

        accountSummary.setUserId(userId);
        accountSummary.setPolNbr(BLANK_STRING);
        accountSummary.setPolSymbolCd(BLANK_STRING);
        accountSummary.setBilAcyTs(execContext.getApplicationDateTime());
        bilActSummaryRepository.saveAndFlush(accountSummary);
    }

    private String findPaymentTypeDescription(String receiptTypeCode) {
        List<BusCdTranslation> busCodeTranslation = busCdTranslationRepository.findAllByCodeAndTypeList(receiptTypeCode,
                Arrays.asList(BusCodeTranslationType.PAYMENT_TYPE_CODE.getValue(),
                        BusCodeTranslationType.CASH_PAYMENT.getValue(),
                        BusCodeTranslationType.COLLECT_AGENCY_PAYMENT.getValue(),
                        BusCodeTranslationType.EXTERNAL_BANK_PAYMENT.getValue(),
                        BilDesReasonType.DISPOSITION_REASON.getValue(), BilDesReasonType.ONE_XA.getValue(),
                        BusCodeTranslationType.OTHER_PAYMENT.getValue()),
                execContext.getLanguage());
        if (busCodeTranslation != null) {
            receiptTypeCode = busCodeTranslation.get(0).getBusCdTranslationId().getBusType().trim();
        }
        return receiptTypeCode;
    }

    private String generateAddDataText(ZonedDateTime entryDate, String entryNumber, String userId,
            Short distributionSequenceNumber, Character cashEntryMethod, String agencyNumber, Short entrySequenceNumber,
            boolean isDigitalWallet) {
        StringBuilder dataText = new StringBuilder();
        switch (CashEntryMethod.getEnumValue(cashEntryMethod)) {
        case CREDIT_CARD:
            dataText.append(StringUtils.rightPad(CREDITTEXT, 6, BLANK_STRING));
            dataText.append(StringUtils.rightPad(PENDING, 8, BLANK_STRING));
            dataText.append(StringUtils.rightPad("#", 1));
            dataText.append(StringUtils.rightPad(distributionSequenceNumber.toString(), 1));
            dataText.append(StringUtils.rightPad("#", 1));
            dataText.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
            dataText.append(StringUtils.rightPad(entryNumber, 4));
            dataText.append(StringUtils.rightPad(userId, 8));
            dataText.append(StringUtils.rightPad("0", 4, CHAR_ZERO));
            break;
        case AGENCY_SWEEP:
            dataText.append(StringUtils.rightPad(agencyNumber, 15, BLANK_STRING));
            dataText.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
            dataText.append(StringUtils.rightPad(entryNumber, 4));
            dataText.append(StringUtils.rightPad(userId, 8));
            dataText.append(StringUtils.rightPad(BLANK_STRING, 4));
            dataText.append(StringUtils.rightPad("0", 5, CHAR_ZERO));

            break;
        default:
            if (isDigitalWallet) {
                dataText.append(StringUtils.rightPad(BilDesReasonType.WALLET_PAYMENT_TYPE.getValue(), 4, BLANK_STRING));
                dataText.append(StringUtils.rightPad(PENDING, 8, BLANK_STRING));
                dataText.append(StringUtils.rightPad("#", 1));
                dataText.append(StringUtils.rightPad(distributionSequenceNumber.toString(), 1));
                dataText.append(StringUtils.rightPad("#", 1));
                dataText.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
                dataText.append(StringUtils.rightPad(entryNumber, 4));
                dataText.append(StringUtils.rightPad(userId, 8));
                dataText.append(StringUtils.rightPad("0", 4, CHAR_ZERO));
            } else {
                dataText.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
                dataText.append(StringUtils.rightPad(entryNumber, 4));
                dataText.append(StringUtils.rightPad(userId, 8));
                dataText.append(StringUtils.leftPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
                dataText.append(StringUtils.rightPad(BLANK_STRING, 73));
            }

            break;
        }
        return dataText.toString();
    }

    private Short getIncrementalValue(Short sequenceNumber) {
        if (sequenceNumber == null) {
            return SHORT_ZERO;
        } else {
            return (short) (sequenceNumber + 1);
        }
    }

    private boolean isEftDigitalWallet(Character cashEntryMethod, String receivedTypeCode) {
        return busCdTranslationRepository.existsById(new BusCdTranslationId(String.valueOf(cashEntryMethod),
                BilDesReasonType.PYS.getValue(), execContext.getLanguage(), receivedTypeCode)) ? Boolean.TRUE
                        : Boolean.FALSE;
    }
}
