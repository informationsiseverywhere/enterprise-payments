package billing.service.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import application.security.service.impl.AppSecurityServiceImpl;
import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilBank;
import billing.data.entity.BilBankEntry;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilEftActivity;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilBankEntryId;
import billing.data.entity.id.BilCashEntryId;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.repository.BilBankEntryRepository;
import billing.data.repository.BilBankRepository;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilEftActivityRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.model.SuspendedPayment;
import billing.model.SuspendedPaymentList;
import billing.model.embedded.Party;
import billing.service.SuspendedPaymentService;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilDesReasonCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilTechKeyTypeCode;
import billing.utils.BillingConstants.BillingEftTransactionStatus;
import billing.utils.BillingConstants.BusCodeTranslationParentCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CashEntryMethod;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.IdentifierType;
import billing.utils.BillingConstants.PaymentOperations;
import billing.utils.BillingConstants.PaymentProviderType;
import billing.utils.BillingConstants.PaymentType;
import billing.utils.BillingConstants.Status;
import billing.utils.BillingConstants.TechnicalKeyTypeCode;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import core.utils.Formatter;
import core.utils.MultiFiltersSearch;
import core.utils.PageNavigation;
import disbursement.utils.DisbursementConstants.DisbursementStatusCode;
import party.service.impl.PartyElasticSearchServiceImpl;

@Service
public class SuspendedPaymentServiceImpl implements SuspendedPaymentService {

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private PartyElasticSearchServiceImpl partyElasticSearchServiceImpl;

    @Autowired
    private BilBankRepository bilBankRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    @Autowired
    private BilEftActivityRepository bilEftActivityRepository;

    @Autowired
    private BilBankEntryRepository bilBankEntryRepository;

    private static final Logger logger = LogManager.getLogger(SuspendedPaymentServiceImpl.class);

    private static final String SINCE = "since";
    private static final String UNTIL = "until";
    private static final String FROM_AMOUNT = "fromAmount";
    private static final String TO_AMOUNT = "toAmount";
    private static final String PAYMENT_METHOD = "paymentMethod";
    private static final String POLICY_IDENTIFIER = "Policy";
    private static final String USER_ID = "userId";
    private static final String PAYMENT_ID = "paymentId";
    private static final String IDENTIFIER_TYPE = "identifierType";
    private static final String CURRENCY = "currency";
    private static final String DEFAULT_DATE = "9999-12-31";
    private static final String BANK_PROFILE_ATTR_SUB_TYPE = "BankProfileAttrSubType";
    private static final String BILUNIDCASHID = "bilUnIdCashId.bilEntryDt";
    private static final String IDENTIFIER = "identifier";
    private static final String STATUS = "status";
    private static final String CASH_WITH_APPLICATION = "cashWithApplication";
    private static final String TRUE = "Yes";
    private static final String BIL_ENTRY_SEQNBR = "bilEntrySeqNbr";
    private static final String BIL_UNIDCASHID_BIL_DTB_SEQ_NBR = "bilUnIdCashId.bilDtbSeqNbr";
    private static final String SUPPORT_DATA_NOT_FOUND = "supportData.notfound";

    @Value("${enablePaymentIntegration}")
    private String paymentIntegration;

    @Value("${paymentIntegration.provider}")
    private String provider;

    @Override
    public SuspendedPayment findSuspendedPayment(String batchId, short sequenceNumber, String postedDate, String user,
            String distributionDate) {
        String userId = appSecurityServiceImpl.authenticateUser(user).getUserSequenceId();
        BilUnIdCashId bilUnIdCashId = new BilUnIdCashId(DateRoutine.dateTimeAsYYYYMMDD(distributionDate),
                sequenceNumber, batchId, userId, DateRoutine.dateTimeAsYYYYMMDD(postedDate));
        BilUnIdCash bilUnIdCash = bilUnIdCashRepository.findById(bilUnIdCashId).orElse(null);
        if (bilUnIdCash != null) {
            return mapUnIdCashToSuspendedPayment(bilUnIdCash);
        } else {
            return null;
        }
    }

    @Override
    public SuspendedPaymentList findSuspendedPayments(int evalPage, int evalPageSize, String filters) {
        Map<String, List<String>> matchFilterMap = new HashMap<>();
        matchFilterMap = updateMatchFilterMap(filters, matchFilterMap);
        ZonedDateTime since = DateRoutine.defaultSinceDateTime();
        ZonedDateTime until = DateRoutine.defaultDateTime();
        Double fromAmount = 0d;
        Double toAmount = 999999999999d;
        String fromUserId = BLANK_STRING;
        String toUserId = Formatter.generateField(BilUnIdCashId.class, USER_ID);
        String fromPaymentId = BLANK_STRING;
        String toPaymentId = Formatter.generateField(BilUnIdCash.class, "bilRctId");
        String identifierType = null;
        String currency = null;
        Character cashEntryMethod = null;
        Page<BilUnIdCash> bilUnIdCashList = null;
        String fromIdentifier = BLANK_STRING;
        String toIdentifier = Formatter.generateField(BilUnIdCash.class, "bilAccountNbr");
        String cashWithApplication = null;
        List<String> statusList = getStatusDesctiptionList();

        if (matchFilterMap.get(SINCE) != null) {
            since = DateRoutine.dateTimeAsYYYYMMDD(matchFilterMap.get(SINCE).get(SHORT_ZERO));
        }
        if (matchFilterMap.get(UNTIL) != null) {
            until = DateRoutine.dateTimeAsYYYYMMDD(matchFilterMap.get(UNTIL).get(SHORT_ZERO));
        }
        if (matchFilterMap.get(FROM_AMOUNT) != null) {
            fromAmount = Double.parseDouble(matchFilterMap.get(FROM_AMOUNT).get(SHORT_ZERO));
        }
        if (matchFilterMap.get(TO_AMOUNT) != null) {
            toAmount = Double.parseDouble(matchFilterMap.get(TO_AMOUNT).get(SHORT_ZERO));
        }
        if (matchFilterMap.get(CURRENCY) != null) {
            currency = matchFilterMap.get(CURRENCY).get(SHORT_ZERO);
        }
        if (matchFilterMap.get(PAYMENT_METHOD) != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    matchFilterMap.get(PAYMENT_METHOD).get(SHORT_ZERO).trim(),
                    BusCodeTranslationType.PAYMENTMETHOD.getValue(), execContext.getLanguage());
            if (busCdTranslation != null) {
                cashEntryMethod = busCdTranslation.getBusCdTranslationId().getBusCd().charAt(SHORT_ZERO);
            }
        }
        if (matchFilterMap.get(USER_ID) != null && !matchFilterMap.get(USER_ID.trim()).isEmpty()) {
            if (!matchFilterMap.get(USER_ID.trim()).get(0).equals(execContext.getUserId().trim())) {
                try {
                    fromUserId = appSecurityServiceImpl.authenticateUser(matchFilterMap.get(USER_ID).get(SHORT_ZERO))
                            .getUserSequenceId().toUpperCase();
                } catch (NullPointerException exception) {
                    logger.error("userId:{} does not exit. Setting it to blank", matchFilterMap.get(USER_ID.trim()));
                    fromUserId = BLANK_STRING;
                }
            } else {
                fromUserId = execContext.getUserSeqeunceId();
            }
            if (fromUserId.length() > 8) {
                return null;
            }
            toUserId = fromUserId;
        }
        if (matchFilterMap.get(PAYMENT_ID) != null) {
            fromPaymentId = matchFilterMap.get(PAYMENT_ID).get(SHORT_ZERO).toUpperCase();
            toPaymentId = fromPaymentId;
        }
        if (matchFilterMap.get(IDENTIFIER_TYPE) != null) {
            identifierType = matchFilterMap.get(IDENTIFIER_TYPE).get(SHORT_ZERO);
        }
        if (matchFilterMap.get(IDENTIFIER) != null) {
            if (identifierType != null) {
                fromIdentifier = matchFilterMap.get(IDENTIFIER).get(SHORT_ZERO).toUpperCase();
                toIdentifier = fromIdentifier;
            } else {
                throw new InvalidDataException("identifiertype.required");
            }
        }

        if (matchFilterMap.get(CASH_WITH_APPLICATION) != null) {
            cashWithApplication = matchFilterMap.get(CASH_WITH_APPLICATION).get(SHORT_ZERO).trim();
        }

        if (matchFilterMap.get(STATUS) != null) {
            List<String> statusDescriptionList = matchFilterMap.get(STATUS);
            statusList = new ArrayList<>();
            List<String> busCodeList = busCdTranslationRepository
                    .findCodeListByDescriptionListAndType(statusDescriptionList, "DSP", execContext.getLanguage());
            if (busCodeList != null && !busCodeList.isEmpty()) {
                statusList.addAll(busCodeList);
            } else {
                statusList.add(BLANK_STRING);
            }
        }

        if (identifierType != null && !identifierType.trim().isEmpty()) {
            String identifier = getIdentifierTypeCode(identifierType);
            switch (IdentifierType.getEnumKey(identifier)) {
            case GROUP:
                if (currency != null && !currency.trim().isEmpty()) {
                    if (cashEntryMethod == null) {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findGroupSuspendedPaymentsWithCurrency(since, until,
                                    fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository.findGroupSuspendedPaymentsWithCurrencyAndCAPTrue(
                                    since, until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId,
                                    toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository.findGroupSuspendedPaymentsWithCurrencyAndCAPFalse(
                                    since, until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId,
                                    toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    } else {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findGroupSuspendedPaymentsWithPaymentMethodAndCurrency(since, until, fromAmount,
                                            toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                            BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findGroupSuspendedPaymentsWithPaymentMethodAndCurrencyWithCAPTrue(since, until,
                                            fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                            toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier,
                                            statusList, BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findGroupSuspendedPaymentsWithPaymentMethodAndCurrencyWithCAPFalse(since, until,
                                            fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                            toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier,
                                            statusList, BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    }
                } else {
                    if (cashEntryMethod == null) {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findGroupSuspendedPayments(since, until, fromAmount,
                                    toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId, BLANK_STRING,
                                    fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository.findGroupSuspendedPaymentsWithCAPTrue(since, until,
                                    fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository.findGroupSuspendedPaymentsWithCAPFalse(since, until,
                                    fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    } else {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findGroupSuspendedPaymentsWithPaymentMethod(since,
                                    until, fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                    toPaymentId, BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findGroupSuspendedPaymentsWithPaymentMethodAndCAPTrue(since, until, fromAmount,
                                            toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                            BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findGroupSuspendedPaymentsWithPaymentMethodAndCAPFalse(since, until, fromAmount,
                                            toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                            BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    }
                }
                break;
            case CONSUMER_DIRECT:
                if (currency != null && !currency.trim().isEmpty()) {
                    if (cashEntryMethod == null) {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findBillAccountSuspendedPaymentsWithCurrency(since,
                                    until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findBillAccountSuspendedPaymentsWithCurrencyAndCAPTrue(since, until, fromAmount,
                                            toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId, BLANK_STRING,
                                            currency, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findBillAccountSuspendedPaymentsWithCurrencyAndCAPFalse(since, until, fromAmount,
                                            toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId, BLANK_STRING,
                                            currency, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    } else {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findBillAccountSuspendedPaymentsWithPaymentMethodAndCurrency(since, until,
                                            fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                            toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier,
                                            statusList,
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findBillAccountSuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPTrue(since,
                                            until, fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId,
                                            fromPaymentId, toPaymentId, BLANK_STRING, currency, fromIdentifier,
                                            toIdentifier, statusList, BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findBillAccountSuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPFalse(since,
                                            until, fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId,
                                            fromPaymentId, toPaymentId, BLANK_STRING, currency, fromIdentifier,
                                            toIdentifier, statusList, BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    }
                } else {
                    if (cashEntryMethod == null) {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findBillAccountSuspendedPayments(since, until,
                                    fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository.findBillAccountSuspendedPaymentsWithCAPTrue(since,
                                    until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository.findBillAccountSuspendedPaymentsWithCAPFalse(since,
                                    until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    } else {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findBillAccountSuspendedPaymentsWithPaymentMethod(
                                    since, until, fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId,
                                    fromPaymentId, toPaymentId, BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findBillAccountSuspendedPaymentsWithPaymentMethodAndCAPTrue(since, until,
                                            fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                            toPaymentId, BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findBillAccountSuspendedPaymentsWithPaymentMethodAndCAPFalse(since, until,
                                            fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                            toPaymentId, BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    }
                }
                break;
            case AGENCY:
                if (currency != null && !currency.trim().isEmpty()) {
                    if (cashEntryMethod == null) {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findAgencySuspendedPaymentsWithCurrency(since,
                                    until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository.findAgencySuspendedPaymentsWithCurrencyAndCAPTrue(
                                    since, until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId,
                                    toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository.findAgencySuspendedPaymentsWithCurrencyAndCAPFalse(
                                    since, until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId,
                                    toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    } else {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findAgencySuspendedPaymentsWithPaymentMethodAndCurrency(since, until, fromAmount,
                                            toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                            BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findAgencySuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPTrue(since, until,
                                            fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                            toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier,
                                            statusList, BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findAgencySuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPFalse(since, until,
                                            fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                            toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier,
                                            statusList, BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    }
                } else {
                    if (cashEntryMethod == null) {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findAgencySuspendedPayments(since, until,
                                    fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository.findAgencySuspendedPaymentsWithCAPTrue(since, until,
                                    fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository.findAgencySuspendedPaymentsWithCAPFalse(since,
                                    until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    } else {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findAgencySuspendedPaymentsWithPaymentMethod(since,
                                    until, fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                    toPaymentId, BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findAgencySuspendedPaymentsWithPaymentMethodAndCAPTrue(since, until, fromAmount,
                                            toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                            BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findAgencySuspendedPaymentsWithPaymentMethodAndCAPFalse(since, until, fromAmount,
                                            toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                            BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    }
                }
                break;
            case POLICY:
                if (currency != null && !currency.trim().isEmpty()) {
                    if (cashEntryMethod == null) {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findPolicySuspendedPaymentsWithCurrency(since,
                                    until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository.findPolicySuspendedPaymentsWithCurrencyAndCAPTrue(
                                    since, until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId,
                                    toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository.findPolicySuspendedPaymentsWithCurrencyAndCAPFalse(
                                    since, until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId,
                                    toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    } else {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findPolicySuspendedPaymentsWithPaymentMethodAndCurrency(since, until, fromAmount,
                                            toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                            BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findPolicySuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPTrue(since, until,
                                            fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                            toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier,
                                            statusList, BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findPolicySuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPFalse(since, until,
                                            fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                            toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier,
                                            statusList, BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    }
                } else {
                    if (cashEntryMethod == null) {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findPolicySuspendedPayments(since, until,
                                    fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository.findPolicySuspendedPaymentsAndCAPTrue(since, until,
                                    fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository.findPolicySuspendedPaymentsAndCAPFalse(since, until,
                                    fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    } else {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findPolicySuspendedPaymentsWithPaymentMethod(since,
                                    until, fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                    toPaymentId, BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findPolicySuspendedPaymentsWithPaymentMethodAndCAPTrue(since, until, fromAmount,
                                            toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                            BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findPolicySuspendedPaymentsWithPaymentMethodAndCAPFalse(since, until, fromAmount,
                                            toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                            BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    }
                }
                break;
            case ADDITIONALID:
                if (currency != null && !currency.trim().isEmpty()) {
                    if (cashEntryMethod == null) {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findAdditionalIdSuspendedPaymentsWithCurrency(since,
                                    until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, currency, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findAdditionalIdSuspendedPaymentsWithCurrencyAndCAPTrue(since, until, fromAmount,
                                            toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId, BLANK_STRING,
                                            currency, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findAdditionalIdSuspendedPaymentsWithCurrencyAndCAPFalse(since, until, fromAmount,
                                            toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId, BLANK_STRING,
                                            currency, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    } else {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findAdditionalIdSuspendedPaymentsWithPaymentMethodAndCurrency(since, until,
                                            fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                            toPaymentId, BLANK_STRING, currency, fromIdentifier, toIdentifier,
                                            statusList,
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findAdditionalIdSuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPTrue(since,
                                            until, fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId,
                                            fromPaymentId, toPaymentId, BLANK_STRING, currency, fromIdentifier,
                                            toIdentifier, statusList, BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findAdditionalIdSuspendedPaymentsWithPaymentMethodAndCurrencyAndCAPFalse(since,
                                            until, fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId,
                                            fromPaymentId, toPaymentId, BLANK_STRING, currency, fromIdentifier,
                                            toIdentifier, statusList, BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    }
                } else {
                    if (cashEntryMethod == null) {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findAdditionalIdSuspendedPayments(since, until,
                                    fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository.findAdditionalIdSuspendedPaymentsWithCAPTrue(since,
                                    until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository.findAdditionalIdSuspendedPaymentsWithCAPFalse(since,
                                    until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                    BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    BilDesReasonType.CAP.getValue(), PageRequest.of(evalPage - 1, evalPageSize, Sort
                                            .by(Sort.Order.desc(BILUNIDCASHID), Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    } else {
                        if (cashWithApplication == null) {
                            bilUnIdCashList = bilUnIdCashRepository.findAdditionalIdSuspendedPaymentsWithPaymentMethod(
                                    since, until, fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId,
                                    fromPaymentId, toPaymentId, BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                    PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else if (cashWithApplication.equals(TRUE)) {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findAdditionalIdSuspendedPaymentsWithPaymentMethodAndCAPTrue(since, until,
                                            fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                            toPaymentId, BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        } else {
                            bilUnIdCashList = bilUnIdCashRepository
                                    .findAdditionalIdSuspendedPaymentsWithPaymentMethodAndCAPFalse(since, until,
                                            fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                            toPaymentId, BLANK_STRING, fromIdentifier, toIdentifier, statusList,
                                            BilDesReasonType.CAP.getValue(),
                                            PageRequest.of(evalPage - 1, evalPageSize,
                                                    Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                            Sort.Order.desc(BIL_ENTRY_SEQNBR))));
                        }
                    }
                }
                break;
            default:
                break;
            }
        } else {
            if (currency != null && !currency.trim().isEmpty()) {
                if (cashEntryMethod == null) {
                    if (cashWithApplication == null) {
                        bilUnIdCashList = bilUnIdCashRepository.getSuspendedPaymentListWithCurrency(since, until,
                                fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId, currency,
                                statusList,
                                PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                        Sort.Order.desc(BIL_UNIDCASHID_BIL_DTB_SEQ_NBR))));
                    } else if (cashWithApplication.equals(TRUE)) {
                        bilUnIdCashList = bilUnIdCashRepository.getSuspendedPaymentListWithCurrencyAndCAPTrue(since,
                                until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId, currency,
                                statusList, BilDesReasonType.CAP.getValue(),
                                PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                        Sort.Order.desc(BIL_UNIDCASHID_BIL_DTB_SEQ_NBR))));
                    } else {
                        bilUnIdCashList = bilUnIdCashRepository.getSuspendedPaymentListWithCurrencyAndCAPFalse(since,
                                until, fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId, currency,
                                statusList, BilDesReasonType.CAP.getValue(),
                                PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                        Sort.Order.desc(BIL_UNIDCASHID_BIL_DTB_SEQ_NBR))));
                    }
                } else {
                    if (cashWithApplication == null) {
                        bilUnIdCashList = bilUnIdCashRepository.getSuspendedPaymentListWithPaymentMethodAndCurrency(
                                since, until, fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId,
                                fromPaymentId, toPaymentId, currency, statusList,
                                PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                        Sort.Order.desc(BIL_UNIDCASHID_BIL_DTB_SEQ_NBR))));
                    } else if (cashWithApplication.equals(TRUE)) {
                        bilUnIdCashList = bilUnIdCashRepository
                                .getSuspendedPaymentListWithPaymentMethodAndCurrencyAndCAPTrue(since, until, fromAmount,
                                        toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                        currency, statusList, BilDesReasonType.CAP.getValue(),
                                        PageRequest.of(evalPage - 1, evalPageSize,
                                                Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                        Sort.Order.desc(BIL_UNIDCASHID_BIL_DTB_SEQ_NBR))));
                    } else {
                        bilUnIdCashList = bilUnIdCashRepository
                                .getSuspendedPaymentListWithPaymentMethodAndCurrencyAndCAPFalse(since, until,
                                        fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId,
                                        toPaymentId, currency, statusList, BilDesReasonType.CAP.getValue(),
                                        PageRequest.of(evalPage - 1, evalPageSize,
                                                Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                                        Sort.Order.desc(BIL_UNIDCASHID_BIL_DTB_SEQ_NBR))));
                    }
                }
            } else {
                if (cashEntryMethod == null) {
                    if (cashWithApplication == null) {
                        bilUnIdCashList = bilUnIdCashRepository.getSuspendedPaymentList(since, until, fromAmount,
                                toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId, statusList,
                                PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                        Sort.Order.desc(BIL_UNIDCASHID_BIL_DTB_SEQ_NBR))));
                    } else if (cashWithApplication.equals(TRUE)) {
                        bilUnIdCashList = bilUnIdCashRepository.getSuspendedPaymentListWithCAPTrue(since, until,
                                fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId, statusList,
                                BilDesReasonType.CAP.getValue(),
                                PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                        Sort.Order.desc(BIL_UNIDCASHID_BIL_DTB_SEQ_NBR))));
                    } else {
                        bilUnIdCashList = bilUnIdCashRepository.getSuspendedPaymentListWithCAPFalse(since, until,
                                fromAmount, toAmount, fromUserId, toUserId, fromPaymentId, toPaymentId, statusList,
                                BilDesReasonType.CAP.getValue(),
                                PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                        Sort.Order.desc(BIL_UNIDCASHID_BIL_DTB_SEQ_NBR))));
                    }
                } else {
                    if (cashWithApplication == null) {
                        bilUnIdCashList = bilUnIdCashRepository.getSuspendedPaymentListWithPaymentMethod(since, until,
                                fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId, fromPaymentId, toPaymentId,
                                statusList,
                                PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                        Sort.Order.desc(BIL_UNIDCASHID_BIL_DTB_SEQ_NBR))));
                    } else if (cashWithApplication.equals(TRUE)) {
                        bilUnIdCashList = bilUnIdCashRepository.getSuspendedPaymentListWithPaymentMethodAndCAPTrue(
                                since, until, fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId,
                                fromPaymentId, toPaymentId, statusList, BilDesReasonType.CAP.getValue(),
                                PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                        Sort.Order.desc(BIL_UNIDCASHID_BIL_DTB_SEQ_NBR))));
                    } else {
                        bilUnIdCashList = bilUnIdCashRepository.getSuspendedPaymentListWithPaymentMethodAndCAPFalse(
                                since, until, fromAmount, toAmount, cashEntryMethod, fromUserId, toUserId,
                                fromPaymentId, toPaymentId, statusList, BilDesReasonType.CAP.getValue(),
                                PageRequest.of(evalPage - 1, evalPageSize, Sort.by(Sort.Order.desc(BILUNIDCASHID),
                                        Sort.Order.desc(BIL_UNIDCASHID_BIL_DTB_SEQ_NBR))));
                    }
                }
            }
        }

        List<SuspendedPayment> suspendedPaymentList = new ArrayList<>();
        if (bilUnIdCashList != null && bilUnIdCashList.getSize() > 0) {
            for (BilUnIdCash bilUnIdCash : bilUnIdCashList.getContent()) {
                SuspendedPayment suspendedPayment = mapLimitedUnIdCashToSuspendedPayment(bilUnIdCash);
                suspendedPaymentList.add(suspendedPayment);
            }
        } else {
            return null;
        }
        SuspendedPaymentList paymentList = new SuspendedPaymentList();
        paymentList.setPayments(suspendedPaymentList);
        paymentList.setPage(
                PageNavigation.derivePageAttributes((int) bilUnIdCashList.getTotalElements(), evalPageSize, evalPage));
        return paymentList;
    }

    @Override
    public Map<String, Boolean> areOperationsAllowed(short sequenceNumber, String postedDate, String user,
            String batchId, String distributionDate) {
        Map<String, Boolean> allowedOperations = new HashMap<>();
        allowedOperations.put(PaymentOperations.ALLOW_REVERSE.getValue(), true);
        allowedOperations.put(PaymentOperations.ALLOW_WRITEOFF.getValue(), false);
        allowedOperations.put(PaymentOperations.ALLOW_DISBUSEMENT.getValue(), false);
        allowedOperations.put(PaymentOperations.ALLOW_SUSPENSE.getValue(), false);
        allowedOperations.put(PaymentOperations.ALLOW_UPDATE.getValue(), false);
        String userId = appSecurityServiceImpl.authenticateUser(user).getUserSequenceId();

        BilUnIdCash bilUnIdCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(DateRoutine.dateTimeAsYYYYMMDD(distributionDate), sequenceNumber, batchId,
                        userId, DateRoutine.dateTimeAsYYYYMMDD(postedDate)))
                .orElse(null);
        if (bilUnIdCash != null) {
            boolean isPendingAchPayment = false;
            if (bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()
                    || bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()
                    || bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()
                    || isDigitalWalletPayment(bilUnIdCash.getBilCshEtrMthCd())
                            && !bilUnIdCash.getBillUnidCashId().trim().isEmpty()) {
                isPendingAchPayment = checkPendingAchPayment(bilUnIdCash.getBillUnidCashId(),
                        DateRoutine.dateTimeAsYYYYMMDD(distributionDate), bilUnIdCash.getBilCshEtrMthCd(), bilUnIdCash.getBilUnIdCashId().getBilDtbSeqNbr());
            }
            if (bilUnIdCash.getBilDspTypeCd().equals(BilDspTypeCode.SUSPENDED.getValue()) && !isPendingAchPayment) {
                allowedOperations.put(PaymentOperations.ALLOW_DISBUSEMENT.getValue(), true);
            }
            if (bilUnIdCash.getBilDspTypeCd().equals(BilDspTypeCode.SUSPENDED.getValue()) || isPendingAchPayment) {
                allowedOperations.put(PaymentOperations.ALLOW_WRITEOFF.getValue(), true);
            }
            if (bilUnIdCash.getBilDspTypeCd().equals(BilDspTypeCode.WRITE_OFF.getValue())) {
                allowedOperations.put(PaymentOperations.ALLOW_SUSPENSE.getValue(), true);
            }
            if (bilUnIdCash.getBilDspTypeCd().equals(BilDspTypeCode.DISBURSED.getValue())
                    && (bilUnIdCash.getDwsStatusCd() == DisbursementStatusCode.PENDING.toChar()
                            || bilUnIdCash.getDwsStatusCd() == DisbursementStatusCode.HOLD.toChar())) {
                allowedOperations.put(PaymentOperations.ALLOW_SUSPENSE.getValue(), true);
            }
            if (bilUnIdCash.getBilDspTypeCd().equals(BilDspTypeCode.SUSPENDED.getValue()) || isPendingAchPayment) {
                allowedOperations.put(PaymentOperations.ALLOW_UPDATE.getValue(), true);
            }
            if (!isPendingAchPayment && bilUnIdCash.getBilDspTypeCd().equals(BilDspTypeCode.SUSPENDED.getValue())
                    && (bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()
                            || bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod())
                    || bilUnIdCash.getBilDspTypeCd().equals(BilDspTypeCode.REVERSED.getValue())) {
                allowedOperations.put(PaymentOperations.ALLOW_REVERSE.getValue(), false);
            }
            if (!isPendingAchPayment
                    && (bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()
                            || bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()
                            || bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod())) {
                allowedOperations.put(PaymentOperations.ALLOW_REVERSE.getValue(), false);
            }

            if (provider.equals(PaymentProviderType.PAYMENTUS.getValue())
                    && (bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()
                            || bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()
                            || bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()
                            || isDigitalWalletPayment(bilUnIdCash.getBilCshEtrMthCd()))) {
                allowedOperations.put(PaymentOperations.ALLOW_REVERSE.getValue(), false);
            }
        }
        return allowedOperations;
    }

    private boolean checkPendingAchPayment(String bilUnidCashId, ZonedDateTime distributionDate,
            char cashEntryMethodCode, short dtbSequenceNumber) {
        List<Character> bilEftRecordTypeList = new ArrayList<>();
        bilEftRecordTypeList.add(EftRecordType.RECONCILED_CASH.getValue());
        bilEftRecordTypeList.add(EftRecordType.UNIDENTIFIED.getValue());
        bilEftRecordTypeList.add(EftRecordType.ONE_TIME_ACH.getValue());
        bilEftRecordTypeList.add(EftRecordType.AGENCY_SWEEP.getValue());
        bilEftRecordTypeList.add(EftRecordType.DIGITAL_WALLET.getValue());

        if (cashEntryMethodCode == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()
                || cashEntryMethodCode == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()
                || checkIfEFTRow(bilUnidCashId)) {
            ZonedDateTime insertedTimeStamp = bilEftPendingTapeRepository.getTimeStamp(bilUnidCashId, distributionDate,
                    dtbSequenceNumber, bilEftRecordTypeList, TechnicalKeyTypeCode.UNIDENTIFIED.getValue());
            if (insertedTimeStamp != null) {
                return true;
            }
        }
        return false;
    }

    private SuspendedPayment mapUnIdCashToSuspendedPayment(BilUnIdCash bilUnIdCash) {
        SuspendedPayment suspendedPayment = new SuspendedPayment();
        boolean isDigitalPayment = isDigitalWalletPayment(bilUnIdCash.getBilCshEtrMthCd());
        suspendedPayment.setDistributionDate(bilUnIdCash.getBilUnIdCashId().getBilDtbDt());
        suspendedPayment.setPaymentSequenceNumber(bilUnIdCash.getBilUnIdCashId().getBilDtbSeqNbr());
        suspendedPayment.setPostedDate(bilUnIdCash.getBilUnIdCashId().getBilEntryDt());
        if (!bilUnIdCash.getBilPstmrkDt().toString().substring(0, 10).equalsIgnoreCase(DEFAULT_DATE)) {
            suspendedPayment.setPostMarkDate(bilUnIdCash.getBilPstmrkDt());
        }
        suspendedPayment.setBatchId(bilUnIdCash.getBilUnIdCashId().getBilEntryNbr().trim());
        suspendedPayment.setUserId(
                appSecurityServiceImpl.getUserIdByUserSeqId(bilUnIdCash.getBilUnIdCashId().getUserId()).trim());
        suspendedPayment.setUserName(appSecurityServiceImpl
                .getUserNameByUserSeqId(bilUnIdCash.getBilUnIdCashId().getUserId().trim()).trim());
        suspendedPayment.setAdditionalId(bilUnIdCash.getBilAdditionalId().trim());
        if (bilUnIdCash.getBilRctAdrSeq() != 0) {
            suspendedPayment.setAddressSequenceNumber(bilUnIdCash.getBilRctAdrSeq());
        }
        suspendedPayment.setPaymentAmount(bilUnIdCash.getBilRctAmt());
        suspendedPayment.setPaymentComments(bilUnIdCash.getBilRctCmt().trim());
        if (!bilUnIdCash.getBilAdjDueDt().isEqual(DateRoutine.defaultDateTime())) {
            suspendedPayment.setDueDate(bilUnIdCash.getBilAdjDueDt());
        }
        if (!bilUnIdCash.getBilDepositDt().equals(DateRoutine.defaultDateTime())) {
            suspendedPayment.setDepositDate(bilUnIdCash.getBilDepositDt());
        }
        suspendedPayment.setPolicyNumber(bilUnIdCash.getPolNbr().trim());
        suspendedPayment.setActivityUserId(bilUnIdCash.getActivityUserId().trim());
        suspendedPayment.setPayorId(bilUnIdCash.getBilRctCltId().trim());
        if (!bilUnIdCash.getBilPayeeCltId().trim().isEmpty()) {
            Party payor = getPayorName(bilUnIdCash.getBilPayeeCltId().trim());
            if (payor != null) {
                suspendedPayment.setPayorName(payor.getName());
            }
        }
        suspendedPayment.setPaymentId(bilUnIdCash.getBilRctId().trim());
        suspendedPayment.setPayableItem(getDescriptionForPayableItem(bilUnIdCash.getBilPblItemCd().trim()));

        if (bilUnIdCash.getBilCshEtrMthCd() == CHAR_X) {
            suspendedPayment.setPaymentType(BLANK_STRING);
        } else {
            suspendedPayment.setPaymentType(
                    getPaymentTypeDescription(bilUnIdCash.getBilRctTypeCd(), bilUnIdCash.getBilCshEtrMthCd()));
        }
        suspendedPayment.setPolicySymbol(getDescriptionForPolicySymbol(bilUnIdCash.getPolSymbolCd()));

        if (!bilUnIdCash.getBilAccountNbr().trim().isEmpty()) {
            suspendedPayment.setIdentifier(bilUnIdCash.getBilAccountNbr().trim());
            String identifierType = findDescriptionByCodeAndType(AccountType.BILLACCOUNT.toString(),
                    BusCodeTranslationType.BILLACCOUNTTYPE.getValue());
            if (!identifierType.isEmpty()) {
                suspendedPayment.setIdentifierType(identifierType);
            } else {
                throw new InvalidDataException(SUPPORT_DATA_NOT_FOUND,
                        new Object[] { BusCodeTranslationType.BILLACCOUNTTYPE.getValue() });
            }
        } else if (!bilUnIdCash.getBilTtyNbr().trim().isEmpty()) {
            suspendedPayment.setIdentifier(bilUnIdCash.getBilTtyNbr().trim());
            String identifierType = findDescriptionByCodeAndType(AccountType.GROUP_ACCOUNT.toString(),
                    BusCodeTranslationType.BILLACCOUNTTYPE.getValue());
            if (!identifierType.isEmpty()) {
                suspendedPayment.setIdentifierType(identifierType);
            } else {
                throw new InvalidDataException(SUPPORT_DATA_NOT_FOUND,
                        new Object[] { BusCodeTranslationType.BILLACCOUNTTYPE.getValue() });
            }
        } else if (!bilUnIdCash.getBilAgtActNbr().trim().isEmpty()) {
            suspendedPayment.setIdentifier(bilUnIdCash.getBilAgtActNbr().trim());
            String identifierType = findDescriptionByCodeAndType(AccountType.AGENCY_ACCOUNT.toString(),
                    BusCodeTranslationType.BILLACCOUNTTYPE.getValue());
            if (!identifierType.isEmpty()) {
                suspendedPayment.setIdentifierType(identifierType);
            } else {
                throw new InvalidDataException(SUPPORT_DATA_NOT_FOUND,
                        new Object[] { BusCodeTranslationType.BILLACCOUNTTYPE.getValue() });
            }
        } else if (!bilUnIdCash.getPolNbr().trim().isEmpty()) {
            suspendedPayment.setIdentifier(
                    bilUnIdCash.getPolSymbolCd().trim().concat(SEPARATOR_BLANK).concat(bilUnIdCash.getPolNbr().trim()));
            suspendedPayment.setIdentifierType(POLICY_IDENTIFIER);
        }

        suspendedPayment.setHoldSuspenseIndicator(bilUnIdCash.getBilManualSusInd() == CHAR_Y);
        if (bilUnIdCash.getBilDspTypeCd().trim().equals(BilDspTypeCode.SUSPENDED.getValue())) {
            if (Boolean.FALSE.equals(suspendedPayment.getHoldSuspenseIndicator())) {
                suspendedPayment.setSuspenseReason(BLANK_STRING);
            } else {
                suspendedPayment.setSuspenseReason(findDescriptionByCodeAndType(bilUnIdCash.getBilDspReasonCd(),
                        BusCodeTranslationType.DIRECT_TYPE.getValue()));
            }

        } else if (bilUnIdCash.getBilDspTypeCd().trim().equals(BilDspTypeCode.WRITE_OFF.getValue())) {
            List<BusCdTranslation> busCdTranslationList = busCdTranslationRepository.findAllByCodeAndTypeList(
                    bilUnIdCash.getBilDspReasonCd(), Arrays.asList(BusCodeTranslationType.WRITEOFF_LIFE_TYPE.getValue(),
                            BusCodeTranslationType.WRITEOFF_MANUAL_TYPE.getValue()),
                    execContext.getLanguage());
            if (busCdTranslationList != null && !busCdTranslationList.isEmpty()) {
                suspendedPayment.setSuspenseReason(busCdTranslationList.get(SHORT_ZERO).getBusDescription().trim());
            }
        } else if (bilUnIdCash.getBilDspTypeCd().trim().equals(BilDspTypeCode.REVERSED.getValue())) {
            List<BusCdTranslation> busCdTranslationList = busCdTranslationRepository.findAllByCodeAndTypeList(
                    bilUnIdCash.getBilDspReasonCd(), Arrays.asList(BusCodeTranslationType.REVERSE_SUSPENSE_TYPE.getValue()),
                    execContext.getLanguage());
            if (busCdTranslationList != null && !busCdTranslationList.isEmpty()) {
                suspendedPayment.setSuspenseReason(busCdTranslationList.get(SHORT_ZERO).getBusDescription().trim());
            }
            
        }

        suspendedPayment.setStatus(getStatus(bilUnIdCash.getBilDspTypeCd().trim(), bilUnIdCash.getBilCshEtrMthCd(),
                bilUnIdCash.getBillUnidCashId(), isDigitalPayment));

        char paymentMethod = bilUnIdCash.getBilCshEtrMthCd();
        if (paymentMethod == CHAR_X) {
            suspendedPayment.setTransferToAgency(true);
        } else {
            suspendedPayment.setTransferToAgency(true);
            suspendedPayment.setTransferToDirect(true);
            suspendedPayment.setTransferToGroup(true);
        }
        suspendedPayment.setPaymentMethod(getPaymentMethodDescription(Character.toString(paymentMethod)));
        suspendedPayment.setBusinessGroup(getBusinessGroupReasonDescription((bilUnIdCash.getBilBusGrpCd()).trim()));
        suspendedPayment.setBusinessGroupCode(bilUnIdCash.getBilBusGrpCd().trim());
        ZonedDateTime entryDate = bilUnIdCash.getBilUnIdCashId().getBilEntryDt();
        String userId = bilUnIdCash.getBilUnIdCashId().getUserId();
        String batchId = bilUnIdCash.getBilUnIdCashId().getBilEntryNbr();

        if (bilUnIdCash.getBilCshEtrMthCd() == PaymentType.AGENCY_SWEEP.getValue().charAt(SHORT_ZERO)) {
            if (paymentIntegration != null && !paymentIntegration.isEmpty()
                    && Boolean.TRUE.equals(Boolean.valueOf(paymentIntegration))) {
                getpaymentIdForAgencySweep(bilUnIdCash, suspendedPayment);
            }

        } else {
            BilCashEntry bilCashEntry = bilCashEntryRepository
                    .findById(new BilCashEntryId(entryDate, batchId, userId, bilUnIdCash.getBilEntrySeqNbr()))
                    .orElse(null);
            if (bilCashEntry != null && !bilCashEntry.getBilPmtOrderId().trim().isEmpty()) {
                suspendedPayment.setPaymentId(bilCashEntry.getBilPmtOrderId().trim());
            }
        }

        if (isDigitalPayment) {
            suspendedPayment.setWalletType(suspendedPayment.getPaymentMethod());
            suspendedPayment.setWalletNumber(bilUnIdCash.getBilCrcrdActNbr());
        }

        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(entryDate, batchId, userId)).orElse(null);
        if (bilCashEntryTot != null) {
            suspendedPayment.setCurrency(multiCurrencyService.getCurrencyCode(bilCashEntryTot.getCurrencyCd()));
            suspendedPayment.setDepositBank(getBankName(bilCashEntryTot.getBctDepBankCd().trim()));
            suspendedPayment.setControlBank(getBankName(bilCashEntryTot.getBilBankCd().trim()));
        }

        if (bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()) {
            BilBankEntry bilBankEntry = bilBankEntryRepository
                    .findById(new BilBankEntryId(entryDate, batchId, userId, bilUnIdCash.getBilEntrySeqNbr()))
                    .orElse(null);
            if (bilBankEntry != null) {
                suspendedPayment.setBankAccountNumber(bilBankEntry.getBankAccountNbr().trim());
            }
        } else if (bilUnIdCash.getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()) {
            suspendedPayment.setCreditCardNumber(bilUnIdCash.getBilCrcrdActNbr().trim());
            suspendedPayment.setCreditCardType(getCodeDescription(String.valueOf(bilUnIdCash.getBilCrcrdTypeCd()),
                    BusCodeTranslationType.CREDIT_CARD_TYPE.getValue()));
        }

        return suspendedPayment;
    }

    private String getCodeDescription(String busCode, String busTypeCode) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(busCode, busTypeCode,
                execContext.getLanguage());
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusDescription().trim();
        }
        return busCode;
    }

    private void getpaymentIdForAgencySweep(BilUnIdCash bilUnIdCash, SuspendedPayment suspendedPayment) {
        List<BilEftActivity> bilEftActivityList = bilEftActivityRepository.getPaymentIdRow(
                bilUnIdCash.getBillUnidCashId(), BilTechKeyTypeCode.UNIDENTIFIED.getValue(),
                bilUnIdCash.getBilUnIdCashId().getBilDtbDt(), bilUnIdCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                BillingEftTransactionStatus.APPLIED.getValue());
        if (bilEftActivityList != null && !bilEftActivityList.isEmpty()) {
            suspendedPayment.setPaymentId(bilEftActivityList.get(SHORT_ZERO).getBilPmtOrderId().trim());
        } else {
            suspendedPayment.setPaymentId(BLANK_STRING);
        }

    }

    private String getBankName(String bankCode) {
        if (!bankCode.isEmpty()) {
            BilBank bilBank = bilBankRepository.findById(bankCode).orElse(null);
            if (bilBank != null) {
                return bilBank.getBbkAccountNm().trim();
            }
        }
        return BLANK_STRING;
    }

    private String getBusinessGroupReasonDescription(String bilBusGrpCd) {
        if (!bilBusGrpCd.trim().isEmpty()) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndTypeAndParentCode(bilBusGrpCd,
                    BANK_PROFILE_ATTR_SUB_TYPE, execContext.getLanguage(), "ACT");
            if (busCdTranslation != null) {
                return busCdTranslation.getBusDescription().trim();
            } else {
                throw new DataNotFoundException(SUPPORT_DATA_NOT_FOUND,
                        new Object[] { bilBusGrpCd, BANK_PROFILE_ATTR_SUB_TYPE });
            }
        }
        return BLANK_STRING;
    }

    private String getStatus(String dispositionTypeCode, Character cashEntryMethodCode, String bilUnIdCashId,
            boolean isDigitalPayment) {
        boolean isPendingAchPayment = false;
        if ((cashEntryMethodCode.equals(CashEntryMethod.CREDIT_CARD.getCashEntryMethod())
                || cashEntryMethodCode.equals(CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod())
                || cashEntryMethodCode.equals(CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()) || isDigitalPayment)
                && dispositionTypeCode.equals(BilDspTypeCode.SUSPENDED.getValue())) {
            isPendingAchPayment = checkIfEFTRow(bilUnIdCashId);
        }
        if (isPendingAchPayment) {
            return findDescriptionByCodeAndTypeAndParent(BilDesReasonCode.PAC.getValue(),
                    BilDesReasonType.RCT.getValue());
        } else {
            return findDescriptionByCodeAndTypeAndParent(dispositionTypeCode, BilDesReasonType.DSP.getValue());
        }
    }

    private String findDescriptionByCodeAndType(String busCode, String busCodeType) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(busCode, busCodeType,
                execContext.getLanguage());
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusDescription().trim();
        }
        return BLANK_STRING;
    }

    private String findDescriptionByCodeAndTypeAndParent(String busCode, String busCodeType) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(busCode, busCodeType,
                execContext.getLanguage());
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusDescription().trim();
        }
        return BLANK_STRING;
    }

    private String getDescriptionForPayableItem(String payableItemCode) {

        if (!payableItemCode.trim().isEmpty()) {
            List<String> payableItemList = new ArrayList<>();
            payableItemList.add(BusCodeTranslationType.BILLING_ITEM.getValue());
            payableItemList.add(BusCodeTranslationType.AMOUNT.getValue());
            payableItemList.add(BusCodeTranslationType.BPA.getValue());
            payableItemList.add(BusCodeTranslationType.COMMISION.getValue());
            payableItemList.add(BusCodeTranslationType.EIT.getValue());
            payableItemList.add(BusCodeTranslationType.BPS.getValue());

            List<BusCdTranslation> busCodeTranslationList = busCdTranslationRepository
                    .findAllByCodeAndTypeListAndParentCode(payableItemCode, payableItemList,
                            BusCodeTranslationParentCode.PAYABLE_ITEM.getValue(), execContext.getLanguage());
            if (busCodeTranslationList != null && !busCodeTranslationList.isEmpty()) {
                return busCodeTranslationList.get(0).getBusDescription().trim();
            } else {
                throw new DataNotFoundException("support.data.missing", new Object[] { "Payable Item" });
            }
        } else {
            return BLANK_STRING;
        }
    }

    private String getDescriptionForPolicySymbol(String polSymbolCd) {
        if (polSymbolCd.trim().equals(BLANK_STRING)) {
            return BLANK_STRING;
        }
        return findDescriptionByCodeAndType(polSymbolCd, BusCodeTranslationType.POLICY_SYMBOL_TYPE.getValue());
    }

    private Map<String, List<String>> updateMatchFilterMap(String filters, Map<String, List<String>> matchFilterMap) {
        if (filters != null) {
            matchFilterMap = MultiFiltersSearch.getFiltersMatchList(filters, matchFilterMap);
        }
        return matchFilterMap;
    }

    private String getPaymentTypeDescription(String receiptTypeCode, char paymentMethod) {
        BusCdTranslation busCodeTranslation = null;
        if (paymentMethod == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(receiptTypeCode,
                    BusCodeTranslationType.CREDIT_CARD_PAYMENT.getValue(), execContext.getLanguage());

        } else if (paymentMethod == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(receiptTypeCode,
                    BusCodeTranslationType.BANK_ACCOUNT_PAYMENT.getValue(), execContext.getLanguage());

        } else if (paymentMethod == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(receiptTypeCode,
                    BilDesReasonType.ASW.getValue(), execContext.getLanguage());

        } else if (paymentMethod == CashEntryMethod.ONE_TIME_ACH.getCashEntryMethod()) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(receiptTypeCode,
                    BilDesReasonType.ACH.getValue(), execContext.getLanguage());

        } else if (paymentMethod == CashEntryMethod.AGENT_CREDIT_CARD.getCashEntryMethod()) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(receiptTypeCode,
                    BilDesReasonType.GPY.getValue(), execContext.getLanguage());

        } else if (paymentMethod == CashEntryMethod.VENDOR_COLLECTIONS.getCashEntryMethod()) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(receiptTypeCode,
                    BilDesReasonType.COL.getValue(), execContext.getLanguage());
        } else if (isDigitalWalletPayment(paymentMethod)) {
            busCodeTranslation = busCdTranslationRepository.findByCodeAndType(receiptTypeCode,
                    BilDesReasonType.WALLET_PAYMENT_TYPE.getValue(), execContext.getLanguage());
        } else {
            List<BusCdTranslation> busCodeTranslationList = busCdTranslationRepository.findAllByCodeAndTypeList(
                    receiptTypeCode, Arrays.asList(BusCodeTranslationType.CASH_PAYMENT.getValue(),
                            BilDesReasonType.PYC.getValue(), BilDesReasonType.COL.getValue()),
                    execContext.getLanguage());
            if (busCodeTranslationList != null && !busCodeTranslationList.isEmpty()) {
                busCodeTranslation = busCodeTranslationList.get(SHORT_ZERO);
            }
        }
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusDescription().trim();
        }
        return receiptTypeCode;
    }

    private boolean checkIfEFTRow(String accountId) {
        List<Character> bilEftRecordTypeList = new ArrayList<>();
        bilEftRecordTypeList.add(EftRecordType.RECONCILED_CASH.getValue());
        bilEftRecordTypeList.add(EftRecordType.UNIDENTIFIED.getValue());
        bilEftRecordTypeList.add(EftRecordType.ONE_TIME_ACH.getValue());
        bilEftRecordTypeList.add(EftRecordType.AGENCY_SWEEP.getValue());
        bilEftRecordTypeList.add(EftRecordType.DIGITAL_WALLET.getValue());
        Integer rowCount = bilEftPendingTapeRepository.checkIfExists(accountId, bilEftRecordTypeList);
        return rowCount != null && rowCount > SHORT_ZERO;
    }

    private Party getPayorName(String clientId) {
        try {
            Party partySearch = partyElasticSearchServiceImpl.fetchPartyDocument(clientId.trim());
            if (partySearch != null) {
                return partySearch;
            }
        } catch (IOException e) {
            logger.error("Error in fetching the Party Document");
        }
        return null;
    }

    private SuspendedPayment mapLimitedUnIdCashToSuspendedPayment(BilUnIdCash bilUnIdCash) {

        SuspendedPayment suspendedPayment = new SuspendedPayment();
        boolean isDigitalPayment = isDigitalWalletPayment(bilUnIdCash.getBilCshEtrMthCd());
        suspendedPayment.setDistributionDate(bilUnIdCash.getBilUnIdCashId().getBilDtbDt());
        suspendedPayment.setPaymentSequenceNumber(bilUnIdCash.getBilUnIdCashId().getBilDtbSeqNbr());
        suspendedPayment.setPostedDate(bilUnIdCash.getBilUnIdCashId().getBilEntryDt());
        suspendedPayment.setBatchId(bilUnIdCash.getBilUnIdCashId().getBilEntryNbr().trim());
        suspendedPayment.setUserId(
                appSecurityServiceImpl.getUserIdByUserSeqId(bilUnIdCash.getBilUnIdCashId().getUserId().trim()).trim());
        suspendedPayment.setUserName(appSecurityServiceImpl
                .getUserNameByUserSeqId(bilUnIdCash.getBilUnIdCashId().getUserId().trim()).trim());
        suspendedPayment.setPaymentAmount(bilUnIdCash.getBilRctAmt());
        if (!bilUnIdCash.getBilPayeeCltId().trim().isEmpty()) {
            Party payor = getPayorName(bilUnIdCash.getBilPayeeCltId().trim());
            if (payor != null) {
                suspendedPayment.setPayorName(payor.getName());
            }
        }
        suspendedPayment.setPaymentType(
                getPaymentTypeDescription(bilUnIdCash.getBilRctTypeCd(), bilUnIdCash.getBilCshEtrMthCd()));

        if (!bilUnIdCash.getBilAccountNbr().trim().isEmpty()) {
            suspendedPayment.setIdentifier(bilUnIdCash.getBilAccountNbr().trim());
            String identifierType = findDescriptionByCodeAndType(AccountType.BILLACCOUNT.toString(),
                    BusCodeTranslationType.BILLACCOUNTTYPE.getValue());
            if (!identifierType.isEmpty()) {
                suspendedPayment.setIdentifierType(identifierType);
            } else {
                throw new InvalidDataException(SUPPORT_DATA_NOT_FOUND,
                        new Object[] { BusCodeTranslationType.BILLACCOUNTTYPE.getValue() });
            }
        } else if (!bilUnIdCash.getBilTtyNbr().trim().isEmpty()) {
            suspendedPayment.setIdentifier(bilUnIdCash.getBilTtyNbr().trim());
            String identifierType = findDescriptionByCodeAndType(AccountType.GROUP_ACCOUNT.toString(),
                    BusCodeTranslationType.BILLACCOUNTTYPE.getValue());
            if (!identifierType.isEmpty()) {
                suspendedPayment.setIdentifierType(identifierType);
            } else {
                throw new InvalidDataException(SUPPORT_DATA_NOT_FOUND,
                        new Object[] { BusCodeTranslationType.BILLACCOUNTTYPE.getValue() });
            }
        } else if (!bilUnIdCash.getBilAgtActNbr().trim().isEmpty()) {
            suspendedPayment.setIdentifier(bilUnIdCash.getBilAgtActNbr().trim());
            String identifierType = findDescriptionByCodeAndType(AccountType.AGENCY_ACCOUNT.toString(),
                    BusCodeTranslationType.BILLACCOUNTTYPE.getValue());
            if (!identifierType.isEmpty()) {
                suspendedPayment.setIdentifierType(identifierType);
            } else {
                throw new InvalidDataException(SUPPORT_DATA_NOT_FOUND,
                        new Object[] { BusCodeTranslationType.BILLACCOUNTTYPE.getValue() });
            }
        } else if (!bilUnIdCash.getPolNbr().trim().isEmpty()) {
            suspendedPayment.setIdentifier(
                    bilUnIdCash.getPolSymbolCd().trim().concat(SEPARATOR_BLANK).concat(bilUnIdCash.getPolNbr().trim()));
            suspendedPayment.setIdentifierType(POLICY_IDENTIFIER);
        }

        suspendedPayment.setStatus(getStatus(bilUnIdCash.getBilDspTypeCd().trim(), bilUnIdCash.getBilCshEtrMthCd(),
                bilUnIdCash.getBillUnidCashId(), isDigitalPayment));
        suspendedPayment
                .setPaymentMethod(getPaymentMethodDescription(Character.toString(bilUnIdCash.getBilCshEtrMthCd())));
        suspendedPayment.setPaymentId(bilUnIdCash.getBilRctId().trim());

        ZonedDateTime entryDate = bilUnIdCash.getBilUnIdCashId().getBilEntryDt();
        String userId = bilUnIdCash.getBilUnIdCashId().getUserId();
        String batchId = bilUnIdCash.getBilUnIdCashId().getBilEntryNbr();

        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(entryDate, batchId, userId)).orElse(null);
        if (bilCashEntryTot != null) {
            suspendedPayment.setCurrency(multiCurrencyService.getCurrencyCode(bilCashEntryTot.getCurrencyCd()));
        }
        return suspendedPayment;
    }

    private String getPaymentMethodDescription(String cashEntryMethodCode) {
        List<BusCdTranslation> busCodeTranslationList = busCdTranslationRepository.findAllByCodeAndTypeList(
                cashEntryMethodCode,
                Arrays.asList(BusCodeTranslationType.PAYMENTMETHOD.getValue(), BilDesReasonType.PYS.getValue()),
                execContext.getLanguage());
        if (busCodeTranslationList != null && !busCodeTranslationList.isEmpty()) {
            return busCodeTranslationList.get(SHORT_ZERO).getBusDescription().trim();
        }
        return cashEntryMethodCode;
    }

    @Override
    public List<String> getPaymentTypeList(short sequenceNumber, String postedDate, String userId, String batchId,
            String distributionDate) {
        char paymentMethodCode = BLANK_CHAR;
        boolean isDigitalPayment = false;
        String userSequenceId = appSecurityServiceImpl.authenticateUser(userId).getUserSequenceId();
        logger.info("fetch unid payment from BIL_UNID_CASH.");
        BilUnIdCash bilUnidentifiedCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(DateRoutine.dateTimeAsYYYYMMDD(distributionDate), sequenceNumber, batchId,
                        userSequenceId, DateRoutine.dateTimeAsYYYYMMDD(postedDate)))
                .orElse(null);
        if (bilUnidentifiedCash != null) {
            paymentMethodCode = bilUnidentifiedCash.getBilCshEtrMthCd();
        }
        List<String> busTypeCodeList = new ArrayList<>();
        switch (CashEntryMethod.getEnumValue(paymentMethodCode)) {
        case CREDIT_CARD:
            busTypeCodeList.add(BusCodeTranslationType.CREDIT_CARD_PAYMENT.getValue());
            break;
        case AGENT_CREDIT_CARD:
            busTypeCodeList.add(BilDesReasonType.GPY.getValue());
            break;
        case EFT:
        case EFT_CREDIT_CARD:
            busTypeCodeList.add(BLANK_STRING);
            break;
        case ONE_TIME_ACH:
            busTypeCodeList.add(BilDesReasonType.ACH.getValue());
            break;
        case BANK_ACCOUNT:
            busTypeCodeList.add(BusCodeTranslationType.BANK_ACCOUNT_PAYMENT.getValue());
            break;
        case AGENCY_SWEEP:
            busTypeCodeList.add(BilDesReasonType.ASW.getValue());
            break;
        case VENDOR_COLLECTIONS:
            busTypeCodeList.add(BilDesReasonType.COL.getValue());
            break;
        default:
            if (isDigitalWalletPayment(paymentMethodCode)) {
                busTypeCodeList.add(BilDesReasonType.WALLET_PAYMENT_TYPE.getValue());
                isDigitalPayment = true;
            } else {
                busTypeCodeList.add(BilDesReasonType.PYT.getValue());
                busTypeCodeList.add(BilDesReasonType.PYC.getValue());
                busTypeCodeList.add(BilDesReasonType.COL.getValue());
            }

            break;
        }
        if (isDigitalPayment) {
            return busCdTranslationRepository.findDescriptionListByTypeListAndParentCode(busTypeCodeList,
                    execContext.getLanguage(), String.valueOf(paymentMethodCode));
        }
        return busCdTranslationRepository.findDescriptionListByTypeList(busTypeCodeList, execContext.getLanguage());
    }

    private List<String> getStatusDesctiptionList() {
        List<String> statusList = new ArrayList<>();
        statusList.add(Status.SUSPENDED.getValue());
        statusList.add(Status.DISBURSED.getValue());
        statusList.add(Status.WRITE_OFF.getValue());
        statusList.add(Status.REVERSED.getValue());
        statusList = busCdTranslationRepository.findCodeListByDescriptionListAndType(statusList,
                BilDesReasonType.DSP.getValue(), execContext.getLanguage());
        return statusList;
    }

    private String getIdentifierTypeCode(String accountType) {
        List<BusCdTranslation> busCodeTranslationList = busCdTranslationRepository.findAllByDescriptionAndTypeList(
                accountType, Arrays.asList(BusCodeTranslationType.BILLACCOUNTTYPE.getValue(),
                        BusCodeTranslationType.ADDITIONALID.getValue()),
                execContext.getLanguage());
        if (busCodeTranslationList != null && !busCodeTranslationList.isEmpty()) {
            return busCodeTranslationList.get(SHORT_ZERO).getBusCdTranslationId().getBusCd().trim();
        }
        return BLANK_STRING;
    }

    private boolean isDigitalWalletPayment(Character cashEntryMethodCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(
                String.valueOf(cashEntryMethodCode), BusCodeTranslationType.DIGITAL_WALLET_TYPE.getValue(),
                execContext.getLanguage());

        return busCdTranslation != null ? Boolean.TRUE : Boolean.FALSE;
    }
}
