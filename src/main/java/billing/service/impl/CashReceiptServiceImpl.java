package billing.service.impl;

import static billing.utils.BillingConstants.COMPANY_LOCATION_NUMBER;
import static billing.utils.BillingConstants.ERROR_CODE;
import static billing.utils.BillingConstants.MASTER_COMPANY_NUMBER;
import static billing.utils.BillingConstants.PAYMENT_REVERSED;
import static billing.utils.BillingConstants.PLUS_SIGN;
import static billing.utils.BillingConstants.TECH_KEY_TYPE_CODE;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_G;
import static core.utils.CommonConstants.CHAR_L;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_U;
import static core.utils.CommonConstants.CHAR_W;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_Z;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilAgent;
import billing.data.entity.BilAgtCashDsp;
import billing.data.entity.BilAgtCashRct;
import billing.data.entity.BilAgtSummary;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilInvTtyAct;
import billing.data.entity.BilObjectCfg;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSchAutoReconCsh;
import billing.data.entity.BilThirdParty;
import billing.data.entity.BilTtyCashDsp;
import billing.data.entity.BilTtyCashRct;
import billing.data.entity.BilTtySummary;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilAgtCashDspId;
import billing.data.entity.id.BilAgtCashRctId;
import billing.data.entity.id.BilAgtSummaryId;
import billing.data.entity.id.BilCashDspId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.entity.id.BilObjectCfgId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.entity.id.BilSchAutoReconCshId;
import billing.data.entity.id.BilTtyCashDspId;
import billing.data.entity.id.BilTtyCashRctId;
import billing.data.entity.id.BilTtySummaryId;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilAgentRepository;
import billing.data.repository.BilAgtCashDspRepository;
import billing.data.repository.BilAgtCashRctRepository;
import billing.data.repository.BilAgtSummaryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilInvTtyActRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilObjectCfgRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilSchAutoReconCshRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.data.repository.BilTtyCashDspRepository;
import billing.data.repository.BilTtyCashRctRepository;
import billing.data.repository.BilTtySummaryRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.model.AddUpdateBankInformation;
import billing.model.CashApplyDriver;
import billing.service.AddUpdateBankInformationService;
import billing.service.BilRulesUctService;
import billing.service.CashApplyDriverService;
import billing.service.CashApplyUserExitService;
import billing.service.CashReceiptService;
import billing.service.ReplacementPaymentService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilCashEntryMethodCode;
import billing.utils.BillingConstants.BilDesReasonCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CashEntryMethod;
import core.datetime.service.DateService;
import core.exception.validation.InvalidDataException;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;

@Service
public class CashReceiptServiceImpl implements CashReceiptService {

    private static final Logger logger = LoggerFactory.getLogger(CashReceiptServiceImpl.class);

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    private BilAgentRepository bilAgentRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilTtyCashRctRepository bilTtyCashRctRepository;

    @Autowired
    private BilAgtCashRctRepository bilAgtCashRctRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private CashApplyDriverService cashApplyDriverService;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private BilTtySummaryRepository bilTtySummaryRepository;

    @Autowired
    private BilTtyCashDspRepository bilTtyCashDspRepository;

    @Autowired
    private BilSchAutoReconCshRepository bilSchAutoReconCshRepository;

    @Autowired
    private BilAgtCashDspRepository bilAgtCashDspRepository;

    @Autowired
    private BilAgtSummaryRepository bilAgtSummaryRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilObjectCfgRepository bilObjectCfgRepository;

    @Autowired
    private ReplacementPaymentService replacementPaymentService;

    @Autowired
    private BilInvTtyActRepository bilInvTtyActRepository;

    @Autowired
    private AddUpdateBankInformationService addUpdateBankInformationService;

    @Autowired
    private CashApplyUserExitService cashApplyUserExitService;

    @Autowired
    private DateService dateService;
    
    @Value("${language}")
    private String language;

    private static final String BCMOCR = "BCMOCR";
    private static final String BCMS = "BCMS";
    private static final String SPAR = "SPAR";
    private static final String BCMOCSH3 = "BCMOCSH3";
    private static final String NBUB = "NBUB";
    private static final String POTD = "POTD";
    private static final String DEFAULT_DATE = "9999-12-31";

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void processBatchSystem(String batchId, String postedDate, String userId) {

        BilCashEntryTot bilCashEntryTotRow = bilCashEntryTotRepository
                .findBilCashEntryTotal(DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId, userId, CHAR_D);
        logger.error("bil cash entry tot found row : {}, {}, {} ", batchId, postedDate, userId);
        Set<String> billAccountSet = new HashSet<>();
        if (bilCashEntryTotRow != null) {
            List<BilCashEntry> bilCashEntryList = bilCashEntryRepository
                    .findBilCashEntry(DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId, userId);
            if (bilCashEntryList != null && !bilCashEntryList.isEmpty()) {
                for (BilCashEntry bilCashEntry : bilCashEntryList) {
                    validateAndProcessBilCashEntry(bilCashEntry, bilCashEntryTotRow, billAccountSet);
                }
            }
            try {
                billAccountSet.forEach(accountId -> callCashApplyDriver(accountId, dateService.currentDate(), userId));
            }catch (Exception e) {
                throw new RuntimeException(e);
            }
            
            bilCashEntryTotRepository.updateRow(bilCashEntryTotRow.getBilCashEntryTotId().getBilEntryDt(),
                    bilCashEntryTotRow.getBilCashEntryTotId().getBilEntryNbr(),
                    bilCashEntryTotRow.getBilCashEntryTotId().getUserId());
        }
    }

    private void validateAndProcessBilCashEntry(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow, Set<String> billAccountSet) {
        
        if (StringUtils.isNotBlank(bilCashEntry.getBilAccountNbr())) {
        	processDirectBillAccount(bilCashEntry, bilCashEntryTotRow, billAccountSet);
            
        } else if (StringUtils.isNotBlank(bilCashEntry.getBilTtyNbr())) {
        	processThirdParty(bilCashEntry, bilCashEntryTotRow);
            
        } else if (StringUtils.isNotBlank(bilCashEntry.getPolNbr())) {
            processPolicy(bilCashEntry, bilCashEntryTotRow, billAccountSet);
        	
        } else if (StringUtils.isNotBlank(bilCashEntry.getBilAgtActNbr())) {
            processAgent(bilCashEntry, bilCashEntryTotRow);
        } else {
            unIdentifiedCash(bilCashEntry, bilCashEntryTotRow);
        }
    }

    private void processPolicy(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow,
			Set<String> billAccountSet) {
    	BilPolicyTerm bilPolicyTerm = getPolicyBillAccount(bilCashEntry);
        if (bilPolicyTerm != null) {
            BilAccount bilAccount = bilAccountRepository.findById(bilPolicyTerm.getBillPolicyTermId().getBilAccountId()).orElse(null);
            if (bilAccount == null) {
                unIdentifiedCash(bilCashEntry, bilCashEntryTotRow);
                return;
            }
            processBillAccount(bilCashEntry, bilCashEntryTotRow, bilPolicyTerm, bilAccount, billAccountSet);
        } else {
            unIdentifiedCash(bilCashEntry, bilCashEntryTotRow);
        }
		
	}

	private void processAgent(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow) {
    	BilAgent bilAgent = bilAgentRepository.findByBilAgtActNbr(bilCashEntry.getBilAgtActNbr());
        if (bilAgent == null) {
            unIdentifiedCash(bilCashEntry, bilCashEntryTotRow);
            return;
        }
        processBilAgent(bilCashEntry, bilCashEntryTotRow, bilAgent);
		
	}

	private void processThirdParty(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow) {
    	BilThirdParty bilThirdParty = bilThirdPartyRepository.findByBilTtyNbr(bilCashEntry.getBilTtyNbr());
        if (bilThirdParty == null) {
            unIdentifiedCash(bilCashEntry, bilCashEntryTotRow);
        } else {
            if (StringUtils.isNotBlank(bilCashEntry.getBilAdditionalId())) {
                BilAccount bilAccount = bilAccountRepository.findBilAccountByAdditionalIdAndTtyNumber(
                        bilThirdParty.getBillThirdPartyId(), bilCashEntry.getBilAdditionalId());
                if (bilAccount == null) {
                    unIdentifiedCash(bilCashEntry, bilCashEntryTotRow);
                    return;
                }
            }
            processBilThirdParty(bilCashEntry, bilCashEntryTotRow, bilThirdParty);
        }
		
	}

	private void processDirectBillAccount(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow, Set<String> billAccountSet) {
		BilPolicyTerm bilPolicyTerm = null;
		BilAccount bilAccount = bilAccountRepository.findByAccountNumber(bilCashEntry.getBilAccountNbr());
        if (bilAccount == null) {
            unIdentifiedCash(bilCashEntry, bilCashEntryTotRow);
        } else {
        	if (StringUtils.isNotBlank(bilCashEntry.getPolNbr())) {
                bilPolicyTerm = getPolicyInformation(bilCashEntry, bilAccount.getAccountId());
            }
        	if (bilPolicyTerm == null) {
                bilPolicyTerm = new BilPolicyTerm();
                BilPolicyTermId bilPolicyTermId = new BilPolicyTermId();
                bilPolicyTermId.setPolicyId(BLANK_STRING);
                bilPolicyTerm.setBillPolicyTermId(bilPolicyTermId);
                bilPolicyTerm.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
                bilPolicyTerm.setBillStatePvnCd(BLANK_STRING);
            }
        	
            processBillAccount(bilCashEntry, bilCashEntryTotRow, bilPolicyTerm, bilAccount, billAccountSet);
        }
		
	}

	private void processBilAgent(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow, BilAgent bilAgent) {

        BilAgtCashRct agtCashRct = bilAgtCashRctRepository.findBilAgtCashReceipt(
                bilCashEntry.getBilCashEntryId().getBilEntryDt(), bilCashEntry.getBilCashEntryId().getBilEntryNbr(),
                bilCashEntry.getBilCashEntryId().getUserId(), bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
        if (agtCashRct == null) {
            BilAgtCashRct bilAgtCashRct = writeBilAgtCashRct(bilCashEntry, bilCashEntryTotRow, bilAgent);
            if (!bilCashEntry.getBilAdjDueDt().isEqual(DateRoutine.defaultDateTime())
                    && bilCashEntry.getBilManualSusInd() != CHAR_Y) {
                BilInvTtyAct reversedInvoiceActivity = bilInvTtyActRepository.findByTechnicalKeyAndDueDate(
                        bilAgent.getBilAgtActId(), TECH_KEY_TYPE_CODE, bilCashEntry.getBilAdjDueDt(), PAYMENT_REVERSED);
                if (reversedInvoiceActivity != null) {
                    replacementPaymentService.reapplyPaymentForAgent(bilAgent.getBilAgtActId(),
                            bilCashEntry.getBilAdjDueDt(), BigDecimal.valueOf(bilAgtCashRct.getBilRctAmt()),
                            bilCashEntry.getBilSusReasonCd(), bilAgent.getBilAgtActNbr(), reversedInvoiceActivity,
                            bilAgtCashRct.getBilEntryDt(), bilAgtCashRct.getBilEntryNbr(),
                            bilAgtCashRct.getBilEntrySeqNbr(), bilAgtCashRct.getBilAgtCashRctId().getBilDtbDt(),
                            bilAgtCashRct.getBilAgtCashRctId().getBilDtbSeqNbr(), bilAgtCashRct.getUserId(),
                            bilAgtCashRct.getBilCshEtrMthCd(), bilAgtCashRct.getBilBankCd(), SHORT_ZERO,
                            AccountType.AGENCY_ACCOUNT.toString(), bilAgent);
                }
            }
            writeBilAgtCashDsp(bilCashEntry, bilAgent, bilAgtCashRct.getBilAgtCashRctId().getBilDtbSeqNbr());
            writeBilAgtSummary(bilCashEntry, bilCashEntryTotRow, bilAgent);
            writeFinancilaApiForAgent(bilCashEntry, bilCashEntryTotRow, bilAgent);
        }
    }

    private void writeFinancilaApiForAgent(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow,
            BilAgent bilAgent) {

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();

        financialApiActivity.setFunctionCode(CHAR_A);
        financialApiActivity.setReferenceId(BLANK_STRING);
        financialApiActivity.setApplicationName(BCMOCR);
        financialApiActivity.setUserId(BLANK_STRING);
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setTransactionObjectCode(BilCashEntryMethodCode.CASH.getValue());
        financialApiActivity.setTransactionActionCode(BilCashEntryMethodCode.SUSPENDED.getValue());
        financialApiActivity.setApplicationProductIdentifier(BCMS);
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(CHAR_Y);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        financialApiActivity.setOperatorId(BLANK_STRING);
        financialApiActivity.setAppProgramId(BCMOCR);
        financialApiActivity.setActivityAmount(bilCashEntry.getBilRctAmt());
        financialApiActivity.setActivityNetAmount(bilCashEntry.getBilRctAmt());
        financialApiActivity.setBilBankCode(bilCashEntryTotRow.getBilBankCd());
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        financialApiActivity.setBilDatabaseKey(getDatabaseKey(bilCashEntry.getBilCashEntryId().getBilEntryDt(),
                bilCashEntry.getBilCashEntryId().getBilEntryNbr(), bilCashEntry.getBilCashEntryId().getUserId(),
                bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr()));
        financialApiActivity.setOriginalEffectiveDate(BLANK_STRING);
        financialApiActivity.setBusinessCaseId(BLANK_STRING);
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setAgentTtyId(bilCashEntry.getBilAgtActNbr());
        financialApiActivity.setBilAccountId(BLANK_STRING);
        financialApiActivity.setBilReceiptTypeCode(bilCashEntry.getBilRctTypeCd());
        financialApiActivity.setBilReasonCode(BLANK_STRING);
        financialApiActivity.setClientId(BLANK_STRING);
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setCurrencyCode(bilAgent.getCurrencyCd());
        financialApiActivity.setDepositeBankCode(bilCashEntryTotRow.getBctDepBankCd());
        financialApiActivity.setDisbursementId(BLANK_STRING);
        financialApiActivity.setLineOfBusCode(BLANK_STRING);
        financialApiActivity.setMarketSectorCode(BLANK_STRING);
        financialApiActivity.setPayableItemCode(BLANK_STRING);
        financialApiActivity.setPolicyId(BLANK_STRING);
        financialApiActivity.setStateCode(BLANK_STRING);
        financialApiActivity.setStatusCode(BLANK_CHAR);
        financialApiActivity.setBilSourceCode(String.valueOf(bilCashEntryTotRow.getBilCshEtrMthCd()));
        financialApiActivity.setCountryCode(BLANK_STRING);
        financialApiActivity.setBilTypeCode(bilAgent.getBilTypeCd());
        financialApiActivity.setBilClassCode(bilAgent.getBilClassCd());

        insertBcmActivity(financialApiActivity);

    }

    private void writeBilAgtSummary(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow, BilAgent bilAgent) {
        BilAgtSummary bilAgtSummary = new BilAgtSummary();
        bilAgtSummary.setBilAgtSummaryId(new BilAgtSummaryId(bilAgent.getBilAgtActId(),
                dateService.currentDate(), getMaxSequenceNumber(bilAgtSummaryRepository
                        .getMaxSequenceNumber(bilAgent.getBilAgtActId(), dateService.currentDate()))));
        bilAgtSummary.setUserId(bilCashEntry.getBilCashEntryId().getUserId());
        bilAgtSummary.setBilAcyAmt(bilCashEntry.getBilRctAmt());
        bilAgtSummary.setBilAcyDesCd(bilCashEntry.getBilRctTypeCd());
        if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()) {
            bilAgtSummary.setBilDesReaTyp(BilDesReasonType.CPY.getValue());
        } else if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.EFT.getCashEntryMethod()
                || bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.EFT_CREDIT_CARD.getCashEntryMethod()) {
            bilAgtSummary.setBilDesReaTyp(BLANK_STRING);
        } else if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.AGENT_CREDIT_CARD.getCashEntryMethod()) {
            bilAgtSummary.setBilDesReaTyp(BilDesReasonType.GPY.getValue());
        } else if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.ONE_TIME_ACH.getCashEntryMethod()) {
            bilAgtSummary.setBilDesReaTyp(BilDesReasonType.ACH.getValue());
        } else if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.VENDOR_COLLECTIONS.getCashEntryMethod()) {
            bilAgtSummary.setBilDesReaTyp(BilDesReasonType.COL.getValue());
        } else {
            bilAgtSummary.setBilDesReaTyp(busCdTranslationRepository
                    .findAllByCodeAndTypeList(bilCashEntry.getBilRctTypeCd(),
                            Arrays.asList(BilDesReasonType.PYT.getValue(), BilDesReasonType.PYC.getValue(),
                                    BilDesReasonType.COL.getValue()),
                            language)
                    .get(0).getBusCdTranslationId().getBusType());
        }
        bilAgtSummary.setBilAcyDes1Dt(bilCashEntryTotRow.getBilDepositDt());
        bilAgtSummary.setBilAcyDes2Dt(DateRoutine.defaultDateTime());
        bilAgtSummary.setBilAcyTs(dateService.currentDateTime());
        bilAgtSummaryRepository.saveAndFlush(bilAgtSummary);

    }

    private void writeBilAgtCashDsp(BilCashEntry bilCashEntry, BilAgent bilAgent, short bilDtbSeqNbr) {

        BilAgtCashDsp bilAgtCashDsp = new BilAgtCashDsp();
        bilAgtCashDsp.setBilAgtCashDspId(
                new BilAgtCashDspId(bilAgent.getBilAgtActId(), dateService.currentDate(), bilDtbSeqNbr,
                        getIncrementalValue(bilAgtCashDspRepository.findMaxDispositionNumber(bilAgent.getBilAgtActId(),
                                dateService.currentDate(), bilDtbSeqNbr))));
        bilAgtCashDsp.setBilAdjDueDt(bilCashEntry.getBilAdjDueDt());
        bilAgtCashDsp.setUserId(bilCashEntry.getBilCashEntryId().getUserId());
        bilAgtCashDsp.setBilDspDt(dateService.currentDate());
        bilAgtCashDsp.setBilDspTypeCd(BilDspTypeCode.SUSPENDED.getValue());
        bilAgtCashDsp.setBilDspReasonCd(bilCashEntry.getBilSusReasonCd());
        bilAgtCashDsp.setBilDspAmt(bilCashEntry.getBilRctAmt());
        bilAgtCashDsp.setBilPayeeCltId(BLANK_STRING);
        bilAgtCashDsp.setBilPayeeAdrSeq(SHORT_ZERO);
        bilAgtCashDsp.setBilManualSusInd(bilCashEntry.getBilManualSusInd());
        bilAgtCashDsp.setDwsStatusCd(BLANK_CHAR);
        bilAgtCashDsp.setBilDsbId(BLANK_STRING);
        bilAgtCashDsp.setBilChkPrdMthCd(BLANK_CHAR);
        bilAgtCashDsp.setBilDsbDt(DateRoutine.defaultDateTime());
        bilAgtCashDsp.setBilToFroTrfNbr(BLANK_STRING);
        bilAgtCashDsp.setBilTrfTypeCd(BLANK_CHAR);
        bilAgtCashDspRepository.saveAndFlush(bilAgtCashDsp);

        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct(SPAR, bilAgent.getBilTypeCd(),
                bilAgent.getBilClassCd(), BLANK_STRING);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            writeBilSchAutoReconCsh(bilDtbSeqNbr, bilAgent.getBilAgtActId(), CHAR_G, bilCashEntry);
        }
    }

    private Short getIncrementalValue(Short sequenceNumber) {
        if (sequenceNumber == null) {
            return SHORT_ZERO;
        } else {
            return (short) (sequenceNumber + 1);
        }
    }

    private BilAgtCashRct writeBilAgtCashRct(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow,
            BilAgent bilAgent) {

        BilAgtCashRct bilAgtCashRct = new BilAgtCashRct();
        bilAgtCashRct.setBilAgtCashRctId(new BilAgtCashRctId(bilAgent.getBilAgtActId(),
                dateService.currentDate(), getMaxSqeNumber(bilAgtCashRctRepository
                        .getMaxBilDtbSequenceNumber(bilAgent.getBilAgtActId(), dateService.currentDate()))));
        bilAgtCashRct.setBilPstmrkCd(bilCashEntry.getBilPstMrkCd());
        bilAgtCashRct.setBilEntryDt(bilCashEntry.getBilCashEntryId().getBilEntryDt());
        bilAgtCashRct.setBilEntryNbr(bilCashEntry.getBilCashEntryId().getBilEntryNbr());
        bilAgtCashRct.setBilEntrySeqNbr(bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
        bilAgtCashRct.setUserId(bilCashEntry.getBilCashEntryId().getUserId());
        bilAgtCashRct.setBilRctAmt(bilCashEntry.getBilRctAmt());
        bilAgtCashRct.setBilRctTypeCd(bilCashEntry.getBilRctTypeCd());
        bilAgtCashRct.setBilRctId(bilCashEntry.getBilRctId());
        bilAgtCashRct.setBilRctCmt(bilCashEntry.getBilRctCmt());
        bilAgtCashRct.setBilTrfTypeCd(BLANK_CHAR);
        bilAgtCashRct.setBilRctReceive(bilCashEntry.getBilRctReceiveDt());
        bilAgtCashRct.setBilBankCd(bilCashEntryTotRow.getBilBankCd());
        bilAgtCashRct.setBilCrcrdTypeCd(bilCashEntry.getBilCrcrdTypeCd());
        bilAgtCashRct.setBilCrcrdActNbr(bilCashEntry.getBilCrcrdActNbr());
        bilAgtCashRct.setBilCrcrdExpDt(bilCashEntry.getBilCrcrdExpDt());
        bilAgtCashRct.setBilCrcrdAutApv(bilCashEntry.getBilCrcrdAutApv());
        bilAgtCashRct.setBilCshEtrMthCd(bilCashEntryTotRow.getBilCshEtrMthCd());
        bilAgtCashRct.setBilTaxYearNbr(bilCashEntry.getBilTaxYearNbr());
        bilAgtCashRct.setBilPstmrkDt(bilCashEntry.getBilPstMrkDt());
        bilAgtCashRct.setBilDepositDt(bilCashEntryTotRow.getBilDepositDt());
        bilAgtCashRct.setBilRctCmt(bilCashEntry.getBilRctCmt());
        bilAgtCashRct.setBilToFroTrfNbr(BLANK_STRING);
        bilAgtCashRctRepository.saveAndFlush(bilAgtCashRct);
        return bilAgtCashRct;

    }

    private Short getMaxSqeNumber(Short sequenceNumber) {
        if (sequenceNumber == null) {
            return SHORT_ZERO;
        } else {
            return (short) (sequenceNumber + 1);
        }
    }

    private void processBilThirdParty(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow,
            BilThirdParty bilThirdParty) {
        BilTtyCashRct ttyCashRct = bilTtyCashRctRepository.findBilTtyCashReceipt(
                bilCashEntry.getBilCashEntryId().getBilEntryDt(), bilCashEntry.getBilCashEntryId().getBilEntryNbr(),
                bilCashEntry.getBilCashEntryId().getUserId(), bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
        if (ttyCashRct == null) {
            BilTtyCashRct bilTtyCashRct = writeBilTtyCashReceipt(bilCashEntry, bilCashEntryTotRow, bilThirdParty);
            writeBilTtySummary(bilCashEntry, bilCashEntryTotRow, bilThirdParty);
            if (!bilCashEntry.getBilAdjDueDt().isEqual(DateRoutine.defaultDateTime())
                    && bilCashEntry.getBilManualSusInd() != CHAR_Y) {
                BilInvTtyAct bilInvoiceThirdPartyAccount = bilInvTtyActRepository.getRow(
                        bilThirdParty.getBillThirdPartyId(), CHAR_T, bilCashEntry.getBilAdjDueDt(),
                        BillingConstants.PAYMENT_REVERSED);
                if (bilInvoiceThirdPartyAccount != null) {
                    replacementPaymentService.reapplyPaymentForThirdParty(bilThirdParty.getBillThirdPartyId(),
                            bilCashEntry.getBilAdjDueDt(), BigDecimal.valueOf(bilTtyCashRct.getBilRctAmt()),
                            bilCashEntry.getBilSusReasonCd(), bilThirdParty.getBilTtyNbr(), bilInvoiceThirdPartyAccount,
                            bilCashEntry.getBilCashEntryId().getBilEntryDt(),
                            bilCashEntry.getBilCashEntryId().getBilEntryNbr(),
                            bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr(),
                            bilTtyCashRct.getBilTtyCashRctId().getBilDtbDt(),
                            bilTtyCashRct.getBilTtyCashRctId().getBilDtbSeqNbr(),
                            bilCashEntry.getBilCashEntryId().getUserId(), bilCashEntryTotRow.getBilCshEtrMthCd(),
                            bilCashEntryTotRow.getBilBankCd(), SHORT_ZERO, AccountType.GROUP_ACCOUNT.toString());
                }
            }

            writeBilThirdPartyDisposition(bilCashEntry, bilThirdParty,
                    bilTtyCashRct.getBilTtyCashRctId().getBilDtbSeqNbr());
            writeFinancilaApiForThirdParty(bilCashEntry, bilCashEntryTotRow, bilThirdParty);
        }
    }

    private void writeFinancilaApiForThirdParty(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow,
            BilThirdParty bilThirdParty) {
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();

        financialApiActivity.setFunctionCode(CHAR_A);
        financialApiActivity.setReferenceId(BLANK_STRING);
        financialApiActivity.setApplicationName(BCMOCR);
        financialApiActivity.setUserId(BLANK_STRING);
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setTransactionObjectCode(BilCashEntryMethodCode.CASH.getValue());
        financialApiActivity.setTransactionActionCode(BilCashEntryMethodCode.SUSPENDED.getValue());
        financialApiActivity.setApplicationProductIdentifier(BCMS);
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(CHAR_Y);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        financialApiActivity.setOperatorId(BLANK_STRING);
        financialApiActivity.setAppProgramId(BCMOCR);
        financialApiActivity.setActivityAmount(bilCashEntry.getBilRctAmt());
        financialApiActivity.setActivityNetAmount(bilCashEntry.getBilRctAmt());
        financialApiActivity.setBilBankCode(bilCashEntryTotRow.getBilBankCd());
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        financialApiActivity.setBilDatabaseKey(getDatabaseKey(bilCashEntry.getBilCashEntryId().getBilEntryDt(),
                bilCashEntry.getBilCashEntryId().getBilEntryNbr(), bilCashEntry.getBilCashEntryId().getUserId(),
                bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr()));
        financialApiActivity.setOriginalEffectiveDate(BLANK_STRING);
        financialApiActivity.setBusinessCaseId(BLANK_STRING);
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setAgentTtyId(bilCashEntry.getBilTtyNbr());
        financialApiActivity.setBilAccountId(BLANK_STRING);
        financialApiActivity.setBilReceiptTypeCode(bilCashEntry.getBilRctTypeCd());
        financialApiActivity.setBilReasonCode(BLANK_STRING);
        financialApiActivity.setClientId(BLANK_STRING);
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setCurrencyCode(bilThirdParty.getCurrencyCd());
        financialApiActivity.setDepositeBankCode(bilCashEntryTotRow.getBctDepBankCd());
        financialApiActivity.setDisbursementId(BLANK_STRING);
        financialApiActivity.setLineOfBusCode(BLANK_STRING);
        financialApiActivity.setMarketSectorCode(BLANK_STRING);
        financialApiActivity.setPayableItemCode(BLANK_STRING);
        financialApiActivity.setPolicyId(BLANK_STRING);
        financialApiActivity.setStateCode(BLANK_STRING);
        financialApiActivity.setStatusCode(BLANK_CHAR);
        financialApiActivity.setBilSourceCode(String.valueOf(bilCashEntryTotRow.getBilCshEtrMthCd()));
        financialApiActivity.setCountryCode(BLANK_STRING);
        financialApiActivity.setBilTypeCode(bilThirdParty.getBilTypeCd());
        financialApiActivity.setBilClassCode(bilThirdParty.getBilClassCd());

        insertBcmActivity(financialApiActivity);

    }

    private void writeBilThirdPartyDisposition(BilCashEntry bilCashEntry, BilThirdParty bilThirdParty,
            short bilDtbSeqNbr) {

        BilTtyCashDsp bilTtyCashDsp = new BilTtyCashDsp();
        bilTtyCashDsp.setBilTtyCashDspId(new BilTtyCashDspId(bilThirdParty.getBillThirdPartyId(),
                dateService.currentDate(), bilDtbSeqNbr, SHORT_ZERO));
        bilTtyCashDsp.setBilAdjDueDt(bilCashEntry.getBilAdjDueDt());
        bilTtyCashDsp.setUserId(bilCashEntry.getBilCashEntryId().getUserId());
        bilTtyCashDsp.setBilDspDt(dateService.currentDate());
        bilTtyCashDsp.setBilDspTypeCd(BilDspTypeCode.SUSPENDED.getValue());
        bilTtyCashDsp.setBilDspReasonCd(bilCashEntry.getBilSusReasonCd());
        bilTtyCashDsp.setBilDspAmt(bilCashEntry.getBilRctAmt());
        bilTtyCashDsp.setBilPayeeCltId(SEPARATOR_BLANK);
        bilTtyCashDsp.setBilPayeeAdrSeq(SHORT_ZERO);
        bilTtyCashDsp.setBilManualSusInd(bilCashEntry.getBilManualSusInd());
        bilTtyCashDsp.setDwsCkDrfNbr(SHORT_ZERO);
        bilTtyCashDsp.setBilRevsRsusInd(BLANK_CHAR);
        bilTtyCashDsp.setBilToFroTrfNbr(SEPARATOR_BLANK);
        bilTtyCashDsp.setBilTrfTypeCd(BLANK_CHAR);
        bilTtyCashDsp.setDwsStatusCd(BLANK_CHAR);
        bilTtyCashDsp.setBilDsbId(SEPARATOR_BLANK);
        bilTtyCashDsp.setBilChkPrdMthCd(BLANK_CHAR);
        bilTtyCashDsp.setDdsDsbDt(DateRoutine.defaultDateTime());
        bilTtyCashDspRepository.saveAndFlush(bilTtyCashDsp);

        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct(SPAR, bilThirdParty.getBilTypeCd(),
                bilThirdParty.getBilClassCd(), BLANK_STRING);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            writeBilSchAutoReconCsh(bilDtbSeqNbr, bilThirdParty.getBillThirdPartyId(), CHAR_T, bilCashEntry);
        }
    }

    private void writeBilSchAutoReconCsh(short bilDtbSeqNbr, String accountId, char techTypeCode,
            BilCashEntry bilCashEntry) {

        BilSchAutoReconCsh bilSchAutoReconCsh = new BilSchAutoReconCsh();
        BilSchAutoReconCshId bilSchAutoReconCshId = new BilSchAutoReconCshId();
        bilSchAutoReconCsh.setBilSchAutoReconCshId(bilSchAutoReconCshId);

        bilSchAutoReconCsh.getBilSchAutoReconCshId().setBilTchKeyId(accountId);
        bilSchAutoReconCsh.setBilDtbSeqNbr(bilDtbSeqNbr);
        bilSchAutoReconCsh.setBilEntryDt(bilCashEntry.getBilCashEntryId().getBilEntryDt());
        bilSchAutoReconCsh.setBilEntryNbr(bilCashEntry.getBilCashEntryId().getBilEntryNbr());
        bilSchAutoReconCsh.setBilEntrySeqNbr(bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
        bilSchAutoReconCsh.setBilDtbDt(dateService.currentDate());

        bilSchAutoReconCsh.setProcessInd(CHAR_N);
        bilSchAutoReconCsh.getBilSchAutoReconCshId().setBilTchKeyTypCd(techTypeCode);
        Short sequenceNbr = bilSchAutoReconCshRepository.findMaxSequenceNumber(
                bilSchAutoReconCsh.getBilSchAutoReconCshId().getBilTchKeyId(),
                bilSchAutoReconCsh.getBilSchAutoReconCshId().getBilTchKeyTypCd());
        if (sequenceNbr == null) {
            sequenceNbr = 0;
        } else {
            sequenceNbr = (short) (sequenceNbr + 1);
        }
        bilSchAutoReconCsh.getBilSchAutoReconCshId().setBilSeqNbr(sequenceNbr);
        bilSchAutoReconCshRepository.saveAndFlush(bilSchAutoReconCsh);
    }

    private void writeBilTtySummary(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow,
            BilThirdParty bilThirdParty) {
        BilTtySummary bilTtySummary = new BilTtySummary();
        bilTtySummary.setBilTtySummaryId(new BilTtySummaryId(bilThirdParty.getBillThirdPartyId(),
                dateService.currentDate(), getMaxSequenceNumber(bilTtySummaryRepository
                        .getMaxSequenceNumber(bilThirdParty.getBillThirdPartyId(), dateService.currentDate()))));
        bilTtySummary.setUserId(bilCashEntry.getBilCashEntryId().getUserId());
        bilTtySummary.setBilAcyAmt(bilCashEntry.getBilRctAmt());
        bilTtySummary.setBilAcyDesCd(bilCashEntry.getBilRctTypeCd());
        if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()) {
            bilTtySummary.setBilDesReaTyp(BilDesReasonType.CPY.getValue());
        } else if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.EFT.getCashEntryMethod()
                || bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.EFT_CREDIT_CARD.getCashEntryMethod()) {
            bilTtySummary.setBilDesReaTyp(BLANK_STRING);
        } else if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.AGENT_CREDIT_CARD.getCashEntryMethod()) {
            bilTtySummary.setBilDesReaTyp(BilDesReasonType.GPY.getValue());
        } else if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.ONE_TIME_ACH.getCashEntryMethod()) {
            bilTtySummary.setBilDesReaTyp(BilDesReasonType.ACH.getValue());
        } else if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.VENDOR_COLLECTIONS.getCashEntryMethod()) {
            bilTtySummary.setBilDesReaTyp(BilDesReasonType.COL.getValue());
        } else {
            bilTtySummary.setBilDesReaTyp(busCdTranslationRepository
                    .findAllByCodeAndTypeList(bilCashEntry.getBilRctTypeCd(),
                            Arrays.asList(BilDesReasonType.PYT.getValue(), BilDesReasonType.PYC.getValue(),
                                    BilDesReasonType.COL.getValue()),
                            language)
                    .get(0).getBusCdTranslationId().getBusType());
        }
        bilTtySummary.setBilAcyDes1Dt(bilCashEntryTotRow.getBilDepositDt());
        bilTtySummary.setBilAcyDes2Dt(DateRoutine.defaultDateTime());
        bilTtySummary.setBilAcyTs(dateService.currentDateTime());
        bilTtySummaryRepository.saveAndFlush(bilTtySummary);

    }

    private BilTtyCashRct writeBilTtyCashReceipt(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow,
            BilThirdParty bilThirdParty) {
        BilTtyCashRct bilTtyCashRct = new BilTtyCashRct();
        bilTtyCashRct.setBilTtyCashRctId(
                new BilTtyCashRctId(bilThirdParty.getBillThirdPartyId(), dateService.currentDate(),
                        getMaxSequenceNumber(bilTtyCashRctRepository.getMaxBilDtbSequenceNumber(
                                bilThirdParty.getBillThirdPartyId(), dateService.currentDate()))));
        bilTtyCashRct.setBilPstmrkCd(bilCashEntry.getBilPstMrkCd());
        bilTtyCashRct.setBilEntryDt(bilCashEntry.getBilCashEntryId().getBilEntryDt());
        bilTtyCashRct.setBilEntryNbr(bilCashEntry.getBilCashEntryId().getBilEntryNbr());
        bilTtyCashRct.setBilEntrySeqNbr(bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
        bilTtyCashRct.setUserId(bilCashEntry.getBilCashEntryId().getUserId());
        bilTtyCashRct.setBilRctAmt(bilCashEntry.getBilRctAmt());
        bilTtyCashRct.setBilRctTypeCd(bilCashEntry.getBilRctTypeCd());
        bilTtyCashRct.setBilRctId(bilCashEntry.getBilRctId());
        bilTtyCashRct.setBilRctCmt(bilCashEntry.getBilRctCmt());
        bilTtyCashRct.setBilTrfTypeCd(BLANK_CHAR);
        bilTtyCashRct.setBilRctReceiveDt(bilCashEntry.getBilRctReceiveDt());
        bilTtyCashRct.setBilBankCd(bilCashEntryTotRow.getBilBankCd());
        bilTtyCashRct.setBilCrcrdTypeCd(bilCashEntry.getBilCrcrdTypeCd());
        bilTtyCashRct.setBilCrcrdActNbr(bilCashEntry.getBilCrcrdActNbr());
        bilTtyCashRct.setBilCrcrdExpDt(bilCashEntry.getBilCrcrdExpDt());
        bilTtyCashRct.setBilCrcrdAutApv(bilCashEntry.getBilCrcrdAutApv());
        bilTtyCashRct.setBilCshEtrMthCd(bilCashEntryTotRow.getBilCshEtrMthCd());
        bilTtyCashRct.setBilTaxYearNbr(bilCashEntry.getBilTaxYearNbr());
        bilTtyCashRct.setBilPstmrkDt(bilCashEntry.getBilPstMrkDt());
        bilTtyCashRct.setBilDepositDt(bilCashEntryTotRow.getBilDepositDt());
        bilTtyCashRct.setBilToFroTrfNbr(BLANK_STRING);
        bilTtyCashRctRepository.save(bilTtyCashRct);
        return bilTtyCashRct;
    }

    private short getMaxSequenceNumber(Short sequenceNumber) {
        if (sequenceNumber == null) {
            return SHORT_ZERO;
        } else {
            return (short) (sequenceNumber + 1);
        }
    }

    private void processBillAccount(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow,
    		BilPolicyTerm bilPolicyTerm, BilAccount bilAccount, Set<String> billAccountSet) {
    	double vendorAccountDirectionAmount = DECIMAL_ZERO;
    	double vendorPolicyDirectionAmount = DECIMAL_ZERO;
    	char manualSuspendIndicator;
    	String suspendReason;
    	BilCashDsp bilCashDsp = null;
    	BilCashReceipt cashReceipt = bilCashReceiptRepository.findBilCashReciptRow(
                bilCashEntry.getBilCashEntryId().getBilEntryDt(), bilCashEntry.getBilCashEntryId().getBilEntryNbr(),
                bilCashEntry.getBilCashEntryId().getUserId(), bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
        if (cashReceipt == null) {
        	Short bilDtbSeqNbr = getMaxSequenceNumber(bilAccount);
            writeBilCashReceipt(bilCashEntry, bilCashEntryTotRow, bilDtbSeqNbr, bilAccount);
            
            boolean isVendorPayment = checkVendorPayment(bilCashEntryTotRow, bilCashEntry);
            if (isVendorPayment
                    && !bilAccount.getBillTypeCd().equals(BilDspTypeCode.SUSPENDED.getValue())) {
            	Double[] object = checkForBcdAmountSplit(bilPolicyTerm, bilAccount, bilCashEntry);
                vendorPolicyDirectionAmount = object[0];
                vendorAccountDirectionAmount = object[1];
            }            

            if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.AGENT_ACH.getCashEntryMethod()
                    || bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.INSURED.getCashEntryMethod()
                    || bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.ONE_TIME_ACH.getCashEntryMethod()) {
                moveBankInformation(bilCashEntry, bilAccount.getAccountId());
            }
            
			if (StringUtils.isNotBlank(bilCashEntry.getPolNbr())) {
				suspendReason = getPolicyRow(bilAccount, bilCashEntry);
			} else {
				suspendReason = getAccountData(bilAccount, bilCashEntry, false);			
			}
            if (validateIfDownPayment(bilCashEntry.getBilRctTypeCd())) {
                manualSuspendIndicator = CHAR_Y;
				if (suspendReason.isEmpty()) {
					suspendReason = BilDesReasonCode.DOWN_PAYMENT_RECEIPT.getValue();
				}
                bilCashDsp = writeBilCashDsp(bilCashEntry, bilAccount, bilPolicyTerm, bilDtbSeqNbr,
                        vendorPolicyDirectionAmount, vendorAccountDirectionAmount, manualSuspendIndicator,
                        suspendReason);
            } else if (validateIfEquityPayment(bilCashEntry.getBilRctTypeCd())
                    && StringUtils.isBlank(bilCashEntry.getPolNbr())) {
                manualSuspendIndicator = CHAR_Y;
                if (suspendReason.isEmpty()) {
                suspendReason = BilDesReasonCode.EQUITY_NO_POLICY_NUMBER.getValue();
                }
                bilCashDsp = writeBilCashDsp(bilCashEntry, bilAccount, bilPolicyTerm, bilDtbSeqNbr,
                        vendorPolicyDirectionAmount, vendorAccountDirectionAmount, manualSuspendIndicator,
                        suspendReason);
            } else {
                Object[] object = checkOtherRules(isVendorPayment, bilCashEntry, bilCashEntryTotRow, bilAccount,
                        bilPolicyTerm);
                manualSuspendIndicator = (char) object[0];
				if (suspendReason.isEmpty()) {
					suspendReason = (String) object[1];
				}
                bilCashDsp = writeBilCashDsp(bilCashEntry, bilAccount, bilPolicyTerm, bilDtbSeqNbr,
                        vendorPolicyDirectionAmount, vendorAccountDirectionAmount, manualSuspendIndicator,
                        suspendReason);
            }
        	           
            writeFinancialApiForBillAccount(bilCashEntry, bilPolicyTerm, bilAccount, bilCashEntryTotRow, bilCashDsp);
            writeBilActSummary(bilAccount, bilCashEntry, bilCashEntryTotRow, bilCashDsp);
            if (bilCashEntry.getBilManualSusInd() == CHAR_Y
                    && !bilCashEntry.getBilSusReasonCd().equals(BilDesReasonCode.DOWN_PAYMENT_RECEIPT.getValue())) {
                return;
            }
            billAccountSet.add(bilAccount.getAccountId());
            
        }
    }
    
    private String getPolicyRow(BilAccount bilAccount, BilCashEntry bilCashEntry) {
    	double currentDue = DECIMAL_ZERO;
        double totalDue = DECIMAL_ZERO;
        String suspendReason = BLANK_STRING;
        List<BilPolicy> bilPolicyList = null;
		bilPolicyList = bilPolicyRepository.findAccountAndPolicyNumber(bilAccount.getAccountNumber(),
				bilCashEntry.getPolNbr(), bilCashEntry.getPolSymbolCd(), Arrays.asList('W', 'Y', 'Z'));
		
        Integer rowCnt = null;
        String accountId = BLANK_STRING;
        String policyId = BLANK_STRING;
        String prevPolicyId = BLANK_STRING;
        String preAccountId = BLANK_STRING;
        int policyCount = 0;
        int issuesCount = 0;
        boolean isValidPolicy = false;
        boolean isMultiCnrPol = false;
        int notIssuesCount = 0;
        String cnrPolId = BLANK_STRING;
        for (BilPolicy bilPolicyTO : bilPolicyList) {
            accountId = bilPolicyTO.getBillPolicyId().getBilAccountId();
            policyId = bilPolicyTO.getBillPolicyId().getPolicyId();

            if (bilPolicyTO.getBilIssueInd() != 'W') {
                rowCnt = bilPolicyTermRepository.getPolicyRowCount(bilPolicyTO.getBillPolicyId().getBilAccountId(),
                        bilPolicyTO.getBillPolicyId().getPolicyId(), 'T');
                if (rowCnt == null || rowCnt == 0) {
                    accountId = preAccountId;
                    policyId = prevPolicyId;
                }
            }
            prevPolicyId = accountId;
            preAccountId = policyId;
            ++policyCount;

            if (bilPolicyTO.getBilIssueInd() == 'W') {
                ++issuesCount;
                if (!cnrPolId.isEmpty() && cnrPolId.compareTo(policyId) != 0) {
                    isMultiCnrPol = true;
                }
                cnrPolId = policyId;
            } else {
                ++notIssuesCount;
            }
        }

        if (policyCount == 0) {
            return BLANK_STRING;
        }

        if (policyCount - issuesCount > 1) {
            return BLANK_STRING;
        }
        if (policyCount > 1 && policyCount - issuesCount == 1) {
            isValidPolicy = true;
        } else {
            if (policyCount == issuesCount) {
                if (isMultiCnrPol) {
                    return BLANK_STRING;
                } 
            } else {
                if (policyCount == notIssuesCount) {
                    isValidPolicy = true;
                }
            }
        }

        boolean isValidAmtType = false;
        if (bilCashEntry.getBilPblItemCd() != null && !bilCashEntry.getBilPblItemCd().trim().isEmpty() && isValidPolicy) {
            isValidAmtType = true;
            Short maxSeq = bilIstScheduleRepository.getMaxSeqNbrByBilPayableItem(accountId, policyId,
            		bilCashEntry.getBilPblItemCd());
            if (maxSeq == null) {
                isValidAmtType = false;
            }
            if (isValidAmtType) {
               Object[] object = getPolicyPayableItem(accountId, policyId, bilCashEntry.getBilPblItemCd(), bilCashEntry.getBilAdjDueDt());
               suspendReason = (String) object[0];
               currentDue = (double) object[1];
            }
        }

        if (currentDue == 0 && totalDue == 0 && isValidPolicy) {
        	Object[] object = getPolicyData(currentDue,totalDue, accountId, policyId, bilCashEntry.getBilAdjDueDt(), isValidPolicy);
        	suspendReason = (String) object[0];
            currentDue = (double) object[1];
        }

        if (currentDue == 0) {
        	suspendReason = getAccountData(bilAccount, bilCashEntry, isValidPolicy);
        }
        
        if (suspendReason.isEmpty()) {
        	suspendReason = opyUpyCheck(currentDue, bilCashEntry);
        }

		return suspendReason;	
	}
    
	private Object[] getPolicyPayableItem(String accountId, String policyId, String payableItem,
			ZonedDateTime dueDate) {
		Double payableItemCurrDue = null;
		Integer payableItemRowCount = payableItemRowsCount(accountId, policyId, payableItem, dueDate);
		if (payableItemRowCount != null) {
			return new Object[] { BilDesReasonCode.OVERPAYMENT.getValue(), DECIMAL_ZERO };
		} else {
			payableItemCurrDue = getpayableItemCurrentDue(accountId, policyId, payableItem, dueDate);
			if (payableItemCurrDue != null) {
				List<Object> scgAmountObj = getScgAmount(accountId, dueDate);
				Double scgAmount = (Double) scgAmountObj.get(1);
				return new Object[] { BLANK_STRING, payableItemCurrDue + scgAmount };
			}
		}

		return new Object[] { BLANK_STRING, DECIMAL_ZERO };
	}
    
    private Double getpayableItemCurrentDue(String accountId, String policyId, String payableItem,
            ZonedDateTime bilAdjDueDt) {
        Double currDueAmt = null;
        currDueAmt = bilIstScheduleRepository.getCurrentDueAmountByPayableItem(accountId, policyId, payableItem,
                Arrays.asList(CHAR_Y, CHAR_L), bilAdjDueDt, DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt),
                DEFAULT_DATE);
        return currDueAmt;
    }
    
    private Integer payableItemRowsCount(String accountId, String policyId, String payableItem, ZonedDateTime dueDate) {
        return bilIstScheduleRepository.getRowCountNumberByPayableItem(accountId, policyId, payableItem,
                Arrays.asList(CHAR_Y, CHAR_L), dueDate, DateRoutine.dateTimeAsYYYYMMDDString(dueDate), DEFAULT_DATE);
    }
    
    private boolean countPolicies(Double currentDue, Double totalDue, boolean isValidPolicy, String accountId,
            String policyId) {
        boolean isCharge = false;
        if (currentDue == 0 && totalDue == 0 && isValidPolicy) {
            BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
            if (bilAccount != null && bilAccount.getBillTypeCd().compareTo("SP") == 0) {
                isCharge = true;

            }
        }
        BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(policyId, accountId)).orElse(null);
        if (bilPolicy == null) {
            isCharge = true;
        }
        return isCharge;
    }

    
	private Object[] getPolicyData(double currentDue, double totalDue, String accountId, String policyId,
			ZonedDateTime dueDate, boolean isValidPolicy) {

		boolean isCharge = countPolicies(currentDue, totalDue, isValidPolicy, accountId, policyId);

		Double polCurrDue = null;
		Integer polRowsCnt = policyRowsCount(accountId, policyId, dueDate);
		if (polRowsCnt == null || polRowsCnt == 0) {
			return new Object[] { BilDesReasonCode.OVERPAYMENT.getValue(), DECIMAL_ZERO };
		} else {
			polCurrDue = getPolicyCurrentDue(accountId, policyId, dueDate);
			if (polCurrDue != null) {
				currentDue = polCurrDue;
				if (isCharge) {
					List<Object> scgAmountObj = getScgAmount(accountId, dueDate);
					Double scgAmount = (Double) scgAmountObj.get(1);
					return new Object[] { BLANK_STRING, scgAmount + currentDue };
				}
			}
		}
		return new Object[] { BLANK_STRING, currentDue };
	}
    
    private Double getPolicyCurrentDue(String accountId, String policyId, ZonedDateTime bilAdjDueDt) {
        Double currDueAmt = null;
        currDueAmt = bilIstScheduleRepository.getCurrentDueAmountByPolicyId(accountId, policyId,
                Arrays.asList(CHAR_Y, CHAR_L), bilAdjDueDt, DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt),
                DEFAULT_DATE);
        return currDueAmt;
    }

    
    private Integer policyRowsCount(String accountId, String policyId, ZonedDateTime dueDate) {
        return bilIstScheduleRepository.getRowCountNumberByPolicyId(accountId, policyId, Arrays.asList(CHAR_Y, CHAR_L),
                dueDate, DateRoutine.dateTimeAsYYYYMMDDString(dueDate), DEFAULT_DATE);
    }
    
	private String getAccountData(BilAccount bilAccount, BilCashEntry bilCashEntry, boolean isValidPolicy) {
        Double acctCurrDue = null;
        double currentDue = DECIMAL_ZERO;
        String suspendReason = BLANK_STRING;
        Integer rowCount = accountRowsCount(bilAccount.getAccountId(), bilCashEntry.getBilAdjDueDt());
        if (rowCount == null || rowCount == 0) {
            rowCount = 0;
            acctCurrDue = DECIMAL_ZERO;
        } else {
            acctCurrDue = getAccountCurrentDue(bilAccount.getAccountId(), bilCashEntry.getBilAdjDueDt());
            if (acctCurrDue == null) {
                acctCurrDue = DECIMAL_ZERO;
            }
        }
        List<Object> scgAmountObj = getScgAmount(bilAccount.getAccountId(), bilCashEntry.getBilAdjDueDt());
        Integer scgRowCnt = (Integer) scgAmountObj.get(0);
        Double scgAmount = (Double) scgAmountObj.get(1);

        if (rowCount == 0 && scgRowCnt == 0) {
        	suspendReason = BilDesReasonCode.OVERPAYMENT.getValue();
        } else {
            acctCurrDue = scgAmount + acctCurrDue;
			if (!isValidPolicy) {
				currentDue = acctCurrDue;
			}
        }
        if (suspendReason.isEmpty()) {
        	suspendReason = opyUpyCheck(currentDue, bilCashEntry);
        }
        return suspendReason;
    }
    
	private String opyUpyCheck(double currentDue, BilCashEntry bilCashEntry) {
		if (bilCashEntry.getBilRctAmt() > currentDue) {
			return BilDesReasonCode.OVERPAYMENT.getValue();
		}
		return BLANK_STRING;
	}

    private Integer accountRowsCount(String accountId, ZonedDateTime dueDate) {
        return bilIstScheduleRepository.getRowCountNumber(accountId, Arrays.asList(CHAR_Y, CHAR_L, CHAR_B), dueDate,
                DateRoutine.dateTimeAsYYYYMMDDString(dueDate), DEFAULT_DATE);
    }
    
    private Integer getScgRowCount(String accountId, ZonedDateTime bilAdjDueDt) {
        return bilCrgAmountsRepository.getRowCountNumber(Arrays.asList(CHAR_Y, CHAR_L, CHAR_B), accountId, bilAdjDueDt,
                DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt), DEFAULT_DATE);
    }
    
    private List<Object> getScgAmount(String accountId, ZonedDateTime bilAdjDueDt) {
        List<Object> scgAmountObject = new ArrayList<>();
        Integer scgRowCnt = null;
        Double scgAmount = null;

        scgRowCnt = getScgRowCount(accountId, bilAdjDueDt);
        if (scgRowCnt == null || scgRowCnt == 0) {
            scgRowCnt = 0;
        } else {
            scgAmount = bilCrgAmountsRepository.getServiceChargeAmount(accountId, Arrays.asList(CHAR_Y, CHAR_L, CHAR_B),
                    bilAdjDueDt, DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt), DEFAULT_DATE);
        }
        scgAmountObject.add(scgRowCnt);
        scgAmountObject.add(scgAmount == null ? DECIMAL_ZERO : scgAmount);
        return scgAmountObject;
    }
    
    private Double getAccountCurrentDue(String accountId, ZonedDateTime bilAdjDueDt) {
        Double currDueAmt = null;
        currDueAmt = bilIstScheduleRepository.getCurrentlDueAmountByAccount(accountId,
                Arrays.asList(CHAR_Y, CHAR_L, CHAR_B), bilAdjDueDt, DateRoutine.dateTimeAsYYYYMMDDString(bilAdjDueDt),
                DEFAULT_DATE);
        if (currDueAmt == null) {
            return DECIMAL_ZERO;
        }
        return currDueAmt;
    }
    
    private boolean checkVendorPayment(BilCashEntryTot bilCashEntryTotRow, BilCashEntry bilCashEntry) {
    	if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.VENDOR_COLLECTIONS.getCashEntryMethod()
                || bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.ONLINE_ENTRY.getCashEntryMethod()) {
            return busCdTranslationRepository.findByCodeAndType(bilCashEntry.getBilRctTypeCd(),
                    BilDesReasonType.COL.getValue(), language) != null;
            
        }
		return false;
	}

	private Object[] checkOtherRules(boolean isVendorPayment, BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow,
    		BilAccount bilAccount, BilPolicyTerm bilPolicyTerm) {
    	
    	if(bilCashEntry.getBilManualSusInd() != CHAR_Y && (isVendorPayment || bilCashEntryTotRow.getBilCshEtrMthCd() != CashEntryMethod.EFT.getCashEntryMethod()
                    && bilCashEntryTotRow.getBilCshEtrMthCd() != CashEntryMethod.CREDIT.getCashEntryMethod()
                    && bilCashEntryTotRow.getBilCshEtrMthCd() != CashEntryMethod.EFT_CREDIT_CARD.getCashEntryMethod()
                    && bilCashEntryTotRow.getBilCshEtrMthCd() != CashEntryMethod.ONLINE_ENTRY.getCashEntryMethod()
                    && bilCashEntryTotRow.getBilCshEtrMthCd() != CashEntryMethod.CREDIT_CARD.getCashEntryMethod()
                    && bilCashEntryTotRow.getBilCshEtrMthCd() != CashEntryMethod.TOLERANCE_WRITEOFF.getCashEntryMethod()
                    && bilCashEntryTotRow.getBilCshEtrMthCd() != CashEntryMethod.STATEMENT.getCashEntryMethod()
                    && bilCashEntryTotRow.getBilCshEtrMthCd() != CashEntryMethod.TAPE_FROM_CASH_RECONCILIATION
                            .getCashEntryMethod())) {
              return chkSuspenseRules(bilAccount, bilCashEntry, bilPolicyTerm);
        }
    	return new Object[] { bilCashEntry.getBilManualSusInd(), bilCashEntry.getBilSusReasonCd() };
	}

	private void moveBankInformation(BilCashEntry bilCashEntry, String bilAccountId) {
        AddUpdateBankInformation addUpdateBankInformation = new AddUpdateBankInformation();
        addUpdateBankInformation.setBilAccountId(bilAccountId);
        addUpdateBankInformation.setBilEntryDate(bilCashEntry.getBilCashEntryId().getBilEntryDt());
        addUpdateBankInformation.setBilEntryNumber(bilCashEntry.getBilCashEntryId().getBilEntryNbr());
        addUpdateBankInformation.setEffectiveDate(dateService.currentDate());
        addUpdateBankInformation.setBilEntrySeqNumber(bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
        addUpdateBankInformation.setUserId(bilCashEntry.getBilCashEntryId().getUserId());
        addUpdateBankInformationService.addBankInformation(addUpdateBankInformation);

    }

    private void callCashApplyDriver(String accountId, ZonedDateTime date, String userSequenceId) {
        CashApplyDriver cashApplyDriver = new CashApplyDriver();
        cashApplyDriver.setAccountId(accountId);
        cashApplyDriver.setQuoteIndicator(String.valueOf(BLANK_CHAR));
        cashApplyDriver.setCallBcmoax(true);
        cashApplyDriver.setCashApplyDate(date);
        cashApplyDriver.setUserSequenceId(userSequenceId);
        cashApplyDriverService.processCashApply(cashApplyDriver);
    }

    private String getAdditionalData(ZonedDateTime entryDate, String entryNumber, String userId,
            short entrySequenceNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(entryNumber, 4, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad(userId, 8, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
        return stringBuilder.toString();
    }

    private void writeBilActSummary(BilAccount bilAccount, BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow,
            BilCashDsp bilCashDsp) {
        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId();
        bilActSummaryId.setBilAccountId(bilAccount.getAccountId());
        if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()) {
            bilActSummary.setBilDesReaTyp(BilDesReasonType.CPY.getValue());
        } else if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.EFT.getCashEntryMethod()
                || bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.EFT_CREDIT_CARD.getCashEntryMethod()) {
            bilActSummary.setBilDesReaTyp(BLANK_STRING);
        } else if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.AGENT_CREDIT_CARD.getCashEntryMethod()) {
            bilActSummary.setBilDesReaTyp(BilDesReasonType.GPY.getValue());
        } else if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.ONE_TIME_ACH.getCashEntryMethod()) {
            bilActSummary.setBilDesReaTyp(BilDesReasonType.ACH.getValue());
        } else if (bilCashEntryTotRow.getBilCshEtrMthCd() == CashEntryMethod.VENDOR_COLLECTIONS.getCashEntryMethod()) {
            bilActSummary.setBilDesReaTyp(BilDesReasonType.COL.getValue());
        } else {
            bilActSummary.setBilDesReaTyp(busCdTranslationRepository
                    .findAllByCodeAndTypeList(bilCashEntry.getBilRctTypeCd(),
                            Arrays.asList(BilDesReasonType.PYT.getValue(), BilDesReasonType.PYC.getValue(),
                                    BilDesReasonType.COL.getValue()),
                            language)
                    .get(0).getBusCdTranslationId().getBusType());
        }
        bilActSummary
                .setBillActSummaryId(new BilActSummaryId(bilAccount.getAccountId(), dateService.currentDate(),
                        fetchMaxSequenceNumber(bilAccount.getAccountId(), dateService.currentDate())));
        bilActSummaryId.setBilAcyDt(dateService.currentDate());
        bilActSummary.setPolNbr(bilCashDsp.getPolicyNumber());
        bilActSummary.setPolSymbolCd(bilCashDsp.getPolicySymbolCd());
        bilActSummary.setBilAcyDes1Dt(DateRoutine.defaultDateTime());
        bilActSummary.setBilAcyDes2Dt(bilCashEntry.getBilPstMrkDt());
        bilActSummary.setBilAcyDesCd(bilCashEntry.getBilRctTypeCd());
        bilActSummary.setBilAcyAmt(bilCashEntry.getBilRctAmt());
        bilActSummary.setUserId(bilCashEntry.getBilCashEntryId().getUserId());
        bilActSummary.setBilAcyTs(dateService.currentDate());
        bilActSummary.setBasAddDataTxt(getAdditionalData(bilCashEntry.getBilCashEntryId().getBilEntryDt(),
                bilCashEntry.getBilCashEntryId().getBilEntryNbr(), bilCashEntry.getBilCashEntryId().getUserId(),
                bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr()));

        bilActSummaryRepository.saveAndFlush(bilActSummary);

    }

    private short fetchMaxSequenceNumber(String accountId, ZonedDateTime activityDate) {

        Short seqNumber = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, activityDate);
        if (seqNumber == null) {
            seqNumber = 0;
        } else {
            seqNumber = (short) (seqNumber + 1);
        }
        return seqNumber;
    }

    private void writeFinancialApiForBillAccount(BilCashEntry bilCashEntry, BilPolicyTerm bilPolicyTerm,
            BilAccount bilAccount, BilCashEntryTot bilCashEntryTotRow, BilCashDsp bilCashDsp) {
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();

        financialApiActivity.setFunctionCode(CHAR_A);
        financialApiActivity.setReferenceId(BLANK_STRING);
        financialApiActivity.setApplicationName(BCMOCR);
        financialApiActivity.setUserId(BLANK_STRING);
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setTransactionObjectCode(BilCashEntryMethodCode.CASH.getValue());
        financialApiActivity.setTransactionActionCode(BilCashEntryMethodCode.SUSPENDED.getValue());
        financialApiActivity.setApplicationProductIdentifier(BCMS);
        financialApiActivity.setMasterCompanyNbr(bilPolicyTerm.getMasterCompanyNbr());
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(CHAR_Y);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        financialApiActivity.setOperatorId(BLANK_STRING);
        financialApiActivity.setAppProgramId(BCMOCR);
        financialApiActivity.setActivityAmount(bilCashEntry.getBilRctAmt());
        financialApiActivity.setActivityNetAmount(bilCashEntry.getBilRctAmt());
        financialApiActivity.setBilBankCode(bilCashEntryTotRow.getBilBankCd());
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        financialApiActivity.setBilDatabaseKey(getDatabaseKey(bilCashEntry.getBilCashEntryId().getBilEntryDt(),
                bilCashEntry.getBilCashEntryId().getBilEntryNbr(), bilCashEntry.getBilCashEntryId().getUserId(),
                bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr()));
        financialApiActivity.setOriginalEffectiveDate(BLANK_STRING);
        financialApiActivity.setBusinessCaseId(BLANK_STRING);
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setAgentTtyId(BLANK_STRING);
        financialApiActivity.setBilAccountId(bilCashEntry.getBilAccountNbr());
        financialApiActivity.setBilReceiptTypeCode(bilCashEntry.getBilRctTypeCd());
        financialApiActivity.setBilReasonCode(bilCashDsp.getDspReasonCd());
        financialApiActivity.setClientId(BLANK_STRING);
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setCurrencyCode(bilAccount.getCurrencyCode());
        financialApiActivity.setDepositeBankCode(bilCashEntryTotRow.getBctDepBankCd());
        financialApiActivity.setDisbursementId(BLANK_STRING);
        financialApiActivity.setLineOfBusCode(BLANK_STRING);
        financialApiActivity.setMarketSectorCode(BLANK_STRING);
        financialApiActivity.setPayableItemCode(BLANK_STRING);
        financialApiActivity.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
        financialApiActivity.setStateCode(BLANK_STRING);
        financialApiActivity.setStatusCode(BLANK_CHAR);
        financialApiActivity.setBilSourceCode(String.valueOf(bilCashEntryTotRow.getBilCshEtrMthCd()));
        financialApiActivity.setCountryCode(BLANK_STRING);
        financialApiActivity.setBilTypeCode(bilAccount.getBillTypeCd());
        financialApiActivity.setBilClassCode(bilAccount.getBillClassCd());

        insertBcmActivity(financialApiActivity);

    }

    private BilCashDsp writeBilCashDsp(BilCashEntry bilCashEntry, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm,
            Short bilDtbSeqNbr, double vendorPolicyDirectionAmount, double vendorAccountDirectionAmount,
            char manualSuspendIndicator, String suspendReason) {
    	
    	BilCashDsp bilCashDsp = new BilCashDsp();
        if (suspendReason.equals(BilDesReasonCode.EQUITY_NO_POLICY_NUMBER.getValue())) {
            bilCashDsp.setPolicyNumber(bilCashEntry.getBilAccountNbr());
        } else {
            bilCashDsp.setPolicyNumber(bilCashEntry.getPolNbr());
        }
        if (StringUtils.isNotBlank(bilCashEntry.getPolNbr())) {
            if (StringUtils.isBlank(bilCashEntry.getPolSymbolCd())) {
                bilCashDsp.setPolicySymbolCd(getPolicySymbolCode(bilAccount, bilCashEntry, bilPolicyTerm));
            } else {
                bilCashDsp.setPolicySymbolCd(bilCashEntry.getPolSymbolCd());
            }
        }
        
        if (vendorPolicyDirectionAmount > DECIMAL_ZERO && vendorAccountDirectionAmount > DECIMAL_ZERO) {
            bilCashDsp.setDspAmount(vendorPolicyDirectionAmount);
        } else {
            bilCashDsp.setDspAmount(bilCashEntry.getBilRctAmt());
        }
        Short dspSeqNumber = 0;
        BilCashDspId bilCashDspId = new BilCashDspId(bilAccount.getAccountId(), dateService.currentDate(),
                bilDtbSeqNbr, dspSeqNumber);
        bilCashDsp.setBilCashDspId(bilCashDspId);
        bilCashDsp.setPolicyId(SEPARATOR_BLANK);
        bilCashDsp.setPayableItemCd(bilCashEntry.getBilPblItemCd());
        bilCashDsp.setAdjustmentDueDate(bilCashEntry.getBilAdjDueDt());
        bilCashDsp.setChargeTypeCd(BLANK_CHAR);
        bilCashDsp.setUserId(bilCashEntry.getBilCashEntryId().getUserId());
        bilCashDsp.setDspDate(dateService.currentDate());
        bilCashDsp.setDspTypeCd(BilDspTypeCode.SUSPENDED.getValue());
        bilCashDsp.setManualSuspenseIndicator(manualSuspendIndicator);
        bilCashDsp.setDspReasonCd(suspendReason);
        bilCashDsp.setPayeeClientId(SEPARATOR_BLANK);
        bilCashDsp.setPayeeAddressSequenceNumber(SHORT_ZERO);
        bilCashDsp.setDraftNbr(SHORT_ZERO);
        bilCashDsp.setCreditIndicator(CHAR_N);
        bilCashDsp.setCreditPolicyId(SEPARATOR_BLANK);
        bilCashDsp.setCreditAmountType(SEPARATOR_BLANK);
        bilCashDsp.setRevsRsusIndicator(BLANK_CHAR);
        bilCashDsp.setToFromTransferNbr(SEPARATOR_BLANK);
        bilCashDsp.setTransferTypeCd(BLANK_CHAR);
        bilCashDsp.setStatusCd(BLANK_CHAR);
        bilCashDsp.setDisbursementId(SEPARATOR_BLANK);
        bilCashDsp.setCheckProdMethodCd(BLANK_CHAR);
        bilCashDsp.setPolicyEffectiveDate(DateRoutine.defaultDateTime());
        bilCashDsp.setBilSequenceNumber(SHORT_ZERO);
        bilCashDsp.setSystemDueDate(DateRoutine.defaultDateTime());
        bilCashDsp.setInvoiceDate(DateRoutine.defaultDateTime());
        bilCashDsp.setDisbursementDate(DateRoutine.defaultDateTime());
        bilCashDsp.setStatementDetailTypeCd(SEPARATOR_BLANK);
        bilCashDspRepository.saveAndFlush(bilCashDsp);

        if (vendorPolicyDirectionAmount > DECIMAL_ZERO && vendorAccountDirectionAmount > DECIMAL_ZERO) {
            writeBilCashReceiptForVendorAmount(bilDtbSeqNbr, bilCashEntry, dspSeqNumber, bilAccount,
                    vendorAccountDirectionAmount, bilCashDsp);
        }

        return bilCashDsp;

    }

    private Object[] chkSuspenseRules(BilAccount bilAccount, BilCashEntry bilCashEntry, BilPolicyTerm bilPolicyTerm) {
    	boolean isBalanceDue = checkRule(NBUB, bilAccount);
    	boolean isPotdRule = checkRule(POTD, bilAccount);
    	char manualSuspendIndicator = BLANK_CHAR;
    	String suspendReason = SEPARATOR_BLANK;
    	Double amount = null;
    	if(isBalanceDue || isPotdRule) {
    		if (StringUtils.isNotBlank(bilCashEntry.getPolNbr())
                    && StringUtils.isNotBlank(bilPolicyTerm.getBillPolicyTermId().getPolicyId())) {
                amount = calculatePolicyBalance(bilAccount, bilPolicyTerm);
            } else {
            	amount = calculateAccountBalance(bilAccount);
            }
    		if(isBalanceDue && amount <= DECIMAL_ZERO) {
    		    manualSuspendIndicator = CHAR_Y;
    		    suspendReason = BusCodeTranslationType.NO_BALANCE_FOUND.getValue();
            } else if (isPotdRule && bilCashEntry.getBilRctAmt() > amount) {
                manualSuspendIndicator = CHAR_Y;
                suspendReason = BusCodeTranslationType.PAYMENT_OVER_TOTAL_DUE.getValue();
            }
    	}
    	
        if (bilCashEntry.getBilManualSusInd() != CHAR_Y) {
           return checkRenewRule(bilAccount, bilCashEntry, bilPolicyTerm);
        }
        
        return new Object[] {manualSuspendIndicator, suspendReason};
    }

    private Object[] checkRenewRule(BilAccount bilAccount, BilCashEntry bilCashEntry, BilPolicyTerm bilPolicyTerm) {
        BilObjectCfg bilObjectCfg = bilObjectCfgRepository.findById(new BilObjectCfgId(BCMOCSH3, CHAR_U)).orElse(null);
        if (bilObjectCfg != null && bilObjectCfg.getBocObjPrcCd() != CHAR_Y) {
            return new Object[] { bilCashEntry.getBilManualSusInd(), bilCashEntry.getBilSusReasonCd() };
        }
        if (StringUtils.isNotBlank(bilCashEntry.getPolNbr())
                && StringUtils.isBlank(bilPolicyTerm.getBillPolicyTermId().getPolicyId())
                || bilPolicyTerm.getBptAgreementInd() == CHAR_Y) {
            return new Object[] { bilCashEntry.getBilManualSusInd(), bilCashEntry.getBilSusReasonCd() };
        }
        if(StringUtils.isBlank(bilCashEntry.getPolNbr()) && bilPolicyTerm.getBillPolicyTermId() != null
        	&& StringUtils.isBlank(bilPolicyTerm.getBillPolicyTermId().getPolicyId())) {
        	bilPolicyTerm = bilPolicyTermRepository.getPolicyId(bilAccount.getAccountId(),
                    Arrays.asList(CHAR_W, CHAR_T, CHAR_D), CHAR_Y);
        }
        if(bilPolicyTerm != null) {
        	ZonedDateTime effectiveDate = callReExitReturn(bilAccount, bilCashEntry, bilPolicyTerm);
            if (!effectiveDate.isEqual(DateRoutine.defaultDateTime()) && callCashReceiptUserExit(bilPolicyTerm.getBillPolicyTermId().getPolicyId())) {
                    return new Object[] {CHAR_Y, BusCodeTranslationType.NY_RENEWAL_CANCELLED.getValue()};
            }
        }
        return new Object[] { bilCashEntry.getBilManualSusInd(), bilCashEntry.getBilSusReasonCd() };
    }

    private boolean callCashReceiptUserExit(String policyId) {
        return cashApplyUserExitService.callCashApplyUserExit(policyId);
    }

    private ZonedDateTime callReExitReturn(BilAccount bilAccount, BilCashEntry bilCashEntry,
            BilPolicyTerm bilPolicyTerm) {
        ZonedDateTime polEffectiveDate = bilIstScheduleRepository.getMinPolicyEffectiveDt(bilAccount.getAccountId(),
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilCashEntry.getBilAdjDueDt(),
                Arrays.asList(CHAR_Y, CHAR_N), DateRoutine.defaultDateTime());
        if (polEffectiveDate != null) {
            return polEffectiveDate;
        }
        polEffectiveDate = bilIstScheduleRepository.getMinPolicyEffectiveDt(bilAccount.getAccountId(),
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilCashEntry.getBilAdjDueDt(),
                Arrays.asList(CHAR_A, CHAR_B, BLANK_CHAR), DateRoutine.defaultDateTime());
        if (polEffectiveDate != null) {
            return polEffectiveDate;
        }

        return DateRoutine.defaultDateTime();

    }

    private boolean checkRule(String ruleId, BilAccount bilAccount) {
        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct(ruleId, bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), BLANK_STRING);
        
        return bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y ? Boolean.TRUE : Boolean.FALSE;
        
    }

    private Double calculateAccountBalance(BilAccount bilAccount) {
        Double accountBalance = bilIstScheduleRepository.getTotalDueAmountByAccountId(bilAccount.getAccountId());
        if (accountBalance != null) {
        	accountBalance += getAllCharges(bilAccount);
        }
        
        return accountBalance == null ? DECIMAL_ZERO : accountBalance;
    }

    private Double calculatePolicyBalance(BilAccount bilAccount, BilPolicyTerm bilPolicyTerm) {
        Double amount = bilIstScheduleRepository.getCivAmountByAccountIdAndPolicyId(bilAccount.getAccountId(),
                bilPolicyTerm.getBillPolicyTermId().getPolicyId());
        if (amount != null) {
        	Boolean isGetAllCharges = checkOnePolicyAccount(bilAccount);
            if (Boolean.TRUE.equals(isGetAllCharges)) {
                amount += getAllCharges(bilAccount);
            }
            
        }
        return amount == null? DECIMAL_ZERO : amount;
        
    }

    private Double getAllCharges(BilAccount bilAccount) {
        Double chargeAmount = bilCrgAmountsRepository.getCivChargeAmount(Arrays.asList(CHAR_Y, CHAR_L),
                bilAccount.getAccountId(), Arrays.asList('S'));
        if (chargeAmount == null) {
            chargeAmount = DECIMAL_ZERO;
        }
        Double scgBalance = bilCrgAmountsRepository.getChargeAmount(bilAccount.getAccountId(),
                Arrays.asList(CHAR_P, CHAR_L, CHAR_D));
        if (scgBalance == null) {
            scgBalance = DECIMAL_ZERO;
        }
        return chargeAmount + scgBalance;
    }

    private Boolean checkOnePolicyAccount(BilAccount bilAccount) {
        if (bilAccount.getBillTypeCd().equals(BilDspTypeCode.SUSPENDED.getValue())) {
            return true;
        }
        return bilPolicyTermRepository.getPolicyId(bilAccount.getAccountId(),
                Arrays.asList(CHAR_W, CHAR_T, CHAR_D, CHAR_G)) != null;
    }

    private Double[] checkForBcdAmountSplit(BilPolicyTerm bilPolicyTerm, BilAccount bilAccount,
            BilCashEntry bilCashEntry) {
        BilPolicyTerm bilPolTerm = bilPolicyTermRepository.getCollectionCodeAndAmount(bilAccount.getAccountId(),
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), DECIMAL_ZERO, CHAR_Y);
        if (bilPolTerm == null) {
            return new Double[] { DECIMAL_ZERO, DECIMAL_ZERO };
        }
        Double vendorPolicyDirectionAmount = bilIstScheduleRepository.getCivAmount(bilAccount.getAccountId(),
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), Arrays.asList(CHAR_Y, CHAR_L));
        Double vendorAccountDirectionAmount = DECIMAL_ZERO;
        if (vendorPolicyDirectionAmount > DECIMAL_ZERO && bilCashEntry.getBilRctAmt() > vendorPolicyDirectionAmount) {
            vendorAccountDirectionAmount = bilCashEntry.getBilRctAmt() - vendorPolicyDirectionAmount;
        }

        return new Double[] { vendorPolicyDirectionAmount, vendorAccountDirectionAmount };

    }

    private void writeBilCashReceiptForVendorAmount(Short bilDtbSeqNbr, BilCashEntry bilCashEntry, Short dspSeqNumber,
            BilAccount bilAccount, double vendorAccountDirectionAmount, BilCashDsp bilCashDisposition) {
        BilCashDsp bilCashDsp = new BilCashDsp();
        bilCashDsp.setPolicyNumber(bilCashDisposition.getPolicyNumber());
        BilCashDspId bilCashDspId = new BilCashDspId(bilAccount.getAccountId(), dateService.currentDate(),
                bilDtbSeqNbr, ++dspSeqNumber);
        bilCashDsp.setBilCashDspId(bilCashDspId);
        bilCashDsp.setPolicyId(SEPARATOR_BLANK);
        bilCashDsp.setPolicySymbolCd(bilCashDisposition.getPolicySymbolCd());
        bilCashDsp.setPayableItemCd(bilCashEntry.getBilPblItemCd());
        bilCashDsp.setAdjustmentDueDate(bilCashEntry.getBilAdjDueDt());
        bilCashDsp.setChargeTypeCd(BLANK_CHAR);
        bilCashDsp.setUserId(bilCashEntry.getBilCashEntryId().getUserId());
        bilCashDsp.setDspDate(dateService.currentDate());
        bilCashDsp.setDspTypeCd(BilDspTypeCode.SUSPENDED.getValue());
        bilCashDsp.setDspReasonCd(bilCashDisposition.getDspReasonCd());
        bilCashDsp.setDspAmount(vendorAccountDirectionAmount);
        bilCashDsp.setPayeeClientId(SEPARATOR_BLANK);
        bilCashDsp.setPayeeAddressSequenceNumber(SHORT_ZERO);
        bilCashDsp.setManualSuspenseIndicator(bilCashDisposition.getManualSuspenseIndicator());
        bilCashDsp.setDraftNbr(SHORT_ZERO);
        bilCashDsp.setCreditIndicator(CHAR_N);
        bilCashDsp.setCreditPolicyId(SEPARATOR_BLANK);
        bilCashDsp.setCreditAmountType(SEPARATOR_BLANK);
        bilCashDsp.setRevsRsusIndicator(BLANK_CHAR);
        bilCashDsp.setToFromTransferNbr(SEPARATOR_BLANK);
        bilCashDsp.setTransferTypeCd(BLANK_CHAR);
        bilCashDsp.setStatusCd(BLANK_CHAR);
        bilCashDsp.setDisbursementId(SEPARATOR_BLANK);
        bilCashDsp.setCheckProdMethodCd(BLANK_CHAR);
        bilCashDsp.setPolicyEffectiveDate(DateRoutine.defaultDateTime());
        bilCashDsp.setBilSequenceNumber(SHORT_ZERO);
        bilCashDsp.setSystemDueDate(DateRoutine.defaultDateTime());
        bilCashDsp.setInvoiceDate(DateRoutine.defaultDateTime());
        bilCashDsp.setDisbursementDate(DateRoutine.defaultDateTime());
        bilCashDsp.setStatementDetailTypeCd(SEPARATOR_BLANK);
        bilCashDspRepository.saveAndFlush(bilCashDsp);

    }

    private String getPolicySymbolCode(BilAccount bilAccount, BilCashEntry bilCashEntry, BilPolicyTerm bilPolicyTerm) {
        ZonedDateTime maxPolicyEffectiveDate = bilPolicyTermRepository.getMaxPolicyEffDate(bilAccount.getAccountId(),
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), CHAR_T);
        if (maxPolicyEffectiveDate == null) {
            return BLANK_STRING;
        }
        BilPolicy bilPolicy = bilPolicyRepository.findByPolicyIdAndPolicyNumberAndAccountId(
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilCashEntry.getPolNbr(), bilAccount.getAccountId(),
                Arrays.asList(CHAR_Y, CHAR_Z));
        if (bilPolicy != null) {
            return bilPolicy.getPolSymbolCd();
        }

        return BLANK_STRING;
    }

    private BilCashReceipt writeBilCashReceipt(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow,
            Short bilDtbSeqNbr, BilAccount bilAccount) {
        BilCashReceipt bilCashReceipt = new BilCashReceipt();

        BilCashReceiptId bilCashReceiptId = new BilCashReceiptId(bilAccount.getAccountId(),
                dateService.currentDate(), bilDtbSeqNbr);
        bilCashReceipt.setBilCashReceiptId(bilCashReceiptId);

        bilCashReceipt.setEntryDate(bilCashEntry.getBilCashEntryId().getBilEntryDt());
        bilCashReceipt.setEntryNumber(bilCashEntry.getBilCashEntryId().getBilEntryNbr());
        bilCashReceipt.setEntrySequenceNumber(bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
        bilCashReceipt.setUserId(bilCashEntry.getBilCashEntryId().getUserId());
        bilCashReceipt.setReceiptAmount(bilCashEntry.getBilRctAmt());
        bilCashReceipt.setReceiptId(bilCashEntry.getBilRctId());
        bilCashReceipt.setReceiptTypeCd(bilCashEntry.getBilRctTypeCd());
        bilCashReceipt.setToFroTransferNumber(SEPARATOR_BLANK);
        bilCashReceipt.setTransferTypeCd(BLANK_CHAR);
        bilCashReceipt.setReceiptReceiveDate(bilCashEntry.getBilRctReceiveDt());
        bilCashReceipt.setBankCd(bilCashEntryTotRow.getBilBankCd());
        bilCashReceipt.setDepositeDate(bilCashEntryTotRow.getBilDepositDt());
        bilCashReceipt.setCreditCardTypeCd(bilCashEntry.getBilCrcrdTypeCd());
        bilCashReceipt.setCreditCardAccounttNumber(bilCashEntry.getBilCrcrdActNbr());
        bilCashReceipt.setCreditCardExpirationDate(bilCashEntry.getBilCrcrdExpDt());
        bilCashReceipt.setCredutCardAuthroizationApproval(bilCashEntry.getBilCrcrdAutApv());
        bilCashReceipt.setReceiptComment(bilCashEntry.getBilRctCmt());
        bilCashReceipt.setCashEntryMethodCd(bilCashEntryTotRow.getBilCshEtrMthCd());
        bilCashReceipt.setTaxYearNumber(bilCashEntry.getBilTaxYearNbr());
        bilCashReceipt.setThirdPartyCashIdentifier(BLANK_CHAR);
        bilCashReceipt.setPostMarkDate(bilCashEntry.getBilPstMrkDt());
        bilCashReceipt.setPostMarkCd(bilCashEntry.getBilPstMrkCd());
        bilCashReceipt.setAgencyNumber(SEPARATOR_BLANK);
        bilCashReceiptRepository.saveAndFlush(bilCashReceipt);
        return bilCashReceipt;

    }

    private Short getMaxSequenceNumber(BilAccount bilAccount) {
        Short bilEntrySequenceNumber = bilCashReceiptRepository.getMaxBilDtbSeqNbr(bilAccount.getAccountId(),
                dateService.currentDate());
        if (bilEntrySequenceNumber == null) {
            bilEntrySequenceNumber = 0;
        } else {
            bilEntrySequenceNumber = (short) (bilEntrySequenceNumber + 1);
        }
        return bilEntrySequenceNumber;
    }

    private void unIdentifiedCash(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow) {
        BilUnIdCash bilUnIdCash = new BilUnIdCash();
        Boolean isReceiptFound = false;
        BilUnIdCash unIdCash = bilUnIdCashRepository.findUnIdCash(bilCashEntry.getBilCashEntryId().getBilEntryDt(),
                bilCashEntry.getBilCashEntryId().getBilEntryNbr(), bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr(),
                bilCashEntry.getBilCashEntryId().getUserId(), bilCashEntry.getBilRctAmt());
        if (unIdCash == null) {
            isReceiptFound = checkCashReceipt(bilCashEntry, isReceiptFound);
            if (Boolean.TRUE.equals(isReceiptFound)) {
                return;
            }
            bilUnIdCash.setPolNbr(bilCashEntry.getPolNbr());
            bilUnIdCash.setBilDspReasonCd(bilCashEntry.getBilSusReasonCd());
            bilUnIdCash.setBilManualSusInd(bilCashEntry.getBilManualSusInd());
            if (validateIfDownPayment(bilCashEntry.getBilRctTypeCd())) {
                bilUnIdCash.setBilManualSusInd(CHAR_Y);
                bilUnIdCash.setBilDspReasonCd(BilDesReasonCode.DOWN_PAYMENT_RECEIPT.getValue());
            } else if (validateIfEquityPayment(bilCashEntry.getBilRctTypeCd())
                    && StringUtils.isNotBlank(bilCashEntry.getBilAccountNbr())
                    && StringUtils.isBlank(bilCashEntry.getPolNbr())) {
                bilUnIdCash.setBilManualSusInd(CHAR_Y);
                bilUnIdCash.setBilDspReasonCd(BilDesReasonCode.EQUITY_NO_POLICY_NUMBER.getValue());
                bilUnIdCash.setPolNbr(bilCashEntry.getBilAccountNbr());
            }
            Short bilDtbSeqNbr = getBilDtbSeqNbr(dateService.currentDate());
            bilUnIdCash.setBilUnIdCashId(new BilUnIdCashId(dateService.currentDate(), bilDtbSeqNbr,
                    bilCashEntry.getBilCashEntryId().getBilEntryNbr(), bilCashEntry.getBilCashEntryId().getUserId(),
                    bilCashEntry.getBilCashEntryId().getBilEntryDt()));
            writeBilUnIdCashRow(bilUnIdCash, bilCashEntry, bilCashEntryTotRow);
            writeFinancilaApiForUnId(bilCashEntry, bilCashEntryTotRow);
        }
    }

    private void writeBilUnIdCashRow(BilUnIdCash bilUnIdCash, BilCashEntry bilCashEntry,
            BilCashEntryTot bilCashEntryTotRow) {
        bilUnIdCash.setBilEntrySeqNbr(bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
        bilUnIdCash.setBilAccountNbr(bilCashEntry.getBilAccountNbr());
        bilUnIdCash.setBilTtyNbr(bilCashEntry.getBilTtyNbr());
        bilUnIdCash.setBilAgtActNbr(bilCashEntry.getBilAgtActNbr());
        bilUnIdCash.setAgencyNbr(bilCashEntry.getAgencyNbr());
        bilUnIdCash.setBilAdditionalId(bilCashEntry.getBilAdditionalId());
        bilUnIdCash.setPolSymbolCd(bilCashEntry.getPolSymbolCd());
        bilUnIdCash.setBilAdjDueDt(bilCashEntry.getBilAdjDueDt());
        bilUnIdCash.setBilPblItemCd(bilCashEntry.getBilPblItemCd());
        bilUnIdCash.setBilRctAmt(bilCashEntry.getBilRctAmt());
        bilUnIdCash.setBilRctTypeCd(bilCashEntry.getBilRctTypeCd());
        bilUnIdCash.setBilRctId(bilCashEntry.getBilRctId());
        bilUnIdCash.setBilRctCltId(bilCashEntry.getBilRctCltId());
        bilUnIdCash.setBilRctAdrSeq(bilCashEntry.getBilRctAdrSeq());
        bilUnIdCash.setBilRctCmt(bilCashEntry.getBilRctCmt());
        bilUnIdCash.setBilRctReceiveDt(bilCashEntry.getBilRctReceiveDt());
        bilUnIdCash.setBilDspDt(dateService.currentDate());
        bilUnIdCash.setBilPayeeCltId(BLANK_STRING);
        bilUnIdCash.setBilPayeeAdrSeq(SHORT_ZERO);
        bilUnIdCash.setBilBankCd(bilCashEntryTotRow.getBilBankCd());
        bilUnIdCash.setBilDepositDt(bilCashEntryTotRow.getBilDepositDt());
        bilUnIdCash.setBilCshEtrMthCd(bilCashEntryTotRow.getBilCshEtrMthCd());
        bilUnIdCash.setBilCrcrdTypeCd(bilCashEntry.getBilCrcrdTypeCd());
        bilUnIdCash.setBilCrcrdExpDt(bilCashEntry.getBilCrcrdExpDt());
        bilUnIdCash.setBilCrcrdAutApv(bilCashEntry.getBilCrcrdAutApv());
        bilUnIdCash.setBilCrcrdArspCd(bilCashEntry.getBilCrcrdArspCd());
        bilUnIdCash.setBilCrcrdActNbr(bilCashEntry.getBilCrcrdActNbr());
        bilUnIdCash.setBilTaxYearNbr(bilCashEntry.getBilTaxYearNbr());
        bilUnIdCash.setBilCwaId(bilCashEntry.getBilCwaId());
        bilUnIdCash.setDdsDsbDt(DateRoutine.defaultDateTime());
        bilUnIdCash.setDwsStatusCd(BLANK_CHAR);
        bilUnIdCash.setBilDsbId(BLANK_STRING);
        bilUnIdCash.setBilChkPrdMthCd(BLANK_CHAR);
        bilUnIdCash.setActivityUserId(bilCashEntry.getBilCashEntryId().getUserId());
        bilUnIdCash.setBilPstmrkDt(bilCashEntry.getBilPstMrkDt());
        bilUnIdCash.setBilPstmrkCd(bilCashEntry.getBilPstMrkCd());
        bilUnIdCash.setBillUnidCashId(BLANK_STRING);
        bilUnIdCash.setAgencyNbr(BLANK_STRING);
        bilUnIdCash.setBilCrcrdArspTyc(bilCashEntry.getBilCrcrdArspTyc());
        bilUnIdCash.setBilDspTypeCd(BilDspTypeCode.SUSPENDED.getValue());
        bilUnIdCash.setDwsCkDrfNbr(SHORT_ZERO);
        bilUnIdCash.setBucRevsDsbInd(CHAR_N);
        bilUnIdCash.setBilAcyTs(dateService.currentDateTime());
        bilUnIdCashRepository.saveAndFlush(bilUnIdCash);

    }

    private void writeFinancilaApiForUnId(BilCashEntry bilCashEntry, BilCashEntryTot bilCashEntryTotRow) {

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();

        financialApiActivity.setFunctionCode(CHAR_A);
        financialApiActivity.setReferenceId(BLANK_STRING);
        financialApiActivity.setApplicationName(BCMOCR);
        financialApiActivity.setUserId(BLANK_STRING);
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setTransactionObjectCode(BilCashEntryMethodCode.UNIDENTIFIED.getValue());
        financialApiActivity.setTransactionActionCode(BilCashEntryMethodCode.SUSPENDED.getValue());
        financialApiActivity.setApplicationProductIdentifier(BCMS);
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(CHAR_Y);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        financialApiActivity.setOperatorId(BLANK_STRING);
        financialApiActivity.setAppProgramId(BCMOCR);
        financialApiActivity.setActivityAmount(bilCashEntry.getBilRctAmt());
        financialApiActivity.setActivityNetAmount(bilCashEntry.getBilRctAmt());
        financialApiActivity.setBilBankCode(bilCashEntryTotRow.getBilBankCd());
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        financialApiActivity.setBilDatabaseKey(getDatabaseKey(bilCashEntry.getBilCashEntryId().getBilEntryDt(),
                bilCashEntry.getBilCashEntryId().getBilEntryNbr(), bilCashEntry.getBilCashEntryId().getUserId(),
                bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr()));
        financialApiActivity.setOriginalEffectiveDate(BLANK_STRING);
        financialApiActivity.setBusinessCaseId(BLANK_STRING);
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setAgentTtyId(BLANK_STRING);
        if (StringUtils.isNotBlank(bilCashEntry.getBilAgtActNbr())) {
            financialApiActivity.setAgentTtyId(bilCashEntry.getBilAgtActNbr());
        } else if (StringUtils.isNotBlank(bilCashEntry.getBilTtyNbr())) {
            financialApiActivity.setAgentTtyId(bilCashEntry.getBilTtyNbr());
        }
        financialApiActivity.setBilAccountId(bilCashEntry.getBilAccountNbr());
        financialApiActivity.setBilReceiptTypeCode(bilCashEntry.getBilRctTypeCd());
        financialApiActivity.setBilReasonCode(BLANK_STRING);
        financialApiActivity.setClientId(BLANK_STRING);
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setCurrencyCode(bilCashEntryTotRow.getCurrencyCd());
        financialApiActivity.setDepositeBankCode(bilCashEntryTotRow.getBctDepBankCd());
        financialApiActivity.setDisbursementId(BLANK_STRING);
        financialApiActivity.setLineOfBusCode(BLANK_STRING);
        financialApiActivity.setMarketSectorCode(BLANK_STRING);
        financialApiActivity.setPayableItemCode(BLANK_STRING);
        financialApiActivity.setPolicyId(bilCashEntry.getPolNbr().trim());
        financialApiActivity.setStateCode(BLANK_STRING);
        financialApiActivity.setStatusCode(BLANK_CHAR);
        financialApiActivity.setBilSourceCode(String.valueOf(bilCashEntryTotRow.getBilCshEtrMthCd()));
        financialApiActivity.setCountryCode(BLANK_STRING);
        financialApiActivity.setBilTypeCode(BLANK_STRING);
        financialApiActivity.setBilClassCode(BLANK_STRING);

        insertBcmActivity(financialApiActivity);
    }

    private void insertBcmActivity(FinancialApiActivity financialApiActivity) {

        String[] fwsReturnMessage = null;

        fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                dateService.currentDate());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            logger.error("Error Occured in FinancialApi while writing BCM_BUS_ACTIVITY");
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
    }

    private String getDatabaseKey(ZonedDateTime entryDate, String entryNumber, String userId,
            short entrySequenceNumber) {

        StringBuilder databaseKey = new StringBuilder();
        databaseKey.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        databaseKey.append(StringUtils.rightPad(entryNumber, 4));
        databaseKey.append(StringUtils.rightPad(userId, 8));
        databaseKey.append(PLUS_SIGN);
        databaseKey.append(StringUtils.rightPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
        return databaseKey.toString();
    }

    private boolean validateIfDownPayment(String paymentType) {

        return busCdTranslationRepository.findByCodeAndType(paymentType, BusCodeTranslationType.DOWN_PAYMENT.getValue(),
                language) != null;
    }

    private boolean validateIfEquityPayment(String paymentType) {

        return busCdTranslationRepository.findByCodeAndType(paymentType, BilDesReasonType.EQT.getValue(),
                language) != null;
    }

    private Short getBilDtbSeqNbr(ZonedDateTime se3DateTime) {
        Short bilDtbSeqNbr = 0;
        bilDtbSeqNbr = bilUnIdCashRepository.findMaxSequenceNumber(se3DateTime);

        if (bilDtbSeqNbr == null) {
            bilDtbSeqNbr = 0;
        } else {
            bilDtbSeqNbr = (short) (bilDtbSeqNbr + 1);
        }
        return bilDtbSeqNbr;
    }

    private Boolean checkCashReceipt(BilCashEntry bilCashEntry, Boolean isReceiptFound) {
        if (StringUtils.isNotBlank(bilCashEntry.getBilAccountNbr())) {
            BilCashReceipt bilCashReceipt = bilCashReceiptRepository.findBilCashReciptRow(
                    bilCashEntry.getBilCashEntryId().getBilEntryDt(), bilCashEntry.getBilCashEntryId().getBilEntryNbr(),
                    bilCashEntry.getBilCashEntryId().getUserId(), bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
            if (bilCashReceipt != null) {
                isReceiptFound = true;
            }
        }

        if (StringUtils.isNotBlank(bilCashEntry.getBilTtyNbr())) {
            BilTtyCashRct bilTtyCashRct = bilTtyCashRctRepository.findBilTtyCashReceipt(
                    bilCashEntry.getBilCashEntryId().getBilEntryDt(), bilCashEntry.getBilCashEntryId().getBilEntryNbr(),
                    bilCashEntry.getBilCashEntryId().getUserId(), bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
            if (bilTtyCashRct != null) {
                isReceiptFound = true;
            }
        }

        if (StringUtils.isNotBlank(bilCashEntry.getBilAgtActNbr())) {
            BilAgtCashRct bilAgtCashRct = bilAgtCashRctRepository.findBilAgtCashReceipt(
                    bilCashEntry.getBilCashEntryId().getBilEntryDt(), bilCashEntry.getBilCashEntryId().getBilEntryNbr(),
                    bilCashEntry.getBilCashEntryId().getUserId(), bilCashEntry.getBilCashEntryId().getBilEntrySeqNbr());
            if (bilAgtCashRct != null) {
                isReceiptFound = true;
            }
        }
        return isReceiptFound;

    }

    private BilPolicyTerm getPolicyInformation(BilCashEntry bilCashEntry, String bilAccountId) {
        
        if (StringUtils.isNotBlank(bilCashEntry.getPolSymbolCd())) {
            return bilPolicyTermRepository.getBilPolicyTermByPolicyNumberAndPolicySymbol(bilAccountId,
                    bilCashEntry.getPolNbr(), bilCashEntry.getPolSymbolCd());
        } else {
            return bilPolicyTermRepository.getBilPolicyTermByPolicyNumber(bilAccountId,
                    bilCashEntry.getPolNbr().trim());
        }
        
    }

    private BilPolicyTerm getPolicyBillAccount(BilCashEntry bilCashEntry) {
        List<String> bilAccountIdList = null;
        String policyId = null;
        if (StringUtils.isNotBlank(bilCashEntry.getPolSymbolCd())) {
            bilAccountIdList = bilPolicyRepository.findAccountIdByPolicyNumberAndPolicySymbolCode(bilCashEntry.getPolNbr(),
                    bilCashEntry.getPolSymbolCd());
            if (bilAccountIdList != null && !bilAccountIdList.isEmpty()) {
                policyId = bilPolicyRepository.findPolicyIdByPolicyNumberAndPolicySymbolCode(bilCashEntry.getPolNbr(),
                        bilCashEntry.getPolSymbolCd(), bilAccountIdList.get(SHORT_ZERO));
            } 
        } else {
            bilAccountIdList = bilPolicyRepository.findAccountIdByPolicyNumber(bilCashEntry.getPolNbr());
            if (bilAccountIdList != null && !bilAccountIdList.isEmpty()) {
                policyId = bilPolicyRepository.findPolicyIdByPolicyNumber(bilCashEntry.getPolNbr(), bilAccountIdList.get(SHORT_ZERO));
            } 
        }
        if (policyId != null) {
            BilPolicyTerm bilPolicyTerm =  new BilPolicyTerm();
            BilPolicyTermId bilPolicyTermId = new BilPolicyTermId();
            bilPolicyTermId.setPolicyId(policyId);
            bilPolicyTermId.setBilAccountId(bilAccountIdList.get(SHORT_ZERO));
            bilPolicyTerm.setBillPolicyTermId(bilPolicyTermId);
            return bilPolicyTerm;
        } 
        return null;
    }
}
