package billing.service.impl;

import static billing.utils.BillingConstants.COMPANY_LOCATION_NUMBER;
import static billing.utils.BillingConstants.ERROR_CODE;
import static billing.utils.BillingConstants.MASTER_COMPANY_NUMBER;
import static billing.utils.BillingConstants.PLUS_SIGN;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilAgent;
import billing.data.entity.BilBank;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilThirdParty;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilAgentRepository;
import billing.data.repository.BilBankRepository;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.model.Batch;
import billing.model.ImmediateActivity;
import billing.model.Payment;
import billing.service.BatchPaymentService;
import billing.service.BatchService;
import billing.service.BusinessGroupService;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilFunctionCtl;
import billing.utils.BillingConstants.BilRules;
import billing.utils.PB360Service;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.application.AppAuthenticationException;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.model.Paging;
import core.security.service.SecurityService;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import core.utils.MultiFiltersSearch;
import core.utils.PageNavigation;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class BatchServiceImpl implements BatchService {

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private BilBankRepository bilBankRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private PB360Service pb360Service;

    @Autowired
    private DateService dateService;

    @Autowired
    private BusinessGroupService businessGroupService;

    @Autowired
    private BatchPaymentService batchPaymentService;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    private BilAgentRepository bilAgentRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Value("${multi.currency}")
    String multiCurrency;

    public static final Logger logger = LogManager.getLogger(BatchServiceImpl.class);
    private static final String STRING_SINCE_DATE = "1970-01-01";
    private static final String STRING_UNTIL_DATE = "9999-12-31";
    private static final String SINCE_DATE = "since";
    private static final String UNTIL_DATE = "until";
    private static final String TO_AMOUNT = "toAmount";
    private static final String FROM_AMOUNT = "fromAmount";
    private static final String CURRENCY = "currency";
    private static final String BATCH_ADMIN = "batchAdmin";
    private static final String BATCH_REVIEW_ADMIN = "batchReviewAdmin";
    private static final String BATCH_VIEW_ADMIN = "batchViewAdmin";
    private static final String USERID_UNATHORIZED_BATCHPAYMENT = "userId.unauthorized.batchPayment";

    @Override
    public Batch findBatchDetails(String batchId, String postedDate, String userId) {
        Batch batch;
        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(postedDate);
        boolean batchAdmin = true;
        boolean reviewAdmin = true;
        boolean viewAdmin = true;
        try {
            securityService.authenticateFunction(BATCH_ADMIN);
        } catch (AppAuthenticationException e) {
            batchAdmin = false;
            try {
                securityService.authenticateFunction(BATCH_REVIEW_ADMIN);
            } catch (AppAuthenticationException ex) {
                reviewAdmin = false;
                try {
                    securityService.authenticateFunction(BATCH_VIEW_ADMIN);
                } catch (AppAuthenticationException exe) {
                    viewAdmin = false;
                }
            }
        }

        BilCashEntryTot bilCashEntryTotRow = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(bilEntryDt, batchId, userId)).orElse(null);

        if (bilCashEntryTotRow == null) {
            throw new DataNotFoundException("batch.notfound", new Object[] { batchId });
        }

        String userName = securityService
                .getUserNameByUserSeqId(bilCashEntryTotRow.getBilCashEntryTotId().getUserId().trim());

        boolean isRuleEnabled = checkRuleEnabled(BilRules.BUSINESS_GROUP.getValue(), BLANK_STRING, BLANK_STRING,
                BLANK_STRING, execContext.getApplicationDate(), execContext.getApplicationDate());

        batch = mapBilCashEntryTotToBatch(bilCashEntryTotRow, userName, isRuleEnabled, batchAdmin, reviewAdmin,
                viewAdmin);

        return batch;
    }

    @Override
    public List<Batch> findAllBatches(String query, String filters, String sort, int size, int page, Paging paging) {
        List<Batch> batchList = new LinkedList<>();
        boolean batchAdmin = true;
        boolean reviewAdmin = true;
        boolean viewAdmin = true;
        int totalRows = 0;
        List<BilCashEntryTot> bilCashEntryTotRows = new ArrayList<>();
        Page<BilCashEntryTot> bilCashEntryTotPage = null;

        try {
            securityService.authenticateFunction(BATCH_ADMIN);
        } catch (AppAuthenticationException e) {
            batchAdmin = false;
            try {
                securityService.authenticateFunction(BATCH_REVIEW_ADMIN);
            } catch (AppAuthenticationException ex) {
                reviewAdmin = false;
                try {
                    securityService.authenticateFunction(BATCH_VIEW_ADMIN);
                } catch (AppAuthenticationException exe) {
                    viewAdmin = false;
                }
            }
        }

        if (filters != null && !filters.isEmpty()) {
            bilCashEntryTotPage = batchRowsByParams(page, size, filters, batchAdmin, reviewAdmin, viewAdmin);

        } else {
            bilCashEntryTotPage = bilCashEntryTotRepository
                    .getBilCashEntryTotRowsByPaging(execContext.getUserSeqeunceId(), PageRequest.of(page - 1, size));
        }

        if (bilCashEntryTotPage != null && bilCashEntryTotPage.getTotalElements() != 0) {
            totalRows = (int) bilCashEntryTotPage.getTotalElements();
            bilCashEntryTotRows = bilCashEntryTotPage.getContent();
        }

        if (bilCashEntryTotRows.isEmpty()) {
            return batchList;
        }

        boolean isRuleEnabled = checkRuleEnabled(BilRules.BUSINESS_GROUP.getValue(), BLANK_STRING, BLANK_STRING,
                BLANK_STRING, execContext.getApplicationDate(), execContext.getApplicationDate());

        for (BilCashEntryTot bilCashEntryTotRow : bilCashEntryTotRows) {
            String userName = securityService
                    .getUserNameByUserSeqId(bilCashEntryTotRow.getBilCashEntryTotId().getUserId().trim());
            batchList.add(mapBilCashEntryTotToBatch(bilCashEntryTotRow, userName, isRuleEnabled, batchAdmin,
                    reviewAdmin, viewAdmin));
        }

        PageNavigation.getPagingAttributes(totalRows, size, page, paging);
        return batchList;
    }

    private Page<BilCashEntryTot> batchRowsByParams(int page, int size, String filters, boolean batchAdmin,
            boolean reviewAdmin, boolean viewAdmin) {
        String since = BLANK_STRING;
        String until = BLANK_STRING;
        Double toAmount = null;
        Double fromAmount = null;
        String currency = null;
        boolean allBatches = false;

        Map<String, String> matchFilterMap = new HashMap<>();

        matchFilterMap = MultiFiltersSearch.getFilters(filters, matchFilterMap);

        if (matchFilterMap.get("allBatches") != null) {
            allBatches = Boolean.parseBoolean(matchFilterMap.get("allBatches"));
        }
        for (Map.Entry<String, String> entry : matchFilterMap.entrySet()) {
            String filter = entry.getKey();
            String match = entry.getValue();
            if (match != null) {
                if (filter.equalsIgnoreCase(SINCE_DATE)) {
                    since = match;
                } else if (filter.equalsIgnoreCase(UNTIL_DATE)) {
                    until = match;
                } else if (filter.equalsIgnoreCase(TO_AMOUNT)) {
                    toAmount = Double.valueOf(match);
                } else if (filter.equalsIgnoreCase(FROM_AMOUNT)) {
                    fromAmount = Double.valueOf(match);
                } else if (filter.equalsIgnoreCase(CURRENCY)) {
                    currency = match;
                }
            }
        }

        if (since.equals(BLANK_STRING) && until.equals(BLANK_STRING)) {
            since = STRING_SINCE_DATE;
            until = STRING_UNTIL_DATE;
        } else if (since.equals(BLANK_STRING) && !until.equals(BLANK_STRING)) {
            since = STRING_SINCE_DATE;
        } else if (!since.equals(BLANK_STRING) && until.equals(BLANK_STRING)) {
            until = STRING_UNTIL_DATE;
        }

        ZonedDateTime sinceDate = DateRoutine.dateTimeAsYYYYMMDD(since);
        ZonedDateTime untilDate = DateRoutine.dateTimeAsYYYYMMDD(until);
        if (sinceDate.compareTo(untilDate) > SHORT_ZERO) {
            throw new InvalidDataException("Invalid date Range");
        }
        if (allBatches) {
            if (batchAdmin || reviewAdmin || viewAdmin) {
                return getBatchWithFiltersWithAdmin(fromAmount, toAmount, sinceDate, untilDate, page, size, currency);
            } else {
                throw new AppAuthenticationException("userId.unauthorized.batch");
            }
        } else {
            return getBatchWithFilters(fromAmount, toAmount, sinceDate, untilDate, page, size, currency);
        }
    }

    private Page<BilCashEntryTot> getBatchWithFiltersWithAdmin(Double fromAmount, Double toAmount,
            ZonedDateTime sinceDate, ZonedDateTime untilDate, int page, int size, String currency) {
        Page<BilCashEntryTot> bilCashEntryTotPage = null;
        if (toAmount == null && fromAmount == null) {
            if (!sinceDate.equals(DateRoutine.defaultSinceDateTime())
                    || !untilDate.equals(DateRoutine.defaultSinceDateTime())) {
                if (currency != null && !currency.trim().isEmpty()) {
                    List<String> currencyList = getDefaultCurrencyCode(currency);
                    bilCashEntryTotPage = bilCashEntryTotRepository.getBilCashEntryTotRowsWithDateRangeAndCurrency(
                            sinceDate, untilDate, currencyList, PageRequest.of(page - 1, size));
                } else {
                    bilCashEntryTotPage = bilCashEntryTotRepository.getBilCashEntryTotRowsWithDateRange(sinceDate,
                            untilDate, PageRequest.of(page - 1, size));
                }

            }
        } else {
            if (toAmount != null && fromAmount == null) {
                fromAmount = DECIMAL_ZERO;
            } else if (toAmount == null) {
                toAmount = 999999999999.99;
            }

            if (!sinceDate.equals(DateRoutine.defaultSinceDateTime())
                    || !untilDate.equals(DateRoutine.defaultSinceDateTime())) {
                if (currency != null && !currency.trim().isEmpty()) {
                    List<String> currencyList = getDefaultCurrencyCode(currency);
                    bilCashEntryTotPage = bilCashEntryTotRepository.getRowsWithDateRangeAmountRangeAndCurrency(
                            sinceDate, untilDate, fromAmount, toAmount, currencyList, PageRequest.of(page - 1, size));
                } else {
                    bilCashEntryTotPage = bilCashEntryTotRepository.getRowsWithDateRangeAmountRange(sinceDate,
                            untilDate, fromAmount, toAmount, PageRequest.of(page - 1, size));
                }

            } else {
                if (currency != null && !currency.trim().isEmpty()) {
                    List<String> currencyList = getDefaultCurrencyCode(currency);
                    bilCashEntryTotPage = bilCashEntryTotRepository.getBilCashEntryTotRowsWithAmountRangeAndCurrency(
                            fromAmount, toAmount, currencyList, PageRequest.of(page - 1, size));
                } else {
                    bilCashEntryTotPage = bilCashEntryTotRepository.getBilCashEntryTotRowsWithAmountRange(fromAmount,
                            toAmount, PageRequest.of(page - 1, size));
                }

            }
        }

        return bilCashEntryTotPage;
    }

    private Page<BilCashEntryTot> getBatchWithFilters(Double fromAmount, Double toAmount, ZonedDateTime sinceDate,
            ZonedDateTime untilDate, int page, int size, String currency) {

        Page<BilCashEntryTot> bilCashEntryTotPage = null;
        if (toAmount == null && fromAmount == null) {
            if (!sinceDate.equals(DateRoutine.defaultSinceDateTime())
                    || !untilDate.equals(DateRoutine.defaultSinceDateTime())) {
                if (currency != null && !currency.trim().isEmpty()) {
                    List<String> currencyList = getDefaultCurrencyCode(currency);
                    bilCashEntryTotPage = bilCashEntryTotRepository.getBilCashEntryTotRowsWithDateRangeAndCurrency(
                            sinceDate, untilDate, execContext.getUserSeqeunceId(), currencyList,
                            PageRequest.of(page - 1, size));
                } else {
                    bilCashEntryTotPage = bilCashEntryTotRepository.getBilCashEntryTotRowsWithDateRange(sinceDate,
                            untilDate, execContext.getUserSeqeunceId(), PageRequest.of(page - 1, size));
                }

            }
        } else {

            if (toAmount != null && fromAmount == null) {
                fromAmount = DECIMAL_ZERO;
            } else if (toAmount == null) {
                toAmount = 999999999999.99;
            }

            if (!sinceDate.equals(DateRoutine.defaultSinceDateTime())
                    || !untilDate.equals(DateRoutine.defaultSinceDateTime())) {
                if (currency != null && !currency.trim().isEmpty()) {
                    List<String> currencyList = getDefaultCurrencyCode(currency);
                    bilCashEntryTotPage = bilCashEntryTotRepository.getRowsWithDateRangeAmountRangeAndCurrency(
                            sinceDate, untilDate, fromAmount, toAmount, execContext.getUserSeqeunceId(), currencyList,
                            PageRequest.of(page - 1, size));
                } else {
                    bilCashEntryTotPage = bilCashEntryTotRepository.getRowsWithDateRangeAmountRange(sinceDate,
                            untilDate, fromAmount, toAmount, execContext.getUserSeqeunceId(),
                            PageRequest.of(page - 1, size));
                }

            } else {
                if (currency != null && !currency.trim().isEmpty()) {
                    List<String> currencyList = getDefaultCurrencyCode(currency);
                    bilCashEntryTotPage = bilCashEntryTotRepository.getBilCashEntryTotRowsWithAmountRangeAndCurrency(
                            fromAmount, toAmount, execContext.getUserSeqeunceId(), currencyList,
                            PageRequest.of(page - 1, size));
                } else {
                    bilCashEntryTotPage = bilCashEntryTotRepository.getBilCashEntryTotRowsWithAmountRange(fromAmount,
                            toAmount, execContext.getUserSeqeunceId(), PageRequest.of(page - 1, size));
                }

            }
        }

        return bilCashEntryTotPage;
    }

    private Batch mapBilCashEntryTotToBatch(BilCashEntryTot bilCashEntryTotRow, String userName, boolean isRuleEnabled,
            boolean batchAdmin, boolean reviewAdmin, boolean viewAdmin) {
        Batch batch = new Batch();
        ZonedDateTime entryDt = bilCashEntryTotRow.getBilCashEntryTotId().getBilEntryDt();
        String entryNbr = bilCashEntryTotRow.getBilCashEntryTotId().getBilEntryNbr();
        String userId = bilCashEntryTotRow.getBilCashEntryTotId().getUserId().trim();
        batch.setPostedDate(entryDt);
        batch.setBatchId(entryNbr.trim());
        batch.setUser(userName);
        batch.setValidBatch(true);
        batch.setBatchAmount(bilCashEntryTotRow.getBctTotCashAmt());
        batch.setDepositDate(bilCashEntryTotRow.getBilDepositDt().compareTo(DateRoutine.defaultDateTime()) == 0 ? null
                : bilCashEntryTotRow.getBilDepositDt());
        batch.setIsEdit(false);
        batch.setIsDelete(false);
        batch.setIsAcceptAndDeposit(false);
        batch.setUserId(userId);
        batch.setIsDeposit(false);
        if (bilCashEntryTotRow.getBctAcceptInd() == CHAR_Y || bilCashEntryTotRow.getBctAcceptInd() == CHAR_D) {
            batch.setIsAccept(false);
        } else {
            batch.setIsAccept(true);
            batch.setIsAcceptAndDeposit(true);
        }
        if (Boolean.FALSE.equals(batch.getIsAccept())) {
            batch.setIsDeposit(bilCashEntryTotRow.getBctDepositInd() != CHAR_Y);
        }

        Double accumulatedAmt = getAccumulatedAmount(entryDt, entryNbr, userId);
        batch.setCashAmount(getCashAmount(entryDt, entryNbr, userId, accumulatedAmt));
        batch.setNonCashAmount(accumulatedAmt - batch.getCashAmount());
        batch.setAccumulatedAmount(accumulatedAmt);

        if (Boolean.FALSE.equals(batch.getIsAccept()) && bilCashEntryTotRow.getBctDepositInd() == CHAR_Y) {
            batch.setStatus("Deposited");
        } else if (Boolean.FALSE.equals(batch.getIsAccept()) && bilCashEntryTotRow.getBctDepositInd() == CHAR_N) {
            batch.setStatus("Accepted");
        } else {
            batch.setStatus("Pending");
        }
        if (batchAdmin || execContext.getUserSeqeunceId().equals(userId)) {
            batch.setIsEdit(true);
            batch.setIsDelete(true);
        } else if (reviewAdmin && !execContext.getUserSeqeunceId().equals(userId)) {
            batch.setIsAcceptAndDeposit(false);
            batch.setIsDeposit(false);
        } else if (viewAdmin && !execContext.getUserSeqeunceId().equals(userId)) {
            batch.setIsAcceptAndDeposit(false);
            batch.setIsDeposit(false);
            batch.setIsAccept(false);
        }
        if (bilCashEntryTotRow.getBctAcceptInd() == CHAR_Y || bilCashEntryTotRow.getBctAcceptInd() == CHAR_D) {
            batch.setIsDelete(false);
            batch.setIsEdit(false);
        }
        batch.setEnableAddPayment(false);
        if (execContext.getUserSeqeunceId().equals(userId)) {
            batch.setEnableAddPayment(true);
        }
        if (!bilCashEntryTotRow.getBctDepBankCd().trim().isEmpty()) {
            BilBank bilBank = bilBankRepository.findById(bilCashEntryTotRow.getBctDepBankCd()).orElse(null);
            if (bilBank == null) {
                batch.setDepositBank(BLANK_STRING);
            } else {
                batch.setDepositBank(bilBank.getBbkAccountNm().trim());
            }
        }

        if (!bilCashEntryTotRow.getBilBankCd().trim().isEmpty()) {
            BilBank bilBank = bilBankRepository.findById(bilCashEntryTotRow.getBilBankCd()).orElse(null);
            if (bilBank == null) {
                batch.setControlBank(BLANK_STRING);
            } else {
                batch.setControlBank(bilBank.getBbkAccountNm().trim());
                if (isRuleEnabled) {
                    batch.setBusinessGroup(businessGroupService.getBusinessGroup(BilFunctionCtl.BANK.toString(),
                            bilCashEntryTotRow.getBilBankCd()));

                }
            }
        }

        batch.setPaymentMethod(
                busCdTranslationRepository.findByCodeAndType(String.valueOf(bilCashEntryTotRow.getBilCshEtrMthCd()),
                        BilDesReasonType.PYS.getValue(), execContext.getLanguage()).getBusDescription().trim());
        Page<BilCashEntry> bilCashEntryPage = bilCashEntryRepository.getBilCashEntryRowsByPaging(entryDt, entryNbr,
                userId, PageRequest.of(0, 10));
        if (bilCashEntryPage != null && bilCashEntryPage.getTotalElements() != 0) {
            batch.setNumberOfTransactions((int) bilCashEntryPage.getTotalElements());
            if (Boolean.FALSE.equals(batch.getIsAccept())) {
                batch.setEnableAddPayment(false);
            }
        } else {
            batch.setNumberOfTransactions(0);
            batch.setValidBatch(true);
            batch.setIsAcceptAndDeposit(false);
            batch.setIsDeposit(false);
            batch.setIsAccept(false);
        }
        if (Boolean.TRUE.equals(Boolean.valueOf(multiCurrency))) {
            String reviewDescription = BLANK_STRING;
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType("CNM", "REV",
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                reviewDescription = busCdTranslation.getBusDescription().trim();
            }
            List<Payment> batchPaymentList = batchPaymentService.findAllBatchPayments(
                    DateRoutine.dateTimeAsYYYYMMDDString(entryDt), entryNbr, null, null, null, 9999, 1, new Paging(),
                    userId);
            if (batchPaymentList != null && !batchPaymentList.isEmpty()) {
                for (Payment batchPayment : batchPaymentList) {
                    if (batchPayment.getReviewCode().equals(reviewDescription)) {
                        batch.setValidBatch(false);
                        break;
                    }
                }
            }
        }

        batch.setCurrency(multiCurrencyService.getCurrencyCode(bilCashEntryTotRow.getCurrencyCd()));
        return batch;
    }

    private double getCashAmount(ZonedDateTime bilEntryDt, String bilEntryNumber, String userId,
            Double accumulatedAmt) {
        Double cashAmount = DECIMAL_ZERO;
        if (accumulatedAmt != null && accumulatedAmt > DECIMAL_ZERO) {
            List<BusCdTranslation> busCdTranslationList = busCdTranslationRepository
                    .findByBusCdTranslationIdBusType(BilDesReasonType.PYT.getValue());
            List<String> paymentTypes = new ArrayList<>();
            for (BusCdTranslation busCdTranslation : busCdTranslationList) {
                paymentTypes.add(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            }
            cashAmount = bilCashEntryRepository.findCashAmount(bilEntryDt, bilEntryNumber, userId, paymentTypes);
            if (cashAmount == null) {
                return DECIMAL_ZERO;
            }
        }
        return cashAmount;
    }

    private Double getAccumulatedAmount(ZonedDateTime bilEntryDt, String bilEntryNumber, String userId) {
        Double accumulatedAmount = bilCashEntryRepository.findAccumulatedAmount(bilEntryDt, bilEntryNumber, userId);
        return accumulatedAmount != null ? accumulatedAmount : DECIMAL_ZERO;
    }

    @Override
    public String save(Batch batch) {
        BilCashEntryTot bilCashEntryTot = saveBatch(batch);
        return bilCashEntryTot.getBilCashEntryTotId().getBilEntryNbr();
    }

    @Override
    public void scheduleBatch(String batchId, String postedDate, String userId) {
        StringBuilder parmListData = new StringBuilder();
        parmListData.append(StringUtils.rightPad("00", 2, BLANK_CHAR));
        parmListData.append(StringUtils.rightPad("", 10, BLANK_CHAR));
        parmListData.append(StringUtils.rightPad(postedDate, 10, BLANK_CHAR));
        parmListData.append(StringUtils.rightPad(batchId, 4, BLANK_CHAR));
        parmListData.append(StringUtils.rightPad(userId, 8, BLANK_CHAR));

        ImmediateActivity immediateActivity = new ImmediateActivity();
        immediateActivity.setActivityDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        immediateActivity.setModuleName("BCMOCR");
        immediateActivity.setParameter(parmListData.toString());
        immediateActivity.setReferenceId(batchId);
        immediateActivity.setUserId(execContext.getUserSeqeunceId());
        pb360Service.scheduleImmediateActivity(immediateActivity);

    }

    private void saveIntoFinancialApi(BilCashEntryTot bilCashEntryTot) {

        logger.debug("Save Into FinancialApi : START ");
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setFunctionCode(CHAR_A);
        financialApiActivity.setFolderId("BCWSCASH");
        financialApiActivity.setReferenceId(BLANK_STRING);
        financialApiActivity.setApplicationName(execContext.getUserSeqeunceId());
        financialApiActivity.setUserId(execContext.getUserSeqeunceId());
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setTransactionObjectCode(ObjectCode.CASH.toString());
        financialApiActivity.setTransactionActionCode("DEP");
        financialApiActivity
                .setApplicationProductIdentifier(FinancialIntegratorConstants.ApplicationProduct.BCMS.toString());
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(CHAR_Y);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(bilCashEntryTot.getBilDepositDt()));
        financialApiActivity.setOperatorId(bilCashEntryTot.getBilCashEntryTotId().getUserId());
        financialApiActivity.setAppProgramId("BACPDEP");
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setStateCode(BLANK_STRING);
        financialApiActivity.setCountryCode(BLANK_STRING);
        financialApiActivity.setLineOfBusCode(BLANK_STRING);
        financialApiActivity.setPolicyId(BLANK_STRING);
        financialApiActivity.setAgentId(BLANK_STRING);

        financialApiActivity.setActivityAmount(bilCashEntryTot.getBctTotCashAmt());
        financialApiActivity.setActivityNetAmount(bilCashEntryTot.getBctTotCashAmt());
        financialApiActivity.setCurrencyCode(bilCashEntryTot.getCurrencyCd());
        financialApiActivity.setOriginalEffectiveDate(BLANK_STRING);
        financialApiActivity.setBilBankCode(bilCashEntryTot.getBilBankCd());
        financialApiActivity.setDepositeBankCode(bilCashEntryTot.getBctDepBankCd());
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(bilCashEntryTot.getBilDepositDt()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(bilCashEntryTot.getBctAcceptDt()));
        financialApiActivity.setBilAccountId(BLANK_STRING);
        financialApiActivity.setBilReasonCode(BLANK_STRING);
        financialApiActivity.setBilReceiptTypeCode(BLANK_STRING);
        financialApiActivity.setBilTypeCode(BLANK_STRING);
        financialApiActivity.setBilClassCode(BLANK_STRING);
        financialApiActivity.setPayableItemCode(BLANK_STRING);

        ZonedDateTime fmsEntryDt = bilCashEntryTot.getBilCashEntryTotId().getBilEntryDt();
        String fmsEntryNbr = bilCashEntryTot.getBilCashEntryTotId().getBilEntryNbr();
        String fmsUserId = bilCashEntryTot.getBilCashEntryTotId().getUserId();
        short fmsSeqNbr = 0;

        financialApiActivity.setBilDatabaseKey(generateDatabaseKey(fmsEntryDt, fmsEntryNbr, fmsUserId, fmsSeqNbr));

        financialApiActivity.setAgentTtyId(BLANK_STRING);
        financialApiActivity.setBilSourceCode(Character.toString(bilCashEntryTot.getBilCshEtrMthCd()));

        String[] fwsReturnMessage = null;
        fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                execContext.getApplicationDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
        logger.debug("Save Into FinancialApi: END");
    }

    private String generateDatabaseKey(ZonedDateTime entryDate, String entryNumber, String userId,
            Short entrySequenceNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        sb.append(StringUtils.rightPad(entryNumber, 4));
        sb.append(StringUtils.rightPad(userId, 8));
        sb.append(PLUS_SIGN);
        sb.append(StringUtils.rightPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
        return sb.toString();
    }

    private BilCashEntryTot mapBatchToBilCashEntryTot(Batch batch) {
        BilCashEntryTot bilCashEntryTot = new BilCashEntryTot();
        ZonedDateTime bilEntryDt = batch.getPostedDate();
        bilCashEntryTot.setBilCashEntryTotId(
                new BilCashEntryTotId(bilEntryDt, batch.getBatchId(), execContext.getUserSeqeunceId()));
        bilCashEntryTot.setBctAcceptInd(CHAR_N);
        bilCashEntryTot.setBctDepBankCd(batch.getDepositBank());
        if (batch.getControlBank() == null) {
            bilCashEntryTot.setBilBankCd(batch.getDepositBank());
        } else {
            bilCashEntryTot.setBilBankCd(batch.getControlBank());
        }

        bilCashEntryTot.setBilDepositDt(batch.getDepositDate());
        bilCashEntryTot.setBctAcceptDt(execContext.getApplicationDate());
        bilCashEntryTot.setBctDepositInd(CHAR_N);
        bilCashEntryTot.setBilCshEtrMthCd(batch.getPaymentMethod().charAt(SHORT_ZERO));
        String currency = multiCurrencyService.getCurrencyCode(batch.getCurrency());
        bilCashEntryTot.setCurrencyCd(currency == null ? SEPARATOR_BLANK : currency);
        bilCashEntryTot.setBctTotCashAmt(batch.getBatchAmount());
        return bilCashEntryTot;
    }

    private BilCashEntryTot saveBatch(Batch batch) {
        BilCashEntryTot bilCashEntryTot = mapBatchToBilCashEntryTot(batch);
        bilCashEntryTotRepository.save(bilCashEntryTot);
        return bilCashEntryTot;
    }

    @Override
    public String deleteBatch(String batchId, String postedDate, String userId) {
        if (!userId.equals(execContext.getUserSeqeunceId())) {
            try {
                securityService.authenticateFunction(BATCH_ADMIN);
            } catch (AppAuthenticationException e) {
                throw new AppAuthenticationException(USERID_UNATHORIZED_BATCHPAYMENT);
            }
        }
        Page<BilCashEntry> bilCashEntryPage = bilCashEntryRepository.getBilCashEntryRowsByPaging(
                DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId, userId, PageRequest.of(0, 1));

        if (bilCashEntryPage != null && bilCashEntryPage.getTotalElements() != 0) {
            bilCashEntryRepository.deleteByBilCashEntryIdBilEntryDtAndBilCashEntryIdBilEntryNbrAndBilCashEntryIdUserId(
                    DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId, userId);
        }

        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId, userId))
                .orElse(null);
        if (bilCashEntryTot == null) {
            throw new DataNotFoundException("no.data.found");
        } else {
            if (bilCashEntryTot.getBctAcceptInd() == CHAR_Y || bilCashEntryTot.getBctAcceptInd() == CHAR_D) {
                throw new DataNotFoundException("batch.delete.not.valid");
            }
            bilCashEntryTotRepository
                    .deleteById(new BilCashEntryTotId(DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId, userId));
        }

        return "Success";
    }

    @Override
    public boolean patchBatch(String batchId, String postedDate, Batch batch, String userId) {

        if (!userId.equals(execContext.getUserSeqeunceId())) {
            try {
                securityService.authenticateFunction(BATCH_ADMIN);
            } catch (AppAuthenticationException e) {
                try {
                    securityService.authenticateFunction(BATCH_REVIEW_ADMIN);
                    if (Boolean.FALSE.equals(batch.getIsAccept()) || (Boolean.TRUE.equals(batch.getIsAccept())
                            && Boolean.TRUE.equals(batch.getIsDeposit()))) {
                        throw new AppAuthenticationException(USERID_UNATHORIZED_BATCHPAYMENT);
                    }
                } catch (AppAuthenticationException ex) {
                    throw new AppAuthenticationException(USERID_UNATHORIZED_BATCHPAYMENT);
                }
            }
        }

        BilCashEntryTot bilCashEntryTotRow = getExistingRow(batchId, postedDate, userId);
        ZonedDateTime entryDt = bilCashEntryTotRow.getBilCashEntryTotId().getBilEntryDt();
        String entryNbr = bilCashEntryTotRow.getBilCashEntryTotId().getBilEntryNbr();

        if (batch.getDepositBank() != null
                && !batch.getDepositBank().trim().equalsIgnoreCase(bilCashEntryTotRow.getBctDepBankCd())) {
            bilCashEntryTotRow.setBctDepBankCd(batch.getDepositBank().trim());
        }
        if (batch.getControlBank() != null && !batch.getControlBank().trim().isEmpty()
                && !batch.getControlBank().trim().equalsIgnoreCase(bilCashEntryTotRow.getBilBankCd())) {
            if (checkRuleEnabled(BilRules.BUSINESS_GROUP.getValue(), BLANK_STRING, BLANK_STRING, BLANK_STRING,
                    execContext.getApplicationDate(), execContext.getApplicationDate())) {

                List<BilCashEntry> bilCashEntries = bilCashEntryRepository.findPaymentsByEntryDtAndEntryNumber(entryDt,
                        batchId, userId);

                if (bilCashEntries != null && !bilCashEntries.isEmpty()
                        && checkIfIdentifiedPayementExist(bilCashEntries)) {

                    String businessGrpCdForExisting = businessGroupService
                            .getBusinessGroup(BilFunctionCtl.BANK.toString(), bilCashEntryTotRow.getBilBankCd());
                    String businessGrpCdForNewControlingBank = businessGroupService
                            .getBusinessGroup(BilFunctionCtl.BANK.toString(), batch.getControlBank());

                    if (businessGrpCdForExisting == null || businessGrpCdForNewControlingBank == null
                            || !businessGrpCdForNewControlingBank.equals(businessGrpCdForExisting)) {
                        throw new InvalidDataException("deposite.bank.not.valid");
                    }
                }
            }

            bilCashEntryTotRow.setBilBankCd(batch.getControlBank().trim());
        }
        if (batch.getCurrency() != null && !multiCurrencyService
                .compareCurrency(bilCashEntryTotRow.getCurrencyCd().trim(), batch.getCurrency().trim())) {
            Page<BilCashEntry> bilCashEntryPage = bilCashEntryRepository.getBilCashEntryRowsByPaging(entryDt, entryNbr,
                    bilCashEntryTotRow.getBilCashEntryTotId().getUserId(), PageRequest.of(0, 10));
            if (bilCashEntryPage != null && bilCashEntryPage.getTotalElements() != 0) {
                throw new InvalidDataException("batch.patch.currency.invalid");
            }
            String currency = multiCurrencyService.getCurrencyCode(batch.getCurrency());
            bilCashEntryTotRow.setCurrencyCd(currency == null ? SEPARATOR_BLANK : currency);
        }
        boolean isAccept = bilCashEntryTotRow.getBctAcceptInd() == CHAR_Y;
        if (!isAccept) {
            isAccept = bilCashEntryTotRow.getBctAcceptInd() == CHAR_D;
        }
        if (Boolean.TRUE.equals(batch.getIsAccept() != isAccept) && !isAccept) {
            Double accumulatedAmt = getAccumulatedAmount(entryDt, entryNbr, userId);
            if (accumulatedAmt.compareTo(bilCashEntryTotRow.getBctTotCashAmt()) != 0) {
                throw new InvalidDataException("batch.amount.not.valid");
            } else {
                bilCashEntryTotRow.setBctAcceptInd(Boolean.TRUE.equals(batch.getIsAccept()) ? CHAR_D : CHAR_N);
                bilCashEntryTotRow.setBctAcceptDt(execContext.getApplicationDate());
                if (batch.getBatchAmount() != DECIMAL_ZERO
                        && batch.getBatchAmount().compareTo(bilCashEntryTotRow.getBctTotCashAmt()) != 0) {
                    bilCashEntryTotRow.setBctTotCashAmt(batch.getBatchAmount());
                }
            }

        }

        if (bilCashEntryTotRow.getBctDepositInd() != CHAR_Y && Boolean.TRUE.equals(batch.getIsDeposit())) {
            Double accumulatedAmt = getAccumulatedAmount(entryDt, entryNbr, userId);
            if (accumulatedAmt.compareTo(bilCashEntryTotRow.getBctTotCashAmt()) != 0) {
                throw new InvalidDataException("batch.amount.not.valid");
            } else {
                bilCashEntryTotRow.setBctDepositInd(CHAR_Y);
                if (batch.getDepositDate() != null
                        && batch.getDepositDate().compareTo(bilCashEntryTotRow.getBilDepositDt()) != 0) {
                    bilCashEntryTotRow.setBilDepositDt(batch.getDepositDate());
                }
            }

        } else {
            if (batch.getDepositDate() != null) {
                bilCashEntryTotRow.setBilDepositDt(batch.getDepositDate());
            }

            if (batch.getBatchAmount() != DECIMAL_ZERO
                    && batch.getBatchAmount().compareTo(bilCashEntryTotRow.getBctTotCashAmt()) != 0) {
                bilCashEntryTotRow.setBctTotCashAmt(batch.getBatchAmount());
            }
        }

        bilCashEntryTotRepository.save(bilCashEntryTotRow);

        if (bilCashEntryTotRow.getBctDepositInd() == CHAR_Y && Boolean.TRUE.equals(batch.getIsDeposit())) {
            saveIntoFinancialApi(bilCashEntryTotRow);
        }

        return isAccept;

    }

    private BilCashEntryTot getExistingRow(String batchId, String postedDate, String userId) {
        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(postedDate);

        BilCashEntryTot bilCashEntryTotRow = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(bilEntryDt, batchId, userId)).orElse(null);
        if (bilCashEntryTotRow == null) {
            throw new DataNotFoundException("batch.notfound", new Object[] { batchId });
        }
        return bilCashEntryTotRow;
    }

    private boolean checkRuleEnabled(String ruleId, String bilTypeCd, String bilClassCd, String bilPlanCd,
            ZonedDateTime effectiveDt, ZonedDateTime expirationDt) {

        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(ruleId, bilTypeCd, bilClassCd, bilPlanCd,
                effectiveDt, expirationDt);
        return bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y;
    }

    private boolean checkIfIdentifiedPayementExist(List<BilCashEntry> bilCashEntries) {

        for (BilCashEntry bilCashEntry : bilCashEntries) {

            if (bilCashEntry.getBilAccountNbr().trim().compareTo(BLANK_STRING) != 0) {
                BilAccount bilAccount = bilAccountRepository
                        .findByAccountNumber(bilCashEntry.getBilAccountNbr().trim());

                if (bilAccount != null) {
                    return true;
                }

            } else if (bilCashEntry.getBilTtyNbr().trim().compareTo(BLANK_STRING) != 0) {
                BilThirdParty bilThirdParty = bilThirdPartyRepository
                        .findByBilTtyNbr(bilCashEntry.getBilTtyNbr().trim());
                if (bilThirdParty != null) {
                    return true;
                }

            } else if (bilCashEntry.getBilAgtActNbr() != null
                    && bilCashEntry.getBilAgtActNbr().trim().compareTo(BLANK_STRING) != 0) {
                BilAgent bilAgent = bilAgentRepository.findByBilAgtActNbr(bilCashEntry.getBilAgtActNbr().trim());

                if (bilAgent != null) {
                    return true;
                }
            } else if (bilCashEntry.getPolNbr() != null
                    && bilCashEntry.getPolNbr().trim().compareTo(BLANK_STRING) != 0) {
                List<BilPolicy> bilPolicyList = bilPolicyRepository.findByPolicyNumber(bilCashEntry.getPolNbr().trim());
                if (bilPolicyList != null && !bilPolicyList.isEmpty()) {
                    return true;
                }
            }

        }

        return false;

    }

    public List<String> getDefaultCurrencyCode(String currencyCode) {
        List<String> currencyList = new ArrayList<>();
        currencyList.add(currencyCode);
        if (currencyCode.trim().equals("USD")) {
            currencyList.add("");
        }
        return currencyList;
    }
}
