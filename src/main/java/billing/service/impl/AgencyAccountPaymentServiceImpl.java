package billing.service.impl;

import static billing.utils.BillingConstants.COMPANY_LOCATION_NUMBER;
import static billing.utils.BillingConstants.ERROR_CODE;
import static billing.utils.BillingConstants.MASTER_COMPANY_NUMBER;
import static billing.utils.BillingConstants.PLUS_SIGN;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAgent;
import billing.data.entity.BilAgtCashDsp;
import billing.data.entity.BilAgtCashRct;
import billing.data.entity.BilAgtSummary;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilAgtCashDspId;
import billing.data.entity.id.BilAgtCashRctId;
import billing.data.entity.id.BilAgtSummaryId;
import billing.data.repository.BilAgtCashDspRepository;
import billing.data.repository.BilAgtCashRctRepository;
import billing.data.repository.BilAgtSummaryRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.service.AgencyAccountPaymentService;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class AgencyAccountPaymentServiceImpl implements AgencyAccountPaymentService {

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private BilAgtCashRctRepository bilAgtCashRctRepository;

    @Autowired
    private BilAgtCashDspRepository bilAgtCashDspRepository;

    @Autowired
    private BilAgtSummaryRepository bilAgtSummaryRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private FinancialApiService financialApiService;

    @Override
    public void applyUnidentifiedCash(BilAgent bilAgent) {
        List<BilUnIdCash> unidentifiedCashList = bilUnIdCashRepository.findUnidentifiedPayment(SEPARATOR_BLANK,
                SEPARATOR_BLANK, bilAgent.getBilAgtActNbr(), SEPARATOR_BLANK, BilDspTypeCode.SUSPENDED.getValue());
        if (unidentifiedCashList != null && !unidentifiedCashList.isEmpty()) {
            for (BilUnIdCash unidentifiedCash : unidentifiedCashList) {
                BilAgtCashRct bilAgtCashReceipt = saveAgentReceipt(bilAgent.getBilAgtActId(),
                        execContext.getApplicationDate(), unidentifiedCash);

                saveAgencyDisposition(bilAgent.getBilAgtActId(), execContext.getApplicationDate(), unidentifiedCash,
                        bilAgtCashReceipt.getBilAgtCashRctId().getBilDtbSeqNbr());

                saveFinancialApi(bilAgent.getBilAgtActId(), execContext.getApplicationDate(), unidentifiedCash,
                        bilAgent.getBilAgtActNbr(), BLANK_STRING, BLANK_STRING, ObjectCode.UNIDENTIFIED_CASH.toString(),
                        unidentifiedCash.getBilRctAmt(), bilAgtCashReceipt.getBilEntryDt(),
                        bilAgtCashReceipt.getBilEntryNbr(), bilAgtCashReceipt.getUserId(),
                        bilAgtCashReceipt.getBilEntrySeqNbr(), bilAgent.getCurrencyCd());

                saveAgencySummary(bilAgent.getBilAgtActId(), unidentifiedCash.getBilPstmrkDt(),
                        unidentifiedCash.getBilRctTypeCd(), unidentifiedCash.getBilRctAmt(),
                        unidentifiedCash.getBilUnIdCashId().getUserId());

                bilUnIdCashRepository.delete(unidentifiedCash);
            }
        }

    }

    private void saveAgencySummary(String accountId, ZonedDateTime postMarkDate, String receivedTypeCode, double amount,
            String userId) {
        BilAgtSummary bilAgtSummary = new BilAgtSummary();
        Short sequenceNumber = getIncrementalValue(
                bilAgtSummaryRepository.getMaxSeqNumber(accountId, execContext.getApplicationDate()));
        bilAgtSummary
                .setBilAgtSummaryId(new BilAgtSummaryId(accountId, execContext.getApplicationDate(), sequenceNumber));
        bilAgtSummary.setBilDesReaTyp(findPaymentTypeDescription(receivedTypeCode));
        bilAgtSummary.setBilAcyDes1Dt(DateRoutine.defaultDateTime());
        bilAgtSummary.setBilAcyTs(execContext.getApplicationDateTime());
        bilAgtSummary.setBilAcyDesCd(receivedTypeCode);
        bilAgtSummary.setBilAcyDes2Dt(postMarkDate);
        bilAgtSummary.setBilAcyAmt(amount);
        bilAgtSummary.setUserId(userId);
        bilAgtSummaryRepository.save(bilAgtSummary);
    }

    private void saveFinancialApi(String accountId, ZonedDateTime distributionDate, BilUnIdCash unidentifiedCash,
            String accountNumber, String bilTypeCode, String bilClassCode, String transactionObjectCode, double amount,
            ZonedDateTime entryDate, String entryNumber, String userId, short entrySequenceNumber,
            String currencyCode) {
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setFolderId(accountId);
        financialApiActivity.setApplicationName(ApplicationName.BACPDUC.toString());
        financialApiActivity.setFunctionCode(CHAR_A);
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setTransactionActionCode(ActionCode.SUSPENSE.toString());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity
                .setApplicationProductIdentifier(FinancialIntegratorConstants.ApplicationProduct.BCMS.toString());
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(distributionDate));
        financialApiActivity.setAppProgramId(ApplicationName.BACPDUC.toString());
        financialApiActivity.setBilBankCode(unidentifiedCash.getBilBankCd());
        financialApiActivity.setAgentTtyId(accountNumber);

        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setBilReceiptTypeCode(unidentifiedCash.getBilRctTypeCd());
        financialApiActivity.setBilTypeCode(bilTypeCode);
        financialApiActivity.setBilClassCode(bilClassCode);
        financialApiActivity
                .setBilDatabaseKey(generateDatabaseKey(entryDate, entryNumber, userId, entrySequenceNumber));
        financialApiActivity.setBilSourceCode(String.valueOf(unidentifiedCash.getBilCshEtrMthCd()));
        financialApiActivity.setSummaryEffectiveDate(DateRoutine.defaultDateTime());
        financialApiActivity.setOriginalTimeStamp(execContext.getApplicationDateTime());
        financialApiActivity.setBilReasonCode(unidentifiedCash.getBilDspReasonCd());
        financialApiActivity.setCurrencyCode(currencyCode);

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                execContext.getApplicationDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
    }

    private String generateDatabaseKey(ZonedDateTime entryDate, String entryNumber, String userId,
            short entrySequenceNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        sb.append(StringUtils.rightPad(entryNumber, 4));
        sb.append(StringUtils.rightPad(userId, 8));
        sb.append(PLUS_SIGN);
        sb.append(StringUtils.rightPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
        return sb.toString();
    }

    private BilAgtCashDsp saveAgencyDisposition(String accountId, ZonedDateTime distributionDate,
            BilUnIdCash unidentifiedCash, short distributionSequenceNumber) {
        BilAgtCashDsp agencyCashDisposition = new BilAgtCashDsp();
        agencyCashDisposition.setBilAgtCashDspId(new BilAgtCashDspId(accountId, distributionDate,
                distributionSequenceNumber, getIncrementalValue(bilAgtCashDspRepository
                        .findMaxDispositionNumber(accountId, distributionDate, distributionSequenceNumber))));
        agencyCashDisposition.setBilAdjDueDt(unidentifiedCash.getBilAdjDueDt());
        agencyCashDisposition.setUserId(unidentifiedCash.getBilUnIdCashId().getUserId());
        agencyCashDisposition.setBilDspDt(unidentifiedCash.getBilDspDt());
        agencyCashDisposition.setBilDspTypeCd(unidentifiedCash.getBilDspTypeCd());
        agencyCashDisposition.setBilDspReasonCd(unidentifiedCash.getBilDspReasonCd());
        agencyCashDisposition.setBilDspAmt(unidentifiedCash.getBilRctAmt());
        agencyCashDisposition.setBilPayeeCltId(unidentifiedCash.getBilPayeeCltId());
        agencyCashDisposition.setBilPayeeAdrSeq(unidentifiedCash.getBilPayeeAdrSeq());
        agencyCashDisposition.setBilManualSusInd(unidentifiedCash.getBilManualSusInd());
        agencyCashDisposition.setDwsStatusCd(unidentifiedCash.getDwsStatusCd());
        agencyCashDisposition.setBilDsbId(unidentifiedCash.getBilDsbId());
        agencyCashDisposition.setBilChkPrdMthCd(unidentifiedCash.getBilChkPrdMthCd());
        agencyCashDisposition.setBilDsbDt(DateRoutine.defaultDateTime());
        bilAgtCashDspRepository.saveAndFlush(agencyCashDisposition);
        return agencyCashDisposition;
    }

    private BilAgtCashRct saveAgentReceipt(String accountId, ZonedDateTime distributionDate,
            BilUnIdCash unidentifiedCash) {
        BilAgtCashRct agencyReceipt = new BilAgtCashRct();
        agencyReceipt.setBilAgtCashRctId(new BilAgtCashRctId(accountId, distributionDate,
                getIncrementalValue(bilAgtCashRctRepository.getMaxBilDtbSequenceNumber(accountId, distributionDate))));
        agencyReceipt.setBilPstmrkCd(unidentifiedCash.getBilPstmrkCd());
        agencyReceipt.setBilEntryDt(unidentifiedCash.getBilUnIdCashId().getBilEntryDt());
        agencyReceipt.setBilEntryNbr(unidentifiedCash.getBilUnIdCashId().getBilEntryNbr());
        agencyReceipt.setBilEntrySeqNbr(unidentifiedCash.getBilEntrySeqNbr());
        agencyReceipt.setUserId(unidentifiedCash.getBilUnIdCashId().getUserId());
        agencyReceipt.setBilRctAmt(unidentifiedCash.getBilRctAmt());
        agencyReceipt.setBilRctTypeCd(unidentifiedCash.getBilRctTypeCd());
        agencyReceipt.setBilRctId(unidentifiedCash.getBilRctId());
        agencyReceipt.setBilRctCmt(unidentifiedCash.getBilRctCmt());
        agencyReceipt.setBilTrfTypeCd(BLANK_CHAR);
        agencyReceipt.setBilRctReceive(unidentifiedCash.getBilRctReceiveDt());
        agencyReceipt.setBilBankCd(unidentifiedCash.getBilBankCd());
        agencyReceipt.setBilCrcrdTypeCd(unidentifiedCash.getBilCrcrdTypeCd());
        agencyReceipt.setBilCrcrdActNbr(unidentifiedCash.getBilCrcrdActNbr());
        agencyReceipt.setBilCrcrdExpDt(unidentifiedCash.getBilCrcrdExpDt());
        agencyReceipt.setBilCrcrdAutApv(unidentifiedCash.getBilCrcrdAutApv());
        agencyReceipt.setBilCshEtrMthCd(unidentifiedCash.getBilCshEtrMthCd());
        agencyReceipt.setBilTaxYearNbr(unidentifiedCash.getBilTaxYearNbr());
        agencyReceipt.setBilPstmrkDt(unidentifiedCash.getBilPstmrkDt());
        agencyReceipt.setBilDepositDt(unidentifiedCash.getBilDepositDt());
        agencyReceipt.setBilRctCmt(unidentifiedCash.getBilRctCmt());
        bilAgtCashRctRepository.save(agencyReceipt);
        return agencyReceipt;
    }

    private String findPaymentTypeDescription(String receiptTypeCode) {
        List<BusCdTranslation> busCodeTranslation = busCdTranslationRepository.findAllByCodeAndTypeList(receiptTypeCode,
                Arrays.asList(BusCodeTranslationType.PAYMENT_TYPE_CODE.getValue(),
                        BusCodeTranslationType.CASH_PAYMENT.getValue(),
                        BusCodeTranslationType.COLLECT_AGENCY_PAYMENT.getValue(),
                        BusCodeTranslationType.EXTERNAL_BANK_PAYMENT.getValue(),
                        BilDesReasonType.DISPOSITION_REASON.getValue(), BilDesReasonType.ONE_XA.getValue(),
                        BusCodeTranslationType.OTHER_PAYMENT.getValue()),
                execContext.getLanguage());
        if (busCodeTranslation != null && !busCodeTranslation.isEmpty()) {
            receiptTypeCode = busCodeTranslation.get(0).getBusCdTranslationId().getBusType().trim();
        }
        return receiptTypeCode;
    }

    private Short getIncrementalValue(Short sequenceNumber) {
        if (sequenceNumber == null) {
            return SHORT_ZERO;
        } else {
            return (short) (sequenceNumber + 1);
        }
    }
}
