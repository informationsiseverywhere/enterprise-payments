package billing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.TransRateHst;
import billing.data.entity.id.TransRateHstId;
import billing.data.repository.TransRateHstRepository;
import billing.model.TransactionRateHistory;
import billing.service.TransactionRateHistoryService;
import core.api.ExecContext;
import core.utils.DateRoutine;

@Service
public class TransactionRateHistoryServiceImpl implements TransactionRateHistoryService {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private TransRateHstRepository transRateHstRepository;

    @Override
    public void postTransactionRateHistory(String batchId, String postedDate, short paymentSeqNumber,
            TransactionRateHistory transactionRateHistory) {
        TransRateHst transRateHst = mapTransactionRateHistoryToTransRateHst(batchId, postedDate, paymentSeqNumber,
                transactionRateHistory);
        transRateHstRepository.save(transRateHst);
    }

    private TransRateHst mapTransactionRateHistoryToTransRateHst(String batchId, String postedDate,
            short paymentSeqNumber, TransactionRateHistory transactionRateHistory) {
        TransRateHst transRateHst = new TransRateHst();
        TransRateHstId transRateHstId = new TransRateHstId();
        transRateHstId.setBilEntryDt(DateRoutine.dateTimeAsYYYYMMDD(postedDate));
        transRateHstId.setBilEntryNbr(batchId);
        transRateHstId.setBilEntrySeqNbr(paymentSeqNumber);
        transRateHstId.setUserId(execContext.getUserSeqeunceId());
        transRateHst.setTransRateHstId(transRateHstId);
        transRateHst.setPaymentCurrency(transactionRateHistory.getPaymentCurrency());
        transRateHst.setPaymentAmount(transactionRateHistory.getPaymentAmount());
        transRateHst.setReceivedCurrency(transactionRateHistory.getReceivedCurrency());
        transRateHst.setReceivedAmount(transactionRateHistory.getReceivedAmount());
        transRateHst.setRate(transactionRateHistory.getRate());
        return transRateHst;
    }

    @Override
    public TransactionRateHistory findTransactionRateHistory(String batchId, String postedDate,
            short paymentSeqNumber) {
        TransRateHst transRateHst = transRateHstRepository
                .findById(new TransRateHstId(DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId,
                        execContext.getUserSeqeunceId(), paymentSeqNumber))
                .orElse(null);
        if (transRateHst != null) {
            return mapTransRateHstToTransactionRateHistory(transRateHst);
        }
        return null;
    }

    private TransactionRateHistory mapTransRateHstToTransactionRateHistory(TransRateHst transRateHst) {
        TransactionRateHistory transactionRateHistory = new TransactionRateHistory();
        transactionRateHistory.setPaymentAmount(transRateHst.getPaymentAmount());
        transactionRateHistory.setPaymentCurrency(transRateHst.getPaymentCurrency().trim());
        transactionRateHistory.setReceivedAmount(transRateHst.getReceivedAmount());
        transactionRateHistory.setReceivedCurrency(transRateHst.getReceivedCurrency().trim());
        transactionRateHistory.setRate(transRateHst.getRate());
        return transactionRateHistory;
    }

    @Override
    public void deleteTransactionRateHistory(String batchId, String postedDate, short paymentSeqNumber) {
        TransRateHst transRateHst = transRateHstRepository
                .findById(new TransRateHstId(DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId,
                        execContext.getUserSeqeunceId(), paymentSeqNumber))
                .orElse(null);
        if (transRateHst != null) {
            transRateHstRepository.delete(transRateHst);
        }

    }
}
