package billing.service.impl;

import static billing.utils.BillingConstants.ERROR_CODE;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_20;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_4;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.text.DecimalFormat;
import java.time.LocalTime;
import java.time.ZonedDateTime;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilBankEntry;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilEftExtPyt;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.BilMstCoSt;
import billing.data.entity.BilOrgSupport;
import billing.data.entity.BilRulesUct;
import billing.data.entity.HolidayPrc;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilBankEntryId;
import billing.data.entity.id.BilCashDspId;
import billing.data.entity.id.BilCashEntryId;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.entity.id.BilEftExtPytId;
import billing.data.entity.id.BilEftPendingTapeId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilBankEntryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilEftExtPytRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilMstCoStRepository;
import billing.data.repository.BilOrgSupportRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.HolidayPrcRepository;
import billing.model.CashApplyDriver;
import billing.model.Payment;
import billing.service.BilRulesUctService;
import billing.service.CashApplyDriverService;
import billing.service.CredicardService;
import billing.service.PaymentService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.BilCashEntryMethodCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BillingMethod;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.PayableItemCode;
import billing.utils.BillingConstants.PaymentType;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.data.entity.id.BusCdTranslationId;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import core.utils.IdGenerator;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;

@Service("billAccountPaymentService")
public class BillAccountPaymentServiceImpl {

    private static final String RECURRING_TYPE = "RECUR";

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private BilBankEntryRepository bilBankEntryRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilOrgSupportRepository bilOrgSupportRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private BilMstCoStRepository bilMstCoStRepository;

    @Autowired
    private HolidayPrcRepository holidayPrcRepository;

    @Autowired
    private CashApplyDriverService cashApplyDriverService;

    @Autowired
    private BillAccountRecurringPaymentServiceImpl billAccountRecurringPaymentServiceImpl;

    @Autowired
    private DateService dateService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private CredicardService credicardService;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    @Autowired
    private BilEftExtPytRepository bilEftExtPytRepository;

    @Value("${onetimepayment.integration}")
    String oneTimePaymentIntegration;

    @Value("${paymentIntegration.provider}")
    String providerIntegration;

    @Value("${autopayment.integration}")
    private String autoPaymentIntegration;

    private static final String RULE_XHOL = "XHOL";
    private static final String HOLIDAY_8 = "H8";
    private static final String HOLIDAY_6 = "H6";
    private static final String HOLIDAY_5 = "H5";
    private static final String HOLIDAY_3 = "H3";
    private static final String HOLIDAY_1 = "H1";
    private static final String AGENCY_SWEEP = "ASW";
    private static final String AGENCY_SWEEP_RULE = "AGSW";
    private static final String TYPE_FORM_CD = "ACH";

    public Payment makeAPayment(Payment payment) {

        ZonedDateTime se3DateTime = dateService.currentDate();
        short fileTypeCode = 0;
        String bilBankCode;
        String bilEntryNumber;
        Short bilDtbSeqNbr = 0;
        char arspCode;
        String autApv;
        String crcrdArspTyc;
        Integer bilCashEntrySeqNbr = 0;
        String orderId;
        ZonedDateTime tapeDate = se3DateTime;
        boolean isPaymentIntegration = false;
        boolean isOneTimePaymentIntegration = false;
        boolean isAutoPaymentIntegration = false;

        if (StringUtils.isNotBlank(providerIntegration)) {
            isPaymentIntegration = true;
            isOneTimePaymentIntegration = BooleanUtils.toBoolean(oneTimePaymentIntegration);
            isAutoPaymentIntegration = BooleanUtils.toBoolean(autoPaymentIntegration);
        }

        bilEntryNumber = getUniqueBilEntryNumber(se3DateTime);
        payment.setUserId(execContext.getUserSeqeunceId());
        bilCashEntrySeqNbr = getBilEntrySequenceNumber(bilEntryNumber, se3DateTime, payment.getUserId());
        payment.setBatchId(bilEntryNumber);
        payment.setPaymentSequenceNumber(bilCashEntrySeqNbr.shortValue());
        BilAccount bilAccount = bilAccountRepository.findById(payment.getAccountId()).orElse(null);
        String currencyCode = multiCurrencyService.getCurrencyCode(bilAccount.getCurrencyCode().trim());

        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
            fileTypeCode = getFileTypeCodeFromBilRuleUct(se3DateTime);
            bilBankCode = getBankCode(payment.getPaymentType(), BilDesReasonType.ECH.getValue(), fileTypeCode);
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            fileTypeCode = determineFileTypeCode(null,
                    isOneTimePaymentIntegration ? payment.getPaymentType() : BLANK_STRING, "1XA");
            bilBankCode = getBankCode(BLANK_STRING, "ACH", fileTypeCode);
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            String agencySweepParam = getAgencySweepFileTypeCode(bilAccount, payment);
            fileTypeCode = Short.valueOf(agencySweepParam.substring(0, 3));
            tapeDate = getTapeDate(tapeDate, agencySweepParam);
            bilBankCode = getBankCode(BLANK_STRING, "ACH", fileTypeCode);
        } else {
            fileTypeCode = determineFileTypeCode(null, payment.getPaymentType(), null);
            bilBankCode = getBankCode(BLANK_STRING, BilDesReasonType.DIGITAL_WALLET_HOUSE.getValue(), fileTypeCode);
        }
        if (Boolean.TRUE.equals(payment.getIsRecurring()) && isAutoPaymentIntegration) {
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
                fileTypeCode = readRecurringFileType(fileTypeCode, BillingMethod.ECC.toString(),
                        bilAccount.getBillTypeCd().trim());
            } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
                fileTypeCode = readRecurringFileType(fileTypeCode, BillingMethod.EFT.toString(),
                        bilAccount.getBillTypeCd().trim());
                bilBankCode = getBankCode(BLANK_STRING, "ACH", fileTypeCode);
            } else {
                BusCdTranslation busCdTranslation = busCdTranslationRepository
                        .findById(new BusCdTranslationId(BillingMethod.ELECTRONIC_WALLET.toString(), SEPARATOR_BLANK,
                                execContext.getLanguage(), payment.getPaymentType()))
                        .orElse(null);
                if (busCdTranslation != null) {
                    fileTypeCode = Short.valueOf(busCdTranslation.getBusDescription().trim());
                }
                bilBankCode = getBankCode(BLANK_STRING, BilDesReasonType.DIGITAL_WALLET_HOUSE.getValue(), fileTypeCode);
            }
        }

        if (payment.getTransactionId() == null) {
            orderId = generateOrderId(bilAccount.getAccountNumber().trim(), payment.getPaymentType());
        } else {
            orderId = payment.getTransactionId();
        }

        if (payment.getAuthorization() == null) {
            autApv = BLANK_STRING;
        } else {
            autApv = payment.getAuthorization();
        }
        if (payment.getAuthorizationResponse() == null) {
            arspCode = BLANK_CHAR;
        } else {
            arspCode = payment.getAuthorizationResponse().charAt(SHORT_ZERO);
        }

        if (payment.getAuthorizationResponseType() == null) {
            crcrdArspTyc = BLANK_STRING;
        } else {
            crcrdArspTyc = payment.getAuthorizationResponseType();
        }

        bilDtbSeqNbr = getBilDtbSeqNbr(payment.getAccountId(), se3DateTime);

        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {

            insertBillCashReceipt(payment, se3DateTime, bilDtbSeqNbr, bilCashEntrySeqNbr, bilEntryNumber, bilBankCode,
                    autApv);

            insertBilCashDsp(payment, se3DateTime, bilDtbSeqNbr);

            insertBilActSummary(payment, se3DateTime, tapeDate, bilDtbSeqNbr);

            insertBilEFtPendingTape(payment, bilDtbSeqNbr, tapeDate, se3DateTime, fileTypeCode, crcrdArspTyc, orderId);

            insertBilBankEntry(payment, bilCashEntrySeqNbr, bilEntryNumber, se3DateTime);

            insertBilCashEntryTotal(payment, se3DateTime, bilEntryNumber, bilBankCode, currencyCode);

            insertBilCashEntry(payment, se3DateTime, bilEntryNumber, bilCashEntrySeqNbr,
                    bilAccount.getAccountNumber().trim(), arspCode, autApv, crcrdArspTyc, orderId);

            paymentService.insertAccountBalance(bilAccount.getAccountId().trim(), bilAccount.getAccountNumber().trim(),
                    bilAccount.getBillTypeCd().trim(), bilAccount.getStatus(), se3DateTime);
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            orderId = SEPARATOR_BLANK;
            payment.setPaymentId(BLANK_STRING);
            if (isPaymentIntegration) {
                payment.setPaymentId(payment.getPaymentProfileId());
            }

            insertBilCashEntryTotal(payment, se3DateTime, bilEntryNumber, bilBankCode, currencyCode);

            insertBilCashEntry(payment, se3DateTime, bilEntryNumber, bilCashEntrySeqNbr,
                    bilAccount.getAccountNumber().trim(), arspCode, autApv, crcrdArspTyc, orderId);

            insertBillCashReceipt(payment, se3DateTime, bilDtbSeqNbr, bilCashEntrySeqNbr, bilEntryNumber, bilBankCode,
                    autApv);

            insertBilCashDsp(payment, se3DateTime, bilDtbSeqNbr);

            insertBilEFtPendingTape(payment, bilDtbSeqNbr, tapeDate, se3DateTime, fileTypeCode, crcrdArspTyc, orderId);

            insertBilActSummary(payment, se3DateTime, tapeDate, bilDtbSeqNbr);

            paymentService.insertAccountBalance(bilAccount.getAccountId().trim(), bilAccount.getAccountNumber().trim(),
                    bilAccount.getBillTypeCd().trim(), bilAccount.getStatus(), se3DateTime);
        } else {
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())
                    && !isPaymentIntegration) {
                credicardService.processCreditRespone(payment);
            }
            insertBilCashEntryTotal(payment, se3DateTime, bilEntryNumber, bilBankCode, currencyCode);

            insertBilCashEntry(payment, se3DateTime, bilEntryNumber, bilCashEntrySeqNbr,
                    bilAccount.getAccountNumber().trim(), arspCode, autApv, crcrdArspTyc, orderId);

            insertBillCashReceipt(payment, se3DateTime, bilDtbSeqNbr, bilCashEntrySeqNbr, bilEntryNumber, bilBankCode,
                    autApv);

            insertBilCashDsp(payment, se3DateTime, bilDtbSeqNbr);

            insertBilEFtPendingTape(payment, bilDtbSeqNbr, tapeDate, se3DateTime, fileTypeCode, crcrdArspTyc, orderId);

            insertBilActSummary(payment, se3DateTime, tapeDate, bilDtbSeqNbr);

            paymentService.insertAccountBalance(bilAccount.getAccountId().trim(), bilAccount.getAccountNumber().trim(),
                    bilAccount.getBillTypeCd().trim(), bilAccount.getStatus(), se3DateTime);

        }

        callFinancialApi(payment.getPaymentAmount(), dateService.currentDate(), bilAccount.getAccountId(),
                payment.getPaymentSequenceNumber(), bilBankCode, se3DateTime, bilEntryNumber, payment.getUserId(),
                payment.getIdentifier(), BilCashEntryMethodCode.CASH.getValue(), PayableItemCode.SUS.getValue(),
                BLANK_STRING, payment.getPaymentMethod(), payment.getPaymentType(), payment.getSuspenseReason());

        cashApplyProcess(bilAccount.getAccountId().trim(), se3DateTime);

        if (Boolean.TRUE.equals(payment.getIsRecurring())) {
            billAccountRecurringPaymentServiceImpl.processRecurringPayments(payment);
        }

        payment.setPostedDate(se3DateTime);
        payment.setUserId(execContext.getUserId());
        payment.setIsUnidentified(false);
        return payment;
    }

    private short readRecurringFileType(short fileTypeCode, String eftType, String bilTypeCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository
                .findById(new BusCdTranslationId(eftType, RECURRING_TYPE, execContext.getLanguage(), SEPARATOR_BLANK))
                .orElse(null);
        if (busCdTranslation != null && !busCdTranslation.getBusDescription().trim().isEmpty()) {
            fileTypeCode = Short.valueOf(busCdTranslation.getBusDescription().trim());
        }
        busCdTranslation = busCdTranslationRepository
                .findById(new BusCdTranslationId(eftType, RECURRING_TYPE, execContext.getLanguage(), bilTypeCode))
                .orElse(null);
        if (busCdTranslation != null && !busCdTranslation.getBusDescription().trim().isEmpty()) {
            fileTypeCode = Short.valueOf(busCdTranslation.getBusDescription().trim());
        }
        return fileTypeCode;
    }

    private ZonedDateTime getTapeDate(ZonedDateTime tapeDate, String agencySweepParam) {
        tapeDate = DateRoutine.adjustDateWithOffset(tapeDate, true, Integer.valueOf(agencySweepParam.substring(3, 6)),
                '+', tapeDate);
        String holidayCd = "";
        if (agencySweepParam.length() > 7) {
            holidayCd = agencySweepParam.substring(7, 9);
        }
        return holidayDateProcessing(tapeDate, holidayCd);
    }

    private ZonedDateTime holidayDateProcessing(ZonedDateTime dateTime, String holidayCd) {
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(RULE_XHOL, BLANK_STRING, BLANK_STRING,
                BLANK_STRING, dateTime, dateTime);
        char holidayValue;
        boolean isHoliday = false;
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == 'Y' && bilRulesUct.getBrtParmListTxt() != null) {
            if (holidayCd.equals("")) {
                holidayCd = bilRulesUct.getBrtParmListTxt().trim();
            }
            if (holidayCd.equalsIgnoreCase(HOLIDAY_8)) {
                holidayValue = '9';
            } else if (holidayCd.equalsIgnoreCase(HOLIDAY_6)) {
                holidayValue = '7';
            } else if (holidayCd.equalsIgnoreCase(HOLIDAY_5)) {
                holidayValue = '4';
            } else if (bilRulesUct.getBrtParmListTxt().trim().equalsIgnoreCase(HOLIDAY_3)) {
                holidayValue = '4';
            } else if (holidayCd.equalsIgnoreCase(HOLIDAY_1)) {
                holidayValue = '2';
            } else {
                holidayValue = holidayCd.trim().replace("H", "").charAt(0);
            }
            HolidayPrc holidayPrc = holidayPrcRepository.findHolidayRow(dateTime);
            if (holidayPrc != null) {
                isHoliday = true;
            }
            dateTime = DateRoutine.dateTimeWithHolidayProcess(dateTime, holidayValue, isHoliday);
            boolean holidayDateCheck = true;
            while (holidayDateCheck) {
                isHoliday = false;
                holidayPrc = holidayPrcRepository.findHolidayRow(dateTime);
                if (holidayPrc != null) {
                    isHoliday = true;
                }
                if (isHoliday) {
                    dateTime = DateRoutine.dateTimeWithHolidayProcess(dateTime, holidayValue, isHoliday);
                } else {
                    holidayDateCheck = false;
                }
            }
        }
        return dateTime;
    }

    private String getAgencySweepFileTypeCode(BilAccount bilAccount, Payment payment) {
        Short fileType = determineFileTypeCode(payment.getPaymentMethod(), payment.getPaymentType(), AGENCY_SWEEP);
        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct(AGENCY_SWEEP_RULE, bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), BLANK_STRING);

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == 'Y') {
            String agencySweepParam = bilRulesUct.getBrtParmListTxt();
            if (fileType != null && fileType != 0) {
                agencySweepParam = StringUtils.leftPad(String.valueOf(fileType), 3, CHAR_ZERO)
                        + agencySweepParam.substring(3);
            } else {
                if (agencySweepParam != null && !agencySweepParam.isEmpty() && agencySweepParam.length() >= 3) {
                    fileType = Short.parseShort(agencySweepParam.substring(0, 3));
                    Integer count = bilOrgSupportRepository.getCountByFileTypeCdAndFormCd(fileType, TYPE_FORM_CD);
                    if (count != null && count <= 0) {
                        throw new InvalidDataException("receipt.maynot.entered",
                                new Object[] { payment.getAgencyNumber() });
                    }
                }
            }
            return agencySweepParam;
        } else {
            throw new InvalidDataException("account.agency.invalid",
                    new Object[] { bilAccount.getAccountNumber().trim(), payment.getAgencyNumber() });
        }
    }

    private void insertBilCashEntryTotal(Payment payment, ZonedDateTime se3DateTime, String bilEntryNumber,
            String bilBankCode, String currencyCode) {
        BilCashEntryTot bilCashEntryTot = new BilCashEntryTot();
        bilCashEntryTot.setBilCashEntryTotId(new BilCashEntryTotId(se3DateTime, bilEntryNumber, payment.getUserId()));
        bilCashEntryTot.setBctTotCashAmt(payment.getPaymentAmount());
        bilCashEntryTot.setBilDepositDt(DateRoutine.defaultDateTime());
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())
                || payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilCashEntryTot.setBctDepositInd(CHAR_N);
            bilCashEntryTot.setBctAcceptInd(CHAR_Y);
            bilCashEntryTot.setBilBankCd(bilBankCode);
            bilCashEntryTot.setBctDepBankCd(bilCashEntryTot.getBilBankCd());
        } else {
            bilCashEntryTot.setBctDepositInd(CHAR_N);
            bilCashEntryTot.setBctAcceptInd(CHAR_Y);
            bilCashEntryTot.setBilBankCd(bilBankCode);
            bilCashEntryTot.setBctDepBankCd(SEPARATOR_BLANK);
        }
        bilCashEntryTot.setBctAcceptDt(se3DateTime);
        bilCashEntryTot.setBilCshEtrMthCd(payment.getPaymentMethod().charAt(0));
        bilCashEntryTot.setCurrencyCd(currencyCode == null ? SEPARATOR_BLANK : currencyCode);
        bilCashEntryTotRepository.save(bilCashEntryTot);
    }

    private void insertBilCashEntry(Payment payment, ZonedDateTime se3DateTime, String bilEntryNumber,
            Integer bilEntrySequenceNumber, String bilAccountNumber, char arspCode, String autApv, String crcrdArspTyc,
            String orderId) {
        BilCashEntry bilCashEntry = new BilCashEntry();
        bilCashEntry.setBilCashEntryId(new BilCashEntryId(se3DateTime, bilEntryNumber, payment.getUserId(),
                bilEntrySequenceNumber.shortValue()));
        bilCashEntry.setBilAccountNbr(bilAccountNumber);
        bilCashEntry.setBilTtyNbr(SEPARATOR_BLANK);
        bilCashEntry.setBilAgtActNbr(SEPARATOR_BLANK);
        bilCashEntry.setBilAdditionalId(payment.getAdditionalId());
        bilCashEntry.setPolSymbolCd(payment.getPolicySymbol());
        bilCashEntry.setPolNbr(payment.getPolicyNumber());
        bilCashEntry.setBilPblItemCd(payment.getPayableItem());
        bilCashEntry.setBilAdjDueDt(payment.getDueDate());
        bilCashEntry.setBilRctAmt(payment.getPaymentAmount());
        bilCashEntry.setBilRctTypeCd(payment.getPaymentType());
        bilCashEntry.setBilSusReasonCd(SEPARATOR_BLANK);
        bilCashEntry.setBilManualSusInd(CHAR_N);
        if (!payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilCashEntry.setBilRctId(payment.getPaymentId());
        }
        bilCashEntry.setBilRctCltId(SEPARATOR_BLANK);
        bilCashEntry.setBilRctAdrSeq(SHORT_ZERO);
        bilCashEntry.setBilRctReceiveDt(se3DateTime);
        bilCashEntry.setBilTaxYearNbr((short) se3DateTime.getYear());
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())
                || payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilCashEntry.setBilCrcrdTypeCd(BLANK_CHAR);
            bilCashEntry.setBilCrcrdArspCd(BLANK_CHAR);
            bilCashEntry.setBilCrcrdActNbr(SEPARATOR_BLANK);
            bilCashEntry.setBilCrcrdExpDt(SEPARATOR_BLANK);
            bilCashEntry.setBilCrcrdAutApv(SEPARATOR_BLANK);
            bilCashEntry.setBilCrcrdPstCd(SEPARATOR_BLANK);
        } else {
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
                bilCashEntry.setBilCrcrdTypeCd(payment.getCreditCardType().charAt(SHORT_ZERO));
                bilCashEntry.setBilCrcrdActNbr(payment.getCreditCardNumber());

            } else {
                bilCashEntry.setBilCrcrdTypeCd(payment.getWalletType().charAt(SHORT_ZERO));
                bilCashEntry.setBilCrcrdActNbr(payment.getWalletNumber());
            }
            bilCashEntry.setBilCrcrdExpDt(payment.getCreditCardExpiryDate());
            bilCashEntry.setBilCrcrdArspCd(arspCode);
            bilCashEntry.setBilCrcrdAutApv(autApv);
            bilCashEntry.setBilCrcrdPstCd(payment.getPostalCode());
        }
        bilCashEntry.setBilCrcrdArspTyc(crcrdArspTyc);
        bilCashEntry.setBilCwaId(SEPARATOR_BLANK);
        bilCashEntry.setBilPstMrkDt(se3DateTime);
        bilCashEntry.setBilPstMrkCd(BLANK_CHAR);
        bilCashEntry.setBilRctCmt(SEPARATOR_BLANK);
        bilCashEntry.setBecChgUserId(payment.getUserId());
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilCashEntry.setAgencyNbr(payment.getAgencyNumber());
        } else {
            bilCashEntry.setAgencyNbr(SEPARATOR_BLANK);
        }
        bilCashEntry.setInsertedRowTs(dateService.currentDateTime());
        bilCashEntry.setBilPmtOrderId(orderId);
        bilCashEntry.setBilBusGrpCd(payment.getBusinessGroup());
        bilCashEntryRepository.save(bilCashEntry);
    }

    private void insertBillCashReceipt(Payment payment, ZonedDateTime se3DateTime, Short bilDtbSeqNbr,
            Integer bilCashRctSeqNbr, String bilEntryNumber, String bilBankCode, String autApv) {
        BilCashReceipt bilCashReceipt = new BilCashReceipt();
        BilCashReceiptId bilCashReceiptId = new BilCashReceiptId(payment.getAccountId(), se3DateTime, bilDtbSeqNbr);
        bilCashReceipt.setBilCashReceiptId(bilCashReceiptId);
        bilCashReceipt.setEntryDate(se3DateTime);
        bilCashReceipt.setEntryNumber(bilEntryNumber);
        bilCashReceipt.setEntrySequenceNumber(bilCashRctSeqNbr.shortValue());
        bilCashReceipt.setUserId(payment.getUserId());
        bilCashReceipt.setReceiptAmount(payment.getPaymentAmount());
        bilCashReceipt.setReceiptId(SEPARATOR_BLANK);
        bilCashReceipt.setReceiptTypeCd(payment.getPaymentType());
        bilCashReceipt.setReceiptComment(payment.getPaymentComments());
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())
                || payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilCashReceipt.setCreditCardTypeCd(BLANK_CHAR);
            bilCashReceipt.setCreditCardAccounttNumber(SEPARATOR_BLANK);
            bilCashReceipt.setCreditCardExpirationDate(SEPARATOR_BLANK);
            bilCashReceipt.setCredutCardAuthroizationApproval(SEPARATOR_BLANK);
        } else {
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
                bilCashReceipt.setCreditCardTypeCd(payment.getCreditCardType().charAt(0));
                bilCashReceipt.setCreditCardAccounttNumber(payment.getCreditCardNumber());
                bilCashReceipt.setCreditCardExpirationDate(payment.getCreditCardExpiryDate().substring(0, 4));
            } else {
                bilCashReceipt.setCreditCardTypeCd(payment.getWalletType().charAt(0));
                bilCashReceipt.setCreditCardAccounttNumber(payment.getWalletNumber());
                bilCashReceipt.setCreditCardExpirationDate(payment.getCreditCardExpiryDate());
            }

            bilCashReceipt.setCredutCardAuthroizationApproval(autApv);
        }

        bilCashReceipt.setToFroTransferNumber(SEPARATOR_BLANK);
        bilCashReceipt.setTransferTypeCd(BLANK_CHAR);
        bilCashReceipt.setReceiptReceiveDate(se3DateTime);
        bilCashReceipt.setBankCd(bilBankCode);
        bilCashReceipt.setDepositeDate(DateRoutine.defaultDateTime());
        bilCashReceipt.setCashEntryMethodCd(payment.getPaymentMethod().charAt(0));
        bilCashReceipt.setTaxYearNumber((short) se3DateTime.getYear());
        bilCashReceipt.setPostMarkDate(se3DateTime);
        bilCashReceipt.setPostMarkCd(BLANK_CHAR);
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilCashReceipt.setThirdPartyCashIdentifier(CHAR_N);
            bilCashReceipt.setAgencyNumber(payment.getAgencyNumber());
            bilCashReceipt.setPaymentProfileId(payment.getPaymentId());
        } else {
            bilCashReceipt.setThirdPartyCashIdentifier(BLANK_CHAR);
            bilCashReceipt.setAgencyNumber(SEPARATOR_BLANK);
        }
        bilCashReceiptRepository.saveAndFlush(bilCashReceipt);
    }

    private void insertBilCashDsp(Payment payment, ZonedDateTime se3DateTime, Short bilDtbSeqNbr) {
        BilCashDsp bilCashDsp = new BilCashDsp();
        BilCashDspId bilCashDspId = new BilCashDspId(payment.getAccountId(), se3DateTime, bilDtbSeqNbr, SHORT_ZERO);
        bilCashDsp.setBilCashDspId(bilCashDspId);
        bilCashDsp.setPolicyId(SEPARATOR_BLANK);
        if (payment.getPolicySymbol() == null) {
            bilCashDsp.setPolicySymbolCd(SEPARATOR_BLANK);
        } else {
            bilCashDsp.setPolicySymbolCd(payment.getPolicySymbol());
        }
        if (payment.getPolicyNumber() == null) {
            bilCashDsp.setPolicyNumber(SEPARATOR_BLANK);
        } else {
            bilCashDsp.setPolicyNumber(payment.getPolicyNumber());
        }

        bilCashDsp.setPayableItemCd(payment.getPayableItem());
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())
                || payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilCashDsp.setAdjustmentDueDate(payment.getDueDate());
        } else {
            bilCashDsp.setAdjustmentDueDate(DateRoutine.defaultDateTime());
        }

        bilCashDsp.setChargeTypeCd(BLANK_CHAR);
        bilCashDsp.setUserId(payment.getUserId());
        bilCashDsp.setDspDate(se3DateTime);
        bilCashDsp.setDspTypeCd("SP");
        if (payment.getSuspenseReason().trim().isEmpty()) {
            bilCashDsp.setDspReasonCd(SEPARATOR_BLANK);
            bilCashDsp.setManualSuspenseIndicator(CHAR_N);
        } else {
            bilCashDsp.setManualSuspenseIndicator(CHAR_Y);
            bilCashDsp.setDspReasonCd(payment.getSuspenseReason());
        }

        bilCashDsp.setDspAmount(payment.getPaymentAmount());
        bilCashDsp.setPayeeClientId(SEPARATOR_BLANK);
        bilCashDsp.setPayeeAddressSequenceNumber(SHORT_ZERO);

        bilCashDsp.setDraftNbr(SHORT_ZERO);
        bilCashDsp.setCreditIndicator(CHAR_N);
        bilCashDsp.setCreditPolicyId(SEPARATOR_BLANK);
        bilCashDsp.setCreditAmountType(SEPARATOR_BLANK);
        bilCashDsp.setRevsRsusIndicator(BLANK_CHAR);
        bilCashDsp.setToFromTransferNbr(SEPARATOR_BLANK);
        bilCashDsp.setTransferTypeCd(BLANK_CHAR);
        bilCashDsp.setStatusCd(BLANK_CHAR);
        bilCashDsp.setDisbursementId(SEPARATOR_BLANK);
        bilCashDsp.setCheckProdMethodCd(BLANK_CHAR);
        bilCashDsp.setPolicyEffectiveDate(DateRoutine.defaultDateTime());
        bilCashDsp.setBilSequenceNumber(SHORT_ZERO);
        bilCashDsp.setSystemDueDate(DateRoutine.defaultDateTime());
        bilCashDsp.setInvoiceDate(DateRoutine.defaultDateTime());
        bilCashDsp.setDisbursementDate(DateRoutine.defaultDateTime());
        bilCashDsp.setStatementDetailTypeCd(SEPARATOR_BLANK);
        bilCashDspRepository.saveAndFlush(bilCashDsp);

    }

    private void insertBilEFtPendingTape(Payment payment, Short bilDtbSeqNbr, ZonedDateTime tapeDate,
            ZonedDateTime se3DateTime, short fileTypeCode, String crcrdArspTyc, String orderId) {

        Short bilSequenceNumber = 0;
        BilEftPendingTape bilEftPendingTape = new BilEftPendingTape();
        BilEftPendingTapeId bilEftPendingTapeId = null;
        bilSequenceNumber = getBilSequenceNumber(payment.getAccountId(), 'A', tapeDate, se3DateTime);
        bilEftPendingTapeId = new BilEftPendingTapeId(payment.getAccountId(), 'A', tapeDate, se3DateTime,
                bilSequenceNumber);

        bilEftPendingTape.setBilEftPendingTapeId(bilEftPendingTapeId);
        bilEftPendingTape.setEftDrDate(tapeDate);
        bilEftPendingTape.setEftDrAmount(payment.getPaymentAmount());
        bilEftPendingTape.setEftPostPaymentDate(se3DateTime);
        bilEftPendingTape.setEftNotIndicator('N');
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
            bilEftPendingTape.setEftReceivedType(EftRecordType.RECONCILED_CASH.getValue());
            bilEftPendingTape.setBusinessActivityCd(getBusinessActivityCode(crcrdArspTyc));
            bilEftPendingTape.setWebAutomatedIndicator(BLANK_CHAR);
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            bilEftPendingTape.setEftReceivedType(EftRecordType.ONE_TIME_ACH.getValue());
            bilEftPendingTape.setBusinessActivityCd(getBusinessActivityCode(payment.getAccountType()));
            bilEftPendingTape.setWebAutomatedIndicator(CHAR_N);
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilEftPendingTape.setEftReceivedType(EftRecordType.AGENCY_SWEEP.getValue());
            bilEftPendingTape.setBusinessActivityCd(getBusinessActivityCode(payment.getAccountType()));
            bilEftPendingTape.setWebAutomatedIndicator(CHAR_N);
        } else {
            bilEftPendingTape.setEftReceivedType(EftRecordType.DIGITAL_WALLET.getValue());
            bilEftPendingTape.setBusinessActivityCd(getBusinessActivityCode(crcrdArspTyc));
            bilEftPendingTape.setWebAutomatedIndicator(BLANK_CHAR);
        }
        bilEftPendingTape.setFileTypeCd(fileTypeCode);
        bilEftPendingTape.setDisbursementId(SEPARATOR_BLANK);
        bilEftPendingTape.setReceiptSequenceNumber(bilDtbSeqNbr);
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilEftPendingTape.setAgencyNbr(payment.getAgencyNumber());
        } else {
            bilEftPendingTape.setAgencyNbr(SEPARATOR_BLANK);
        }
        bilEftPendingTape.setInsertedRowTime(dateService.currentDateTime());
        bilEftPendingTape.setPaymentOrderId(orderId);
        bilEftPendingTape.setBilBusGrpCd(payment.getBusinessGroup());
        bilEftPendingTapeRepository.saveAndFlush(bilEftPendingTape);
        if (StringUtils.isNotBlank(payment.getWalletIdentifier())) {
            insertBilEftExtPayment(payment, bilEftPendingTape);
        }

    }

    private void insertBilEftExtPayment(Payment payment, BilEftPendingTape bilEftPendingTape) {
        BilEftExtPyt bilEftExtPyt = new BilEftExtPyt();
        BilEftExtPytId bilEftExtPytId = new BilEftExtPytId();
        bilEftExtPytId.setBilTchKeyId(payment.getAccountId());
        bilEftExtPytId.setBilTchKeyTypCd(bilEftPendingTape.getBilEftPendingTapeId().getTechnicalKeyType());
        bilEftExtPytId.setBilEftTapeDt(bilEftPendingTape.getBilEftPendingTapeId().getEftTapeDate());
        bilEftExtPytId.setBilEftAcyDt(bilEftPendingTape.getBilEftPendingTapeId().getEftActivityDate());
        bilEftExtPytId.setBilSeqNbr(bilEftPendingTape.getBilEftPendingTapeId().getSequenceNbr());
        bilEftExtPyt.setBilWalTypeCd(payment.getWalletType());
        bilEftExtPyt.setBilWalIdentifier(payment.getWalletIdentifier());
        bilEftExtPyt.setBilEftExtPytId(bilEftExtPytId);
        bilEftExtPytRepository.saveAndFlush(bilEftExtPyt);
    }

    private Short getBilSequenceNumber(String accountId, char techKeyTypCd, ZonedDateTime tapeDate,
            ZonedDateTime se3DateTime) {
        Short bilSequenceNumber = bilEftPendingTapeRepository.getMaximunSequenceNumber(accountId, techKeyTypCd,
                tapeDate, se3DateTime.with(LocalTime.MIN));
        if (bilSequenceNumber == null) {
            bilSequenceNumber = 0;
        } else {
            bilSequenceNumber = (short) (bilSequenceNumber + 1);
        }
        return bilSequenceNumber;
    }

    private void insertBilActSummary(Payment payment, ZonedDateTime se3DateTime, ZonedDateTime tapeDate,
            Short bilDtbSeqNbr) {
        short bilAcySeq = getBilAcySeq(payment.getAccountId(), se3DateTime);
        String xcce = "1XCCE";
        String pending = "Pending";

        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(payment.getAccountId(), se3DateTime, bilAcySeq);
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(payment.getPolicySymbol());
        bilActSummary.setPolNbr(payment.getPolicyNumber());
        bilActSummary.setBilAcyDesCd(payment.getPaymentType());
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
            bilActSummary.setBilDesReaTyp("CPY");
            bilActSummary.setBasAddDataTxt(xcce + SEPARATOR_BLANK + pending + SEPARATOR_BLANK + "#" + bilDtbSeqNbr + "#"
                    + DateRoutine.dateTimeAsYYYYMMDDString(se3DateTime) + payment.getBatchId()
                    + StringUtils.rightPad(payment.getUserId(), 8)
                    + StringUtils.leftPad(String.valueOf(payment.getPaymentSequenceNumber()), 4, CHAR_ZERO));

            bilActSummary.setBilAcyDes1Dt(se3DateTime);
            bilActSummary.setBilAcyDes2Dt(se3DateTime);
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            bilActSummary.setBilDesReaTyp("1XA");
            bilActSummary.setBasAddDataTxt(pending);
            bilActSummary.setBilAcyDes1Dt(DateRoutine.defaultDateTime());
            bilActSummary.setBilAcyDes2Dt(DateRoutine.defaultDateTime());
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilActSummary.setBilDesReaTyp("ASW");
            bilActSummary.setBasAddDataTxt(
                    StringUtils.rightPad(pending, 10) + StringUtils.rightPad(payment.getAgencyNumber(), 12));
            bilActSummary.setBilAcyDes1Dt(tapeDate);
            bilActSummary.setBilAcyDes2Dt(DateRoutine.defaultDateTime());
        } else {
            bilActSummary.setBilDesReaTyp(BilDesReasonType.WALLET_PAYMENT_TYPE.getValue());
            bilActSummary.setBasAddDataTxt(BilDesReasonType.WALLET_PAYMENT_TYPE.getValue() + SEPARATOR_BLANK + pending
                    + SEPARATOR_BLANK + "#" + bilDtbSeqNbr + "#" + DateRoutine.dateTimeAsYYYYMMDDString(se3DateTime)
                    + payment.getBatchId() + StringUtils.rightPad(payment.getUserId(), 8)
                    + StringUtils.leftPad(String.valueOf(payment.getPaymentSequenceNumber()), 4, CHAR_ZERO));

            bilActSummary.setBilAcyDes1Dt(se3DateTime);
            bilActSummary.setBilAcyDes2Dt(se3DateTime);
        }

        bilActSummary.setBilAcyAmt(payment.getPaymentAmount());
        bilActSummary.setUserId(payment.getUserId());
        bilActSummary.setBilAcyTs(dateService.currentDateTime());

        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

    private Short getBilAcySeq(String accountId, ZonedDateTime se3DateTime) {
        Short bilAcySeq = 0;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, se3DateTime.with(LocalTime.MIN));
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        return bilAcySeq;
    }

    private void insertBilBankEntry(Payment payment, Integer cashRctSeqNbr, String bilEntryNumber,
            ZonedDateTime se3DateTime) {
        BilBankEntry bilBankEntry = new BilBankEntry();
        bilBankEntry.setBilBankEntryId(
                new BilBankEntryId(se3DateTime, bilEntryNumber, payment.getUserId(), cashRctSeqNbr.shortValue()));

        bilBankEntry.setBankAccountNbr(payment.getBankAccountNumber());
        bilBankEntry.setRoutingTransitNbr(payment.getRoutingNumber());
        bilBankEntry.setAccountTypCd(payment.getAccountType());
        bilBankEntry.setWebAuthInd(CHAR_N);
        bilBankEntry.setConfirmationNbr(generateConfirmationNumber());
        bilBankEntry.setEftPayorLongName(payment.getBankHolderName());
        bilBankEntryRepository.save(bilBankEntry);

    }

    private String generateConfirmationNumber() {
        String uniqueId;
        do {
            uniqueId = generateUniqueId(GENERATED_ID_LENGTH_20);
        } while (bilBankEntryRepository.findByConfirmationNbr(uniqueId) != null);
        return uniqueId;
    }

    private String generateUniqueId(int generatedIdLength) {
        return IdGenerator.generateUniqueIdUpperCase(generatedIdLength);
    }

    private void cashApplyProcess(String accountId, ZonedDateTime se3DateTime) {

        CashApplyDriver cashApplyDriver = new CashApplyDriver();
        cashApplyDriver.setAccountId(accountId);
        cashApplyDriver.setCashApplyDate(se3DateTime);
        cashApplyDriver.setCallBcmoax(true);
        cashApplyDriver.setNoReinstatement(false);
        cashApplyDriver.setUserSequenceId(execContext.getUserSeqeunceId());
        cashApplyDriverService.processCashApply(cashApplyDriver);
    }

    private Short getBilDtbSeqNbr(String accountId, ZonedDateTime se3DateTime) {
        Short bilDtbSeqNbr = 0;
        bilDtbSeqNbr = bilCashReceiptRepository.getMaxBilDtbSeqNbr(accountId, se3DateTime);
        if (bilDtbSeqNbr == null) {
            bilDtbSeqNbr = 0;
        } else {
            bilDtbSeqNbr = (short) (bilDtbSeqNbr + 1);
        }
        return bilDtbSeqNbr;
    }

    private String getUniqueBilEntryNumber(ZonedDateTime se3DateTime) {
        String bilEntryNumber;
        do {
            bilEntryNumber = generateUniqueId(GENERATED_ID_LENGTH_4);
        } while (bilCashEntryTotRepository.getBilEntryNumber(se3DateTime, bilEntryNumber,
                execContext.getUserSeqeunceId()) != 0);
        return bilEntryNumber;
    }

    private Integer getBilEntrySequenceNumber(String bilEntryNumber, ZonedDateTime se3DateTime, String userId) {
        Integer bilEntrySequenceNumber = bilCashEntryRepository.getMaxBilEntrySeq(bilEntryNumber, se3DateTime, userId);
        if (bilEntrySequenceNumber == null) {
            bilEntrySequenceNumber = 0;
        } else {
            bilEntrySequenceNumber = bilEntrySequenceNumber + 1;
        }
        return bilEntrySequenceNumber;
    }

    private String getBankCode(String paymentType, String bilDesReasonType, Short fileType) {
        short fileTypeCode = 0;
        if (!paymentType.equals(BLANK_STRING)) {
            fileTypeCode = determineFileTypeCode(null, paymentType, "CPY");
        } else {
            fileTypeCode = fileType;
        }

        BilOrgSupport bilOrgSupport = bilOrgSupportRepository.getByBilFileTypeCd(fileTypeCode, bilDesReasonType);
        if (bilOrgSupport != null) {
            return bilOrgSupport.getBilBankCd().trim();
        }
        return SEPARATOR_BLANK;
    }

    private short determineFileTypeCode(String paymentMethod, String paymentType, String bilDesReasonCode) {

        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(paymentType,
                BilDesReasonType.FTC.getValue(), execContext.getLanguage());
        if (busCdTranslation == null) {
            busCdTranslation = busCdTranslationRepository.findByCodeAndType(bilDesReasonCode,
                    BilDesReasonType.FTC.getValue(), execContext.getLanguage());
        }

        if (busCdTranslation == null && paymentMethod != null) {
            if (!paymentMethod.equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
                throw new InvalidDataException("bankCode.invalid");
            } else {
                return 0;
            }
        } else if (busCdTranslation != null) {
            return Short.parseShort(busCdTranslation.getBusDescription().trim());
        }
        return 0;

    }

    private char getBusinessActivityCode(String crcrdArspTyc) {
        if (busCdTranslationRepository.findByCodeAndType(crcrdArspTyc, BusCodeTranslationType.CCD.getValue(),
                execContext.getLanguage()) != null) {
            return CHAR_Y;
        }
        return CHAR_N;
    }

    private void callFinancialApi(Double amount, ZonedDateTime applicationDate, String accountId,
            short entrySequenceNumber, String bankCode, ZonedDateTime entryDate, String entryNumber, String userId,
            String accountNumber, String transactionObjectCode, String transactionActionCode, String paybleItemCode,
            String sourceCode, String paymentType, String bilReasonCode) {

        String masterCompany = BillingConstants.MASTER_COMPANY_NUMBER;
        String riskState = BLANK_STRING;
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
        if (bilAccount == null) {
            throw new DataNotFoundException("no.data.found");
        }
        BilMstCoSt bilMstCoSt = bilMstCoStRepository.findRow(bilAccount.getBillClassCd(), bilAccount.getBillTypeCd(),
                BLANK_STRING);
        if (bilMstCoSt != null) {
            masterCompany = bilMstCoSt.getMasterCompanyNbr();
            riskState = bilMstCoSt.getBilStatePvnCd();
        }
        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(BLANK_STRING);
        financialApiActivity.setApplicationName(ApplicationName.BACPCCP.toString());
        financialApiActivity.setUserId(execContext.getUserSeqeunceId());
        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setAppProgramId(ApplicationProgramIdentifier.BACPCCP.toString());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilTypeCode(bilAccount.getBillTypeCd());
        financialApiActivity.setBilClassCode(bilAccount.getBillClassCd());
        financialApiActivity.setPayableItemCode(paybleItemCode);
        financialApiActivity.setBilAccountId(accountNumber);
        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setBilSourceCode(sourceCode);
        financialApiActivity.setMasterCompanyNbr(masterCompany);
        financialApiActivity.setStateCode(riskState);
        financialApiActivity.setBilDatabaseKey(getBilDatabaseKey(entryDate, entryNumber, userId, entrySequenceNumber));
        financialApiActivity.setStatusCode(BLANK_CHAR);
        financialApiActivity.setBilReceiptTypeCode(paymentType);
        financialApiActivity.setBilReasonCode(bilReasonCode);
        financialApiActivity.setSummaryEffectiveDate(applicationDate);
        financialApiActivity.setOperatorId(execContext.getUserSeqeunceId());
        financialApiActivity.setCurrencyCode(bilAccount.getCurrencyCode());

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {

            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

    private String getBilDatabaseKey(ZonedDateTime entryDate, String entryNumber, String operatorId,
            short bilDtbSequenceNumber) {
        String seqNumberSign = "+";

        String dataString = DateRoutine.dateTimeAsYYYYMMDDString(entryDate);
        dataString += entryNumber;
        dataString += StringUtils.rightPad(operatorId, 8, BLANK_CHAR);
        dataString += seqNumberSign;
        dataString += StringUtils.leftPad(String.valueOf(bilDtbSequenceNumber), 5, CHAR_ZERO);

        return dataString;
    }

    private short getFileTypeCodeFromBilRuleUct(ZonedDateTime se3DateTime) {
        final String STRING_1XCC = "1XCC";
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(STRING_1XCC, SEPARATOR_BLANK, SEPARATOR_BLANK,
                SEPARATOR_BLANK, se3DateTime, se3DateTime);
        if (bilRulesUct != null) {
            return new Short(bilRulesUct.getBrtParmListTxt());
        }
        return 0;
    }

    private String generateOrderId(String bilAccountNumber, String paymentType) {
        StringBuilder orderId = new StringBuilder();
        ZonedDateTime currentDateTime = dateService.currentDateTime();
        DecimalFormat df = new DecimalFormat("00");
        if (bilAccountNumber.length() < 8) {
            orderId.append(StringUtils.rightPad(bilAccountNumber, 8, "0"));
        } else {
            orderId.append(bilAccountNumber.substring(SHORT_ZERO, 8));
        }
        orderId.append(paymentType.substring(0, 1)).append(df.format(currentDateTime.getMonth()))
                .append(df.format(currentDateTime.getDayOfMonth())).append(df.format(currentDateTime.getHour()))
                .append(df.format(currentDateTime.getMinute())).append(df.format(currentDateTime.getSecond()))
                .append("X");
        return orderId.toString();
    }

}
