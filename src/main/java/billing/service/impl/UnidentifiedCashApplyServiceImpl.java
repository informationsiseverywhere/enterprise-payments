package billing.service.impl;

import static billing.utils.BillingConstants.ERROR_CODE;
import static billing.utils.BillingConstants.MASTER_COMPANY_NUMBER;
import static billing.utils.BillingConstants.PLUS_SIGN;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_Z;
import static core.utils.CommonConstants.CHAR_ZERO;
import static disbursement.utils.DisbursementConstants.COMPANY_LOCATION_NUMBER;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilAccount;
import billing.data.entity.BilAgent;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilThirdParty;
import billing.data.entity.BilUnIdCash;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilAgentRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.service.AgencyAccountPaymentService;
import billing.service.ApplyUnidentifiedCashService;
import billing.service.GroupAccountPaymentService;
import billing.service.UnidentifiedCashApplyService;
import billing.utils.BillingConstants.AccountType;
import core.api.ExecContext;
import core.exception.validation.InvalidDataException;
import core.utils.DateRoutine;
import disbursement.utils.DisbursementConstants.DisbursementIdType;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class UnidentifiedCashApplyServiceImpl implements UnidentifiedCashApplyService {

    private static final List<Character> issueId = Arrays.asList(CHAR_Y, CHAR_Z);

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    private BilAgentRepository bilAgentRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private ApplyUnidentifiedCashService applyUnidentifiedCashService;

    @Autowired
    private GroupAccountPaymentService groupAccountPaymentService;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private AgencyAccountPaymentService agencyAccountPaymentService;

    @Override
    @Transactional
    public void applyUnidentifiedCash(String accountNumber, String policyNumber, String policySymbolCode,
            String folderId, BilUnIdCash bilUnidentifiedCash, String identifierType, Boolean isBusGrpOverride,
            Boolean isUpdate, String previousIdentifier, String currencyCode) {
        if (identifierType.equals(AccountType.GROUP_ACCOUNT.toString())) {
            BilThirdParty bilThirdParty = bilThirdPartyRepository.findByBilTtyNbr(accountNumber);
            if (bilThirdParty != null) {
                groupAccountPaymentService.applyUnidentifiedCash(bilThirdParty);
            }
        } else if (identifierType.equals(AccountType.BILLACCOUNT.toString())) {
            BilAccount bilAccount = bilAccountRepository.findByAccountNumber(accountNumber);
            if (bilAccount != null) {
                applyUnidentifiedCashService.applyUnidentifiedCash(bilAccount, policyNumber, identifierType,
                        isBusGrpOverride);
            } else if (Boolean.TRUE.equals(isUpdate)) {
                saveFinancialActivity(folderId, bilUnidentifiedCash, bilUnidentifiedCash.getBilRctAmt() * -1,
                        previousIdentifier, currencyCode);
                saveFinancialActivity(folderId, bilUnidentifiedCash, bilUnidentifiedCash.getBilRctAmt(), accountNumber,
                        currencyCode);
            }
        } else if (identifierType.equals(AccountType.AGENCY_ACCOUNT.toString())) {
            BilAgent bilAgent = bilAgentRepository.findByBilAgtActNbr(accountNumber);
            if (bilAgent != null) {
                agencyAccountPaymentService.applyUnidentifiedCash(bilAgent);
            }
        } else {
            List<BilPolicy> bilPolicy = bilPolicyRepository.findPolicyIdByPolicyNumber(policyNumber, policySymbolCode,
                    issueId);
            if (!bilPolicy.isEmpty() && bilPolicy.get(0) != null) {
                BilAccount bilAccount = bilAccountRepository.findByAccountNumber(bilPolicy.get(0).getBilAccountNbr());
                applyUnidentifiedCashService.applyUnidentifiedCash(bilAccount, policyNumber, identifierType,
                        isBusGrpOverride);
            }
        }
    }

    private void saveFinancialActivity(String folderId, BilUnIdCash bilUnidentifiedCash, Double amount,
            String accountNumber, String currencyCode) {
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setTransactionObjectCode(ObjectCode.UNIDENTIFIED_CASH.toString());
        financialApiActivity.setTransactionActionCode(ActionCode.SUSPENSE.toString());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(folderId);
        financialApiActivity.setApplicationName(ApplicationName.BACPDUC.toString());
        financialApiActivity.setUserId(execContext.getUserSeqeunceId());
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setApplicationProductIdentifier(DisbursementIdType.BCMS.getDisbursementIdType());
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setOperatorId(execContext.getUserSeqeunceId());
        financialApiActivity.setAppProgramId(ApplicationProgramIdentifier.BACPDUC.toString());
        financialApiActivity.setReferenceDate(
                DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setBilAccountId(accountNumber);
        financialApiActivity.setBilReceiptTypeCode(bilUnidentifiedCash.getBilRctTypeCd());
        financialApiActivity.setBilBankCode(bilUnidentifiedCash.getBilBankCd());
        financialApiActivity.setBilSourceCode(String.valueOf(bilUnidentifiedCash.getBilCshEtrMthCd()));
        financialApiActivity.setBilReasonCode(bilUnidentifiedCash.getBilDspReasonCd());
        financialApiActivity
                .setBilDatabaseKey(generateDatabaseKey(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(), bilUnidentifiedCash.getBilEntrySeqNbr()));
        financialApiActivity.setSummaryEffectiveDate(DateRoutine.defaultDateTime());
        financialApiActivity.setPolicyId(bilUnidentifiedCash.getPolNbr());
        financialApiActivity.setUserId(bilUnidentifiedCash.getActivityUserId());
        financialApiActivity.setCurrencyCode(currencyCode);

        String[] fwsReturnMessage = null;
        fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                execContext.getApplicationDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

    private String generateDatabaseKey(ZonedDateTime entryDate, String entryNumber, String userId,
            Short entrySequenceNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        sb.append(StringUtils.rightPad(entryNumber, 4));
        sb.append(StringUtils.rightPad(userId, 8));
        sb.append(PLUS_SIGN);
        sb.append(StringUtils.leftPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
        return sb.toString();
    }
}
