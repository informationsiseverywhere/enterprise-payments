package billing.service.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilEftActivity;
import billing.data.entity.BilEftRejBch;
import billing.data.entity.id.BilEftRejBchId;
import billing.data.repository.BilEftActivityRepository;
import billing.data.repository.BilEftRejBchRepository;
import billing.model.BatchRejection;
import billing.model.ImmediateActivity;
import billing.service.BatchRejectionService;
import billing.utils.PB360Service;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.model.Paging;
import core.utils.DateRoutine;
import core.utils.MultiFiltersSearch;
import core.utils.PageNavigation;

@Service
public class BatchRejectionServiceImpl implements BatchRejectionService {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private PB360Service pb360Service;

    @Autowired
    private DateService dateService;

    @Autowired
    private BilEftRejBchRepository bilEftRejBchRepository;

    @Autowired
    private BilEftActivityRepository bilEftActivityRepository;

    public static final Logger logger = LogManager.getLogger(BatchRejectionServiceImpl.class);
    private static final String SINCE_DATE = "since";
    private static final String UNTIL_DATE = "until";
    private static final String TO_AMOUNT = "toAmount";
    private static final String FROM_AMOUNT = "fromAmount";
    private static final String BATCH_APPLIED = "batchApplied";
    private static final String NO_DATA_FOUND = "no.data.found";

    @Override
    public BatchRejection findBatchDetails(String batchId, String entryDate) {

        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(entryDate);

        Integer rejectionBatchCount = getBatchRejection(batchId, entryDate);
        if (rejectionBatchCount == null || rejectionBatchCount == SHORT_ZERO) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }
        Double accumulatedAmount = bilEftRejBchRepository.getAmountByBatchId(batchId, bilEntryDt,
                execContext.getUserSeqeunceId());

        return mapBillEftRejectionToBatch(batchId, bilEntryDt, accumulatedAmount);

    }

    @Override
    public List<BatchRejection> findAllBatches(String query, String filters, String sort, int size, int page,
            Paging paging) {
        List<BatchRejection> batchList = new LinkedList<>();

        List<Object[]> bilEftRejBchArray = null;

        if (filters != null && !filters.isEmpty()) {
            bilEftRejBchArray = batchRowsByParams(page, size, filters);

        } else {
            bilEftRejBchArray = bilEftRejBchRepository.getAmountByBatchId(execContext.getUserSeqeunceId(),
                    Arrays.asList(CHAR_O, BLANK_CHAR), PageRequest.of(page - 1, size));
        }

        if (bilEftRejBchArray == null || bilEftRejBchArray.isEmpty() || bilEftRejBchArray.get(SHORT_ZERO)[0] == null) {
            return batchList;
        }

        int totalRows = bilEftRejBchArray.size();

        for (Object[] object : bilEftRejBchArray) {
            batchList.add(mapBilEftRejectiontoBatch(object));
        }

    PageNavigation.getPagingAttributes(totalRows, size, page, paging);
        return batchList;
    }

    private BatchRejection mapBilEftRejectiontoBatch(Object[] object) {

        BatchRejection batch = new BatchRejection();
        String batchNumber = (String) object[0];
        batch.setBatchId(batchNumber.trim());
        batch.setEntryDate((ZonedDateTime) object[1]);
        batch.setAccumulatedAmount((Double) object[2]);
        batch.setIsAccept(false);
        if (batch.getAccumulatedAmount() == null || batch.getAccumulatedAmount() == DECIMAL_ZERO) {
            batch.setAccumulatedAmount(DECIMAL_ZERO);
            batch.setNumberOfTransactions(0);
        } else {
            List<BilEftRejBch> bilEftRejBchList = bilEftRejBchRepository.getBilEftDataRows(batch.getBatchId(),
                    batch.getEntryDate(), execContext.getUserSeqeunceId());
            if (bilEftRejBchList != null && !bilEftRejBchList.isEmpty()) {
                batch.setNumberOfTransactions(bilEftRejBchList.size());
                if (bilEftRejBchList.get(SHORT_ZERO).getBrbRejStaInd() != CHAR_O) {
                    batch.setIsAccept(true);
                }
            } else {
                batch.setNumberOfTransactions(0);
            }
        }
        return batch;
    }

    private List<Object[]> batchRowsByParams(int page, int size, String filters) {
        Map<String, String> matchFilterMap = new HashMap<>();

        if (filters != null) {
            matchFilterMap = MultiFiltersSearch.getFilters(filters, matchFilterMap);
        }

        ZonedDateTime sinceDate = DateRoutine.defaultSinceDateTime();
        ZonedDateTime untilDate = DateRoutine.defaultDateTime();
        Double fromAmount = 0d;
        Double toAmount = 999999999999d;
        boolean isBatchApplied = false;

        if (matchFilterMap.get(SINCE_DATE) != null) {
            sinceDate = DateRoutine.dateTimeAsYYYYMMDD(matchFilterMap.get(SINCE_DATE));
        }
        if (matchFilterMap.get(UNTIL_DATE) != null) {
            untilDate = DateRoutine.dateTimeAsYYYYMMDD(matchFilterMap.get(UNTIL_DATE));
        }
        if (matchFilterMap.get(FROM_AMOUNT) != null) {
            fromAmount = Double.parseDouble(matchFilterMap.get(FROM_AMOUNT));
        }
        if (matchFilterMap.get(TO_AMOUNT) != null) {
            toAmount = Double.parseDouble(matchFilterMap.get(TO_AMOUNT));
        }

        if (matchFilterMap.get(BATCH_APPLIED) != null) {

            isBatchApplied = Boolean.parseBoolean(matchFilterMap.get(BATCH_APPLIED));
        }

        return getBatchWithFilters(isBatchApplied, fromAmount, toAmount, sinceDate, untilDate, page, size);
    }

    private List<Object[]> getBatchWithFilters(boolean isBatchApplied, Double fromAmount, Double toAmount,
            ZonedDateTime sinceDate, ZonedDateTime untilDate, int page, int size) {

        List<Object[]> bilEftRejBchData = null;

        if (isBatchApplied) {
            bilEftRejBchData = bilEftRejBchRepository.getAllRejectionDatawithAccept(sinceDate, untilDate, fromAmount,
                    toAmount, execContext.getUserSeqeunceId(), Arrays.asList(CHAR_O, BLANK_CHAR),
                    PageRequest.of(page - 1, size));
        } else {
            bilEftRejBchData = bilEftRejBchRepository.getAllRejectionData(sinceDate, untilDate, fromAmount, toAmount,
                    execContext.getUserSeqeunceId(), Arrays.asList(CHAR_O, BLANK_CHAR), PageRequest.of(page - 1, size));
        }

        return bilEftRejBchData;
    }

    private BatchRejection mapBillEftRejectionToBatch(String batchId, ZonedDateTime bilEntryDt,
            Double accumulatedAmount) {
        BatchRejection batch = new BatchRejection();

        batch.setBatchId(batchId.trim());
        batch.setEntryDate(bilEntryDt);
        if (accumulatedAmount == null) {
            batch.setAccumulatedAmount(DECIMAL_ZERO);
        } else {
            batch.setAccumulatedAmount(accumulatedAmount);
            List<BilEftRejBch> bilEftRejBchList = bilEftRejBchRepository.getBilEftDataRows(batch.getBatchId(),
                    batch.getEntryDate(), execContext.getUserSeqeunceId());
            if (bilEftRejBchList != null && !bilEftRejBchList.isEmpty()) {
                batch.setNumberOfTransactions(bilEftRejBchList.size());
                if (bilEftRejBchList.get(SHORT_ZERO).getBrbRejStaInd() != CHAR_O) {
                    batch.setIsAccept(true);
                }
            } else {
                batch.setNumberOfTransactions(0);
            }
        }

        if (batch.getAccumulatedAmount() == null || batch.getAccumulatedAmount() == DECIMAL_ZERO) {
            batch.setAccumulatedAmount(DECIMAL_ZERO);
            batch.setNumberOfTransactions(0);
        }
        return batch;
    }

    @Override
    public String save(BatchRejection batch) {
        BilEftRejBch bilEftRejBch = saveBatch(batch);
        return bilEftRejBch.getBilEftRejBchId().getBrbBchNbr();
    }

    @Override
    public void scheduleBatch(String batchId, String entryDate) {
        StringBuilder parmListData = new StringBuilder();
        parmListData.append(StringUtils.rightPad("00", 2, BLANK_CHAR));
        parmListData.append(StringUtils.rightPad("", 10, BLANK_CHAR));
        parmListData.append(StringUtils.rightPad(entryDate, 10, BLANK_CHAR));
        parmListData.append(StringUtils.rightPad(batchId, 4, BLANK_CHAR));
        parmListData.append(StringUtils.rightPad(execContext.getUserSeqeunceId(), 8, BLANK_CHAR));
        parmListData.append(StringUtils.rightPad(entryDate, 10, BLANK_CHAR));

        ImmediateActivity immediateActivity = new ImmediateActivity();
        immediateActivity.setActivityDate(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        immediateActivity.setModuleName("BCMOEV");
        immediateActivity.setParameter(parmListData.toString());
        immediateActivity.setReferenceId(batchId);
        immediateActivity.setUserId(execContext.getUserSeqeunceId());
        pb360Service.scheduleImmediateActivity(immediateActivity);
    }

    private BilEftRejBch mapBatchToBilEftRejBch(BatchRejection batch) {
        BilEftRejBch bilEftRejBch = new BilEftRejBch();

        ZonedDateTime bilEntryDt = batch.getEntryDate();
        bilEftRejBch
                .setBilEftRejBchId(new BilEftRejBchId(bilEntryDt, batch.getBatchId(), execContext.getUserSeqeunceId(),
                        getBilEntrySeqNumber(bilEntryDt, batch.getBatchId(), execContext.getUserSeqeunceId())));

        bilEftRejBch.setBilAccountId(BLANK_STRING);
        bilEftRejBch.setBilAccountNumber(BLANK_STRING);
        bilEftRejBch.setBilThirdPartyId(BLANK_STRING);
        bilEftRejBch.setBilTtyNbr(BLANK_STRING);
        bilEftRejBch.setBilEftRejCd(BLANK_STRING);
        bilEftRejBch.setBilEftDrDt(DateRoutine.defaultDateTime());
        bilEftRejBch.setBilEftTraceNbr(BLANK_STRING);
        bilEftRejBch.setBilBankAccNbr(BLANK_STRING);
        bilEftRejBch.setBrbAceDt(DateRoutine.defaultDateTime());
        bilEftRejBch.setBilEftDrAmt(DECIMAL_ZERO);
        bilEftRejBch.setBrbReviewCd(BLANK_STRING);
        bilEftRejBch.setBrbRejStaInd(CHAR_O);
        bilEftRejBch.setBilCrCrdTypeCd(BLANK_CHAR);
        bilEftRejBch.setBilPayLngNm(BLANK_STRING);
        bilEftRejBch.setBilUnidCashId(BLANK_STRING);
        bilEftRejBch.setBilUnidCashDir(BLANK_STRING);
        bilEftRejBch.setAgencyNbr(BLANK_STRING);
        return bilEftRejBch;
    }

    private short getBilEntrySeqNumber(ZonedDateTime bilEntryDt, String batchId, String userId) {
        Integer bilEntrySequenceNumber = bilEftRejBchRepository.getMaxBilEntrySeq(batchId, bilEntryDt, userId);
        if (bilEntrySequenceNumber == null) {
            bilEntrySequenceNumber = 0;
        } else {
            bilEntrySequenceNumber = bilEntrySequenceNumber + 1;
        }
        return bilEntrySequenceNumber.shortValue();
    }

    private BilEftRejBch saveBatch(BatchRejection batch) {
        BilEftRejBch bilEftRejBch = mapBatchToBilEftRejBch(batch);
        bilEftRejBchRepository.save(bilEftRejBch);
        return bilEftRejBch;
    }

    @Override
    public String deleteBatch(String batchId, String entryDate) {

        Integer rejectionBatchCount = getBatchRejection(batchId, entryDate);

        if (rejectionBatchCount != null && rejectionBatchCount > 0) {

            List<BilEftRejBch> bilEftRejBchList = getRejectionPaymentCount(batchId, entryDate);
            if (bilEftRejBchList != null && !bilEftRejBchList.isEmpty()
                    && bilEftRejBchList.get(SHORT_ZERO).getBrbRejStaInd() != BLANK_CHAR) {
                throw new DataNotFoundException("batch.delete.not.valid");
            }

            bilEftRejBchRepository.deleteByBilEftRejBchIdBrbEntryDtAndBilEftRejBchIdBrbBchNbrAndBilEftRejBchIdUserId(
                    DateRoutine.dateTimeAsYYYYMMDD(entryDate), batchId, execContext.getUserSeqeunceId());
        } else {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }

        return "Success";
    }

    @Override
    @Transactional
    public void patchBatch(String batchId, String entryDate, BatchRejection batch) {

        Integer rejectionBatchCount = getBatchRejection(batchId, entryDate);

        if (rejectionBatchCount != null && rejectionBatchCount > 0) {

            List<BilEftRejBch> bilEftRejBchList = getRejectionPaymentData(batchId, entryDate);

            if (bilEftRejBchList != null && !bilEftRejBchList.isEmpty()) {

                for (BilEftRejBch bilEftRejBch : bilEftRejBchList) {
                    Integer traceNumberCount = bilEftRejBchRepository.countTraceNumberExits(
                            bilEftRejBch.getBilEftTraceNbr(), bilEftRejBch.getBilEftDrDt(), CHAR_A);
                    if (traceNumberCount == null || traceNumberCount == SHORT_ZERO) {
                        List<BilEftActivity> bilEftActivityList = bilEftActivityRepository.checkTraceNumber(
                                bilEftRejBch.getBilEftDrDt(), bilEftRejBch.getBilEftTraceNbr().trim(),
                                PageRequest.of(SHORT_ZERO, SHORT_ONE));

                        if (bilEftActivityList != null && !bilEftActivityList.isEmpty()
                                && (bilEftActivityList.get(SHORT_ZERO).getBeaTrsSta() == CHAR_X
                                        || bilEftActivityList.get(SHORT_ZERO).getBeaTrsSta() == CHAR_D
                                        || bilEftActivityList.get(SHORT_ZERO).getBeaTrsSta() == CHAR_R)) {
                            throw new DataNotFoundException("batch.transaction.rejected",
                                    new Object[] { bilEftRejBch.getBilEftTraceNbr(),
                                            DateRoutine.dateTimeAsMMDDYYYYAsString(bilEftRejBch.getBilEftDrDt()) });
                        }
                    } else {
                        throw new DataNotFoundException("batch.transaction.initiate",
                                new Object[] { bilEftRejBch.getBilEftTraceNbr(),
                                        DateRoutine.dateTimeAsMMDDYYYYAsString(bilEftRejBch.getBilEftDrDt()),
                                        bilEftRejBch.getBilEftRejBchId().getBrbBchNbr().trim() });
                    }

                }
            } else {
                throw new DataNotFoundException("empty.batch", new Object[] { batchId });
            }

            bilEftRejBchRepository.updateRow(CHAR_A, execContext.getApplicationDateTime(), batchId,
                    DateRoutine.dateTimeAsYYYYMMDD(entryDate), execContext.getUserSeqeunceId());
        } else {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }

    }

    private List<BilEftRejBch> getRejectionPaymentCount(String batchId, String entryDate) {
        return bilEftRejBchRepository.getbatchPayments(batchId, DateRoutine.dateTimeAsYYYYMMDD(entryDate),
                execContext.getUserSeqeunceId(), CHAR_O, PageRequest.of(SHORT_ZERO, SHORT_ONE));
    }

    private List<BilEftRejBch> getRejectionPaymentData(String batchId, String entryDate) {
        return bilEftRejBchRepository.getBilEftDataRows(batchId, DateRoutine.dateTimeAsYYYYMMDD(entryDate),
                execContext.getUserSeqeunceId());
    }

    private Integer getBatchRejection(String batchId, String entryDate) {
        return bilEftRejBchRepository.countbatchExits(batchId, DateRoutine.dateTimeAsYYYYMMDD(entryDate),
                execContext.getUserSeqeunceId());
    }

}
