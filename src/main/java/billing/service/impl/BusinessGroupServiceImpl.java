package billing.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilFunctionCtl;
import billing.data.repository.BilFunctionCtlRepository;
import billing.service.BusinessGroupService;

@Service
public class BusinessGroupServiceImpl implements BusinessGroupService {

    @Autowired
    private BilFunctionCtlRepository bilFunctionCtlRepository;

    @Override
    public String getBusinessGroup(String billFunctionType, String bilTypeCode) {

        if (billFunctionType != null && !billFunctionType.trim().isEmpty() && bilTypeCode != null
                && !bilTypeCode.trim().isEmpty()) {

            List<BilFunctionCtl> bilFunctionCtl = bilFunctionCtlRepository.getBussinessGroup(billFunctionType.trim(),
                    bilTypeCode.trim());
            if (bilFunctionCtl != null && !bilFunctionCtl.isEmpty()) {
                return bilFunctionCtl.get(0).getBilFunctionCtlId().getLobCd();
            }
        }
        return null;
    }

}
