package billing.service.impl;

import static billing.utils.BillingConstants.ERROR_CODE;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_20;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_4;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_6;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import billing.data.entity.BilBankEntry;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilEftExtPyt;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.BilOrgSupport;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.HolidayPrc;
import billing.data.entity.id.BilBankEntryId;
import billing.data.entity.id.BilCashEntryId;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilEftExtPytId;
import billing.data.entity.id.BilEftPendingTapeId;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.repository.BilBankEntryRepository;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilEftExtPytRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilOrgSupportRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.data.repository.HolidayPrcRepository;
import billing.model.Payment;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilCashEntryMethodCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.PayableItemCode;
import billing.utils.BillingConstants.PaymentType;
import core.api.ExecContext;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import core.utils.IdGenerator;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;

@Service("unIdentifiedAccountPaymentService")
public class UnIdentifiedAccountPaymentServiceImpl {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private BilOrgSupportRepository bilOrgSupportRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilBankEntryRepository bilBankEntryRepository;

    @Autowired
    private HolidayPrcRepository holidayPrcRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private BilEftExtPytRepository bilEftExtPytRepository;

    @Value("${onetimepayment.integration}")
    private String oneTimePaymentIntegration;

    @Value("${agencysweeppayment.integration}")
    private String agencyPaymentIntegration;

    @Value("${paymentIntegration.provider}")
    private String providerIntegration;

    private static final String RULE_XHOL = "XHOL";
    private static final String HOLIDAY_8 = "H8";
    private static final String HOLIDAY_6 = "H6";
    private static final String HOLIDAY_5 = "H5";
    private static final String HOLIDAY_3 = "H3";
    private static final String HOLIDAY_1 = "H1";

    public Payment makeAPayment(Payment payment) {

        ZonedDateTime se3DateTime = execContext.getApplicationDate();
        short fileTypeCode = 0;
        String bilBankCode;
        String bilEntryNumber;
        Short bilDtbSeqNbr = 0;
        char arspCode;
        String autApv;
        String crcrdArspTyc;
        Integer bilCashEntrySeqNbr = 0;
        String orderId;
        String techKeyId;
        ZonedDateTime tapeDate = se3DateTime;

        bilEntryNumber = getUniqueBilEntryNumber(se3DateTime);
        payment.setUserId(execContext.getUserSeqeunceId());
        bilCashEntrySeqNbr = getBilEntrySequenceNumber(bilEntryNumber, se3DateTime, payment.getUserId());
        payment.setBatchId(bilEntryNumber);
        payment.setPaymentSequenceNumber(bilCashEntrySeqNbr.shortValue());

        boolean isOneTimePaymentIntegration = false;
        if (StringUtils.isNotBlank(providerIntegration)) {
            isOneTimePaymentIntegration = BooleanUtils.toBoolean(oneTimePaymentIntegration);
        }

        techKeyId = generateUnidCashId();
        orderId = payment.getTransactionId();

        if (payment.getAuthorization() == null) {
            autApv = BLANK_STRING;
        } else {
            autApv = payment.getAuthorization();
        }
        if (payment.getAuthorizationResponse() == null) {
            arspCode = BLANK_CHAR;
        } else {
            arspCode = payment.getAuthorizationResponse().charAt(SHORT_ZERO);
        }

        if (payment.getAuthorizationResponseType() == null) {
            crcrdArspTyc = BLANK_STRING;
        } else {
            crcrdArspTyc = payment.getAuthorizationResponseType();
        }
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
            fileTypeCode = getFileTypeCodeFromBilRuleUct(se3DateTime);
            bilBankCode = getBankCode(payment.getPaymentType(), BilDesReasonType.ECH.getValue(), fileTypeCode);
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            fileTypeCode = determineFileTypeCode(isOneTimePaymentIntegration ? payment.getPaymentType() : BLANK_STRING,
                    "1XA");
            bilBankCode = getBankCode(BLANK_STRING, "ACH", fileTypeCode);
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            String agencySweepParam = getAgencySweepFileTypeCode(payment.getPaymentMethod(), payment.getPaymentType());
            if (agencySweepParam != null) {
                fileTypeCode = Short.valueOf(agencySweepParam.substring(0, 3));
                tapeDate = getTapeDate(tapeDate, agencySweepParam);
            }
            bilBankCode = getBankCode(BLANK_STRING, "ACH", fileTypeCode);
        } else {
            fileTypeCode = determineFileTypeCode(payment.getPaymentType(), null);
            bilBankCode = getBankCode(BLANK_STRING, BilDesReasonType.DIGITAL_WALLET_HOUSE.getValue(), fileTypeCode);
        }

        bilDtbSeqNbr = getBilDtbSeqNbr(se3DateTime);

        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {

            insertBilCashEntryTotal(payment, se3DateTime, bilEntryNumber, bilBankCode);

            insertBilCashEntry(payment, se3DateTime, bilEntryNumber, bilCashEntrySeqNbr, arspCode, autApv, crcrdArspTyc,
                    orderId);

            insertBilUnIdCash(payment, bilBankCode, bilDtbSeqNbr,  se3DateTime, arspCode, autApv, crcrdArspTyc,
                    techKeyId);

            insertBilEFtPendingTape(payment, bilDtbSeqNbr, se3DateTime, tapeDate, fileTypeCode, crcrdArspTyc, orderId,
                    techKeyId);

            insertBilBankEntry(payment, bilCashEntrySeqNbr, bilEntryNumber, se3DateTime);

        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {

            orderId = SEPARATOR_BLANK;
            payment.setPaymentId(BLANK_STRING);

            insertBilCashEntryTotal(payment, se3DateTime, bilEntryNumber, bilBankCode);

            insertBilCashEntry(payment, se3DateTime, bilEntryNumber, bilCashEntrySeqNbr, arspCode, autApv, crcrdArspTyc,
                    orderId);

            insertBilUnIdCash(payment, bilBankCode, bilDtbSeqNbr,  se3DateTime, arspCode, autApv, crcrdArspTyc,
                    techKeyId);

            insertBilEFtPendingTape(payment, bilDtbSeqNbr, se3DateTime, tapeDate, fileTypeCode, crcrdArspTyc, orderId,
                    techKeyId);

        } else {
            insertBilCashEntryTotal(payment, se3DateTime, bilEntryNumber, bilBankCode);

            insertBilCashEntry(payment, se3DateTime, bilEntryNumber, bilCashEntrySeqNbr, arspCode, autApv, crcrdArspTyc,
                    orderId);

            insertBilUnIdCash(payment, bilBankCode, bilDtbSeqNbr,  se3DateTime, arspCode, autApv, crcrdArspTyc,
                    techKeyId);

            insertBilEFtPendingTape(payment, bilDtbSeqNbr, se3DateTime, tapeDate, fileTypeCode, crcrdArspTyc, orderId,
                    techKeyId);
        }

        callFinancialApi(payment, execContext.getApplicationDateTime(), bilBankCode,
                BilCashEntryMethodCode.UNIDENTIFIED.getValue(), PayableItemCode.SUS.getValue());

        payment.setPostedDate(se3DateTime);
        payment.setUserId(execContext.getUserId());
        payment.setIsUnidentified(true);
        return payment;

    }

    private String getAgencySweepFileTypeCode(String paymentMethod, String paymentType) {
        Short fileType = determineFileTypeCode(paymentMethod, paymentType, "ASW");

        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("AGSW", BLANK_STRING, BLANK_STRING, BLANK_STRING);
        if (bilRulesUct != null) {
            String agencySweepParam = bilRulesUct.getBrtParmListTxt();
            if (fileType != null && fileType != 0) {
                agencySweepParam = StringUtils.leftPad(String.valueOf(fileType), 3, CHAR_ZERO)
                        + agencySweepParam.substring(3);
                return agencySweepParam;
            }
        }
        return null;
    }

    private short determineFileTypeCode(String paymentMethod, String paymentType, String bilDesReasonCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(paymentType,
                BilDesReasonType.FTC.getValue(), execContext.getLanguage());
        if (busCdTranslation == null) {
            busCdTranslation = busCdTranslationRepository.findByCodeAndType(bilDesReasonCode,
                    BilDesReasonType.FTC.getValue(), execContext.getLanguage());
        }
        if (busCdTranslation == null) {
            if (!paymentMethod.equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
                throw new InvalidDataException("bankCode.invalid");
            } else {
                return 0;
            }
        }
        return Short.parseShort(busCdTranslation.getBusDescription().trim());
    }

    private ZonedDateTime getTapeDate(ZonedDateTime tapeDate, String agencySweepParam) {
        tapeDate = DateRoutine.adjustDateWithOffset(tapeDate, true, Integer.valueOf(agencySweepParam.substring(3, 6)),
                '+', tapeDate);
        String holidayCd = "";
        if (agencySweepParam.length() > 7) {
            holidayCd = agencySweepParam.substring(7, 9);
        }
        return holidayDateProcessing(tapeDate, holidayCd);
    }

    private ZonedDateTime holidayDateProcessing(ZonedDateTime dateTime, String holidayCd) {
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(RULE_XHOL, BLANK_STRING, BLANK_STRING,
                BLANK_STRING, dateTime, dateTime);
        char holidayValue;
        boolean isHoliday = false;
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == 'Y' && bilRulesUct.getBrtParmListTxt() != null) {
            if (holidayCd.equals("")) {
                holidayCd = bilRulesUct.getBrtParmListTxt().trim();
            }
            if (holidayCd.equalsIgnoreCase(HOLIDAY_8)) {
                holidayValue = '9';
            } else if (holidayCd.equalsIgnoreCase(HOLIDAY_6)) {
                holidayValue = '7';
            } else if (holidayCd.equalsIgnoreCase(HOLIDAY_5)) {
                holidayValue = '4';
            } else if (bilRulesUct.getBrtParmListTxt().trim().equalsIgnoreCase(HOLIDAY_3)) {
                holidayValue = '4';
            } else if (holidayCd.equalsIgnoreCase(HOLIDAY_1)) {
                holidayValue = '2';
            } else {
                holidayValue = holidayCd.trim().replace("H", "").charAt(0);
            }
            HolidayPrc holidayPrc = holidayPrcRepository.findHolidayRow(dateTime);
            if (holidayPrc != null) {
                isHoliday = true;
            }
            dateTime = DateRoutine.dateTimeWithHolidayProcess(dateTime, holidayValue, isHoliday);
            boolean holidayDateCheck = true;
            while (holidayDateCheck) {
                isHoliday = false;
                holidayPrc = holidayPrcRepository.findHolidayRow(dateTime);
                if (holidayPrc != null) {
                    isHoliday = true;
                }
                if (isHoliday) {
                    dateTime = DateRoutine.dateTimeWithHolidayProcess(dateTime, holidayValue, isHoliday);
                } else {
                    holidayDateCheck = false;
                }
            }
        }
        return dateTime;
    }

    private Short getBilDtbSeqNbr(ZonedDateTime se3DateTime) {
        Short bilDtbSeqNbr = 0;
        bilDtbSeqNbr = bilUnIdCashRepository.findMaxSequenceNumber(se3DateTime);

        if (bilDtbSeqNbr == null) {
            bilDtbSeqNbr = 0;
        } else {
            bilDtbSeqNbr = (short) (bilDtbSeqNbr + 1);
        }
        return bilDtbSeqNbr;
    }

    private void insertBilUnIdCash(Payment payment, String bilBanckCode, Short bilDtbSeqNbr,
            ZonedDateTime se3DateTime, char arspCode, String autApv, String crcrdArspTyc, String techKeyId) {

        ZonedDateTime entryDate = se3DateTime;
        BilUnIdCash bilUnIdCash = new BilUnIdCash();
        bilUnIdCash.setBilUnIdCashId(
                new BilUnIdCashId(se3DateTime, bilDtbSeqNbr, payment.getBatchId(), payment.getUserId(), se3DateTime));
        bilUnIdCash.setBilEntrySeqNbr(payment.getPaymentSequenceNumber());

        if (payment.getIdentifierType().equals(AccountType.BILLACCOUNT.toString())) {
            bilUnIdCash.setBilAccountNbr(payment.getIdentifier());
            bilUnIdCash.setBilTtyNbr(SEPARATOR_BLANK);
            bilUnIdCash.setBilAgtActNbr(SEPARATOR_BLANK);
        } else if (payment.getIdentifierType().equals(AccountType.GROUP_ACCOUNT.toString())) {
            bilUnIdCash.setBilAccountNbr(BLANK_STRING);
            bilUnIdCash.setBilTtyNbr(payment.getIdentifier());
            bilUnIdCash.setBilAgtActNbr(SEPARATOR_BLANK);
        } else {
            bilUnIdCash.setBilAccountNbr(BLANK_STRING);
            bilUnIdCash.setBilTtyNbr(BLANK_STRING);
            bilUnIdCash.setBilAgtActNbr(SEPARATOR_BLANK);
        }

        bilUnIdCash.setBilAdditionalId(BLANK_STRING);
        bilUnIdCash.setPolNbr(payment.getPolicyNumber());
        bilUnIdCash.setPolSymbolCd(payment.getPolicySymbol());
        bilUnIdCash.setBilAdjDueDt(payment.getDueDate());
        bilUnIdCash.setBilPblItemCd(payment.getPayableItem());
        bilUnIdCash.setBilRctAmt(payment.getPaymentAmount());
        bilUnIdCash.setBilRctTypeCd(payment.getPaymentType());
        bilUnIdCash.setBilRctId(payment.getPaymentId() != null ? payment.getPaymentId() : SEPARATOR_BLANK);
        bilUnIdCash.setBilRctCltId(BLANK_STRING);
        bilUnIdCash.setBilRctAdrSeq(SHORT_ZERO);
        bilUnIdCash.setBilRctReceiveDt(entryDate);
        bilUnIdCash.setBilTaxYearNbr((short) entryDate.getYear());
        bilUnIdCash.setBilDspDt(entryDate);
        bilUnIdCash.setBilDspTypeCd("SP");
        bilUnIdCash.setBilDspReasonCd(payment.getSuspenseReason());
        if (payment.getSuspenseReason().equals(BLANK_STRING)) {
            bilUnIdCash.setBilManualSusInd(CHAR_N);
        } else {
            bilUnIdCash.setBilManualSusInd(CHAR_Y);
        }
        bilUnIdCash.setBilPayeeCltId(BLANK_STRING);
        bilUnIdCash.setBilPayeeAdrSeq(SHORT_ZERO);

        bilUnIdCash.setDwsCkDrfNbr(SHORT_ZERO);
        bilUnIdCash.setBilBankCd(bilBanckCode);
        bilUnIdCash.setBilDepositDt(DateRoutine.defaultDateTime());
        bilUnIdCash.setBilAcyTs(execContext.getApplicationDateTime());
        bilUnIdCash.setBilCshEtrMthCd(payment.getPaymentMethod().charAt(SHORT_ZERO));
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())
                || payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilUnIdCash.setBilCrcrdTypeCd(BLANK_CHAR);
            bilUnIdCash.setBilCrcrdArspCd(BLANK_CHAR);
            bilUnIdCash.setBilCrcrdActNbr(BLANK_STRING);
            bilUnIdCash.setBilCrcrdExpDt(BLANK_STRING);
            bilUnIdCash.setBilCrcrdAutApv(BLANK_STRING);
        } else {
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
                bilUnIdCash.setBilCrcrdTypeCd(payment.getCreditCardType().charAt(SHORT_ZERO));
                bilUnIdCash.setBilCrcrdActNbr(payment.getCreditCardNumber());
            } else {
                bilUnIdCash.setBilCrcrdTypeCd(payment.getWalletType().charAt(SHORT_ZERO));
                bilUnIdCash.setBilCrcrdActNbr(payment.getWalletNumber());
            }

            bilUnIdCash.setBilCrcrdArspCd(arspCode);
            bilUnIdCash.setBilCrcrdExpDt(payment.getCreditCardExpiryDate());
            bilUnIdCash.setBilCrcrdAutApv(autApv);
        }
        bilUnIdCash.setBilCrcrdArspTyc(crcrdArspTyc);
        bilUnIdCash.setBucRevsDsbInd(CHAR_N);
        bilUnIdCash.setBilCwaId(BLANK_STRING);
        bilUnIdCash.setDdsDsbDt(DateRoutine.defaultDateTime());
        bilUnIdCash.setDwsStatusCd(BLANK_CHAR);
        bilUnIdCash.setBilDsbId(BLANK_STRING);
        bilUnIdCash.setBilChkPrdMthCd(BLANK_CHAR);
        bilUnIdCash.setActivityUserId(payment.getUserId());
        bilUnIdCash.setBilPstmrkDt(entryDate);
        bilUnIdCash.setBilPstmrkCd(BLANK_CHAR);
        bilUnIdCash.setBilRctCmt(payment.getPaymentComments());
        bilUnIdCash.setBillUnidCashId(techKeyId);
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilUnIdCash.setAgencyNbr(payment.getAgencyNumber());
            bilUnIdCash.setPaymentProfileId(payment.getPaymentProfileId());
        } else {
            bilUnIdCash.setAgencyNbr(SEPARATOR_BLANK);
        }
        bilUnIdCash.setBilCrcrdArspTyc(BLANK_STRING);
        bilUnIdCash.setBilBusGrpCd(payment.getBusinessGroup());
        bilUnIdCashRepository.save(bilUnIdCash);

    }

    private void callFinancialApi(Payment payment, ZonedDateTime applicationDate, String bankCode,
            String transactionObjectCode, String transactionActionCode) {

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();

        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(BLANK_STRING);
        financialApiActivity.setApplicationName(ApplicationName.BACPCCP.toString());
        financialApiActivity.setUserId(payment.getUserId());
        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setAppProgramId(ApplicationProgramIdentifier.BACPCCP.toString());
        financialApiActivity.setActivityAmount(payment.getPaymentAmount());
        financialApiActivity.setActivityNetAmount(payment.getPaymentAmount());
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setPolicyId(payment.getPolicyNumber());
        financialApiActivity.setBilTypeCode(BLANK_STRING);
        financialApiActivity.setBilClassCode(BLANK_STRING);
        financialApiActivity.setPayableItemCode(payment.getPayableItem());
        if (!payment.getIdentifierType().equals(AccountType.GROUP_ACCOUNT.toString())) {
            financialApiActivity.setBilAccountId(payment.getIdentifier());
        } else {
            financialApiActivity.setAgentTtyId(payment.getIdentifier());
        }

        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setBilSourceCode(payment.getPaymentMethod());
        financialApiActivity.setMasterCompanyNbr(BillingConstants.MASTER_COMPANY_NUMBER);
        financialApiActivity.setBilDatabaseKey(getBilDatabaseKey(execContext.getApplicationDate(), payment.getBatchId(),
                payment.getUserId(), payment.getPaymentSequenceNumber()));
        financialApiActivity.setStatusCode(BLANK_CHAR);
        financialApiActivity.setBilReceiptTypeCode(payment.getPaymentType());
        financialApiActivity.setBilReasonCode(payment.getSuspenseReason());
        financialApiActivity.setSummaryEffectiveDate(applicationDate);
        financialApiActivity.setOperatorId(payment.getUserId());
        financialApiActivity.setCurrencyCode(payment.getCurrency());

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                execContext.getApplicationDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
    }

    private String getBilDatabaseKey(ZonedDateTime entryDate, String entryNumber, String operatorId,
            short bilDtbSequenceNumber) {
        String seqNumberSign = "+";

        String dataString = DateRoutine.dateTimeAsYYYYMMDDString(entryDate);
        dataString += entryNumber;
        dataString += StringUtils.rightPad(operatorId, 8, BLANK_CHAR);
        dataString += seqNumberSign;
        dataString += StringUtils.leftPad(String.valueOf(bilDtbSequenceNumber), 5, CHAR_ZERO);

        return dataString;
    }

    private void insertBilEFtPendingTape(Payment payment, Short bilDtbSeqNbr, ZonedDateTime se3DateTime,
            ZonedDateTime tapeDate, short fileTypeCode, String crcrdArspTyc, String orderId, String techKeyId) {

        BilEftPendingTape bilEftPendingTape = new BilEftPendingTape();
        BilEftPendingTapeId bilEftPendingTapeId = null;
        bilEftPendingTapeId = new BilEftPendingTapeId(techKeyId, 'U', tapeDate, se3DateTime, SHORT_ZERO);

        bilEftPendingTape.setBilEftPendingTapeId(bilEftPendingTapeId);
        bilEftPendingTape.setEftDrDate(tapeDate);
        bilEftPendingTape.setEftDrAmount(payment.getPaymentAmount());
        bilEftPendingTape.setEftPostPaymentDate(se3DateTime);
        bilEftPendingTape.setEftNotIndicator('N');
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
            bilEftPendingTape.setEftReceivedType('U');
            bilEftPendingTape.setBusinessActivityCd(getBusinessActivityCode(crcrdArspTyc));
            bilEftPendingTape.setWebAutomatedIndicator(BLANK_CHAR);
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
            bilEftPendingTape.setEftReceivedType(EftRecordType.ONE_TIME_ACH.getValue());
            bilEftPendingTape.setBusinessActivityCd(getBusinessActivityCode(payment.getAccountType()));
            bilEftPendingTape.setWebAutomatedIndicator(CHAR_N);
        } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilEftPendingTape.setEftReceivedType(EftRecordType.AGENCY_SWEEP.getValue());
            bilEftPendingTape.setBusinessActivityCd(getBusinessActivityCode(payment.getAccountType()));
            bilEftPendingTape.setWebAutomatedIndicator(CHAR_Y);
        } else {
            bilEftPendingTape.setEftReceivedType(EftRecordType.DIGITAL_WALLET.getValue());
            bilEftPendingTape.setBusinessActivityCd(getBusinessActivityCode(crcrdArspTyc));
            bilEftPendingTape.setWebAutomatedIndicator(BLANK_CHAR);
        }
        bilEftPendingTape.setFileTypeCd(fileTypeCode);
        bilEftPendingTape.setDisbursementId(SEPARATOR_BLANK);
        bilEftPendingTape.setReceiptSequenceNumber(bilDtbSeqNbr);
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilEftPendingTape.setAgencyNbr(payment.getAgencyNumber());
        } else {
            bilEftPendingTape.setAgencyNbr(SEPARATOR_BLANK);
        }
        bilEftPendingTape.setInsertedRowTime(execContext.getApplicationDateTime());
        bilEftPendingTape.setPaymentOrderId(orderId);
        bilEftPendingTape.setBilBusGrpCd(payment.getBusinessGroup());
        bilEftPendingTapeRepository.save(bilEftPendingTape);

        if (StringUtils.isNotBlank(payment.getWalletIdentifier())) {
            insertBilEftExtPayment(payment, bilEftPendingTape);
        }
    }

    private void insertBilEftExtPayment(Payment payment, BilEftPendingTape bilEftPendingTape) {
        BilEftExtPyt bilEftExtPyt = new BilEftExtPyt();
        BilEftExtPytId bilEftExtPytId = new BilEftExtPytId();
        bilEftExtPytId.setBilTchKeyId(payment.getAccountId());
        bilEftExtPytId.setBilTchKeyTypCd(bilEftPendingTape.getBilEftPendingTapeId().getTechnicalKeyType());
        bilEftExtPytId.setBilEftTapeDt(bilEftPendingTape.getBilEftPendingTapeId().getEftTapeDate());
        bilEftExtPytId.setBilEftAcyDt(bilEftPendingTape.getBilEftPendingTapeId().getEftActivityDate());
        bilEftExtPytId.setBilSeqNbr(bilEftPendingTape.getBilEftPendingTapeId().getSequenceNbr());
        bilEftExtPyt.setBilWalTypeCd(payment.getWalletType());
        bilEftExtPyt.setBilWalIdentifier(payment.getWalletIdentifier());
        bilEftExtPyt.setBilEftExtPytId(bilEftExtPytId);
        bilEftExtPytRepository.saveAndFlush(bilEftExtPyt);
    }

    private char getBusinessActivityCode(String crcrdArspTyc) {
        if (busCdTranslationRepository.findByCodeAndType(crcrdArspTyc, BusCodeTranslationType.CCD.getValue(),
                execContext.getLanguage()) != null) {
            return CHAR_Y;
        }
        return CHAR_N;
    }

    private String generateUnidCashId() {
        String unidCashId;
        do {
            unidCashId = generateUniqueId(GENERATED_ID_LENGTH_6);
            unidCashId = "XX" + unidCashId;
        } while (bilUnIdCashRepository.getBillUnidCashId(unidCashId) != 0);
        return unidCashId;
    }

    private String getUniqueBilEntryNumber(ZonedDateTime se3DateTime) {
        String bilEntryNumber;
        do {
            bilEntryNumber = generateUniqueId(GENERATED_ID_LENGTH_4);
        } while (bilCashEntryTotRepository.getBilEntryNumber(se3DateTime, bilEntryNumber,
                execContext.getUserSeqeunceId()) != 0);
        return bilEntryNumber;
    }

    private String generateUniqueId(int generatedIdLength) {
        return IdGenerator.generateUniqueIdUpperCase(generatedIdLength);
    }

    private Integer getBilEntrySequenceNumber(String bilEntryNumber, ZonedDateTime se3DateTime, String userId) {
        Integer bilEntrySequenceNumber = bilCashEntryRepository.getMaxBilEntrySeq(bilEntryNumber, se3DateTime, userId);
        if (bilEntrySequenceNumber == null) {
            bilEntrySequenceNumber = 0;
        } else {
            bilEntrySequenceNumber = bilEntrySequenceNumber + 1;
        }
        return bilEntrySequenceNumber;
    }

    private short getFileTypeCodeFromBilRuleUct(ZonedDateTime se3DateTime) {
        final String STRING_1XCC = "1XCC";
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(STRING_1XCC, SEPARATOR_BLANK, SEPARATOR_BLANK,
                SEPARATOR_BLANK, se3DateTime, se3DateTime);
        if (bilRulesUct != null) {
            return new Short(bilRulesUct.getBrtParmListTxt());
        }
        return 0;
    }

    private String getBankCode(String paymentType, String bilDesReasonType, Short fileType) {

        short fileTypeCode = 0;

        if (!paymentType.equals(BLANK_STRING)) {
            fileTypeCode = determineFileTypeCode(paymentType, "CPY");
        } else {
            fileTypeCode = fileType;
        }

        BilOrgSupport bilOrgSupport = bilOrgSupportRepository.getByBilFileTypeCd(fileTypeCode, bilDesReasonType);
        if (bilOrgSupport != null) {
            return bilOrgSupport.getBilBankCd().trim();
        }
        return SEPARATOR_BLANK;
    }

    private short determineFileTypeCode(String paymentType, String bilDesReasonCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(paymentType,
                BilDesReasonType.FTC.getValue(), execContext.getLanguage());
        if (busCdTranslation == null) {
            busCdTranslation = busCdTranslationRepository.findByCodeAndType(bilDesReasonCode,
                    BilDesReasonType.FTC.getValue(), execContext.getLanguage());
        }
        if (busCdTranslation == null) {
            throw new InvalidDataException("bankCode.invalid");
        }
        return Short.parseShort(busCdTranslation.getBusDescription().trim());
    }

    private void insertBilCashEntryTotal(Payment payment, ZonedDateTime se3DateTime, String bilEntryNumber,
            String bilBankCode) {
        BilCashEntryTot bilCashEntryTot = new BilCashEntryTot();
        bilCashEntryTot.setBilCashEntryTotId(new BilCashEntryTotId(se3DateTime, bilEntryNumber, payment.getUserId()));
        bilCashEntryTot.setBctTotCashAmt(payment.getPaymentAmount());
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())
                || payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilCashEntryTot.setBctDepositInd(CHAR_N);
            bilCashEntryTot.setBctAcceptInd(CHAR_Y);
            bilCashEntryTot.setBilBankCd(bilBankCode);
            bilCashEntryTot.setBctDepBankCd(bilCashEntryTot.getBilBankCd());
            bilCashEntryTot.setBilDepositDt(se3DateTime);
        } else {
            bilCashEntryTot.setBctDepositInd(CHAR_N);
            bilCashEntryTot.setBctAcceptInd(CHAR_Y);
            bilCashEntryTot.setBilBankCd(bilBankCode);
            bilCashEntryTot.setBctDepBankCd(SEPARATOR_BLANK);
            bilCashEntryTot.setBilDepositDt(DateRoutine.defaultDateTime());
        }
        bilCashEntryTot.setBctAcceptDt(se3DateTime);
        bilCashEntryTot.setBilCshEtrMthCd(payment.getPaymentMethod().charAt(0));
        bilCashEntryTot.setCurrencyCd(SEPARATOR_BLANK);
        bilCashEntryTotRepository.save(bilCashEntryTot);
    }

    private void insertBilCashEntry(Payment payment, ZonedDateTime se3DateTime, String bilEntryNumber,
            Integer bilEntrySequenceNumber, char arspCode, String autApv, String crcrdArspTyc, String orderId) {
        BilCashEntry bilCashEntry = new BilCashEntry();
        bilCashEntry.setBilCashEntryId(new BilCashEntryId(se3DateTime, bilEntryNumber, payment.getUserId(),
                bilEntrySequenceNumber.shortValue()));
        if (payment.getIdentifierType().equals(AccountType.BILLACCOUNT.toString())) {
            bilCashEntry.setBilAccountNbr(payment.getIdentifier());
            bilCashEntry.setBilTtyNbr(SEPARATOR_BLANK);
            bilCashEntry.setBilAgtActNbr(SEPARATOR_BLANK);
        } else if (payment.getIdentifierType().equals(AccountType.GROUP_ACCOUNT.toString())) {
            bilCashEntry.setBilAccountNbr(BLANK_STRING);
            bilCashEntry.setBilTtyNbr(payment.getIdentifier());
            bilCashEntry.setBilAgtActNbr(SEPARATOR_BLANK);
        } else {
            bilCashEntry.setBilAccountNbr(BLANK_STRING);
            bilCashEntry.setBilTtyNbr(BLANK_STRING);
            bilCashEntry.setBilAgtActNbr(SEPARATOR_BLANK);
        }

        bilCashEntry.setBilAdditionalId(payment.getAdditionalId());
        bilCashEntry.setPolSymbolCd(payment.getPolicySymbol());
        bilCashEntry.setPolNbr(payment.getPolicyNumber());
        bilCashEntry.setBilPblItemCd(payment.getPayableItem());
        bilCashEntry.setBilAdjDueDt(payment.getDueDate());
        bilCashEntry.setBilRctAmt(payment.getPaymentAmount());
        bilCashEntry.setBilRctTypeCd(payment.getPaymentType());
        bilCashEntry.setBilSusReasonCd(SEPARATOR_BLANK);
        bilCashEntry.setBilManualSusInd(CHAR_N);
        bilCashEntry.setBilRctId(payment.getPaymentId());
        bilCashEntry.setBilRctCltId(SEPARATOR_BLANK);
        bilCashEntry.setBilRctAdrSeq(SHORT_ZERO);
        bilCashEntry.setBilRctReceiveDt(se3DateTime);
        bilCashEntry.setBilTaxYearNbr((short) se3DateTime.getYear());
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())
                || payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilCashEntry.setBilCrcrdTypeCd(BLANK_CHAR);
            bilCashEntry.setBilCrcrdArspCd(BLANK_CHAR);
            bilCashEntry.setBilCrcrdActNbr(SEPARATOR_BLANK);
            bilCashEntry.setBilCrcrdExpDt(SEPARATOR_BLANK);
            bilCashEntry.setBilCrcrdAutApv(SEPARATOR_BLANK);
            bilCashEntry.setBilCrcrdPstCd(SEPARATOR_BLANK);
        } else {
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
                bilCashEntry.setBilCrcrdTypeCd(payment.getCreditCardType().charAt(SHORT_ZERO));
                bilCashEntry.setBilCrcrdActNbr(payment.getCreditCardNumber());
            } else {
                bilCashEntry.setBilCrcrdTypeCd(payment.getWalletType().charAt(SHORT_ZERO));
                bilCashEntry.setBilCrcrdActNbr(payment.getWalletNumber());
            }

            bilCashEntry.setBilCrcrdArspCd(arspCode);
            bilCashEntry.setBilCrcrdExpDt(payment.getCreditCardExpiryDate());
            bilCashEntry.setBilCrcrdAutApv(autApv);
            bilCashEntry.setBilCrcrdPstCd(payment.getPostalCode());
        }
        bilCashEntry.setBilCrcrdArspTyc(crcrdArspTyc);
        bilCashEntry.setBilCwaId(SEPARATOR_BLANK);
        bilCashEntry.setBilPstMrkDt(se3DateTime);
        bilCashEntry.setBilPstMrkCd(BLANK_CHAR);
        bilCashEntry.setBilRctCmt(SEPARATOR_BLANK);
        bilCashEntry.setBecChgUserId(payment.getUserId());
        if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
            bilCashEntry.setAgencyNbr(payment.getAgencyNumber());
        } else {
            bilCashEntry.setAgencyNbr(SEPARATOR_BLANK);
        }

        bilCashEntry.setInsertedRowTs(execContext.getApplicationDateTime());
        bilCashEntry.setBilPmtOrderId(orderId);
        bilCashEntry.setBilBusGrpCd(payment.getBusinessGroup());
        bilCashEntryRepository.save(bilCashEntry);
    }

    private void insertBilBankEntry(Payment payment, Integer cashRctSeqNbr, String bilEntryNumber,
            ZonedDateTime se3DateTime) {
        BilBankEntry bilBankEntry = new BilBankEntry();
        bilBankEntry.setBilBankEntryId(
                new BilBankEntryId(se3DateTime, bilEntryNumber, payment.getUserId(), cashRctSeqNbr.shortValue()));

        bilBankEntry.setBankAccountNbr(payment.getBankAccountNumber());
        bilBankEntry.setRoutingTransitNbr(payment.getRoutingNumber());
        bilBankEntry.setAccountTypCd(payment.getAccountType());
        bilBankEntry.setWebAuthInd(CHAR_N);
        bilBankEntry.setConfirmationNbr(generateConfirmationNumber());
        bilBankEntryRepository.save(bilBankEntry);

    }

    private String generateConfirmationNumber() {
        String uniqueId;
        do {
            uniqueId = generateUniqueId(GENERATED_ID_LENGTH_20);
        } while (bilBankEntryRepository.findByConfirmationNbr(uniqueId) != null);
        return uniqueId;
    }

}
