package billing.service.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.DECIMAL_ZERO;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilAccountBalance;
import billing.data.entity.id.BilAccountBalanceId;
import billing.data.repository.BilAccountBalanceRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.service.BillingAccountBalanceService;
import billing.utils.BillingConstants.BilDspTypeCode;
import core.api.ExecContext;
import core.datetime.service.DateService;

@Service
public class BillingAccountBalanceServiceImpl implements BillingAccountBalanceService {

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilAccountBalanceRepository bilAccountBalanceRepository;

    @Autowired
    private DateService dateService;

    private static final List<Character> reverseIndicator = Arrays.asList(BLANK_CHAR, CHAR_N);
    private static final List<String> dispositionTypeCodeList = Arrays.asList(BilDspTypeCode.SUSPENDED.getValue(),
            BilDspTypeCode.REVERSED.getValue(), BilDspTypeCode.WRITE_OFF.getValue(),
            BilDspTypeCode.DISBURSED.getValue());
    private static final String UNIDENTIFIED_CASH = "UNIDCASH";

    @Override
    @Transactional
    public void saveUnidentifiedBalance() {
        Double suspendedAmount = DECIMAL_ZERO;
        Double reversedAmount = DECIMAL_ZERO;
        Double disbursedAmount = DECIMAL_ZERO;
        Double writeOffAmount = DECIMAL_ZERO;

        List<Object[]> unidentifiedCashObjectList = bilUnIdCashRepository.getAmountByDspTypeCode(reverseIndicator,
                dispositionTypeCodeList);
        for (Object[] unidentifiedCashObject : unidentifiedCashObjectList) {
            switch (BilDspTypeCode.getEnumKey(unidentifiedCashObject[1].toString())) {
            case SUSPENDED:
                suspendedAmount = Double.valueOf(unidentifiedCashObject[0].toString());
                break;
            case DISBURSED:
                disbursedAmount = Double.valueOf(unidentifiedCashObject[0].toString());
                break;
            case REVERSED:
                reversedAmount = Double.valueOf(unidentifiedCashObject[0].toString());
                break;
            case WRITE_OFF:
                writeOffAmount = Double.valueOf(unidentifiedCashObject[0].toString());
                break;
            default:
                break;
            }
        }
        saveBilAccountBalance(suspendedAmount, disbursedAmount, reversedAmount, writeOffAmount);
    }

    private void saveBilAccountBalance(Double suspendedAmount, Double disbursedAmount, Double reversedAmount,
            Double writeOffAmount) {
        BilAccountBalance bilAccountBalance = new BilAccountBalance();
        bilAccountBalance
                .setBillAccountBalanceId(new BilAccountBalanceId(UNIDENTIFIED_CASH, dateService.currentDateTime()));
        bilAccountBalance.setUserId(execContext.getUserSeqeunceId());
        bilAccountBalance.setBatStatusCode(BLANK_CHAR);
        bilAccountBalance.setTotCashDsbAmt(disbursedAmount);
        bilAccountBalance.setTotCashRevAmt(reversedAmount);
        bilAccountBalance.setTotCashSusAmt(suspendedAmount);
        bilAccountBalance.setTotCashWroAmt(writeOffAmount);
        bilAccountBalanceRepository.save(bilAccountBalance);
    }
}
