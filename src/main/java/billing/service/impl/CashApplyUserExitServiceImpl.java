package billing.service.impl;

import java.util.Arrays;

import org.springframework.stereotype.Service;

import billing.data.repository.BilPolicyTermRepository;
import billing.service.CashApplyUserExitService;

@Service
public class CashApplyUserExitServiceImpl implements CashApplyUserExitService {

    private BilPolicyTermRepository bilPolicyTermRepository;

    @Override
    public boolean callCashApplyUserExit(String policyId) {
        policyId = bilPolicyTermRepository.getPolicyIdByStatusCodeAndStatePvnCode(policyId, Arrays.asList('C', 'F'),
                "NY");
        if (policyId != null) {
            return true;
        }
        return false;
    }
}
