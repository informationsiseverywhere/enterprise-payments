package billing.service.impl;

import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import application.security.data.entity.FieldAttribute;
import application.security.data.repository.FieldAttributeRepository;
import application.security.data.repository.FieldDataMapRepository;
import billing.data.entity.BilBank;
import billing.data.repository.BilBankRepository;
import billing.data.repository.BilBankRltRepository;
import billing.service.SupportDataService;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.options.model.SupportData;
import core.translation.repository.BusCdTranslationRepository;

@Service("supportDataService")
public class SupportDataServiceImpl implements SupportDataService {

    @Value("${appId.value}")
    private String appId;

    @Autowired
    public BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private FieldAttributeRepository fieldAttributeRepository;

    @Autowired
    private FieldDataMapRepository fieldDataMapRepository;

    @Autowired
    public ExecContext execContext;

    @Autowired
    private BilBankRepository bilBankRepository;

    @Autowired
    private BilBankRltRepository bilBankRltRepository;

    private static final String DEFAULT = "DEFAULT";

    @Override
    public SupportData getSupportData(String parentCode) {

        BilBank bilBank = bilBankRepository.findByBbkAccountNm(parentCode);
        if (bilBank == null) {
            throw new DataNotFoundException("Invalid bank Code");
        }
        List<String> bankCodeList = bilBankRltRepository.getBankCode(getDeposiBankType(),
                bilBank.getBilBankCd().trim());
        List<String> bankCdList = getBankCodeList(bankCodeList);
        SupportData supportData = new SupportData();
        supportData.setPropertyName("ControlBank");
        supportData.setPropertyValues(bankCdList);
        getRestrictedFields(supportData, "FieldAscType");
        return supportData;
    }

    private List<String> getBankCodeList(List<String> bankCodeList) {
        List<String> descriptionList = new ArrayList<>();
        for (String bankCode : bankCodeList) {
            BilBank bilBank = bilBankRepository.findById(bankCode).orElse(null);
            if (bilBank != null) {
                descriptionList.add(bilBank.getBbkAccountNm().trim());
            }
        }
        return descriptionList;
    }

    private List<Character> getDeposiBankType() {
        List<Character> controlBankList = new ArrayList<>();
        controlBankList.add(CHAR_B);
        controlBankList.add(CHAR_D);
        return controlBankList;

    }

    public SupportData getRestrictedFields(SupportData supportData, String codeType) {
        List<String> propertyValuesList = supportData.getPropertyValues();
        List<String> includeValues;
        int roleId = Integer.parseInt(execContext.getUserRole());
        List<Integer> fieldIdList = getFieldId(codeType);
        if (fieldIdList.isEmpty()) {
            return supportData;
        }
        List<Integer> fieldIdsList = getFieldIdListFieldAttribute(fieldIdList, roleId);
        if (fieldIdsList == null || fieldIdsList.isEmpty()) {
            return supportData;
        }
        List<FieldAttribute> fieldAttributesList = null;
        Integer fieldId = fieldIdsList.get(0);
        fieldAttributesList = fieldAttributeRepository.findByFieldAttributeIdFieldIdAndRoleId(fieldId, roleId);
        if (fieldAttributesList == null || fieldAttributesList.isEmpty()) {
            fieldAttributesList = fieldAttributeRepository.findByFieldAttributeIdFieldIdAndRoleIdIsNull(fieldId);
        }
        if (fieldAttributesList == null || fieldAttributesList.isEmpty()) {
            return supportData;
        }
        String defaultValue;
        List<String> includeList;
        List<String> defaultList = fieldAttributesList.stream()
                .filter(list -> list.getAttrType().trim().equalsIgnoreCase(DEFAULT))
                .map(fieldAttribute -> fieldAttribute.getAttrValue().trim()).collect(Collectors.toList());

        if (defaultList != null && !defaultList.isEmpty()) {
            defaultValue = defaultList.get(0);
        } else {
            List<FieldAttribute> fieldAttributeList = fieldAttributeRepository
                    .findAllByFieldAttributeIdFieldIdAndAttrTypeAndRoleIdIsNull(fieldId, DEFAULT);
            defaultList = fieldAttributeList.stream()
                    .filter(list -> list.getAttrType().trim().equalsIgnoreCase(DEFAULT))
                    .map(list -> list.getAttrValue().trim()).collect(Collectors.toList());

            defaultValue = defaultList != null && !defaultList.isEmpty() ? defaultList.get(0) : null;

        }
        includeList = fieldAttributesList.stream().filter(list -> list.getAttrType().trim().equalsIgnoreCase("INCLUDE"))
                .map(fieldAttribute -> fieldAttribute.getAttrValue().trim()).collect(Collectors.toList());
        if (includeList == null || includeList.isEmpty()) {
            List<String> excludeList = fieldAttributesList.stream()
                    .filter(list -> list.getAttrType().trim().equalsIgnoreCase("EXCLUDE"))
                    .map(fieldAttribute -> fieldAttribute.getAttrValue().trim()).collect(Collectors.toList());

            includeList = getPropertyValues(codeType).stream()
                    .filter(list -> (excludeList.stream().noneMatch(exclude -> exclude.equalsIgnoreCase(list.trim()))))
                    .map(String::trim).collect(Collectors.toList());
        }
        if (defaultValue != null && !includeList.contains(defaultValue)) {
            includeList.add(defaultValue);
            includeList = includeList.stream().sorted(Comparator.comparing(String::toLowerCase)).map(String::trim)
                    .collect(Collectors.toList());
        }

        if (propertyValuesList != null) {
            String defaultedValue = defaultValue;
            if (codeType.equals("FieldAscType")) {
                includeValues = new ArrayList<>();
                defaultedValue = getBankPropertyValuesList(includeValues, includeList, defaultValue);
            } else {
                includeValues = includeList;
            }
            supportData.setDefaultValue(propertyValuesList.contains(defaultedValue) ? defaultedValue : null);
            List<String> propertyValueList = propertyValuesList.stream().filter(list -> includeValues.contains(list))
                    .collect(Collectors.toList());
            supportData.setPropertyValues(propertyValueList);
            return supportData;
        }

     
        return supportData;
    }

    private String getBankPropertyValuesList(List<String> includeValues, List<String> includeList,
            String defaultValue) {
        if (defaultValue != null) {
            String[] value = defaultValue.split(SEPARATOR_BLANK, 2);
            defaultValue = value.length > 1 ? value[1] : null;
        }
        for (String IncludeValue : includeList) {
            String[] values = IncludeValue.split(SEPARATOR_BLANK, 2);
            if (values.length > 1) {
                includeValues.add(values[1]);
            }
        }
        if (defaultValue != null && !includeValues.contains(defaultValue)) {
            includeValues.add(defaultValue);
        }
        return defaultValue;
    }

    private List<Integer> getFieldId(String codeType) {
        return fieldDataMapRepository.findByCodeType(codeType, Long.parseLong(appId));
    }

    public List<Integer> getFieldIdListFieldAttribute(List<Integer> fieldIdList, Integer roleId) {
        List<Integer> fieldAttributesList = fieldAttributeRepository.findByFieldIdAndRoleId(fieldIdList, roleId);
        if (fieldAttributesList.isEmpty()) {
            fieldAttributesList = fieldAttributeRepository.findByFieldIdAndRoleIdIsNull(fieldIdList);
        }
        return fieldAttributesList;

    }

    public List<String> getPropertyValues(String busType) {
        List<String> propertyValuesList = new ArrayList<>();
        propertyValuesList
                .addAll(busCdTranslationRepository.findDescriptionListByType(busType, execContext.getLanguage()));
        if (!propertyValuesList.isEmpty()) {
            propertyValuesList = propertyValuesList.stream().sorted(Comparator.comparing(e -> e.toLowerCase()))
                    .map(String::trim).collect(Collectors.toList());
        }
        return propertyValuesList;
    }
}
