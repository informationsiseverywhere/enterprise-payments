package billing.service;

import billing.data.entity.BilUnIdCash;

public interface UnidentifiedCashApplyService {

    public void applyUnidentifiedCash(String accountNumber, String policyNumber, String policySymbolCode,
            String folderId, BilUnIdCash bilUnidentifiedCash, String identifierType, Boolean isBusGrpOverride,
            Boolean isUpdate, String previousIdentifier, String currencyCode);
}
