package billing.service;

import java.util.List;

import billing.model.Payment;
import core.model.Paging;

public interface BatchPaymentService {

    public List<Payment> findAllBatchPayments(String postedDate, String batchId, String query, String filters,
            String sort, int size, int page, Paging paging, String userId);

    public Payment findBatchPaymentDetails(String batchId, String postedDate, short paymentSeqNumber, String userId);

    public Short saveBatchPayment(String batchId, String postedDate, Payment batchPayment);

    public void deleteBatchPayment(String batchId, String postedDate, short paymentSeqNumber, String userId);

    public void patchBatchPayment(String batchId, String postedDate, short paymentSeqNumber, Payment batchPayment, String userId);
}
