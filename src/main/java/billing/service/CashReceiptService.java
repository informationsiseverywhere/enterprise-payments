package billing.service;

public interface CashReceiptService {
	
    public void processBatchSystem(String batchId, String postedDate, String userId);

}
