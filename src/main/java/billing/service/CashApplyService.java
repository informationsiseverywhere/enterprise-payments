package billing.service;

import billing.model.CashApply;

public interface CashApplyService {

    public CashApply cashApply(CashApply cashApply, boolean firstTime);

}
