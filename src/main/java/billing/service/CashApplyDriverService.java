package billing.service;

import java.time.ZonedDateTime;
import java.util.List;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActRules;
import billing.model.CashApplyDriver;

public interface CashApplyDriverService {

    public void processCashApply(CashApplyDriver cashApplyDriver);

    public boolean checkFullPayServiceWriteOff(BilActRules bilActRules, Double totalCashAmount, String accountId,
            BilAccount bilAccount, List<Double> lastQualServiceChargeAmounts, List<Double> downpaymentAmounts,
            List<ZonedDateTime> fullpayDatesArrays);

}
