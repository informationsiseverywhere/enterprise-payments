package billing.service;

public interface BusinessGroupService {

	public String getBusinessGroup(String billFunctionType, String bilTypeCode);
}
