package billing.service;

import billing.model.AddUpdateBankInformation;

public interface AddUpdateBankInformationService {
    
   public void addBankInformation(AddUpdateBankInformation addUpdateBankInformation);

}
