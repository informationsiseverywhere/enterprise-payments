package billing.service;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public interface BatchDetailReportService {

    public XSSFWorkbook buildExcelDocument(String batchId, String postedDate, String metadataId, String userId);
}
