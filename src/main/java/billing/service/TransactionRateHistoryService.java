package billing.service;

import billing.model.TransactionRateHistory;

public interface TransactionRateHistoryService {

    public TransactionRateHistory findTransactionRateHistory(String batchId, String postedDate, short paymentSeqNumber);

    public void deleteTransactionRateHistory(String batchId, String postedDate, short paymentSeqNumber);

    public void postTransactionRateHistory(String batchId, String postedDate, short paymentSeqNumber,
            TransactionRateHistory transactionRateHistory);
}
