package billing.service;

import java.util.List;
import java.util.Map;

import billing.model.SuspendedPayment;
import billing.model.SuspendedPaymentList;

public interface SuspendedPaymentService {

    public SuspendedPaymentList findSuspendedPayments(int evalPage, int evalPageSize, String filters);

    public SuspendedPayment findSuspendedPayment(String batchId, short sequenceNumber, String postedDate, String userId,
            String distributionDate);

    public Map<String, Boolean> areOperationsAllowed(short sequenceNumber, String postedDate, String userId,
            String batchId, String distributionDate);

    public List<String> getPaymentTypeList(short sequenceNumber, String postedDate, String userId, String batchId,
            String distributionDate);
}
