package billing.service;

import java.util.List;

import billing.model.BatchRejection;
import core.model.Paging;

public interface BatchRejectionService {

    public List<BatchRejection> findAllBatches(String query, String filters, String sort, int size, int page, Paging paging);

    public BatchRejection findBatchDetails(String batchId, String postedDate);

    public String save(BatchRejection batch);

    public String deleteBatch(String batchId, String postedDate);

    public void patchBatch(String batchId, String postedDate, BatchRejection batch);

    public void scheduleBatch(String batchId, String postedDate);

}
