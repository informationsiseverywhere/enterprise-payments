package billing.service;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import billing.data.entity.BilAgent;
import billing.data.entity.BilInvTtyAct;

public interface ReplacementPaymentService {

    void reapplyPaymentForThirdParty(String accountId, ZonedDateTime dueDate, BigDecimal amount, String reason, String accountNumber,
            BilInvTtyAct invoiceActivityRow, ZonedDateTime entryDate, String entryNumber, short entrySequenceNumber,
            ZonedDateTime distributionDate, short distributionSequenceNumber, String userId, char cashEntryMethodCode,
            String bankCode, Short dispositionSequenceNumber, String accountType);
    
    void reapplyPaymentForAgent(String accountId, ZonedDateTime dueDate, BigDecimal amount, String reason, String accountNumber,
            BilInvTtyAct invoiceActivityRow, ZonedDateTime entryDate, String entryNumber, short entrySequenceNumber,
            ZonedDateTime distributionDate, short distributionSequenceNumber, String userId, char cashEntryMethodCode,
            String bankCode, Short dispositionSequenceNumber, String accountType, BilAgent bilAgent);
}
