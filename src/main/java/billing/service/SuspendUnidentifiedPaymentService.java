package billing.service;

import java.time.ZonedDateTime;

public interface SuspendUnidentifiedPaymentService {

    void save(Short sequenceNumber, ZonedDateTime postedDate, String userId, String batchId,
            ZonedDateTime distributionDate);
}
