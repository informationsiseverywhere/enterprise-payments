package billing.service;

import java.util.List;

import billing.model.Batch;
import core.model.Paging;

public interface BatchService {

    public List<Batch> findAllBatches(String query, String filters, String sort, int size, int page, Paging paging);

    public Batch findBatchDetails(String batchId, String postedDate, String userId);

    public String save(Batch batch);

    public String deleteBatch(String batchId, String postedDate, String userId);

    public boolean patchBatch(String batchId, String postedDate, Batch batch, String userId);

    public void scheduleBatch(String batchId, String postedDate, String userId);

}
