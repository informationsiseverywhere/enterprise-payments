package billing.service;

import java.time.ZonedDateTime;

import billing.model.PayableOption;

public interface DisbursementSuspendPaymentService {

    void save(Short sequenceNumber, ZonedDateTime postedDate, String user, String batchId,
            ZonedDateTime distributionDate, PayableOption payableOption);
}
