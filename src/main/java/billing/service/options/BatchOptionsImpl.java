package billing.service.options;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import application.utils.service.FieldLevelAuthenticationService;
import application.utils.service.MultiCurrencyService;
import core.exception.application.AppAuthenticationException;
import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.Parameters;
import core.options.model.SupportData;
import core.security.service.SecurityService;
import core.utils.CommonConstants.PropertyValuesType;
import core.utils.OptionUtilities;

@Component
public class BatchOptionsImpl extends AbstractOptionsImpl implements OptionsService {

    @Value("${enterprise.payments.url}")
    private String billingServiceUrl;

    @Value("${multi.currency}")
    String multiCurrency;

    @Autowired
    private OptionUtilities optionUtilities;

    @Autowired
    private FieldLevelAuthenticationService fieldLevelAuthenticationService;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    @Autowired
    private SecurityService securityService;

    private static final String OPTIONS_TITLE = "Options for basic operations on Batches Resource";
    private static final String BATCH_ADMIN = "batchAdmin";
    private static final String BATCH_REVIEW_ADMIN = "batchReviewAdmin";
    private static final String BATCH_VIEW_ADMIN = "batchViewAdmin";
    private static final String STRING = "String";
    private static final String REQUEST_PARAM = "requestParam";

    String href = "";
    String batchId;
    String postedDate;
    String userId;
    Integer page = 1;
    Integer size = 10;

    public enum BatchProperties {
        DepositBank("DEPOSIT-BANK"), ControlBank("CONTROL-BANK"), AllBatches("ALL-BATCHES");

        private final String prop;

        private BatchProperties(String prop) {
            this.prop = prop;
        }

        public String getValue() {
            return prop;
        }

        public static List<String> getAllProperties() {
            List<String> propertyList = new ArrayList<>();
            for (BatchProperties e : BatchProperties.values()) {
                propertyList.add(e.name());
            }
            return propertyList;
        }
    }

    public OptionsResponseEntity getCollectionsResponse(Class<? extends ResourceSupport> className, String query,
            String filters, String sort, Integer page, Integer size) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        this.page = page != null ? page : this.page;
        this.size = size != null ? size : this.size;
        href = billingServiceUrl + "/billing/batches?page=" + this.page + "&size=" + this.size;
        if (query != null && !query.isEmpty()) {
            href = href + "&query=" + query;
        }
        if (filters != null) {
            href = href + "&filters=" + filters;
        }
        if (sort != null) {
            href = href + "&sort=" + sort;
        }
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        List<SupportData> supportDataList = getSupportDataList(BatchProperties.getAllProperties());
        multiCurrencySupport(supportDataList);
        optionsResponseEntity.setSupportData(supportDataList);
        return optionsResponseEntity;
    }

    public OptionsResponseEntity getResourceResponse(Class<? extends ResourceSupport> className, String batchId,
            String postedDate, String userId) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        this.batchId = batchId;
        this.postedDate = postedDate;
        this.userId = userId;
        href = billingServiceUrl + "/billing/batches/" + this.batchId + "?postedDate=" + this.postedDate + "&userId="
                + userId;
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getResourceEntity(className, optionsResponseEntity);
        List<SupportData> supportDataList = getSupportDataList(BatchProperties.getAllProperties());
        multiCurrencySupport(supportDataList);
        optionsResponseEntity.setSupportData(supportDataList);
        return optionsResponseEntity;
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        SupportData supportData = new SupportData();
        switch (BatchProperties.valueOf(supportDataProperty)) {
        case DepositBank:
            supportData.setPropertyName(supportDataProperty);
            supportData
                    .setPropertyValues(getPropertyValuesList(BatchProperties.valueOf(supportDataProperty).getValue()));
            getRestrictedFields(supportData, "FieldAscType", PropertyValuesType.BUS_TYPE.getValue());
            break;
        case ControlBank:
            supportData.setPropertyName(supportDataProperty);
            supportData
                    .setPropertyValues(getPropertyValuesList(BatchProperties.valueOf(supportDataProperty).getValue()));
            break;
        case AllBatches:
            supportData.setPropertyName("allBatches");
            supportData.setPropertyValues(getIsAllBatches());
            break;
        default:
            break;
        }
        return supportData;
    }

    private List<String> getIsAllBatches() {
        List<String> allBatches = new ArrayList<>();
        boolean batchAdmin = true;
        boolean reviewAdmin = true;
        boolean viewAdmin = true;
        try {
            securityService.authenticateFunction(BATCH_ADMIN);
        } catch (AppAuthenticationException e) {
            batchAdmin = false;
            try {
                securityService.authenticateFunction(BATCH_REVIEW_ADMIN);
            } catch (AppAuthenticationException ex) {
                reviewAdmin = false;
                try {
                    securityService.authenticateFunction(BATCH_VIEW_ADMIN);
                } catch (AppAuthenticationException exe) {
                    viewAdmin = false;
                }
            }
        }
        if (batchAdmin || reviewAdmin || viewAdmin) {
            allBatches.add("true");
            return allBatches;
        }
        allBatches.add("false");
        return allBatches;
    }

    private void multiCurrencySupport(List<SupportData> supportDataList) {
        if (Boolean.TRUE.equals(Boolean.valueOf(multiCurrency))) {
            SupportData multiCurrencySupport = new SupportData();
            multiCurrencySupport.setPropertyName("multiCurrency");
            multiCurrencySupport.setPropertyValues(Arrays.asList(multiCurrency));
            supportDataList.add(multiCurrencySupport);

            SupportData currencySupportData = new SupportData();
            currencySupportData.setPropertyName("currency");
            multiCurrencyService.setSupportedCurrency(currencySupportData);
            supportDataList.add(currencySupportData);
        }
    }

    public List<String> getPropertyValuesList(String busType) {
        List<String> propertyValuesList = new ArrayList<>();
        propertyValuesList.addAll(fieldLevelAuthenticationService.findAllAuthFieldLevels(busType));
        return propertyValuesList;
    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.addAll(optionUtilities.generateQueryParameters());
        parameters.addAll(optionUtilities.generateFiltersParameters());
        parameters.addAll(optionUtilities.generateSortParameters());
        parameters.addAll(optionUtilities.generatePageParameters());
        optionLink.setParameters(parameters);
    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(billingServiceUrl + "/billing/batches");
        optionLink.setMethod(RequestMethod.POST.name());
        optionLink.setTitle("Create Batch");
        optionLink.setRel("create");
        generateSchema(optionLink, className);
        optionLink.setExample(getExample("/samples/BatchPost.txt"));
        return optionLink;
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Search Batches");
        optionLink.setRel("search");
        generateSchema(optionLink, className);
        generateParameters(optionLink);
        return optionLink;
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Get Batch");
        optionLink.setRel("fetch");
        generateSchema(optionLink, className);
        generateResourceParameters(optionLink);
        return optionLink;
    }

    public void generateResourceParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.add(new Parameters(REQUEST_PARAM, "batchId", STRING, true, "Batch Identifier"));
        parameters.add(new Parameters(REQUEST_PARAM, "postedDate", STRING, true, "Posted Date"));
        parameters.add(new Parameters(REQUEST_PARAM, "userId", STRING, true, "User Id"));
        optionLink.setParameters(parameters);
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.PATCH.name());
        optionLink.setTitle("Update Batch");
        optionLink.setRel("patch");
        generateSchema(optionLink, className);
        generateResourceParameters(optionLink);
        optionLink.setExample(getExample("/samples/BatchPatch.txt"));
        return optionLink;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.DELETE.name());
        optionLink.setTitle("Delete Batch");
        optionLink.setRel("delete");
        generateResourceParameters(optionLink);
        return optionLink;
    }

}
