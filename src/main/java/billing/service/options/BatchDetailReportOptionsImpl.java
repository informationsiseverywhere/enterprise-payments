package billing.service.options;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import billing.service.options.BatchPaymentOptionsImpl.BatchPaymentProperties;
import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.SupportData;

@Component
public class BatchDetailReportOptionsImpl extends AbstractOptionsImpl implements OptionsService {

    @Value("${enterprise.payments.url}")
    private String billingServiceUrl;

    private static final String OPTIONS_TITLE = "Options for basic operations on Batch Detail Report";

    String batchId;
    String postedDate;
    String userId;

    @Override
    public List<String> getPropertyValues(String type, String supportDataProperty) {
        return Collections.emptyList();
    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        // Auto-generated method stub
    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        String href = billingServiceUrl + "/billing/batches/" + this.batchId + "/_report?postedDate=" + this.postedDate
                + "&userId=" + this.userId;
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Get the items of the Batch Detail Report");
        optionLink.setRel("search");
        generateSchema(optionLink, className);
        generateParameters(optionLink);
        return optionLink;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        return null;
    }

    public OptionsResponseEntity getCollectionsResponse(Class<? extends ResourceSupport> className, String batchId,
            String postedDate, String userId) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        this.batchId = batchId;
        this.postedDate = postedDate;
        this.userId = userId;
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        optionsResponseEntity.setSupportData(getSupportDataList(BatchPaymentProperties.getAllProperties()));
        return optionsResponseEntity;
    }

}
