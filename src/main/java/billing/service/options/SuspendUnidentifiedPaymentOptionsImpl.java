package billing.service.options;

import static core.utils.CommonConstants.BLANK_STRING;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import billing.model.Payment;
import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.SupportData;

@Component
public class SuspendUnidentifiedPaymentOptionsImpl extends AbstractOptionsImpl implements OptionsService {

    private static final String OPTIONS_TITLE = "Options for basic operations on Suspend Resource";

    private String href = BLANK_STRING;

    @Value("${enterprise.payments.url}")
    private String paymentsServiceUrl;

    @Override
    public void generateParameters(OptionLink optionLink) {
        // Auto-generated method stub
    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.POST.name());
        optionLink.setTitle("Suspend Activity Details");
        optionLink.setRel(RequestMethod.POST.name());
        generateSchema(optionLink, className);
        return optionLink;
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {

        return null;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        return null;
    }

    public OptionsResponseEntity getDocumentResponse(Class<Payment> className, Short sequenceNumber, String postedDate,
            String userId, String batchId, String distributionDate) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        href = paymentsServiceUrl + "/billing/suspendedPayments/" + sequenceNumber + "/_suspend?postedDate="
                + postedDate + "&userId=" + userId + "&batchId=" + batchId + "&distributionDate=" + distributionDate;

        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        return optionsResponseEntity;
    }
}
