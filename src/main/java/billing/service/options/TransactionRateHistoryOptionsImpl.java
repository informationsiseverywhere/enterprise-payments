package billing.service.options;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.Parameters;
import core.options.model.SupportData;

@Component
public class TransactionRateHistoryOptionsImpl extends AbstractOptionsImpl implements OptionsService {

    @Value("${enterprise.payments.url}")
    private String billingServiceUrl;

    private static final String OPTIONS_TITLE = "Options for basic operations on Transaction Rate History Resource";
    private static final String REQUEST_PARAM = "requestParam";

    String href = "";
    String batchId;
    short paymentSequenceNumber = 0;
    String postedDate;

    public OptionsResponseEntity getCollectionsResponse(Class<? extends ResourceSupport> className, String batchId,
            short paymentSequenceNumber, String postedDate) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        this.batchId = batchId;
        this.postedDate = postedDate;
        this.paymentSequenceNumber = paymentSequenceNumber;

        href = billingServiceUrl + "/billing/batches/" + this.batchId + "/payments/" + this.paymentSequenceNumber
                + "?postedDate=" + this.postedDate;
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        OptionLink fetchOptionLink = addFetchOptionLink(className);
        if (fetchOptionLink != null) {
            optionsResponseEntity.getLinks().add(fetchOptionLink);
        }
        OptionLink deleteOptionLink = addDeleteOptionLink(className);
        if (deleteOptionLink != null) {
            optionsResponseEntity.getLinks().add(deleteOptionLink);
        }
        return optionsResponseEntity;
    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        // Auto-generated method stub
    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.POST.name());
        optionLink.setTitle("Create Transaction Rate History");
        optionLink.setRel("create");
        generateSchema(optionLink, className);
        generateResourceParameters(optionLink);
        return optionLink;
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.DELETE.name());
        optionLink.setTitle("Delete Batch Payment");
        optionLink.setRel("delete");
        generateResourceParameters(optionLink);
        return optionLink;
    }

    public void generateResourceParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.add(new Parameters(REQUEST_PARAM, "batchId", "String", true, "Batch Identifier"));
        parameters
                .add(new Parameters(REQUEST_PARAM, "paymentSequenceNumber", "short", true, "Payment Sequence Number"));
        parameters.add(new Parameters(REQUEST_PARAM, "postedDate", "String", true, "Posted Date"));
        optionLink.setParameters(parameters);
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        return null;
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Find Transaction Rate History");
        optionLink.setRel("find");
        generateSchema(optionLink, className);
        generateParameters(optionLink);
        return optionLink;
    }

}
