package billing.service.options;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import billing.data.entity.BilRulesUct;
import billing.data.repository.BilEftPlanRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.datetime.service.DateService;
import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.Parameters;
import core.options.model.SupportData;
import core.translation.data.entity.BusCdTranslation;
import core.utils.CommonConstants.PropertyValuesType;
import core.utils.OptionUtilities;

@Component
public class PaymentSubscriptionOptionsImpl extends AbstractOptionsImpl implements OptionsService {

    @Value("${enterprise.payments.url}")
    private String paymentsServiceUrl;

    @Autowired
    private OptionUtilities optionUtilities;

    @Autowired
    private BilEftPlanRepository bilEftPlanRepository;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private DateService dateService;

    private static final String OPTIONS_TITLE = "Options for basic operations on Subscription Resource";

    public enum PaymentProperties {
        BANK_PAYMENT_METHOD_PLAN("bankEftPlan"),
        EFT_ACCOUNT_TYPE("accountType"),
        CREDIT_CARD_TYPE("creditCardType"),
        CARD_PAYMENT_METHOD_PLAN("cardEftPlan");

        private final String prop;

        private PaymentProperties(String prop) {
            this.prop = prop;
        }

        public String getValue() {
            return prop;
        }

        public static List<String> getAllProperties() {
            List<String> propertyList = new ArrayList<>();
            for (PaymentProperties e : PaymentProperties.values()) {
                propertyList.add(e.name());
            }
            return propertyList;
        }
    }

    public OptionsResponseEntity getCollectionsResponse(Class<? extends ResourceSupport> className) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();

        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        optionsResponseEntity.setSupportData(getSupportDataList(PaymentProperties.getAllProperties()));

        return optionsResponseEntity;
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        SupportData supportData = new SupportData();
        switch (PaymentProperties.valueOf(supportDataProperty)) {
        case BANK_PAYMENT_METHOD_PLAN:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            supportData.setPropertyValues(getEftPlanValues(PaymentProperties.BANK_PAYMENT_METHOD_PLAN.getValue()));
            getRestrictedFields(supportData, "EFT_PLAN", PropertyValuesType.BUS_TYPE.getValue());
            break;
        case CARD_PAYMENT_METHOD_PLAN:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            supportData.setPropertyValues(getEftPlanValues(PaymentProperties.CARD_PAYMENT_METHOD_PLAN.getValue()));
            getRestrictedFields(supportData, "EFT_PLAN", PropertyValuesType.BUS_TYPE.getValue());
            break;
        case EFT_ACCOUNT_TYPE:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, BusCodeTranslationType.EFT_ACCOUNT_TYPE.getValue(),
                    PropertyValuesType.BUS_TYPE.getValue());
            break;
        case CREDIT_CARD_TYPE:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, BusCodeTranslationType.CREDIT_CARD_TYPE.getValue(),
                    PropertyValuesType.BUS_TYPE.getValue());
            break;

        default:
            break;

        }
        return supportData;
    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        String href = paymentsServiceUrl + "/billing/subscriptions";
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.POST.name());
        optionLink.setTitle("Create Subscription");
        optionLink.setRel("create");
        generateSchema(optionLink, className);
        optionLink.setExample(getExample("/samples/PaymentExample.txt"));
        return optionLink;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.addAll(optionUtilities.generateFiltersParameters());
        parameters.addAll(optionUtilities.generatePageParameters());
        optionLink.setParameters(parameters);
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    private List<String> getEftPlanValues(String paymentMethod) {

        List<String> propertyValuesList = new ArrayList<>();
        if (paymentMethod.equals(PaymentProperties.CARD_PAYMENT_METHOD_PLAN.getValue())) {

            short fileTypeCode = getFileTypeCodeFromBilRuleUct(dateService.currentDate());

            propertyValuesList.addAll(bilEftPlanRepository.findByFileTypeCode(fileTypeCode));
        }

        if (paymentMethod.equalsIgnoreCase(PaymentProperties.BANK_PAYMENT_METHOD_PLAN.getValue())) {

            short fileTypeCode = determineFileTypeCode(BilDesReasonType.ONE_XA.getValue());
            propertyValuesList.addAll(bilEftPlanRepository.findByFileTypeCode(fileTypeCode));
        }

        if (!propertyValuesList.isEmpty()) {
            propertyValuesList = propertyValuesList.stream().map(String::trim).collect(Collectors.toList());
        }
        return propertyValuesList;
    }

    private short getFileTypeCodeFromBilRuleUct(ZonedDateTime se3DateTime) {
        final String STRING_1XCC = "1XCC";
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(STRING_1XCC, SEPARATOR_BLANK, SEPARATOR_BLANK,
                SEPARATOR_BLANK, se3DateTime, se3DateTime);
        if (bilRulesUct != null) {
            return new Short(bilRulesUct.getBrtParmListTxt());
        }
        return 0;
    }

    private short determineFileTypeCode(String bilDesReasonCode) {

        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(bilDesReasonCode, "FTC",
                execContext.getLanguage());

        if (busCdTranslation != null) {
            return Short.parseShort(busCdTranslation.getBusDescription().trim());
        }
        return 0;

    }

}