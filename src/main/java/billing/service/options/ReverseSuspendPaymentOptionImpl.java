package billing.service.options;

import static core.utils.CommonConstants.BLANK_STRING;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import billing.model.ReverseOption;
import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.SupportData;
import core.utils.CommonConstants.PropertyValuesType;

@Component
public class ReverseSuspendPaymentOptionImpl extends AbstractOptionsImpl implements OptionsService {

    private static final String OPTIONS_TITLE = "Options for basic operations on reverse payment Resource";
    private String href = BLANK_STRING;

    @Value("${enterprise.payments.url}")
    private String paymentsServiceUrl;

    public enum ReverseProperties {
        ReversalReason("PRV");

        private final String property;

        private ReverseProperties(String property) {
            this.property = property;
        }

        public String getValue() {
            return property;
        }

        public static List<String> getAllProperties() {
            List<String> propertyList = new ArrayList<>();
            for (ReverseProperties property : ReverseProperties.values()) {
                propertyList.add(property.name());
            }
            return propertyList;
        }

    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        // Auto-generated method stub

    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.POST.name());
        optionLink.setTitle("Get Reverse Operation Details");
        optionLink.setRel(RequestMethod.POST.name());
        generateSchema(optionLink, className);
        return optionLink;
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        SupportData supportData = new SupportData();
        supportData.setPropertyName(ReverseProperties.valueOf(supportDataProperty).name());
        getRestrictedFields(supportData, ReverseProperties.valueOf(supportDataProperty).getValue(),
                PropertyValuesType.BUS_TYPE.getValue());
        return supportData;
    }

    public OptionsResponseEntity getDocumentResponse(Class<ReverseOption> className, Short sequenceNumber,
            String postedDate, String userId, String batchId, String distributionDate) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();

        href = paymentsServiceUrl + "/billing/suspendedPayments/" + sequenceNumber + "/_reverse?postedDate="
                + postedDate + "&userId=" + userId + "&batchId=" + batchId + "&distributionDate=" + distributionDate;

        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        optionsResponseEntity.setSupportData(getSupportDataList(ReverseProperties.getAllProperties()));
        return optionsResponseEntity;

    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

}
