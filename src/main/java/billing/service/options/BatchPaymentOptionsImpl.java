package billing.service.options;

import static core.utils.CommonConstants.BLANK_STRING;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import application.utils.service.FieldLevelAuthenticationService;
import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.Parameters;
import core.options.model.SupportData;
import core.utils.CommonConstants.PropertyValuesType;
import core.utils.OptionUtilities;

@Component
public class BatchPaymentOptionsImpl extends AbstractOptionsImpl implements OptionsService {

    @Value("${enterprise.payments.url}")
    private String billingServiceUrl;

    @Autowired
    private OptionUtilities optionUtilities;

    @Autowired
    private FieldLevelAuthenticationService fieldLevelAuthenticationService;

    private static final String OPTIONS_TITLE = "Options for basic operations on Batch Payment Resource";
    private static final String STRING = "String";
    private static final String BILLING_BATCHES = "/billing/batches/";
    private static final String REQUEST_PARAM = "requestParam";

    String href = "";
    String batchId;
    short paymentSequenceNumber = 0;
    String userId;
    String postedDate;
    Integer page = 1;
    Integer size = 10;

    public enum BatchPaymentProperties {
        SuspenseReason(
                "SUS"),
        PayableItem("PAYITEM"),
        PaymentType("PYC"),
        PolicySymbol("SYM"),
        IdentifierType("ACCOUNT_TYPE");

        private final String prop;

        private BatchPaymentProperties(String prop) {
            this.prop = prop;
        }

        public String getValue() {
            return prop;
        }

        public static List<String> getAllProperties() {
            List<String> propertyList = new ArrayList<>();
            for (BatchPaymentProperties e : BatchPaymentProperties.values()) {
                propertyList.add(e.name());
            }
            return propertyList;
        }
    }

    public OptionsResponseEntity getCollectionsResponse(Class<? extends ResourceSupport> className, String batchId,
            String postedDate, String query, String filters, String sort, Integer page, Integer size, String userId) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        this.page = page != null ? page : this.page;
        this.size = size != null ? size : this.size;
        this.batchId = batchId;
        this.userId = userId;
        href = billingServiceUrl + BILLING_BATCHES + this.batchId + "/payments?postedDate=" + postedDate + "&page="
                + this.page + "&size=" + this.size + "&userId=" + userId;
        if (query != null) {
            href = href + "&query=" + query;
        }
        if (filters != null) {
            href = href + "&filters=" + filters;
        }
        if (sort != null) {
            href = href + "&sort=" + sort;
        }
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        optionsResponseEntity.setSupportData(getSupportDataList(BatchPaymentProperties.getAllProperties()));
        return optionsResponseEntity;
    }

    public OptionsResponseEntity getResourceResponse(Class<? extends ResourceSupport> className, String batchId,
            short paymentSequenceNumber, String postedDate, String userId) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        this.batchId = batchId;
        this.postedDate = postedDate;
        this.paymentSequenceNumber = paymentSequenceNumber;
        this.userId = userId;

        href = billingServiceUrl + BILLING_BATCHES + this.batchId + "/payments/" + this.paymentSequenceNumber
                + "?postedDate=" + this.postedDate + "&userId=" + userId;
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getResourceEntity(className, optionsResponseEntity);
        optionsResponseEntity.setSupportData(getSupportDataList(BatchPaymentProperties.getAllProperties()));
        return optionsResponseEntity;
    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.addAll(optionUtilities.generateQueryParameters());
        parameters.addAll(optionUtilities.generateFiltersParameters());
        parameters.addAll(optionUtilities.generateSortParameters());
        parameters.addAll(optionUtilities.generatePageParameters());
        optionLink.setParameters(parameters);
    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(
                billingServiceUrl + BILLING_BATCHES + this.batchId + "/payments?postedDate=" + this.postedDate);
        optionLink.setMethod(RequestMethod.POST.name());
        optionLink.setTitle("Create Batch Payment");
        optionLink.setRel("create");
        generateSchema(optionLink, className);
        generateResourceParameters(optionLink);
        return optionLink;
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Search Batch Payments");
        optionLink.setRel("search");
        generateSchema(optionLink, className);
        generateParameters(optionLink);
        return optionLink;
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Get Batch Payment");
        optionLink.setRel("fetch");
        generateSchema(optionLink, className);
        generateResourceParameters(optionLink);
        return optionLink;
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.PATCH.name());
        optionLink.setTitle("Update Batch Payment");
        optionLink.setRel("patch");
        generateSchema(optionLink, className);
        generateResourceParameters(optionLink);
        optionLink.setExample(getExample("/samples/BatchPaymentPatch.txt"));
        return optionLink;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.DELETE.name());
        optionLink.setTitle("Delete Batch Payment");
        optionLink.setRel("delete");
        generateResourceParameters(optionLink);
        return optionLink;
    }

    public void generateResourceParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.add(new Parameters(REQUEST_PARAM, "batchId", STRING, true, "Batch Identifier"));
        parameters
                .add(new Parameters(REQUEST_PARAM, "paymentSequenceNumber", "short", true, "Payment Sequence Number"));
        parameters.add(new Parameters(REQUEST_PARAM, "postedDate", STRING, true, "Posted Date"));
        parameters.add(new Parameters(REQUEST_PARAM, "userId", STRING, true, "User Id"));
        optionLink.setParameters(parameters);
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        SupportData supportData = new SupportData();
        switch (BatchPaymentProperties.valueOf(supportDataProperty)) {
        case SuspenseReason:
        case PolicySymbol:
            supportData.setPropertyName(supportDataProperty);
            getRestrictedFields(supportData, BatchPaymentProperties.valueOf(supportDataProperty).getValue(),
                    BLANK_STRING, PropertyValuesType.BUSTYPE_AND_PARENTCODE.getValue());
            break;
        case PayableItem:
            supportData.setPropertyName(supportDataProperty);
            getRestrictedFields(supportData, null, BatchPaymentProperties.valueOf(supportDataProperty).getValue(),
                    PropertyValuesType.PARENT_CODE.getValue());
            break;
        case PaymentType:
            supportData.setPropertyName(supportDataProperty);
            setPaymentTypeValues(supportData);
            break;
        case IdentifierType:
            supportData.setPropertyName(supportDataProperty);
            getRestrictedFields(supportData, BatchPaymentProperties.valueOf(supportDataProperty).getValue(),
                    PropertyValuesType.BUS_TYPE.getValue());
            break;
        default:
            break;
        }
        return supportData;
    }

    public List<String> getBankValues(String busType) {
        List<String> propertyValuesList = new ArrayList<>();
        propertyValuesList.addAll(fieldLevelAuthenticationService.findAllAuthFieldLevels(busType));
        return propertyValuesList;
    }

    public void setPaymentTypeValues(SupportData supportData) {
        Set<String> propertyValuesList = new HashSet<>();
        List<String> busCodeTypeList = Arrays.asList("PYT", "PYC", "COL");
        for (String codeType : busCodeTypeList) {
            SupportData newSupportData = new SupportData();
            getRestrictedFields(newSupportData, codeType, PropertyValuesType.BUS_TYPE.getValue());
            propertyValuesList.addAll(newSupportData.getPropertyValues());
            if (newSupportData.getDefaultValue() != null) {
                supportData.setDefaultValue(newSupportData.getDefaultValue());
            }
        }
        supportData.setPropertyValues(propertyValuesList.stream().sorted().collect(Collectors.toList()));
    }
}
