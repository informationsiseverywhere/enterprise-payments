package billing.service.options;

import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilRulesUct;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilEftPlanRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants.BilDesReasonCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BillingMethod;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.datetime.service.DateService;
import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.Parameters;
import core.options.model.SupportData;
import core.translation.data.entity.BusCdTranslation;
import core.translation.data.entity.id.BusCdTranslationId;
import core.utils.CommonConstants.PropertyValuesType;
import core.utils.OptionUtilities;
import party.model.UserParty;
import party.service.UserPartySecurityService;

@Component
public class PaymentOptionsImpl extends AbstractOptionsImpl implements OptionsService {

    @Value("${enterprise.payments.url}")
    private String paymentsServiceUrl;

    @Value("${multi.currency}")
    String multiCurrency;

    @Autowired
    private OptionUtilities optionUtilities;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private UserPartySecurityService userPartySecurityService;

    @Autowired
    private BilEftPlanRepository bilEftPlanRepository;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private DateService dateService;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Value("${onetimepayment.integration}")
    String oneTimePaymentIntegration;

    @Value("${agencysweeppayment.integration}")
    String agencyPaymentIntegration;

    @Value("${autopayment.integration}")
    String autoPaymentIntegration;

    @Value("${commissionpayment.integration}")
    String commissionPaymentIntegration;

    @Value("${paymentIntegration.provider}")
    private String providerIntegration;

    private static final String OPTIONS_TITLE = "Options for basic operations on payments Resource";

    private static final String AGENT_ACCESS_RULE = "ACAG";
    private static final String AGENT_SWEEP_RULE = "AGSW";

    String href = "";
    Integer page = 1;
    Integer size = 10;
    String accountId;

    boolean isAutoPaymentIntegration = false;

    private static final String RECURRING_TYPE = "RECUR";

    public enum PaymentProperties {
        PaymentMethod("paymentMethod"),
        PaymentType("paymentType"),
        EftAccountType("accountType"),
        CreditCardType("creditCardType"),
        ONE_XA("1XA"),
        IdentifierType("identifierType"),
        PayableItem("PayableItem"),
        UnIdentifierType("UnIdentifierType"),
        STATUS("Status"),
        CASH_WITH_APPLICATON("CashWithApplication"),
        PolicySymbol("SYM");

        private final String prop;

        private PaymentProperties(String prop) {
            this.prop = prop;
        }

        public String getValue() {
            return prop;
        }

        public static List<String> getAllProperties() {
            List<String> propertyList = new ArrayList<>();
            for (PaymentProperties e : PaymentProperties.values()) {
                propertyList.add(e.name());
            }
            return propertyList;
        }
    }

    public OptionsResponseEntity getCollectionsResponse(Class<? extends ResourceSupport> className, Integer page,
            Integer size, String filters) {
        this.page = page != null ? page : this.page;
        this.size = size != null ? size : this.size;
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        href = paymentsServiceUrl + "/billing/payments?page=" + this.page + "&size=" + this.size;
        if (filters != null) {
            href = href + "&filters=" + filters;
        }
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        List<SupportData> supportDataList = getSupportDataList(PaymentProperties.getAllProperties());
        multiCurrencySupport(supportDataList);
        checkIfAgencySweepAllowed(supportDataList);
        checkIfPaymentIntegrationAllowed(supportDataList);
        optionsResponseEntity.setSupportData(supportDataList);
        return optionsResponseEntity;
    }

    public OptionsResponseEntity getResourceResponse(Class<? extends ResourceSupport> className, short paymentSeqNbr,
            String postedDate, String userId, String batchId, String accountId) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        if (accountId == null || accountId.trim().isEmpty()) {
            href = paymentsServiceUrl + "/billing/payments/" + paymentSeqNbr + "?postedDate=" + postedDate + "&userId="
                    + userId + "&batchId=" + batchId;

            getResourceEntity(className, optionsResponseEntity);
            List<SupportData> supportDataList = getSupportDataList(PaymentProperties.getAllProperties());
            multiCurrencySupport(supportDataList);
            optionsResponseEntity.setSupportData(supportDataList);
        } else {
            List<SupportData> supportDataList = new ArrayList<>();
            getEftPlans(accountId, supportDataList);
            optionsResponseEntity.setSupportData(supportDataList);
        }
        return optionsResponseEntity;
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        SupportData supportData = new SupportData();
        switch (PaymentProperties.valueOf(supportDataProperty)) {
        case PaymentMethod:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, BusCodeTranslationType.PAYMENTMETHOD.getValue(),
                    PropertyValuesType.BUS_TYPE.getValue());
            break;
        case PaymentType:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, BilDesReasonType.CPY.getValue(), PropertyValuesType.BUS_TYPE.getValue());
            break;
        case ONE_XA:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, BilDesReasonType.ONE_XA.getValue(),
                    PropertyValuesType.BUS_TYPE.getValue());

            break;
        case EftAccountType:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, BusCodeTranslationType.EFT_ACCOUNT_TYPE.getValue(),
                    PropertyValuesType.BUS_TYPE.getValue());
            break;
        case CreditCardType:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, BusCodeTranslationType.CREDIT_CARD_TYPE.getValue(),
                    PropertyValuesType.BUS_TYPE.getValue());
            break;
        case IdentifierType:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, BusCodeTranslationType.BILLACCOUNTTYPE.getValue(),
                    PropertyValuesType.BUS_TYPE.getValue());
            break;
        case PayableItem:
            supportData.setPropertyName(supportDataProperty);
            getRestrictedFields(supportData, null, "PAYITEM", PropertyValuesType.PARENT_CODE.getValue());
            break;
        case UnIdentifierType:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            Set<String> dataProperty = new HashSet<>();
            dataProperty.addAll(getDataProperty(BusCodeTranslationType.BILLACCOUNTTYPE.getValue(), supportData));
            dataProperty.addAll(getDataProperty(BusCodeTranslationType.ADDITIONALID.getValue(), supportData));
            supportData.setPropertyValues(dataProperty.stream().collect(Collectors.toList()));
            break;
        case STATUS:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            List<String> statusList = new ArrayList<>();
            statusList.add(BilDspTypeCode.SUSPENDED.getValue());
            statusList.add(BilDspTypeCode.DISBURSED.getValue());
            statusList.add(BilDspTypeCode.WRITE_OFF.getValue());
            statusList.add(BilDspTypeCode.REVERSED.getValue());
            supportData.setPropertyValues(getStatusList(statusList, BilDesReasonType.DSP.getValue()));
            break;
        case CASH_WITH_APPLICATON:
            supportData.setPropertyName(PaymentProperties.valueOf(supportDataProperty).getValue());
            List<String> valuesCAP = new ArrayList<>();
            valuesCAP.add("Yes");
            valuesCAP.add("No");
            supportData.setPropertyValues(valuesCAP);
            break;
        case PolicySymbol:
            supportData.setPropertyName(supportDataProperty);
            getRestrictedFields(supportData, PaymentProperties.valueOf(supportDataProperty).getValue(), BLANK_STRING,
                    PropertyValuesType.BUSTYPE_AND_PARENTCODE.getValue());
        }
        return supportData;
    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(paymentsServiceUrl + "/billing/payments");
        optionLink.setMethod(RequestMethod.POST.name());
        optionLink.setTitle("Create Payment");
        optionLink.setRel("create");
        generateSchema(optionLink, className);
        optionLink.setExample(getExample("/samples/PaymentExample.txt"));
        return optionLink;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Search Payment");
        optionLink.setRel("search");
        generateSchema(optionLink, className);
        generateParameters(optionLink);
        return optionLink;
    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.addAll(optionUtilities.generateFiltersParameters());
        parameters.addAll(optionUtilities.generatePageParameters());
        optionLink.setParameters(parameters);
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Get Payment");
        optionLink.setRel("fetch");
        generateSchema(optionLink, className);
        generateParameters(optionLink);
        return optionLink;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    private void multiCurrencySupport(List<SupportData> supportDataList) {
        if (Boolean.TRUE.equals(Boolean.valueOf(multiCurrency))) {
            SupportData multiCurrencySupport = new SupportData();
            multiCurrencySupport.setPropertyName("multiCurrency");
            multiCurrencySupport.setPropertyValues(Arrays.asList(multiCurrency));
            supportDataList.add(multiCurrencySupport);

            SupportData currencySupportData = new SupportData();
            currencySupportData.setPropertyName("currency");
            multiCurrencyService.setSupportedCurrency(currencySupportData);
            supportDataList.add(currencySupportData);
        }
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    private void checkIfAgencySweepAllowed(List<SupportData> supportDataList) {
        boolean isAgencySweep = false;
        boolean isAgentAccess = false;
        boolean isAgencySweepPayorChange = true;

        UserParty userParty = userPartySecurityService.getPartyByUserId();

        if (!userParty.getRole().equals("USR")) {
            BilRulesUct agswpBilRulesUct = bilRulesUctService.readBilRulesUct(AGENT_SWEEP_RULE, BLANK_STRING,
                    BLANK_STRING, BLANK_STRING);
            if (agswpBilRulesUct != null && agswpBilRulesUct.getBrtRuleCd() == 'Y') {
                isAgencySweep = true;
                BilRulesUct acagBilRulesUct = bilRulesUctService.readBilRulesUct(AGENT_ACCESS_RULE, BLANK_STRING,
                        BLANK_STRING, BLANK_STRING);

                isAgentAccess = acagBilRulesUct.getBrtRuleCd() == 'Y' ? Boolean.TRUE : Boolean.FALSE;

                if (userParty.getAgencies() != null && !userParty.getAgencies().isEmpty()) {
                    isAgencySweepPayorChange = userParty.getAgencies().size() > 1 ? Boolean.TRUE : Boolean.FALSE;
                    SupportData agencyChangeSupportData = new SupportData();
                    agencyChangeSupportData.setPropertyName("agencies");
                    agencyChangeSupportData.setPropertyValues(userParty.getAgencies());
                    supportDataList.add(agencyChangeSupportData);
                } else if ((userParty.getRole().equals("AGT") || userParty.getRole().equals("AGYN"))
                        && (userParty.getAgencies() == null || userParty.getAgencies().isEmpty())) {
                    isAgencySweep = false;
                    isAgentAccess = false;
                }
            }

        }

        SupportData agencySweepSupportData = new SupportData();
        agencySweepSupportData.setPropertyName("agencySweep");
        agencySweepSupportData.setPropertyValues(Arrays.asList(String.valueOf(isAgencySweep)));
        supportDataList.add(agencySweepSupportData);

        SupportData agentAccessSupportData = new SupportData();
        agentAccessSupportData.setPropertyName("userAgent");
        agentAccessSupportData.setPropertyValues(Arrays.asList(String.valueOf(isAgentAccess)));
        supportDataList.add(agentAccessSupportData);

        SupportData agencyChangeSupportData = new SupportData();
        agencyChangeSupportData.setPropertyName("agencySweepPayorChange");
        agencyChangeSupportData.setPropertyValues(Arrays.asList(String.valueOf(isAgencySweepPayorChange)));
        supportDataList.add(agencyChangeSupportData);

    }

    private void checkIfPaymentIntegrationAllowed(List<SupportData> supportDataList) {

        boolean isOneTimePaymentIntegration = false;
        boolean isAgencyPaymentIntegration = false;
        isAutoPaymentIntegration = false;
        boolean isCommissionPaymentIntegration = false;

        if (StringUtils.isNotBlank(providerIntegration)) {
            if (StringUtils.isNotBlank(oneTimePaymentIntegration)) {
                isOneTimePaymentIntegration = Boolean.valueOf(oneTimePaymentIntegration);
            }

            if (StringUtils.isNotBlank(commissionPaymentIntegration)) {
                isCommissionPaymentIntegration = Boolean.valueOf(commissionPaymentIntegration);
            }

            if (StringUtils.isNotBlank(agencyPaymentIntegration)) {
                isAgencyPaymentIntegration = Boolean.valueOf(agencyPaymentIntegration);
            }

            if (StringUtils.isNotBlank(autoPaymentIntegration)) {
                isAutoPaymentIntegration = Boolean.valueOf(autoPaymentIntegration);
            }
        }

        SupportData oneTimePaymentIntegrationSupportData = new SupportData();
        oneTimePaymentIntegrationSupportData.setPropertyName("oneTimePaymentIntegration");
        oneTimePaymentIntegrationSupportData
                .setPropertyValues(Arrays.asList(String.valueOf(isOneTimePaymentIntegration)));
        supportDataList.add(oneTimePaymentIntegrationSupportData);

        SupportData commissionPaymentIntegrationSupportData = new SupportData();
        commissionPaymentIntegrationSupportData.setPropertyName("commissionPaymentIntegration");
        commissionPaymentIntegrationSupportData
                .setPropertyValues(Arrays.asList(String.valueOf(isCommissionPaymentIntegration)));
        supportDataList.add(commissionPaymentIntegrationSupportData);

        SupportData agencyPaymentIntegrationSupportData = new SupportData();
        agencyPaymentIntegrationSupportData.setPropertyName("agencySweepPaymentIntegration");
        agencyPaymentIntegrationSupportData
                .setPropertyValues(Arrays.asList(String.valueOf(isAgencyPaymentIntegration)));
        supportDataList.add(agencyPaymentIntegrationSupportData);

        SupportData autoPaymentIntegrationSupportData = new SupportData();
        autoPaymentIntegrationSupportData.setPropertyName("autoPaymentIntegration");
        autoPaymentIntegrationSupportData.setPropertyValues(Arrays.asList(String.valueOf(isAutoPaymentIntegration)));
        supportDataList.add(autoPaymentIntegrationSupportData);

        SupportData paymentProviderSupportData = new SupportData();
        paymentProviderSupportData.setPropertyName("paymentProvider");
        paymentProviderSupportData.setPropertyValues(Arrays.asList(providerIntegration));
        supportDataList.add(paymentProviderSupportData);

    }

    private List<String> getDataProperty(String codeType, SupportData supportData) {
        SupportData newSupportData = new SupportData();
        getRestrictedFields(newSupportData, codeType, PropertyValuesType.BUS_TYPE.getValue());
        if (newSupportData.getDefaultValue() != null) {
            supportData.setDefaultValue(newSupportData.getDefaultValue());
        }
        return newSupportData.getPropertyValues();
    }

    private void getEftPlans(String accountId, List<SupportData> supportDataList) {

        BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);

        if (bilAccount != null) {
            supportDataList.add(getEftPlan("cardEftPlan", isAutoPaymentIntegration, bilAccount.getBillTypeCd()));
            supportDataList.add(getEftPlan("bankEftPlan", isAutoPaymentIntegration, bilAccount.getBillTypeCd()));
            if (isAutoPaymentIntegration) {
                List<SupportData> walletSupportDataPlanList = walletPlans();
                if (CollectionUtils.isNotEmpty(walletSupportDataPlanList)) {
                    supportDataList.addAll(walletSupportDataPlanList);
                }
            }
        }

    }

    private SupportData getEftPlan(String paymentMethod, boolean isPaymentIntegration, String accountType) {
        SupportData supportData = new SupportData();
        supportData.setPropertyName(paymentMethod);
        List<String> propertyValuesList = new ArrayList<>();

        if (paymentMethod.equals("cardEftPlan")) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository
                    .findById(new BusCdTranslationId(BillingMethod.ECC.toString(), RECURRING_TYPE,
                            execContext.getLanguage(), accountType))
                    .orElse(null);

            short fileTypeCode = busCdTranslation != null ? Short.valueOf(busCdTranslation.getBusDescription().trim())
                    : getFileTypeCodeFromBilRuleUct(dateService.currentDate());

            if (isPaymentIntegration) {
                propertyValuesList.addAll(bilEftPlanRepository
                        .findPlanNamesByFileTypeAndNoticeAndReportMethod(fileTypeCode, CHAR_N, String.valueOf(CHAR_N)));
            } else {
                propertyValuesList.addAll(bilEftPlanRepository.findPlanNamesByFileTypeCode(fileTypeCode));
            }
        }

        if (paymentMethod.equalsIgnoreCase("bankEftPlan")) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository
                    .findById(new BusCdTranslationId(BillingMethod.EFT.toString(), RECURRING_TYPE,
                            execContext.getLanguage(), accountType))
                    .orElse(null);
            short fileTypeCode = busCdTranslation != null ? Short.valueOf(busCdTranslation.getBusDescription().trim())
                    : determineFileTypeCode(BilDesReasonType.ONE_XA.getValue());
            if (isPaymentIntegration) {
                propertyValuesList.addAll(bilEftPlanRepository
                        .findPlanNamesByFileTypeAndNoticeAndReportMethod(fileTypeCode, CHAR_N, String.valueOf(CHAR_N)));
            } else {
                propertyValuesList.addAll(bilEftPlanRepository.findPlanNamesByFileTypeCode(fileTypeCode));
            }
        }

        if (!propertyValuesList.isEmpty()) {
            propertyValuesList = propertyValuesList.stream().map(String::trim).collect(Collectors.toList());
            supportData.setPropertyValues(propertyValuesList);
        }
        getRestrictedFields(supportData, "EFT_PLAN", PropertyValuesType.BUS_TYPE.getValue());
        return supportData;
    }

    private short determineFileTypeCode(String bilDesReasonCode) {

        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(bilDesReasonCode,
                BilDesReasonCode.FILE_TYPE.getValue(), execContext.getLanguage());

        if (busCdTranslation != null) {
            return Short.parseShort(busCdTranslation.getBusDescription().trim());
        }
        return 0;

    }

    private List<SupportData> walletPlans() {
        List<SupportData> walletPlansList = new ArrayList<>();
        List<BusCdTranslation> busCdTranslationList = busCdTranslationRepository.findAllByCodeAndType(
                BillingMethod.ELECTRONIC_WALLET.toString(), SEPARATOR_BLANK, execContext.getLanguage());
        if (!busCdTranslationList.isEmpty()) {
            busCdTranslationList.forEach(busTranslation -> {
                SupportData supportData = new SupportData();
                BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(
                        busTranslation.getBusCdTranslationId().getBusParentCd().trim(),
                        BusCodeTranslationType.WALLET_PAYMENT_TYPE.getValue(), execContext.getLanguage());
                if (busCdTranslation != null) {
                    supportData.setPropertyName(
                            busCdTranslation.getBusDescription().replace(SEPARATOR_BLANK, BLANK_STRING).trim()
                                    + "EftPlan");
                    List<String> propertyValuesList = new ArrayList<>();
                    propertyValuesList.addAll(bilEftPlanRepository.findPlanNamesByFileTypeAndNoticeAndReportMethod(
                            Short.valueOf(busTranslation.getBusDescription().trim()), CHAR_N, String.valueOf(CHAR_N)));
                    if (!propertyValuesList.isEmpty()) {
                        propertyValuesList = propertyValuesList.stream().map(String::trim).collect(Collectors.toList());
                        supportData.setPropertyValues(propertyValuesList);
                    }
                    walletPlansList.add(supportData);
                }

            });
        }
        return walletPlansList;
    }

    private short getFileTypeCodeFromBilRuleUct(ZonedDateTime se3DateTime) {
        final String STRING_1XCC = "1XCC";
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(STRING_1XCC, SEPARATOR_BLANK, SEPARATOR_BLANK,
                SEPARATOR_BLANK, se3DateTime, se3DateTime);
        if (bilRulesUct != null) {
            return new Short(bilRulesUct.getBrtParmListTxt());
        }
        return 0;
    }
}