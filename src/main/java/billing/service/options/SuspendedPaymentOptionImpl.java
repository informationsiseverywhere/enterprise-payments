package billing.service.options;

import static core.utils.CommonConstants.BLANK_STRING;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import application.security.service.impl.AppSecurityServiceImpl;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilCashEntryId;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.model.SuspendedPayment;
import billing.service.SuspendedPaymentService;
import billing.utils.BillingConstants.BusCodeTranslationParentCode;
import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.Parameters;
import core.options.model.SupportData;
import core.utils.CommonConstants.PropertyValuesType;
import core.utils.DateRoutine;
import core.utils.OptionUtilities;

@Component
public class SuspendedPaymentOptionImpl extends AbstractOptionsImpl implements OptionsService {

    @Value("${enterprise.payments.url}")
    private String paymentsServiceUrl;

    private String href = BLANK_STRING;

    private static final String OPTIONS_TITLE = "Options for basic operations on Suspended payment Resource";

    @Autowired
    private OptionUtilities optionUtilities;

    @Autowired
    private SuspendedPaymentService suspendedPaymentService;

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    Integer page = 1;
    Integer size = 10;

    public enum SuspendedPaymentProperties {
        PMS("Payment_Method"),
        SUS("Suspense_Reason"),
        PYC("Payment_Type"),
        SYM("Policy_Symbol"),
        BIT("PayableItem"),
        ACCOUNT_TYPE("Account_Type");

        private final String prop;

        private SuspendedPaymentProperties(String prop) {
            this.prop = prop;
        }

        public String getValue() {
            return prop;
        }

        public static List<String> getAllProperties() {
            List<String> propertyList = new ArrayList<>();
            for (SuspendedPaymentProperties e : SuspendedPaymentProperties.values()) {
                propertyList.add(e.name());
            }
            return propertyList;
        }
    }

    public OptionsResponseEntity getCollectionsResponse(Class<SuspendedPayment> className, Integer page, Integer size,
            String filters) {
        this.page = page != null ? page : this.page;
        this.size = size != null ? size : this.size;

        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        href = paymentsServiceUrl + "/billing/suspendedPayments?page=" + this.page + "&size=" + this.size;
        if (filters != null) {
            href = href + "&filters=" + filters;
        }
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        optionsResponseEntity.setSupportData(getSupportDataList(SuspendedPaymentProperties.getAllProperties()));

        return optionsResponseEntity;
    }

    public OptionsResponseEntity getResourceResponse(Class<SuspendedPayment> className, Short sequenceNumber,
            String postedDate, String userId, String batchId, String distributionDate) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        href = paymentsServiceUrl + "/billing/suspendedPayments/" + sequenceNumber + "?postedDate=" + postedDate
                + "&userId=" + userId + "&batchId=" + batchId + "&distributionDate=" + distributionDate;
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getResourceEntity(className, optionsResponseEntity);
        List<SupportData> supportDataList = getSupportDataList(SuspendedPaymentProperties.getAllProperties(),
                sequenceNumber, postedDate, userId, batchId, distributionDate);
        checkIfPaymentIdEnable(supportDataList, sequenceNumber, postedDate, userId, batchId, distributionDate);
        optionsResponseEntity.setSupportData(supportDataList);

        return optionsResponseEntity;
    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.addAll(optionUtilities.generateFiltersParameters());
        parameters.addAll(optionUtilities.generatePageParameters());
        optionLink.setParameters(parameters);
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Search Suspended Payments");
        optionLink.setRel("search");
        generateSchema(optionLink, className);
        generateParameters(optionLink);
        List<Parameters> parameters = new ArrayList<>();
        parameters.addAll(optionUtilities.generatePageParameters());
        optionLink.setParameters(parameters);
        return optionLink;
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Get Suspended Payment");
        optionLink.setRel("fetch");
        generateSchema(optionLink, className);
        return optionLink;
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    public SupportData getSupportData(String supportDataProperty, Short sequenceNumber, String postedDate,
            String userId, String batchId, String distributionDate) {
        SupportData supportData = new SupportData();
        switch (SuspendedPaymentProperties.valueOf(supportDataProperty)) {
        case BIT:
            supportData.setPropertyName(SuspendedPaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, supportDataProperty, BusCodeTranslationParentCode.PAYABLE_ITEM.getValue(),
                    PropertyValuesType.BUSTYPE_AND_PARENTCODE.getValue());
            break;
        case PYC:
            supportData.setPropertyName(SuspendedPaymentProperties.valueOf(supportDataProperty).getValue());
            supportData.setPropertyValues(suspendedPaymentService.getPaymentTypeList(sequenceNumber, postedDate, userId,
                    batchId, distributionDate));
            break;
        case SUS:
            supportData.setPropertyName(SuspendedPaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, supportDataProperty, BLANK_STRING,
                    PropertyValuesType.BUSTYPE_AND_PARENTCODE.getValue());
            break;
        default:
            supportData.setPropertyName(SuspendedPaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, supportDataProperty, PropertyValuesType.BUS_TYPE.getValue());
            break;
        }
        return supportData;
    }

    public List<SupportData> getSupportDataList(List<String> propertiesList, Short sequenceNumber, String postedDate,
            String userId, String batchId, String distributionDate) {
        List<SupportData> supportDataList = new ArrayList<>();
        List<String> supportDataProperties = getSupportDataPropertiesList(propertiesList);
        for (String supportDataProperty : supportDataProperties) {
            supportDataList.add(
                    getSupportData(supportDataProperty, sequenceNumber, postedDate, userId, batchId, distributionDate));
        }
        return supportDataList;
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        return null;
    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    private void checkIfPaymentIdEnable(List<SupportData> supportDataList, Short sequenceNumber, String postedDate,
            String userId, String batchId, String distributionDate) {
        boolean isenablePaymentId = true;
        String user = appSecurityServiceImpl.authenticateUser(userId.trim()).getUserSequenceId();
        BilUnIdCash bilUnIdCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(DateRoutine.dateTimeAsYYYYMMDD(distributionDate), sequenceNumber, batchId,
                        user, DateRoutine.dateTimeAsYYYYMMDD(postedDate)))
                .orElse(null);
        if (bilUnIdCash != null) {
            BilCashEntry bilCashEntry = bilCashEntryRepository
                    .findById(new BilCashEntryId(DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId, user,
                            bilUnIdCash.getBilEntrySeqNbr()))
                    .orElse(null);

            if (bilCashEntry != null && !bilCashEntry.getBilPmtOrderId().trim().isEmpty()) {
                isenablePaymentId = false;
            }
        }

        SupportData paymentIdSupportData = new SupportData();
        paymentIdSupportData.setPropertyName("enablePaymentId");
        paymentIdSupportData.setPropertyValues(Arrays.asList(String.valueOf(isenablePaymentId)));
        supportDataList.add(paymentIdSupportData);

    }

}
