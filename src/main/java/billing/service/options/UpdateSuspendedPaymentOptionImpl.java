package billing.service.options;

import static core.utils.CommonConstants.BLANK_STRING;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import billing.model.SuspendedPayment;
import billing.service.SuspendedPaymentService;
import billing.utils.BillingConstants.BusCodeTranslationParentCode;
import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.Parameters;
import core.options.model.SupportData;
import core.utils.CommonConstants.PropertyValuesType;
import core.utils.OptionUtilities;

@Component
public class UpdateSuspendedPaymentOptionImpl extends AbstractOptionsImpl implements OptionsService {

    @Value("${enterprise.payments.url}")
    private String paymentsServiceUrl;

    private String href = BLANK_STRING;

    private static final String OPTIONS_TITLE = "Options for basic operations on Update Suspended payment";

    @Autowired
    private OptionUtilities optionUtilities;

    @Autowired
    private SuspendedPaymentService suspendedPaymentService;

    public enum SuspendedPaymentProperties {
        PMS("Payment_Method"),
        SUS("Suspense_Reason"),
        PYC("Payment_Type"),
        SYM("Policy_Symbol"),
        BIT("PayableItem"),
        ACCOUNT_TYPE("Account_Type");

        private final String prop;

        private SuspendedPaymentProperties(String prop) {
            this.prop = prop;
        }

        public String getValue() {
            return prop;
        }

        public static List<String> getAllProperties() {
            List<String> propertyList = new ArrayList<>();
            for (SuspendedPaymentProperties e : SuspendedPaymentProperties.values()) {
                propertyList.add(e.name());
            }
            return propertyList;
        }
    }

    public OptionsResponseEntity getDocumentResponse(Class<SuspendedPayment> className, Short sequenceNumber,
            String postedDate, String userId, String batchId, String distributionDate) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        href = paymentsServiceUrl + "/billing/suspendedPayments/" + sequenceNumber + "/_update?postedDate=" + postedDate
                + "&userId=" + userId + "&batchId=" + batchId + "&distributionDate=" + distributionDate;
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        optionsResponseEntity.setSupportData(getSupportDataList(SuspendedPaymentProperties.getAllProperties()));

        return optionsResponseEntity;
    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.POST.name());
        optionLink.setTitle("Post Update Suspend Payment");
        optionLink.setRel(RequestMethod.POST.name());
        generateSchema(optionLink, className);
        generateParameters(optionLink);
        return optionLink;
    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.addAll(optionUtilities.generateFiltersParameters());
        parameters.addAll(optionUtilities.generatePageParameters());
        optionLink.setParameters(parameters);
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    public SupportData getSupportData(String supportDataProperty, Short sequenceNumber, String postedDate,
            String userId, String batchId, String distributionDate) {
        SupportData supportData = new SupportData();
        switch (SuspendedPaymentProperties.valueOf(supportDataProperty)) {
        case BIT:
            supportData.setPropertyName(SuspendedPaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, supportDataProperty, BusCodeTranslationParentCode.PAYABLE_ITEM.getValue(),
                    PropertyValuesType.BUSTYPE_AND_PARENTCODE.getValue());
            break;
        case PYC:
            supportData.setPropertyName(SuspendedPaymentProperties.valueOf(supportDataProperty).getValue());
            supportData.setPropertyValues(suspendedPaymentService.getPaymentTypeList(sequenceNumber, postedDate, userId,
                    batchId, distributionDate));
            break;
        case SUS:
            supportData.setPropertyName(SuspendedPaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, supportDataProperty, BLANK_STRING,
                    PropertyValuesType.BUSTYPE_AND_PARENTCODE.getValue());
            break;
        default:
            supportData.setPropertyName(SuspendedPaymentProperties.valueOf(supportDataProperty).getValue());
            getRestrictedFields(supportData, supportDataProperty, PropertyValuesType.BUS_TYPE.getValue());
            break;
        }
        return supportData;
    }

    public List<SupportData> getSupportDataList(List<String> propertiesList, Short sequenceNumber, String postedDate,
            String userId, String batchId, String distributionDate) {
        List<SupportData> supportDataList = new ArrayList<>();
        List<String> supportDataProperties = getSupportDataPropertiesList(propertiesList);
        for (String supportDataProperty : supportDataProperties) {
            supportDataList.add(
                    getSupportData(supportDataProperty, sequenceNumber, postedDate, userId, batchId, distributionDate));
        }
        return supportDataList;
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

}
