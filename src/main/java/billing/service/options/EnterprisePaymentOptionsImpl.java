package billing.service.options;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import billing.model.Batch;
import billing.model.Payment;
import billing.model.SuspendedPayment;
import core.options.model.OptionsResponseEntity;

@Component
public class EnterprisePaymentOptionsImpl {

    private static final String OPTIONS_TITLE = "Operations on Enterprise Payments";

    @Autowired
    private BatchOptionsImpl batchOptionsImpl;

    @Autowired
    private PaymentOptionsImpl paymentOptionsImpl;

    @Autowired
    private SuspendedPaymentOptionImpl suspendedPaymentOptionImpl;

    public OptionsResponseEntity getResourceResponse() {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        OptionsResponseEntity batchOptionsResponseEntity = batchOptionsImpl.getCollectionsResponse(Batch.class, null,
                null, null, 1, 10);
        optionsResponseEntity.getLinks().addAll(batchOptionsResponseEntity.getLinks());
        OptionsResponseEntity paymentOptionsResponseEntity = paymentOptionsImpl.getCollectionsResponse(Payment.class, 1,
                10, null);
        optionsResponseEntity.getLinks().addAll(paymentOptionsResponseEntity.getLinks());
        OptionsResponseEntity suspendPaymentOptionsResponseEntity = suspendedPaymentOptionImpl
                .getCollectionsResponse(SuspendedPayment.class, 1, 10, null);
        optionsResponseEntity.getLinks().addAll(suspendPaymentOptionsResponseEntity.getLinks());
        return optionsResponseEntity;
    }
}
