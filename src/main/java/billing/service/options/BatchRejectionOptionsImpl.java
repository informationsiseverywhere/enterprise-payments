package billing.service.options;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.Parameters;
import core.options.model.SupportData;
import core.utils.OptionUtilities;

@Component
public class BatchRejectionOptionsImpl extends AbstractOptionsImpl implements OptionsService {

    @Value("${enterprise.payments.url}")
    private String billingServiceUrl;

    @Autowired
    private OptionUtilities optionUtilities;

    private static final String OPTIONS_TITLE = "Options for basic operations on Rejection Batches Resource";

    String href = "";
    String batchId;
    String entryDate;
    Integer page = 1;
    Integer size = 10;

    public OptionsResponseEntity getCollectionsResponse(Class<? extends ResourceSupport> className, String query,
            String filters, String sort, Integer page, Integer size) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        this.page = page != null ? page : this.page;
        this.size = size != null ? size : this.size;
        href = billingServiceUrl + "/billing/rejectionBatches?page=" + this.page + "&size=" + this.size;
        if (query != null && !query.isEmpty()) {
            href = href + "&query=" + query;
        }
        if (filters != null) {
            href = href + "&filters=" + filters;
        }
        if (sort != null) {
            href = href + "&sort=" + sort;
        }
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        return optionsResponseEntity;
    }

    public OptionsResponseEntity getResourceResponse(Class<? extends ResourceSupport> className, String batchId,
            String entryDate) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        this.batchId = batchId;
        this.entryDate = entryDate;
        href = billingServiceUrl + "/billing/rejectionBatches/" + this.batchId + "?entryDate=" + this.entryDate;
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getResourceEntity(className, optionsResponseEntity);
        return optionsResponseEntity;
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {

        return null;
    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.addAll(optionUtilities.generateQueryParameters());
        parameters.addAll(optionUtilities.generateFiltersParameters());
        parameters.addAll(optionUtilities.generateSortParameters());
        parameters.addAll(optionUtilities.generatePageParameters());
        optionLink.setParameters(parameters);
    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(billingServiceUrl + "/billing/rejectionBatches");
        optionLink.setMethod(RequestMethod.POST.name());
        optionLink.setTitle("Create Batch Rejection");
        optionLink.setRel("create");
        generateSchema(optionLink, className);
        optionLink.setExample(getExample("/samples/BatchRejectionPost.txt"));
        return optionLink;
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Search Rejection Batches");
        optionLink.setRel("search");
        generateSchema(optionLink, className);
        generateParameters(optionLink);
        return optionLink;
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Get Rejection Batch");
        optionLink.setRel("fetch");
        generateSchema(optionLink, className);
        generateResourceParameters(optionLink);
        return optionLink;
    }

    public void generateResourceParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.add(new Parameters("requestParam", "batchId", "String", true, "Batch Identifier"));
        parameters.add(new Parameters("requestParam", "entryDate", "String", true, "Entry Date"));
        optionLink.setParameters(parameters);
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.PATCH.name());
        optionLink.setTitle("Update Rejection Batch");
        optionLink.setRel("patch");
        generateSchema(optionLink, className);
        generateResourceParameters(optionLink);
        optionLink.setExample(getExample("/samples/BatchRejectionPatch.txt"));
        return optionLink;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.DELETE.name());
        optionLink.setTitle("Delete Rejection Batch");
        optionLink.setRel("delete");
        generateResourceParameters(optionLink);
        return optionLink;
    }

}
