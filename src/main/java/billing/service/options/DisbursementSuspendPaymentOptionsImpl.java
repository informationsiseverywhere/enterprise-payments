package billing.service.options;

import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import application.security.service.impl.AppSecurityServiceImpl;
import billing.data.entity.BilEftActivity;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.repository.BilEftActivityRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.model.PayableOption;
import billing.utils.BillingConstants.BilTechKeyTypeCode;
import billing.utils.BillingConstants.BillingEftTransactionStatus;
import billing.utils.BillingConstants.CashEntryMethod;
import billing.utils.BillingConstants.PaymentProviderType;
import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.SupportData;
import core.utils.CommonConstants.PropertyValuesType;
import core.utils.DateRoutine;

@Component
public class DisbursementSuspendPaymentOptionsImpl extends AbstractOptionsImpl implements OptionsService {

    private static final String OPTIONS_TITLE = "Options for basic operations on Disbursed Resource";
    private String href = BLANK_STRING;

    @Value("${enterprise.payments.url}")
    private String paymentsServiceUrl;

    @Autowired
    private BilEftActivityRepository bilEftActivityRepository;

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    @Value("${onetimepayment.integration}")
    private String oneTimePaymentIntegration;

    @Value("${agencysweeppayment.integration}")
    private String agencySweepPaymentIntegration;

    @Value("${paymentIntegration.provider}")
    private String providerIntegration;

    private static final String EFT_DISBURSEMENT = "EFT/ACH";
    private static final String CREDIT_CARD_DISBURSEMENT = "Credit Card";
    private static final String DISBURSEMENT_TYPE = "DisbursementType";
    private static final String PAYMENT_TYPE = "PaymentType";
    private static final String PAYMENT_INTEGRATION = "PaymentIntegration";
    private static final String PAYMENT_PROVIDER = "PaymentProvider";

    public enum DisbursementProperties {
        DBR("Disbursement_Reason"), DAS("Disburse_Agency_Sweep"), DAD("Disburse_Agency"), RMI("Refund_Method");

        private final String prop;

        private DisbursementProperties(String prop) {
            this.prop = prop;
        }

        public String getValue() {
            return prop;
        }

        public static List<String> getAllProperties() {
            List<String> propertyList = new ArrayList<>();
            for (DisbursementProperties e : DisbursementProperties.values()) {
                propertyList.add(e.name());
            }
            return propertyList;
        }
    }

    public OptionsResponseEntity getDocumentResponse(Class<PayableOption> className, Short sequenceNumber,
            String postedDate, String userId, String batchId, String distributionDate) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        href = paymentsServiceUrl + "/billing/suspendedPayments/" + sequenceNumber + "/_disburse?postedDate="
                + postedDate + "&userId=" + userId + "&batchId=" + batchId + "&distributionDate=" + distributionDate;

        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        List<SupportData> supportDataList = getSupportDataList(DisbursementProperties.getAllProperties());
        getCollectionEntity(className, optionsResponseEntity);
        getDisbursementMethod(batchId, sequenceNumber, distributionDate, userId, supportDataList, postedDate);
        optionsResponseEntity.setSupportData(supportDataList);
        return optionsResponseEntity;
    }

    private void getDisbursementMethod(String batchId, Short sequenceNumber, String distributionDate, String userId,
            List<SupportData> supportDataList, String postedDate) {
        List<String> disbursementTypes = busCdTranslationRepository.findDescriptionListByType(DISBURSEMENT_TYPE,
                execContext.getLanguage());
        String userSequenceId = appSecurityServiceImpl.authenticateUser(userId).getUserSequenceId();
        BilUnIdCash bilUnIdCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(DateRoutine.dateTimeAsYYYYMMDD(distributionDate), sequenceNumber, batchId,
                        userSequenceId, DateRoutine.dateTimeAsYYYYMMDD(postedDate)))
                .orElse(null);
        if (bilUnIdCash != null) {
            if (getCashEntryMethodList().contains(bilUnIdCash.getBilCshEtrMthCd())) {
                String paymentOrderId = getpaymentOrderId(bilUnIdCash);
                switch (CashEntryMethod.getEnumValue(bilUnIdCash.getBilCshEtrMthCd())) {
                case CREDIT_CARD:
                    if ((!BooleanUtils.toBoolean(oneTimePaymentIntegration) && paymentOrderId.isEmpty())
                            || (BooleanUtils.toBoolean(oneTimePaymentIntegration) && !paymentOrderId.isEmpty()
                                    && providerIntegration
                                            .equalsIgnoreCase(PaymentProviderType.AUTHORIZE_NET.getValue()))) {
                        disbursementTypes.remove(EFT_DISBURSEMENT);

                        SupportData oneTimeCreditCardPaymentType = new SupportData();
                        oneTimeCreditCardPaymentType.setPropertyName(PAYMENT_TYPE);
                        oneTimeCreditCardPaymentType.setPropertyValues(Arrays.asList("OneTimePayment"));
                        supportDataList.add(oneTimeCreditCardPaymentType);

                        SupportData oneTimeCreditCardIntegration = new SupportData();
                        oneTimeCreditCardIntegration.setPropertyName(PAYMENT_INTEGRATION);
                        oneTimeCreditCardIntegration
                                .setPropertyValues(Arrays.asList(String.valueOf(oneTimePaymentIntegration)));
                        supportDataList.add(oneTimeCreditCardIntegration);

                        SupportData paymentProviderSupportData = new SupportData();
                        paymentProviderSupportData.setPropertyName(PAYMENT_PROVIDER);
                        paymentProviderSupportData.setPropertyValues(Arrays.asList(providerIntegration));
                        supportDataList.add(paymentProviderSupportData);
                    } else {
                        disbursementTypes.removeAll(Arrays.asList(EFT_DISBURSEMENT, CREDIT_CARD_DISBURSEMENT));
                    }
                    break;
                case BANK_ACCOUNT:
                    if ((!BooleanUtils.toBoolean(oneTimePaymentIntegration) && paymentOrderId.isEmpty())
                            || (BooleanUtils.toBoolean(oneTimePaymentIntegration) && !paymentOrderId.isEmpty()
                                    && providerIntegration
                                            .equalsIgnoreCase(PaymentProviderType.AUTHORIZE_NET.getValue()))) {
                        disbursementTypes.remove(CREDIT_CARD_DISBURSEMENT);

                        SupportData oneTimeBankPaymentType = new SupportData();
                        oneTimeBankPaymentType.setPropertyName(PAYMENT_TYPE);
                        oneTimeBankPaymentType.setPropertyValues(Arrays.asList("OneTimePayment"));
                        supportDataList.add(oneTimeBankPaymentType);

                        SupportData oneTimeBankPaymentIntegration = new SupportData();
                        oneTimeBankPaymentIntegration.setPropertyName(PAYMENT_INTEGRATION);
                        oneTimeBankPaymentIntegration
                                .setPropertyValues(Arrays.asList(String.valueOf(oneTimePaymentIntegration)));
                        supportDataList.add(oneTimeBankPaymentIntegration);

                        SupportData paymentProviderSupportData = new SupportData();
                        paymentProviderSupportData.setPropertyName(PAYMENT_PROVIDER);
                        paymentProviderSupportData.setPropertyValues(Arrays.asList(providerIntegration));
                        supportDataList.add(paymentProviderSupportData);
                    } else {
                        disbursementTypes.removeAll(Arrays.asList(EFT_DISBURSEMENT, CREDIT_CARD_DISBURSEMENT));
                    }
                    break;
                case AGENCY_SWEEP:
                    if ((!BooleanUtils.toBoolean(agencySweepPaymentIntegration) && paymentOrderId.isEmpty())
                            || (BooleanUtils.toBoolean(agencySweepPaymentIntegration) && !paymentOrderId.isEmpty()
                                    && providerIntegration
                                            .equalsIgnoreCase(PaymentProviderType.AUTHORIZE_NET.getValue()))) {
                        disbursementTypes.remove(CREDIT_CARD_DISBURSEMENT);

                        SupportData agencySweepPaymentType = new SupportData();
                        agencySweepPaymentType.setPropertyName(PAYMENT_TYPE);
                        agencySweepPaymentType.setPropertyValues(Arrays.asList("AgencySweep"));
                        supportDataList.add(agencySweepPaymentType);

                        SupportData agencySweepIntegration = new SupportData();
                        agencySweepIntegration.setPropertyName(PAYMENT_INTEGRATION);
                        agencySweepIntegration
                                .setPropertyValues(Arrays.asList(String.valueOf(agencySweepPaymentIntegration)));
                        supportDataList.add(agencySweepIntegration);

                        SupportData paymentProviderSupportData = new SupportData();
                        paymentProviderSupportData.setPropertyName(PAYMENT_PROVIDER);
                        paymentProviderSupportData.setPropertyValues(Arrays.asList(providerIntegration));
                        supportDataList.add(paymentProviderSupportData);
                    } else {
                        disbursementTypes.removeAll(Arrays.asList(EFT_DISBURSEMENT, CREDIT_CARD_DISBURSEMENT));
                    }
                    break;
                default:
                    break;
                }
            } else {
                disbursementTypes.removeAll(Arrays.asList(EFT_DISBURSEMENT, CREDIT_CARD_DISBURSEMENT));
            }

        } else {
            disbursementTypes.clear();
        }
        SupportData disursementSupport = new SupportData();
        disursementSupport.setPropertyName("Disbursement_Method_Types");
        disursementSupport.setPropertyValues(disbursementTypes);
        supportDataList.add(disursementSupport);

    }

    private String getpaymentOrderId(BilUnIdCash bilUnIdCash) {
        List<BilEftActivity> bilEftActivityList = bilEftActivityRepository.getPaymentIdRow(
                bilUnIdCash.getBillUnidCashId(), BilTechKeyTypeCode.UNIDENTIFIED.getValue(),
                bilUnIdCash.getBilUnIdCashId().getBilDtbDt(), bilUnIdCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                BillingEftTransactionStatus.APPLIED.getValue());
        if (bilEftActivityList != null && !bilEftActivityList.isEmpty()) {
            return bilEftActivityList.get(SHORT_ZERO).getBilPmtOrderId().trim();
        } else {
            return BLANK_STRING;
        }

    }

    private List<Character> getCashEntryMethodList() {
        ArrayList<Character> list = new ArrayList<>();
        list.add(CashEntryMethod.CREDIT_CARD.getCashEntryMethod());
        list.add(CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod());
        list.add(CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod());
        return list;
    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        // Auto-generated method stub
    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.POST.name());
        optionLink.setTitle("Post Disbursement");
        optionLink.setRel(RequestMethod.POST.name());
        generateSchema(optionLink, className);
        generateParameters(optionLink);
        return optionLink;
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        SupportData supportData = new SupportData();
        supportData.setPropertyName(DisbursementProperties.valueOf(supportDataProperty).getValue());
        getRestrictedFields(supportData, supportDataProperty, PropertyValuesType.BUS_TYPE.getValue());
        return supportData;
    }
}
