package billing.service.options;

import static core.utils.CommonConstants.BLANK_STRING;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import billing.model.WriteOffOption;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.SupportData;
import core.utils.CommonConstants.PropertyValuesType;

@Component
public class WriteOffSuspendPaymentOptionsImpl extends AbstractOptionsImpl implements OptionsService {

    private static final String OPTIONS_TITLE = "Options for basic operations on Write Off Resource";
    private String href = BLANK_STRING;

    @Value("${enterprise.payments.url}")
    private String paymentsServiceUrl;

    public enum WriteOffProperties {
        WriteOffReason("CWM");

        private final String property;

        private WriteOffProperties(String prop) {
            this.property = prop;
        }

        public String getValue() {
            return property;
        }

        public static List<String> getAllProperties() {
            List<String> propertyList = new ArrayList<>();
            for (WriteOffProperties e : WriteOffProperties.values()) {
                propertyList.add(e.name());
            }
            return propertyList;
        }
    }

    public OptionsResponseEntity getDocumentResponse(Class<WriteOffOption> className, Short sequenceNumber,
            String postedDate, String userId, String batchId, String distributionDate) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        href = paymentsServiceUrl + "/billing/suspendedPayments/" + sequenceNumber + "/_writeOff?postedDate="
                + postedDate + "&userId=" + userId + "&batchId=" + batchId + "&distributionDate=" + distributionDate;

        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        optionsResponseEntity.setSupportData(getSupportDataList(WriteOffProperties.getAllProperties()));
        return optionsResponseEntity;
    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        // Auto-generated method stub

    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.POST.name());
        optionLink.setTitle("Post Write Off");
        optionLink.setRel(RequestMethod.POST.name());
        generateSchema(optionLink, className);
        generateParameters(optionLink);
        return optionLink;
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        SupportData supportData = new SupportData();
        supportData.setPropertyName(WriteOffProperties.valueOf(supportDataProperty).name());
        Set<String> dataProperty = new HashSet<>();
        dataProperty.addAll(getDataProperty(BusCodeTranslationType.WRITEOFF_MANUAL_TYPE.getValue(), supportData));
        dataProperty.addAll(getDataProperty(BusCodeTranslationType.WRITEOFF_LIFE_TYPE.getValue(), supportData));
        supportData.setPropertyValues(dataProperty.stream().collect(Collectors.toList()));
        return supportData;
    }

    private List<String> getDataProperty(String codeType, SupportData supportData) {
        SupportData newSupportData = new SupportData();
        getRestrictedFields(newSupportData, codeType, PropertyValuesType.BUS_TYPE.getValue());
        if (newSupportData.getDefaultValue() != null) {
            supportData.setDefaultValue(newSupportData.getDefaultValue());
        }
        return newSupportData.getPropertyValues();
    }
}
