package billing.service.options;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import billing.data.repository.BilEftRejReaRepository;
import core.options.AbstractOptionsImpl;
import core.options.OptionsService;
import core.options.model.OptionLink;
import core.options.model.OptionsResponseEntity;
import core.options.model.Parameters;
import core.options.model.SupportData;
import core.utils.CommonConstants.PropertyValuesType;
import core.utils.OptionUtilities;

@Component
public class BatchRejectionPaymentOptionsImpl extends AbstractOptionsImpl implements OptionsService {

    @Value("${enterprise.payments.url}")
    private String billingServiceUrl;

    @Autowired
    private OptionUtilities optionUtilities;

    @Autowired
    private BilEftRejReaRepository bilEftRejReaRepository;

    private static final String OPTIONS_TITLE = "Options for basic operations on Batch Rejection Payment Resource";
    private static final String REQUEST_PARAM = "requestParam";
    private static final String BILLING_REJECTION_BATCHES = "/billing/rejectionBatches/";

    String href = "";
    String batchId;
    short paymentSequenceNumber = 0;
    String entryDate;
    Integer page = 1;
    Integer size = 10;

    public enum RejectionPaymentProperties {
        RejectionReason("REJ");

        private final String prop;

        private RejectionPaymentProperties(String prop) {
            this.prop = prop;
        }

        public String getValue() {
            return prop;
        }

        public static List<String> getAllProperties() {
            List<String> propertyList = new ArrayList<>();
            for (RejectionPaymentProperties e : RejectionPaymentProperties.values()) {
                propertyList.add(e.name());
            }
            return propertyList;
        }
    }

    public OptionsResponseEntity getCollectionsResponse(Class<? extends ResourceSupport> className, String batchId,
            String entryDate, String query, String filters, String sort, Integer page, Integer size) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        this.page = page != null ? page : this.page;
        this.size = size != null ? size : this.size;
        this.batchId = batchId;
        this.entryDate = entryDate;
        href = billingServiceUrl + BILLING_REJECTION_BATCHES + this.batchId + "/payments?entryDate=" + this.entryDate
                + "&page=" + this.page + "&size=" + this.size;
        if (query != null) {
            href = href + "&query=" + query;
        }
        if (filters != null) {
            href = href + "&filters=" + filters;
        }
        if (sort != null) {
            href = href + "&sort=" + sort;
        }

        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getCollectionEntity(className, optionsResponseEntity);
        optionsResponseEntity.setSupportData(getSupportDataList(RejectionPaymentProperties.getAllProperties()));
        return optionsResponseEntity;
    }

    public OptionsResponseEntity getResourceResponse(Class<? extends ResourceSupport> className, String batchId,
            short paymentSequenceNumber, String entryDate) {
        OptionsResponseEntity optionsResponseEntity = new OptionsResponseEntity();
        this.batchId = batchId;
        this.entryDate = entryDate;
        this.paymentSequenceNumber = paymentSequenceNumber;

        href = billingServiceUrl + BILLING_REJECTION_BATCHES + this.batchId + "/payments/" + this.paymentSequenceNumber
                + "?entryDate=" + this.entryDate;
        optionsResponseEntity.setTitle(OPTIONS_TITLE);
        getResourceEntity(className, optionsResponseEntity);
        optionsResponseEntity.setSupportData(getSupportDataList(RejectionPaymentProperties.getAllProperties()));
        return optionsResponseEntity;
    }

    @Override
    public void generateParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.addAll(optionUtilities.generateQueryParameters());
        parameters.addAll(optionUtilities.generateFiltersParameters());
        parameters.addAll(optionUtilities.generateSortParameters());
        parameters.addAll(optionUtilities.generatePageParameters());
        optionLink.setParameters(parameters);
    }

    @Override
    public OptionLink addCreateOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(
                billingServiceUrl + BILLING_REJECTION_BATCHES + this.batchId + "/payments?entryDate=" + this.entryDate);
        optionLink.setMethod(RequestMethod.POST.name());
        optionLink.setTitle("Create Rejection Batch Payment");
        optionLink.setRel("create");
        generateSchema(optionLink, className);
        generateResourceParameters(optionLink);
        optionLink.setExample(getExample("/samples/BatchRejectionPaymentPatch.txt"));
        return optionLink;
    }

    @Override
    public OptionLink addPutOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addSearchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Search Rejection Batch Payments");
        optionLink.setRel("search");
        generateSchema(optionLink, className);
        generateParameters(optionLink);
        return optionLink;
    }

    @Override
    public OptionLink addFetchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.GET.name());
        optionLink.setTitle("Get Rejection Batch Payment");
        optionLink.setRel("fetch");
        generateSchema(optionLink, className);
        generateResourceParameters(optionLink);
        return optionLink;
    }

    @Override
    public OptionLink addFetchAllOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addFetchAllByPagesOptionLink(Class<? extends ResourceSupport> className) {
        return null;
    }

    @Override
    public OptionLink addPatchOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.PATCH.name());
        optionLink.setTitle("Update Rejection Batch Payment");
        optionLink.setRel("patch");
        generateSchema(optionLink, className);
        generateResourceParameters(optionLink);
        optionLink.setExample(getExample("/samples/BatchRejectionPaymentPatch.txt"));
        return optionLink;
    }

    @Override
    public OptionLink addDeleteOptionLink(Class<? extends ResourceSupport> className) {
        OptionLink optionLink = new OptionLink();
        optionLink.setHref(href);
        optionLink.setMethod(RequestMethod.DELETE.name());
        optionLink.setTitle("Delete Batch Rejection Payment");
        optionLink.setRel("delete");
        generateResourceParameters(optionLink);
        return optionLink;
    }

    public void generateResourceParameters(OptionLink optionLink) {
        List<Parameters> parameters = new ArrayList<>();
        parameters.add(new Parameters(REQUEST_PARAM, "batchId", "String", true, "Batch Identifier"));
        parameters
                .add(new Parameters(REQUEST_PARAM, "paymentSequenceNumber", "short", true, "Payment Sequence Number"));
        parameters.add(new Parameters(REQUEST_PARAM, "entryDate", "String", true, "Entry Date"));
        optionLink.setParameters(parameters);
    }

    @Override
    public SupportData getSupportData(String supportDataProperty) {
        SupportData supportData = new SupportData();
        switch (RejectionPaymentProperties.valueOf(supportDataProperty)) {
        case RejectionReason:
            supportData.setPropertyName(supportDataProperty);
            supportData.setPropertyValues(getRejectionReasonValues());
            getRestrictedFields(supportData, "REJECTION_REASON", PropertyValuesType.BUS_TYPE.getValue());
            break;

        default:
            break;
        }
        return supportData;
    }

    public List<String> getRejectionReasonValues() {
        List<String> propertyValuesList = new ArrayList<>();
        propertyValuesList.addAll(bilEftRejReaRepository.findsupportData());
        return propertyValuesList.stream().map(String::trim).collect(Collectors.toList());
    }

}
