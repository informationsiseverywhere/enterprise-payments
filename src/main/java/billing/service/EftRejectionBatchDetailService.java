package billing.service;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public interface EftRejectionBatchDetailService {

    XSSFWorkbook buildExcelDocument(String batchId, String postedDate, String reportId);

}
