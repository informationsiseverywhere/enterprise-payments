package billing.service;

import java.util.List;

import billing.model.RejectionPayment;
import core.model.Paging;

public interface BatchRejectionPaymentService {

    public List<RejectionPayment> findAllBatchPayments(String entryDate, String batchId, String query, String filters,
            String sort, int size, int page, Paging paging);

    public RejectionPayment findBatchPaymentDetails(String batchId, String entryDate, short paymentSeqNumber);

    public Short saveBatchPayment(String batchId, String entryDate, RejectionPayment batchPayment);

    public void deleteBatchPayment(String batchId, String entryDate, short paymentSeqNumber);

    public void patchBatchPayment(String batchId, String entryDate, short paymentSeqNumber, RejectionPayment batchPayment);
}
