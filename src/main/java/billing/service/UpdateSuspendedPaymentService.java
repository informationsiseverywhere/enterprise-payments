package billing.service;

import java.time.ZonedDateTime;

import billing.model.SuspendedPayment;

public interface UpdateSuspendedPaymentService {

    public void save(Short sequenceNumber, ZonedDateTime postedDate, String userId, String batchId,
            ZonedDateTime distributionDate, SuspendedPayment suspendedPayment);
}
