package billing.service;

import java.util.List;

import billing.model.CurrencyExchange;
import core.model.Paging;

public interface ExchangeRateService {

    public List<CurrencyExchange> findAllExchangeRates(String filters, int size, int page, Paging paging);

    public CurrencyExchange findExchangeRate(String exchangeDate, String fromCurrency, String toCurrency,
            Double fromAmount);

    public void patchExchangeRate(String exchangeDate, String fromCurrency, String toCurrency, CurrencyExchange currencyExchange);
}
