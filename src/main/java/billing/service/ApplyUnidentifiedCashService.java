package billing.service;

import billing.data.entity.BilAccount;

public interface ApplyUnidentifiedCashService {

	public void applyUnidentifiedCash(BilAccount bilAccount, String policyNumber, String identifierType,
			Boolean isBusGrpOverride);
}
