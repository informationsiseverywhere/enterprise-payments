package billing.service;

import billing.data.entity.BilAgent;

public interface AgencyAccountPaymentService {

	public void applyUnidentifiedCash(BilAgent bilAgent);
}
