package billing.service;

import java.time.ZonedDateTime;

import javax.validation.Valid;

import billing.model.ReverseOption;

public interface ReverseSuspendPaymentService {

    public void save(Short sequenceNumber, ZonedDateTime postedDate, String user, String batchId,
            ZonedDateTime distributionDate, @Valid ReverseOption reverseOption);

}
