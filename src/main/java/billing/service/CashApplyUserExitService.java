package billing.service;

public interface CashApplyUserExitService {
    
    public boolean callCashApplyUserExit(String policyId);

}
