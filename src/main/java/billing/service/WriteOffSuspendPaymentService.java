package billing.service;

import java.time.ZonedDateTime;

import billing.model.WriteOffOption;

public interface WriteOffSuspendPaymentService {

    void save(Short sequenceNumber, ZonedDateTime postedDate, String userId, String batchId,
            ZonedDateTime distributionDate, WriteOffOption writeOffOption);
}
