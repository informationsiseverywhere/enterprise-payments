package billing.validation;

import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.DECIMAL_ZERO;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import application.security.service.impl.AppSecurityServiceImpl;
import application.utils.service.FieldLevelAuthenticationService;
import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.model.WriteOffOption;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class WriteOffSuspendPaymentValidator implements Validator {

    private Short sequenceNumber;
    private String postedDate;
    private String user;
    private String batchId;
    private String distributionDate;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    MultiCurrencyService multiCurrencyService;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    @Autowired
    private FieldLevelAuthenticationService fieldLevelAuthenticationService;

    private static final Logger logger = LogManager.getLogger(WriteOffSuspendPaymentValidator.class);

    @Override
    public boolean supports(Class<?> clazz) {

        return WriteOffOption.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        WriteOffOption writeOffOption = (WriteOffOption) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reason", "bad.request.missing.parameter",
                new Object[] { "reason" });

        if (errors != null && !errors.getAllErrors().isEmpty()) {
            logger.error("Fields can't be null");
            return;
        }
        String typeCode = validateReason(writeOffOption);
        checkIfWriteOffAllowed(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate), user, batchId,
                DateRoutine.dateTimeAsYYYYMMDD(distributionDate), typeCode);

    }

    private void checkIfWriteOffAllowed(Short sequenceNumber, ZonedDateTime postedDate, String user, String batchId,
            ZonedDateTime distributionDate, String typeCode) {
        String userId = appSecurityServiceImpl.authenticateUser(user).getUserSequenceId();
        BilUnIdCash bilUnidentifiedCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(distributionDate, sequenceNumber, batchId, userId, postedDate))
                .orElse(null);

        if (bilUnidentifiedCash == null) {
            logger.debug("Payment not found in BIL_UNID_CASH");
            throw new DataNotFoundException("payment.does.not.exists");
        }
        if (!bilUnidentifiedCash.getBilDspTypeCd().trim().equals(BilDspTypeCode.SUSPENDED.getValue())) {
            logger.debug("Only suspended row can be disbursed");
            throw new InvalidDataException("row.selected.does.not.qualify.for", new Object[] { "Write-Off" });
        }
        if (bilUnidentifiedCash.getBilRctAmt() <= DECIMAL_ZERO) {
            logger.debug("Payment amount must be greater than zero");
            throw new InvalidDataException("paymentAmount.invalid");
        }
        if (typeCode.equals(BusCodeTranslationType.WRITEOFF_LIFE_TYPE.getValue())
                && bilUnidentifiedCash.getPolNbr().trim().equals(BLANK_STRING)) {
            logger.debug("Policy Number direction is required for the selected write-off Reason");
            throw new InvalidDataException("policy.required", new Object[] { "Manual Life Pmt W-OFF" });
        }
        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(postedDate, batchId, userId)).orElse(null);
        if (bilCashEntryTot == null) {
            throw new InvalidDataException("no.data.found");
        }
        fieldLevelAuthenticationService.validateAmount("UNIDWRITE-OFF", bilUnidentifiedCash.getBilRctAmt(),
                multiCurrencyService.getCurrencySymbolByCode(bilCashEntryTot.getCurrencyCd()));
    }

    private String validateReason(WriteOffOption paymentOperation) {
        List<BusCdTranslation> busCodeTranslationList = busCdTranslationRepository.findAllByDescriptionAndTypeList(
                paymentOperation.getReason(), Arrays.asList(BusCodeTranslationType.WRITEOFF_MANUAL_TYPE.getValue(),
                        BusCodeTranslationType.WRITEOFF_LIFE_TYPE.getValue()),
                execContext.getLanguage());
        if (busCodeTranslationList == null || busCodeTranslationList.isEmpty()) {
            logger.error("Record not found in support table corresponding to the given code.");
            throw new DataNotFoundException("support.data.missing", new Object[] { paymentOperation.getReason() });
        }
        paymentOperation.setReason(busCodeTranslationList.get(0).getBusCdTranslationId().getBusCd().trim());
        return busCodeTranslationList.get(0).getBusCdTranslationId().getBusCd().trim();
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public void setSequenceNumber(Short sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setDistributionDate(String distributionDate) {
        this.distributionDate = distributionDate;
    }
}
