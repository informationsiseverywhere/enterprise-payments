package billing.validation;

import static core.utils.CommonConstants.DECIMAL_ZERO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import application.security.service.impl.AppSecurityServiceImpl;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.repository.BilUnIdCashRepository;
import billing.model.ReverseOption;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CashEntryMethod;
import billing.utils.BillingConstants.PaymentProviderType;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ReverseSuspendPaymentValidator implements Validator {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    @Value("${paymentIntegration.provider}")
    private String provider;

    private static final Logger logger = LogManager.getLogger(ReverseSuspendPaymentValidator.class);

    private Short sequenceNumber;
    private String postedDate;
    private String userId;
    private String batchId;
    private String distributionDate;

    @Override
    public boolean supports(Class<?> clazz) {
        return ReverseOption.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ReverseOption reverseOption = (ReverseOption) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reason", "bad.request.missing.parameter",
                new Object[] { "reason" });
        if (errors != null && !errors.getAllErrors().isEmpty()) {
            return;
        }

        validateReason(reverseOption);
        validateUnidentifiedCashDisposition(reverseOption);

    }

    private void validateUnidentifiedCashDisposition(ReverseOption reverseOption) {
        String userSequenceId = appSecurityServiceImpl.authenticateUser(userId).getUserSequenceId();
        BilUnIdCash bilUnidentifiedCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(DateRoutine.dateTimeAsYYYYMMDD(distributionDate), sequenceNumber, batchId,
                        userSequenceId, DateRoutine.dateTimeAsYYYYMMDD(postedDate)))
                .orElse(null);

        if (bilUnidentifiedCash == null) {
            logger.debug("Payment not found in BIL_UNID_CASH");
            throw new DataNotFoundException("payment.does.not.exists");
        }

        if (provider.equals(PaymentProviderType.PAYMENTUS.getValue())
                && (bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()
                        || bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()
                        || bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()
                        || isDigitalWalletPayment(bilUnidentifiedCash.getBilCshEtrMthCd()))) {
            throw new InvalidDataException("not.allowing.reverse.paymentus");
        }

        if (!bilUnidentifiedCash.getBilDspTypeCd().equals(BilDspTypeCode.SUSPENDED.getValue())
                && !bilUnidentifiedCash.getBilDspTypeCd().equals(BilDspTypeCode.DISBURSED.getValue())
                && !bilUnidentifiedCash.getBilDspTypeCd().equals(BilDspTypeCode.WRITE_OFF.getValue())) {

            logger.error("The selected row does not qualify for reverse operation. "
                    + "Reversals could be performed only on Suspended/Disbursed/WriteOff activity");

            throw new InvalidDataException("activity.does.not.qualify.for.reverse");
        }

        if (bilUnidentifiedCash.getBilRctAmt() == DECIMAL_ZERO) {
            logger.error("Amount to be reversed cannot be equal to zero");
            throw new InvalidDataException("reverseAmount.invalid");
        }

        validateDepositDate(reverseOption, DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilDepositDt()));

    }

    private void validateDepositDate(ReverseOption reverseOption, String depositDate) {

        if (reverseOption.getDepositDate() == null || reverseOption.getDepositDate().trim().isEmpty()) {
            reverseOption.setDepositDate(depositDate.trim());
        }

        validateDateFormat(reverseOption.getDepositDate());

    }

    private void validateDateFormat(String depositDate) {
        try {
            DateRoutine.dateTimeAsYYYYMMDD(depositDate);
        } catch (Exception exception) {
            logger.error("Deposit date passed is not a valid date");
            throw new InvalidDataException("depositDate.invalid", new Object[] {
                    DateRoutine.dateTimeAsMMDDYYYYAsString(DateRoutine.dateTimeAsYYYYMMDD(depositDate)) });
        }

    }

    private void validateReason(ReverseOption reverseOption) {
        logger.debug("fetch reason with busType as PRV");
        BusCdTranslation supportData = busCdTranslationRepository.findByDescriptionAndType(reverseOption.getReason(),
                BusCodeTranslationType.REVERSE_SUSPENSE_TYPE.getValue(), execContext.getLanguage());
        if (supportData == null) {
            throw new InvalidDataException("support.data.not.found", new Object[] { reverseOption.getReason(),
                    BusCodeTranslationType.REVERSE_SUSPENSE_TYPE.getValue() });
        }
        reverseOption.setReason(supportData.getBusCdTranslationId().getBusCd().trim());
    }

    private boolean isDigitalWalletPayment(Character cashEntryMethodCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(
                String.valueOf(cashEntryMethodCode), BusCodeTranslationType.DIGITAL_WALLET_TYPE.getValue(),
                execContext.getLanguage());

        return busCdTranslation != null ? Boolean.TRUE : Boolean.FALSE;
    }

    public void setSequenceNumber(Short sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public void setDistributionDate(String distributionDate) {
        this.distributionDate = distributionDate;
    }
}
