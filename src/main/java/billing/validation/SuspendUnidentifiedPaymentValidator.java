package billing.validation;

import static core.utils.CommonConstants.DECIMAL_ZERO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import application.security.service.impl.AppSecurityServiceImpl;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.repository.BilUnIdCashRepository;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.CheckProductionMethod;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.utils.DateRoutine;
import disbursement.utils.DisbursementConstants.DisbursementStatusCode;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SuspendUnidentifiedPaymentValidator implements Validator {

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    public static final Logger logger = LogManager.getLogger(SuspendUnidentifiedPaymentValidator.class);

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }

    @Override
    public void validate(Object target, Errors errors) {
        // Auto-generated method stub
    }

    public void validate(Short sequenceNumber, String postedDate, String userId, String batchId,
            String distributionDate) {

        String userSequenceId = appSecurityServiceImpl.authenticateUser(userId).getUserSequenceId();
        BilUnIdCash bilUnidentifiedCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(DateRoutine.dateTimeAsYYYYMMDD(distributionDate), sequenceNumber, batchId,
                        userSequenceId, DateRoutine.dateTimeAsYYYYMMDD(postedDate)))
                .orElse(null);
        if (bilUnidentifiedCash == null) {
            logger.debug("Payment not found in BIL_UNID_CASH");
            throw new DataNotFoundException("payment.does.not.exists");
        }
        if (bilUnidentifiedCash.getBilRctAmt() <= DECIMAL_ZERO) {
            logger.debug("Payment amount must be greater than zero");
            throw new InvalidDataException("paymentAmount.invalid");
        }
        if (!bilUnidentifiedCash.getBilDspTypeCd().trim().equals(BilDspTypeCode.WRITE_OFF.getValue())
                && !bilUnidentifiedCash.getBilDspTypeCd().trim().equals(BilDspTypeCode.DISBURSED.getValue())) {
            logger.debug("Only disbursed/WriteOff row can be suspended");
            throw new InvalidDataException("row.selected.does.not.qualify.for", new Object[] { "Suspend" });
        }
        if (bilUnidentifiedCash.getBilDspTypeCd().trim().equals(BilDspTypeCode.DISBURSED.getValue())) {
            if (bilUnidentifiedCash.getBilChkPrdMthCd() == CheckProductionMethod.WIRE_TRANSFER.getValue()
                    || bilUnidentifiedCash.getBilChkPrdMthCd() == CheckProductionMethod.WIRE_TRANSFER_COMBINED
                            .getValue()) {
                logger.error(
                        "The disbursed activity selected has been converted to a wire transfer and cannot be manually suspended.");
                throw new InvalidDataException("disbursement.cannot.suspend");
            }
            if (bilUnidentifiedCash.getDwsStatusCd() != DisbursementStatusCode.PENDING.toChar()
                    && bilUnidentifiedCash.getDwsStatusCd() != DisbursementStatusCode.SELECTCOMBINED.toChar()
                    && bilUnidentifiedCash.getDwsStatusCd() != DisbursementStatusCode.HOLD.toChar()
                    && bilUnidentifiedCash.getDwsStatusCd() != DisbursementStatusCode.COMBINEDPAYMENT.toChar()) {
                logger.error(
                        "The disbursed activity selected is not in pending or held status and cannot be resuspended.");
                throw new InvalidDataException("disbursement.cannot.suspend");
            }
        }
    }
}
