package billing.validation;

import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import application.security.service.impl.AppSecurityServiceImpl;
import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilAgent;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilThirdParty;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilCashEntryId;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilAgentRepository;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.model.SuspendedPayment;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilReceiptTypeCode;
import billing.utils.BillingConstants.BilRules;
import billing.utils.BillingConstants.BusCodeTranslationParentCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CashEntryMethod;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.data.entity.id.BusCdTranslationId;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import party.data.entity.CcmAgency;
import party.data.entity.ClientReference;
import party.data.entity.CltObjRelation;
import party.data.repository.CcmAgencyRepository;
import party.data.repository.ClientReferenceRepository;
import party.data.repository.CltObjRelationRepository;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UpdateSuspendedPaymentValidator implements Validator {

    private Short sequenceNumber;
    private String postedDate;
    private String user;
    private String batchId;
    private String distributionDate;

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private CcmAgencyRepository ccmAgencyRepository;

    @Autowired
    private ClientReferenceRepository clientReferenceRepository;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    private BilAgentRepository bilAgentRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private CltObjRelationRepository cltObjRelationRepository;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    public static final Logger logger = LogManager.getLogger(UpdateSuspendedPaymentValidator.class);

    private static final String AGENCY = "AGY";
    private static final String ADDITIONAL_ID = "Additional Id";
    private static final String CURRENCY_NOT_MATCHING = "Currency Not matching, Payments cannot be directed.";
    private static final String EQUITY_PAYMENT_INVALID = "equity.payment.invalid";
    private static final String AGENCY_SWEEP_PROCESS_FAILED = "agency.sweep.process.failed";
    private static final String DIRECTION_NOT_ALLOWED = "direction.not.allowed";
    private static final String POLICY_DIRECTION_NOT_ALLOWED = "policy.direction.not.allowed";
    private static final String SUPPORT_DATA_MISSING = "support.data.missing";
    private static final String IDENTIFIER_TYPE = "identifierType";
    private static final String CURRENCY_DIRECTION_NOT_ALLOWED = "currency.direction.not.allowed";

    @Override
    public boolean supports(Class<?> clazz) {
        return SuspendedPayment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        SuspendedPayment suspendedPayment = (SuspendedPayment) target;

        if ((suspendedPayment.getIdentifier() == null || suspendedPayment.getIdentifier().trim().isEmpty())
                && (suspendedPayment.getPolicyNumber() == null || suspendedPayment.getPolicyNumber().trim().isEmpty())
                && (suspendedPayment.getPayableItem() == null || suspendedPayment.getPayableItem().trim().isEmpty())
                && (suspendedPayment.getPaymentId() == null || suspendedPayment.getPaymentId().trim().isEmpty())) {
            throw new InvalidDataException("identifier.needed");
        }

        if (suspendedPayment.getIdentifier() != null && !suspendedPayment.getIdentifier().trim().isEmpty()
                && (suspendedPayment.getIdentifierType() == null
                        || suspendedPayment.getIdentifierType().trim().isEmpty())) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, IDENTIFIER_TYPE, "bad.request.data.invalid",
                    new Object[] { "Type Of Identifier" });
        }

        if (suspendedPayment.getPolicySymbol() != null && !suspendedPayment.getPolicySymbol().trim().isEmpty()
                && (suspendedPayment.getPolicyNumber() == null
                        || suspendedPayment.getPolicyNumber().trim().isEmpty())) {
            errors.rejectValue(IDENTIFIER_TYPE, "policy.missing", new Object[] { "Policy Symbol" }, BLANK_STRING);
        }

        if (suspendedPayment.getPayableItem() != null && !suspendedPayment.getPayableItem().trim().isEmpty()
                && (suspendedPayment.getPolicyNumber() == null
                        || suspendedPayment.getPolicyNumber().trim().isEmpty())) {
            errors.rejectValue(IDENTIFIER_TYPE, "policy.missing", new Object[] { "Payable Item" }, BLANK_STRING);
        }

        if (suspendedPayment.getHoldSuspenseIndicator() != null && suspendedPayment.getHoldSuspenseIndicator()
                && (suspendedPayment.getSuspenseReason() == null
                        || suspendedPayment.getSuspenseReason().trim().isEmpty())) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "SuspenseReason", "bad.request.data.invalid",
                    new Object[] { "Reason" });
        }

        if (suspendedPayment.getSuspenseReason() != null && !suspendedPayment.getSuspenseReason().trim().isEmpty()
                && Boolean.FALSE.equals(suspendedPayment.getHoldSuspenseIndicator())) {
            errors.rejectValue("holdSuspenseIndicator", "holdSuspense.invalid",
                    new Object[] { "Hold Suspense Indicator" }, BLANK_STRING);
        }

        if (errors != null && errors.getAllErrors() != null && !errors.getAllErrors().isEmpty()) {
            logger.error("Fields can't be null");
            return;
        }

        if ((suspendedPayment.getIdentifier() == null || suspendedPayment.getIdentifier().trim().isEmpty())
                && suspendedPayment.getPolicyNumber() != null && !suspendedPayment.getPolicyNumber().trim().isEmpty()) {
            suspendedPayment.setIdentifierType(BLANK_STRING);
        }
        suspendedPayment.setIdentifierType(formatIdentifierType(suspendedPayment.getIdentifierType()));

        if (suspendedPayment.getHoldSuspenseIndicator() == null || !suspendedPayment.getHoldSuspenseIndicator()) {
            suspendedPayment.setSuspenseReason(BLANK_STRING);
        } else {
            suspendedPayment.setSuspenseReason(formatReason(suspendedPayment.getSuspenseReason()));
        }

        validateIfUpdateAllowed(sequenceNumber, postedDate, batchId, user, distributionDate, suspendedPayment);
    }

    private String formatPolicySymbol(String policySymbol, String existingPolicySymbol) {
        if (policySymbol == null) {
            return existingPolicySymbol;
        }
        if (!policySymbol.trim().isEmpty()) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(policySymbol,
                    BusCodeTranslationType.POLICY_SYMBOL_TYPE.getValue(), execContext.getLanguage());
            if (busCdTranslation == null) {
                logger.debug("Support data not found for description = {}", policySymbol);
                throw new DataNotFoundException(SUPPORT_DATA_MISSING, new Object[] { policySymbol });
            } else {
                return busCdTranslation.getBusCdTranslationId().getBusCd().trim();
            }
        } else {
            return BLANK_STRING;
        }
    }

    private String formatIdentifierType(String identifierType) {
        if (identifierType != null && !identifierType.trim().isEmpty()) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(identifierType,
                    BusCodeTranslationType.BILLACCOUNTTYPE.getValue(), execContext.getLanguage());
            if (busCdTranslation == null) {
                logger.debug("Support data not found for description = {}", identifierType);
                throw new DataNotFoundException(SUPPORT_DATA_MISSING, new Object[] { identifierType });
            } else {
                return busCdTranslation.getBusCdTranslationId().getBusCd().trim();
            }
        } else {
            return BLANK_STRING;
        }
    }

    private String formatReason(String reason) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(reason,
                BusCodeTranslationType.DIRECT_TYPE.getValue(), execContext.getLanguage());
        if (busCdTranslation == null) {
            logger.debug("Support data not found for reason description = {}", reason);
            throw new DataNotFoundException(SUPPORT_DATA_MISSING, new Object[] { reason });
        } else {
            return busCdTranslation.getBusCdTranslationId().getBusCd().trim();
        }
    }

    private String formatPayableItemCode(String payableItem, String existingPayableItem) {
        if (payableItem == null) {
            return existingPayableItem;
        }
        if (!payableItem.trim().isEmpty()) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndParentCode(payableItem,
                    BusCodeTranslationParentCode.PAYABLE_ITEM.getValue(), execContext.getLanguage());
            if (busCdTranslation != null) {
                return busCdTranslation.getBusCdTranslationId().getBusCd();
            } else {
                throw new DataNotFoundException(SUPPORT_DATA_MISSING, new Object[] { "Payable Item" });
            }
        } else {
            return BLANK_STRING;
        }
    }

    private void validateIfUpdateAllowed(Short sequenceNumber, String postedDate, String batchId, String user,
            String distributionDate, SuspendedPayment suspendedPayment) {
        String userId = appSecurityServiceImpl.authenticateUser(user).getUserSequenceId();
        String currency = "";
        BilUnIdCash bilUnidentifiedCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(DateRoutine.dateTimeAsYYYYMMDD(distributionDate), sequenceNumber, batchId,
                        userId, DateRoutine.dateTimeAsYYYYMMDD(postedDate)))
                .orElse(null);
        if (bilUnidentifiedCash == null) {
            logger.debug("Payment not found in BIL_UNID_CASH");
            throw new DataNotFoundException("payment.does.not.exists");
        }
        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId, userId))
                .orElse(null);
        if (bilCashEntryTot != null) {
            currency = bilCashEntryTot.getCurrencyCd().trim();
        }

        if (!bilUnidentifiedCash.getBilDspTypeCd().equals(BilDspTypeCode.SUSPENDED.getValue())) {
            logger.debug("Only suspended row can be updated");
            throw new InvalidDataException("row.selected.does.not.qualify.for", new Object[] { "Update" });
        }
        suspendedPayment.setPolicySymbol(
                formatPolicySymbol(suspendedPayment.getPolicySymbol(), bilUnidentifiedCash.getPolSymbolCd()));
        suspendedPayment.setPayableItem(
                formatPayableItemCode(suspendedPayment.getPayableItem(), bilUnidentifiedCash.getBilPblItemCd()));

        if (suspendedPayment.getPolicyNumber() == null) {
            suspendedPayment.setPolicyNumber(bilUnidentifiedCash.getPolNbr());
        }
        if (suspendedPayment.getPaymentComments() == null) {
            suspendedPayment.setPaymentComments(bilUnidentifiedCash.getBilRctCmt());
        }
        if (suspendedPayment.getPaymentId() == null) {
            suspendedPayment.setPaymentId(bilUnidentifiedCash.getBilRctId());
        } else {
            BilCashEntry bilCashEntry = bilCashEntryRepository
                    .findById(new BilCashEntryId(DateRoutine.dateTimeAsYYYYMMDD(postedDate), batchId, userId,
                            bilUnidentifiedCash.getBilEntrySeqNbr()))
                    .orElse(null);
            if (bilCashEntry != null && !bilCashEntry.getBilPmtOrderId().trim().isEmpty()) {
                suspendedPayment.setPaymentId(bilUnidentifiedCash.getBilRctId());
            }
        }
        if (suspendedPayment.getAdditionalId() == null) {
            suspendedPayment.setAdditionalId(bilUnidentifiedCash.getBilAdditionalId());
        }
        if (suspendedPayment.getPaymentType() != null && !suspendedPayment.getPaymentType().trim().isEmpty()) {
            suspendedPayment.setPaymentType(
                    formatPaymentType(suspendedPayment.getPaymentType(), bilUnidentifiedCash.getBilCshEtrMthCd()));
        } else {
            suspendedPayment.setPaymentType(bilUnidentifiedCash.getBilRctTypeCd());
        }
        if (suspendedPayment.getPaymentType() == null || suspendedPayment.getPaymentType().trim().isEmpty()
                && !bilUnidentifiedCash.getBilRctTypeCd().trim().equals(BilReceiptTypeCode.EFT.getValue())
                && !bilUnidentifiedCash.getBilRctTypeCd().trim().equals(BilReceiptTypeCode.EFT_CREDIT_CARD.getValue())
                && !isDigitalWalletPayment(bilUnidentifiedCash.getBilRctTypeCd().trim())) {
            throw new InvalidDataException("payment.type.required");
        }
        if (suspendedPayment.getDueDate() == null
                || suspendedPayment.getDueDate().equals(DateRoutine.defaultDateTime())) {
            suspendedPayment.setDueDate(bilUnidentifiedCash.getBilAdjDueDt());
        } else if (suspendedPayment.getDueDate().isBefore(DateRoutine.defaultSinceDateTime())) {
            throw new InvalidDataException("invalid.date",
                    new Object[] { DateRoutine.dateTimeAsMMDDYYYYAsString(suspendedPayment.getDueDate()) });
        }

        validateIfEquityPayment(suspendedPayment.getPaymentType(), suspendedPayment);
        validateIfDownPayment(suspendedPayment.getPaymentType(), suspendedPayment);

        if (suspendedPayment.getIdentifierType().equals(AccountType.BILLACCOUNT.toString())) {
            validateForBillAccountDirection(suspendedPayment, bilUnidentifiedCash.getBilCshEtrMthCd(),
                    bilUnidentifiedCash.getAgencyNbr(), currency);
        }
        if (suspendedPayment.getIdentifierType().equals(AccountType.GROUP_ACCOUNT.toString())) {
            validateForGroupBillDirection(suspendedPayment, bilUnidentifiedCash.getBilCshEtrMthCd(), currency);
        }
        if (suspendedPayment.getIdentifierType().equals(AccountType.AGENCY_ACCOUNT.toString())) {
            validateForAgencyDirection(suspendedPayment, bilUnidentifiedCash.getBilCshEtrMthCd(), currency);
        }
        if (suspendedPayment.getPolicyNumber() != null && !suspendedPayment.getPolicyNumber().trim().isEmpty()) {
            validateForPolicyDirection(suspendedPayment, bilUnidentifiedCash.getBilCshEtrMthCd(),
                    bilUnidentifiedCash.getAgencyNbr(), currency);
        }
        if (suspendedPayment.getAdditionalId() != null && !suspendedPayment.getAdditionalId().trim().isEmpty()) {
            validateForAdditionalIdDirection(bilUnidentifiedCash.getBilCshEtrMthCd(),
                    suspendedPayment.getIdentifierType());
        }
        if (suspendedPayment.getBusinessGroup() != null && !suspendedPayment.getBusinessGroup().trim().isEmpty()) {
            suspendedPayment.setBusinessGroup(formatBusinessGroup(suspendedPayment.getBusinessGroup()));

        }
    }

    private String formatBusinessGroup(String bilBusGrpCd) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndTypeAndParentCode(
                bilBusGrpCd, BusCodeTranslationType.BANK_PROFILE_ATTR_SUB_TYPE.getValue(), execContext.getLanguage(),
                "ACT");
        if (busCdTranslation != null) {
            return busCdTranslation.getBusCdTranslationId().getBusCd().trim();
        } else {
            throw new DataNotFoundException("supportData.notfound",
                    new Object[] { bilBusGrpCd, BusCodeTranslationType.BANK_PROFILE_ATTR_SUB_TYPE.getValue() });
        }

    }

    private void validateForBillAccountDirection(SuspendedPayment suspendedPayment, char cashEntryMethodCode,
            String agencyNumber, String currency) {
        if (suspendedPayment.getAdditionalId() != null && !suspendedPayment.getAdditionalId().trim().isEmpty()) {
            logger.error("Additional Id direction to policy number/Account number not allowed.");
            throw new InvalidDataException("additionalId.direction.not.allowed");
        }
        BilAccount bilAccount = bilAccountRepository.findByAccountNumber(suspendedPayment.getIdentifier());
        if (bilAccount != null) {
            if (!multiCurrencyService.compareCurrency(currency, bilAccount.getCurrencyCode().trim())) {
                logger.error(CURRENCY_NOT_MATCHING);
                throw new InvalidDataException(CURRENCY_DIRECTION_NOT_ALLOWED);
            }
            String billType = getBillingType(bilAccount.getBillTypeCd());
            if (billType.equals(BusCodeTranslationType.BTG.getValue())) {
                validateCashEntryMethod(cashEntryMethodCode, AccountType.AGENCYBILL.toString());
            }
            if (cashEntryMethodCode == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()) {
                Short count = bilPolicyTermRepository.findPolicyByAccountId(suspendedPayment.getIdentifier(), CHAR_Y);
                if (count != null && count < SHORT_ZERO) {
                    validateAgencySweepPayment(agencyNumber, suspendedPayment.getIdentifier());
                }
            }
        }
    }

    private void validateForGroupBillDirection(SuspendedPayment suspendedPayment, char cashEntryMethodCode,
            String currency) {
        if (suspendedPayment.getIdentifier() != null) {
            BilThirdParty bilThirdParty = bilThirdPartyRepository.findByBilTtyNbr(suspendedPayment.getIdentifier());
            if (bilThirdParty != null
                    && !multiCurrencyService.compareCurrency(currency, bilThirdParty.getCurrencyCd().trim())) {
                logger.error(CURRENCY_NOT_MATCHING);
                throw new InvalidDataException(CURRENCY_DIRECTION_NOT_ALLOWED);
            }
        }
        if (suspendedPayment.getPolicyNumber() != null && !suspendedPayment.getPolicyNumber().trim().isEmpty()
                || suspendedPayment.getPayableItem() != null && !suspendedPayment.getPayableItem().trim().isEmpty()) {
            logger.error("Additional direction to policy number/Payable item not allowed.");
            throw new InvalidDataException(POLICY_DIRECTION_NOT_ALLOWED);
        }
        validateCashEntryMethod(cashEntryMethodCode, AccountType.GROUP_ACCOUNT.toString());

        if (suspendedPayment.getAdditionalId() != null && !suspendedPayment.getAdditionalId().trim().isEmpty()) {
            BilAccount bilAccount = bilAccountRepository
                    .findTopByAdditionalIdOrderByAdditionalIdDesc(suspendedPayment.getAdditionalId());
            if (bilAccount != null) {
                if (!multiCurrencyService.compareCurrency(currency, bilAccount.getCurrencyCode().trim())) {
                    logger.error(CURRENCY_NOT_MATCHING);
                    throw new InvalidDataException(CURRENCY_DIRECTION_NOT_ALLOWED);
                }
                String billType = getBillingType(bilAccount.getBillTypeCd());
                if (!billType.equals(BusCodeTranslationType.BTP.getValue())
                        || !bilAccount.getThirdPartyIdentifier().trim().equals(suspendedPayment.getIdentifier())) {
                    logger.error("Additional Id was found but the associated Third party was different.");
                    throw new InvalidDataException("additional.id.did.not.match");
                }
            }
        }
    }

    private void validateForAgencyDirection(SuspendedPayment suspendedPayment, char cashEntryMethodCode,
            String currency) {
        if (suspendedPayment.getIdentifier() != null) {
            BilAgent bilAgent = bilAgentRepository.findByBilAgtActNbr(suspendedPayment.getIdentifier());
            if (bilAgent != null && !multiCurrencyService.compareCurrency(currency, bilAgent.getCurrencyCd().trim())) {
                logger.error(CURRENCY_NOT_MATCHING);
                throw new InvalidDataException(CURRENCY_DIRECTION_NOT_ALLOWED);
            }
        }
        if (suspendedPayment.getPolicyNumber() != null && !suspendedPayment.getPolicyNumber().trim().isEmpty()
                || suspendedPayment.getPayableItem() != null && !suspendedPayment.getPayableItem().trim().isEmpty()) {
            logger.error("Additional direction to policy number/Payable item not allowed.");
            throw new InvalidDataException(POLICY_DIRECTION_NOT_ALLOWED);
        }
        validateCashEntryMethod(cashEntryMethodCode, AccountType.AGENCY_ACCOUNT.toString());
    }

    private String getBillingType(String billTypeCd) {
        List<BusCdTranslation> busCdTranslationList = busCdTranslationRepository
                .findAllByCodeAndTypeList(
                        billTypeCd, Arrays.asList(BusCodeTranslationType.BTY.getValue(),
                                BusCodeTranslationType.BTP.getValue(), BusCodeTranslationType.BTG.getValue()),
                        execContext.getLanguage());
        if (busCdTranslationList == null || busCdTranslationList.isEmpty()) {
            throw new DataNotFoundException(SUPPORT_DATA_MISSING, new Object[] { billTypeCd });
        }
        return busCdTranslationList.get(0).getBusCdTranslationId().getBusType().trim();
    }

    public void validateCashEntryMethod(char cashEntryMethodCode, String identifier) {
        switch (CashEntryMethod.getEnumValue(cashEntryMethodCode)) {
        case AGENT_CREDIT_CARD:
        case CREDIT_CARD:
            logger.error("Unidentified credit card payments cannot be directed to {}", identifier);
            throw new InvalidDataException(DIRECTION_NOT_ALLOWED,
                    new Object[] { " Unidentified Credit Card", identifier });
        case BANK_ACCOUNT:
            logger.error("Bank Account payments cannot be directed to {}", identifier);
            throw new InvalidDataException(DIRECTION_NOT_ALLOWED, new Object[] { " Bank Account", identifier });
        case AGENCY_SWEEP:
            logger.error("Agency Sweep payments cannot be directed to {}", identifier);
            throw new InvalidDataException(DIRECTION_NOT_ALLOWED, new Object[] { " Agency Sweep", identifier });
        default:
            if (isDigitalWalletPayment(cashEntryMethodCode)) {
                logger.error("Wallet payments cannot be directed to {}", identifier);
                throw new InvalidDataException(DIRECTION_NOT_ALLOWED, new Object[] { " Digital Wallet", identifier });
            }
            break;
        }
    }

    private boolean isDigitalWalletPayment(Character cashEntryMethodCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(
                String.valueOf(cashEntryMethodCode), BusCodeTranslationType.DIGITAL_WALLET_TYPE.getValue(),
                execContext.getLanguage());

        return busCdTranslation != null ? Boolean.TRUE : Boolean.FALSE;
    }

    private void validateForPolicyDirection(SuspendedPayment suspendedPayment, char cashEntryMethodCode,
            String agencyNumber, String currency) {
        String billType = BLANK_STRING;
        if (suspendedPayment.getIdentifierType().equals(AccountType.GROUP_ACCOUNT.toString())
                || suspendedPayment.getIdentifierType().equals(AccountType.AGENCY_ACCOUNT.toString())
                || suspendedPayment.getAdditionalId() != null && !suspendedPayment.getAdditionalId().trim().isEmpty()) {
            logger.error("When directing to a Policy/Agreement, additional direction is not allowed");
            throw new InvalidDataException(POLICY_DIRECTION_NOT_ALLOWED);
        }
        List<BilPolicy> bilPolicy = null;
        if (suspendedPayment.getPolicySymbol() == null || suspendedPayment.getPolicySymbol().trim().isEmpty()) {
            bilPolicy = bilPolicyRepository.findByPolicyNumber(suspendedPayment.getPolicyNumber());
        } else {
            bilPolicy = bilPolicyRepository.findBilPolicyByPolicyNumber(suspendedPayment.getPolicyNumber(),
                    suspendedPayment.getPolicySymbol());
        }

        if (bilPolicy != null && !bilPolicy.isEmpty() && bilPolicy.size() == 1) {
            BilAccount bilAccount = bilAccountRepository.findById(bilPolicy.get(0).getBillPolicyId().getBilAccountId())
                    .orElse(null);
            if (bilAccount != null
                    && !multiCurrencyService.compareCurrency(currency, bilAccount.getCurrencyCode().trim())) {
                logger.error(CURRENCY_NOT_MATCHING);
                throw new InvalidDataException(CURRENCY_DIRECTION_NOT_ALLOWED);
            }
            if (bilPolicy.get(0).getBilTypeCd().trim().compareTo(BLANK_STRING) != 0) {
                billType = getBillingType(bilPolicy.get(0).getBilTypeCd());
            }
            if (billType.equals(BusCodeTranslationType.BTG.getValue())) {
                logger.error("Unidentified cash cannot be directed to an policy associated with agency bill account.");
                throw new InvalidDataException(DIRECTION_NOT_ALLOWED,
                        new Object[] { " Unidentified Cash", "Agency Bill" });
            }
            if (cashEntryMethodCode == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()) {
                validateAgencySweepPayment(agencyNumber, suspendedPayment.getIdentifier());
            }
        }
    }

    private void validateAgencySweepPayment(String agencyNumber, String identifier) {
        String ruleText = checkAgencySweepProcessing();
        if (ruleText.isEmpty()) {
            throw new InvalidDataException(AGENCY_SWEEP_PROCESS_FAILED);
        }
        Boolean isContract = ruleText.charAt(6) == CHAR_Y;
        String agencyClient = getAgencyClient(agencyNumber, isContract);
        CltObjRelation clientObjectRealtion = cltObjRelationRepository.findClientRelation(agencyClient, identifier,
                AGENCY);
        if (clientObjectRealtion == null) {
            logger.error(
                    "account number/policy must be associated with agency client for agency sweep payment direction");
            throw new InvalidDataException("relation.not.found");
        }
    }

    private String getAgencyClient(String agencyNumber, Boolean isContract) {
        String agencyClient;

        if (Boolean.TRUE.equals(isContract)) {
            CcmAgency ccmAgency = ccmAgencyRepository.findCagAgcCltIdByCagAgencyNbr(agencyNumber);
            agencyClient = ccmAgency.getCagAgcCltId();
        } else {
            List<ClientReference> clientReference = clientReferenceRepository
                    .findClientReferenceIdClientIdByCirfRefIdAndRefTypCd(agencyNumber, getReferenceTypeCodeList());
            agencyClient = clientReference.get(SHORT_ZERO).getClientReferenceId().getClientId();
        }
        if (agencyClient.isEmpty()) {
            throw new InvalidDataException(AGENCY_SWEEP_PROCESS_FAILED);
        }
        return agencyClient;
    }

    public String checkAgencySweepProcessing() {
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(BilRules.AGENCY_SWEEP_PROCESSING.getValue(),
                BLANK_STRING, BLANK_STRING, BLANK_STRING, execContext.getApplicationDate(),
                execContext.getApplicationDate());
        if (bilRulesUct == null || bilRulesUct.getBrtRuleCd() != CHAR_Y
                || bilRulesUct.getBrtParmListTxt().trim().isEmpty()) {
            throw new InvalidDataException(AGENCY_SWEEP_PROCESS_FAILED);
        }
        return bilRulesUct.getBrtParmListTxt().trim();
    }

    private void validateForAdditionalIdDirection(char cashEntryMethodCode, String identifierType) {
        validateCashEntryMethod(cashEntryMethodCode, ADDITIONAL_ID);
        if (!identifierType.equals(AccountType.GROUP_ACCOUNT.toString())) {
            logger.error("Group Account Number direction is required when directing to an Additional ID.");
            throw new InvalidDataException("group.direction.required");
        }
    }

    private String formatPaymentType(String paymentType, char cashEntryMethodCode) {
        BusCdTranslation busCdTranslation = null;
        switch (CashEntryMethod.getEnumValue(cashEntryMethodCode)) {
        case CREDIT_CARD:
            busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType,
                    BusCodeTranslationType.CREDIT_CARD_PAYMENT.getValue(), execContext.getLanguage());
            break;
        case AGENT_CREDIT_CARD:
            busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType,
                    BilDesReasonType.GPY.getValue(), execContext.getLanguage());
            break;
        case EFT:
        case EFT_CREDIT_CARD:
            busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType, BLANK_STRING,
                    execContext.getLanguage());
            if (busCdTranslation == null) {
                busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType,
                        BilDesReasonType.EFT_WALLET_PAYMENT_TYPE.getValue(), execContext.getLanguage());
            }
            break;
        case ONE_TIME_ACH:
            busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType,
                    BilDesReasonType.ACH.getValue(), execContext.getLanguage());
            break;
        case BANK_ACCOUNT:
            busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType,
                    BusCodeTranslationType.BANK_ACCOUNT_PAYMENT.getValue(), execContext.getLanguage());
            break;
        case AGENCY_SWEEP:
            busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType,
                    BilDesReasonType.ASW.getValue(), execContext.getLanguage());
            break;
        case VENDOR_COLLECTIONS:
            busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType,
                    BilDesReasonType.COL.getValue(), execContext.getLanguage());
            break;
        default:
            busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType,
                    BilDesReasonType.WALLET_PAYMENT_TYPE.getValue(), execContext.getLanguage());
            if (busCdTranslation == null) {
                busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType,
                        BilDesReasonType.PYT.getValue(), execContext.getLanguage());
                if (busCdTranslation == null) {
                    busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType,
                            BilDesReasonType.PYC.getValue(), execContext.getLanguage());
                    if (busCdTranslation == null) {
                        busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(paymentType,
                                BilDesReasonType.COL.getValue(), execContext.getLanguage());
                    }
                }
            }

            break;
        }
        if (busCdTranslation == null) {
            throw new InvalidDataException("payment.type.invalid");
        }
        return busCdTranslation.getBusCdTranslationId().getBusCd().trim();
    }

    private void validateIfEquityPayment(String bilReceiptTypeCode, SuspendedPayment suspendedPayment) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(bilReceiptTypeCode,
                BusCodeTranslationType.EQUITY.getValue(), execContext.getLanguage());
        if (busCdTranslation != null) {
            if ((suspendedPayment.getPolicyNumber() == null || suspendedPayment.getPolicyNumber().trim().isEmpty())
                    && (suspendedPayment.getPaymentType() != null
                            && !suspendedPayment.getPaymentType().trim().isEmpty())) {
                logger.error("Policy/Agreement is required for equity payment types.");
                throw new InvalidDataException(EQUITY_PAYMENT_INVALID, new Object[] { "Policy/Agreement" });
            }
            if (suspendedPayment.getDueDate() != null && !suspendedPayment.getDueDate().toLocalDate()
                    .equals(DateRoutine.defaultDateTime().toLocalDate())) {
                logger.error("Equity payments cannot be directed to an Account/Due Date.");
                throw new InvalidDataException(EQUITY_PAYMENT_INVALID, new Object[] { "Due date" });
            }
            if (suspendedPayment.getPayableItem() != null && !suspendedPayment.getPayableItem().trim().isEmpty()) {
                logger.error("Equity payments cannot be directed to  Payable Item.");
                throw new InvalidDataException(EQUITY_PAYMENT_INVALID, new Object[] { "Payable item" });
            }
            if (suspendedPayment.getIdentifierType().equals(AccountType.AGENCY_ACCOUNT.toString())
                    || suspendedPayment.getIdentifierType().equals(AccountType.GROUP_ACCOUNT.toString())) {
                logger.error("An equity payment type is not valid for Group  or Agency types.");
                throw new InvalidDataException(EQUITY_PAYMENT_INVALID, new Object[] { "Group /Agency " });
            }
        }
    }

    private void validateIfDownPayment(String paymentType, SuspendedPayment suspendedPayment) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(paymentType,
                BusCodeTranslationType.DOWN_PAYMENT.getValue(), execContext.getLanguage());
        if (busCodeTranslation != null) {
            if (!suspendedPayment.getIdentifierType().trim().equals(AccountType.BILLACCOUNT.toString())) {
                logger.error("A down payment receipt can only be directed to a bill account.");
                throw new InvalidDataException("down.payment.invalid");
            }
            if (suspendedPayment.getIdentifier() == null || suspendedPayment.getIdentifier().trim().isEmpty()
                    || (suspendedPayment.getDueDate() != null && !suspendedPayment.getDueDate().toLocalDate()
                            .equals(DateRoutine.defaultDateTime().toLocalDate()))
                    || (suspendedPayment.getPolicyNumber() != null
                            && !suspendedPayment.getPolicyNumber().trim().isEmpty())) {
                logger.error("A down payment receipt can only be directed to a bill account.");
                throw new InvalidDataException("down.payment.invalid");
            }
        }
    }

    private boolean isDigitalWalletPayment(String paymentType) {

        return busCdTranslationRepository.existsById(new BusCdTranslationId(paymentType,
                BilDesReasonType.EFT_WALLET_PAYMENT_TYPE.getValue(), execContext.getLanguage(), SEPARATOR_BLANK));
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public void setSequenceNumber(Short sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setDistributionDate(String distributionDate) {
        this.distributionDate = distributionDate;
    }

    private ArrayList<String> getReferenceTypeCodeList() {
        ArrayList<String> referenceTypeCdList = new ArrayList<>();
        referenceTypeCdList.add("ANBR");
        referenceTypeCdList.add("AGYN");
        return referenceTypeCdList;
    }
}
