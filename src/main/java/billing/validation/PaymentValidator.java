package billing.validation;

import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_G;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_W;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_Z;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import application.defaults.service.DefaultDataService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilAgent;
import billing.data.entity.BilEftPlan;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilThirdParty;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilAgentRepository;
import billing.data.repository.BilEftPlanRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.model.Payment;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.AccountTypeCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilRules;
import billing.utils.BillingConstants.BillingMethod;
import billing.utils.BillingConstants.BusCodeTranslationParentCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.PaymentType;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.data.entity.id.BusCdTranslationId;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.CommonConstants;
import core.utils.DateRoutine;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PaymentValidator implements Validator {

    private static final String RECURRING_TYPE = "RECUR";

    @Autowired
    private PaymentCreditCardValidator paymentCreditCardValidator;

    @Autowired
    private PaymentBankAccountValidator paymentBankAccountValidator;

    @Autowired
    private AgencySweepPaymentValidator agencySweepPaymentValidator;

    @Autowired
    private PaymentWalletCardValidator paymentWalletCardValidator;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    private BilAgentRepository bilAgentRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilEftPlanRepository bilEftPlanRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private DefaultDataService defaultDataService;

    @Autowired
    private BankAccountValidator bankAccountValidator;

    @Autowired
    private AgencySweepValidator agencySweepValidator;

    @Autowired
    private CreditCardValidator creditCardValidator;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Value("${autopayment.integration}")
    private String autoPaymentIntegration;

    @Value("${onetimepayment.integration}")
    private String oneTimePaymentIntegration;

    @Value("${agencysweeppayment.integration}")
    private String agencyPaymentIntegration;

    @Value("${paymentIntegration.provider}")
    private String providerIntegration;

    private boolean isPaymentIntegration = false;
    private boolean isAutoPaymentIntegration = false;
    private boolean isOneTimePaymentIntegration = false;
    private boolean isAgencyPaymentIntegration = false;
    private static final String BAD_REQUEST_DATA_INVALID = "bad.request.data.invalid";
    private static final String BAD_REQUEST_MISSING_PARAMETER = "bad.request.missing.parameter";
    private static final String PAYMENT_METHOD = "paymentMethod";
    private static final String PAYMENT_TYPE = "paymentType";
    private static final String COLLECTION_METHOD = "collectionMethod";
    private static final String IDENTIFIER = "identifier";
    private static final String BAD_ACCOUNT_DATA = "bad.account.data";
    private static final String NO_ACCOUNT_EXISTS = "no.account.exists";
    private static final String IDENTIFIER_TYPE = "identifierType";
    private static final String PAYMENT_AMOUNT = "paymentAmount";

    @Override
    public boolean supports(Class<?> clazz) {
        return Payment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Payment payment = (Payment) target;

        String identifierType = readBusCdTranslation(payment);
        if (StringUtils.isNotBlank(providerIntegration)) {
            isPaymentIntegration = true;
        }
        checkPaymentIntegration(identifierType, payment);

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, PAYMENT_METHOD, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { PAYMENT_METHOD });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, PAYMENT_TYPE, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { PAYMENT_TYPE });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, PAYMENT_AMOUNT, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { PAYMENT_AMOUNT });

        validatePaymentAmount(payment, errors);

        if (!errors.getAllErrors().isEmpty()) {
            return;
        }

        format(payment, errors);

        validateData(errors, payment, identifierType);

    }

    private void validateData(Errors errors, Payment payment, String identifierType) {
        boolean isPaymentTypeExist = false;
        if (PaymentType.CREDIT_CARD.getValue().equalsIgnoreCase(payment.getPaymentMethod())) {
            if (isOneTimePaymentIntegration || isAutoPaymentIntegration) {
                ValidationUtils.invokeValidator(paymentCreditCardValidator, payment, errors);
            } else {
                ValidationUtils.invokeValidator(creditCardValidator, payment, errors);
            }
            isPaymentTypeExist = true;
        }

        if (PaymentType.BANK_ACCOUNT.getValue().equalsIgnoreCase(payment.getPaymentMethod())) {
            if (isOneTimePaymentIntegration || isAutoPaymentIntegration) {
                ValidationUtils.invokeValidator(paymentBankAccountValidator, payment, errors);
            } else {
                ValidationUtils.invokeValidator(bankAccountValidator, payment, errors);
            }
            isPaymentTypeExist = true;
        }

        if (PaymentType.AGENCY_SWEEP.getValue().equalsIgnoreCase(payment.getPaymentMethod())) {
            if (payment.getIdentifierType().equalsIgnoreCase(AccountType.GROUP_ACCOUNT.name())) {
                errors.rejectValue(IDENTIFIER, BAD_ACCOUNT_DATA, new Object[] { IDENTIFIER },
                        CommonConstants.BLANK_STRING);
            }
            if (isAgencyPaymentIntegration) {
                ValidationUtils.invokeValidator(agencySweepPaymentValidator, payment, errors);
            } else {
                ValidationUtils.invokeValidator(agencySweepValidator, payment, errors);
            }
            isPaymentTypeExist = true;
        }

        if ((!isPaymentTypeExist) && (isOneTimePaymentIntegration || isAutoPaymentIntegration)) {
            ValidationUtils.invokeValidator(paymentWalletCardValidator, payment, errors);
        }

        if (identifierType.equalsIgnoreCase(AccountType.AGENCY_ACCOUNT.toString())) {
            errors.rejectValue(IDENTIFIER, BAD_ACCOUNT_DATA, new Object[] { IDENTIFIER }, CommonConstants.BLANK_STRING);
        }

        if (!payment.getIdentifier().trim().equals(BLANK_STRING)) {
            validateAccountDetails(payment, errors, identifierType);

        }

        if (!payment.getPolicyNumber().trim().equals(BLANK_STRING)) {
            validatePolicyDetails(payment, errors);
        }
    }

    private void validatePolicyDetails(Payment payment, Errors errors) {

        if (payment.getAccountId() != null) {
            List<String> accountIdList = bilPolicyTermRepository.findAccountByPolicy(payment.getPolicyNumber(),
                    payment.getPolicySymbol(), getBillPolStatusCdList(), getBilIssueIndList(), payment.getAccountId());
            if (accountIdList.isEmpty()) {
                payment.setIsUnidentified(true);
                payment.setAccountId(BLANK_STRING);
            }
        } else {
            List<String> accountIdList = bilPolicyTermRepository.findAccountByPolicy(payment.getPolicyNumber(),
                    payment.getPolicySymbol(), getBillPolStatusCdList(), getBilIssueIndList());
            if (accountIdList.isEmpty() || accountIdList.size() > 1) {
                payment.setIsUnidentified(true);
                payment.setAccountId(BLANK_STRING);
            } else {
                String accountId = accountIdList.get(SHORT_ZERO);
                BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
                if (bilAccount != null) {
                    payment.setAccountId(bilAccount.getAccountId());
                    payment.setIdentifierType(AccountType.BILLACCOUNT.toString());
                    if (!bilAccount.getAgentAccountNumber().trim().equals(BLANK_STRING)) {
                        errors.rejectValue("identifier", "bad.account.data", new Object[] { "identifier" },
                                CommonConstants.BLANK_STRING);
                    }

                    payment.setIsUnidentified(false);
                } else {
                    payment.setIsUnidentified(true);
                    payment.setAccountId(BLANK_STRING);
                }
            }
        }

    }

    private List<Character> getBilIssueIndList() {
        List<Character> bilIssueIndList = new ArrayList<>();
        bilIssueIndList.add(CHAR_Y);
        bilIssueIndList.add(CHAR_Z);
        return bilIssueIndList;
    }

    private List<Character> getBillPolStatusCdList() {
        List<Character> billPolStatusCdList = new ArrayList<>();
        billPolStatusCdList.add(CHAR_W);
        billPolStatusCdList.add(CHAR_T);
        billPolStatusCdList.add(CHAR_D);
        billPolStatusCdList.add(CHAR_G);
        return billPolStatusCdList;
    }

    private void validateAccountDetails(Payment payment, Errors errors, String identifierType) {

        if (identifierType.equals(AccountType.BILLACCOUNT.toString())) {
            BilAccount bilAccount = bilAccountRepository.findByAccountNumber(payment.getIdentifier());
            if (bilAccount != null) {
                payment.setAccountId(bilAccount.getAccountId());
                payment.setIsUnidentified(false);
                payment.setIdentifierType(identifierType);
                if (!bilAccount.getAgentAccountNumber().trim().equals(BLANK_STRING)) {
                    errors.rejectValue(IDENTIFIER, BAD_ACCOUNT_DATA, new Object[] { IDENTIFIER },
                            CommonConstants.BLANK_STRING);
                }
                if (Boolean.TRUE.equals(payment.getIsRecurring())
                        && !bilAccount.getCollectionMethod().trim().equals(BillingMethod.DI.name())) {
                    if (bilAccount.getCollectionMethod().trim().equals(BillingMethod.EFT.name())
                            || bilAccount.getCollectionMethod().trim().equals(BillingMethod.ECC.name())
                            || bilAccount.getCollectionMethod().trim().equals(BillingMethod.ELECTRONIC_WALLET.name())) {
                        errors.rejectValue(COLLECTION_METHOD, "bad.collection.data", new Object[] { COLLECTION_METHOD },
                                CommonConstants.BLANK_STRING);
                    } else {
                        errors.rejectValue(COLLECTION_METHOD, BAD_ACCOUNT_DATA, new Object[] { COLLECTION_METHOD },
                                CommonConstants.BLANK_STRING);
                    }

                }
                return;
            }
        } else if (identifierType.equals(AccountType.GROUP_ACCOUNT.toString())) {
            BilThirdParty bilThirdParty = bilThirdPartyRepository.findByBilTtyNbr(payment.getIdentifier());
            if (bilThirdParty != null) {
                payment.setAccountId(bilThirdParty.getBillThirdPartyId());
                payment.setIsUnidentified(false);
                payment.setIdentifierType(identifierType);
                return;
            }
        } else if (identifierType.equals(AccountType.AGENCY_ACCOUNT.toString())) {
            BilAgent bilAgent = bilAgentRepository.findByBilAgtActNbr(payment.getIdentifier());
            if (bilAgent != null) {
                payment.setAccountId(bilAgent.getBilAgtActId());
                payment.setIsUnidentified(false);
                payment.setIdentifierType(identifierType);
                errors.rejectValue(IDENTIFIER, BAD_ACCOUNT_DATA, new Object[] { IDENTIFIER },
                        CommonConstants.BLANK_STRING);
                return;
            }
        }

        payment.setAccountId(BLANK_STRING);
        payment.setIsUnidentified(true);
        payment.setIdentifierType(identifierType);

    }

    private void validatePaymentAmount(Payment payment, Errors errors) {
        if (payment.getPaymentAmount() <= 0) {
            errors.rejectValue(PAYMENT_AMOUNT, "paymentAmount.invalid");
        }
    }

    private void format(Payment payment, Errors errors) {
        formatBillTypeCode(payment);
        formatBillAccountNumber(payment, errors);
        formatSymbolCode(payment, errors);
        formatPolicyNumber(payment, errors);
        formatPaymentMethod(payment, errors);
        formatPaymentType(payment, errors);
        formatPaymentId(payment);
        formatPaymentComments(payment);
        formatSuspenReason(payment, errors);
        formatPayableItem(payment, errors);
        formatTransactionId(payment);
        formatAdditionalId(payment);
        formatAdditonalValidations(payment);
        formatRecurringPayment(payment, errors);
        formatBankHolderName(payment);
        formatCardHolderName(payment);
        formatBusinesGroup(payment);
    }

    private void formatBusinesGroup(Payment payment) {
        if (payment.getBusinessGroup() == null) {
            payment.setBusinessGroup(SEPARATOR_BLANK);
        }
    }

    private void formatCardHolderName(Payment payment) {
        if (payment.getCreditCardHolderName() == null) {
            payment.setCreditCardHolderName(SEPARATOR_BLANK);
        }

    }

    private void formatBankHolderName(Payment payment) {
        if (payment.getBankHolderName() == null) {
            payment.setBankHolderName(SEPARATOR_BLANK);
        }

    }

    private void formatTransactionId(Payment payment) {
        if (payment.getTransactionId() == null) {
            payment.setTransactionId(SEPARATOR_BLANK);
        }
    }

    private void formatRecurringPayment(Payment payment, Errors errors) {
        if (payment.getIsRecurring() == null) {
            payment.setIsRecurring(false);
        }
        if (Boolean.TRUE.equals(payment.getIsRecurring())) {
            if (PaymentType.CREDIT_CARD.getValue().equalsIgnoreCase(payment.getPaymentMethod())) {
                payment.setCollectionMethod(BillingMethod.ECC.name());
            } else if (PaymentType.BANK_ACCOUNT.getValue().equalsIgnoreCase(payment.getPaymentMethod())) {
                payment.setCollectionMethod(BillingMethod.EFT.name());
            } else {
                payment.setCollectionMethod(BillingMethod.ELECTRONIC_WALLET.toString());
            }

            if (payment.getCollectionType() == null) {

                String collectionType = defaultDataService.getDefaultData("UserCollectionTypeProfile", "CollectionType",
                        payment.getPaymentMethod());

                if (!bilEftPlanRepository.existsById(collectionType)) {
                    throw new InvalidDataException("collectionType.invalid");
                }
                payment.setCollectionType(collectionType);
            } else {
                short fileTypeCode = 0;
                BilAccount bilAccount = bilAccountRepository.findById(payment.getAccountId()).orElse(null);
                if (bilAccount == null) {
                    throw new DataNotFoundException(NO_ACCOUNT_EXISTS, new Object[] { payment.getIdentifier() });
                }

                if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {

                    if (isAutoPaymentIntegration) {
                        fileTypeCode = getFileTypeCodeFromBilRuleUct(execContext.getApplicationDate());
                        if (!payment.getPaymentType().equals(BLANK_STRING)) {
                            fileTypeCode = determineFileTypeCode(payment.getPaymentType(),
                                    BilDesReasonType.ONE_CC.getValue(), BilDesReasonType.CPY.getValue());
                        }
                        fileTypeCode = readRecurringFileType(fileTypeCode, BillingMethod.ECC.toString(),
                                bilAccount.getBillTypeCd().trim());
                    } else {
                        BusCdTranslation busCdTranslation = busCdTranslationRepository
                                .findById(new BusCdTranslationId(BillingMethod.ECC.toString(), RECURRING_TYPE,
                                        execContext.getLanguage(), bilAccount.getBillTypeCd().trim()))
                                .orElse(null);
                        if (busCdTranslation != null) {
                            fileTypeCode = Short.valueOf(busCdTranslation.getBusDescription().trim());
                        } else {
                            fileTypeCode = getFileTypeCodeFromBilRuleUct(execContext.getApplicationDate());
                            if (!payment.getPaymentType().equals(BLANK_STRING)) {
                                fileTypeCode = determineFileTypeCode(payment.getPaymentType(),
                                        BilDesReasonType.ONE_CC.getValue(), BilDesReasonType.CPY.getValue());
                            }
                        }
                    }

                } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {

                    if (isAutoPaymentIntegration) {
                        fileTypeCode = determineFileTypeCode(payment.getPaymentType(), BilDesReasonType.FTC.getValue(),
                                BilDesReasonType.ONE_XA.getValue());

                        fileTypeCode = readRecurringFileType(fileTypeCode, BillingMethod.EFT.toString(),
                                bilAccount.getBillTypeCd().trim());
                    } else {
                        BusCdTranslation busCdTranslation = busCdTranslationRepository
                                .findById(new BusCdTranslationId(BillingMethod.EFT.toString(), RECURRING_TYPE,
                                        execContext.getLanguage(), bilAccount.getBillTypeCd().trim()))
                                .orElse(null);
                        if (busCdTranslation != null) {
                            fileTypeCode = Short.valueOf(busCdTranslation.getBusDescription().trim());
                        } else {
                            fileTypeCode = determineFileTypeCode(BLANK_STRING, BilDesReasonType.FTC.getValue(),
                                    BilDesReasonType.ONE_XA.getValue());
                        }
                    }

                } else {
                    if (isAutoPaymentIntegration) {
                        BusCdTranslation busCdTranslation = busCdTranslationRepository
                                .findById(new BusCdTranslationId(BillingMethod.ELECTRONIC_WALLET.toString(),
                                        SEPARATOR_BLANK, execContext.getLanguage(), payment.getPaymentType()))
                                .orElse(null);
                        if (busCdTranslation != null) {
                            fileTypeCode = Short.valueOf(busCdTranslation.getBusDescription().trim());
                        }
                    } else {
                        errors.rejectValue("recurring", "recurring.not.allowed");
                    }
                }

                List<BilEftPlan> bilEftPlanList = bilEftPlanRepository.findByCollectionPlan(payment.getCollectionType(),
                        fileTypeCode);
                if (bilEftPlanList == null || bilEftPlanList.isEmpty()) {
                    throw new InvalidDataException("collectionType.invalid");
                }

                payment.setCollectionType(bilEftPlanList.get(SHORT_ZERO).getBilCollectionPln());
            }

        }

    }

    private void formatAdditonalValidations(Payment payment) {

        if (payment.getDueDate() != null) {
            if (validateIfEquityPayment(payment.getPaymentType())) {
                throw new InvalidDataException("equity.payment.invalid", new Object[] { "Due date" });
            } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())
                    && validateIfDownPayment(payment.getPaymentType())) {
                throw new InvalidDataException("downpayment.invalid");
            }
        } else {
            payment.setDueDate(DateRoutine.defaultDateTime());
        }

    }

    private boolean validateIfEquityPayment(String paymentType) {
        return busCdTranslationRepository.findByCodeAndType(paymentType, BilDesReasonType.EQT.getValue(),
                execContext.getLanguage()) != null;
    }

    private boolean validateIfDownPayment(String paymentType) {
        return busCdTranslationRepository.findByCodeAndType(paymentType, BusCodeTranslationType.DOWN_PAYMENT.getValue(),
                execContext.getLanguage()) != null;
    }

    private void formatAdditionalId(Payment payment) {
        if (payment.getAdditionalId() == null) {
            payment.setAdditionalId(BLANK_STRING);
        }

    }

    private void formatSuspenReason(Payment payment, Errors errors) {
        if (payment.getSuspenseReason() != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    payment.getSuspenseReason().trim(), BusCodeTranslationType.DIRECT_TYPE.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                payment.setSuspenseReason(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue("suspenseReason", BAD_REQUEST_DATA_INVALID, new Object[] { "suspenseReason" },
                        CommonConstants.BLANK_STRING);
            }
        } else {
            payment.setSuspenseReason(BLANK_STRING);
        }
    }

    private void formatPolicyNumber(Payment payment, Errors errors) {
        if (payment.getPolicyNumber() == null) {
            payment.setPolicyNumber(SEPARATOR_BLANK);
        }

        if (!payment.getPolicySymbol().trim().equalsIgnoreCase(BLANK_STRING)
                && payment.getPolicyNumber().trim().equals(BLANK_STRING)) {
            errors.rejectValue("policy Number", "bad.policy.number.data", new Object[] { "policy Number" },
                    CommonConstants.BLANK_STRING);
        }

    }

    private void formatSymbolCode(Payment payment, Errors errors) {
        if (payment.getPolicySymbol() != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository
                    .findByDescriptionAndType(payment.getPolicySymbol().trim(), "SYM", execContext.getLanguage());
            if (busCdTranslation != null) {
                payment.setPolicySymbol(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue("policySymbol", BAD_REQUEST_DATA_INVALID, new Object[] { "policySymbol" },
                        CommonConstants.BLANK_STRING);
            }
        } else {
            payment.setPolicySymbol(SEPARATOR_BLANK);
        }

    }

    private void formatBillAccountNumber(Payment payment, Errors errors) {
        if (payment.getIdentifier() == null) {
            payment.setIdentifier(SEPARATOR_BLANK);
        }
        if (!payment.getIdentifierType().trim().equalsIgnoreCase(BLANK_STRING)
                && payment.getIdentifier().trim().equals(BLANK_STRING)) {
            errors.rejectValue(IDENTIFIER, "bad.account.number.data", new Object[] { IDENTIFIER },
                    CommonConstants.BLANK_STRING);
        }

        if (payment.getIdentifierType().trim().equalsIgnoreCase(BLANK_STRING)
                && !payment.getIdentifier().trim().equals(BLANK_STRING)) {
            errors.rejectValue(IDENTIFIER_TYPE, "bad.account.type.data", new Object[] { IDENTIFIER_TYPE },
                    CommonConstants.BLANK_STRING);
        }
    }

    private void formatBillTypeCode(Payment payment) {
        if (payment.getIdentifierType() == null) {
            payment.setIdentifierType(BLANK_STRING);
        }

    }

    private void formatPaymentComments(Payment payment) {
        if (payment.getPaymentComments() == null) {
            payment.setPaymentComments(SEPARATOR_BLANK);
        }
    }

    private void formatPayableItem(Payment payment, Errors errors) {

        if (payment.getPolicyNumber().trim().equalsIgnoreCase(BLANK_STRING) && payment.getPayableItem() != null) {
            errors.rejectValue("payable Item", "bad.payable.item.data", new Object[] { "payable Item" },
                    CommonConstants.BLANK_STRING);
        }

        if (!payment.getPolicyNumber().trim().equalsIgnoreCase(BLANK_STRING) && payment.getPayableItem() != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndParentCode(
                    payment.getPayableItem().trim(), BusCodeTranslationParentCode.PAYABLE_ITEM.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                payment.setPayableItem(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue("payableItem", BAD_REQUEST_DATA_INVALID, new Object[] { "payableItem" },
                        CommonConstants.BLANK_STRING);
            }
        } else {
            payment.setPayableItem(BLANK_STRING);
        }
    }

    private void formatPaymentId(Payment payment) {
        if (payment.getPaymentId() == null) {
            payment.setPaymentId(SEPARATOR_BLANK);
        }
    }

    private void formatPaymentMethod(Payment payment, Errors errors) {
        if (payment.getPaymentMethod() != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    payment.getPaymentMethod(), BusCodeTranslationType.PAYMENTMETHOD.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                payment.setPaymentMethod(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue(PAYMENT_METHOD, BAD_REQUEST_DATA_INVALID, new Object[] { PAYMENT_METHOD },
                        CommonConstants.BLANK_STRING);
            }
        }
    }

    private void formatPaymentType(Payment payment, Errors errors) {
        if (payment.getPaymentType() != null) {
            BusCdTranslation busCdTranslation = null;
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
                busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(payment.getPaymentType(),
                        BilDesReasonType.CPY.getValue(), execContext.getLanguage());
            } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
                busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(payment.getPaymentType(),
                        BilDesReasonType.ONE_XA.getValue(), execContext.getLanguage());
            } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
                busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(payment.getPaymentType(),
                        BilDesReasonType.ASW.getValue(), execContext.getLanguage());
            } else {
                busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(payment.getPaymentType(),
                        BilDesReasonType.WALLET_PAYMENT_TYPE.getValue(), execContext.getLanguage());
            }
            if (busCdTranslation != null) {
                payment.setPaymentType(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue(PAYMENT_TYPE, BAD_REQUEST_DATA_INVALID, new Object[] { PAYMENT_TYPE },
                        CommonConstants.BLANK_STRING);
            }
        }
    }

    private String readBusCdTranslation(Payment payment) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByDescriptionAndType(
                payment.getIdentifierType(), BusCodeTranslationType.BILLACCOUNTTYPE.getValue(),
                execContext.getLanguage());
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusCdTranslationId().getBusCd().trim();
        }
        return BLANK_STRING;
    }

    private short getFileTypeCodeFromBilRuleUct(ZonedDateTime se3DateTime) {
        final String STRING_1XCC = BilRules.ONE_TIME_CREDIT_CARD.getValue();
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(STRING_1XCC, SEPARATOR_BLANK, SEPARATOR_BLANK,
                SEPARATOR_BLANK, se3DateTime, se3DateTime);
        if (bilRulesUct != null) {
            return new Short(bilRulesUct.getBrtParmListTxt());
        }
        return 0;
    }

    private short determineFileTypeCode(String paymentType, String busCodeType, String bilDesReasonCode) {

        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(paymentType, busCodeType,
                execContext.getLanguage());
        if (busCdTranslation == null) {
            busCdTranslation = busCdTranslationRepository.findByCodeAndType(bilDesReasonCode,
                    BilDesReasonType.FTC.getValue(), execContext.getLanguage());
        }

        if (busCdTranslation != null) {
            return Short.parseShort(busCdTranslation.getBusDescription().trim());
        }
        return 0;

    }

    private void checkPaymentIntegration(String identifierType, Payment payment) {

        if (identifierType.equals(AccountType.BILLACCOUNT.toString())) {
            BilAccount bilAccount = bilAccountRepository.findByAccountNumber(payment.getIdentifier());

            if (bilAccount != null) {
                payment.setAccountId(bilAccount.getAccountId());
                if (bilAccount.getBillTypeCd().trim()
                        .equalsIgnoreCase(AccountTypeCode.COMMISSIONACCOUNTCD.getValue())) {
                    isPaymentIntegration = false;
                }
            }
        }

        if (isPaymentIntegration) {
            isOneTimePaymentIntegration = BooleanUtils.toBoolean(oneTimePaymentIntegration);
            isAgencyPaymentIntegration = BooleanUtils.toBoolean(agencyPaymentIntegration);
            isAutoPaymentIntegration = BooleanUtils.toBoolean(autoPaymentIntegration);
        }

    }

    private short readRecurringFileType(short fileTypeCode, String eftType, String bilTypeCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository
                .findById(new BusCdTranslationId(eftType, RECURRING_TYPE, execContext.getLanguage(), SEPARATOR_BLANK))
                .orElse(null);
        if (busCdTranslation != null && !busCdTranslation.getBusDescription().trim().isEmpty()) {
            fileTypeCode = Short.valueOf(busCdTranslation.getBusDescription().trim());
        }
        busCdTranslation = busCdTranslationRepository
                .findById(new BusCdTranslationId(eftType, RECURRING_TYPE, execContext.getLanguage(), bilTypeCode))
                .orElse(null);
        if (busCdTranslation != null && !busCdTranslation.getBusDescription().trim().isEmpty()) {
            fileTypeCode = Short.valueOf(busCdTranslation.getBusDescription().trim());
        }
        return fileTypeCode;
    }

}