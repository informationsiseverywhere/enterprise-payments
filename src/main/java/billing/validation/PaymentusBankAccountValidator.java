package billing.validation;

import static core.utils.CommonConstants.BLANK_STRING;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import billing.model.Payment;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PaymentusBankAccountValidator implements Validator {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    private static final String PAYMENT_PROFILE_ID = "paymentProfileId";
    private static final String BAD_REQUEST_MISSING_PARAMETER = "bad.request.missing.parameter";
    private static final String ACCOUNTTYPE = "accountType";

    @Override
    public boolean supports(Class<?> clazz) {
        return Payment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Payment payment = (Payment) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, ACCOUNTTYPE, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { ACCOUNTTYPE });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, PAYMENT_PROFILE_ID, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { PAYMENT_PROFILE_ID });

        format(payment, errors);
    }

    private void format(Payment payment, Errors errors) {
        formatAccountType(payment, errors);
        formatRoutingNumber(payment);
        formartPaymentProfileId(payment, errors);
        formatCustomerProfileId(payment);
        formatEftAccountNumber(payment);
    }

    private void formatCustomerProfileId(Payment payment) {
        if (payment.getCustomerProfileId() == null) {
            payment.setCustomerProfileId(BLANK_STRING);
        }
    }

    private void formartPaymentProfileId(Payment payment, Errors errors) {
        if (payment.getPaymentProfileId() == null) {
            errors.rejectValue(PAYMENT_PROFILE_ID, "paymentProfileId.can.not.null");
        }

    }

    private void formatRoutingNumber(Payment payment) {
        if (payment.getRoutingNumber() == null) {
            payment.setRoutingNumber(BLANK_STRING);
        }

    }

    private void formatEftAccountNumber(Payment payment) {
        if (payment.getBankAccountNumber() == null) {
            payment.setBankAccountNumber(BLANK_STRING);
        }
    }

    private void formatAccountType(Payment payment, Errors errors) {
        final boolean isPatch = execContext.isPatchRequest();
        if (!isPatch || payment.getAccountType() != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    payment.getAccountType(), BusCodeTranslationType.EFT_ACCOUNT_TYPE.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                payment.setAccountType(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue(ACCOUNTTYPE, "bad.request.data.invalid", new Object[] { ACCOUNTTYPE }, BLANK_STRING);
            }
        }
    }

}
