package billing.validation;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_U;
import static core.utils.CommonConstants.CHAR_W;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_4;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import billing.data.entity.BilEftActivity;
import billing.data.entity.BilEftRejBch;
import billing.data.repository.BilEftActivityRepository;
import billing.data.repository.BilEftRejBchRepository;
import billing.data.repository.BilEftRejReaRepository;
import billing.model.RejectionPayment;
import billing.utils.BillingConstants.PaymentProviderType;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.utils.CommonConstants;
import core.utils.DateRoutine;
import core.utils.IdGenerator;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class BatchRejectionPaymentValidator implements Validator {

    private String batchId;

    private String entryDate;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilEftRejBchRepository bilEftRejBchRepository;

    @Autowired
    private BilEftActivityRepository bilEftActivityRepository;

    @Autowired
    private BilEftRejReaRepository bilEftRejReaRepository;

    @Value("${paymentIntegration.provider}")
    private String providerIntegration;

    private static final String REJECTION_REASON = "rejectionReason";
    private static final String PAYMENT_DIGITAL_CARD = "payment.digitalcard";
    private static final String PAYMENT_CREDIT_CARD = "payment.creditcard";

    @Override
    public boolean supports(Class<?> clazz) {
        return RejectionPayment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RejectionPayment rejectionPayment = (RejectionPayment) target;
        final boolean isPatch = execContext.isPatchRequest();

        if (isPatch) {
            rejectionPayment.setBatchId(getBatchId());
        } else {
            if (rejectionPayment.getBatchId() != null) {
                if (rejectionPayment.getBatchId().trim().length() <= 4) {
                    rejectionPayment.setBatchId(rejectionPayment.getBatchId().trim());
                } else {
                    errors.rejectValue("batchId", "bad.request.data.invalid", new Object[] { "batchId" },
                            CommonConstants.BLANK_STRING);
                }
            } else {
                rejectionPayment.setBatchId(generateUniqueId());
            }

        }

        if (StringUtils.isNotBlank(providerIntegration)
                && providerIntegration.equalsIgnoreCase(PaymentProviderType.AUTHORIZE_NET.getValue())) {
            throw new InvalidDataException("not.allowed.authorize.net");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "settlementDate", "bad.request.missing.parameter",
                new Object[] { "settlementDate" });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, REJECTION_REASON, "bad.request.missing.parameter",
                new Object[] { REJECTION_REASON });

        if (errors != null && !errors.getAllErrors().isEmpty()) {
            return;
        }

        formatFields();
        validateFields(rejectionPayment, errors);

    }

    private void validateFields(RejectionPayment rejectionPayment, Errors errors) {
        validateRejectionReason(rejectionPayment, errors);
        validateTransactionNumber(rejectionPayment);
    }

    private void validateRejectionReason(RejectionPayment rejectionPayment, Errors errors) {
        String bilEftRejReaCode = bilEftRejReaRepository.findEftRejCd(rejectionPayment.getRejectionReason().trim());
        if (bilEftRejReaCode == null) {
            errors.rejectValue(REJECTION_REASON, "bad.request.data.invalid", new Object[] { REJECTION_REASON },
                    CommonConstants.BLANK_STRING);
        } else {
            rejectionPayment.setRejectionReason(bilEftRejReaCode.trim());
        }

    }

    private void formatFields() {
        formatBatchId();
    }

    private void formatBatchId() {
        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(getEntryDate());
        List<BilEftRejBch> bilEftRejBchList = bilEftRejBchRepository.getbatchPayments(getBatchId(), bilEntryDt,
                execContext.getUserSeqeunceId(), CHAR_O, PageRequest.of(SHORT_ZERO, SHORT_ONE));
        if (bilEftRejBchList != null && !bilEftRejBchList.isEmpty()
                && bilEftRejBchList.get(SHORT_ZERO).getBrbRejStaInd() != BLANK_CHAR) {
            if (execContext.isPatchRequest()) {
                throw new InvalidDataException("payment.accept.not.updated");
            } else {
                throw new InvalidDataException("payment.accept.not.valid");
            }
        }

    }

    private void validateTransactionNumber(RejectionPayment rejectionPayment) {

        if ((rejectionPayment.getReferenceNumber() == null || rejectionPayment.getReferenceNumber().trim().isEmpty())
                && (rejectionPayment.getTransactionNumber() == null
                        || rejectionPayment.getTransactionNumber().trim().isEmpty())) {

            throw new InvalidDataException("transaction.or.reference.number.required");

        } else if (rejectionPayment.getReferenceNumber() == null && rejectionPayment.getTransactionNumber() != null) {
            checkByTraceNumber(rejectionPayment);

        } else if (rejectionPayment.getReferenceNumber() != null && rejectionPayment.getTransactionNumber() == null) {
            checkByReferenceNumber(rejectionPayment);

        } else {
            checkByTransactionId(rejectionPayment);

        }

    }

    private void checkByTransactionId(RejectionPayment rejectionPayment) {
        BilEftActivity bilEftActivity = bilEftActivityRepository.checkTraceNumberAndTransactionId(
                rejectionPayment.getSettlementDate(), rejectionPayment.getTransactionNumber(),
                rejectionPayment.getReferenceNumber());
        if (bilEftActivity != null) {
            if (StringUtils.isBlank(providerIntegration)
                    || providerIntegration.equalsIgnoreCase(PaymentProviderType.AUTHORIZE_NET.getValue())) {
                if (bilEftActivity.getBilEftRcdTyc() == CHAR_T || bilEftActivity.getBilEftRcdTyc() == CHAR_U) {
                    throw new InvalidDataException(PAYMENT_CREDIT_CARD);
                } else if (bilEftActivity.getBilEftRcdTyc() == CHAR_W) {
                    throw new InvalidDataException(PAYMENT_DIGITAL_CARD);
                }
            }
        } else {
            throw new DataNotFoundException("transaction.not.found.with.trace.and.reference.number",
                    new Object[] { rejectionPayment.getTransactionNumber(), rejectionPayment.getReferenceNumber(),
                            DateRoutine.dateTimeAsMMDDYYYYAsString(rejectionPayment.getSettlementDate()) });
        }

    }

    private void checkByReferenceNumber(RejectionPayment rejectionPayment) {
        BilEftActivity bilEftActivity = bilEftActivityRepository
                .checkTransactionId(rejectionPayment.getSettlementDate(), rejectionPayment.getReferenceNumber());
        if (bilEftActivity != null) {
            if (StringUtils.isBlank(providerIntegration)
                    || providerIntegration.equalsIgnoreCase(PaymentProviderType.AUTHORIZE_NET.getValue())) {
                if (bilEftActivity.getBilEftRcdTyc() == CHAR_T || bilEftActivity.getBilEftRcdTyc() == CHAR_U) {
                    throw new InvalidDataException(PAYMENT_CREDIT_CARD);
                } else if (bilEftActivity.getBilEftRcdTyc() == CHAR_W) {
                    throw new InvalidDataException(PAYMENT_DIGITAL_CARD);
                }
            }

            rejectionPayment.setTransactionNumber(bilEftActivity.getBilEftActivityId().getBilEftTraceNbr());
        } else {
            throw new DataNotFoundException("transaction.not.found.with.reference.number",
                    new Object[] { rejectionPayment.getReferenceNumber(),
                            DateRoutine.dateTimeAsMMDDYYYYAsString(rejectionPayment.getSettlementDate()) });
        }

    }

    private void checkByTraceNumber(RejectionPayment rejectionPayment) {
        List<BilEftActivity> bilEftActivityList = bilEftActivityRepository.checkTraceNumber(
                rejectionPayment.getSettlementDate(), rejectionPayment.getTransactionNumber(),
                PageRequest.of(SHORT_ZERO, SHORT_ONE));

        if (bilEftActivityList == null || bilEftActivityList.isEmpty()) {
            throw new DataNotFoundException("transaction.not.found",
                    new Object[] { rejectionPayment.getTransactionNumber(),
                            DateRoutine.dateTimeAsMMDDYYYYAsString(rejectionPayment.getSettlementDate()) });
        } else {
            if (StringUtils.isBlank(providerIntegration)
                    || providerIntegration.equalsIgnoreCase(PaymentProviderType.AUTHORIZE_NET.getValue())) {
                if (bilEftActivityList.get(SHORT_ZERO).getBilEftRcdTyc() == CHAR_T
                        || bilEftActivityList.get(SHORT_ZERO).getBilEftRcdTyc() == CHAR_U) {
                    throw new InvalidDataException(PAYMENT_CREDIT_CARD);
                } else if (bilEftActivityList.get(SHORT_ZERO).getBilEftRcdTyc() == CHAR_W) {
                    throw new InvalidDataException(PAYMENT_DIGITAL_CARD);
                }
            }

        }

    }

	private String generateUniqueId() {
        return IdGenerator.generateUniqueIdUpperCase(GENERATED_ID_LENGTH_4);
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

}