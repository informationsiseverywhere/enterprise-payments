package billing.validation;

import static core.utils.CommonConstants.BLANK_STRING;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import application.utils.model.embedded.AccountSearch;
import application.utils.service.FieldLevelAuthenticationService;
import application.utils.service.impl.AccountsElasticSearchServiceImpl;
import billing.data.entity.BilEftBankEd;
import billing.data.entity.BilRulesUct;
import billing.data.repository.BilEftBankEdRepository;
import billing.model.Payment;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import party.model.UserParty;
import party.service.UserPartySecurityService;
import party.service.impl.UserPartySecurityServiceImpl.Role;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AgencySweepValidator implements Validator {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private UserPartySecurityService userPartySecurityService;

    @Autowired
    private FieldLevelAuthenticationService fieldLevelAuthenticationService;

    @Autowired
    private AccountsElasticSearchServiceImpl accountsElasticSearchServiceImpl;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private BilEftBankEdRepository bilEftBankEdRepository;

    public static final Logger logger = LogManager.getLogger(AgencySweepValidator.class);

    private static final String AGENT_ACCESS_RULE = "ACAG";
    private static final String AGENCY_SWEEP_RULE = "AGSW";
    private static final String ACCOUNT_TYPE = "accountType";
    private static final String BAD_REQUEST_MISSING_PARAMETER = "bad.request.missing.parameter";
    private static final String BAD_REQUEST_DATA_INVALID = "bad.request.data.invalid";
    private static final String BANK_ACCOUNT_NUMBER = "bankAccountNumber";
    private static final String AGENCY_NUMBER = "agencyNumber";

    @Override
    public boolean supports(Class<?> clazz) {
        return Payment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Payment payment = (Payment) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, ACCOUNT_TYPE, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { ACCOUNT_TYPE });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, BANK_ACCOUNT_NUMBER, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { BANK_ACCOUNT_NUMBER });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, AGENCY_NUMBER, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { AGENCY_NUMBER });

        validateEftAccountNumber(payment, errors);
        validateAgencySweepRule();
        validateAgencyDetails(payment, errors);

        if (errors != null && !errors.getAllErrors().isEmpty()) {
            return;
        }
        format(payment, errors);

    }

    private void validateAgencySweepRule() {
        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct(AGENCY_SWEEP_RULE, BLANK_STRING, BLANK_STRING,
                BLANK_STRING);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() != 'Y') {
            throw new InvalidDataException("agency.sweep.rule.not.found");
        }
    }

    private void validateAgencyDetails(Payment payment, Errors errors) {
        boolean isAuthorizedPayment = false;
        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct(AGENT_ACCESS_RULE, BLANK_STRING, BLANK_STRING,
                BLANK_STRING);
        if (bilRulesUct == null || bilRulesUct.getBrtRuleCd() == 'N') {
            return;
        }
        isAuthorizedPayment = fieldLevelAuthenticationService.validateAmount("AGENTSWEEPADD", 0.00,
                payment.getCurrency());
        if (!isAuthorizedPayment) {
            UserParty userParty = userPartySecurityService.getPartyByUserId();
            if (userParty.getPartyId() != null && !userParty.getPartyId().trim().isEmpty()) {
                if (isAgent(userParty.getRole()) && userParty.getAgencies().contains(payment.getAgencyNumber())) {
                    isAuthorizedPayment = true;
                } else {
                    errors.rejectValue(AGENCY_NUMBER, "user.agency.invalid",
                            new Object[] { execContext.getUserId(), payment.getAgencyNumber() }, BLANK_STRING);
                }
            } else {
                isAuthorizedPayment = true;
            }

            try {
                AccountSearch accountSearch = accountsElasticSearchServiceImpl
                        .fetchAccountDocument(payment.getIdentifier());
                if (accountSearch != null && accountSearch.getAccountId() != null
                        && !accountSearch.getAccountId().isEmpty()) {
                    if (accountSearch.getAgencies().contains(payment.getAgencyNumber())) {
                        isAuthorizedPayment = true;
                    } else {
                        errors.rejectValue(AGENCY_NUMBER, "account.agency.invalid",
                                new Object[] { payment.getIdentifier(), payment.getAgencyNumber() }, BLANK_STRING);
                    }
                }
            } catch (IOException e) {
                logger.error("Elastic Search failed for accounts");
            }
        }

        if (!isAuthorizedPayment) {
            errors.rejectValue(AGENCY_NUMBER, "payment.agency.invalid", new Object[] { payment.getAgencyNumber() },
                    BLANK_STRING);
        }
    }

    private boolean isAgent(String role) {
        return role.equals(Role.AGENT.getValue()) || role.equals(Role.AGENCY.getValue());
    }

    private void format(Payment payment, Errors errors) {
        formatAccountType(payment, errors);
        formatRoutingNumber(payment, errors);

    }

    private void formatRoutingNumber(Payment payment, Errors errors) {

        BilEftBankEd bilEftBankEd = bilEftBankEdRepository.findByRoutingTransitNbr(payment.getRoutingNumber());
        if (bilEftBankEd == null) {
            errors.rejectValue("routingNumber", BAD_REQUEST_DATA_INVALID, new Object[] { "routingNumber" },
                    BLANK_STRING);
        } else {
            payment.setRoutingNumber(bilEftBankEd.getRoutingTransitNbr().trim());
        }

    }

    private void validateEftAccountNumber(Payment payment, Errors errors) {
        if (payment.getBankAccountNumber() != null && !payment.getBankAccountNumber().matches("[a-zA-Z0-9]*")) {
            errors.rejectValue(BANK_ACCOUNT_NUMBER, BAD_REQUEST_DATA_INVALID, new Object[] { BANK_ACCOUNT_NUMBER },
                    BLANK_STRING);
        }
    }

    private void formatAccountType(Payment payment, Errors errors) {

        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                payment.getAccountType(), BusCodeTranslationType.EFT_ACCOUNT_TYPE.getValue(),
                execContext.getLanguage());
        if (busCdTranslation != null) {
            payment.setAccountType(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
        } else {
            errors.rejectValue(ACCOUNT_TYPE, BAD_REQUEST_DATA_INVALID, new Object[] { ACCOUNT_TYPE }, BLANK_STRING);
        }
    }

}
