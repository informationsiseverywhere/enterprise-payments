package billing.validation;

import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_K;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_4;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import billing.data.entity.BilAccount;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilPolicy;
import billing.data.entity.id.BilCashEntryId;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilPolicyRepository;
import billing.model.Payment;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BusCodeTranslationParentCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.CommonConstants;
import core.utils.DateRoutine;
import core.utils.IdGenerator;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class BatchPaymentValidator implements Validator {

    private String batchId;

    private String postedDate;

    private String userId;

    private String paymentSequenceNumber;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    private static final String PAYMENT_TYPE = "paymentType";
    private static final String BAD_REQUEST_MISSING_PARAMETER = "bad.request.missing.parameter";
    private static final String BAD_REQUEST_DATA_INVALID = "bad.request.data.invalid";
    private static final String POLICY_NUMBER = "policyNumber";
    private static final String IDENTIFIER_TYPE = "identifierType";

    @Override
    public boolean supports(Class<?> clazz) {
        return Payment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Payment batchPayment = (Payment) target;
        final boolean isPatch = execContext.isPatchRequest();

        if (isPatch) {
            batchPayment.setBatchId(getBatchId());
            batchPayment.setUserId(getUserId());
            batchPayment.setPaymentSequenceNumber(Short.parseShort(getPaymentSequenceNumber()));
        } else {
            if (batchPayment.getBatchId() != null) {
                if (batchPayment.getBatchId().trim().length() <= 4) {
                    batchPayment.setBatchId(batchPayment.getBatchId().trim());
                } else {
                    errors.rejectValue("batchId", BAD_REQUEST_DATA_INVALID, new Object[] { "batchId" },
                            CommonConstants.BLANK_STRING);
                }
            } else {
                batchPayment.setBatchId(generateUniqueId());
            }

            ValidationUtils.rejectIfEmptyOrWhitespace(errors, PAYMENT_TYPE, BAD_REQUEST_MISSING_PARAMETER,
                    new Object[] { PAYMENT_TYPE });

            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "paymentAmount", BAD_REQUEST_MISSING_PARAMETER,
                    new Object[] { "paymentAmount" });

            validatePaymentAmount(batchPayment);

        }

        if (errors != null && !errors.getAllErrors().isEmpty()) {
            return;
        }

        formatFields(batchPayment, errors);

    }

    private void validatePaymentAmount(Payment batchPayment) {
        if (batchPayment.getPaymentAmount() != null && batchPayment.getPaymentAmount() <= 0) {
            throw new InvalidDataException(BAD_REQUEST_DATA_INVALID, new Object[] { batchPayment.getPaymentAmount() });
        }
    }

    private void formatFields(Payment batchPayment, Errors errors) {
        formatBatchId(batchPayment);
        formatDates(batchPayment);
        formatPaymentType(batchPayment, errors);
        formatPolicy(batchPayment, errors);
        formatPayableItem(batchPayment, errors);
        formatSuspenseReason(batchPayment, errors);
        formatPaymentId(batchPayment);
        formatPaymentComments(batchPayment);
        formatIdentifierType(batchPayment, errors);
        formatIdentifier(batchPayment);
        formatAdditionalId(batchPayment, errors);
        formatRemitterId(batchPayment);
        formatAdditonalValidations(batchPayment);
    }

    private void formatIdentifier(Payment batchPayment) {
        if (batchPayment.getIdentifier() != null && !batchPayment.getIdentifier().trim().equals(BLANK_STRING)) {
            batchPayment.setIdentifier(batchPayment.getIdentifier().trim().toUpperCase());
        }
    }

    private void formatBatchId(Payment batchPayment) {
        ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(getPostedDate());
        BilCashEntryTot bilCashEntryTot;
        if (execContext.isPatchRequest()) {
            bilCashEntryTot = bilCashEntryTotRepository
                    .findById(new BilCashEntryTotId(bilEntryDt, getBatchId(), batchPayment.getUserId())).orElse(null);
        } else {
            bilCashEntryTot = bilCashEntryTotRepository
                    .findById(new BilCashEntryTotId(bilEntryDt, getBatchId(), execContext.getUserSeqeunceId()))
                    .orElse(null);
        }
        if (bilCashEntryTot == null) {
            throw new DataNotFoundException("batch.notfound", new Object[] { batchId });
        }
        if (bilCashEntryTot.getBctAcceptInd() != CHAR_N && bilCashEntryTot.getBctDepositInd() != CHAR_N) {
            if (execContext.isPatchRequest()) {
                throw new InvalidDataException("payment.accept.not.updated");
            } else {
                throw new InvalidDataException("payment.accept.not.valid");
            }

        }
    }

    private void formatAdditonalValidations(Payment payment) {
        boolean isDownPayment = validateIfDownPayment(payment.getPaymentType());
        if (isDownPayment && payment.getIdentifierType() != null && !payment.getIdentifierType().trim().isEmpty()
                && !payment.getIdentifierType().equals(AccountType.BILLACCOUNT.toString())) {
            throw new InvalidDataException("downpayment.invalid");
        }
        if (isDownPayment) {
            findBatchPaymentDetails(payment);
        }

        boolean isEquity = validateIfEquityPayment(payment.getPaymentType());
        if (isEquity && (payment.getIdentifierType().equals(AccountType.GROUP_ACCOUNT.toString())
                || payment.getIdentifierType().equals(AccountType.AGENCY_ACCOUNT.toString()))) {
            throw new InvalidDataException("equity.payment.not.valid");
        }
        if (isEquity && payment.getPolicyNumber().trim().isEmpty()) {
            throw new InvalidDataException("equity.payment.not.valid.policy");
        }

        if (!payment.getPolicyNumber().trim().isEmpty()) {
            List<BilPolicy> bilPolicyList = bilPolicyRepository.findByPolicyNumber(payment.getPolicyNumber(), CHAR_K);
            if (bilPolicyList != null && !bilPolicyList.isEmpty()) {
                BilPolicy bilPolicy = bilPolicyList.get(0);
                if (!busCdTranslationRepository.findAllByCodeAndType(bilPolicy.getBilTypeCd(),
                        BusCodeTranslationType.BTG.getValue(), execContext.getLanguage()).isEmpty()) {
                    throw new InvalidDataException("policy.agency.invalid");
                }
            }
        } else if (!payment.getIdentifier().trim().isEmpty()
                && payment.getIdentifierType().equals(AccountType.AGENCYBILL.toString())) {
            BilAccount bilAccount = bilAccountRepository.findByAccountNumber(payment.getIdentifier());
            if (bilAccount != null && !busCdTranslationRepository.findAllByCodeAndType(bilAccount.getBillTypeCd(),
                    BusCodeTranslationType.BTG.getValue(), execContext.getLanguage()).isEmpty()) {
                throw new InvalidDataException("policy.agency.invalid");
            }
        }
    }

    private boolean validateIfDownPayment(String paymentType) {
        return busCdTranslationRepository.findByCodeAndType(paymentType, BusCodeTranslationType.DOWN_PAYMENT.getValue(),
                execContext.getLanguage()) != null;
    }

    private boolean validateIfEquityPayment(String paymentType) {
        return busCdTranslationRepository.findByCodeAndType(paymentType, BilDesReasonType.EQT.getValue(),
                execContext.getLanguage()) != null;
    }

    private void formatIdentifierType(Payment payment, Errors errors) {
        if (payment.getIdentifierType() != null && !payment.getIdentifierType().trim().isEmpty()) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "identifier", "identifier.type.require",
                    new Object[] { IDENTIFIER_TYPE });

            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    payment.getIdentifierType().trim(), BusCodeTranslationType.BILLACCOUNTTYPE.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                payment.setIdentifierType(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue(IDENTIFIER_TYPE, BAD_REQUEST_DATA_INVALID, new Object[] { IDENTIFIER_TYPE },
                        CommonConstants.BLANK_STRING);
            }
        } else if (payment.getIdentifier() != null) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, IDENTIFIER_TYPE, "identifier.require",
                    new Object[] { "identifier" });
        } else {
            payment.setIdentifierType(CommonConstants.BLANK_STRING);
            payment.setIdentifier(CommonConstants.BLANK_STRING);
        }
    }

    private void formatRemitterId(Payment payment) {
        if (payment.getRemitterId() == null) {
            payment.setRemitterId(BLANK_STRING);
        }
    }

    private void formatAdditionalId(Payment payment, Errors errors) {
        if (payment.getAdditionalId() == null || payment.getAdditionalId().trim().isEmpty()) {
            payment.setAdditionalId(SEPARATOR_BLANK);
        } else if (!payment.getIdentifierType().equalsIgnoreCase(AccountType.GROUP_ACCOUNT.toString())) {
            errors.rejectValue("additionalId", BAD_REQUEST_DATA_INVALID, new Object[] { "additionalId" },
                    CommonConstants.BLANK_STRING);
        }
    }

    private void formatPaymentComments(Payment payment) {
        if (payment.getPaymentComments() == null) {
            payment.setPaymentComments(SEPARATOR_BLANK);
        }
    }

    private void formatPaymentId(Payment payment) {
        if (payment.getPaymentId() == null) {
            payment.setPaymentId(SEPARATOR_BLANK);
        }
    }

    private void formatSuspenseReason(Payment batchPayment, Errors errors) {
        if (batchPayment.getPaymentOnHold() != null && !batchPayment.getPaymentOnHold()
                && batchPayment.getSuspenseReason() != null && !batchPayment.getSuspenseReason().trim().isEmpty()) {
            throw new InvalidDataException("holdSuspense.invalid");
        }
        if (batchPayment.getSuspenseReason() != null && !batchPayment.getSuspenseReason().trim().isEmpty()) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    batchPayment.getSuspenseReason().trim(), BusCodeTranslationType.DIRECT_TYPE.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                batchPayment.setSuspenseReason(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue("suspenseReason", BAD_REQUEST_DATA_INVALID, new Object[] { "suspenseReason" },
                        CommonConstants.BLANK_STRING);
            }
        } else {
            batchPayment.setSuspenseReason(BLANK_STRING);
        }
    }

    private void formatPolicy(Payment batchPayment, Errors errors) {
        if (batchPayment.getPolicySymbol() != null && !batchPayment.getPolicySymbol().trim().isEmpty()) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, POLICY_NUMBER, "policy.symbol.data.require",
                    new Object[] { POLICY_NUMBER });

            BusCdTranslation busCdTranslation = busCdTranslationRepository
                    .findByDescriptionAndType(batchPayment.getPolicySymbol().trim(), "SYM", execContext.getLanguage());
            if (busCdTranslation != null) {
                batchPayment.setPolicySymbol(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                busCdTranslation = busCdTranslationRepository.findByCodeAndType(batchPayment.getPolicySymbol().trim(),
                        "SYM", execContext.getLanguage());
                if (busCdTranslation != null) {
                    batchPayment.setPolicySymbol(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
                } else {
                    errors.rejectValue("policySymbol", BAD_REQUEST_DATA_INVALID, new Object[] { "policySymbol" },
                            CommonConstants.BLANK_STRING);
                }

            }
        } else {
            batchPayment.setPolicySymbol(BLANK_STRING);
        }

        if (batchPayment.getPolicyNumber() == null) {
            batchPayment.setPolicyNumber(BLANK_STRING);
        } else {
            batchPayment.setPolicyNumber(batchPayment.getPolicyNumber().trim().toUpperCase());
        }
    }

    private void formatPayableItem(Payment batchPayment, Errors errors) {
        if (batchPayment.getPayableItem() != null && !batchPayment.getPayableItem().trim().isEmpty()) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, POLICY_NUMBER, "payable.data.require",
                    new Object[] { batchPayment.getPayableItem() });

            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndParentCode(
                    batchPayment.getPayableItem().trim(), BusCodeTranslationParentCode.PAYABLE_ITEM.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                batchPayment.setPayableItem(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue("payableItem", BAD_REQUEST_DATA_INVALID, new Object[] { "payableItem" },
                        CommonConstants.BLANK_STRING);
            }
        } else {
            batchPayment.setPayableItem(BLANK_STRING);
        }
    }

    private void formatPaymentType(Payment batchPayment, Errors errors) {
        if (batchPayment.getPaymentType() != null && !batchPayment.getPaymentType().trim().isEmpty()) {
            List<BusCdTranslation> busCdTranslationList = busCdTranslationRepository.findAllByDescriptionAndTypeList(
                    batchPayment.getPaymentType().trim(),
                    Arrays.asList(BusCodeTranslationType.PAYMENT_TYPE_CODE.getValue(),
                            BusCodeTranslationType.CASH_PAYMENT.getValue(),
                            BusCodeTranslationType.COLLECT_AGENCY_PAYMENT.getValue()),
                    execContext.getLanguage());
            if (busCdTranslationList != null && !busCdTranslationList.isEmpty()) {
                batchPayment.setPaymentType(busCdTranslationList.get(0).getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue(PAYMENT_TYPE, BAD_REQUEST_DATA_INVALID, new Object[] { PAYMENT_TYPE },
                        CommonConstants.BLANK_STRING);
            }
        }
    }

    private void formatDates(Payment batchPayment) {

        assignDates(batchPayment);
    }

    private void assignDates(Payment batchPayment) {
        final boolean isPatch = execContext.isPatchRequest();
        if (!isPatch) {
            if (batchPayment.getPostedDate() == null) {
                batchPayment.setPostedDate(execContext.getApplicationDate());
            }
            if (batchPayment.getPostmarkDate() == null) {
                batchPayment.setPostmarkDate(DateRoutine.defaultDateTime());
            }
            if (batchPayment.getPaymentDate() == null) {
                batchPayment.setPaymentDate(execContext.getApplicationDate());
            }
            if (batchPayment.getDueDate() == null) {
                batchPayment.setDueDate(DateRoutine.defaultDateTime());
            }
        }
    }

    private void findBatchPaymentDetails(Payment payment) {
        boolean accountPaymentDirection = false;
        boolean policyPaymentDirection = false;
        boolean dueDatePaymentDirection = false;
        if (execContext.isPatchRequest()) {
            ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(getPostedDate());
            BilCashEntry bilCashEntry = bilCashEntryRepository.findById(new BilCashEntryId(bilEntryDt,
                    payment.getBatchId(), payment.getUserId(), payment.getPaymentSequenceNumber())).orElse(null);
            if (bilCashEntry != null) {
                if (bilCashEntry.getBilAccountNbr() != null && !bilCashEntry.getBilAccountNbr().trim().isEmpty()) {
                    accountPaymentDirection = true;
                    if ((bilCashEntry.getBilAdjDueDt() != null
                            && !bilCashEntry.getBilAdjDueDt().equals(DateRoutine.defaultDateTime()))
                            || (payment.getDueDate() != null
                                    && !payment.getDueDate().equals(DateRoutine.defaultDateTime()))) {
                        dueDatePaymentDirection = true;
                    }
                    BilAccount bilAccount = bilAccountRepository
                            .findByAccountNumber(bilCashEntry.getBilAccountNbr().trim());

                    if (bilAccount != null && !bilCashEntry.getPolNbr().trim().isEmpty()) {
                        policyPaymentDirection = true;
                    }
                } else if (!bilCashEntry.getPolNbr().trim().isEmpty()) {
                    policyPaymentDirection = true;
                }
            }
        } else {
            if (payment.getPolicyNumber() != null && !payment.getPolicyNumber().trim().isEmpty()) {
                policyPaymentDirection = true;
            } else {
                accountPaymentDirection = true;
                if (payment.getDueDate() != null && !payment.getDueDate().equals(DateRoutine.defaultDateTime())) {
                    dueDatePaymentDirection = true;
                }
            }
        }
        if (policyPaymentDirection || (accountPaymentDirection && dueDatePaymentDirection)) {
            throw new InvalidDataException("downpayment.invalid.direction");
        }
    }

    private String generateUniqueId() {
        return IdGenerator.generateUniqueIdUpperCase(GENERATED_ID_LENGTH_4);
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPaymentSequenceNumber() {
        return paymentSequenceNumber;
    }

    public void setPaymentSequenceNumber(String paymentSequenceNumber) {
        this.paymentSequenceNumber = paymentSequenceNumber;
    }

}
