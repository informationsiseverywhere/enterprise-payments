package billing.validation;

import static core.utils.CommonConstants.GENERATED_ID_LENGTH_4;
import static core.utils.CommonConstants.SHORT_ZERO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import billing.data.repository.BilEftRejBchRepository;
import billing.model.BatchRejection;
import core.api.ExecContext;
import core.exception.validation.InvalidDataException;
import core.utils.CommonConstants;
import core.utils.DateRoutine;
import core.utils.IdGenerator;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class BatchRejectionValidator implements Validator {

    private String batchId;

    private String entryDate;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilEftRejBchRepository bilEftRejBchRepository;

    @Override
    public boolean supports(Class<?> clazz) {
        return BatchRejection.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BatchRejection batch = (BatchRejection) target;
        final boolean isPatch = execContext.isPatchRequest();
        boolean isGeneratedBatchId = false;

        if (isPatch) {
            batch.setBatchId(getBatchId());
        } else {
            if (batch.getBatchId() != null) {
                if (batch.getBatchId().trim().length() <= 4) {
                    batch.setBatchId(batch.getBatchId().trim());
                } else {
                    errors.rejectValue("batchId", "bad.request.data.invalid", new Object[] { "batchId" },
                            CommonConstants.BLANK_STRING);
                }
            } else {
                batch.setBatchId(generateUniqueId());
                isGeneratedBatchId = true;
            }

        }
        validateAcceptBatch(batch, errors);
        if (errors != null && !errors.getAllErrors().isEmpty()) {
            return;
        }

        formatFields(batch);
        formatBatchId(batch, isGeneratedBatchId);

    }

    private void formatFields(BatchRejection batch) {
        formatDates(batch);
        formatIsAccept(batch);

    }

    private void formatIsAccept(BatchRejection batch) {
        if (batch.getIsAccept() == null) {
            batch.setIsAccept(false);
        }

    }

    private void formatDates(BatchRejection batch) {
        final boolean isPatch = execContext.isPatchRequest();
        if (!isPatch && batch.getEntryDate() == null) {
            batch.setEntryDate(execContext.getApplicationDate());
        }
    }

    private void validateAcceptBatch(BatchRejection batch, Errors errors) {
        final boolean isPatch = execContext.isPatchRequest();
        if (isPatch && batch.getIsAccept() != null && !batch.getIsAccept().booleanValue()) {
            errors.rejectValue("isAccept", "accept.not.valid", new Object[] { "isAccept" },
                    CommonConstants.BLANK_STRING);
        }

    }

    private String generateUniqueId() {
        return IdGenerator.generateUniqueIdUpperCase(GENERATED_ID_LENGTH_4);
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    private void formatBatchId(BatchRejection batch, boolean isGeneratedBatchId) {

        int selectAttempts = SHORT_ZERO;
        int retryNbr = 2;
        final boolean isPatch = execContext.isPatchRequest();
        if (!isPatch) {
            if (isGeneratedBatchId) {
                while (selectAttempts <= retryNbr) {
                    Integer count = checkBatchExist(batch);
                    if (count != null && count != SHORT_ZERO) {
                        batch.setBatchId(generateUniqueId());
                        selectAttempts = (short) (selectAttempts + 1);
                    } else {
                        selectAttempts = (short) (retryNbr + 1);
                    }
                }
                Integer count = checkBatchExist(batch);
                if (count != null && count != SHORT_ZERO) {
                    throw new InvalidDataException("batch.add.not.valid", new Object[] { batch.getBatchId(),
                            DateRoutine.dateTimeAsMMDDYYYYAsString(batch.getEntryDate()) });
                }
            } else {
                Integer count = checkBatchExist(batch);
                if (count != null && count != SHORT_ZERO) {
                    throw new InvalidDataException("batch.add.not.valid", new Object[] { batch.getBatchId(),
                            DateRoutine.dateTimeAsMMDDYYYYAsString(batch.getEntryDate()) });
                }
            }
        }

    }

    private Integer checkBatchExist(BatchRejection batch) {
        return bilEftRejBchRepository.countbatchExits(batch.getBatchId(), batch.getEntryDate(),
                execContext.getUserSeqeunceId());
    }

}
