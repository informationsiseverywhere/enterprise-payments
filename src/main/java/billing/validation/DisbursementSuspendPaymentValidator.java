package billing.validation;

import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import application.security.service.impl.AppSecurityServiceImpl;
import application.utils.service.FieldLevelAuthenticationService;
import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilUnIdCashId;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.model.PayableOption;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CashEntryMethod;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.TechnicalKeyTypeCode;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import disbursement.utils.DisbursementConstants.DisbursementType;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class DisbursementSuspendPaymentValidator implements Validator {

    private Short sequenceNumber;
    private String postedDate;
    private String user;
    private String batchId;
    private String distributionDate;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private FieldLevelAuthenticationService fieldLevelAuthenticationService;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    MultiCurrencyService multiCurrencyService;

    @Autowired
    private AppSecurityServiceImpl appSecurityServiceImpl;

    private static final Logger logger = LogManager.getLogger(DisbursementSuspendPaymentValidator.class);

    private static final String BAD_REQUEST_MISSING_PARAMETER = "bad.request.missing.parameter";
    private static final String BAD_REQUEST_DATA_INVALID = "bad.request.data.invalid";
    private static final String INVALID_REASON = "invalid.reason";

    @Override
    public boolean supports(Class<?> clazz) {
        return PayableOption.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PayableOption payableOption = (PayableOption) target;

        if (payableOption.getRefundMethod() == null || payableOption.getRefundMethod().trim().isEmpty()) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "refundMethod", BAD_REQUEST_MISSING_PARAMETER,
                    new Object[] { "refundMethod" });
        }
        if (payableOption.getReason() == null || payableOption.getReason().trim().isEmpty()) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reason", BAD_REQUEST_MISSING_PARAMETER,
                    new Object[] { "reason" });
        }
        if (payableOption.getPayeeId() == null || payableOption.getPayeeId().trim().isEmpty()) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "payeeId", BAD_REQUEST_MISSING_PARAMETER,
                    new Object[] { "payeeId" });
        }
        if (payableOption.getAddressSequenceNumber() == null) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "addressSequenceNumber", BAD_REQUEST_MISSING_PARAMETER,
                    new Object[] { "addressSequenceNumber" });
        }
        if (payableOption.getAddressSequenceNumber() <= SHORT_ZERO) {
            throw new InvalidDataException(BAD_REQUEST_DATA_INVALID, new Object[] { "address sequence number" });
        }
        if (errors != null && !errors.getAllErrors().isEmpty()) {
            return;
        }
        formatRefundMethod(payableOption);

        if (payableOption.getRefundMethod().equals(DisbursementType.MANUAL.getDisburseType())) {
            logger.info("Check number required for only manual disbursement.");
            if (payableOption.getCheckNumber() == null) {
                throw new InvalidDataException(BAD_REQUEST_DATA_INVALID, new Object[] { "check number" });
            }
        } else {
            payableOption.setCheckNumber((int) SHORT_ZERO);
        }

        if (payableOption.getTransactionId() == null) {
            payableOption.setTransactionId(BLANK_STRING);
        }
        validateCheckNumber(payableOption);

        String disburseType = formatReason(payableOption);
        checkIfDisbursementAllowed(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate), user, batchId,
                DateRoutine.dateTimeAsYYYYMMDD(distributionDate), payableOption, disburseType);
    }

    private void checkIfDisbursementAllowed(Short sequenceNumber, ZonedDateTime postedDate, String user, String batchId,
            ZonedDateTime distributionDate, PayableOption payableOption, String disburseType) {
        String userId = appSecurityServiceImpl.authenticateUser(user).getUserSequenceId();
        BilUnIdCash bilUnidentifiedCash = bilUnIdCashRepository
                .findById(new BilUnIdCashId(distributionDate, sequenceNumber, batchId, userId, postedDate))
                .orElse(null);

        if (bilUnidentifiedCash == null) {
            logger.debug("Payment not found in BIL_UNID_CASH");
            throw new DataNotFoundException("payment.does.not.exists");
        }
        if (!bilUnidentifiedCash.getBilDspTypeCd().trim().equals(BilDspTypeCode.SUSPENDED.getValue())) {
            logger.debug("Only suspended row can be disbursed");
            throw new InvalidDataException("row.selected.does.not.qualify.for.disbursement");
        }
        if (bilUnidentifiedCash.getBilRctAmt() <= DECIMAL_ZERO) {
            logger.debug("Payment amount must be greater than zero");
            throw new InvalidDataException("paymentAmount.invalid");
        }
        if ((payableOption.getRefundMethod().equals(DisbursementType.ELECTRONIC.getDisburseType())
                || payableOption.getRefundMethod().equals(DisbursementType.SYSTEM.getDisburseType()))
                && payableOption.getCheckNumber() != SHORT_ZERO) {
            throw new InvalidDataException("check.number.should.not.be.defined");
        }
        if (payableOption.getRefundMethod().equals(DisbursementType.MANUAL.getDisburseType())
                && payableOption.getCheckNumber() == SHORT_ZERO) {
            throw new InvalidDataException("check.number.should.be.defined");
        }
        if (checkPendingAchPayment(bilUnidentifiedCash.getBillUnidCashId(), distributionDate,
                bilUnidentifiedCash.getBilCshEtrMthCd())) {
            logger.debug("Cannot disburse pending payment");
            throw new InvalidDataException("pending.payment.cannot.disburse");
        }
        if (bilUnidentifiedCash.getBilCshEtrMthCd() != CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()
                && !disburseType.trim().equals(BilDesReasonType.DISBURSE_MANUAL.getValue())) {
            logger.debug(
                    "Disbursement for payment other than agency sweep can use only reason with busTypeCd as DBR only");
            throw new InvalidDataException(INVALID_REASON);
        }
        BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                .findById(new BilCashEntryTotId(postedDate, batchId, userId)).orElse(null);
        if (bilCashEntryTot == null) {
            throw new InvalidDataException("no.data.found");
        }
        if (bilUnidentifiedCash.getBilCshEtrMthCd() == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()) {
            checkForAgencySweepPayment(bilUnidentifiedCash, payableOption, disburseType,
                    multiCurrencyService.getCurrencySymbolByCode(bilCashEntryTot.getCurrencyCd()));
        } else {
            fieldLevelAuthenticationService.validateAmount("UNIDDISBURSEMENT", bilUnidentifiedCash.getBilRctAmt(),
                    multiCurrencyService.getCurrencySymbolByCode(bilCashEntryTot.getCurrencyCd()));
        }
    }

    private void checkForAgencySweepPayment(BilUnIdCash bilUnidentifiedCash, PayableOption payableOption,
            String disburseType, String currency) {

        if (payableOption.getRefundMethod().equals(DisbursementType.ELECTRONIC.getDisburseType())
                && !disburseType.trim().equals(BusCodeTranslationType.DISBURSE_AGENCY_SWEEP.getValue())) {
            logger.debug(
                    "Disbursement with combination electronic disburse with reason type other than DAS not allowed");
            throw new InvalidDataException(INVALID_REASON);
        }
        if ((payableOption.getRefundMethod().equals(DisbursementType.SYSTEM.getDisburseType())
                || payableOption.getRefundMethod().equals(DisbursementType.MANUAL.getDisburseType()))
                && disburseType.trim().equals(BusCodeTranslationType.DISBURSE_AGENCY_SWEEP.getValue())) {
            logger.debug("Disbursement with combination system/manual with reason type as DAS not allowed");
            throw new InvalidDataException(INVALID_REASON);
        }
        if (payableOption.getRefundMethod().equals(DisbursementType.ELECTRONIC.getDisburseType())
                && disburseType.trim().equals(BusCodeTranslationType.DISBURSE_AGENCY_SWEEP.getValue())
                || payableOption.getRefundMethod().equals(DisbursementType.SYSTEM.getDisburseType())
                        && !disburseType.trim().equals(BusCodeTranslationType.DISBURSE_SUSPENSE_TYPE.getValue())) {
            fieldLevelAuthenticationService.validateAmount("AGSWEEPDISB", bilUnidentifiedCash.getBilRctAmt(), currency);
        } else {
            fieldLevelAuthenticationService.validateAmount("UNIDDISBURSEMENT", bilUnidentifiedCash.getBilRctAmt(),
                    currency);
        }
    }

    private boolean checkPendingAchPayment(String bilUnidCashId, ZonedDateTime distributionDate,
            char cashEntryMethodCode) {
        List<Character> bilEftRecordTypeList = new ArrayList<>();
        boolean isRowOf1XCC = false;
        bilEftRecordTypeList.add(EftRecordType.RECONCILED_CASH.getValue());
        bilEftRecordTypeList.add(EftRecordType.UNIDENTIFIED.getValue());
        bilEftRecordTypeList.add(EftRecordType.ONE_TIME_ACH.getValue());
        bilEftRecordTypeList.add(EftRecordType.AGENCY_SWEEP.getValue());

        if (cashEntryMethodCode == CashEntryMethod.CREDIT_CARD.getCashEntryMethod()) {
            isRowOf1XCC = checkIfEFTRow(bilUnidCashId);
        }
        if (cashEntryMethodCode == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()
                || cashEntryMethodCode == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod() || isRowOf1XCC) {
            ZonedDateTime insertedTimeStamp = bilEftPendingTapeRepository.getTimeStamp(bilUnidCashId, distributionDate,
                    SHORT_ZERO, bilEftRecordTypeList, TechnicalKeyTypeCode.UNIDENTIFIED.getValue());
            if (insertedTimeStamp != null) {
                return true;
            }
        }
        return false;
    }

    private boolean checkIfEFTRow(String accountId) {
        List<Character> bilEftRecordTypeList = new ArrayList<>();
        bilEftRecordTypeList.add(EftRecordType.RECONCILED_CASH.getValue());
        bilEftRecordTypeList.add(EftRecordType.UNIDENTIFIED.getValue());
        Integer rowCount = bilEftPendingTapeRepository.checkIfExists(accountId, bilEftRecordTypeList);
        return rowCount != null && rowCount > SHORT_ZERO;
    }

    private void validateCheckNumber(PayableOption payableOption) {
        logger.debug("Check number cannot be less than zero");
        if (payableOption.getCheckNumber() < SHORT_ZERO) {
            throw new InvalidDataException(BAD_REQUEST_DATA_INVALID, new Object[] { "checkNumber" });
        }
    }

    private void formatRefundMethod(PayableOption payableOption) {
        BusCdTranslation supportData = busCdTranslationRepository.findByDescriptionAndType(
                payableOption.getRefundMethod(), BilDesReasonType.REFUND_METHOD.getValue(), execContext.getLanguage());
        if (supportData == null) {
            throw new InvalidDataException("support.data.not.found",
                    new Object[] { payableOption.getRefundMethod(), BilDesReasonType.REFUND_METHOD.getValue() });
        }
        payableOption.setRefundMethod(supportData.getBusCdTranslationId().getBusCd().trim());
    }

    private String formatReason(PayableOption payableOption) {
        logger.debug("fetch reason with busType as DBR");
        BusCdTranslation supportData = busCdTranslationRepository.findByDescriptionAndType(payableOption.getReason(),
                BilDesReasonType.DISBURSE_MANUAL.getValue(), execContext.getLanguage());
        String disburseType = BilDesReasonType.DISBURSE_MANUAL.getValue();
        if (supportData == null) {
            logger.debug("fetch reason with busType as DAD");
            supportData = busCdTranslationRepository.findByDescriptionAndType(payableOption.getReason().trim(),
                    BusCodeTranslationType.DISBURSE_SUSPENSE_TYPE.getValue(), execContext.getLanguage());
            disburseType = BusCodeTranslationType.DISBURSE_SUSPENSE_TYPE.getValue();
            if (supportData == null) {
                logger.debug("fetch reason with busType as DAS");
                supportData = busCdTranslationRepository.findByDescriptionAndType(payableOption.getReason().trim(),
                        BusCodeTranslationType.DISBURSE_AGENCY_SWEEP.getValue(), execContext.getLanguage());
                disburseType = BusCodeTranslationType.DISBURSE_AGENCY_SWEEP.getValue();
                if (supportData == null) {
                    throw new InvalidDataException("support.data.not.found",
                            new Object[] { payableOption.getReason(), BilDesReasonType.DISBURSE_MANUAL.getValue() });
                }
            }
        }
        payableOption.setReason(supportData.getBusCdTranslationId().getBusCd());
        return disburseType;
    }

    public void setSequenceNumber(Short sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public void setDistributionDate(String distributionDate) {
        this.distributionDate = distributionDate;
    }
}
