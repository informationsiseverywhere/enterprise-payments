package billing.validation;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import billing.model.Payment;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.CommonConstants;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PaymentWalletCardValidator implements Validator {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    private static final String WALLET_TYPE = "walletType";

    @Override
    public boolean supports(Class<?> clazz) {
        return Payment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Payment payment = (Payment) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, WALLET_TYPE, "bad.request.missing.parameter",
                new Object[] { WALLET_TYPE });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "walletNumber", "bad.request.missing.parameter",
                new Object[] { "walletNumber" });

        format(payment, errors);
    }

    private void format(Payment payment, Errors errors) {
        formatWalletType(payment, errors);
        formatPostalCode(payment);
        formatExpirationDate(payment);
        formatWalletIdentifier(payment);
    }

    private void formatWalletIdentifier(Payment payment) {
        if (payment.getWalletIdentifier() == null) {
            payment.setWalletIdentifier(SEPARATOR_BLANK);
        }
    }

    private void formatExpirationDate(Payment payment) {
        if (payment.getCreditCardExpiryDate() == null) {
            payment.setCreditCardExpiryDate(SEPARATOR_BLANK);
        }

    }

    private void formatPostalCode(Payment payment) {
        if (payment.getPostalCode() == null) {
            payment.setPostalCode(CommonConstants.BLANK_STRING);
        }

    }

    private void formatWalletType(Payment payment, Errors errors) {
        final boolean isPatch = execContext.isPatchRequest();
        if (!isPatch || payment.getCreditCardType() != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    payment.getWalletType(), BusCodeTranslationType.DIGITAL_WALLET_TYPE.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                payment.setWalletType(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue(WALLET_TYPE, "bad.request.data.invalid", new Object[] { WALLET_TYPE },
                        CommonConstants.BLANK_STRING);
            }
        }
    }

}
