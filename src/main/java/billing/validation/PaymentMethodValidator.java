package billing.validation;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_G;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_W;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_Z;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import billing.data.entity.BilAccount;
import billing.data.entity.BilAgent;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilThirdParty;
import billing.data.entity.id.BilEftBankId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilAgentRepository;
import billing.data.repository.BilEftBankRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilThirdPartyRepository;
import billing.model.Payment;
import billing.utils.BillingConstants.AccountType;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BillingMethod;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.PaymentType;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.CommonConstants;
import core.utils.DateRoutine;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PaymentMethodValidator implements Validator {

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilThirdPartyRepository bilThirdPartyRepository;

    @Autowired
    private BilAgentRepository bilAgentRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BankAccountValidator bankAccountValidator;

    @Autowired
    private AgencySweepValidator agencySweepValidator;

    @Autowired
    private CreditCardValidator creditCardValidator;

    private static final String PAYMENT_METHOD = "paymentMethod";
    private static final String PAYMENT_TYPE = "paymentType";
    private static final String COLLECTION_METHOD = "collectionMethod";
    private static final String IDENTIFIER = "identifier";
    private static final String BAD_ACCOUNT_DATA = "bad.account.data";

    @Override
    public boolean supports(Class<?> clazz) {
        return Payment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Payment payment = (Payment) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, PAYMENT_METHOD, "bad.request.missing.parameter",
                new Object[] { PAYMENT_METHOD });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, PAYMENT_TYPE, "bad.request.missing.parameter",
                new Object[] { PAYMENT_TYPE });

        if (!errors.getAllErrors().isEmpty()) {
            return;
        }

        format(payment, errors);

        validateData(errors, payment);

    }

    private void validateData(Errors errors, Payment payment) {
        if (PaymentType.CREDIT_CARD.getValue().equalsIgnoreCase(payment.getPaymentMethod())) {
            ValidationUtils.invokeValidator(creditCardValidator, payment, errors);
        }

        if (PaymentType.BANK_ACCOUNT.getValue().equalsIgnoreCase(payment.getPaymentMethod())) {
            ValidationUtils.invokeValidator(bankAccountValidator, payment, errors);
        }

        if (PaymentType.AGENCY_SWEEP.getValue().equalsIgnoreCase(payment.getPaymentMethod())) {
            ValidationUtils.invokeValidator(agencySweepValidator, payment, errors);
        }

        String identifierType = readBusCdTranslation(payment);

        if (identifierType.equalsIgnoreCase(AccountType.AGENCY_ACCOUNT.toString())) {
            errors.rejectValue(IDENTIFIER, BAD_ACCOUNT_DATA, new Object[] { IDENTIFIER }, CommonConstants.BLANK_STRING);
        }

        if (payment.getIdentifier() != null && !payment.getIdentifier().trim().equals(BLANK_STRING)) {
            validateAccountDetails(payment, errors, identifierType);

        }

        if (payment.getPolicyNumber() != null && !payment.getPolicyNumber().trim().equals(BLANK_STRING)) {
            validatePolicyDetails(payment, errors);
        }
    }

    private void validatePolicyDetails(Payment payment, Errors errors) {

        List<String> accountIdList = bilPolicyTermRepository.findAccountByPolicy(payment.getPolicyNumber(),
                payment.getPolicySymbol(), getBillPolStatusCdList(), getBilIssueIndList(),
                PageRequest.of(SHORT_ZERO, SHORT_ONE));
        if (accountIdList != null && !accountIdList.isEmpty()) {
            String accountId = accountIdList.get(SHORT_ZERO);
            BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
            if (bilAccount != null) {
                payment.setAccountId(bilAccount.getAccountId());
                payment.setIdentifierType(AccountType.BILLACCOUNT.toString());
                if (!bilAccount.getAgentAccountNumber().trim().equals(BLANK_STRING)) {
                    errors.rejectValue(IDENTIFIER, BAD_ACCOUNT_DATA, new Object[] { IDENTIFIER },
                            CommonConstants.BLANK_STRING);
                }

                payment.setIsUnidentified(false);
            } else {
                payment.setIsUnidentified(true);
                payment.setAccountId(BLANK_STRING);
            }
        }

    }

    private List<Character> getBilIssueIndList() {
        List<Character> bilIssueIndList = new ArrayList<>();
        bilIssueIndList.add(CHAR_Y);
        bilIssueIndList.add(CHAR_Z);
        return bilIssueIndList;
    }

    private List<Character> getBillPolStatusCdList() {
        List<Character> billPolStatusCdList = new ArrayList<>();
        billPolStatusCdList.add(CHAR_W);
        billPolStatusCdList.add(CHAR_T);
        billPolStatusCdList.add(CHAR_D);
        billPolStatusCdList.add(CHAR_G);
        return billPolStatusCdList;
    }

    private void validateAccountDetails(Payment payment, Errors errors, String identifierType) {

        if (identifierType.equals(AccountType.BILLACCOUNT.toString())) {
            BilAccount bilAccount = bilAccountRepository.findByAccountNumber(payment.getIdentifier());
            if (bilAccount != null) {
                payment.setAccountId(bilAccount.getAccountId());
                payment.setIsUnidentified(false);
                payment.setIdentifierType(identifierType);
                if (!bilAccount.getAgentAccountNumber().trim().equals(BLANK_STRING)) {
                    errors.rejectValue(IDENTIFIER, BAD_ACCOUNT_DATA, new Object[] { IDENTIFIER },
                            CommonConstants.BLANK_STRING);
                }
                if (Boolean.TRUE.equals(payment.getIsRecurring())) {
                    if (!bilAccount.getCollectionMethod().trim().equals(BillingMethod.DI.toString())) {
                        if (bilAccount.getCollectionMethod().trim().equals(BillingMethod.EFT.toString())
                                || bilAccount.getCollectionMethod().trim().equals(BillingMethod.ECC.toString())
                                || bilAccount.getCollectionMethod().trim()
                                        .equals(BillingMethod.ELECTRONIC_WALLET.toString())) {
                            errors.rejectValue(COLLECTION_METHOD, "bad.collection.data",
                                    new Object[] { COLLECTION_METHOD }, CommonConstants.BLANK_STRING);
                        } else {
                            errors.rejectValue(COLLECTION_METHOD, BAD_ACCOUNT_DATA, new Object[] { COLLECTION_METHOD },
                                    CommonConstants.BLANK_STRING);
                        }

                    }

                    BilEftBank bilEftBank = bilEftBankRepository
                            .findById(new BilEftBankId(payment.getAccountId(), 'A', execContext.getApplicationDate()))
                            .orElse(null);

                    if (bilEftBank != null) {
                        if (bilEftBank.getBilCrcrdTypeCd() != BLANK_CHAR
                                && payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
                            throw new DataNotFoundException("bad.card.data", new Object[] {
                                    DateRoutine.dateTimeAsMMDDYYYYAsString(execContext.getApplicationDate()) });
                        } else {

                            throw new DataNotFoundException("bad.bank.data", new Object[] {
                                    DateRoutine.dateTimeAsMMDDYYYYAsString(execContext.getApplicationDate()) });
                        }
                    }
                }
                return;
            }
        } else if (identifierType.equals(AccountType.GROUP_ACCOUNT.toString())) {
            BilThirdParty bilThirdParty = bilThirdPartyRepository.findByBilTtyNbr(payment.getIdentifier());
            if (bilThirdParty != null) {
                payment.setAccountId(bilThirdParty.getBillThirdPartyId());
                payment.setIsUnidentified(false);
                payment.setIdentifierType(identifierType);
                return;
            }
        } else if (identifierType.equals(AccountType.AGENCY_ACCOUNT.toString())) {
            BilAgent bilAgent = bilAgentRepository.findByBilAgtActNbr(payment.getIdentifier());
            if (bilAgent != null) {
                payment.setAccountId(bilAgent.getBilAgtActId());
                payment.setIsUnidentified(false);
                payment.setIdentifierType(identifierType);
                errors.rejectValue(IDENTIFIER, BAD_ACCOUNT_DATA, new Object[] { IDENTIFIER },
                        CommonConstants.BLANK_STRING);
                return;
            }
        }

        payment.setAccountId(BLANK_STRING);
        payment.setIsUnidentified(true);

    }

    private void format(Payment payment, Errors errors) {
        formatPaymentMethod(payment, errors);
        formatPaymentType(payment, errors);
        formatAdditonalValidations(payment);

    }

    private void formatAdditonalValidations(Payment payment) {

        if (payment.getDueDate() != null) {
            if (validateIfEquityPayment(payment.getPaymentType())) {
                throw new InvalidDataException("equity.payment.invalid", new Object[] { "Due date" });
            } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())
                    && validateIfDownPayment(payment.getPaymentType())) {
                throw new InvalidDataException("downpayment.invalid");
            }
        } else {
            payment.setDueDate(DateRoutine.defaultDateTime());
        }

    }

    private boolean validateIfEquityPayment(String paymentType) {
        return busCdTranslationRepository.findByCodeAndType(paymentType, BilDesReasonType.EQT.getValue(),
                execContext.getLanguage()) != null;
    }

    private boolean validateIfDownPayment(String paymentType) {
        return busCdTranslationRepository.findByCodeAndType(paymentType, BusCodeTranslationType.DOWN_PAYMENT.getValue(),
                execContext.getLanguage()) != null;
    }

    private void formatPaymentMethod(Payment payment, Errors errors) {
        if (payment.getPaymentMethod() != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    payment.getPaymentMethod(), BusCodeTranslationType.PAYMENTMETHOD.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                payment.setPaymentMethod(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue(PAYMENT_METHOD, "bad.request.data.invalid", new Object[] { PAYMENT_METHOD },
                        CommonConstants.BLANK_STRING);
            }
        }
    }

    private void formatPaymentType(Payment payment, Errors errors) {
        if (payment.getPaymentType() != null) {
            BusCdTranslation busCdTranslation = null;
            if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.CREDIT_CARD.getValue())) {
                busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(payment.getPaymentType(),
                        BilDesReasonType.CPY.getValue(), execContext.getLanguage());
            } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.BANK_ACCOUNT.getValue())) {
                busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(payment.getPaymentType(),
                        BilDesReasonType.ONE_XA.getValue(), execContext.getLanguage());
            } else if (payment.getPaymentMethod().equalsIgnoreCase(PaymentType.AGENCY_SWEEP.getValue())) {
                busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(payment.getPaymentType(),
                        BilDesReasonType.ASW.getValue(), execContext.getLanguage());
            } else {
                busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(payment.getPaymentType(),
                        BilDesReasonType.WALLET_PAYMENT_TYPE.getValue(), execContext.getLanguage());
            }
            if (busCdTranslation != null) {
                payment.setPaymentType(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue(PAYMENT_TYPE, "bad.request.data.invalid", new Object[] { PAYMENT_TYPE },
                        CommonConstants.BLANK_STRING);
            }
        }
    }

    private String readBusCdTranslation(Payment payment) {
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByDescriptionAndType(
                payment.getIdentifierType(), BusCodeTranslationType.BILLACCOUNTTYPE.getValue(),
                execContext.getLanguage());
        if (busCodeTranslation != null) {
            return busCodeTranslation.getBusCdTranslationId().getBusCd().trim();
        }
        return BLANK_STRING;
    }

}
