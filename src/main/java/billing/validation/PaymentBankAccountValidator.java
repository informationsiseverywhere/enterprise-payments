package billing.validation;

import static core.utils.CommonConstants.BLANK_STRING;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import billing.model.Payment;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PaymentBankAccountValidator implements Validator {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    private static final String ACCOUNT_TYPE = "accountType";
    private static final String BANK_ACCOUNT_NUMBER = "bankAccountNumber";

    @Override
    public boolean supports(Class<?> clazz) {
        return Payment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Payment payment = (Payment) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, ACCOUNT_TYPE, "bad.request.missing.parameter",
                new Object[] { ACCOUNT_TYPE });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, BANK_ACCOUNT_NUMBER, "bad.request.missing.parameter",
                new Object[] { BANK_ACCOUNT_NUMBER });

        format(payment, errors);

        validateEftAccountNumber(payment, errors);
    }

    private void format(Payment payment, Errors errors) {
        formatAccountType(payment, errors);
        formatRoutingNumber(payment);

    }

    private void formatRoutingNumber(Payment payment) {
        if (payment.getRoutingNumber() == null) {
            payment.setRoutingNumber(BLANK_STRING);
        }

    }

    private void validateEftAccountNumber(Payment payment, Errors errors) {
        if (payment.getBankAccountNumber() != null && !payment.getBankAccountNumber().matches("[a-zA-Z0-9*]*")) {
            errors.rejectValue(BANK_ACCOUNT_NUMBER, "bad.request.data.invalid", new Object[] { BANK_ACCOUNT_NUMBER },
                    BLANK_STRING);
        }
    }

    private void formatAccountType(Payment payment, Errors errors) {
        final boolean isPatch = execContext.isPatchRequest();
        if (!isPatch || payment.getAccountType() != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    payment.getAccountType(), BusCodeTranslationType.EFT_ACCOUNT_TYPE.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                payment.setAccountType(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue(ACCOUNT_TYPE, "bad.request.data.invalid", new Object[] { ACCOUNT_TYPE },
                        BLANK_STRING);
            }
        }
    }

}
