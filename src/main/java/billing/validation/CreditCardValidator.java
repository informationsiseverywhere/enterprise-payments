package billing.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import billing.model.Payment;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.CommonConstants;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CreditCardValidator implements Validator {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    private static final String MASTER_CARD_EXPRESSION = "^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$";
    private static final String VISA_CARD_EXPRESSION = "^4[0-9]{12}(?:[0-9]{3})?$";
    private static final String AMERICAN_CAED_EXPRESSION = "^3[47][0-9]{13}$";
    private static final String DISCOVER_CAED_EXPRESSION = "^65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])[0-9]{10})$";
    private static final String CREDIT_CARD_TYPE = "creditCardType";
    private static final String BAD_REQUEST_MISSING_PARAMETER = "bad.request.missing.parameter";
    private static final String BAD_REQUEST_DATA_INVALID = "bad.request.data.invalid";
    private static final String CREDIT_CARD_EXPIRY_DATE = "creditCardExpiryDate";
    private static final String CREDIT_CARD_NUMBER = "creditCardNumber";

    @Override
    public boolean supports(Class<?> clazz) {
        return Payment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Payment payment = (Payment) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, CREDIT_CARD_TYPE, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { CREDIT_CARD_TYPE });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, CREDIT_CARD_EXPIRY_DATE, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { CREDIT_CARD_EXPIRY_DATE });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, CREDIT_CARD_NUMBER, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { CREDIT_CARD_NUMBER });
        format(payment, errors);
    }

    private void format(Payment payment, Errors errors) {
        formatCreditCardType(payment, errors);
        formatPostalCode(payment);
        formatCardExpiryDate(payment, errors);
    }

    private void formatCardExpiryDate(Payment payment, Errors errors) {
        if (payment.getCreditCardExpiryDate() == null || payment.getCreditCardExpiryDate().length() != 4) {
            errors.rejectValue(CREDIT_CARD_EXPIRY_DATE, BAD_REQUEST_DATA_INVALID,
                    new Object[] { CREDIT_CARD_EXPIRY_DATE }, CommonConstants.BLANK_STRING);

        }
        
        int century = execContext.getApplicationDate().getYear()/100;
        if(Integer.parseInt(century + payment.getCreditCardExpiryDate().substring(2, 4)) < execContext.getApplicationDate().getYear()
        		|| (Integer.parseInt(century + payment.getCreditCardExpiryDate().substring(2, 4)) == execContext.getApplicationDate().getYear()
        		 && Integer.parseInt(payment.getCreditCardExpiryDate().substring(0, 2)) < execContext.getApplicationDate().getMonthValue())) {
        	errors.rejectValue(CREDIT_CARD_EXPIRY_DATE, "creditCardNumber.date.invalid");
        }

        payment.setCreditCardExpiryDate(payment.getCreditCardExpiryDate().substring(2, 4)
                .concat(payment.getCreditCardExpiryDate().substring(0, 2)));

    }

    private void formatPostalCode(Payment payment) {
        if (payment.getPostalCode() == null) {
            payment.setPostalCode(CommonConstants.BLANK_STRING);
        }

    }

    private void formatCreditCardType(Payment payment, Errors errors) {
        final boolean isPatch = execContext.isPatchRequest();
        if (!isPatch || payment.getCreditCardType() != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    payment.getCreditCardType(), BusCodeTranslationType.CREDIT_CARD_TYPE.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                payment.setCreditCardType(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue(CREDIT_CARD_TYPE, BAD_REQUEST_DATA_INVALID, new Object[] { CREDIT_CARD_TYPE },
                        CommonConstants.BLANK_STRING);
            }
        }

        if (payment.getCreditCardType().equals("M")) {
            if (!payment.getCreditCardNumber().matches(MASTER_CARD_EXPRESSION)) {
                errors.rejectValue(CREDIT_CARD_NUMBER, BAD_REQUEST_DATA_INVALID, new Object[] { CREDIT_CARD_NUMBER },
                        CommonConstants.BLANK_STRING);
            }
        } else if (payment.getCreditCardType().equals("V")) {
            if (!payment.getCreditCardNumber().matches(VISA_CARD_EXPRESSION)) {
                errors.rejectValue(CREDIT_CARD_NUMBER, BAD_REQUEST_DATA_INVALID, new Object[] { CREDIT_CARD_NUMBER },
                        CommonConstants.BLANK_STRING);
            }
        } else if (payment.getCreditCardType().equals("D")) {
            if (!payment.getCreditCardNumber().matches(DISCOVER_CAED_EXPRESSION)) {
                errors.rejectValue(CREDIT_CARD_NUMBER, BAD_REQUEST_DATA_INVALID, new Object[] { CREDIT_CARD_NUMBER },
                        CommonConstants.BLANK_STRING);
            }
        } else if (payment.getCreditCardType().equals("A")
                && !payment.getCreditCardNumber().matches(AMERICAN_CAED_EXPRESSION)) {
            errors.rejectValue(CREDIT_CARD_NUMBER, BAD_REQUEST_DATA_INVALID, new Object[] { CREDIT_CARD_NUMBER },
                    CommonConstants.BLANK_STRING);

        }
    }

}
