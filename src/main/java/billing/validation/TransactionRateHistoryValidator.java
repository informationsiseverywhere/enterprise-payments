package billing.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import billing.model.TransactionRateHistory;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.data.entity.id.BusCdTranslationId;
import core.translation.repository.BusCdTranslationRepository;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class TransactionRateHistoryValidator implements Validator {

    private String batchId;

    private String postedDate;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    private static final String BAD_REQUEST_MISSING_PARAMETER = "bad.request.missing.parameter";
    private static final String PAYMENT_CURRENCY = "paymentCurrency";

    @Override
    public boolean supports(Class<?> clazz) {
        return TransactionRateHistory.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        TransactionRateHistory transactionRateHistory = (TransactionRateHistory) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, PAYMENT_CURRENCY, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { PAYMENT_CURRENCY });
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "paymentAmount", BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { "paymentAmount" });
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "receivedCurrency", BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { "receivedCurrency" });
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "receivedAmount", BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { "receivedAmount" });
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rate", BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { "rate" });
        if (!errors.getAllErrors().isEmpty()) {
            return;
        }

        formatFields(transactionRateHistory);

    }

    private void formatFields(TransactionRateHistory transactionRateHistory) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository
                .findById(new BusCdTranslationId(transactionRateHistory.getPaymentCurrency(), "Currency",
                        execContext.getLanguage(), ""))
                .orElse(null);
        if (busCdTranslation == null) {
            throw new DataNotFoundException("currency.param.invalid",
                    new Object[] { PAYMENT_CURRENCY, transactionRateHistory.getPaymentCurrency() });
        }

        BusCdTranslation receivedBusCdTranslation = busCdTranslationRepository
                .findById(new BusCdTranslationId(transactionRateHistory.getReceivedCurrency(), "Currency",
                        execContext.getLanguage(), ""))
                .orElse(null);
        if (receivedBusCdTranslation == null) {
            throw new DataNotFoundException("currency.param.invalid",
                    new Object[] { PAYMENT_CURRENCY, transactionRateHistory.getReceivedCurrency() });
        }

    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

}