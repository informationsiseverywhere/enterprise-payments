package billing.validation;

import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_C;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_G;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_4;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import application.defaults.service.DefaultDataService;
import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilBank;
import billing.data.entity.BilCashEntryTot;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.repository.BilBankRepository;
import billing.data.repository.BilBankRltRepository;
import billing.data.repository.BilCashEntryTotRepository;
import billing.model.Batch;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.CommonConstants;
import core.utils.DateRoutine;
import core.utils.IdGenerator;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class BatchValidator implements Validator {

    private String batchId;

    private String postedDate;

    private String userId;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilBankRepository bilBankRepository;

    @Autowired
    private BilBankRltRepository bilBankRltRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private DefaultDataService defaultDataService;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    private static final String CONTROL_BANK = "controlBank";
    private static final String DEPOSIT_BANK = "depositBank";
    private static final String BAD_REQUEST_DATA_INVALID = "bad.request.data.invalid";

    @Override
    public boolean supports(Class<?> clazz) {
        return Batch.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Batch batch = (Batch) target;
        final boolean isPatch = execContext.isPatchRequest();
        boolean isGeneratedBatchId = false;

        if (isPatch) {
            batch.setBatchId(getBatchId());
            batch.setUserId(getUserId());
        } else {
            if (batch.getBatchId() != null) {
                if (batch.getBatchId().trim().length() <= 4) {
                    batch.setBatchId(batch.getBatchId().trim().toUpperCase());
                } else {
                    errors.rejectValue("batchId", BAD_REQUEST_DATA_INVALID, new Object[] { "batchId" },
                            CommonConstants.BLANK_STRING);
                }
            } else {
                batch.setBatchId(generateUniqueId());
                isGeneratedBatchId = true;
            }
            if (batch.getDepositBank() != null) {
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, DEPOSIT_BANK, "bad.request.missing.parameter",
                        new Object[] { DEPOSIT_BANK });
            }
        }

        if (errors != null && !errors.getAllErrors().isEmpty()) {
            return;
        }
        if (isPatch) {
            ZonedDateTime bilEntryDt = DateRoutine.dateTimeAsYYYYMMDD(getPostedDate());
            BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                    .findById(new BilCashEntryTotId(bilEntryDt, getBatchId(), batch.getUserId())).orElse(null);

            if (bilCashEntryTot != null && bilCashEntryTot.getBctAcceptInd() != CHAR_N
                    && bilCashEntryTot.getBctDepositInd() != CHAR_N) {
                throw new InvalidDataException("batch.accept.not.updated");

            }
        }

        formatFields(batch, errors);
        formatBatchId(batch, errors, isGeneratedBatchId, SHORT_ZERO);

    }

    private void formatFields(Batch batch, Errors errors) {
        formatDates(batch);
        formatBank(batch, errors);
        formatIsAccept(batch);
        formatIsDeposit(batch);
        formatPaymentMethod(batch, errors);
        formatBatchAmount(batch, errors);
        formatCurrency(batch);
    }

    private void formatCurrency(Batch batch) {
        if (batch.getCurrency() != null) {
            batch.setCurrency(multiCurrencyService.getCurrencyCode(batch.getCurrency()));
            multiCurrencyService.validateCurrencyCode(batch.getCurrency());
        }
    }

    private void formatBatchAmount(Batch batch, Errors errors) {
        if (batch.getBatchAmount() == null) {
            batch.setBatchAmount(DECIMAL_ZERO);
        } else if (batch.getBatchAmount() < 0) {
            errors.rejectValue("batchAmount", "batchAmount.invalid");
        }

    }

    private void formatPaymentMethod(Batch batch, Errors errors) {
        if (batch.getPaymentMethod() == null) {
            batch.setPaymentMethod(String.valueOf(CHAR_G));
        } else {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    batch.getPaymentMethod(), BusCodeTranslationType.PAYMENTMETHOD.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                batch.setPaymentMethod(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue("paymentMethod", BAD_REQUEST_DATA_INVALID, new Object[] { "paymentMethod" },
                        CommonConstants.BLANK_STRING);
            }
        }

    }

    private void formatIsDeposit(Batch batch) {
        if (batch.getIsDeposit() == null) {
            batch.setIsDeposit(false);
        }
    }

    private void formatIsAccept(Batch batch) {
        if (batch.getIsAccept() == null) {
            batch.setIsAccept(false);
        }

    }

    private void formatBank(Batch batch, Errors errors) {
        String controlBank = "";

        if (batch.getDepositBank() != null) {

            BilBank bilBank = bilBankRepository.findByBbkAccountNm(batch.getDepositBank());

            if (bilBank != null) {
                batch.setDepositBank(bilBank.getBilBankCd());
            } else {
                errors.rejectValue(DEPOSIT_BANK, BAD_REQUEST_DATA_INVALID, new Object[] { DEPOSIT_BANK },
                        CommonConstants.BLANK_STRING);
            }
        } else {
            if (!execContext.isPatchRequest() && batch.getDepositBank() == null) {
                String depositBank = defaultDataService.getDefaultData("UserBatchEntryProfile", DEPOSIT_BANK);
                if (depositBank == null || depositBank.trim().isEmpty()) {
                    errors.rejectValue(DEPOSIT_BANK, BAD_REQUEST_DATA_INVALID, new Object[] { DEPOSIT_BANK },
                            CommonConstants.BLANK_STRING);
                }
                batch.setDepositBank(depositBank);
            }
        }

        if (batch.getDepositBank() != null && !batch.getDepositBank().isEmpty()) {
            List<String> bankCodeList = bilBankRltRepository.getBankCode(getDeposiBankType(), batch.getDepositBank());
            if (bankCodeList == null || bankCodeList.isEmpty()) {
                errors.rejectValue(DEPOSIT_BANK, BAD_REQUEST_DATA_INVALID, new Object[] { DEPOSIT_BANK },
                        CommonConstants.BLANK_STRING);
            } else if (bankCodeList.size() > SHORT_ONE) {
                errors.rejectValue("depositeBank", "bad.request.multiple.data", new Object[] { "depositeBank" },
                        CommonConstants.BLANK_STRING);
            } else {
                controlBank = bankCodeList.get(SHORT_ZERO);
            }

            if (controlBank.isEmpty()) {
                errors.rejectValue(CONTROL_BANK, BAD_REQUEST_DATA_INVALID);
            }
        }

        if (batch.getControlBank() != null) {
            BilBank bilBank = bilBankRepository.findByBbkAccountNm(batch.getControlBank());

            if (bilBank != null) {
                List<String> bankCodeList = bilBankRltRepository.getBankCode(getControlBankType(),
                        bilBank.getBilBankCd());
                if (bankCodeList == null || bankCodeList.isEmpty() || bankCodeList.size() > SHORT_ONE) {
                    errors.rejectValue(CONTROL_BANK, "bad.request.relation.data", new Object[] { CONTROL_BANK },
                            CommonConstants.BLANK_STRING);
                } else {
                    if (!bankCodeList.get(SHORT_ZERO).trim().equals(controlBank)) {
                        errors.rejectValue(CONTROL_BANK, "bad.request.relation.data", new Object[] { CONTROL_BANK },
                                CommonConstants.BLANK_STRING);
                    }
                    batch.setControlBank(controlBank);
                }
            } else {
                errors.rejectValue(CONTROL_BANK, BAD_REQUEST_DATA_INVALID, new Object[] { CONTROL_BANK },
                        CommonConstants.BLANK_STRING);
            }

        } else {
            batch.setControlBank(controlBank);
        }
    }

    private List<Character> getControlBankType() {
        List<Character> controlBankList = new ArrayList<>();
        controlBankList.add(CHAR_B);
        controlBankList.add(CHAR_C);
        return controlBankList;

    }

    private List<Character> getDeposiBankType() {
        List<Character> controlBankList = new ArrayList<>();
        controlBankList.add(CHAR_B);
        controlBankList.add(CHAR_D);
        return controlBankList;

    }

    private void formatDates(Batch batch) {
        assignDates(batch);
    }

    private void assignDates(Batch batch) {
        final boolean isPatch = execContext.isPatchRequest();
        if (!isPatch) {
            if (batch.getPostedDate() == null) {
                batch.setPostedDate(execContext.getApplicationDate());
            }
            if (batch.getDepositDate() == null) {
                batch.setDepositDate(DateRoutine.defaultDateTime());
            }
        }
    }

    private String generateUniqueId() {
        return IdGenerator.generateUniqueIdUpperCase(GENERATED_ID_LENGTH_4);
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    private void formatBatchId(Batch batch, Errors errors, boolean isGeneratedBatchId, short retryCount) {

        final boolean isPatch = execContext.isPatchRequest();
        if (!isPatch && bilCashEntryTotRepository.existsById(
                new BilCashEntryTotId(batch.getPostedDate(), batch.getBatchId(), execContext.getUserSeqeunceId()))) {
            if (isGeneratedBatchId) {
                if (retryCount <= 2) {
                    batch.setBatchId(generateUniqueId());
                    retryCount++;
                    formatBatchId(batch, errors, isGeneratedBatchId, retryCount);
                } else {
                    throw new InvalidDataException("batch.add.not.valid", new Object[] { batch.getBatchId(),
                            DateRoutine.dateTimeAsMMDDYYYYAsString(batch.getPostedDate()) });
                }

            } else {
                throw new InvalidDataException("batch.add.not.valid", new Object[] { batch.getBatchId(),
                        DateRoutine.dateTimeAsMMDDYYYYAsString(batch.getPostedDate()) });
            }
        }
    }
}
