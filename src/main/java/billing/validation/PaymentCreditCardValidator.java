package billing.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import billing.model.Payment;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.CommonConstants;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PaymentCreditCardValidator implements Validator {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    private static final String BAD_REQUEST_MISSING_PARAMETER = "bad.request.missing.parameter";
    private static final String CREDIT_CARD_TYPE = "creditCardType";

    @Override
    public boolean supports(Class<?> clazz) {
        return Payment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Payment payment = (Payment) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, CREDIT_CARD_TYPE, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { CREDIT_CARD_TYPE });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creditCardExpiryDate", BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { "creditCardExpiryDate" });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creditCardNumber", BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { "creditCardNumber" });
        format(payment, errors);
    }

    private void format(Payment payment, Errors errors) {
        formatCreditCardType(payment, errors);
        formatPostalCode(payment);
    }

    private void formatPostalCode(Payment payment) {
        if (payment.getPostalCode() == null) {
            payment.setPostalCode(CommonConstants.BLANK_STRING);
        }

    }

    private void formatCreditCardType(Payment payment, Errors errors) {
        final boolean isPatch = execContext.isPatchRequest();
        if (!isPatch || payment.getCreditCardType() != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    payment.getCreditCardType(), BusCodeTranslationType.CREDIT_CARD_TYPE.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                payment.setCreditCardType(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue(CREDIT_CARD_TYPE, "bad.request.data.invalid", new Object[] { CREDIT_CARD_TYPE },
                        CommonConstants.BLANK_STRING);
            }
        }
    }

}
