package billing.validation;

import static core.utils.CommonConstants.BLANK_STRING;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import billing.data.entity.BilEftBankEd;
import billing.data.repository.BilEftBankEdRepository;
import billing.model.Payment;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class BankAccountValidator implements Validator {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilEftBankEdRepository bilEftBankEdRepository;

    private static final String ACCOUNT_TYPE = "accountType";
    private static final String BAD_REQUEST_MISSING_PARAMETER = "bad.request.missing.parameter";
    private static final String BAD_REQUEST_DATA_INVALID = "bad.request.data.invalid";
    private static final String BANK_ACCOUNT_NUMBER = "bankAccountNumber";
    private static final String ROUTING_NUMBER = "routingNumber";

    @Override
    public boolean supports(Class<?> clazz) {
        return Payment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Payment payment = (Payment) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, ACCOUNT_TYPE, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { ACCOUNT_TYPE });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, BANK_ACCOUNT_NUMBER, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { BANK_ACCOUNT_NUMBER });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, ROUTING_NUMBER, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { ROUTING_NUMBER });

        validateRoutingNumber(payment, errors);
        validateEftAccountNumber(payment, errors);
        if (errors != null && !errors.getAllErrors().isEmpty()) {
            return;
        }
        format(payment, errors);

    }

    private void format(Payment payment, Errors errors) {
        formatAccountType(payment, errors);

    }

    private void validateRoutingNumber(Payment payment, Errors errors) {

        BilEftBankEd bilEftBankEd = bilEftBankEdRepository.findByRoutingTransitNbr(payment.getRoutingNumber());
        if (bilEftBankEd == null) {
            errors.rejectValue(ROUTING_NUMBER, BAD_REQUEST_DATA_INVALID, new Object[] { ROUTING_NUMBER }, BLANK_STRING);
        }

    }

    private void validateEftAccountNumber(Payment payment, Errors errors) {
        if (payment.getBankAccountNumber() != null && !payment.getBankAccountNumber().matches("[a-zA-Z0-9]*")) {
            errors.rejectValue(BANK_ACCOUNT_NUMBER, BAD_REQUEST_DATA_INVALID, new Object[] { BANK_ACCOUNT_NUMBER },
                    BLANK_STRING);
        }
    }

    private void formatAccountType(Payment payment, Errors errors) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                payment.getAccountType(), BusCodeTranslationType.EFT_ACCOUNT_TYPE.getValue(),
                execContext.getLanguage());
        if (busCdTranslation != null) {
            payment.setAccountType(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
        } else {
            errors.rejectValue(ACCOUNT_TYPE, BAD_REQUEST_DATA_INVALID, new Object[] { ACCOUNT_TYPE }, BLANK_STRING);
        }
    }

}
