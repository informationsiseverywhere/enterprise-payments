package billing.validation;

import static core.utils.CommonConstants.BLANK_STRING;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import billing.model.Payment;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.api.ExecContext;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.CommonConstants;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PaymentusCreditCardValidator implements Validator {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    private static final String PAYMENT_PROFILE_ID = "paymentProfileId";
    private static final String CREDIT_CARD_TYPE = "creditCardType";
    private static final String BAD_REQUEST_MISSING_PARAMETER = "bad.request.missing.parameter";
    
    @Override
    public boolean supports(Class<?> clazz) {
        return Payment.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Payment payment = (Payment) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, CREDIT_CARD_TYPE, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { CREDIT_CARD_TYPE });

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, PAYMENT_PROFILE_ID, BAD_REQUEST_MISSING_PARAMETER,
                new Object[] { PAYMENT_PROFILE_ID });

        format(payment, errors);
    }

    private void format(Payment payment, Errors errors) {
        formatCreditCardType(payment, errors);
        formatPostalCode(payment);
        formartPaymentProfileId(payment, errors);
        formatCustomerProfileId(payment);
        formatCreditCardExpiryDate(payment);
        formatCreditCardNumber(payment);
    }

    private void formatCreditCardNumber(Payment payment) {
        if (payment.getCreditCardNumber() == null) {
            payment.setCreditCardNumber(BLANK_STRING);
        }

    }

    private void formatCreditCardExpiryDate(Payment payment) {
        if (payment.getCreditCardExpiryDate() == null) {
            payment.setCreditCardExpiryDate(BLANK_STRING);
        }

    }

    private void formatCustomerProfileId(Payment payment) {
        if (payment.getCustomerProfileId() == null) {
            payment.setCustomerProfileId(BLANK_STRING);
        }
    }

    private void formartPaymentProfileId(Payment payment, Errors errors) {
        if (payment.getPaymentProfileId() == null) {
            errors.rejectValue(PAYMENT_PROFILE_ID, "paymentProfileId.can.not.null");
        }

    }

    private void formatPostalCode(Payment payment) {
        if (payment.getPostalCode() == null) {
            payment.setPostalCode(CommonConstants.BLANK_STRING);
        }

    }

    private void formatCreditCardType(Payment payment, Errors errors) {
        final boolean isPatch = execContext.isPatchRequest();
        if (!isPatch || payment.getCreditCardType() != null) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository.findByDescriptionAndType(
                    payment.getCreditCardType(), BusCodeTranslationType.CREDIT_CARD_TYPE.getValue(),
                    execContext.getLanguage());
            if (busCdTranslation != null) {
                payment.setCreditCardType(busCdTranslation.getBusCdTranslationId().getBusCd().trim());
            } else {
                errors.rejectValue(CREDIT_CARD_TYPE, "bad.request.data.invalid", new Object[] { CREDIT_CARD_TYPE },
                        CommonConstants.BLANK_STRING);
            }
        }
    }
}
