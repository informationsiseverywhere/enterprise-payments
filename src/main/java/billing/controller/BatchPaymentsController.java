package billing.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.UriTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import billing.model.Payment;
import billing.service.BatchPaymentService;
import billing.service.options.BatchPaymentOptionsImpl;
import billing.validation.BatchPaymentValidator;
import core.api.ExecContext;
import core.model.Paging;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import core.utils.DateRoutine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/batches/{batchId}/payments")
@Api(tags = { "Batch Payment" }, description = "Batch Payment Operations")
public class BatchPaymentsController {

    @Autowired
    private BatchPaymentService batchPaymentService;

    @Autowired
    private BatchPaymentOptionsImpl batchPaymentOptionsImpl;

    @Autowired
    private BatchPaymentValidator batchPaymentValidator;

    @Value("${billing.url}")
    private String externalBillingService;

    @Autowired
    private ExecContext execContext;

    private static final int INITIAL_PAGE = 1;
    private static final int INITIAL_PAGE_SIZE = 10;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(batchPaymentValidator);
    }

    @InitBinder("batchId")
    protected void initBinder(WebDataBinder binder, @PathVariable("batchId") String batchId,
            @RequestParam("postedDate") String postedDate,
            @RequestParam(value = "userId", required = false) String userId,
            @PathVariable(value = "paymentSequenceNumber", required = false) String paymentSequenceNumber) {
        batchPaymentValidator.setBatchId(batchId);
        batchPaymentValidator.setPostedDate(postedDate);
        batchPaymentValidator.setUserId(userId);
        batchPaymentValidator.setPaymentSequenceNumber(paymentSequenceNumber);
        binder.setValidator(batchPaymentValidator);
    }

    @UserAuthorization("createBatchPayment")
    @PostMapping
    @ApiOperation(value = "Create a Batch Payment")
    public HttpEntity<Payment> createBatchPayment(@PathVariable("batchId") String batchId,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @Valid @RequestBody Payment batchPayment) {
        Short paymentSeq = batchPaymentService.saveBatchPayment(batchId, postedDate, batchPayment);
        if (paymentSeq == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Payment savedBatchPayment = new Payment();
        savedBatchPayment.add(linkTo(methodOn(BatchPaymentsController.class).findBatchPayment(batchId, paymentSeq,
                postedDate, execContext.getUserSeqeunceId())).withRel("payment").expand());
        savedBatchPayment.add(linkTo(methodOn(BatchPaymentsController.class).findBatchPayments(batchId, postedDate,
                execContext.getUserSeqeunceId(), null, null, null, INITIAL_PAGE, INITIAL_PAGE_SIZE)).withRel("payments")
                        .expand());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(
                ServletUriComponentsBuilder.fromCurrentRequest().path("/{batchId}").buildAndExpand(batchId).toUri());
        savedBatchPayment.setBatchId(batchId);
        savedBatchPayment.setPostedDate(DateRoutine.dateTimeAsYYYYMMDD(postedDate));
        savedBatchPayment.setPaymentSequenceNumber(paymentSeq);
        savedBatchPayment.setUserId(execContext.getUserSeqeunceId());
        return new ResponseEntity<>(savedBatchPayment, httpHeaders, HttpStatus.CREATED);

    }

    @UserAuthorization("findBatchPayments")
    @GetMapping
    @ApiOperation(value = "Get All Batch Payments by batch Identifiers")
    public HttpEntity<Resources<Resource<Payment>>> findBatchPayments(@PathVariable("batchId") String batchId,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @RequestParam(value = "userId", required = true) String userId,
            @RequestParam(value = "query", required = false) String query,
            @RequestParam(value = "filters", required = false) String filters,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {
        List<Resource<Payment>> batchPaymentResource = new ArrayList<>();
        List<Link> links = new ArrayList<>();

        Paging paging = new Paging();
        int evalPageSize = size == null ? INITIAL_PAGE_SIZE : size;
        int evalPage = page == null || page < 1 ? INITIAL_PAGE : page;

        List<Payment> batchPaymentList = batchPaymentService.findAllBatchPayments(postedDate, batchId, query, filters,
                sort, evalPageSize, evalPage, paging, userId);

        if (batchPaymentList != null && !batchPaymentList.isEmpty()) {
            for (Payment batchPayment : batchPaymentList) {
                String id = batchPayment.getBatchId();
                String postedDt = DateRoutine.dateTimeAsYYYYMMDDString(batchPayment.getPostedDate());
                short seqNbr = batchPayment.getPaymentSequenceNumber();

                Link batchPaymentLink = linkTo(
                        methodOn(BatchPaymentsController.class).findBatchPayment(id, seqNbr, postedDt, userId))
                                .withRel("payment").expand();

                batchPaymentResource.add(new Resource<Payment>(batchPayment, batchPaymentLink));
                if (Boolean.TRUE.equals(batchPayment.getIsEdit())) {
                    batchPayment.add(linkTo(methodOn(BatchPaymentsController.class).patchBatchPayment(batchId,
                            batchPayment.getPaymentSequenceNumber(), postedDate, userId, batchPayment)).withRel("edit")
                                    .expand());
                }

                if (Boolean.TRUE.equals(batchPayment.getIsDelete())) {
                    batchPayment.add(linkTo(methodOn(BatchPaymentsController.class).deleteBatchPayment(batchId,
                            batchPayment.getPaymentSequenceNumber(), postedDate, userId)).withRel("delete").expand());
                }

                Link item = linkTo(
                        methodOn(BatchPaymentsController.class).findBatchPayment(id, seqNbr, postedDt, userId))
                                .withRel("item").expand();
                links.add(item);
            }
            if (paging.getCurrentPage() > 0) {
                links.add(linkTo(methodOn(BatchPaymentsController.class).findBatchPayments(batchId, postedDate, userId,
                        query, filters, sort, paging.getCurrentPage(), paging.getPageSize())).withRel("current")
                                .expand());
            }
            if (paging.getFirstPage() > 0) {
                links.add(linkTo(methodOn(BatchPaymentsController.class).findBatchPayments(batchId, postedDate, userId,
                        query, filters, sort, paging.getFirstPage(), paging.getPageSize())).withRel("first").expand());
            }
            if (paging.getLastPage() > 0) {
                links.add(linkTo(methodOn(BatchPaymentsController.class).findBatchPayments(batchId, postedDate, userId,
                        query, filters, sort, paging.getLastPage(), paging.getPageSize())).withRel("last").expand());
            }
            if (paging.getNextPage() > 0) {
                links.add(linkTo(methodOn(BatchPaymentsController.class).findBatchPayments(batchId, postedDate, userId,
                        query, filters, sort, paging.getNextPage(), paging.getPageSize())).withRel("next").expand());
            }
            if (paging.getPreviousPage() > 0) {
                links.add(linkTo(methodOn(BatchPaymentsController.class).findBatchPayments(batchId, postedDate, userId,
                        query, filters, sort, paging.getPreviousPage(), paging.getPageSize())).withRel("previous")
                                .expand());
            }
        }
        Link self = linkTo(methodOn(BatchPaymentsController.class).findBatchPayments(batchId, postedDate, userId, query,
                filters, sort, page, size)).withSelfRel().expand();
        Link parent = linkTo(methodOn(BatchController.class).findBatch(batchId, postedDate, userId)).withRel("up")
                .expand();
        links.add(self);
        links.add(parent);

        Resources<Resource<Payment>> batchPaymentResources = new Resources<>(batchPaymentResource, links);
        return new ResponseEntity<>(batchPaymentResources, HttpStatus.OK);
    }

    @UserAuthorization("findBatchPayment")
    @GetMapping(value = "/{paymentSequenceNumber}")
    @ApiOperation(value = "Find Batch Payment by batch Identifiers")
    public HttpEntity<Payment> findBatchPayment(@PathVariable("batchId") String batchId,
            @PathVariable(value = "paymentSequenceNumber") short paymentSequenceNumber,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @RequestParam(value = "userId", required = true) String userId) {
        batchId = batchId.toUpperCase();
        Payment batchPayment = batchPaymentService.findBatchPaymentDetails(batchId, postedDate, paymentSequenceNumber,
                userId);
        batchPayment.add(linkTo(methodOn(BatchPaymentsController.class).findBatchPayment(batchId, paymentSequenceNumber,
                postedDate, batchPayment.getUserId())).withSelfRel().expand());
        batchPayment
                .add(linkTo(methodOn(BatchController.class).findBatch(batchId, postedDate, batchPayment.getUserId()))
                        .withRel("up").expand());

        if (Boolean.TRUE.equals(!batchPayment.getIsUnidentified())) {
            Link accountLink = new Link(new UriTemplate(externalBillingService + batchPayment.getAccountId()) + "?type="
                    + batchPayment.getType(), "identifier").expand();
            batchPayment.add(accountLink);
        }
        return new ResponseEntity<>(batchPayment, HttpStatus.OK);
    }

    @UserAuthorization("deleteBatchPayment")
    @DeleteMapping(value = "/{paymentSequenceNumber}")
    @ApiOperation(value = "Delete a Batch Payment")
    public HttpEntity<Payment> deleteBatchPayment(@PathVariable("batchId") String batchId,
            @PathVariable(value = "paymentSequenceNumber") short paymentSequenceNumber,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @RequestParam(value = "userId", required = true) String userId) {
        batchId = batchId.toUpperCase();
        batchPaymentService.deleteBatchPayment(batchId, postedDate, paymentSequenceNumber, userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @UserAuthorization("patchBatchPayment")
    @PatchMapping(value = "/{paymentSequenceNumber}")
    @ApiOperation(value = "Update a Batch Payment")
    public HttpEntity<Payment> patchBatchPayment(@PathVariable("batchId") String batchId,
            @PathVariable(value = "paymentSequenceNumber") short paymentSequenceNumber,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @RequestParam(value = "userId", required = true) String userId, @Valid @RequestBody Payment batchPayment) {
        batchId = batchId.toUpperCase();
        batchPaymentService.patchBatchPayment(batchId, postedDate, paymentSequenceNumber, batchPayment, userId);
        Payment savedBatchPayment = new Payment();
        savedBatchPayment.add(linkTo(methodOn(BatchPaymentsController.class).findBatchPayment(batchId,
                paymentSequenceNumber, postedDate, userId)).withSelfRel().expand());
        return new ResponseEntity<>(savedBatchPayment, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Batch Payment Collection")
    public HttpEntity<OptionsResponseEntity> batchPaymentCollectionOptions(@PathVariable("batchId") String batchId,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @RequestParam(value = "userId", required = false) String userId,
            @RequestParam(value = "query", required = false) String query,
            @RequestParam(value = "filters", required = false) String filters,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {
        OptionsResponseEntity response = null;
        response = batchPaymentOptionsImpl.getCollectionsResponse(Payment.class, batchId, postedDate, query, filters,
                sort, page, size, userId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/{paymentSequenceNumber}", method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Batch Payment Resource")
    public HttpEntity<OptionsResponseEntity> batchPaymentResourceOptions(@PathVariable("batchId") String batchId,
            @PathVariable(value = "paymentSequenceNumber") short paymentSequenceNumber,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @RequestParam(value = "userId", required = true) String userId) {
        OptionsResponseEntity response = null;
        response = batchPaymentOptionsImpl.getResourceResponse(Payment.class, batchId, paymentSequenceNumber,
                postedDate, userId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
