package billing.controller;

import static core.utils.CommonConstants.BLANK_STRING;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import billing.model.TransactionRateHistory;
import billing.service.TransactionRateHistoryService;
import billing.service.options.TransactionRateHistoryOptionsImpl;
import billing.validation.TransactionRateHistoryValidator;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/batches/{batchId}/payments/{paymentSequenceNumber}/transactionRateHistory")
@Api(tags = { "Currency Exchange Rate" }, description = "Currency Exchange Rate Operations")
public class TransactionRateHistoryController {

    @Autowired
    private TransactionRateHistoryService transactionRateHistoryService;

    @Autowired
    private TransactionRateHistoryOptionsImpl transactionRateHistoryOptionsImpl;

    @Autowired
    private TransactionRateHistoryValidator transactionRateHistoryValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(transactionRateHistoryValidator);
    }

    @UserAuthorization("findTransactionRateHistory")
    @GetMapping
    @ApiOperation(value = "Find Transaction Rate History")
    public HttpEntity<TransactionRateHistory> findTransactionRateHistory(@PathVariable("batchId") String batchId,
            @PathVariable(value = "paymentSequenceNumber") short paymentSequenceNumber,
            @RequestParam(value = "postedDate", required = true) String postedDate) {
        TransactionRateHistory transactionRateHistory = transactionRateHistoryService
                .findTransactionRateHistory(batchId, postedDate, paymentSequenceNumber);
        transactionRateHistory.add(linkTo(methodOn(TransactionRateHistoryController.class)
                .findTransactionRateHistory(batchId, paymentSequenceNumber, postedDate)).withSelfRel().expand());
        return new ResponseEntity<>(transactionRateHistory, HttpStatus.OK);
    }

    @UserAuthorization("deleteTransactionRateHistory")
    @DeleteMapping
    @ApiOperation(value = "delete Transaction Rate History")
    public HttpEntity<TransactionRateHistory> deleteTransactionRateHistory(@PathVariable("batchId") String batchId,
            @PathVariable(value = "paymentSequenceNumber") short paymentSequenceNumber,
            @RequestParam(value = "postedDate", required = true) String postedDate) {
        transactionRateHistoryService.deleteTransactionRateHistory(batchId, postedDate, paymentSequenceNumber);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @UserAuthorization("createTransactionRateHistory")
    @PostMapping()
    @ApiOperation(value = "Create Transaction Rate History")
    public HttpEntity<TransactionRateHistory> createTransactionRateHistory(@PathVariable("batchId") String batchId,
            @PathVariable(value = "paymentSequenceNumber") short paymentSequenceNumber,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @Valid @RequestBody TransactionRateHistory transactionRateHistory) {
        transactionRateHistoryService.postTransactionRateHistory(batchId, postedDate, paymentSequenceNumber,
                transactionRateHistory);
        transactionRateHistory.add(linkTo(methodOn(TransactionRateHistoryController.class)
                .findTransactionRateHistory(batchId, paymentSequenceNumber, postedDate)).withSelfRel().expand());
        transactionRateHistory.add(linkTo(methodOn(BatchPaymentsController.class).findBatchPayment(batchId,
                paymentSequenceNumber, postedDate, BLANK_STRING)).withRel("up").expand());
        return new ResponseEntity<>(transactionRateHistory, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Transaction Rate History Collection")
    public HttpEntity<OptionsResponseEntity> transactionRateHistoryCollectionOptions(
            @PathVariable("batchId") String batchId,
            @PathVariable(value = "paymentSequenceNumber") short paymentSequenceNumber,
            @RequestParam(value = "postedDate", required = true) String postedDate) {
        OptionsResponseEntity response = null;
        response = transactionRateHistoryOptionsImpl.getCollectionsResponse(TransactionRateHistory.class, batchId,
                paymentSequenceNumber, postedDate);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}