package billing.controller;

import static core.utils.CommonConstants.BLANK_STRING;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import billing.kafka.config.ProducerConfig;
import billing.kafka.service.BatchPaymentProducerService;
import billing.model.Batch;
import billing.service.BatchService;
import billing.service.CashReceiptService;
import billing.service.options.BatchOptionsImpl;
import billing.utils.PB360Service;
import billing.validation.BatchValidator;
import core.api.ExecContext;
import core.model.Paging;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import core.utils.DateRoutine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/batches")
@Api(tags = { "Batch" }, description = "Batch Operations")
public class BatchController {

    @Autowired
    private BatchService batchService;

    @Autowired
    private BatchOptionsImpl batchOptionsImpl;

    @Autowired
    private BatchValidator batchValidator;

    @Autowired
    private PB360Service pb360Service;

    @Autowired
    private ExecContext execContext;
    
    @Autowired
    private BatchPaymentProducerService porducerConfig;
        
    @Value("${scheduleActivities}")
    private String scheduleActivities;

    private static final int INITIAL_PAGE = 1;
    private static final int INITIAL_PAGE_SIZE = 10;
    private static final String BATCH = "batch";

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(batchValidator);
    }

    @InitBinder("batchId")
    protected void initBinder(WebDataBinder binder, @PathVariable("batchId") String batchId,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId) {
        batchValidator.setBatchId(batchId);
        batchValidator.setPostedDate(postedDate);
        batchValidator.setUserId(userId);
        binder.setValidator(batchValidator);
    }

    @UserAuthorization("findBatches")
    @GetMapping
    @ApiOperation(value = "Get All Batches")
    public ResponseEntity<Resources<Resource<Batch>>> findBatches(
            @RequestParam(value = "query", required = false) String query,
            @RequestParam(value = "filters", required = false) String filters,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {
        List<Resource<Batch>> batchResource = new ArrayList<>();
        List<Link> links = new ArrayList<>();
        Paging paging = new Paging();
        int evalPageSize = size == null ? INITIAL_PAGE_SIZE : size;
        int evalPage = page == null || page < 1 ? INITIAL_PAGE : page;

        List<Batch> batchList = batchService.findAllBatches(query, filters, sort, evalPageSize, evalPage, paging);
        if (batchList != null && !batchList.isEmpty()) {

            for (Batch batch : batchList) {
                batch.setDepositBank(BLANK_STRING);
                batch.setControlBank(BLANK_STRING);
                batch.setPaymentMethod(BLANK_STRING);
                batch.setCashAmount(null);
                batch.setNonCashAmount(null);
                Link batchLink = linkTo(methodOn(BatchController.class).findBatch(batch.getBatchId(),
                        DateRoutine.dateTimeAsYYYYMMDDString(batch.getPostedDate()), batch.getUserId())).withRel(BATCH)
                                .expand();
                if (Boolean.TRUE.equals(batch.getIsAccept())) {
                    batch.add(linkTo(methodOn(BatchController.class).patchBatch(batch.getBatchId(),
                            DateRoutine.dateTimeAsYYYYMMDDString(batch.getPostedDate()), batch.getUserId(), null))
                                    .withRel("accept").expand());
                }
                if (Boolean.TRUE.equals(batch.getIsDeposit())) {
                    batch.add(linkTo(methodOn(BatchController.class).patchBatch(batch.getBatchId(),
                            DateRoutine.dateTimeAsYYYYMMDDString(batch.getPostedDate()), batch.getUserId(), null))
                                    .withRel("deposit").expand());
                }
                if (Boolean.TRUE.equals(batch.getIsEdit())) {
                    batch.add(linkTo(methodOn(BatchController.class).patchBatch(batch.getBatchId(),
                            DateRoutine.dateTimeAsYYYYMMDDString(batch.getPostedDate()), batch.getUserId(), null))
                                    .withRel("edit").expand());
                }
                if (Boolean.TRUE.equals(batch.getIsDelete())) {
                    batch.add(linkTo(methodOn(BatchController.class).deleteBatch(batch.getBatchId(),
                            DateRoutine.dateTimeAsYYYYMMDDString(batch.getPostedDate()), batch.getUserId()))
                                    .withRel("delete").expand());
                }
                if (Boolean.TRUE.equals(batch.getIsAcceptAndDeposit())) {
                    batch.add(linkTo(methodOn(BatchController.class).patchBatch(batch.getBatchId(),
                            DateRoutine.dateTimeAsYYYYMMDDString(batch.getPostedDate()), batch.getUserId(), null))
                                    .withRel("acceptAndDeposit").expand());
                }
                if (batch.getNumberOfTransactions() > 0) {
                    Link paymentLink = linkTo(methodOn(BatchPaymentsController.class).findBatchPayments(
                            batch.getBatchId(), DateRoutine.dateTimeAsYYYYMMDDString(batch.getPostedDate()),
                            batch.getUserId(), null, null, null, INITIAL_PAGE, INITIAL_PAGE_SIZE)).withRel("payments")
                                    .expand();
                    batchResource.add(new Resource<Batch>(batch, batchLink, paymentLink));
                } else {
                    batchResource.add(new Resource<Batch>(batch, batchLink));
                }

                Link item = linkTo(methodOn(BatchController.class).findBatch(batch.getBatchId(),
                        DateRoutine.dateTimeAsYYYYMMDDString(batch.getPostedDate()), batch.getUserId())).withRel("item")
                                .expand();
                links.add(item);
            }
            if (paging.getCurrentPage() > 0) {
                links.add(linkTo(methodOn(BatchController.class).findBatches(query, filters, sort,
                        paging.getCurrentPage(), paging.getPageSize())).withRel("current").expand());
            }
            if (paging.getFirstPage() > 0) {
                links.add(linkTo(methodOn(BatchController.class).findBatches(query, filters, sort,
                        paging.getFirstPage(), paging.getPageSize())).withRel("first").expand());
            }
            if (paging.getLastPage() > 0) {
                links.add(linkTo(methodOn(BatchController.class).findBatches(query, filters, sort, paging.getLastPage(),
                        paging.getPageSize())).withRel("last").expand());
            }
            if (paging.getNextPage() > 0) {
                links.add(linkTo(methodOn(BatchController.class).findBatches(query, filters, sort, paging.getNextPage(),
                        paging.getPageSize())).withRel("next").expand());
            }
            if (paging.getPreviousPage() > 0) {
                links.add(linkTo(methodOn(BatchController.class).findBatches(query, filters, sort,
                        paging.getPreviousPage(), paging.getPageSize())).withRel("previous").expand());
            }
        }
        Link self = linkTo(methodOn(BatchController.class).findBatches(query, filters, sort, page, size)).withSelfRel()
                .expand();
        Link parent = linkTo(methodOn(EnterprisePaymentController.class).paymentOptions()).withRel("up").expand();
        links.add(self);
        links.add(parent);

        Resources<Resource<Batch>> batchResources = new Resources<>(batchResource, links);
        return new ResponseEntity<>(batchResources, HttpStatus.OK);
    }

    @UserAuthorization("findBatch")
    @ApiOperation(value = "Get Batch by batchId")
    @GetMapping(value = "/{batchId}")
    public HttpEntity<Batch> findBatch(@PathVariable("batchId") String batchId,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @RequestParam(value = "userId", required = true) String userId) {
        batchId = batchId.toUpperCase();
        Batch batch = batchService.findBatchDetails(batchId, postedDate, userId);
        batch.add(linkTo(methodOn(BatchController.class).findBatch(batchId, postedDate, batch.getUserId()))
                .withSelfRel().expand());
        batch.add(linkTo(methodOn(BatchPaymentsController.class).findBatchPayments(batchId, postedDate, userId, null,
                null, null, INITIAL_PAGE, INITIAL_PAGE_SIZE)).withRel("batchPayments").expand());
        batch.add(linkTo(methodOn(EnterprisePaymentController.class).paymentOptions()).withRel("up").expand());
        return new ResponseEntity<>(batch, HttpStatus.OK);
    }

    @UserAuthorization("createBatch")
    @ApiOperation(value = "Create Batch")
    @PostMapping
    public HttpEntity<Batch> createBatch(@Valid @RequestBody Batch batch) {

        String bilEntryDt = DateRoutine.dateTimeAsYYYYMMDDString(batch.getPostedDate());
        String batchId = batchService.save(batch);
        Batch savedBatch = new Batch();
        savedBatch.add(
                linkTo(methodOn(BatchController.class).findBatch(batchId, bilEntryDt, execContext.getUserSeqeunceId()))
                        .withRel(BATCH).expand());
        savedBatch.add(
                linkTo(methodOn(BatchController.class).findBatches(null, null, null, INITIAL_PAGE, INITIAL_PAGE_SIZE))
                        .withRel("batches").expand());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(
                ServletUriComponentsBuilder.fromCurrentRequest().path("/{batchId}").buildAndExpand(batchId).toUri());
        savedBatch.setBatchId(batchId);
        savedBatch.setPostedDate(batch.getPostedDate());
        savedBatch.setUserId(execContext.getUserSeqeunceId());
        return new ResponseEntity<>(savedBatch, httpHeaders, HttpStatus.CREATED);

    }

    @UserAuthorization("deleteBatch")
    @ApiOperation(value = "Delete Batch")
    @DeleteMapping(value = "/{batchId}")
    public HttpEntity<Batch> deleteBatch(@PathVariable("batchId") String batchId,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @RequestParam(value = "userId", required = true) String userId) {
        batchId = batchId.toUpperCase();
        String message = batchService.deleteBatch(batchId, postedDate, userId);
        if (message.compareTo("Success") != 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @UserAuthorization("patchBatch")
    @ApiOperation(value = "Update Batch")
    @PatchMapping(value = "/{batchId}")
    public HttpEntity<Batch> patchBatch(@PathVariable("batchId") String batchId,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @RequestParam(value = "userId", required = true) String userId, @Valid @RequestBody Batch batch) {
        //pb360Service.checkPMStatus();
        batchId = batchId.toUpperCase();
        batchService.patchBatch(batchId, postedDate, batch, userId);
        if (BooleanUtils.toBoolean(scheduleActivities)) {
            batchService.scheduleBatch(batchId, postedDate, userId);
        } else {
        	porducerConfig.sendMessage(batchId+";"+ postedDate+";"+ userId);
            
        }

        Batch updatedBatch = new Batch();
        updatedBatch.add(
                linkTo(methodOn(BatchController.class).findBatch(batchId, postedDate, userId)).withRel(BATCH).expand());
        updatedBatch.add(
                linkTo(methodOn(BatchController.class).findBatches(null, null, null, INITIAL_PAGE, INITIAL_PAGE_SIZE))
                        .withRel("batches").expand());
        return new ResponseEntity<>(updatedBatch, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Batch Collection")
    public HttpEntity<OptionsResponseEntity> batchCollectionOptions(
            @RequestParam(value = "query", required = false) String query,
            @RequestParam(value = "filters", required = false) String filters,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {
        OptionsResponseEntity response = null;
        response = batchOptionsImpl.getCollectionsResponse(Batch.class, query, filters, sort, page, size);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/{batchId}", method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Batch Resource")
    public HttpEntity<OptionsResponseEntity> batchResourceOptions(@PathVariable("batchId") String batchId,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @RequestParam(value = "userId", required = true) String userId) {
        OptionsResponseEntity response = null;
        response = batchOptionsImpl.getResourceResponse(Batch.class, batchId, postedDate, userId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
