package billing.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import billing.model.CurrencyExchange;
import billing.service.ExchangeRateService;
import core.model.Paging;
import core.security.annotation.UserAuthorization;
import core.utils.DateRoutine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/utils/exchangeRates")
@Api(tags = { "Currency Exchange Rate" }, description = "Currency Exchange Rate Operations")
public class ExchangeRateController {

    @Autowired
    private ExchangeRateService exchangeRateService;

    private static final int INITIAL_PAGE = 1;
    private static final int INITIAL_PAGE_SIZE = 10;

    @UserAuthorization("searchExchangeRates")
    @GetMapping
    @ApiOperation(value = "Search all Currency Conversions")
    public HttpEntity<Resources<Resource<CurrencyExchange>>> findAllExchangeRate(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "filters", required = false) String filters) {
        List<Resource<CurrencyExchange>> currencyExchangeResource = new ArrayList<>();
        List<Link> links = new ArrayList<>();
        Paging paging = new Paging();

        int evalPageSize = size == null || size < 1 ? INITIAL_PAGE_SIZE : size;
        int evalPage = page == null || page < 1 ? INITIAL_PAGE : page;

        List<CurrencyExchange> currencyExchangeList = exchangeRateService.findAllExchangeRates(filters, evalPageSize,
                evalPage, paging);
        if (currencyExchangeList != null && !currencyExchangeList.isEmpty()) {
            for (CurrencyExchange exchangeRate : currencyExchangeList) {
                Link exchangeRateLink = linkTo(methodOn(ExchangeRateController.class).findExchangeRate(
                        DateRoutine.dateTimeAsYYYYMMDDString(exchangeRate.getExchangeDate()),
                        exchangeRate.getFromCurrency(), exchangeRate.getToCurrency(), null)).withRel("exchangeRate")
                                .expand();
                currencyExchangeResource.add(new Resource<CurrencyExchange>(exchangeRate, exchangeRateLink));

                Link item = linkTo(methodOn(ExchangeRateController.class).findExchangeRate(
                        DateRoutine.dateTimeAsYYYYMMDDString(exchangeRate.getExchangeDate()),
                        exchangeRate.getFromCurrency(), exchangeRate.getToCurrency(), null)).withRel("item").expand();
                links.add(item);
            }
            if (paging.getCurrentPage() > 0) {
                links.add(linkTo(methodOn(ExchangeRateController.class).findAllExchangeRate(paging.getCurrentPage(),
                        paging.getPageSize(), filters)).withRel("current").expand());
            }
            if (paging.getFirstPage() > 0) {
                links.add(linkTo(methodOn(ExchangeRateController.class).findAllExchangeRate(paging.getFirstPage(),
                        paging.getPageSize(), filters)).withRel("first").expand());
            }
            if (paging.getLastPage() > 0) {
                links.add(linkTo(methodOn(ExchangeRateController.class).findAllExchangeRate(paging.getLastPage(),
                        paging.getPageSize(), filters)).withRel("last").expand());
            }
            if (paging.getNextPage() > 0) {
                links.add(linkTo(methodOn(ExchangeRateController.class).findAllExchangeRate(paging.getNextPage(),
                        paging.getPageSize(), filters)).withRel("next").expand());
            }
            if (paging.getPreviousPage() > 0) {
                links.add(linkTo(methodOn(ExchangeRateController.class).findAllExchangeRate(paging.getPreviousPage(),
                        paging.getPageSize(), filters)).withRel("previous").expand());
            }
        }
        Link self = linkTo(methodOn(ExchangeRateController.class).findAllExchangeRate(page, size, filters))
                .withSelfRel().expand();
        Link parent = linkTo(methodOn(EnterprisePaymentController.class).paymentOptions()).withRel("up").expand();
        links.add(self);
        links.add(parent);

        Resources<Resource<CurrencyExchange>> currencyExchangeResources = new Resources<>(currencyExchangeResource,
                links);
        return new ResponseEntity<>(currencyExchangeResources, HttpStatus.OK);
    }

    @UserAuthorization("findExchangeRate")
    @GetMapping(value = "/{exchangeDate}")
    @ApiOperation(value = "Find a ExchangeRate")
    public HttpEntity<CurrencyExchange> findExchangeRate(@PathVariable("exchangeDate") String exchangeDate,
            @RequestParam("fromCurrency") String fromCurrency, @RequestParam("toCurrency") String toCurrency,
            @RequestParam(value = "fromAmount", required = false) Double fromAmount) {
        CurrencyExchange currencyExchange = exchangeRateService.findExchangeRate(exchangeDate, fromCurrency, toCurrency,
                fromAmount);
        currencyExchange.add(linkTo(methodOn(ExchangeRateController.class).findExchangeRate(exchangeDate, fromCurrency,
                toCurrency, fromAmount)).withSelfRel().expand());
        currencyExchange.add(linkTo(
                methodOn(ExchangeRateController.class).findAllExchangeRate(INITIAL_PAGE, INITIAL_PAGE_SIZE, null))
                        .withRel("up").expand());
        return new ResponseEntity<>(currencyExchange, HttpStatus.OK);
    }

    @UserAuthorization("patchExchangeRate")
    @PatchMapping(value = "/{exchangeDate}")
    @ApiOperation(value = "Patch ExchangeRate")
    public HttpEntity<CurrencyExchange> patchExchangeRate(@PathVariable("exchangeDate") String exchangeDate,
            @RequestParam("fromCurrency") String fromCurrency, @RequestParam("toCurrency") String toCurrency,
            @RequestBody CurrencyExchange currencyExchange) {
        exchangeRateService.patchExchangeRate(exchangeDate, fromCurrency, toCurrency, currencyExchange);
        CurrencyExchange saveCurrencyExchange = new CurrencyExchange();
        saveCurrencyExchange.add(linkTo(
                methodOn(ExchangeRateController.class).findExchangeRate(exchangeDate, fromCurrency, toCurrency, null))
                        .withSelfRel().expand());
        saveCurrencyExchange.add(linkTo(
                methodOn(ExchangeRateController.class).findAllExchangeRate(INITIAL_PAGE, INITIAL_PAGE_SIZE, null))
                        .withRel("up").expand());
        return new ResponseEntity<>(saveCurrencyExchange, HttpStatus.OK);
    }

}