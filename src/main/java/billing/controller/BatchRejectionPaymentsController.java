package billing.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import billing.model.RejectionPayment;
import billing.service.BatchRejectionPaymentService;
import billing.service.options.BatchRejectionPaymentOptionsImpl;
import billing.validation.BatchRejectionPaymentValidator;
import core.model.Paging;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import core.utils.DateRoutine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/rejectionBatches/{batchId}/payments")
@Api(tags = { "Batch Rejection Payment" }, description = "Batch Rejection Payment Operations")
public class BatchRejectionPaymentsController {

    @Autowired
    private BatchRejectionPaymentService batchRejectionPaymentService;

    @Autowired
    private BatchRejectionPaymentOptionsImpl batchRejectionPaymentOptionsImpl;

    @Autowired
    private BatchRejectionPaymentValidator batchRejectionPaymentValidator;

    @Value("${billing.url}")
    private String externalBillingService;

    private static final int INITIAL_PAGE = 1;
    private static final int INITIAL_PAGE_SIZE = 10;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(batchRejectionPaymentValidator);
    }

    @InitBinder("batchId")
    protected void initBinder(WebDataBinder binder, @PathVariable("batchId") String batchId,
            @RequestParam("entryDate") String entryDate) {
        batchRejectionPaymentValidator.setBatchId(batchId);
        batchRejectionPaymentValidator.setEntryDate(entryDate);
        binder.setValidator(batchRejectionPaymentValidator);
    }

    @UserAuthorization("createBatchRejectionPayment")
    @PostMapping
    @ApiOperation(value = "Create a Batch Payment")
    public HttpEntity<RejectionPayment> createBatchPayment(@PathVariable("batchId") String batchId,
            @RequestParam(value = "entryDate", required = true) String entryDate,
            @Valid @RequestBody RejectionPayment batchPayment) {
        Short paymentSeq = batchRejectionPaymentService.saveBatchPayment(batchId, entryDate, batchPayment);
        if (paymentSeq == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        RejectionPayment savedBatchPayment = new RejectionPayment();
        savedBatchPayment.add(linkTo(
                methodOn(BatchRejectionPaymentsController.class).findBatchPayment(batchId, paymentSeq, entryDate))
                        .withRel("payment").expand());
        savedBatchPayment.add(linkTo(methodOn(BatchRejectionPaymentsController.class).findBatchPayments(batchId,
                entryDate, null, null, null, INITIAL_PAGE, INITIAL_PAGE_SIZE)).withRel("payments").expand());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(
                ServletUriComponentsBuilder.fromCurrentRequest().path("/{batchId}").buildAndExpand(batchId).toUri());
        savedBatchPayment.setBatchId(batchId);
        savedBatchPayment.setEntryDate(DateRoutine.dateTimeAsYYYYMMDD(entryDate));
        savedBatchPayment.setPaymentSequenceNumber(paymentSeq);
        return new ResponseEntity<>(savedBatchPayment, httpHeaders, HttpStatus.CREATED);

    }

    @UserAuthorization("findBatchRejectionPayments")
    @GetMapping
    @ApiOperation(value = "Get All Batch Rejection Payments by batch Identifiers")
    public HttpEntity<Resources<Resource<RejectionPayment>>> findBatchPayments(@PathVariable("batchId") String batchId,
            @RequestParam(value = "entryDate", required = true) String entryDate,
            @RequestParam(value = "query", required = false) String query,
            @RequestParam(value = "filters", required = false) String filters,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {
        List<Resource<RejectionPayment>> batchPaymentResource = new ArrayList<>();
        List<Link> links = new ArrayList<>();

        Paging paging = new Paging();
        int evalPageSize = size == null ? INITIAL_PAGE_SIZE : size;
        int evalPage = page == null || page < 1 ? INITIAL_PAGE : page;

        List<RejectionPayment> batchRejectionPaymentList = batchRejectionPaymentService.findAllBatchPayments(entryDate,
                batchId, query, filters, sort, evalPageSize, evalPage, paging);

        if (batchRejectionPaymentList != null && !batchRejectionPaymentList.isEmpty()) {
            for (RejectionPayment rejectionPayment : batchRejectionPaymentList) {
                String id = rejectionPayment.getBatchId();
                String entryDt = DateRoutine.dateTimeAsYYYYMMDDString(rejectionPayment.getEntryDate());
                short seqNbr = rejectionPayment.getPaymentSequenceNumber();

                Link batchPaymentLink = linkTo(
                        methodOn(BatchRejectionPaymentsController.class).findBatchPayment(id, seqNbr, entryDt))
                                .withRel("payment").expand();

                batchPaymentResource.add(new Resource<RejectionPayment>(rejectionPayment, batchPaymentLink));

                Link item = linkTo(
                        methodOn(BatchRejectionPaymentsController.class).findBatchPayment(id, seqNbr, entryDt))
                                .withRel("item").expand();
                links.add(item);
            }
            if (paging.getCurrentPage() > 0) {
                links.add(linkTo(methodOn(BatchRejectionPaymentsController.class).findBatchPayments(batchId, entryDate,
                        query, filters, sort, paging.getCurrentPage(), paging.getPageSize())).withRel("current")
                                .expand());
            }
            if (paging.getFirstPage() > 0) {
                links.add(linkTo(methodOn(BatchRejectionPaymentsController.class).findBatchPayments(batchId, entryDate,
                        query, filters, sort, paging.getFirstPage(), paging.getPageSize())).withRel("first").expand());
            }
            if (paging.getLastPage() > 0) {
                links.add(linkTo(methodOn(BatchRejectionPaymentsController.class).findBatchPayments(batchId, entryDate,
                        query, filters, sort, paging.getLastPage(), paging.getPageSize())).withRel("last").expand());
            }
            if (paging.getNextPage() > 0) {
                links.add(linkTo(methodOn(BatchRejectionPaymentsController.class).findBatchPayments(batchId, entryDate,
                        query, filters, sort, paging.getNextPage(), paging.getPageSize())).withRel("next").expand());
            }
            if (paging.getPreviousPage() > 0) {
                links.add(linkTo(methodOn(BatchRejectionPaymentsController.class).findBatchPayments(batchId, entryDate,
                        query, filters, sort, paging.getPreviousPage(), paging.getPageSize())).withRel("previous")
                                .expand());
            }
        }
        Link self = linkTo(methodOn(BatchRejectionPaymentsController.class).findBatchPayments(batchId, entryDate, query,
                filters, sort, page, size)).withSelfRel().expand();
        Link parent = linkTo(methodOn(BatchRejectionController.class).findBatch(batchId, entryDate)).withRel("up")
                .expand();
        links.add(self);
        links.add(parent);

        Resources<Resource<RejectionPayment>> batchPaymentResources = new Resources<>(batchPaymentResource, links);
        return new ResponseEntity<>(batchPaymentResources, HttpStatus.OK);
    }

    @UserAuthorization("findBatchRejectionPayment")
    @GetMapping(value = "/{paymentSequenceNumber}")
    @ApiOperation(value = "Find Batch Rejection Payment by batch Identifiers")
    public HttpEntity<RejectionPayment> findBatchPayment(@PathVariable("batchId") String batchId,
            @PathVariable(value = "paymentSequenceNumber") short paymentSequenceNumber,
            @RequestParam(value = "entryDate", required = true) String entryDate) {
        batchId = batchId.toUpperCase();
        RejectionPayment batchPayment = batchRejectionPaymentService.findBatchPaymentDetails(batchId, entryDate,
                paymentSequenceNumber);
        batchPayment.add(linkTo(methodOn(BatchRejectionPaymentsController.class).findBatchPayment(batchId,
                paymentSequenceNumber, entryDate)).withSelfRel().expand());
        batchPayment.add(
                linkTo(methodOn(BatchRejectionController.class).findBatch(batchId, entryDate)).withRel("up").expand());

        return new ResponseEntity<>(batchPayment, HttpStatus.OK);
    }

    @UserAuthorization("deleteBatchRejectionPayment")
    @DeleteMapping(value = "/{paymentSequenceNumber}")
    @ApiOperation(value = "Delete a Batch Rejection Payment")
    public HttpEntity<RejectionPayment> deleteBatchPayment(@PathVariable("batchId") String batchId,
            @PathVariable(value = "paymentSequenceNumber") short paymentSequenceNumber,
            @RequestParam(value = "entryDate", required = true) String entryDate) {
        batchId = batchId.toUpperCase();
        batchRejectionPaymentService.deleteBatchPayment(batchId, entryDate, paymentSequenceNumber);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @UserAuthorization("patchBatchRejectionPayment")
    @PatchMapping(value = "/{paymentSequenceNumber}")
    @ApiOperation(value = "Update a Batch Rejection Payment")
    public HttpEntity<RejectionPayment> patchBatchPayment(@PathVariable("batchId") String batchId,
            @PathVariable(value = "paymentSequenceNumber") short paymentSequenceNumber,
            @RequestParam(value = "entryDate", required = true) String entryDate,
            @Valid @RequestBody RejectionPayment rejectionPayment) {
        batchId = batchId.toUpperCase();
        batchRejectionPaymentService.patchBatchPayment(batchId, entryDate, paymentSequenceNumber, rejectionPayment);
        RejectionPayment savedBatchPayment = new RejectionPayment();
        savedBatchPayment.add(linkTo(methodOn(BatchRejectionPaymentsController.class).findBatchPayment(batchId,
                paymentSequenceNumber, entryDate)).withSelfRel().expand());
        return new ResponseEntity<>(savedBatchPayment, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Batch Payment Collection")
    public HttpEntity<OptionsResponseEntity> batchPaymentCollectionOptions(@PathVariable("batchId") String batchId,
            @RequestParam(value = "entryDate", required = true) String entryDate,
            @RequestParam(value = "query", required = false) String query,
            @RequestParam(value = "filters", required = false) String filters,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {
        OptionsResponseEntity response = null;
        response = batchRejectionPaymentOptionsImpl.getCollectionsResponse(RejectionPayment.class, batchId, entryDate,
                query, filters, sort, page, size);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/{paymentSequenceNumber}", method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Batch Payment Resource")
    public HttpEntity<OptionsResponseEntity> batchPaymentResourceOptions(@PathVariable("batchId") String batchId,
            @PathVariable(value = "paymentSequenceNumber") short paymentSequenceNumber,
            @RequestParam(value = "entryDate", required = true) String entryDate) {
        OptionsResponseEntity response = null;
        response = batchRejectionPaymentOptionsImpl.getResourceResponse(RejectionPayment.class, batchId,
                paymentSequenceNumber, entryDate);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
