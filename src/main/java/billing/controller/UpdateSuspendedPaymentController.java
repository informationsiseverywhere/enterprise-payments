package billing.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import billing.model.SuspendedPayment;
import billing.service.UpdateSuspendedPaymentService;
import billing.service.options.UpdateSuspendedPaymentOptionImpl;
import billing.utils.PB360Service;
import billing.validation.UpdateSuspendedPaymentValidator;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import core.utils.DateRoutine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/suspendedPayments/{sequenceNumber}/_update")
@Api(tags = { "Update Suspended Payments" }, description = "Update Suspended Payment Operations")
public class UpdateSuspendedPaymentController {

    @Autowired
    private UpdateSuspendedPaymentValidator updateSuspendedPaymentValidator;

    @Autowired
    private UpdateSuspendedPaymentService updateSuspendedPaymentService;

    @Autowired
    private UpdateSuspendedPaymentOptionImpl updateSuspendedPaymentOptionImpl;

    @Autowired
    private PB360Service pb360Service;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(updateSuspendedPaymentValidator);
    }

    @InitBinder("sequenceNumber")
    protected void initBinder(WebDataBinder binder, @PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate) {
        binder.setValidator(updateSuspendedPaymentValidator);
        updateSuspendedPaymentValidator.setSequenceNumber(sequenceNumber);
        updateSuspendedPaymentValidator.setPostedDate(postedDate);
        updateSuspendedPaymentValidator.setUser(userId);
        updateSuspendedPaymentValidator.setBatchId(batchId);
        updateSuspendedPaymentValidator.setDistributionDate(distributionDate);
    }

    @UserAuthorization("updateSuspendedPayment")
    @ApiOperation(value = "Update Suspended Payments")
    @PostMapping
    public HttpEntity<SuspendedPayment> save(@PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate,
            @Valid @RequestBody SuspendedPayment suspendedPayment) {
        pb360Service.checkPMStatus();
        updateSuspendedPaymentService.save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate), userId, batchId,
                DateRoutine.dateTimeAsYYYYMMDD(distributionDate), suspendedPayment);
        SuspendedPayment saveSuspendedPayment = new SuspendedPayment();
        saveSuspendedPayment.add(linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayment(sequenceNumber,
                postedDate, userId, batchId, distributionDate)).withRel("payment").expand());
        return new ResponseEntity<>(saveSuspendedPayment, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Update Suspended Payment")
    public HttpEntity<OptionsResponseEntity> updateSuspendedPaymentOptions(
            @PathVariable("sequenceNumber") Short sequenceNumber, @RequestParam("postedDate") String postedDate,
            @RequestParam("userId") String userId, @RequestParam("batchId") String batchId,
            @RequestParam("distributionDate") String distributionDate) {
        OptionsResponseEntity response = updateSuspendedPaymentOptionImpl.getDocumentResponse(SuspendedPayment.class,
                sequenceNumber, postedDate, userId, batchId, distributionDate);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
