package billing.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import billing.model.PayableOption;
import billing.service.DisbursementSuspendPaymentService;
import billing.service.options.DisbursementSuspendPaymentOptionsImpl;
import billing.validation.DisbursementSuspendPaymentValidator;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import core.utils.DateRoutine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/suspendedPayments/{sequenceNumber}/_disburse")
@Api(tags = { "Disburse Suspended Payment" }, description = "Disburse Suspended Payment Operations")
public class DisburseSuspendedPaymentController {

    @Autowired
    private DisbursementSuspendPaymentValidator disbursementSuspendPaymentValidator;

    @Autowired
    private DisbursementSuspendPaymentService disbursementSuspendPaymentService;

    @Autowired
    private DisbursementSuspendPaymentOptionsImpl disbursementSuspendPaymentOptionsImpl;

    @InitBinder
    protected void initBinder(WebDataBinder binder, @PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate) {
        binder.setValidator(disbursementSuspendPaymentValidator);
        disbursementSuspendPaymentValidator.setSequenceNumber(sequenceNumber);
        disbursementSuspendPaymentValidator.setPostedDate(postedDate);
        disbursementSuspendPaymentValidator.setUser(userId);
        disbursementSuspendPaymentValidator.setBatchId(batchId);
        disbursementSuspendPaymentValidator.setDistributionDate(distributionDate);
    }

    @UserAuthorization("postDisbursement")
    @PostMapping
    @ApiOperation(value = "Disburse Suspended Payment")
    public HttpEntity<PayableOption> save(@PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate,
            @Valid @RequestBody PayableOption payableOption) {
        disbursementSuspendPaymentService.save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate), userId,
                batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate), payableOption);
        PayableOption savedPayableOption = new PayableOption();
        savedPayableOption.add(linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayment(sequenceNumber,
                postedDate, userId, batchId, distributionDate)).withRel("payment").expand());
        return new ResponseEntity<>(savedPayableOption, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options response for Disbursement")
    public HttpEntity<OptionsResponseEntity> disbursementOptions(@PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate) {
        OptionsResponseEntity response = disbursementSuspendPaymentOptionsImpl.getDocumentResponse(PayableOption.class,
                sequenceNumber, postedDate, userId, batchId, distributionDate);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
