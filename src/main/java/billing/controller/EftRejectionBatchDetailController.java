package billing.controller;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import billing.service.EftRejectionBatchDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/rejectionBatches/{batchId}/_report")
@Api(tags = { "Eft Rejection Batch Detail" }, description = "Download the Rejection Batch Detail Report")
public class EftRejectionBatchDetailController {

    private static final String REPORT_ID = "eftRejectionBatchDetail";
    private static final String CONTENT_TYPE_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    @Autowired
    private EftRejectionBatchDetailService eftRejectionBatchDetailService;

    @GetMapping
    @ApiOperation(value = "Generating the Eft Rejection Batch Detail Report")
    public HttpEntity<String> findReport(@PathVariable("batchId") String batchId,
            @RequestParam(value = "entryDate", required = true) String entryDate, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        String disposition = "Content-Disposition";
        XSSFWorkbook workbook = eftRejectionBatchDetailService.buildExcelDocument(batchId, entryDate, REPORT_ID);
        String attachment = "attachment; filename=\"" + REPORT_ID + SEPARATOR_BLANK + batchId + ".xlsx\"";
        response.setContentType(CONTENT_TYPE_XLSX);
        response.setHeader(disposition, attachment);
        ServletOutputStream report = response.getOutputStream();
        workbook.write(report);
        report.flush();
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
