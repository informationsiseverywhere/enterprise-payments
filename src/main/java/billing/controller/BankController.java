package billing.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import billing.service.SupportDataService;
import core.options.model.SupportData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/banks")
@Api(tags = { "Bank" }, description = "Bank Operations")
public class BankController {

    @Autowired
    private SupportDataService supportDataService;

    @GetMapping(value = "/{depositeBank}/supportData")
    @ApiOperation(value = "Get the support data description")
    public HttpEntity<SupportData> findSupportDataWithDescription(@PathVariable("depositeBank") String depositeBank) {
        SupportData supportData = supportDataService.getSupportData(depositeBank);
        List<String> supportDataList = supportData.getPropertyValues();
        if (supportDataList == null || supportDataList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(supportData, HttpStatus.OK);
    }
}
