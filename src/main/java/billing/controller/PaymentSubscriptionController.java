package billing.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.UriTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import billing.model.Payment;
import billing.service.impl.BillAccountRecurringPaymentServiceImpl;
import billing.service.options.PaymentSubscriptionOptionsImpl;
import billing.validation.PaymentSubscriptionValidator;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/subscriptions")
@Api(tags = { "Payment Subscriptions" }, description = "Payment Subscription Operations")
public class PaymentSubscriptionController {

    @Autowired
    private BillAccountRecurringPaymentServiceImpl billAccountRecurringPaymentServiceImpl;

    @Autowired
    private PaymentSubscriptionValidator paymentSubscriptionValidator;
    
    @Autowired
    private PaymentSubscriptionOptionsImpl paymentSubscriptionOptionsImpl;

    @Value("${billing.url}")
    private String externalBillingUrl;

    private static final String SUBSCRIPTIONS = "/subscriptions";

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(paymentSubscriptionValidator);
    }

    @UserAuthorization("paymentSubscriptions")
    @PostMapping
    @ApiOperation(value = "Payment Subscriptions")
    public HttpEntity<Payment> makeAPayment(@Valid @RequestBody Payment payment) {
        payment = billAccountRecurringPaymentServiceImpl.processRecurringPayments(payment);
        Payment savedPayment = new Payment();
        Link accountSubscriptionLink = new Link(
                new UriTemplate(externalBillingUrl + payment.getAccountId() + SUBSCRIPTIONS), "accountSubscription")
                        .expand();
        savedPayment.add(accountSubscriptionLink);
        return new ResponseEntity<>(savedPayment, HttpStatus.CREATED);

    }
    
    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Eft Plan Collection")
    public HttpEntity<OptionsResponseEntity> paymentCollectionOptions() {
        OptionsResponseEntity response = null;
        response = paymentSubscriptionOptionsImpl.getCollectionsResponse(Payment.class);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
