package billing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import billing.service.options.EnterprisePaymentOptionsImpl;
import core.options.model.OptionsResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing")
@Api(tags = { "Enterprise Payment" }, description = "Root API for Enterprise Payments")
public class EnterprisePaymentController {

    @Autowired
    private EnterprisePaymentOptionsImpl enterprisePaymentOptionsImpl;

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Enterprise Payment Root")
    public HttpEntity<OptionsResponseEntity> paymentOptions() {
        OptionsResponseEntity response = enterprisePaymentOptionsImpl.getResourceResponse();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}