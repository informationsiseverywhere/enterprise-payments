package billing.controller;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import billing.model.BatchDetailReport;
import billing.service.BatchDetailReportService;
import billing.service.options.BatchDetailReportOptionsImpl;
import core.options.model.OptionsResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/batches/{batchId}/_report")
@Api(tags = { "Batch Detail Report" }, description = "Download the Batch Detail Report")

public class BatchDetailReportController {

    private static final String REPORT_ID = "batchDetailReport";

    private static final String CONTENT_TYPE_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    @Autowired
    private BatchDetailReportService batchDetailReportService;

    @Autowired
    private BatchDetailReportOptionsImpl batchDetailReportOptionsImpl;

    @GetMapping
    @ApiOperation(value = "Generating the Batch Detail Report")
    public HttpEntity<String> findReport(@PathVariable("batchId") String batchId,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @RequestParam(value = "userId", required = true) String userId, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        String disposition = "Content-Disposition";
        XSSFWorkbook workbook = batchDetailReportService.buildExcelDocument(batchId, postedDate, REPORT_ID, userId);
        String attachment = "attachment; filename=\"" + REPORT_ID + SEPARATOR_BLANK + batchId + ".xlsx\"";
        response.setContentType(CONTENT_TYPE_XLSX);
        response.setHeader(disposition, attachment);
        ServletOutputStream report = response.getOutputStream();
        workbook.write(report);
        report.flush();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    public HttpEntity<OptionsResponseEntity> batchDetailReportCollectionOptions(@PathVariable("batchId") String batchId,
            @RequestParam(value = "postedDate", required = true) String postedDate,
            @RequestParam(value = "userId", required = true) String userId) {
        OptionsResponseEntity response = batchDetailReportOptionsImpl.getCollectionsResponse(BatchDetailReport.class,
                batchId, postedDate, userId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
