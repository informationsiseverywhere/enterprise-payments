package billing.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.UriTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import application.utils.service.JsonToPdfConverter;
import billing.model.Payment;
import billing.model.PaymentList;
import billing.service.PaymentService;
import billing.service.options.PaymentOptionsImpl;
import billing.utils.PB360Service;
import billing.validation.PaymentValidator;
import core.model.Paging;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import core.utils.DateRoutine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/payments")
@Api(tags = { "Payment" }, description = "Payment Operations")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentOptionsImpl paymentOptionsImpl;

    @Autowired
    private PB360Service pb360Service;

    @Autowired
    private PaymentValidator paymentValidator;

    @Autowired
    private JsonToPdfConverter jsonToPdfConverter;

    private static final int INITIAL_PAGE = 1;
    private static final int INITIAL_PAGE_SIZE = 10;
    private static final String PAYMENT_RESOURCE = "payments";
    private static final String PAYMENT_XSLT_FILE = "payment2fo.xsl";

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(paymentValidator);
    }

    @UserAuthorization("createPayment")
    @PostMapping
    @ApiOperation(value = "Create a Payment")
    public HttpEntity<Payment> makeAPayment(@Valid @RequestBody Payment payment) {
        pb360Service.checkPMStatus();
        payment = paymentService.makeAPayment(payment);

        Payment savedPayment = new Payment();
        String postedDate = DateRoutine.dateTimeAsYYYYMMDDString(payment.getPostedDate());
        savedPayment.setPaymentSequenceNumber(payment.getPaymentSequenceNumber());
        savedPayment.setIsUnidentified(payment.getIsUnidentified());
        savedPayment.setPostedDate(payment.getPostedDate());
        savedPayment.setUserId(payment.getUserId());
        savedPayment.setBatchId(payment.getBatchId());
        savedPayment.add(linkTo(methodOn(PaymentController.class).findPayment(payment.getPaymentSequenceNumber(),
                postedDate, payment.getUserId(), payment.getBatchId())).withRel("payment").expand());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder.fromCurrentRequest().path("/{paymentSeqNbr}")
                .query("&postedDate={postedDate}").query("&userId={userId}").query("&batchId={batchId}")
                .buildAndExpand(payment.getPaymentSequenceNumber(),
                        DateRoutine.dateTimeAsYYYYMMDDString(payment.getPostedDate()), payment.getUserId(),
                        payment.getBatchId())
                .toUri());
        return new ResponseEntity<>(savedPayment, httpHeaders, HttpStatus.CREATED);
    }

    @UserAuthorization("searchPayments")
    @GetMapping
    @ApiOperation(value = "Search all Payments")
    public HttpEntity<Resources<Resource<Payment>>> findPayments(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "filters", required = false) String filters) {
        List<Resource<Payment>> paymentResource = new ArrayList<>();
        List<Link> links = new ArrayList<>();

        int evalPageSize = size == null || size < 1 ? INITIAL_PAGE_SIZE : size;
        int evalPage = page == null || page < 1 ? INITIAL_PAGE : page;

        PaymentList paymentList = paymentService.findPayments(evalPage, evalPageSize, filters);
        if (paymentList != null && paymentList.getPayments() != null && !paymentList.getPayments().isEmpty()) {
            for (Payment payment : paymentList.getPayments()) {
                Link paymentLink = linkTo(
                        methodOn(PaymentController.class).findPayment(payment.getPaymentSequenceNumber(),
                                DateRoutine.dateTimeAsYYYYMMDDString(payment.getPostedDate()), payment.getUserId(),
                                payment.getBatchId())).withRel("payment").expand();
                String externalLink = paymentService.getExternalReferenceLink(payment.getType().trim(),
                        payment.getAccountId());
                Link accountLink = new Link(new UriTemplate(externalLink), PAYMENT_RESOURCE).expand();
                paymentResource.add(new Resource<Payment>(payment, paymentLink, accountLink));

                Link item = linkTo(methodOn(PaymentController.class).findPayment(payment.getPaymentSequenceNumber(),
                        DateRoutine.dateTimeAsYYYYMMDDString(payment.getPostedDate()), payment.getUserId(),
                        payment.getBatchId())).withRel("item").expand();
                links.add(item);
            }
            Paging paging = paymentList.getPage();
            if (paging.getCurrentPage() > 0) {
                links.add(linkTo(methodOn(PaymentController.class).findPayments(paging.getCurrentPage(),
                        paging.getPageSize(), filters)).withRel("current").expand());
            }
            if (paging.getFirstPage() > 0) {
                links.add(linkTo(methodOn(PaymentController.class).findPayments(paging.getFirstPage(),
                        paging.getPageSize(), filters)).withRel("first").expand());
            }
            if (paging.getLastPage() > 0) {
                links.add(linkTo(methodOn(PaymentController.class).findPayments(paging.getLastPage(),
                        paging.getPageSize(), filters)).withRel("last").expand());
            }
            if (paging.getNextPage() > 0) {
                links.add(linkTo(methodOn(PaymentController.class).findPayments(paging.getNextPage(),
                        paging.getPageSize(), filters)).withRel("next").expand());
            }
            if (paging.getPreviousPage() > 0) {
                links.add(linkTo(methodOn(PaymentController.class).findPayments(paging.getPreviousPage(),
                        paging.getPageSize(), filters)).withRel("previous").expand());
            }
        }
        Link self = linkTo(methodOn(PaymentController.class).findPayments(page, size, filters)).withSelfRel().expand();
        Link parent = linkTo(methodOn(EnterprisePaymentController.class).paymentOptions()).withRel("up").expand();
        links.add(self);
        links.add(parent);

        Resources<Resource<Payment>> paymentResources = new Resources<>(paymentResource, links);
        return new ResponseEntity<>(paymentResources, HttpStatus.OK);
    }

    @UserAuthorization("findPayment")
    @GetMapping(value = "/{sequenceNumber}")
    @ApiOperation(value = "Find a Payment")
    public HttpEntity<Payment> findPayment(@PathVariable("sequenceNumber") short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId) {
        Payment payment = paymentService.findPayment(batchId, sequenceNumber, postedDate, userId);
        payment.add(linkTo(methodOn(PaymentController.class).findPayment(sequenceNumber, postedDate, userId, batchId))
                .withSelfRel().expand());
        payment.add(linkTo(methodOn(PaymentController.class).findPayments(INITIAL_PAGE, INITIAL_PAGE_SIZE, null))
                .withRel(PAYMENT_RESOURCE).expand());
        payment.add(linkTo(methodOn(EnterprisePaymentController.class).paymentOptions()).withRel("up").expand());
        return new ResponseEntity<>(payment, HttpStatus.OK);
    }

    @UserAuthorization("findPayment")
    @GetMapping(value = "/{sequenceNumber}/pdf", produces = "application/pdf")
    @ApiOperation(value = "Generate Payment PDF")
    public HttpEntity<InputStreamResource> getPaymentPdf(@PathVariable("sequenceNumber") short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId) throws Exception {
        Payment payment = paymentService.findPayment(batchId, sequenceNumber, postedDate, userId);
        payment = paymentService.getCurrency(payment);
        String fileName = "PaymentReceipt_" + postedDate + "_" + userId + "_" + sequenceNumber + "_" + batchId + ".pdf";
        return jsonToPdfConverter.convert(payment, PAYMENT_XSLT_FILE, fileName);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Payment Collection")
    public HttpEntity<OptionsResponseEntity> paymentCollectionOptions(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "filters", required = false) String filters) {
        OptionsResponseEntity response = null;
        response = paymentOptionsImpl.getCollectionsResponse(Payment.class, page, size, filters);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/{sequenceNumber}", method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Payment Resource")
    public HttpEntity<OptionsResponseEntity> paymentResourceOptions(
            @PathVariable("sequenceNumber") short sequenceNumber, @RequestParam("postedDate") String postedDate,
            @RequestParam("userId") String userId, @RequestParam("batchId") String batchId) {
        OptionsResponseEntity response = paymentOptionsImpl.getResourceResponse(Payment.class, sequenceNumber,
                postedDate, userId, batchId, null);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/{accountId}/eftPlans", method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Eft Plans Resource")
    public HttpEntity<OptionsResponseEntity> eftPlansResourceOptions(@PathVariable("accountId") String accountId) {
        short sequenceNumber = 0;
        OptionsResponseEntity response = paymentOptionsImpl.getResourceResponse(Payment.class, sequenceNumber, null,
                null, null, accountId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}