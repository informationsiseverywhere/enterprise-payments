package billing.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import billing.model.WriteOffOption;
import billing.service.WriteOffSuspendPaymentService;
import billing.service.options.WriteOffSuspendPaymentOptionsImpl;
import billing.validation.WriteOffSuspendPaymentValidator;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import core.utils.DateRoutine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/suspendedPayments/{sequenceNumber}/_writeOff")
@Api(tags = { "Write Off Suspended Payment" }, description = "Write-Off Suspended Payment Operations")
public class WriteOffSuspendedPaymentController {

    @Autowired
    WriteOffSuspendPaymentService writeOffSuspendPaymentService;

    @Autowired
    WriteOffSuspendPaymentValidator writeOffSuspendPaymentValidator;

    @Autowired
    WriteOffSuspendPaymentOptionsImpl writeOffSuspendPaymentOptionsImpl;

    @InitBinder
    protected void initBinder(WebDataBinder binder, @PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate) {
        binder.setValidator(writeOffSuspendPaymentValidator);
        writeOffSuspendPaymentValidator.setSequenceNumber(sequenceNumber);
        writeOffSuspendPaymentValidator.setPostedDate(postedDate);
        writeOffSuspendPaymentValidator.setUser(userId);
        writeOffSuspendPaymentValidator.setBatchId(batchId);
        writeOffSuspendPaymentValidator.setDistributionDate(distributionDate);
    }

    @UserAuthorization("postUnidentifiedWriteOff")
    @PostMapping
    @ApiOperation(value = "Write Off Suspended Payment")
    public HttpEntity<WriteOffOption> save(@PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate,
            @Valid @RequestBody WriteOffOption writeOffOption) {
        writeOffSuspendPaymentService.save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate), userId, batchId,
                DateRoutine.dateTimeAsYYYYMMDD(distributionDate), writeOffOption);
        WriteOffOption savedWriteOffOption = new WriteOffOption();
        savedWriteOffOption.add(linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayment(sequenceNumber,
                postedDate, userId, batchId, distributionDate)).withRel("payment").expand());
        return new ResponseEntity<>(savedWriteOffOption, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Write off Suspended Payment")
    public HttpEntity<OptionsResponseEntity> writeOffOptions(@PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate) {
        OptionsResponseEntity response = writeOffSuspendPaymentOptionsImpl.getDocumentResponse(WriteOffOption.class,
                sequenceNumber, postedDate, userId, batchId, distributionDate);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
