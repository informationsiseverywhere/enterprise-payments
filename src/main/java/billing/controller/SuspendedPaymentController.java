package billing.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import billing.model.SuspendedPayment;
import billing.model.SuspendedPaymentList;
import billing.service.SuspendedPaymentService;
import billing.service.options.SuspendedPaymentOptionImpl;
import billing.utils.BillingConstants.PaymentOperations;
import core.model.Paging;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import core.utils.DateRoutine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/suspendedPayments")
@Api(tags = { "Suspended Payments" }, description = "Suspended Payment Operations")
public class SuspendedPaymentController {

    private static final int INITIAL_PAGE = 1;
    private static final int INITIAL_PAGE_SIZE = 10;

    @Autowired
    private SuspendedPaymentService suspendedPaymentService;

    @Autowired
    private SuspendedPaymentOptionImpl suspendedPaymentOptionImpl;

    @UserAuthorization("searchPayments")
    @GetMapping
    @ApiOperation(value = "Search all Suspended Payments")
    public HttpEntity<Resources<Resource<SuspendedPayment>>> findSuspendedPayments(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "filters", required = false) String filters) {
        List<Resource<SuspendedPayment>> suspendedPaymentResource = new ArrayList<>();
        List<Link> links = new ArrayList<>();

        int evalPageSize = size == null || size < 1 ? INITIAL_PAGE_SIZE : size;
        int evalPage = page == null || page < 1 ? INITIAL_PAGE : page;

        SuspendedPaymentList suspendedPaymentList = suspendedPaymentService.findSuspendedPayments(evalPage,
                evalPageSize, filters);
        if (suspendedPaymentList != null && suspendedPaymentList.getSuspendedPayments() != null
                && !suspendedPaymentList.getSuspendedPayments().isEmpty()) {
            for (SuspendedPayment suspendedPayment : suspendedPaymentList.getSuspendedPayments()) {
                Link paymentLink = linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayment(
                        suspendedPayment.getPaymentSequenceNumber(),
                        DateRoutine.dateTimeAsYYYYMMDDString(suspendedPayment.getPostedDate()),
                        suspendedPayment.getUserId(), suspendedPayment.getBatchId(),
                        DateRoutine.dateTimeAsYYYYMMDDString(suspendedPayment.getDistributionDate())))
                                .withRel("payment").expand();
                suspendedPaymentResource.add(new Resource<SuspendedPayment>(suspendedPayment, paymentLink));

                Link link = linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayment(
                        suspendedPayment.getPaymentSequenceNumber(),
                        DateRoutine.dateTimeAsYYYYMMDDString(suspendedPayment.getPostedDate()),
                        suspendedPayment.getUserId(), suspendedPayment.getBatchId(),
                        DateRoutine.dateTimeAsYYYYMMDDString(suspendedPayment.getDistributionDate()))).withRel("item")
                                .expand();
                links.add(link);
            }
            Paging paging = suspendedPaymentList.getPage();
            if (paging.getCurrentPage() > 0) {
                links.add(
                        linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayments(paging.getCurrentPage(),
                                paging.getPageSize(), filters)).withRel("current").expand());
            }
            if (paging.getFirstPage() > 0) {
                links.add(linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayments(paging.getFirstPage(),
                        paging.getPageSize(), filters)).withRel("first").expand());
            }
            if (paging.getLastPage() > 0) {
                links.add(linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayments(paging.getLastPage(),
                        paging.getPageSize(), filters)).withRel("last").expand());
            }
            if (paging.getNextPage() > 0) {
                links.add(linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayments(paging.getNextPage(),
                        paging.getPageSize(), filters)).withRel("next").expand());
            }
            if (paging.getPreviousPage() > 0) {
                links.add(linkTo(methodOn(SuspendedPaymentController.class)
                        .findSuspendedPayments(paging.getPreviousPage(), paging.getPageSize(), filters))
                                .withRel("previous").expand());
            }
        }
        Link self = linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayments(page, size, filters))
                .withSelfRel().expand();
        Link parent = linkTo(methodOn(EnterprisePaymentController.class).paymentOptions()).withRel("up").expand();
        links.add(self);
        links.add(parent);

        Resources<Resource<SuspendedPayment>> suspendedPaymentResources = new Resources<>(suspendedPaymentResource,
                links);
        return new ResponseEntity<>(suspendedPaymentResources, HttpStatus.OK);
    }

    @UserAuthorization("findPayment")
    @GetMapping(value = "/{sequenceNumber}")
    @ApiOperation(value = "Find Suspended Payment")
    public HttpEntity<SuspendedPayment> findSuspendedPayment(@PathVariable("sequenceNumber") short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate) {
        SuspendedPayment suspendedPayment = suspendedPaymentService.findSuspendedPayment(batchId, sequenceNumber,
                postedDate, userId, distributionDate);

        if (suspendedPayment == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        suspendedPayment.add(linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayment(sequenceNumber,
                postedDate, userId, batchId, distributionDate)).withSelfRel().expand());

        Map<String, Boolean> allowedOperation = suspendedPaymentService.areOperationsAllowed(sequenceNumber, postedDate,
                userId, batchId, distributionDate);

        if (Boolean.TRUE.equals(allowedOperation.get(PaymentOperations.ALLOW_REVERSE.getValue()))) {
            suspendedPayment.add(linkTo(methodOn(ReverseSuspendedPaymentController.class).save(sequenceNumber,
                    postedDate, userId, batchId, distributionDate, null)).withRel("_reverse").expand());
        }
        if (Boolean.TRUE.equals(allowedOperation.get(PaymentOperations.ALLOW_DISBUSEMENT.getValue()))) {
            suspendedPayment.add(linkTo(methodOn(DisburseSuspendedPaymentController.class).save(sequenceNumber,
                    postedDate, userId, batchId, distributionDate, null)).withRel("_disburse").expand());
        }
        if (Boolean.TRUE.equals(allowedOperation.get(PaymentOperations.ALLOW_WRITEOFF.getValue()))) {
            suspendedPayment.add(linkTo(methodOn(WriteOffSuspendedPaymentController.class).save(sequenceNumber,
                    postedDate, userId, batchId, distributionDate, null)).withRel("_writeOff").expand());
        }
        if (Boolean.TRUE.equals(allowedOperation.get(PaymentOperations.ALLOW_SUSPENSE.getValue()))) {
            suspendedPayment.add(linkTo(methodOn(SuspendUnidentifiedPaymentController.class).save(sequenceNumber,
                    postedDate, userId, batchId, distributionDate)).withRel("_suspend").expand());
        }
        if (Boolean.TRUE.equals(allowedOperation.get(PaymentOperations.ALLOW_UPDATE.getValue()))) {
            suspendedPayment.add(linkTo(methodOn(UpdateSuspendedPaymentController.class).save(sequenceNumber,
                    postedDate, userId, batchId, distributionDate, null)).withRel("_update").expand());
        }
        suspendedPayment.add(linkTo(
                methodOn(SuspendedPaymentController.class).findSuspendedPayments(INITIAL_PAGE, INITIAL_PAGE_SIZE, null))
                        .withRel("payments").expand());
        suspendedPayment
                .add(linkTo(methodOn(EnterprisePaymentController.class).paymentOptions()).withRel("up").expand());
        return new ResponseEntity<>(suspendedPayment, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Suspended Payment Collection")
    public HttpEntity<OptionsResponseEntity> suspendedPaymentOptions(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "filters", required = false) String filters) {
        OptionsResponseEntity response = suspendedPaymentOptionImpl.getCollectionsResponse(SuspendedPayment.class, page,
                size, filters);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/{sequenceNumber}", method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Suspended Payment Resource")
    public HttpEntity<OptionsResponseEntity> suspendedPaymentResourceOptions(
            @PathVariable("sequenceNumber") Short sequenceNumber, @RequestParam("postedDate") String postedDate,
            @RequestParam("userId") String userId, @RequestParam("batchId") String batchId,
            @RequestParam("distributionDate") String distributionDate) {
        OptionsResponseEntity response = suspendedPaymentOptionImpl.getResourceResponse(SuspendedPayment.class,
                sequenceNumber, postedDate, userId, batchId, distributionDate);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
