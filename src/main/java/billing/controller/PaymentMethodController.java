package billing.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import billing.model.Payment;
import billing.validation.PaymentMethodValidator;
import core.security.annotation.UserAuthorization;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/validatepaymentmethod")
@Api(tags = { "Payment" }, description = "Validate Payment Method Operations")
public class PaymentMethodController {

    @Autowired
    private PaymentMethodValidator paymentMethodValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(paymentMethodValidator);
    }

    @UserAuthorization("validatePaymentMethod")
    @PostMapping
    @ApiOperation(value = "Validate Payment Method")
    public HttpEntity<Payment> makeAPayment(@Valid @RequestBody Payment payment) {

        Payment savedPayment = new Payment();
        HttpHeaders httpHeaders = new HttpHeaders();
        return new ResponseEntity<>(savedPayment, httpHeaders, HttpStatus.CREATED);
    }

}