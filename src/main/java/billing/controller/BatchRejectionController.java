package billing.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import billing.model.BatchRejection;
import billing.service.BatchRejectionService;
import billing.service.options.BatchRejectionOptionsImpl;
import billing.utils.PB360Service;
import billing.validation.BatchRejectionValidator;
import core.model.Paging;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import core.utils.DateRoutine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/rejectionBatches")
@Api(tags = { "Batch Rejection" }, description = "Batch Rejection Operations")
public class BatchRejectionController {

    @Autowired
    private BatchRejectionService batchRejectionService;

    @Autowired
    private BatchRejectionValidator batchRejectionValidator;

    @Autowired
    private BatchRejectionOptionsImpl batchRejectionOptionsImpl;

    @Autowired
    private PB360Service pb360Service;

    private static final int INITIAL_PAGE = 1;
    private static final int INITIAL_PAGE_SIZE = 10;
    private static final String BATCH = "batch";

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(batchRejectionValidator);
    }

    @InitBinder("batchId")
    protected void initBinder(WebDataBinder binder, @PathVariable("batchId") String batchId,
            @RequestParam("entryDate") String entryDate) {
        batchRejectionValidator.setBatchId(batchId);
        batchRejectionValidator.setEntryDate(entryDate);
        binder.setValidator(batchRejectionValidator);
    }

    @UserAuthorization("findRejectionBatches")
    @GetMapping
    @ApiOperation(value = "Get All Rejection Batches")
    public ResponseEntity<Resources<Resource<BatchRejection>>> findBatches(
            @RequestParam(value = "query", required = false) String query,
            @RequestParam(value = "filters", required = false) String filters,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {
        List<Resource<BatchRejection>> batchResource = new ArrayList<>();
        List<Link> links = new ArrayList<>();

        Paging paging = new Paging();
        int evalPageSize = size == null ? INITIAL_PAGE_SIZE : size;
        int evalPage = page == null || page < 1 ? INITIAL_PAGE : page;

        List<BatchRejection> batchList = batchRejectionService.findAllBatches(query, filters, sort, evalPageSize,
                evalPage, paging);
        if (batchList != null && !batchList.isEmpty()) {

            for (BatchRejection batch : batchList) {

                Link batchLink = linkTo(methodOn(BatchRejectionController.class).findBatch(batch.getBatchId(),
                        DateRoutine.dateTimeAsYYYYMMDDString(batch.getEntryDate()))).withRel(BATCH).expand();

                if (batch.getNumberOfTransactions() > 0) {
                    Link paymentLink = linkTo(methodOn(BatchRejectionPaymentsController.class).findBatchPayments(
                            batch.getBatchId(), DateRoutine.dateTimeAsYYYYMMDDString(batch.getEntryDate()), null, null,
                            null, INITIAL_PAGE, INITIAL_PAGE_SIZE)).withRel("payments").expand();
                    batchResource.add(new Resource<BatchRejection>(batch, batchLink, paymentLink));
                } else {
                    batchResource.add(new Resource<BatchRejection>(batch, batchLink));
                }

                Link item = linkTo(methodOn(BatchRejectionController.class).findBatch(batch.getBatchId(),
                        DateRoutine.dateTimeAsYYYYMMDDString(batch.getEntryDate()))).withRel("item").expand();
                links.add(item);
            }
            if (paging.getCurrentPage() > 0) {
                links.add(linkTo(methodOn(BatchRejectionController.class).findBatches(query, filters, sort,
                        paging.getCurrentPage(), paging.getPageSize())).withRel("current").expand());
            }
            if (paging.getFirstPage() > 0) {
                links.add(linkTo(methodOn(BatchRejectionController.class).findBatches(query, filters, sort,
                        paging.getFirstPage(), paging.getPageSize())).withRel("first").expand());
            }
            if (paging.getLastPage() > 0) {
                links.add(linkTo(methodOn(BatchRejectionController.class).findBatches(query, filters, sort,
                        paging.getLastPage(), paging.getPageSize())).withRel("last").expand());
            }
            if (paging.getNextPage() > 0) {
                links.add(linkTo(methodOn(BatchRejectionController.class).findBatches(query, filters, sort,
                        paging.getNextPage(), paging.getPageSize())).withRel("next").expand());
            }
            if (paging.getPreviousPage() > 0) {
                links.add(linkTo(methodOn(BatchRejectionController.class).findBatches(query, filters, sort,
                        paging.getPreviousPage(), paging.getPageSize())).withRel("previous").expand());
            }
        }
        Link self = linkTo(methodOn(BatchRejectionController.class).findBatches(query, filters, sort, page, size))
                .withSelfRel().expand();
        Link parent = linkTo(methodOn(EnterprisePaymentController.class).paymentOptions()).withRel("up").expand();
        links.add(self);
        links.add(parent);

        Resources<Resource<BatchRejection>> batchResources = new Resources<>(batchResource, links);
        return new ResponseEntity<>(batchResources, HttpStatus.OK);
    }

    @UserAuthorization("findRejectionBatch")
    @ApiOperation(value = "Get Rejection Batch by batchId")
    @GetMapping(value = "/{batchId}")
    public HttpEntity<BatchRejection> findBatch(@PathVariable("batchId") String batchId,
            @RequestParam(value = "entryDate", required = true) String entryDate) {
        batchId = batchId.toUpperCase();
        BatchRejection batch = batchRejectionService.findBatchDetails(batchId, entryDate);
        batch.add(
                linkTo(methodOn(BatchRejectionController.class).findBatch(batchId, entryDate)).withSelfRel().expand());
        batch.add(linkTo(methodOn(BatchRejectionPaymentsController.class).findBatchPayments(batchId, entryDate, null,
                null, null, INITIAL_PAGE, INITIAL_PAGE_SIZE)).withRel("batchPayments").expand());
        batch.add(linkTo(methodOn(BatchRejectionController.class).findBatches(null, null, null, null, null))
                .withRel("up").expand());
        return new ResponseEntity<>(batch, HttpStatus.OK);
    }

    @UserAuthorization("createBatchRejection")
    @ApiOperation(value = "Create Batch Rejection")
    @PostMapping
    public HttpEntity<BatchRejection> createBatch(@Valid @RequestBody BatchRejection batchRejection) {

        String bilEntryDt = DateRoutine.dateTimeAsYYYYMMDDString(batchRejection.getEntryDate());
        String batchId = batchRejectionService.save(batchRejection);
        BatchRejection savedBatch = new BatchRejection();
        savedBatch.add(linkTo(methodOn(BatchRejectionController.class).findBatch(batchId, bilEntryDt)).withRel(BATCH)
                .expand());
        savedBatch.add(linkTo(
                methodOn(BatchRejectionController.class).findBatches(null, null, null, INITIAL_PAGE, INITIAL_PAGE_SIZE))
                        .withRel("batches").expand());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(
                ServletUriComponentsBuilder.fromCurrentRequest().path("/{batchId}").buildAndExpand(batchId).toUri());
        savedBatch.setBatchId(batchId);
        savedBatch.setEntryDate(batchRejection.getEntryDate());
        return new ResponseEntity<>(savedBatch, httpHeaders, HttpStatus.CREATED);

    }

    @UserAuthorization("deleteBatchRejection")
    @ApiOperation(value = "Delete Batch Rejection")
    @DeleteMapping(value = "/{batchId}")
    public HttpEntity<BatchRejection> deleteBatch(@PathVariable("batchId") String batchId,
            @RequestParam(value = "entryDate", required = true) String entryDate) {
        batchId = batchId.toUpperCase();
        String message = batchRejectionService.deleteBatch(batchId, entryDate);
        if (message.compareTo("Success") != 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @UserAuthorization("patchBatchRejection")
    @ApiOperation(value = "Update Batch Rejection")
    @PatchMapping(value = "/{batchId}")
    public HttpEntity<BatchRejection> patchBatch(@PathVariable("batchId") String batchId,
            @RequestParam(value = "entryDate", required = true) String entryDate,
            @Valid @RequestBody BatchRejection batch) {
        pb360Service.checkPMStatus();
        batchId = batchId.toUpperCase();
        batchRejectionService.patchBatch(batchId, entryDate, batch);
        batchRejectionService.scheduleBatch(batchId, entryDate);

        BatchRejection updatedBatch = new BatchRejection();
        updatedBatch.add(
                linkTo(methodOn(BatchRejectionController.class).findBatch(batchId, entryDate)).withRel(BATCH).expand());
        updatedBatch.add(linkTo(
                methodOn(BatchRejectionController.class).findBatches(null, null, null, INITIAL_PAGE, INITIAL_PAGE_SIZE))
                        .withRel("batches").expand());
        return new ResponseEntity<>(updatedBatch, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Batch Rejection Collection")
    public HttpEntity<OptionsResponseEntity> batchCollectionOptions(
            @RequestParam(value = "query", required = false) String query,
            @RequestParam(value = "filters", required = false) String filters,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {
        OptionsResponseEntity response = null;
        response = batchRejectionOptionsImpl.getCollectionsResponse(BatchRejection.class, query, filters, sort, page,
                size);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/{batchId}", method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Batch Rejection Resource")
    public HttpEntity<OptionsResponseEntity> batchResourceOptions(@PathVariable("batchId") String batchId,
            @RequestParam(value = "entryDate", required = true) String entryDate) {
        OptionsResponseEntity response = null;
        response = batchRejectionOptionsImpl.getResourceResponse(BatchRejection.class, batchId, entryDate);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
