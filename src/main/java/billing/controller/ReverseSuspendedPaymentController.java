package billing.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import billing.model.ReverseOption;
import billing.service.ReverseSuspendPaymentService;
import billing.service.options.ReverseSuspendPaymentOptionImpl;
import billing.utils.PB360Service;
import billing.validation.ReverseSuspendPaymentValidator;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import core.utils.DateRoutine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/suspendedPayments/{sequenceNumber}/_reverse")
@Api(tags = { "Reverse Suspended Payment" }, description = "Reverse Suspended Payment Operations")
public class ReverseSuspendedPaymentController {

    @Autowired
    private ReverseSuspendPaymentValidator reverseSuspendPaymentValidator;

    @Autowired
    private ReverseSuspendPaymentService reverseSuspendPaymentService;

    @Autowired
    private ReverseSuspendPaymentOptionImpl reverseSuspendPaymentOptionImpl;

    @Autowired
    private PB360Service pb360Service;

    @InitBinder
    protected void initBinder(WebDataBinder binder, @PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate) {
        binder.setValidator(reverseSuspendPaymentValidator);
        reverseSuspendPaymentValidator.setSequenceNumber(sequenceNumber);
        reverseSuspendPaymentValidator.setPostedDate(postedDate);
        reverseSuspendPaymentValidator.setUserId(userId);
        reverseSuspendPaymentValidator.setBatchId(batchId);
        reverseSuspendPaymentValidator.setDistributionDate(distributionDate);
    }

    @UserAuthorization("postReverse")
    @PostMapping
    @ApiOperation(value = "Reverse Suspended Payment")
    public HttpEntity<ReverseOption> save(@PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate,
            @Valid @RequestBody ReverseOption reverseOption) {
        pb360Service.checkPMStatus();
        reverseSuspendPaymentService.save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate), userId, batchId,
                DateRoutine.dateTimeAsYYYYMMDD(distributionDate), reverseOption);
        ReverseOption savedReverseOption = new ReverseOption();
        savedReverseOption.add(linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayment(sequenceNumber,
                postedDate, userId, batchId, distributionDate)).withRel("payment").expand());
        return new ResponseEntity<>(savedReverseOption, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Reverse Suspended Payment")
    public HttpEntity<OptionsResponseEntity> reverseOptions(@PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate) {
        OptionsResponseEntity response = reverseSuspendPaymentOptionImpl.getDocumentResponse(ReverseOption.class,
                sequenceNumber, postedDate, userId, batchId, distributionDate);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
