package billing.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import billing.model.Payment;
import billing.model.SuspendedPayment;
import billing.service.SuspendUnidentifiedPaymentService;
import billing.service.options.SuspendUnidentifiedPaymentOptionsImpl;
import billing.utils.PB360Service;
import billing.validation.SuspendUnidentifiedPaymentValidator;
import core.options.model.OptionsResponseEntity;
import core.security.annotation.UserAuthorization;
import core.utils.DateRoutine;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/suspendedPayments/{sequenceNumber}/_suspend")
@Api(tags = { "Suspend Unidentified Payment" }, description = "Suspend Unidentified Payment Operations")
public class SuspendUnidentifiedPaymentController {

    @Autowired
    private SuspendUnidentifiedPaymentService suspendUnidentifiedPaymentService;

    @Autowired
    private SuspendUnidentifiedPaymentValidator suspendUnidentifiedPaymentValidator;

    @Autowired
    private SuspendUnidentifiedPaymentOptionsImpl suspendUnidentifiedPaymentOptionsImpl;

    @Autowired
    private PB360Service pb360Service;

    @UserAuthorization("postUnidentifiedSuspend")
    @PostMapping
    @ApiOperation(value = "Suspend Unidentified Payment")
    public HttpEntity<SuspendedPayment> save(@PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate) {
        pb360Service.checkPMStatus();
        suspendUnidentifiedPaymentValidator.validate(sequenceNumber, postedDate, userId, batchId, distributionDate);
        suspendUnidentifiedPaymentService.save(sequenceNumber, DateRoutine.dateTimeAsYYYYMMDD(postedDate), userId,
                batchId, DateRoutine.dateTimeAsYYYYMMDD(distributionDate));
        SuspendedPayment saveSuspendedPayment = new SuspendedPayment();
        saveSuspendedPayment.add(linkTo(methodOn(SuspendedPaymentController.class).findSuspendedPayment(sequenceNumber,
                postedDate, userId, batchId, distributionDate)).withRel("payment").expand());
        return new ResponseEntity<>(saveSuspendedPayment, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    @ApiOperation(value = "Options for Suspend Unidentified Payment")
    public HttpEntity<OptionsResponseEntity> suspendOptions(@PathVariable("sequenceNumber") Short sequenceNumber,
            @RequestParam("postedDate") String postedDate, @RequestParam("userId") String userId,
            @RequestParam("batchId") String batchId, @RequestParam("distributionDate") String distributionDate) {
        OptionsResponseEntity response = suspendUnidentifiedPaymentOptionsImpl.getDocumentResponse(Payment.class,
                sequenceNumber, postedDate, userId, batchId, distributionDate);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
