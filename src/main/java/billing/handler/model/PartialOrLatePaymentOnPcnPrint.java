package billing.handler.model;

import static core.utils.CommonConstants.DECIMAL_ZERO;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

public class PartialOrLatePaymentOnPcnPrint extends ResourceSupport {

    private String accountId;
    private String policyId;
    private String policyNumber;
    private String policySymbolCode;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime effectiveDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime expirationDate;
    private char reasonCode;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime noRescindDate;
    private double receiptAmount = DECIMAL_ZERO;
    private double pastDueAmount = DECIMAL_ZERO;
    private char staticReviewIndicator;

    public PartialOrLatePaymentOnPcnPrint() {
        super();
    }

    public PartialOrLatePaymentOnPcnPrint(String accountId, String policyId, String policyNumber,
            String policySymbolCode, ZonedDateTime effectiveDate, ZonedDateTime expirationDate, char reasonCode,
            ZonedDateTime noRescindDate, double receiptAmount, double pastDueAmount, char staticReviewIndicator) {
        super();
        this.accountId = accountId;
        this.policyId = policyId;
        this.policyNumber = policyNumber;
        this.policySymbolCode = policySymbolCode;
        this.effectiveDate = effectiveDate;
        this.expirationDate = expirationDate;
        this.reasonCode = reasonCode;
        this.noRescindDate = noRescindDate;
        this.receiptAmount = receiptAmount;
        this.pastDueAmount = pastDueAmount;
        this.staticReviewIndicator = staticReviewIndicator;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicySymbolCode() {
        return policySymbolCode;
    }

    public void setPolicySymbolCode(String policySymbolCode) {
        this.policySymbolCode = policySymbolCode;
    }

    public ZonedDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public ZonedDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(ZonedDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public char getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(char reasonCode) {
        this.reasonCode = reasonCode;
    }

    public ZonedDateTime getNoRescindDate() {
        return noRescindDate;
    }

    public void setNoRescindDate(ZonedDateTime noRescindDate) {
        this.noRescindDate = noRescindDate;
    }

    public double getReceiptAmount() {
        return receiptAmount;
    }

    public void setReceiptAmount(double receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public char getStaticReviewIndicator() {
        return staticReviewIndicator;
    }

    public void setStaticReviewIndicator(char staticReviewIndicator) {
        this.staticReviewIndicator = staticReviewIndicator;
    }

    public double getPastDueAmount() {
        return pastDueAmount;
    }

    public void setPastDueAmount(double pastDueAmount) {
        this.pastDueAmount = pastDueAmount;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountId, effectiveDate, expirationDate, noRescindDate, pastDueAmount,
                policyId, policyNumber, policySymbolCode, reasonCode, receiptAmount, staticReviewIndicator);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PartialOrLatePaymentOnPcnPrint other = (PartialOrLatePaymentOnPcnPrint) obj;
        return Objects.equals(accountId, other.accountId) && Objects.equals(effectiveDate, other.effectiveDate)
                && Objects.equals(expirationDate, other.expirationDate)
                && Objects.equals(noRescindDate, other.noRescindDate)
                && Double.doubleToLongBits(pastDueAmount) == Double.doubleToLongBits(other.pastDueAmount)
                && Objects.equals(policyId, other.policyId) && Objects.equals(policyNumber, other.policyNumber)
                && Objects.equals(policySymbolCode, other.policySymbolCode) && reasonCode == other.reasonCode
                && Double.doubleToLongBits(receiptAmount) == Double.doubleToLongBits(other.receiptAmount)
                && staticReviewIndicator == other.staticReviewIndicator;
    }

}
