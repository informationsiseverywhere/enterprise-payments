package billing.handler.model;

import static core.utils.CommonConstants.BLANK_STRING;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

public class ReverseWriteOffPayment extends ResourceSupport {

    private double cashInSuspenseAmount;
    private double premiumWriteOffcAmount;
    private double premiumWriteOffeAmount;
    private double penaltyWriteOffAmount;
    private double lateWriteOffAmount;
    private double serviceChargeWriteOffAmount;
    private double downPaymentWriteOffAmount;
    private String quoteIndicator = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime applicationDate;
    private String policyId = BLANK_STRING;;

    public ReverseWriteOffPayment() {
        super();
    }

    public ReverseWriteOffPayment(double cashInSuspenseAmount, double premiumWriteOffcAmount,
            double premiumWriteOffeAmount, double penaltyWriteOffAmount, double lateWriteOffAmount,
            double serviceChargeWriteOffAmount, double downPaymentWriteOffAmount, String quoteIndicator,
            ZonedDateTime applicationDate, String policyId) {
        super();
        this.cashInSuspenseAmount = cashInSuspenseAmount;
        this.premiumWriteOffcAmount = premiumWriteOffcAmount;
        this.premiumWriteOffeAmount = premiumWriteOffeAmount;
        this.penaltyWriteOffAmount = penaltyWriteOffAmount;
        this.lateWriteOffAmount = lateWriteOffAmount;
        this.serviceChargeWriteOffAmount = serviceChargeWriteOffAmount;
        this.downPaymentWriteOffAmount = downPaymentWriteOffAmount;
        this.quoteIndicator = quoteIndicator;
        this.applicationDate = applicationDate;
        this.policyId = policyId;
    }

    public double getCashInSuspenseAmount() {
        return cashInSuspenseAmount;
    }

    public void setCashInSuspenseAmount(double cashInSuspenseAmount) {
        this.cashInSuspenseAmount = cashInSuspenseAmount;
    }

    public double getPremiumWriteOffcAmount() {
        return premiumWriteOffcAmount;
    }

    public void setPremiumWriteOffcAmount(double premiumWriteOffcAmount) {
        this.premiumWriteOffcAmount = premiumWriteOffcAmount;
    }

    public double getPremiumWriteOffeAmount() {
        return premiumWriteOffeAmount;
    }

    public void setPremiumWriteOffeAmount(double premiumWriteOffeAmount) {
        this.premiumWriteOffeAmount = premiumWriteOffeAmount;
    }

    public double getPenaltyWriteOffAmount() {
        return penaltyWriteOffAmount;
    }

    public void setPenaltyWriteOffAmount(double penaltyWriteOffAmount) {
        this.penaltyWriteOffAmount = penaltyWriteOffAmount;
    }

    public double getLateWriteOffAmount() {
        return lateWriteOffAmount;
    }

    public void setLateWriteOffAmount(double lateWriteOffAmount) {
        this.lateWriteOffAmount = lateWriteOffAmount;
    }

    public double getServiceChargeWriteOffAmount() {
        return serviceChargeWriteOffAmount;
    }

    public void setServiceChargeWriteOffAmount(double serviceChargeWriteOffAmount) {
        this.serviceChargeWriteOffAmount = serviceChargeWriteOffAmount;
    }

    public double getDownPaymentWriteOffAmount() {
        return downPaymentWriteOffAmount;
    }

    public void setDownPaymentWriteOffAmount(double downPaymentWriteOffAmount) {
        this.downPaymentWriteOffAmount = downPaymentWriteOffAmount;
    }

    public String getQuoteIndicator() {
        return quoteIndicator;
    }

    public void setQuoteIndicator(String quoteIndicator) {
        this.quoteIndicator = quoteIndicator;
    }

    public ZonedDateTime getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(ZonedDateTime applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(applicationDate, cashInSuspenseAmount, downPaymentWriteOffAmount,
                lateWriteOffAmount, penaltyWriteOffAmount, policyId, premiumWriteOffcAmount, premiumWriteOffeAmount,
                quoteIndicator, serviceChargeWriteOffAmount);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ReverseWriteOffPayment other = (ReverseWriteOffPayment) obj;
        return Objects.equals(applicationDate, other.applicationDate)
                && Double.doubleToLongBits(cashInSuspenseAmount) == Double.doubleToLongBits(other.cashInSuspenseAmount)
                && Double.doubleToLongBits(downPaymentWriteOffAmount) == Double
                        .doubleToLongBits(other.downPaymentWriteOffAmount)
                && Double.doubleToLongBits(lateWriteOffAmount) == Double.doubleToLongBits(other.lateWriteOffAmount)
                && Double
                        .doubleToLongBits(penaltyWriteOffAmount) == Double.doubleToLongBits(other.penaltyWriteOffAmount)
                && Objects.equals(policyId, other.policyId)
                && Double.doubleToLongBits(premiumWriteOffcAmount) == Double
                        .doubleToLongBits(other.premiumWriteOffcAmount)
                && Double.doubleToLongBits(premiumWriteOffeAmount) == Double
                        .doubleToLongBits(other.premiumWriteOffeAmount)
                && Objects.equals(quoteIndicator, other.quoteIndicator)
                && Double.doubleToLongBits(serviceChargeWriteOffAmount) == Double
                        .doubleToLongBits(other.serviceChargeWriteOffAmount);
    }

}
