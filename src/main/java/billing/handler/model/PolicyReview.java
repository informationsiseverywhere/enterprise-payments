package billing.handler.model;

import java.io.Serializable;
import java.time.ZonedDateTime;

public class PolicyReview implements Serializable {

    private static final long serialVersionUID = 1L;

    private ZonedDateTime nextDisbursmentDate;
    private ZonedDateTime nextAcwDt;
    private ZonedDateTime nextLateDate;
    private ZonedDateTime nextWriteoffDate;
    private ZonedDateTime nextReviewDate;
    private ZonedDateTime nextInvoiceDate;
    private boolean cashInSuspense = false;
    private boolean lateCharge = false;
    private boolean polLvlCnDisb = false;
    private boolean polLvlCrDisb = false;

    public PolicyReview() {
        super();
    }

    public PolicyReview(ZonedDateTime nextDisbursmentDate, ZonedDateTime nextAcwDt, ZonedDateTime nextLateDate,
            ZonedDateTime nextWriteoffDate, ZonedDateTime nextReviewDate, ZonedDateTime nextInvoiceDate,
            boolean cashInSuspense, boolean lateCharge, boolean polLvlCnDisb, boolean polLvlCrDisb) {
        super();
        this.nextDisbursmentDate = nextDisbursmentDate;
        this.nextAcwDt = nextAcwDt;
        this.nextLateDate = nextLateDate;
        this.nextWriteoffDate = nextWriteoffDate;
        this.nextReviewDate = nextReviewDate;
        this.nextInvoiceDate = nextInvoiceDate;
        this.cashInSuspense = cashInSuspense;
        this.lateCharge = lateCharge;
        this.polLvlCnDisb = polLvlCnDisb;
        this.polLvlCrDisb = polLvlCrDisb;
    }

    public ZonedDateTime getNextDisbursmentDate() {
        return nextDisbursmentDate;
    }

    public void setNextDisbursmentDate(ZonedDateTime nextDisbursmentDate) {
        this.nextDisbursmentDate = nextDisbursmentDate;
    }

    public ZonedDateTime getNextAcwDt() {
        return nextAcwDt;
    }

    public void setNextAcwDt(ZonedDateTime nextAcwDt) {
        this.nextAcwDt = nextAcwDt;
    }

    public ZonedDateTime getNextLateDate() {
        return nextLateDate;
    }

    public void setNextLateDate(ZonedDateTime nextLateDate) {
        this.nextLateDate = nextLateDate;
    }

    public ZonedDateTime getNextWriteoffDate() {
        return nextWriteoffDate;
    }

    public void setNextWriteoffDate(ZonedDateTime nextWriteoffDate) {
        this.nextWriteoffDate = nextWriteoffDate;
    }

    public ZonedDateTime getNextReviewDate() {
        return nextReviewDate;
    }

    public void setNextReviewDate(ZonedDateTime nextReviewDate) {
        this.nextReviewDate = nextReviewDate;
    }

    public ZonedDateTime getNextInvoiceDate() {
        return nextInvoiceDate;
    }

    public void setNextInvoiceDate(ZonedDateTime nextInvoiceDate) {
        this.nextInvoiceDate = nextInvoiceDate;
    }

    public boolean isCashInSuspense() {
        return cashInSuspense;
    }

    public void setCashInSuspense(boolean cashInSuspense) {
        this.cashInSuspense = cashInSuspense;
    }

    public boolean isLateCharge() {
        return lateCharge;
    }

    public void setLateCharge(boolean lateCharge) {
        this.lateCharge = lateCharge;
    }

    public boolean isPolLvlCnDisb() {
        return polLvlCnDisb;
    }

    public void setPolLvlCnDisb(boolean polLvlCnDisb) {
        this.polLvlCnDisb = polLvlCnDisb;
    }

    public boolean isPolLvlCrDisb() {
        return polLvlCrDisb;
    }

    public void setPolLvlCrDisb(boolean polLvlCrDisb) {
        this.polLvlCrDisb = polLvlCrDisb;
    }

}
