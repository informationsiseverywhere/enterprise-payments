package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

public class WriteOffFeeCharges extends ResourceSupport {

    private char chargeTypeCode = BLANK_CHAR;
    private double amount;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime dueDate;
    private String reasonCode = BLANK_STRING;
    private String reasonTypeCode = BLANK_STRING;
    private String quoteIndicator = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime applicationDate;

    public WriteOffFeeCharges() {
        super();
    }

    public WriteOffFeeCharges(char chargeTypeCode, double amount, ZonedDateTime dueDate, String reasonCode,
            String reasonTypeCode, String quoteIndicator, ZonedDateTime applicationDate) {
        super();
        this.chargeTypeCode = chargeTypeCode;
        this.amount = amount;
        this.dueDate = dueDate;
        this.reasonCode = reasonCode;
        this.reasonTypeCode = reasonTypeCode;
        this.quoteIndicator = quoteIndicator;
        this.applicationDate = applicationDate;
    }

    public char getChargeTypeCode() {
        return chargeTypeCode;
    }

    public void setChargeTypeCode(char chargeTypeCode) {
        this.chargeTypeCode = chargeTypeCode;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public ZonedDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(ZonedDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonTypeCode() {
        return reasonTypeCode;
    }

    public void setReasonTypeCode(String reasonTypeCode) {
        this.reasonTypeCode = reasonTypeCode;
    }

    public String getQuoteIndicator() {
        return quoteIndicator;
    }

    public void setQuoteIndicator(String quoteIndicator) {
        this.quoteIndicator = quoteIndicator;
    }

    public ZonedDateTime getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(ZonedDateTime applicationDate) {
        this.applicationDate = applicationDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(amount, applicationDate, chargeTypeCode, dueDate, quoteIndicator,
                reasonCode, reasonTypeCode);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        WriteOffFeeCharges other = (WriteOffFeeCharges) obj;
        return Double.doubleToLongBits(amount) == Double.doubleToLongBits(other.amount)
                && Objects.equals(applicationDate, other.applicationDate) && chargeTypeCode == other.chargeTypeCode
                && Objects.equals(dueDate, other.dueDate) && Objects.equals(quoteIndicator, other.quoteIndicator)
                && Objects.equals(reasonCode, other.reasonCode) && Objects.equals(reasonTypeCode, other.reasonTypeCode);
    }

}
