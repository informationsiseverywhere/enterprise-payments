package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

public class AccountReview implements Serializable {
    private static final long serialVersionUID = 1L;
    private String accountId;
    private ZonedDateTime recoveryDate;
    private char recoverCheckIndicator = BLANK_CHAR;
    private char cashStatusCode = BLANK_CHAR;
    private String quoteIndicator = SEPARATOR_BLANK;

    public AccountReview() {
        super();
    }

    public AccountReview(String accountId, ZonedDateTime recoveryDate, char recoverCheckIndicator, char cashStatusCode,
            String quoteIndicator) {
        super();
        this.accountId = accountId;
        this.recoveryDate = recoveryDate;
        this.recoverCheckIndicator = recoverCheckIndicator;
        this.cashStatusCode = cashStatusCode;
        this.quoteIndicator = quoteIndicator;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public ZonedDateTime getRecoveryDate() {
        return recoveryDate;
    }

    public void setRecoveryDate(ZonedDateTime recoveryDate) {
        this.recoveryDate = recoveryDate;
    }

    public char getRecoverCheckIndicator() {
        return recoverCheckIndicator;
    }

    public void setRecoverCheckIndicator(char recoverCheckIndicator) {
        this.recoverCheckIndicator = recoverCheckIndicator;
    }

    public char getCashStatusCode() {
        return cashStatusCode;
    }

    public void setCashStatusCode(char cashStatusCode) {
        this.cashStatusCode = cashStatusCode;
    }

    public String getQuoteIndicator() {
        return quoteIndicator;
    }

    public void setQuoteIndicator(String quoteIndicator) {
        this.quoteIndicator = quoteIndicator;
    }

}
