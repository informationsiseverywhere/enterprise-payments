package billing.handler.model;

import java.io.Serializable;

public class AccountAdjustSupportData implements Serializable {
    private static final long serialVersionUID = 1L;

    private char serviceChargeIndicator;
    private String serviceChargeTypeCode;
    private char serviceChargeCode;
    private Double serviceChargeAmount;
    private Double accountPlanChargeAmount;
    private Double premiumAmount;
    private Double seviceChargeIncAmount;
    private Double premiumIncAmount;
    private Double maxServiceChargeAmount;

    public AccountAdjustSupportData() {
        super();
    }

    public AccountAdjustSupportData(char serviceChargeIndicator, String serviceChargeTypeCode, char serviceChargeCode,
            Double serviceChargeAmount, Double accountPlanChargeAmount, Double premiumAmount,
            Double seviceChargeIncAmount, Double premiumIncAmount, Double maxServiceChargeAmount) {
        super();
        this.serviceChargeIndicator = serviceChargeIndicator;
        this.serviceChargeTypeCode = serviceChargeTypeCode;
        this.serviceChargeCode = serviceChargeCode;
        this.serviceChargeAmount = serviceChargeAmount;
        this.accountPlanChargeAmount = accountPlanChargeAmount;
        this.premiumAmount = premiumAmount;
        this.seviceChargeIncAmount = seviceChargeIncAmount;
        this.premiumIncAmount = premiumIncAmount;
        this.maxServiceChargeAmount = maxServiceChargeAmount;
    }

    public char getServiceChargeIndicator() {
        return serviceChargeIndicator;
    }

    public void setServiceChargeIndicator(char serviceChargeIndicator) {
        this.serviceChargeIndicator = serviceChargeIndicator;
    }

    public String getServiceChargeTypeCode() {
        return serviceChargeTypeCode;
    }

    public void setServiceChargeTypeCode(String serviceChargeTypeCode) {
        this.serviceChargeTypeCode = serviceChargeTypeCode;
    }

    public char getServiceChargeCode() {
        return serviceChargeCode;
    }

    public void setServiceChargeCode(char serviceChargeCode) {
        this.serviceChargeCode = serviceChargeCode;
    }

    public Double getServiceChargeAmount() {
        return serviceChargeAmount;
    }

    public void setServiceChargeAmount(Double serviceChargeAmount) {
        this.serviceChargeAmount = serviceChargeAmount;
    }

    public Double getAccountPlanChargeAmount() {
        return accountPlanChargeAmount;
    }

    public void setAccountPlanChargeAmount(Double accountPlanChargeAmount) {
        this.accountPlanChargeAmount = accountPlanChargeAmount;
    }

    public Double getPremiumAmount() {
        return premiumAmount;
    }

    public void setPremiumAmount(Double premiumAmount) {
        this.premiumAmount = premiumAmount;
    }

    public Double getSeviceChargeIncAmount() {
        return seviceChargeIncAmount;
    }

    public void setSeviceChargeIncAmount(Double seviceChargeIncAmount) {
        this.seviceChargeIncAmount = seviceChargeIncAmount;
    }

    public Double getPremiumIncAmount() {
        return premiumIncAmount;
    }

    public void setPremiumIncAmount(Double premiumIncAmount) {
        this.premiumIncAmount = premiumIncAmount;
    }

    public Double getMaxServiceChargeAmount() {
        return maxServiceChargeAmount;
    }

    public void setMaxServiceChargeAmount(Double maxServiceChargeAmount) {
        this.maxServiceChargeAmount = maxServiceChargeAmount;
    }

}
