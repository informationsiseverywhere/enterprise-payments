package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class VariableSeviceChargeOverride {
    private String programName = BLANK_STRING;
    private String rulePrefix = "SV";
    private String ruleState = BLANK_STRING;
    private String stateCode = BLANK_STRING;
    private char stateFiller = BLANK_CHAR;
    private char ruleInd = CHAR_N;
    private String effectiveDate = BLANK_STRING;
    private Double calculatedChargeAmount = DECIMAL_ZERO;
    private Double prevChargeAmount = DECIMAL_ZERO;
    private String policyId = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime currentPolicyEffectiveDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime prevPolicyEffectiveDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime istPolicyEffectiveDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime currentDueDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime prevDueDate;
    private short numOfTerms = SHORT_ZERO;

    public VariableSeviceChargeOverride() {
        super();
        // TODO Auto-generated constructor stub
    }

    @JsonCreator
    public VariableSeviceChargeOverride(@JsonProperty("programName") String programName,
            @JsonProperty("rulePrefix") String rulePrefix, @JsonProperty("ruleState") String ruleState,
            @JsonProperty("stateCode") String stateCode, @JsonProperty("stateFiller") char stateFiller,
            @JsonProperty("ruleInd") char ruleInd, @JsonProperty("effectiveDate") String effectiveDate,
            @JsonProperty("calculatedChargeAmount") Double calculatedChargeAmount,
            @JsonProperty("prevChargeAmount") Double prevChargeAmount, @JsonProperty("policyId") String policyId,
            @JsonProperty("currentPolicyEffectiveDate") ZonedDateTime currentPolicyEffectiveDate,
            @JsonProperty("prevPolicyEffectiveDate") ZonedDateTime prevPolicyEffectiveDate,
            @JsonProperty("istPolicyEffectiveDate") ZonedDateTime istPolicyEffectiveDate,
            @JsonProperty("currentDueDate") ZonedDateTime currentDueDate,
            @JsonProperty("prevDueDate") ZonedDateTime prevDueDate, @JsonProperty("numOfTerms") short numOfTerms) {
        super();
        this.programName = programName;
        this.rulePrefix = rulePrefix;
        this.ruleState = ruleState;
        this.stateCode = stateCode;
        this.stateFiller = stateFiller;
        this.ruleInd = ruleInd;
        this.effectiveDate = effectiveDate;
        this.calculatedChargeAmount = calculatedChargeAmount;
        this.prevChargeAmount = prevChargeAmount;
        this.policyId = policyId;
        this.currentPolicyEffectiveDate = currentPolicyEffectiveDate;
        this.prevPolicyEffectiveDate = prevPolicyEffectiveDate;
        this.istPolicyEffectiveDate = istPolicyEffectiveDate;
        this.currentDueDate = currentDueDate;
        this.prevDueDate = prevDueDate;
        this.numOfTerms = numOfTerms;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getRulePrefix() {
        return rulePrefix;
    }

    public void setRulePrefix(String rulePrefix) {
        this.rulePrefix = rulePrefix;
    }

    public String getRuleState() {
        return ruleState;
    }

    public void setRuleState(String ruleState) {
        this.ruleState = ruleState;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public char getStateFiller() {
        return stateFiller;
    }

    public void setStateFiller(char stateFiller) {
        this.stateFiller = stateFiller;
    }

    public char getRuleInd() {
        return ruleInd;
    }

    public void setRuleInd(char ruleInd) {
        this.ruleInd = ruleInd;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Double getCalculatedChargeAmount() {
        return calculatedChargeAmount;
    }

    public void setCalculatedChargeAmount(Double calculatedChargeAmount) {
        this.calculatedChargeAmount = calculatedChargeAmount;
    }

    public Double getPrevChargeAmount() {
        return prevChargeAmount;
    }

    public void setPrevChargeAmount(Double prevChargeAmount) {
        this.prevChargeAmount = prevChargeAmount;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public ZonedDateTime getCurrentPolicyEffectiveDate() {
        return currentPolicyEffectiveDate;
    }

    public void setCurrentPolicyEffectiveDate(ZonedDateTime currentPolicyEffectiveDate) {
        this.currentPolicyEffectiveDate = currentPolicyEffectiveDate;
    }

    public ZonedDateTime getPrevPolicyEffectiveDate() {
        return prevPolicyEffectiveDate;
    }

    public void setPrevPolicyEffectiveDate(ZonedDateTime prevPolicyEffectiveDate) {
        this.prevPolicyEffectiveDate = prevPolicyEffectiveDate;
    }

    public ZonedDateTime getIstPolicyEffectiveDate() {
        return istPolicyEffectiveDate;
    }

    public void setIstPolicyEffectiveDate(ZonedDateTime istPolicyEffectiveDate) {
        this.istPolicyEffectiveDate = istPolicyEffectiveDate;
    }

    public ZonedDateTime getCurrentDueDate() {
        return currentDueDate;
    }

    public void setCurrentDueDate(ZonedDateTime currentDueDate) {
        this.currentDueDate = currentDueDate;
    }

    public ZonedDateTime getPrevDueDate() {
        return prevDueDate;
    }

    public void setPrevDueDate(ZonedDateTime prevDueDate) {
        this.prevDueDate = prevDueDate;
    }

    public short getNumOfTerms() {
        return numOfTerms;
    }

    public void setNumOfTerms(short numOfTerms) {
        this.numOfTerms = numOfTerms;
    }
}
