package billing.handler.model;

import java.io.Serializable;
import java.time.ZonedDateTime;

public class Rescission implements Serializable {

    private static final long serialVersionUID = 1L;

    private String accountId;
    private boolean isForceRescind;
    private double termBalanceAmount;
    private boolean isBillPlanChange;
    private boolean isRescind;
    private ZonedDateTime nextReviewDate;
    private ZonedDateTime nextInvoiceDate;

    public Rescission() {
        super();
    }

    public Rescission(String accountId, boolean isForceRescind, double termBalanceAmount, boolean isBillPlanChange,
            boolean isRescind, ZonedDateTime nextReviewDate, ZonedDateTime nextInvoiceDate) {
        super();
        this.accountId = accountId;
        this.isForceRescind = isForceRescind;
        this.termBalanceAmount = termBalanceAmount;
        this.isBillPlanChange = isBillPlanChange;
        this.isRescind = isRescind;
        this.nextReviewDate = nextReviewDate;
        this.nextInvoiceDate = nextInvoiceDate;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public boolean isForceRescind() {
        return isForceRescind;
    }

    public void setForceRescind(boolean isForceRescind) {
        this.isForceRescind = isForceRescind;
    }

    public double getTermBalanceAmount() {
        return termBalanceAmount;
    }

    public void setTermBalanceAmount(double termBalanceAmount) {
        this.termBalanceAmount = termBalanceAmount;
    }

    public boolean isBillPlanChange() {
        return isBillPlanChange;
    }

    public void setBillPlanChange(boolean isBillPlanChange) {
        this.isBillPlanChange = isBillPlanChange;
    }

    public boolean isRescind() {
        return isRescind;
    }

    public void setRescind(boolean isRescind) {
        this.isRescind = isRescind;
    }

    public ZonedDateTime getNextReviewDate() {
        return nextReviewDate;
    }

    public void setNextReviewDate(ZonedDateTime nextReviewDate) {
        this.nextReviewDate = nextReviewDate;
    }

    public ZonedDateTime getNextInvoiceDate() {
        return nextInvoiceDate;
    }

    public void setNextInvoiceDate(ZonedDateTime nextInvoiceDate) {
        this.nextInvoiceDate = nextInvoiceDate;
    }

}
