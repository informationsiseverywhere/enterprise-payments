package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;

import java.io.Serializable;
import java.time.ZonedDateTime;

public class RecoveryAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    private String accountId;
    private String policyId;
    private ZonedDateTime polEffDate;
    private char recoverCheckIndicator = BLANK_CHAR;
    private char driverIndicator = BLANK_CHAR;
    private char rescindCallIndicator = BLANK_CHAR;
    private char cashAcyCallIndicator = BLANK_CHAR;

    public RecoveryAccount() {
        super();
    }

    public RecoveryAccount(String accountId, String policyId, ZonedDateTime polEffDate, char recoverCheckIndicator,
            char driverIndicator, char rescindCallIndicator, char cashAcyCallIndicator) {
        super();
        this.accountId = accountId;
        this.policyId = policyId;
        this.polEffDate = polEffDate;
        this.recoverCheckIndicator = recoverCheckIndicator;
        this.driverIndicator = driverIndicator;
        this.rescindCallIndicator = rescindCallIndicator;
        this.cashAcyCallIndicator = cashAcyCallIndicator;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public ZonedDateTime getPolEffDate() {
        return polEffDate;
    }

    public void setPolEffDate(ZonedDateTime polEffDate) {
        this.polEffDate = polEffDate;
    }

    public char getRecoverCheckIndicator() {
        return recoverCheckIndicator;
    }

    public void setRecoverCheckIndicator(char recoverCheckIndicator) {
        this.recoverCheckIndicator = recoverCheckIndicator;
    }

    public char getDriverIndicator() {
        return driverIndicator;
    }

    public void setDriverIndicator(char driverIndicator) {
        this.driverIndicator = driverIndicator;
    }

    public char getRescindCallIndicator() {
        return rescindCallIndicator;
    }

    public void setRescindCallIndicator(char rescindCallIndicator) {
        this.rescindCallIndicator = rescindCallIndicator;
    }

    public char getCashAcyCallIndicator() {
        return cashAcyCallIndicator;
    }

    public void setCashAcyCallIndicator(char cashAcyCallIndicator) {
        this.cashAcyCallIndicator = cashAcyCallIndicator;
    }

}
