package billing.handler.model;

import static core.utils.CommonConstants.DECIMAL_ZERO;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

public class StaticPcnRescind extends ResourceSupport {

    private boolean isStaticPcnRescind = false;
    private String accountId;
    private String policyId;
    private String policyNumber;
    private String policySymbolCode;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime effectiveDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime expirationDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime termPastDueDate;
    private double termPremiumAmount = DECIMAL_ZERO;
    private double receiptAmount = DECIMAL_ZERO;
    private double termPastAmount = DECIMAL_ZERO;
    private double termPastPercent = DECIMAL_ZERO;
    private String staticRuleParm;
    private char policyStatus;

    public StaticPcnRescind() {
        super();
    }

    public StaticPcnRescind(boolean isStaticPcnRescind, String accountId, String policyId, String policyNumber,
            String policySymbolCode, ZonedDateTime effectiveDate, ZonedDateTime expirationDate,
            ZonedDateTime termPastDueDate, double termPremiumAmount, double receiptAmount, double termPastAmount,
            double termPastPercent, String staticRuleParm, char policyStatus) {
        super();
        this.isStaticPcnRescind = isStaticPcnRescind;
        this.accountId = accountId;
        this.policyId = policyId;
        this.policyNumber = policyNumber;
        this.policySymbolCode = policySymbolCode;
        this.effectiveDate = effectiveDate;
        this.expirationDate = expirationDate;
        this.termPastDueDate = termPastDueDate;
        this.termPremiumAmount = termPremiumAmount;
        this.receiptAmount = receiptAmount;
        this.termPastAmount = termPastAmount;
        this.termPastPercent = termPastPercent;
        this.staticRuleParm = staticRuleParm;
        this.policyStatus = policyStatus;
    }

    public boolean isStaticPcnRescind() {
        return isStaticPcnRescind;
    }

    public void setStaticPcnRescind(boolean isStaticPcnRescind) {
        this.isStaticPcnRescind = isStaticPcnRescind;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicySymbolCode() {
        return policySymbolCode;
    }

    public void setPolicySymbolCode(String policySymbolCode) {
        this.policySymbolCode = policySymbolCode;
    }

    public ZonedDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public ZonedDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(ZonedDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public ZonedDateTime getTermPastDueDate() {
        return termPastDueDate;
    }

    public void setTermPastDueDate(ZonedDateTime termPastDueDate) {
        this.termPastDueDate = termPastDueDate;
    }

    public double getTermPremiumAmount() {
        return termPremiumAmount;
    }

    public void setTermPremiumAmount(double termPremiumAmount) {
        this.termPremiumAmount = termPremiumAmount;
    }

    public double getReceiptAmount() {
        return receiptAmount;
    }

    public void setReceiptAmount(double receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public double getTermPastAmount() {
        return termPastAmount;
    }

    public void setTermPastAmount(double termPastAmount) {
        this.termPastAmount = termPastAmount;
    }

    public double getTermPastPercent() {
        return termPastPercent;
    }

    public void setTermPastPercent(double termPastPercent) {
        this.termPastPercent = termPastPercent;
    }

    public String getStaticRuleParm() {
        return staticRuleParm;
    }

    public void setStaticRuleParm(String staticRuleParm) {
        this.staticRuleParm = staticRuleParm;
    }

    public char getPolicyStatus() {
        return policyStatus;
    }

    public void setPolicyStatus(char policyStatus) {
        this.policyStatus = policyStatus;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountId, effectiveDate, expirationDate, isStaticPcnRescind, policyId,
                policyNumber, policyStatus, policySymbolCode, receiptAmount, staticRuleParm, termPastAmount,
                termPastDueDate, termPastPercent, termPremiumAmount);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        StaticPcnRescind other = (StaticPcnRescind) obj;
        return Objects.equals(accountId, other.accountId) && Objects.equals(effectiveDate, other.effectiveDate)
                && Objects.equals(expirationDate, other.expirationDate)
                && isStaticPcnRescind == other.isStaticPcnRescind && Objects.equals(policyId, other.policyId)
                && Objects.equals(policyNumber, other.policyNumber) && policyStatus == other.policyStatus
                && Objects.equals(policySymbolCode, other.policySymbolCode)
                && Double.doubleToLongBits(receiptAmount) == Double.doubleToLongBits(other.receiptAmount)
                && Objects.equals(staticRuleParm, other.staticRuleParm)
                && Double.doubleToLongBits(termPastAmount) == Double.doubleToLongBits(other.termPastAmount)
                && Objects.equals(termPastDueDate, other.termPastDueDate)
                && Double.doubleToLongBits(termPastPercent) == Double.doubleToLongBits(other.termPastPercent)
                && Double.doubleToLongBits(termPremiumAmount) == Double.doubleToLongBits(other.termPremiumAmount);
    }

}
