package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;

public class PcnRescindCollection {

    private String apiType = BLANK_STRING;
    private String policyId = BLANK_STRING;
    private ZonedDateTime originEffectiveDate;
    private ZonedDateTime effectiveDate;
    private ZonedDateTime planExpDate;
    private ZonedDateTime newExpDate;
    private String polPcnReason = BLANK_STRING;
    private char effTypeCode = BLANK_CHAR;
    private String userId = BLANK_STRING;
    private ZonedDateTime activityDate;
    private ZonedDateTime equityDate;
    private char cancelType = BLANK_CHAR;
    private String issueSysId = BLANK_STRING;
    private String programName = BLANK_STRING;
    private String paraName = BLANK_STRING;
    private char autoCollectionMethodChange = CHAR_N;
    private char nsfActivityIndicator = CHAR_N;
    private String accountId = BLANK_STRING;
    private String polSymbolCode = BLANK_STRING;
    private String polNumber = BLANK_STRING;
    private String statePvnCode = BLANK_STRING;
    private char restartPcnIndicator = CHAR_N;
    private ZonedDateTime restartPcnDate;
    private ZonedDateTime standCncDate;
    private short ldnLegalDays = SHORT_ZERO;
    private short ldnMailDays = SHORT_ZERO;
    private short ldnMaxDays = SHORT_ZERO;
    private String masterCompanyNumber = BLANK_STRING;
    private String lineOfBusinessCode = BLANK_STRING;
    private ZonedDateTime bilAcyDate;
    private short bilSequenceNumber = SHORT_ZERO;
    private String bilAcyDesCode = BLANK_STRING;
    private String bilDesReaType = BLANK_STRING;
    private char reinstNtcIndicator = BLANK_CHAR;
    private char pcnFlatCncIndicator = BLANK_CHAR;
    private char reinstCode = BLANK_CHAR;
    private String userSequenceId = BLANK_STRING;

    public PcnRescindCollection() {
        super();
    }

    public PcnRescindCollection(String apiType, String policyId, ZonedDateTime originEffectiveDate,
            ZonedDateTime effectiveDate, ZonedDateTime planExpDate, ZonedDateTime newExpDate, String polPcnReason,
            char effTypeCode, String userId, ZonedDateTime activityDate, ZonedDateTime equityDate, char cancelType,
            String issueSysId, String programName, String paraName, char autoCollectionMethodChange,
            char nsfActivityIndicator, String accountId, String polSymbolCode, String polNumber, String statePvnCode,
            char restartPcnIndicator, ZonedDateTime restartPcnDate, ZonedDateTime standCncDate, short ldnLegalDays,
            short ldnMailDays, short ldnMaxDays, String masterCompanyNumber, String lineOfBusinessCode,
            ZonedDateTime bilAcyDate, short bilSequenceNumber, String bilAcyDesCode, String bilDesReaType,
            char reinstNtcIndicator, char pcnFlatCncIndicator, char reinstCode, String userSequenceId) {
        super();
        this.apiType = apiType;
        this.policyId = policyId;
        this.originEffectiveDate = originEffectiveDate;
        this.effectiveDate = effectiveDate;
        this.planExpDate = planExpDate;
        this.newExpDate = newExpDate;
        this.polPcnReason = polPcnReason;
        this.effTypeCode = effTypeCode;
        this.userId = userId;
        this.activityDate = activityDate;
        this.equityDate = equityDate;
        this.cancelType = cancelType;
        this.issueSysId = issueSysId;
        this.programName = programName;
        this.paraName = paraName;
        this.autoCollectionMethodChange = autoCollectionMethodChange;
        this.nsfActivityIndicator = nsfActivityIndicator;
        this.accountId = accountId;
        this.polSymbolCode = polSymbolCode;
        this.polNumber = polNumber;
        this.statePvnCode = statePvnCode;
        this.restartPcnIndicator = restartPcnIndicator;
        this.restartPcnDate = restartPcnDate;
        this.standCncDate = standCncDate;
        this.ldnLegalDays = ldnLegalDays;
        this.ldnMailDays = ldnMailDays;
        this.ldnMaxDays = ldnMaxDays;
        this.masterCompanyNumber = masterCompanyNumber;
        this.lineOfBusinessCode = lineOfBusinessCode;
        this.bilAcyDate = bilAcyDate;
        this.bilSequenceNumber = bilSequenceNumber;
        this.bilAcyDesCode = bilAcyDesCode;
        this.bilDesReaType = bilDesReaType;
        this.reinstNtcIndicator = reinstNtcIndicator;
        this.pcnFlatCncIndicator = pcnFlatCncIndicator;
        this.reinstCode = reinstCode;
        this.userSequenceId = userSequenceId;
    }

    public String getApiType() {
        return apiType;
    }

    public void setApiType(String apiType) {
        this.apiType = apiType;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public ZonedDateTime getOriginEffectiveDate() {
        return originEffectiveDate;
    }

    public void setOriginEffectiveDate(ZonedDateTime originEffectiveDate) {
        this.originEffectiveDate = originEffectiveDate;
    }

    public ZonedDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public ZonedDateTime getPlanExpDate() {
        return planExpDate;
    }

    public void setPlanExpDate(ZonedDateTime planExpDate) {
        this.planExpDate = planExpDate;
    }

    public ZonedDateTime getNewExpDate() {
        return newExpDate;
    }

    public void setNewExpDate(ZonedDateTime newExpDate) {
        this.newExpDate = newExpDate;
    }

    public String getPolPcnReason() {
        return polPcnReason;
    }

    public void setPolPcnReason(String polPcnReason) {
        this.polPcnReason = polPcnReason;
    }

    public char getEffTypeCode() {
        return effTypeCode;
    }

    public void setEffTypeCode(char effTypeCode) {
        this.effTypeCode = effTypeCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ZonedDateTime getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(ZonedDateTime activityDate) {
        this.activityDate = activityDate;
    }

    public ZonedDateTime getEquityDate() {
        return equityDate;
    }

    public void setEquityDate(ZonedDateTime equityDate) {
        this.equityDate = equityDate;
    }

    public char getCancelType() {
        return cancelType;
    }

    public void setCancelType(char cancelType) {
        this.cancelType = cancelType;
    }

    public String getIssueSysId() {
        return issueSysId;
    }

    public void setIssueSysId(String issueSysId) {
        this.issueSysId = issueSysId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getParaName() {
        return paraName;
    }

    public void setParaName(String paraName) {
        this.paraName = paraName;
    }

    public char getAutoCollectionMethodChange() {
        return autoCollectionMethodChange;
    }

    public void setAutoCollectionMethodChange(char autoCollectionMethodChange) {
        this.autoCollectionMethodChange = autoCollectionMethodChange;
    }

    public char getNsfActivityIndicator() {
        return nsfActivityIndicator;
    }

    public void setNsfActivityIndicator(char nsfActivityIndicator) {
        this.nsfActivityIndicator = nsfActivityIndicator;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPolSymbolCode() {
        return polSymbolCode;
    }

    public void setPolSymbolCode(String polSymbolCode) {
        this.polSymbolCode = polSymbolCode;
    }

    public String getPolNumber() {
        return polNumber;
    }

    public void setPolNumber(String polNumber) {
        this.polNumber = polNumber;
    }

    public String getStatePvnCode() {
        return statePvnCode;
    }

    public void setStatePvnCode(String statePvnCode) {
        this.statePvnCode = statePvnCode;
    }

    public char getRestartPcnIndicator() {
        return restartPcnIndicator;
    }

    public void setRestartPcnIndicator(char restartPcnIndicator) {
        this.restartPcnIndicator = restartPcnIndicator;
    }

    public ZonedDateTime getRestartPcnDate() {
        return restartPcnDate;
    }

    public void setRestartPcnDate(ZonedDateTime restartPcnDate) {
        this.restartPcnDate = restartPcnDate;
    }

    public ZonedDateTime getStandCncDate() {
        return standCncDate;
    }

    public void setStandCncDate(ZonedDateTime standCncDate) {
        this.standCncDate = standCncDate;
    }

    public short getLdnLegalDays() {
        return ldnLegalDays;
    }

    public void setLdnLegalDays(short ldnLegalDays) {
        this.ldnLegalDays = ldnLegalDays;
    }

    public short getLdnMailDays() {
        return ldnMailDays;
    }

    public void setLdnMailDays(short ldnMailDays) {
        this.ldnMailDays = ldnMailDays;
    }

    public short getLdnMaxDays() {
        return ldnMaxDays;
    }

    public void setLdnMaxDays(short ldnMaxDays) {
        this.ldnMaxDays = ldnMaxDays;
    }

    public String getMasterCompanyNumber() {
        return masterCompanyNumber;
    }

    public void setMasterCompanyNumber(String masterCompanyNumber) {
        this.masterCompanyNumber = masterCompanyNumber;
    }

    public String getLineOfBusinessCode() {
        return lineOfBusinessCode;
    }

    public void setLineOfBusinessCode(String lineOfBusinessCode) {
        this.lineOfBusinessCode = lineOfBusinessCode;
    }

    public ZonedDateTime getBilAcyDate() {
        return bilAcyDate;
    }

    public void setBilAcyDate(ZonedDateTime bilAcyDate) {
        this.bilAcyDate = bilAcyDate;
    }

    public short getBilSequenceNumber() {
        return bilSequenceNumber;
    }

    public void setBilSequenceNumber(short bilSequenceNumber) {
        this.bilSequenceNumber = bilSequenceNumber;
    }

    public String getBilAcyDesCode() {
        return bilAcyDesCode;
    }

    public void setBilAcyDesCode(String bilAcyDesCode) {
        this.bilAcyDesCode = bilAcyDesCode;
    }

    public String getBilDesReaType() {
        return bilDesReaType;
    }

    public void setBilDesReaType(String bilDesReaType) {
        this.bilDesReaType = bilDesReaType;
    }

    public char getReinstNtcIndicator() {
        return reinstNtcIndicator;
    }

    public void setReinstNtcIndicator(char reinstNtcIndicator) {
        this.reinstNtcIndicator = reinstNtcIndicator;
    }

    public char getPcnFlatCncIndicator() {
        return pcnFlatCncIndicator;
    }

    public void setPcnFlatCncIndicator(char pcnFlatCncIndicator) {
        this.pcnFlatCncIndicator = pcnFlatCncIndicator;
    }

    public char getReinstCode() {
        return reinstCode;
    }

    public void setReinstCode(char reinstCode) {
        this.reinstCode = reinstCode;
    }

    public String getUserSequenceId() {
        return userSequenceId;
    }

    public void setUserSequenceId(String userSequenceId) {
        this.userSequenceId = userSequenceId;
    }
    
    

}
