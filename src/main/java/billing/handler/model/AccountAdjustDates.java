package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AccountAdjustDates extends ResourceSupport {

    private String accountId = BLANK_STRING;
    private String applicationName = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime processDate;
    private char dateResetIndicator = BLANK_CHAR;
    private char startDateIndicator = BLANK_CHAR;
    private char referenceDateIndicator = BLANK_CHAR;
    private char billPlanChangeIndicator = BLANK_CHAR;
    private char supportPlanIndicator = BLANK_CHAR;
    private char invoiceIndicator = BLANK_CHAR;
    private char suspendBillIndicator = BLANK_CHAR;
    private char addDateIndicator = BLANK_CHAR;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime currentDueDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime invoiceDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime newDueDate;
    private char addChargeIndicator = BLANK_CHAR;
    private char chargeType = BLANK_CHAR;
    private Double chargeAmount = DECIMAL_ZERO;
    private String policyId = BLANK_STRING;
    private ZonedDateTime policyEffectiveDate;
    private String quoteIndicator = SEPARATOR_BLANK;
    private String oldBilClass = BLANK_STRING;
    private String activityDescriptionCode = BLANK_STRING;
    private String reasonType = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime startReferenceDate;
    private String userSequenceId = BLANK_STRING;

    @JsonCreator
    public AccountAdjustDates(@JsonProperty("accountId") String accountId,
            @JsonProperty("applicationName") String applicationName,
            @JsonProperty("processDate") ZonedDateTime processDate,
            @JsonProperty("dateResetIndicator") char dateResetIndicator,
            @JsonProperty("startDateIndicator") char startDateIndicator,
            @JsonProperty("referenceDateIndicator") char referenceDateIndicator,
            @JsonProperty("billPlanChangeIndicator") char billPlanChangeIndicator,
            @JsonProperty("supportPlanIndicator") char supportPlanIndicator,
            @JsonProperty("invoiceIndicator") char invoiceIndicator,
            @JsonProperty("suspendBillIndicator") char suspendBillIndicator,
            @JsonProperty("addDateIndicator") char addDateIndicator,
            @JsonProperty("currentDueDate") ZonedDateTime currentDueDate,
            @JsonProperty("invoiceDate") ZonedDateTime invoiceDate,
            @JsonProperty("newDueDate") ZonedDateTime newDueDate,
            @JsonProperty("addChargeIndicator") char addChargeIndicator, @JsonProperty("chargeType") char chargeType,
            @JsonProperty("chargeAmount") Double chargeAmount, @JsonProperty("policyId") String policyId,
            @JsonProperty("policyEffectiveDate") ZonedDateTime policyEffectiveDate,
            @JsonProperty("quoteIndicator") String quoteIndicator, @JsonProperty("oldBilClass") String oldBilClass,
            @JsonProperty("activityDescriptionCode") String activityDescriptionCode,
            @JsonProperty("reasonType") String reasonType,
            @JsonProperty("startReferenceDate") ZonedDateTime startReferenceDate,
            @JsonProperty("userSequenceId") String userSequenceId) {
        super();
        this.accountId = accountId;
        this.applicationName = applicationName;
        this.processDate = processDate;
        this.dateResetIndicator = dateResetIndicator;
        this.startDateIndicator = startDateIndicator;
        this.referenceDateIndicator = referenceDateIndicator;
        this.billPlanChangeIndicator = billPlanChangeIndicator;
        this.supportPlanIndicator = supportPlanIndicator;
        this.invoiceIndicator = invoiceIndicator;
        this.suspendBillIndicator = suspendBillIndicator;
        this.addDateIndicator = addDateIndicator;
        this.currentDueDate = currentDueDate;
        this.invoiceDate = invoiceDate;
        this.newDueDate = newDueDate;
        this.addChargeIndicator = addChargeIndicator;
        this.chargeType = chargeType;
        this.chargeAmount = chargeAmount;
        this.policyId = policyId;
        this.policyEffectiveDate = policyEffectiveDate;
        this.quoteIndicator = quoteIndicator;
        this.oldBilClass = oldBilClass;
        this.activityDescriptionCode = activityDescriptionCode;
        this.reasonType = reasonType;
        this.startReferenceDate = startReferenceDate;
        this.userSequenceId = userSequenceId;
    }

    public AccountAdjustDates() {
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public ZonedDateTime getProcessDate() {
        return processDate;
    }

    public void setProcessDate(ZonedDateTime processDate) {
        this.processDate = processDate;
    }

    public char getDateResetIndicator() {
        return dateResetIndicator;
    }

    public void setDateResetIndicator(char dateResetIndicator) {
        this.dateResetIndicator = dateResetIndicator;
    }

    public char getStartDateIndicator() {
        return startDateIndicator;
    }

    public void setStartDateIndicator(char startDateIndicator) {
        this.startDateIndicator = startDateIndicator;
    }

    public char getReferenceDateIndicator() {
        return referenceDateIndicator;
    }

    public void setReferenceDateIndicator(char referenceDateIndicator) {
        this.referenceDateIndicator = referenceDateIndicator;
    }

    public char getBillPlanChangeIndicator() {
        return billPlanChangeIndicator;
    }

    public void setBillPlanChangeIndicator(char billPlanChangeIndicator) {
        this.billPlanChangeIndicator = billPlanChangeIndicator;
    }

    public char getSupportPlanIndicator() {
        return supportPlanIndicator;
    }

    public void setSupportPlanIndicator(char supportPlanIndicator) {
        this.supportPlanIndicator = supportPlanIndicator;
    }

    public char getInvoiceIndicator() {
        return invoiceIndicator;
    }

    public void setInvoiceIndicator(char invoiceIndicator) {
        this.invoiceIndicator = invoiceIndicator;
    }

    public char getSuspendBillIndicator() {
        return suspendBillIndicator;
    }

    public void setSuspendBillIndicator(char suspendBillIndicator) {
        this.suspendBillIndicator = suspendBillIndicator;
    }

    public char getAddDateIndicator() {
        return addDateIndicator;
    }

    public void setAddDateIndicator(char addDateIndicator) {
        this.addDateIndicator = addDateIndicator;
    }

    public ZonedDateTime getCurrentDueDate() {
        return currentDueDate;
    }

    public void setCurrentDueDate(ZonedDateTime currentDueDate) {
        this.currentDueDate = currentDueDate;
    }

    public ZonedDateTime getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(ZonedDateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public ZonedDateTime getNewDueDate() {
        return newDueDate;
    }

    public void setNewDueDate(ZonedDateTime newDueDate) {
        this.newDueDate = newDueDate;
    }

    public char getAddChargeIndicator() {
        return addChargeIndicator;
    }

    public void setAddChargeIndicator(char addChargeIndicator) {
        this.addChargeIndicator = addChargeIndicator;
    }

    public char getChargeType() {
        return chargeType;
    }

    public void setChargeType(char chargeType) {
        this.chargeType = chargeType;
    }

    public Double getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(Double chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public ZonedDateTime getPolicyEffectiveDate() {
        return policyEffectiveDate;
    }

    public void setPolicyEffectiveDate(ZonedDateTime policyEffectiveDate) {
        this.policyEffectiveDate = policyEffectiveDate;
    }

    public String getQuoteIndicator() {
        return quoteIndicator;
    }

    public void setQuoteIndicator(String quoteIndicator) {
        this.quoteIndicator = quoteIndicator;
    }

    public String getOldBilClass() {
        return oldBilClass;
    }

    public void setOldBilClass(String oldBilClass) {
        this.oldBilClass = oldBilClass;
    }

    public String getActivityDescriptionCode() {
        return activityDescriptionCode;
    }

    public void setActivityDescriptionCode(String activityDescriptionCode) {
        this.activityDescriptionCode = activityDescriptionCode;
    }

    public String getReasonType() {
        return reasonType;
    }

    public void setReasonType(String reasonType) {
        this.reasonType = reasonType;
    }

    public ZonedDateTime getStartReferenceDate() {
        return startReferenceDate;
    }

    public void setStartReferenceDate(ZonedDateTime startReferenceDate) {
        this.startReferenceDate = startReferenceDate;
    }
    
    public String getUserSequenceId() {
        return userSequenceId;
    }

    public void setUserSequenceId(String userSeqeunceId) {
        this.userSequenceId = userSeqeunceId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountId, activityDescriptionCode, addChargeIndicator, addDateIndicator,
                applicationName, billPlanChangeIndicator, chargeAmount, chargeType, currentDueDate, dateResetIndicator,
                invoiceDate, invoiceIndicator, newDueDate, oldBilClass, policyEffectiveDate, policyId, processDate,
                quoteIndicator, reasonType, referenceDateIndicator, startDateIndicator, startReferenceDate,
                supportPlanIndicator, suspendBillIndicator, userSequenceId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AccountAdjustDates other = (AccountAdjustDates) obj;
        return Objects.equals(accountId, other.accountId)
                && Objects.equals(activityDescriptionCode, other.activityDescriptionCode)
                && addChargeIndicator == other.addChargeIndicator && addDateIndicator == other.addDateIndicator
                && Objects.equals(applicationName, other.applicationName)
                && billPlanChangeIndicator == other.billPlanChangeIndicator
                && Objects.equals(chargeAmount, other.chargeAmount) && chargeType == other.chargeType
                && Objects.equals(currentDueDate, other.currentDueDate)
                && dateResetIndicator == other.dateResetIndicator && Objects.equals(invoiceDate, other.invoiceDate)
                && invoiceIndicator == other.invoiceIndicator && Objects.equals(newDueDate, other.newDueDate)
                && Objects.equals(oldBilClass, other.oldBilClass)
                && Objects.equals(policyEffectiveDate, other.policyEffectiveDate)
                && Objects.equals(policyId, other.policyId) && Objects.equals(processDate, other.processDate)
                && Objects.equals(quoteIndicator, other.quoteIndicator) && Objects.equals(reasonType, other.reasonType)
                && referenceDateIndicator == other.referenceDateIndicator
                && startDateIndicator == other.startDateIndicator
                && Objects.equals(startReferenceDate, other.startReferenceDate)
                && supportPlanIndicator == other.supportPlanIndicator
                && Objects.equals(userSequenceId, other.userSequenceId)
                && suspendBillIndicator == other.suspendBillIndicator;
    }

}