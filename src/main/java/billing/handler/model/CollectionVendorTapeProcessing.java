package billing.handler.model;

import java.time.ZonedDateTime;

public class CollectionVendorTapeProcessing {
    private String apiType;
    private String bilAccountId;
    private String policyId;
    private String polSymbolCd;
    private String polNbr;
    private ZonedDateTime activityDt;
    private String bilTypeCd;
    private String bilClassCd;
    private String bilAccountNbr;
    private String userId;
    private ZonedDateTime polEffDate;
    private ZonedDateTime polExpDate;
    private char classChgSw;
    private char stopManStartSw;
    private char suspendType;
    private String suspendReasonCode;
    private ZonedDateTime resumeDate;
    private double totColAmt;
    private char actColCd;
    private char collectionType;
    private char collectionQuote;

    public CollectionVendorTapeProcessing() {
        super();
    }

    public CollectionVendorTapeProcessing(String apiType, String bilAccountId, String policyId, String polSymbolCd,
            String polNbr, ZonedDateTime activityDt, String bilTypeCd, String bilClassCd, String bilAccountNbr,
            String userId, ZonedDateTime polEffDate, ZonedDateTime polExpDate, char classChgSw, char stopManStartSw,
            char suspendType, String suspendReasonCode, ZonedDateTime resumeDate, double totColAmt, char actColCd,
            char collectionType, char collectionQuote) {
        super();
        this.apiType = apiType;
        this.bilAccountId = bilAccountId;
        this.policyId = policyId;
        this.polSymbolCd = polSymbolCd;
        this.polNbr = polNbr;
        this.activityDt = activityDt;
        this.bilTypeCd = bilTypeCd;
        this.bilClassCd = bilClassCd;
        this.bilAccountNbr = bilAccountNbr;
        this.userId = userId;
        this.polEffDate = polEffDate;
        this.polExpDate = polExpDate;
        this.classChgSw = classChgSw;
        this.stopManStartSw = stopManStartSw;
        this.suspendType = suspendType;
        this.suspendReasonCode = suspendReasonCode;
        this.resumeDate = resumeDate;
        this.totColAmt = totColAmt;
        this.actColCd = actColCd;
        this.collectionType = collectionType;
        this.collectionQuote = collectionQuote;
    }

    public String getApiType() {
        return apiType;
    }

    public void setApiType(String apiType) {
        this.apiType = apiType;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolSymbolCd() {
        return polSymbolCd;
    }

    public void setPolSymbolCd(String polSymbolCd) {
        this.polSymbolCd = polSymbolCd;
    }

    public String getPolNbr() {
        return polNbr;
    }

    public void setPolNbr(String polNbr) {
        this.polNbr = polNbr;
    }

    public ZonedDateTime getActivityDt() {
        return activityDt;
    }

    public void setActivityDt(ZonedDateTime activityDt) {
        this.activityDt = activityDt;
    }

    public String getBilTypeCd() {
        return bilTypeCd;
    }

    public void setBilTypeCd(String bilTypeCd) {
        this.bilTypeCd = bilTypeCd;
    }

    public String getBilClassCd() {
        return bilClassCd;
    }

    public void setBilClassCd(String bilClassCd) {
        this.bilClassCd = bilClassCd;
    }

    public String getBilAccountNbr() {
        return bilAccountNbr;
    }

    public void setBilAccountNbr(String bilAccountNbr) {
        this.bilAccountNbr = bilAccountNbr;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ZonedDateTime getPolEffDate() {
        return polEffDate;
    }

    public void setPolEffDate(ZonedDateTime polEffDate) {
        this.polEffDate = polEffDate;
    }

    public ZonedDateTime getPolExpDate() {
        return polExpDate;
    }

    public void setPolExpDate(ZonedDateTime polExpDate) {
        this.polExpDate = polExpDate;
    }

    public char getClassChgSw() {
        return classChgSw;
    }

    public void setClassChgSw(char classChgSw) {
        this.classChgSw = classChgSw;
    }

    public char getStopManStartSw() {
        return stopManStartSw;
    }

    public void setStopManStartSw(char stopManStartSw) {
        this.stopManStartSw = stopManStartSw;
    }

    public char getSuspendType() {
        return suspendType;
    }

    public void setSuspendType(char suspendType) {
        this.suspendType = suspendType;
    }

    public String getSuspendReasonCode() {
        return suspendReasonCode;
    }

    public void setSuspendReasonCode(String suspendReasonCode) {
        this.suspendReasonCode = suspendReasonCode;
    }

    public ZonedDateTime getResumeDate() {
        return resumeDate;
    }

    public void setResumeDate(ZonedDateTime resumeDate) {
        this.resumeDate = resumeDate;
    }

    public double getTotColAmt() {
        return totColAmt;
    }

    public void setTotColAmt(double totColAmt) {
        this.totColAmt = totColAmt;
    }

    public char getActColCd() {
        return actColCd;
    }

    public void setActColCd(char actColCd) {
        this.actColCd = actColCd;
    }

    public char getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(char collectionType) {
        this.collectionType = collectionType;
    }

    public char getCollectionQuote() {
        return collectionQuote;
    }

    public void setCollectionQuote(char collectionQuote) {
        this.collectionQuote = collectionQuote;
    }

}
