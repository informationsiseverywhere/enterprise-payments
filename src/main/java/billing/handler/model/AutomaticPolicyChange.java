package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

public class AutomaticPolicyChange extends ResourceSupport {

    private String accountId = BLANK_STRING;
    private String policyId = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime policyEffDate;
    private String callingModuleName = BLANK_STRING;
    private char eftReversalIndicator = BLANK_CHAR;
    private boolean isPolPlanChange = false;
    private String newPolPlan = BLANK_STRING;
    private boolean isRequestForPlanChange = false;
    private String programName = BLANK_STRING;
    private String parameter = BLANK_STRING;

    public AutomaticPolicyChange() {
        super();
    }

    public AutomaticPolicyChange(String accountId, String policyId, ZonedDateTime policyEffDate,
            String callingModuleName, char eftReversalIndicator, boolean isPolPlanChange, String newPolPlan,
            boolean isRequestForPlanChange, String programName, String parameter) {
        super();
        this.accountId = accountId;
        this.policyId = policyId;
        this.policyEffDate = policyEffDate;
        this.callingModuleName = callingModuleName;
        this.eftReversalIndicator = eftReversalIndicator;
        this.isPolPlanChange = isPolPlanChange;
        this.newPolPlan = newPolPlan;
        this.isRequestForPlanChange = isRequestForPlanChange;
        this.programName = programName;
        this.parameter = parameter;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public ZonedDateTime getPolicyEffDate() {
        return policyEffDate;
    }

    public void setPolicyEffDate(ZonedDateTime policyEffDate) {
        this.policyEffDate = policyEffDate;
    }

    public String getCallingModuleName() {
        return callingModuleName;
    }

    public void setCallingModuleName(String callingModuleName) {
        this.callingModuleName = callingModuleName;
    }

    public char getEftReversalIndicator() {
        return eftReversalIndicator;
    }

    public void setEftReversalIndicator(char eftReversalIndicator) {
        this.eftReversalIndicator = eftReversalIndicator;
    }

    public boolean isPolPlanChange() {
        return isPolPlanChange;
    }

    public void setPolPlanChange(boolean isPolPlanChange) {
        this.isPolPlanChange = isPolPlanChange;
    }

    public String getNewPolPlan() {
        return newPolPlan;
    }

    public void setNewPolPlan(String newPolPlan) {
        this.newPolPlan = newPolPlan;
    }

    public boolean isRequestForPlanChange() {
        return isRequestForPlanChange;
    }

    public void setRequestForPlanChange(boolean isRequestForPlanChange) {
        this.isRequestForPlanChange = isRequestForPlanChange;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountId, callingModuleName, eftReversalIndicator, isPolPlanChange,
                isRequestForPlanChange, newPolPlan, parameter, policyEffDate, policyId, programName);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AutomaticPolicyChange other = (AutomaticPolicyChange) obj;
        return Objects.equals(accountId, other.accountId) && Objects.equals(callingModuleName, other.callingModuleName)
                && eftReversalIndicator == other.eftReversalIndicator && isPolPlanChange == other.isPolPlanChange
                && isRequestForPlanChange == other.isRequestForPlanChange
                && Objects.equals(newPolPlan, other.newPolPlan) && Objects.equals(parameter, other.parameter)
                && Objects.equals(policyEffDate, other.policyEffDate) && Objects.equals(policyId, other.policyId)
                && Objects.equals(programName, other.programName);
    }

}
