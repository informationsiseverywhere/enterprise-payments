package billing.handler.model;

import static core.utils.CommonConstants.DECIMAL_ZERO;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class DownPaymentFee {

    private BigDecimal totalDownpaymentPaidAmount = BigDecimal.valueOf(DECIMAL_ZERO);
    private BigDecimal totalDownpaymentChargeAmount = BigDecimal.valueOf(DECIMAL_ZERO);
    private BigDecimal totalDownpaymentWriteOffAmount = BigDecimal.valueOf(DECIMAL_ZERO);
    private ZonedDateTime downpaymentInvoiceDate;
    private ZonedDateTime downpaymentDueDate;

    public BigDecimal getTotalDownpaymentPaidAmount() {
        return totalDownpaymentPaidAmount;
    }

    public void setTotalDownpaymentPaidAmount(BigDecimal totalDownpaymentPaidAmount) {
        this.totalDownpaymentPaidAmount = totalDownpaymentPaidAmount;
    }

    public BigDecimal getTotalDownpaymentChargeAmount() {
        return totalDownpaymentChargeAmount;
    }

    public void setTotalDownpaymentChargeAmount(BigDecimal totalDownpaymentChargeAmount) {
        this.totalDownpaymentChargeAmount = totalDownpaymentChargeAmount;
    }

    public BigDecimal getTotalDownpaymentWriteOffAmount() {
        return totalDownpaymentWriteOffAmount;
    }

    public void setTotalDownpaymentWriteOffAmount(BigDecimal totalDownpaymentWriteOffAmount) {
        this.totalDownpaymentWriteOffAmount = totalDownpaymentWriteOffAmount;
    }

    public ZonedDateTime getDownpaymentInvoiceDate() {
        return downpaymentInvoiceDate;
    }

    public void setDownpaymentInvoiceDate(ZonedDateTime downpaymentInvoiceDate) {
        this.downpaymentInvoiceDate = downpaymentInvoiceDate;
    }

    public ZonedDateTime getDownpaymentDueDate() {
        return downpaymentDueDate;
    }

    public void setDownpaymentDueDate(ZonedDateTime downpaymentDueDate) {
        this.downpaymentDueDate = downpaymentDueDate;
    }

}
