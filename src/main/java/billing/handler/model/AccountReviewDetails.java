package billing.handler.model;

import java.io.Serializable;
import java.time.ZonedDateTime;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActRules;
import billing.data.entity.BilSupportPlan;

public class AccountReviewDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    private BilAccount bilAccount;
    private BilActRules bilActRules;
    private BilSupportPlan bilSupportPlan;
    private ZonedDateTime nextRecoveryDate;
    private boolean invoiceRequire;
    private boolean lateCharge;
    private char suspendBillIndicator;
    private char eftCollectionIndicator;

    public AccountReviewDetails() {
        super();
    }

    public AccountReviewDetails(BilAccount bilAccount, BilActRules bilActRules, BilSupportPlan bilSupportPlan,
            ZonedDateTime nextRecoveryDate, boolean invoiceRequire, boolean lateCharge, char suspendBillIndicator,
            char eftCollectionIndicator) {
        super();
        this.bilAccount = bilAccount;
        this.bilActRules = bilActRules;
        this.bilSupportPlan = bilSupportPlan;
        this.nextRecoveryDate = nextRecoveryDate;
        this.invoiceRequire = invoiceRequire;
        this.lateCharge = lateCharge;
        this.suspendBillIndicator = suspendBillIndicator;
        this.eftCollectionIndicator = eftCollectionIndicator;
    }

    public BilAccount getBilAccount() {
        return bilAccount;
    }

    public void setBilAccount(BilAccount bilAccount) {
        this.bilAccount = bilAccount;
    }

    public BilActRules getBilActRules() {
        return bilActRules;
    }

    public void setBilActRules(BilActRules bilActRules) {
        this.bilActRules = bilActRules;
    }

    public BilSupportPlan getBilSupportPlan() {
        return bilSupportPlan;
    }

    public void setBilSupportPlan(BilSupportPlan bilSupportPlan) {
        this.bilSupportPlan = bilSupportPlan;
    }

    public ZonedDateTime getNextRecoveryDate() {
        return nextRecoveryDate;
    }

    public void setNextRecoveryDate(ZonedDateTime nextRecoveryDate) {
        this.nextRecoveryDate = nextRecoveryDate;
    }

    public boolean isInvoiceRequire() {
        return invoiceRequire;
    }

    public void setInvoiceRequire(boolean invoiceRequire) {
        this.invoiceRequire = invoiceRequire;
    }

    public boolean isLateCharge() {
        return lateCharge;
    }

    public void setLateCharge(boolean lateCharge) {
        this.lateCharge = lateCharge;
    }

    public char getSuspendBillIndicator() {
        return suspendBillIndicator;
    }

    public void setSuspendBillIndicator(char suspendBillIndicator) {
        this.suspendBillIndicator = suspendBillIndicator;
    }

    public char getEftCollectionIndicator() {
        return eftCollectionIndicator;
    }

    public void setEftCollectionIndicator(char eftCollectionIndicator) {
        this.eftCollectionIndicator = eftCollectionIndicator;
    }

}
