package billing.handler.model;

import java.util.Map;

import billing.data.entity.BilIstSchedule;

public class DownPaymentInformation {

    private double remainingDownPayment;
    private double applyDownPaymentAmount;
    private Map<BilIstSchedule, Double> scheduleData;

    public DownPaymentInformation() {

    }

    public DownPaymentInformation(double remainingDownPayment, double applyDownPaymentAmount,
            Map<BilIstSchedule, Double> scheduleData) {
        super();
        this.remainingDownPayment = remainingDownPayment;
        this.applyDownPaymentAmount = applyDownPaymentAmount;
        this.scheduleData = scheduleData;
    }

    public double getRemainingDownPayment() {
        return remainingDownPayment;
    }

    public void setRemainingDownPayment(double remainingDownPayment) {
        this.remainingDownPayment = remainingDownPayment;
    }

    public Map<BilIstSchedule, Double> getScheduleData() {
        return scheduleData;
    }

    public void setScheduleData(Map<BilIstSchedule, Double> scheduleData) {
        this.scheduleData = scheduleData;
    }

    public double getApplyDownPaymentAmount() {
        return applyDownPaymentAmount;
    }

    public void setApplyDownPaymentAmount(double applyDownPaymentAmount) {
        this.applyDownPaymentAmount = applyDownPaymentAmount;
    }

}
