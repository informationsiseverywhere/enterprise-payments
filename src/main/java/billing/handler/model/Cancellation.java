package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;

import java.io.Serializable;
import java.time.ZonedDateTime;

import core.utils.DateRoutine;

public class Cancellation implements Serializable {

    private static final long serialVersionUID = 1L;

    private String accountId = BLANK_STRING;
    private char invoicingIndicator = BLANK_CHAR;
    private char eftCollectionIndicator = BLANK_CHAR;
    private char eftReversalIndicator = BLANK_CHAR;
    private char reviewDatesIndicator = BLANK_CHAR;
    private char lapseCancIndicator = BLANK_CHAR;
    private char pcnmMaxIndicator = BLANK_CHAR;
    private boolean isEftResumeSus = false;
    private boolean isPolicyPedValued = false;
    private String originalPcnReason = BLANK_STRING;
    private ZonedDateTime restartPcnDate = DateRoutine.defaultDateTime();
    private boolean isEquityUsed = false;
    private boolean pcnmApplied = false;

    public Cancellation() {
        super();
    }

    public Cancellation(String accountId, char invoicingIndicator, char eftCollectionIndicator,
            char eftReversalIndicator, char reviewDatesIndicator, char lapseCancIndicator, char pcnmMaxIndicator,
            boolean isEftResumeSus, boolean isPolicyPedValued, String originalPcnReason, ZonedDateTime restartPcnDate,
            boolean isEquityUsed) {
        super();
        this.accountId = accountId;
        this.invoicingIndicator = invoicingIndicator;
        this.eftCollectionIndicator = eftCollectionIndicator;
        this.eftReversalIndicator = eftReversalIndicator;
        this.reviewDatesIndicator = reviewDatesIndicator;
        this.lapseCancIndicator = lapseCancIndicator;
        this.pcnmMaxIndicator = pcnmMaxIndicator;
        this.isEftResumeSus = isEftResumeSus;
        this.isPolicyPedValued = isPolicyPedValued;
        this.originalPcnReason = originalPcnReason;
        this.restartPcnDate = restartPcnDate;
        this.isEquityUsed = isEquityUsed;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public char getInvoicingIndicator() {
        return invoicingIndicator;
    }

    public void setInvoicingIndicator(char invoicingIndicator) {
        this.invoicingIndicator = invoicingIndicator;
    }

    public char getEftCollectionIndicator() {
        return eftCollectionIndicator;
    }

    public void setEftCollectionIndicator(char eftCollectionIndicator) {
        this.eftCollectionIndicator = eftCollectionIndicator;
    }

    public char getEftReversalIndicator() {
        return eftReversalIndicator;
    }

    public void setEftReversalIndicator(char eftReversalIndicator) {
        this.eftReversalIndicator = eftReversalIndicator;
    }

    public char getReviewDatesIndicator() {
        return reviewDatesIndicator;
    }

    public void setReviewDatesIndicator(char reviewDatesIndicator) {
        this.reviewDatesIndicator = reviewDatesIndicator;
    }

    public char getLapseCancIndicator() {
        return lapseCancIndicator;
    }

    public void setLapseCancIndicator(char lapseCancIndicator) {
        this.lapseCancIndicator = lapseCancIndicator;
    }

    public char getPcnmMaxIndicator() {
        return pcnmMaxIndicator;
    }

    public void setPcnmMaxIndicator(char pcnmMaxIndicator) {
        this.pcnmMaxIndicator = pcnmMaxIndicator;
    }

    public boolean isEftResumeSus() {
        return isEftResumeSus;
    }

    public void setEftResumeSus(boolean isEftResumeSus) {
        this.isEftResumeSus = isEftResumeSus;
    }

    public boolean isPolicyPedValued() {
        return isPolicyPedValued;
    }

    public void setPolicyPedValued(boolean isPolicyPedValued) {
        this.isPolicyPedValued = isPolicyPedValued;
    }

    public String getOriginalPcnReason() {
        return originalPcnReason;
    }

    public void setOriginalPcnReason(String originalPcnReason) {
        this.originalPcnReason = originalPcnReason;
    }

    public ZonedDateTime getRestartPcnDate() {
        return restartPcnDate;
    }

    public void setRestartPcnDate(ZonedDateTime restartPcnDate) {
        this.restartPcnDate = restartPcnDate;
    }

    public boolean isEquityUsed() {
        return isEquityUsed;
    }

    public void setEquityUsed(boolean isEquityUsed) {
        this.isEquityUsed = isEquityUsed;
    }

	public boolean isPcnmApplied() {
		return pcnmApplied;
	}

	public void setPcnmApplied(boolean pcnmApplied) {
		this.pcnmApplied = pcnmApplied;
	}
}
