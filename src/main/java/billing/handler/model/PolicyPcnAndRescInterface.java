package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;

import java.time.ZonedDateTime;

public class PolicyPcnAndRescInterface extends PcnRescindCollection {

    private char actionCode = BLANK_CHAR;
    private String requestProgramName = BLANK_STRING;
    private String context = BLANK_STRING;
    private char queueWrtIndicator = BLANK_CHAR;
    private String rntReaAmdCode = BLANK_STRING;
    private ZonedDateTime rntNewEffDate;

    public PolicyPcnAndRescInterface() {
        super();
    }

    public PolicyPcnAndRescInterface(String apiType, String policyId, ZonedDateTime originEffectiveDate,
            ZonedDateTime effectiveDate, ZonedDateTime planExpDate, ZonedDateTime newExpDate, String polPcnReason,
            char effTypeCode, String userId, ZonedDateTime activityDate, ZonedDateTime equityDate, char cancelType,
            String issueSysId, String programName, String paraName, char autoCollectionMethodChange,
            char nsfActivityIndicator, String accountId, String polSymbolCode, String polNumber, String statePvnCode,
            String rekickNumber, String rekickDays, char restartPcnIndicator, ZonedDateTime restartPcnDate,
            ZonedDateTime standCncDate, short ldnLegalDays, short ldnMailDays, short ldnMaxDays,
            String masterCompanyNumber, String lineOfBusinessCode, ZonedDateTime bilAcyDate, short bilSequenceNumber,
            String bilAcyDesCode, String bilDesReaType, char reinstNtcIndicator, char pcnFlatCncIndicator,
            char reinstCode, String userSequenceId) {
        super(apiType, policyId, originEffectiveDate, effectiveDate, planExpDate, newExpDate, polPcnReason, effTypeCode,
                userId, activityDate, equityDate, cancelType, issueSysId, programName, paraName,
                autoCollectionMethodChange, nsfActivityIndicator, accountId, polSymbolCode, polNumber, statePvnCode,
                restartPcnIndicator, restartPcnDate, standCncDate, ldnLegalDays, ldnMailDays, ldnMaxDays,
                masterCompanyNumber, lineOfBusinessCode, bilAcyDate, bilSequenceNumber, bilAcyDesCode, bilDesReaType,
                reinstNtcIndicator, pcnFlatCncIndicator, reinstCode, userSequenceId);
    }

    public PolicyPcnAndRescInterface(char actionCode, String requestProgramName, String context, char queueWrtIndicator,
            String rntReaAmdCode, ZonedDateTime rntNewEffDate) {
        super();
        this.actionCode = actionCode;
        this.requestProgramName = requestProgramName;
        this.context = context;
        this.queueWrtIndicator = queueWrtIndicator;
        this.rntReaAmdCode = rntReaAmdCode;
        this.rntNewEffDate = rntNewEffDate;
    }

    public char getActionCode() {
        return actionCode;
    }

    public void setActionCode(char actionCode) {
        this.actionCode = actionCode;
    }

    public String getRequestProgramName() {
        return requestProgramName;
    }

    public void setRequestProgramName(String requestProgramName) {
        this.requestProgramName = requestProgramName;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public char getQueueWrtIndicator() {
        return queueWrtIndicator;
    }

    public void setQueueWrtIndicator(char queueWrtIndicator) {
        this.queueWrtIndicator = queueWrtIndicator;
    }

    public String getRntReaAmdCode() {
        return rntReaAmdCode;
    }

    public void setRntReaAmdCode(String rntReaAmdCode) {
        this.rntReaAmdCode = rntReaAmdCode;
    }

    public ZonedDateTime getRntNewEffDate() {
        return rntNewEffDate;
    }

    public void setRntNewEffDate(ZonedDateTime rntNewEffDate) {
        this.rntNewEffDate = rntNewEffDate;
    }

}
