package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.INVALID_INT_VAL;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;

public class VariableSeviceCharge {
    private char cancelResetIndicator = BLANK_CHAR;
    private char bcmoppKickIndicator = BLANK_CHAR;
    private char cnrIndicator = BLANK_CHAR;
    private Double annualPremium = DECIMAL_ZERO;
    private Double annualPrm = DECIMAL_ZERO;
    private Double termPrm = DECIMAL_ZERO;
    private Double coveragePrm = DECIMAL_ZERO;
    private Double covFct = DECIMAL_ZERO;
    private Double invFct = DECIMAL_ZERO;
    private Double dateFct = DECIMAL_ZERO;
    private int coverageDuration = INVALID_INT_VAL;
    private int policyDuration = INVALID_INT_VAL;
    private Double calculatedScgAmt = DECIMAL_ZERO;
    private Double newTermScgAmt = DECIMAL_ZERO;
    private Double bspScgTempAmt = DECIMAL_ZERO;
    private Double calculatedPrmAmt = DECIMAL_ZERO;
    private Double gradPrmAmt = DECIMAL_ZERO;
    private Double gradScgAmt = DECIMAL_ZERO;
    private Double acctPlanSvcAmt = DECIMAL_ZERO;
    private String policyIdTrm = BLANK_STRING;
    private String policyId = BLANK_STRING;
    private ZonedDateTime maxPolicyExpDt;
    private ZonedDateTime issueAcyTs;
    private String bamAmtTs = BLANK_STRING;
    private short bamSeqNbr = Short.MIN_VALUE;
    private short bilSeqNbr = Short.MIN_VALUE;
    private char bamEffInd = BLANK_CHAR;
    private ZonedDateTime maxPolEffDt;
    private ZonedDateTime mamMaxEffDt;
    private ZonedDateTime istPolEffDt;
    private ZonedDateTime bccBilAdjDueDt;
    private short numOfTerms = SHORT_ZERO;
    private ZonedDateTime actEffDt;
    private ZonedDateTime apMaxEffDt;
    private String issueSystemId = BLANK_STRING;

    public VariableSeviceCharge() {
        super();
    }

    public VariableSeviceCharge(char cancelResetIndicator, char bcmoppKickIndicator, char cnrIndicator,
            Double annualPremium, Double annualPrm, Double termPrm, Double coveragePrm, Double covFct, Double invFct,
            Double dateFct, int coverageDuration, int policyDuration, Double calculatedScgAmt, Double newTermScgAmt,
            Double bspScgTempAmt, Double calculatedPrmAmt, Double gradPrmAmt, Double gradScgAmt, Double acctPlanSvcAmt,
            String policyIdTrm, String policyId, ZonedDateTime maxPolicyExpDt, ZonedDateTime issueAcyTs,
            String bamAmtTs, short bamSeqNbr, short bilSeqNbr, char bamEffInd, ZonedDateTime maxPolEffDt,
            ZonedDateTime mamMaxEffDt, ZonedDateTime istPolEffDt, ZonedDateTime bccBilAdjDueDt, short numOfTerms,
            ZonedDateTime actEffDt, ZonedDateTime apMaxEffDt, String issueSystemId) {
        super();
        this.cancelResetIndicator = cancelResetIndicator;
        this.bcmoppKickIndicator = bcmoppKickIndicator;
        this.cnrIndicator = cnrIndicator;
        this.annualPremium = annualPremium;
        this.annualPrm = annualPrm;
        this.termPrm = termPrm;
        this.coveragePrm = coveragePrm;
        this.covFct = covFct;
        this.invFct = invFct;
        this.dateFct = dateFct;
        this.coverageDuration = coverageDuration;
        this.policyDuration = policyDuration;
        this.calculatedScgAmt = calculatedScgAmt;
        this.newTermScgAmt = newTermScgAmt;
        this.bspScgTempAmt = bspScgTempAmt;
        this.calculatedPrmAmt = calculatedPrmAmt;
        this.gradPrmAmt = gradPrmAmt;
        this.gradScgAmt = gradScgAmt;
        this.acctPlanSvcAmt = acctPlanSvcAmt;
        this.policyIdTrm = policyIdTrm;
        this.policyId = policyId;
        this.maxPolicyExpDt = maxPolicyExpDt;
        this.issueAcyTs = issueAcyTs;
        this.bamAmtTs = bamAmtTs;
        this.bamSeqNbr = bamSeqNbr;
        this.bilSeqNbr = bilSeqNbr;
        this.bamEffInd = bamEffInd;
        this.maxPolEffDt = maxPolEffDt;
        this.mamMaxEffDt = mamMaxEffDt;
        this.istPolEffDt = istPolEffDt;
        this.bccBilAdjDueDt = bccBilAdjDueDt;
        this.numOfTerms = numOfTerms;
        this.actEffDt = actEffDt;
        this.apMaxEffDt = apMaxEffDt;
        this.issueSystemId = issueSystemId;
    }

    public char getCancelResetIndicator() {
        return cancelResetIndicator;
    }

    public void setCancelResetIndicator(char cancelResetIndicator) {
        this.cancelResetIndicator = cancelResetIndicator;
    }

    public char getBcmoppKickIndicator() {
        return bcmoppKickIndicator;
    }

    public void setBcmoppKickIndicator(char bcmoppKickIndicator) {
        this.bcmoppKickIndicator = bcmoppKickIndicator;
    }

    public char getCnrIndicator() {
        return cnrIndicator;
    }

    public void setCnrIndicator(char cnrIndicator) {
        this.cnrIndicator = cnrIndicator;
    }

    public Double getAnnualPremium() {
        return annualPremium;
    }

    public void setAnnualPremium(Double annualPremium) {
        this.annualPremium = annualPremium;
    }

    public Double getAnnualPrm() {
        return annualPrm;
    }

    public void setAnnualPrm(Double annualPrm) {
        this.annualPrm = annualPrm;
    }

    public Double getTermPrm() {
        return termPrm;
    }

    public void setTermPrm(Double termPrm) {
        this.termPrm = termPrm;
    }

    public Double getCoveragePrm() {
        return coveragePrm;
    }

    public void setCoveragePrm(Double coveragePrm) {
        this.coveragePrm = coveragePrm;
    }

    public Double getCovFct() {
        return covFct;
    }

    public void setCovFct(Double covFct) {
        this.covFct = covFct;
    }

    public Double getInvFct() {
        return invFct;
    }

    public void setInvFct(Double invFct) {
        this.invFct = invFct;
    }

    public Double getDateTimeFct() {
        return dateFct;
    }

    public void setDateTimeFct(Double dateFct) {
        this.dateFct = dateFct;
    }

    public int getCoverageDuration() {
        return coverageDuration;
    }

    public void setCoverageDuration(int coverageDuration) {
        this.coverageDuration = coverageDuration;
    }

    public int getPolicyDuration() {
        return policyDuration;
    }

    public void setPolicyDuration(int policyDuration) {
        this.policyDuration = policyDuration;
    }

    public Double getCalculatedScgAmt() {
        return calculatedScgAmt;
    }

    public void setCalculatedScgAmt(Double calculatedScgAmt) {
        this.calculatedScgAmt = calculatedScgAmt;
    }

    public Double getNewTermScgAmt() {
        return newTermScgAmt;
    }

    public void setNewTermScgAmt(Double newTermScgAmt) {
        this.newTermScgAmt = newTermScgAmt;
    }

    public Double getBspScgTempAmt() {
        return bspScgTempAmt;
    }

    public void setBspScgTempAmt(Double bspScgTempAmt) {
        this.bspScgTempAmt = bspScgTempAmt;
    }

    public Double getCalculatedPrmAmt() {
        return calculatedPrmAmt;
    }

    public void setCalculatedPrmAmt(Double calculatedPrmAmt) {
        this.calculatedPrmAmt = calculatedPrmAmt;
    }

    public Double getGradPrmAmt() {
        return gradPrmAmt;
    }

    public void setGradPrmAmt(Double gradPrmAmt) {
        this.gradPrmAmt = gradPrmAmt;
    }

    public Double getGradScgAmt() {
        return gradScgAmt;
    }

    public void setGradScgAmt(Double gradScgAmt) {
        this.gradScgAmt = gradScgAmt;
    }

    public Double getAcctPlanSvcAmt() {
        return acctPlanSvcAmt;
    }

    public void setAcctPlanSvcAmt(Double acctPlanSvcAmt) {
        this.acctPlanSvcAmt = acctPlanSvcAmt;
    }

    public String getPolicyIdTrm() {
        return policyIdTrm;
    }

    public void setPolicyIdTrm(String policyIdTrm) {
        this.policyIdTrm = policyIdTrm;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public ZonedDateTime getMaxPolicyExpDt() {
        return maxPolicyExpDt;
    }

    public void setMaxPolicyExpDt(ZonedDateTime maxPolicyExpDt) {
        this.maxPolicyExpDt = maxPolicyExpDt;
    }

    public ZonedDateTime getIssueAcyTs() {
        return issueAcyTs;
    }

    public void setIssueAcyTs(ZonedDateTime issueAcyTs) {
        this.issueAcyTs = issueAcyTs;
    }

    public String getBamAmtTs() {
        return bamAmtTs;
    }

    public void setBamAmtTs(String bamAmtTs) {
        this.bamAmtTs = bamAmtTs;
    }

    public short getBamSeqNbr() {
        return bamSeqNbr;
    }

    public void setBamSeqNbr(short bamSeqNbr) {
        this.bamSeqNbr = bamSeqNbr;
    }

    public short getBilSeqNbr() {
        return bilSeqNbr;
    }

    public void setBilSeqNbr(short bilSeqNbr) {
        this.bilSeqNbr = bilSeqNbr;
    }

    public char getBamEffInd() {
        return bamEffInd;
    }

    public void setBamEffInd(char bamEffInd) {
        this.bamEffInd = bamEffInd;
    }

    public ZonedDateTime getMaxPolEffDt() {
        return maxPolEffDt;
    }

    public void setMaxPolEffDt(ZonedDateTime maxPolEffDt) {
        this.maxPolEffDt = maxPolEffDt;
    }

    public ZonedDateTime getMamMaxEffDt() {
        return mamMaxEffDt;
    }

    public void setMamMaxEffDt(ZonedDateTime mamMaxEffDt) {
        this.mamMaxEffDt = mamMaxEffDt;
    }

    public ZonedDateTime getIstPolEffDt() {
        return istPolEffDt;
    }

    public void setIstPolEffDt(ZonedDateTime istPolEffDt) {
        this.istPolEffDt = istPolEffDt;
    }

    public ZonedDateTime getBccBilAdjDueDt() {
        return bccBilAdjDueDt;
    }

    public void setBccBilAdjDueDt(ZonedDateTime bccBilAdjDueDt) {
        this.bccBilAdjDueDt = bccBilAdjDueDt;
    }

    public short getNumOfTerms() {
        return numOfTerms;
    }

    public void setNumOfTerms(short numOfTerms) {
        this.numOfTerms = numOfTerms;
    }

    public ZonedDateTime getActEffDt() {
        return actEffDt;
    }

    public void setActEffDt(ZonedDateTime actEffDt) {
        this.actEffDt = actEffDt;
    }

    public ZonedDateTime getApMaxEffDt() {
        return apMaxEffDt;
    }

    public void setApMaxEffDt(ZonedDateTime apMaxEffDt) {
        this.apMaxEffDt = apMaxEffDt;
    }

    public String getIssueSystemId() {
        return issueSystemId;
    }

    public void setIssueSystemId(String issueSystemId) {
        this.issueSystemId = issueSystemId;
    }
}
