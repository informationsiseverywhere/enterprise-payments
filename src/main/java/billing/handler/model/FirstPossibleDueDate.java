package billing.handler.model;

import java.time.ZonedDateTime;

public class FirstPossibleDueDate {

    private ZonedDateTime invoiceDate;
    private ZonedDateTime date;
    private ZonedDateTime pendingCancellationDate;
    private ZonedDateTime lookAheadDate;
    private ZonedDateTime referenceDate;
    private ZonedDateTime referenceDateOnTime;
    private boolean isInvoicedToday;
    private boolean isFpddSet = false;

    public ZonedDateTime getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(ZonedDateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public ZonedDateTime getPendingCancellationDate() {
        return pendingCancellationDate;
    }

    public void setPendingCancellationDate(ZonedDateTime pendingCancellationDate) {
        this.pendingCancellationDate = pendingCancellationDate;
    }

    public ZonedDateTime getLookAheadDate() {
        return lookAheadDate;
    }

    public void setLookAheadDate(ZonedDateTime lookAheadDate) {
        this.lookAheadDate = lookAheadDate;
    }

    public ZonedDateTime getReferenceDate() {
        return referenceDate;
    }

    public void setReferenceDate(ZonedDateTime referenceDate) {
        this.referenceDate = referenceDate;
    }

    public ZonedDateTime getReferenceDateOnTime() {
        return referenceDateOnTime;
    }

    public void setReferenceDateOnTime(ZonedDateTime referenceDateOnTime) {
        this.referenceDateOnTime = referenceDateOnTime;
    }

    public boolean isInvoicedToday() {
        return isInvoicedToday;
    }

    public void setInvoicedToday(boolean isInvoicedToday) {
        this.isInvoicedToday = isInvoicedToday;
    }

    public boolean isFpddSet() {
        return isFpddSet;
    }

    public void setFpddSet(boolean isFpddSet) {
        this.isFpddSet = isFpddSet;
    }

}
