package billing.handler.model;

import java.io.Serializable;
import java.time.ZonedDateTime;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActRules;
import billing.data.entity.BilSupportPlan;

public class AdjustDatesReviewDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    private BilAccount bilAccount;
    private BilActRules bilActRules;
    private BilSupportPlan bilSupportPlan;
    private boolean eftCollectionMethod;
    private char fpddIndicator;
    private int referenceDuration;
    private short dueDateDisplacementNumber;
    private boolean isSeasonalBill;
    private char outOfCycle;
    private boolean isManualReinvoice;
    private boolean isErcvRule;
    private int ercvDays;
    private ZonedDateTime earliestEftDueDt;
    private boolean isErcvIssDtInvoice;
    private boolean isAccountBilled;
    private boolean isLateEntered;
    private int dicvDays;
    private boolean isDicvRule;
    private boolean isFpddOnTimeProcessing;
    private char wsceRule;
    private boolean isWsceBamFound;
    private boolean isSpcrRule;
    private ZonedDateTime holdNewDate;
    private ZonedDateTime serviceDueDate;
    private ZonedDateTime termEffDate;
    private char efftiveTypeCode;
    private ZonedDateTime newSystemDate;
    private ZonedDateTime newAdjustDueDate;
    private ZonedDateTime newInvoiceDate;
    private ZonedDateTime newReferenceDate;
    private char dateType;
    private boolean isDpifRule;
    private double dpifAmount;

    public AdjustDatesReviewDetails() {
        super();
    }

    public AdjustDatesReviewDetails(BilAccount bilAccount, BilActRules bilActRules, BilSupportPlan bilSupportPlan,
            boolean eftCollectionMethod, char fpddIndicator, int referenceDuration, short dueDateDisplacementNumber,
            boolean isSeasonalBill, char outOfCycle, boolean isManualReinvoice, boolean isErcvRule, int ercvDays,
            ZonedDateTime earliestEftDueDt, boolean isErcvIssDtInvoice, boolean isAccountBilled, boolean isLateEntered,
            int dicvDays, boolean isDicvRule, boolean isFpddOnTimeProcessing, char wsceRule, boolean isWsceBamFound,
            boolean isSpcrRule, ZonedDateTime holdNewDate, ZonedDateTime serviceDueDate, ZonedDateTime termEffDate,
            char efftiveTypeCode, ZonedDateTime newSystemDate, ZonedDateTime newAdjustDueDate,
            ZonedDateTime newInvoiceDate, ZonedDateTime newReferenceDate, char dateType, boolean isDpifRule,
            double dpifAmount) {
        super();
        this.bilAccount = bilAccount;
        this.bilActRules = bilActRules;
        this.bilSupportPlan = bilSupportPlan;
        this.eftCollectionMethod = eftCollectionMethod;
        this.fpddIndicator = fpddIndicator;
        this.referenceDuration = referenceDuration;
        this.dueDateDisplacementNumber = dueDateDisplacementNumber;
        this.isSeasonalBill = isSeasonalBill;
        this.outOfCycle = outOfCycle;
        this.isManualReinvoice = isManualReinvoice;
        this.isErcvRule = isErcvRule;
        this.ercvDays = ercvDays;
        this.earliestEftDueDt = earliestEftDueDt;
        this.isErcvIssDtInvoice = isErcvIssDtInvoice;
        this.isAccountBilled = isAccountBilled;
        this.isLateEntered = isLateEntered;
        this.dicvDays = dicvDays;
        this.isDicvRule = isDicvRule;
        this.isFpddOnTimeProcessing = isFpddOnTimeProcessing;
        this.wsceRule = wsceRule;
        this.isWsceBamFound = isWsceBamFound;
        this.isSpcrRule = isSpcrRule;
        this.holdNewDate = holdNewDate;
        this.serviceDueDate = serviceDueDate;
        this.termEffDate = termEffDate;
        this.efftiveTypeCode = efftiveTypeCode;
        this.newSystemDate = newSystemDate;
        this.newAdjustDueDate = newAdjustDueDate;
        this.newInvoiceDate = newInvoiceDate;
        this.newReferenceDate = newReferenceDate;
        this.dateType = dateType;
        this.isDpifRule = isDpifRule;
        this.dpifAmount = dpifAmount;
    }

    public BilAccount getBilAccount() {
        return bilAccount;
    }

    public void setBilAccount(BilAccount bilAccount) {
        this.bilAccount = bilAccount;
    }

    public BilActRules getBilActRules() {
        return bilActRules;
    }

    public void setBilActRules(BilActRules bilActRules) {
        this.bilActRules = bilActRules;
    }

    public BilSupportPlan getBilSupportPlan() {
        return bilSupportPlan;
    }

    public void setBilSupportPlan(BilSupportPlan bilSupportPlan) {
        this.bilSupportPlan = bilSupportPlan;
    }

    public boolean isEftCollectionMethod() {
        return eftCollectionMethod;
    }

    public void setEftCollectionMethod(boolean eftCollectionMethod) {
        this.eftCollectionMethod = eftCollectionMethod;
    }

    public char getFpddIndicator() {
        return fpddIndicator;
    }

    public void setFpddIndicator(char fpddIndicator) {
        this.fpddIndicator = fpddIndicator;
    }

    public int getReferenceDuration() {
        return referenceDuration;
    }

    public void setReferenceDuration(int referenceDuration) {
        this.referenceDuration = referenceDuration;
    }

    public short getDueDateDisplacementNumber() {
        return dueDateDisplacementNumber;
    }

    public void setDueDateDisplacementNumber(short dueDateDisplacementNumber) {
        this.dueDateDisplacementNumber = dueDateDisplacementNumber;
    }

    public boolean isSeasonalBill() {
        return isSeasonalBill;
    }

    public void setSeasonalBill(boolean isSeasonalBill) {
        this.isSeasonalBill = isSeasonalBill;
    }

    public char getOutOfCycle() {
        return outOfCycle;
    }

    public void setOutOfCycle(char outOfCycle) {
        this.outOfCycle = outOfCycle;
    }

    public boolean isManualReinvoice() {
        return isManualReinvoice;
    }

    public void setManualReinvoice(boolean isManualReinvoice) {
        this.isManualReinvoice = isManualReinvoice;
    }

    public boolean isErcvRule() {
        return isErcvRule;
    }

    public void setErcvRule(boolean isErcvRule) {
        this.isErcvRule = isErcvRule;
    }

    public int getErcvDays() {
        return ercvDays;
    }

    public void setErcvDays(int ercvDays) {
        this.ercvDays = ercvDays;
    }

    public ZonedDateTime getEarliestEftDueDt() {
        return earliestEftDueDt;
    }

    public void setEarliestEftDueDt(ZonedDateTime earliestEftDueDt) {
        this.earliestEftDueDt = earliestEftDueDt;
    }

    public boolean isErcvIssDtInvoice() {
        return isErcvIssDtInvoice;
    }

    public void setErcvIssDtInvoice(boolean isErcvIssDtInvoice) {
        this.isErcvIssDtInvoice = isErcvIssDtInvoice;
    }

    public boolean isAccountBilled() {
        return isAccountBilled;
    }

    public void setAccountBilled(boolean isAccountBilled) {
        this.isAccountBilled = isAccountBilled;
    }

    public boolean isLateEntered() {
        return isLateEntered;
    }

    public void setLateEntered(boolean isLateEntered) {
        this.isLateEntered = isLateEntered;
    }

    public int getDicvDays() {
        return dicvDays;
    }

    public void setDicvDays(int dicvDays) {
        this.dicvDays = dicvDays;
    }

    public boolean isDicvRule() {
        return isDicvRule;
    }

    public void setDicvRule(boolean isDicvRule) {
        this.isDicvRule = isDicvRule;
    }

    public boolean isFpddOnTimeProcessing() {
        return isFpddOnTimeProcessing;
    }

    public void setFpddOnTimeProcessing(boolean isFpddOnTimeProcessing) {
        this.isFpddOnTimeProcessing = isFpddOnTimeProcessing;
    }

    public char getWsceRule() {
        return wsceRule;
    }

    public void setWsceRule(char wsceRule) {
        this.wsceRule = wsceRule;
    }

    public boolean isWsceBamFound() {
        return isWsceBamFound;
    }

    public void setWsceBamFound(boolean isWsceBamFound) {
        this.isWsceBamFound = isWsceBamFound;
    }

    public boolean isSpcrRule() {
        return isSpcrRule;
    }

    public void setSpcrRule(boolean isSpcrRule) {
        this.isSpcrRule = isSpcrRule;
    }

    public ZonedDateTime getHoldNewDate() {
        return holdNewDate;
    }

    public void setHoldNewDate(ZonedDateTime holdNewDate) {
        this.holdNewDate = holdNewDate;
    }

    public ZonedDateTime getServiceDueDate() {
        return serviceDueDate;
    }

    public void setServiceDueDate(ZonedDateTime serviceDueDate) {
        this.serviceDueDate = serviceDueDate;
    }

    public ZonedDateTime getNewSystemDate() {
        return newSystemDate;
    }

    public void setNewSystemDate(ZonedDateTime newSystemDate) {
        this.newSystemDate = newSystemDate;
    }

    public ZonedDateTime getNewAdjustDueDate() {
        return newAdjustDueDate;
    }

    public void setNewAdjustDueDate(ZonedDateTime newAdjustDueDate) {
        this.newAdjustDueDate = newAdjustDueDate;
    }

    public ZonedDateTime getNewInvoiceDate() {
        return newInvoiceDate;
    }

    public void setNewInvoiceDate(ZonedDateTime newInvoiceDate) {
        this.newInvoiceDate = newInvoiceDate;
    }

    public ZonedDateTime getNewReferenceDate() {
        return newReferenceDate;
    }

    public void setNewReferenceDate(ZonedDateTime newReferenceDate) {
        this.newReferenceDate = newReferenceDate;
    }

    public char getDateType() {
        return dateType;
    }

    public void setDateType(char dateType) {
        this.dateType = dateType;
    }

    public ZonedDateTime getTermEffDate() {
        return termEffDate;
    }

    public void setTermEffDate(ZonedDateTime termEffDate) {
        this.termEffDate = termEffDate;
    }

    public char getEfftiveTypeCode() {
        return efftiveTypeCode;
    }

    public void setEfftiveTypeCode(char efftiveTypeCode) {
        this.efftiveTypeCode = efftiveTypeCode;
    }

    public boolean isDpifRule() {
        return isDpifRule;
    }

    public void setDpifRule(boolean isDpifRule) {
        this.isDpifRule = isDpifRule;
    }

    public double getDpifAmount() {
        return dpifAmount;
    }

    public void setDpifAmount(double dpifAmount) {
        this.dpifAmount = dpifAmount;
    }

}
