package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.DECIMAL_ZERO;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ChargeVariables extends ResourceSupport {

    private String callModuleName = BLANK_STRING;
    private String accountId = BLANK_STRING;
    private String bilTypeCd = BLANK_STRING;
    private String bilClassCd = BLANK_STRING;
    private String eftPlanCd = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime acctDueDate;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime effectiveDate;
    private char chargeEffectiveTypeCd = BLANK_CHAR;
    private char tmpEffectiveTypeCd = BLANK_CHAR;
    private boolean isEffectActive = false;
    private boolean isSpcrRule = false;
    private char sptEftScgInd = BLANK_CHAR;
    private String scgTypeCd = BLANK_STRING;
    private char scgActionCd = BLANK_CHAR;
    private Double svcCrgAmt = DECIMAL_ZERO;
    private Double nsfPnyAmt = DECIMAL_ZERO;
    private Double lateCrgAmt = DECIMAL_ZERO;
    private Double lateCrgPct = DECIMAL_ZERO;
    private Double prmAmount = DECIMAL_ZERO;
    private Double scgIcrAmount = DECIMAL_ZERO;
    private Double prmIcrAmount = DECIMAL_ZERO;
    private Double maxScgAmount = DECIMAL_ZERO;
    private String PrevPolicyId = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime PrevTermEffDate;
    private boolean isCrgEff = false;
    private String policyId = BLANK_STRING;
    private String policyNumber = BLANK_STRING;
    private String polSymbolCd = BLANK_STRING;
    private char polEffTypCd = BLANK_CHAR;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime PrevTrmEffDate;
    private boolean isPersActive = false;
    private char existenceCheck = BLANK_CHAR;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime max1EffDt;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime max2EffDt;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime min1EffDt;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime min2EffDt;
    private String filler = BLANK_STRING;

    public ChargeVariables() {
        super();
        // TODO Auto-generated constructor stub
    }

    @JsonCreator
    public ChargeVariables(@JsonProperty("callModuleName") String callModuleName,
            @JsonProperty("accountId") String accountId, @JsonProperty("bilTypeCd") String bilTypeCd,
            @JsonProperty("bilClassCd") String bilClassCd, @JsonProperty("eftPlanCd") String eftPlanCd,
            @JsonProperty("acctDueDate") ZonedDateTime acctDueDate,
            @JsonProperty("effectiveDate") ZonedDateTime effectiveDate,
            @JsonProperty("chargeEffectiveTypeCd") char chargeEffectiveTypeCd,
            @JsonProperty("tmpEffectiveTypeCd") char tmpEffectiveTypeCd,
            @JsonProperty("isEffectActive") boolean isEffectActive, @JsonProperty("isSpcrRule") boolean isSpcrRule,
            @JsonProperty("sptEftScgInd") char sptEftScgInd, @JsonProperty("scgTypeCd") String scgTypeCd,
            @JsonProperty("scgActionCd") char scgActionCd, @JsonProperty("svcCrgAmt") Double svcCrgAmt,
            @JsonProperty("nsfPnyAmt") Double nsfPnyAmt, @JsonProperty("lateCrgAmt") Double lateCrgAmt,
            @JsonProperty("lateCrgPct") Double lateCrgPct, @JsonProperty("prmAmount") Double prmAmount,
            @JsonProperty("scgIcrAmount") Double scgIcrAmount, @JsonProperty("prmIcrAmount") Double prmIcrAmount,
            @JsonProperty("maxScgAmount") Double maxScgAmount, @JsonProperty("prevPolicyId") String prevPolicyId,
            @JsonProperty("prevTermEffDate") ZonedDateTime prevTermEffDate, @JsonProperty("isCrgEff") boolean isCrgEff,
            @JsonProperty("policyId") String policyId, @JsonProperty("policyNumber") String policyNumber,
            @JsonProperty("polSymbolCd") String polSymbolCd, @JsonProperty("polEffTypCd") char polEffTypCd,
            @JsonProperty("prevTrmEffDate") ZonedDateTime prevTrmEffDate,
            @JsonProperty("isPersActive") boolean isPersActive, @JsonProperty("existenceCheck") char existenceCheck,
            @JsonProperty("max1EffDt") ZonedDateTime max1EffDt, @JsonProperty("max2EffDt") ZonedDateTime max2EffDt,
            @JsonProperty("min1EffDt") ZonedDateTime min1EffDt, @JsonProperty("min2EffDt") ZonedDateTime min2EffDt,
            @JsonProperty("filler") String filler) {
        super();
        this.callModuleName = callModuleName;
        this.accountId = accountId;
        this.bilTypeCd = bilTypeCd;
        this.bilClassCd = bilClassCd;
        this.eftPlanCd = eftPlanCd;
        this.acctDueDate = acctDueDate;
        this.effectiveDate = effectiveDate;
        this.chargeEffectiveTypeCd = chargeEffectiveTypeCd;
        this.tmpEffectiveTypeCd = tmpEffectiveTypeCd;
        this.isEffectActive = isEffectActive;
        this.isSpcrRule = isSpcrRule;
        this.sptEftScgInd = sptEftScgInd;
        this.scgTypeCd = scgTypeCd;
        this.scgActionCd = scgActionCd;
        this.svcCrgAmt = svcCrgAmt;
        this.nsfPnyAmt = nsfPnyAmt;
        this.lateCrgAmt = lateCrgAmt;
        this.lateCrgPct = lateCrgPct;
        this.prmAmount = prmAmount;
        this.scgIcrAmount = scgIcrAmount;
        this.prmIcrAmount = prmIcrAmount;
        this.maxScgAmount = maxScgAmount;
        PrevPolicyId = prevPolicyId;
        PrevTermEffDate = prevTermEffDate;
        this.isCrgEff = isCrgEff;
        this.policyId = policyId;
        this.policyNumber = policyNumber;
        this.polSymbolCd = polSymbolCd;
        this.polEffTypCd = polEffTypCd;
        PrevTrmEffDate = prevTrmEffDate;
        this.isPersActive = isPersActive;
        this.existenceCheck = existenceCheck;
        this.max1EffDt = max1EffDt;
        this.max2EffDt = max2EffDt;
        this.min1EffDt = min1EffDt;
        this.min2EffDt = min2EffDt;
        this.filler = filler;
    }

    public String getCallModuleName() {
        return callModuleName;
    }

    public void setCallModuleName(String callModuleName) {
        this.callModuleName = callModuleName;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBilTypeCd() {
        return bilTypeCd;
    }

    public void setBilTypeCd(String bilTypeCd) {
        this.bilTypeCd = bilTypeCd;
    }

    public String getBilClassCd() {
        return bilClassCd;
    }

    public void setBilClassCd(String bilClassCd) {
        this.bilClassCd = bilClassCd;
    }

    public String getEftPlanCd() {
        return eftPlanCd;
    }

    public void setEftPlanCd(String eftPlanCd) {
        this.eftPlanCd = eftPlanCd;
    }

    public ZonedDateTime getAcctDueDate() {
        return acctDueDate;
    }

    public void setAcctDueDate(ZonedDateTime acctDueDate) {
        this.acctDueDate = acctDueDate;
    }

    public ZonedDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public char getChargeEffectiveTypeCd() {
        return chargeEffectiveTypeCd;
    }

    public void setChargeEffectiveTypeCd(char chargeEffectiveTypeCd) {
        this.chargeEffectiveTypeCd = chargeEffectiveTypeCd;
    }

    public char getTmpEffectiveTypeCd() {
        return tmpEffectiveTypeCd;
    }

    public void setTmpEffectiveTypeCd(char tmpEffectiveTypeCd) {
        this.tmpEffectiveTypeCd = tmpEffectiveTypeCd;
    }

    public boolean isEffectActive() {
        return isEffectActive;
    }

    public void setEffectActive(boolean isEffectActive) {
        this.isEffectActive = isEffectActive;
    }

    public boolean isSpcrRule() {
        return isSpcrRule;
    }

    public void setSpcrRule(boolean isSpcrRule) {
        this.isSpcrRule = isSpcrRule;
    }

    public char getSptEftScgInd() {
        return sptEftScgInd;
    }

    public void setSptEftScgInd(char sptEftScgInd) {
        this.sptEftScgInd = sptEftScgInd;
    }

    public String getScgTypeCd() {
        return scgTypeCd;
    }

    public void setScgTypeCd(String scgTypeCd) {
        this.scgTypeCd = scgTypeCd;
    }

    public char getScgActionCd() {
        return scgActionCd;
    }

    public void setScgActionCd(char scgActionCd) {
        this.scgActionCd = scgActionCd;
    }

    public Double getSvcCrgAmt() {
        return svcCrgAmt;
    }

    public void setSvcCrgAmt(Double svcCrgAmt) {
        this.svcCrgAmt = svcCrgAmt;
    }

    public Double getNsfPnyAmt() {
        return nsfPnyAmt;
    }

    public void setNsfPnyAmt(Double nsfPnyAmt) {
        this.nsfPnyAmt = nsfPnyAmt;
    }

    public Double getLateCrgAmt() {
        return lateCrgAmt;
    }

    public void setLateCrgAmt(Double lateCrgAmt) {
        this.lateCrgAmt = lateCrgAmt;
    }

    public Double getLateCrgPct() {
        return lateCrgPct;
    }

    public void setLateCrgPct(Double lateCrgPct) {
        this.lateCrgPct = lateCrgPct;
    }

    public Double getPrmAmount() {
        return prmAmount;
    }

    public void setPrmAmount(Double prmAmount) {
        this.prmAmount = prmAmount;
    }

    public Double getScgIcrAmount() {
        return scgIcrAmount;
    }

    public void setScgIcrAmount(Double scgIcrAmount) {
        this.scgIcrAmount = scgIcrAmount;
    }

    public Double getPrmIcrAmount() {
        return prmIcrAmount;
    }

    public void setPrmIcrAmount(Double prmIcrAmount) {
        this.prmIcrAmount = prmIcrAmount;
    }

    public Double getMaxScgAmount() {
        return maxScgAmount;
    }

    public void setMaxScgAmount(Double maxScgAmount) {
        this.maxScgAmount = maxScgAmount;
    }

    public String getPrevPolicyId() {
        return PrevPolicyId;
    }

    public void setPrevPolicyId(String prevPolicyId) {
        PrevPolicyId = prevPolicyId;
    }

    public ZonedDateTime getPrevTermEffDate() {
        return PrevTermEffDate;
    }

    public void setPrevTermEffDate(ZonedDateTime prevTermEffDate) {
        PrevTermEffDate = prevTermEffDate;
    }

    public boolean isCrgEff() {
        return isCrgEff;
    }

    public void setCrgEff(boolean isCrgEff) {
        this.isCrgEff = isCrgEff;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolSymbolCd() {
        return polSymbolCd;
    }

    public void setPolSymbolCd(String polSymbolCd) {
        this.polSymbolCd = polSymbolCd;
    }

    public char getPolEffTypCd() {
        return polEffTypCd;
    }

    public void setPolEffTypCd(char polEffTypCd) {
        this.polEffTypCd = polEffTypCd;
    }

    public ZonedDateTime getPrevTrmEffDate() {
        return PrevTrmEffDate;
    }

    public void setPrevTrmEffDate(ZonedDateTime prevTrmEffDate) {
        PrevTrmEffDate = prevTrmEffDate;
    }

    public boolean isPersActive() {
        return isPersActive;
    }

    public void setPersActive(boolean isPersActive) {
        this.isPersActive = isPersActive;
    }

    public char getExistenceCheck() {
        return existenceCheck;
    }

    public void setExistenceCheck(char existenceCheck) {
        this.existenceCheck = existenceCheck;
    }

    public ZonedDateTime getMax1EffDt() {
        return max1EffDt;
    }

    public void setMax1EffDt(ZonedDateTime max1EffDt) {
        this.max1EffDt = max1EffDt;
    }

    public ZonedDateTime getMax2EffDt() {
        return max2EffDt;
    }

    public void setMax2EffDt(ZonedDateTime max2EffDt) {
        this.max2EffDt = max2EffDt;
    }

    public ZonedDateTime getMin1EffDt() {
        return min1EffDt;
    }

    public void setMin1EffDt(ZonedDateTime min1EffDt) {
        this.min1EffDt = min1EffDt;
    }

    public ZonedDateTime getMin2EffDt() {
        return min2EffDt;
    }

    public void setMin2EffDt(ZonedDateTime min2EffDt) {
        this.min2EffDt = min2EffDt;
    }

    public String getFiller() {
        return filler;
    }

    public void setFiller(String filler) {
        this.filler = filler;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(PrevPolicyId, PrevTermEffDate, PrevTrmEffDate, accountId, acctDueDate,
                bilClassCd, bilTypeCd, callModuleName, chargeEffectiveTypeCd, effectiveDate, eftPlanCd, existenceCheck,
                filler, isCrgEff, isEffectActive, isPersActive, isSpcrRule, lateCrgAmt, lateCrgPct, max1EffDt,
                max2EffDt, maxScgAmount, min1EffDt, min2EffDt, nsfPnyAmt, polEffTypCd, polSymbolCd, policyId,
                policyNumber, prmAmount, prmIcrAmount, scgActionCd, scgIcrAmount, scgTypeCd, sptEftScgInd, svcCrgAmt,
                tmpEffectiveTypeCd);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChargeVariables other = (ChargeVariables) obj;
        return Objects.equals(PrevPolicyId, other.PrevPolicyId)
                && Objects.equals(PrevTermEffDate, other.PrevTermEffDate)
                && Objects.equals(PrevTrmEffDate, other.PrevTrmEffDate) && Objects.equals(accountId, other.accountId)
                && Objects.equals(acctDueDate, other.acctDueDate) && Objects.equals(bilClassCd, other.bilClassCd)
                && Objects.equals(bilTypeCd, other.bilTypeCd) && Objects.equals(callModuleName, other.callModuleName)
                && chargeEffectiveTypeCd == other.chargeEffectiveTypeCd
                && Objects.equals(effectiveDate, other.effectiveDate) && Objects.equals(eftPlanCd, other.eftPlanCd)
                && existenceCheck == other.existenceCheck && Objects.equals(filler, other.filler)
                && isCrgEff == other.isCrgEff && isEffectActive == other.isEffectActive
                && isPersActive == other.isPersActive && isSpcrRule == other.isSpcrRule
                && Objects.equals(lateCrgAmt, other.lateCrgAmt) && Objects.equals(lateCrgPct, other.lateCrgPct)
                && Objects.equals(max1EffDt, other.max1EffDt) && Objects.equals(max2EffDt, other.max2EffDt)
                && Objects.equals(maxScgAmount, other.maxScgAmount) && Objects.equals(min1EffDt, other.min1EffDt)
                && Objects.equals(min2EffDt, other.min2EffDt) && Objects.equals(nsfPnyAmt, other.nsfPnyAmt)
                && polEffTypCd == other.polEffTypCd && Objects.equals(polSymbolCd, other.polSymbolCd)
                && Objects.equals(policyId, other.policyId) && Objects.equals(policyNumber, other.policyNumber)
                && Objects.equals(prmAmount, other.prmAmount) && Objects.equals(prmIcrAmount, other.prmIcrAmount)
                && scgActionCd == other.scgActionCd && Objects.equals(scgIcrAmount, other.scgIcrAmount)
                && Objects.equals(scgTypeCd, other.scgTypeCd) && sptEftScgInd == other.sptEftScgInd
                && Objects.equals(svcCrgAmt, other.svcCrgAmt) && tmpEffectiveTypeCd == other.tmpEffectiveTypeCd;
    }

}