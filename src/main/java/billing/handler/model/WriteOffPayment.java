package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

public class WriteOffPayment extends ResourceSupport {

    private char processIndicator = BLANK_CHAR;
    private double paymentAmount;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime adjustDueDate;
    private String policyNumber = BLANK_STRING;
    private String quoteIndicator = BLANK_STRING;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime applicationDate;

    public WriteOffPayment() {
        super();
    }

    public WriteOffPayment(char processIndicator, double paymentAmount, ZonedDateTime adjustDueDate,
            String policyNumber, String quoteIndicator, ZonedDateTime applicationDate) {
        super();
        this.processIndicator = processIndicator;
        this.paymentAmount = paymentAmount;
        this.adjustDueDate = adjustDueDate;
        this.policyNumber = policyNumber;
        this.quoteIndicator = quoteIndicator;
        this.applicationDate = applicationDate;
    }

    public char getProcessIndicator() {
        return processIndicator;
    }

    public void setProcessIndicator(char processIndicator) {
        this.processIndicator = processIndicator;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public ZonedDateTime getAdjustDueDate() {
        return adjustDueDate;
    }

    public void setAdjustDueDate(ZonedDateTime adjustDueDate) {
        this.adjustDueDate = adjustDueDate;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getQuoteIndicator() {
        return quoteIndicator;
    }

    public void setQuoteIndicator(String quoteIndicator) {
        this.quoteIndicator = quoteIndicator;
    }

    public ZonedDateTime getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(ZonedDateTime applicationDate) {
        this.applicationDate = applicationDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(adjustDueDate, applicationDate, paymentAmount, policyNumber,
                processIndicator, quoteIndicator);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        WriteOffPayment other = (WriteOffPayment) obj;
        return Objects.equals(adjustDueDate, other.adjustDueDate)
                && Objects.equals(applicationDate, other.applicationDate)
                && Double.doubleToLongBits(paymentAmount) == Double.doubleToLongBits(other.paymentAmount)
                && Objects.equals(policyNumber, other.policyNumber) && processIndicator == other.processIndicator
                && Objects.equals(quoteIndicator, other.quoteIndicator);
    }

}
