package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;

import core.utils.DateRoutine;

public class MaxPendingCancellation {

    private String policyId = BLANK_STRING;
    private String policyNumber = BLANK_STRING;
    private String policySymbol = BLANK_STRING;
    private ZonedDateTime effectiveDate;
    private String masterCompany = BLANK_STRING;
    private String lineOfBusiness = BLANK_STRING;
    private String riskState = BLANK_STRING;
    private String issueSystemId = BLANK_STRING;
    private ZonedDateTime expirationDate;
    private char policyStatus = BLANK_CHAR;
    private ZonedDateTime equityDate = DateRoutine.defaultDateTime();
    private ZonedDateTime standredCancellationDate = DateRoutine.defaultDateTime();
    private ZonedDateTime minPendingCancellationDate = DateRoutine.defaultDateTime();
    private double termBalance = DECIMAL_ZERO;
    private double termPremimumAmount = DECIMAL_ZERO;
    private char cancellationIndicator = BLANK_CHAR;
    private double pendingCancellationAmount = DECIMAL_ZERO;
    private double pendingCancellationPercent = DECIMAL_ZERO;
    private boolean isSplitPolicy = false;
    private boolean isQualifyPcn = false;
    private boolean isNoPcnCpe = false;
    private boolean isNoPcnCpn = false;
    private boolean isNsfPcnCancel = false;
    private boolean isBisTermInvoice = false;
    private boolean isTermNeverBilled = false;
    private ZonedDateTime bilIstDueDate = DateRoutine.defaultDateTime();
    private ZonedDateTime pedEffectiveDate = DateRoutine.defaultDateTime();
    private ZonedDateTime pedExpirationDate = DateRoutine.defaultDateTime();
    private short legalDays = SHORT_ZERO;
    private short mailDays = SHORT_ZERO;
    private short maximumDays = SHORT_ZERO;
    private char pendingCancelFlatCnc = BLANK_CHAR;
    private char reinstCode = BLANK_CHAR;
    private char addBasRenewalIndicator = BLANK_CHAR;

    public MaxPendingCancellation() {
        super();
    }

    public MaxPendingCancellation(String policyId, String policyNumber, String policySymbol,
            ZonedDateTime effectiveDate, String masterCompany, String lineOfBusiness, String riskState,
            String issueSystemId, ZonedDateTime expirationDate, char policyStatus, ZonedDateTime equityDate,
            ZonedDateTime standredCancellationDate, ZonedDateTime minPendingCancellationDate, double termBalance,
            double termPremimumAmount, char cancellationIndicator, double pendingCancellationAmount,
            double pendingCancellationPercent, boolean isSplitPolicy, boolean isQualifyPcn, boolean isNoPcnCpe,
            boolean isNoPcnCpn, boolean isNsfPcnCancel, boolean isBisTermInvoice, boolean isTermNeverBilled,
            ZonedDateTime bilIstDueDate, ZonedDateTime pedEffectiveDate, ZonedDateTime pedExpirationDate,
            short legalDays, short mailDays, short maximumDays, char pendingCancelFlatCnc, char reinstCode,
            char addBasRenewalIndicator) {
        super();
        this.policyId = policyId;
        this.policyNumber = policyNumber;
        this.policySymbol = policySymbol;
        this.effectiveDate = effectiveDate;
        this.masterCompany = masterCompany;
        this.lineOfBusiness = lineOfBusiness;
        this.riskState = riskState;
        this.issueSystemId = issueSystemId;
        this.expirationDate = expirationDate;
        this.policyStatus = policyStatus;
        this.equityDate = equityDate;
        this.standredCancellationDate = standredCancellationDate;
        this.minPendingCancellationDate = minPendingCancellationDate;
        this.termBalance = termBalance;
        this.termPremimumAmount = termPremimumAmount;
        this.cancellationIndicator = cancellationIndicator;
        this.pendingCancellationAmount = pendingCancellationAmount;
        this.pendingCancellationPercent = pendingCancellationPercent;
        this.isSplitPolicy = isSplitPolicy;
        this.isQualifyPcn = isQualifyPcn;
        this.isNoPcnCpe = isNoPcnCpe;
        this.isNoPcnCpn = isNoPcnCpn;
        this.isNsfPcnCancel = isNsfPcnCancel;
        this.isBisTermInvoice = isBisTermInvoice;
        this.isTermNeverBilled = isTermNeverBilled;
        this.bilIstDueDate = bilIstDueDate;
        this.pedEffectiveDate = pedEffectiveDate;
        this.pedExpirationDate = pedExpirationDate;
        this.legalDays = legalDays;
        this.mailDays = mailDays;
        this.maximumDays = maximumDays;
        this.pendingCancelFlatCnc = pendingCancelFlatCnc;
        this.reinstCode = reinstCode;
        this.addBasRenewalIndicator = addBasRenewalIndicator;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicySymbol() {
        return policySymbol;
    }

    public void setPolicySymbol(String policySymbol) {
        this.policySymbol = policySymbol;
    }

    public ZonedDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getMasterCompany() {
        return masterCompany;
    }

    public void setMasterCompany(String masterCompany) {
        this.masterCompany = masterCompany;
    }

    public String getLineOfBusiness() {
        return lineOfBusiness;
    }

    public void setLineOfBusiness(String lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }

    public String getRiskState() {
        return riskState;
    }

    public void setRiskState(String riskState) {
        this.riskState = riskState;
    }

    public String getIssueSystemId() {
        return issueSystemId;
    }

    public void setIssueSystemId(String issueSystemId) {
        this.issueSystemId = issueSystemId;
    }

    public ZonedDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(ZonedDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public char getPolicyStatus() {
        return policyStatus;
    }

    public void setPolicyStatus(char policyStatus) {
        this.policyStatus = policyStatus;
    }

    public ZonedDateTime getEquityDate() {
        return equityDate;
    }

    public void setEquityDate(ZonedDateTime equityDate) {
        this.equityDate = equityDate;
    }

    public ZonedDateTime getStandredCancellationDate() {
        return standredCancellationDate;
    }

    public void setStandredCancellationDate(ZonedDateTime standredCancellationDate) {
        this.standredCancellationDate = standredCancellationDate;
    }

    public ZonedDateTime getMinPendingCancellationDate() {
        return minPendingCancellationDate;
    }

    public void setMinPendingCancellationDate(ZonedDateTime minPendingCancellationDate) {
        this.minPendingCancellationDate = minPendingCancellationDate;
    }

    public double getTermBalance() {
        return termBalance;
    }

    public void setTermBalance(double termBalance) {
        this.termBalance = termBalance;
    }

    public double getTermPremimumAmount() {
        return termPremimumAmount;
    }

    public void setTermPremimumAmount(double termPremimumAmount) {
        this.termPremimumAmount = termPremimumAmount;
    }

    public char getCancellationIndicator() {
        return cancellationIndicator;
    }

    public void setCancellationIndicator(char cancellationIndicator) {
        this.cancellationIndicator = cancellationIndicator;
    }

    public double getPendingCancellationAmount() {
        return pendingCancellationAmount;
    }

    public void setPendingCancellationAmount(double pendingCancellationAmount) {
        this.pendingCancellationAmount = pendingCancellationAmount;
    }

    public double getPendingCancellationPercent() {
        return pendingCancellationPercent;
    }

    public void setPendingCancellationPercent(double pendingCancellationPercent) {
        this.pendingCancellationPercent = pendingCancellationPercent;
    }

    public boolean isSplitPolicy() {
        return isSplitPolicy;
    }

    public void setSplitPolicy(boolean isSplitPolicy) {
        this.isSplitPolicy = isSplitPolicy;
    }

    public boolean isQualifyPcn() {
        return isQualifyPcn;
    }

    public void setQualifyPcn(boolean isQualifyPcn) {
        this.isQualifyPcn = isQualifyPcn;
    }

    public boolean isNoPcnCpe() {
        return isNoPcnCpe;
    }

    public void setNoPcnCpe(boolean isNoPcnCpe) {
        this.isNoPcnCpe = isNoPcnCpe;
    }

    public boolean isNoPcnCpn() {
        return isNoPcnCpn;
    }

    public void setNoPcnCpn(boolean isNoPcnCpn) {
        this.isNoPcnCpn = isNoPcnCpn;
    }

    public boolean isNsfPcnCancel() {
        return isNsfPcnCancel;
    }

    public void setNsfPcnCancel(boolean isNsfPcnCancel) {
        this.isNsfPcnCancel = isNsfPcnCancel;
    }

    public boolean isBisTermInvoice() {
        return isBisTermInvoice;
    }

    public void setBisTermInvoice(boolean isBisTermInvoice) {
        this.isBisTermInvoice = isBisTermInvoice;
    }

    public boolean isTermNeverBilled() {
        return isTermNeverBilled;
    }

    public void setTermNeverBilled(boolean isTermNeverBilled) {
        this.isTermNeverBilled = isTermNeverBilled;
    }

    public ZonedDateTime getBilIstDueDate() {
        return bilIstDueDate;
    }

    public void setBilIstDueDate(ZonedDateTime bilIstDueDate) {
        this.bilIstDueDate = bilIstDueDate;
    }

    public ZonedDateTime getPedEffectiveDate() {
        return pedEffectiveDate;
    }

    public void setPedEffectiveDate(ZonedDateTime pedEffectiveDate) {
        this.pedEffectiveDate = pedEffectiveDate;
    }

    public ZonedDateTime getPedExpirationDate() {
        return pedExpirationDate;
    }

    public void setPedExpirationDate(ZonedDateTime pedExpirationDate) {
        this.pedExpirationDate = pedExpirationDate;
    }

    public short getLegalDays() {
        return legalDays;
    }

    public void setLegalDays(short legalDays) {
        this.legalDays = legalDays;
    }

    public short getMailDays() {
        return mailDays;
    }

    public void setMailDays(short mailDays) {
        this.mailDays = mailDays;
    }

    public short getMaximumDays() {
        return maximumDays;
    }

    public void setMaximumDays(short maximumDays) {
        this.maximumDays = maximumDays;
    }

    public char getPendingCancelFlatCnc() {
        return pendingCancelFlatCnc;
    }

    public void setPendingCancelFlatCnc(char pendingCancelFlatCnc) {
        this.pendingCancelFlatCnc = pendingCancelFlatCnc;
    }

    public char getReinstCode() {
        return reinstCode;
    }

    public void setReinstCode(char reinstCode) {
        this.reinstCode = reinstCode;
    }

    public char getAddBasRenewalIndicator() {
        return addBasRenewalIndicator;
    }

    public void setAddBasRenewalIndicator(char addBasRenewalIndicator) {
        this.addBasRenewalIndicator = addBasRenewalIndicator;
    }

}
