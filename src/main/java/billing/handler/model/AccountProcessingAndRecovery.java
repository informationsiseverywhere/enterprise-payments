package billing.handler.model;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import billing.model.Contract;
import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

public class AccountProcessingAndRecovery extends ResourceSupport {
    private String accountId;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime recoveryDate;
    private char recoverCheckIndicator = BLANK_CHAR;
    private char cashAcyCallIndicator = BLANK_CHAR;
    private char cashStatusCd = BLANK_CHAR;
    private char driverIndicator = BLANK_CHAR;
    private char correctedInvocie = BLANK_CHAR;
    private char quoteIndicator = BLANK_CHAR;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime distributionDate;
    private Short distributionSequenceNumber;
    private String policyId;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime polEffDate;
    private boolean isNsfPcn = false;
    private boolean isForceResind = false;
    private boolean isAccountForce = false;
    private char eftReversalIndicator = BLANK_CHAR;
    private String pendingCancelType = BLANK_STRING;
    private List<Contract> contractList;
    private String userSequenceId = BLANK_STRING;

    public AccountProcessingAndRecovery() {
        super();
    }

    public AccountProcessingAndRecovery(String accountId, ZonedDateTime recoveryDate, char recoverCheckIndicator,
            char cashAcyCallIndicator, char cashStatusCd, char driverIndicator, char correctedInvocie,
            char quoteIndicator, ZonedDateTime distributionDate, Short distributionSequenceNumber, String policyId,
            ZonedDateTime polEffDate, boolean isNsfPcn, boolean isForceResind, boolean isAccountForce,
            char eftReversalIndicator, String userSequenceId) {
        super();
        this.accountId = accountId;
        this.recoveryDate = recoveryDate;
        this.recoverCheckIndicator = recoverCheckIndicator;
        this.cashAcyCallIndicator = cashAcyCallIndicator;
        this.cashStatusCd = cashStatusCd;
        this.driverIndicator = driverIndicator;
        this.correctedInvocie = correctedInvocie;
        this.quoteIndicator = quoteIndicator;
        this.distributionDate = distributionDate;
        this.distributionSequenceNumber = distributionSequenceNumber;
        this.policyId = policyId;
        this.polEffDate = polEffDate;
        this.isNsfPcn = isNsfPcn;
        this.isForceResind = isForceResind;
        this.isAccountForce = isAccountForce;
        this.eftReversalIndicator = eftReversalIndicator;
        this.userSequenceId = userSequenceId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public ZonedDateTime getRecoveryDate() {
        return recoveryDate;
    }

    public void setRecoveryDate(ZonedDateTime recoveryDate) {
        this.recoveryDate = recoveryDate;
    }

    public char getRecoverCheckIndicator() {
        return recoverCheckIndicator;
    }

    public void setRecoverCheckIndicator(char recoverCheckIndicator) {
        this.recoverCheckIndicator = recoverCheckIndicator;
    }

    public char getCashAcyCallIndicator() {
        return cashAcyCallIndicator;
    }

    public void setCashAcyCallIndicator(char cashAcyCallIndicator) {
        this.cashAcyCallIndicator = cashAcyCallIndicator;
    }

    public char getCashStatusCd() {
        return cashStatusCd;
    }

    public void setCashStatusCd(char cashStatusCd) {
        this.cashStatusCd = cashStatusCd;
    }

    public char getQuoteIndicator() {
        return quoteIndicator;
    }

    public void setQuoteIndicator(char quoteIndicator) {
        this.quoteIndicator = quoteIndicator;
    }

    public void setDriverIndicator(char driverIndicator) {
        this.driverIndicator = driverIndicator;
    }

    public char getDriverIndicator() {
        return driverIndicator;
    }

    public char getCorrectedInvocie() {
        return correctedInvocie;
    }

    public void setCorrectedInvocie(char correctedInvocie) {
        this.correctedInvocie = correctedInvocie;
    }

    public ZonedDateTime getDistributionDate() {
        return distributionDate;
    }

    public void setDistributionDate(ZonedDateTime distributionDate) {
        this.distributionDate = distributionDate;
    }

    public Short getDistributionSequenceNumber() {
        return distributionSequenceNumber;
    }

    public void setDistributionSequenceNumber(Short distributionSequenceNumber) {
        this.distributionSequenceNumber = distributionSequenceNumber;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public ZonedDateTime getPolEffDate() {
        return polEffDate;
    }

    public void setPolEffDate(ZonedDateTime polEffDate) {
        this.polEffDate = polEffDate;
    }

    public boolean isNsfPcn() {
        return isNsfPcn;
    }

    public void setNsfPcn(boolean isNsfPcn) {
        this.isNsfPcn = isNsfPcn;
    }

    public boolean isForceResind() {
        return isForceResind;
    }

    public void setForceResind(boolean isForceResind) {
        this.isForceResind = isForceResind;
    }

    public boolean isAccountForce() {
        return isAccountForce;
    }

    public void setAccountForce(boolean isAccountForce) {
        this.isAccountForce = isAccountForce;
    }

    public char getEftReversalIndicator() {
        return eftReversalIndicator;
    }

    public void setEftReversalIndicator(char eftReversalIndicator) {
        this.eftReversalIndicator = eftReversalIndicator;
    }

    public String getPendingCancelType() {
        return pendingCancelType;
    }

    public void setPendingCancelType(String pendingCancelType) {
        this.pendingCancelType = pendingCancelType;
    }

    public List<Contract> getContractList() {
        return contractList;
    }

    public void setContractList(List<Contract> contractList) {
        this.contractList = contractList;
    }
    
    public String getUserSequenceId() {
        return userSequenceId;
    }

    public void setUserSequenceId(String userSequenceId) {
        this.userSequenceId = userSequenceId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountId, cashAcyCallIndicator, cashStatusCd, contractList,
                correctedInvocie, distributionDate, distributionSequenceNumber, driverIndicator, eftReversalIndicator,
                isAccountForce, isForceResind, isNsfPcn, pendingCancelType, polEffDate, policyId, quoteIndicator,
                recoverCheckIndicator, recoveryDate, userSequenceId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AccountProcessingAndRecovery other = (AccountProcessingAndRecovery) obj;
        return Objects.equals(accountId, other.accountId) && cashAcyCallIndicator == other.cashAcyCallIndicator
                && cashStatusCd == other.cashStatusCd && Objects.equals(contractList, other.contractList)
                && correctedInvocie == other.correctedInvocie
                && Objects.equals(distributionDate, other.distributionDate)
                && Objects.equals(distributionSequenceNumber, other.distributionSequenceNumber)
                && driverIndicator == other.driverIndicator && eftReversalIndicator == other.eftReversalIndicator
                && isAccountForce == other.isAccountForce && isForceResind == other.isForceResind
                && isNsfPcn == other.isNsfPcn && Objects.equals(pendingCancelType, other.pendingCancelType)
                && Objects.equals(polEffDate, other.polEffDate) && Objects.equals(policyId, other.policyId)
                && quoteIndicator == other.quoteIndicator && recoverCheckIndicator == other.recoverCheckIndicator
                && Objects.equals(userSequenceId, other.userSequenceId)
                && Objects.equals(recoveryDate, other.recoveryDate);
    }

}
