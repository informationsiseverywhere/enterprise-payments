package billing.handler.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilBillPlan;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.id.BilActRulesId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilActRulesRepository;
import billing.data.repository.BilAmountsRepository;
import billing.data.repository.BilBillPlanRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.handler.service.CalculatePaidToDateService;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants.BilAmountsEftIndicator;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.utils.DateRoutine;

@Service
public class CalculatePaidToDateServiceImpl implements CalculatePaidToDateService {

    @Autowired
    private BilActRulesRepository bilActRulesRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilBillPlanRepository bilBillPlanRepository;

    @Autowired
    private BilAmountsRepository bilAmountsRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    private static final char PTDR_ROUND_UP_HALF_DAY = '1';
    private static final char PTDR_ALWAYS_ROUND_UP = '2';
    private static final char TERM_ANNUAL = CHAR_A;
    private static final char PLUS_SIGN = '+';
    private static final char MINUS_SIGN = '-';
    private static final List<Character> bilAmtEffIndList = new ArrayList<>(Arrays.asList(
            BilAmountsEftIndicator.CANCELLATION_P.getValue(), BilAmountsEftIndicator.CANCELLATION_S.getValue(),
            BilAmountsEftIndicator.FLAT_CANCEL_FOR_REWRITE.getValue(), '3', '5'));
    private static final List<Character> secondBilAmtEffIndList = new ArrayList<>(Arrays.asList(
            BilAmountsEftIndicator.REINSTATEMENT_K.getValue(), BilAmountsEftIndicator.REINSTATEMENT_L.getValue(),
            BilAmountsEftIndicator.REINSTATEMENT_O.getValue(), BilAmountsEftIndicator.REINSTATEMENT_T.getValue(),
            BilAmountsEftIndicator.REINSTATEMENT_Y.getValue(), BilAmountsEftIndicator.REINSTATEMENT_Z.getValue(), '4',
            '6'));
    private static final List<String> bilDesReaTypList = new ArrayList<>(Arrays.asList("BAM", "AMT", "BAS", "BPS"));
    private static final List<String> bilDesReaTypListSecond = new ArrayList<>(
            Arrays.asList("BAM", "AMT", "BAS", "BPS", "BAA", "BPA", "BEQ"));
    private static final List<String> bilDesReaTypListThird = new ArrayList<>(
            Arrays.asList("BAM", "AMT", "BAS", "BPS", "BAA", "BPA"));
    private static final List<String> cncTimestampDesReasonList = new ArrayList<>(
            Arrays.asList("BAM", "AMT", "BAS", "BPS", "BEQ"));
    private static final Double ROUND_UP_HALF_NUM = 0.499999;
    private static final String INVALID_INPUT_PAID_DATE = "invalie.input.paid.date";

    @Override
    public ZonedDateTime getPaidToDate(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime polExpirationDate, ZonedDateTime firstDate, boolean isEditPolBits, boolean policyLevel) {

        ZonedDateTime policyPaidDate = null;
        editInputParams(accountId, policyId, polEffectiveDate, polExpirationDate);

        Character[] bilTermObjects = readPolicyTerm(accountId, policyId, polEffectiveDate, isEditPolBits);
        char agreementIndicator = bilTermObjects[0];
        char scheduleOptionIndicator = bilTermObjects[1];
        char polStatusCode = bilTermObjects[2];

        Double policyTotalPaid = DECIMAL_ZERO;
        Double policyTotalDue = DECIMAL_ZERO;
        List<Object[]> bisPaidAmounts = new ArrayList<>();

        Integer bamCounts = fetchPolicyTotalAmounts(accountId, policyId, polEffectiveDate, polExpirationDate,
                agreementIndicator, scheduleOptionIndicator, bisPaidAmounts, policyLevel);
        if (bisPaidAmounts != null && !bisPaidAmounts.isEmpty()) {
            policyTotalPaid = bisPaidAmounts.get(0)[0] != null ? (Double) bisPaidAmounts.get(0)[0] : DECIMAL_ZERO;
            policyTotalDue = bisPaidAmounts.get(0)[1] != null ? (Double) bisPaidAmounts.get(0)[1] : DECIMAL_ZERO;
        }

        policyTotalDue = adjustTotalDueAmount(agreementIndicator, accountId, policyId, polEffectiveDate, policyTotalDue,
                scheduleOptionIndicator, policyLevel);

        Double bamPremium = DECIMAL_ZERO;
        ZonedDateTime cncEffectiveDate = null;
        Object[] cncObjects = null;
        if (policyTotalPaid > DECIMAL_ZERO) {
            Object[] editBamObjects = editBam(accountId, policyId, polEffectiveDate, polExpirationDate,
                    agreementIndicator, policyTotalDue, policyLevel);
            bamPremium = editBamObjects[0] != null ? (Double) editBamObjects[0] : DECIMAL_ZERO;
            cncObjects = (Object[]) editBamObjects[1];
            if (cncObjects != null) {
                policyTotalDue = (Double) cncObjects[0] == null ? DECIMAL_ZERO : (Double) cncObjects[0];
                cncEffectiveDate = (ZonedDateTime) cncObjects[2];
            }

        }

        Object[] termLengthObject = termLength(accountId, policyId, polEffectiveDate, polExpirationDate, policyLevel);

        policyPaidDate = fetchPaidToDateByPolicyAmounts(accountId, policyId, polEffectiveDate, polExpirationDate,
                firstDate, termLengthObject, policyTotalPaid, policyTotalDue, bamPremium, bamCounts, agreementIndicator,
                cncObjects, policyLevel);

        if (agreementIndicator != CHAR_Y && polStatusCode == CHAR_T) {
            policyPaidDate = polEffectiveDate;
        }

        if (policyTotalPaid > DECIMAL_ZERO && agreementIndicator != CHAR_Y
                && policyPaidDate.compareTo(cncEffectiveDate) > 0
                && (polStatusCode == BilPolicyStatusCode.CANCELLED.getValue()
                        || polStatusCode == BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue())) {
            policyPaidDate = cncEffectiveDate;
        }

        return policyPaidDate;
    }

    private ZonedDateTime fetchPaidToDateByPolicyAmounts(String accountId, String policyId,
            ZonedDateTime polEffectiveDate, ZonedDateTime polExpirationDate, ZonedDateTime firstDate,
            Object[] termLengthObject, Double policyTotalPaid, Double policyTotalDue, Double bamPremium,
            Integer bamCounts, char agreementIndicator, Object[] cncObjects, boolean policyLevel) {
        int noTerm = (int) termLengthObject[0];
        ZonedDateTime minEffDate = (ZonedDateTime) termLengthObject[1];
        ZonedDateTime maxExpirationDate = (ZonedDateTime) termLengthObject[2];
        ZonedDateTime policyPaidDate = null;

        if (policyTotalDue < DECIMAL_ZERO || policyTotalPaid >= policyTotalDue) {
            if (noTerm <= 1) {
                policyPaidDate = maxExpirationDate;
            } else {
                policyPaidDate = polExpirationDate;

                if (maxExpirationDate.compareTo(polExpirationDate) < 0) {
                    policyPaidDate = maxExpirationDate;
                }
            }
        } else {
            policyPaidDate = minEffDate;

            if (policyTotalPaid > bamPremium) {
                policyPaidDate = maxExpirationDate;
            } else if (bamCounts > 0 && (policyTotalPaid > 0
                    || firstDate != null && firstDate.compareTo(DateRoutine.defaultDateTime()) != 0)) {
                policyPaidDate = paidToDate(accountId, policyId, polEffectiveDate, polExpirationDate,
                        agreementIndicator, cncObjects, policyTotalPaid, minEffDate, policyLevel);
            }
        }

        return policyPaidDate;
    }

    private Double adjustTotalDueAmount(char agreementIndicator, String accountId, String policyId,
            ZonedDateTime polEffectiveDate, Double policyTotalDue, char scheduleOptionIndicator, boolean policyLevel) {
        if (agreementIndicator != CHAR_Y && scheduleOptionIndicator != TERM_ANNUAL) {
            Double policyAdjDue = null;
            if (policyLevel) {
                policyAdjDue = bilIstScheduleRepository.getSumCreditOpenAmountsForPolicy(policyId, polEffectiveDate,
                        DECIMAL_ZERO, DECIMAL_ZERO, getBilReaTypeList());
            } else {
                policyAdjDue = bilIstScheduleRepository.getSumCreditOpenAmounts(accountId, policyId, polEffectiveDate,
                        DECIMAL_ZERO, DECIMAL_ZERO, getBilReaTypeList());
            }
            if (policyAdjDue != null) {
                policyTotalDue = policyTotalDue - policyAdjDue;
            }
        }
        return policyTotalDue;
    }

    private Integer fetchPolicyTotalAmounts(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime polExpirationDate, char agreementIndicator, char scheduleOptionIndicator,
            List<Object[]> bisPaidAmounts, boolean policyLevel) {
        Integer bamCounts = 0;
        List<Object[]> bilIstScheduleList;

        if (policyLevel) {
            if (agreementIndicator != CHAR_Y) {
                bamCounts = bilAmountsRepository.countBamAmountsByReasonTypeForPolicy(policyId, polEffectiveDate,
                        polExpirationDate, CHAR_Y, getBilReaTypeList());
                if (scheduleOptionIndicator == TERM_ANNUAL) {
                    bilIstScheduleList = bilIstScheduleRepository.getSumPaidAmountsForAnnualByPayItemForPolicy(policyId,
                            polEffectiveDate, getBilReaTypeList());
                } else {
                    bilIstScheduleList = bilIstScheduleRepository.getSumPaidAmountsByPayItemForPolicy(policyId,
                            polEffectiveDate, getBilReaTypeList());
                }
            } else {
                bamCounts = bilAmountsRepository.countBamAmountsForPolicy(policyId, polEffectiveDate, polExpirationDate,
                        CHAR_Y);
                if (scheduleOptionIndicator == TERM_ANNUAL) {
                    bilIstScheduleList = bilIstScheduleRepository.getSumPaidAmountsForAnnualForPolicy(policyId,
                            polEffectiveDate);
                } else {
                    bilIstScheduleList = bilIstScheduleRepository.getSumPaidAmountsForPolicy(policyId,
                            polEffectiveDate);
                }
            }
        } else {
            if (agreementIndicator != CHAR_Y) {
                bamCounts = bilAmountsRepository.countBamAmountsByReasonType(accountId, policyId, polEffectiveDate,
                        polExpirationDate, CHAR_Y, getBilReaTypeList());
                if (scheduleOptionIndicator == TERM_ANNUAL) {
                    bilIstScheduleList = bilIstScheduleRepository.getSumPaidAmountsForAnnualByPayItem(accountId,
                            policyId, polEffectiveDate, getBilReaTypeList());
                } else {
                    bilIstScheduleList = bilIstScheduleRepository.getSumPaidAmountsByPayItem(accountId, policyId,
                            polEffectiveDate, getBilReaTypeList());
                }
            } else {
                bamCounts = bilAmountsRepository.countBamAmounts(accountId, policyId, polEffectiveDate,
                        polExpirationDate, CHAR_Y);
                if (scheduleOptionIndicator == TERM_ANNUAL) {
                    bilIstScheduleList = bilIstScheduleRepository.getSumPaidAmountsForAnnual(accountId, policyId,
                            polEffectiveDate);
                } else {
                    bilIstScheduleList = bilIstScheduleRepository.getSumPaidAmounts(accountId, policyId,
                            polEffectiveDate);
                }
            }
        }
        if (bilIstScheduleList != null) {
            bisPaidAmounts.addAll(bilIstScheduleList);
        }

        return bamCounts;
    }

    private ZonedDateTime paidToDate(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime polExpirationDate, char agreementIndicator, Object[] cncObjects, double polTotalPaid,
            ZonedDateTime minEffDate, boolean policyLevel) {
        ZonedDateTime policyPaidToDate = null;
        LinkedList<Object[]> timeCostsList = new LinkedList<>();
        LinkedList<Object[]> amountGroupList = new LinkedList<>();

        buildGroupAmtAndTimeCosts(accountId, policyId, polEffectiveDate, polExpirationDate, agreementIndicator,
                cncObjects, amountGroupList, timeCostsList, policyLevel);
        if (polTotalPaid == 0) {
            policyPaidToDate = minEffDate;
            return policyPaidToDate;
        }

        Object[] policyTotalPaidIndex = figureCoverage(timeCostsList, polTotalPaid);
        polTotalPaid = (double) policyTotalPaidIndex[0];
        int timeFrameIndex = (int) policyTotalPaidIndex[1];

        if (polTotalPaid > 0 && (Double) timeCostsList.get(timeFrameIndex)[2] != 0) {
            policyPaidToDate = partialCoverage(timeCostsList, polTotalPaid, polEffectiveDate, timeFrameIndex);
        } else {
            policyPaidToDate = (ZonedDateTime) timeCostsList.get(timeFrameIndex - 1)[1];
        }

        return policyPaidToDate;
    }

    private Object[] figureCoverage(LinkedList<Object[]> timeCostsList, double polTotalPaid) {
        int timeCostIndex = 0;
        int index = 0;
        Double timeTotalCost;
        while (!(timeCostIndex >= timeCostsList.size() || polTotalPaid <= 0)) {
            timeTotalCost = (Double) timeCostsList.get(timeCostIndex)[3];
            if (timeTotalCost > polTotalPaid) {
                index = timeCostIndex + 1;
                timeCostIndex = timeCostsList.size();
            } else {
                polTotalPaid = polTotalPaid - timeTotalCost;
            }
            timeCostIndex = timeCostIndex + 1;
        }

        if (index != 0) {
            timeCostIndex = index - 1;
        } else {
            timeCostIndex = timeCostIndex - 1;
        }
        return new Object[] { polTotalPaid, timeCostIndex };
    }

    private void buildGroupAmtAndTimeCosts(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime polExpirationDate, char agreementIndicator, Object[] cncObjects,
            LinkedList<Object[]> amountGroupList, LinkedList<Object[]> timeCostsList, boolean policyLevel) {
        boolean includeWeekend = checkForIncludeWeekend(BLANK_STRING, BLANK_STRING);

        List<Object[]> bilAmountsObjects = null;
        if (policyLevel) {
            if (agreementIndicator != CHAR_Y) {
                bilAmountsObjects = bilAmountsRepository.fetchGroupAmountsForPolicy(policyId, polEffectiveDate,
                        polExpirationDate, CHAR_Y, getBilReaTypeList());
            } else {
                bilAmountsObjects = bilAmountsRepository.fetchGroupAmountsForAgreementForPolicy(policyId,
                        polEffectiveDate, polExpirationDate, CHAR_Y);
            }
        } else {
            if (agreementIndicator != CHAR_Y) {
                bilAmountsObjects = bilAmountsRepository.fetchGroupAmounts(accountId, policyId, polEffectiveDate,
                        polExpirationDate, CHAR_Y, getBilReaTypeList());
            } else {
                bilAmountsObjects = bilAmountsRepository.fetchGroupAmountsForAgreement(accountId, policyId,
                        polEffectiveDate, polExpirationDate, CHAR_Y);
            }
        }
        ZonedDateTime maximumExpirationDate = null;
        if (bilAmountsObjects != null && !bilAmountsObjects.isEmpty()) {
            maximumExpirationDate = fetchGroupAmount(bilAmountsObjects, cncObjects, agreementIndicator, amountGroupList,
                    includeWeekend);
        }

        processTimeCosts(amountGroupList, timeCostsList, maximumExpirationDate, includeWeekend);

        for (Object[] amountGroups : amountGroupList) {
            calculateTimeCosts(amountGroups, timeCostsList);
        }
    }

    private ZonedDateTime partialCoverage(LinkedList<Object[]> timeCostsList, double polTotalPaid,
            ZonedDateTime polEffectiveDate, int index) {
        ZonedDateTime policyPaidToDate = null;
        Double timeDailyCost = (Double) timeCostsList.get(index)[2];
        Double partialCoverage = polTotalPaid / timeDailyCost;
        Double startRound = partialCoverage;
        int endRound = partialCoverage.intValue();
        int partialDaysNum = endRound;
        startRound = BigDecimal.valueOf(startRound - endRound).setScale(6, RoundingMode.UP).doubleValue();

        if (startRound > 0) {
            BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("PTDR", BLANK_STRING, BLANK_STRING,
                    BLANK_STRING);
            char ptdRoundingRule = BLANK_CHAR;
            if (bilRulesUct != null) {
                ptdRoundingRule = bilRulesUct.getBrtRuleCd();
            }

            if (ptdRoundingRule == PTDR_ALWAYS_ROUND_UP
                    || ptdRoundingRule == PTDR_ROUND_UP_HALF_DAY && startRound > ROUND_UP_HALF_NUM) {
                partialDaysNum = partialDaysNum + 1;
            }
        }

        if (partialCoverage < 1) {
            if (timeCostsList.size() == 1) {
                policyPaidToDate = polEffectiveDate;
            } else {
                policyPaidToDate = (ZonedDateTime) timeCostsList.get(index)[1];
            }

        } else if (partialDaysNum > 0) {
            partialDaysNum = partialDaysNum - 1;
            ZonedDateTime timeEffectiveDate = (ZonedDateTime) timeCostsList.get(index)[0];
            policyPaidToDate = DateRoutine.adjustDateWithOffset(timeEffectiveDate, false, partialDaysNum, PLUS_SIGN,
                    null);
        } else if (timeCostsList.size() == 1) {
            policyPaidToDate = polEffectiveDate;
        } else {
            policyPaidToDate = (ZonedDateTime) timeCostsList.get(index)[1];
        }
        return policyPaidToDate;
    }

    private void calculateTimeCosts(Object[] amountGroup, LinkedList<Object[]> timeFrameTable) {
        ZonedDateTime amtGrpEffectiveDate = (ZonedDateTime) amountGroup[0];
        ZonedDateTime amtGrpExpirationDate = (ZonedDateTime) amountGroup[1];
        int maxCostsIndex = timeFrameTable.size();

        for (int costIndex = 0; costIndex < maxCostsIndex; costIndex++) {
            Object[] timeTable = timeFrameTable.get(costIndex);
            ZonedDateTime timeEffectiveDate = (ZonedDateTime) timeTable[0];
            ZonedDateTime timeExpitionDate = (ZonedDateTime) timeTable[1];

            if (timeEffectiveDate.compareTo(amtGrpEffectiveDate) >= 0
                    && timeExpitionDate.compareTo(amtGrpExpirationDate) <= 0) {
                Double timeDailyCost = (Double) timeTable[2];
                Double groupDailyCost = (Double) amountGroup[2];

                timeDailyCost = timeDailyCost + groupDailyCost;

                int timeFrameDays = (int) getBetweenDate(timeEffectiveDate, timeExpitionDate)[1];
                timeFrameDays = timeFrameDays + 1;
                Double timeTotalCost = timeDailyCost * timeFrameDays;

                timeFrameTable.remove(costIndex);
                timeFrameTable.add(costIndex,
                        new Object[] { timeEffectiveDate, timeExpitionDate, timeDailyCost, timeTotalCost });
            }
        }
    }

    private void processTimeCosts(LinkedList<Object[]> amountGroupList, LinkedList<Object[]> timeCostsList,
            ZonedDateTime maximumExpirationDate, boolean includeWeekend) {
        ZonedDateTime timeEffectiveDate = null;
        ZonedDateTime timeExpirationDate = null;
        ZonedDateTime lastMaxExpDate = null;

        for (Object[] amountGroup : amountGroupList) {
            if (timeCostsList.isEmpty()) {
                timeEffectiveDate = (ZonedDateTime) amountGroup[0];
                timeExpirationDate = (ZonedDateTime) amountGroup[1];
                timeCostsList.add(new Object[] { timeEffectiveDate, timeExpirationDate, DECIMAL_ZERO, DECIMAL_ZERO });

                lastMaxExpDate = timeExpirationDate;
                continue;
            }

            lastMaxExpDate = buildTimeCostsData(amountGroup, timeCostsList, maximumExpirationDate, lastMaxExpDate,
                    includeWeekend);
        }

        timeExpirationDate = (ZonedDateTime) timeCostsList.getLast()[1];
        if (timeExpirationDate.compareTo(maximumExpirationDate) < 0) {
            timeEffectiveDate = DateRoutine.adjustDateWithOffset(maximumExpirationDate, !includeWeekend, 1, PLUS_SIGN,
                    null);
            timeCostsList.add(new Object[] { timeEffectiveDate, maximumExpirationDate, DECIMAL_ZERO, DECIMAL_ZERO });
        }
    }

    private ZonedDateTime buildTimeCostsData(Object[] amountGroup, LinkedList<Object[]> timeCostsList,
            ZonedDateTime maximumExpirationDate, ZonedDateTime lastMaxExpDate, boolean includeWeekend) {
        boolean timeEffSwitch = false;
        boolean timeExpSwitch = false;
        ZonedDateTime groupEffectiveDate = (ZonedDateTime) amountGroup[0];
        ZonedDateTime groupExpirationDate = (ZonedDateTime) amountGroup[1];

        Object[] compareEffObjects = null;
        Object[] timeObject = null;
        for (int costIndex = 0; costIndex < timeCostsList.size(); costIndex++) {
            timeObject = timeCostsList.get(costIndex);
            compareEffObjects = compareEffectiveDate(groupEffectiveDate, groupExpirationDate, timeObject, timeCostsList,
                    maximumExpirationDate, lastMaxExpDate, includeWeekend, costIndex);
            lastMaxExpDate = (ZonedDateTime) compareEffObjects[2];

            timeEffSwitch = (boolean) compareEffObjects[0];
            timeExpSwitch = (boolean) compareEffObjects[1];
            if (timeEffSwitch) {
                break;
            }
        }
        for (int costIndex = 0; costIndex < timeCostsList.size(); costIndex++) {
            timeObject = timeCostsList.get(costIndex);
            if (!timeExpSwitch) {
                timeExpSwitch = compareExpirationDate(groupExpirationDate, timeObject, timeCostsList, includeWeekend,
                        costIndex);
                if (timeExpSwitch) {
                    break;
                }
            }
        }

        return lastMaxExpDate;
    }

    private boolean compareExpirationDate(ZonedDateTime groupExpirationDate, Object[] timeObject,
            LinkedList<Object[]> timeCostsList, boolean includeWeekend, int costIndex) {
        boolean timeExpSwitch = false;
        ZonedDateTime timeExpirationDate = (ZonedDateTime) timeObject[1];

        if (timeExpirationDate == null || groupExpirationDate == null) {
            timeExpSwitch = true;
            return timeExpSwitch;
        }
        if (groupExpirationDate.compareTo(timeExpirationDate) < 0) {
            timeExpSwitch = true;
            ZonedDateTime tempTimeEffDate = (ZonedDateTime) timeObject[0];

            timeCostsList.set(costIndex,
                    new Object[] { tempTimeEffDate, groupExpirationDate, DECIMAL_ZERO, DECIMAL_ZERO });
            ZonedDateTime differenceDate = DateRoutine.adjustDateWithOffset(groupExpirationDate, !includeWeekend, 1,
                    PLUS_SIGN, null);
            timeCostsList.add(costIndex + 1,
                    new Object[] { differenceDate, timeExpirationDate, DECIMAL_ZERO, DECIMAL_ZERO });

        } else {
            if (groupExpirationDate.compareTo(timeExpirationDate) == 0) {
                timeExpSwitch = true;
            }
        }

        return timeExpSwitch;
    }

    private Object[] compareEffectiveDate(ZonedDateTime groupEffectiveDate, ZonedDateTime groupExpirationDate,
            Object[] timeObject, LinkedList<Object[]> timeCostsList, ZonedDateTime maximumExpirationDate,
            ZonedDateTime lastMaxExpDate, boolean includeWeekend, int costIndex) {
        boolean timeEffSwitch = false;
        boolean timeExpSwitch = false;
        ZonedDateTime timeEffectiveDate = (ZonedDateTime) timeObject[0];
        ZonedDateTime timeExpirationDate = (ZonedDateTime) timeObject[1];

        if (groupEffectiveDate.compareTo(timeEffectiveDate) == 0) {
            if (groupExpirationDate.compareTo(timeExpirationDate) == 0) {
                timeEffSwitch = true;
                timeExpSwitch = true;
            } else if (groupExpirationDate.compareTo(timeExpirationDate) < 0) {
                timeEffSwitch = true;
                timeCostsList.set(costIndex,
                        new Object[] { timeEffectiveDate, groupExpirationDate, DECIMAL_ZERO, DECIMAL_ZERO });

                ZonedDateTime differenceDate = DateRoutine.adjustDateWithOffset(groupExpirationDate, !includeWeekend, 1,
                        PLUS_SIGN, null);
                timeCostsList.add(costIndex + 1,
                        new Object[] { differenceDate, timeExpirationDate, DECIMAL_ZERO, DECIMAL_ZERO });

            } else if (groupExpirationDate.compareTo(timeExpirationDate) > 0) {
                Object[] addToMaxObjects = addToMaxExpiration(groupExpirationDate, timeExpirationDate, timeCostsList,
                        maximumExpirationDate, lastMaxExpDate, includeWeekend, costIndex);
                timeEffSwitch = (boolean) addToMaxObjects[0];
                timeExpSwitch = (boolean) addToMaxObjects[1];
                lastMaxExpDate = (ZonedDateTime) addToMaxObjects[2];
            }

        } else if (groupEffectiveDate.compareTo(timeEffectiveDate) > 0) {
            if (groupEffectiveDate.compareTo(timeExpirationDate) < 0
                    || groupEffectiveDate.compareTo(timeExpirationDate) == 0) {
                timeEffSwitch = true;
                ZonedDateTime nextDate = groupEffectiveDate;
                if (groupEffectiveDate.compareTo(timeExpirationDate) == 0) {
                    nextDate = groupExpirationDate;
                }
                ZonedDateTime differenceDate = DateRoutine.adjustDateWithOffset(nextDate, !includeWeekend, 1,
                        MINUS_SIGN, null);
                timeCostsList.set(costIndex,
                        new Object[] { timeEffectiveDate, differenceDate, DECIMAL_ZERO, DECIMAL_ZERO });

                timeCostsList.add(costIndex + 1,
                        new Object[] { groupEffectiveDate, timeExpirationDate, DECIMAL_ZERO, DECIMAL_ZERO });

            } else if (groupEffectiveDate.compareTo(timeExpirationDate) > 0 && costIndex == timeCostsList.size()) {
                timeCostsList.add(new Object[] { groupEffectiveDate, groupExpirationDate, DECIMAL_ZERO, DECIMAL_ZERO });
                timeEffSwitch = true;
                timeExpSwitch = true;
            }
        }
        return new Object[] { timeEffSwitch, timeExpSwitch, lastMaxExpDate };
    }

    private Object[] addToMaxExpiration(ZonedDateTime groupExpirationDate, ZonedDateTime timeExpDate,
            LinkedList<Object[]> timeFrameTable, ZonedDateTime maximumExpirationDate, ZonedDateTime lastMaxExpDate,
            boolean includeWeekend, int costIndex) {
        boolean timeEffSwitch = false;
        boolean timeExpSwitch = false;

        if (timeExpDate.compareTo(maximumExpirationDate) == 0) {
            timeEffSwitch = true;
            timeExpSwitch = true;
        } else {
            ZonedDateTime differenceDate = DateRoutine.adjustDateWithOffset(timeExpDate, !includeWeekend, 1, PLUS_SIGN,
                    null);
            if (groupExpirationDate.compareTo(lastMaxExpDate) > 0 && lastMaxExpDate.compareTo(timeExpDate) != 0) {
                timeExpDate = lastMaxExpDate;
            } else {
                timeExpDate = groupExpirationDate;
            }
            timeFrameTable.add(costIndex, new Object[] { differenceDate, timeExpDate, DECIMAL_ZERO, DECIMAL_ZERO });

            timeEffSwitch = true;
            timeExpSwitch = true;
            lastMaxExpDate = groupExpirationDate;
        }
        return new Object[] { timeEffSwitch, timeExpSwitch, lastMaxExpDate };
    }

    private ZonedDateTime fetchGroupAmount(List<Object[]> bilAmountsObjects, Object[] cncObjects,
            char agreementIndicator, LinkedList<Object[]> amountGroupList, boolean includeWeekend) {
        Double cncBamSum = DECIMAL_ZERO;
        ZonedDateTime cncEffectiveDate = null;
        ZonedDateTime cncExpirationDate = null;
        ZonedDateTime maximumExpDate = null;
        if (cncObjects != null) {
            cncBamSum = cncObjects[1] != null ? (Double) cncObjects[1] : DECIMAL_ZERO;
            cncEffectiveDate = (ZonedDateTime) cncObjects[2];
            cncExpirationDate = (ZonedDateTime) cncObjects[3];
        }

        for (Object[] bilAmounts : bilAmountsObjects) {
            ZonedDateTime bamEffectiveDate = (ZonedDateTime) bilAmounts[0];
            ZonedDateTime bamExpirationDate = (ZonedDateTime) bilAmounts[1];
            Double bamAmounts = bilAmounts[2] != null ? (Double) bilAmounts[2] : DECIMAL_ZERO;

            if (bamEffectiveDate == null || bamExpirationDate == null
                    || bamExpirationDate.compareTo(bamEffectiveDate) < 0) {
                throw new InvalidDataException("invalid.amount.paid.date",
                        new Object[] { "Group Amount", "BIL_AMOUNTS" });
            }

            if ((agreementIndicator != CHAR_Y) && (cncEffectiveDate != null && bamEffectiveDate != null
                    && cncEffectiveDate.compareTo(bamEffectiveDate) == 0 && cncExpirationDate != null
                    && bamExpirationDate != null && cncExpirationDate.compareTo(bamExpirationDate) == 0
                    && cncBamSum < 0) && (bamAmounts - cncBamSum == 0)) {
                continue;
            }

            maximumExpDate = updateGroupAmounts(amountGroupList, bamEffectiveDate, bamExpirationDate, bamAmounts,
                    includeWeekend);
        }

        return maximumExpDate;
    }

    private ZonedDateTime updateGroupAmounts(LinkedList<Object[]> amountGroupList, ZonedDateTime bamEffectiveDate,
            ZonedDateTime bamExpirationDate, Double bamAmounts, boolean includeWeekend) {
        ZonedDateTime groupEffectiveDate = bamEffectiveDate;
        ZonedDateTime groupExpirationDate = bamExpirationDate;

        if (bamEffectiveDate.compareTo(bamExpirationDate) != 0) {
            groupExpirationDate = DateRoutine.adjustDateWithOffset(bamExpirationDate, !includeWeekend, 1, MINUS_SIGN,
                    null);
        } else if (bamEffectiveDate.compareTo(bamExpirationDate) == 0) {
            groupExpirationDate = DateRoutine.adjustDateWithOffset(bamEffectiveDate, !includeWeekend, 1, MINUS_SIGN,
                    null);
            groupEffectiveDate = groupExpirationDate;
        }

        ZonedDateTime maximumExpDate = groupExpirationDate;
        int betweenDays = (int) getBetweenDate(groupEffectiveDate, groupExpirationDate)[1];
        int groupDays = betweenDays + 1;
        Double dailyCost = bamAmounts / groupDays;
        amountGroupList.add(new Object[] { groupEffectiveDate, groupExpirationDate, dailyCost });

        return maximumExpDate;
    }

    private Object[] termLength(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime polExpirationDate, boolean policyLevel) {
        int polDuration = (int) getBetweenDate(polExpirationDate, polEffectiveDate)[1];
        int noTerm = polDuration / 365;
        ZonedDateTime maxExpirationDate = null;
        ZonedDateTime minEffDate = null;

        if (policyLevel) {
            maxExpirationDate = bilAmountsRepository.getMaxBamAmtExpDateForPolicy(policyId, polEffectiveDate,
                    polExpirationDate, CHAR_Y, getBilReaTypeList());

            minEffDate = bilAmountsRepository.getMinBamAmtEffDateForPolicy(policyId, polEffectiveDate,
                    polExpirationDate, CHAR_Y, getBilReaTypeList());
        } else {
            maxExpirationDate = bilAmountsRepository.getMaxBamAmtExpDate(accountId, policyId, polEffectiveDate,
                    polExpirationDate, CHAR_Y, getBilReaTypeList());

            minEffDate = bilAmountsRepository.getMinBamAmtEffDate(accountId, policyId, polEffectiveDate,
                    polExpirationDate, CHAR_Y, getBilReaTypeList());
        }

        if (maxExpirationDate == null) {
            maxExpirationDate = polExpirationDate;
        }
        if (minEffDate == null) {
            minEffDate = polEffectiveDate;
        }

        return new Object[] { noTerm, minEffDate, maxExpirationDate };
    }

    private Object[] getBetweenDate(ZonedDateTime startDate, ZonedDateTime endDate) {
        char dateAdjustSign;
        int pendingDays;
        if (endDate.isAfter(startDate)) {
            dateAdjustSign = PLUS_SIGN;
            pendingDays = (int) ChronoUnit.DAYS.between(startDate, endDate);
        } else {
            dateAdjustSign = MINUS_SIGN;
            pendingDays = (int) ChronoUnit.DAYS.between(endDate, startDate);
        }
        return new Object[] { dateAdjustSign, pendingDays };
    }

    private Object[] editBam(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime polExpirationDate, char agreementIndicator, Double policyTotalDue, boolean policyLevel) {
        Double bamPremium;
        Object[] cncObjects = null;

        if (policyLevel) {
            if (agreementIndicator != CHAR_Y) {
                bamPremium = bilAmountsRepository.getSumBamAmountByReasonTypeForPolicy(policyId, polEffectiveDate,
                        polExpirationDate, CHAR_Y, getBilReaTypeList());
            } else {
                bamPremium = bilAmountsRepository.getSumBamAmountsForPolicy(policyId, polEffectiveDate,
                        polExpirationDate, CHAR_Y);
            }
        } else {
            if (agreementIndicator != CHAR_Y) {
                bamPremium = bilAmountsRepository.getSumBamAmountByReasonType(accountId, policyId, polEffectiveDate,
                        polExpirationDate, CHAR_Y, getBilReaTypeList());
            } else {
                bamPremium = bilAmountsRepository.getSumBamAmounts(accountId, policyId, polEffectiveDate,
                        polExpirationDate, CHAR_Y);
            }
        }

        if (agreementIndicator != CHAR_Y) {
            cncObjects = adjustForCncTrigger(accountId, policyId, polEffectiveDate, polExpirationDate, policyTotalDue,
                    policyLevel);
        }

        return new Object[] { bamPremium, cncObjects };
    }

    private Object[] adjustForCncTrigger(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime polExpirationDate, Double policyTotalDue, boolean policyLevel) {
        Double firstCncBamSum = DECIMAL_ZERO;
        Double secondCncBamSum = DECIMAL_ZERO;
        ZonedDateTime cncEffectiveDate = DateRoutine.defaultDateTime();
        ZonedDateTime cncExpirationDate = DateRoutine.defaultDateTime();

            ZonedDateTime maxCncTimestamp = null;
            ZonedDateTime maxReinstTimestamp = null;
            List<Object[]> maxCncBilAmounts = null;
            List<Object[]> maxCncBilAmountsSecond = null;

            if (policyLevel) {
                maxCncTimestamp = bilAmountsRepository.getMaxBamAmtTsForPolicy(policyId, polEffectiveDate,
                        polExpirationDate, CHAR_Y, BilAmountsEftIndicator.CANCELLATION_F.getValue(),
                        cncTimestampDesReasonList, bilAmtEffIndList, bilDesReaTypListSecond);
                if (maxCncTimestamp == null) {
                    return new Object[] { policyTotalDue, firstCncBamSum, cncEffectiveDate, cncExpirationDate };
                }

                maxReinstTimestamp = bilAmountsRepository.getMaxReinstAmountTimestampForPolicy(policyId,
                        polEffectiveDate, polExpirationDate, CHAR_Y, secondBilAmtEffIndList, bilDesReaTypListSecond);
                if (maxReinstTimestamp != null && maxReinstTimestamp.compareTo(maxCncTimestamp) > 0) {
                    return new Object[] { policyTotalDue, firstCncBamSum, cncEffectiveDate, cncExpirationDate };
                }
                maxCncBilAmounts = bilAmountsRepository.getMaxBilAmountsForPolicy(policyId, polEffectiveDate,
                        polExpirationDate, maxCncTimestamp, DECIMAL_ZERO, CHAR_Y,
                        BilAmountsEftIndicator.CANCELLATION_F.getValue(), bilDesReaTypList, bilAmtEffIndList,
                        bilDesReaTypListThird);
                if (maxCncBilAmounts != null && !maxCncBilAmounts.isEmpty() && maxCncBilAmounts.get(0)[0] != null) {
                    firstCncBamSum = maxCncBilAmounts.get(0)[0] != null ? (Double) maxCncBilAmounts.get(0)[0]
                            : DECIMAL_ZERO;
                    cncEffectiveDate = maxCncBilAmounts.get(0)[0] != null ? (ZonedDateTime) maxCncBilAmounts.get(0)[1]
                            : DateRoutine.defaultDateTime();
                    cncExpirationDate = maxCncBilAmounts.get(0)[0] != null ? (ZonedDateTime) maxCncBilAmounts.get(0)[2]
                            : DateRoutine.defaultDateTime();
                } else {
                    return new Object[] { policyTotalDue, firstCncBamSum, cncEffectiveDate, cncExpirationDate };
                }

                maxCncBilAmountsSecond = bilAmountsRepository.getMaxBilAmountsByLessBamAmountForPolicy(policyId,
                        polEffectiveDate, polExpirationDate, maxCncTimestamp, DECIMAL_ZERO, CHAR_Y,
                        BilAmountsEftIndicator.CANCELLATION_F.getValue(), bilDesReaTypList, bilAmtEffIndList,
                        bilDesReaTypListThird);
                if (maxCncBilAmountsSecond != null && !maxCncBilAmountsSecond.isEmpty()
                        && maxCncBilAmountsSecond.get(0)[0] != null) {
                    secondCncBamSum = maxCncBilAmountsSecond.get(0)[0] != null
                            ? (Double) maxCncBilAmountsSecond.get(0)[0]
                            : DECIMAL_ZERO;
                    cncEffectiveDate = maxCncBilAmountsSecond.get(0)[0] != null
                            ? (ZonedDateTime) maxCncBilAmountsSecond.get(0)[1]
                            : DateRoutine.defaultDateTime();
                    cncExpirationDate = maxCncBilAmountsSecond.get(0)[0] != null
                            ? (ZonedDateTime) maxCncBilAmountsSecond.get(0)[2]
                            : DateRoutine.defaultDateTime();
                } else {
                    return new Object[] { policyTotalDue, firstCncBamSum, cncEffectiveDate, cncExpirationDate };
                }
            } else {

                maxCncTimestamp = bilAmountsRepository.getMaxBamAmtTs(accountId, policyId, polEffectiveDate,
                        polExpirationDate, CHAR_Y, BilAmountsEftIndicator.CANCELLATION_F.getValue(),
                        cncTimestampDesReasonList, bilAmtEffIndList, bilDesReaTypListSecond);
                if (maxCncTimestamp == null) {
                    return new Object[] { policyTotalDue, firstCncBamSum, cncEffectiveDate, cncExpirationDate };
                }

                maxReinstTimestamp = bilAmountsRepository.getMaxReinstAmountTimestamp(accountId, policyId,
                        polEffectiveDate, polExpirationDate, CHAR_Y, secondBilAmtEffIndList, bilDesReaTypListSecond);
                if (maxReinstTimestamp != null && maxReinstTimestamp.compareTo(maxCncTimestamp) > 0) {
                    return new Object[] { policyTotalDue, firstCncBamSum, cncEffectiveDate, cncExpirationDate };
                }
                maxCncBilAmounts = bilAmountsRepository.getMaxBilAmounts(accountId, policyId, polEffectiveDate,
                        polExpirationDate, maxCncTimestamp, DECIMAL_ZERO, CHAR_Y,
                        BilAmountsEftIndicator.CANCELLATION_F.getValue(), bilDesReaTypList, bilAmtEffIndList,
                        bilDesReaTypListThird);
                if (maxCncBilAmounts != null && !maxCncBilAmounts.isEmpty() && maxCncBilAmounts.get(0)[0] != null) {
                    firstCncBamSum = maxCncBilAmounts.get(0)[0] != null ? (Double) maxCncBilAmounts.get(0)[0]
                            : DECIMAL_ZERO;
                    cncEffectiveDate = maxCncBilAmounts.get(0)[0] != null ? (ZonedDateTime) maxCncBilAmounts.get(0)[1]
                            : DateRoutine.defaultDateTime();
                    cncExpirationDate = maxCncBilAmounts.get(0)[0] != null ? (ZonedDateTime) maxCncBilAmounts.get(0)[2]
                            : DateRoutine.defaultDateTime();
                } else {
                    return new Object[] { policyTotalDue, firstCncBamSum, cncEffectiveDate, cncExpirationDate };
                }

                maxCncBilAmountsSecond = bilAmountsRepository.getMaxBilAmountsByLessBamAmount(accountId, policyId,
                        polEffectiveDate, polExpirationDate, maxCncTimestamp, DECIMAL_ZERO, CHAR_Y,
                        BilAmountsEftIndicator.CANCELLATION_F.getValue(), bilDesReaTypList, bilAmtEffIndList,
                        bilDesReaTypListThird);
                if (maxCncBilAmountsSecond != null && !maxCncBilAmountsSecond.isEmpty()
                        && maxCncBilAmountsSecond.get(0)[0] != null) {
                    secondCncBamSum = maxCncBilAmountsSecond.get(0)[0] != null
                            ? (Double) maxCncBilAmountsSecond.get(0)[0]
                            : DECIMAL_ZERO;
                    cncEffectiveDate = maxCncBilAmountsSecond.get(0)[0] != null
                            ? (ZonedDateTime) maxCncBilAmountsSecond.get(0)[1]
                            : DateRoutine.defaultDateTime();
                    cncExpirationDate = maxCncBilAmountsSecond.get(0)[0] != null
                            ? (ZonedDateTime) maxCncBilAmountsSecond.get(0)[2]
                            : DateRoutine.defaultDateTime();
                } else {
                    return new Object[] { policyTotalDue, firstCncBamSum, cncEffectiveDate, cncExpirationDate };
                }
            }

            policyTotalDue = policyTotalDue + firstCncBamSum;
            firstCncBamSum = firstCncBamSum + secondCncBamSum;
        return new Object[] { policyTotalDue, firstCncBamSum, cncEffectiveDate, cncExpirationDate };
    }

    private Character[] readPolicyTerm(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            boolean isEditPolBits) {
        char agreementIndicator;
        char polStatusCode;
        char scheduleOptionIndicator;

        BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository
                .findById(new BilPolicyTermId(accountId, policyId, polEffectiveDate)).orElse(null);
        if (bilPolicyTerm == null) {
            throw new DataNotFoundException("invalid.term.paid.date",
                    new Object[] { "Policy Term", "Agreement Indicator", accountId, policyId,
                            DateRoutine.dateTimeAsMMDDYYYYAsString(polEffectiveDate) });
        }
        agreementIndicator = bilPolicyTerm.getBptAgreementInd();
        polStatusCode = bilPolicyTerm.getBillPolStatusCd();

        BilBillPlan bilBillPlan = bilBillPlanRepository.findById(bilPolicyTerm.getBillPlanCd()).orElse(null);
        if (bilBillPlan == null) {
            throw new DataNotFoundException("invalid.term.paid.date", new Object[] { "BilBillPlan", "BBP_SPREAD_IND",
                    accountId, policyId, DateRoutine.dateTimeAsMMDDYYYYAsString(polEffectiveDate) });
        }

        scheduleOptionIndicator = bilBillPlan.getBbpSpreadInd();
        if (isEditPolBits) {
            agreementIndicator = CHAR_Y;
        }

        return new Character[] { agreementIndicator, scheduleOptionIndicator, polStatusCode };
    }

    private void editInputParams(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime polExpirationDate) {
        if (accountId == null || accountId.trim().isEmpty()) {
            throw new InvalidDataException(INVALID_INPUT_PAID_DATE, new Object[] { "0020-MAIN: Account", "AccountId" });
        }
        if (policyId == null || policyId.trim().isEmpty()) {
            throw new InvalidDataException(INVALID_INPUT_PAID_DATE, new Object[] { "0020-MAIN: Policy", "PolicyId" });
        }
        if (polEffectiveDate == null) {
            throw new InvalidDataException(INVALID_INPUT_PAID_DATE,
                    new Object[] { "0020-MAIN: Policy Effective Date", "PolEffDate" });
        }
        if (polExpirationDate == null) {
            throw new InvalidDataException(INVALID_INPUT_PAID_DATE,
                    new Object[] { "0020-MAIN: Policy Expiration Date", "PolExpDate" });
        }
    }

    private boolean checkForIncludeWeekend(String bilTypeCode, String bilClassCode) {
        boolean includeWeekend = false;
        if (bilActRulesRepository.existsById(new BilActRulesId(bilTypeCode, bilClassCode))) {
            includeWeekend = true;
        }

        return includeWeekend;
    }

    private List<String> getBilReaTypeList() {
        List<String> bilDesReaTypeList = new ArrayList<>();
        bilDesReaTypeList.add(BusCodeTranslationType.BILLING_ITEM.getValue());
        bilDesReaTypeList.add("BEQ");
        return bilDesReaTypeList;
    }

}
