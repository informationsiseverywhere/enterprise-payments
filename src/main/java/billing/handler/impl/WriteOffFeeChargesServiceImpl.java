package billing.handler.impl;

import static billing.utils.BillingConstants.ERROR_CODE;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilCrgAmounts;
import billing.data.entity.BilMstCoSt;
import billing.data.entity.BilRulesUct;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilCashDspId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilMstCoStRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.handler.model.WriteOffFeeCharges;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilReceiptTypeCode;
import billing.utils.BillingConstants.ChargeTypeCodes;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.ManualSuspendIndicator;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.id.BusCdTranslationId;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class WriteOffFeeChargesServiceImpl {

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilMstCoStRepository bilMstCoStRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Value("${language}")
    private String language;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private DateService dateService;

    @Transactional
    public void processWriteOffFeeCharges(WriteOffFeeCharges writeOffFeeCharges, BilAccount bilAccount) {
        List<BilCrgAmounts> bilCrgAmountsList = bilCrgAmountsRepository.readWriteOffRowsByDueDate(
                bilAccount.getAccountId(), writeOffFeeCharges.getDueDate(), writeOffFeeCharges.getChargeTypeCode());
        if (bilCrgAmountsList != null && !bilCrgAmountsList.isEmpty()) {
            BilCrgAmounts bilCrgAmounts = bilCrgAmountsList.get(SHORT_ZERO);
            if (writeOffFeeCharges.getAmount() > bilCrgAmounts.getBcaCrgAmt()) {
                throw new InvalidDataException("writeoff.amount.greter.original.amount");
            }
            if (bilCrgAmounts.getBcaCrgAmt() == bilCrgAmounts.getBcaWroCrgAmt()
                    || bilCrgAmounts.getBcaWroCrgAmt() > writeOffFeeCharges.getAmount()
                            && writeOffFeeCharges.getAmount() > DECIMAL_ZERO) {
                return;
            }

            if (writeOffFeeCharges.getAmount() == DECIMAL_ZERO) {
                writeOffFeeAmount(bilCrgAmounts, bilAccount, writeOffFeeCharges);
            } else {
                writeOffGivenFeeAmount(bilCrgAmounts, bilAccount, writeOffFeeCharges);
            }
        }
    }

    private void writeOffFeeAmount(BilCrgAmounts bilCrgAmounts, BilAccount bilAccount,
            WriteOffFeeCharges writeOffFeeCharges) {

        boolean paiCharge = false;
        double differenceAmount;
        double writeOffChargeAmount = bilCrgAmounts.getBcaWroCrgAmt();
        double chargeAmount = bilCrgAmounts.getBcaCrgAmt();
        double paidAmount = bilCrgAmounts.getBcaCrgPaidAmt();
        String objectCode = BLANK_STRING;
        String actionCode = BLANK_STRING;

        if (bilCrgAmounts.getBcaCrgPaidAmt() > DECIMAL_ZERO) {
            paiCharge = true;
        }
        bilCrgAmounts.setBcaCrgPaidAmt(DECIMAL_ZERO);
        bilCrgAmounts.setBcaWroCrgAmt(bilCrgAmounts.getBcaCrgAmt());
        bilCrgAmountsRepository.saveAndFlush(bilCrgAmounts);

        if (writeOffChargeAmount == DECIMAL_ZERO) {
            differenceAmount = chargeAmount;
        } else {
            differenceAmount = chargeAmount - writeOffChargeAmount;
        }

        insertBilAccountSummary(bilAccount.getAccountId(), writeOffFeeCharges.getDueDate(), differenceAmount,
                writeOffFeeCharges.getReasonCode(), writeOffFeeCharges.getApplicationDate(),
                writeOffFeeCharges.getReasonTypeCode(), BillingConstants.SYSTEM_USER_ID, BLANK_STRING, BLANK_STRING);

        if (writeOffFeeCharges.getChargeTypeCode() == ChargeTypeCodes.LATE_CHARGE.getValue()) {
            objectCode = ObjectCode.LATE_CHARGE.toString();
            if (bilCrgAmounts.getBilInvoiceCd() == InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue() || bilCrgAmounts
                    .getBilInvoiceCd() == InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()) {
                actionCode = ActionCode.WRITE_OFF_INSTALL.toString();
            } else {
                actionCode = ActionCode.WRITE_OFF_UNBILLED.toString();
            }
        }
        String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(), bilAccount.getBillTypeCd());

        callFinancialApi(differenceAmount, writeOffFeeCharges.getApplicationDate(), BLANK_STRING,
                bilAccount.getAccountNumber(), objectCode, actionCode, BLANK_STRING, BLANK_STRING, BLANK_STRING,
                writeOffFeeCharges.getReasonCode(), bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                writeOffFeeCharges.getApplicationDate(), BLANK_STRING, BillingConstants.SYSTEM_USER_ID,
                DateRoutine.dateTimeAsYYYYMMDDString(writeOffFeeCharges.getApplicationDate()),
                ApplicationProgramIdentifier.BCMOWTF.toString(), BLANK_STRING, BLANK_STRING, BLANK_STRING,
                bilmasterCompanyState[0], bilmasterCompanyState[1], bilAccount.getCurrencyCode());

        if (paiCharge) {
            processResuspendPaidAmount(paidAmount, bilAccount, writeOffFeeCharges, bilCrgAmounts, DECIMAL_ZERO);
        }

    }

    private void writeOffGivenFeeAmount(BilCrgAmounts bilCrgAmounts, BilAccount bilAccount,
            WriteOffFeeCharges writeOffFeeCharges) {
        boolean paiCharge = false;
        double differenceAmount;
        double writeOffChargeAmount = bilCrgAmounts.getBcaWroCrgAmt();
        double chargeAmount = bilCrgAmounts.getBcaCrgAmt();
        double paidAmount = bilCrgAmounts.getBcaCrgPaidAmt();
        String objectCode = BLANK_STRING;
        String actionCode = BLANK_STRING;
        double chargeDifference = DECIMAL_ZERO;

        if (bilCrgAmounts.getBcaCrgPaidAmt() > DECIMAL_ZERO) {
            paiCharge = true;
        }

        if (paiCharge) {
            chargeDifference = chargeAmount - writeOffFeeCharges.getAmount();
            if (chargeDifference >= paidAmount) {
                paiCharge = false;
                bilCrgAmounts.setBcaWroCrgAmt(writeOffFeeCharges.getAmount());
            } else {
                bilCrgAmounts.setBcaCrgPaidAmt(chargeDifference);
                bilCrgAmounts.setBcaWroCrgAmt(writeOffFeeCharges.getAmount());
            }
        } else {
            bilCrgAmounts.setBcaWroCrgAmt(writeOffFeeCharges.getAmount());
        }

        bilCrgAmountsRepository.saveAndFlush(bilCrgAmounts);

        if (writeOffChargeAmount == DECIMAL_ZERO) {
            differenceAmount = writeOffFeeCharges.getAmount();
        } else {
            differenceAmount = writeOffFeeCharges.getAmount() - writeOffChargeAmount;
        }

        insertBilAccountSummary(bilAccount.getAccountId(), writeOffFeeCharges.getDueDate(), differenceAmount,
                writeOffFeeCharges.getReasonCode(), writeOffFeeCharges.getApplicationDate(),
                writeOffFeeCharges.getReasonTypeCode(), BillingConstants.SYSTEM_USER_ID, BLANK_STRING, BLANK_STRING);

        if (writeOffFeeCharges.getChargeTypeCode() == ChargeTypeCodes.LATE_CHARGE.getValue()) {
            objectCode = ObjectCode.LATE_CHARGE.toString();
            if (bilCrgAmounts.getBilInvoiceCd() == InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue() || bilCrgAmounts
                    .getBilInvoiceCd() == InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()) {
                actionCode = ActionCode.WRITE_OFF_INSTALL.toString();
            } else {
                actionCode = ActionCode.WRITE_OFF_UNBILLED.toString();
            }
        }
        String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(), bilAccount.getBillTypeCd());

        callFinancialApi(differenceAmount, writeOffFeeCharges.getApplicationDate(), BLANK_STRING,
                bilAccount.getAccountNumber(), objectCode, actionCode, BLANK_STRING, BLANK_STRING, BLANK_STRING,
                writeOffFeeCharges.getReasonCode(), bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                writeOffFeeCharges.getApplicationDate(), BLANK_STRING, BillingConstants.SYSTEM_USER_ID,
                DateRoutine.dateTimeAsYYYYMMDDString(writeOffFeeCharges.getApplicationDate()),
                ApplicationProgramIdentifier.BCMOWTF.toString(), BLANK_STRING, BLANK_STRING, BLANK_STRING,
                bilmasterCompanyState[0], bilmasterCompanyState[1], bilAccount.getCurrencyCode());

        if (paiCharge) {
            double paidDifference = paidAmount - chargeDifference;
            processResuspendPaidAmount(paidDifference, bilAccount, writeOffFeeCharges, bilCrgAmounts, chargeDifference);
        }

    }

    private void insertBilAccountSummary(String accountId, ZonedDateTime adjustDueDate, double amount,
            String bilAcyDesCd, ZonedDateTime se3DateTime, String bilDesReaTyp, String userId, String polNbr,
            String polSymbolCd) {

        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId, se3DateTime,
                getBilAcySeq(accountId, se3DateTime));
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(polSymbolCd);
        bilActSummary.setPolNbr(polNbr);
        bilActSummary.setBilAcyDesCd(bilAcyDesCd);
        bilActSummary.setBilDesReaTyp(bilDesReaTyp);
        bilActSummary.setBasAddDataTxt(SEPARATOR_BLANK);

        bilActSummary.setBilAcyDes1Dt(adjustDueDate);
        bilActSummary.setBilAcyDes2Dt(DateRoutine.defaultDateTime());
        bilActSummary.setBilAcyAmt(amount);
        bilActSummary.setUserId(userId);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());

        bilActSummaryRepository.saveAndFlush(bilActSummary);

    }

    private Short getBilAcySeq(String accountId, ZonedDateTime se3DateTime) {
        Short bilAcySeq = 0;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, se3DateTime.with(LocalTime.MIN));
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        return bilAcySeq;
    }

    private String[] readBilMstCoSt(String bilClassCode, String bilTypeCode) {
        String masterCompany = BillingConstants.MASTER_COMPANY_NUMBER;
        String riskState = BLANK_STRING;
        BilMstCoSt bilMstCoSt = bilMstCoStRepository.findRow(bilClassCode, bilTypeCode, BLANK_STRING);
        if (bilMstCoSt != null) {
            masterCompany = bilMstCoSt.getMasterCompanyNbr();
            riskState = bilMstCoSt.getBilStatePvnCd();
        }
        return new String[] { masterCompany, riskState };
    }

    private void callFinancialApi(Double amount, ZonedDateTime applicationDate, String bankCode, String accountNumber,
            String transactionObjectCode, String transactionActionCode, String paybleItemCode, String sourceCode,
            String paymentType, String bilReasonCode, String bilTypeCode, String bilClassCode,
            ZonedDateTime referenceDate, String databaseKey, String operatorId, String orginalEffectiveDate,
            String applicationProgram, String policyId, String countryCode, String lineOfBusiness, String masterCompany,
            String riskState, String currencyCode) {

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();

        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(BLANK_STRING);
        financialApiActivity.setApplicationName(ApplicationName.BCMOCA.toString());
        financialApiActivity.setUserId(BLANK_STRING);
        financialApiActivity.setErrorCode(ERROR_CODE);

        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setMasterCompanyNbr(masterCompany);
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setAgentTtyId(BLANK_STRING);
        financialApiActivity.setAppProgramId(applicationProgram);
        financialApiActivity.setBilAccountId(accountNumber);
        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setBilClassCode(bilClassCode);
        financialApiActivity.setBilDatabaseKey(databaseKey);
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilReceiptTypeCode(paymentType);
        financialApiActivity.setBilReasonCode(bilReasonCode);
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(referenceDate));
        financialApiActivity.setBilSourceCode(sourceCode);
        financialApiActivity.setBilTypeCode(bilTypeCode);
        financialApiActivity.setClientId(BLANK_STRING);
        financialApiActivity.setCountryCode(countryCode);
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setCurrencyCode(currencyCode);
        financialApiActivity.setDepositeBankCode(BLANK_STRING);
        financialApiActivity.setDisbursementId(BLANK_STRING);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setLineOfBusCode(lineOfBusiness);
        financialApiActivity.setMarketSectorCode(BLANK_STRING);
        financialApiActivity.setOriginalEffectiveDate(orginalEffectiveDate);
        financialApiActivity.setOperatorId(operatorId);
        financialApiActivity.setPayableItemCode(paybleItemCode);
        financialApiActivity.setPolicyId(policyId);
        financialApiActivity.setStateCode(riskState);
        financialApiActivity.setStatusCode(BLANK_CHAR);

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {

            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

    private void processResuspendPaidAmount(double reSuspendAmount, BilAccount bilAccount,
            WriteOffFeeCharges writeOffFeeCharges, BilCrgAmounts bilCrgAmounts, double chargeDifference) {

        double dispostionAmount = DECIMAL_ZERO;
        double newDispostionAmount;
        String dispostionTypCode = BLANK_STRING;
        Short distributionNumber = SHORT_ZERO;
        ZonedDateTime distributionDate = null;
        boolean downPaymentType = false;

        List<BilCashDsp> bilCashDspList = bilCashDspRepository.fetchAppliedRowsByInvoiceDate(bilAccount.getAccountId(),
                BilDspTypeCode.APPLIED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                writeOffFeeCharges.getDueDate(), bilCrgAmounts.getBilInvDt(), writeOffFeeCharges.getChargeTypeCode());

        if (bilCashDspList != null && !bilCashDspList.isEmpty()) {
            for (BilCashDsp bilCashDsp : bilCashDspList) {

                BilCashReceipt bilCashReceipt = bilCashReceiptRepository.findById(
                        new BilCashReceiptId(bilAccount.getAccountId(), bilCashDsp.getBilCashDspId().getBilDtbDate(),
                                bilCashDsp.getBilCashDspId().getDtbSequenceNbr()))
                        .orElse(null);
                if (bilCashReceipt == null) {
                    throw new DataNotFoundException("no.data.found");
                }
                if (bilCashReceipt.getReceiptTypeCd().equals(BilReceiptTypeCode.UNDERPAY_WRITEOFF.getValue())) {
                    dispostionTypCode = BilDspTypeCode.WRITE_OFF.getValue();
                    downPaymentType = false;
                } else {
                    if (distributionNumber != bilCashDsp.getBilCashDspId().getDtbSequenceNbr()
                            || distributionDate != bilCashDsp.getBilCashDspId().getBilDtbDate()) {
                        distributionNumber = bilCashDsp.getBilCashDspId().getDtbSequenceNbr();
                        distributionDate = bilCashDsp.getBilCashDspId().getBilDtbDate();
                        dispostionTypCode = BilDspTypeCode.SUSPENDED.getValue();
                        downPaymentType = checkDownPayment(bilCashReceipt, writeOffFeeCharges.getApplicationDate());
                    }
                }

                if (dispostionAmount <= reSuspendAmount) {
                    reSuspendAmount = reSuspendAmount - dispostionAmount;
                    newDispostionAmount = dispostionAmount;
                    bilCashDsp.setRevsRsusIndicator(ReverseReSuspendIndicator.RESUSPENDED.getValue());
                    bilCashDsp.setDspAmount(chargeDifference);
                } else {
                    bilCashDsp.setDspAmount(bilCashDsp.getDspAmount() - reSuspendAmount);
                    newDispostionAmount = reSuspendAmount;
                    reSuspendAmount = DECIMAL_ZERO;
                }

                bilCashDspRepository.saveAndFlush(bilCashDsp);
                insertBilCashDsp(bilCashDsp, writeOffFeeCharges, newDispostionAmount, dispostionTypCode,
                        downPaymentType);

                if (writeOffFeeCharges.getQuoteIndicator().charAt(SHORT_ZERO) != CHAR_Q) {
                    String databaseKey = getBilDatabaseKey(bilCashReceipt.getEntryDate(),
                            bilCashReceipt.getEntryNumber(), bilCashReceipt.getUserId(),
                            bilCashReceipt.getEntrySequenceNumber());
                    String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(),
                            bilAccount.getBillTypeCd());

                    if (dispostionTypCode.equals(BilDspTypeCode.WRITE_OFF.getValue())) {
                        callFinancialApi(newDispostionAmount, writeOffFeeCharges.getApplicationDate(),
                                bilCashReceipt.getBankCd(), bilAccount.getAccountNumber(), ObjectCode.CASH.toString(),
                                ActionCode.WRITE_OFF.toString(), BLANK_STRING,
                                String.valueOf(bilCashReceipt.getCashEntryMethodCd()),
                                bilCashReceipt.getReceiptTypeCd(), BLANK_STRING, bilAccount.getBillTypeCd(),
                                bilAccount.getBillClassCd(), bilCashDsp.getBilCashDspId().getBilDtbDate(), databaseKey,
                                BillingConstants.SYSTEM_USER_ID, BLANK_STRING,
                                ApplicationProgramIdentifier.BCMOWTF.toString(), BLANK_STRING, BLANK_STRING,
                                BLANK_STRING, bilmasterCompanyState[0], bilmasterCompanyState[1],
                                bilAccount.getCurrencyCode());
                    } else {
                        callFinancialApi(newDispostionAmount * -1, writeOffFeeCharges.getApplicationDate(),
                                bilCashReceipt.getBankCd(), bilAccount.getAccountNumber(), ObjectCode.CASH.toString(),
                                ActionCode.APPLIED.toString(), BLANK_STRING,
                                String.valueOf(bilCashReceipt.getCashEntryMethodCd()),
                                bilCashReceipt.getReceiptTypeCd(), BLANK_STRING, bilAccount.getBillTypeCd(),
                                bilAccount.getBillClassCd(), bilCashDsp.getBilCashDspId().getBilDtbDate(), databaseKey,
                                BillingConstants.SYSTEM_USER_ID, BLANK_STRING,
                                ApplicationProgramIdentifier.BCMOWTF.toString(), BLANK_STRING, BLANK_STRING,
                                BLANK_STRING, bilmasterCompanyState[0], bilmasterCompanyState[1],
                                bilAccount.getCurrencyCode());
                    }
                    createServiceCharge(newDispostionAmount, writeOffFeeCharges, bilAccount, bilCrgAmounts);

                }

                if (reSuspendAmount == DECIMAL_ZERO) {
                    break;
                }
            }
        }

    }

    private boolean checkDownPayment(BilCashReceipt bilCashReceipt, ZonedDateTime applicationDate) {

        boolean downPayment = false;
        if (busCdTranslationRepository.existsById(
                new BusCdTranslationId(bilCashReceipt.getReceiptTypeCd(), "DPT", language, "DPTCD"))) {
            BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow("DPCA", BLANK_STRING, BLANK_STRING,
                    SEPARATOR_BLANK, applicationDate, applicationDate);
            if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                try {
                    int downPaymentDay = Integer.parseInt(bilRulesUct.getBrtParmListTxt().substring(0, 3));
                    if (downPaymentDay != SHORT_ZERO) {
                        ZonedDateTime displaceDate = DateRoutine.getAdjustedDate(applicationDate, false, downPaymentDay,
                                null);
                        if (bilCashReceipt.getEntryDate().compareTo(displaceDate) <= SHORT_ZERO) {
                            downPayment = true;
                        }
                    }

                } catch (Exception exception) {

                }

            }
        }
        return downPayment;
    }

    private void createServiceCharge(double newDispostionAmount, WriteOffFeeCharges writeOffFeeCharges,
            BilAccount bilAccount, BilCrgAmounts bilCrgAmounts) {

        String objectCode = BLANK_STRING;
        String actionCode = BLANK_STRING;

        if (writeOffFeeCharges.getChargeTypeCode() == ChargeTypeCodes.LATE_CHARGE.getValue()) {
            objectCode = ObjectCode.LATE_CHARGE.toString();
            if (bilCrgAmounts.getBilInvoiceCd() == InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue() || bilCrgAmounts
                    .getBilInvoiceCd() == InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()) {
                actionCode = ActionCode.APPLY_TO_INSTALL.toString();
            } else {
                actionCode = ActionCode.APPLY_TO_UNBILLED.toString();
            }
        }
        String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(), bilAccount.getBillTypeCd());
        callFinancialApi(newDispostionAmount * -1, writeOffFeeCharges.getApplicationDate(), BLANK_STRING,
                bilAccount.getAccountNumber(), objectCode, actionCode, BLANK_STRING, BLANK_STRING, BLANK_STRING,
                BLANK_STRING, bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                writeOffFeeCharges.getApplicationDate(), BLANK_STRING, BillingConstants.SYSTEM_USER_ID,
                DateRoutine.dateTimeAsYYYYMMDDString(writeOffFeeCharges.getApplicationDate()),
                ApplicationProgramIdentifier.BCMOWTF.toString(), BLANK_STRING, BLANK_STRING, BLANK_STRING,
                bilmasterCompanyState[0], bilmasterCompanyState[1], bilAccount.getCurrencyCode());

    }

    private void insertBilCashDsp(BilCashDsp bilCashDsp, WriteOffFeeCharges writeOffFeeCharges,
            double newDispostionAmount, String dispostionTypeCode, boolean downPaymentType) {
        BilCashDsp bilCashDspSave = new BilCashDsp();
        Short bilDspSequenceNbr = getBilDspSeqNbr(bilCashDsp.getBilCashDspId().getAccountId(),
                bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr());
        BilCashDspId bilCashDspId = new BilCashDspId(bilCashDsp.getBilCashDspId().getAccountId(),
                bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr(),
                bilDspSequenceNbr);
        bilCashDspSave.setBilCashDspId(bilCashDspId);
        bilCashDspSave.setPolicyId(BLANK_STRING);
        bilCashDspSave.setPolicySymbolCd(BLANK_STRING);
        bilCashDspSave.setPolicyNumber(BLANK_STRING);
        bilCashDspSave.setPayableItemCd(BLANK_STRING);
        bilCashDspSave.setAdjustmentDueDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setChargeTypeCd(BLANK_CHAR);
        bilCashDspSave.setUserId(BillingConstants.SYSTEM_USER_ID);
        bilCashDspSave.setDspDate(writeOffFeeCharges.getApplicationDate());
        bilCashDspSave.setDspTypeCd(dispostionTypeCode);

        if (downPaymentType) {
            bilCashDspSave.setManualSuspenseIndicator(ManualSuspendIndicator.ENABLED.getValue());
            bilCashDspSave.setDspReasonCd("DPR");
        } else {
            bilCashDspSave.setManualSuspenseIndicator(BLANK_CHAR);
            bilCashDspSave.setDspReasonCd(BLANK_STRING);
        }

        bilCashDspSave.setDspAmount(newDispostionAmount);
        bilCashDspSave.setPayeeClientId(BLANK_STRING);
        bilCashDspSave.setPayeeAddressSequenceNumber(bilCashDsp.getPayeeAddressSequenceNumber());
        bilCashDspSave.setDraftNbr(SHORT_ZERO);
        if (bilCashDsp.getCreditIndicator() != CHAR_P) {
            bilCashDspSave.setCreditIndicator(BLANK_CHAR);
            bilCashDspSave.setCreditPolicyId(BLANK_STRING);
            bilCashDspSave.setCreditAmountType(BLANK_STRING);
        } else {
            bilCashDspSave.setCreditIndicator(bilCashDsp.getCreditIndicator());
            bilCashDspSave.setCreditPolicyId(bilCashDsp.getCreditPolicyId());
            bilCashDspSave.setCreditAmountType(bilCashDsp.getCreditAmountType());
        }

        bilCashDspSave.setRevsRsusIndicator(BLANK_CHAR);
        bilCashDspSave.setToFromTransferNbr(BLANK_STRING);
        bilCashDspSave.setTransferTypeCd(BLANK_CHAR);
        bilCashDspSave.setStatusCd(BLANK_CHAR);
        bilCashDspSave.setDisbursementId(BLANK_STRING);
        bilCashDspSave.setCheckProdMethodCd(BLANK_CHAR);
        bilCashDspSave.setPolicyEffectiveDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setBilSequenceNumber(SHORT_ZERO);
        bilCashDspSave.setSystemDueDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setInvoiceDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setDisbursementDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setStatementDetailTypeCd(BLANK_STRING);
        bilCashDspRepository.saveAndFlush(bilCashDspSave);

    }

    private String getBilDatabaseKey(ZonedDateTime entryDate, String entryNumber, String operatorId,
            short bilDtbSequenceNumber) {
        String seqNumberSign = "+";

        String dataString = DateRoutine.dateTimeAsYYYYMMDDString(entryDate);
        dataString += entryNumber;
        dataString += StringUtils.rightPad(operatorId, 8, BLANK_CHAR);
        dataString += seqNumberSign;
        dataString += StringUtils.leftPad(String.valueOf(bilDtbSequenceNumber), 5, CHAR_ZERO);

        return dataString;
    }

    private Short getBilDspSeqNbr(String accountId, ZonedDateTime bilDtbDate, Short bilDtbSeqNbr) {
        Short bilDspSeqNbr = 0;
        bilDspSeqNbr = bilCashDspRepository.findMaxDispositionNumber(accountId, bilDtbDate, bilDtbSeqNbr);
        if (bilDspSeqNbr == null) {
            bilDspSeqNbr = 0;
        } else {
            bilDspSeqNbr = (short) (bilDspSeqNbr + 1);
        }
        return bilDspSeqNbr;
    }
}
