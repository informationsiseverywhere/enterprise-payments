package billing.handler.impl;

import static billing.utils.BillingConstants.SYSTEM_USER_ID;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_E;
import static core.utils.CommonConstants.CHAR_H;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_U;
import static core.utils.CommonConstants.CHAR_V;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActInquiry;
import billing.data.entity.BilActRules;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.BilPolActivity;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.HalBoMduXrf;
import billing.data.entity.id.BilActInquiryId;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.entity.id.BilEftBankId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilDatesRepository;
import billing.data.repository.BilEftBankRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.HalBoMduXrfRepository;
import billing.handler.impl.BillingRulesServiceImpl.DisburseKeys;
import billing.handler.model.AccountAdjustDates;
import billing.handler.model.AccountReviewDetails;
import billing.handler.model.RecoveryAccount;
import billing.handler.service.AccountReviewService;
import billing.handler.service.AllDatesResetService;
import billing.handler.service.BillingRulesService;
import billing.handler.service.ChargesAccountAdjustDatesService;
import billing.handler.service.RecoveryService;
import billing.handler.service.RescindDateResetService;
import billing.handler.service.ResumeDateResetService;
import billing.service.BilRulesUctService;
import billing.service.ContractResumeService;
import billing.utils.BillingConstants.AccountStatus;
import billing.utils.BillingConstants.ActivityType;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BilTechKeyTypeCode;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.EftStatusCode;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.ManualSuspendIndicator;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import billing.utils.PB360Service;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.schedule.service.ScheduleService;
import core.utils.DateRoutine;

@Service
public class RecoveryServiceImpl implements RecoveryService {

    @Autowired
    private DateService dateService;

    @Autowired
    private PB360Service pb360Service;

    @Autowired
    private ContractResumeService contractResumeService;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilDatesRepository bilDatesRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private HalBoMduXrfRepository halBoMduXrfRepository;

    @Autowired
    private BillingRulesService billingRulesService;

    @Autowired
    private AccountReviewService accountReviewService;

    @Autowired
    private AllDatesResetService allResetService;

    @Autowired
    private RescindDateResetService rescindResetService;

    @Autowired
    private ResumeDateResetService resumeResetService;

    @Autowired
    private ChargesAccountAdjustDatesService chargesAccountAdjustDatesService;

    @Autowired
    private ScheduleService scheduleService;

    private static final String ACCOUNT_PROCESSING_AND_RECOVERY = "BCMOAX";
    private static final String DISBUR_VOID_MODULE_NAME = "BIL-INT-DISB-STATUS-UPD-VOID";
    private static final String SINGLE_POLICY = "SP";
    private static final String ACCOUNT_BILL = "AB";
    private static final char ENABLED = CHAR_Y;
    private static final char DISABLED = CHAR_N;
    private static final char EXCLUDED_DEDUCTION_DATE = CHAR_X;

    @Override
    @Transactional
    public RecoveryAccount processRecovery(RecoveryAccount recoveryAccount, AccountReviewDetails accountReviewDetails,
            ZonedDateTime recoveryDate, String userSequenceId) {
        String accountId = recoveryAccount.getAccountId();
        char recoverCheckIndicator = ENABLED;
        boolean isResumeAutoDsb = false;
        boolean isDateReset = false;

        BilAccount bilAccount = accountReviewDetails.getBilAccount();
        BilActRules bilActRules = accountReviewDetails.getBilActRules();
        BilSupportPlan bilSupportPlan = accountReviewDetails.getBilSupportPlan();
        Map<String, Object> nextDisburseDates = new HashMap<>();
        nextDisburseDates.put(DisburseKeys.NEXTDISBURSEMENTDATE.toString(), DateRoutine.defaultDateTime());
        nextDisburseDates.put(DisburseKeys.NEXTACWDATE.toString(), DateRoutine.defaultDateTime());
        nextDisburseDates.put(DisburseKeys.CASHINSUSPEND.toString(), false);
        nextDisburseDates.put(DisburseKeys.ACCOUNTLVLDISB.toString(), false);

        if (bilActRules.getBruRnlQteInd() == ENABLED) {
            processQuoteInforce(accountId, bilAccount, recoveryDate);
        }

        if (!bilAccount.getCollectionPlan().trim().isEmpty()) {
            isDateReset = checkEftAccountStatus(accountId, recoveryAccount, bilAccount, bilActRules, nextDisburseDates,
                    accountReviewDetails, recoveryDate, userSequenceId);
        }

        if (isResumeAccount(bilAccount.getStatus()) && !bilAccount.getSuspendedFullReasonCd().trim().isEmpty()) {
            isDateReset = resumeAccount(accountId, bilAccount, recoveryAccount, isDateReset, accountReviewDetails,
                    recoveryDate, userSequenceId);
            boolean isInvoiceRequire = accountReviewService.processAccountStatus(accountId, bilAccount.getStatus(),
                    bilAccount.getSuspendedFullReasonCd(), BLANK_CHAR);
            accountReviewDetails.setInvoiceRequire(isInvoiceRequire);
        } else if (bilAccount.getCashStatusCd() == AccountStatus.DISBURSEMENT.toChar()) {
            isResumeAutoDsb = resumeAutoDisbursement(accountId, bilAccount, bilActRules, recoveryAccount,
                    bilSupportPlan, nextDisburseDates, recoveryDate);
        }

        char suspendPolIndicator = resumePolicy(accountId, recoveryDate);
        if (accountReviewDetails.getSuspendBillIndicator() != CHAR_R && suspendPolIndicator != CHAR_Y) {
            recoverCheckIndicator = recoverCheck(accountId, recoveryAccount.getDriverIndicator(), isResumeAutoDsb,
                    isDateReset, recoverCheckIndicator, recoveryDate);
        }
        if (recoverCheckIndicator == ENABLED) {
            updateAccountAdjustDates(recoveryAccount, isDateReset, accountReviewDetails, recoveryDate, userSequenceId);
            recoverCheckIndicator = DISABLED;
        }

        recoveryAccount.setRecoverCheckIndicator(recoverCheckIndicator);
        return recoveryAccount;
    }

    private boolean checkEftAccountStatus(String accountId, RecoveryAccount recoveryAccount, BilAccount bilAccount,
            BilActRules bilActRules, Map<String, Object> nextDisburseDates, AccountReviewDetails accountReviewDetails,
            ZonedDateTime recoveryDate, String userSequenceId) {

        boolean isDateReset = false;

        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(accountId, ActivityType.ACCOUNT_EFT_NOTICE.getValue())).orElse(null);
        if (bilActInquiry == null || bilActInquiry.getBilNxtAcyDt().compareTo(recoveryDate) > 0) {
            return isDateReset;
        }
        ZonedDateTime bankEffectiveDt = bilEftBankRepository.getMaxEffectiveDateByStatus(accountId,
                BilTechKeyTypeCode.BILL_ACCOUNT.getValue(), recoveryDate, EftStatusCode.OLD.getValue());
        if (bankEffectiveDt == null) {
            updateNextEffectiveDate(bilActInquiry, DateRoutine.defaultDateTime());
        } else {
            isDateReset = updateAccountAdjustDates(recoveryAccount, true, accountReviewDetails, recoveryDate, userSequenceId);
            updateNextEffectiveDate(bilActInquiry, DateRoutine.defaultDateTime());
            List<BilEftBank> bilEftBankList = bilEftBankRepository.fetchOldEftRow(accountId,
                    BilTechKeyTypeCode.BILL_ACCOUNT.getValue(), recoveryDate);
            if (bilEftBankList == null || bilEftBankList.isEmpty()) {
                throw new DataNotFoundException("eft.status.not.found", new Object[] { "accountId", accountId });
            }

            updateStatusOnEftRow(bilEftBankList, recoveryDate);
            bilEftPendingTapeRepository.deleteEftPendingRows(accountId, BilTechKeyTypeCode.BILL_ACCOUNT.getValue(),
                    getEftRecordList(), bankEffectiveDt);

            BilEftBank bilEftBankUpdate = bilEftBankRepository
                    .findById(new BilEftBankId(accountId, BilTechKeyTypeCode.BILL_ACCOUNT.getValue(), bankEffectiveDt))
                    .orElse(null);
            if (bilEftBankUpdate == null || bilEftBankUpdate.getBebAccountStaCd() != AccountStatus.ACTIVE.toChar()) {
                List<BilEftPendingTape> bilEftPendingTapeList = checkPendingDisb(accountId);
                if (bilEftPendingTapeList != null) {
                    boolean epncRule = checkEpncRule(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd());
                    if (epncRule) {
                        if (bilEftBankUpdate != null
                                && bilEftBankUpdate.getBebAccountStaCd() == EftStatusCode.PENDING.getValue()) {
                            bilEftBankUpdate.setBebAccountStaCd(EftStatusCode.ACTIVE.getValue());
                            bilEftBankRepository.saveAndFlush(bilEftBankUpdate);
                        }
                    } else {
                        voidPendingDisb(bilAccount, bilActRules, bilEftPendingTapeList, nextDisburseDates,
                                recoveryDate);
                    }
                }
            }
        }

        return isDateReset;
    }

    private void updateStatusOnEftRow(List<BilEftBank> bilEftBankList, ZonedDateTime bankEffectiveDt) {
        for (BilEftBank bilEftBank : bilEftBankList) {
            if (bilEftBank.getBilEftBankId().getBebEffectiveDt().compareTo(bankEffectiveDt) < 0) {
                bilEftBank.setBebAccountStaCd(EftStatusCode.OLD.getValue());
                bilEftBankRepository.saveAndFlush(bilEftBank);
            }
        }
    }

    private void voidPendingDisb(BilAccount bilAccount, BilActRules bilActRules,
            List<BilEftPendingTape> bilEftPendingTapeList, Map<String, Object> nextDisburseDates,
            ZonedDateTime recoveryDate) {
        processDisbursementRow(bilAccount.getAccountNumber(), bilEftPendingTapeList, recoveryDate);
        billingRulesService.disburseToSource(bilAccount, bilActRules, nextDisburseDates);
    }

    private void processDisbursementRow(String accountNumber, List<BilEftPendingTape> bilEftPendingTapeList,
            ZonedDateTime recoveryDate) {
        String prevDisbursementId = BLANK_STRING;
        for (BilEftPendingTape bilEftPendingTape : bilEftPendingTapeList) {
            String disbursementId = bilEftPendingTape.getDisbursementId();
            callToDisbursementInterfaceAdapter(bilEftPendingTape.getDisbursementId(), accountNumber, recoveryDate);

            if (!disbursementId.equalsIgnoreCase(prevDisbursementId)) {
                bilEftPendingTapeRepository.removePendingDisbursement(
                        bilEftPendingTape.getBilEftPendingTapeId().getTechnicalKey(),
                        bilEftPendingTape.getBilEftPendingTapeId().getTechnicalKeyType(), disbursementId);
            }
            prevDisbursementId = disbursementId;
        }
    }

    private void callToDisbursementInterfaceAdapter(String disbursementId, String accountNumber,
            ZonedDateTime recoveryDate) {
        final String functionCode = "VOIDBDSB";
        final String disbTypeCode = "R";
        final char disbStatus = CHAR_V;

        HalBoMduXrf halBoMduXrf = halBoMduXrfRepository.findById(DISBUR_VOID_MODULE_NAME).orElse(null);
        if (halBoMduXrf == null || halBoMduXrf.getHbmxBobjMduNm().trim().isEmpty()) {
            throw new DataNotFoundException("eft.status.program.not.found",
                    new Object[] { "Disb void", DISBUR_VOID_MODULE_NAME });
        }

        pb360Service.updateDisbursementStatus(ACCOUNT_PROCESSING_AND_RECOVERY, disbStatus, disbTypeCode, disbursementId,
                accountNumber, recoveryDate, ACCOUNT_PROCESSING_AND_RECOVERY, functionCode);
    }

    private boolean checkEpncRule(String bilTypeCd, String bilClassCd) {
        boolean epncRule = false;
        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("EPNC", bilTypeCd, bilClassCd, BLANK_STRING);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == ENABLED) {
            try {
                Integer.parseInt(bilRulesUct.getBrtParmListTxt().trim());
                epncRule = true;
            } catch (NumberFormatException e) {
                epncRule = false;
            }
        }
        return epncRule;
    }

    private List<BilEftPendingTape> checkPendingDisb(String accountId) {
        List<BilEftPendingTape> bilEftPendingTapeList = bilEftPendingTapeRepository.fetchDisbursementRows(accountId,
                BilTechKeyTypeCode.BILL_ACCOUNT.getValue(), DECIMAL_ZERO,
                new ArrayList<>(Arrays.asList(CHAR_H, CHAR_S, CHAR_T, CHAR_U)), BLANK_STRING);
        if (bilEftPendingTapeList == null || bilEftPendingTapeList.isEmpty()) {
            bilEftPendingTapeList = null;
        }
        return bilEftPendingTapeList;
    }

    private void updateNextEffectiveDate(BilActInquiry bilActInquiry, ZonedDateTime nextAcyDate) {
        bilActInquiry.setBilNxtAcyDt(nextAcyDate);
        bilActInquiryRepository.saveAndFlush(bilActInquiry);
    }

    private char recoverCheck(String accountId, char driverSwitch, boolean isResumeAutoDsb, boolean isDateReset,
            char recoverCheckSwitch, ZonedDateTime recoveryDate) {
        if (isDateReset) {
            recoverCheckSwitch = DISABLED;
        }
        ZonedDateTime recoverInvoiceDate = DateRoutine.defaultDateTime();
        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(accountId, ActivityType.BILLING_INVOICE.getValue())).orElse(null);
        if (bilActInquiry != null) {
            recoverInvoiceDate = bilActInquiry.getBilNxtAcyDt();
        }
        if (recoveryDate.compareTo(recoverInvoiceDate) <= SHORT_ZERO) {
            if (driverSwitch != ENABLED && !isResumeAutoDsb) {
                recoverCheckSwitch = BLANK_CHAR;
            }
            if (driverSwitch == ENABLED) {
                short bilDays = checkBilDateExists(accountId, recoveryDate);
                if (bilDays > SHORT_ONE) {
                    recoverCheckSwitch = DISABLED;
                }
            }
        }
        return recoverCheckSwitch;
    }

    private short checkBilDateExists(String accountId, ZonedDateTime recoveryDate) {
        short bilDays = SHORT_ZERO;
        List<ZonedDateTime> invoicedDays = bilDatesRepository.getInvoicedDateCount(accountId,
                new ArrayList<>(
                        Arrays.asList(BLANK_CHAR, InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue())),
                EXCLUDED_DEDUCTION_DATE);
        if (invoicedDays != null && !invoicedDays.isEmpty()) {
            bilDays = (short) invoicedDays.size();
        }
        if (bilDays > SHORT_ONE) {
            for (ZonedDateTime invoiceDate : invoicedDays) {
                if (invoiceDate.compareTo(recoveryDate) < SHORT_ZERO) {
                    bilDays = SHORT_ONE;
                    break;
                }
            }
        }
        return bilDays;
    }

    private char resumePolicy(String accountId, ZonedDateTime recoveryDate) {
        char suspendPolIndicator = BLANK_CHAR;
        List<BilPolActivity> bilPolActivityList = bilPolActivityRepository.fetchBilPolActivityByNextAcyDate(accountId,
                ActivityType.POLICY_RESUME.getValue(), recoveryDate);
        for (BilPolActivity bilPolActivity : bilPolActivityList) {
            BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository
                    .findById(new BilPolicyTermId(accountId, bilPolActivity.getBillPolActivityId().getPolicyId(),
                            bilPolActivity.getBillPolActivityId().getPolEffectiveDt()))
                    .orElse(null);
            if (bilPolicyTerm != null
                    && (bilPolicyTerm.getBillPolStatusCd() == BilPolicyStatusCode.SUSPEND_BILLING.getValue()
                            || bilPolicyTerm.getBillPolStatusCd() == BilPolicyStatusCode.SUSPENDBILLING_FOLLOWUP
                                    .getValue()
                            || bilPolicyTerm.getBillPolStatusCd() == BilPolicyStatusCode.SUSPEND_FOLLOWUP.getValue())) {
                contractResumeService.resumeContract(accountId, bilPolActivity.getBillPolActivityId().getPolicyId(),
                        DateRoutine
                                .dateTimeAsYYYYMMDDString(bilPolActivity.getBillPolActivityId().getPolEffectiveDt()));
            }
            suspendPolIndicator = CHAR_Y;
        }
        return suspendPolIndicator;
    }

    private boolean resumeAutoDisbursement(String accountId, BilAccount bilAccount, BilActRules bilActRules,
            RecoveryAccount recoveryAccount, BilSupportPlan bilSupportPlan, Map<String, Object> nextDisburseDates,
            ZonedDateTime recoveryDate) {
        boolean isResumeAutoDsb = false;
        BilActInquiry bilActInquiryDelete = bilActInquiryRepository
                .findById(new BilActInquiryId(accountId, ActivityType.DISBURSE_RESUME.getValue())).orElse(null);
        if (bilActInquiryDelete != null && bilActInquiryDelete.getBilNxtAcyDt().compareTo(recoveryDate) <= 0) {
            String acyDesCode = getBilAcyDes(bilAccount.getSuspendDisbReasonCd());
            String bilDesReaType = BilDesReasonType.AUTOMATIC_RESUME_BILLING.getValue();
            writeAccountSummary(accountId, acyDesCode, bilDesReaType, BLANK_STRING, BLANK_STRING,
                    DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime(), recoveryDate);
            bilAccount.setCashStatusCd(AccountStatus.ACTIVE.toChar());
            bilAccount.setSuspendDisbReasonCd(BLANK_STRING);
            bilAccountRepository.saveAndFlush(bilAccount);

            bilActInquiryRepository.delete(bilActInquiryDelete);
            isResumeAutoDsb = true;

            String sourceTranferType;
            ZonedDateTime nextDisbursementDate = null;
            ZonedDateTime nextRunDate = DateRoutine.getAdjustedDate(recoveryDate, false, 0, null);
            if (bilActRules.getBruDsbRctDtInd() == ENABLED) {
                nextDisburseDates = billingRulesService.disburseToSource(bilAccount, bilActRules, nextDisburseDates);
            } else {
                nextDisburseDates = billingRulesService.paymentDisburse(bilAccount, bilSupportPlan, nextDisburseDates);
            }

            sourceTranferType = (String) nextDisburseDates.get(DisburseKeys.SOURCETRANSFERTYPE.toString());
            nextDisbursementDate = (ZonedDateTime) nextDisburseDates.get(DisburseKeys.NEXTDISBURSEMENTDATE.toString());

            if (nextDisbursementDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
                if (nextRunDate.compareTo(nextDisbursementDate) > 0) {
                    nextDisbursementDate = nextRunDate;
                }
                recoveryAccount.setCashAcyCallIndicator(CHAR_Y);
                if (bilActRules.getBruDsbRctDtInd() == ENABLED) {
                    updateSourceDisburse(accountId, nextDisbursementDate, sourceTranferType, recoveryDate);
                } else {
                    updateNextDisburse(accountId, nextDisbursementDate);
                }
            }
        }
        return isResumeAutoDsb;
    }

    private void updateNextDisburse(String accountId, ZonedDateTime nextDisbursementDate) {
        BilActInquiry bilActInquiryUpdate = bilActInquiryRepository
                .findById(new BilActInquiryId(accountId, ActivityType.ACCOUNT_DISBURSEMENT.getValue())).orElse(null);
        if (bilActInquiryUpdate == null) {
            bilActInquiryUpdate = new BilActInquiry();
            BilActInquiryId bilActInquiryId = new BilActInquiryId(accountId,
                    ActivityType.ACCOUNT_DISBURSEMENT.getValue());
            bilActInquiryUpdate.setBilActInquiryId(bilActInquiryId);
        }
        bilActInquiryUpdate.setBilNxtAcyDt(nextDisbursementDate);
        bilActInquiryRepository.saveAndFlush(bilActInquiryUpdate);
    }

    private void updateSourceDisburse(String accountId, ZonedDateTime nextDisbursementDate, String sourceTranferType,
            ZonedDateTime recoveryDate) {
        List<BilActInquiry> bilActInquiryList = bilActInquiryRepository.getSuspendDisbRow(accountId,
                new ArrayList<>(Arrays.asList(sourceTranferType)), recoveryDate);
        if (bilActInquiryList != null && !bilActInquiryList.isEmpty()) {
            BilActInquiry bilActInquiryUpdate = bilActInquiryList.get(0);
            bilActInquiryUpdate.setBilNxtAcyDt(nextDisbursementDate);
            bilActInquiryRepository.saveAndFlush(bilActInquiryUpdate);
        }
        List<BilCashDsp> bilCashDspList = bilCashDspRepository.getCreditCashInPast(accountId,
                BilDspTypeCode.SUSPENDED.getValue(), ManualSuspendIndicator.ENABLED.getValue(),
                ReverseReSuspendIndicator.BLANK.getValue(), recoveryDate);
        for (BilCashDsp bilCashDsp : bilCashDspList) {
            if (bilCashReceiptRepository.existsById(new BilCashReceiptId(accountId,
                    bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr()))) {
                bilCashDsp.setDisbursementDate(nextDisbursementDate);
                bilCashDspRepository.saveAndFlush(bilCashDsp);
            }
        }
    }

    @Override
    public boolean resumeAccount(String accountId, BilAccount bilAccount, RecoveryAccount recoveryAccount,
            boolean isDateReset, AccountReviewDetails accountReviewDetails, ZonedDateTime recoveryDate,
            String userSequenceId) {
        BilActInquiry bilActInquiryUpdate = bilActInquiryRepository
                .findById(new BilActInquiryId(accountId, ActivityType.BILLING_RESUME.getValue())).orElse(null);
        if (bilActInquiryUpdate != null && bilActInquiryUpdate.getBilNxtAcyDt().compareTo(recoveryDate) <= 0) {
            String acyDesCode = getBilAcyDes(bilAccount.getSuspendedFullReasonCd());
            String bilDesReaType = BilDesReasonType.RESUME_BILLING_FOLLOWUP_AUTOMATIC.getValue();
            if (bilAccount.getStatus() == AccountStatus.SUSPEND_BILLING.toChar()) {
                bilDesReaType = BilDesReasonType.AUTOMATIC_RESUME_BILLING.getValue();
            } else if (bilAccount.getStatus() == AccountStatus.SUSPEND_FOLLOW_UP.toChar()) {
                bilDesReaType = BilDesReasonType.RESUME_FOLLOWUP_AUTOMATIC.getValue();
            }
            writeAccountSummary(accountId, acyDesCode, bilDesReaType, BLANK_STRING, BLANK_STRING,
                    DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime(), recoveryDate);

            bilAccount.setStatus(AccountStatus.ACTIVE.toChar());
            bilAccount.setSuspendedFullReasonCd(BLANK_STRING);

            bilActInquiryUpdate.setBilNxtAcyDt(DateRoutine.defaultDateTime());
            bilActInquiryRepository.saveAndFlush(bilActInquiryUpdate);

            accountReviewDetails.setSuspendBillIndicator(CHAR_R);
            isDateReset = updateAccountAdjustDates(recoveryAccount, isDateReset, accountReviewDetails, recoveryDate, userSequenceId);

        }
        return isDateReset;
    }

    private void writeAccountSummary(String accountId, String acyDesCode, String bilDesReaType, String policyNumber,
            String policySymbol, ZonedDateTime acyDate1, ZonedDateTime acyDate2, ZonedDateTime recoveryDate) {
        Short bilAcySeq = 0;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, recoveryDate);
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId,
                recoveryDate.with(LocalTime.MIN), bilAcySeq);
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(policySymbol);
        bilActSummary.setPolNbr(policyNumber);
        bilActSummary.setBilAcyDesCd(acyDesCode);
        bilActSummary.setBilDesReaTyp(bilDesReaType);
        bilActSummary.setBilAcyDes1Dt(acyDate1);
        bilActSummary.setBilAcyDes2Dt(acyDate2);
        bilActSummary.setBilAcyAmt(DECIMAL_ZERO);
        bilActSummary.setUserId(SYSTEM_USER_ID);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());
        bilActSummary.setBasAddDataTxt(BLANK_STRING);
        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

    @Override
    public boolean updateAccountAdjustDates(RecoveryAccount recoveryAccount, boolean isDateReset,
            AccountReviewDetails accountReviewDetails, ZonedDateTime recoveryDate, String userSequenceId) {
        String adjustPolicyId = BLANK_STRING;
        char adjustDateResetIndicator = BLANK_CHAR;
        char supportPlanSwitch = BLANK_CHAR;
        if (recoveryAccount.getPolicyId() != null && !recoveryAccount.getPolicyId().trim().isEmpty()) {
            adjustPolicyId = recoveryAccount.getPolicyId();
            adjustDateResetIndicator = CHAR_B;
        }
        if (accountReviewDetails.getSuspendBillIndicator() == CHAR_R) {
            adjustDateResetIndicator = CHAR_S;
        }

        ZonedDateTime eftEffectiveDate = bilEftBankRepository.getMaxEffectiveDateByStatus(
                recoveryAccount.getAccountId(), BilTechKeyTypeCode.BILL_ACCOUNT.getValue(), recoveryDate,
                EftStatusCode.OLD.getValue());
        if (isDateReset && eftEffectiveDate != null && eftEffectiveDate.compareTo(recoveryDate) <= 0) {
            Integer eftBankCount = bilEftBankRepository.countActiveEftBank(recoveryAccount.getAccountId(),
                    BilTechKeyTypeCode.BILL_ACCOUNT.getValue(), recoveryDate,
                    new ArrayList<>(Arrays.asList(EftStatusCode.OLD.getValue())));
            if (eftBankCount == null) {
                eftBankCount = (int) SHORT_ZERO;
            }
            if (eftBankCount == SHORT_ONE) {
                adjustDateResetIndicator = CHAR_A;
                supportPlanSwitch = CHAR_E;
            } else if (eftBankCount > SHORT_ONE) {
                supportPlanSwitch = CHAR_E;
            }
        }

        callAccountAdjustDatesForRecoveryDate(recoveryAccount.getAccountId(), adjustPolicyId,
                recoveryAccount.getPolEffDate(), adjustDateResetIndicator, supportPlanSwitch, recoveryDate,
                userSequenceId);

        isDateReset = false;
        recoveryAccount.setRescindCallIndicator(DISABLED);
        return isDateReset;
    }

    private void processQuoteInforce(String accountId, BilAccount bilAccount, ZonedDateTime recoveryDate) {
        if (bilAccount.getBillTypeCd().equalsIgnoreCase(SINGLE_POLICY)
                || bilAccount.getBillTypeCd().equalsIgnoreCase(ACCOUNT_BILL)) {
            List<BilPolActivity> bilPolActivityList = bilPolActivityRepository
                    .fetchBilPolActivityByNextAcyDate(accountId, ActivityType.RENEWAL_INFORCE.getValue(), recoveryDate);
            for (BilPolActivity bilPolActivity : bilPolActivityList) {
                String policyId = bilPolActivity.getBillPolActivityId().getPolicyId();
                String userData = createBillingApiParam(accountId, policyId, bilAccount, recoveryDate);
                scheduleService.scheduleDeferActivity("BCMOAPI", userData);
            }
        }
    }

    private String createBillingApiParam(String accountId, String policyId, BilAccount bilAccount,
            ZonedDateTime recoveryDate) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(StringUtils.leftPad("INF", 3, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad(accountId, 8, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad(policyId, 16, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(recoveryDate), 10, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad(bilAccount.getBillTypeCd(), 2, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad(bilAccount.getBillClassCd(), 3, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad(bilAccount.getAccountNumber(), 30, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad("N", 1));
        stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 45, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad("N", 1));
        stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 38, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad("0", 8, '0'));
        stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 101, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad("N", 1));
        stringBuilder.append(StringUtils.rightPad("N", 1));
        stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 25, BLANK_CHAR));
        return stringBuilder.toString();
    }

    private String getBilAcyDes(String currentAcyCode) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(StringUtils.left(currentAcyCode, 2));
        stringBuilder.append(CHAR_R);
        return stringBuilder.toString();
    }

    private List<Character> getEftRecordList() {
        List<Character> eftRecordList = new ArrayList<>();
        eftRecordList.add(EftRecordType.PRENOTE.getValue());
        eftRecordList.add(EftRecordType.COMPLETE.getValue());
        return eftRecordList;
    }

    private boolean isResumeAccount(char status) {
        return (status == AccountStatus.SUSPEND_BILLING.toChar()
                || status == AccountStatus.SUSPEND_BILLING_FOLLOW_UP.toChar()
                || status == AccountStatus.SUSPEND_FOLLOW_UP.toChar());
    }

    private void callAccountAdjustDatesForRecoveryDate(String accountId, String policyId, ZonedDateTime polEffDate,
            char adjustDateResetIndicator, char supportPlanSwitch, ZonedDateTime recoveryDate, String userSequenceId) {
        AccountAdjustDates accountAdjustDates = new AccountAdjustDates();
        accountAdjustDates.setAccountId(accountId);
        accountAdjustDates.setPolicyId(policyId);
        if (polEffDate != null) {
            accountAdjustDates.setPolicyEffectiveDate(polEffDate);
        }
        accountAdjustDates.setDateResetIndicator(adjustDateResetIndicator);
        accountAdjustDates.setSupportPlanIndicator(supportPlanSwitch);
        accountAdjustDates.setProcessDate(recoveryDate);
        accountAdjustDates.setUserSequenceId(userSequenceId);
        if (adjustDateResetIndicator == CHAR_A) {
            allResetService.processAllDatesReset(accountAdjustDates);
        } else if (adjustDateResetIndicator == CHAR_B) {
            rescindResetService.processRescindDateReset(accountAdjustDates);
        } else if (adjustDateResetIndicator == CHAR_S) {
            resumeResetService.processResumeDateReset(accountAdjustDates);
        } else {
            chargesAccountAdjustDatesService.processChargesAccountAdjustDates(accountAdjustDates);
        }
    }

}
