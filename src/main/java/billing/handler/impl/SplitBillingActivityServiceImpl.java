package billing.handler.impl;

import static billing.utils.BillingConstants.SYSTEM_USER_ID;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.DECIMAL_ZERO;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilActSummary;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilPolicyId;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.handler.service.CalculatePaidToDateService;
import billing.handler.service.SplitBillingActivityService;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.utils.DateRoutine;

@Service
public class SplitBillingActivityServiceImpl implements SplitBillingActivityService {

    @Autowired
    private DateService dateService;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private CalculatePaidToDateService calculatePaidToDateService;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Override
    public void noncausalRequest(String transactionType, boolean isPolicyPed, String accountId, String policyId,
            ZonedDateTime polEffectiveDate) {

        List<BilPolicyTerm> bilPolicyTermList = null;
        BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(policyId, accountId)).orElse(null);
        if (bilPolicy == null) {
            throw new DataNotFoundException("split.policy.not.found", new Object[] { accountId, policyId });
        }

        switch (NoncausalType.getEnumValue(transactionType)) {
        case PCN_REQUEST:
            List<Character> policyStatusList = getStatusCodeList();
            policyStatusList.add(BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue());
            policyStatusList.add(BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue());
            policyStatusList.add(BilPolicyStatusCode.TRANSFERRED.getValue());
            policyStatusList.add(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue());

            bilPolicyTermList = bilPolicyTermRepository.fetchSplitPoliciesRow(accountId, policyId, polEffectiveDate,
                    policyStatusList);
            noncausalPcnRequest(bilPolicyTermList, bilPolicy, isPolicyPed);
            break;

        case RESCIND_REQUEST:
            bilPolicyTermList = bilPolicyTermRepository.fetchSplitPoliciesRow(accountId, policyId, polEffectiveDate,
                    getStatusCodeList());
            noncausalRescindRequest(bilPolicyTermList, bilPolicy);
            break;

        default:
            throw new InvalidDataException("invalid.split.trans.type", new Object[] { transactionType });
        }
    }

    private void noncausalRescindRequest(List<BilPolicyTerm> bilPolicyTermList, BilPolicy bilPolicy) {

        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
            char noncausalStatus;
            String noncausalAccountId = bilPolicyTerm.getBillPolicyTermId().getBilAccountId();

            writeAccountSummary(noncausalAccountId, bilPolicy.getPolSymbolCd(), bilPolicy.getPolNbr(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "B");

            Double noncausalBalAmount = bilIstScheduleRepository.calculateBalanceAmount(noncausalAccountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            if (noncausalBalAmount == null || noncausalBalAmount == DECIMAL_ZERO) {
                noncausalStatus = BilPolicyStatusCode.CLOSED.getValue();
            } else {
                noncausalStatus = BilPolicyStatusCode.OPEN.getValue();
            }

            updateNoncausalRescindTerm(bilPolicyTerm, noncausalStatus);
        }
    }

    private void updateNoncausalRescindTerm(BilPolicyTerm bilPolicyTerm, char noncausalStatus) {
        bilPolicyTerm.setBptStatusEffDt(dateService.currentDate());
        bilPolicyTerm.setBptWroPrcInd(BLANK_CHAR);
        bilPolicyTerm.setBillPolStatusCd(noncausalStatus);

        bilPolicyTermRepository.saveAndFlush(bilPolicyTerm);
    }

    private void noncausalPcnRequest(List<BilPolicyTerm> bilPolicyTermList, BilPolicy bilPolicy, boolean isPolicyPed) {

        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
            String noncausalAccountId = bilPolicyTerm.getBillPolicyTermId().getBilAccountId();

            updateNoncausalTerm(bilPolicyTerm);

            writeAccountSummary(noncausalAccountId, bilPolicy.getPolSymbolCd(), bilPolicy.getPolNbr(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "NC");

            if (isPolicyPed) {
                ZonedDateTime pedNextAcyDate = getPedNextAcyDate(noncausalAccountId, bilPolicyTerm);
                writeAccountSummary(noncausalAccountId, bilPolicy.getPolSymbolCd(), bilPolicy.getPolNbr(),
                        pedNextAcyDate, "PTD");
            }
        }
    }

    private ZonedDateTime getPedNextAcyDate(String noncausalAccountId, BilPolicyTerm bilPolicyTerm) {
        return calculatePaidToDateService.getPaidToDate(noncausalAccountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilPolicyTerm.getPlnExpDt(), null, false,
                false);
    }

    private void writeAccountSummary(String accountId, String polSymbolCd, String polNumber, ZonedDateTime bilAcyDate1,
            String bilAcyDesCode) {
        Short bilAcySeq = 0;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, dateService.currentDate());
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId, dateService.currentDate(), bilAcySeq);
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(polSymbolCd);
        bilActSummary.setPolNbr(polNumber);
        bilActSummary.setBilAcyDesCd(bilAcyDesCode);
        bilActSummary.setBilDesReaTyp(BLANK_STRING);
        bilActSummary.setBilAcyDes1Dt(bilAcyDate1);
        bilActSummary.setBilAcyDes2Dt(DateRoutine.defaultDateTime());
        bilActSummary.setBilAcyAmt(DECIMAL_ZERO);
        bilActSummary.setUserId(SYSTEM_USER_ID);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());
        bilActSummary.setBasAddDataTxt(BLANK_STRING);
        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

    private void updateNoncausalTerm(BilPolicyTerm bilPolicyTerm) {
        bilPolicyTerm.setBptStatusEffDt(dateService.currentDate());
        bilPolicyTerm.setBillPolStatusCd('P');
        bilPolicyTerm.setBptPcnCausalInd(CHAR_N);

        bilPolicyTermRepository.saveAndFlush(bilPolicyTerm);
    }

    private List<Character> getStatusCodeList() {
        List<Character> policyStatusList = new ArrayList<>();
        policyStatusList.add(BilPolicyStatusCode.PENDING_CANCELLATION.getValue());
        policyStatusList.add(BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue());
        policyStatusList.add(BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue());
        policyStatusList.add(BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue());
        return policyStatusList;
    }

    public enum NoncausalType {

        PCN_REQUEST("NCP"), RESCIND_REQUEST("NCR"), SPACE("");

        private final String noncausalTyp;

        private NoncausalType(String noncausalTyp) {
            this.noncausalTyp = noncausalTyp;
        }

        public boolean hasValue(String value) {
            return noncausalTyp.equals(value);
        }

        @Override
        public String toString() {
            return this.noncausalTyp;
        }

        public static NoncausalType getEnumValue(String typeCode) {
            for (NoncausalType noncausalType : values()) {
                if (noncausalType.noncausalTyp.equals(typeCode)) {
                    return noncausalType;
                }
            }
            return SPACE;
        }
    }

}
