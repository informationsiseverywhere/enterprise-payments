package billing.handler.impl;

import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_C;
import static core.utils.CommonConstants.CHAR_E;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilActInquiry;
import billing.data.entity.BilActRules;
import billing.data.entity.BilCashEntry;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.id.BilActInquiryId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilCashEntryRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilDatesRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.handler.model.AccountProcessingAndRecovery;
import billing.handler.service.CorrectedInvoiceService;
import billing.utils.BillingConstants.ActivityType;
import billing.utils.BillingConstants.DedudctionDate;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.ManualSuspendIndicator;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.utils.DateRoutine;

@Service
public class CorrectedInvoiceServiceImpl implements CorrectedInvoiceService {

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private BilDatesRepository bilDatesRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilCashEntryRepository bilCashEntryRepository;

    private static final List<Character> EFT_CORRECTED_INVOICE_LIST = Arrays.asList('1', '2', '3');

    private static final List<Character> CORRECTED_INVOICE_LIST = Arrays.asList(CHAR_B, CHAR_C, CHAR_E, '1', '2', '3');

    private static final List<String> EFT_CORRECTED_INVOICE_TRIGGERS_LIST = Arrays.asList(ActivityType.ECC.getValue(),
            ActivityType.EEC.getValue(), ActivityType.EBC.getValue());

    @Override
    public void processCorrectedInvoiceTriggers(ZonedDateTime nextInvoiceDate, ZonedDateTime recoveryDate,
            BilActRules bilActRules, AccountProcessingAndRecovery accountProcessingAndRecovery) {

        boolean isCorrectedInvoice = checkCorrectedInvoice(accountProcessingAndRecovery.getCorrectedInvocie());

        if (isCorrectedInvoice) {

            if (!nextInvoiceDate.isEqual(accountProcessingAndRecovery.getRecoveryDate())) {
                processCorrectedInvoiceRequest(accountProcessingAndRecovery, bilActRules);
            }
        } else {
            if (recoveryDate.isEqual(nextInvoiceDate)) {
                updateCorrectedInvoiceDate(accountProcessingAndRecovery);
            }
        }

    }

    private boolean checkCorrectedInvoice(char correctedInvoiceCode) {
        return CORRECTED_INVOICE_LIST.stream().anyMatch(correctedInvoice -> correctedInvoice == correctedInvoiceCode);
    }

    private boolean isEftInvoice(char eftCorrectedInvoiceCode) {
        return EFT_CORRECTED_INVOICE_LIST.stream()
                .anyMatch(eftCorrectedInvoice -> eftCorrectedInvoice == eftCorrectedInvoiceCode);
    }

    private boolean isEftInvoiceTrigger(String eftTriggerCode) {
        return EFT_CORRECTED_INVOICE_TRIGGERS_LIST.stream().anyMatch(eftTrigger -> eftTrigger.equals(eftTriggerCode));
    }

    private void updateCorrectedInvoiceDate(AccountProcessingAndRecovery accountProcessingAndRecovery) {

        List<BilActInquiry> bilActInquiryList = bilActInquiryRepository
                .fetchBilActInquiry(accountProcessingAndRecovery.getAccountId(),
                        Arrays.asList(ActivityType.CIV.getValue(), ActivityType.BIV.getValue(),
                                ActivityType.EIV.getValue(), ActivityType.ECC.getValue(), ActivityType.EBC.getValue(),
                                ActivityType.EEC.getValue()));

        if (bilActInquiryList != null && !bilActInquiryList.isEmpty()) {
            for (BilActInquiry bilActInquiry : bilActInquiryList) {
                if (bilActInquiry.getBilNxtAcyDt().compareTo(DateRoutine.defaultDateTime()) != 0) {
                    bilActInquiry.setBilNxtAcyDt(DateRoutine.defaultDateTime());
                    bilActInquiryRepository.saveAndFlush(bilActInquiry);
                }
            }
        }

    }

    private void processCorrectedInvoiceRequest(AccountProcessingAndRecovery accountProcessingAndRecovery,
            BilActRules bilActRules) {

        ZonedDateTime civDueDate = bilDatesRepository.getMaxCivAdjDueDate(accountProcessingAndRecovery.getAccountId(),
                getInvoiceCodeList(), DedudctionDate.EXCLUDE.getValue());
        if (civDueDate == null) {
            return;
        }

        ZonedDateTime bilAdjDueDate = bilIstScheduleRepository
                .getMaxAdjDueDt(accountProcessingAndRecovery.getAccountId(), getInvoiceCodeList());
        ZonedDateTime chargeDueDate = null;
        if (bilAdjDueDate != null && civDueDate.isEqual(bilAdjDueDate)) {
            chargeDueDate = bilAdjDueDate;
        } else {
            chargeDueDate = bilCrgAmountsRepository.getMaxAdjDueDt(accountProcessingAndRecovery.getAccountId(),
                    getInvoiceCodeList());
        }
        if (bilAdjDueDate == null && chargeDueDate == null) {
            return;
        }
        civDueDate = calculateCivDueDate(civDueDate, bilAdjDueDate, chargeDueDate);

        List<BilActInquiry> bilActInquiryList = bilActInquiryRepository
                .fetchBilActInquiry(accountProcessingAndRecovery.getAccountId(),
                        Arrays.asList(ActivityType.CIV.getValue(), ActivityType.BIV.getValue(),
                                ActivityType.EIV.getValue(), ActivityType.ECC.getValue(), ActivityType.EBC.getValue(),
                                ActivityType.EEC.getValue()));

        if (bilActInquiryList != null && !bilActInquiryList.isEmpty()) {

            boolean isCorrectedInvoiceDate = checkCorrectedInvocieDate(civDueDate, bilActInquiryList.get(SHORT_ZERO),
                    bilActRules, accountProcessingAndRecovery.getRecoveryDate());
            if (isCorrectedInvoiceDate) {
                updateCorrectedInvoiceTriggers(isEftInvoice(accountProcessingAndRecovery.getCorrectedInvocie()),
                        accountProcessingAndRecovery, bilActInquiryList.get(SHORT_ZERO));
            }

        } else if (isEftInvoice(accountProcessingAndRecovery.getCorrectedInvocie())) {
            updateCorrectedInvoiceTriggers(true, accountProcessingAndRecovery, null);
        } else {
            boolean isCorrectedInvoiceDate = checkCorrectedInvocieDate(civDueDate, null, bilActRules,
                    accountProcessingAndRecovery.getRecoveryDate());
            if (isCorrectedInvoiceDate) {
                updateCorrectedInvoiceTriggers(false, accountProcessingAndRecovery, null);
            }
        }
    }

    private ZonedDateTime calculateCivDueDate(ZonedDateTime civDueDate, ZonedDateTime bilAdjDueDate,
            ZonedDateTime chargeDueDate) {

        if (!civDueDate.isEqual(bilAdjDueDate) && !civDueDate.isEqual(chargeDueDate)) {
            if (chargeDueDate.isAfter(bilAdjDueDate)) {
                civDueDate = chargeDueDate;
            } else {
                civDueDate = bilAdjDueDate;
            }
        }
        return civDueDate;
    }

    private void updateCorrectedInvoiceTriggers(boolean isEftInvoice,
            AccountProcessingAndRecovery accountProcessingAndRecovery, BilActInquiry bilActInquiry) {

        ZonedDateTime tomorrow = null;
        ZonedDateTime correctedInvoiceDate = bilActSummaryRepository.getMaxCorrectedBilAcyDate(
                accountProcessingAndRecovery.getAccountId(), Arrays.asList("INV", "ENV", "CNV"),
                accountProcessingAndRecovery.getRecoveryDate());

        if (correctedInvoiceDate.isEqual(accountProcessingAndRecovery.getRecoveryDate())) {
            tomorrow = DateRoutine.adjustDateWithOffset(accountProcessingAndRecovery.getRecoveryDate(), false, 1, '+', null);
        }
        processCorrectedInvoiceTriggers(tomorrow, isEftInvoice, accountProcessingAndRecovery, bilActInquiry);
    }

    private void processCorrectedInvoiceTriggers(ZonedDateTime tomorrow, boolean isEftInvoice,
            AccountProcessingAndRecovery accountProcessingAndRecovery, BilActInquiry bilActInquiry) {

        if (bilActInquiry != null) {
            boolean isCivPayment = checkCivPayment(bilActInquiry);
            boolean isCivCredit = checkCivCredit(bilActInquiry);
            boolean isCivCreditPayment = checkCivCreditPayment(bilActInquiry);
            if (isCivCreditPayment
                    || isCivPayment && (accountProcessingAndRecovery.getCorrectedInvocie() == CHAR_C
                            || accountProcessingAndRecovery.getCorrectedInvocie() == '2')
                    || isCivCredit && (accountProcessingAndRecovery.getCorrectedInvocie() == CHAR_E
                            || accountProcessingAndRecovery.getCorrectedInvocie() == '1')) {
                return;
            }
            if (isCivPayment
                    && (accountProcessingAndRecovery.getCorrectedInvocie() == CHAR_E
                            || accountProcessingAndRecovery.getCorrectedInvocie() == '1'
                            || accountProcessingAndRecovery.getCorrectedInvocie() == CHAR_B
                            || accountProcessingAndRecovery.getCorrectedInvocie() == '3')
                    || isCivCredit && (accountProcessingAndRecovery.getCorrectedInvocie() == CHAR_C
                            || accountProcessingAndRecovery.getCorrectedInvocie() == '2'
                            || accountProcessingAndRecovery.getCorrectedInvocie() == CHAR_B
                            || accountProcessingAndRecovery.getCorrectedInvocie() == '3')) {
                updateCorrectedInvoiceTriggers(tomorrow, bilActInquiry, accountProcessingAndRecovery);
                return;
            }
        }

        boolean isSuspend = false;
        ZonedDateTime tapeDate = getTapeDate(accountProcessingAndRecovery);
        String civTrigger = BLANK_STRING;
        if (accountProcessingAndRecovery.getCorrectedInvocie() == CHAR_C
                || accountProcessingAndRecovery.getCorrectedInvocie() == '2') {
            if (isEftInvoice) {
                civTrigger = ActivityType.ECC.getValue();
            } else {
                civTrigger = ActivityType.CIV.getValue();
            }

            isSuspend = checkCiv(accountProcessingAndRecovery);

        } else if (accountProcessingAndRecovery.getCorrectedInvocie() == CHAR_B
                || accountProcessingAndRecovery.getCorrectedInvocie() == '3') {

            if (isEftInvoice) {
                civTrigger = ActivityType.EBC.getValue();
            } else {
                civTrigger = ActivityType.BIV.getValue();
            }

        } else if (accountProcessingAndRecovery.getCorrectedInvocie() == CHAR_E
                || accountProcessingAndRecovery.getCorrectedInvocie() == '1') {

            if (isEftInvoice) {
                civTrigger = ActivityType.EEC.getValue();
            } else {
                civTrigger = ActivityType.EIV.getValue();
            }
        }

        if (!isSuspend && tapeDate != null) {
            writeCivTrigger(accountProcessingAndRecovery.getAccountId(), tapeDate, tomorrow, civTrigger, isEftInvoice,
                    bilActInquiry, accountProcessingAndRecovery.getRecoveryDate());
        }

    }

    private boolean checkCivCreditPayment(BilActInquiry bilActInquiry) {
        return bilActInquiry.getBilActInquiryId().getBilAcyTypeCd().equals(ActivityType.BIV.getValue())
                || bilActInquiry.getBilActInquiryId().getBilAcyTypeCd().equals(ActivityType.EBC.getValue());
    }

    private boolean checkCivCredit(BilActInquiry bilActInquiry) {
        return bilActInquiry.getBilActInquiryId().getBilAcyTypeCd().equals(ActivityType.EIV.getValue())
                || bilActInquiry.getBilActInquiryId().getBilAcyTypeCd().equals(ActivityType.EEC.getValue());
    }

    private boolean checkCivPayment(BilActInquiry bilActInquiry) {
        return bilActInquiry.getBilActInquiryId().getBilAcyTypeCd().equals(ActivityType.CIV.getValue())
                || bilActInquiry.getBilActInquiryId().getBilAcyTypeCd().equals(ActivityType.ECC.getValue());
    }

    private ZonedDateTime getTapeDate(AccountProcessingAndRecovery accountProcessingAndRecovery) {

        ZonedDateTime tapeDate = null;
        List<BilEftPendingTape> bilEftPendingTapeList = bilEftPendingTapeRepository.fetchEftDrAmount(
                accountProcessingAndRecovery.getAccountId(), CHAR_A, Arrays.asList(EftRecordType.ORIGINAL.getValue()),
                SEPARATOR_BLANK);
        if (bilEftPendingTapeList != null && !bilEftPendingTapeList.isEmpty()) {
            tapeDate = bilEftPendingTapeList.get(SHORT_ZERO).getBilEftPendingTapeId().getEftTapeDate();
        } else {
            BilActInquiry bilActInquiry1 = bilActInquiryRepository
                    .findById(new BilActInquiryId(accountProcessingAndRecovery.getAccountId(), "ERP")).orElse(null);
            if (bilActInquiry1 != null) {
                tapeDate = bilActInquiry1.getBilNxtAcyDt();
                bilActInquiryRepository.delete(bilActInquiry1);
            }
        }
        return tapeDate;
    }

    private void updateCorrectedInvoiceTriggers(ZonedDateTime tomorrow, BilActInquiry bilActInquiry,
            AccountProcessingAndRecovery accountProcessingAndRecovery) {

        if (isEftInvoiceTrigger(bilActInquiry.getBilActInquiryId().getBilAcyTypeCd())) {
            bilActInquiryRepository.updateBilAcyTypeCode(bilActInquiry.getBilActInquiryId(), "EBC");
        } else if (tomorrow == null) {
            bilActInquiryRepository.updateBilAcyTypeCode(bilActInquiry.getBilActInquiryId(), "BIV",
                    accountProcessingAndRecovery.getRecoveryDate());
        } else {
            bilActInquiryRepository.updateBilAcyTypeCode(bilActInquiry.getBilActInquiryId(), "BIV", tomorrow);
        }

    }

    private boolean checkCorrectedInvocieDate(ZonedDateTime civDueDate, BilActInquiry bilActInquiry,
            BilActRules bilActRules, ZonedDateTime recoveryDate) {

        if (bilActInquiry != null && isEftInvoiceTrigger(bilActInquiry.getBilActInquiryId().getBilAcyTypeCd())) {
            return true;
        }
        return checkCivDate(civDueDate, bilActRules, recoveryDate);
    }

    private boolean checkCivDate(ZonedDateTime civDueDate, BilActRules bilActRules, ZonedDateTime recoveryDate) {
        ZonedDateTime civDate = null;
        if (bilActRules.getBruCcrNbrDays() == SHORT_ZERO) {
            civDate = civDueDate;
        } else {
            civDate = DateRoutine.adjustDateWithOffset(civDueDate, false, bilActRules.getBruCcrNbrDays(), '+', null);
        }

        return recoveryDate.isEqual(civDate) || recoveryDate.isBefore(civDate);

    }

    private boolean checkCiv(AccountProcessingAndRecovery accountProcessingAndRecovery) {

        BilCashReceipt bilCashReceipt = bilCashReceiptRepository
                .findById(new BilCashReceiptId(accountProcessingAndRecovery.getAccountId(),
                        accountProcessingAndRecovery.getDistributionDate(),
                        accountProcessingAndRecovery.getDistributionSequenceNumber()))
                .orElse(null);
        if (bilCashReceipt != null) {
            List<BilCashEntry> bilCashEntryList = bilCashEntryRepository.getPaymentsByManualSusInd(
                    bilCashReceipt.getEntryDate(), bilCashReceipt.getEntryNumber(),
                    bilCashReceipt.getEntrySequenceNumber(), ManualSuspendIndicator.ENABLED.getValue());
            if (bilCashEntryList != null && !bilCashEntryList.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    private void writeCivTrigger(String accountId, ZonedDateTime tapeDate, ZonedDateTime tomorrow, String civTrigger,
            boolean isEftInvocie, BilActInquiry bilActInquiry, ZonedDateTime recoveryDate) {
        if ((bilActInquiry != null) && ((civTrigger.equals(ActivityType.BIV.getValue())
                || civTrigger.equals(ActivityType.EBC.getValue()))
                && (bilActInquiry.getBilActInquiryId().getBilAcyTypeCd().equals(ActivityType.CIV.getValue())
                        || bilActInquiry.getBilActInquiryId().getBilAcyTypeCd().equals(ActivityType.ECC.getValue())
                        || bilActInquiry.getBilActInquiryId().getBilAcyTypeCd().equals(ActivityType.EIV.getValue())
                        || bilActInquiry.getBilActInquiryId().getBilAcyTypeCd().equals(ActivityType.EEC.getValue())))) {
            bilActInquiryRepository.deleteCorrectedInvoiceRows(bilActInquiry.getBilActInquiryId().getBilAccountId(),
                    Arrays.asList(ActivityType.CIV.getValue(), ActivityType.ECC.getValue(), ActivityType.EIV.getValue(),
                            ActivityType.EEC.getValue()));
        }

        BilActInquiry bilActInquiry1 = new BilActInquiry();
        BilActInquiryId bilActInquiryId = new BilActInquiryId();
        bilActInquiryId.setBilAccountId(accountId);
        bilActInquiryId.setBilAcyTypeCd(civTrigger);
        bilActInquiry1.setBilActInquiryId(bilActInquiryId);
        if (isEftInvocie) {
            bilActInquiry1.setBilNxtAcyDt(tapeDate);
        } else if (tomorrow == null) {
            bilActInquiry1.setBilNxtAcyDt(recoveryDate);
        } else {
            bilActInquiry1.setBilNxtAcyDt(tomorrow);
        }
        bilActInquiryRepository.save(bilActInquiry1);
    }

    private List<Character> getInvoiceCodeList() {
        return Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());
    }

}
