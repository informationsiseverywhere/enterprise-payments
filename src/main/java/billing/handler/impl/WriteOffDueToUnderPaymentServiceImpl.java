package billing.handler.impl;

import static billing.utils.BillingConstants.ERROR_CODE;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_V;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.LocalTime;
import java.time.ZonedDateTime;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActRules;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilMstCoSt;
import billing.data.entity.id.BilActRulesId;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilCashDspId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.repository.BilActRulesRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilMstCoStRepository;
import billing.handler.model.WriteOffPayment;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilReceiptTypeCode;
import billing.utils.BillingConstants.CashEntryMethod;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class WriteOffDueToUnderPaymentServiceImpl {

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilActRulesRepository bilActRulesRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilMstCoStRepository bilMstCoStRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private DateService dateService;

    private static final String SYSTEM_IW = "SYSTEMIW";

    @Transactional
    public BilCashDsp writeOffDueToUnderPayment(BilAccount bilAccount, WriteOffPayment writeOffPayment) {

        String receiptType = BilReceiptTypeCode.UNDERPAY_WRITEOFF.getValue();
        String bilAcyDesCd = "WU";
        if (writeOffPayment.getProcessIndicator() == CHAR_V) {
            receiptType = BilReceiptTypeCode.EVEN_ADJUSTMENT.getValue();
            bilAcyDesCd = "IE";
        }

        Short bilDtbSequenceNbr = getBilDtbSeqNbr(bilAccount.getAccountId(), writeOffPayment.getApplicationDate());
        BilCashReceipt bilCashReceipt = insertBilCashReceipt(bilAccount.getAccountId(), writeOffPayment, receiptType,
                bilDtbSequenceNbr);
        BilCashDsp bilCashDsp = insertBilCashDsp(bilAccount.getAccountId(), writeOffPayment, bilDtbSequenceNbr,
                receiptType);
        insertBilAccountSummary(bilAccount.getAccountId(), bilCashDsp.getAdjustmentDueDate(), bilCashDsp.getDspAmount(),
                bilAcyDesCd, writeOffPayment.getApplicationDate(), "CWA", SYSTEM_IW, BLANK_STRING, BLANK_STRING);
        if (writeOffPayment.getQuoteIndicator().charAt(SHORT_ZERO) != CHAR_Q) {
            String databaseKey = getBilDatabaseKey(writeOffPayment.getApplicationDate(), BillingConstants.ENTRY_NUMBER,
                    SYSTEM_IW, SHORT_ZERO);

            String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(), bilAccount.getBillTypeCd());

            callFinancialApi(writeOffPayment.getPaymentAmount(), writeOffPayment.getApplicationDate(),
                    bilCashReceipt.getBankCd(), bilAccount.getAccountNumber(), ObjectCode.CASH.toString(),
                    ActionCode.SUSPENSE.toString(), BLANK_STRING, String.valueOf(bilCashReceipt.getCashEntryMethodCd()),
                    receiptType, BLANK_STRING, bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                    bilCashDsp.getBilCashDspId().getBilDtbDate(), databaseKey, SYSTEM_IW, BLANK_STRING,
                    ApplicationProgramIdentifier.BCMOIW.toString(), BLANK_STRING, BLANK_STRING, BLANK_STRING,
                    bilmasterCompanyState[0], bilmasterCompanyState[1], bilAccount.getCurrencyCode());
        }
        return bilCashDsp;
    }

    private Short getBilDtbSeqNbr(String accountId, ZonedDateTime se3DateTime) {
        Short bilDtbSeqNbr = 0;
        bilDtbSeqNbr = bilCashReceiptRepository.getMaxBilDtbSeqNbr(accountId, se3DateTime);
        if (bilDtbSeqNbr == null) {
            bilDtbSeqNbr = 0;
        } else {
            bilDtbSeqNbr = (short) (bilDtbSeqNbr + 1);
        }
        return bilDtbSeqNbr;
    }

    private BilCashReceipt insertBilCashReceipt(String accountId, WriteOffPayment writeOffPayment, String receiptType,
            Short bilDtbSequenceNbr) {
        BilCashReceipt bilCashReceipt = new BilCashReceipt();
        BilCashReceiptId bilCashReceiptId = new BilCashReceiptId(accountId, writeOffPayment.getApplicationDate(),
                bilDtbSequenceNbr);
        bilCashReceipt.setBilCashReceiptId(bilCashReceiptId);
        bilCashReceipt.setEntryDate(writeOffPayment.getApplicationDate());
        bilCashReceipt.setEntryNumber(BillingConstants.ENTRY_NUMBER);
        bilCashReceipt.setEntrySequenceNumber(getEntrySequenceNumber(accountId, BillingConstants.ENTRY_NUMBER,
                writeOffPayment.getApplicationDate(), SYSTEM_IW, receiptType));
        bilCashReceipt.setUserId(SYSTEM_IW);
        bilCashReceipt.setReceiptAmount(writeOffPayment.getPaymentAmount());
        bilCashReceipt.setReceiptId(BLANK_STRING);
        bilCashReceipt.setReceiptTypeCd(receiptType);
        bilCashReceipt.setReceiptComment(BLANK_STRING);
        bilCashReceipt.setCreditCardTypeCd(BLANK_CHAR);
        bilCashReceipt.setCreditCardAccounttNumber(SEPARATOR_BLANK);
        bilCashReceipt.setCreditCardExpirationDate(SEPARATOR_BLANK);
        bilCashReceipt.setCredutCardAuthroizationApproval(SEPARATOR_BLANK);
        bilCashReceipt.setToFroTransferNumber(SEPARATOR_BLANK);
        bilCashReceipt.setTransferTypeCd(BLANK_CHAR);
        bilCashReceipt.setReceiptReceiveDate(writeOffPayment.getApplicationDate());
        bilCashReceipt.setBankCd(BillingConstants.BIL_BANK_CODE);
        bilCashReceipt.setDepositeDate(DateRoutine.defaultDateTime());
        bilCashReceipt.setCashEntryMethodCd(CashEntryMethod.TOLERANCE_WRITEOFF.getCashEntryMethod());
        bilCashReceipt.setTaxYearNumber(SHORT_ZERO);
        bilCashReceipt.setThirdPartyCashIdentifier(BLANK_CHAR);
        bilCashReceipt.setPostMarkDate(getPostMarkDate(writeOffPayment.getApplicationDate()));
        bilCashReceipt.setPostMarkCd(BLANK_CHAR);
        bilCashReceipt.setAgencyNumber(SEPARATOR_BLANK);
        bilCashReceiptRepository.saveAndFlush(bilCashReceipt);
        return bilCashReceipt;

    }

    private ZonedDateTime getPostMarkDate(ZonedDateTime applicattionDate) {

        ZonedDateTime postMarkDate = DateRoutine.defaultDateTime();
        BilActRules bilActRules = bilActRulesRepository.findById(new BilActRulesId(BLANK_STRING, BLANK_STRING))
                .orElse(null);
        if (bilActRules == null) {
            throw new DataNotFoundException("config.bil.account.rules.table.not.found");
        }

        if (bilActRules.getBruPstmrkReqInd() == CHAR_Y) {
            postMarkDate = applicattionDate;
        }
        return postMarkDate;
    }

    private Short getEntrySequenceNumber(String accountId, String entryNumber, ZonedDateTime applicattionDate,
            String userId, String receiptTypeCd) {
        Short entrySequenceNbr = bilCashReceiptRepository.getMaxEntrySeqNbr(accountId, applicattionDate, entryNumber,
                userId, receiptTypeCd);
        if (entrySequenceNbr == null) {
            entrySequenceNbr = 0;
        } else {
            entrySequenceNbr = (short) (entrySequenceNbr + 1);
        }
        return entrySequenceNbr;
    }

    private BilCashDsp insertBilCashDsp(String accountId, WriteOffPayment writeOffPayment, Short bilDtbSequenceNbr,
            String receiptType) {
        BilCashDsp bilCashDsp = new BilCashDsp();
        BilCashDspId bilCashDspId = new BilCashDspId(accountId, writeOffPayment.getApplicationDate(), bilDtbSequenceNbr,
                SHORT_ZERO);
        bilCashDsp.setBilCashDspId(bilCashDspId);
        bilCashDsp.setPolicyId(SEPARATOR_BLANK);
        bilCashDsp.setPolicySymbolCd(SEPARATOR_BLANK);

        if (receiptType.equals("IE")) {
            bilCashDsp.setPolicyNumber(writeOffPayment.getPolicyNumber());
        } else {
            bilCashDsp.setPolicyNumber(SEPARATOR_BLANK);
        }

        bilCashDsp.setPayableItemCd(SEPARATOR_BLANK);
        bilCashDsp.setAdjustmentDueDate(writeOffPayment.getAdjustDueDate());
        bilCashDsp.setChargeTypeCd(BLANK_CHAR);
        bilCashDsp.setUserId(SYSTEM_IW);
        bilCashDsp.setDspDate(writeOffPayment.getApplicationDate());
        bilCashDsp.setDspTypeCd(BilDspTypeCode.SUSPENDED.getValue());
        bilCashDsp.setDspReasonCd("WU");
        bilCashDsp.setManualSuspenseIndicator(CHAR_N);
        bilCashDsp.setDspAmount(writeOffPayment.getPaymentAmount());
        bilCashDsp.setPayeeClientId(SEPARATOR_BLANK);
        bilCashDsp.setPayeeAddressSequenceNumber(SHORT_ZERO);
        bilCashDsp.setDraftNbr(SHORT_ZERO);
        bilCashDsp.setCreditIndicator(CHAR_N);
        bilCashDsp.setCreditPolicyId(SEPARATOR_BLANK);
        bilCashDsp.setCreditAmountType(SEPARATOR_BLANK);
        bilCashDsp.setRevsRsusIndicator(BLANK_CHAR);
        bilCashDsp.setToFromTransferNbr(SEPARATOR_BLANK);
        bilCashDsp.setTransferTypeCd(BLANK_CHAR);
        bilCashDsp.setStatusCd(BLANK_CHAR);
        bilCashDsp.setDisbursementId(SEPARATOR_BLANK);
        bilCashDsp.setCheckProdMethodCd(BLANK_CHAR);
        bilCashDsp.setPolicyEffectiveDate(DateRoutine.defaultDateTime());
        bilCashDsp.setBilSequenceNumber(SHORT_ZERO);
        bilCashDsp.setSystemDueDate(DateRoutine.defaultDateTime());
        bilCashDsp.setInvoiceDate(DateRoutine.defaultDateTime());
        bilCashDsp.setDisbursementDate(DateRoutine.defaultDateTime());
        bilCashDsp.setStatementDetailTypeCd(SEPARATOR_BLANK);
        bilCashDspRepository.saveAndFlush(bilCashDsp);

        return bilCashDsp;

    }

    private void insertBilAccountSummary(String accountId, ZonedDateTime adjustDueDate, double amount,
            String bilAcyDesCd, ZonedDateTime se3DateTime, String bilDesReaTyp, String userId, String polNbr,
            String polSymbolCd) {

        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId, se3DateTime,
                getBilAcySeq(accountId, se3DateTime));
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(polSymbolCd);
        bilActSummary.setPolNbr(polNbr);
        bilActSummary.setBilAcyDesCd(bilAcyDesCd);
        bilActSummary.setBilDesReaTyp(bilDesReaTyp);
        bilActSummary.setBasAddDataTxt(SEPARATOR_BLANK);

        bilActSummary.setBilAcyDes1Dt(adjustDueDate);
        bilActSummary.setBilAcyDes2Dt(DateRoutine.defaultDateTime());
        bilActSummary.setBilAcyAmt(amount);
        bilActSummary.setUserId(userId);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());

        bilActSummaryRepository.saveAndFlush(bilActSummary);

    }

    private Short getBilAcySeq(String accountId, ZonedDateTime se3DateTime) {
        Short bilAcySeq = 0;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, se3DateTime.with(LocalTime.MIN));
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        return bilAcySeq;
    }

    private String getBilDatabaseKey(ZonedDateTime entryDate, String entryNumber, String operatorId,
            short bilDtbSequenceNumber) {
        String seqNumberSign = "+";

        String dataString = DateRoutine.dateTimeAsYYYYMMDDString(entryDate);
        dataString += entryNumber;
        dataString += StringUtils.rightPad(operatorId, 8, BLANK_CHAR);
        dataString += seqNumberSign;
        dataString += StringUtils.leftPad(String.valueOf(bilDtbSequenceNumber), 5, CHAR_ZERO);

        return dataString;
    }

    private String[] readBilMstCoSt(String bilClassCode, String bilTypeCode) {
        String masterCompany = BillingConstants.MASTER_COMPANY_NUMBER;
        String riskState = BLANK_STRING;
        BilMstCoSt bilMstCoSt = bilMstCoStRepository.findRow(bilClassCode, bilTypeCode, BLANK_STRING);
        if (bilMstCoSt != null) {
            masterCompany = bilMstCoSt.getMasterCompanyNbr();
            riskState = bilMstCoSt.getBilStatePvnCd();
        }
        return new String[] { masterCompany, riskState };
    }

    private void callFinancialApi(Double amount, ZonedDateTime applicationDate, String bankCode, String accountNumber,
            String transactionObjectCode, String transactionActionCode, String paybleItemCode, String sourceCode,
            String paymentType, String bilReasonCode, String bilTypeCode, String bilClassCode,
            ZonedDateTime referenceDate, String databaseKey, String operatorId, String orginalEffectiveDate,
            String applicationProgram, String policyId, String countryCode, String lineOfBusiness, String masterCompany,
            String riskState, String currencyCode) {

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();

        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(BLANK_STRING);
        financialApiActivity.setApplicationName(ApplicationName.BCMOCA.toString());
        financialApiActivity.setUserId(BLANK_STRING);
        financialApiActivity.setErrorCode(ERROR_CODE);

        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setMasterCompanyNbr(masterCompany);
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setAgentTtyId(BLANK_STRING);
        financialApiActivity.setAppProgramId(applicationProgram);
        financialApiActivity.setBilAccountId(accountNumber);
        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setBilClassCode(bilClassCode);
        financialApiActivity.setBilDatabaseKey(databaseKey);
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilReceiptTypeCode(paymentType);
        financialApiActivity.setBilReasonCode(bilReasonCode);
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(referenceDate));
        financialApiActivity.setBilSourceCode(sourceCode);
        financialApiActivity.setBilTypeCode(bilTypeCode);
        financialApiActivity.setClientId(BLANK_STRING);
        financialApiActivity.setCountryCode(countryCode);
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setCurrencyCode(currencyCode);
        financialApiActivity.setDepositeBankCode(BLANK_STRING);
        financialApiActivity.setDisbursementId(BLANK_STRING);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setLineOfBusCode(lineOfBusiness);
        financialApiActivity.setMarketSectorCode(BLANK_STRING);
        financialApiActivity.setOriginalEffectiveDate(orginalEffectiveDate);
        financialApiActivity.setOperatorId(operatorId);
        financialApiActivity.setPayableItemCode(paybleItemCode);
        financialApiActivity.setPolicyId(policyId);
        financialApiActivity.setStateCode(riskState);
        financialApiActivity.setStatusCode(BLANK_CHAR);

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {

            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

}
