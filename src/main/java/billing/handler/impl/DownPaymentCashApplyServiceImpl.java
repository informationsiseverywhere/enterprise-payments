package billing.handler.impl;

import static billing.utils.BillingConstants.ERROR_CODE;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActInquiry;
import billing.data.entity.BilAmtTrmRlt;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilIstSchedule;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.HalBoMduXrf;
import billing.data.entity.id.BilActInquiryId;
import billing.data.entity.id.BilCashDspId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.entity.id.BilPolicyId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilAmtSchRltRepository;
import billing.data.repository.BilAmtTrmRltRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilEftBankRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.HalBoMduXrfRepository;
import billing.handler.model.DownPaymentInformation;
import billing.model.Event;
import billing.service.BilRulesUctService;
import billing.service.EventPostingService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.BilAmountsEftIndicator;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.InvoiceTypeCode;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.id.BusCdTranslationId;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class DownPaymentCashApplyServiceImpl {

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilAmtSchRltRepository bilAmtSchRltRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private DateService dateService;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private BilAmtTrmRltRepository bilAmtTrmRltRepository;

    @Autowired
    private HalBoMduXrfRepository halBoMduXrfRepository;

    @Autowired
    private EventPostingService eventPostingService;
    
    @Value("${language}")
    private String language;

    private static final String BCMCODP = "BCMOCDP";
    public static final Logger logger = LogManager.getLogger(DownPaymentCashApplyServiceImpl.class);

    @Transactional
    public boolean cashApply(BilCashDsp bilCashDsp, String quoteIndicator, ZonedDateTime cashApplyDate, String userSequenceId) {

        boolean isDownPaymentRowsExist = false;
        List<String> excludePolicyLob = excludeLobs();
        List<BilPolicyTerm> bilPolicyTermList = readDownPaymentPolicies(bilCashDsp, excludePolicyLob);
        if (!bilPolicyTermList.isEmpty()) {
            isDownPaymentRowsExist = readPolicyScheduleData(bilPolicyTermList, bilCashDsp, quoteIndicator,
                    cashApplyDate, userSequenceId);
        }
        return isDownPaymentRowsExist;
    }

    private boolean readPolicyScheduleData(List<BilPolicyTerm> bilPolicyTermList, BilCashDsp bilCashDsp,
            String quoteIndicator, ZonedDateTime cashApplyDate, String userSequenceId) {

        Map<BilPolicyTerm, DownPaymentInformation> policyTermMap = new HashMap<>();
        double totalBalance = DECIMAL_ZERO;

        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
            ZonedDateTime minIstDueDate = bilIstScheduleRepository.getMinnimumIstDueDt(
                    bilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            if (minIstDueDate != null) {
                List<BilIstSchedule> bilIstScheduleList = bilIstScheduleRepository.getScheduleRowsByDueDate(
                        bilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), minIstDueDate);
                if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
                    totalBalance = processScheduleData(bilIstScheduleList, policyTermMap, bilPolicyTerm, totalBalance);
                }
            }
        }

        if (totalBalance > DECIMAL_ZERO) {
            boolean isinvoicePaid = false;
            BilCashReceipt bilCashReceipt = bilCashReceiptRepository
                    .findById(new BilCashReceiptId(bilCashDsp.getBilCashDspId().getAccountId(),
                            bilCashDsp.getBilCashDspId().getBilDtbDate(),
                            bilCashDsp.getBilCashDspId().getDtbSequenceNbr()))
                    .orElse(null);
            BilAccount bilAccount = bilAccountRepository.findById(bilCashDsp.getBilCashDspId().getAccountId())
                    .orElse(null);
            if (bilAccount == null) {
                throw new DataNotFoundException("no.data.found");
            }

            getApplyDownPayment(totalBalance, policyTermMap, bilCashDsp);
            Object[] downPaymentData = applyPaymentToEachPolicy(policyTermMap, bilCashDsp, bilCashReceipt, bilAccount,
                    quoteIndicator, cashApplyDate, isinvoicePaid, userSequenceId);
            boolean isDownPaymentRowsExist = (boolean) downPaymentData[SHORT_ZERO];
            isinvoicePaid = (boolean) downPaymentData[SHORT_ONE];
            if (isinvoicePaid) {
                checkPendingTape(bilAccount, cashApplyDate);
            }
            return isDownPaymentRowsExist;
        } else {
            return true;
        }
    }

    private Object[] applyPaymentToEachPolicy(Map<BilPolicyTerm, DownPaymentInformation> policyTermMap,
            BilCashDsp bilCashDsp, BilCashReceipt bilCashReceipt, BilAccount bilAccount, String quoteIndicator,
            ZonedDateTime cashApplyDate, boolean isinvoicePaid, String userSequenceId) {

        double workBcdAmt = bilCashDsp.getDspAmount();
        Short maxSequence = null;
        String previousPolicyId = BLANK_STRING;
        String policyNumber = BLANK_STRING;
        String policySymbol = BLANK_STRING;
        boolean isDownPaymentRowsExist = false;

        for (Map.Entry<BilPolicyTerm, DownPaymentInformation> policyTerm : policyTermMap.entrySet()) {
            BilPolicyTerm bilPolicyTerm = policyTerm.getKey();
            DownPaymentInformation downPaymentInformation = policyTerm.getValue();

            if (!previousPolicyId.equals(bilPolicyTerm.getBillPolicyTermId().getPolicyId())) {
                BilPolicy bilPolicy = bilPolicyRepository
                        .findById(new BilPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                bilPolicyTerm.getBillPolicyTermId().getBilAccountId()))
                        .orElse(null);
                if (bilPolicy == null) {
                    throw new DataNotFoundException("no.data.found");
                }
                policyNumber = bilPolicy.getPolNbr();
                policySymbol = bilPolicy.getPolSymbolCd();
                previousPolicyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
            }

            Object[] scheduleData = applyPaymentsToScheduleRows(downPaymentInformation, bilCashDsp, maxSequence,
                    workBcdAmt, policyNumber, policySymbol, bilCashReceipt, bilAccount, bilPolicyTerm, quoteIndicator,
                    cashApplyDate, isinvoicePaid, userSequenceId);
            bilPolicyTerm.setBptWroPrcInd(BLANK_CHAR);
            bilPolicyTermRepository.saveAndFlush(bilPolicyTerm);
            maxSequence = (Short) scheduleData[SHORT_ZERO];
            workBcdAmt = (double) scheduleData[SHORT_ONE];
            isinvoicePaid = (boolean) scheduleData[2];
        }

        if (workBcdAmt > DECIMAL_ZERO) {
            bilCashDsp.setDspAmount(workBcdAmt);
            bilCashDspRepository.saveAndFlush(bilCashDsp);
            isDownPaymentRowsExist = true;
        } else {
            bilCashDspRepository.delete(bilCashDsp);
            bilCashDspRepository.flush();
            isDownPaymentRowsExist = false;
        }
        return new Object[] { isDownPaymentRowsExist, isinvoicePaid };

    }

    private void getApplyDownPayment(double totalBalance, Map<BilPolicyTerm, DownPaymentInformation> policyTermMap,
            BilCashDsp bilCashDsp) {

        double remainingApplyAmount = bilCashDsp.getDspAmount();
        int count = SHORT_ONE;

        for (DownPaymentInformation downPaymentInformation : policyTermMap.values()) {
            if (count == SHORT_ONE && count == policyTermMap.size()) {
                if (bilCashDsp.getDspAmount() >= downPaymentInformation.getRemainingDownPayment()) {
                    downPaymentInformation.setApplyDownPaymentAmount(downPaymentInformation.getRemainingDownPayment());
                } else {
                    downPaymentInformation.setApplyDownPaymentAmount(bilCashDsp.getDspAmount());
                }
            } else {
                if (bilCashDsp.getDspAmount() >= totalBalance) {
                    downPaymentInformation.setApplyDownPaymentAmount(downPaymentInformation.getRemainingDownPayment());
                } else {
                    if (count == policyTermMap.size()) {
                        downPaymentInformation.setApplyDownPaymentAmount(remainingApplyAmount);
                    } else {
                        double quotient = downPaymentInformation.getRemainingDownPayment() / totalBalance;
                        double product = quotient * bilCashDsp.getDspAmount();
                        int roundSepRec = (int) product;
                        double fraction = product - roundSepRec;
                        BigDecimal fourDecimalFraction = BigDecimal.valueOf(fraction).setScale(4,
                                RoundingMode.HALF_EVEN);
                        if (fourDecimalFraction.doubleValue() > DECIMAL_ZERO) {
                            fraction = fourDecimalFraction.doubleValue() + 0.01;
                            BigDecimal twoDecimalFraction = BigDecimal.valueOf(fraction).setScale(2,
                                    RoundingMode.HALF_EVEN);
                            product = roundSepRec + twoDecimalFraction.doubleValue();
                        }
                        downPaymentInformation.setApplyDownPaymentAmount(product);
                        remainingApplyAmount = remainingApplyAmount - product;
                        count++;
                    }
                }
            }
        }

    }

    private Object[] applyPaymentsToScheduleRows(DownPaymentInformation downPaymentInformation, BilCashDsp bilCashDsp,
            Short maxSequence, double workBcdAmt, String policyNumber, String policySymbol,
            BilCashReceipt bilCashReceipt, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, String quoteIndicator,
            ZonedDateTime cashApplyDate, boolean isinvoicePaid, String userSequenceId) {

        for (Map.Entry<BilIstSchedule, Double> scheduleData : downPaymentInformation.getScheduleData().entrySet()) {
            BilIstSchedule bilIstSchedule = scheduleData.getKey();
            double balanceDownpayment = scheduleData.getValue();
            double downPayment;

            if (balanceDownpayment > downPaymentInformation.getApplyDownPaymentAmount()) {
                downPayment = downPaymentInformation.getApplyDownPaymentAmount();
                downPaymentInformation.setApplyDownPaymentAmount(DECIMAL_ZERO);
            } else {
                downPayment = balanceDownpayment;
                downPaymentInformation.setApplyDownPaymentAmount(
                        downPaymentInformation.getApplyDownPaymentAmount() - balanceDownpayment);
            }

            workBcdAmt = workBcdAmt - downPayment;
            bilIstSchedule.setBisPrmPaidAmt(bilIstSchedule.getBisPrmPaidAmt() + downPayment);

            bilIstScheduleRepository.saveAndFlush(bilIstSchedule);
            maxSequence = insertCash(bilCashDsp, bilIstSchedule, policyNumber, policySymbol, downPayment, maxSequence);
            postEventRecord(bilPolicyTerm, bilCashDsp, bilCashReceipt, bilAccount, bilIstSchedule, policyNumber,
                    policySymbol, cashApplyDate, userSequenceId);
            // Call CCMOBCM
            if (!quoteIndicator.equals(String.valueOf(CHAR_Q))) {
                String databaseKey = getBilDatabaseKey(bilCashReceipt.getEntryDate(), bilCashReceipt.getEntryNumber(),
                        bilCashReceipt.getUserId(), bilCashReceipt.getEntrySequenceNumber());
                callFinancialApi(downPayment, downPayment, cashApplyDate, bilCashReceipt.getBankCd(),
                        bilAccount.getAccountNumber(), ObjectCode.CASH.toString(), ActionCode.APPLIED.toString(),
                        bilIstSchedule.getBillPblItemCd(), String.valueOf(bilCashReceipt.getCashEntryMethodCd()),
                        bilCashReceipt.getReceiptTypeCd(), BLANK_STRING, bilAccount.getBillTypeCd(),
                        bilAccount.getBillClassCd(), bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                        databaseKey, BLANK_STRING,
                        DateRoutine.dateTimeAsYYYYMMDDString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()),
                        BCMCODP, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilPolicyTerm.getBillCountryCd(),
                        bilPolicyTerm.getLobCd(), bilPolicyTerm.getMasterCompanyNbr(),
                        bilPolicyTerm.getBillStatePvnCd(), bilAccount.getCurrencyCode());

                String actionCode;
                if (bilIstSchedule.getBillInvoiceCd() == InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()
                        || bilIstSchedule
                                .getBillInvoiceCd() == InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE
                                        .getValue()) {
                    actionCode = ActionCode.APPLY_TO_INSTALL.toString();
                    isinvoicePaid = true;
                } else {
                    actionCode = ActionCode.APPLY_TO_UNBILLED.toString();
                }
                double netAmount = downPayment - bilIstSchedule.getBillCommissionPct() * downPayment / 100;
                int roundSepRec = (int) netAmount;
                double fraction = netAmount - roundSepRec;
                BigDecimal twoDecimalFraction = BigDecimal.valueOf(fraction).setScale(2, RoundingMode.HALF_EVEN);
                netAmount = roundSepRec + twoDecimalFraction.doubleValue();
                callFinancialApi(downPayment, netAmount, cashApplyDate, bilCashReceipt.getBankCd(),
                        bilAccount.getAccountNumber(), ObjectCode.PREMIUM.toString(), actionCode,
                        bilIstSchedule.getBillPblItemCd(), String.valueOf(bilCashReceipt.getCashEntryMethodCd()),
                        bilCashReceipt.getReceiptTypeCd(), BLANK_STRING, bilAccount.getBillTypeCd(),
                        bilAccount.getBillClassCd(), bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                        databaseKey, BLANK_STRING,
                        DateRoutine.dateTimeAsYYYYMMDDString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()),
                        BCMCODP, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilPolicyTerm.getBillCountryCd(),
                        bilPolicyTerm.getLobCd(), bilPolicyTerm.getMasterCompanyNbr(),
                        bilPolicyTerm.getBillStatePvnCd(), bilAccount.getCurrencyCode());
            }
            maxSequence++;
        }
        return new Object[] { maxSequence, workBcdAmt, isinvoicePaid };
    }

    private void postEventRecord(BilPolicyTerm bilPolicyTermRow, BilCashDsp bilCashDspRow,
            BilCashReceipt bilCashReceiptRow, BilAccount bilAccountRow, BilIstSchedule bilIstSchedule,
            String policyNumber, String policySymbol, ZonedDateTime cashApplyDate, String userSequenceId) {
        final String postEventBusObjCash = "BIL-POST-EVENT-CASH-";
        final String invokeProgram = "BCMOBE01";
        final String requestProgram = BCMCODP;
        Event event = new Event();
        event.setIssueSystemCode(
                bilPolicyTermRow.getBptIssueSysId().isEmpty() ? BillingConstants.EventIssueSystemCode.UWS.getValue()
                        : bilPolicyTermRow.getBptIssueSysId());
        String postEventBusObjName = postEventBusObjCash.concat(event.getIssueSystemCode());
        event.setEventCode(BillingConstants.EventCode.EVENT_PREM_PYMT.getValue());
        event.setPolicyId(bilIstSchedule.getBillIstScheduleId().getPolicyId());
        event.setPolicyEffectiveDate(bilPolicyTermRow.getBillPolicyTermId().getPolEffectiveDt());
        event.setPlannedExpirationDate(bilPolicyTermRow.getPlnExpDt());
        event.setPolicySymbolCode(policySymbol.trim());
        event.setPolicyNumber(policyNumber.trim());
        event.setLineOfBusinessCode(bilPolicyTermRow.getLobCd());
        event.setMasterCompanyNumber(bilPolicyTermRow.getMasterCompanyNbr());
        event.setStateProvinceCode(bilPolicyTermRow.getBillStatePvnCd());
        event.setCountryCode(bilPolicyTermRow.getBillCountryCd());
        event.setPolicyStatusCode(String.valueOf(bilPolicyTermRow.getBillPolStatusCd()));
        event.setBilPlanCode(bilPolicyTermRow.getBillPlanCd());
        event.setBilAccountId(bilCashDspRow.getBilCashDspId().getAccountId());
        event.setBilAccountNumber(bilAccountRow.getAccountNumber());
        event.setBilTypeCode(bilAccountRow.getBillTypeCd());
        event.setBilClassCode(bilAccountRow.getBillClassCd());
        event.setCollectionMethod(bilAccountRow.getCollectionMethod());
        event.setCollectionPlan(bilAccountRow.getCollectionPlan());
        event.setBilThirdPartyNumber(bilAccountRow.getThirdPartyIdentifier());
        event.setBilAdditionalId(bilAccountRow.getAdditionalId());
        event.setRequestProgramName(requestProgram);
        event.setPaymentInstallmentDueDate(bilIstSchedule.getBillIstDueDt());
        event.setPaymentDepositTypeCode(BilDspTypeCode.APPLIED.getValue());
        event.setPaymentReceiveCode(bilIstSchedule.getBillPblItemCd());
        event.setPaymentDepositAmount(bilCashDspRow.getDspAmount());
        event.setPaymentEntryDate(bilCashReceiptRow.getEntryDate());
        event.setPaymentLoanDate(DateRoutine.defaultDateTime());
        if (!bilIstSchedule.getBillPblItemCd().isEmpty()) {
            BilAmtTrmRlt bilAmtTrmRlt = bilAmtTrmRltRepository.getLoanInceptionDate(
                    bilCashDspRow.getBilCashDspId().getAccountId(), bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                    bilIstSchedule.getBillPblItemCd(), bilIstSchedule.getBillIstScheduleId().getBillSeqNbr());
            if (bilAmtTrmRlt != null) {
                event.setPaymentLoanDate(bilAmtTrmRlt.getRltGnrc01Dt());
            }
        }
        event.setReceivableCode(bilCashDspRow.getPayableItemCd());
        event.setReceiptTypeCode(bilCashReceiptRow.getReceiptTypeCd());
        event.setBilDepositDate(bilCashDspRow.getDspDate());
        event.setBussinessObjectName(postEventBusObjName);
        event.setEventKeyId(event.getBilAccountId());
        event.setEventKeyTypeCode(BillingConstants.EventKeyTypeCode.BILLACCOUNT.getValue());
        event.setEventStatusCode(CHAR_P);
        event.setEffectiveDate(cashApplyDate);
        event.setUserSequenceId(userSequenceId);
        event.setEventMduNm(requestProgram);

        HalBoMduXrf halBoMduXrf = halBoMduXrfRepository.findById(postEventBusObjName).orElse(null);
        if (halBoMduXrf != null) {
            String programName = halBoMduXrf.getHbmxBobjMduNm().trim();
            if (programName.isEmpty()) {
                logger.error("Program name is not found in HAL_BO_MDU_XRF. {}", postEventBusObjName);
            } else if (programName.equals(invokeProgram)) {
                eventPostingService.postHalEventDtl(event);
            }
        } else {
            logger.error("Program name is not found in HAL_BO_MDU_XRF. {}", postEventBusObjName);
        }
    }

    private double processScheduleData(List<BilIstSchedule> bilIstScheduleList,
            Map<BilPolicyTerm, DownPaymentInformation> policyTermMap, BilPolicyTerm bilPolicyTerm,
            double totalBalance) {

        for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
            double downPaymentAmount = getDownPaymentAmount(bilIstSchedule, bilPolicyTerm);
            double downPaymentCreditAmount = getDownPaymentCreditAmount(bilIstSchedule, bilPolicyTerm);
            double scheduleCreditAmount = bilIstSchedule.getBisCreFutAmt() + bilIstSchedule.getBisCreOpnAmt()
                    + bilIstSchedule.getBisCncFutAmt() + bilIstSchedule.getBisCncOpnAmt();
            double creditDifference = scheduleCreditAmount - downPaymentCreditAmount;
            double balanceDownPayment = downPaymentAmount + creditDifference - bilIstSchedule.getBisPrmPaidAmt()
                    - bilIstSchedule.getBisCrePaidAmt() - bilIstSchedule.getBisWroPrmAmt();
            if (balanceDownPayment > DECIMAL_ZERO) {
                storePolicyScheduleData(bilIstSchedule, balanceDownPayment, bilPolicyTerm, policyTermMap);
                totalBalance = totalBalance + balanceDownPayment;
            }
        }
        return totalBalance;

    }

    private void storePolicyScheduleData(BilIstSchedule bilIstSchedule, double balanceDownPayment,
            BilPolicyTerm bilPolicyTerm, Map<BilPolicyTerm, DownPaymentInformation> policyTermMap) {

        DownPaymentInformation downPaymentInformation = null;
        if (policyTermMap.containsKey(bilPolicyTerm)) {
            downPaymentInformation = policyTermMap.get(bilPolicyTerm);
            downPaymentInformation
                    .setRemainingDownPayment(downPaymentInformation.getRemainingDownPayment() + balanceDownPayment);
        } else {
            downPaymentInformation = new DownPaymentInformation();
            downPaymentInformation.setRemainingDownPayment(balanceDownPayment);
        }

        storeScheduleData(bilIstSchedule, balanceDownPayment, downPaymentInformation);
        policyTermMap.put(bilPolicyTerm, downPaymentInformation);

    }

    private void storeScheduleData(BilIstSchedule bilIstSchedule, double balanceDownPayment,
            DownPaymentInformation downPaymentInformation) {

        Map<BilIstSchedule, Double> scheduleMap = null;

        if (downPaymentInformation.getScheduleData() == null || downPaymentInformation.getScheduleData().isEmpty()) {
            scheduleMap = new HashMap<>();
        } else {
            scheduleMap = downPaymentInformation.getScheduleData();
        }
        scheduleMap.put(bilIstSchedule, balanceDownPayment);
        downPaymentInformation.setScheduleData(scheduleMap);

    }

    private double getDownPaymentCreditAmount(BilIstSchedule bilIstSchedule, BilPolicyTerm bilPolicyTerm) {

        Double amount = bilAmtSchRltRepository.getDownPaymentCreditAmount(
                bilIstSchedule.getBillIstScheduleId().getBillAccountId(),
                bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                Arrays.asList(BilAmountsEftIndicator.NEW_BUSINESS.getValue(), BilAmountsEftIndicator.RENEWAL.getValue(),
                        BilAmountsEftIndicator.CHANGE_AUDIT_FREQUENCY.getValue(),
                        BilAmountsEftIndicator.AMENDED_INTERIM_AUDIT.getValue(),
                        BilAmountsEftIndicator.REVERSE_AUDIT_OPERATOR.getValue(),
                        BilAmountsEftIndicator.NEWBUSINESS.getValue(), BilAmountsEftIndicator.RENEWAL_UNDER.getValue(),
                        BilAmountsEftIndicator.RENEWALS.getValue(), BilAmountsEftIndicator.REISSUE_NEW_TERM.getValue()),
                bilIstSchedule.getBillIstScheduleId().getBillSeqNbr(), bilPolicyTerm.getPlnExpDt(),
                Arrays.asList("AMT", "BAM"), DECIMAL_ZERO);

        if (amount == null) {
            amount = DECIMAL_ZERO;
        }
        return amount;
    }

    private double getDownPaymentAmount(BilIstSchedule bilIstSchedule, BilPolicyTerm bilPolicyTerm) {

        Double amount = bilAmtSchRltRepository.getDownPaymentAmount(
                bilIstSchedule.getBillIstScheduleId().getBillAccountId(),
                bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                Arrays.asList(BilAmountsEftIndicator.NEW_BUSINESS.getValue(), BilAmountsEftIndicator.RENEWAL.getValue(),
                        BilAmountsEftIndicator.CHANGE_AUDIT_FREQUENCY.getValue(),
                        BilAmountsEftIndicator.AMENDED_INTERIM_AUDIT.getValue(),
                        BilAmountsEftIndicator.REVERSE_AUDIT_OPERATOR.getValue(),
                        BilAmountsEftIndicator.NEWBUSINESS.getValue(), BilAmountsEftIndicator.RENEWAL_UNDER.getValue(),
                        BilAmountsEftIndicator.RENEWALS.getValue(), BilAmountsEftIndicator.REISSUE_NEW_TERM.getValue()),
                bilIstSchedule.getBillIstScheduleId().getBillSeqNbr(), bilPolicyTerm.getPlnExpDt(),
                Arrays.asList("AMT", "BAM"));

        if (amount == null) {
            amount = DECIMAL_ZERO;
        }
        return amount;
    }

    private List<BilPolicyTerm> readDownPaymentPolicies(BilCashDsp bilCashDsp, List<String> excludePolicyLob) {

        List<BilPolicyTerm> includepolicyTerms = new ArrayList<>();
        List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.getDownPaymentPolicies(
                bilCashDsp.getBilCashDspId().getAccountId(), CHAR_Y,
                Arrays.asList(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                        BilPolicyStatusCode.TRANSFERRED.getValue(), BilPolicyStatusCode.PENDING_TRANSFER.getValue(),
                        BilPolicyStatusCode.PENDING_TRANSFER_APPLYPAYMENTS.getValue(),
                        BilPolicyStatusCode.PENDING_TRANSFER_HOLDPAYMENTS.getValue(), '4'));
        if (bilPolicyTermList != null && !bilPolicyTermList.isEmpty()) {
            for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
                if (excludePolicyLob.contains(bilPolicyTerm.getLobCd())) {
                    continue;
                } else {
                    includepolicyTerms.add(bilPolicyTerm);
                }
            }
        }
        return includepolicyTerms;
    }

    private List<String> excludeLobs() {
        List<String> excludeLob = new ArrayList<>();
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow("DPCA", SEPARATOR_BLANK, SEPARATOR_BLANK,
                SEPARATOR_BLANK, dateService.currentDate(), dateService.currentDate());
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            int length = bilRulesUct.getBrtParmListTxt().trim().length();
            int maxLength = 0;
            if (length > 96) {
                maxLength = 96;
            } else {
                maxLength = length;
            }
            if (maxLength > 3 && !bilRulesUct.getBrtParmListTxt().trim().substring(3, maxLength).trim().isEmpty()) {
                String parmListTxt = bilRulesUct.getBrtParmListTxt().substring(3, maxLength).trim();
                parmListTxt = parmListTxt.replaceAll("(.{3})", "$1|");
                String[] lineOfBusiness = parmListTxt.split("\\|");
                if (lineOfBusiness.length > 0) {
                    excludeLob.addAll(Arrays.asList(lineOfBusiness));
                }
            }
        }
        return excludeLob;
    }

    private Short insertCash(BilCashDsp bilCashDsp, BilIstSchedule bilIstSchedule, String policyNumber,
            String policySymbol, double dispositionAmount, Short maxSequence) {

        BilCashDsp bilCashDspSave = new BilCashDsp();
        if (maxSequence == null) {
            maxSequence = getMaxDispositionNumber(bilCashDsp);
        }
        BilCashDspId bilCashDspId = new BilCashDspId(bilCashDsp.getBilCashDspId().getAccountId(),
                bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr(),
                maxSequence);
        bilCashDspSave.setPolicyId(bilIstSchedule.getBillIstScheduleId().getPolicyId());
        bilCashDspSave.setPolicyNumber(policyNumber);
        bilCashDspSave.setPolicySymbolCd(policySymbol);
        bilCashDspSave.setPolicyEffectiveDate(bilIstSchedule.getPolicyEffectiveDt());
        bilCashDspSave.setBilSequenceNumber(bilIstSchedule.getBillIstScheduleId().getBillSeqNbr());
        bilCashDspSave.setSystemDueDate(bilIstSchedule.getBillSysDueDt());
        bilCashDspSave.setAdjustmentDueDate(bilIstSchedule.getBillAdjDueDt());
        bilCashDspSave.setInvoiceDate(bilIstSchedule.getBillInvDt());
        bilCashDspSave.setChargeTypeCd(BLANK_CHAR);
        bilCashDspSave.setUserId(bilCashDsp.getUserId());
        bilCashDspSave.setDspDate(bilCashDsp.getDspDate());
        bilCashDspSave.setDspTypeCd(BilDspTypeCode.APPLIED.getValue());
        bilCashDspSave.setDspAmount(dispositionAmount);
        bilCashDspSave.setPayeeClientId(BLANK_STRING);
        bilCashDspSave.setPayeeAddressSequenceNumber(SHORT_ZERO);
        bilCashDspSave.setManualSuspenseIndicator(BLANK_CHAR);
        bilCashDspSave.setDraftNbr(SHORT_ZERO);
        bilCashDspSave.setCreditPolicyId(BLANK_STRING);
        bilCashDspSave.setCreditAmountType(BLANK_STRING);
        bilCashDspSave.setCreditIndicator(BLANK_CHAR);
        bilCashDspSave.setRevsRsusIndicator(BLANK_CHAR);
        bilCashDspSave.setToFromTransferNbr(BLANK_STRING);
        bilCashDspSave.setTransferTypeCd(BLANK_CHAR);
        bilCashDspSave.setDisbursementDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setStatusCd(BLANK_CHAR);
        bilCashDspSave.setDisbursementId(BLANK_STRING);
        bilCashDspSave.setCheckProdMethodCd(BLANK_CHAR);
        bilCashDspSave.setStatementDetailTypeCd(BLANK_STRING);
        bilCashDspSave.setBilCashDspId(bilCashDspId);
        bilCashDspSave.setPayableItemCd(bilIstSchedule.getBillPblItemCd());
        bilCashDspRepository.saveAndFlush(bilCashDspSave);

        return maxSequence;
    }

    private Short getMaxDispositionNumber(BilCashDsp bilCashDsp) {
        Short maxSequence = bilCashDspRepository.findMaxDispositionNumber(bilCashDsp.getBilCashDspId().getAccountId(),
                bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr());
        if (maxSequence == null) {
            maxSequence = 0;
        } else {
            maxSequence = (short) (maxSequence + 1);
        }
        return maxSequence;
    }

    private void callFinancialApi(Double amount, Double netAmount, ZonedDateTime applicationDate, String bankCode,
            String accountNumber, String transactionObjectCode, String transactionActionCode, String paybleItemCode,
            String sourceCode, String paymentType, String bilReasonCode, String bilTypeCode, String bilClassCode,
            ZonedDateTime referenceDate, String databaseKey, String operatorId, String orginalEffectiveDate,
            String applicationProgram, String policyId, String countryCode, String lineOfBusiness, String masterCompany,
            String riskState, String currencyCode) {

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(BLANK_STRING);
        financialApiActivity.setApplicationName(ApplicationName.BCMOCA.toString());
        financialApiActivity.setUserId(BLANK_STRING);
        financialApiActivity.setErrorCode(ERROR_CODE);

        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setMasterCompanyNbr(masterCompany);
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(netAmount);
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setAgentTtyId(BLANK_STRING);
        financialApiActivity.setAppProgramId(applicationProgram);
        financialApiActivity.setBilAccountId(accountNumber);
        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setBilClassCode(bilClassCode);
        financialApiActivity.setBilDatabaseKey(databaseKey);
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilReceiptTypeCode(paymentType);
        financialApiActivity.setBilReasonCode(bilReasonCode);
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(referenceDate));
        financialApiActivity.setBilSourceCode(sourceCode);
        financialApiActivity.setBilTypeCode(bilTypeCode);
        financialApiActivity.setClientId(BLANK_STRING);
        financialApiActivity.setCountryCode(countryCode);
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setCurrencyCode(currencyCode);
        financialApiActivity.setDepositeBankCode(BLANK_STRING);
        financialApiActivity.setDisbursementId(BLANK_STRING);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setLineOfBusCode(lineOfBusiness);
        financialApiActivity.setMarketSectorCode(BLANK_STRING);
        financialApiActivity.setOriginalEffectiveDate(orginalEffectiveDate);
        financialApiActivity.setOperatorId(operatorId);
        financialApiActivity.setPayableItemCode(paybleItemCode);
        financialApiActivity.setPolicyId(policyId);
        financialApiActivity.setStateCode(riskState);
        financialApiActivity.setStatusCode(BLANK_CHAR);

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {

            throw new InvalidDataException(fwsReturnMessage[1]);
        }
    }

    private String getBilDatabaseKey(ZonedDateTime entryDate, String entryNumber, String operatorId,
            short bilDtbSequenceNumber) {
        String seqNumberSign = "+";

        String dataString = DateRoutine.dateTimeAsYYYYMMDDString(entryDate);
        dataString += entryNumber;
        dataString += StringUtils.rightPad(operatorId, 8, BLANK_CHAR);
        dataString += seqNumberSign;
        dataString += StringUtils.leftPad(String.valueOf(bilDtbSequenceNumber), 5, CHAR_ZERO);

        return dataString;
    }

    private void checkPendingTape(BilAccount bilAccount, ZonedDateTime cashApplyDate) {
        final String EFT_COLLECTION_METHOD = "CET";
        final char ACTIVE_STATUS = CHAR_A;
        boolean rowExists = fetchDesReasonCode(bilAccount.getCollectionMethod(), EFT_COLLECTION_METHOD);
        if (rowExists) {
            List<BilEftBank> bilEftBankList = bilEftBankRepository.findBilEftBankEntry(bilAccount.getAccountId(),
                    CHAR_A, cashApplyDate, ACTIVE_STATUS, PageRequest.of(SHORT_ZERO, SHORT_ONE));
            if (bilEftBankList != null && !bilEftBankList.isEmpty()) {
                ZonedDateTime minEftTapeDate = bilEftPendingTapeRepository.getMinEftTapeDate(bilAccount.getAccountId(),
                        CHAR_A, EftRecordType.ORIGINAL.getValue(), BLANK_STRING);
                if (minEftTapeDate != null && minEftTapeDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
                    BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("EADJ", bilAccount.getBillTypeCd(),
                            bilAccount.getBillClassCd(), BLANK_STRING);
                    if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                        insertBilActInquiry(bilAccount.getAccountId(), minEftTapeDate);
                    }
                }
            }
        }
    }

    private boolean fetchDesReasonCode(String busCd, String busType) {
        return busCdTranslationRepository
                .existsById(new BusCdTranslationId(busCd, busType, language, "CETCD"));
    }

    private void insertBilActInquiry(String accountId, ZonedDateTime minEftTapeDate) {
        BilActInquiry bilActInquiry = new BilActInquiry();
        BilActInquiryId bilActInquiryId = new BilActInquiryId();
        bilActInquiryId.setBilAccountId(accountId);
        bilActInquiryId.setBilAcyTypeCd("EPA");
        bilActInquiry.setBilActInquiryId(bilActInquiryId);
        bilActInquiry.setBilNxtAcyDt(minEftTapeDate);
        bilActInquiryRepository.save(bilActInquiry);
    }

}
