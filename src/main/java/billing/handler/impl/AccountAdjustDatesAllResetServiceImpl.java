package billing.handler.impl;

import static billing.utils.BillingConstants.SYSTEM_USER_ID;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_F;
import static core.utils.CommonConstants.CHAR_H;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActInquiry;
import billing.data.entity.BilActRules;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilAmounts;
import billing.data.entity.BilCrgAmounts;
import billing.data.entity.BilDates;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilEftPlnCrg;
import billing.data.entity.BilIstSchedule;
import billing.data.entity.BilPolActivity;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSeasExcl;
import billing.data.entity.BilSptPlnCrg;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilDatesId;
import billing.data.entity.id.BilPolActivityId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilAmountsRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCreScheduleRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilDatesRepository;
import billing.data.repository.BilEftBankRepository;
import billing.data.repository.BilEftPlnCrgRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilSeasExclRepository;
import billing.data.repository.BilSptPlnCrgRepository;
import billing.handler.model.AccountAdjustDates;
import billing.handler.model.AccountAdjustSupportData;
import billing.handler.model.AdjustDatesReviewDetails;
import billing.handler.model.ChargeVariables;
import billing.handler.model.DownPaymentFee;
import billing.handler.model.FirstPossibleDueDate;
import billing.handler.model.VariableSeviceCharge;
import billing.handler.model.VariableSeviceChargeOverride;
import billing.handler.service.AccountAdjustDatesService;
import billing.handler.service.AllDatesResetService;
import billing.handler.service.FirstPossibleDueDateService;
import billing.service.BilRulesUctService;
import billing.utils.AdjustDueDatesConstants.ScheduleCode;
import billing.utils.BillingConstants.AccountTypeCode;
import billing.utils.BillingConstants.ActivityType;
import billing.utils.BillingConstants.AmountProcessIndicator;
import billing.utils.BillingConstants.BilAmountsEftIndicator;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.ChargeType;
import billing.utils.BillingConstants.ChargeTypeCodes;
import billing.utils.BillingConstants.InvoiceTypeCode;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.utils.DateRoutine;

@Service
public class AccountAdjustDatesAllResetServiceImpl implements AllDatesResetService {

    private static final Logger logger = LoggerFactory.getLogger(AccountAdjustDatesAllResetServiceImpl.class);

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BilDatesRepository bilDatesRepository;

    @Autowired
    private BilCreScheduleRepository bilCreScheduleRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private BilAmountsRepository bilAmountsRepository;

    @Autowired
    private BilSeasExclRepository bilSeasExclRepository;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private BilSptPlnCrgRepository bilSptPlnCrgRepository;

    @Autowired
    private BilEftPlnCrgRepository bilEftPlnCrgRepository;

    @Autowired
    private AccountAdjustDatesService accountAdjustDatesService;

    @Autowired
    private FirstPossibleDueDateService firstPossibleDueDateService;
    
    @Autowired
    private DateService dateService;

    private static final List<Character> FUTURE_INVOICE_CODES = Arrays.asList(BLANK_CHAR,
            InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue());

    private static final String RULE_XHOL = "XHOL";
    private static final char ACTIVE = CHAR_Y;

    private static final char PLUS_SIGN = '+';
    private static final char MINUS_SIGN = '-';

    @Override
    @Transactional
    public void processAllDatesReset(AccountAdjustDates accountAdjustDates) {

        AdjustDatesReviewDetails adjustDatesReviewDetails = accountAdjustDatesService
                .processAdjustDates(accountAdjustDates);

        BilActRules bilActRules = adjustDatesReviewDetails.getBilActRules();
        BilAccount bilAccount = adjustDatesReviewDetails.getBilAccount();
        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();

        char fpddIndicator = adjustDatesReviewDetails.getFpddIndicator();
        boolean isEftCollectionMethod = adjustDatesReviewDetails.isEftCollectionMethod();
        boolean isManualReinvoice = adjustDatesReviewDetails.isManualReinvoice();
        boolean disableFpdd = false;
        // need to add all sevice classes starts
        VariableSeviceCharge variableSeviceCharge = new VariableSeviceCharge();
        ChargeVariables chargeVariables = new ChargeVariables();
        VariableSeviceChargeOverride variableSeviceChargeOverride = new VariableSeviceChargeOverride();
        AccountAdjustSupportData accountAdjustSupportData = new AccountAdjustSupportData();

        if (bilAccount.getBillTypeCd().equals("SP")) {
            variableSeviceChargeOverride = accountAdjustDatesService.readOverrideServiceCharge(bilAccount);
            if (variableSeviceChargeOverride == null) {
                variableSeviceChargeOverride = new VariableSeviceChargeOverride();
            }
            if (accountAdjustDates.getAddDateIndicator() != CHAR_Y && accountAdjustDates.getAddDateIndicator() != CHAR_Q
                    && accountAdjustDates.getAddDateIndicator() != CHAR_S
                    && accountAdjustDates.getAddDateIndicator() != CHAR_F
                    && variableSeviceChargeOverride.getRuleInd() != CHAR_Y) {
                Integer count = bilSptPlnCrgRepository.getCount(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                        SHORT_ZERO, CHAR_Y, "GA");
                if (count != null && count > SHORT_ZERO) {
                    accountAdjustSupportData.setServiceChargeIndicator(CHAR_Y);
                    accountAdjustSupportData.setServiceChargeTypeCode("GA");
                } else {
                    accountAdjustSupportData.setServiceChargeIndicator(BLANK_CHAR);
                    accountAdjustSupportData.setServiceChargeTypeCode(BLANK_STRING);
                }
            }
        }

        List<BilEftPlnCrg> bilEftPlanChargeList = null;
        if (variableSeviceChargeOverride.getRuleInd() != CHAR_Y && adjustDatesReviewDetails.isEftCollectionMethod()) {
            bilEftPlanChargeList = bilEftPlnCrgRepository.findByCollectionPlan(bilAccount.getCollectionPlan());
        }

        List<BilCrgAmounts> downPaymentList = null;
        DownPaymentFee downPaymentFee = new DownPaymentFee();
        if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y
                && (adjustDatesReviewDetails.isDpifRule() || accountAdjustDates.getSupportPlanIndicator() == CHAR_Y)) {
            downPaymentList = accountAdjustDatesService.getFutureDownpaymentRows(accountAdjustDates.getAccountId(),
                    downPaymentFee);
        }

        List<BilSptPlnCrg> bilSupportPlanChargeList = bilSptPlnCrgRepository
                .findByBilTypeCodeClassCode(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd());
        logger.debug("get bill support plan charges for bilTypeCode:{} and bilClassCode:{}", bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd());

        if (bilSupportPlanChargeList.size() == SHORT_ONE) {
            BilSptPlnCrg bilSptPlnCrg = bilSupportPlanChargeList.get(SHORT_ZERO);
            accountAdjustSupportData.setServiceChargeIndicator(bilSptPlnCrg.getBilSptScgInd());
            accountAdjustSupportData.setServiceChargeTypeCode(bilSptPlnCrg.getBilScgTypeCd());
            accountAdjustSupportData.setServiceChargeCode(bilSptPlnCrg.getBilScgActionCd());
            accountAdjustSupportData.setServiceChargeAmount(bilSptPlnCrg.getBilSvcCrgAmt());
            accountAdjustSupportData.setPremiumAmount(bilSptPlnCrg.getBilPrmAmt());
            accountAdjustSupportData.setSeviceChargeIncAmount(bilSptPlnCrg.getBilScgIcrAmt());
            accountAdjustSupportData.setPremiumIncAmount(bilSptPlnCrg.getBilPrmIcrAmt());
            accountAdjustSupportData.setMaxServiceChargeAmount(bilSptPlnCrg.getBilMaxScgAmt());
        } else {
            accountAdjustSupportData.setServiceChargeIndicator(bilSupportPlan.getBspScgChgInd());
            accountAdjustSupportData.setServiceChargeTypeCode(bilSupportPlan.getBspScgchgTypCd());
            accountAdjustSupportData.setServiceChargeCode(bilSupportPlan.getBspSvcCd());
            accountAdjustSupportData.setServiceChargeAmount(bilSupportPlan.getBspScgAmt());
            accountAdjustSupportData.setPremiumAmount(bilSupportPlan.getBspPrmAmt());
            accountAdjustSupportData.setSeviceChargeIncAmount(bilSupportPlan.getBspScgIncAmt());
            accountAdjustSupportData.setPremiumIncAmount(bilSupportPlan.getBspPrmIncAmt());
            accountAdjustSupportData.setMaxServiceChargeAmount(bilSupportPlan.getBspMaxScgAmt());
        }

        boolean isXHOLRule = false;
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(RULE_XHOL, BLANK_STRING, BLANK_STRING,
                BLANK_STRING, accountAdjustDates.getProcessDate(), accountAdjustDates.getProcessDate());

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == 'Y' && bilRulesUct.getBrtParmListTxt() != null
                && !bilRulesUct.getBrtParmListTxt().trim().isEmpty()) {
            isXHOLRule = true;

        }

        char previousSchedule = ScheduleCode.NO_SCHEDULE.getValue();
        if (fpddIndicator == CHAR_Y) {
            previousSchedule = accountAdjustDatesService.getPreviousSchedule(adjustDatesReviewDetails.isAccountBilled(),
                    accountAdjustDates);
            if (previousSchedule == ScheduleCode.FUTURE_SCHEDULE.getValue()) {
                previousSchedule = ScheduleCode.NO_SCHEDULE.getValue();
            }
        }

        FirstPossibleDueDate firstPossibleDueDate = firstPossibleDueDateService.processFirstPossibleDueDateLogic(
                bilActRules.getBruIncWknInd(), isXHOLRule, bilRulesUct, accountAdjustDates, adjustDatesReviewDetails);

        if (isManualReinvoice) {
            isManualReinvoice = cancelManualReinvoice(accountAdjustDates, DateRoutine.dateTimeAsYYYYMMDD(0));
            adjustDatesReviewDetails.setManualReinvoice(isManualReinvoice);
        }

        dateResetAll(accountAdjustDates.getAccountId());

        processSkipIst(accountAdjustDates.getAccountId(), firstPossibleDueDate.getDate(),
                firstPossibleDueDate.getReferenceDate(), bilSupportPlan.getBilRvwFqyCd());

        Object[] newDatesData = firstPossibleDueDateService.checkDateAdd(true, adjustDatesReviewDetails, bilAccount,
                accountAdjustDates, isXHOLRule, bilRulesUct);

        ZonedDateTime newReferemceDate = (ZonedDateTime) newDatesData[0];
        ZonedDateTime newAdjustDate = (ZonedDateTime) newDatesData[1];
        ZonedDateTime newInvoiceDate = (ZonedDateTime) newDatesData[2];
        ZonedDateTime newSystemDate = (ZonedDateTime) newDatesData[3];
        char dateType = (char) newDatesData[4];

        adjustDatesReviewDetails.setNewAdjustDueDate(newAdjustDate);
        adjustDatesReviewDetails.setNewReferenceDate(newReferemceDate);
        adjustDatesReviewDetails.setNewInvoiceDate(newInvoiceDate);
        adjustDatesReviewDetails.setNewSystemDate(newSystemDate);
        adjustDatesReviewDetails.setDateType(dateType);

        boolean isRpcExist = checkRpcTrigger(accountAdjustDates);

        int cancelCnt = cancelReset(adjustDatesReviewDetails.getReferenceDuration(), bilAccount, accountAdjustDates,
                firstPossibleDueDate, bilSupportPlan, bilActRules, isXHOLRule, bilRulesUct,
                adjustDatesReviewDetails.getDueDateDisplacementNumber(), newDatesData);
        newReferemceDate = (ZonedDateTime) newDatesData[0];
        newAdjustDate = (ZonedDateTime) newDatesData[1];
        newInvoiceDate = (ZonedDateTime) newDatesData[2];
        newSystemDate = (ZonedDateTime) newDatesData[3];
        dateType = (char) newDatesData[4];

        adjustDatesReviewDetails.setNewAdjustDueDate(newAdjustDate);
        adjustDatesReviewDetails.setNewReferenceDate(newReferemceDate);
        adjustDatesReviewDetails.setNewInvoiceDate(newInvoiceDate);
        adjustDatesReviewDetails.setNewSystemDate(newSystemDate);
        adjustDatesReviewDetails.setDateType(dateType);

        Object[] unAssignedObjectData = checkDateBeforeResetAll(fpddIndicator, isEftCollectionMethod, bilAccount);
        logger.debug("Retrieved isUnassignedBisFound, minUninvBisIstDate");
        boolean isUnassignedBisFound = (boolean) unAssignedObjectData[SHORT_ZERO];
        ZonedDateTime minUninvBisIstDate = (ZonedDateTime) unAssignedObjectData[SHORT_ONE];

        if (!isUnassignedBisFound && minUninvBisIstDate.compareTo(DateRoutine.defaultDateTime()) != SHORT_ZERO
                && fpddIndicator != CHAR_Y && !isEftCollectionMethod) {
            disableFpdd = disableFpddCheck(bilAccount, minUninvBisIstDate, bilSupportPlan);
        }

        if (bilActRules.getBruRnlBillInd() == CHAR_Y && bilAccount.getBillTypeCd().compareTo("SP") == 0
                && !isEftCollectionMethod || isRpcExist) {
            testForRenew(adjustDatesReviewDetails, bilSupportPlan, bilActRules.getBruIncWknInd(), bilAccount,
                    isXHOLRule, bilRulesUct, accountAdjustDates, firstPossibleDueDate, isRpcExist,
                    adjustDatesReviewDetails.isFpddOnTimeProcessing());
        }

        // common starts
        accountAdjustDatesService.processDatesAssignment(dateType, bilAccount, accountAdjustDates,
                adjustDatesReviewDetails, firstPossibleDueDate, variableSeviceChargeOverride, variableSeviceCharge,
                chargeVariables, accountAdjustSupportData, disableFpdd, bilEftPlanChargeList, bilSupportPlanChargeList,
                isXHOLRule, bilRulesUct, bilSupportPlan);
        // common ends

        firstPossibleDueDateService.updatePenalty(bilAccount, accountAdjustDates, bilSupportPlan, variableSeviceCharge,
                chargeVariables, cancelCnt, adjustDatesReviewDetails.getNewAdjustDueDate(),
                adjustDatesReviewDetails.getNewInvoiceDate(), variableSeviceChargeOverride, adjustDatesReviewDetails,
                accountAdjustSupportData, bilEftPlanChargeList, bilSupportPlanChargeList, isXHOLRule, bilRulesUct,
                firstPossibleDueDate);

        accountAdjustDatesService.processChargeRows(cancelCnt, previousSchedule, bilAccount, accountAdjustDates,
                firstPossibleDueDate, adjustDatesReviewDetails, isXHOLRule, bilRulesUct, bilEftPlanChargeList,
                bilSupportPlanChargeList, variableSeviceChargeOverride, variableSeviceCharge, accountAdjustSupportData,
                chargeVariables, downPaymentList, downPaymentFee);

    }

    private boolean checkRpcTrigger(AccountAdjustDates accountAdjustDates) {

        List<BilActInquiry> bilActInquiryList = bilActInquiryRepository.getSuspendDisbRow(
                accountAdjustDates.getAccountId(), Arrays.asList("RPC"), accountAdjustDates.getProcessDate());
        return bilActInquiryList != null && !bilActInquiryList.isEmpty();
    }

    private void processSkipIst(String accountId, ZonedDateTime fpddDate, ZonedDateTime trueFpddReferenceDate,
            String bilFrequencyCode) {
        ZonedDateTime resetFpddDate = DateRoutine.calculateDateTimeByFrequency(bilFrequencyCode, fpddDate);
        ZonedDateTime resetFpddReferenceDate = DateRoutine.calculateDateTimeByFrequency(bilFrequencyCode,
                trueFpddReferenceDate);
        List<BilDates> bilDatesList = bilDatesRepository.fetchBilDatesBySetDates(accountId,
                Arrays.asList(InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue(),
                        InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                        InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue()),
                resetFpddReferenceDate, resetFpddDate, resetFpddDate, CHAR_X);
        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            skipIstRow(accountId, bilDatesList);
        }
    }

    private void skipIstRow(String accountId, List<BilDates> bilDatesList) {
        for (BilDates bilDates : bilDatesList) {
            List<BilIstSchedule> bilIstScheduleList = bilIstScheduleRepository.getScheduleByReferenceDate(accountId,
                    bilDates.getBilDatesId().getBilReferenceDt());
            if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
                continue;
            }
            Character crgIndicator = bilCrgAmountsRepository.findInvoiceCode(accountId, bilDates.getBilAdjDueDt(),
                    bilDates.getBilInvDt());
            if (crgIndicator != null && crgIndicator != BLANK_CHAR) {
                continue;
            }
            logger.debug("Remove BilDates row - accountId: {} reference date: {}", accountId,
                    bilDates.getBilDatesId().getBilReferenceDt());
            bilDatesRepository.delete(bilDates);
            bilDatesRepository.flush();
        }
    }

    private void dateResetAll(String accountId) {
        List<ZonedDateTime> bilSysDueDtList = bilDatesRepository.selectSysDueDateByInvoiceCdList(accountId,
                getInvoiceCodes());
        if (bilSysDueDtList != null && !bilSysDueDtList.isEmpty()) {
            for (ZonedDateTime bilSystemDueDate : bilSysDueDtList) {
                ZonedDateTime oldSystemDate = bilSystemDueDate;

                updateScheduleAll(accountId, oldSystemDate);
                updateCreditScheduleAll(accountId, oldSystemDate);
                updateCashAll(accountId, oldSystemDate);
                deleteBilDatesAll(accountId, oldSystemDate);

            }

        }
    }

    private void deleteBilDatesAll(String accountId, ZonedDateTime oldSystemDate) {

        bilDatesRepository.deleteRowsByInvoiceCd(accountId, oldSystemDate, getInvoiceCodes());

    }

    private void updateCashAll(String accountId, ZonedDateTime oldSystemDate) {

        ZonedDateTime defaultDate = DateRoutine.defaultDateTime();
        bilCashDspRepository.updateCashRowBySysDate(defaultDate, defaultDate, defaultDate, accountId, oldSystemDate,
                ChargeType.EMPTY_CHAR.getChargeType());
        logger.debug("Update All Bil Cash Deposit row to new date ");

    }

    private void updateCreditScheduleAll(String accountId, ZonedDateTime oldSystemDate) {

        ZonedDateTime defaultDate = DateRoutine.defaultDateTime();
        bilCreScheduleRepository.updateCreScheduleRowBySystemDate(defaultDate, defaultDate, defaultDate, accountId,
                oldSystemDate);
        logger.debug("Update All Bil Credit scheduling row to new date");

    }

    private void updateScheduleAll(String accountId, ZonedDateTime oldSystemDate) {

        ZonedDateTime defaultDate = DateRoutine.defaultDateTime();
        bilIstScheduleRepository.updateScheduleRowByInvoiceCode(defaultDate, defaultDate, defaultDate, defaultDate,
                BLANK_CHAR, InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue(), accountId, oldSystemDate,
                getInvoiceCodes());
        logger.debug("Update All Bil Ist scheduling row to new date");

    }

    private Object[] checkDateBeforeResetAll(char fpddIndicator, boolean collectionMethod, BilAccount bilAccount) {
        boolean isUnassignedBisFound = false;
        ZonedDateTime minUninvBisIstDate = DateRoutine.defaultDateTime();

        if (fpddIndicator != ACTIVE && !(collectionMethod && !bilAccount.getCollectionPlan().trim().isEmpty())) {
            Integer unassignedRow = bilIstScheduleRepository.getUnassignedBisRowCount(bilAccount.getAccountId(),
                    DateRoutine.defaultDateTime());
            if (unassignedRow != null && unassignedRow > 0) {
                logger.debug("Number Un-assigned row is existing in BilIstSchedule: {}", unassignedRow);
                isUnassignedBisFound = true;
            } else {
                ZonedDateTime minIstDueDate = bilIstScheduleRepository.getMinIstDueDate(bilAccount.getAccountId(),
                        getInvoiceCodes());
                if (minIstDueDate != null) {
                    minUninvBisIstDate = minIstDueDate;
                }
            }
        }

        logger.debug("Found un-invoice date: {}", minUninvBisIstDate);
        return new Object[] { isUnassignedBisFound, minUninvBisIstDate };
    }

    @Override
    public boolean pcnOnNoResponseRenewalCheck(String accountId, ZonedDateTime se3Date) {
        boolean isRenewalProcess = false;
        List<BilActInquiry> bilActInquiries = bilActInquiryRepository.getSuspendDisbRow(accountId,
                new ArrayList<>(Arrays.asList(ActivityType.PCN_NO_RESPONSE_RENEWAL.getValue())), se3Date);
        if (bilActInquiries != null && !bilActInquiries.isEmpty()) {
            logger.debug("Enable process PCN on No Response Renewal term");
            isRenewalProcess = true;
        }
        return isRenewalProcess;
    }

    private boolean disableFpddCheck(BilAccount bilAccount, ZonedDateTime minUninvBisIstDate,
            BilSupportPlan bilSupportPlan) {

        ZonedDateTime minUninvBdtRfrDate = bilDatesRepository.getMinDueDateByInvoiceCd(bilAccount.getAccountId(),
                getInvoiceCodes(), CHAR_X);
        if (minUninvBdtRfrDate == null) {
            return false;
        }

        if (minUninvBisIstDate.compareTo(minUninvBdtRfrDate) >= 0) {
            return true;
        }
        ZonedDateTime dateMinus1Freq = DateRoutine.calculateDateTimeBySubtractFrequency(bilSupportPlan.getBilRvwFqyCd(),
                minUninvBdtRfrDate);
        if (minUninvBisIstDate.compareTo(dateMinus1Freq) > 0) {
            return false;
        }
        return false;
    }

    @Override
    public int cancelReset(int referenceDuration, BilAccount bilAccount, AccountAdjustDates accountAdjustDates,
            FirstPossibleDueDate firstPossibleDueDate, BilSupportPlan bilSupportPlan, BilActRules bilActRules,
            boolean isXHOLRule, BilRulesUct bilRulesUct, short dueDateDisplacementNumber, Object[] dates) {
        int cancelCount = 0;

        List<BilPolicyTerm> bilPolicyTermsList = bilPolicyTermRepository.getTermCancelReset(
                accountAdjustDates.getAccountId(), getStatusCodes(), DateRoutine.defaultDateTime(), getInvoiceCodes());
        if (bilPolicyTermsList != null && !bilPolicyTermsList.isEmpty()) {
            cancelCount = 1;
            char cancelResetTypeIndicator = CHAR_B;
            resetCancelTerm(bilAccount, cancelResetTypeIndicator, bilPolicyTermsList.get(0).getBillPolStatusCd(),
                    accountAdjustDates, bilSupportPlan, referenceDuration, firstPossibleDueDate, bilActRules,
                    isXHOLRule, bilRulesUct, dueDateDisplacementNumber, dates);
        }
        return cancelCount;
    }

    private void resetCancelTerm(BilAccount bilAccount, char cancelResetTypeIndicator, char cancelStatusCode,
            AccountAdjustDates accountAdjustDates, BilSupportPlan bilSupportPlan, int referenceDuration,
            FirstPossibleDueDate firstPossibleDueDate, BilActRules bilActRules, boolean isXHOLRule,
            BilRulesUct bilRulesUct, short dueDateDisplacementNumber, Object[] dates) {
        boolean isSpCancelReset = false;
        Boolean isCancelDate = null;

        ZonedDateTime newSystemDate = (ZonedDateTime) dates[3];
        ZonedDateTime newAdjustDate = (ZonedDateTime) dates[1];
        ZonedDateTime newReferenceDate = (ZonedDateTime) dates[0];
        ZonedDateTime newInvoiceDate = (ZonedDateTime) dates[2];

        char seasonalBill = getSeasonalBillCode(bilAccount.getSeasionalExclusionCode());

        if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                && cancelStatusCode == BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue()) {
            isSpCancelReset = deterCancelSpReset(cancelResetTypeIndicator, accountAdjustDates.getAccountId(),
                    accountAdjustDates.getProcessDate());
            if (!isSpCancelReset && accountAdjustDates.getPolicyEffectiveDate() != null) {
                Object[] epivObject = checkEpiv(accountAdjustDates, bilAccount, firstPossibleDueDate, bilSupportPlan,
                        referenceDuration, seasonalBill, bilActRules.getBruIncWknInd(), isXHOLRule, bilRulesUct,
                        dueDateDisplacementNumber);
                if (epivObject != null) {
                    isSpCancelReset = (boolean) epivObject[0];
                    isCancelDate = (Boolean) epivObject[1];
                    newSystemDate = (ZonedDateTime) epivObject[2];
                    newAdjustDate = (ZonedDateTime) epivObject[3];
                    newReferenceDate = (ZonedDateTime) epivObject[4];
                    newInvoiceDate = (ZonedDateTime) epivObject[5];
                }
            }
        }

        if (!bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                || isCancelDate != null && !isCancelDate || isSpCancelReset) {

            List<BilDates> bilDatesList = bilDatesRepository.getFutureInvoicedDateByInvoiceDate(
                    accountAdjustDates.getAccountId(), accountAdjustDates.getProcessDate(), FUTURE_INVOICE_CODES,
                    CHAR_X);
            if (bilDatesList != null && !bilDatesList.isEmpty()) {
                newAdjustDate = bilDatesList.get(SHORT_ZERO).getBilAdjDueDt();
                newInvoiceDate = bilDatesList.get(SHORT_ZERO).getBilInvDt();
            } else {
                throw new DataNotFoundException("all.reset.service.future.rows.not.found");
            }

        }

        if (isCancelDate != null && !isCancelDate) {

            List<BilDates> bilDatesList = bilDatesRepository.getFutureSystemDateByInvoiceDate(
                    accountAdjustDates.getAccountId(), newAdjustDate, newInvoiceDate, FUTURE_INVOICE_CODES, CHAR_X);

            if (bilDatesList != null && !bilDatesList.isEmpty()) {
                newSystemDate = bilDatesList.get(SHORT_ZERO).getBilSysDueDt();
                newReferenceDate = bilDatesList.get(SHORT_ZERO).getBilDatesId().getBilReferenceDt();
            } else {
                throw new DataNotFoundException("all.reset.service.future.rows.not.found");
            }
        } else if (!bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                || isSpCancelReset) {
            // retrieve newSysDate - newRefDate
            ZonedDateTime minSysDate = bilDatesRepository.selectMinSysDueDateByAdjInvDate(
                    accountAdjustDates.getAccountId(), getInvoiceCodes(), accountAdjustDates.getProcessDate(),
                    newAdjustDate, newInvoiceDate, CHAR_X);
            if (minSysDate == null) {
                throw new DataNotFoundException("all.reset.service.select.min.system.date");
            }
            newSystemDate = minSysDate;

            ZonedDateTime minReferenceDate = bilDatesRepository.getMinReferenceDate(accountAdjustDates.getAccountId(),
                    newInvoiceDate, newAdjustDate, newSystemDate, getInvoiceCodes(), CHAR_X);
            if (minReferenceDate == null) {
                throw new DataNotFoundException("all.reset.service.select.min.reference.date");
            }
            newReferenceDate = minReferenceDate;

        }

        List<BilPolicyTerm> bilPolicyTermList = null;
        if (cancelResetTypeIndicator == CHAR_B) {
            bilPolicyTermList = bilPolicyTermRepository.findCancelPolicyByBilAccountId(
                    accountAdjustDates.getAccountId(),
                    Arrays.asList(BilPolicyStatusCode.CANCELLED.getValue(),
                            BilPolicyStatusCode.FLAT_CANCELLATION.getValue(),
                            BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue()));
        } else {
            bilPolicyTermList = bilPolicyTermRepository.fetchBilPolicyTermByStatusEffective(
                    accountAdjustDates.getAccountId(),
                    Arrays.asList(BilPolicyStatusCode.CANCELLED.getValue(),
                            BilPolicyStatusCode.FLAT_CANCELLATION.getValue(),
                            BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                            BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(),
                            BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                            BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue()),
                    accountAdjustDates.getProcessDate());
        }

        if (bilPolicyTermList != null && !bilPolicyTermList.isEmpty()) {
            fetchCancelTerms(accountAdjustDates, bilPolicyTermList, cancelResetTypeIndicator, newSystemDate,
                    newAdjustDate, newInvoiceDate, newReferenceDate, bilAccount, bilActRules);
        }

    }

    private void fetchCancelTerms(AccountAdjustDates accountAdjustDates, List<BilPolicyTerm> bilPolicyTermList,
            char cancelResetTypeIndicator, ZonedDateTime newSystemDate, ZonedDateTime newAdjustDate,
            ZonedDateTime newInvoiceDate, ZonedDateTime newReferenceDate, BilAccount bilAccount,
            BilActRules bilActRules) {

        String accountId = accountAdjustDates.getAccountId();
        List<ZonedDateTime> bilSystemDueDateList = null;
        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {

            boolean isDateChange = true;
            List<Character> istScheduleCount = bilIstScheduleRepository.fetchInvoiceCode(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), getInvoiceCodes());
            if (istScheduleCount == null || istScheduleCount.isEmpty()) {
                isDateChange = false;
            }

            if (isDateChange && cancelResetTypeIndicator != CHAR_B) {

                processCancelLoop(accountId, bilPolicyTerm, newSystemDate, newAdjustDate, newInvoiceDate,
                        newReferenceDate, bilSystemDueDateList);

            } else if (isDateChange && cancelResetTypeIndicator == CHAR_B) {

                resetPolicyLevel(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), newSystemDate, newAdjustDate,
                        newInvoiceDate, newReferenceDate, DateRoutine.defaultDateTime());
            }

            if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                    && (bilPolicyTerm.getBillPolStatusCd() == BilPolicyStatusCode.FLAT_CANCELLATION.getValue()
                            || bilPolicyTerm.getBillPolStatusCd() == BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED
                                    .getValue())
                    && accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y
                    && cancelResetTypeIndicator != CHAR_B) {

                accountAdjustDatesService.processServiceWriteOff(bilAccount, bilActRules, accountAdjustDates);

            }

        }

    }

    private void processCancelLoop(String accountId, BilPolicyTerm bilPolicyTerm, ZonedDateTime newSystemDate,
            ZonedDateTime newAdjustDate, ZonedDateTime newInvoiceDate, ZonedDateTime newReferenceDate,
            List<ZonedDateTime> bilSystemDueDateList) {

        if (bilSystemDueDateList == null) {
            bilSystemDueDateList = bilDatesRepository.fetchDistinctSystemDateCount(accountId, getInvoiceCodes(),
                    CHAR_X);
        }

        if (bilSystemDueDateList != null && !bilSystemDueDateList.isEmpty()) {
            for (ZonedDateTime bilSysDueDate : bilSystemDueDateList) {
                resetPolicyLevel(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), newSystemDate, newAdjustDate,
                        newInvoiceDate, newReferenceDate, bilSysDueDate);
            }
        }

    }

    private void resetPolicyLevel(String accountId, String policyId, ZonedDateTime polEffectiveDt,
            ZonedDateTime newSystemDate, ZonedDateTime newAdjustDate, ZonedDateTime newInvoiceDate,
            ZonedDateTime newReferenceDate, ZonedDateTime minSysDueDate) {

        resetPolicyOnBis(accountId, policyId, polEffectiveDt, minSysDueDate, newSystemDate, newAdjustDate,
                newInvoiceDate, newReferenceDate);
        resetPolicyOnBcs(accountId, policyId, polEffectiveDt, minSysDueDate, newSystemDate, newAdjustDate,
                newInvoiceDate);
        resetPolicyOnBcd(accountId, policyId, polEffectiveDt, minSysDueDate, newSystemDate, newAdjustDate,
                newInvoiceDate);
    }

    private void resetPolicyOnBcd(String accountId, String policyId, ZonedDateTime policyEffectiveDate,
            ZonedDateTime minSysDueDate, ZonedDateTime newSystemDate, ZonedDateTime newAdjustDate,
            ZonedDateTime newInvoiceDate) {

        bilCashDspRepository.updateCashRowByPolicyId(newSystemDate, newAdjustDate, newInvoiceDate, accountId, policyId,
                policyEffectiveDate, minSysDueDate);
        logger.debug("Update BilCashDsp to new Adjust - SystemDue - Invoice - Date");

    }

    private void resetPolicyOnBcs(String accountId, String policyId, ZonedDateTime polEffectiveDt,
            ZonedDateTime minSysDueDate, ZonedDateTime newSystemDate, ZonedDateTime newAdjustDate,
            ZonedDateTime newInvoiceDate) {

        bilCreScheduleRepository.updateCreScheduleRowByPolicyId(newSystemDate, newAdjustDate, newInvoiceDate, accountId,
                policyId, polEffectiveDt, minSysDueDate);
        logger.debug("Update BilCreSchedule to new Adjust - SystemDue - Invoice - Date");

    }

    private void resetPolicyOnBis(String accountId, String policyId, ZonedDateTime polEffectiveDt,
            ZonedDateTime minSysDate, ZonedDateTime newSystemDate, ZonedDateTime newAdjustDate,
            ZonedDateTime newInvoiceDate, ZonedDateTime newReferenceDate) {

        bilIstScheduleRepository.updateScheduleRowsByPolicyId(newSystemDate, newAdjustDate, newReferenceDate,
                newInvoiceDate, accountId, policyId, polEffectiveDt, minSysDate);
        logger.debug("Update BilIstSchedule to new Adjust - SystemDue - Invoice - Reference Date");

    }

    private Object[] insertCancelDate(String accountId, ZonedDateTime newSystemDate, ZonedDateTime newAdjustDate,
            ZonedDateTime newReferenceDate, ZonedDateTime newInvoiceDate, char nonCpiLetInd) {

        Boolean isCancelDate = null;

        BilDates bilDates = bilDatesRepository.findById(new BilDatesId(accountId, newReferenceDate)).orElse(null);
        if (bilDates == null) {
            bilDates = new BilDates();
            bilDates.setBilDatesId(new BilDatesId(accountId, newReferenceDate));
            bilDates.setBilSysDueDt(newSystemDate);
            bilDates.setBilAdjDueDt(newAdjustDate);
            bilDates.setBilInvDt(newInvoiceDate);
            bilDates.setBilInvoiceCd(BLANK_CHAR);
            bilDates.setBdtDateInd(CHAR_N);
            bilDates.setBdtDateType(CHAR_F);
            bilDates.setBdtLateCrgInd(CHAR_N);
            bilDates.setBdtNoncpiInd(CHAR_Y);
            if (nonCpiLetInd == CHAR_N) {
                bilDates.setBdtNoncpiInd(nonCpiLetInd);
            }

            logger.debug("Insert new cancel bildates for accountId: {}", accountId);
            bilDatesRepository.saveAndFlush(bilDates);
            isCancelDate = true;

        } else if (isValidBilDate(bilDates)) {
            newSystemDate = bilDates.getBilSysDueDt();
            newAdjustDate = bilDates.getBilAdjDueDt();
            newInvoiceDate = bilDates.getBilInvDt();
        } else {
            isCancelDate = false;
        }

        logger.debug("Retrieved cancelDateIndicator - newSystemDate - newAdjustDate - newInvoiceDate");
        return new Object[] { isCancelDate, newSystemDate, newAdjustDate, newReferenceDate, newInvoiceDate };
    }

    private boolean isValidBilDate(BilDates bilDates) {
        return bilDates.getBdtDateType() == CHAR_F && isValidInvoiceCode(bilDates.getBilInvoiceCd())
                && bilDates.getBilExcInd() != CHAR_X;
    }

    private boolean isValidInvoiceCode(char invoiceCode) {
        return FUTURE_INVOICE_CODES.stream().anyMatch(bilInvoiceCd -> bilInvoiceCd == invoiceCode);
    }

    private Object[] checkEpiv(AccountAdjustDates accountAdjustDates, BilAccount bilAccount,
            FirstPossibleDueDate firstPossibleDueDate, BilSupportPlan bilSupportPlan, int referenceDuration,
            char seasonalBill, char includeWeekend, boolean isXHOLRule, BilRulesUct bilRulesUct,
            short dueDateDisplacementNumber) {

        String policyId = accountAdjustDates.getPolicyId();
        ZonedDateTime fpddInvDate = firstPossibleDueDate.getInvoiceDate();

        boolean isCollectionsOn = false;
        ZonedDateTime epivNextAcyDate = null;
        ZonedDateTime epivPolEffDate = accountAdjustDates.getPolicyEffectiveDate();
        int epivNumberDays;

        BilRulesUct epivRule = bilRulesUctService.readBilRulesUct("EPIV", bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), BLANK_STRING);
        if (epivRule == null || epivRule.getBrtRuleCd() == CHAR_N) {

            return checkCancelDate(seasonalBill, firstPossibleDueDate.getDate(), firstPossibleDueDate.getDate(),
                    firstPossibleDueDate.getInvoiceDate(), firstPossibleDueDate.getReferenceDate(), bilAccount,
                    bilSupportPlan);

        } else {
            epivNumberDays = getNumberDays(epivRule.getBrtParmListTxt().trim());
        }

        if (bilAccount.getThirdPartyIdentifier().trim().isEmpty()
                && bilAccount.getAgentAccountNumber().trim().isEmpty()) {
            BilRulesUct collRule = bilRulesUctService.readBilRulesUct("COLL", bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd(), BLANK_STRING);
            if (collRule != null && collRule.getBrtRuleCd() == ACTIVE) {
                isCollectionsOn = true;
            }
        }

        BilPolActivity bilPolActivity = bilPolActivityRepository
                .findById(new BilPolActivityId(accountAdjustDates.getAccountId(), policyId, epivPolEffDate,
                        ActivityType.LAST_DAY_REINSTATE.getValue()))
                .orElse(null);
        if (bilPolActivity != null
                && bilPolActivity.getBilNxtAcyDt().compareTo(accountAdjustDates.getProcessDate()) >= 0) {
            epivNextAcyDate = bilPolActivity.getBilNxtAcyDt();
        } else {
            if (isCollectionsOn) {
                epivNextAcyDate = getCncDate(accountAdjustDates.getAccountId(), policyId);
                if (epivNextAcyDate == null || epivNextAcyDate.compareTo(epivPolEffDate) < 0) {
                    BilDates bilDates = getFlatCncDate(accountAdjustDates.getAccountId(), epivPolEffDate);
                    if (bilDates == null) {
                        return new Object[] { false, null, null, null, null, null };
                    } else {
                        return new Object[] { false, null, bilDates.getBilSysDueDt(), bilDates.getBilAdjDueDt(),
                                bilDates.getBilDatesId().getBilReferenceDt(), bilDates.getBilInvDt() };
                    }

                }
            } else {
                epivNextAcyDate = getBamDate(accountAdjustDates.getAccountId(), policyId, epivPolEffDate);
            }
        }

        ZonedDateTime epivInvoiceDate = null;

        boolean excludeWeekend = getOverrideWeekendIndicator(true, includeWeekend);
        ZonedDateTime epivSaveDate = DateRoutine.getAdjustedDate(epivNextAcyDate, excludeWeekend, epivNumberDays,
                accountAdjustDates.getProcessDate());

        if (isXHOLRule) {
            ZonedDateTime differenceDate = firstPossibleDueDateService.holidayDateProcessing(epivSaveDate,
                    accountAdjustDates.getProcessDate(), bilRulesUct);

            if (differenceDate.compareTo(fpddInvDate) <= 0) {
                return checkCancelDate(seasonalBill, firstPossibleDueDate.getDate(), firstPossibleDueDate.getDate(),
                        firstPossibleDueDate.getInvoiceDate(), firstPossibleDueDate.getReferenceDate(), bilAccount,
                        bilSupportPlan);
            }
            epivInvoiceDate = differenceDate;
        } else {
            if (epivSaveDate.compareTo(fpddInvDate) <= 0) {
                return checkCancelDate(seasonalBill, firstPossibleDueDate.getDate(), firstPossibleDueDate.getDate(),
                        firstPossibleDueDate.getInvoiceDate(), firstPossibleDueDate.getReferenceDate(), bilAccount,
                        bilSupportPlan);
            }
            epivInvoiceDate = epivSaveDate;
        }

        if (epivInvoiceDate.compareTo(epivNextAcyDate) < 0) {
            if (!isXHOLRule) {
                epivInvoiceDate = epivNextAcyDate;
            } else {
                epivInvoiceDate = firstPossibleDueDateService.holidayDateProcessing(epivSaveDate,
                        accountAdjustDates.getProcessDate(), bilRulesUct);
            }

        }

        ZonedDateTime differenceDate = DateRoutine.getAdjustedDate(epivInvoiceDate, false, dueDateDisplacementNumber,
                null);
        ZonedDateTime epivDueDate = differenceDate;
        ZonedDateTime epivSystemDate = differenceDate;
        if (seasonalBill == CHAR_Y) {
            char blackoutDateIndicator = checkSeasonalDate(bilAccount.getSeasionalExclusionCode(), epivSystemDate);
            if (blackoutDateIndicator == CHAR_N && epivSystemDate.compareTo(epivDueDate) != SHORT_ZERO) {
                blackoutDateIndicator = checkSeasonalDate(bilAccount.getSeasionalExclusionCode(), epivDueDate);
            }

            if (blackoutDateIndicator == CHAR_Y) {
                return new Object[] { true, null, null, null, null, null };
            }
        }

        ZonedDateTime[] epivDates = checkEpivInvDate(accountAdjustDates, epivInvoiceDate, bilAccount.getBillTypeCd(),
                epivDueDate, bilSupportPlan, bilRulesUct, isXHOLRule, excludeWeekend, dueDateDisplacementNumber);
        if (epivDates != null && epivDates.length != 0) {
            epivSystemDate = epivDates[SHORT_ZERO];
            epivDueDate = epivDates[SHORT_ONE];
        }

        ZonedDateTime epivReferenceDate = epivDueDate;
        if (referenceDuration != 0) {
            char signAdjust = MINUS_SIGN;
            if (bilAccount.getStartReferenceDate().compareTo(bilAccount.getStartDeductionDate()) > 0) {
                signAdjust = PLUS_SIGN;
            }

            epivReferenceDate = DateRoutine.adjustDateWithOffset(epivDueDate, false, referenceDuration, signAdjust,
                    null);
        }

        return checkCancelDate(seasonalBill, epivSystemDate, epivDueDate, epivInvoiceDate, epivReferenceDate,
                bilAccount, bilSupportPlan);

    }

    private Object[] checkCancelDate(char seasonalBill, ZonedDateTime newSystemDate, ZonedDateTime newAdjustDate,
            ZonedDateTime newInvoiceDate, ZonedDateTime newReferenceDate, BilAccount bilAccount,
            BilSupportPlan bilSupportPlan) {

        if (seasonalBill == CHAR_Y) {
            logger.debug("Verify if System or Adjusted due date fall in an excluded period.");
            char blackoutDate = checkSeasonalDate(bilAccount.getSeasionalExclusionCode(), newSystemDate);
            if (blackoutDate == CHAR_N && newSystemDate.compareTo(newAdjustDate) != SHORT_ZERO) {
                blackoutDate = checkSeasonalDate(bilAccount.getSeasionalExclusionCode(), newAdjustDate);
            }
            if (blackoutDate == CHAR_Y) {
                return new Object[] { true, null, newSystemDate, newAdjustDate, newReferenceDate, newInvoiceDate };
            } else {
                Object[] cancelObjects = insertCancelDate(bilAccount.getAccountId(), newSystemDate, newAdjustDate,
                        newReferenceDate, newInvoiceDate, bilSupportPlan.getBspNoncpiLetInd());

                return new Object[] { false, cancelObjects[0], cancelObjects[1], cancelObjects[2], cancelObjects[3],
                        cancelObjects[4] };
            }

        } else {
            Object[] cancelObjects = insertCancelDate(bilAccount.getAccountId(), newSystemDate, newAdjustDate,
                    newReferenceDate, newInvoiceDate, bilSupportPlan.getBspNoncpiLetInd());
            return new Object[] { false, cancelObjects[0], cancelObjects[1], cancelObjects[2], cancelObjects[3],
                    cancelObjects[4] };
        }

    }

    private ZonedDateTime[] checkEpivInvDate(AccountAdjustDates accountAdjustDates, ZonedDateTime epivInvDate,
            String bilTypeCode, ZonedDateTime epivDueDate, BilSupportPlan bilSupportPlan, BilRulesUct bilRulesUct,
            boolean isXHOLRule, boolean excludeWeekend, short dueDateDisplacementNumber) {

        List<BilDates> bilDatesList = bilDatesRepository.getMaxInvoicedDateByExcCd(accountAdjustDates.getAccountId(),
                epivInvDate, CHAR_X);
        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            BilDates bilDates = bilDatesList.get(SHORT_ZERO);

            if (bilDates.getBilInvoiceCd() == InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue()
                    || bilDates.getBilInvoiceCd() == InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue()) {

                updateThruEpivDate(accountAdjustDates.getAccountId(), bilTypeCode, bilDates.getBilInvoiceCd(),
                        bilDates.getBilAdjDueDt(), bilDates.getBilInvDt(), epivInvDate, epivDueDate);

            } else if (evalCheckDate(accountAdjustDates.getAccountId(), bilDates.getBilAdjDueDt(), bilTypeCode)) {
                epivDueDate = adjustEpivInvoiceDate(accountAdjustDates, bilDates.getBilAdjDueDt(), epivInvDate,
                        bilSupportPlan, bilRulesUct, isXHOLRule, excludeWeekend, dueDateDisplacementNumber);
                ZonedDateTime epivSysDate = epivDueDate;
                return new ZonedDateTime[] { epivDueDate, epivSysDate };
            }
        }

        return new ZonedDateTime[0];

    }

    private ZonedDateTime adjustEpivInvoiceDate(AccountAdjustDates accountAdjustDates, ZonedDateTime checkEpivDueDate,
            ZonedDateTime epivInvDate, BilSupportPlan bilSupportPlan, BilRulesUct bilRulesUct, boolean isXHOLRule,
            boolean excludeWeekend, short dueDateDisplacementNumber) {

        ZonedDateTime differenceDate = DateRoutine.getAdjustedDate(checkEpivDueDate, false,
                bilSupportPlan.getBspPndCncNbr(), null);
        if (differenceDate.compareTo(epivInvDate) < 0) {
            return null;
        }
        epivInvDate = differenceDate;
        differenceDate = DateRoutine.getAdjustedDate(epivInvDate, excludeWeekend, SHORT_ONE, null);

        if (isXHOLRule) {
            firstPossibleDueDateService.holidayDateProcessing(differenceDate, accountAdjustDates.getProcessDate(),
                    bilRulesUct);
        }

        return DateRoutine.getAdjustedDate(epivInvDate, false, dueDateDisplacementNumber, null);

    }

    private void updateThruEpivDate(String accountId, String bilTypeCode, Character checkEpivInvoiceCode,
            ZonedDateTime checkEpivDueDate, ZonedDateTime checkEpivInvDate, ZonedDateTime epivInvDate,
            ZonedDateTime epivDueDate) {

        if ((checkEpivInvoiceCode.charValue() == InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING
                .getValue()
                || checkEpivInvoiceCode.charValue() == InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue()
                || bilTypeCode.equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                        && isCrgOnCheckDate(accountId, checkEpivDueDate))
                && epivInvDate.compareTo(checkEpivDueDate) <= 0) {

            bilDatesRepository.updateBilDatesByAdjDueDate(epivDueDate, epivInvDate, BLANK_CHAR, accountId,
                    checkEpivDueDate, checkEpivInvDate);
            bilIstScheduleRepository.updateRescindScheduleRowByAdjustDate(epivDueDate, epivInvDate, BLANK_CHAR,
                    accountId, checkEpivDueDate, checkEpivInvDate);
            bilCreScheduleRepository.updateRescindCreScheduleRowBybilAdjDueDt(epivDueDate, epivInvDate, accountId,
                    checkEpivDueDate, checkEpivInvDate);
            bilCashDspRepository.updateRescindCashRowByAdjDueDate(epivDueDate, epivInvDate, accountId, checkEpivDueDate,
                    checkEpivInvDate, ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue());
            bilCrgAmountsRepository.updateRescindCrgAmountsBybilAdjDueDt(epivDueDate, epivInvDate, accountId,
                    checkEpivDueDate, checkEpivInvDate,
                    Arrays.asList(ChargeTypeCodes.LATE_CHARGE.getValue(), ChargeTypeCodes.PENALTY_CHARGE.getValue()));
        }
    }

    private boolean isCrgOnCheckDate(String accountId, ZonedDateTime checkEpivDueDate) {

        List<BilCrgAmounts> bilCrgAmounts = bilCrgAmountsRepository.evaluateChargeAmounts(accountId, checkEpivDueDate,
                new ArrayList<>(Arrays.asList(ChargeType.LATECHARGES.getChargeType(),
                        ChargeType.PENALTYCHARGES.getChargeType())),
                getInvoiceCodes());
        return bilCrgAmounts != null && !bilCrgAmounts.isEmpty();
    }

    private boolean evalCheckDate(String accountId, ZonedDateTime checkEpivDueDate, String bilTypeCode) {

        if (!bilTypeCode.equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())) {
            List<BilPolicyTerm> bilPolicyTerms = bilPolicyTermRepository.evaluateCheckDate(accountId,
                    Arrays.asList(BilPolicyStatusCode.FLAT_CANCELLATION.getValue(),
                            BilPolicyStatusCode.CANCELLED.getValue(),
                            BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                            BilPolicyStatusCode.CANCELLED_SUBJECT_TO_AUDIT.getValue(),
                            BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                            BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(), 'R'),
                    checkEpivDueDate);
            if (bilPolicyTerms != null && !bilPolicyTerms.isEmpty()) {
                return true;
            }
        }

        return false;
    }

    private char checkSeasonalDate(String bilSeaXclCode, ZonedDateTime deductionDate) {
        BilSeasExcl bilSeasExcl = bilSeasExclRepository.getBilSeaRowByDate(bilSeaXclCode, deductionDate);
        if (bilSeasExcl != null) {
            return CHAR_Y;
        }
        return CHAR_N;
    }

    private char getSeasonalBillCode(String seasionalExclusionCode) {
        Integer seasionalCounts = bilSeasExclRepository.getBillingItemRow(seasionalExclusionCode);
        if (seasionalCounts != null && seasionalCounts > 0) {
            return CHAR_Y;
        }
        return BLANK_CHAR;
    }

    private ZonedDateTime getBamDate(String accountId, String policyId, ZonedDateTime epivPolEffDate) {

        BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository
                .findById(new BilPolicyTermId(accountId, policyId, epivPolEffDate)).orElse(null);
        if (bilPolicyTerm == null) {
            throw new DataNotFoundException("epiv.exp.date.not.found");
        }

        List<BilAmounts> bilAmountsList = bilAmountsRepository.getMaxCancelBamAmtTs(accountId, policyId, epivPolEffDate,
                bilPolicyTerm.getPlnExpDt(), AmountProcessIndicator.INTERMEDIATEPROCESSING.getValue(),
                BilAmountsEftIndicator.CANCELLATION_F.getValue(), Arrays.asList("BAM", "AMT", "BAS", "BPS", "BEQ"),
                Arrays.asList(BilAmountsEftIndicator.CANCELLATION_P.getValue(),
                        BilAmountsEftIndicator.CANCELLATION_S.getValue()),
                Arrays.asList("BAM", "AMT", "BAA", "BPA", "BAS", "BPS", "BEQ"));
        if (bilAmountsList != null && !bilAmountsList.isEmpty()) {

            return bilAmountsList.get(SHORT_ZERO).getBamAmtEffDt();
        } else {
            throw new DataNotFoundException("max.cancel.amount.timestamp.not.found");
        }

    }

    private BilDates getFlatCncDate(String accountId, ZonedDateTime epivPolEffDate) {
        List<BilDates> bilDatesList = bilDatesRepository.fetchFlatBilDates(accountId, epivPolEffDate, CHAR_X);
        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            logger.debug("Returning BilSysDate - BilAdjDate - BilReferenceDate - BilInvoiceDate");
            return bilDatesList.get(0);
        }
        return null;
    }

    private ZonedDateTime getCncDate(String accountId, String policyId) {
        ZonedDateTime epivNextAcyDate = null;
        BilPolActivity bilPolActivity = bilPolActivityRepository
                .findById(new BilPolActivityId(accountId, policyId, DateRoutine.defaultDateTime(), "CNC")).orElse(null);
        if (bilPolActivity != null) {
            epivNextAcyDate = bilPolActivity.getBilNxtAcyDt();
        }
        return epivNextAcyDate;
    }

    private int getNumberDays(String parmList) {
        int epivNumberDays = 0;
        try {
            epivNumberDays = Integer.parseInt(parmList.trim());
        } catch (NumberFormatException e) {
            epivNumberDays = 0;
        }
        return epivNumberDays;
    }

    private boolean deterCancelSpReset(char cancelResetTypeIndicator, String accountId, ZonedDateTime se3Date) {
        boolean isSpCancelReset = false;
        List<Object[]> statusObject = null;
        if (cancelResetTypeIndicator == CHAR_A) {
            statusObject = bilPolicyTermRepository.getMaxMinPolicyStatusByEffDate(accountId, getStatusCodes(), se3Date);
        } else if (cancelResetTypeIndicator == CHAR_B) {
            statusObject = bilPolicyTermRepository.getMaxMinPolicyStatus(accountId, getStatusCodes(),
                    DateRoutine.defaultDateTime(), getInvoiceCodes());
        }

        if (statusObject == null || statusObject.isEmpty() || statusObject.get(0)[0] == null) {
            throw new DataNotFoundException("status.not.found.bil.policy.trm");
        }
        Character minStatus = (Character) statusObject.get(0)[0];
        Character maxStatus = (Character) statusObject.get(0)[1];
        if (minStatus.equals(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue())
                && maxStatus.equals(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue())) {
            isSpCancelReset = true;
        }
        return isSpCancelReset;
    }

    private Object[] testForRenew(AdjustDatesReviewDetails adjustDatesReviewDetails, BilSupportPlan bilSupportPlan,
            char includeWeekend, BilAccount bilAccount, boolean isXHOLRule, BilRulesUct bilRulesUct,
            AccountAdjustDates accountAdjustDates, FirstPossibleDueDate firstPossibleDueDate, boolean isRpcExist,
            boolean isFpddOnTimeProcessing) {
        char renewBillIndicator;
        ZonedDateTime newInvoiceDate = null;
        ZonedDateTime renewDueDate = null;
        ZonedDateTime renewInvoiceDate = null;
        ZonedDateTime maxPolicyEffDate = bilIstScheduleRepository.getMaxPolicyEffectiveDate(bilAccount.getAccountId(),
                DateRoutine.defaultDateTime());
        if (maxPolicyEffDate == null) {
            return new Object[0];
        }
        List<Character> invoiceCodeList = bilIstScheduleRepository.fetchInvoiceCodeForBis(bilAccount.getAccountId(),
                maxPolicyEffDate,
                Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                        InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                        InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()));
        if (invoiceCodeList != null && !invoiceCodeList.isEmpty()) {
            return new Object[0];
        }
        ZonedDateTime holdDate = maxPolicyEffDate;
        ZonedDateTime newReferenceDate = maxPolicyEffDate;

        if (adjustDatesReviewDetails.getReferenceDuration() != 0) {
            ZonedDateTime[] correctDueObject = getCorrectDueDate(adjustDatesReviewDetails.getReferenceDuration(),
                    newReferenceDate, bilAccount, bilRulesUct, includeWeekend,
                    adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, accountAdjustDates);
            logger.debug("Retrieved newSystemDate - newAdjDate - newInvoiceDate - holdDate");
            newInvoiceDate = correctDueObject[2];
            ZonedDateTime newSystemDate = correctDueObject[0];
            adjustDatesReviewDetails.setNewSystemDate(newSystemDate);
            adjustDatesReviewDetails.setNewInvoiceDate(newInvoiceDate);
            adjustDatesReviewDetails.setNewAdjustDueDate(correctDueObject[1]);
        } else {
            newInvoiceDate = firstPossibleDueDateService.getFpddInvoiceDate(holdDate,
                    accountAdjustDates.getProcessDate(), includeWeekend,
                    adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, bilRulesUct);
            adjustDatesReviewDetails.setNewSystemDate(newReferenceDate);
            adjustDatesReviewDetails.setNewInvoiceDate(newInvoiceDate);
        }

        renewDueDate = maxPolicyEffDate;
        renewInvoiceDate = newInvoiceDate;
        if (newInvoiceDate.compareTo(accountAdjustDates.getProcessDate()) <= 0) {
            renewBillIndicator = getBam(bilAccount.getAccountId(), maxPolicyEffDate);
            if (renewBillIndicator == BLANK_CHAR) {
                renewBillIndicator = CHAR_F;
                createRenewDate(adjustDatesReviewDetails, accountAdjustDates, firstPossibleDueDate, bilSupportPlan,
                        includeWeekend, isXHOLRule, bilRulesUct, bilAccount, isRpcExist, isFpddOnTimeProcessing);
            }

        } else {
            renewBillIndicator = getCurFutureDate(adjustDatesReviewDetails, maxPolicyEffDate, accountAdjustDates,
                    newInvoiceDate, firstPossibleDueDate, bilSupportPlan, includeWeekend, isXHOLRule, bilRulesUct,
                    bilAccount, isRpcExist, isFpddOnTimeProcessing);
        }

        logger.debug("TestForRenew: fetched renewBillIndicator - newInvoiceDate - renewDueDate - renewInvoiceDate");
        return new Object[] { renewBillIndicator, newInvoiceDate, renewDueDate, renewInvoiceDate };
    }

    private char getCurFutureDate(AdjustDatesReviewDetails adjustDatesReviewDetails, ZonedDateTime maxPolicyEffDate,
            AccountAdjustDates accountAdjustDates, ZonedDateTime newInvoiceDate,
            FirstPossibleDueDate firstPossibleDueDate, BilSupportPlan bilSupportPlan, char includeWeekend,
            boolean isXHOLRule, BilRulesUct bilRulesUct, BilAccount bilAccount, boolean isRpcExist,
            boolean isFpddOnTimeProcessing) {

        char renewBillIndicator;

        ZonedDateTime maxInvoiceDate = bilDatesRepository.getMaxInvoiceDateByRefDate(accountAdjustDates.getAccountId(),
                Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()), maxPolicyEffDate, CHAR_X);

        if (maxInvoiceDate == null) {
            maxInvoiceDate = accountAdjustDates.getProcessDate();
        }

        if (newInvoiceDate.compareTo(maxInvoiceDate) <= 0) {
            renewBillIndicator = getBam(accountAdjustDates.getAccountId(), maxPolicyEffDate);
            if (renewBillIndicator == BLANK_CHAR) {
                renewBillIndicator = CHAR_S;
            }
        } else {
            ZonedDateTime maxCurInvoiceDate = bilDatesRepository
                    .getMinInvoiceDateByRefDate(accountAdjustDates.getAccountId(),
                            Arrays.asList(InvoiceTypeCode.EMPTY_CHAR.getValue(),
                                    InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue()),
                            maxPolicyEffDate, CHAR_X);

            if (maxCurInvoiceDate == null) {
                maxCurInvoiceDate = accountAdjustDates.getProcessDate();
            }

            if (newInvoiceDate.compareTo(maxCurInvoiceDate) <= 0) {
                renewBillIndicator = getBam(accountAdjustDates.getAccountId(), maxPolicyEffDate);
                if (renewBillIndicator == BLANK_CHAR) {
                    renewBillIndicator = CHAR_S;
                }
            } else {
                renewBillIndicator = CHAR_N;
            }
        }

        logger.debug("GetCurFutureDate: Retrieved maxInvoiceDate - maxCurInvoiceDate");

        if (renewBillIndicator == CHAR_S) {
            adjustDatesReviewDetails.setHoldNewDate(accountAdjustDates.getNewDueDate());
            accountAdjustDates.setNewDueDate(maxPolicyEffDate);
            accountAdjustDates.setInvoiceDate(newInvoiceDate);
            accountAdjustDates.setAddDateIndicator(CHAR_F);
            createRenewDate(adjustDatesReviewDetails, accountAdjustDates, firstPossibleDueDate, bilSupportPlan,
                    includeWeekend, isXHOLRule, bilRulesUct, bilAccount, isRpcExist, isFpddOnTimeProcessing);
        }
        return renewBillIndicator;
    }

    private void createRenewDate(AdjustDatesReviewDetails adjustDatesReviewDetails,
            AccountAdjustDates accountAdjustDates, FirstPossibleDueDate firstPossibleDueDate,
            BilSupportPlan bilSupportPlan, char includeWeekend, boolean isXHOLRule, BilRulesUct bilRulesUct,
            BilAccount bilAccount, boolean isRpcExist, boolean isFpddOnTimeProcessing) {

        boolean isInvoicedToday = firstPossibleDueDate.isInvoicedToday();
        if (accountAdjustDates.getAddDateIndicator() == CHAR_Q) {
            isInvoicedToday = firstPossibleDueDateService.determineIfInvoicedToday(accountAdjustDates.getAccountId(),
                    accountAdjustDates.getProcessDate());
            firstPossibleDueDateService.getFpddDate(isInvoicedToday, accountAdjustDates, includeWeekend,
                    adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, bilRulesUct);
            firstPossibleDueDate.setInvoicedToday(isInvoicedToday);
        }

        if (accountAdjustDates.getAddDateIndicator() == CHAR_F) {
            ZonedDateTime newSystemDate = accountAdjustDates.getNewDueDate();
            ZonedDateTime newAdjustDate = accountAdjustDates.getNewDueDate();
            ZonedDateTime newReferenceDate = accountAdjustDates.getNewDueDate();
            ZonedDateTime newInvoiceDate = accountAdjustDates.getInvoiceDate();

            if (adjustDatesReviewDetails.getReferenceDuration() != SHORT_ZERO) {
                ZonedDateTime[] correctDueObject = getCorrectDueDate(adjustDatesReviewDetails.getReferenceDuration(),
                        newReferenceDate, bilAccount, bilRulesUct, includeWeekend,
                        adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, accountAdjustDates);
                newSystemDate = correctDueObject[SHORT_ZERO];
                newAdjustDate = correctDueObject[SHORT_ONE];
                newInvoiceDate = correctDueObject[2];
            }

            firstPossibleDueDateService.newFpddBillCheck(accountAdjustDates, newSystemDate, newAdjustDate,
                    newReferenceDate, newInvoiceDate, bilSupportPlan, adjustDatesReviewDetails.isErcvIssDtInvoice(),
                    adjustDatesReviewDetails.isAccountBilled());
        } else {

            ZonedDateTime[] newReferenceDate = firstPossibleDueDateService.getFpddReferenceDate(
                    adjustDatesReviewDetails.getReferenceDuration(), firstPossibleDueDate.getDate(), bilAccount,
                    isFpddOnTimeProcessing);
            firstPossibleDueDate.setReferenceDate(newReferenceDate[SHORT_ZERO]);
            firstPossibleDueDate.setReferenceDateOnTime(newReferenceDate[SHORT_ONE]);
            ZonedDateTime newInvoiceDate = accountAdjustDates.getProcessDate();
            if (isInvoicedToday && isRpcExist) {
                newInvoiceDate = firstPossibleDueDateService.getFpddInvoiceDate(firstPossibleDueDate.getDate(),
                        accountAdjustDates.getProcessDate(), includeWeekend,
                        adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, bilRulesUct);
            }
            firstPossibleDueDateService.newFpddBillCheck(accountAdjustDates, firstPossibleDueDate.getDate(),
                    firstPossibleDueDate.getDate(), firstPossibleDueDate.getReferenceDate(), newInvoiceDate,
                    bilSupportPlan, adjustDatesReviewDetails.isErcvIssDtInvoice(),
                    adjustDatesReviewDetails.isAccountBilled());
        }

    }

    private char getBam(String accountId, ZonedDateTime bisPolicyEffDate) {
        char renewBillIndicator = BLANK_CHAR;
        String maxPolicyId = bilPolicyTermRepository.getMaxPolicyIdByEffDate(accountId, bisPolicyEffDate,
                Arrays.asList(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                        BilPolicyStatusCode.TRANSFERRED.getValue(), BilPolicyStatusCode.CANCELLED.getValue(),
                        BilPolicyStatusCode.FLAT_CANCELLATION.getValue()));
        if (maxPolicyId == null || maxPolicyId.trim().isEmpty()) {
            renewBillIndicator = CHAR_N;
        } else {
            List<Character> invoiceCodeList = bilIstScheduleRepository.findInvoiceCodeByAmounts(accountId, maxPolicyId,
                    bisPolicyEffDate, DECIMAL_ZERO, DECIMAL_ZERO);
            if (invoiceCodeList != null && !invoiceCodeList.isEmpty()) {
                renewBillIndicator = CHAR_N;
            } else {
                List<BilAmounts> bilAmounts = bilAmountsRepository.getBilAmountsByEffectiveDate(accountId, maxPolicyId,
                        bisPolicyEffDate,
                        Arrays.asList(BilAmountsEftIndicator.RENEWAL.getValue(),
                                BilAmountsEftIndicator.RENEWAL_UNDER.getValue(),
                                BilAmountsEftIndicator.REVERSE_AUDIT_OPERATOR.getValue(),
                                BilAmountsEftIndicator.RENEWALS.getValue(), CHAR_X),
                        CHAR_H);
                if (bilAmounts == null || bilAmounts.isEmpty()) {
                    renewBillIndicator = CHAR_N;
                }
            }
        }
        return renewBillIndicator;
    }

    private ZonedDateTime[] getCorrectDueDate(int referenceDuration, ZonedDateTime newReferenceDate,
            BilAccount bilAccount, BilRulesUct bilRulesUct, char includeWeekend, short dueDateDisplacementNumber,
            boolean isXHOLRule, AccountAdjustDates accountAdjustDates) {
        ZonedDateTime newSystemDate = null;
        ZonedDateTime newAdjDate = null;
        ZonedDateTime holdDate = null;
        ZonedDateTime newInvoiceDate = null;
        if (referenceDuration == 0) {
            newSystemDate = newReferenceDate;
        } else {
            char adjustSign = PLUS_SIGN;
            if (bilAccount.getStartReferenceDate().compareTo(bilAccount.getStartDeductionDate()) > 0) {
                adjustSign = MINUS_SIGN;
            }

            newSystemDate = DateRoutine.adjustDateWithOffset(newReferenceDate, false, referenceDuration, adjustSign,
                    null);
        }
        newAdjDate = newSystemDate;
        holdDate = newSystemDate;
        newInvoiceDate = firstPossibleDueDateService.getFpddInvoiceDate(newSystemDate,
                accountAdjustDates.getProcessDate(), includeWeekend, dueDateDisplacementNumber, isXHOLRule,
                bilRulesUct);

        logger.debug("Retrieved newSystemDate - newAdjDate - newInvoiceDate - holdDate");
        return new ZonedDateTime[] { newSystemDate, newAdjDate, newInvoiceDate, holdDate };
    }

    private List<Character> getStatusCodes() {
        List<Character> statusCodeList = new ArrayList<>();
        statusCodeList.add(BilPolicyStatusCode.FLAT_CANCELLATION.getValue());
        statusCodeList.add(BilPolicyStatusCode.CANCELLED.getValue());
        statusCodeList.add(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue());
        return statusCodeList;
    }

    private List<Character> getInvoiceCodes() {
        List<Character> invoiceCodeList = new ArrayList<>();
        invoiceCodeList.add(InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue());
        invoiceCodeList.add(InvoiceTypeCode.EMPTY_CHAR.getValue());
        return invoiceCodeList;
    }

    private boolean cancelManualReinvoice(AccountAdjustDates accountAdjustDates, ZonedDateTime holdNewDate) {

        bilActInquiryRepository.deleteManualReInvoiceRows(accountAdjustDates.getAccountId(), "MRI");

        String bilAcyDesCode = "REP";
        String bilDesReasonType = "MRI";
        double bilAmounts = DECIMAL_ZERO;

        if (isWriteHistory(accountAdjustDates.getQuoteIndicator(), accountAdjustDates.getAddChargeIndicator(),
                accountAdjustDates.getChargeAmount())) {
            accountSummary(bilAcyDesCode, bilDesReasonType, bilAmounts, holdNewDate, accountAdjustDates);
        }

        return false;
    }

    private boolean isWriteHistory(String quoteIndicator, char addChargeIndicator, Double chargeAmount) {
        return quoteIndicator.charAt(SHORT_ZERO) != CHAR_Q
                && !((addChargeIndicator == ChargeType.PENALTYCHARGES.getChargeType()
                        || addChargeIndicator == ChargeType.LATECHARGES.getChargeType())
                        && chargeAmount == DECIMAL_ZERO);
    }

    private void accountSummary(String bilAcyDesCode, String bilDesReasonType, double bilAmounts,
            ZonedDateTime holdNewDate, AccountAdjustDates accountAdjustDates) {

        ZonedDateTime acyDate1 = DateRoutine.defaultDateTime();
        ZonedDateTime acyDate2 = DateRoutine.defaultDateTime();
        Short bilAcySeq = 0;

        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountAdjustDates.getAccountId(),
                accountAdjustDates.getProcessDate());
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }

        if (accountAdjustDates.getReferenceDateIndicator() == CHAR_Y
                || accountAdjustDates.getStartDateIndicator() == CHAR_Y) {
            acyDate1 = accountAdjustDates.getCurrentDueDate();
            acyDate2 = accountAdjustDates.getNewDueDate();
            if (holdNewDate.compareTo(DateRoutine.dateTimeAsYYYYMMDD(0)) != 0) {
                acyDate2 = holdNewDate;
            }
        }

        if (bilAcyDesCode.equalsIgnoreCase(ActivityType.ECC.getValue())) {
            List<BilEftBank> bilEftBankList = bilEftBankRepository.findBilEftBankEntry(
                    accountAdjustDates.getAccountId(), CHAR_A, accountAdjustDates.getProcessDate(), 'A',
                    PageRequest.of(SHORT_ZERO, SHORT_ONE));

            if (bilEftBankList != null && !bilEftBankList.isEmpty()) {
                acyDate1 = bilEftBankList.get(SHORT_ZERO).getBilEftBankId().getBebEffectiveDt();
            }
        }

        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountAdjustDates.getAccountId(),
                accountAdjustDates.getProcessDate(), bilAcySeq.shortValue());
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(BLANK_STRING);
        bilActSummary.setPolNbr(BLANK_STRING);
        bilActSummary.setBilAcyDesCd(bilAcyDesCode);
        bilActSummary.setBilDesReaTyp(bilDesReasonType);
        bilActSummary.setBilAcyDes1Dt(acyDate1);
        bilActSummary.setBilAcyDes2Dt(acyDate2);
        bilActSummary.setBilAcyAmt(bilAmounts);
        if (!accountAdjustDates.getUserSequenceId().trim().isEmpty()) {
            bilActSummary.setUserId(accountAdjustDates.getUserSequenceId());
        } else {
            bilActSummary.setUserId(SYSTEM_USER_ID);
        }

        bilActSummary.setBilAcyTs(dateService.currentDateTime());
        bilActSummary.setBasAddDataTxt(BLANK_STRING);
        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

    private boolean getOverrideWeekendIndicator(boolean excludeWeekend, char includeWeekend) {
        return !(excludeWeekend && includeWeekend == CHAR_Y);

    }

}