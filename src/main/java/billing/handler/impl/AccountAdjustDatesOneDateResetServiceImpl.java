package billing.handler.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_F;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActRules;
import billing.data.entity.BilCrgAmounts;
import billing.data.entity.BilDates;
import billing.data.entity.BilEftPlnCrg;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSeasExcl;
import billing.data.entity.BilSptPlnCrg;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.id.BilDatesId;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCreScheduleRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilDatesRepository;
import billing.data.repository.BilEftPlnCrgRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilSeasExclRepository;
import billing.data.repository.BilSptPlnCrgRepository;
import billing.handler.model.AccountAdjustDates;
import billing.handler.model.AccountAdjustSupportData;
import billing.handler.model.AdjustDatesReviewDetails;
import billing.handler.model.ChargeVariables;
import billing.handler.model.DownPaymentFee;
import billing.handler.model.FirstPossibleDueDate;
import billing.handler.model.VariableSeviceCharge;
import billing.handler.model.VariableSeviceChargeOverride;
import billing.handler.service.AccountAdjustDatesService;
import billing.handler.service.FirstPossibleDueDateService;
import billing.handler.service.OneDateResetService;
import billing.utils.AdjustDueDatesConstants;
import billing.utils.AdjustDueDatesConstants.ScheduleCode;
import billing.utils.BillingConstants.ChargeType;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;

@Service
public class AccountAdjustDatesOneDateResetServiceImpl implements OneDateResetService {

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilDatesRepository bilDatesRepository;

    @Autowired
    private BilCreScheduleRepository bilCreScheduleRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private AccountAdjustDatesService accountAdjustDatesService;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private FirstPossibleDueDateService firstPossibleDueDateService;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilSeasExclRepository bilSeasExclRepository;

    @Autowired
    private BilSptPlnCrgRepository bilSptPlnCrgRepository;

    @Autowired
    private BilEftPlnCrgRepository bilEftPlnCrgRepository;

    private static final String RULE_XHOL = "XHOL";

    @Override
    @Transactional
    public void processOneDateReset(AccountAdjustDates accountAdjustDates) {

        AdjustDatesReviewDetails adjustDatesReviewDetails = accountAdjustDatesService
                .processAdjustDates(accountAdjustDates);

        BilActRules bilActRules = adjustDatesReviewDetails.getBilActRules();
        BilAccount bilAccount = adjustDatesReviewDetails.getBilAccount();
        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();
        char fpddIndicator = adjustDatesReviewDetails.getFpddIndicator();
        boolean disableFpdd = false;
        // need to add all sevice classes starts
        VariableSeviceCharge variableSeviceCharge = new VariableSeviceCharge();
        ChargeVariables chargeVariables = new ChargeVariables();
        VariableSeviceChargeOverride variableSeviceChargeOverride = new VariableSeviceChargeOverride();
        AccountAdjustSupportData accountAdjustSupportData = new AccountAdjustSupportData();
        if (bilAccount.getBillTypeCd().equals("SP")) {
            variableSeviceChargeOverride = accountAdjustDatesService.readOverrideServiceCharge(bilAccount);
            if (variableSeviceChargeOverride == null) {
                variableSeviceChargeOverride = new VariableSeviceChargeOverride();
            }
            if (accountAdjustDates.getAddDateIndicator() != CHAR_Y && accountAdjustDates.getAddDateIndicator() != CHAR_Q
                    && accountAdjustDates.getAddDateIndicator() != CHAR_S
                    && accountAdjustDates.getAddDateIndicator() != CHAR_F
                    && variableSeviceChargeOverride.getRuleInd() != CHAR_Y) {
                Integer count = bilSptPlnCrgRepository.getCount(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                        SHORT_ZERO, CHAR_Y, "GA");
                if (count != null && count > SHORT_ZERO) {
                    accountAdjustSupportData.setServiceChargeIndicator(CHAR_Y);
                    accountAdjustSupportData.setServiceChargeTypeCode("GA");
                } else {
                    accountAdjustSupportData.setServiceChargeIndicator(BLANK_CHAR);
                    accountAdjustSupportData.setServiceChargeTypeCode(BLANK_STRING);
                }
            }
        }

        List<BilEftPlnCrg> bilEftPlanChargeList = null;
        if (variableSeviceChargeOverride.getRuleInd() != CHAR_Y && adjustDatesReviewDetails.isEftCollectionMethod()) {
            bilEftPlanChargeList = bilEftPlnCrgRepository.findByCollectionPlan(bilAccount.getCollectionPlan());

        }

        List<BilCrgAmounts> downPaymentList = null;
        DownPaymentFee downPaymentFee = new DownPaymentFee();
        if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y
                && (adjustDatesReviewDetails.isDpifRule() || accountAdjustDates.getSupportPlanIndicator() == CHAR_Y)) {
            downPaymentList = accountAdjustDatesService.getFutureDownpaymentRows(accountAdjustDates.getAccountId(),
                    downPaymentFee);
        }

        List<BilSptPlnCrg> bilSupportPlanChargeList = bilSptPlnCrgRepository
                .findByBilTypeCodeClassCode(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd());

        if (bilSupportPlanChargeList.size() == SHORT_ONE) {
            BilSptPlnCrg bilSptPlnCrg = bilSupportPlanChargeList.get(SHORT_ZERO);
            accountAdjustSupportData.setServiceChargeIndicator(bilSptPlnCrg.getBilSptScgInd());
            accountAdjustSupportData.setServiceChargeTypeCode(bilSptPlnCrg.getBilScgTypeCd());
            accountAdjustSupportData.setServiceChargeCode(bilSptPlnCrg.getBilScgActionCd());
            accountAdjustSupportData.setServiceChargeAmount(bilSptPlnCrg.getBilSvcCrgAmt());
            accountAdjustSupportData.setPremiumAmount(bilSptPlnCrg.getBilPrmAmt());
            accountAdjustSupportData.setSeviceChargeIncAmount(bilSptPlnCrg.getBilScgIcrAmt());
            accountAdjustSupportData.setPremiumIncAmount(bilSptPlnCrg.getBilPrmIcrAmt());
            accountAdjustSupportData.setMaxServiceChargeAmount(bilSptPlnCrg.getBilMaxScgAmt());
        } else {
            accountAdjustSupportData.setServiceChargeIndicator(bilSupportPlan.getBspScgChgInd());
            accountAdjustSupportData.setServiceChargeTypeCode(bilSupportPlan.getBspScgchgTypCd());
            accountAdjustSupportData.setServiceChargeCode(bilSupportPlan.getBspSvcCd());
            accountAdjustSupportData.setServiceChargeAmount(bilSupportPlan.getBspScgAmt());
            accountAdjustSupportData.setPremiumAmount(bilSupportPlan.getBspPrmAmt());
            accountAdjustSupportData.setSeviceChargeIncAmount(bilSupportPlan.getBspScgIncAmt());
            accountAdjustSupportData.setPremiumIncAmount(bilSupportPlan.getBspPrmIncAmt());
            accountAdjustSupportData.setMaxServiceChargeAmount(bilSupportPlan.getBspMaxScgAmt());
        }
        boolean isXHOLRule = false;
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(RULE_XHOL, BLANK_STRING, BLANK_STRING,
                BLANK_STRING, accountAdjustDates.getProcessDate(), accountAdjustDates.getProcessDate());

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == 'Y' && bilRulesUct.getBrtParmListTxt() != null
                && !bilRulesUct.getBrtParmListTxt().trim().isEmpty()) {
            isXHOLRule = true;

        }

        char previousSchedule = ScheduleCode.NO_SCHEDULE.getValue();
        if (fpddIndicator == CHAR_Y) {
            previousSchedule = accountAdjustDatesService.getPreviousSchedule(adjustDatesReviewDetails.isAccountBilled(),
                    accountAdjustDates);
            if (previousSchedule == ScheduleCode.FUTURE_SCHEDULE.getValue()) {
                previousSchedule = accountAdjustDatesService.getNonPreviousSchedule(accountAdjustDates,
                        bilSupportPlan.getBspMinInvAmt());
            }
        }

        FirstPossibleDueDate firstPossibleDueDate = firstPossibleDueDateService.processFirstPossibleDueDateLogic(
                bilActRules.getBruIncWknInd(), isXHOLRule, bilRulesUct, accountAdjustDates, adjustDatesReviewDetails);

        Character invoiceCode = bilDatesRepository.getMaxInvoiceCodeByDueDateInvoiceDate(
                accountAdjustDates.getAccountId(), accountAdjustDates.getCurrentDueDate(),
                accountAdjustDates.getInvoiceDate(), CHAR_X);

        if (invoiceCode == null) {
            if (accountAdjustDates.getDateResetIndicator() == CHAR_T) {
                invoiceCode = CHAR_Y;
            } else {
                throw new DataNotFoundException("invoice.code.not.found",
                        new Object[] { accountAdjustDates.getAccountId() });

            }
        }
        ZonedDateTime invoiceDate = accountAdjustDates.getInvoiceDate();
        boolean isInvoice = false;
        boolean isexit = false;
        if (invoiceCode == CHAR_A || invoiceCode == BLANK_CHAR) {
            invoiceDate = firstPossibleDueDateService.getFpddInvoiceDate(accountAdjustDates.getNewDueDate(),
                    accountAdjustDates.getProcessDate(), bilActRules.getBruIncWknInd(),
                    adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, bilRulesUct);
        } else {
            isInvoice = true;
            if (invoiceCode == CHAR_Y) {
                if (accountAdjustDates.getDateResetIndicator() == CHAR_T) {
                    ttyInvChange(accountAdjustDates, invoiceDate, invoiceCode, bilSupportPlan,
                            adjustDatesReviewDetails);
                    isexit = true;
                } else {
                    bilActSummaryRepository.updateBilAtSummaryRowsByOldDate(accountAdjustDates.getCurrentDueDate(),
                            accountAdjustDates.getUserSequenceId(), accountAdjustDates.getProcessDate(),
                            accountAdjustDates.getAccountId(), invoiceDate, accountAdjustDates.getCurrentDueDate(),
                            Arrays.asList("INV", "CNV", "ENV"), SEPARATOR_BLANK);
                }
            }
        }

        char dateType;
        if (!isexit) {
            char nonReceiptIndicator = CHAR_Y;
            if (bilSupportPlan.getBspNoncpiLetInd() == CHAR_N) {
                nonReceiptIndicator = CHAR_N;
            }
            dateType = checkOcbDate(accountAdjustDates, invoiceCode, invoiceDate, nonReceiptIndicator);
            if (dateType != CHAR_O) {
                bilDatesRepository.updateOneDateBilDatesByNewAdjDueDate(accountAdjustDates.getNewDueDate(), invoiceDate,
                        CHAR_Y, nonReceiptIndicator, invoiceCode, accountAdjustDates.getAccountId(),
                        accountAdjustDates.getCurrentDueDate(), accountAdjustDates.getInvoiceDate());
            }

            bilIstScheduleRepository.updateRescindScheduleRowByAdjustDate(accountAdjustDates.getNewDueDate(),
                    invoiceDate, invoiceCode, accountAdjustDates.getAccountId(), accountAdjustDates.getCurrentDueDate(),
                    accountAdjustDates.getInvoiceDate());

            if (invoiceCode == CHAR_Y || invoiceCode == 'L') {

                bilCashDspRepository.updateRescindCashRowByAdjDueDate(accountAdjustDates.getNewDueDate(), invoiceDate,
                        accountAdjustDates.getAccountId(), accountAdjustDates.getCurrentDueDate(),
                        accountAdjustDates.getInvoiceDate());
            } else {
                bilCashDspRepository.updateRescindCashRowByAdjDueDate(accountAdjustDates.getNewDueDate(), invoiceDate,
                        accountAdjustDates.getAccountId(), accountAdjustDates.getCurrentDueDate(),
                        accountAdjustDates.getInvoiceDate(), ChargeType.DOWNPAYMENTCHARGES.getChargeType());
            }

            bilCreScheduleRepository.updateRescindCreScheduleRowBybilAdjDueDt(accountAdjustDates.getNewDueDate(),
                    invoiceDate, accountAdjustDates.getAccountId(), accountAdjustDates.getCurrentDueDate(),
                    accountAdjustDates.getInvoiceDate());

            bilCrgAmountsRepository.updateRescindCrgAmountsBybilAdjDueDt(accountAdjustDates.getNewDueDate(),
                    invoiceDate, accountAdjustDates.getAccountId(), accountAdjustDates.getCurrentDueDate(),
                    accountAdjustDates.getInvoiceDate(), AdjustDueDatesConstants.getChargeTypes());

            if (isInvoice) {
                bilCrgAmountsRepository.updateRescindCrgAmountsBybilAdjDueDt(accountAdjustDates.getNewDueDate(),
                        invoiceDate, accountAdjustDates.getAccountId(), accountAdjustDates.getCurrentDueDate(),
                        accountAdjustDates.getInvoiceDate(),
                        Arrays.asList(ChargeType.DOWNPAYMENTCHARGES.getChargeType(),
                                ChargeType.SERVICECHARGES.getChargeType()));
            }
        }

        Object[] newDatesData = firstPossibleDueDateService.checkDateAdd(true, adjustDatesReviewDetails, bilAccount,
                accountAdjustDates, isXHOLRule, bilRulesUct);
        ZonedDateTime newReferemceDate = (ZonedDateTime) newDatesData[0];
        ZonedDateTime newAdjustDate = (ZonedDateTime) newDatesData[1];
        ZonedDateTime newInvoiceDate = (ZonedDateTime) newDatesData[2];
        ZonedDateTime newSystemDate = (ZonedDateTime) newDatesData[3];
        dateType = (char) newDatesData[4];
        adjustDatesReviewDetails.setNewAdjustDueDate(newAdjustDate);
        adjustDatesReviewDetails.setNewReferenceDate(newReferemceDate);
        adjustDatesReviewDetails.setNewInvoiceDate(newInvoiceDate);
        adjustDatesReviewDetails.setNewSystemDate(newSystemDate);
        adjustDatesReviewDetails.setDateType(dateType);

        accountAdjustDatesService.processDatesAssignment(dateType, bilAccount, accountAdjustDates,
                adjustDatesReviewDetails, firstPossibleDueDate, variableSeviceChargeOverride, variableSeviceCharge,
                chargeVariables, accountAdjustSupportData, disableFpdd, bilEftPlanChargeList, bilSupportPlanChargeList,
                isXHOLRule, bilRulesUct, bilSupportPlan);

        accountAdjustDatesService.processChargeRows(SHORT_ZERO, previousSchedule, bilAccount, accountAdjustDates,
                firstPossibleDueDate, adjustDatesReviewDetails, isXHOLRule, bilRulesUct, bilEftPlanChargeList,
                bilSupportPlanChargeList, variableSeviceChargeOverride, variableSeviceCharge, accountAdjustSupportData,
                chargeVariables, downPaymentList, downPaymentFee);
    }

    private void ttyInvChange(AccountAdjustDates accountAdjustDates, ZonedDateTime invoiceDate, Character invoiceCode,
            BilSupportPlan bilSupportPlan, AdjustDatesReviewDetails adjustDatesReviewDetails) {
        bilActSummaryRepository.updateBilAtSummaryRowsByOldDate(accountAdjustDates.getNewDueDate(),
                accountAdjustDates.getNewDueDate(), accountAdjustDates.getUserSequenceId(),
                accountAdjustDates.getProcessDate(), accountAdjustDates.getAccountId(),
                accountAdjustDates.getCurrentDueDate(), Arrays.asList("STT", "CST", "ETT"), SEPARATOR_BLANK);

        ZonedDateTime systemDueDate = null;
        List<BilDates> bilDatesList = bilDatesRepository.getMaxSystemDueDateByInvoiceCode(
                accountAdjustDates.getAccountId(), accountAdjustDates.getNewDueDate(),
                AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            systemDueDate = bilDatesList.get(SHORT_ZERO).getBilSysDueDt();
        } else {
            BilDates bilDates = insertBilDates(accountAdjustDates.getAccountId(), accountAdjustDates.getNewDueDate(),
                    null, invoiceDate, accountAdjustDates.getNewDueDate(), invoiceCode, CHAR_Y, CHAR_T, bilSupportPlan,
                    adjustDatesReviewDetails);
            systemDueDate = bilDates.getBilSysDueDt();
        }
        bilIstScheduleRepository.updateScheduleRowsByOldScheduleDate(systemDueDate, invoiceDate,
                accountAdjustDates.getNewDueDate(), accountAdjustDates.getAccountId(),
                accountAdjustDates.getCurrentDueDate(), accountAdjustDates.getInvoiceDate());
        bilCreScheduleRepository.updateCreScheduleRowByOldScheduleDate(systemDueDate,
                accountAdjustDates.getNewDueDate(), invoiceDate, accountAdjustDates.getAccountId(),
                accountAdjustDates.getCurrentDueDate(), accountAdjustDates.getInvoiceDate());
        bilCashDspRepository.updateCashRowByOldScheduleDate(systemDueDate, invoiceDate,
                accountAdjustDates.getNewDueDate(), accountAdjustDates.getAccountId(),
                accountAdjustDates.getCurrentDueDate(), accountAdjustDates.getInvoiceDate());

    }

    private BilDates insertBilDates(String accountId, ZonedDateTime maxSystemDueDate, ZonedDateTime maxReferenceDueDate,
            ZonedDateTime newInvDate, ZonedDateTime adjustDate, char invoiceCode, char dateIndicator, char dateType,
            BilSupportPlan bilSupportPlan, AdjustDatesReviewDetails adjustDatesReviewDetails) {

        BilDates bilDates = new BilDates();
        bilDates.setBilDatesId(new BilDatesId(accountId, maxReferenceDueDate));
        bilDates.setBilSysDueDt(maxSystemDueDate);
        bilDates.setBdtDateInd(dateIndicator);
        bilDates.setBdtLateCrgInd(CHAR_N);
        bilDates.setBilInvoiceCd(invoiceCode);
        if (bilSupportPlan.getBspNoncpiLetInd() == CHAR_N) {
            bilDates.setBdtNoncpiInd(CHAR_N);
        } else {
            bilDates.setBdtNoncpiInd(CHAR_Y);
        }
        bilDates.setBdtDateType(dateType);
        bilDates.setBilInvDt(newInvDate);

        bilDates.setBilAdjDueDt(adjustDate);

        bilDates.setBilExcInd(BLANK_CHAR);
        if (adjustDatesReviewDetails.isSeasonalBill() && dateIndicator != 'Y') {
            char blackoutDtInd = getSeasonalDate(accountId, bilDates.getBilSysDueDt());
            if (blackoutDtInd == 'Y') {
                bilDates.setBilExcInd('X');
            } else {
                blackoutDtInd = getSeasonalDate(accountId, bilDates.getBilAdjDueDt());
                if (blackoutDtInd == 'Y') {
                    bilDates.setBilExcInd('X');
                }
            }

        }

        bilDatesRepository.saveAndFlush(bilDates);
        return bilDates;

    }

    private char getSeasonalDate(String accountId, ZonedDateTime deductionDt) {
        char blackoutDtInd = CHAR_N;
        BilSeasExcl bilSeasExcl = bilSeasExclRepository.getBilSeaRowByDate(accountId, deductionDt);
        if (bilSeasExcl != null) {
            blackoutDtInd = CHAR_Y;
        }
        return blackoutDtInd;
    }

    private Character checkOcbDate(AccountAdjustDates accountAdjustDates, Character invoiceCode,
            ZonedDateTime invoiceDate, char nonReceiptIndicator) {
        Character dateType = bilDatesRepository.getBilDateType(accountAdjustDates.getAccountId(),
                accountAdjustDates.getCurrentDueDate(), accountAdjustDates.getInvoiceDate(), invoiceCode, CHAR_X);
        if (dateType == null) {
            throw new DataNotFoundException("date.type.not.found", new Object[] { accountAdjustDates.getAccountId() });
        }

        if (dateType != CHAR_O) {
            return dateType;
        }

        List<BilDates> bilDatesList = bilDatesRepository.getMaxSystemDueDateByInvoiceCode(
                accountAdjustDates.getAccountId(), accountAdjustDates.getNewDueDate(),
                AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
        if (bilDatesList == null || bilDatesList.isEmpty()) {
            bilDatesRepository.updateOneDateBilDatesByAdjDueDate(accountAdjustDates.getNewDueDate(), invoiceDate,
                    CHAR_Y, CHAR_S, nonReceiptIndicator, invoiceCode, accountAdjustDates.getAccountId(),
                    accountAdjustDates.getCurrentDueDate(), accountAdjustDates.getInvoiceDate());
        } else {
            BilDates bilDates = bilDatesList.get(SHORT_ZERO);
            List<BilDates> oldBilDatesList = bilDatesRepository.getSystemDateByInvoiceDate(
                    accountAdjustDates.getAccountId(), accountAdjustDates.getCurrentDueDate(),
                    accountAdjustDates.getInvoiceDate(), invoiceCode, CHAR_O, CHAR_X);
            if (oldBilDatesList != null && !oldBilDatesList.isEmpty()) {
                BilDates oldBilDates = oldBilDatesList.get(SHORT_ZERO);
                bilDatesRepository.deleteRowsByDateType(accountAdjustDates.getAccountId(),
                        accountAdjustDates.getCurrentDueDate(), accountAdjustDates.getInvoiceDate(), CHAR_O);
                bilCrgAmountsRepository.deleteServiceChargesByDates(accountAdjustDates.getAccountId(),
                        accountAdjustDates.getCurrentDueDate(), accountAdjustDates.getInvoiceDate(),
                        ChargeType.SERVICECHARGES.getChargeType(), AdjustDueDatesConstants.getFutureInvoiceCodes());

                bilIstScheduleRepository.updateScheduleRowsByOldScheduleDate(bilDates.getBilSysDueDt(),
                        bilDates.getBilDatesId().getBilReferenceDt(), accountAdjustDates.getAccountId(),
                        oldBilDates.getBilDatesId().getBilReferenceDt(), oldBilDates.getBilSysDueDt(),
                        accountAdjustDates.getCurrentDueDate(), accountAdjustDates.getInvoiceDate(), invoiceCode);
                bilCreScheduleRepository.updateCreScheduleRowByOldScheduleDate(bilDates.getBilSysDueDt(),
                        accountAdjustDates.getAccountId(), oldBilDates.getBilSysDueDt(),
                        accountAdjustDates.getCurrentDueDate(), accountAdjustDates.getInvoiceDate());
                bilCashDspRepository.updateCashRowByOldScheduleDate(bilDates.getBilSysDueDt(),
                        accountAdjustDates.getAccountId(), oldBilDates.getBilSysDueDt(),
                        accountAdjustDates.getCurrentDueDate(), accountAdjustDates.getInvoiceDate());
            }

        }
        return dateType;
    }

}