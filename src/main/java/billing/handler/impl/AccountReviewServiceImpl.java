package billing.handler.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActInquiry;
import billing.data.entity.BilActRules;
import billing.data.entity.BilIstSchedule;
import billing.data.entity.BilStRulesUct;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.BilSystemCfg;
import billing.data.entity.id.BilActInquiryId;
import billing.data.entity.id.BilActRulesId;
import billing.data.entity.id.BilSupportPlanId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActRulesRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilSptPlnCrgRepository;
import billing.data.repository.BilStRulesUctRepository;
import billing.data.repository.BilSupportPlanRepository;
import billing.data.repository.BilSystemCfgRepository;
import billing.handler.model.AccountReview;
import billing.handler.model.AccountReviewDetails;
import billing.handler.service.AccountReviewService;
import billing.handler.service.BillingRulesService;
import billing.model.AccountHistory;
import billing.service.AccountSummaryService;
import billing.utils.AccountOperationsWrapper;
import billing.utils.BillingConstants.AccountStatus;
import billing.utils.BillingConstants.ActivityType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.ChargeType;
import billing.utils.BillingConstants.InvoiceTypeCode;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.utils.DateRoutine;

@Service("accountReviewService")
public class AccountReviewServiceImpl implements AccountReviewService {

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BillingRulesService billingRulesService;

    @Autowired
    private BilSupportPlanRepository bilSupportPlanRepository;

    @Autowired
    private BilActRulesRepository bilActRulesRepository;

    @Autowired
    private BilSptPlnCrgRepository bilSptPlnCrgRepository;

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private DateService dateService;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilStRulesUctRepository bilStRulesUctRepository;

    @Autowired
    private AccountSummaryService accountSummaryService;

    @Autowired
    private BilSystemCfgRepository bilSystemCfgRepository;

    public static final Logger logger = LogManager.getLogger(AccountReviewServiceImpl.class);
    private static final char PLUS_SIGN = '+';
    private static final char SUSPEND_DISBURSEMENT = 'D';
    private static final char ACCT_IS_SPLIT = CHAR_Y;

    @Override
    @Transactional
    public AccountReviewDetails accountProcessReview(AccountReview accountReview) {
        char suspendBillIndicator = BLANK_CHAR;
        BilAccount bilAccount = bilAccountRepository.findById(accountReview.getAccountId()).orElse(null);
        if (bilAccount == null) {
            throw new DataNotFoundException("no.bil.account.exists", new Object[] { accountReview.getAccountId() });
        }

        if (bilAccount.getStatus() == AccountStatus.SUSPEND_BILLING_FOLLOW_UP.toChar()
                || bilAccount.getStatus() == AccountStatus.SUSPEND_FOLLOW_UP.toChar()) {
            suspendBillIndicator = CHAR_Y;
        }

        BilSupportPlan bilSupportPlan = getSupportPlan(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd());

        BilActRules bilActRules = readBilActRules(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd());

        boolean lateCharge = false;
        if (accountReview.getQuoteIndicator().charAt(0) != CHAR_Q) {
            Integer count = bilSptPlnCrgRepository.countByTypeClassCd(bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd(), DECIMAL_ZERO, DECIMAL_ZERO);
            if (count != null) {
                lateCharge = true;
            }
        }

        ZonedDateTime nextRecoveryDate = getNextRecoveryDate(accountReview.getRecoveryDate(),
                bilActRules.getBruIncWknInd());

        if (accountReview.getCashStatusCode() == SUSPEND_DISBURSEMENT
                || bilAccount.getCashStatusCd() == SUSPEND_DISBURSEMENT) {
            resetDisbursementTrigger(accountReview.getAccountId(), bilActRules.getBruDsbRctDtInd());
        }

        AccountReviewDetails accountReviewDetails = new AccountReviewDetails();
        accountReviewDetails.setBilAccount(bilAccount);
        accountReviewDetails.setBilSupportPlan(bilSupportPlan);
        accountReviewDetails.setBilActRules(bilActRules);
        accountReviewDetails.setNextRecoveryDate(nextRecoveryDate);
        accountReviewDetails.setLateCharge(lateCharge);
        accountReviewDetails.setInvoiceRequire(true);
        accountReviewDetails.setSuspendBillIndicator(suspendBillIndicator);

        return accountReviewDetails;
    }

    @Override
    public boolean processAccountStatus(String accountId, char status, String suspendedFullReasonCd,
            char renewDownpayBillIndicator) {
        boolean isInvoiceRequire = true;
        Double accountBalance = bilIstScheduleRepository.findBillingAccountBalance(accountId);
        if (accountBalance == null) {
            accountBalance = DECIMAL_ZERO;
        }
        if (accountBalance == DECIMAL_ZERO) {
            List<BilIstSchedule> bilIstSchedules = bilIstScheduleRepository.getScheduleInvoicedRows(accountId,
                    getInvoiceCode());
            if (bilIstSchedules == null && renewDownpayBillIndicator != CHAR_Y) {
                isInvoiceRequire = billingRulesService.checkForRenewDownpay(accountId);
            }
        }

        if (accountBalance != DECIMAL_ZERO) {
            updateActiveStatus(accountId, status, AccountStatus.INACTIVE.toChar(), AccountStatus.ACTIVE.toChar(),
                    suspendedFullReasonCd);
        } else {
            Double accountServiceCharge = bilCrgAmountsRepository.getCollectionScgBalanceAmount(accountId,
                    getChargeTypeList(), ChargeType.SERVICECHARGES.getChargeType(), getInvoiceCode());
            if (accountServiceCharge == null) {
                accountServiceCharge = DECIMAL_ZERO;
            }
            if (accountServiceCharge != 0) {
                isInvoiceRequire = true;
                updateActiveStatus(accountId, status, AccountStatus.INACTIVE.toChar(), AccountStatus.ACTIVE.toChar(),
                        suspendedFullReasonCd);
            } else {
                Character invoiceCd = bilCrgAmountsRepository.getInvoiceCd(accountId, getInvoiceCode(),
                        new ArrayList<>(Arrays.asList(ChargeType.PENALTYCHARGES.getChargeType(),
                                ChargeType.LATECHARGES.getChargeType())));
                if (invoiceCd != null) {
                    isInvoiceRequire = true;
                }
            }

            if (status == AccountStatus.SUSPEND_BILLING_FOLLOW_UP.toChar() && suspendedFullReasonCd.compareTo("AC") == 0
                    || suspendedFullReasonCd.compareTo(BilDspTypeCode.APPLIED.getValue()) == 0) {
                return isInvoiceRequire;
            }

            updateActiveStatus(accountId, status, AccountStatus.ACTIVE.toChar(), AccountStatus.INACTIVE.toChar(),
                    suspendedFullReasonCd);

        }
        return isInvoiceRequire;
    }

    private void updateActiveStatus(String accountId, char accountStatus, char currentStatus, char newStatus,
            String suspendedFullReasonCd) {
        if (!(accountStatus == AccountStatus.SUSPEND_BILLING_FOLLOW_UP.toChar()
                && (suspendedFullReasonCd.equalsIgnoreCase(BilDspTypeCode.APPLIED.getValue())
                        || suspendedFullReasonCd.equalsIgnoreCase("AC")))) {
            BilAccount bilAccountUpdate = bilAccountRepository.findById(accountId).orElse(null);
            if (bilAccountUpdate != null && bilAccountUpdate.getStatus() == currentStatus) {
                bilAccountUpdate.setStatus(newStatus);
                bilAccountRepository.save(bilAccountUpdate);
            }
        }
    }

    private ZonedDateTime getNextRecoveryDate(ZonedDateTime recoveryDate, char weekendInd) {
        ZonedDateTime nextRecoveryDate = null;
        Boolean isExcludeWeekend = billingRulesService.overrideWkendInd(CHAR_Y, weekendInd);
        if (recoveryDate != null) {
            nextRecoveryDate = DateRoutine.adjustDateWithOffset(recoveryDate, isExcludeWeekend, 0, PLUS_SIGN,
                    recoveryDate);
        }
        return nextRecoveryDate;
    }

    private void resetDisbursementTrigger(String accountId, char disburRctDateInd) {
        if (disburRctDateInd != CHAR_Y) {
            updateDisburseTrigger(accountId);
        } else {
            updateOtherDisburseTrigger(accountId);
        }
    }

    private void updateDisburseTrigger(String accountId) {
        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(accountId, ActivityType.ACCOUNT_DISBURSEMENT.getValue())).orElse(null);
        if (bilActInquiry != null) {
            bilActInquiry.setBilNxtAcyDt(DateRoutine.defaultDateTime());
            bilActInquiryRepository.saveAndFlush(bilActInquiry);
        }
    }

    private void updateOtherDisburseTrigger(String accountId) {
        bilActInquiryRepository.updateOtherDisbTriggers(DateRoutine.defaultDateTime(), accountId, getTypeCodeList());
        // Note - Use Default Date Once. Check for CR.
        bilCashDspRepository.updateDsbDtByBilAccontId(DateRoutine.defaultDateTime(), accountId,
                BilDspTypeCode.SUSPENDED.getValue(), BLANK_CHAR, CHAR_Y, DateRoutine.defaultDateTime(), "CR");
    }

    private BilSupportPlan getSupportPlan(String billTypeCd, String billClassCd) {
        BilSupportPlan bilSupportPlan = bilSupportPlanRepository.findById(new BilSupportPlanId(billTypeCd, billClassCd))
                .orElse(null);
        if (bilSupportPlan == null) {
            logger.error("Rows not found in bil support plan");
            throw new DataNotFoundException("supportData.notfound", new Object[] { billTypeCd, billTypeCd });
        }
        return bilSupportPlan;
    }

    private BilActRules readBilActRules(String billTypeCd, String billClassCd) {
        BilActRules bilActRules = null;
        bilActRules = bilActRulesRepository.findById(new BilActRulesId(billTypeCd, billClassCd)).orElse(null);
        if (bilActRules == null) {
            bilActRules = new BilActRules();
            bilActRules.setBilActRulesId(new BilActRulesId(BLANK_STRING, BLANK_STRING));
            bilActRules.setBruDsbRctDtInd(CHAR_N);
            bilActRules.setBruRnlQteInd(CHAR_N);
            bilActRules.setBruIncWknInd(CHAR_N);
            bilActRules.setBruCcrCreditInd(CHAR_N);
            bilActRules.setBruDsbRctDtInd(CHAR_N);
            bilActRules.setBruCcrNbrDays(SHORT_ZERO);
            bilActRules.setBruMinWroInd(CHAR_N);
        }
        return bilActRules;
    }

    private List<String> getTypeCodeList() {
        List<String> typeCodeList = new ArrayList<>();
        typeCodeList.add(ActivityType.DISBURSE_CREDIT.getValue());
        typeCodeList.add(ActivityType.DISBURSE_ACH.getValue());
        typeCodeList.add(ActivityType.DISBURSE_EFT_CREDIT.getValue());
        typeCodeList.add(ActivityType.DISBURSE_EFT.getValue());
        typeCodeList.add(ActivityType.DISBURSE_CHECK_DRAFT.getValue());
        typeCodeList.add(ActivityType.ONETIME_ACH.getValue());
        return typeCodeList;
    }

    private List<Character> getInvoiceCode() {
        List<Character> invoiceCodeList = new ArrayList<>();
        invoiceCodeList.add(InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue());
        invoiceCodeList.add(InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());
        invoiceCodeList.add(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue());
        return invoiceCodeList;
    }

    private List<Character> getChargeTypeList() {
        List<Character> bilChargeTypeList = new ArrayList<>();
        bilChargeTypeList.add(ChargeType.PENALTYCHARGES.getChargeType());
        bilChargeTypeList.add(ChargeType.LATECHARGES.getChargeType());
        bilChargeTypeList.add(ChargeType.DOWNPAYMENTCHARGES.getChargeType());
        return bilChargeTypeList;
    }

    @Override
    public void writeHistory(String accountId, String activityDesCd, String activityDesType, String policyNumber,
            String policySymbolCd, ZonedDateTime activityDesc1Dt, ZonedDateTime activityDesc2Dt, double amount,
            String additionalDataTxt, String userId) {
        AccountHistory accountHistory = AccountOperationsWrapper.createActivity(activityDesCd, activityDesType,
                policyNumber, policySymbolCd, activityDesc1Dt, activityDesc2Dt, amount, additionalDataTxt, userId);
        List<AccountHistory> accountHistories = new ArrayList<>();
        accountHistories.add(accountHistory);
        accountSummaryService.writeAccountSummary(accountId, accountHistories);
    }

    @Override
    public char checkSplitIndicator(String policyId, ZonedDateTime polEffectiveDate) {
        char splitIndicator = CHAR_N;
        Short splitCount = bilPolicyTermRepository.countBilPolicyTermByStatus(policyId, polEffectiveDate,
                BilPolicyStatusCode.TRANSFERRED.getValue());
        if (splitCount != null && splitCount > 1) {
            splitIndicator = ACCT_IS_SPLIT;
        }
        return splitIndicator;
    }

    @Override
    public BilStRulesUct getBilStRuleUct(String ruleId, String billStatePvnCode, String lobCode,
            String masterCompanyNbr) {
        List<BilStRulesUct> bilStRulesUctList = bilStRulesUctRepository.getRulesRow(ruleId,
                dateService.currentDate(), dateService.currentDate(),
                new ArrayList<>(Arrays.asList(billStatePvnCode, BLANK_STRING)),
                new ArrayList<>(Arrays.asList(lobCode, BLANK_STRING)),
                new ArrayList<>(Arrays.asList(masterCompanyNbr, BLANK_STRING, "99")));
        if (bilStRulesUctList != null && !bilStRulesUctList.isEmpty()) {
            return bilStRulesUctList.get(0);
        }
        return null;
    }

    @Override
    public char getQueueWrtIndicator() {
        char queueWrtIndicator = BLANK_CHAR;
        BilSystemCfg bilSystemCfg = bilSystemCfgRepository.findRow();
        if (bilSystemCfg != null) {
            queueWrtIndicator = bilSystemCfg.getBilSystemCfgId().getBscQueueWrtInd();
        }
        return queueWrtIndicator;
    }

}
