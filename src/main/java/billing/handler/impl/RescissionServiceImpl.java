package billing.handler.impl;

import static billing.utils.BillingConstants.SYSTEM_USER_ID;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_C;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_E;
import static core.utils.CommonConstants.CHAR_I;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilBillPlan;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilIstSchedule;
import billing.data.entity.BilObjectCfg;
import billing.data.entity.BilPolActivity;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRenDays;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSeasExcl;
import billing.data.entity.BilStRulesUct;
import billing.data.entity.BilTtySeasExcl;
import billing.data.entity.HalBoMduXrf;
import billing.data.entity.HalUniversalCtl2;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.entity.id.BilObjectCfgId;
import billing.data.entity.id.BilPolActivityId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.entity.id.BilRenDaysId;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilObjectCfgRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilRenDaysRepository;
import billing.data.repository.BilSeasExclRepository;
import billing.data.repository.BilTtySeasExclRepository;
import billing.data.repository.HalBoMduXrfRepository;
import billing.data.repository.HalUniversalCtl2Repository;
import billing.handler.impl.PolicyReviewServiceImpl.NsfPcnDataType;
import billing.handler.impl.PolicyReviewServiceImpl.TermAcyType;
import billing.handler.impl.SplitBillingActivityServiceImpl.NoncausalType;
import billing.handler.model.AccountProcessingAndRecovery;
import billing.handler.model.AccountReviewDetails;
import billing.handler.model.PartialOrLatePaymentOnPcnPrint;
import billing.handler.model.PcnRescindCollection;
import billing.handler.model.RecoveryAccount;
import billing.handler.model.Rescission;
import billing.handler.model.StaticPcnRescind;
import billing.handler.service.AccountProcessingAndRecoveryService;
import billing.handler.service.AccountReviewService;
import billing.handler.service.CancellationService;
import billing.handler.service.PartialOrLatePaymentOnPcnPrintService;
import billing.handler.service.RecoveryService;
import billing.handler.service.RescissionService;
import billing.handler.service.StaticPcnRescindService;
import billing.model.RescindNotice;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants.AccountTypeCode;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CreditIndicator;
import billing.utils.BillingConstants.EventCode;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import billing.utils.BillingConstants.WipAcitivityId;
import billing.utils.PB360Service;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.schedule.service.ScheduleService;
import core.utils.DateRoutine;

@Service("rescissionService")
public class RescissionServiceImpl implements RescissionService {

    public static final Logger logger = LogManager.getLogger(RescissionServiceImpl.class);

    @Autowired
    private AccountReviewService accountReviewService;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private HalUniversalCtl2Repository halUniversalCtl2Repository;

    @Autowired
    private HalBoMduXrfRepository halBoMduXrfRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private RecoveryService recoveryService;

    @Autowired
    private BilRenDaysRepository bilRenDaysRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilObjectCfgRepository bilObjectCfgRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private AccountProcessingAndRecoveryService accountProcessingAndRecoveryService;

    @Autowired
    private BilSeasExclRepository bilSeasExclRepository;

    @Autowired
    private BilTtySeasExclRepository bilTtySeasExclRepository;

    @Autowired
    private CancellationService cancellationService;

    @Autowired
    private StaticPcnRescindService staticPcnRescindService;

    @Autowired
    private PB360Service pb360Service;

    @Autowired
    private DateService dateService;

    @Autowired
    private PartialOrLatePaymentOnPcnPrintService partialOrLatePaymentOnPcnPrintService;

    @Autowired
    private BillingApiService billingApiService;

    private static final char PLUS_SIGN = '+';
    private static final char MINUS_SIGN = '-';
    private static final String CANTT_USER = "SYSCANTT";
    private static final char ACCT_IS_SPLIT = CHAR_Y;
    public static final char NO_RESP_RENEWAL = 'R';
    public static final char EQT_RULE_PED = 'P';
    public static final char EQT_RULE_PCN = 'C';
    public static final char EFT = 'E';
    public static final char DRIVER_CA = 'C';
    private static final String START_DATE = "1900-01-01";
    private static final String INIT_DATE = "1900-02-01";
    private static final String BCMOPPC = "BCMOPPC";
    public static final String BCMOAX = "BCMOAX";

    @Override
    public char processRescission(char polStatusCode, AccountProcessingAndRecovery accountProcessingAndRecovery,
            BilAccount bilAccount, BilBillPlan bilBillPlan, BilPolicy bilPolicy, BilPolicyTerm bilPolicyTerm,
            AccountReviewDetails accountReviewDetails, Rescission rescission, Map<String, Object> termDataMap,
            boolean isEquityUsed) {

        ZonedDateTime polEffectiveDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
        String policyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();

        rescind: {
            if (checkTranTransferPending(policyId, polEffectiveDate)) {
                break rescind;
            }
            char splitIndicator = accountReviewService.checkSplitIndicator(policyId, polEffectiveDate);
            if (splitIndicator == ACCT_IS_SPLIT) {
                BilPolicyTerm pcnPolicyTerm = bilPolicyTermRepository
                        .findById(new BilPolicyTermId(rescission.getAccountId(), policyId, polEffectiveDate))
                        .orElse(null);
                if (pcnPolicyTerm != null) {
                    char pcnCausalIndicator = pcnPolicyTerm.getBptPcnCausalInd();
                    if (pcnCausalIndicator == CHAR_N && bilBillPlan.getBilEqyRescCd() != CHAR_N) {
                        processNonCausalRescind(rescission.getAccountId(), bilPolicyTerm,
                                accountProcessingAndRecovery.getRecoveryDate());
                        break rescind;
                    }
                }
            }

            if (checkPriorPcn(bilAccount.getAccountId(), bilPolicy.getPolSymbolCd(), bilPolicy.getPolNbr(),
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt())) {
                break rescind;
            }
            return rescindAccount(polStatusCode, accountProcessingAndRecovery, bilAccount, bilBillPlan, bilPolicy,
                    bilPolicyTerm, accountReviewDetails, splitIndicator, rescission, termDataMap, isEquityUsed);
        }

        return polStatusCode;
    }

    private char rescindAccount(char polStatusCode, AccountProcessingAndRecovery accountProcessingAndRecovery,
            BilAccount bilAccount, BilBillPlan bilBillPlan, BilPolicy bilPolicy, BilPolicyTerm bilPolicyTerm,
            AccountReviewDetails accountReviewDetails, char splitIndicator, Rescission rescission,
            Map<String, Object> termDataMap, boolean isEquityUsed) {

        char statusCode;
        boolean isNsfPcnCancel = (boolean) termDataMap.get(NsfPcnDataType.NSFPCNCANCEL.getValue());
        label1: {

            Object[] rescindObject = rescindByPolicyStatus(polStatusCode, accountProcessingAndRecovery, bilAccount,
                    splitIndicator, bilPolicy, bilPolicyTerm, bilBillPlan, accountReviewDetails, rescission,
                    termDataMap);

            statusCode = (char) rescindObject[0];
            boolean isExit = (boolean) rescindObject[1];
            if (isExit || polStatusCode == CHAR_N && statusCode != CHAR_N) {
                return statusCode;
            }

            Double termPastAmount = (Double) termDataMap.get(TermAcyType.TERMPASTAMOUNT.getValue());
            Double termPastPercent = (Double) termDataMap.get(TermAcyType.TERMPASTPERCENT.getValue());
            Double nsfPcnAmount = (Double) termDataMap.get(NsfPcnDataType.NSFPCNAMOUNT.getValue());

            BilRulesUct spcnRules = bilRulesUctService.readBilRulesUct("SPCN", bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd(), BLANK_STRING);
            char spcnRuleCd = spcnRules.getBrtRuleCd();
            boolean isTermNeverBilled = (boolean) termDataMap.get(NsfPcnDataType.TERMNEVERBILLED.getValue());
            if (spcnRuleCd == CHAR_Y || spcnRuleCd == CHAR_D && !isTermNeverBilled
                    || statusCode == BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue()) {
                StaticPcnRescind staticPcnRescind = callStaticPcnRescind(bilAccount.getAccountId(), bilPolicyTerm,
                        spcnRuleCd, spcnRules.getBrtParmListTxt().trim(), termDataMap, polStatusCode);
                if (staticPcnRescind != null && staticPcnRescind.isStaticPcnRescind()) {
                    termPastAmount = staticPcnRescind.getTermPastAmount();
                    termPastPercent = staticPcnRescind.getTermPastPercent();
                    termDataMap.put(TermAcyType.TERMPASTAMOUNT.getValue(), staticPcnRescind.getTermPastAmount());
                    termDataMap.put(TermAcyType.TERMPASTPERCENT.getValue(), staticPcnRescind.getTermPastPercent());
                }
            }

            if ((bilBillPlan.getBilEqyRescCd() == EQT_RULE_PED || bilBillPlan.getBilEqyRescCd() == EQT_RULE_PCN)
                    && statusCode != BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue()
                    && bilBillPlan.getBbpInvDuringPcnInd() == CHAR_N) {
                statusCode = processByEquityRescind(accountProcessingAndRecovery, bilAccount, statusCode, bilBillPlan,
                        splitIndicator, bilPolicy, bilPolicyTerm, accountReviewDetails, nsfPcnAmount, termPastAmount,
                        rescission, isNsfPcnCancel);
            }

            if (bilBillPlan.getBbpInvDuringPcnInd() == CHAR_N && (bilBillPlan.getBbpCoffCncAmt() == 0
                    && bilBillPlan.getBbpCoffCncPct() == 0 && termPastAmount == 0
                    || bilBillPlan.getBbpCoffCncAmt() != 0 && bilBillPlan.getBbpCoffCncPct() != 0
                            && termPastAmount < bilBillPlan.getBbpCoffCncAmt()
                            && termPastPercent < bilBillPlan.getBbpCoffCncPct()
                    || bilBillPlan.getBbpCoffCncPct() != 0 && bilBillPlan.getBbpCoffCncAmt() == 0
                            && termPastPercent < bilBillPlan.getBbpCoffCncPct()
                    || bilBillPlan.getBbpCoffCncPct() == 0 && bilBillPlan.getBbpCoffCncAmt() != 0
                            && termPastAmount < bilBillPlan.getBbpCoffCncAmt())) {

                statusCode = processByCalloffCancellation(statusCode, accountProcessingAndRecovery, bilAccount,
                        splitIndicator, termPastAmount, bilPolicy, bilPolicyTerm, accountReviewDetails, nsfPcnAmount,
                        rescission, isNsfPcnCancel);
                break label1;
            }

            ZonedDateTime nextReviewDate = processByDriverQuoteInd(statusCode, accountProcessingAndRecovery,
                    bilAccount.getAccountId(), bilBillPlan, bilPolicyTerm, bilPolicy, termPastAmount, nsfPcnAmount,
                    isEquityUsed);
            rescission.setNextReviewDate(nextReviewDate);
        }

        return statusCode;
    }

    private ZonedDateTime processByDriverQuoteInd(char polStatusCode,
            AccountProcessingAndRecovery accountProcessingAndRecovery, String accountId, BilBillPlan bilBillPlan,
            BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy, double termPastAmount, double nsfPcnAmount,
            boolean isEquityUsed) {
        ZonedDateTime cancelGraceDate = accountProcessingAndRecovery.getRecoveryDate();
        ZonedDateTime nextReviewDate = DateRoutine.defaultDateTime();

        if (accountProcessingAndRecovery.getDriverIndicator() == DRIVER_CA
                && accountProcessingAndRecovery.getQuoteIndicator() != CHAR_Q) {
            char prtReason = BLANK_CHAR;

            Short creditSeqNumber = checkCreditInd(accountId, accountProcessingAndRecovery.getRecoveryDate());
            Short paymentSeqNumber = checkForPayment(CHAR_Y, accountId, bilPolicy.getPolNbr(),
                    bilPolicy.getPolSymbolCd(), accountProcessingAndRecovery.getDistributionSequenceNumber(),
                    accountProcessingAndRecovery.getRecoveryDate());
            if (creditSeqNumber != null || paymentSeqNumber != null) {
                if (creditSeqNumber != null && paymentSeqNumber != null) {
                    if (creditSeqNumber > paymentSeqNumber) {
                        prtReason = CHAR_R;
                    } else {
                        prtReason = CHAR_C;
                    }
                }
                if (creditSeqNumber != null && paymentSeqNumber == null) {
                    prtReason = CHAR_R;
                }
                if (creditSeqNumber == null && paymentSeqNumber != null) {
                    prtReason = CHAR_C;
                }

                BilRenDays bilRenDays = readBilRenday(bilPolicyTerm);
                char postmarkDateInd = bilRenDays.getBrdPstmrkDtInd();
                char renTypeCode = bilRenDays.getBrdRenTypeCd();
                char rescPrtCode = bilRenDays.getBrdRescPrtCd();
                if (postmarkDateInd == CHAR_Y && renTypeCode != BLANK_CHAR && prtReason != CHAR_R
                        && paymentSeqNumber != null) {
                    BilPolActivity bilPolActivity = bilPolActivityRepository
                            .findById(new BilPolActivityId(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "CGD"))
                            .orElse(null);
                    if (bilPolActivity != null) {
                        cancelGraceDate = bilPolActivity.getBilNxtAcyDt();
                    }

                    boolean needGoodPostmarkWipIndicator = false;
                    BilCashReceipt bilCashReceipt = checkForCurrPaymentOnly(accountId,
                            accountProcessingAndRecovery.getRecoveryDate());
                    if (bilCashReceipt != null && bilCashReceipt.getPostMarkDate().compareTo(cancelGraceDate) <= 0) {
                        needGoodPostmarkWipIndicator = true;
                    }

                    if (needGoodPostmarkWipIndicator) {
                        prtReason = CHAR_C;
                    } else {
                        prtReason = CHAR_P;
                    }
                }
                if (rescPrtCode == CHAR_R) {
                    ppcPrintRequest(accountId, bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), bilPolicyTerm,
                            accountProcessingAndRecovery, nsfPcnAmount, termPastAmount, prtReason, polStatusCode);
                } else {
                    if (rescPrtCode == CHAR_B) {
                        if (creditSeqNumber != null && paymentSeqNumber != null && prtReason != CHAR_P) {
                            prtReason = CHAR_B;
                        }
                        writeNrdTriggerNfo(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), prtReason,
                                accountProcessingAndRecovery.getRecoveryDate());
                    }
                }
            }
        }

        if ((accountProcessingAndRecovery.getDriverIndicator() != CHAR_Y
                || bilPolicyTerm.getPlnExpDt().compareTo(accountProcessingAndRecovery.getRecoveryDate()) > 0
                || bilBillPlan.getBbpInvDuringPcnInd() != CHAR_N)
                && (!isEquityUsed && bilPolicyTerm.getPlnExpDt().compareTo(nextReviewDate) < 0)) {
            nextReviewDate = bilPolicyTerm.getPlnExpDt();
        }
        return nextReviewDate;
    }

    private char processByCalloffCancellation(char polStatusCode,
            AccountProcessingAndRecovery accountProcessingAndRecovery, BilAccount bilAccount, char splitIndicator,
            double termPastAmount, BilPolicy bilPolicy, BilPolicyTerm bilPolicyTerm,
            AccountReviewDetails accountReviewDetails, double nsfPcnAmount, Rescission rescission,
            boolean isNsfPcnCancel) {

        rescission.setRescind(postmarkRescind(bilAccount.getAccountId(), bilAccount.getAccountNumber(), bilPolicyTerm,
                bilPolicy, accountProcessingAndRecovery, termPastAmount, nsfPcnAmount, polStatusCode));
        if (rescission.isRescind() || rescission.isBillPlanChange()) {
            polStatusCode = rescindRequest(bilAccount, bilPolicy, bilPolicyTerm, splitIndicator, accountReviewDetails,
                    accountProcessingAndRecovery, rescission, polStatusCode, isNsfPcnCancel);
            if (splitIndicator == ACCT_IS_SPLIT) {
                cancellationService.callSplitBillingActivity(bilAccount.getAccountId(),
                        NoncausalType.RESCIND_REQUEST.toString(), false,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            }
        }
        Object[] statusObject = status(rescission.isRescind(), rescission.isBillPlanChange(),
                rescission.getTermBalanceAmount(), polStatusCode, bilPolicyTerm.getBillSusFuReaCd());
        polStatusCode = (char) statusObject[0];
        return polStatusCode;
    }

    private char processByEquityRescind(AccountProcessingAndRecovery accountProcessingAndRecovery,
            BilAccount bilAccount, char polStatusCode, BilBillPlan bilBillPlan, char splitIndicator,
            BilPolicy bilPolicy, BilPolicyTerm bilPolicyTerm, AccountReviewDetails accountReviewDetails,
            double nsfPcnAmount, double termPastAmount, Rescission rescission, boolean isNsfPcnCancel) {
        char equityRescindCode = bilBillPlan.getBilEqyRescCd();

        Object[] equityObject = processEquityDate(bilAccount.getAccountId(), bilPolicy.getPolNbr(),
                bilPolicy.getPolSymbolCd(), equityRescindCode, bilPolicyTerm, bilBillPlan.getBilEqyRescNbr());
        boolean isSkipEquityRescind = (boolean) equityObject[0];
        if (isSkipEquityRescind) {
            return polStatusCode;
        }
        ZonedDateTime canDatePlusBerc = (ZonedDateTime) equityObject[1];
        ZonedDateTime pedNextAcyDate = (ZonedDateTime) equityObject[2];
        Object[] betweenDateObject = getBetweenDate(canDatePlusBerc, pedNextAcyDate);
        char dateAdjustSign = (char) betweenDateObject[0];
        int dateAdjustNumber = (int) betweenDateObject[1];

        if (accountProcessingAndRecovery.getDriverIndicator() == DRIVER_CA && splitIndicator == CHAR_Y) {
            ZonedDateTime pedNextAcyDate2 = cancellationService.callCalculatePolicyPaidToDate(bilAccount.getAccountId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilPolicyTerm.getPlnExpDt());
        } else if (dateAdjustSign == PLUS_SIGN && dateAdjustNumber > 0 && (bilBillPlan.getBbpInvDuringPcnInd() == CHAR_N
                || accountProcessingAndRecovery.getDriverIndicator() == DRIVER_CA && splitIndicator == CHAR_Y)) {

            rescission.setRescind(postmarkRescind(bilAccount.getAccountId(), bilAccount.getAccountId(), bilPolicyTerm,
                    bilPolicy, accountProcessingAndRecovery, termPastAmount, nsfPcnAmount, polStatusCode));
            if (rescission.isRescind() || rescission.isBillPlanChange()) {
                polStatusCode = rescindRequest(bilAccount, bilPolicy, bilPolicyTerm, splitIndicator,
                        accountReviewDetails, accountProcessingAndRecovery, rescission, polStatusCode, isNsfPcnCancel);
                if (splitIndicator == CHAR_Y) {
                    cancellationService.callSplitBillingActivity(bilAccount.getAccountId(),
                            NoncausalType.RESCIND_REQUEST.toString(), false,
                            bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                            bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                }
            }
        }

        return polStatusCode;
    }

    private Object[] rescindByPolicyStatus(char polStatusCode,
            AccountProcessingAndRecovery accountProcessingAndRecovery, BilAccount bilAccount, char splitIndicator,
            BilPolicy bilPolicy, BilPolicyTerm bilPolicyTerm, BilBillPlan bilBillPlan,
            AccountReviewDetails accountReviewDetails, Rescission rescission, Map<String, Object> termDataMap) {

        ZonedDateTime se3Date = accountProcessingAndRecovery.getRecoveryDate();
        String accountId = bilAccount.getAccountId();
        boolean isExit = false;
        char billDatesIndicator = (char) termDataMap.get(TermAcyType.BILLDATESINDICATOR.getValue());

        switch (BilPolicyStatusCode.getEnumValue(polStatusCode)) {
        case PENDING_CANCELLATION_NONSUFFICIENTFUNDS:

            polStatusCode = nsfRescind(polStatusCode, accountProcessingAndRecovery, bilAccount, bilPolicyTerm,
                    bilPolicy, splitIndicator, accountReviewDetails, rescission, termDataMap);
            if (polStatusCode != CHAR_N) {
                logger.debug("Rescisission is completed by nsfRescind, go to next term");
                return new Object[] { polStatusCode, false };
            }
            break;

        case PENDINGCANCELLATION_NORESPONSE:
            checkNoRespRenewalInvoiceDate(bilPolicyTerm, accountProcessingAndRecovery, accountId, termDataMap);
            break;

        case BILLACCOUNT_PENDINGCANCELLATION_MAX:
            if (checkPttInProgress(bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt())) {
                break;
            }
            Object[] pcnmObject = pcnmRescind(polStatusCode, accountProcessingAndRecovery, bilAccount, bilPolicyTerm,
                    bilPolicy, splitIndicator, accountReviewDetails, rescission, termDataMap);
            isExit = (boolean) pcnmObject[1];
            polStatusCode = (char) pcnmObject[0];

            if (isExit) {
                logger.debug("Rescisission is completed by pcnmRescind, go to next term");
                return new Object[] { polStatusCode, isExit };
            }
            break;
        default:
            break;
        }

        label1: {
            if (polStatusCode == BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue()
                    || polStatusCode == BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue()) {
                break label1;
            } else {

                ZonedDateTime cnrDate = DateRoutine.dateTimeAsYYYYMMDD(SHORT_ZERO);
                if (bilPolicyTerm.getBptIssueSysId().compareTo("UW") != 0) {
                    cnrDate = DateRoutine.dateTimeAsYYYYMMDD(INIT_DATE);
                }
                if (bilPolicyTerm.getPlnExpDt().compareTo(se3Date) <= 0
                        && bilPolicyTerm.getBptIssueSysId().compareTo("UW") != 0) {
                    if (bilPolActivityRepository.existsById(
                            new BilPolActivityId(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "CDT"))) {
                        break label1;
                    }
                    cnrDate = getCnrDate(accountId, bilPolicy, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                }

                ZonedDateTime termPcnDate = (ZonedDateTime) termDataMap.get(TermAcyType.TERMPCNDATE.getValue());
                ZonedDateTime termPastDate = (ZonedDateTime) termDataMap.get(TermAcyType.TERMPASTDATE.getValue());
                Double termPastAmount = (Double) termDataMap.get(TermAcyType.TERMPASTAMOUNT.getValue());
                Double nsfPcnAmount = (Double) termDataMap.get(NsfPcnDataType.NSFPCNAMOUNT.getValue());
                boolean isNsfPcnCancel = (boolean) termDataMap.get(NsfPcnDataType.NSFPCNCANCEL.getValue());

                if (bilPolicyTerm.getPlnExpDt().compareTo(se3Date) == 0
                        || bilPolicyTerm.getPlnExpDt().compareTo(se3Date) < 0
                                && cnrDate.compareTo(DateRoutine.dateTimeAsYYYYMMDD(SHORT_ZERO)) == 0
                                && bilBillPlan.getBbpInvDuringPcnInd() == CHAR_N
                        || cnrDate.compareTo(se3Date) <= 0
                                && cnrDate.compareTo(DateRoutine.dateTimeAsYYYYMMDD(INIT_DATE)) > 0
                                && (bilPolicyTerm.getPlnExpDt().compareTo(se3Date) == 0
                                        || bilPolicyTerm.getPlnExpDt().compareTo(se3Date) < 0)
                        || termPcnDate.compareTo(se3Date) == 0
                                && polStatusCode != BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue()
                        || termPastDate.compareTo(DateRoutine.defaultDateTime()) == 0
                                && polStatusCode != BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue()
                        || termPcnDate.compareTo(se3Date) < 0 && termPastAmount == 0
                        || termPcnDate.compareTo(se3Date) > 0 && billDatesIndicator == CHAR_Y
                        || rescission.isForceRescind()) {
                    isExit = true;
                    rescission.setRescind(postmarkRescind(accountId, bilAccount.getAccountNumber(), bilPolicyTerm,
                            bilPolicy, accountProcessingAndRecovery, termPastAmount, nsfPcnAmount, polStatusCode));

                    boolean isPcnRescindCheck = false;
                    List<BilActSummary> bilActSummaries = bilActSummaryRepository.findRescindTrigger(accountId, se3Date,
                            bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                            bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "BPC", BLANK_STRING);
                    if (bilActSummaries != null && !bilActSummaries.isEmpty()) {
                        isPcnRescindCheck = true;
                    }

                    if (rescission.isRescind() || isPcnRescindCheck
                            || termPcnDate.compareTo(se3Date) > 0 && billDatesIndicator == CHAR_Y
                            || bilPolicyTerm.getPlnExpDt().compareTo(se3Date) == 0
                                    && cnrDate.compareTo(DateRoutine.dateTimeAsYYYYMMDD(START_DATE)) == 0
                            || bilPolicyTerm.getPlnExpDt().compareTo(se3Date) < 0
                                    && cnrDate.compareTo(DateRoutine.dateTimeAsYYYYMMDD(START_DATE)) == 0
                                    && bilBillPlan.getBbpInvDuringPcnInd() == CHAR_N
                            || rescission.isForceRescind()) {
                        rescission.setRescind(true);
                        polStatusCode = rescindRequest(bilAccount, bilPolicy, bilPolicyTerm, splitIndicator,
                                accountReviewDetails, accountProcessingAndRecovery, rescission, polStatusCode,
                                isNsfPcnCancel);
                        if (splitIndicator == CHAR_Y) {
                            cancellationService.callSplitBillingActivity(bilAccount.getAccountId(),
                                    NoncausalType.RESCIND_REQUEST.toString(), false,
                                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                        }
                        Object[] statusObject = status(rescission.isRescind(), rescission.isBillPlanChange(),
                                rescission.getTermBalanceAmount(), polStatusCode, bilPolicyTerm.getBillSusFuReaCd());
                        polStatusCode = (char) statusObject[0];
                    }
                }
            }
        }

        return new Object[] { polStatusCode, isExit };
    }

    private ZonedDateTime getCnrDate(String accountId, BilPolicy bilPolicy, ZonedDateTime polEffectiveDt) {
        ZonedDateTime cnrDate = DateRoutine.dateTimeAsYYYYMMDD(SHORT_ZERO);
        List<String> bilAcyDesCdList = Arrays.asList("C", "RCN");
        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.getMaxRescindData(accountId,
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), polEffectiveDt, bilAcyDesCdList);
        if (bilActSummaryList != null && !bilActSummaryList.isEmpty()) {
            BilActSummary bilActSummary = bilActSummaryList.get(SHORT_ZERO);
            ZonedDateTime maxAcyDate = bilActSummaryRepository.findMaxIniBilActivityDate(accountId,
                    bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), "CNR", polEffectiveDt,
                    bilActSummary.getBillActSummaryId().getBilAcySeq(),
                    bilActSummary.getBillActSummaryId().getBilAcyDt());
            if (maxAcyDate != null) {
                cnrDate = maxAcyDate;
            }
        }

        return cnrDate;
    }

    private boolean checkPriorPcn(String accountId, String polSymbol, String polNumber, String policyId,
            ZonedDateTime polEffDate) {
        String userId = BLANK_STRING;
        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.findUserIdRescinded(accountId, polNumber,
                polSymbol, polEffDate, new ArrayList<>(Arrays.asList("C", EventCode.EVENT_RECN_REA.getValue())),
                new ArrayList<>(Arrays.asList(BLANK_STRING, "CAM", "NQM", "AMP")));
        if (bilActSummaryList != null && !bilActSummaryList.isEmpty()) {
            userId = bilActSummaryList.get(0).getUserId();
        }
        if (CANTT_USER.equalsIgnoreCase(userId)) {
            BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository
                    .findById(new BilPolicyTermId(accountId, policyId, polEffDate)).orElse(null);
            if (bilPolicyTerm != null) {
                return true;
            }
        }
        return false;
    }

    private void writeNrdTriggerNfo(String accountId, String policyId, ZonedDateTime polEffectiveDate, char prtReason,
            ZonedDateTime recoveryDate) {
        BilPolActivityId bilPolActivityId = new BilPolActivityId(accountId, policyId, polEffectiveDate, "NRD");
        BilPolActivity bilPolActivity = bilPolActivityRepository.findById(bilPolActivityId).orElse(null);
        if (bilPolActivity == null) {
            bilPolActivity = new BilPolActivity();
            bilPolActivity.setBillPolActivityId(bilPolActivityId);
            bilPolActivity.setBilNxtAcyDt(recoveryDate);
            bilPolActivityRepository.saveAndFlush(bilPolActivity);
        } else {
            String bilAcyTypeCode;
            List<BilPolActivity> bilPolActivityList = bilPolActivityRepository.fetchNrdActivityTriger(accountId,
                    policyId, polEffectiveDate, new ArrayList<>(Arrays.asList("RDC", "RDR", "RDB", "RDP")));
            if (bilPolActivityList != null && !bilPolActivityList.isEmpty()) {
                bilAcyTypeCode = bilPolActivityList.get(0).getBillPolActivityId().getBilAcyTypeCd().trim();
                if (bilAcyTypeCode.equalsIgnoreCase("RDP")) {
                    return;
                }
                bilPolActivityRepository.deleteAllInBatch(bilPolActivityList);
            }

        }

        String acyTypeCode = "RD" + prtReason;
        BilPolActivityId secondBilPolActivityId = new BilPolActivityId(accountId, policyId, polEffectiveDate,
                acyTypeCode);
        BilPolActivity secondBilPolActivity = bilPolActivityRepository.findById(secondBilPolActivityId).orElse(null);
        if (secondBilPolActivity == null || secondBilPolActivity.getBilNxtAcyDt().compareTo(recoveryDate) != 0) {
            secondBilPolActivity = new BilPolActivity();
            secondBilPolActivity.setBillPolActivityId(secondBilPolActivityId);
            secondBilPolActivity.setBilNxtAcyDt(recoveryDate);
            bilPolActivityRepository.saveAndFlush(secondBilPolActivity);
        }
    }

    private void ppcPrintRequest(String accountId, String polNumber, String polSymbolCd, BilPolicyTerm bilPolicyTerm,
            AccountProcessingAndRecovery accountProcessingAndRecovery, double nsfPcnAmount, double termPastAmount,
            char prtReason, char polStatusCode) {
        String policyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
        ZonedDateTime polEffectiveDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();

        BilObjectCfg bilObjectCfg = bilObjectCfgRepository.findById(new BilObjectCfgId(BCMOPPC, CHAR_I)).orElse(null);
        if (accountProcessingAndRecovery.getQuoteIndicator() == CHAR_Q
                || bilObjectCfg != null && bilObjectCfg.getBocObjPrcCd() != CHAR_Y) {
            logger.debug("The ppcPrintRequest is break by Object processing code is not Initiate Process");
            return;
        }

        Double bilDspAmount = bilCashDspRepository.getDspAmountForAppliedByDspDate(accountId,
                BilDspTypeCode.APPLIED.getValue(), policyId, polEffectiveDate,
                accountProcessingAndRecovery.getRecoveryDate(), BLANK_CHAR);
        if (bilDspAmount == null || bilDspAmount <= 0) {
            return;
        }

        double postDueAmount;
        if (polStatusCode == CHAR_N && accountProcessingAndRecovery.getDriverIndicator() == DRIVER_CA) {
            postDueAmount = nsfPcnAmount;
        } else {
            postDueAmount = termPastAmount;
        }

        boolean linkBcmoppc = false;
        List<String> bilAcyDesCdList = Arrays.asList("CNR");
        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.getMaxRescindData(accountId, polNumber,
                polSymbolCd, polEffectiveDate, bilAcyDesCdList);
        if (bilActSummaryList != null && !bilActSummaryList.isEmpty()) {
            linkBcmoppc = checkPriorPcn(accountId, polNumber, polSymbolCd, polEffectiveDate,
                    bilActSummaryList.get(SHORT_ZERO).getBillActSummaryId().getBilAcyDt(),
                    bilActSummaryList.get(SHORT_ZERO).getBillActSummaryId().getBilAcySeq());
        } else {
            linkBcmoppc = true;
        }

        bilObjectCfg = bilObjectCfgRepository.findById(new BilObjectCfgId(BCMOPPC, CHAR_P)).orElse(null);
        if (bilObjectCfg == null || bilObjectCfg.getBocObjPrcCd() != CHAR_T) {
            if (linkBcmoppc) {
                logger.debug("Make a call to PartialOrLatePaymentOnPcnPrint module when linkBcmoppc trigger is active");
                linkPartialOrLatePaymentOnPcnPrint(accountId, policyId, polEffectiveDate, bilPolicyTerm.getPlnExpDt(),
                        postDueAmount, prtReason, accountProcessingAndRecovery.getRecoveryDate());
            }
        } else {

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(StringUtils.rightPad(accountId, 8, BLANK_CHAR));
            stringBuilder.append(StringUtils.rightPad(policyId, 16, BLANK_CHAR));
            stringBuilder.append(StringUtils.right(DateRoutine.dateTimeAsYYYYMMDDString(polEffectiveDate), 10));
            stringBuilder
                    .append(StringUtils.right(DateRoutine.dateTimeAsYYYYMMDDString(bilPolicyTerm.getPlnExpDt()), 10));
            stringBuilder.append(StringUtils.rightPad("0", 2, '0'));
            stringBuilder.append(StringUtils.leftPad(String.valueOf(postDueAmount), 14, '0'));
            stringBuilder.append(String.valueOf(prtReason));
            stringBuilder.append(StringUtils
                    .right(DateRoutine.dateTimeAsYYYYMMDDString(accountProcessingAndRecovery.getRecoveryDate()), 10));
            stringBuilder.append(StringUtils.rightPad("0", 8, '0'));
            stringBuilder.append("N");
            String userData = stringBuilder.toString();

            logger.debug("Insert into DFR_ACY_QUE table for processing BCMOPPC module");
            scheduleService.scheduleDeferActivity(BCMOPPC, userData, accountId);
        }
    }

    private BilCashReceipt checkForCurrPaymentOnly(String accountId, ZonedDateTime recoveryDate) {
        List<BilCashReceipt> bilCashReceiptList = bilCashReceiptRepository.getMaxPostMarkDateRow(accountId,
                recoveryDate);
        if (bilCashReceiptList != null && !bilCashReceiptList.isEmpty()) {
            return bilCashReceiptList.get(SHORT_ZERO);
        }

        return null;

    }

    @Override
    public BilRenDays readBilRenday(BilPolicyTerm bilPolicyTerm) {
        BilRenDaysId bilRenDaysId = new BilRenDaysId(bilPolicyTerm.getBillStatePvnCd(), bilPolicyTerm.getLobCd(),
                bilPolicyTerm.getMasterCompanyNbr(), bilPolicyTerm.getBptIssueSysId());
        BilRenDays bilRenDays = bilRenDaysRepository.findById(bilRenDaysId).orElse(null);
        if (bilRenDays == null) {
            bilRenDaysId.setMasterCompanyNbr("99");
            bilRenDays = bilRenDaysRepository.findById(bilRenDaysId).orElse(null);
            if (bilRenDays == null) {
                bilRenDaysId.setLobCd(BLANK_STRING);
                bilRenDays = bilRenDaysRepository.findById(bilRenDaysId).orElse(null);
                if (bilRenDays == null) {
                    bilRenDaysId.setBilStatePvnCd(BLANK_STRING);
                    bilRenDaysId.setBilIssueSysId(BLANK_STRING);
                    bilRenDays = bilRenDaysRepository.findById(bilRenDaysId)
                            .orElse(new BilRenDays(bilRenDaysId, SHORT_ZERO, BLANK_CHAR, CHAR_N, 'R', CHAR_N,
                                    SHORT_ZERO, CHAR_N, CHAR_N, SHORT_ZERO, SHORT_ZERO));
                }
            }
        }
        return bilRenDays;
    }

    @Override
    public Short checkForPayment(char checkForRescindInd, String accountId, String polNumber, String polSymbolCd,
            short bilDtbSequence, ZonedDateTime recoveryDate) {
        List<String> bilDspTypeList = new ArrayList<>(
                Arrays.asList(BilDspTypeCode.SUSPENDED.getValue(), BilDspTypeCode.APPLIED.getValue()));
        List<Character> bcdCreditIndList = new ArrayList<>(
                Arrays.asList(CreditIndicator.OPERATOR.getValue(), BLANK_CHAR));
        List<Character> bilRevsRsusIndList = new ArrayList<>(
                Arrays.asList(ReverseReSuspendIndicator.RECONCILIATION_NOT_COMPLETE.getValue(), BLANK_CHAR));
        Short maxPaymentSequence = null;

        Integer bilCashPaymentCount = null;
        if (checkForRescindInd != CHAR_Y) {
            bilCashPaymentCount = bilCashDspRepository.checkForPaymentToday(accountId, recoveryDate, bilDspTypeList,
                    bilRevsRsusIndList, bcdCreditIndList);
        } else {
            BilCashReceipt bilCashReceipt = bilCashReceiptRepository
                    .findById(new BilCashReceiptId(accountId, recoveryDate, bilDtbSequence)).orElse(null);
            if (bilCashReceipt != null) {
                Short maxDtbSequence = bilCashReceiptRepository.getMaxEntrySeqNbrByEntryNumber(accountId,
                        bilCashReceipt.getEntryNumber(), recoveryDate);
                if (maxDtbSequence != null) {
                    bilCashPaymentCount = bilCashDspRepository.countPaymentCashToday(accountId, recoveryDate,
                            maxDtbSequence, polNumber, polSymbolCd, BilDspTypeCode.APPLIED.getValue(), bcdCreditIndList,
                            bilRevsRsusIndList);
                }
            }
        }
        if (bilCashPaymentCount != null) {
            maxPaymentSequence = bilActSummaryRepository.getMaxPaymentSequence(accountId, recoveryDate,
                    new ArrayList<>(
                            Arrays.asList("PYT", "ACH", "CPY", "1XA", "GPY", "COL", "PYC", "ASW", "DWP", "EWP")),
                    new ArrayList<>(Arrays.asList("EC", "EY")), BLANK_STRING, "%%T", "CTR");
        }
        return maxPaymentSequence;
    }

    @Override
    public Short checkCreditInd(String accountId, ZonedDateTime recoveryDate) {
        Short maxCreditSequence = null;
        Integer creditCount = bilCashDspRepository.checkForCreditToday(accountId, recoveryDate,
                new ArrayList<>(Arrays.asList(BilDspTypeCode.SUSPENDED.getValue(), BilDspTypeCode.APPLIED.getValue())),
                new ArrayList<>(Arrays.asList(CreditIndicator.CANCELLATION_CREDIT.getValue(),
                        CreditIndicator.CREDIT.getValue())),
                ReverseReSuspendIndicator.RESUSPENDED.getValue());
        if (creditCount != null) {
            maxCreditSequence = bilActSummaryRepository.getMaxCreditSequence(accountId, recoveryDate,
                    new ArrayList<>(Arrays.asList("BAM", "AMT", "BIT")), DECIMAL_ZERO);
        }
        return maxCreditSequence;
    }

    private Object[] processEquityDate(String accountId, String polNumber, String polSymbolCode, char equityRescindCode,
            BilPolicyTerm bilPolicyTerm, short equityRescindNumber) {
        String pedDate = BLANK_STRING;
        ZonedDateTime canDatePlusBerc = null;
        ZonedDateTime pedNextAcyDate = null;
        boolean isSkipEquityRescind = false;

        List<String> bilAcyDesCdList = Arrays.asList("C");
        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.getMaxEquityData(accountId, polNumber,
                polSymbolCode, bilAcyDesCdList);
        if (bilActSummaryList == null || bilActSummaryList.isEmpty()) {
            throw new DataNotFoundException("equity.not.found");
        }
        BilActSummary bilActSummary = bilActSummaryList.get(SHORT_ZERO);

        ZonedDateTime bilAcyDate2 = bilActSummary.getBilAcyDes2Dt();
        String addDataText = bilActSummary.getBasAddDataTxt();
        if (!addDataText.trim().isEmpty()) {
            pedDate = addDataText.substring(13, 23);
        }

        if (equityRescindCode == EQT_RULE_PED && pedDate.trim().isEmpty()) {
            isSkipEquityRescind = true;
            return new Object[] { isSkipEquityRescind, canDatePlusBerc, pedNextAcyDate };
        }

        BilPolActivity bilPolActivity = bilPolActivityRepository
                .findById(new BilPolActivityId(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "PED"))
                .orElse(null);
        if (bilPolActivity != null) {
            pedNextAcyDate = bilPolActivity.getBilNxtAcyDt();
        } else {
            pedNextAcyDate = cancellationService.callCalculatePaidToDate(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilPolicyTerm.getPlnExpDt());
        }

        if (equityRescindNumber > 0) {
            if (equityRescindCode == EQT_RULE_PCN) {
                canDatePlusBerc = bilAcyDate2;
            } else {
                canDatePlusBerc = DateRoutine.dateTimeAsYYYYMMDD(pedDate);
            }
            canDatePlusBerc = DateRoutine.adjustDateWithOffset(canDatePlusBerc, false, equityRescindNumber, PLUS_SIGN,
                    null);
        } else if (equityRescindCode == EQT_RULE_PCN) {
            canDatePlusBerc = bilActSummary.getBilAcyDes2Dt();
        } else {
            canDatePlusBerc = DateRoutine.dateTimeAsYYYYMMDD(pedDate);
        }

        return new Object[] { isSkipEquityRescind, canDatePlusBerc, pedNextAcyDate };
    }

    @Override
    public Object[] status(boolean isRescind, boolean isBpcCheck, double termBalanceAmount, char polStatus,
            String billSusFuReaCd) {
        if (isRescind || isBpcCheck) {
            if (termBalanceAmount == DECIMAL_ZERO) {
                polStatus = BilPolicyStatusCode.CLOSED.getValue();
            } else {
                polStatus = BilPolicyStatusCode.OPEN.getValue();
            }
            billSusFuReaCd = BLANK_STRING;
        }
        return new Object[] { polStatus, billSusFuReaCd };
    }

    private char rescindRequest(BilAccount bilAccount, BilPolicy bilPolicy, BilPolicyTerm bilPolicyTerm,
            char splitIndicator, AccountReviewDetails accountReviewDetails,
            AccountProcessingAndRecovery accountProcessingAndRecovery, Rescission rescission, char polStatusCode,
            boolean isNsfPcnCancel) {
        char eftCollectionInd = accountReviewDetails.getEftCollectionIndicator();
        String userId = BLANK_STRING;
        String bilAcyDesCode = "B";
        String accountId = bilAccount.getAccountId();
        String suspendedFullReasonCd = bilAccount.getSuspendedFullReasonCd().trim();
        char rssBillIndicator = accountReviewDetails.getBilActRules().getBruRssBillInd();
        short bilDueDateDplNumber = accountReviewDetails.getBilSupportPlan().getBilDueDtDplNbr();

        writeHistory(accountId, bilAcyDesCode, BLANK_STRING, bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), DateRoutine.defaultDateTime(), BLANK_STRING,
                SYSTEM_USER_ID, accountProcessingAndRecovery.getRecoveryDate());

        boolean isCanttRule = checkCanttRules(accountProcessingAndRecovery.getRecoveryDate());
        if (isCanttRule) {
            Object[] renewalObject = checkForRenewal(accountId, bilPolicy, bilPolicyTerm, true, bilAcyDesCode,
                    isNsfPcnCancel, splitIndicator, accountProcessingAndRecovery.getRecoveryDate(), polStatusCode);
            boolean isRenewalPresent = (boolean) renewalObject[0];
            polStatusCode = (char) renewalObject[1];
            if (isRenewalPresent) {
                userId = "CANTT";
            }
        }

        if (accountProcessingAndRecovery.getQuoteIndicator() != CHAR_Q) {
            RecoveryAccount recoveryAccount = null;
            PcnRescindCollection pcnRescindCollection = createBillingApiParam(accountId, "ACR",
                    accountProcessingAndRecovery.getRecoveryDate(), bilPolicyTerm, bilPolicy, userId);
            logger.debug("Make a call to Billing API module with type ACR");
            billingApiService.processPcnOrRescindCollection("BCMOAX", accountReviewService.getQueueWrtIndicator(),
                    pcnRescindCollection);

            deleteBilPolActivity(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());

            if (bilAccount.getSuspendedFullReasonCd().trim().equalsIgnoreCase("SS")) {
                recoveryAccount = new RecoveryAccount();
                recoveryAccount.setAccountId(accountId);
                recoveryAccount.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
                recoveryService.resumeAccount(accountId, bilAccount, recoveryAccount, false, accountReviewDetails,
                        accountProcessingAndRecovery.getRecoveryDate(), accountProcessingAndRecovery.getUserSequenceId());
            }

            if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                    && rssBillIndicator != CHAR_N) {
                char blackoutDateIndicator = verifySeaExclData(bilAccount, bilDueDateDplNumber,
                        accountProcessingAndRecovery.getRecoveryDate());
                if (blackoutDateIndicator != CHAR_Y) {
                    recoveryAccount = new RecoveryAccount();
                    recoveryAccount.setAccountId(accountId);
                    recoveryAccount.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
                    recoveryAccount.setPolEffDate(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                    recoveryAccount.setRescindCallIndicator(CHAR_Y);
                    boolean isDateReset = true;

                    if (rssBillIndicator == CHAR_Y && (eftCollectionInd != EFT)
                            || eftCollectionInd == EFT && suspendedFullReasonCd.equalsIgnoreCase("SS")) {
                        recoveryService.updateAccountAdjustDates(recoveryAccount, isDateReset, accountReviewDetails,
                                accountProcessingAndRecovery.getRecoveryDate(), accountProcessingAndRecovery.getUserSequenceId());
                        logger.debug("Get Next invoice date then send to updateBAI");
                        rescission
                                .setNextInvoiceDate(accountProcessingAndRecoveryService.getNextInvoiceDate(accountId));
                    } else {
                        if (rssBillIndicator == CHAR_D && (eftCollectionInd != EFT)) {
                            recoveryService.updateAccountAdjustDates(recoveryAccount, isDateReset, accountReviewDetails,
                                    accountProcessingAndRecovery.getRecoveryDate(), accountProcessingAndRecovery.getUserSequenceId());
                            rescission.setNextInvoiceDate(
                                    accountProcessingAndRecoveryService.getNextInvoiceDate(accountId));
                        } else if (rssBillIndicator == CHAR_E && eftCollectionInd == EFT
                                && suspendedFullReasonCd.equalsIgnoreCase("SS")) {
                            recoveryService.updateAccountAdjustDates(recoveryAccount, isDateReset, accountReviewDetails,
                                    accountProcessingAndRecovery.getRecoveryDate(), accountProcessingAndRecovery.getUserSequenceId());
                            logger.debug("Get Next invoice date then send to updateBAI");
                            rescission.setNextInvoiceDate(
                                    accountProcessingAndRecoveryService.getNextInvoiceDate(accountId));
                        }
                    }
                    recoveryAccount.setRescindCallIndicator(CHAR_N);
                }
            }

            processAcpTrigger(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            boolean isRescindNotice = false;

            List<String> bilAcyDesCdList = Arrays.asList("CNR");

            List<BilActSummary> bilActSummaryList = bilActSummaryRepository.getMaxRescindData(accountId,
                    bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilAcyDesCdList);
            if (bilActSummaryList != null && !bilActSummaryList.isEmpty()) {
                isRescindNotice = checkPriorPcn(accountId, bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                        bilActSummaryList.get(SHORT_ZERO).getBillActSummaryId().getBilAcyDt(),
                        bilActSummaryList.get(SHORT_ZERO).getBillActSummaryId().getBilAcySeq());
            } else {
                isRescindNotice = true;
            }

            if (isRescindNotice) {
				RescindNotice rescindNotice = setRescindNoticeForRequest(accountId,
						bilPolicyTerm.getBillPolicyTermId().getPolicyId().trim(), bilAccount, bilPolicy, bilPolicyTerm,
						pcnRescindCollection, accountProcessingAndRecovery.getRecoveryDate());
				rescindNotice(bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), bilAccount, bilPolicyTerm,
						accountProcessingAndRecovery.getRecoveryDate(), rescindNotice);
            }
        }

        return polStatusCode;
    }

    private char verifySeaExclData(BilAccount bilAccount, short bilDueDateDplNumber, ZonedDateTime recoveryDate) {
        char blackoutDateIndicator = CHAR_N;
        ZonedDateTime fpddDate = DateRoutine.getAdjustedDate(recoveryDate, false, bilDueDateDplNumber, null);
        if (!bilAccount.getThirdPartyIdentifier().trim().isEmpty()) {
            BilTtySeasExcl bilTtySeasExcl = bilTtySeasExclRepository.findBilTtySeasExcl(
                    bilAccount.getThirdPartyIdentifier(), SHORT_ZERO, bilAccount.getSeasionalExclusionCode(), fpddDate);
            if (bilTtySeasExcl != null) {
                blackoutDateIndicator = CHAR_Y;
            }
        } else {
            BilSeasExcl bilSeasExcl = bilSeasExclRepository.getBilSeaRowByDate(bilAccount.getSeasionalExclusionCode(),
                    fpddDate);
            if (bilSeasExcl != null) {
                blackoutDateIndicator = CHAR_Y;
            }
        }

        return blackoutDateIndicator;
    }

    private boolean checkPriorPcn(String accountId, String policyNumber, String polSymbolCode,
            ZonedDateTime effectiveDate, ZonedDateTime priorCnrDate, Short priorCnrSequence) {
        List<String> bilAcyDesCdList = Arrays.asList("C");
        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.getMaxRescindData(accountId, policyNumber,
                polSymbolCode, effectiveDate, bilAcyDesCdList);
        if (bilActSummaryList != null && !bilActSummaryList.isEmpty()) {
            BilActSummary bilActSummary = bilActSummaryList.get(SHORT_ZERO);
            if ((priorCnrDate.compareTo(bilActSummary.getBillActSummaryId().getBilAcyDt()) < 0)
                    && (priorCnrDate.compareTo(bilActSummary.getBillActSummaryId().getBilAcyDt()) == 0
                            && priorCnrSequence < bilActSummary.getBillActSummaryId().getBilAcySeq())) {
                return true;
            }
        }

        return false;
    }

    private void rescindNotice(String polNumber, String polSymbolCd, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm,
            ZonedDateTime recoveryDate, RescindNotice rescindNotice) {
    	String rescindNoticeSysId = bilPolicyTerm.getBptIssueSysId().trim();
        if (rescindNoticeSysId.trim().isEmpty()) {
            rescindNoticeSysId = "UW";
        }
        String interfaceObjName = "BIL-RESCIND-NOTICE-" + rescindNoticeSysId;
        HalBoMduXrf halBoMduXrf = halBoMduXrfRepository.findById(interfaceObjName).orElse(null);
        if (halBoMduXrf != null && !halBoMduXrf.getHbmxBobjMduNm().trim().isEmpty()) {
            pb360Service.postRescindNoticeRequest(bilAccount.getAccountId(), rescindNotice);
            writeHistory(bilAccount.getAccountId(), "RSN", BLANK_STRING, polNumber, polSymbolCd,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), DateRoutine.defaultDateTime(),
                    BLANK_STRING, SYSTEM_USER_ID, recoveryDate);
        }
    }

    private void writeHistory(String accountId, String activityDesCd, String activityDesType, String policyNumber,
            String policySymbolCd, ZonedDateTime activityDesc1Dt, ZonedDateTime activityDesc2Dt,
            String additionalDataTxt, String userId, ZonedDateTime recoveryDate) {
        Short bilAcySeq = 0;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, recoveryDate);
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId, recoveryDate.with(LocalTime.MIN), bilAcySeq);
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(policySymbolCd);
        bilActSummary.setPolNbr(policyNumber);
        bilActSummary.setBilAcyDesCd(activityDesCd);
        bilActSummary.setBilDesReaTyp(activityDesType);
        bilActSummary.setBilAcyDes1Dt(activityDesc1Dt);
        bilActSummary.setBilAcyDes2Dt(activityDesc2Dt);
        bilActSummary.setBilAcyAmt(DECIMAL_ZERO);
        bilActSummary.setUserId(userId);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());
        bilActSummary.setBasAddDataTxt(additionalDataTxt);
        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

    private void processAcpTrigger(String accountId, String policyId, ZonedDateTime effectiveDate) {
        BilPolActivity bilPolActivity = bilPolActivityRepository
                .findById(new BilPolActivityId(accountId, policyId, effectiveDate, "ACP")).orElse(null);
        if (bilPolActivity != null) {
            logger.debug("Call to AutomaticPolicyPlanChange module");
            pb360Service.postAutomaticPolicyPlanChange(accountId, policyId, effectiveDate, "BCMOAX", false, CHAR_N);

            bilPolActivityRepository.delete(bilPolActivity);
        }
    }

    private void deleteBilPolActivity(String accountId, String policyId, ZonedDateTime polEffectiveDt) {
        List<String> bilAcyTypeList = new ArrayList<>(Arrays.asList("LDR", "CGD", "NBL", "PRV"));
        for (String bilAcyTypeCode : bilAcyTypeList) {
            BilPolActivity bilPolActivity = bilPolActivityRepository
                    .findById(new BilPolActivityId(accountId, policyId, polEffectiveDt, bilAcyTypeCode)).orElse(null);
            if (bilPolActivity != null) {
                bilPolActivityRepository.delete(bilPolActivity);
            }
        }
    }

    private PcnRescindCollection createBillingApiParam(String accountId, String apiType, ZonedDateTime activityDate,
            BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy, String userId) {
        PcnRescindCollection pcnRescindCollection = new PcnRescindCollection();
        pcnRescindCollection.setApiType(apiType);
        pcnRescindCollection.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
        pcnRescindCollection.setEffectiveDate(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
        pcnRescindCollection.setPlanExpDate(bilPolicyTerm.getPlnExpDt());
        pcnRescindCollection.setUserId(userId);
        pcnRescindCollection.setActivityDate(activityDate);
        if (bilPolicyTerm.getBptIssueSysId().trim().isEmpty()) {
            pcnRescindCollection.setIssueSysId("UW");
        } else {
            pcnRescindCollection.setIssueSysId(bilPolicyTerm.getBptIssueSysId().trim());
        }
        pcnRescindCollection.setAccountId(accountId);
        pcnRescindCollection.setPolSymbolCode(bilPolicy.getPolSymbolCd());
        pcnRescindCollection.setPolNumber(bilPolicy.getPolNbr());
        pcnRescindCollection.setStatePvnCode(bilPolicyTerm.getBillStatePvnCd());
        pcnRescindCollection.setMasterCompanyNumber(bilPolicyTerm.getMasterCompanyNbr());
        pcnRescindCollection.setLineOfBusinessCode(bilPolicyTerm.getLobCd());
        pcnRescindCollection.setReinstNtcIndicator(getReinstNoticeName().charAt(0));

        return pcnRescindCollection;
    }

    private Object[] checkForRenewal(String accountId, BilPolicy bilPolicy, BilPolicyTerm bilPolicyTerm,
            boolean rescindProcess, String bilAcyDesCode, boolean isNsfPcnCancel, char splitIndicator,
            ZonedDateTime recoveryDate, char polStatusCode) {
        boolean isRenewalPresent = false;
        BilPolicyTerm renewalTerm = bilPolicyTermRepository.findSuspendActivityData(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilPolicyTerm.getPlnExpDt(),
                new ArrayList<>(Arrays.asList(BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue(),
                        BilPolicyStatusCode.FLAT_CANCELLATION.getValue(), BilPolicyStatusCode.CANCELLED.getValue(),
                        BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(), 'R',
                        BilPolicyStatusCode.TRANSFERRED.getValue(),
                        BilPolicyStatusCode.SUSPENDBILLING_FOLLOWUP.getValue(),
                        BilPolicyStatusCode.SUSPEND_FOLLOWUP.getValue(),
                        BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                        BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue())));
        if (renewalTerm != null) {
            char renewalStatusCode = renewalTerm.getBillPolStatusCd();
            if (rescindProcess && renewalStatusCode != BilPolicyStatusCode.PENDING_CANCELLATION.getValue()
                    && renewalStatusCode != BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue()
                    && renewalStatusCode != BilPolicyStatusCode.OPEN.getValue()) {
                return new Object[] { isRenewalPresent, polStatusCode };
            }

            List<BilIstSchedule> bilIstScheduleList = bilIstScheduleRepository.getRenewalPresentRows(
                    bilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    new ArrayList<>(
                            Arrays.asList(BLANK_CHAR, InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue())),
                    bilPolicyTerm.getPlnExpDt());
            if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
                isRenewalPresent = true;
            } else {
                isRenewalPresent = true;
                boolean isCancelSplitAccRwl = false;
                boolean isRescindAccRwl = false;
                if (bilAcyDesCode.equalsIgnoreCase("B")) {
                    if (polStatusCode == BilPolicyStatusCode.PENDING_CANCELLATION.getValue()
                            || polStatusCode == BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS
                                    .getValue()) {
                        polStatusCode = BilPolicyStatusCode.OPEN.getValue();
                        isCancelSplitAccRwl = true;
                        isRescindAccRwl = true;
                    } else {
                        isRenewalPresent = false;
                        return new Object[] { isRenewalPresent, polStatusCode };
                    }
                } else {
                    polStatusCode = BilPolicyStatusCode.PENDING_CANCELLATION.getValue();
                    isCancelSplitAccRwl = true;
                }
                if (isNsfPcnCancel) {
                    polStatusCode = BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue();
                }

                BilPolicyTerm updateTerm = bilPolicyTermRepository.findSuspendActivityData(
                        bilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilPolicyTerm.getPlnExpDt(),
                        new ArrayList<>(Arrays.asList(BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue())));
                if (updateTerm != null) {
                    updateTerm.setBillPolStatusCd(polStatusCode);
                    updateTerm.setBptStatusEffDt(bilPolicyTerm.getBptStatusEffDt());
                    bilPolicyTermRepository.saveAndFlush(updateTerm);
                }

                String additionalDataTxt = BLANK_STRING;
                if (!bilAcyDesCode.equalsIgnoreCase("B")) {
                    additionalDataTxt = "RNP";
                }
                writeHistory(accountId, bilAcyDesCode, BLANK_STRING, bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                        bilPolicyTerm.getPlnExpDt(), bilPolicyTerm.getPlnExpDt(), additionalDataTxt, CANTT_USER,
                        recoveryDate);

                if (splitIndicator == ACCT_IS_SPLIT && isCancelSplitAccRwl) {
                    String nonCausalTypeCode;
                    if (isRescindAccRwl) {
                        nonCausalTypeCode = NoncausalType.RESCIND_REQUEST.toString();
                    } else {
                        nonCausalTypeCode = NoncausalType.PCN_REQUEST.toString();
                    }
                    cancellationService.callSplitBillingActivity(accountId, nonCausalTypeCode, false,
                            bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                            bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                }
            }
        }
        return new Object[] { isRenewalPresent, polStatusCode };
    }

    private boolean checkCanttRules(ZonedDateTime recoveryDate) {

        List<HalUniversalCtl2> halUniversalCtl2List = halUniversalCtl2Repository.findActiveEntryByCode("CANTT", "Y",
                recoveryDate);
        return halUniversalCtl2List != null && !halUniversalCtl2List.isEmpty();
    }

    private String getReinstNoticeName() {
        final String moduleName = "BIL_UEXT_NOTICE_OF_REINSTATEMENT";
        String reinstNotice = BLANK_STRING;
        HalBoMduXrf halBoMduXrf = halBoMduXrfRepository.findById(moduleName).orElse(null);
        if (halBoMduXrf != null) {
            reinstNotice = halBoMduXrf.getHbmxBobjMduNm();
        }
        return reinstNotice;
    }

    private boolean postmarkRescind(String accountId, String accountNumber, BilPolicyTerm bilPolicyTerm,
            BilPolicy bilPolicy, AccountProcessingAndRecovery accountProcessingAndRecovery, double termPastAmount,
            double nsfPcnAmount, char polStatusCode) {

        BilPolActivity bilPolActivity = bilPolActivityRepository
                .findById(new BilPolActivityId(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "LDR"))
                .orElse(null);
        if (bilPolActivity == null) {
            return true;
        }

        if (bilPolActivity.getBilNxtAcyDt().compareTo(accountProcessingAndRecovery.getRecoveryDate()) < 0) {
            return false;
        }

        BilRenDays bilRenDays = readBilRenday(bilPolicyTerm);
        if (bilRenDays.getBrdPstmrkDtInd() == CHAR_Y && bilRenDays.getBrdRenTypeCd() != BLANK_CHAR) {
            boolean needGoodPostmarkWipIndicator = false;
            bilPolActivity = bilPolActivityRepository
                    .findById(new BilPolActivityId(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                            bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "CGD"))
                    .orElse(null);
            if (bilPolActivity == null) {
                throw new DataNotFoundException("canc.grace.date.not.found", new Object[] { accountId });
            }
            ZonedDateTime cancelGraceDate = bilPolActivity.getBilNxtAcyDt();
            List<ZonedDateTime> bilDspDates = bilCashDspRepository.getBilAcyDateByPortMarkDate(
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), accountId,
                    BilDspTypeCode.APPLIED.getValue(), BLANK_CHAR, cancelGraceDate);
            if (bilDspDates != null && !bilDspDates.isEmpty()) {
                BilCashReceipt bilCashReceipt = checkForCurrPaymentOnly(accountId,
                        accountProcessingAndRecovery.getRecoveryDate());
                if (bilCashReceipt == null) {
                    return false;
                } else if (bilCashReceipt.getPostMarkDate().compareTo(cancelGraceDate) <= 0) {
                    needGoodPostmarkWipIndicator = true;
                }

                if (needGoodPostmarkWipIndicator && accountProcessingAndRecovery.getQuoteIndicator() != CHAR_Q) {
                    if (accountProcessingAndRecovery.getDriverIndicator() != DRIVER_CA) {
                        cancellationService.wipLink(accountId, accountNumber, WipAcitivityId.GOODPSTMRK.getValue());
                    }
                    return false;
                } else if (accountProcessingAndRecovery.getDriverIndicator() == DRIVER_CA) {
                    char prtReason = CHAR_P;
                    if (bilRenDays.getBrdRescPrtCd() == CHAR_R) {
                        ppcPrintRequest(accountId, bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), bilPolicyTerm,
                                accountProcessingAndRecovery, nsfPcnAmount, termPastAmount, prtReason, polStatusCode);
                    }
                    if (bilRenDays.getBrdRescPrtCd() == CHAR_B
                            && accountProcessingAndRecovery.getQuoteIndicator() != CHAR_Q) {
                        writeNrdTriggerNfo(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), prtReason,
                                accountProcessingAndRecovery.getRecoveryDate());
                    }
                }

                return false;
            }
        }
        return true;
    }

    private Object[] pcnmRescind(char polStatusCode, AccountProcessingAndRecovery accountProcessingAndRecovery,
            BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy, char splitIndicator,
            AccountReviewDetails accountReviewDetails, Rescission rescission, Map<String, Object> termDataMap) {

        ZonedDateTime se3Date = accountProcessingAndRecovery.getRecoveryDate();
        String accountId = bilAccount.getAccountId();
        boolean isExist = false;
        char billDatesIndicator = (char) termDataMap.get(TermAcyType.BILLDATESINDICATOR.getValue());
        boolean isNsfPcnCancel = (boolean) termDataMap.get(NsfPcnDataType.NSFPCNCANCEL.getValue());

        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.getLastMaxAcyDate(accountId, se3Date,
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "C", Arrays.asList("CAM", "NQM"));
        if (bilActSummaryList == null || bilActSummaryList.isEmpty()) {
            throw new DataNotFoundException("pcn.activity.not.found", new Object[] { "date", accountId });
        }

        BilActSummary bilActSummary = bilActSummaryList.get(SHORT_ZERO);

        boolean isPcnmBpcCheck = checkPcnmByActivityCode(accountId, bilActSummary.getBillActSummaryId().getBilAcyDt(),
                bilActSummary.getBillActSummaryId().getBilAcySeq(), bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), Arrays.asList("BPC"));
        if (isPcnmBpcCheck) {
            rescission.setRescind(true);
            polStatusCode = rescindRequest(bilAccount, bilPolicy, bilPolicyTerm, splitIndicator, accountReviewDetails,
                    accountProcessingAndRecovery, rescission, polStatusCode, isNsfPcnCancel);
            if (splitIndicator == ACCT_IS_SPLIT) {
                cancellationService.callSplitBillingActivity(accountId, NoncausalType.RESCIND_REQUEST.toString(), false,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            }
            Object[] statusObject = status(rescission.isRescind(), rescission.isBillPlanChange(),
                    rescission.getTermBalanceAmount(), polStatusCode, bilPolicyTerm.getBillSusFuReaCd());
            polStatusCode = (char) statusObject[0];
            isExist = true;
        } else {
            boolean isPcnmRedistCheck = checkPcnmByActivityCode(accountId,
                    bilActSummary.getBillActSummaryId().getBilAcyDt(),
                    bilActSummary.getBillActSummaryId().getBilAcySeq(), bilPolicy.getPolNbr(),
                    bilPolicy.getPolSymbolCd(), bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    Arrays.asList("RDP", "RPB"));
            if (isPcnmRedistCheck) {
                rescission.setRescind(true);
                polStatusCode = rescindRequest(bilAccount, bilPolicy, bilPolicyTerm, splitIndicator,
                        accountReviewDetails, accountProcessingAndRecovery, rescission, polStatusCode, isNsfPcnCancel);
                if (splitIndicator == ACCT_IS_SPLIT) {
                    cancellationService.callSplitBillingActivity(accountId, NoncausalType.RESCIND_REQUEST.toString(),
                            false, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                            bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                }
                Object[] statusObject = status(rescission.isRescind(), rescission.isBillPlanChange(),
                        rescission.getTermBalanceAmount(), polStatusCode, bilPolicyTerm.getBillSusFuReaCd());
                polStatusCode = (char) statusObject[0];
                isExist = true;
            } else {

                Double totalIssuedBalance = bilIstScheduleRepository.getTotalIssueBalance(accountId,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                        BusCodeTranslationType.BILLING_ITEM.getValue());
                if (totalIssuedBalance == null) {
                    totalIssuedBalance = DECIMAL_ZERO;
                }
                double termPastAmount = totalIssuedBalance;
                double termPastPercent = Math.round(totalIssuedBalance / bilActSummary.getBilAcyAmt() * 100.00);
                termDataMap.put(TermAcyType.TERMPASTAMOUNT.getValue(), termPastAmount);
                termDataMap.put(TermAcyType.TERMPASTPERCENT.getValue(), termPastPercent);

                ZonedDateTime termPcnDate = (ZonedDateTime) termDataMap.get(TermAcyType.TERMPCNDATE.getValue());
                Double nsfPcnAmount = (Double) termDataMap.get(NsfPcnDataType.NSFPCNAMOUNT.getValue());

                if (bilPolicyTerm.getPlnExpDt().compareTo(se3Date) <= 0
                        || termPcnDate.compareTo(se3Date) < 0 && totalIssuedBalance == 0
                        || termPcnDate.compareTo(se3Date) > 0
                                && termPcnDate.compareTo(DateRoutine.defaultDateTime()) != 0
                                && billDatesIndicator == CHAR_Y
                        || rescission.isForceRescind()) {

                    rescission.setRescind(postmarkRescind(accountId, bilAccount.getAccountNumber(), bilPolicyTerm,
                            bilPolicy, accountProcessingAndRecovery, termPastAmount, nsfPcnAmount, polStatusCode));
                    if (rescission.isRescind() || termPcnDate != null && termPcnDate.compareTo(se3Date) > 0
                            && termPcnDate.compareTo(DateRoutine.defaultDateTime()) != 0 && billDatesIndicator == CHAR_Y
                            || bilPolicyTerm.getPlnExpDt().compareTo(se3Date) <= 0) {
                        rescission.setRescind(true);
                        polStatusCode = rescindRequest(bilAccount, bilPolicy, bilPolicyTerm, splitIndicator,
                                accountReviewDetails, accountProcessingAndRecovery, rescission, polStatusCode,
                                isNsfPcnCancel);
                        if (splitIndicator == ACCT_IS_SPLIT) {
                            cancellationService.callSplitBillingActivity(accountId,
                                    NoncausalType.RESCIND_REQUEST.toString(), false,
                                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                        }
                        Object[] statusObject = status(rescission.isRescind(), rescission.isBillPlanChange(),
                                rescission.getTermBalanceAmount(), polStatusCode, bilPolicyTerm.getBillSusFuReaCd());
                        polStatusCode = (char) statusObject[0];
                    }
                }
            }
        }
        return new Object[] { polStatusCode, isExist };
    }

    private boolean checkPcnmByActivityCode(String accountId, ZonedDateTime lastPcnDate, Short lastPcnSequence,
            String polNumber, String symbolCode, ZonedDateTime polEffectiveDate, List<String> bilAcyDesCodeList) {
        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.getLastBilActivity(accountId, lastPcnDate,
                lastPcnSequence, polNumber, symbolCode, polEffectiveDate, bilAcyDesCodeList, BLANK_STRING);
        return bilActSummaryList != null && !bilActSummaryList.isEmpty();
    }

    private Object[] checkNoRespRenewalInvoiceDate(BilPolicyTerm bilPolicyTerm,
            AccountProcessingAndRecovery accountProcessingAndRecovery, String accountId,
            Map<String, Object> termDataMap) {

        ZonedDateTime nrrInvoiceDate = null;
        Double nrrTermPremAmount = DECIMAL_ZERO;
        Double nrrPcnAmount = DECIMAL_ZERO;
        Double nrrPcnPercent = DECIMAL_ZERO;

        if (checkPttInProgress(bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt())) {
            return new Object[] { nrrInvoiceDate, nrrPcnPercent, nrrPcnAmount, nrrTermPremAmount };
        }

        if (accountProcessingAndRecovery.getDriverIndicator() == NO_RESP_RENEWAL) {
            nrrInvoiceDate = accountProcessingAndRecovery.getRecoveryDate();
        } else {
            nrrInvoiceDate = bilIstScheduleRepository.getMaxBilInvoiceDateByPayItem(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    BusCodeTranslationType.BILLING_ITEM.getValue());
            if (nrrInvoiceDate == null) {
                termDataMap.put(TermAcyType.TERMPCNDATE.getValue(), DateRoutine.defaultDateTime());
                termDataMap.put(TermAcyType.TERMPASTDATE.getValue(), DateRoutine.defaultDateTime());
                nrrInvoiceDate = DateRoutine.defaultDateTime();
            }
        }

        if (nrrInvoiceDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
            List<Object[]> nsfSumAmounts = bilIstScheduleRepository.findNsfSumAmountsByInvoiceDate(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), nrrInvoiceDate,
                    BusCodeTranslationType.BILLING_ITEM.getValue());
            if (nsfSumAmounts != null && !nsfSumAmounts.isEmpty()) {
                nrrTermPremAmount = (Double) nsfSumAmounts.get(0)[0];
                nrrPcnAmount = (Double) nsfSumAmounts.get(0)[1];
            }
        }
        if (nrrTermPremAmount == null && nrrPcnAmount == null) {
            nrrTermPremAmount = DECIMAL_ZERO;
        }
        if (nrrPcnAmount != 0) {
            nrrPcnPercent = (double) Math.round(nrrPcnAmount / nrrTermPremAmount * 100.00);
        }

        return new Object[] { nrrInvoiceDate, nrrPcnPercent, nrrPcnAmount, nrrTermPremAmount };
    }

    private char nsfRescind(char polStatusCode, AccountProcessingAndRecovery accountProcessingAndRecovery,
            BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy, char splitIndicator,
            AccountReviewDetails accountReviewDetails, Rescission rescission, Map<String, Object> termDataMap) {

        String billSusFuReaCode = bilPolicyTerm.getBillSusFuReaCd();
        boolean isNsfPcnCancel = (boolean) termDataMap.get(NsfPcnDataType.NSFPCNCANCEL.getValue());

        if (checkPttInProgress(bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt())) {
            return polStatusCode;
        } else if (rescission.isForceRescind()) {
            rescission.setRescind(true);
            polStatusCode = rescindRequest(bilAccount, bilPolicy, bilPolicyTerm, splitIndicator, accountReviewDetails,
                    accountProcessingAndRecovery, rescission, polStatusCode, isNsfPcnCancel);

            Object[] statusObject = status(rescission.isRescind(), rescission.isBillPlanChange(),
                    rescission.getTermBalanceAmount(), polStatusCode, billSusFuReaCode);
            polStatusCode = (char) statusObject[0];
        } else {
            ZonedDateTime nrvNextAcyDate = bilPolicyTerm.getBptStatusEffDt();
            BilPolActivity bilPolActivity = bilPolActivityRepository.findById(
                    new BilPolActivityId(bilAccount.getAccountId(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                            bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "NBL"))
                    .orElse(null);
            if (bilPolActivity != null
                    && bilPolActivity.getBilNxtAcyDt().compareTo(DateRoutine.defaultDateTime()) == 0) {
                rescission.setRescind(true);
                polStatusCode = rescindRequest(bilAccount, bilPolicy, bilPolicyTerm, splitIndicator,
                        accountReviewDetails, accountProcessingAndRecovery, rescission, polStatusCode, isNsfPcnCancel);
                if (splitIndicator == ACCT_IS_SPLIT) {
                    cancellationService.callSplitBillingActivity(bilAccount.getAccountId(),
                            NoncausalType.RESCIND_REQUEST.toString(), false,
                            bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                            bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                }
                Object[] statusObject = status(rescission.isRescind(), rescission.isBillPlanChange(),
                        rescission.getTermBalanceAmount(), polStatusCode, billSusFuReaCode);
                polStatusCode = (char) statusObject[0];
                return polStatusCode;
            }

            Double nsfPcnAmount = (Double) termDataMap.get(NsfPcnDataType.NSFPCNAMOUNT.getValue());
            Double nsfPcnPercent = (Double) termDataMap.get(NsfPcnDataType.NSFPCNPERCENT.getValue());

            if (bilPolActivity != null) {
                termDataMap.put(TermAcyType.TERMINVOICEINDICATOR.getValue(), CHAR_Y);
                Double[] nsfPcnObject = nsfNeverBilledAmount(bilAccount.getAccountId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilPolActivity.getBilNxtAcyDt());
                nsfPcnPercent = nsfPcnObject[0];
                nsfPcnAmount = nsfPcnObject[1];
            } else {

                char bisTermInvoiceIndicator = (char) termDataMap.get(TermAcyType.TERMINVOICEINDICATOR.getValue());
                boolean isTermNeverBilled = checkIsTermNeverBilled(bisTermInvoiceIndicator, bilPolicyTerm);
                if (!isTermNeverBilled) {
                    Object[] calcNsfPcnObject = calcNsfPcnAmount(bilAccount.getAccountId(), bilPolicyTerm,
                            nrvNextAcyDate, isNsfPcnCancel);
                    isNsfPcnCancel = (boolean) calcNsfPcnObject[0];
                    nsfPcnAmount = (Double) calcNsfPcnObject[1];
                    nsfPcnPercent = (Double) calcNsfPcnObject[2];
                    termDataMap.put(NsfPcnDataType.NSFPCNCANCEL.getValue(), isNsfPcnCancel);
                }

            }

            termDataMap.put(TermAcyType.TERMPASTAMOUNT.getValue(), nsfPcnAmount);
            termDataMap.put(TermAcyType.TERMPASTPERCENT.getValue(), nsfPcnPercent);
            termDataMap.put(NsfPcnDataType.NSFPCNAMOUNT.getValue(), nsfPcnAmount);
            termDataMap.put(NsfPcnDataType.NSFPCNPERCENT.getValue(), nsfPcnPercent);

        }
        return polStatusCode;
    }

    private boolean checkPttInProgress(String policyId, ZonedDateTime polEffectiveDate) {
        List<BilPolicyTerm> pttPolicyTerms = bilPolicyTermRepository.findBilPolicyTermByStatus(policyId,
                polEffectiveDate, '4');
        return pttPolicyTerms != null && !pttPolicyTerms.isEmpty();
    }

    private boolean checkIsTermNeverBilled(char bisTermInvoiceIndicator, BilPolicyTerm bilPolicyTerm) {

        if (isFutureInvoicedTerm(bisTermInvoiceIndicator)) {
            return true;
        } else if (isCurrentInvoicedTerm(bisTermInvoiceIndicator)) {
            return false;
        } else {
            List<Character> bisCheckList = bilIstScheduleRepository.fetchInvoiceCode(
                    bilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), getInvoiceCode());
            if (bisCheckList == null || bisCheckList.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    private Object[] calcNsfPcnAmount(String accountId, BilPolicyTerm bilPolicyTerm, ZonedDateTime nrvNextAcyDate,
            boolean isNsfPcnCancel) {

        double nsfPcnPercent = DECIMAL_ZERO;

        Double nsfTermPremiumAmount = calculateNsfTermPremiumAmount(accountId, nrvNextAcyDate, bilPolicyTerm);
        Double nsfPcnAmount = calculateNsfPcnAmount(accountId, nrvNextAcyDate, bilPolicyTerm);
        if (nsfPcnAmount == null) {
            isNsfPcnCancel = false;
            nsfPcnAmount = DECIMAL_ZERO;
        }

        if (nsfTermPremiumAmount != DECIMAL_ZERO) {
            nsfPcnPercent = Math.round(nsfPcnAmount / nsfTermPremiumAmount * 100.00);
        }
        return new Object[] { isNsfPcnCancel, nsfPcnAmount, nsfPcnPercent };
    }

    private Double calculateNsfPcnAmount(String accountId, ZonedDateTime nrvNextAcyDate, BilPolicyTerm bilPolicyTerm) {
        Double nsfPcnAmount = bilIstScheduleRepository.findTermLastAmtByInvoiceDate(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), nrvNextAcyDate,
                BusCodeTranslationType.BILLING_ITEM.getValue());
        if (nsfPcnAmount == null) {
            ZonedDateTime nsfInvDate = bilIstScheduleRepository.getMinBilInvoiceDateByPayItem(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    BusCodeTranslationType.BILLING_ITEM.getValue());
            if (nsfInvDate == null) {
                return null;
            }

            if (nsfInvDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
                nsfPcnAmount = bilIstScheduleRepository.findTermLastAmtByInvoiceDate(accountId,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), nsfInvDate,
                        BusCodeTranslationType.BILLING_ITEM.getValue());
                if (nsfPcnAmount == null) {
                    return null;
                }
            }
        }

        return nsfPcnAmount;
    }

    private Double calculateNsfTermPremiumAmount(String accountId, ZonedDateTime nrvNextAcyDate,
            BilPolicyTerm bilPolicyTerm) {
        Double nsfTermPremiumAmount = bilIstScheduleRepository.findNsfTermPremiumAmtByInvDate(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), nrvNextAcyDate,
                BusCodeTranslationType.BILLING_ITEM.getValue());
        if (nsfTermPremiumAmount == null) {
            ZonedDateTime nsfInvDate = bilIstScheduleRepository.getMinBilInvoiceDateByPayItem(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    BusCodeTranslationType.BILLING_ITEM.getValue());
            if (nsfInvDate == null) {
                nsfInvDate = DateRoutine.defaultDateTime();
                nsfTermPremiumAmount = DECIMAL_ZERO;
            }

            if (nsfInvDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
                nsfTermPremiumAmount = bilIstScheduleRepository.findNsfTermPremiumAmtByInvDate(accountId,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), nsfInvDate,
                        BusCodeTranslationType.BILLING_ITEM.getValue());
                if (nsfTermPremiumAmount == null) {
                    nsfTermPremiumAmount = DECIMAL_ZERO;
                }
            }
        }
        return nsfTermPremiumAmount;
    }

    private Double[] nsfNeverBilledAmount(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime bilNextAcyDate) {
        double nsfPcnPercent;
        Double nsfTermPremAmount = DECIMAL_ZERO;
        Double nsfPcnAmount = DECIMAL_ZERO;
        List<Object[]> nsfSumAmounts = bilIstScheduleRepository.findNsfSumAmountsByDueDate(accountId, policyId,
                polEffectiveDate, bilNextAcyDate, BusCodeTranslationType.BILLING_ITEM.getValue());
        if (nsfSumAmounts != null && !nsfSumAmounts.isEmpty()) {
            nsfTermPremAmount = (Double) nsfSumAmounts.get(0)[0];
            nsfPcnAmount = (Double) nsfSumAmounts.get(0)[1];
        }

        if (nsfPcnAmount == null) {
            logger.debug("Get Ist Due Amount failed by accountId: {}", accountId);
            throw new DataNotFoundException("pcn.ist.due.not.found",
                    new Object[] { "Ist due amount", accountId, policyId,
                            DateRoutine.dateTimeAsMMDDYYYYAsString(polEffectiveDate),
                            DateRoutine.dateTimeAsMMDDYYYYAsString(bilNextAcyDate),
                            BusCodeTranslationType.BILLING_ITEM.getValue() });
        }

        if (nsfTermPremAmount == null || nsfTermPremAmount == DECIMAL_ZERO) {
            nsfPcnPercent = DECIMAL_ZERO;
        } else {
            nsfPcnPercent = Math.round(nsfPcnAmount / nsfTermPremAmount * 100.00);
        }
        return new Double[] { nsfPcnPercent, nsfPcnAmount };
    }

    private void processNonCausalRescind(String accountId, BilPolicyTerm bilPolicyTerm, ZonedDateTime recoveryDate) {
        String objectId = "BCMOCER";
        char polTermEpcnRule = BLANK_CHAR;
        BilStRulesUct bilStRulesUct = accountReviewService.getBilStRuleUct("SBER", bilPolicyTerm.getBillStatePvnCd(),
                bilPolicyTerm.getLobCd(), bilPolicyTerm.getMasterCompanyNbr());
        if (bilStRulesUct != null && bilStRulesUct.getBilRuleCd() == CHAR_Y) {
            polTermEpcnRule = bilStRulesUct.getBilParmListTxt().trim().charAt(0);
        }

        if (bilStRulesUct == null && polTermEpcnRule != '2') {
            return;
        }
        BilPolActivityId bilPolActivityId = new BilPolActivityId(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "CER");

        BilPolActivity bilPolActivity = bilPolActivityRepository.findById(bilPolActivityId).orElse(null);
        if (bilPolActivity == null) {
            bilPolActivity = new BilPolActivity();
            bilPolActivity.setBillPolActivityId(bilPolActivityId);
            bilPolActivity.setBilNxtAcyDt(recoveryDate);
            bilPolActivityRepository.saveAndFlush(bilPolActivity);

            String userData = generalBCMOCERData(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            logger.debug("Scheduling a row to DFR_ACY_QUE for processing {}  module", objectId);
            scheduleService.scheduleDeferActivity(objectId, userData, accountId);
        }
    }

    private String generalBCMOCERData(String accountId, String policyId, ZonedDateTime polEffectiveDt) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(StringUtils.rightPad(accountId, 8, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad(policyId, 16, BLANK_CHAR));
        stringBuilder
                .append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(polEffectiveDt), 10, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 6, BLANK_CHAR));
        return stringBuilder.toString();
    }

    private boolean checkTranTransferPending(String policyId, ZonedDateTime polEffectiveDate) {
        List<BilPolicyTerm> bilPolicyTerms = bilPolicyTermRepository.findBilPolicyTermByStatus(policyId,
                polEffectiveDate, '4');
        return bilPolicyTerms != null && !bilPolicyTerms.isEmpty();
    }

    private Object[] getBetweenDate(ZonedDateTime startDate, ZonedDateTime endDate) {
        char dateAdjustSign;
        int pendingDays;
        if (endDate.isAfter(startDate)) {
            dateAdjustSign = PLUS_SIGN;
            pendingDays = (int) ChronoUnit.DAYS.between(startDate, endDate);
        } else {
            dateAdjustSign = MINUS_SIGN;
            pendingDays = (int) ChronoUnit.DAYS.between(endDate, startDate);
        }
        return new Object[] { dateAdjustSign, pendingDays };
    }

    private StaticPcnRescind callStaticPcnRescind(String accountId, BilPolicyTerm bilPolicyTerm, char spcnRuleCode,
            String spcnRuleParam, Map<String, Object> termDataMap, char polStatusCode) {
        char spcnCashRv = spcnRuleParam.charAt(0);
        char spcnCashTrf = spcnRuleParam.charAt(1);
        char spcnPrmWoRv = spcnRuleParam.charAt(2);

        if (polStatusCode == BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue()
                || spcnRuleCode == CHAR_Y
                || spcnRuleCode == CHAR_D && (spcnCashRv == CHAR_Y || spcnCashTrf == CHAR_Y || spcnPrmWoRv == CHAR_Y)) {
            StaticPcnRescind staticPcnRescind = new StaticPcnRescind();
            ZonedDateTime termPastDate = (ZonedDateTime) termDataMap.get(TermAcyType.TERMPASTDATE.getValue());
            Double termPremiumAmount = (Double) termDataMap.get(TermAcyType.TERMPREMIUMAMOUNT.getValue());
            staticPcnRescind.setAccountId(accountId);
            staticPcnRescind.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
            staticPcnRescind.setEffectiveDate(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            staticPcnRescind.setExpirationDate(bilPolicyTerm.getPlnExpDt());
            staticPcnRescind.setPolicyStatus(polStatusCode);
            staticPcnRescind.setTermPastDueDate(termPastDate);
            staticPcnRescind.setTermPremiumAmount(termPremiumAmount);
            staticPcnRescindService.processStaticPcnRescind(staticPcnRescind);
            return staticPcnRescind;
        }
        logger.debug("Call to StaticPcnRescind module to get PcnRescind Amounts");
        return null;
    }

    private void linkPartialOrLatePaymentOnPcnPrint(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime expirationDate, double postDueAmount, char prtReason, ZonedDateTime recoveryDate) {
        logger.debug("Call to PartialOrLatePaymentOnPcnPrint module");
        PartialOrLatePaymentOnPcnPrint partialOrLatePaymentOnPcnPrint = new PartialOrLatePaymentOnPcnPrint();
        partialOrLatePaymentOnPcnPrint.setAccountId(accountId);
        partialOrLatePaymentOnPcnPrint.setPolicyId(policyId);
        partialOrLatePaymentOnPcnPrint.setEffectiveDate(polEffectiveDate);
        partialOrLatePaymentOnPcnPrint.setExpirationDate(expirationDate);
        partialOrLatePaymentOnPcnPrint.setNoRescindDate(recoveryDate);
        partialOrLatePaymentOnPcnPrint.setPastDueAmount(postDueAmount);
        partialOrLatePaymentOnPcnPrint.setReasonCode(prtReason);
        partialOrLatePaymentOnPcnPrint.setStaticReviewIndicator(CHAR_N);

        partialOrLatePaymentOnPcnPrintService.processPartialOrLatePaymentOnPcnPrint(partialOrLatePaymentOnPcnPrint);
    }

    private List<Character> getInvoiceCode() {
        List<Character> invoiceCodeList = new ArrayList<>();
        invoiceCodeList.add(InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue());
        invoiceCodeList.add(InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());
        invoiceCodeList.add(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue());
        return invoiceCodeList;
    }

    private boolean isCurrentInvoicedTerm(char invoiceTypeCode) {
        return Arrays
                .asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                        InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                        InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue())
                .stream().anyMatch(invoiceType -> invoiceType == invoiceTypeCode);
    }

    private boolean isFutureInvoicedTerm(char invoiceTypeCode) {
        return Arrays.asList(BLANK_CHAR, InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue()).stream()
                .anyMatch(invoiceType -> invoiceType == invoiceTypeCode);
    }

	private RescindNotice setRescindNoticeForRequest(String accountId, String policyId, BilAccount bilAccount,
			BilPolicy bilPolicy, BilPolicyTerm bilPolicyTerm, PcnRescindCollection pcnRescindCollection,
			ZonedDateTime recoveryDate) {
		RescindNotice rescindNotice = new RescindNotice();
		rescindNotice.setAccountId(accountId);
		rescindNotice.setBilTypeCode(bilAccount.getBillTypeCd());
		rescindNotice.setBilClassCode(bilAccount.getBillClassCd());
		rescindNotice.setAccountNumber(bilAccount.getAccountNumber());
		rescindNotice.setBilCollectionPlan(bilAccount.getCollectionPlan());
		rescindNotice.setPayClientId(bilAccount.getPayorClientId());
		rescindNotice.setPayAdrSeq(bilAccount.getAddressSequenceNumber());
		rescindNotice.setPayAdrSeqSign(PLUS_SIGN);
		if (rescindNotice.getPayAdrSeq() < 0) {
			rescindNotice.setPayAdrSeqSign(MINUS_SIGN);
		}
		if (pcnRescindCollection.getActivityDate() != null) {
			rescindNotice.setActivityDate(DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getActivityDate()));
		}
		String[] se3Date = DateRoutine.dateTimeAsYYYYMMDDHHmmssSSSString(recoveryDate).split("-");
		rescindNotice.setSe3Date(DateRoutine.dateTimeAsYYYYMMDDString(recoveryDate));
		rescindNotice.setFillerSe3DateTs("-" + se3Date[3]);
		rescindNotice.setPolicyId(policyId);
		rescindNotice.setPolicySymbolCode(bilPolicy.getPolSymbolCd());
		rescindNotice.setPolicyNumber(bilPolicy.getPolNbr());
		if (bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt() != null) {
			rescindNotice.setPolicyEffDate(
					DateRoutine.dateTimeAsYYYYMMDDString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()));
		}
		if (bilPolicyTerm.getPlnExpDt() != null) {
			rescindNotice.setPolicyExpDate(DateRoutine.dateTimeAsYYYYMMDDString(bilPolicyTerm.getPlnExpDt()));
		}
		rescindNotice.setLobCode(bilPolicyTerm.getLobCd());
		rescindNotice.setPolicyPrintRskStCode(bilPolicyTerm.getBillStatePvnCd());
		rescindNotice.setMasterCoNumber(bilPolicyTerm.getMasterCompanyNbr());
		rescindNotice.setIssueCltId(bilPolicyTerm.getBptIsuCltId());
		rescindNotice.setIssueAdrSeq(bilPolicyTerm.getBptAgtAdrSeq());
		rescindNotice.setIssueAdrSeqSign(PLUS_SIGN);
		if (rescindNotice.getIssueAdrSeq() < 0) {
			rescindNotice.setIssueAdrSeqSign(MINUS_SIGN);
		}
		rescindNotice.setAgtCltId(bilPolicyTerm.getBptAgtCltId());
		rescindNotice.setAgtAdrSeq(bilPolicyTerm.getBptAgtAdrSeq());
		rescindNotice.setAgtAdrSeqSign(PLUS_SIGN);
		if (rescindNotice.getAgtAdrSeq() < 0) {
			rescindNotice.setAgtAdrSeqSign(MINUS_SIGN);
		}
		rescindNotice.setIssueSystemId(pcnRescindCollection.getIssueSysId());
		final String programId = BCMOAX;
		rescindNotice.setReqPgmName(programId);
		rescindNotice.setQueueWriteInd(accountReviewService.getQueueWrtIndicator());
		return rescindNotice;
	}
}
