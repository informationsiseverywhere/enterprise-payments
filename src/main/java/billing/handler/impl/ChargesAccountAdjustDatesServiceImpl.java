package billing.handler.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_F;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActRules;
import billing.data.entity.BilCrgAmounts;
import billing.data.entity.BilEftPlnCrg;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSptPlnCrg;
import billing.data.entity.BilSupportPlan;
import billing.data.repository.BilEftPlnCrgRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilSptPlnCrgRepository;
import billing.handler.model.AccountAdjustDates;
import billing.handler.model.AccountAdjustSupportData;
import billing.handler.model.AdjustDatesReviewDetails;
import billing.handler.model.ChargeVariables;
import billing.handler.model.DownPaymentFee;
import billing.handler.model.FirstPossibleDueDate;
import billing.handler.model.VariableSeviceCharge;
import billing.handler.model.VariableSeviceChargeOverride;
import billing.handler.service.AccountAdjustDatesService;
import billing.handler.service.ChargesAccountAdjustDatesService;
import billing.handler.service.FirstPossibleDueDateService;
import billing.utils.AdjustDueDatesConstants.ScheduleCode;

@Service
public class ChargesAccountAdjustDatesServiceImpl implements ChargesAccountAdjustDatesService {

    @Autowired
    private AccountAdjustDatesService accountAdjustDatesService;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private BilSptPlnCrgRepository bilSptPlnCrgRepository;

    @Autowired
    private BilEftPlnCrgRepository bilEftPlnCrgRepository;

    @Autowired
    private FirstPossibleDueDateService firstPossibleDueDateService;

    private static final String RULE_XHOL = "XHOL";

    @Override
    @Transactional
    public void processChargesAccountAdjustDates(AccountAdjustDates accountAdjustDates) {

        AdjustDatesReviewDetails adjustDatesReviewDetails = accountAdjustDatesService
                .processAdjustDates(accountAdjustDates);

        BilActRules bilActRules = adjustDatesReviewDetails.getBilActRules();
        BilAccount bilAccount = adjustDatesReviewDetails.getBilAccount();
        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();

        char fpddIndicator = adjustDatesReviewDetails.getFpddIndicator();
        boolean disableFpdd = false;

        VariableSeviceCharge variableSeviceCharge = new VariableSeviceCharge();
        ChargeVariables chargeVariables = new ChargeVariables();
        VariableSeviceChargeOverride variableSeviceChargeOverride = new VariableSeviceChargeOverride();
        AccountAdjustSupportData accountAdjustSupportData = new AccountAdjustSupportData();
        if (bilAccount.getBillTypeCd().equals("SP")) {
            variableSeviceChargeOverride = accountAdjustDatesService.readOverrideServiceCharge(bilAccount);
            if (variableSeviceChargeOverride == null) {
                variableSeviceChargeOverride = new VariableSeviceChargeOverride();
            }
            if (accountAdjustDates.getAddDateIndicator() != CHAR_Y && accountAdjustDates.getAddDateIndicator() != CHAR_Q
                    && accountAdjustDates.getAddDateIndicator() != CHAR_S
                    && accountAdjustDates.getAddDateIndicator() != CHAR_F
                    && variableSeviceChargeOverride.getRuleInd() != CHAR_Y) {
                Integer count = bilSptPlnCrgRepository.getCount(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                        SHORT_ZERO, CHAR_Y, "GA");
                if (count != null && count > SHORT_ZERO) {
                    accountAdjustSupportData.setServiceChargeIndicator(CHAR_Y);
                    accountAdjustSupportData.setServiceChargeTypeCode("GA");
                } else {
                    accountAdjustSupportData.setServiceChargeIndicator(BLANK_CHAR);
                    accountAdjustSupportData.setServiceChargeTypeCode(BLANK_STRING);
                }
            }
        }

        List<BilEftPlnCrg> bilEftPlanChargeList = null;
        if (variableSeviceChargeOverride.getRuleInd() != CHAR_Y && adjustDatesReviewDetails.isEftCollectionMethod()) {
            bilEftPlanChargeList = bilEftPlnCrgRepository.findByCollectionPlan(bilAccount.getCollectionPlan());
        }

        List<BilCrgAmounts> downPaymentList = null;
        DownPaymentFee downPaymentFee = new DownPaymentFee();
        if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y
                && (adjustDatesReviewDetails.isDpifRule() || accountAdjustDates.getSupportPlanIndicator() == CHAR_Y)) {
            downPaymentList = accountAdjustDatesService.getFutureDownpaymentRows(accountAdjustDates.getAccountId(),
                    downPaymentFee);
        }

        List<BilSptPlnCrg> bilSupportPlanChargeList = bilSptPlnCrgRepository
                .findByBilTypeCodeClassCode(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd());

        if (bilSupportPlanChargeList.size() == SHORT_ONE) {
            BilSptPlnCrg bilSptPlnCrg = bilSupportPlanChargeList.get(SHORT_ZERO);
            accountAdjustSupportData.setServiceChargeIndicator(bilSptPlnCrg.getBilSptScgInd());
            accountAdjustSupportData.setServiceChargeTypeCode(bilSptPlnCrg.getBilScgTypeCd());
            accountAdjustSupportData.setServiceChargeCode(bilSptPlnCrg.getBilScgActionCd());
            accountAdjustSupportData.setServiceChargeAmount(bilSptPlnCrg.getBilSvcCrgAmt());
            accountAdjustSupportData.setPremiumAmount(bilSptPlnCrg.getBilPrmAmt());
            accountAdjustSupportData.setSeviceChargeIncAmount(bilSptPlnCrg.getBilScgIcrAmt());
            accountAdjustSupportData.setPremiumIncAmount(bilSptPlnCrg.getBilPrmIcrAmt());
            accountAdjustSupportData.setMaxServiceChargeAmount(bilSptPlnCrg.getBilMaxScgAmt());
        } else {
            accountAdjustSupportData.setServiceChargeIndicator(bilSupportPlan.getBspScgChgInd());
            accountAdjustSupportData.setServiceChargeTypeCode(bilSupportPlan.getBspScgchgTypCd());
            accountAdjustSupportData.setServiceChargeCode(bilSupportPlan.getBspSvcCd());
            accountAdjustSupportData.setServiceChargeAmount(bilSupportPlan.getBspScgAmt());
            accountAdjustSupportData.setPremiumAmount(bilSupportPlan.getBspPrmAmt());
            accountAdjustSupportData.setSeviceChargeIncAmount(bilSupportPlan.getBspScgIncAmt());
            accountAdjustSupportData.setPremiumIncAmount(bilSupportPlan.getBspPrmIncAmt());
            accountAdjustSupportData.setMaxServiceChargeAmount(bilSupportPlan.getBspMaxScgAmt());
        }

        boolean isXHOLRule = false;
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(RULE_XHOL, BLANK_STRING, BLANK_STRING,
                BLANK_STRING, accountAdjustDates.getProcessDate(), accountAdjustDates.getProcessDate());

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == 'Y' && bilRulesUct.getBrtParmListTxt() != null
                && !bilRulesUct.getBrtParmListTxt().trim().isEmpty()) {
            isXHOLRule = true;

        }

        char previousSchedule = ScheduleCode.NO_SCHEDULE.getValue();
        if (fpddIndicator == CHAR_Y) {
            previousSchedule = accountAdjustDatesService.getPreviousSchedule(adjustDatesReviewDetails.isAccountBilled(),
                    accountAdjustDates);
            if (previousSchedule == ScheduleCode.FUTURE_SCHEDULE.getValue()) {
                previousSchedule = ScheduleCode.NO_SCHEDULE.getValue();
            }
        }

        FirstPossibleDueDate firstPossibleDueDate = firstPossibleDueDateService.processFirstPossibleDueDateLogic(
                bilActRules.getBruIncWknInd(), isXHOLRule, bilRulesUct, accountAdjustDates, adjustDatesReviewDetails);

        Object[] newDatesData = firstPossibleDueDateService.checkDateAdd(false, adjustDatesReviewDetails, bilAccount,
                accountAdjustDates, isXHOLRule, bilRulesUct);
        ZonedDateTime newReferemceDate = (ZonedDateTime) newDatesData[0];
        ZonedDateTime newAdjustDate = (ZonedDateTime) newDatesData[1];
        ZonedDateTime newInvoiceDate = (ZonedDateTime) newDatesData[2];
        ZonedDateTime newSystemDate = (ZonedDateTime) newDatesData[3];
        char dateType = (char) newDatesData[4];
        adjustDatesReviewDetails.setNewAdjustDueDate(newAdjustDate);
        adjustDatesReviewDetails.setNewReferenceDate(newReferemceDate);
        adjustDatesReviewDetails.setNewInvoiceDate(newInvoiceDate);
        adjustDatesReviewDetails.setNewSystemDate(newSystemDate);
        adjustDatesReviewDetails.setDateType(dateType);

        accountAdjustDatesService.processDatesAssignment(dateType, bilAccount, accountAdjustDates,
                adjustDatesReviewDetails, firstPossibleDueDate, variableSeviceChargeOverride, variableSeviceCharge,
                chargeVariables, accountAdjustSupportData, disableFpdd, bilEftPlanChargeList, bilSupportPlanChargeList,
                isXHOLRule, bilRulesUct, bilSupportPlan);

        accountAdjustDatesService.processChargeRows(SHORT_ZERO, previousSchedule, bilAccount, accountAdjustDates,
                firstPossibleDueDate, adjustDatesReviewDetails, isXHOLRule, bilRulesUct, bilEftPlanChargeList,
                bilSupportPlanChargeList, variableSeviceChargeOverride, variableSeviceCharge, accountAdjustSupportData,
                chargeVariables, downPaymentList, downPaymentFee);
    }

}
