package billing.handler.impl;

import static billing.utils.BillingConstants.SYSTEM_USER_ID;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_E;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.COMMA_STRING;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import application.utils.service.ActivityService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilPolActivity;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilPrmSchedule;
import billing.data.entity.HalBoMduXrf;
import billing.data.entity.id.BilPolActivityId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.entity.id.BilPrmScheduleId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPrmScheduleRepository;
import billing.data.repository.HalBoMduXrfRepository;
import billing.handler.model.PolicyPcnAndRescInterface;
import billing.handler.service.CancellationService;
import billing.handler.service.RequestPolicyPcnOrRescindPiService;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.WipAcitivityId;
import billing.utils.BillingConstants.WipObjectCode;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.schedule.service.ScheduleService;
import core.utils.DateRoutine;

@Service
public class RequestPolicyPcnOrRescindPiServiceImpl implements RequestPolicyPcnOrRescindPiService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestPolicyPcnOrRescindPiServiceImpl.class);

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private BilPrmScheduleRepository bilPrmScheduleRepository;

    @Autowired
    private HalBoMduXrfRepository halBoMduXrfRepository;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private CancellationService cancellationService;

    private static final String PCN_REQ_MODULE = "BIL-INT-BCMOPI10-PCN-REQ-";
    private static final String RESCIND_REQ_MODULE = "BIL-INT-BCMOPI10-RESCIND-REQ-";
    private static final String CDT_CNC_REQ_MODULE = "BIL-INT-BCMOPI10-CDT-CNC-REQ-";
    private static final String AUTO_REI_REQ_MODULE = "BIL-INT-BCMOPI10-AUTO-REI-REQ-";
    private static final String AUTO_RNT_REQ_MODULE = "BIL-INT-BCMOPI10-AUTO-RNT-REQ-";
    private static final String DEFAULT_BUS_CASE = "ACTCNCDATE";
    private static final String CANCEL_ID = "CANTT";
    private static final char STANDARD_CNC = CHAR_S;
    private static final char EQUITY_CNC = CHAR_E;
    private static final List<String> bilAcyTypeCodes = new ArrayList<>(Arrays.asList(BLANK_STRING, "CAM", "NQM"));

    @Override
    public String[] processPcnOrRescind(PolicyPcnAndRescInterface policyPcnAndRescInterface) {

        String[] editMessage = editParms(policyPcnAndRescInterface);
        if (editMessage.length == 0) {

            switch (RequestPolicyType.getEnumValue(policyPcnAndRescInterface.getActionCode())) {
            case PCN_ACTION:
                processPcn(policyPcnAndRescInterface);
                sendRequestToIssSys(policyPcnAndRescInterface);
                break;

            case RESCIND_ACTION:
                boolean isRescindNotice = processRescind(policyPcnAndRescInterface);
                BilActSummary bilActSummary = getLastPcnBas(policyPcnAndRescInterface.getAccountId(),
                        policyPcnAndRescInterface.getPolNumber(), policyPcnAndRescInterface.getPolSymbolCode(),
                        policyPcnAndRescInterface.getEffectiveDate());
                if (bilActSummary != null
                        && bilActSummary.getBilAcyDes2Dt().compareTo(DateRoutine.defaultDateTime()) == 0) {
                    fetchBilPrmScheduleData(bilActSummary, policyPcnAndRescInterface.getAccountId(),
                            policyPcnAndRescInterface.getPolicyId(), policyPcnAndRescInterface.getEffectiveDate());
                }

                if (isRescindNotice) {
                    sendRequestToIssSys(policyPcnAndRescInterface);
                }
                break;

            case REQ_CNC_ACTION:
            case AUTO_REI_ACTION:
            case AUTO_RNT_ACTION:
                sendRequestToIssSys(policyPcnAndRescInterface);
                break;

            default:
                break;
            }
        }

        return editMessage;
    }

    private void fetchBilPrmScheduleData(BilActSummary bilActSummary, String accountId, String policyId,
            ZonedDateTime polEffDate) {
        String busCaseId = DEFAULT_BUS_CASE;
        char processInd = CHAR_N;

        updatePcnHistory(bilActSummary);

        List<BilPrmSchedule> bilPrmScheduleList = bilPrmScheduleRepository.findBilPrmScheduleByBusCase(accountId,
                policyId, busCaseId, processInd);
        if (bilPrmScheduleList != null && !bilPrmScheduleList.isEmpty()) {
            for (BilPrmSchedule bilPrmSchedule : bilPrmScheduleList) {
                String parmPolEftDate = bilPrmSchedule.getBpsParmArea().substring(57, 67);
                if (polEffDate.compareTo(DateRoutine.dateTimeAsYYYYMMDD(parmPolEftDate)) == 0) {
                    bilPrmSchedule.setProcessInd(CHAR_Y);
                    bilPrmScheduleRepository.saveAndFlush(bilPrmSchedule);
                }
            }
        }
    }

    private void updatePcnHistory(BilActSummary bilActSummary) {
        String bilAcyDes2Date = bilActSummary.getBasAddDataTxt().substring(3, 13);
        if (!bilAcyDes2Date.trim().isEmpty()) {
            bilActSummary.setBilAcyDes2Dt(DateRoutine.dateTimeAsYYYYMMDD(bilAcyDes2Date));
        }

        bilActSummary.setBilAcyTs(ZonedDateTime.now());
        bilActSummary.setUserId(SYSTEM_USER_ID);
        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

    private boolean processRescind(PolicyPcnAndRescInterface policyPcnAndRescInterface) {

        BilPolActivityId bilPolActivityId = new BilPolActivityId(policyPcnAndRescInterface.getAccountId(),
                policyPcnAndRescInterface.getPolicyId(), policyPcnAndRescInterface.getEffectiveDate(), "CDT");
        BilPolActivity bilPolActivity = bilPolActivityRepository.findById(bilPolActivityId).orElse(null);
        if (bilPolActivity != null) {
            bilPolActivityRepository.deleteById(bilPolActivityId);
        }

        if (policyPcnAndRescInterface.getUserId().equalsIgnoreCase(CANCEL_ID)) {
            bilPolActivityId.setPolEffectiveDt(policyPcnAndRescInterface.getPlanExpDate());
            bilPolActivity = bilPolActivityRepository.findById(bilPolActivityId).orElse(null);
            if (bilPolActivity != null) {
                bilPolActivityRepository.deleteById(bilPolActivityId);
            }
        }

        if (bilPolActivity == null) {
            postActivityDriver(policyPcnAndRescInterface);
        }
        boolean isRescindNotice = false;
        List<String> bilAcyDesCdList = new ArrayList<>(Arrays.asList("CNR"));
        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.getMaxRescindData(
                policyPcnAndRescInterface.getAccountId(), policyPcnAndRescInterface.getPolNumber(),
                policyPcnAndRescInterface.getPolSymbolCode(), policyPcnAndRescInterface.getEffectiveDate(),
                bilAcyDesCdList);
        if (bilActSummaryList != null && !bilActSummaryList.isEmpty()) {
            isRescindNotice = checkPriorPcn(policyPcnAndRescInterface.getAccountId(),
                    policyPcnAndRescInterface.getPolNumber(), policyPcnAndRescInterface.getPolSymbolCode(),
                    policyPcnAndRescInterface.getEffectiveDate(),
                    bilActSummaryList.get(SHORT_ZERO).getBillActSummaryId().getBilAcyDt(),
                    bilActSummaryList.get(SHORT_ZERO).getBillActSummaryId().getBilAcySeq());
        } else {
            isRescindNotice = true;
        }

        return isRescindNotice;
    }

    private boolean checkPriorPcn(String accountId, String polNumber, String polSymbolCode, ZonedDateTime effectiveDate,
            ZonedDateTime priorCnrDate, short priorCnrSeq) {
        boolean isPriorPcn = false;
        List<String> bilAcyDesCdList = new ArrayList<>(Arrays.asList("C"));
        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.getMaxRescindData(accountId, polNumber,
                polSymbolCode, effectiveDate, bilAcyDesCdList);
        if (bilActSummaryList != null && !bilActSummaryList.isEmpty()) {
            BilActSummary bilActSummary = bilActSummaryList.get(SHORT_ZERO);

            if ((priorCnrDate.compareTo(bilActSummary.getBillActSummaryId().getBilAcyDt()) < 0)
                    && (priorCnrDate.compareTo(bilActSummary.getBillActSummaryId().getBilAcyDt()) == 0
                            && priorCnrSeq < bilActSummary.getBillActSummaryId().getBilAcySeq())) {
                isPriorPcn = true;
            }
        }

        return isPriorPcn;
    }

    private void postActivityDriver(PolicyPcnAndRescInterface policyPcnAndRescInterface) {
        LOGGER.debug("Send request to Activity Api for processing Rescind");
        BilAccount bilAccount = bilAccountRepository.findById(policyPcnAndRescInterface.getAccountId()).orElse(null);
        if (bilAccount == null) {
            throw new DataNotFoundException("no.bil.account.exists",
                    new Object[] { policyPcnAndRescInterface.getAccountId() });
        }
        activityService.postActivity(WipAcitivityId.RESCIND.getValue(), WipObjectCode.BILL_POLICY.getValue(),
                bilAccount.getAccountNumber(), bilAccount.getAccountId());
    }

    private void processPcn(PolicyPcnAndRescInterface policyPcnAndRescInterface) {
        ZonedDateTime cancelDate = null;
        if (policyPcnAndRescInterface.getCancelType() == STANDARD_CNC
                && policyPcnAndRescInterface.getStandCncDate() != null) {
            cancelDate = policyPcnAndRescInterface.getStandCncDate();
        } else if (policyPcnAndRescInterface.getCancelType() == EQUITY_CNC
                && policyPcnAndRescInterface.getEquityDate() != null) {
            cancelDate = policyPcnAndRescInterface.getEquityDate();
        } else {
            throw new InvalidDataException("invalid.cnc.type.date");
        }

        if (policyPcnAndRescInterface.getRestartPcnIndicator() == CHAR_Y
                && policyPcnAndRescInterface.getRestartPcnDate() != null) {
            cancelDate = policyPcnAndRescInterface.getRestartPcnDate();
        }

        BilPolActivityId bilPolActivityId = new BilPolActivityId(policyPcnAndRescInterface.getAccountId(),
                policyPcnAndRescInterface.getPolicyId(), policyPcnAndRescInterface.getEffectiveDate(), "CDT");
        ZonedDateTime bilNextAcyDate = overrideCdtTriggerDate(policyPcnAndRescInterface, cancelDate);
        insertBilPolActivity(bilPolActivityId, bilNextAcyDate);

        if (policyPcnAndRescInterface.getUserId().equalsIgnoreCase(CANCEL_ID)) {
            bilPolActivityId.setPolEffectiveDt(policyPcnAndRescInterface.getPlanExpDate());
            insertBilPolActivity(bilPolActivityId, bilNextAcyDate);
        }

        Short maxBilSeqNumber = bilPrmScheduleRepository.findMaxBilSequenceNumber(
                policyPcnAndRescInterface.getAccountId(), policyPcnAndRescInterface.getPolicyId());
        if (maxBilSeqNumber == null) {
            maxBilSeqNumber = 0;
        } else {
            maxBilSeqNumber = (short) (maxBilSeqNumber + 1);
        }

        String bilParmArea = createPrmScheduleParam(policyPcnAndRescInterface, cancelDate);
        BilPrmSchedule bilPrmSchedule = new BilPrmSchedule();
        BilPrmScheduleId bilPrmScheduleId = new BilPrmScheduleId(policyPcnAndRescInterface.getAccountId(),
                policyPcnAndRescInterface.getPolicyId(), maxBilSeqNumber);
        bilPrmSchedule.setBilPrmScheduleId(bilPrmScheduleId);
        bilPrmSchedule.setBpsParmArea(bilParmArea);
        bilPrmSchedule.setProcessInd(CHAR_N);
        bilPrmSchedule.setBusCaseId(DEFAULT_BUS_CASE);
        bilPrmScheduleRepository.saveAndFlush(bilPrmSchedule);
    }

    private String createPrmScheduleParam(PolicyPcnAndRescInterface policyPcnAndRescInterface,
            ZonedDateTime cancelDate) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(StringUtils.leftPad("ACT", 3, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(policyPcnAndRescInterface.getPolicyId(), 16, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(policyPcnAndRescInterface.getPolNumber(), 25, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(policyPcnAndRescInterface.getPolSymbolCode(), 3, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(cancelDate), 10, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getEffectiveDate()), 10, BLANK_STRING));
        stringBuilder.append(CHAR_N);
        stringBuilder.append(StringUtils.rightPad(policyPcnAndRescInterface.getProgramName(), 8, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(policyPcnAndRescInterface.getParaName(), 30, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad("0", 8, "0"));
        stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 100, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(policyPcnAndRescInterface.getAccountId(), 8, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getBilAcyDate()), 10, BLANK_STRING));
        stringBuilder
                .append(StringUtils.leftPad(String.valueOf(policyPcnAndRescInterface.getBilSequenceNumber()), 6, "0"));
        stringBuilder.append(StringUtils.rightPad(policyPcnAndRescInterface.getBilAcyDesCode(), 3, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(policyPcnAndRescInterface.getBilDesReaType(), 3, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 41, BLANK_STRING));

        return stringBuilder.toString();
    }

    private void insertBilPolActivity(BilPolActivityId bilPolActivityId, ZonedDateTime bilNextAcyDate) {
        BilPolActivity bilPolActivity = bilPolActivityRepository.findById(bilPolActivityId).orElse(null);
        if (bilPolActivity == null || bilPolActivity.getBilNxtAcyDt().compareTo(bilNextAcyDate) != 0) {
            bilPolActivity = new BilPolActivity();
            bilPolActivity.setBilNxtAcyDt(bilNextAcyDate);
            bilPolActivity.setBillPolActivityId(bilPolActivityId);
            bilPolActivityRepository.saveAndFlush(bilPolActivity);
        }
    }

    private ZonedDateTime overrideCdtTriggerDate(PolicyPcnAndRescInterface policyPcnAndRescInterface,
            ZonedDateTime bilNextAcyDate) {
        short ldnLegDays = SHORT_ZERO;
        boolean legalDaysFndInd = false;
        if (policyPcnAndRescInterface.getPcnFlatCncIndicator() != CHAR_Y
                && policyPcnAndRescInterface.getPolPcnReason().equalsIgnoreCase("RNP")) {
            bilNextAcyDate = calcLegalDate(policyPcnAndRescInterface);
        }

        BilPolicy bilPolicy = new BilPolicy();
        bilPolicy.setPolNbr(policyPcnAndRescInterface.getPolNumber());
        bilPolicy.setPolSymbolCd(policyPcnAndRescInterface.getPolSymbolCode());
        BilPolicyTerm bilPolicyTerm = new BilPolicyTerm();
        BilPolicyTermId bilPolicyTermId = new BilPolicyTermId(policyPcnAndRescInterface.getAccountId(),
                policyPcnAndRescInterface.getPolicyId(), policyPcnAndRescInterface.getEffectiveDate());
        bilPolicyTerm.setBillPolicyTermId(bilPolicyTermId);
        bilPolicyTerm.setMasterCompanyNbr(policyPcnAndRescInterface.getMasterCompanyNumber());
        bilPolicyTerm.setBillStatePvnCd(policyPcnAndRescInterface.getStatePvnCode());
        bilPolicyTerm.setBptIssueSysId(policyPcnAndRescInterface.getIssueSysId());
        bilPolicyTerm.setLobCd(policyPcnAndRescInterface.getLineOfBusinessCode());

        Short[] policyLegalObject = cancellationService.getPolicyLegalDays(BilDesReasonType.DELAY_DAYS.getValue(),
                policyPcnAndRescInterface.getIssueSysId(), bilPolicyTerm, bilPolicy);
        if (policyLegalObject != null) {
            ldnLegDays = policyLegalObject[2];
            legalDaysFndInd = true;
        }
        if (!legalDaysFndInd) {
            return bilNextAcyDate;
        }

        bilNextAcyDate = DateRoutine.getAdjustedDate(bilNextAcyDate, false, ldnLegDays, null);
        return bilNextAcyDate;
    }

    private ZonedDateTime calcLegalDate(PolicyPcnAndRescInterface policyPcnAndRescInterface) {
        ZonedDateTime fromDate = null;
        if (policyPcnAndRescInterface.getRestartPcnIndicator() == CHAR_Y) {
            fromDate = getOriginalPcnAcyDate(policyPcnAndRescInterface.getAccountId(),
                    policyPcnAndRescInterface.getPolNumber(), policyPcnAndRescInterface.getPolSymbolCode(),
                    policyPcnAndRescInterface.getEffectiveDate());
        } else {
            fromDate = policyPcnAndRescInterface.getActivityDate();
        }
        short daysToCancel = (short) (policyPcnAndRescInterface.getLdnLegalDays()
                + policyPcnAndRescInterface.getLdnMailDays());
        return DateRoutine.getAdjustedDate(fromDate, false, daysToCancel, null);
    }

    private ZonedDateTime getOriginalPcnAcyDate(String accountId, String polNumber, String polSymbolCode,
            ZonedDateTime effectiveDate) {
        ZonedDateTime maxPcnDate = bilActSummaryRepository.findOriginPcnDate(accountId, polNumber, polSymbolCode,
                effectiveDate, new ArrayList<>(Arrays.asList("C", "NC")), bilAcyTypeCodes);
        if (maxPcnDate == null) {
            throw new DataNotFoundException("8200.select.original.pcn");
        }
        return maxPcnDate;
    }

    private BilActSummary getLastPcnBas(String accountId, String polNumber, String polSymbolCode,
            ZonedDateTime polEffDate) {
        BilActSummary lastPcnHistory = null;
        List<BilActSummary> bilActSummaries = bilActSummaryRepository.findUserIdRescinded(accountId, polNumber,
                polSymbolCode, polEffDate, new ArrayList<>(Arrays.asList("RCN", "C", "NC")), bilAcyTypeCodes);
        if (bilActSummaries != null && !bilActSummaries.isEmpty()) {
            lastPcnHistory = bilActSummaries.get(0);
        }
        return lastPcnHistory;
    }

    private void sendRequestToIssSys(PolicyPcnAndRescInterface policyPcnAndRescInterface) {
        final String interfaceOnlineModule = "BCMOIX10";
        String interfaceObjectName = BLANK_STRING;

        switch (RequestPolicyType.getEnumValue(policyPcnAndRescInterface.getActionCode())) {
        case PCN_ACTION:
            interfaceObjectName = PCN_REQ_MODULE;
            break;
        case RESCIND_ACTION:
            interfaceObjectName = RESCIND_REQ_MODULE;
            break;
        case REQ_CNC_ACTION:
            interfaceObjectName = CDT_CNC_REQ_MODULE;
            break;
        case AUTO_REI_ACTION:
            interfaceObjectName = AUTO_REI_REQ_MODULE;
            break;
        case AUTO_RNT_ACTION:
            interfaceObjectName = AUTO_RNT_REQ_MODULE;
            break;
        default:
            break;
        }
        interfaceObjectName = interfaceObjectName + policyPcnAndRescInterface.getIssueSysId();
        String moduleId = getModuleObjectId(interfaceObjectName);
        if (moduleId.equalsIgnoreCase(interfaceOnlineModule)) {
            LOGGER.debug("Scheduling BCMOIX10 into Defer Que system");
            String userParam = createPcnRntOnlineParam(policyPcnAndRescInterface);
            scheduleService.scheduleDeferActivity(moduleId, userParam);
        }
    }

    private String createPcnRntOnlineParam(PolicyPcnAndRescInterface policyPcnAndRescInterface) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(policyPcnAndRescInterface.getActionCode());
        String activityDate = policyPcnAndRescInterface.getActivityDate() == null ? BLANK_STRING
                : DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getActivityDate());
        stringBuilder.append(StringUtils.leftPad(activityDate, 10, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getRequestProgramName(), 8, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getContext(), 20, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getAccountId(), 8, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getPolicyId(), 16, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getIssueSysId(), 2, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getPolSymbolCode(), 3, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getPolNumber(), 25, BLANK_STRING));
        String effectiveDate = policyPcnAndRescInterface.getEffectiveDate() == null ? BLANK_STRING
                : DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getEffectiveDate());
        stringBuilder.append(StringUtils.leftPad(effectiveDate, 10, BLANK_STRING));
        String expirationDate = policyPcnAndRescInterface.getPlanExpDate() == null ? BLANK_STRING
                : DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getPlanExpDate());
        stringBuilder.append(StringUtils.leftPad(expirationDate, 10, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getLineOfBusinessCode(), 3, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getMasterCompanyNumber(), 2, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getStatePvnCode(), 3, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(String.valueOf(policyPcnAndRescInterface.getLdnLegalDays()), 4, "0"));
        stringBuilder.append(StringUtils.leftPad(String.valueOf(policyPcnAndRescInterface.getLdnMailDays()), 4, "0"));
        stringBuilder.append(StringUtils.leftPad(String.valueOf(policyPcnAndRescInterface.getLdnMaxDays()), 4, "0"));
        stringBuilder.append(policyPcnAndRescInterface.getCancelType());
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getPolPcnReason(), 3, BLANK_STRING));
        String standCncDate = policyPcnAndRescInterface.getStandCncDate() == null ? BLANK_STRING
                : DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getStandCncDate());
        stringBuilder.append(StringUtils.leftPad(standCncDate, 10, BLANK_STRING));
        String equityDate = policyPcnAndRescInterface.getEquityDate() == null ? BLANK_STRING
                : DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getEquityDate());
        stringBuilder.append(StringUtils.leftPad(equityDate, 10, BLANK_STRING));
        stringBuilder.append(policyPcnAndRescInterface.getRestartPcnIndicator());
        String restartPcnDate = policyPcnAndRescInterface.getRestartPcnDate() == null ? BLANK_STRING
                : DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getRestartPcnDate());
        stringBuilder.append(StringUtils.leftPad(restartPcnDate, 10, BLANK_STRING));
        String cdtCncEffDate = policyPcnAndRescInterface.getOriginEffectiveDate() == null ? BLANK_STRING
                : DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getOriginEffectiveDate());
        stringBuilder.append(StringUtils.leftPad(cdtCncEffDate, 10, BLANK_STRING));
        String reinstEffDate = policyPcnAndRescInterface.getEffectiveDate() == null ? BLANK_STRING
                : DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getEffectiveDate());
        stringBuilder.append(StringUtils.leftPad(reinstEffDate, 10, BLANK_STRING));
        String rntNewEffDate = policyPcnAndRescInterface.getRntNewEffDate() == null ? BLANK_STRING
                : DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getRntNewEffDate());
        stringBuilder.append(StringUtils.leftPad(rntNewEffDate, 10, BLANK_STRING));
        String rntNewExpDate = policyPcnAndRescInterface.getNewExpDate() == null ? BLANK_STRING
                : DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getNewExpDate());
        stringBuilder.append(StringUtils.leftPad(rntNewExpDate, 10, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getRntReaAmdCode(), 3, BLANK_STRING));
        stringBuilder.append(policyPcnAndRescInterface.getEffTypeCode());
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getUserId(), 8, BLANK_STRING));
        stringBuilder.append(policyPcnAndRescInterface.getAutoCollectionMethodChange());
        stringBuilder.append(policyPcnAndRescInterface.getNsfActivityIndicator());
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getAccountId(), 8, BLANK_STRING));
        String bilAcyDate = policyPcnAndRescInterface.getBilAcyDate() == null ? BLANK_STRING
                : DateRoutine.dateTimeAsYYYYMMDDString(policyPcnAndRescInterface.getBilAcyDate());
        stringBuilder.append(StringUtils.leftPad(bilAcyDate, 10, BLANK_STRING));
        stringBuilder
                .append(StringUtils.leftPad(String.valueOf(policyPcnAndRescInterface.getBilSequenceNumber()), 6, "0"));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getBilAcyDesCode(), 3, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getBilDesReaType(), 3, BLANK_STRING));
        stringBuilder.append(CHAR_N);
        stringBuilder.append(policyPcnAndRescInterface.getReinstNtcIndicator());
        stringBuilder.append(policyPcnAndRescInterface.getPcnFlatCncIndicator());
        stringBuilder.append(policyPcnAndRescInterface.getReinstCode());
        stringBuilder.append("00000");
        stringBuilder.append(StringUtils.leftPad(BLANK_STRING, 692, BLANK_STRING));
        stringBuilder.append(policyPcnAndRescInterface.getQueueWrtIndicator());
        stringBuilder.append(CHAR_N);
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getProgramName(), 8, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getParaName(), 30, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad("0", 10, "0"));
        stringBuilder.append(StringUtils.leftPad(policyPcnAndRescInterface.getParaName(), 320, BLANK_STRING));
        return stringBuilder.toString();
    }

    private String getModuleObjectId(String interfaceObjectName) {
        String moduleId = BLANK_STRING;
        HalBoMduXrf halBoMduXrf = halBoMduXrfRepository.findById(interfaceObjectName).orElse(null);
        if (halBoMduXrf != null) {
            moduleId = halBoMduXrf.getHbmxBobjMduNm().trim();
        }
        return moduleId;
    }

    private String[] editParms(PolicyPcnAndRescInterface policyPcnAndRescInterface) {
        String errorCode = "0150-EDIT-PARMS";

        if (policyPcnAndRescInterface.getAccountId().trim().isEmpty()
                || policyPcnAndRescInterface.getPolicyId().trim().isEmpty()
                || policyPcnAndRescInterface.getEffectiveDate() == null) {
            StringBuilder errorDesc = new StringBuilder();
            errorDesc.append("PARM LIST MISSING TERM DATA ");
            errorDesc.append(policyPcnAndRescInterface.getAccountId()).append(COMMA_STRING);
            errorDesc.append(policyPcnAndRescInterface.getPolicyId()).append(COMMA_STRING);
            errorDesc.append(policyPcnAndRescInterface.getEffectiveDate()).append(COMMA_STRING);
            return new String[] { errorCode, errorDesc.toString() };
        }

        if (!isValidActionCode(policyPcnAndRescInterface.getActionCode())) {
            StringBuilder errorDesc = new StringBuilder();
            errorDesc.append("PARM LIST - INVALID ACTION ");
            errorDesc.append(policyPcnAndRescInterface.getActionCode()).append(COMMA_STRING);
            errorDesc.append(policyPcnAndRescInterface.getAccountId()).append(COMMA_STRING);
            errorDesc.append(policyPcnAndRescInterface.getPolicyId()).append(COMMA_STRING);
            errorDesc.append(policyPcnAndRescInterface.getEffectiveDate()).append(COMMA_STRING);
            return new String[] { errorCode, errorDesc.toString() };
        }

        return new String[0];
    }

    private boolean isValidActionCode(char actionCode) {
        return Arrays
                .asList(RequestPolicyType.PCN_ACTION.type, RequestPolicyType.RESCIND_ACTION.type,
                        RequestPolicyType.REQ_CNC_ACTION.type, RequestPolicyType.AUTO_REI_ACTION.type,
                        RequestPolicyType.AUTO_RNT_ACTION.type)
                .stream().anyMatch(requestType -> requestType == actionCode);
    }

    public enum RequestPolicyType {
        PCN_ACTION('1'),
        RESCIND_ACTION('2'),
        REQ_CNC_ACTION('3'),
        AUTO_REI_ACTION('4'),
        AUTO_RNT_ACTION('5'),
        BLANK(BLANK_CHAR);

        private final Character type;

        private RequestPolicyType(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }

        public static RequestPolicyType getEnumValue(Character typeCode) {
            for (RequestPolicyType requestPolicyType : values()) {
                if (requestPolicyType.type.equals(typeCode)) {
                    return requestPolicyType;
                }
            }
            return BLANK;
        }
    }

}
