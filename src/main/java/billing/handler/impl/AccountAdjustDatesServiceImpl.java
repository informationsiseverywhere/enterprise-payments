package billing.handler.impl;

import static billing.utils.AdjustDueDatesConstants.RULE_DPIF;
import static billing.utils.AdjustDueDatesConstants.RULE_FFOT;
import static billing.utils.AdjustDueDatesConstants.RULE_SVC;
import static billing.utils.AdjustDueDatesConstants.SVCO_RULE_PREFIX;
import static billing.utils.BillingConstants.COMPANY_LOCATION_NUMBER;
import static billing.utils.BillingConstants.PLUS_SIGN;
import static billing.utils.BillingConstants.SYSTEM_USER_ID;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_E;
import static core.utils.CommonConstants.CHAR_F;
import static core.utils.CommonConstants.CHAR_I;
import static core.utils.CommonConstants.CHAR_L;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import application.handler.model.AccountChargesData;
import application.handler.service.ChargesUserExitService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilActInquiry;
import billing.data.entity.BilActRules;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilBillPlan;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilCreSchedule;
import billing.data.entity.BilCrgAmounts;
import billing.data.entity.BilDates;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilEftPlan;
import billing.data.entity.BilEftPlnCrg;
import billing.data.entity.BilIstSchedule;
import billing.data.entity.BilMstCoSt;
import billing.data.entity.BilObjectCfg;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSptPlnCrg;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.id.BilActInquiryId;
import billing.data.entity.id.BilActRulesId;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilCashDspId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.entity.id.BilCrgAmountsId;
import billing.data.entity.id.BilIstScheduleId;
import billing.data.entity.id.BilObjectCfgId;
import billing.data.entity.id.BilSupportPlanId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActRulesRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilAmountsRepository;
import billing.data.repository.BilAmtSchRltRepository;
import billing.data.repository.BilBillPlanRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilCreScheduleRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilDatesRepository;
import billing.data.repository.BilEftBankRepository;
import billing.data.repository.BilEftPlanRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilMstCoStRepository;
import billing.data.repository.BilObjectCfgRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilSeasExclRepository;
import billing.data.repository.BilSupportPlanRepository;
import billing.handler.model.AccountAdjustDates;
import billing.handler.model.AccountAdjustSupportData;
import billing.handler.model.AdjustDatesReviewDetails;
import billing.handler.model.ChargeVariables;
import billing.handler.model.DownPaymentFee;
import billing.handler.model.FirstPossibleDueDate;
import billing.handler.model.VariableSeviceCharge;
import billing.handler.model.VariableSeviceChargeOverride;
import billing.handler.service.AccountAdjustDatesService;
import billing.handler.service.FirstPossibleDueDateService;
import billing.service.BilRulesUctService;
import billing.service.CashApplyDriverService;
import billing.utils.AdjustDueDatesConstants;
import billing.utils.AdjustDueDatesConstants.BillingExclusionIndicator;
import billing.utils.AdjustDueDatesConstants.BillingTypeCode;
import billing.utils.AdjustDueDatesConstants.DateIndicator;
import billing.utils.AdjustDueDatesConstants.DateTypeCode;
import billing.utils.AdjustDueDatesConstants.ScheduleCode;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.AccountStatus;
import billing.utils.BillingConstants.AccountTypeCode;
import billing.utils.BillingConstants.ActivityType;
import billing.utils.BillingConstants.BilAmountsEftIndicator;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BilReceiptTypeCode;
import billing.utils.BillingConstants.ChargeType;
import billing.utils.BillingConstants.ChargeTypeCodes;
import billing.utils.BillingConstants.FullPayServiceChargeWriteOff;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.FunctionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class AccountAdjustDatesServiceImpl implements AccountAdjustDatesService {
    public static final Logger logger = LogManager.getLogger(AccountAdjustDatesServiceImpl.class);

    //@Autowired
    //private ExecContext execContext;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilSupportPlanRepository bilSupportPlanRepository;

    @Autowired
    private BilDatesRepository bilDatesRepository;

    @Autowired
    private BilAmountsRepository bilAmountsRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilCreScheduleRepository bilCreScheduleRepository;

    @Autowired
    private BilAmtSchRltRepository bilAmtSchRltRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilActRulesRepository bilActRulesRepository;

    @Autowired
    private BilSeasExclRepository bilSeasExclRepository;

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilEftPlanRepository bilEftPlanRepository;

    @Autowired
    private FirstPossibleDueDateService firstPossibleDueDateService;

    @Autowired
    private CashApplyDriverService cashApplyDriverService;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilMstCoStRepository bilMstCoStRepository;

    @Autowired
    private BilBillPlanRepository bilBillPlanRepository;

    @Autowired
    private BilObjectCfgRepository bilObjectCfgRepository;

    @Autowired
    private ChargesUserExitService chargesUserExitService;
    
    @Autowired
    private DateService dateService;
    
    @Value("${language}")
    private String language;

    private static final String BCMOCRG1 = "BCMOCRG1";

    @Override
    @Transactional
    public AdjustDatesReviewDetails processAdjustDates(AccountAdjustDates accountAdjustDates) {

        boolean isErcvRule = false;
        int ercvDays = SHORT_ZERO;
        ZonedDateTime earliestEftDueDt = DateRoutine.dateTimeAsYYYYMMDD(0);
        boolean isErcvIssDtInvoice = false;
        boolean isAccountBilled = false;
        boolean isLateEntered = false;
        int dicvDays = SHORT_ZERO;
        boolean isDicvRule = false;
        boolean isFpddOnTimeProcessing = false;
        char wsceRule = BLANK_CHAR;
        boolean isWsceBamFound = false;
        boolean isSpcrRule = false;
        boolean isDpifRule = false;
        double dpifAmount = DECIMAL_ZERO;

        BilAccount bilAccount = bilAccountRepository.findById(accountAdjustDates.getAccountId()).orElse(null);
        if (bilAccount == null) {
            throw new DataNotFoundException("no.data.found");
        }

        BilActRules bilActRules = getBillAccountRules(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd());
        if (bilActRules == null) {
            bilActRules = defaultBilActRules();
        }

        char fpddIndicator = bilActRules.getBruFpddInd();
        boolean isSeasonalBill = checkForSeasonalBill(bilAccount.getSeasionalExclusionCode());
        if (isSeasonalBill) {
            fpddIndicator = CHAR_Y;
        }

        int referenceDuration = getReferenceDuration(bilAccount.getStartReferenceDate(), bilAccount.getStartDueDate());

        boolean isManualReinvoice = readManualReinvoice(accountAdjustDates);

        if (isManualReinvoice) {
            fpddIndicator = CHAR_Y;
            ZonedDateTime minimumMriDueDt = processManualReinvoice(accountAdjustDates);
            if (minimumMriDueDt != null
                    && minimumMriDueDt.compareTo(accountAdjustDates.getProcessDate()) <= SHORT_ZERO) {
                isManualReinvoice = cancelManualReinvoice(accountAdjustDates, DateRoutine.dateTimeAsYYYYMMDD(0));
            }
        }

        BilSupportPlan bilSupportPlan = getBillSupportPlan(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd());

        boolean eftCollectionMethod = getBilCollectionMethod(accountAdjustDates, bilAccount);

        short dueDateDisplacementNumber = bilSupportPlan.getBilDueDtDplNbr();

        if (eftCollectionMethod) {
            BilEftPlan bilEftPlan = bilEftPlanRepository.findById(bilAccount.getCollectionPlan()).orElse(null);
            if (bilEftPlan == null) {
                throw new DataNotFoundException("no.data.found");
            }

            dueDateDisplacementNumber = bilEftPlan.getBepInvDplNbr();

            BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("ERCV", bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd(), BLANK_STRING);
            if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                isErcvRule = true;
                ercvDays = Integer.parseInt(bilRulesUct.getBrtParmListTxt().trim().substring(SHORT_ZERO,
                        bilRulesUct.getBrtParmListTxt().trim().length() - 2));
                isErcvIssDtInvoice = bilRulesUct.getBrtParmListTxt().trim()
                        .charAt(bilRulesUct.getBrtParmListTxt().trim().length() - 1) == CHAR_Y;
                if (!isManualReinvoice) {
                    boolean excludeWeekend = getOverrideWeekendIndicator(true, bilActRules.getBruIncWknInd());
                    earliestEftDueDt = DateRoutine.adjustDateWithOffset(accountAdjustDates.getProcessDate(),
                            excludeWeekend, 0, '+', null);
                    if (dueDateDisplacementNumber > ercvDays) {
                        earliestEftDueDt = DateRoutine.adjustDateWithOffset(earliestEftDueDt, false, ercvDays, '+',
                                null);
                    } else {
                        earliestEftDueDt = DateRoutine.adjustDateWithOffset(earliestEftDueDt, false,
                                dueDateDisplacementNumber, '+', null);
                    }
                }

                if (accountAdjustDates.getPolicyEffectiveDate() != null && (accountAdjustDates.getPolicyEffectiveDate()
                        .compareTo(accountAdjustDates.getProcessDate()) <= SHORT_ZERO
                        || accountAdjustDates.getPolicyEffectiveDate()
                                .compareTo(accountAdjustDates.getProcessDate()) > SHORT_ZERO
                                && accountAdjustDates.getPolicyEffectiveDate()
                                        .compareTo(earliestEftDueDt) <= SHORT_ZERO)) {
                    ZonedDateTime referenceDate = bilDatesRepository.getMaxReferenceDateByInvoiceCode(
                            accountAdjustDates.getAccountId(), AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
                    if (referenceDate != null) {
                        isAccountBilled = true;
                    } else {
                        if (bilAccount.getStartDueDate().compareTo(accountAdjustDates.getProcessDate()) <= 0
                                || bilAccount.getStartDueDate().compareTo(accountAdjustDates.getProcessDate()) > 0
                                        && bilAccount.getStartDueDate().compareTo(earliestEftDueDt) <= 0) {
                            isLateEntered = true;
                        }

                    }
                }
            }
        } else if (fpddIndicator == CHAR_Y) {
            BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("DICV", bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd(), BLANK_STRING);
            if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {

                isDicvRule = true;
                dicvDays = Integer.parseInt(bilRulesUct.getBrtParmListTxt().trim().substring(SHORT_ZERO,
                        bilRulesUct.getBrtParmListTxt().trim().length() - 1));
                isErcvIssDtInvoice = bilRulesUct.getBrtParmListTxt().trim()
                        .charAt(bilRulesUct.getBrtParmListTxt().trim().length() - 1) == CHAR_Y;
                if (!isManualReinvoice) {
                    boolean excludeWeekend = getOverrideWeekendIndicator(true, bilActRules.getBruIncWknInd());
                    earliestEftDueDt = DateRoutine.adjustDateWithOffset(accountAdjustDates.getProcessDate(),
                            excludeWeekend, 0, '+', null);
                    if (dueDateDisplacementNumber > dicvDays) {
                        earliestEftDueDt = DateRoutine.adjustDateWithOffset(earliestEftDueDt, false, dicvDays, '+',
                                null);
                    } else {
                        earliestEftDueDt = DateRoutine.adjustDateWithOffset(earliestEftDueDt, false,
                                dueDateDisplacementNumber, '+', null);
                    }
                }
            }
        }

        if (bilAccount.getStartReferenceDate().compareTo(bilAccount.getStartDueDate()) <= 0 || fpddIndicator == CHAR_Y
                || eftCollectionMethod && !bilAccount.getCollectionPlan().trim().isEmpty()) {
            isFpddOnTimeProcessing = false;
        } else {
            BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct(RULE_FFOT, bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd(), BLANK_STRING);
            if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                isFpddOnTimeProcessing = true;
            }
        }

        if (bilAccount.getBillTypeCd().trim().equals("SP")) {
            BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("SPCR", bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd(), BLANK_STRING);
            if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                isSpcrRule = true;
            }
        }

        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("WSCE", bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), BLANK_STRING);
        if (bilRulesUct != null && (bilRulesUct.getBrtRuleCd() == '1' || bilRulesUct.getBrtRuleCd() == '2'
                || bilRulesUct.getBrtRuleCd() == '3')) {
            wsceRule = bilRulesUct.getBrtRuleCd();
            Integer count = bilAmountsRepository.findCountByEffectiveIndicatorOrReasonType(bilAccount.getAccountId(),
                    AdjustDueDatesConstants.getAmountEffectiveIndicator(),
                    AdjustDueDatesConstants.getDescriptionReasons());
            if (count != null && count > SHORT_ZERO) {
                isWsceBamFound = true;
            }

        }

        BilRulesUct dpifRuleUct = bilRulesUctService.readBilRulesUct(RULE_DPIF, bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), BLANK_STRING);
        if (dpifRuleUct != null && dpifRuleUct.getBrtRuleCd() == CHAR_Y) {
            isDpifRule = true;
            dpifAmount = Double.valueOf(dpifRuleUct.getBrtParmListTxt()) / 100;

        }

        AdjustDatesReviewDetails adjustDatesReviewDetails = new AdjustDatesReviewDetails();

        adjustDatesReviewDetails.setBilAccount(bilAccount);
        adjustDatesReviewDetails.setEftCollectionMethod(eftCollectionMethod);
        adjustDatesReviewDetails.setFpddIndicator(fpddIndicator);
        adjustDatesReviewDetails.setBilActRules(bilActRules);
        adjustDatesReviewDetails.setReferenceDuration(referenceDuration);
        adjustDatesReviewDetails.setBilSupportPlan(bilSupportPlan);
        adjustDatesReviewDetails.setDueDateDisplacementNumber(dueDateDisplacementNumber);
        adjustDatesReviewDetails.setSeasonalBill(isSeasonalBill);
        adjustDatesReviewDetails.setManualReinvoice(isManualReinvoice);
        if (isSeasonalBill) {
            adjustDatesReviewDetails.setOutOfCycle(CHAR_Y);
            adjustDatesReviewDetails.setErcvIssDtInvoice(false);
        } else {
            adjustDatesReviewDetails.setOutOfCycle(bilSupportPlan.getBspOcbInd());
            adjustDatesReviewDetails.setErcvIssDtInvoice(isErcvIssDtInvoice);
        }
        adjustDatesReviewDetails.setErcvRule(isErcvRule);
        adjustDatesReviewDetails.setErcvDays(ercvDays);
        adjustDatesReviewDetails.setEarliestEftDueDt(earliestEftDueDt);
        adjustDatesReviewDetails.setAccountBilled(isAccountBilled);
        adjustDatesReviewDetails.setLateEntered(isLateEntered);
        adjustDatesReviewDetails.setDicvRule(isDicvRule);
        adjustDatesReviewDetails.setDicvDays(dicvDays);
        adjustDatesReviewDetails.setFpddOnTimeProcessing(isFpddOnTimeProcessing);
        adjustDatesReviewDetails.setWsceRule(wsceRule);
        adjustDatesReviewDetails.setWsceBamFound(isWsceBamFound);
        adjustDatesReviewDetails.setSpcrRule(isSpcrRule);
        adjustDatesReviewDetails.setHoldNewDate(DateRoutine.dateTimeAsYYYYMMDD(0));
        adjustDatesReviewDetails.setDpifRule(isDpifRule);
        adjustDatesReviewDetails.setDpifAmount(dpifAmount);
        return adjustDatesReviewDetails;

    }

    @Override
    public VariableSeviceChargeOverride readOverrideServiceCharge(BilAccount bilAccount) {

        VariableSeviceChargeOverride variableSeviceChargeOverride = new VariableSeviceChargeOverride();
        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("SVC", bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), BLANK_STRING);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            List<BilPolicyTerm> bilPolicyTermsList = bilPolicyTermRepository.getPolicyRowsByAccountId(
                    bilAccount.getAccountId(),
                    Arrays.asList(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                            BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                            BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(),
                            BilPolicyStatusCode.TRANSFERRED.getValue()));
            if (bilPolicyTermsList != null && !bilPolicyTermsList.isEmpty()) {
                variableSeviceChargeOverride.setStateCode(bilPolicyTermsList.get(SHORT_ZERO).getBillStatePvnCd());
            } else {
                List<BilPolicyTerm> bilPolicyTerms = bilPolicyTermRepository.getPolicyRowsByAccountId(
                        bilAccount.getAccountId(), Arrays.asList(BilPolicyStatusCode.TRANSFERRED.getValue()));
                if (bilPolicyTerms != null && !bilPolicyTerms.isEmpty()) {
                    variableSeviceChargeOverride.setStateCode(bilPolicyTerms.get(SHORT_ZERO).getBillStatePvnCd());
                } else {
                    return null;
                }
            }

            variableSeviceChargeOverride.setRuleState(variableSeviceChargeOverride.getStateCode());
            BilRulesUct bilRulesUctSV = bilRulesUctService.readBilRulesUct(
                    "SV" + variableSeviceChargeOverride.getStateCode().trim(), bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd(), BLANK_STRING);
            if (bilRulesUctSV != null && bilRulesUctSV.getBrtRuleCd() == CHAR_Y) {
                variableSeviceChargeOverride.setRuleInd(CHAR_Y);
                variableSeviceChargeOverride.setProgramName(bilRulesUctSV.getBrtParmListTxt().trim());
            }
        }

        return variableSeviceChargeOverride;
    }

    private BilActRules getBillAccountRules(String bilTypeCode, String bilClassCode) {
        logger.debug("get billing account rules for bilTypeCode:{} and bilClassCode:{}", bilTypeCode, bilClassCode);
        BilActRules bilActRules = null;

        bilActRules = bilActRulesRepository.findById(new BilActRulesId(bilTypeCode, bilClassCode)).orElse(null);
        if (bilActRules == null) {
            bilActRules = bilActRulesRepository.findById(new BilActRulesId(bilTypeCode, BLANK_STRING)).orElse(null);
            if (bilActRules == null) {
                return bilActRules;
            }
        }
        if (bilActRules.getBruFlpScgWroCd() != FullPayServiceChargeWriteOff.ACTUAL_INVOICE.getValue()
                && bilActRules.getBruFlpScgWroCd() != FullPayServiceChargeWriteOff.SCHEDULE_INVOICE.getValue()
                && bilActRules.getBruFlpScgWroCd() != FullPayServiceChargeWriteOff.USER_EXIT.getValue()
                && bilActRules.getBruFlpScgWroCd() != FullPayServiceChargeWriteOff.DISABLED.getValue()) {
            bilActRules.setBruFlpScgWroCd(CHAR_A);
        }

        return bilActRules;
    }

    private BilSupportPlan getBillSupportPlan(String bilTypeCode, String bilClassCode) {
        logger.debug("get bill support plan for bilTypeCode:{} and bilClassCode:{}", bilTypeCode, bilClassCode);
        BilSupportPlan bilSupportPlan = bilSupportPlanRepository
                .findById(new BilSupportPlanId(bilTypeCode, bilClassCode)).orElse(null);
        if (bilSupportPlan == null) {
            throw new DataNotFoundException("no.supportPlan.found", new Object[] { bilTypeCode, bilClassCode });
        }
        return bilSupportPlan;
    }

    @Override
    @Transactional
    public void checkForExcessCredit(String accountId, ZonedDateTime referenceDate) {

        logger.debug("check for excess credit and update dates, invoice code for BIL_IST_SCHEDULE/BIL_CRE_SCHEDULE");
        List<BilIstSchedule> excessCreditList = bilIstScheduleRepository.findExcessCreditData(accountId, referenceDate,
                DECIMAL_ZERO, AdjustDueDatesConstants.getFutureInvoiceCodes());

        if (excessCreditList != null && !excessCreditList.isEmpty()) {
            BilDates bilDates = bilDatesRepository.findByReferenceDate(accountId, referenceDate,
                    BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
            if (bilDates != null) {
                ZonedDateTime systemDueDate = bilDates.getBilSysDueDt();
                ZonedDateTime adjustDueDate = bilDates.getBilAdjDueDt();
                ZonedDateTime invoiceDate = bilDates.getBilInvDt();
                for (BilIstSchedule excessCreditData : excessCreditList) {
                    excessCreditData.setBillReferenceDt(referenceDate);
                    excessCreditData.setBillSysDueDt(systemDueDate);
                    excessCreditData.setBillAdjDueDt(adjustDueDate);
                    excessCreditData.setBillInvDt(invoiceDate);
                    excessCreditData.setBillInvoiceCd(bilDates.getBilInvoiceCd());
                    bilIstScheduleRepository.save(excessCreditData);
                    updateExcessCreditSchedule(accountId, excessCreditData.getBillIstScheduleId().getPolicyId(),
                            excessCreditData.getBillIstScheduleId().getBillSeqNbr(), systemDueDate, adjustDueDate,
                            invoiceDate);
                }

            }
            bilDatesRepository.flush();
        }

    }

    private void updateExcessCreditSchedule(String accountId, String policyId, short sequenceNumber,
            ZonedDateTime systemDueDate, ZonedDateTime adjustDueDate, ZonedDateTime invoiceDate) {

        int index = SHORT_ZERO;
        List<BilCreSchedule> bilCreditScheduleList = bilCreScheduleRepository.findByAccountIdPolicyId(accountId,
                policyId, sequenceNumber);
        List<BilCreSchedule> bilCreditScheduleClonedList = new CopyOnWriteArrayList<>(bilCreditScheduleList);
        if (bilCreditScheduleList != null && !bilCreditScheduleList.isEmpty()) {
            for (BilCreSchedule excessCreditData : bilCreditScheduleClonedList) {
                excessCreditData.setBilSysDueDt(systemDueDate);
                excessCreditData.setBilAdjDueDt(adjustDueDate);
                excessCreditData.setBilInvDt(invoiceDate);
                bilCreditScheduleClonedList.set(index, excessCreditData);
                index = index + SHORT_ONE;
            }
            bilCreScheduleRepository.saveAll(bilCreditScheduleClonedList);
        }

    }

    private void deleteBilDates(String accountId) {
        logger.debug("clean up started for BIL_DATES");

        List<BilDates> bilDatesList = bilDatesRepository.findRowsForCleanUp(accountId,
                AdjustDueDatesConstants.getFutureInvoiceCodes(), AdjustDueDatesConstants.getDatetypeCodes(),
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            int cancelledPolicyCount = getCancelledPolicyCount(accountId);
            for (BilDates bilDates : bilDatesList) {
                char dateType = bilDates.getBdtDateType();
                ZonedDateTime referenceDate = bilDates.getBilDatesId().getBilReferenceDt();
                ZonedDateTime invoiceDate = bilDates.getBilInvDt();
                ZonedDateTime adjustDueDate = bilDates.getBilAdjDueDt();
                boolean isScheduleRowsExist = true;
                if (dateType == DateTypeCode.FIRSTPOSSIBLEDUEDATE_SINGLEPOLICYCANCELLATIONINVOICE.getValue()
                        && cancelledPolicyCount == SHORT_ZERO
                        || dateType != DateTypeCode.FIRSTPOSSIBLEDUEDATE_SINGLEPOLICYCANCELLATIONINVOICE.getValue()) {

                    List<BilIstSchedule> bilIstScheduleList = bilIstScheduleRepository
                            .findByAccountIdAndReferenceDate(accountId, referenceDate);
                    if (bilIstScheduleList == null || bilIstScheduleList.isEmpty()) {
                        isScheduleRowsExist = false;
						Integer count = bilCrgAmountsRepository.findCountByAccountIdAndDates(accountId, adjustDueDate,
								invoiceDate);
                        if (count == null || count == SHORT_ZERO) {
                            logger.debug("delete BIL_DATES record for accountId:{} and referenceDate:{}", accountId,
                                    referenceDate);
                            bilDatesRepository.delete(bilDates);
                            continue;
                        }
                    }

                    if (dateType == DateTypeCode.FIRST_POSSIBLEDUE_LATE.getValue()) {

                        logger.debug("get minimum reference date for scheduled rows");
                        ZonedDateTime minimumReferenceDate = getMinimumReferenceDate(accountId, referenceDate,
                                bilDates.getBilSysDueDt(), adjustDueDate, invoiceDate);
                        if (minimumReferenceDate != null) {
                            if (isScheduleRowsExist) {
                                updateInstallmentScheduleReferenceDate(bilIstScheduleList, minimumReferenceDate);
                            }

                            logger.debug("delete BIL_DATES record for accountId:{} and referenceDate:{}", accountId,
                                    referenceDate);
                            bilDatesRepository.delete(bilDates);
                        }

                    }

                }

            }
            bilDatesRepository.flush();
        }

        logger.debug("clean up ended for BIL_DATES");

    }

    private void cleanupSeasonalBillRows(BilAccount bilAccount) {

        List<BilDates> bilDates = bilDatesRepository.getMinAdjustDueDateByInvoiceCode(bilAccount.getAccountId(),
                AdjustDueDatesConstants.getInvoiceCodes(), CHAR_X);
        if (bilDates == null || bilDates.isEmpty()) {
            return;
        }
        ZonedDateTime adjustDate = bilDates.get(SHORT_ZERO).getBilAdjDueDt();
        char invoiceCode = bilDates.get(SHORT_ZERO).getBilInvoiceCd();

        List<BilDates> bilDatesList = bilDatesRepository.getCleanupSeasonalData(bilAccount.getAccountId(), adjustDate,
                invoiceCode, Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                        InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()),
                CHAR_X);

        if (bilDatesList != null && !bilDatesList.isEmpty()) {

            for (BilDates bilDate : bilDatesList) {
                List<BilIstSchedule> bilIstScheduleList = bilIstScheduleRepository.findByAccountIdAndReferenceDate(
                        bilDate.getBilDatesId().getBilAccountId(), bilDate.getBilDatesId().getBilReferenceDt(),
                        bilDate.getBilSysDueDt(), bilDate.getBilAdjDueDt(), bilDate.getBilInvDt());
                if (bilIstScheduleList == null || bilIstScheduleList.isEmpty()) {

                    Integer count = bilCrgAmountsRepository.findCountByAccountIdAndDates(
                            bilDate.getBilDatesId().getBilAccountId(), bilDate.getBilAdjDueDt(), bilDate.getBilInvDt());
                    if (count == null || count == SHORT_ZERO) {
                        bilDate.setBilInvoiceCd(invoiceCode);
                        bilDatesRepository.saveAndFlush(bilDate);
                    }
                }
            }

        }

    }

    private ZonedDateTime getMinimumReferenceDate(String accountId, ZonedDateTime referenceDate,
            ZonedDateTime systemDueDate, ZonedDateTime adjustDueDate, ZonedDateTime invoiceDate) {
        return bilDatesRepository.getMinReferenceDateForScheduledRows(accountId, referenceDate, systemDueDate,
                adjustDueDate, invoiceDate, DateTypeCode.SCHEDULED.getValue(),
                AdjustDueDatesConstants.getFutureInvoiceCodes(),
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

    }

    private void updateInstallmentScheduleReferenceDate(List<BilIstSchedule> bilIstScheduleList,
            ZonedDateTime minimumReferenceDate) {
        int index = SHORT_ZERO;
        List<BilIstSchedule> bilIstScheduleClonedList = new CopyOnWriteArrayList<>(bilIstScheduleList);
        logger.debug("update referenceDate for BIL_IST_SCHEDULE");
        for (BilIstSchedule bilIstSchedule : bilIstScheduleClonedList) {
            bilIstSchedule.setBillReferenceDt(minimumReferenceDate);
            index = index + SHORT_ONE;
            bilIstScheduleClonedList.set(index, bilIstSchedule);
        }
        bilIstScheduleRepository.saveAll(bilIstScheduleClonedList);

    }

    private int getCancelledPolicyCount(String accountId) {
        Integer cancelledPolicyCount = bilPolicyTermRepository.checkCancelledPolicyExistance(accountId,
                AdjustDueDatesConstants.getPolicyStatusCodesCancellation());
        if (cancelledPolicyCount == null) {
            return SHORT_ZERO;
        }
        return cancelledPolicyCount;
    }

    private Integer getChargeMaxSequenceNumber(String accountId) {
        Integer sequenceNumber = bilCrgAmountsRepository.getMaxSeqNumberByAccountId(accountId);
        if (sequenceNumber != null) {
            return sequenceNumber;
        }
        return null;
    }

    private short getIncrementalValue(Integer sequenceNumber) {
        if (sequenceNumber == null) {
            return SHORT_ZERO;
        } else {
            return (short) (sequenceNumber + SHORT_ONE);
        }
    }

    private void deleteServiceCharges(String accountId) {
        logger.debug("delete future service charges");
        bilCrgAmountsRepository.deleteServiceCharges(accountId, AdjustDueDatesConstants.getFutureInvoiceCodes(),
                ChargeType.SERVICECHARGES.getChargeType());
        bilCrgAmountsRepository.flush();
    }

    @Override
    public List<BilCrgAmounts> getFutureDownpaymentRows(String accountId, DownPaymentFee downPaymentFee) {
        logger.debug("get future downpayment charges prior delete");
        List<BilCrgAmounts> bilCrgAmountsList = new ArrayList<>();
        BigDecimal totalPaidAmount = BigDecimal.valueOf(DECIMAL_ZERO);
        BigDecimal totalChargeAmount = BigDecimal.valueOf(DECIMAL_ZERO);
        BigDecimal totalWriteOffAmount = BigDecimal.valueOf(DECIMAL_ZERO);
        ZonedDateTime invoiceDate = null;
        ZonedDateTime dueDate = null;

        List<BilCrgAmounts> bilCrgAmountList = bilCrgAmountsRepository.findDownPaymentCharges(accountId,
                AdjustDueDatesConstants.getFutureInvoiceCodes(), ChargeType.DOWNPAYMENTCHARGES.getChargeType());
        if (bilCrgAmountList != null && !bilCrgAmountList.isEmpty()) {

            for (BilCrgAmounts bilCrgAmounts : bilCrgAmountList) {
                if (bilCrgAmounts.getBcaCrgPaidAmt() != DECIMAL_ZERO
                        || bilCrgAmounts.getBcaWroCrgAmt() != DECIMAL_ZERO) {
                    bilCrgAmountsList.add(bilCrgAmounts);
                    totalPaidAmount = BigDecimal.valueOf(bilCrgAmounts.getBcaCrgPaidAmt()).add(totalPaidAmount);
                    totalWriteOffAmount = BigDecimal.valueOf(bilCrgAmounts.getBcaWroCrgAmt()).add(totalWriteOffAmount);
                    totalChargeAmount = BigDecimal.valueOf(bilCrgAmounts.getBcaCrgAmt()).negate()
                            .add(totalChargeAmount);
                } else {
                    totalChargeAmount = BigDecimal.valueOf(bilCrgAmounts.getBcaCrgAmt()).negate()
                            .add(totalChargeAmount);
                }
            }

            downPaymentFee.setDownpaymentDueDate(dueDate);
            downPaymentFee.setDownpaymentInvoiceDate(invoiceDate);
            downPaymentFee.setTotalDownpaymentChargeAmount(totalChargeAmount);
            downPaymentFee.setTotalDownpaymentPaidAmount(totalPaidAmount);
            downPaymentFee.setTotalDownpaymentWriteOffAmount(totalWriteOffAmount);

        }

        if (bilCrgAmountList != null && !bilCrgAmountList.isEmpty()) {
            deleteDownpaymentCharges(bilCrgAmountList);
        }
        return bilCrgAmountsList;
    }

    private void deleteDownpaymentCharges(List<BilCrgAmounts> downPaymentChargeList) {
        logger.debug("delete future downpayment charges");
        bilCrgAmountsRepository.deleteAllInBatch(downPaymentChargeList);
        bilCrgAmountsRepository.flush();
    }

    @Override
    public void processDownpaymentFees(String accountId, boolean isQuote, double downPaymentFeeAmount,
            DownPaymentFee downpaymentFeeData, VariableSeviceChargeOverride variableSeviceChargeOverride,
            VariableSeviceCharge variableSeviceCharge, AccountAdjustSupportData accountAdjustSupportData,
            String userSequenceId) {

        logger.debug("get adjusted BIL_DATES rows to process downpayment fee");

        List<ZonedDateTime> bilDatesList = bilDatesRepository.fetchDistinctAdjustDate(accountId,
                AdjustDueDatesConstants.getFutureInvoiceCodes(),
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            for (ZonedDateTime adjustDueDate : bilDatesList) {
                reviewDueDate(accountId, isQuote, adjustDueDate, downPaymentFeeAmount, downpaymentFeeData,
                        variableSeviceChargeOverride, variableSeviceCharge, accountAdjustSupportData,
                        userSequenceId);
            }
        }

    }

    private void reviewDueDate(String accountId, boolean isQuote, ZonedDateTime adjustDueDate,
            Double downPaymentFeeAmount, DownPaymentFee downpaymentFeeData,
            VariableSeviceChargeOverride variableSeviceChargeOverride, VariableSeviceCharge variableSeviceCharge,
            AccountAdjustSupportData accountAdjustSupportData, String userSequenceId) {

        logger.debug("check for future due dates rows. If the account doesn't have any more future due dates,"
                + "the dates provided doesn't qualify for downpayment fee");

        boolean isQualify = false;
        int numberOfQualifiedPolicies = SHORT_ZERO;
        String policyId = BLANK_STRING;

        ZonedDateTime minimumInvoiceDate = bilDatesRepository.getMinInvDateByAdjDueDt(accountId, adjustDueDate,
                AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);

        List<BilIstSchedule> bilIstScheduleList = bilIstScheduleRepository.findByAdjustDueDateInvoiceDate(accountId,
                adjustDueDate, minimumInvoiceDate, AdjustDueDatesConstants.getFutureInvoiceCodes());
        if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
            isQualify = true;
            String previousPolicyId = BLANK_STRING;
            ZonedDateTime previousEffectiveDate = null;
            ZonedDateTime policyEffectiveDate = null;

            logger.debug("Iteration started over BilIstSchedule for processing downpayment fees");
            for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                if (isQualify) {
                    policyId = bilIstSchedule.getBillIstScheduleId().getPolicyId();
                    policyEffectiveDate = bilIstSchedule.getPolicyEffectiveDt();
                    short sequenceNumber = bilIstSchedule.getBillIstScheduleId().getBillSeqNbr();

                    if (policyId.equals(previousPolicyId)) {
                        if (!policyEffectiveDate.equals(previousEffectiveDate)) {
                            isQualify = false;
                        }
                    } else {
                        logger.debug("read BIL_POLICY_TERM and check issued status code for the term");
                        BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository.findSuspendActivityData(accountId,
                                policyId, policyEffectiveDate, AdjustDueDatesConstants.getPolicyStatusCodesDpifrule());
                        if (bilPolicyTerm != null && bilPolicyTerm
                                .getBptPolUwStaCd() != BilPolicyStatusCode.FLAT_CANCELLATION.getValue()) {

                            if (bilPolicyTerm.getBptAgreementInd() == CHAR_Y && !isQuote) {
                                isQualify = false;
                                break;
                            }
                            logger.debug("check the due date for non-zero rows for the term");
                            Integer count = bilIstScheduleRepository.findNonZeroAmountRowCount(accountId, policyId,
                                    policyEffectiveDate, adjustDueDate, DECIMAL_ZERO);

                            if (count != null && count != SHORT_ZERO) {
                                logger.debug("check if the installment schedule row is for an agreement");

                                isQualify = reviewEarlierAndFutureDatesAndBilAmounts(accountId, policyId,
                                        policyEffectiveDate, adjustDueDate, sequenceNumber, bilPolicyTerm.getPlnExpDt(),
                                        isQuote);

                                if (isQualify) {
                                    previousPolicyId = policyId;
                                    previousEffectiveDate = policyEffectiveDate;
                                    numberOfQualifiedPolicies = numberOfQualifiedPolicies + SHORT_ONE;
                                } else {
                                    break;
                                }
                            }
                        }
                    }

                } else {
                    break;
                }

            }
        }

        if (isQualify) {
            insertUpdateDownpaymentFee(accountId, adjustDueDate, minimumInvoiceDate, numberOfQualifiedPolicies,
                    downPaymentFeeAmount, policyId, downpaymentFeeData, variableSeviceChargeOverride,
                    variableSeviceCharge, accountAdjustSupportData, userSequenceId);
        }

    }

    private void insertUpdateDownpaymentFee(String accountId, ZonedDateTime adjustDueDate, ZonedDateTime invoiceDate,
            int numberOfQualifiedPolicies, Double downPaymentFeeAmount, String policyId,
            DownPaymentFee downpaymentFeeData, VariableSeviceChargeOverride variableSeviceChargeOverride,
            VariableSeviceCharge variableSeviceCharge, AccountAdjustSupportData accountAdjustSupportData,
            String userSequenceId) {

        char invoiceCode = bilDatesRepository.getMaxInvoiceCodeByDueDateInvoiceDate(accountId, adjustDueDate,
                invoiceDate, BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

        switch (InvoiceTypeCode.getEnumValue(invoiceCode)) {
        case SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE:

            BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
            if (bilAccount == null) {
                throw new InvalidDataException("no.account.exist");
            }
            Double futureBalance = checkFutureBalance(accountId, adjustDueDate, numberOfQualifiedPolicies, policyId);
            if (futureBalance != null && futureBalance != DECIMAL_ZERO) {
                if (downPaymentFeeAmount == DECIMAL_ZERO) {
                    if (variableSeviceChargeOverride.getRuleInd() == CHAR_Y) {
                        firstPossibleDueDateService.getServiceChargeTermAmount(variableSeviceChargeOverride,
                                adjustDueDate, accountId, variableSeviceCharge, accountAdjustSupportData);
                    } else if (bilAccount.getBillTypeCd().equals(BilDspTypeCode.SUSPENDED.getValue())
                            && accountAdjustSupportData.getServiceChargeIndicator() == CHAR_Y
                            && accountAdjustSupportData.getServiceChargeTypeCode().equalsIgnoreCase("GA")) {
                        firstPossibleDueDateService.getServiceChargePerTerm(" ", variableSeviceCharge, adjustDueDate,
                                accountAdjustSupportData, accountId);
                    }
                    if (accountAdjustSupportData.getServiceChargeAmount() != DECIMAL_ZERO) {
                        BilObjectCfg bilObjectCfg = bilObjectCfgRepository.findById(new BilObjectCfgId(BCMOCRG1, 'U'))
                                .orElse(null);
                        if (bilObjectCfg != null && bilObjectCfg.getBocObjPrcCd() == 'Y'
                                && variableSeviceChargeOverride.getRuleInd() != CHAR_Y) {
                            AccountChargesData accountChargesData = mapAccountChargesData(accountId, adjustDueDate,
                                    invoiceDate, accountAdjustSupportData, bilAccount.getBillTypeCd(),
                                    bilAccount.getBillClassCd(), userSequenceId);
                            chargesUserExitService.chargeProcess(accountChargesData);
                            accountAdjustSupportData
                                    .setAccountPlanChargeAmount(accountChargesData.getServiceChargeAmount());
                            accountAdjustSupportData.setServiceChargeCode(accountChargesData.getServiceChargeCode());
                        }
                        insertDownPaymentFee(accountId, accountAdjustSupportData.getServiceChargeAmount(),
                                downpaymentFeeData, invoiceCode, adjustDueDate, invoiceDate);
                    }
                } else {
                    insertDownPaymentFee(accountId, downPaymentFeeAmount, downpaymentFeeData, invoiceCode,
                            adjustDueDate, invoiceDate);
                }
            }
            break;
        case EMPTY_CHAR:
            List<BilCrgAmounts> serviceChargeList = bilCrgAmountsRepository.findByDueDateInvoiceDateChargeType(
                    accountId, adjustDueDate, invoiceDate, Arrays.asList(ChargeType.SERVICECHARGES.getChargeType()));
            if (serviceChargeList != null && !serviceChargeList.isEmpty()) {
                updateServiceChargeToDownpayment(downPaymentFeeAmount, serviceChargeList, downpaymentFeeData);
            } else {
                insertServiceChargeAmountOrDownpaymentAmount(accountId,
                        accountAdjustSupportData.getMaxServiceChargeAmount(), downpaymentFeeData, invoiceCode,
                        downPaymentFeeAmount, adjustDueDate, invoiceDate);
            }
            break;

        default:
            break;

        }

    }

    private AccountChargesData mapAccountChargesData(String accountId, ZonedDateTime adjustDueDate,
            ZonedDateTime invoiceDate, AccountAdjustSupportData accountAdjustSupportData, String accountType,
            String accountPlan, String userSequenceId) {
        AccountChargesData accountChargesData = new AccountChargesData();
        accountChargesData.setAccountId(accountId);
        accountChargesData.setApplicationName("BCMOAD");
        accountChargesData.setDueDate(adjustDueDate);
        accountChargesData.setInvoiceDate(invoiceDate);
        accountChargesData.setServiceChargeIndicator(accountAdjustSupportData.getServiceChargeIndicator());
        accountChargesData.setServiceChargeAmount(accountAdjustSupportData.getServiceChargeAmount());
        accountChargesData.setServiceChargeCode('D');
        accountChargesData.setServiceChargeTypeCode(accountAdjustSupportData.getServiceChargeTypeCode());
        accountChargesData.setCurrentSE3Date(dateService.currentDate());
        accountChargesData.setUserId(userSequenceId);
        accountChargesData.setAccountType(accountType);
        accountChargesData.setAccountPlan(accountPlan);
        return accountChargesData;
    }

    private void insertServiceChargeAmountOrDownpaymentAmount(String accountId, Double serviceChargeAmount,
            DownPaymentFee downpaymentFeeData, char invoiceCode, Double downPaymentFeeAmount,
            ZonedDateTime adjustDueDate, ZonedDateTime invoiceDate) {
        if (serviceChargeAmount != DECIMAL_ZERO) {
            insertDownPaymentFee(accountId, serviceChargeAmount, downpaymentFeeData, invoiceCode, adjustDueDate,
                    invoiceDate);
        } else if (downPaymentFeeAmount != DECIMAL_ZERO) {
            insertDownPaymentFee(accountId, downPaymentFeeAmount, downpaymentFeeData, invoiceCode, adjustDueDate,
                    invoiceDate);
        }

    }

    private void updateServiceChargeToDownpayment(Double downPaymentFeeAmount, List<BilCrgAmounts> serviceChargeList,
            DownPaymentFee downpaymentFeeData) {
        if (serviceChargeList.size() > SHORT_ONE) {
            throw new InvalidDataException("multiple.charges");
        }
        BilCrgAmounts serviceChargeRow = serviceChargeList.get(SHORT_ZERO);

        if (downPaymentFeeAmount == DECIMAL_ZERO) {
            serviceChargeRow.setBilCrgTypeCd(ChargeType.DOWNPAYMENTCHARGES.getChargeType());

            downpaymentFeeData.setTotalDownpaymentChargeAmount(downpaymentFeeData.getTotalDownpaymentChargeAmount()
                    .add(BigDecimal.valueOf(serviceChargeRow.getBcaCrgAmt())));
        } else {
            serviceChargeRow.setBilCrgTypeCd(ChargeType.DOWNPAYMENTCHARGES.getChargeType());
            serviceChargeRow.setBcaCrgAmt(downPaymentFeeAmount);
            downpaymentFeeData.setTotalDownpaymentChargeAmount(
                    downpaymentFeeData.getTotalDownpaymentChargeAmount().add(BigDecimal.valueOf(downPaymentFeeAmount)));
        }

        bilCrgAmountsRepository.saveAndFlush(serviceChargeRow);

    }

    private void insertDownPaymentFee(String accountId, Double amount, DownPaymentFee downpaymentFeeData,
            char invoiceCode, ZonedDateTime adjustDueDate, ZonedDateTime invoiceDate) {
        logger.debug("insert downpayment charge row to BIL_CRG_AMOUNTS");
        Integer maxSequenceNumber = getChargeMaxSequenceNumber(accountId);
        short sequenceNumber = getIncrementalValue(maxSequenceNumber);
        BilCrgAmounts downPaymentCharges = new BilCrgAmounts();
        downPaymentCharges.setBilCrgAmountsId(new BilCrgAmountsId(accountId, sequenceNumber));
        downPaymentCharges.setBilAdjDueDt(adjustDueDate);
        downPaymentCharges.setBilInvDt(invoiceDate);
        downPaymentCharges.setBcaCrgAmt(amount);
        downPaymentCharges.setBcaWroCrgAmt(DECIMAL_ZERO);
        downPaymentCharges.setBcaCrgPaidAmt(DECIMAL_ZERO);
        downPaymentCharges.setBilInvoiceCd(invoiceCode);
        downPaymentCharges.setBilCrgTypeCd(ChargeType.DOWNPAYMENTCHARGES.getChargeType());
        bilCrgAmountsRepository.saveAndFlush(downPaymentCharges);

        downpaymentFeeData.setTotalDownpaymentChargeAmount(
                downpaymentFeeData.getTotalDownpaymentChargeAmount().add(BigDecimal.valueOf(amount)));

    }

    private Double checkFutureBalance(String accountId, ZonedDateTime adjustDueDate, int numberOfQualifiedPolicies,
            String policyId) {

        Double futureBalance = null;
        if (numberOfQualifiedPolicies > SHORT_ONE) {
            futureBalance = bilIstScheduleRepository.findBalanceByAccountIdDueDate(accountId, adjustDueDate);
        } else if (numberOfQualifiedPolicies == SHORT_ONE) {
            futureBalance = bilIstScheduleRepository.findBalanceByAccountIdDueDatePolicyId(accountId, adjustDueDate,
                    policyId);
        }
        return futureBalance;

    }

    private boolean reviewEarlierAndFutureDatesAndBilAmounts(String accountId, String policyId,
            ZonedDateTime policyEffectiveDate, ZonedDateTime adjustDueDate, short sequenceNumber,
            ZonedDateTime planExpirationDate, boolean isQuote) {

        logger.debug("check the schedule for earlier due dates for the policy term");
        Integer earlierDueDateCount = bilIstScheduleRepository.findEarlierDueDateRowCount(accountId, policyId,
                policyEffectiveDate, adjustDueDate);
        if (earlierDueDateCount != null && earlierDueDateCount > SHORT_ZERO) {
            return false;
        }
        logger.debug("check the schedule for future due dates for the policy term");
        Integer futureDueDateCount = bilIstScheduleRepository.findFutureDueDateRowCount(accountId, policyId,
                policyEffectiveDate, adjustDueDate);
        if (futureDueDateCount == null || futureDueDateCount == SHORT_ZERO) {
            return false;
        }

        if (!isQuote) {
            return reviewBilAmounts(accountId, policyId, policyEffectiveDate, sequenceNumber, planExpirationDate);
        }
        return true;

    }

    private boolean reviewBilAmounts(String accountId, String policyId, ZonedDateTime policyEffectiveDate,
            short sequenceNumber, ZonedDateTime planExpirationDate) {

        logger.debug("read non zero BIL_AMT_SCH_RLT rows to exclude the impact of reinstatement after cancellation");
        List<Short> bilAmountSequenceNumberList = bilAmtSchRltRepository.findBilAmountSequenceNumbers(accountId,
                policyId, sequenceNumber, DECIMAL_ZERO);
        if (bilAmountSequenceNumberList != null && !bilAmountSequenceNumberList.isEmpty()) {
            logger.debug("check existance of reissue new terms");
            Integer newBusinessTermCount = bilAmountsRepository.checkExistanceByPolicyIdDatesEffectiveIndicator(
                    accountId, policyId, policyEffectiveDate, planExpirationDate,
                    Arrays.asList(BilAmountsEftIndicator.NEWBUSINESS.getValue(),
                            BilAmountsEftIndicator.NEW_BUSINESS.getValue()),
                    bilAmountSequenceNumberList);
            if (newBusinessTermCount == null || newBusinessTermCount == SHORT_ZERO) {
                Integer reissueNewTermCount = bilAmountsRepository.checkExistanceByPolicyIdDatesEffectiveIndicator(
                        accountId, policyId, policyEffectiveDate, planExpirationDate,
                        Arrays.asList(BilAmountsEftIndicator.REISSUE_NEW_TERM.getValue()), bilAmountSequenceNumberList);
                if (reissueNewTermCount != null && reissueNewTermCount != SHORT_ZERO) {
                    return determineReissue(accountId, policyId);
                }
            }

        }
        return true;

    }

    private boolean determineReissue(String accountId, String policyId) {
        BilPolicy bilPolicy = bilPolicyRepository.findByPolicyId(policyId,
                AdjustDueDatesConstants.getPolicyIssueIndicator());
        if (bilPolicy != null) {
            String oldPolicyId = BLANK_STRING;
            String minimumPolicyId = bilPolicyRepository.findMinPolicyIdByPolNumberPolSymbol(bilPolicy.getPolNbr(),
                    bilPolicy.getPolSymbolCd(), AdjustDueDatesConstants.getPolicyIssueIndicator());
            String maximumPolicyId = bilPolicyRepository.findMaxPolicyIdByPolNumberPolSymbol(bilPolicy.getPolNbr(),
                    bilPolicy.getPolSymbolCd(), AdjustDueDatesConstants.getPolicyIssueIndicator());

            if (minimumPolicyId != null && maximumPolicyId != null && minimumPolicyId.equals(maximumPolicyId)
                    && minimumPolicyId.equals(policyId)) {
                oldPolicyId = policyId;
            } else if (minimumPolicyId != null && !minimumPolicyId.equals(policyId)) {
                oldPolicyId = minimumPolicyId;
            } else if (maximumPolicyId != null && !maximumPolicyId.equals(policyId)) {
                oldPolicyId = maximumPolicyId;
            }
            if (!oldPolicyId.trim().isEmpty()) {
                Integer reissueNewTermCount = bilAmountsRepository.checkExistanceByPolicyIdEffectiveIndicator(accountId,
                        oldPolicyId, AdjustDueDatesConstants.getRenewalAmtEffectiveIndicator());
                if (reissueNewTermCount == null || reissueNewTermCount == SHORT_ZERO) {
                    return true;
                }
            }
        }

        return false;

    }

    private boolean checkForSeasonalBill(String seasionalExclusionCode) {
        Integer count = bilSeasExclRepository.getBillingItemRow(seasionalExclusionCode);
        return count != null && count != SHORT_ZERO;
    }

    private int getReferenceDuration(ZonedDateTime startReferenceDate, ZonedDateTime startDueDate) {
        logger.debug("get days between startReferenceDate and startDueDate");
        if (!startReferenceDate.equals(startDueDate)) {
			return (int) ChronoUnit.DAYS.between(startDueDate, startReferenceDate);
        }
        return SHORT_ZERO;

    }

    private void processLookAheadDueDateResetLogic(String accountId, short dueDateDisplacementNumber,
            char includeWeekend, AccountAdjustDates accountAdjustDates, boolean isXHOLRule, BilRulesUct bilRulesUct) {
        logger.debug("check if the schedule has any univoiced rows that are due before the term is effective"
                + "and have been adjusted by LADD processing. If found reset BIL_DATES/BIL_IST_SCHEDULE/BIL_CRE_SCHEDULE rows");
        List<ZonedDateTime> adjustDueDateList = bilIstScheduleRepository.findDistinctAdjustDueDates(accountId,
                AdjustDueDatesConstants.getFutureInvoiceCodes());

        if (adjustDueDateList != null && !adjustDueDateList.isEmpty()) {

            List<BilDates> bilInvoicingDateScheduleList = bilDatesRepository.findDataForLookAheadDateReset(accountId,
                    DateIndicator.DATEOVERRIDEN_BYUSER.getValue(), AdjustDueDatesConstants.getFutureInvoiceCodes(),
                    adjustDueDateList, BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

            if (bilInvoicingDateScheduleList != null && !bilInvoicingDateScheduleList.isEmpty()) {
                ZonedDateTime previousAdjustDueDate = null;
                ZonedDateTime newAdjustDueDate = null;
                ZonedDateTime newInvoiceDate = null;
                for (BilDates bilInvoicingDateSchedule : bilInvoicingDateScheduleList) {
                    ZonedDateTime adjustDueDate = bilInvoicingDateSchedule.getBilAdjDueDt();
                    ZonedDateTime systemDueDate = bilInvoicingDateSchedule.getBilSysDueDt();

                    if (previousAdjustDueDate == null || adjustDueDate.compareTo(previousAdjustDueDate) != SHORT_ZERO) {
                        newAdjustDueDate = systemDueDate;
                        newInvoiceDate = firstPossibleDueDateService.getFpddInvoiceDate(systemDueDate,
                                accountAdjustDates.getProcessDate(), includeWeekend, dueDateDisplacementNumber,
                                isXHOLRule, bilRulesUct);

                    }
                    if (bilInvoicingDateSchedule.getBdtDateInd() == DateIndicator.LOOKAHEAD_DUEDATE.getValue()) {
                        bilInvoicingDateSchedule.setBdtDateInd(DateIndicator.DATENOTOVERRIDEN_BYUSER.getValue());
                    }

                    bilDatesRepository.updateRescindCashRowBySysDate(newAdjustDueDate, newInvoiceDate,
                            bilInvoicingDateSchedule.getBdtDateInd(), accountId, systemDueDate);

                    bilIstScheduleRepository.updateRescindScheduleRowByInvoiceCode(newAdjustDueDate, newInvoiceDate,
                            accountId, systemDueDate, AdjustDueDatesConstants.getFutureInvoiceCodes());

                    processLookAheadResetForBilCreditSchedule(accountId, newAdjustDueDate, systemDueDate,
                            newInvoiceDate);
                    processLookAheadResetForBilCashDisposition(accountId, newAdjustDueDate, systemDueDate,
                            newInvoiceDate);

                    previousAdjustDueDate = adjustDueDate;
                }

            }
        }

    }

    private void processLookAheadResetForBilCashDisposition(String accountId, ZonedDateTime adjustDueDate,
            ZonedDateTime systemDueDate, ZonedDateTime invoiceDate) {

        List<BilCashDsp> bilCashDspList = bilCashDspRepository.findBySystemDueDate(accountId, systemDueDate);

        if (bilCashDspList != null && !bilCashDspList.isEmpty()) {
            logger.debug("update adjustDueDate, invoiceDate and invoiceCode for BIL_CASH_DSP");
            for (BilCashDsp bilCashDsp : bilCashDspList) {
                bilCashDsp.setAdjustmentDueDate(adjustDueDate);
                bilCashDsp.setInvoiceDate(invoiceDate);
                bilCashDspRepository.saveAndFlush(bilCashDsp);
            }

        }

    }

    private void processLookAheadResetForBilCreditSchedule(String accountId, ZonedDateTime adjustDueDate,
            ZonedDateTime systemDueDate, ZonedDateTime invoiceDate) {

        List<BilCreSchedule> creditScheduleList = bilCreScheduleRepository.findBySystemDueDate(accountId,
                systemDueDate);

        if (creditScheduleList != null && !creditScheduleList.isEmpty()) {
            logger.debug("LADD reset - Update adjustDueDate, invoiceDate for BIL_CRE_SCHEDULE");
            for (BilCreSchedule creditSchedule : creditScheduleList) {
                creditSchedule.setBilAdjDueDt(adjustDueDate);
                creditSchedule.setBilInvDt(invoiceDate);
                bilCreScheduleRepository.saveAndFlush(creditSchedule);
            }

        }

    }

    @Override
    @Transactional
    public boolean checkForStateSpecificServiceCharge(String bilTypeCode, String accountId, String bilClassCode) {
        bilActInquiryRepository.deleteById(new BilActInquiryId(accountId, "NRV"));
        if (bilTypeCode.equals(BillingTypeCode.SINGLEPOLICY.getValue())) {
            logger.debug("check for service charges rule - Single Policy");

            BilRulesUct serviceChargeRule = bilRulesUctService.readBilRulesUct(RULE_SVC, bilTypeCode, bilClassCode,
                    BLANK_STRING);

            if (serviceChargeRule != null && serviceChargeRule.getBrtRuleCd() == CHAR_Y) {
                String stateCode = bilPolicyTermRepository.findStateByMaxPolicyEffectiveDate(accountId,
                        AdjustDueDatesConstants.getPolicyStatusCodesSvcrule());
                if (stateCode != null) {
                    String ruleId = SVCO_RULE_PREFIX.concat(stateCode);

                    BilRulesUct stateServiceChargeRule = bilRulesUctService.readBilRulesUct(ruleId, bilTypeCode,
                            bilClassCode, BLANK_STRING);
                    if (stateServiceChargeRule != null) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    @Transactional
    public void processDatesAssignment(char dateType, BilAccount bilAccount, AccountAdjustDates accountAdjustDates,
            AdjustDatesReviewDetails adjustDatesReviewDetails, FirstPossibleDueDate firstPossibleDueDate,
            VariableSeviceChargeOverride variableSeviceChargeOverride, VariableSeviceCharge variableSeviceCharge,
            ChargeVariables chargeVariables, AccountAdjustSupportData accountAdjustSupportData, boolean isDisableFpdd,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, boolean isXHOLRule,
            BilRulesUct bilRulesUct, BilSupportPlan bilSupportPlan) {

        ZonedDateTime maxAdjustDueDate = bilIstScheduleRepository
                .findMaxAdjustDueDateByAccountId(bilAccount.getAccountId());
        if (maxAdjustDueDate != null && maxAdjustDueDate.compareTo(DateRoutine.defaultDateTime()) == SHORT_ZERO) {
            if (adjustDatesReviewDetails.getFpddIndicator() != CHAR_Y
                    && !adjustDatesReviewDetails.isEftCollectionMethod()) {

                firstPossibleDueDateService.fpddCheck(dateType, adjustDatesReviewDetails, firstPossibleDueDate,
                        bilSupportPlan, accountAdjustDates, bilAccount, variableSeviceChargeOverride,
                        variableSeviceCharge, chargeVariables, isDisableFpdd, true, bilEftPlanChargeList,
                        bilSupportPlanChargeList, isXHOLRule, bilRulesUct, accountAdjustSupportData);
            }

            assignProcess(bilAccount, firstPossibleDueDate.getDate());
            processCreditScheduleRows(bilAccount.getAccountId());
            processCashDipositionRows(bilAccount.getAccountId());
        }
    }

    private void processCashDipositionRows(String accountId) {

        String policyId = BLANK_STRING;
        Short bilSeqNbr = null;

        List<BilCashDsp> bilCashDspList = bilCashDspRepository.fetchDefaultCashRow(accountId, SEPARATOR_BLANK, "AP",
                BLANK_CHAR, DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime(),
                DateRoutine.defaultDateTime());

        if (bilCashDspList != null && !bilCashDspList.isEmpty()) {
            for (BilCashDsp bilCashDsp : bilCashDspList) {

                if (policyId.isEmpty() || !policyId.equals(bilCashDsp.getPolicyId()) || bilSeqNbr == null
                        || bilSeqNbr != bilCashDsp.getBilSequenceNumber()) {
                    BilIstSchedule bilIstSchedule = bilIstScheduleRepository
                            .findById(new BilIstScheduleId(bilCashDsp.getBilCashDspId().getAccountId(),
                                    bilCashDsp.getPolicyId(), bilCashDsp.getBilSequenceNumber()))
                            .orElse(null);
                    if (bilIstSchedule != null) {
                        bilCashDspRepository.updateCashRowBybilSeqNbr(bilIstSchedule.getBillSysDueDt(),
                                bilIstSchedule.getBillAdjDueDt(), bilIstSchedule.getBillInvDt(), accountId,
                                bilCashDsp.getPolicyId(), bilCashDsp.getBilSequenceNumber());
                    }
                }

                policyId = bilCashDsp.getPolicyId();
                bilSeqNbr = bilCashDsp.getBilSequenceNumber();

            }
        }

    }

    private void processCreditScheduleRows(String accountId) {

        String policyId = BLANK_STRING;
        Short bilSeqNbr = null;

        List<BilCreSchedule> bilCreScheduleList = bilCreScheduleRepository.findDefaultCreScheduleData(accountId,
                DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime());
        if (bilCreScheduleList != null && !bilCreScheduleList.isEmpty()) {
            for (BilCreSchedule bilCreSchedule : bilCreScheduleList) {

                if (policyId.isEmpty() || !policyId.equals(bilCreSchedule.getBilCreScheduleId().getPolicyId())
                        || bilSeqNbr == null || bilSeqNbr != bilCreSchedule.getBilCreScheduleId().getBilSeqNbr()) {
                    BilIstSchedule bilIstSchedule = bilIstScheduleRepository
                            .findById(new BilIstScheduleId(bilCreSchedule.getBilCreScheduleId().getBilAccountId(),
                                    bilCreSchedule.getBilCreScheduleId().getPolicyId(),
                                    bilCreSchedule.getBilCreScheduleId().getBilSeqNbr()))
                            .orElse(null);
                    if (bilIstSchedule != null) {
                        bilCreScheduleRepository.updateCreScheduleRowBybilSeqNbr(bilIstSchedule.getBillSysDueDt(),
                                bilIstSchedule.getBillAdjDueDt(), bilIstSchedule.getBillInvDt(), accountId,
                                bilCreSchedule.getBilCreScheduleId().getPolicyId(),
                                bilCreSchedule.getBilCreScheduleId().getBilSeqNbr());
                    }
                }

                policyId = bilCreSchedule.getBilCreScheduleId().getPolicyId();
                bilSeqNbr = bilCreSchedule.getBilCreScheduleId().getBilSeqNbr();

            }
        }
    }

    private BilActRules defaultBilActRules() {
        BilActRules bilActRules = new BilActRules();
        bilActRules.setBruDsbCkDays(SHORT_ZERO);
        bilActRules.setBruDsbEftDays(SHORT_ZERO);
        bilActRules.setBruDsbCrcrdDays(SHORT_ZERO);
        bilActRules.setBruMinDsbAmt(DECIMAL_ZERO);
        bilActRules.setBruMaxDsbAmt(DECIMAL_ZERO);
        bilActRules.setBruRnlIfDays(SHORT_ZERO);
        bilActRules.setBruMinWroNbr(SHORT_ZERO);
        bilActRules.setBruCcrNbrDays(SHORT_ZERO);
        bilActRules.setBruRnlIfStatCd(BLANK_CHAR);
        bilActRules.setBruNsfCncInd(CHAR_N);
        bilActRules.setBruDsbRctDtInd(CHAR_N);
        bilActRules.setBruRnlBillInd(CHAR_N);
        bilActRules.setBruRenBillInd(CHAR_N);
        bilActRules.setBruRssBillInd(CHAR_N);
        bilActRules.setBruRnlQteInd(CHAR_N);
        bilActRules.setBruRnlIfWipInd(CHAR_N);
        bilActRules.setBruCcrCreditInd(CHAR_N);
        bilActRules.setBruCcrPartInd(CHAR_N);
        bilActRules.setBruWroLteInd(CHAR_N);
        bilActRules.setBruPstmrkReqInd(CHAR_N);
        bilActRules.setBruIncWknInd(CHAR_N);
        bilActRules.setBruDsbPacsDays(SHORT_ZERO);
        bilActRules.setBruFlpScgWroCd(CHAR_A);
        return bilActRules;
    }

    private boolean getBilCollectionMethod(AccountAdjustDates accountAdjustDates, BilAccount bilAccount) {
        boolean eftCollectionMethod = false;
        final char ACTIVE_STATUS = CHAR_A;
        final String EFT_COLLECTION_METHOD = "CET";
        BusCdTranslation busCdTranslation = busCdTranslationRepository
                .findByCodeAndType(bilAccount.getCollectionMethod(), EFT_COLLECTION_METHOD, language);
        if (busCdTranslation != null && !bilAccount.getCollectionPlan().trim().isEmpty()) {
            List<BilEftBank> bilEftBankList = bilEftBankRepository.findBilEftBankEntry(
                    accountAdjustDates.getAccountId(), CHAR_A, accountAdjustDates.getProcessDate(), ACTIVE_STATUS,
                    PageRequest.of(SHORT_ZERO, SHORT_ONE));
            if (bilEftBankList != null && !bilEftBankList.isEmpty()) {
                eftCollectionMethod = true;
            }
        }
        return eftCollectionMethod;
    }

    private void getEftCatchupDate(String accountId, AdjustDatesReviewDetails adjustDatesReviewDetails,
            char previousSchedule, AccountAdjustDates accountAdjustDates) {

        boolean isErcvRule = adjustDatesReviewDetails.isErcvRule();
        int ercvEftDays = adjustDatesReviewDetails.getErcvDays();
        ZonedDateTime earliestEftDueDt = adjustDatesReviewDetails.getEarliestEftDueDt();
        boolean isErcvIssDtInvoice = adjustDatesReviewDetails.isErcvIssDtInvoice();
        boolean isDicvRule = adjustDatesReviewDetails.isDicvRule();
        boolean isActBilled = adjustDatesReviewDetails.isAccountBilled();
        boolean isLateEntered = adjustDatesReviewDetails.isLateEntered();
        ZonedDateTime testAdjustDate = null;
        char fpddIndicator = adjustDatesReviewDetails.getFpddIndicator();
        List<BilDates> bilDatesList = bilDatesRepository.getMaxInvoicedDateByInvoiceCdList(accountId,
                accountAdjustDates.getProcessDate(), AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
        if (bilDatesList == null || bilDatesList.isEmpty()) {
            return;
        }

        ZonedDateTime recoveryTestAdjustDate = bilDatesList.get(SHORT_ZERO).getBilAdjDueDt();

        ZonedDateTime minAdjustDate = bilDatesRepository.getNextMinAdjDueDate(accountId,
                accountAdjustDates.getProcessDate(), AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
        if (minAdjustDate == null) {
            throw new DataNotFoundException("future.date.not.found", new Object[] { accountId });
        }
        testAdjustDate = minAdjustDate;
        boolean iRowSw = false;
        if (fpddIndicator == CHAR_Y) {
            ZonedDateTime recoveryTestDate = bilDatesRepository.getMaxInvoiceDate(accountId,
                    AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_I, accountAdjustDates.getProcessDate(),
                    CHAR_X);
            if (recoveryTestDate != null) {
                iRowSw = true;
            }

        }
        if (fpddIndicator == CHAR_Y && accountAdjustDates.getProcessDate().compareTo(testAdjustDate) < 0
                && testAdjustDate.compareTo(earliestEftDueDt) >= 0 && (isDicvRule || isErcvRule) && !iRowSw
                || accountAdjustDates.getProcessDate().compareTo(testAdjustDate) < 0
                        && testAdjustDate.compareTo(earliestEftDueDt) >= 0) {

            effectiveRecoverySe3(testAdjustDate, accountId, accountAdjustDates.getProcessDate());
            if (isErcvIssDtInvoice && !isActBilled && isLateEntered) {
                checkEffectiveInvoice(accountId, accountAdjustDates.getProcessDate());
            }
            return;
        }

        ZonedDateTime newAdjustDate = null;
        ZonedDateTime newInvoiceDate = DateRoutine.defaultSinceDateTime();
        boolean isMinSysDueDate = false;
        if (isErcvIssDtInvoice && !isActBilled && isLateEntered) {
            newAdjustDate = accountAdjustDates.getProcessDate();
            newInvoiceDate = accountAdjustDates.getProcessDate();
        } else {
            if (fpddIndicator == CHAR_Y && previousSchedule == CHAR_N && ercvEftDays == 0 && isErcvRule
                    || iRowSw && fpddIndicator == CHAR_Y) {
                newAdjustDate = bilDatesRepository.getMinAdjustDateByEarliestEftDueDt(accountId,
                        accountAdjustDates.getProcessDate(), earliestEftDueDt,
                        AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
            } else {
                if (fpddIndicator == CHAR_Y && (isDicvRule || isErcvRule) || iRowSw && fpddIndicator == CHAR_Y) {
                    newAdjustDate = bilDatesRepository.getMinAdjustDateByEarliestEftDueDt1(accountId,
                            accountAdjustDates.getProcessDate(), earliestEftDueDt,
                            AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
                } else {
                    if (ercvEftDays == 0 && isErcvRule) {
                        newAdjustDate = bilDatesRepository.getMinSysDueDateByEarliestEftDueDt(accountId,
                                accountAdjustDates.getProcessDate(), earliestEftDueDt,
                                AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
                        if (newAdjustDate != null) {
                            isMinSysDueDate = true;
                        }
                    } else {
                        if (!isDicvRule && !isErcvRule || fpddIndicator == CHAR_N) {
                            newAdjustDate = bilDatesRepository.getMinAdjustDateByEarliestEftDueDt1(accountId,
                                    accountAdjustDates.getProcessDate(), earliestEftDueDt,
                                    AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
                        }
                    }
                }
            }
        }
        if (isMinSysDueDate) {
            newInvoiceDate = bilDatesRepository.getMinInvoicedDateBySystemDueDate(accountId, newAdjustDate,
                    AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
        } else if (!isErcvIssDtInvoice || newAdjustDate.compareTo(newInvoiceDate) != 0) {
            newInvoiceDate = bilDatesRepository.getMinInvoicedDateByAdjustDate(accountId, newAdjustDate,
                    AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
        }

        if (newInvoiceDate != null && newInvoiceDate.compareTo(accountAdjustDates.getProcessDate()) < 0) {
            newInvoiceDate = accountAdjustDates.getProcessDate();
        }

        List<BilDates> bilDateList = bilDatesRepository.getBilDatesByInvoiceCode(accountId,
                AdjustDueDatesConstants.getFutureInvoiceCodes(), recoveryTestAdjustDate, CHAR_X);
        if (bilDateList != null && !bilDateList.isEmpty()) {
            for (BilDates bilDates : bilDateList) {
                bilDates.setBilAdjDueDt(newAdjustDate);
                bilDates.setBilInvDt(newInvoiceDate);
                bilDatesRepository.saveAndFlush(bilDates);
                bilIstScheduleRepository.updateInvoiceDateAndAdjustDateBySystemDueDate(newInvoiceDate, newAdjustDate,
                        accountId, bilDates.getBilSysDueDt());
                bilCreScheduleRepository.updateInvDateAndAdjDateRowBySystemDueDate(newInvoiceDate, newAdjustDate,
                        accountId, bilDates.getBilSysDueDt());
                bilCashDspRepository.updateInvDateAndAdjDateRowBySystemDueDate(newInvoiceDate, newAdjustDate, accountId,
                        bilDates.getBilSysDueDt(), CHAR_D);

            }
        }

    }

    @Override
    public char getPreviousSchedule(boolean isDicvRule, AccountAdjustDates accountAdjustDates) {
        char previousSchedule = ScheduleCode.NO_SCHEDULE.getValue();
        if (isDicvRule) {
            return ScheduleCode.CURRENT_SCHEDULE.getValue();
        }

        Integer count = bilIstScheduleRepository.getRowCountNumber(accountAdjustDates.getAccountId(),
                DateRoutine.defaultDateTime());
        if (count != null && count != SHORT_ZERO) {
            previousSchedule = ScheduleCode.CURRENT_SCHEDULE.getValue();
        }
        ZonedDateTime referenceDate = bilDatesRepository.getMaxReferenceDateByInvoiceCode(
                accountAdjustDates.getAccountId(),
                Arrays.asList(BLANK_CHAR, InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue(),
                        InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue()),
                CHAR_X);
        if (referenceDate != null) {
            return ScheduleCode.CURRENT_SCHEDULE.getValue();
        } else if (previousSchedule == ScheduleCode.CURRENT_SCHEDULE.getValue()) {
            previousSchedule = ScheduleCode.FUTURE_SCHEDULE.getValue();
        } else {
            return previousSchedule;
        }

        return previousSchedule;
    }

    @Override
    public char getNonPreviousSchedule(AccountAdjustDates accountAdjustDates, Double minimumInvoiceAmount) {

        ZonedDateTime invoiceDate = bilIstScheduleRepository.findMaxInvoiceDateByInvoiceCd(
                accountAdjustDates.getAccountId(), DateRoutine.defaultDateTime(), accountAdjustDates.getProcessDate(),
                accountAdjustDates.getProcessDate(), AdjustDueDatesConstants.getFutureInvoiceCodes());
        if (invoiceDate == null) {
            return CHAR_Y;
        }
        Double amount = bilIstScheduleRepository.getDueAmountByInvoiceDate(accountAdjustDates.getAccountId(),
                invoiceDate);

        if (amount.compareTo(minimumInvoiceAmount) >= 0) {
            return CHAR_N;
        } else {
            return CHAR_Y;
        }
    }

    private void effectiveRecoverySe3(ZonedDateTime testAdjustDate, String accountId, ZonedDateTime se3Date) {

        List<BilDates> bilDatesList = bilDatesRepository.getBilDatesByInvoiceCode(accountId,
                AdjustDueDatesConstants.getFutureInvoiceCodes(), testAdjustDate, CHAR_X);
        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            ZonedDateTime preSystemDate = null;
            for (BilDates bilDates : bilDatesList) {

                if (preSystemDate == null || preSystemDate.compareTo(bilDates.getBilSysDueDt()) != SHORT_ZERO) {
                    bilIstScheduleRepository.updateInvoiceDateRowBySystemDueDate(se3Date, accountId,
                            bilDates.getBilSysDueDt());
                    bilCreScheduleRepository.updateInvoiceDateRowBySystemDueDate(se3Date, accountId,
                            bilDates.getBilSysDueDt());
                    bilCashDspRepository.updateInvoiceDateRowBySystemDueDate(se3Date, accountId,
                            bilDates.getBilSysDueDt(), ChargeType.DOWNPAYMENTCHARGES.getChargeType());
                    preSystemDate = bilDates.getBilSysDueDt();
                }

                bilDates.setBilInvDt(se3Date);
                bilDatesRepository.saveAndFlush(bilDates);

            }
        }

    }

    private void checkEffectiveInvoice(String accountId, ZonedDateTime se3Date) {

        List<BilDates> bilDatesList = bilDatesRepository.getEffectiveInvoice(accountId, se3Date, se3Date,
                AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_I, CHAR_X);

        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            for (BilDates bilDates : bilDatesList) {

                bilDatesRepository.updateBilDatesByAdjDueDate(se3Date, se3Date, BLANK_CHAR,
                        bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilAdjDueDt(), bilDates.getBilInvDt());
                bilIstScheduleRepository.updateRescindScheduleRowByAdjustDate(se3Date, se3Date, BLANK_CHAR,
                        bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilAdjDueDt(), bilDates.getBilInvDt());
                bilCreScheduleRepository.updateRescindCreScheduleRowBybilAdjDueDt(se3Date, se3Date,
                        bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilAdjDueDt(), bilDates.getBilInvDt());
                if (bilDates.getBilInvoiceCd() == CHAR_Y || bilDates.getBilInvoiceCd() == 'L') {

                    bilCashDspRepository.updateRescindCashRowByAdjDueDate(se3Date, se3Date,
                            bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilAdjDueDt(),
                            bilDates.getBilInvDt());
                } else {
                    bilCashDspRepository.updateRescindCashRowByAdjDueDate(se3Date, se3Date,
                            bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilAdjDueDt(),
                            bilDates.getBilInvDt(), ChargeType.DOWNPAYMENTCHARGES.getChargeType());
                }
                bilCrgAmountsRepository.updateInvoiceCdBybilAdjDueDt(se3Date, se3Date, bilDates.getBilInvoiceCd(),
                        bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilAdjDueDt(), bilDates.getBilInvDt(),
                        AdjustDueDatesConstants.getChargeTypes());

            }
        }
    }

    @Override
    @Transactional
    public void processChargeRows(int cancelCount, char previousSchedule, BilAccount bilAccount,
            AccountAdjustDates accountAdjustDates, FirstPossibleDueDate firstPossibleDueDate,
            AdjustDatesReviewDetails adjustDatesReviewDetails, boolean isXHOLRule, BilRulesUct bilRulesUct,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList,
            VariableSeviceChargeOverride variableSeviceChargeOverride, VariableSeviceCharge variableSeviceCharge,
            AccountAdjustSupportData accountAdjustSupportData, ChargeVariables chargeVariables,
            List<BilCrgAmounts> downPaymentList, DownPaymentFee downPaymentFee) {

        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();
        ZonedDateTime newAdjustDate = null;
        ZonedDateTime newInvoiceDate = null;
        if (accountAdjustDates.getSuspendBillIndicator() == CHAR_A) {
            suspendBill(accountAdjustDates, firstPossibleDueDate, adjustDatesReviewDetails, isXHOLRule, bilRulesUct,
                    variableSeviceChargeOverride, downPaymentFee, variableSeviceCharge, accountAdjustSupportData,
                    downPaymentList, bilEftPlanChargeList, bilSupportPlanChargeList, chargeVariables);
            return;
        }

        if (bilAccount.getStatus() != AccountStatus.SUSPEND_BILLING_FOLLOW_UP.toChar()
                && bilAccount.getStatus() != AccountStatus.SUSPEND_BILLING.toChar()
                && !adjustDatesReviewDetails.isEftCollectionMethod()
                && adjustDatesReviewDetails.getFpddIndicator() != CHAR_Y
                && accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y) {

            firstPossibleDueDateService.processFirstPossibleDueDateResetLogic(bilAccount, firstPossibleDueDate,
                    bilSupportPlan.getBspMinInvAmt(), bilSupportPlan.getBspPndCncNbr(),
                    adjustDatesReviewDetails.getBilActRules().getBruIncWknInd(), accountAdjustDates,
                    adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, bilRulesUct, bilSupportPlan,
                    variableSeviceCharge, chargeVariables, variableSeviceChargeOverride, adjustDatesReviewDetails,
                    accountAdjustSupportData, bilEftPlanChargeList, bilSupportPlanChargeList, cancelCount);

        }

        if (adjustDatesReviewDetails.getOutOfCycle() == CHAR_N) {
            processOutOfCycleBilling(bilAccount, adjustDatesReviewDetails, accountAdjustDates, firstPossibleDueDate,
                    accountAdjustSupportData, chargeVariables, variableSeviceChargeOverride, variableSeviceCharge,
                    bilEftPlanChargeList, bilSupportPlanChargeList, cancelCount, isXHOLRule, bilRulesUct);
        }

        if (accountAdjustDates.getBillPlanChangeIndicator() == CHAR_Y) {
            deleteServiceCharges(bilAccount.getAccountId());
            return;
        }

        cancelCount = SHORT_ZERO;
        if (accountAdjustDates.getApplicationName().equals("BCMOSC")) {
            List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.fetchBilPolicyTermByStatusEffective(
                    accountAdjustDates.getAccountId(),
                    Arrays.asList(BilPolicyStatusCode.CANCELLED.getValue(),
                            BilPolicyStatusCode.FLAT_CANCELLATION.getValue(),
                            BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                            BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(),
                            BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                            BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue()),
                    accountAdjustDates.getProcessDate());
            if (bilPolicyTermList != null && !bilPolicyTermList.isEmpty()) {
                cancelCount = SHORT_ONE;
            }
            // not to required as of now
        }

        deleteServiceCharges(bilAccount.getAccountId());

        if (!adjustDatesReviewDetails.isEftCollectionMethod()
                && adjustDatesReviewDetails.getFpddIndicator() != CHAR_Y) {
            // check1
            processLookAheadDueDateResetLogic(bilAccount.getAccountId(),
                    adjustDatesReviewDetails.getDueDateDisplacementNumber(),
                    adjustDatesReviewDetails.getBilActRules().getBruIncWknInd(), accountAdjustDates, isXHOLRule,
                    bilRulesUct);
            setServiceChargeInvoive(firstPossibleDueDate, bilAccount, adjustDatesReviewDetails, accountAdjustDates,
                    variableSeviceChargeOverride, variableSeviceCharge, chargeVariables, bilEftPlanChargeList,
                    bilSupportPlanChargeList, isXHOLRule, bilRulesUct, accountAdjustSupportData);

        } else {
            getEftCatchupDate(bilAccount.getAccountId(), adjustDatesReviewDetails, previousSchedule,
                    accountAdjustDates);

            if (bilAccount.getStatus() != AccountStatus.SUSPEND_BILLING_FOLLOW_UP.toChar()
                    && bilAccount.getStatus() != AccountStatus.SUSPEND_BILLING.toChar()
                    && !adjustDatesReviewDetails.isEftCollectionMethod()
                    && adjustDatesReviewDetails.getFpddIndicator() == CHAR_Y
                    && !adjustDatesReviewDetails.isManualReinvoice()) {
                xxxxSetupDiMinReset(accountAdjustDates, adjustDatesReviewDetails, isXHOLRule, bilRulesUct);
            }
            if (adjustDatesReviewDetails.isErcvIssDtInvoice() && !adjustDatesReviewDetails.isAccountBilled()) {
                checkEftPastRef(accountAdjustDates);
                newAdjustDate = accountAdjustDates.getProcessDate();
                newInvoiceDate = accountAdjustDates.getProcessDate();
            }

            List<ZonedDateTime> bilAdjustDueDateList = bilDatesRepository.fetchDistinctAdjustDate(
                    bilAccount.getAccountId(), AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);

            if (bilAdjustDueDateList != null && !bilAdjustDueDateList.isEmpty()) {
                for (ZonedDateTime adjustdueDate : bilAdjustDueDateList) {
                    getDatesInSync(adjustdueDate, accountAdjustDates, adjustDatesReviewDetails, bilAccount,
                            chargeVariables, bilEftPlanChargeList, bilSupportPlanChargeList,
                            variableSeviceChargeOverride, variableSeviceCharge, accountAdjustSupportData);
                }

            }

        }

        firstPossibleDueDateService.updatePenalty(bilAccount, accountAdjustDates, bilSupportPlan, variableSeviceCharge,
                chargeVariables, cancelCount, newAdjustDate, newInvoiceDate, variableSeviceChargeOverride,
                adjustDatesReviewDetails, accountAdjustSupportData, bilEftPlanChargeList, bilSupportPlanChargeList,
                isXHOLRule, bilRulesUct, firstPossibleDueDate);

        firstPossibleDueDateService.processBelowMinimumServiceCharge(bilAccount,
                adjustDatesReviewDetails.isFpddOnTimeProcessing(), bilSupportPlan.getBspMinInvAmt(),
                accountAdjustSupportData.getServiceChargeAmount(), accountAdjustDates, adjustDatesReviewDetails,
                variableSeviceCharge, chargeVariables, variableSeviceChargeOverride, accountAdjustSupportData,
                bilEftPlanChargeList, bilSupportPlanChargeList);

        if (bilAccount.getStatus() != AccountStatus.SUSPEND_BILLING_FOLLOW_UP.toChar()
                && !bilAccount.getSuspendedFullReasonCd().trim().equals("AC")
                && !bilAccount.getSuspendedFullReasonCd().trim().equals("AP")
                && accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y) {
            processServiceWriteOff(bilAccount, adjustDatesReviewDetails.getBilActRules(), accountAdjustDates);
        }

        deleteBilDates(bilAccount.getAccountId());
        if (adjustDatesReviewDetails.isSeasonalBill()) {
            cleanupSeasonalBillRows(bilAccount);
        }

        if (adjustDatesReviewDetails.isDpifRule()) {
            boolean isQuote = accountAdjustDates.getQuoteIndicator().charAt(SHORT_ZERO) == CHAR_Q;
            processDownpaymentFees(accountAdjustDates.getAccountId(), isQuote, adjustDatesReviewDetails.getDpifAmount(),
                    downPaymentFee, variableSeviceChargeOverride, variableSeviceCharge, accountAdjustSupportData,
                    accountAdjustDates.getUserSequenceId());
            if (!downPaymentList.isEmpty()) {
                matchDownPayment(downPaymentList, accountAdjustDates, adjustDatesReviewDetails, bilAccount,
                        downPaymentFee);
            }
        } else {
            if (accountAdjustDates.getSupportPlanIndicator() == CHAR_Y && !adjustDatesReviewDetails.isDpifRule()
                    && downPaymentList != null && !downPaymentList.isEmpty()) {
                for (BilCrgAmounts bilCrgAmounts : downPaymentList) {
                    backoffDpfAmts(bilCrgAmounts, adjustDatesReviewDetails, accountAdjustDates, bilAccount);
                }
                downPaymentList.clear();
            }
        }
        if (downPaymentFee.getTotalDownpaymentChargeAmount().compareTo(BigDecimal.valueOf(DECIMAL_ZERO)) != 0) {
            if (isWriteHistory(accountAdjustDates.getQuoteIndicator(), accountAdjustDates.getAddChargeIndicator(),
                    accountAdjustDates.getChargeAmount())) {
                accountSummary("DPF", BLANK_STRING, downPaymentFee.getTotalDownpaymentChargeAmount().doubleValue(),
                        adjustDatesReviewDetails.getHoldNewDate(), accountAdjustDates, null);
            }

            String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(), bilAccount.getBillTypeCd());
            String masterCompanyNumber = bilmasterCompanyState[0];
            String riskState = bilmasterCompanyState[1];
            String se3Date = DateRoutine.dateTimeAsYYYYMMDDString(accountAdjustDates.getProcessDate());

            createServiceCharge(ObjectCode.DOWN_PAYMENT_FEE.toString(), ActionCode.SCHEDULE_TO_BILL.toString(),
                    downPaymentFee.getTotalDownpaymentChargeAmount().doubleValue(), masterCompanyNumber, riskState,
                    SEPARATOR_BLANK, SEPARATOR_BLANK, BLANK_CHAR, "FSW", SEPARATOR_BLANK, accountAdjustDates,
                    bilAccount, se3Date, BLANK_STRING, se3Date, se3Date);
        }

        if (isWriteHistory(accountAdjustDates.getQuoteIndicator(), accountAdjustDates.getAddChargeIndicator(),
                accountAdjustDates.getChargeAmount())) {

            if (accountAdjustDates.getSupportPlanIndicator() == CHAR_Y) {
                String acyDesCode = "BCC";
                if (!accountAdjustDates.getActivityDescriptionCode().trim().isEmpty()) {
                    acyDesCode = accountAdjustDates.getActivityDescriptionCode().trim();
                }
                accountSummary(acyDesCode, BLANK_STRING, DECIMAL_ZERO, adjustDatesReviewDetails.getHoldNewDate(),
                        accountAdjustDates, bilAccount);
            }

            if (accountAdjustDates.getStartDateIndicator() == CHAR_Y) {
                String acyDesCode = "SDC";
                String bilDesReasonType = BLANK_STRING;
                if (!accountAdjustDates.getActivityDescriptionCode().trim().isEmpty()) {
                    acyDesCode = accountAdjustDates.getActivityDescriptionCode().trim();
                    bilDesReasonType = accountAdjustDates.getReasonType();
                }
                accountSummary(acyDesCode, bilDesReasonType, DECIMAL_ZERO, adjustDatesReviewDetails.getHoldNewDate(),
                        accountAdjustDates, null);
            }

            if (accountAdjustDates.getSupportPlanIndicator() == CHAR_E) {
                String acyDesCode = "ECC";
                String bilDesReasonType = BLANK_STRING;
                accountSummary(acyDesCode, bilDesReasonType, DECIMAL_ZERO, adjustDatesReviewDetails.getHoldNewDate(),
                        accountAdjustDates, null);
            }

            if (accountAdjustDates.getReferenceDateIndicator() == CHAR_Y) {
                String acyDesCode = "RDC";
                String bilDesReasonType = BLANK_STRING;
                accountSummary(acyDesCode, bilDesReasonType, DECIMAL_ZERO, adjustDatesReviewDetails.getHoldNewDate(),
                        accountAdjustDates, null);

            }

        }

    }

    private void matchDownPayment(List<BilCrgAmounts> downPaymentList, AccountAdjustDates accountAdjustDates,
            AdjustDatesReviewDetails adjustDatesReviewDetails, BilAccount bilAccount, DownPaymentFee downPaymentFee) {

        List<BilCrgAmounts> bilCrgAmountList = bilCrgAmountsRepository.findDownPaymentCharges(
                accountAdjustDates.getAccountId(), AdjustDueDatesConstants.getFutureInvoiceCodes(),
                ChargeType.DOWNPAYMENTCHARGES.getChargeType());

        if (bilCrgAmountList == null || bilCrgAmountList.isEmpty()) {
            if (downPaymentFee.getTotalDownpaymentPaidAmount().doubleValue() == DECIMAL_ZERO
                    && downPaymentFee.getTotalDownpaymentWriteOffAmount().doubleValue() == DECIMAL_ZERO) {
                if (isWriteHistory(accountAdjustDates.getQuoteIndicator(), accountAdjustDates.getAddChargeIndicator(),
                        accountAdjustDates.getChargeAmount())) {
                    accountSummary("WDR", "WRR", downPaymentFee.getTotalDownpaymentWriteOffAmount().doubleValue(),
                            adjustDatesReviewDetails.getHoldNewDate(), accountAdjustDates, null);
                }
            } else {
                for (BilCrgAmounts bilCrgAmounts : downPaymentList) {
                    backoffDpfAmts(bilCrgAmounts, adjustDatesReviewDetails, accountAdjustDates, bilAccount);
                }
                downPaymentList.clear();
                return;
            }
        }

        double chargeBalance = DECIMAL_ZERO;

        BilCrgAmounts bilCrgAmounts = readMinimumInvocieDate(accountAdjustDates.getAccountId(),
                DateRoutine.dateTimeAsYYYYMMDD(0));

        String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(), bilAccount.getBillTypeCd());
        String masterCompanyNumber = bilmasterCompanyState[0];
        String riskState = bilmasterCompanyState[1];

        if (bilCrgAmounts != null) {
            chargeBalance = bilCrgAmounts.getBcaCrgAmt() - bilCrgAmounts.getBcaCrgPaidAmt()
                    - bilCrgAmounts.getBcaWroCrgAmt();
        }

        for (BilCrgAmounts downPayment : downPaymentList) {

            if (bilCrgAmounts == null) {
                backoffDpfAmts(downPayment, adjustDatesReviewDetails, accountAdjustDates, bilAccount);
            } else {
                if (downPayment.getBcaCrgPaidAmt() != DECIMAL_ZERO && chargeBalance != DECIMAL_ZERO) {
                    if (downPayment.getBcaCrgPaidAmt() > chargeBalance) {
                        BilCashReceipt bilCashReceipt = reversePaid(downPayment.getBcaCrgPaidAmt(),
                                downPayment.getBilCrgAmountsId().getBilSeqNbr(), bilAccount.getAccountId(), CHAR_D,
                                downPayment.getBilInvDt(), accountAdjustDates, bilAccount, masterCompanyNumber,
                                riskState);
                        if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y
                                && accountAdjustDates.getQuoteIndicator().charAt(SHORT_ZERO) != CHAR_Q) {
                            String se3Date = DateRoutine.dateTimeAsYYYYMMDDString(accountAdjustDates.getProcessDate());
                            createServiceCharge(ObjectCode.DOWN_PAYMENT_FEE.toString(),
                                    ActionCode.APPLY_TO_UNBILLED.toString(), downPayment.getBcaCrgPaidAmt() * -1,
                                    masterCompanyNumber, riskState, bilCashReceipt.getBankCd(),
                                    bilCashReceipt.getReceiptTypeCd(), bilCashReceipt.getCashEntryMethodCd(),
                                    SEPARATOR_BLANK, SEPARATOR_BLANK, accountAdjustDates, bilAccount, se3Date, se3Date,
                                    se3Date, se3Date);
                        }
                    } else {
                        bilCrgAmounts
                                .setBcaCrgPaidAmt(bilCrgAmounts.getBcaCrgPaidAmt() + downPayment.getBcaCrgPaidAmt());
                        bilCrgAmountsRepository.saveAndFlush(bilCrgAmounts);
                        if (bilCrgAmounts.getBilInvDt().compareTo(downPayment.getBilInvDt()) != SHORT_ZERO
                                || bilCrgAmounts.getBilAdjDueDt().compareTo(downPayment.getBilAdjDueDt()) != SHORT_ZERO
                                || bilCrgAmounts.getBilCrgAmountsId().getBilSeqNbr() != downPayment.getBilCrgAmountsId()
                                        .getBilSeqNbr()) {

                            bilCashDspRepository.updateInvDateAndAdjDateRowBySequence(bilCrgAmounts.getBilInvDt(),
                                    bilCrgAmounts.getBilAdjDueDt(), bilCrgAmounts.getBilCrgAmountsId().getBilSeqNbr(),
                                    accountAdjustDates.getAccountId(), BilDspTypeCode.APPLIED.getValue(),
                                    ReverseReSuspendIndicator.BLANK.getValue(), SEPARATOR_BLANK,
                                    DateRoutine.defaultDateTime(), downPayment.getBilCrgAmountsId().getBilSeqNbr(),
                                    ChargeType.DOWNPAYMENTCHARGES.getChargeType(), downPayment.getBilInvDt());
                        }

                        chargeBalance = chargeBalance - downPayment.getBcaCrgPaidAmt();
                    }
                }

                if (downPayment.getBcaWroCrgAmt() != DECIMAL_ZERO && chargeBalance != DECIMAL_ZERO) {
                    if (downPayment.getBcaWroCrgAmt() > chargeBalance) {
                        if (isWriteHistory(accountAdjustDates.getQuoteIndicator(),
                                accountAdjustDates.getAddChargeIndicator(), accountAdjustDates.getChargeAmount())) {
                            accountSummary("WDR", "WRR", downPayment.getBcaWroCrgAmt(),
                                    adjustDatesReviewDetails.getHoldNewDate(), accountAdjustDates, null);
                        }
                    } else {
                        bilCrgAmounts.setBcaWroCrgAmt(bilCrgAmounts.getBcaWroCrgAmt() + downPayment.getBcaWroCrgAmt());
                        bilCrgAmountsRepository.saveAndFlush(bilCrgAmounts);
                        chargeBalance = chargeBalance - downPayment.getBcaWroCrgAmt();
                    }
                }

                if (chargeBalance == DECIMAL_ZERO) {
                    bilCrgAmounts = readMinimumInvocieDate(accountAdjustDates.getAccountId(),
                            bilCrgAmounts.getBilInvDt());
                }
            }

        }
    }

    private BilCrgAmounts readMinimumInvocieDate(String accountId, ZonedDateTime invoiceDate) {

        List<BilCrgAmounts> bilCrgAmountsList = bilCrgAmountsRepository.readChargeRowsByInvoiceDate(accountId,
                invoiceDate, ChargeType.DOWNPAYMENTCHARGES.getChargeType(),
                AdjustDueDatesConstants.getFutureInvoiceCodes());
        if (bilCrgAmountsList != null && !bilCrgAmountsList.isEmpty()) {
            return bilCrgAmountsList.get(SHORT_ZERO);
        }

        return null;
    }

    private void backoffDpfAmts(BilCrgAmounts bilCrgAmounts, AdjustDatesReviewDetails adjustDatesReviewDetails,
            AccountAdjustDates accountAdjustDates, BilAccount bilAccount) {

        String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(), bilAccount.getBillTypeCd());
        String masterCompanyNumber = bilmasterCompanyState[0];
        String riskState = bilmasterCompanyState[1];

        if (bilCrgAmounts.getBcaWroCrgAmt() != DECIMAL_ZERO) {
            if (isWriteHistory(accountAdjustDates.getQuoteIndicator(), accountAdjustDates.getAddChargeIndicator(),
                    accountAdjustDates.getChargeAmount())) {
                accountSummary("WDR", "WRR", bilCrgAmounts.getBcaWroCrgAmt(), adjustDatesReviewDetails.getHoldNewDate(),
                        accountAdjustDates, null);
            }

            if (accountAdjustDates.getQuoteIndicator().charAt(SHORT_ZERO) != CHAR_Q) {
                String se3Date = DateRoutine.dateTimeAsYYYYMMDDString(accountAdjustDates.getProcessDate());
                createServiceCharge(ObjectCode.DOWN_PAYMENT_FEE.toString(), ActionCode.WRITE_OFF_UNBILLED.toString(),
                        bilCrgAmounts.getBcaWroCrgAmt() * -1, masterCompanyNumber, riskState, SEPARATOR_BLANK,
                        SEPARATOR_BLANK, BLANK_CHAR, "FSW", SEPARATOR_BLANK, accountAdjustDates, bilAccount, se3Date,
                        se3Date, SEPARATOR_BLANK, SEPARATOR_BLANK);
            }
        }

        if (bilCrgAmounts.getBcaCrgPaidAmt() != DECIMAL_ZERO) {
            BilCashReceipt bilCashReceipt = reversePaid(bilCrgAmounts.getBcaCrgPaidAmt(),
                    bilCrgAmounts.getBilCrgAmountsId().getBilSeqNbr(), bilAccount.getAccountId(), CHAR_D,
                    bilCrgAmounts.getBilInvDt(), accountAdjustDates, bilAccount, masterCompanyNumber, riskState);
            if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y
                    && accountAdjustDates.getQuoteIndicator().charAt(SHORT_ZERO) != CHAR_Q) {
                String se3Date = DateRoutine.dateTimeAsYYYYMMDDString(accountAdjustDates.getProcessDate());
                createServiceCharge(ObjectCode.DOWN_PAYMENT_FEE.toString(), ActionCode.APPLY_TO_UNBILLED.toString(),
                        bilCrgAmounts.getBcaCrgPaidAmt() * -1, masterCompanyNumber, riskState,
                        bilCashReceipt.getBankCd(), bilCashReceipt.getReceiptTypeCd(),
                        bilCashReceipt.getCashEntryMethodCd(), SEPARATOR_BLANK, SEPARATOR_BLANK, accountAdjustDates,
                        bilAccount, se3Date, se3Date, se3Date, se3Date);
            }
        }
    }

    private void suspendBill(AccountAdjustDates accountAdjustDates, FirstPossibleDueDate firstPossibleDueDate,
            AdjustDatesReviewDetails adjustDatesReviewDetails, boolean isXHOLRule, BilRulesUct bilRulesUct,
            VariableSeviceChargeOverride variableSeviceChargeOverride, DownPaymentFee downPaymentFee,
            VariableSeviceCharge variableSeviceCharge, AccountAdjustSupportData accountAdjustSupportData,
            List<BilCrgAmounts> downPaymentList, List<BilEftPlnCrg> bilEftPlanChargeList,
            List<BilSptPlnCrg> bilSupportPlanChargeList, ChargeVariables chargeVariables) {

        boolean isExist = false;

        if (accountAdjustDates.getSuspendBillIndicator() == CHAR_A) {
            boolean isAllRest = false;

            List<BilDates> bilDatesList = bilDatesRepository.getMaxLaddReferenceDateByInvoiceCode(
                    accountAdjustDates.getAccountId(), firstPossibleDueDate.getDate(), firstPossibleDueDate.getDate(),
                    CHAR_S, CHAR_Y, AdjustDueDatesConstants.getFutureInvoiceCodes(),
                    BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
            if (bilDatesList != null && !bilDatesList.isEmpty()) {
                BilDates bilDates = bilDatesList.get(SHORT_ZERO);

                ZonedDateTime newInvoiceDate = firstPossibleDueDateService.getFpddInvoiceDate(bilDates.getBilSysDueDt(),
                        accountAdjustDates.getProcessDate(),
                        adjustDatesReviewDetails.getBilActRules().getBruIncWknInd(),
                        adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, bilRulesUct);
                bilIstScheduleRepository.updateRescindScheduleRowByAdjustDate(bilDates.getBilSysDueDt(), newInvoiceDate,
                        bilDates.getBilInvoiceCd(), bilDates.getBilDatesId().getBilAccountId(),
                        bilDates.getBilAdjDueDt(), bilDates.getBilInvDt());
                bilCreScheduleRepository.updateRescindCreScheduleRowBybilAdjDueDt(bilDates.getBilSysDueDt(),
                        newInvoiceDate, bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilAdjDueDt(),
                        bilDates.getBilInvDt());
                if (bilDates.getBilInvoiceCd() == CHAR_Y || bilDates.getBilInvoiceCd() == CHAR_L) {

                    bilCashDspRepository.updateRescindCashRowByAdjDueDate(bilDates.getBilSysDueDt(), newInvoiceDate,
                            bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilAdjDueDt(),
                            bilDates.getBilInvDt());
                } else {
                    bilCashDspRepository.updateRescindCashRowByAdjDueDate(bilDates.getBilSysDueDt(), newInvoiceDate,
                            bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilAdjDueDt(),
                            bilDates.getBilInvDt(), ChargeType.DOWNPAYMENTCHARGES.getChargeType());
                }

                bilCrgAmountsRepository.updateInvoiceCdBybilAdjDueDt(bilDates.getBilSysDueDt(), newInvoiceDate,
                        bilDates.getBilInvoiceCd(), bilDates.getBilDatesId().getBilAccountId(),
                        bilDates.getBilAdjDueDt(), bilDates.getBilInvDt(), AdjustDueDatesConstants.getChargeTypes());
                bilDates.setBilAdjDueDt(bilDates.getBilSysDueDt());
                bilDates.setBilInvDt(newInvoiceDate);
                bilDatesRepository.saveAndFlush(bilDates);

                List<BilDates> bilDatesLists = bilDatesRepository.getBilDatesByInvoiceCode(
                        accountAdjustDates.getAccountId(), AdjustDueDatesConstants.getFutureInvoiceCodes(),
                        firstPossibleDueDate.getDate(), CHAR_X);
                if (bilDatesLists != null && !bilDatesLists.isEmpty()) {
                    isAllRest = true;
                }
            }

            if (isAllRest) {
                isExist = true;
            } else {
                suspendBillReset(accountAdjustDates, adjustDatesReviewDetails, isXHOLRule, bilRulesUct);

                firstPossibleDueDateService.processBelowMinimumServiceCharge(adjustDatesReviewDetails.getBilAccount(),
                        firstPossibleDueDate.isFpddSet(),
                        adjustDatesReviewDetails.getBilSupportPlan().getBspMinInvAmt(),
                        accountAdjustSupportData.getServiceChargeAmount(), accountAdjustDates, adjustDatesReviewDetails,
                        variableSeviceCharge, chargeVariables, variableSeviceChargeOverride, accountAdjustSupportData,
                        bilEftPlanChargeList, bilSupportPlanChargeList);
                isExist = true;

            }

            if (isExist) {
                if (adjustDatesReviewDetails.isDpifRule()) {

                    boolean isQuote = accountAdjustDates.getQuoteIndicator().charAt(SHORT_ZERO) == CHAR_Q;
                    processDownpaymentFees(accountAdjustDates.getAccountId(), isQuote,
                            adjustDatesReviewDetails.getDpifAmount(), downPaymentFee, variableSeviceChargeOverride,
                            variableSeviceCharge, accountAdjustSupportData, accountAdjustDates.getUserSequenceId());
                    if (!downPaymentList.isEmpty()) {
                        matchDownPayment(downPaymentList, accountAdjustDates, adjustDatesReviewDetails,
                                adjustDatesReviewDetails.getBilAccount(), downPaymentFee);
                    }
                }

                if (downPaymentFee.getTotalDownpaymentChargeAmount().compareTo(BigDecimal.valueOf(DECIMAL_ZERO)) != 0) {
                    if (isWriteHistory(accountAdjustDates.getQuoteIndicator(),
                            accountAdjustDates.getAddChargeIndicator(), accountAdjustDates.getChargeAmount())) {
                        accountSummary("DPF", BLANK_STRING,
                                downPaymentFee.getTotalDownpaymentChargeAmount().doubleValue(),
                                adjustDatesReviewDetails.getHoldNewDate(), accountAdjustDates, null);
                    }

                    String[] bilmasterCompanyState = readBilMstCoSt(
                            adjustDatesReviewDetails.getBilAccount().getBillClassCd(),
                            adjustDatesReviewDetails.getBilAccount().getBillTypeCd());
                    String masterCompanyNumber = bilmasterCompanyState[0];
                    String riskState = bilmasterCompanyState[1];
                    String se3Date = DateRoutine.dateTimeAsYYYYMMDDString(accountAdjustDates.getProcessDate());

                    createServiceCharge(ObjectCode.DOWN_PAYMENT_FEE.toString(), ActionCode.SCHEDULE_TO_BILL.toString(),
                            downPaymentFee.getTotalDownpaymentChargeAmount().doubleValue(), masterCompanyNumber,
                            riskState, SEPARATOR_BLANK, SEPARATOR_BLANK, BLANK_CHAR, "FSW", SEPARATOR_BLANK,
                            accountAdjustDates, adjustDatesReviewDetails.getBilAccount(), se3Date, BLANK_STRING,
                            se3Date, se3Date);
                }
            }
        }

    }

    private void suspendBillReset(AccountAdjustDates accountAdjustDates,
            AdjustDatesReviewDetails adjustDatesReviewDetails, boolean isXHOLRule, BilRulesUct bilRulesUct) {

        ZonedDateTime suspendAdjustDate = null;
        ZonedDateTime suspendInvoiceDate = null;
        List<BilDates> bilDatesList = bilDatesRepository.getMinAdjustDueDateByInvoiceCode(
                accountAdjustDates.getAccountId(), AdjustDueDatesConstants.getFutureInvoiceCodes(),
                accountAdjustDates.getCurrentDueDate(), BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
        if (bilDatesList == null || bilDatesList.isEmpty()) {
            firstPossibleDueDateService.oneDateAdd(accountAdjustDates, adjustDatesReviewDetails,
                    accountAdjustDates.getCurrentDueDate(), isXHOLRule, bilRulesUct,
                    adjustDatesReviewDetails.getBilActRules().getBruIncWknInd(),
                    adjustDatesReviewDetails.getBilSupportPlan());
        } else {
            BilDates bilDates = bilDatesList.get(SHORT_ZERO);
            suspendAdjustDate = bilDates.getBilAdjDueDt();
            suspendInvoiceDate = bilDates.getBilInvDt();
        }

        bilCrgAmountsRepository.deleteServiceChargesByDates(accountAdjustDates.getAccountId(),
                accountAdjustDates.getCurrentDueDate(), ChargeType.SERVICECHARGES.getChargeType(),
                AdjustDueDatesConstants.getFutureInvoiceCodes());
        updatePreviousSchduleRows(accountAdjustDates, suspendAdjustDate, suspendInvoiceDate);

    }

    private void updatePreviousSchduleRows(AccountAdjustDates accountAdjustDates, ZonedDateTime suspendAdjustDate,
            ZonedDateTime suspendInvoiceDate) {
        List<BilDates> bilDatesList = bilDatesRepository.getBilDatesByInvoiceCode(accountAdjustDates.getAccountId(),
                AdjustDueDatesConstants.getFutureInvoiceCodes(), accountAdjustDates.getCurrentDueDate(),
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            for (BilDates bilDates : bilDatesList) {
                bilIstScheduleRepository.updateInvoiceDateAndAdjustDateBySystemDueDate(suspendInvoiceDate,
                        suspendAdjustDate, bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilSysDueDt());
                bilCreScheduleRepository.updateInvDateAndAdjDateRowBySystemDueDate(suspendAdjustDate,
                        suspendInvoiceDate, bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilSysDueDt());
                bilCashDspRepository.updateInvDateAndAdjDateRowBySystemDueDate(suspendInvoiceDate, suspendAdjustDate,
                        bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilSysDueDt(),
                        ChargeType.DOWNPAYMENTCHARGES.getChargeType());
                processSuspendCharges(bilDates.getBilDatesId().getBilAccountId(), bilDates.getBilSysDueDt(),
                        suspendAdjustDate, suspendInvoiceDate);

            }
        }

    }

    private void processSuspendCharges(String bilAccountId, ZonedDateTime bilSysDueDt, ZonedDateTime suspendAdjustDate,
            ZonedDateTime suspendInvoiceDate) {
        ZonedDateTime adjustDate = null;
        ZonedDateTime invoiceDate = null;

        List<Object[]> maxDates = bilDatesRepository.getMaxDate(bilAccountId, bilSysDueDt,
                AdjustDueDatesConstants.getFutureInvoiceCodes(),
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
        if (maxDates != null) {
            invoiceDate = (ZonedDateTime) maxDates.get(SHORT_ZERO)[0];
            adjustDate = (ZonedDateTime) maxDates.get(SHORT_ZERO)[1];
        }

        Integer count = bilCrgAmountsRepository.updateSuspendCrgAmountsBybilAdjDueDt(suspendAdjustDate,
                suspendInvoiceDate, bilAccountId, adjustDate, invoiceDate,
                Arrays.asList(ChargeType.LATECHARGES.getChargeType(), ChargeType.PENALTYCHARGES.getChargeType()),
                AdjustDueDatesConstants.getFutureInvoiceCodes());
        if (count != null && count > SHORT_ZERO) {
            bilCashDspRepository.updateRescindCashRowByAdjDueDate(suspendAdjustDate, suspendInvoiceDate, bilAccountId,
                    adjustDate, invoiceDate,
                    Arrays.asList(ChargeType.LATECHARGES.getChargeType(), ChargeType.PENALTYCHARGES.getChargeType()));
        }
    }

    @Override
    public void processServiceWriteOff(BilAccount bilAccount, BilActRules bilActRules,
            AccountAdjustDates accountAdjustDates) {

        List<Double> lastQualServiceChargeAmounts = new ArrayList<>();
        List<Double> downpaymentAmounts = new ArrayList<>();
        List<ZonedDateTime> fullpayDatesArrays = new ArrayList<>();
        double totalCashAmount = DECIMAL_ZERO;

        Integer count = bilPolicyTermRepository.checkWriteoffPolicyExistance(bilAccount.getAccountId(), CHAR_Y);
        if (count > SHORT_ZERO) {
            boolean isFullPay = cashApplyDriverService.checkFullPayServiceWriteOff(bilActRules, totalCashAmount,
                    bilAccount.getAccountId(), bilAccount, lastQualServiceChargeAmounts, downpaymentAmounts,
                    fullpayDatesArrays);
            if (isFullPay || !downpaymentAmounts.isEmpty()) {
                char fullPayChargeTypeCode;
                double fmsScgwos;
                ZonedDateTime fullPayInvoiceDate = fullpayDatesArrays.get(SHORT_ZERO);
                if (!downpaymentAmounts.isEmpty() || lastQualServiceChargeAmounts.get(2) > DECIMAL_ZERO) {
                    fullPayChargeTypeCode = CHAR_D;
                    fmsScgwos = lastQualServiceChargeAmounts.get(2);
                } else {
                    fullPayChargeTypeCode = CHAR_S;
                    fmsScgwos = lastQualServiceChargeAmounts.get(SHORT_ZERO);
                }

                returnServiceCharge(fmsScgwos, accountAdjustDates, bilAccount, fullPayChargeTypeCode,
                        fullPayInvoiceDate);
            }
        }
    }

    private void returnServiceCharge(Double fmsScgwos, AccountAdjustDates accountAdjustDates, BilAccount bilAccount,
            char fullPayChargeTypeCode, ZonedDateTime fullPayInvoiceDate) {

        String masterCompanyNumber = BLANK_STRING;
        String riskState = BLANK_STRING;
        String objectCode = null;
        String actionCode = null;

        if (fmsScgwos <= SHORT_ZERO) {
            return;
        }
        List<BilCrgAmounts> bilCrgAmountList = bilCrgAmountsRepository
                .findBilCrgAmounts(accountAdjustDates.getAccountId(), fullPayInvoiceDate, fullPayChargeTypeCode);

        if (bilCrgAmountList != null && !bilCrgAmountList.isEmpty()) {
            BilCrgAmounts bilCrgAmounts = bilCrgAmountList.get(0);
            fullPayChargeTypeCode = bilCrgAmounts.getBilCrgTypeCd();
            short fullPaySeqNumber = bilCrgAmounts.getBilCrgAmountsId().getBilSeqNbr();
            char fullPayInvoiceCode = bilCrgAmounts.getBilInvoiceCd();
            Double fmsScgaps = bilCrgAmounts.getBcaCrgPaidAmt();
            fullPayInvoiceDate = bilCrgAmounts.getBilInvDt();

            String bilAcyDesCode = "FSW";
            if (fullPayChargeTypeCode == CHAR_D) {
                bilAcyDesCode = ObjectCode.DOWN_PAYMENT_FEE.toString();
            }
            String bilDesReasonType = "W01";
            if (isWriteHistory(accountAdjustDates.getQuoteIndicator(), accountAdjustDates.getAddChargeIndicator(),
                    accountAdjustDates.getChargeAmount())) {
                accountSummary(bilAcyDesCode, bilDesReasonType, fmsScgwos, DateRoutine.defaultDateTime(),
                        accountAdjustDates, null);
            }
            if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y
                    && accountAdjustDates.getQuoteIndicator().charAt(SHORT_ZERO) != CHAR_Q) {
                String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(),
                        bilAccount.getBillTypeCd());
                masterCompanyNumber = bilmasterCompanyState[0];
                riskState = bilmasterCompanyState[1];

                if (fullPayChargeTypeCode == CHAR_D) {
                    objectCode = ObjectCode.DOWN_PAYMENT_FEE.toString();
                    if (fullPayInvoiceCode == BLANK_CHAR || fullPayInvoiceCode == CHAR_A
                            || fullPayInvoiceCode == CHAR_B) {
                        actionCode = ActionCode.WRITE_OFF_UNBILLED.toString();
                    } else {
                        actionCode = ActionCode.WRITE_OFF_INSTALL.toString();
                    }
                } else {
                    objectCode = ObjectCode.SERVICE_CHARGE.toString();
                    actionCode = ActionCode.WRITE_OFF_SERVICE_CHARGE.toString();
                }

                String se3Date = DateRoutine.dateTimeAsYYYYMMDDString(accountAdjustDates.getProcessDate());
                createServiceCharge(objectCode, actionCode, fmsScgwos, masterCompanyNumber, riskState, SEPARATOR_BLANK,
                        SEPARATOR_BLANK, BLANK_CHAR, "FSW", SEPARATOR_BLANK, accountAdjustDates, bilAccount, se3Date,
                        se3Date, se3Date, se3Date);

            }

            if (fmsScgaps != 0) {
                BilCashReceipt bilCashReceipt = reversePaid(fmsScgaps, fullPaySeqNumber, bilAccount.getAccountId(),
                        fullPayChargeTypeCode, fullPayInvoiceDate, accountAdjustDates, bilAccount, masterCompanyNumber,
                        riskState);
                if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y
                        && accountAdjustDates.getQuoteIndicator().charAt(SHORT_ZERO) != CHAR_Q) {
                    if (bilCashReceipt == null) {
                        bilCashReceipt = new BilCashReceipt();
                    }

                    if (fullPayChargeTypeCode == CHAR_D) {
                        objectCode = ObjectCode.DOWN_PAYMENT_FEE.toString();
                        if (fullPayInvoiceCode == BLANK_CHAR || fullPayInvoiceCode == CHAR_A
                                || fullPayInvoiceCode == CHAR_B) {
                            actionCode = ActionCode.APPLY_TO_UNBILLED.toString();
                        } else {
                            actionCode = ActionCode.APPLY_TO_INSTALL.toString();
                        }
                    } else {
                        objectCode = ObjectCode.SERVICE_CHARGE.toString();
                        actionCode = ActionCode.APPLY_SERVICE_CHARGE.toString();
                    }

                    String dataBaseKey = SEPARATOR_BLANK;
                    if (!actionCode.equals(ActionCode.APPLY_TO_UNBILLED.toString())
                            || !objectCode.equals(ObjectCode.DOWN_PAYMENT_FEE.toString())) {
                        dataBaseKey = DateRoutine.dateTimeAsYYYYMMDDString(fullPayInvoiceDate);
                    }

                    String se3Date = DateRoutine.dateTimeAsYYYYMMDDString(accountAdjustDates.getProcessDate());
                    createServiceCharge(objectCode, actionCode, fmsScgaps * -1, masterCompanyNumber, riskState,
                            bilCashReceipt.getBankCd(), bilCashReceipt.getReceiptTypeCd(),
                            bilCashReceipt.getCashEntryMethodCd(), SEPARATOR_BLANK, dataBaseKey, accountAdjustDates,
                            bilAccount, se3Date, se3Date, se3Date, se3Date);
                }

            }

            bilCrgAmountsRepository.deleteServiceCharges(bilAccount.getAccountId(),
                    Arrays.asList(InvoiceTypeCode.EMPTY_CHAR.getValue(),
                            InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue(),
                            InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue()),
                    ChargeType.SERVICECHARGES.getChargeType());
            bilCrgAmountsRepository.updateWriteOffChargeAmount(DECIMAL_ZERO, bilAccount.getAccountId(),
                    Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()),
                    fullPayInvoiceDate, fullPayChargeTypeCode);
        }

    }

    private void createServiceCharge(String objectCode, String actionCode, Double fmsScgwos, String masterCompanyNumber,
            String riskState, String bilBankCode, String receiptTypeCode, char bilSourceCode, String bilReasonCode,
            String dataBaseKey, AccountAdjustDates accountAdjustDates, BilAccount bilAccount, String effectiveDate,
            String orginalEffectiveDate, String referenceDate, String postDate) {

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(accountAdjustDates.getAccountId());
        financialApiActivity.setReferenceId(SEPARATOR_BLANK);
        financialApiActivity.setApplicationName(accountAdjustDates.getApplicationName());
        financialApiActivity.setUserId(accountAdjustDates.getUserSequenceId());

        financialApiActivity.setTransactionObjectCode(objectCode);
        financialApiActivity.setTransactionActionCode(actionCode);

        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setMasterCompanyNbr(masterCompanyNumber);
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(CHAR_Y);
        financialApiActivity.setEffectiveDate(effectiveDate);
        financialApiActivity.setOperatorId(accountAdjustDates.getUserSequenceId());
        financialApiActivity.setAppProgramId(ApplicationName.BCMOAD.toString());
        financialApiActivity.setCountyCode(SEPARATOR_BLANK);
        financialApiActivity.setStateCode(riskState);
        financialApiActivity.setCountryCode(SEPARATOR_BLANK);
        financialApiActivity.setLineOfBusCode(SEPARATOR_BLANK);
        financialApiActivity.setPolicyId(SEPARATOR_BLANK);
        financialApiActivity.setAgentId(SEPARATOR_BLANK);
        financialApiActivity.setActivityAmount(fmsScgwos);
        financialApiActivity.setActivityNetAmount(fmsScgwos);
        financialApiActivity.setCurrencyCode(bilAccount.getCurrencyCode());
        financialApiActivity.setOriginalEffectiveDate(orginalEffectiveDate);
        financialApiActivity.setBilBankCode(bilBankCode);
        financialApiActivity.setReferenceDate(referenceDate);
        financialApiActivity.setBilPostDate(postDate);
        financialApiActivity.setBilAccountId(accountAdjustDates.getAccountId());
        financialApiActivity.setBilReasonCode(bilReasonCode);
        financialApiActivity.setBilReceiptTypeCode(receiptTypeCode);
        financialApiActivity.setBilTypeCode(bilAccount.getBillTypeCd());
        financialApiActivity.setBilClassCode(bilAccount.getBillClassCd());
        financialApiActivity.setPayableItemCode(SEPARATOR_BLANK);
        financialApiActivity.setBilSourceCode(String.valueOf(bilSourceCode));
        financialApiActivity.setBilDatabaseKey(dataBaseKey);
        financialApiActivity.setAgentTtyId(SEPARATOR_BLANK);

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                dateService.currentDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

    private BilCashReceipt reversePaid(Double fmsScgaps, short fullPaySeqNbr, String accountId, char fullPayCrgTypeCd,
            ZonedDateTime fullPayInvDate, AccountAdjustDates accountAdjustDates, BilAccount bilAccount,
            String masterCompanyNumber, String riskState) {

        Double totToReverse = fmsScgaps;

        if (totToReverse == 0) {
            return null;
        }

        List<BilCashDsp> bilCashDspList = bilCashDspRepository.getAdjustApplied(accountId,
                BilDspTypeCode.APPLIED.getValue(), BLANK_CHAR, BLANK_STRING, DateRoutine.defaultDateTime(),
                fullPaySeqNbr, fullPayCrgTypeCd, fullPayInvDate);
        boolean isfirstTime = true;
        BilCashReceipt bilCashReceipt = null;
        if (bilCashDspList != null && !bilCashDspList.isEmpty()) {
            for (BilCashDsp bilCashDsp : bilCashDspList) {
                boolean isIWReceipt = false;

                String suspendType = BilDspTypeCode.SUSPENDED.getValue();
                Short maxSequence = null;
                if (isfirstTime) {
                    bilCashReceipt = bilCashReceiptRepository
                            .findById(new BilCashReceiptId(bilCashDsp.getBilCashDspId().getAccountId(),
                                    bilCashDsp.getBilCashDspId().getBilDtbDate(),
                                    bilCashDsp.getBilCashDspId().getDtbSequenceNbr()))
                            .orElse(null);
                    if (bilCashReceipt == null) {
                        throw new DataNotFoundException("receipt.date.not.found", new Object[] { accountId });
                    }
                    isfirstTime = false;
                }

                if (bilCashReceipt.getReceiptTypeCd().equals(BilReceiptTypeCode.UNDERPAY_WRITEOFF.getValue())) {
                    isIWReceipt = true;
                    suspendType = BilDspTypeCode.WRITE_OFF.getValue();
                }

                String financialDBKey = getFmsCashKey(bilCashReceipt.getEntryDate(), bilCashReceipt.getEntryNumber(),
                        bilCashReceipt.getUserId(), bilCashReceipt.getEntrySequenceNumber(), PLUS_SIGN);

                if (bilCashDsp.getDspAmount() < totToReverse) {
                    totToReverse = totToReverse - bilCashDsp.getDspAmount();
                    maxSequence = totalRevert(bilCashDsp, fullPayCrgTypeCd, maxSequence,
                            accountAdjustDates.getProcessDate());
                    insertSuspend(bilCashDsp, totToReverse, suspendType, accountAdjustDates, isIWReceipt,
                            bilCashReceipt, bilAccount, financialDBKey, maxSequence,
                            accountAdjustDates.getProcessDate(), masterCompanyNumber, riskState);
                    continue;
                } else if (bilCashDsp.getDspAmount() == totToReverse) {
                    maxSequence = totalRevert(bilCashDsp, fullPayCrgTypeCd, maxSequence,
                            accountAdjustDates.getProcessDate());
                } else {
                    maxSequence = partialRevert(bilCashDsp, totToReverse, fullPayCrgTypeCd, maxSequence,
                            accountAdjustDates.getProcessDate());
                }

                insertSuspend(bilCashDsp, totToReverse, suspendType, accountAdjustDates, isIWReceipt, bilCashReceipt,
                        bilAccount, financialDBKey, maxSequence, accountAdjustDates.getProcessDate(),
                        masterCompanyNumber, riskState);
                break;
            }
        }

        return bilCashReceipt;

    }

    private Short totalRevert(BilCashDsp bilCashDsp, char fullPayCrgTypeCd, Short maxSequence, ZonedDateTime se3Date) {
        Double revAmt = bilCashDsp.getDspAmount();

        maxSequence = createAppliedHistory(bilCashDsp, revAmt, fullPayCrgTypeCd, maxSequence, se3Date);
        bilCashDspRepository.deleteById(bilCashDsp.getBilCashDspId());
        bilCashDspRepository.flush();
        return maxSequence;
    }

    private Short createAppliedHistory(BilCashDsp bilCashDsp, Double revAmt, char fullPayCrgTypeCd, Short maxSequence,
            ZonedDateTime se3Date) {
        if (maxSequence == null) {
            maxSequence = bilCashDspRepository.findMaxDispositionNumber(bilCashDsp.getBilCashDspId().getAccountId(),
                    bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr());
            if (maxSequence == null) {
                maxSequence = 0;
            } else {
                maxSequence = (short) (maxSequence + 1);
            }
        } else {
            maxSequence = (short) (maxSequence + 1);
        }

        BilCashDsp bilCashDspSave = new BilCashDsp();

        BilCashDspId bilCashDspSaveId = new BilCashDspId(bilCashDsp.getBilCashDspId().getAccountId(),
                bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr(),
                maxSequence);

        bilCashDspSave.setDspAmount(revAmt);
        bilCashDspSave.setDspDate(se3Date);
        bilCashDspSave.setUserId(SYSTEM_USER_ID);
        bilCashDspSave.setRevsRsusIndicator(CHAR_S);
        bilCashDspSave.setPayeeClientId(BLANK_STRING);
        bilCashDspSave.setPayeeAddressSequenceNumber(SHORT_ZERO);
        bilCashDspSave.setManualSuspenseIndicator(BLANK_CHAR);
        bilCashDspSave.setCreditPolicyId(BLANK_STRING);
        bilCashDspSave.setToFromTransferNbr(BLANK_STRING);
        bilCashDspSave.setStatusCd(BLANK_CHAR);
        bilCashDspSave.setDisbursementId(BLANK_STRING);
        bilCashDspSave.setCheckProdMethodCd(BLANK_CHAR);
        bilCashDspSave.setPolicySymbolCd(BLANK_STRING);
        bilCashDspSave.setPolicyNumber(BLANK_STRING);
        bilCashDspSave.setPolicyId(BLANK_STRING);
        bilCashDspSave.setPayableItemCd(BLANK_STRING);
        bilCashDspSave.setSystemDueDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setAdjustmentDueDate(bilCashDsp.getAdjustmentDueDate());
        bilCashDspSave.setUserId(bilCashDsp.getUserId());
        bilCashDspSave.setInvoiceDate(bilCashDsp.getInvoiceDate());
        bilCashDspSave.setDisbursementDate(bilCashDsp.getDisbursementDate());
        bilCashDspSave.setBilSequenceNumber(SHORT_ZERO);
        bilCashDspSave.setTransferTypeCd(BLANK_CHAR);
        bilCashDspSave.setDspReasonCd(BLANK_STRING);
        bilCashDspSave.setChargeTypeCd(fullPayCrgTypeCd);
        if (bilCashDsp.getCreditIndicator() != CHAR_P) {
            bilCashDspSave.setCreditIndicator(BLANK_CHAR);
            bilCashDspSave.setCreditAmountType(BLANK_STRING);
        } else {
            bilCashDspSave.setCreditIndicator(bilCashDsp.getCreditIndicator());
            bilCashDspSave.setCreditAmountType(bilCashDsp.getCreditAmountType());
        }
        bilCashDspSave.setBilCashDspId(bilCashDspSaveId);
        bilCashDspSave.setDspTypeCd(BilDspTypeCode.APPLIED.getValue());
        bilCashDspSave.setPolicyEffectiveDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setDraftNbr(SHORT_ZERO);
        bilCashDspRepository.saveAndFlush(bilCashDspSave);
        return maxSequence;
    }

    private void insertSuspend(BilCashDsp bilCashDsp, Double revAmt, String suspType,
            AccountAdjustDates accountAdjustDates, boolean isIWReceipt, BilCashReceipt bilCashReceipt,
            BilAccount bilAccount, String financialDBKey, Short maxSequence, ZonedDateTime se3Date,
            String masterCompanyNumber, String riskState) {
        if (maxSequence == null) {
            maxSequence = bilCashDspRepository.findMaxDispositionNumber(bilCashDsp.getBilCashDspId().getAccountId(),
                    bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr());

            if (maxSequence == null) {
                maxSequence = 0;
            } else {
                maxSequence = (short) (maxSequence + 1);
            }
        } else {
            maxSequence = (short) (maxSequence + 1);
        }

        BilCashDsp bilCashDspSave = new BilCashDsp();

        BilCashDspId bilCashDspSaveId = new BilCashDspId(bilCashDsp.getBilCashDspId().getAccountId(),
                bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr(),
                maxSequence);

        bilCashDspSave.setBilSequenceNumber(SHORT_ZERO);
        bilCashDspSave.setDspDate(se3Date);
        bilCashDspSave.setSystemDueDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setAdjustmentDueDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setInvoiceDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setUserId(SYSTEM_USER_ID);
        bilCashDspSave.setPolicyId(BLANK_STRING);
        bilCashDspSave.setDspReasonCd(BLANK_STRING);
        bilCashDspSave.setPayeeClientId(BLANK_STRING);
        bilCashDspSave.setPayeeAddressSequenceNumber(bilCashDsp.getPayeeAddressSequenceNumber());
        bilCashDspSave.setManualSuspenseIndicator(BLANK_CHAR);
        bilCashDspSave.setRevsRsusIndicator(BLANK_CHAR);
        bilCashDspSave.setPolicySymbolCd(BLANK_STRING);
        bilCashDspSave.setPolicyNumber(BLANK_STRING);
        bilCashDspSave.setPayableItemCd(BLANK_STRING);
        bilCashDspSave.setToFromTransferNbr(BLANK_STRING);
        bilCashDspSave.setDraftNbr(SHORT_ZERO);
        bilCashDspSave.setDisbursementDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setStatusCd(BLANK_CHAR);
        bilCashDspSave.setDisbursementId(BLANK_STRING);
        bilCashDspSave.setCheckProdMethodCd(BLANK_CHAR);
        bilCashDspSave.setTransferTypeCd(BLANK_CHAR);

        if (bilCashDsp.getCreditIndicator() != CHAR_P) {
            bilCashDspSave.setCreditPolicyId(BLANK_STRING);
            bilCashDspSave.setCreditIndicator(BLANK_CHAR);
            bilCashDspSave.setCreditAmountType(BLANK_STRING);
        } else {
            bilCashDspSave.setCreditPolicyId(bilCashDsp.getCreditPolicyId());
            bilCashDspSave.setCreditIndicator(bilCashDsp.getCreditIndicator());
            bilCashDspSave.setCreditAmountType(bilCashDsp.getCreditAmountType());
        }

        bilCashDspSave.setBilCashDspId(bilCashDspSaveId);
        bilCashDspSave.setDspTypeCd(suspType);
        bilCashDspSave.setDspAmount(revAmt);
        bilCashDspSave.setPolicyEffectiveDate(DateRoutine.defaultDateTime());
        bilCashDspSave.setChargeTypeCd(BLANK_CHAR);
        bilCashDspRepository.saveAndFlush(bilCashDspSave);
        accountAdjustDates.setInvoiceIndicator(CHAR_F);

        if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y
                && accountAdjustDates.getQuoteIndicator().charAt(SHORT_ZERO) != CHAR_Q) {

            String processDate = DateRoutine.dateTimeAsYYYYMMDDString(accountAdjustDates.getProcessDate());
            if (isIWReceipt) {

                createServiceCharge(ObjectCode.CASH.toString(), ActionCode.WRITE_OFF.toString(),
                        bilCashDspSave.getDspAmount(), masterCompanyNumber, riskState, bilCashReceipt.getBankCd(),
                        bilCashReceipt.getReceiptTypeCd(), bilCashReceipt.getCashEntryMethodCd(), SEPARATOR_BLANK,
                        financialDBKey, accountAdjustDates, bilAccount, processDate, SEPARATOR_BLANK,
                        DateRoutine.dateTimeAsYYYYMMDDString(bilCashDspSave.getBilCashDspId().getBilDtbDate()),
                        processDate);

            } else {

                createServiceCharge(ObjectCode.CASH.toString(), ActionCode.APPLIED.toString(),
                        bilCashDspSave.getDspAmount() * -1, masterCompanyNumber, riskState, bilCashReceipt.getBankCd(),
                        bilCashReceipt.getReceiptTypeCd(), bilCashReceipt.getCashEntryMethodCd(), SEPARATOR_BLANK,
                        financialDBKey, accountAdjustDates, bilAccount, processDate, SEPARATOR_BLANK,
                        DateRoutine.dateTimeAsYYYYMMDDString(bilCashDspSave.getBilCashDspId().getBilDtbDate()),
                        processDate);
            }
        }
    }

    private Short partialRevert(BilCashDsp bilCashDsp, Double totToReverse, char fullPayCrgTypeCd, Short maxSequence,
            ZonedDateTime se3Date) {
        bilCashDsp.setDspAmount(bilCashDsp.getDspAmount() - totToReverse);
        Double revAmt = totToReverse;
        maxSequence = createAppliedHistory(bilCashDsp, revAmt, fullPayCrgTypeCd, maxSequence, se3Date);
        bilCashDspRepository.saveAndFlush(bilCashDsp);
        return maxSequence;
    }

    private String getFmsCashKey(ZonedDateTime entryDate, String entryNumber, String operatorId,
            short bilDtbSequenceNumber, String seqNumberSign) {
        String dataString = DateRoutine.dateTimeAsYYYYMMDDString(entryDate);
        dataString += entryNumber;
        dataString += StringUtils.rightPad(operatorId, 8, BLANK_CHAR);
        dataString += seqNumberSign;
        dataString += StringUtils.leftPad(String.valueOf(bilDtbSequenceNumber), 5, CHAR_ZERO);

        return dataString;
    }

    private boolean isWriteHistory(String quoteIndicator, char addChargeIndicator, Double chargeAmount) {
        return quoteIndicator.charAt(SHORT_ZERO) != CHAR_Q
                && !((addChargeIndicator == ChargeType.PENALTYCHARGES.getChargeType()
                        || addChargeIndicator == ChargeType.LATECHARGES.getChargeType())
                        && chargeAmount == DECIMAL_ZERO);
    }

    private void accountSummary(String bilAcyDesCode, String bilDesReasonType, double bilAmounts,
            ZonedDateTime holdNewDate, AccountAdjustDates accountAdjustDates, BilAccount bilAccount) {

        ZonedDateTime acyDate1 = DateRoutine.defaultDateTime();
        ZonedDateTime acyDate2 = DateRoutine.defaultDateTime();
        Short bilAcySeq = 0;

        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountAdjustDates.getAccountId(),
                accountAdjustDates.getProcessDate());
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }

        if (accountAdjustDates.getReferenceDateIndicator() == CHAR_Y
                || accountAdjustDates.getStartDateIndicator() == CHAR_Y) {
            acyDate1 = accountAdjustDates.getCurrentDueDate();
            acyDate2 = accountAdjustDates.getNewDueDate();
            if (holdNewDate.compareTo(DateRoutine.dateTimeAsYYYYMMDD(0)) != 0) {
                acyDate2 = holdNewDate;
            }
        }

        if (bilAcyDesCode.equalsIgnoreCase(ActivityType.ECC.getValue())) {
            List<BilEftBank> bilEftBankList = bilEftBankRepository.findBilEftBankEntry(
                    accountAdjustDates.getAccountId(), CHAR_A, accountAdjustDates.getProcessDate(), 'A',
                    PageRequest.of(SHORT_ZERO, SHORT_ONE));

            if (bilEftBankList != null && !bilEftBankList.isEmpty()) {
                acyDate1 = bilEftBankList.get(SHORT_ZERO).getBilEftBankId().getBebEffectiveDt();
            }
        }

        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountAdjustDates.getAccountId(),
                accountAdjustDates.getProcessDate().with(LocalTime.MIN), bilAcySeq.shortValue());
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(BLANK_STRING);
        bilActSummary.setPolNbr(BLANK_STRING);
        bilActSummary.setBilAcyDesCd(bilAcyDesCode);
        bilActSummary.setBilDesReaTyp(bilDesReasonType);
        bilActSummary.setBilAcyDes1Dt(acyDate1);
        bilActSummary.setBilAcyDes2Dt(acyDate2);
        bilActSummary.setBilAcyAmt(bilAmounts);
        if (!accountAdjustDates.getUserSequenceId().trim().isEmpty()) {
            bilActSummary.setUserId(accountAdjustDates.getUserSequenceId());
        } else {
            bilActSummary.setUserId(SYSTEM_USER_ID);
        }

        bilActSummary.setBilAcyTs(dateService.currentDateTime());
        bilActSummary.setBasAddDataTxt(BLANK_STRING);
        if (accountAdjustDates.getSupportPlanIndicator() == CHAR_Y && bilAccount != null) {
            String additionalDataText = StringUtils.rightPad(accountAdjustDates.getOldBilClass(), 3, BLANK_CHAR)
                    + StringUtils.rightPad(bilAccount.getBillClassCd(), 3, BLANK_CHAR);
            bilActSummary.setBasAddDataTxt(additionalDataText);
        }
        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

    private String[] readBilMstCoSt(String bilClassCode, String bilTypeCode) {
        String masterCompany = BillingConstants.MASTER_COMPANY_NUMBER;
        String riskState = BLANK_STRING;
        BilMstCoSt bilMstCoSt = bilMstCoStRepository.findRow(bilClassCode, bilTypeCode, BLANK_STRING);
        if (bilMstCoSt != null) {
            masterCompany = bilMstCoSt.getMasterCompanyNbr();
            riskState = bilMstCoSt.getBilStatePvnCd();
        }
        return new String[] { masterCompany, riskState };
    }

    private void processOutOfCycleBilling(BilAccount bilAccount, AdjustDatesReviewDetails adjustDatesReviewDetails,
            AccountAdjustDates accountAdjustDates, FirstPossibleDueDate firstPossibleDueDate,
            AccountAdjustSupportData accountAdjustSupportData, ChargeVariables chargeVariables,
            VariableSeviceChargeOverride variableSeviceChargeOverride, VariableSeviceCharge variableSeviceCharge,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, int cancelCount,
            boolean isXHOLRule, BilRulesUct bilRulesUct) {
        logger.debug("Out of Cycle Billing ");

        String bilAccountId = bilAccount.getAccountId();
        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();

        List<BilDates> bilDatesOcbList = bilDatesRepository.getMinAdjustDueDateForInvoice(bilAccountId,
                DateTypeCode.OUTOFCYCLE_BILLING.getValue(), AdjustDueDatesConstants.getFutureInvoiceCodes(),
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

        if (bilDatesOcbList == null || bilDatesOcbList.isEmpty()) {
            return;
        }
        BilDates bilDates = bilDatesOcbList.get(0);
        ZonedDateTime minAdjustDueDate = bilDates.getBilAdjDueDt();
        ZonedDateTime minInvoiceDate = bilDates.getBilInvDt();

        Double ocbBalanceAmount = bilIstScheduleRepository.getBalanceAmountByAdjustDueDateInvoiceDate(bilAccountId,
                minAdjustDueDate, minInvoiceDate);
        if (ocbBalanceAmount == null) {
            ocbBalanceAmount = DECIMAL_ZERO;
        }
        if (ocbBalanceAmount < 0) {
            ocbBalanceAmount = ocbBalanceAmount * -1;
        }
        if (ocbBalanceAmount.compareTo(bilSupportPlan.getBspMinInvAmt()) >= 0) {
            return;
        }

        List<BilDates> bilDatesList = bilDatesRepository.getMinSysDueDtDatebilAdjDueDt(bilAccountId,
                DateTypeCode.SCHEDULED.getValue(), minAdjustDueDate, AdjustDueDatesConstants.getFutureInvoiceCodes(),
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

        if (bilDatesList == null || bilDatesList.isEmpty()) {
            throw new DataNotFoundException("bil.date.not.found", new Object[] { bilAccountId });
        }
        BilDates bilDatesSchedule = bilDatesList.get(0);
        ZonedDateTime newSysDate = bilDatesSchedule.getBilSysDueDt();
        ZonedDateTime newAdjDate = bilDatesSchedule.getBilAdjDueDt();
        ZonedDateTime newInvDate = bilDatesSchedule.getBilInvDt();

        firstPossibleDueDateService.updateOcbFpdd(bilAccountId, newAdjDate, newInvDate, newSysDate,
                bilDates.getBilSysDueDt());
        firstPossibleDueDateService.updatePenalty(bilAccount, accountAdjustDates, bilSupportPlan, variableSeviceCharge,
                chargeVariables, cancelCount, newAdjDate, newInvDate, variableSeviceChargeOverride,
                adjustDatesReviewDetails, accountAdjustSupportData, bilEftPlanChargeList, bilSupportPlanChargeList,
                isXHOLRule, bilRulesUct, firstPossibleDueDate);
    }

    private boolean readManualReinvoice(AccountAdjustDates accountAdjustDates) {
        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(accountAdjustDates.getAccountId(), "MRI")).orElse(null);
        return bilActInquiry != null;
    }

    private ZonedDateTime processManualReinvoice(AccountAdjustDates accountAdjustDates) {
        ZonedDateTime minimumMriDueDt = bilDatesRepository.getMinAdjDueDate(accountAdjustDates.getAccountId(),
                accountAdjustDates.getProcessDate(), InvoiceTypeCode.EMPTY_CHAR.getValue(),
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
        if (minimumMriDueDt == null) {
            minimumMriDueDt = DateRoutine.defaultDateTime();
        }

        return minimumMriDueDt;

    }

    private boolean cancelManualReinvoice(AccountAdjustDates accountAdjustDates, ZonedDateTime holdNewDate) {

        bilActInquiryRepository.deleteManualReInvoiceRows(accountAdjustDates.getAccountId(), "MRI");

        String bilAcyDesCode = "REP";
        String bilDesReasonType = "MRI";
        double bilAmounts = DECIMAL_ZERO;

        if (isWriteHistory(accountAdjustDates.getQuoteIndicator(), accountAdjustDates.getAddChargeIndicator(),
                accountAdjustDates.getChargeAmount())) {
            accountSummary(bilAcyDesCode, bilDesReasonType, bilAmounts, holdNewDate, accountAdjustDates, null);
        }

        return false;
    }

    private boolean getOverrideWeekendIndicator(boolean excludeWeekend, char includeWeekend) {
        if (excludeWeekend && includeWeekend == CHAR_Y) {
            return false;
        }
        return excludeWeekend;

    }

    private void setServiceChargeInvoive(FirstPossibleDueDate firstPossibleDueDate, BilAccount bilAccount,
            AdjustDatesReviewDetails adjustDatesReviewDetails, AccountAdjustDates accountAdjustDates,
            VariableSeviceChargeOverride variableSeviceChargeOverride, VariableSeviceCharge variableSeviceCharge,
            ChargeVariables chargeVariables, List<BilEftPlnCrg> bilEftPlanChargeList,
            List<BilSptPlnCrg> bilSupportPlanChargeList, boolean isXHOLRule, BilRulesUct bilRulesUct,
            AccountAdjustSupportData accountAdjustSupportData) {

        ZonedDateTime oldAdjustDate = DateRoutine.dateTimeAsYYYYMMDD(SHORT_ZERO);

        char bilType = CHAR_N;
        if (!bilAccount.getBillTypeCd().equals("SP")) {
            Character invoiceCode = bilDatesRepository.getMaxInvoicedCode(bilAccount.getAccountId(), CHAR_X);
            if (invoiceCode != null && invoiceCode == CHAR_Y) {
                bilType = CHAR_I;
            }
        }

        if (bilType == CHAR_I) {

            while (oldAdjustDate != null) {

                oldAdjustDate = bilDatesRepository.getAdjustDueDateByBillInvoiceCd(bilAccount.getAccountId(),
                        oldAdjustDate, AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
                if (oldAdjustDate != null) {
                    ZonedDateTime bilInvoiceDate = bilDatesRepository.getMinInvDateByAdjDueDt(bilAccount.getAccountId(),
                            oldAdjustDate, AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
                    processServiceChargeRows(oldAdjustDate, bilInvoiceDate, bilAccount, firstPossibleDueDate,
                            adjustDatesReviewDetails, accountAdjustDates, isXHOLRule, bilRulesUct, variableSeviceCharge,
                            variableSeviceChargeOverride, chargeVariables, bilEftPlanChargeList,
                            bilSupportPlanChargeList, accountAdjustSupportData);
                }

            }
            return;
        }

        List<ZonedDateTime> bilAdjustDueDateList = bilDatesRepository.fetchDistinctAdjustDate(bilAccount.getAccountId(),
                AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);

        for (ZonedDateTime bilAdjustDate : bilAdjustDueDateList) {
            ZonedDateTime bilInvoiceDate = bilDatesRepository.getMinInvoicedDateByAdjustDate(bilAccount.getAccountId(),
                    bilAdjustDate, AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
            if (bilInvoiceDate == null) {
                throw new DataNotFoundException("bil.invoice.date.error");
            }
            processServiceChargeRows(bilAdjustDate, bilInvoiceDate, bilAccount, firstPossibleDueDate,
                    adjustDatesReviewDetails, accountAdjustDates, isXHOLRule, bilRulesUct, variableSeviceCharge,
                    variableSeviceChargeOverride, chargeVariables, bilEftPlanChargeList, bilSupportPlanChargeList,
                    accountAdjustSupportData);

        }

    }

    private void processServiceChargeRows(ZonedDateTime bilAdjustDate, ZonedDateTime bilInvoiceDate,
            BilAccount bilAccount, FirstPossibleDueDate firstPossibleDueDate,
            AdjustDatesReviewDetails adjustDatesReviewDetails, AccountAdjustDates accountAdjustDates,
            boolean isXHOLRule, BilRulesUct bilRulesUct, VariableSeviceCharge variableSeviceCharge,
            VariableSeviceChargeOverride variableSeviceChargeOverride, ChargeVariables chargeVariables,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList,
            AccountAdjustSupportData accountAdjustSupportData) {

        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();
        double chargeAmount = accountAdjustSupportData.getServiceChargeAmount();
        char newInvoiceIndicator = BLANK_CHAR;
        Object[] dateTypeObject = firstPossibleDueDateService.getDateType(bilAccount.getAccountId(), bilAdjustDate,
                bilInvoiceDate);
        logger.debug("Retrieved Max BilDateType and Min BilDateInd by Object type");
        char bilDateType = (char) dateTypeObject[0];

        Double balanceAmounts = firstPossibleDueDateService.getBalance(bilAccount.getAccountId(), bilAdjustDate,
                bilInvoiceDate);

        if ((bilDateType == DateTypeCode.SCHEDULED.getValue()
                || bilDateType == DateTypeCode.FIRSTPOSSIBLEDUEDATE_SINGLEPOLICYCANCELLATIONINVOICE.getValue()
                || bilDateType == DateTypeCode.FIRST_POSSIBLEDUE_LATE.getValue()
                || bilDateType == DateTypeCode.OUTOFCYCLE_BILLING.getValue())
                && balanceAmounts < bilSupportPlan.getBspMinInvAmt() || balanceAmounts == DECIMAL_ZERO) {
            newInvoiceIndicator = InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue();
            chargeAmount = DECIMAL_ZERO;
        }

        if (firstPossibleDueDate.isFpddSet() && bilAdjustDate.compareTo(firstPossibleDueDate.getLookAheadDate()) < 0
                || !firstPossibleDueDate.isFpddSet() && bilDateType == CHAR_L && newInvoiceIndicator == BLANK_CHAR
                        && bilAdjustDate.compareTo(firstPossibleDueDate.getLookAheadDate()) <= 0
                || newInvoiceIndicator == BLANK_CHAR && bilAdjustDate.compareTo(firstPossibleDueDate.getDate()) <= 0) {

            if (firstPossibleDueDate.isFpddSet()) {
                newInvoiceIndicator = BLANK_CHAR;
            }

            Object[] updateForFpdd = firstPossibleDueDateService.updateForFpdd(bilAccount.getAccountId(), bilDateType,
                    bilSupportPlan, bilAccount, BLANK_CHAR, bilAdjustDate, bilInvoiceDate, firstPossibleDueDate,
                    adjustDatesReviewDetails, accountAdjustDates, isXHOLRule, bilRulesUct);

            boolean isOverlapNextAcct = (boolean) updateForFpdd[0];
            char fpinvSetIndicator = (char) updateForFpdd[1];
            ZonedDateTime checkDate = (ZonedDateTime) updateForFpdd[2];
            ZonedDateTime npInvoiceDueDate = (ZonedDateTime) updateForFpdd[3];

            if (isOverlapNextAcct) {
                return;
            }

            if (fpinvSetIndicator == CHAR_Y) {
                bilAdjustDate = checkDate;
                bilInvoiceDate = npInvoiceDueDate;
            } else {
                bilAdjustDate = firstPossibleDueDate.getDate();
                bilInvoiceDate = firstPossibleDueDate.getInvoiceDate();
            }

            if (accountAdjustDates.getBillPlanChangeIndicator() == CHAR_Y) {
                return;
            }
            if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y) {

                firstPossibleDueDateService.readServiceAmount(adjustDatesReviewDetails, bilAccount,
                        variableSeviceCharge, bilAdjustDate, chargeVariables, bilEftPlanChargeList,
                        bilSupportPlanChargeList, newInvoiceIndicator, accountAdjustSupportData, accountAdjustDates,
                        variableSeviceChargeOverride);

                if (variableSeviceChargeOverride.getRuleInd() == CHAR_Y) {
                    firstPossibleDueDateService.getServiceChargeTermAmount(variableSeviceChargeOverride, bilAdjustDate,
                            bilAccount.getAccountId(), variableSeviceCharge, accountAdjustSupportData);
                } else if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                        && accountAdjustSupportData.getServiceChargeIndicator() == CHAR_Y
                        && accountAdjustSupportData.getServiceChargeTypeCode().equalsIgnoreCase("GA")
                        && variableSeviceCharge.getCancelResetIndicator() == CHAR_Y) {
                    firstPossibleDueDateService.getServiceChargePerTerm(accountAdjustDates.getQuoteIndicator(),
                            variableSeviceCharge, bilAdjustDate, accountAdjustSupportData, bilAccount.getAccountId());

                }

                chargeAmount = accountAdjustSupportData.getServiceChargeAmount();

                firstPossibleDueDateService.insertUpdateChargeAmounts(bilAccount.getAccountId(), bilAdjustDate,
                        bilInvoiceDate, chargeAmount, newInvoiceIndicator, bilAccount.getBillTypeCd(), bilDateType,
                        adjustDatesReviewDetails, accountAdjustSupportData,
                        accountAdjustDates.getProcessDate(), accountAdjustDates.getUserSequenceId());

                return;
            }
            if (newInvoiceIndicator == BLANK_CHAR && bilAdjustDate.compareTo(firstPossibleDueDate.getDate()) <= 0) {
                if (bilDateType != CHAR_F || bilDateType != CHAR_O) {
                    firstPossibleDueDate.setFpddSet(true);
                    return;
                } else {
                    return;
                }
            }
        }

        if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y) {

            firstPossibleDueDateService.readServiceAmount(adjustDatesReviewDetails, bilAccount, variableSeviceCharge,
                    bilAdjustDate, chargeVariables, bilEftPlanChargeList, bilSupportPlanChargeList, newInvoiceIndicator,
                    accountAdjustSupportData, accountAdjustDates, variableSeviceChargeOverride);

            if (variableSeviceChargeOverride.getRuleInd() == CHAR_Y) {
                firstPossibleDueDateService.getServiceChargeTermAmount(variableSeviceChargeOverride, bilAdjustDate,
                        bilAccount.getAccountId(), variableSeviceCharge, accountAdjustSupportData);
            } else if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                    && accountAdjustSupportData.getServiceChargeIndicator() == CHAR_Y
                    && accountAdjustSupportData.getServiceChargeTypeCode().equalsIgnoreCase("GA")
                    && variableSeviceCharge.getCancelResetIndicator() == CHAR_Y) {
                firstPossibleDueDateService.getServiceChargePerTerm(accountAdjustDates.getQuoteIndicator(),
                        variableSeviceCharge, bilAdjustDate, accountAdjustSupportData, bilAccount.getAccountId());
                chargeAmount = accountAdjustSupportData.getServiceChargeAmount();

            }

        }

        firstPossibleDueDateService.updateInvoiceIndicator(bilAccount.getAccountId(), newInvoiceIndicator,
                bilAdjustDate, bilInvoiceDate);

        firstPossibleDueDateService.insertUpdateChargeAmounts(bilAccount.getAccountId(), bilAdjustDate, bilInvoiceDate,
                chargeAmount, newInvoiceIndicator, bilAccount.getBillTypeCd(), bilDateType, adjustDatesReviewDetails,
                accountAdjustSupportData, accountAdjustDates.getProcessDate(), accountAdjustDates.getUserSequenceId());

    }

    private void xxxxSetupDiMinReset(AccountAdjustDates accountAdjustDates,
            AdjustDatesReviewDetails adjustDatesReviewDetails, boolean isXHOLRule, BilRulesUct bilRulesUct) {

        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();

        ZonedDateTime testDate = bilDatesRepository.getMinSysDueDtByBilInvDt(accountAdjustDates.getAccountId(),
                accountAdjustDates.getProcessDate(), AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
        if (testDate == null) {
            throw new DataNotFoundException("no.test.data ", new Object[] { accountAdjustDates.getAccountId() });
        }

        Double balance = bilIstScheduleRepository.getSystemDueAmount(accountAdjustDates.getAccountId(),
                AdjustDueDatesConstants.getFutureInvoiceCodes(), testDate);
        if (balance == null) {
            balance = DECIMAL_ZERO;
        }

        Double chargeAmount = bilCrgAmountsRepository.getOutstandChargeBalanceAmount(accountAdjustDates.getAccountId(),
                AdjustDueDatesConstants.getFutureInvoiceCodes(), AdjustDueDatesConstants.getChargeTypes(), testDate);
        if (chargeAmount == null) {
            chargeAmount = DECIMAL_ZERO;
        }
        balance += chargeAmount;

        if (balance < bilSupportPlan.getBspMinInvAmt()) {
            List<BilDates> bilDatesList = bilDatesRepository.fetchRescindDates(accountAdjustDates.getAccountId(),
                    AdjustDueDatesConstants.getFutureInvoiceCodes(), accountAdjustDates.getProcessDate(), CHAR_X);
            if (bilDatesList != null && !bilDatesList.isEmpty()) {
                for (BilDates bilDates : bilDatesList) {
                    balance = bilIstScheduleRepository.findBalanceByAccountIdSysDueDt(accountAdjustDates.getAccountId(),
                            bilDates.getBilSysDueDt());
                    if (balance == null) {
                        balance = DECIMAL_ZERO;
                    }

                    chargeAmount = bilCrgAmountsRepository.getOutstandChargeBalanceAmount(
                            accountAdjustDates.getAccountId(), AdjustDueDatesConstants.getFutureInvoiceCodes(),
                            AdjustDueDatesConstants.getChargeTypes(), testDate);
                    if (chargeAmount == null) {
                        chargeAmount = DECIMAL_ZERO;
                    }
                    balance += chargeAmount;
                    if (balance < bilSupportPlan.getBspMinInvAmt()) {
                        ZonedDateTime invoiceDate = firstPossibleDueDateService.getFpddInvoiceDate(
                                bilDates.getBilSysDueDt(), accountAdjustDates.getProcessDate(),
                                adjustDatesReviewDetails.getBilActRules().getBruIncWknInd(),
                                adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, bilRulesUct);
                        bilIstScheduleRepository.updateRescindScheduleRowByInvoiceCode(bilDates.getBilSysDueDt(),
                                invoiceDate, accountAdjustDates.getAccountId(), bilDates.getBilSysDueDt(),
                                AdjustDueDatesConstants.getFutureInvoiceCodes());
                        bilCreScheduleRepository.updateRescindCreScheduleRowBySystemDate(bilDates.getBilSysDueDt(),
                                invoiceDate, accountAdjustDates.getAccountId(), bilDates.getBilSysDueDt(),
                                Arrays.asList('2', '4'));
                        bilCashDspRepository.updateRescindCashRowBySysDate(bilDates.getBilSysDueDt(), invoiceDate,
                                accountAdjustDates.getAccountId(), bilDates.getBilSysDueDt(),
                                ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue());
                        bilDates.setBilAdjDueDt(bilDates.getBilSysDueDt());
                        bilDates.setBilInvDt(invoiceDate);
                        bilDates.setBdtDateInd(CHAR_N);
                        bilDatesRepository.saveAndFlush(bilDates);
                    }
                }
            }
        }

    }

    private void checkEftPastRef(AccountAdjustDates accountAdjustDates) {

        List<BilDates> bilDatesList = bilDatesRepository.findEftPastReferenceData(accountAdjustDates.getAccountId(),
                accountAdjustDates.getProcessDate(), accountAdjustDates.getProcessDate(),
                AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            for (BilDates bilDates : bilDatesList) {
                bilCreScheduleRepository.updateInvDateAndAdjDateRowBySystemDueDate(accountAdjustDates.getProcessDate(),
                        accountAdjustDates.getProcessDate(), bilDates.getBilDatesId().getBilAccountId(),
                        bilDates.getBilSysDueDt());
                bilCashDspRepository.updateInvDateAndAdjDateRowBySystemDueDate(accountAdjustDates.getProcessDate(),
                        accountAdjustDates.getProcessDate(), bilDates.getBilDatesId().getBilAccountId(),
                        bilDates.getBilSysDueDt());
                bilIstScheduleRepository.updateEftPastReferenceData(accountAdjustDates.getProcessDate(),
                        accountAdjustDates.getProcessDate(), bilDates.getBilDatesId().getBilAccountId(),
                        bilDates.getBilDatesId().getBilReferenceDt());
                bilCrgAmountsRepository.updateEftPastReferenceData(accountAdjustDates.getProcessDate(),
                        accountAdjustDates.getProcessDate(), BLANK_CHAR, bilDates.getBilDatesId().getBilAccountId(),
                        bilDates.getBilAdjDueDt(),
                        Arrays.asList(ChargeType.LATECHARGES.getChargeType(), ChargeType.PENALTYCHARGES.getChargeType(),
                                ChargeType.DOWNPAYMENTCHARGES.getChargeType()));
                bilDates.setBilAdjDueDt(accountAdjustDates.getProcessDate());
                bilDates.setBilInvDt(accountAdjustDates.getProcessDate());
                bilDates.setBilInvoiceCd(BLANK_CHAR);
                bilDatesRepository.saveAndFlush(bilDates);
            }

        }
    }

    private void getDatesInSync(ZonedDateTime adjustdueDate, AccountAdjustDates accountAdjustDates,
            AdjustDatesReviewDetails adjustDatesReviewDetails, BilAccount bilAccount, ChargeVariables chargeVariables,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList,
            VariableSeviceChargeOverride variableSeviceChargeOverride, VariableSeviceCharge variableSeviceCharge,
            AccountAdjustSupportData accountAdjustSupportData) {

        ZonedDateTime invoiceDate = bilDatesRepository.getMinInvDateByAdjDueDt(bilAccount.getAccountId(), adjustdueDate,
                AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_X);
        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();
        char newInvoiceIndicator = BLANK_CHAR;
        double chargeAmount = accountAdjustSupportData.getServiceChargeAmount();

        Object[] dateTypeObject = firstPossibleDueDateService.getDateType(bilAccount.getAccountId(), adjustdueDate,
                invoiceDate);
        logger.debug("Retrieved Max BilDateType and Min BilDateInd by Object type");
        char bilDateType = (char) dateTypeObject[0];

        Double balanceAmounts = firstPossibleDueDateService.getBalance(bilAccount.getAccountId(), adjustdueDate,
                invoiceDate);

        if ((bilDateType == DateTypeCode.SCHEDULED.getValue()
                || bilDateType == DateTypeCode.FIRSTPOSSIBLEDUEDATE_SINGLEPOLICYCANCELLATIONINVOICE.getValue()
                || bilDateType == DateTypeCode.FIRST_POSSIBLEDUE_LATE.getValue()
                || bilDateType == DateTypeCode.OUTOFCYCLE_BILLING.getValue())
                && balanceAmounts < bilSupportPlan.getBspMinInvAmt() || balanceAmounts == DECIMAL_ZERO) {
            newInvoiceIndicator = InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue();
            chargeAmount = DECIMAL_ZERO;
        }

        if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y && newInvoiceIndicator == BLANK_CHAR
                && bilDateType != CHAR_O) {

            firstPossibleDueDateService.readServiceAmount(adjustDatesReviewDetails, bilAccount, variableSeviceCharge,
                    adjustdueDate, chargeVariables, bilEftPlanChargeList, bilSupportPlanChargeList, newInvoiceIndicator,
                    accountAdjustSupportData, accountAdjustDates, variableSeviceChargeOverride);

            if (variableSeviceChargeOverride.getRuleInd() == CHAR_Y) {
                firstPossibleDueDateService.getServiceChargeTermAmount(variableSeviceChargeOverride, adjustdueDate,
                        bilAccount.getAccountId(), variableSeviceCharge, accountAdjustSupportData);
            } else if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                    && accountAdjustSupportData.getServiceChargeIndicator() == CHAR_Y
                    && accountAdjustSupportData.getServiceChargeTypeCode().equalsIgnoreCase("GA")
                    && variableSeviceCharge.getCancelResetIndicator() == CHAR_Y) {
                firstPossibleDueDateService.getServiceChargePerTerm(accountAdjustDates.getQuoteIndicator(),
                        variableSeviceCharge, adjustdueDate, accountAdjustSupportData, bilAccount.getAccountId());

            }
            chargeAmount = accountAdjustSupportData.getServiceChargeAmount();

        }

        firstPossibleDueDateService.updateInvoiceIndicator(bilAccount.getAccountId(), newInvoiceIndicator,
                adjustdueDate, invoiceDate);

        if (bilDateType != 'O' && accountAdjustSupportData.getServiceChargeAmount() > DECIMAL_ZERO) {

            firstPossibleDueDateService.insertUpdateChargeAmounts(bilAccount.getAccountId(), adjustdueDate, invoiceDate,
                    chargeAmount, newInvoiceIndicator, bilAccount.getBillTypeCd(), bilDateType,
                    adjustDatesReviewDetails, accountAdjustSupportData, accountAdjustDates.getProcessDate(),
                    accountAdjustDates.getUserSequenceId());

        }

    }

    private void assignProcess(BilAccount bilAccount, ZonedDateTime fpddDate) {

        String accountId = bilAccount.getAccountId();
        List<Object[]> bilIstScheduleList = bilIstScheduleRepository.getBilIstScheduleAndPolicyTerm(accountId,
                DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime());
        ZonedDateTime nextUninvRfrDate = null;
        ZonedDateTime rfrDtMinus1Freq = null;
        ZonedDateTime calCuLateIstDate = null;
        ZonedDateTime calCuLateRefDate = null;
        boolean isEndOfmonth = false;
        boolean isIstEndOfmonth = false;
        ZonedDateTime newRefDate = null;
        ZonedDateTime newAdjustDate = null;
        ZonedDateTime newInvoiceDate = null;
        ZonedDateTime newSysDate = null;
        ZonedDateTime startDate = null;

        for (Object[] bilIstScheduleObject : bilIstScheduleList) {

            ZonedDateTime istDueDate = (ZonedDateTime) bilIstScheduleObject[0];
            String planCode = (String) bilIstScheduleObject[1];
            String policyId = (String) bilIstScheduleObject[2];
            ZonedDateTime polEffectiveDate = (ZonedDateTime) bilIstScheduleObject[3];

            BilBillPlan bilBilPlan = bilBillPlanRepository.findById(planCode).orElse(null);
            if (bilBilPlan == null) {
                throw new DataNotFoundException("invalid.term.paid.date",
                        new Object[] { "BilBillPlan", "BBP_SPREAD_IND", accountId, policyId,
                                DateRoutine.dateTimeAsMMDDYYYYAsString(polEffectiveDate) });
            }

            if (bilBilPlan.getBbpBillFqyCd().equals("SM")) {
                DateRoutine.calculateDateTimeByFrequency("SM", polEffectiveDate);
            }
            calCuLateIstDate = istDueDate;
            startDate = istDueDate;
            Integer endDayOfMonthEffDate = polEffectiveDate.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth();

            int startDay = polEffectiveDate.getDayOfMonth();
            if (startDay == endDayOfMonthEffDate) {
                isEndOfmonth = true;
                startDate = polEffectiveDate;
            }

            Integer endDayOfMonthDueDate = istDueDate.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth();

            startDay = istDueDate.getDayOfMonth();
            int startMonth = istDueDate.getMonthValue();
            if (startDay == endDayOfMonthDueDate) {
                isIstEndOfmonth = true;
                startDate = istDueDate;
            }
            if (bilBilPlan.getBbpBillFqyCd().equals("SM") || bilBilPlan.getBbpBillFqyCd().equals("WK")
                    || bilBilPlan.getBbpBillFqyCd().equals("BW")) {
                calCuLateIstDate = istDueDate;
            } else {
                if (isEndOfmonth && isIstEndOfmonth && bilAccount.getReferenceLastDay() == '1') {
                    calCuLateIstDate = istDueDate;
                } else {
                    if (isEndOfmonth && bilAccount.getReferenceLastDay() == BLANK_CHAR && startMonth != 2) {
                        calCuLateIstDate = istDueDate;
                    } else {
                        if (isEndOfmonth && startMonth == 2) {
                            if (bilAccount.getReferenceLastDay() == BLANK_CHAR) {
                                // xxxxEomFebAndSpace()
                                if (startDay == 28) {
                                    if (bilAccount.getStartReferenceDate().getDayOfMonth() >= 3) {
                                        calCuLateIstDate = DateRoutine.getAdjustedDate(startDate, false, 3, null);
                                    } else {
                                        if (bilAccount.getStartReferenceDate().getDayOfMonth() == 2) {
                                            calCuLateIstDate = DateRoutine.getAdjustedDate(startDate, false, 2, null);
                                        } else if (bilAccount.getStartReferenceDate().getMonthValue() == 1) {
                                            calCuLateIstDate = DateRoutine.getAdjustedDate(startDate, false, 1, null);
                                        }
                                    }
                                }
                                if (startDay == 29) {
                                    if (bilAccount.getStartReferenceDate().getDayOfMonth() >= 2) {
                                        calCuLateIstDate = DateRoutine.getAdjustedDate(startDate, false, 2, null);
                                    } else if (bilAccount.getStartReferenceDate().getDayOfMonth() == 1) {
                                        calCuLateIstDate = DateRoutine.getAdjustedDate(startDate, false, 1, null);
                                    }
                                } // end xxxxEomFebAndSpace()
                            } else {
                                if (bilAccount.getReferenceLastDay() == '0'
                                        || bilAccount.getReferenceLastDay() == '9') {
                                    calCuLateIstDate = DateRoutine.getAdjustedDate(startDate, false, 2, null);
                                }
                            }
                        } else {
                            if (isEndOfmonth && isIstEndOfmonth && bilAccount.getReferenceLastDay() != '1') {
                                if (startDay == 31) {
                                    calCuLateIstDate = istDueDate;
                                }
                                if (startDay == 30) {
                                    calCuLateIstDate = DateRoutine.getAdjustedDate(startDate, false, 1, null);
                                }
                                if (startDay == 29) {
                                    calCuLateIstDate = DateRoutine.getAdjustedDate(startDate, false, 2, null);
                                }
                                if (startDay == 28) {
                                    calCuLateIstDate = DateRoutine.getAdjustedDate(startDate, false, 3, null);
                                }

                            }
                        }
                    }
                }
            }
            if (!isEndOfmonth && bilAccount.getReferenceLastDay() == '1') {
                calCuLateIstDate = istDueDate;
            }
            if ((!bilBilPlan.getBbpBillFqyCd().equals("SM") && !bilBilPlan.getBbpBillFqyCd().equals("WK")
                    && !bilBilPlan.getBbpBillFqyCd().equals("BW"))
                    && (!isEndOfmonth && startMonth == 2 && (startDay == 28 || startDay == 29))) {
                if (bilAccount.getReferenceLastDay() == '9'
                        && polEffectiveDate.compareTo(bilAccount.getStartDeductionReferenceDate()) > 0) {
                    calCuLateIstDate = DateRoutine.getAdjustedDate(startDate, false, 2, null);
                } else if (bilAccount.getReferenceLastDay() == BLANK_CHAR
                        && polEffectiveDate.compareTo(bilAccount.getStartDeductionReferenceDate()) > 0) {
                    calCuLateIstDate = DateRoutine.getAdjustedDate(startDate, false, 1, null);
                }

            }
            if (bilBilPlan.getBbpBillFqyCd().equals("SM")) {
                calCuLateIstDate = DateRoutine.getAdjustedDate(startDate, false, 1, null);
            }
            newRefDate = bilDatesRepository.getMinReferenceDateForAssignProcess(accountId, calCuLateIstDate,
                    AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_F, CHAR_X);
            if (newRefDate == null) {
                throw new DataNotFoundException("data.not.found",
                        new Object[] { accountId, DateRoutine.dateTimeAsMMDDYYYYAsString(calCuLateIstDate) });
            }
            if (!isEndOfmonth && bilAccount.getReferenceLastDay() == '1' && !bilBilPlan.getBbpBillFqyCd().equals("SM")
                    && !bilBilPlan.getBbpBillFqyCd().equals("WK") && !bilBilPlan.getBbpBillFqyCd().equals("BW")) {
                int nowDays = newRefDate.getDayOfMonth() - startDay;
                ZonedDateTime calCuLateDate = DateRoutine.getAdjustedDate(newRefDate, false, nowDays, null);
                startDay = calCuLateDate.getDayOfMonth();
                calCuLateRefDate = newRefDate;
                if (startDay == 31) {
                    calCuLateRefDate = newRefDate;
                }
                if (startDay == 30) {
                    calCuLateRefDate = DateRoutine.getAdjustedDate(newRefDate, false, 1, null);
                }
                if (startDay == 29) {
                    calCuLateRefDate = DateRoutine.getAdjustedDate(newRefDate, false, 2, null);
                }
                if (startDay == 28) {
                    calCuLateRefDate = DateRoutine.getAdjustedDate(newRefDate, false, 3, null);
                }
                if (calCuLateRefDate.compareTo(calCuLateIstDate) < 0) {
                    newRefDate = bilDatesRepository.getMinReferenceDateForCheckRefDate(accountId, calCuLateIstDate,
                            calCuLateRefDate, AdjustDueDatesConstants.getFutureInvoiceCodes(), CHAR_F, CHAR_X);
                }
            }

            BilDates bilDates = bilDatesRepository.getBilDatesByReferenceDt(accountId,
                    AdjustDueDatesConstants.getFutureInvoiceCodes(), newRefDate, CHAR_F, CHAR_X);
            if (bilDates == null) {
                throw new DataNotFoundException("data.not.found.for.account", new Object[] { accountId });
            }
            newAdjustDate = bilDates.getBilAdjDueDt();
            newInvoiceDate = bilDates.getBilInvDt();
            newSysDate = bilDates.getBilSysDueDt();
            char newDateType = bilDates.getBdtDateType();
            if (newDateType == CHAR_I
                    && (nextUninvRfrDate == null || nextUninvRfrDate != DateRoutine.defaultDateTime())) {
                Object[] assignCheckData = fpddAssignCheck(accountId, nextUninvRfrDate, rfrDtMinus1Freq, newRefDate,
                        calCuLateIstDate, newAdjustDate, newInvoiceDate, newSysDate, newDateType);
                nextUninvRfrDate = (ZonedDateTime) assignCheckData[0];
                newRefDate = (ZonedDateTime) assignCheckData[1];
                newAdjustDate = (ZonedDateTime) assignCheckData[2];
                newSysDate = (ZonedDateTime) assignCheckData[3];
                newInvoiceDate = (ZonedDateTime) assignCheckData[4];
                newDateType = (char) assignCheckData[5];
                rfrDtMinus1Freq = (ZonedDateTime) assignCheckData[6];

            }
            if (newDateType == CHAR_I && newAdjustDate.compareTo(fpddDate) > 0) {
                Object[] fpddBdtData = fpddBdtCheck(accountId, newRefDate, newAdjustDate, newInvoiceDate, newSysDate,
                        newDateType, fpddDate);
                newRefDate = (ZonedDateTime) fpddBdtData[0];
                newAdjustDate = (ZonedDateTime) fpddBdtData[1];
                newSysDate = (ZonedDateTime) fpddBdtData[2];
                newInvoiceDate = (ZonedDateTime) fpddBdtData[4];
            }
            assignSchedule(accountId, newSysDate, newRefDate, newAdjustDate, newInvoiceDate, istDueDate);
            assignCharge(accountId, newAdjustDate, newInvoiceDate);

        }
    }

    private Object[] fpddAssignCheck(String accountId, ZonedDateTime nextUninvRfrDate, ZonedDateTime rfrDtMinus1Freq,
            ZonedDateTime newRefDate, ZonedDateTime calCulateIstDate, ZonedDateTime newAdjustDate,
            ZonedDateTime newInvoiceDate, ZonedDateTime newSysDate, char newDateType) {

        ZonedDateTime nextUninvAdjDt = null;
        ZonedDateTime nextUninvSysDt = null;
        char nextUninvDateType = BLANK_CHAR;
        ZonedDateTime nextUninvInvDt = null;

        if (nextUninvRfrDate == null) {
            List<BilDates> bilDatesList = bilDatesRepository.getAdjDueDateAndInvoiceDate(accountId,
                    AdjustDueDatesConstants.getFutureInvoiceCodes(), Arrays.asList(CHAR_S), CHAR_X);
            if (bilDatesList != null && !bilDatesList.isEmpty()) {
                BilDates bilDates = bilDatesList.get(SHORT_ZERO);
                nextUninvRfrDate = bilDates.getBilDatesId().getBilReferenceDt();
                nextUninvAdjDt = bilDates.getBilAdjDueDt();
                nextUninvSysDt = bilDates.getBilSysDueDt();
                nextUninvDateType = bilDates.getBdtDateType();
                nextUninvInvDt = bilDates.getBilInvDt();
            } else {
                nextUninvRfrDate = DateRoutine.defaultDateTime();
            }
        }

        if (nextUninvRfrDate == DateRoutine.defaultDateTime()) {
            return new Object[] { nextUninvRfrDate, newRefDate, nextUninvAdjDt, nextUninvSysDt, nextUninvInvDt,
                    nextUninvDateType, rfrDtMinus1Freq };
        }
        if (rfrDtMinus1Freq == null && nextUninvRfrDate != null) {
            rfrDtMinus1Freq = DateRoutine.calculateDateTimeBySubtractFrequency("SM", nextUninvRfrDate);
        }
        if (newRefDate.compareTo(nextUninvRfrDate) < 0 && calCulateIstDate.compareTo(nextUninvRfrDate) <= 0
                && calCulateIstDate.compareTo(rfrDtMinus1Freq) > 0) {
            newRefDate = nextUninvRfrDate;
            newAdjustDate = nextUninvAdjDt;
            newSysDate = nextUninvSysDt;
            newDateType = nextUninvDateType;
            newInvoiceDate = nextUninvInvDt;
        }

        return new Object[] { nextUninvRfrDate, newRefDate, newAdjustDate, newSysDate, newInvoiceDate, newDateType,
                rfrDtMinus1Freq };
    }

    private Object[] fpddBdtCheck(String accountId, ZonedDateTime newRefDate, ZonedDateTime newAdjustDate,
            ZonedDateTime newInvoiceDate, ZonedDateTime newSysDate, char newDateType, ZonedDateTime fpddDate) {

        List<BilDates> bilDatesList = bilDatesRepository.fetchBilDatesForCheckBdtFpdd(accountId,
                AdjustDueDatesConstants.getFutureInvoiceCodes(), newRefDate, CHAR_F, CHAR_X);
        for (BilDates bilDates : bilDatesList) {
            if (bilDates.getBdtDateType() == CHAR_I && bilDates.getBilAdjDueDt().compareTo(newAdjustDate) <= 0
                    && newAdjustDate.compareTo(fpddDate) > 0) {
                newRefDate = bilDates.getBilDatesId().getBilReferenceDt();
                newAdjustDate = bilDates.getBilAdjDueDt();
                newSysDate = bilDates.getBilSysDueDt();
                newDateType = bilDates.getBdtDateType();
                newInvoiceDate = bilDates.getBilInvDt();
                if (newAdjustDate.compareTo(fpddDate) <= 0) {
                    break;
                }
            }
            if (bilDates.getBdtDateType() != CHAR_I) {
                if (bilDates.getBilAdjDueDt().compareTo(newAdjustDate) == 0
                        && bilDates.getBilSysDueDt().compareTo(newSysDate) == 0
                        && newInvoiceDate.compareTo(bilDates.getBilInvDt()) == 0) {
                    newRefDate = bilDates.getBilDatesId().getBilReferenceDt();
                    newAdjustDate = bilDates.getBilAdjDueDt();
                    newSysDate = bilDates.getBilSysDueDt();
                    newDateType = bilDates.getBdtDateType();
                    newInvoiceDate = bilDates.getBilInvDt();
                }

                break;
            }
        }

        return new Object[] { newRefDate, newAdjustDate, newSysDate, newDateType, newInvoiceDate };
    }

    private void assignSchedule(String accountId, ZonedDateTime newSysDate, ZonedDateTime newRefDate,
            ZonedDateTime newAdjustDate, ZonedDateTime newInvoiceDate, ZonedDateTime billIstDueDt) {

        bilIstScheduleRepository.updateDefaultDataByBillIstDueDt(newAdjustDate, newInvoiceDate, newRefDate, newSysDate,
                accountId, billIstDueDt, DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime(),
                DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime());
    }

    private void assignCharge(String accountId, ZonedDateTime newAdjustDate, ZonedDateTime newInvoiceDate) {
        bilCrgAmountsRepository.updateChargeAmount(newAdjustDate, newInvoiceDate, accountId,
                DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime(), AdjustDueDatesConstants.getChargeTypes());
    }

}
