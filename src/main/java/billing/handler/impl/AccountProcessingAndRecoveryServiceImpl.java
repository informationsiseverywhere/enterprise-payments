package billing.handler.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_I;
import static core.utils.CommonConstants.CHAR_L;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActInquiry;
import billing.data.entity.BilActRules;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilBillPlan;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilDates;
import billing.data.entity.BilDesReason;
import billing.data.entity.BilEftPendingTape;
import billing.data.entity.BilPolActivity;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRenDays;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.id.BilActInquiryId;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.entity.id.BilDesReasonId;
import billing.data.entity.id.BilPolActivityId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilDatesRepository;
import billing.data.repository.BilDesReasonRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.handler.impl.BillingRulesServiceImpl.DisburseKeys;
import billing.handler.model.AccountProcessingAndRecovery;
import billing.handler.model.AccountReview;
import billing.handler.model.AccountReviewDetails;
import billing.handler.model.PolicyReview;
import billing.handler.model.RecoveryAccount;
import billing.handler.service.AccountProcessingAndRecoveryService;
import billing.handler.service.AccountReviewService;
import billing.handler.service.BillingRulesService;
import billing.handler.service.CorrectedInvoiceService;
import billing.handler.service.PolicyReviewService;
import billing.handler.service.RecoveryService;
import billing.handler.service.ReinstatementService;
import billing.handler.service.RescissionService;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants.ActivityType;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BilReceiptTypeCode;
import billing.utils.BillingConstants.ChargeTypeCodes;
import billing.utils.BillingConstants.EftRecordType;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.ManualSuspendIndicator;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.utils.DateRoutine;

@Service("AccountProcessingAndRecoveryService")
public class AccountProcessingAndRecoveryServiceImpl implements AccountProcessingAndRecoveryService {

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepositoy;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilDesReasonRepository bilDesReasonRepository;

    @Autowired
    private BilDatesRepository bilDatesRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private DateService dateService;

    @Autowired
    private BillingRulesService billingRulesService;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private AccountReviewService accountReviewService;

    @Autowired
    private RecoveryService recoveryService;

    @Autowired
    private PolicyReviewService policyReviewService;

    @Autowired
    private CorrectedInvoiceService correctedInvoiceService;

    @Autowired
    private ReinstatementService reinstatementService;

    @Autowired
    private RescissionService rescissionService;
    
    @Value("${language}")
    private String language;

    private static final char SUSPEND_DISBURSEMENT = 'D';
    private static final char ACTIVE_DISBURSEMENT = 'A';
    private static final char CHECK_FOR_REINSTATEMENT = 'R';
    private static final char CASH_ACTIVITY = CHAR_Y;
    private static final char PLUS_SIGN = '+';
    private static final char MINUS_SIGN = '-';
    private static final char INVOICING_YES = CHAR_Y;
    private static final char DRIVER_YES = CHAR_Y;
    private static final char DELETE_LDR = CHAR_L;
    public static final Logger logger = LogManager.getLogger(AccountProcessingAndRecoveryServiceImpl.class);

    @Override
    @Transactional
    public void accountAndRecoveryProcess(AccountProcessingAndRecovery accountProcessingAndRecovery) {

        boolean excludeAchPayment = false;
        String accountId = accountProcessingAndRecovery.getAccountId();
        Map<String, Object> nextDisburseDates = new HashMap<>();
        nextDisburseDates.put(DisburseKeys.NEXTDISBURSEMENTDATE.toString(), DateRoutine.defaultDateTime());
        nextDisburseDates.put(DisburseKeys.NEXTACWDATE.toString(), DateRoutine.defaultDateTime());
        nextDisburseDates.put(DisburseKeys.CASHINSUSPEND.toString(), false);
        nextDisburseDates.put(DisburseKeys.ACCOUNTLVLDISB.toString(), false);
        nextDisburseDates.put(DisburseKeys.SOURCETRANSFERTYPE.toString(), BLANK_STRING);
        nextDisburseDates.put(DisburseKeys.BILDTBDATE.toString(), accountProcessingAndRecovery.getDistributionDate());
        nextDisburseDates.put(DisburseKeys.BILDTBSEQUENCE.toString(),
                accountProcessingAndRecovery.getDistributionSequenceNumber());

        AccountReview accountReview = new AccountReview();
        AccountReviewDetails accountReviewDetails;
        accountReview.setAccountId(accountId);
        accountReview.setCashStatusCode(accountProcessingAndRecovery.getCashStatusCd());
        accountReview.setRecoverCheckIndicator(accountProcessingAndRecovery.getRecoverCheckIndicator());
        accountReview.setRecoveryDate(accountProcessingAndRecovery.getRecoveryDate());

        accountReviewDetails = accountReviewService.accountProcessReview(accountReview);

        BilAccount bilAccount = accountReviewDetails.getBilAccount();

        ZonedDateTime nextRecoveryDate = accountReviewDetails.getNextRecoveryDate();
        boolean lateCharge = accountReviewDetails.isLateCharge();

        if (accountProcessingAndRecovery.getDriverIndicator() == DELETE_LDR) {
            checkPeiDeleteLdrData(bilAccount, accountProcessingAndRecovery.getRecoveryDate());
        }

        if (accountProcessingAndRecovery.getCashAcyCallIndicator() == CHECK_FOR_REINSTATEMENT) {
            reinstatementService.checkReinstatement(bilAccount,
                    accountProcessingAndRecovery.getDistributionSequenceNumber(),
                    accountProcessingAndRecovery.getRecoveryDate(),
                    accountProcessingAndRecovery.getUserSequenceId());
        }

        if (isCashAcyCall(accountProcessingAndRecovery.getCashAcyCallIndicator())) {
            Object[] nextDateInfo = processCashAcyCall(nextDisburseDates, accountProcessingAndRecovery, bilAccount,
                    accountReviewDetails, nextRecoveryDate);
            excludeAchPayment = (boolean) nextDateInfo[0];
            boolean exit = (boolean) nextDateInfo[1];
            if (exit) {
                return;
            }
        }

        if (accountProcessingAndRecovery.getRecoverCheckIndicator() == CHAR_Y) {
            RecoveryAccount recoveryAccount = new RecoveryAccount();
            recoveryAccount.setAccountId(accountId);
            recoveryAccount.setPolicyId(accountProcessingAndRecovery.getPolicyId());
            recoveryAccount.setPolEffDate(accountProcessingAndRecovery.getPolEffDate());
            recoveryAccount.setDriverIndicator(accountProcessingAndRecovery.getDriverIndicator());
            recoveryService.processRecovery(recoveryAccount, accountReviewDetails,
                    accountProcessingAndRecovery.getRecoveryDate(),
                    accountProcessingAndRecovery.getUserSequenceId());
            if (recoveryAccount.getRecoverCheckIndicator() == BLANK_CHAR) {
                return;
            }
            accountProcessingAndRecovery.setCashAcyCallIndicator(recoveryAccount.getCashAcyCallIndicator());
        }

        ZonedDateTime nextInvoiceDate = evaluateOtherTriggers(bilAccount, accountReviewDetails,
                accountProcessingAndRecovery, nextRecoveryDate, lateCharge, excludeAchPayment, nextDisburseDates);
        correctedInvoiceService.processCorrectedInvoiceTriggers(nextInvoiceDate, nextRecoveryDate,
                accountReviewDetails.getBilActRules(), accountProcessingAndRecovery);
        updatePolicyStatus(accountProcessingAndRecovery.getAccountId(), accountProcessingAndRecovery.getPolicyId(),
                accountProcessingAndRecovery.getRecoveryDate());
    }

    private void checkPeiDeleteLdrData(BilAccount bilAccount, ZonedDateTime recoveryDate) {

        String accountId = bilAccount.getAccountId();
        List<BilPolActivity> bilPolActivities = bilPolActivityRepository.fetchBilPolActivityByNextAcyDate(accountId,
                ActivityType.POLICY_RESUME.getValue(), recoveryDate);
        if (bilPolActivities == null || bilPolActivities.isEmpty()) {
            return;
        }
        char prtReason = BLANK_CHAR;
        boolean isWritePeiForDel = false;

        for (BilPolActivity bilPolActivity : bilPolActivities) {
            String policyId = bilPolActivity.getBillPolActivityId().getPolicyId();
            ZonedDateTime polEffectiveDate = bilPolActivity.getBillPolActivityId().getPolEffectiveDt();

            BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository
                    .findById(new BilPolicyTermId(accountId, policyId, polEffectiveDate)).orElse(null);
            if (bilPolicyTerm == null) {
                continue;
            }

            if (isPrintRequest(bilPolicyTerm.getBillPolStatusCd())) {
                List<String> acyDesCodeList = new ArrayList<>(Arrays.asList("C", "RCN"));
                BilRenDays bilRenDays = rescissionService.readBilRenday(bilPolicyTerm);
                if (bilRenDays != null && bilRenDays.getBrdRenTypeCd() == CHAR_L) {
                    BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(policyId, accountId))
                            .orElse(null);
                    if (bilPolicy == null) {
                        throw new DataNotFoundException("split.policy.not.found", new Object[] { accountId, policyId });
                    }

                    ZonedDateTime maxBilAcyDate = bilActSummaryRepository.findMaxActivityDateByAcyDesCode(accountId,
                            bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), acyDesCodeList, recoveryDate);
                    if (maxBilAcyDate == null) {
                        throw new DataNotFoundException("last.day.to.reinstate.not.found", new Object[] {
                                "Max BilAcyDate", accountId, bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd() });
                    }

                    Double sumPcnAmount = bilActSummaryRepository.getSumPcnAcyAmount(accountId, bilPolicy.getPolNbr(),
                            bilPolicy.getPolSymbolCd(), acyDesCodeList, maxBilAcyDate);
                    if (sumPcnAmount == null) {
                        throw new DataNotFoundException("last.day.to.reinstate.not.found", new Object[] {
                                "Sum Pcn amount", accountId, bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd() });
                    }

                    prtReason = DELETE_LDR;
                    isWritePeiForDel = true;
                }
            }

            if (!isDeleteLdr(bilPolicyTerm.getBillPolStatusCd())) {
                reinstatementService.deleteLDRTrigger(accountId, policyId, polEffectiveDate);
            }
        }

        if (isWritePeiForDel) {
            reinstatementService.insertNreNrdSummary(accountId, "NRE", recoveryDate);
            reinstatementService.lastDayReinsPrintTrigger(accountId, bilAccount.getPayorClientId(), prtReason,
                    bilAccount.getAccountNumber(), "99", BLANK_STRING, recoveryDate);
        }
    }

    private boolean isDeleteLdr(char billPolStatusCd) {
        return Arrays
                .asList(BilPolicyStatusCode.PENDING_CANCELLATION.getValue(),
                        BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue(),
                        BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue(),
                        BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue())
                .stream().anyMatch(statusCpde -> statusCpde == billPolStatusCd);
    }

    private boolean isPrintRequest(char polStatusCd) {
        return Arrays
                .asList(BilPolicyStatusCode.CANCELLED.getValue(), BilPolicyStatusCode.FLAT_CANCELLATION.getValue(),
                        BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                        BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue())
                .stream().anyMatch(statusCpde -> statusCpde == polStatusCd);
    }

    private boolean processBilAccountHasPolicy(String accountId, char disburRctDateIndicator,
            ZonedDateTime nextRecoveryDate, AccountProcessingAndRecovery accountProcessingAndRecovery,
            boolean polLvlCnDisb, boolean polLvlCrDisb, Map<String, Object> nextDisburseDates) {

        String srcTriggerType = (String) nextDisburseDates.get(DisburseKeys.SOURCETRANSFERTYPE.toString());
        ZonedDateTime nextDisbDt = (ZonedDateTime) nextDisburseDates.get(DisburseKeys.NEXTDISBURSEMENTDATE.toString());
        boolean cashInSuspense = (boolean) nextDisburseDates.get(DisburseKeys.CASHINSUSPEND.toString());
        boolean accountLevelDisburse = (boolean) nextDisburseDates.get(DisburseKeys.ACCOUNTLVLDISB.toString());

        if (nextDisbDt.compareTo(DateRoutine.defaultDateTime()) != 0 && disburRctDateIndicator != CHAR_Y) {
            if (nextRecoveryDate.compareTo(nextDisbDt) > 0) {
                nextDisbDt = nextRecoveryDate;
            }

            updtNextDisburse(accountId, accountProcessingAndRecovery, nextDisbDt, cashInSuspense, accountLevelDisburse,
                    polLvlCnDisb, polLvlCrDisb);
            /**
             * call BCMOACT1 if if (actObjPrcCd == 'Y')
             **/
            return true;
        }

        if (nextDisbDt.compareTo(DateRoutine.defaultDateTime()) != 0 && disburRctDateIndicator == CHAR_Y) {
            if (nextRecoveryDate.compareTo(nextDisbDt) > 0) {
                nextDisbDt = nextRecoveryDate;
            }

            updateSrcDisburse(accountId, accountProcessingAndRecovery, nextDisbDt, srcTriggerType);

            /**
             * call BCMOACT1 if if (actObjPrcCd == 'Y')
             **/

            return true;
        }

        return false;
    }

    private ZonedDateTime evaluateOtherTriggers(BilAccount bilAccount, AccountReviewDetails accountReviewDetails,
            AccountProcessingAndRecovery accountProcessingAndRecovery, ZonedDateTime nextRecoveryDate,
            boolean lateCharge, boolean excludeAchPayment, Map<String, Object> nextDisburseDates) {

        ZonedDateTime nextNoncpiDt = DateRoutine.defaultDateTime();
        ZonedDateTime nextLateDate = DateRoutine.defaultDateTime();
        ZonedDateTime nextWriteoffDt = DateRoutine.defaultDateTime();
        ZonedDateTime reviewDateHold = DateRoutine.defaultStartDateTime();
        BilActRules bilActRules = accountReviewDetails.getBilActRules();
        BilSupportPlan bilSupportPlan = accountReviewDetails.getBilSupportPlan();
        boolean isBillPlanChange = false;

        ZonedDateTime nextDisbDt = (ZonedDateTime) nextDisburseDates.get(DisburseKeys.NEXTDISBURSEMENTDATE.toString());
        ZonedDateTime nextAcwDt = (ZonedDateTime) nextDisburseDates.get(DisburseKeys.NEXTACWDATE.toString());
        boolean cashInSuspense = (boolean) nextDisburseDates.get(DisburseKeys.CASHINSUSPEND.toString());
        boolean accountLevelDisburse = (boolean) nextDisburseDates.get(DisburseKeys.ACCOUNTLVLDISB.toString());
        String srcTriggerType = (String) nextDisburseDates.get(DisburseKeys.SOURCETRANSFERTYPE.toString());

        if (bilSupportPlan.getBspNoncpiLetInd() == CHAR_N && accountReviewDetails.getSuspendBillIndicator() != CHAR_Y) {
            nextNoncpiDt = billingRulesService.getNonComplianceDate(bilAccount.getAccountId(), bilSupportPlan);
        }

        ZonedDateTime nextInvoiceDate = getNextInvoiceDate(bilAccount.getAccountId());

        ZonedDateTime reviewDate = DateRoutine.defaultDateTime();
        ZonedDateTime[] reviewDateInfo = getNextReviewDate(bilAccount.getAccountId(), reviewDate, reviewDateHold);
        reviewDate = reviewDateInfo[0];
        reviewDateHold = reviewDateInfo[1];
        if (accountProcessingAndRecovery.isAccountForce()) {
            Integer count = bilActSummaryRepository.checkBpcRowExist(bilAccount.getAccountId(),
                    accountProcessingAndRecovery.getRecoveryDate(), "BPC");
            if (count != null && count > SHORT_ZERO) {
                isBillPlanChange = true;
            }
        }

        PolicyReview policyReviewInfo = createPolicyInfoData(nextDisbDt, cashInSuspense, nextAcwDt, lateCharge,
                nextLateDate, nextWriteoffDt, reviewDate, nextInvoiceDate);
        BilBillPlan bilBillPlan = policyReviewService.processPolicies(bilAccount, accountReviewDetails,
                accountProcessingAndRecovery, policyReviewInfo, isBillPlanChange);
        if (bilBillPlan == null) {
            throw new DataNotFoundException("no.bil.bill.plan.exits", new Object[] { bilAccount.getAccountId() });
        }

        nextDisbDt = policyReviewInfo.getNextDisbursmentDate();
        nextAcwDt = policyReviewInfo.getNextAcwDt();
        boolean polLvlCnDisb = policyReviewInfo.isPolLvlCnDisb();
        boolean polLvlCrDisb = policyReviewInfo.isPolLvlCrDisb();
        nextWriteoffDt = policyReviewInfo.getNextWriteoffDate();
        reviewDate = policyReviewInfo.getNextReviewDate();
        includePend1xach(excludeAchPayment, bilAccount);

        if (accountProcessingAndRecovery.getRecoverCheckIndicator() != CHAR_Y) {
            boolean isInvoiceRequire = accountReviewService.processAccountStatus(bilAccount.getAccountId(),
                    bilAccount.getStatus(), bilAccount.getSuspendedFullReasonCd(), bilBillPlan.getBbpRnlDpBill());
            accountReviewDetails.setInvoiceRequire(isInvoiceRequire);
        }

        ZonedDateTime invoiceDate = null;
        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(bilAccount.getAccountId(), "AIV")).orElse(null);
        if (bilActInquiry != null) {
            invoiceDate = bilActInquiry.getBilNxtAcyDt();
        }

        nextInvoiceDate = checkInvoiceDate(accountReviewDetails.isInvoiceRequire(), nextInvoiceDate, nextRecoveryDate,
                bilAccount, bilActInquiry, accountProcessingAndRecovery.getRecoveryDate());

        checkSecondNoticeDate(accountReviewDetails.isInvoiceRequire(), nextInvoiceDate, nextRecoveryDate, nextNoncpiDt,
                accountReviewDetails.getSuspendBillIndicator(), bilSupportPlan, bilAccount,
                accountProcessingAndRecovery);

        checkReviewDate(accountReviewDetails.isInvoiceRequire(), nextRecoveryDate, bilBillPlan.getBbpPndCncInd(),
                bilSupportPlan.getBspPndCncNbr(), accountProcessingAndRecovery, bilAccount, reviewDate, reviewDateHold);

        checkAutoCashWriteOff(nextAcwDt, bilAccount);
        checkNextDisbursementDate(nextDisbDt, nextRecoveryDate, srcTriggerType, cashInSuspense, accountLevelDisburse,
                polLvlCnDisb, polLvlCrDisb, bilAccount, bilActRules, accountProcessingAndRecovery);

        ZonedDateTime existFutNrv = DateRoutine.defaultDateTime();
        if (invoiceDate != null) {
            checkNrvDate(bilAccount, invoiceDate, existFutNrv);
        }
        checkWriteOffDate(nextWriteoffDt, nextRecoveryDate, bilAccount, bilBillPlan.getBbpWroInd());
        checkMinimumWriteOffDate(nextWriteoffDt, bilAccount, bilActRules, accountProcessingAndRecovery.getRecoveryDate());
        checkLateDate(lateCharge, nextInvoiceDate, bilAccount, accountProcessingAndRecovery, bilSupportPlan);

        return nextInvoiceDate;
    }

    private void checkLateDate(boolean lateCharge, ZonedDateTime nextInvoiceDate, BilAccount bilAccount,
            AccountProcessingAndRecovery accountProcessingAndRecovery, BilSupportPlan bilSupportPlan) {

        ZonedDateTime nextLateDate = DateRoutine.defaultDateTime();
        ZonedDateTime baiAltDt = null;

        if (accountProcessingAndRecovery.getQuoteIndicator() != CHAR_Q) {
            BilActInquiry bilActInquiry = bilActInquiryRepository
                    .findById(new BilActInquiryId(bilAccount.getAccountId(), "ALT")).orElse(null);
            if (bilActInquiry != null) {
                nextLateDate = bilActInquiry.getBilNxtAcyDt();
                baiAltDt = bilActInquiry.getBilNxtAcyDt();
            }
        }

        char invoiceIndicator = BLANK_CHAR;
        Object[] lateChargeInfo = processTriggerByLateCharge(lateCharge, bilAccount.getAccountId(),
                accountProcessingAndRecovery.getQuoteIndicator(), accountProcessingAndRecovery.getDriverIndicator(),
                invoiceIndicator, bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(), bilSupportPlan,
                nextLateDate, accountProcessingAndRecovery.getRecoveryDate());

        boolean isBelowMinNtsl = (boolean) lateChargeInfo[0];
        nextLateDate = (ZonedDateTime) lateChargeInfo[1];
        ZonedDateTime lateAdjChargeDate = (ZonedDateTime) lateChargeInfo[2];
        if (lateAdjChargeDate == null) {
            lateAdjChargeDate = DateRoutine.defaultDateTime();
        }

        if (nextLateDate.compareTo(nextInvoiceDate) > 0 && nextLateDate.compareTo(DateRoutine.defaultDateTime()) != 0
                && lateAdjChargeDate.compareTo(nextInvoiceDate) <= 0) {
            nextLateDate = nextInvoiceDate;
        }

        label24: {
            if (!lateCharge && baiAltDt == null
                    || nextLateDate.compareTo(DateRoutine.defaultDateTime()) == 0 && baiAltDt == null
                    || baiAltDt != null && nextLateDate.compareTo(baiAltDt) == 0
                            && nextLateDate.compareTo(DateRoutine.defaultDateTime()) != 0
                    || accountProcessingAndRecovery.getQuoteIndicator() == CHAR_Q
                    // ||
                    // accountReviewUpdateCpy.getBcmlax00BelowMinInvoicingSw()
                    // ==
                    // AccountReviewUpdateCpy.BCMLAX00_BELOW_MIN_INVOICING_YES
                    || nextLateDate.compareTo(accountProcessingAndRecovery.getRecoveryDate()) < 0) {
                break label24;
            } else {
                updtNxtLate(baiAltDt, nextLateDate, bilAccount.getAccountId(), isBelowMinNtsl, 
                        accountProcessingAndRecovery.getRecoveryDate());

            }
            break label24;
        }

    }

    private void checkMinimumWriteOffDate(ZonedDateTime nextWriteoffDate, BilAccount bilAccount,
            BilActRules bilActRules, ZonedDateTime recoveryDate) {

        int minimumWriteOffDays = 0;
        if (bilActRules.getBruMinWroInd() == CHAR_Y) {
            minimumWriteOffDays = bilActRules.getBruMinWroNbr();
        }
        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(bilAccount.getAccountId(), "MWO")).orElse(null);
        if (bilActInquiry != null && bilActRules.getBruMinWroInd() == CHAR_N) {
            bilActInquiryRepository.deleteById(new BilActInquiryId(bilAccount.getAccountId(), "MWO"));
        } else if (bilActRules.getBruMinWroInd() == CHAR_Y && bilActInquiry != null) {
            ZonedDateTime minimumWriteOffDate = bilActInquiry.getBilNxtAcyDt();
            if (bilActInquiry.getBilNxtAcyDt().isEqual(DateRoutine.defaultDateTime())) {
                minimumWriteOffDate = updateMwoProcess(bilAccount.getAccountId(), minimumWriteOffDays, recoveryDate);
            }

            if (nextWriteoffDate.isEqual(minimumWriteOffDate)
                    && bilActInquiry.getBilNxtAcyDt().compareTo(DateRoutine.defaultDateTime()) != 0) {
                bilActInquiry.setBilNxtAcyDt(DateRoutine.defaultDateTime());
                bilActInquiryRepository.saveAndFlush(bilActInquiry);
            }
        }
    }

    private void checkNrvDate(BilAccount bilAccount, ZonedDateTime invoiceDate, ZonedDateTime existFutNrv) {

        ZonedDateTime nrvNextAcyDt = null;
        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(bilAccount.getAccountId(), "NRV")).orElse(null);

        if (bilActInquiry != null) {
            nrvNextAcyDt = bilActInquiry.getBilNxtAcyDt();
        }
        if (bilActInquiry == null) {
            nrvNextAcyDt = DateRoutine.dateTimeAsYYYYMMDD("0001-01-01");
        }

        if (invoiceDate.isBefore(nrvNextAcyDt)) {
            delNrvTriggers(bilAccount.getAccountId(), nrvNextAcyDt, existFutNrv);
        }

    }

    private void checkWriteOffDate(ZonedDateTime nextWriteoffDt, ZonedDateTime nextRecoveryDate, BilAccount bilAccount,
            char writeOffIndicator) {

        if (nextRecoveryDate.compareTo(nextWriteoffDt) > 0) {
            nextWriteoffDt = nextRecoveryDate;
        }

        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(bilAccount.getAccountId(), "AWO")).orElse(null);

        if ((bilActInquiry != null || writeOffIndicator != CHAR_Y)
                && ((bilActInquiry == null || !nextWriteoffDt.isEqual(bilActInquiry.getBilNxtAcyDt()))
                        && nextWriteoffDt.compareTo(DateRoutine.dateTimeAsYYYYMMDD("8888-12-31")) != 0)) {
            if (bilActInquiry != null) {
                bilActInquiry.setBilNxtAcyDt(nextWriteoffDt);
                bilActInquiryRepository.saveAndFlush(bilActInquiry);
            } else {
                if (writeOffIndicator != CHAR_Y) {
                    bilActInquiry = new BilActInquiry();
                    BilActInquiryId bilActInquiryId = new BilActInquiryId(bilAccount.getAccountId(), "AWO");
                    bilActInquiry.setBilActInquiryId(bilActInquiryId);
                    bilActInquiry.setBilNxtAcyDt(nextWriteoffDt);
                    bilActInquiryRepository.saveAndFlush(bilActInquiry);
                }

            }
        }
    }

    private void checkAutoCashWriteOff(ZonedDateTime nextAcwDt, BilAccount bilAccount) {

        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(bilAccount.getAccountId(), "ACW")).orElse(null);

        if (bilActInquiry == null || bilActInquiry != null && !nextAcwDt.isEqual(bilActInquiry.getBilNxtAcyDt())) {
            updatetNxtAcyDt(bilAccount.getAccountId(), "ACW", nextAcwDt);
        }
    }

    private void checkNextDisbursementDate(ZonedDateTime nextDisbDt, ZonedDateTime nextRecoveryDate,
            String srcTriggerType, boolean cashInSuspense, boolean accountLevelDisburse, boolean polLvlCnDisb,
            boolean polLvlCrDisb, BilAccount bilAccount, BilActRules bilActRules,
            AccountProcessingAndRecovery accountProcessingAndRecovery) {

        if (nextRecoveryDate.compareTo(nextDisbDt) > 0) {
            nextDisbDt = nextRecoveryDate;
        }

        if (bilActRules.getBruDsbRctDtInd() != CHAR_Y) {
            BilActInquiry bilActInquiry = bilActInquiryRepository.findById(
                    new BilActInquiryId(bilAccount.getAccountId(), ActivityType.ACCOUNT_DISBURSEMENT.getValue()))
                    .orElse(null);

            if (bilActInquiry == null || bilActInquiry != null && !nextDisbDt.isEqual(bilActInquiry.getBilNxtAcyDt())) {
                updtNextDisburse(bilAccount.getAccountId(), accountProcessingAndRecovery, nextDisbDt, cashInSuspense,
                        accountLevelDisburse, polLvlCnDisb, polLvlCrDisb);
            }

        } else if (bilActRules.getBruDsbRctDtInd() == CHAR_Y && srcTriggerType.compareTo(BLANK_STRING) != 0) {
            BilActInquiry bilActInquiry = bilActInquiryRepository
                    .findById(new BilActInquiryId(bilAccount.getAccountId(), srcTriggerType)).orElse(null);

            if (bilActInquiry == null || bilActInquiry != null && !nextDisbDt.isEqual(bilActInquiry.getBilNxtAcyDt())) {
                updateSrcDisburse(bilAccount.getAccountId(), accountProcessingAndRecovery, nextDisbDt, srcTriggerType);
            }

        }

    }

    private void checkSecondNoticeDate(boolean isInvoiceRequire, ZonedDateTime nextInvoiceDate,
            ZonedDateTime nextRecoveryDate, ZonedDateTime nextNoncpiDt, char suspendBillIndicator,
            BilSupportPlan bilSupportPlan, BilAccount bilAccount,
            AccountProcessingAndRecovery accountProcessingAndRecovery) {

        char eftCollectionIndicator = CHAR_Y;
        ZonedDateTime nextSecondNoticeDate = DateRoutine.defaultDateTime();
        if (!isInvoiceRequire) {
            nextSecondNoticeDate = DateRoutine.defaultDateTime();
            nextNoncpiDt = DateRoutine.defaultDateTime();
        } else if (bilSupportPlan.getBsp2ndNotInd() == CHAR_N) {
            BilDesReason bilDesReason = bilDesReasonRepository.findDescriptionByCodeAndType("CET", BLANK_STRING,
                    bilAccount.getCollectionMethod());
            if (bilDesReason == null) {
                eftCollectionIndicator = CHAR_Y;
            }

            nextSecondNoticeDate = billingRulesService.checkForSecondNotice(bilAccount.getAccountId(),
                    suspendBillIndicator, accountProcessingAndRecovery.getDriverIndicator(), eftCollectionIndicator,
                    bilSupportPlan.getBsp2ndNotNbr());

        }
        if (nextRecoveryDate.isEqual(nextInvoiceDate) || nextRecoveryDate.isAfter(nextInvoiceDate)) {
            nextSecondNoticeDate = DateRoutine.defaultDateTime();
            nextNoncpiDt = DateRoutine.defaultDateTime();
            updateBilDates(bilSupportPlan, bilAccount, nextNoncpiDt, nextSecondNoticeDate, nextRecoveryDate);
        } else if (nextRecoveryDate.compareTo(nextSecondNoticeDate) > 0
                && nextRecoveryDate.compareTo(nextInvoiceDate) != 0) {
            nextSecondNoticeDate = nextRecoveryDate;
            nextNoncpiDt = DateRoutine.defaultDateTime();
            updateBilDates(bilSupportPlan, bilAccount, nextNoncpiDt, nextSecondNoticeDate, nextRecoveryDate);
        }

        if (nextRecoveryDate.compareTo(nextNoncpiDt) > 0 && nextRecoveryDate.compareTo(nextSecondNoticeDate) != 0) {
            nextNoncpiDt = nextRecoveryDate;
        }

        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(bilAccount.getAccountId(), "SND")).orElse(null);

        if ((bilActInquiry != null || bilSupportPlan.getBsp2ndNotInd() != CHAR_Y) && (bilActInquiry == null
                || bilActInquiry != null && !nextSecondNoticeDate.isEqual(bilActInquiry.getBilNxtAcyDt()))) {
            updatetNxtAcyDt(bilAccount.getAccountId(), "SND", nextSecondNoticeDate);
        }

        bilActInquiry = bilActInquiryRepository.findById(new BilActInquiryId(bilAccount.getAccountId(), "ANC"))
                .orElse(null);

        if ((bilActInquiry != null || bilSupportPlan.getBspNoncpiLetInd() != CHAR_Y) && (bilActInquiry == null
                || bilActInquiry != null && !nextNoncpiDt.isEqual(bilActInquiry.getBilNxtAcyDt()))) {
            updatetNxtAcyDt(bilAccount.getAccountId(), "ANC", nextNoncpiDt);
        }
    }

    private ZonedDateTime checkInvoiceDate(boolean isInvoiceRequire, ZonedDateTime nextInvoiceDate,
            ZonedDateTime nextRecoveryDate, BilAccount bilAccount, BilActInquiry bilActInquiry,
            ZonedDateTime recoveryDate) {
        ZonedDateTime invoiceDateAdjusted;
        if (!isInvoiceRequire) {
            invoiceDateAdjusted = DateRoutine.defaultDateTime();
        } else {
            invoiceDateAdjusted = getNextInvoiceDate(bilAccount.getAccountId());
        }

        if (nextRecoveryDate.isAfter(nextInvoiceDate) || nextRecoveryDate.isEqual(nextInvoiceDate)) {
            invoiceDateAdjusted = nextRecoveryDate;
        }

        if (bilActInquiry == null
                || bilActInquiry != null && !invoiceDateAdjusted.isEqual(bilActInquiry.getBilNxtAcyDt())) {
            updatetNxtAcyDt(bilAccount.getAccountId(), "AIV", invoiceDateAdjusted);
            if (invoiceDateAdjusted.compareTo(DateRoutine.defaultDateTime()) == 0) {
                checkForRpc(bilAccount.getAccountId(), recoveryDate);
            }
        }

        return invoiceDateAdjusted;

    }

    private void checkReviewDate(boolean isInvoiceRequire, ZonedDateTime nextRecoveryDate, char pendingCancelIndicator,
            short pendingCancelNumber, AccountProcessingAndRecovery accountProcessingAndRecovery, BilAccount bilAccount,
            ZonedDateTime reviewDate, ZonedDateTime reviewDateHold) {

        if (!isInvoiceRequire || pendingCancelIndicator == CHAR_Y) {
            reviewDate = DateRoutine.defaultDateTime();
        }

        reviewDate = resetArv(bilAccount, accountProcessingAndRecovery, pendingCancelNumber, reviewDate);
        if (nextRecoveryDate.isAfter(reviewDate)) {
            reviewDate = nextRecoveryDate;
        }

        if (reviewDate.compareTo(reviewDateHold) != 0) {
            updatetNxtAcyDt(bilAccount.getAccountId(), "ARV", reviewDate);
        }

    }

    private ZonedDateTime[] getNextReviewDate(String accountId, ZonedDateTime reviewDate,
            ZonedDateTime reviewDateHold) {

        BilActInquiry bilActInquiry = bilActInquiryRepository.findById(new BilActInquiryId(accountId, "ARV"))
                .orElse(null);
        if (bilActInquiry != null) {
            reviewDate = bilActInquiry.getBilNxtAcyDt();
            return new ZonedDateTime[] { reviewDate, reviewDate };
        }
        return new ZonedDateTime[] { reviewDate, reviewDateHold };
    }

    @Override
    public ZonedDateTime getNextInvoiceDate(String accountId) {
        List<Character> invoiceCodes = Arrays.asList(BLANK_CHAR,
                InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue());
        ZonedDateTime nextInvDt = bilDatesRepository.getMinInvoicedDateByInvoiceCd(accountId, invoiceCodes, CHAR_X);
        if (nextInvDt == null) {
            nextInvDt = DateRoutine.defaultDateTime();
        }
        return nextInvDt;
    }

    private PolicyReview createPolicyInfoData(ZonedDateTime nextDisbDt, boolean cashInSuspense, ZonedDateTime nextAcwDt,
            boolean lateCharge, ZonedDateTime nextLateDate, ZonedDateTime nextWriteoffDt, ZonedDateTime reviewDate,
            ZonedDateTime invoiceDate) {
        PolicyReview policyReview = new PolicyReview();
        policyReview.setNextDisbursmentDate(nextDisbDt);
        policyReview.setCashInSuspense(cashInSuspense);
        policyReview.setNextAcwDt(nextAcwDt);
        policyReview.setLateCharge(lateCharge);
        policyReview.setNextLateDate(nextLateDate);
        policyReview.setNextWriteoffDate(nextWriteoffDt);
        policyReview.setNextReviewDate(reviewDate);
        policyReview.setNextInvoiceDate(invoiceDate);
        return policyReview;
    }

    private Object[] processTriggerByLateCharge(Boolean lateCharge, String accountId, char quoteIndicator,
            char driverIndicator, char invoiceIndicator, String bilTypeCd, String bilClassCd,
            BilSupportPlan bilSupportPlan, ZonedDateTime nextLateDate, ZonedDateTime recoveryDate) {

        boolean isBelowMinNtsl = false;
        ZonedDateTime lateAdjChargeDate = null;
        if (Boolean.TRUE.equals(lateCharge)) {
            isBelowMinNtsl = belowMinCheck(accountId, recoveryDate);
            if (quoteIndicator != CHAR_Q) {
                ZonedDateTime[] sumChargeInfo = sumCharges(accountId, invoiceIndicator, driverIndicator, bilTypeCd,
                        bilClassCd, bilSupportPlan);
                nextLateDate = sumChargeInfo[0];
                lateAdjChargeDate = sumChargeInfo[2];
            }
        } else {
            nextLateDate = DateRoutine.defaultDateTime();
        }
        return new Object[] { isBelowMinNtsl, nextLateDate, lateAdjChargeDate };
    }

    private ZonedDateTime[] sumCharges(String accountId, char invoiceIndicator, char driverIndicator, String bilTypeCd,
            String bilClassCd, BilSupportPlan bilSupportPlan) {
        ZonedDateTime nextLateDateTemp = null;
        Double totalChargeBalance = DECIMAL_ZERO;
        ZonedDateTime maxLateDate = null;
        ZonedDateTime lateAdjChargeDate = bilDatesRepository.getMaxAdjustDueDateByLateCharge(accountId,
                getInvCodeList(), CHAR_N, CHAR_X);
        if (lateAdjChargeDate == null) {
            nextLateDateTemp = DateRoutine.defaultDateTime();
        } else {
            label1: {
                if (driverIndicator != DRIVER_YES
                        || driverIndicator != DRIVER_YES && invoiceIndicator == INVOICING_YES) {
                    ZonedDateTime[] altTrigInfo = getAltTriggerDate(lateAdjChargeDate,
                            bilSupportPlan.getBspLateCrgNbr());
                    nextLateDateTemp = altTrigInfo[0];
                    maxLateDate = altTrigInfo[1];
                    break label1;
                }

                totalChargeBalance = lateCharges(accountId, bilTypeCd, bilClassCd, totalChargeBalance);
                if (totalChargeBalance < bilSupportPlan.getBspMinInvAmt() || totalChargeBalance == DECIMAL_ZERO) {
                    updateBildateLateInd(accountId, lateAdjChargeDate);
                    nextLateDateTemp = DateRoutine.defaultDateTime();
                    break label1;
                }

                Double lateChargeBalance = bilCrgAmountsRepository.findNextChargesDueAmt(accountId, getInvCodeList(),
                        Arrays.asList(ChargeTypeCodes.PENALTY_CHARGE.getValue(), ChargeTypeCodes.LATE_CHARGE.getValue(),
                                ChargeTypeCodes.SERVICE_CHARGE.getValue(),
                                ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()));
                if (lateChargeBalance == null) {
                    lateChargeBalance = DECIMAL_ZERO;
                }
                totalChargeBalance = totalChargeBalance + lateChargeBalance;
                if (totalChargeBalance < bilSupportPlan.getBspMinInvAmt() || totalChargeBalance == DECIMAL_ZERO) {
                    updateBildateLateInd(accountId, lateAdjChargeDate);
                    nextLateDateTemp = DateRoutine.defaultDateTime();
                    break label1;
                }

                ZonedDateTime[] altTrigInfo = getAltTriggerDate(lateAdjChargeDate, bilSupportPlan.getBspLateCrgNbr());
                nextLateDateTemp = altTrigInfo[0];
                maxLateDate = altTrigInfo[1];
            }
        }
        return new ZonedDateTime[] { nextLateDateTemp, maxLateDate, lateAdjChargeDate };
    }

    private void updateBildateLateInd(String accountId, ZonedDateTime lateAdjChargeDate) {
        bilDatesRepository.updateLateChargeInd(CHAR_Y, accountId, lateAdjChargeDate, getInvCodeList(), CHAR_N);
    }

    private Double lateCharges(String accountId, String bilTypeCd, String bilClassCd, Double totalChargeBalance) {

        List<Character> polStatusList = Arrays.asList(BilPolicyStatusCode.SUSPEND_FOLLOWUP.getValue(),
                BilPolicyStatusCode.SUSPENDBILLING_FOLLOWUP.getValue(),
                BilPolicyStatusCode.PENDING_CANCELLATION.getValue(),
                BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue(),
                BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue(),
                BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue(),
                BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue(),
                BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue());

        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("PCLF", bilTypeCd, bilClassCd, BLANK_STRING);
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            polStatusList = Arrays.asList(BilPolicyStatusCode.SUSPEND_FOLLOWUP.getValue(),
                    BilPolicyStatusCode.SUSPENDBILLING_FOLLOWUP.getValue(),
                    BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue(),
                    BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue());
        }

        Double lateFeeBalance = DECIMAL_ZERO;
        List<Object[]> lateFeeObject = bilIstScheduleRepository.getLateChargeBalance(accountId, polStatusList,
                Arrays.asList(InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue(),
                        InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()));
        if (lateFeeObject != null && !lateFeeObject.isEmpty() && lateFeeObject.get(0)[1] != null) {
            String payableItemCd = (String) lateFeeObject.get(0)[1];
            BilDesReason bilDesReason = bilDesReasonRepository
                    .findById(new BilDesReasonId(payableItemCd, "XLT", language)).orElse(null);
            if (bilDesReason == null) {
                lateFeeBalance = (Double) lateFeeObject.get(0)[0];
            }
        }
        totalChargeBalance = totalChargeBalance + lateFeeBalance;
        return totalChargeBalance;
    }

    private ZonedDateTime[] getAltTriggerDate(ZonedDateTime lateAdjChargeDate, short lateChargeNumber) {
        ZonedDateTime nextLateDate;
        ZonedDateTime maxLateDate;
        ZonedDateTime differenceDate = null;
        differenceDate = DateRoutine.getAdjustedDate(lateAdjChargeDate, false, lateChargeNumber, null);
        maxLateDate = differenceDate;
        nextLateDate = differenceDate;
        return new ZonedDateTime[] { nextLateDate, maxLateDate };
    }

    private boolean belowMinCheck(String accountId, ZonedDateTime recoveryDate) {

        List<BilDates> bilDatesList = bilDatesRepository.getMaxAdjDueDateRow(accountId,
                recoveryDate,
                InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(), CHAR_X);

        return bilDatesList != null && !bilDatesList.isEmpty();
    }

    private ZonedDateTime updateMwoProcess(String bilAccountId, int mwoDays, ZonedDateTime recoveryDate) {
        ZonedDateTime mwoPlnExpDt = null;
        ZonedDateTime minMwoDate;
        ZonedDateTime newMwoDate = recoveryDate;
        minMwoDate = DateRoutine.adjustDateWithOffset(newMwoDate, false, mwoDays, MINUS_SIGN, recoveryDate);

        List<Character> billPolStatusCd = Arrays.asList(BilPolicyStatusCode.TRANSFERRED.getValue(),
                BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(), BilPolicyStatusCode.CANCELLED.getValue(),
                BilPolicyStatusCode.FLAT_CANCELLATION.getValue(), BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue());
        mwoPlnExpDt = bilPolicyTermRepository.getMinPlnExpDates(bilAccountId, billPolStatusCd, minMwoDate, BLANK_CHAR,
                CHAR_Y);
        if (mwoPlnExpDt == null) {
            mwoPlnExpDt = DateRoutine.defaultDateTime();
        }

        ZonedDateTime mwoStatEffDt = bilPolicyTermRepository.getMinStatusEffDates(bilAccountId, billPolStatusCd,
                minMwoDate, BLANK_CHAR, CHAR_Y);
        if (mwoStatEffDt == null) {
            mwoStatEffDt = DateRoutine.defaultDateTime();
        }
        if (mwoStatEffDt.compareTo(mwoPlnExpDt) < 0) {
            newMwoDate = mwoStatEffDt;
        } else {
            newMwoDate = mwoPlnExpDt;
        }
        if (newMwoDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
            newMwoDate = DateRoutine.adjustDateWithOffset(newMwoDate, false, mwoDays, PLUS_SIGN, null);
        }
        updatetNxtAcyDt(bilAccountId, "MWO", newMwoDate);
        return newMwoDate;
    }

    private void delNrvTriggers(String bilAccountId, ZonedDateTime nextNrvInvDt, ZonedDateTime existFutNrv) {
        if (existFutNrv.compareTo(nextNrvInvDt) < 0) {
            return;
        }
        if (bilActInquiryRepository.existsById(new BilActInquiryId(bilAccountId, "NRV"))) {
            bilActInquiryRepository.deleteById(new BilActInquiryId(bilAccountId, "NRV"));
        }

        bilPolActivityRepository.deleteBilPolActivityRow(bilAccountId, "NRV");

    }

    private void checkForRpc(String bilAccountId, ZonedDateTime recoveryDate) {

        BilActInquiry bilActInquiry = bilActInquiryRepository.findById(new BilActInquiryId(bilAccountId, "RPC"))
                .orElse(null);
        if (bilActInquiry != null && (bilActInquiry.getBilNxtAcyDt().isEqual(recoveryDate)
                || bilActInquiry.getBilNxtAcyDt().isBefore(recoveryDate))) {
            bilActInquiryRepository.deleteById(new BilActInquiryId(bilAccountId, "RPC"));
        }

    }

    private void updtNxtLate(ZonedDateTime baiAltDt, ZonedDateTime nextLateDt, String bilAccountId,
            boolean isBelowMinNtsl, ZonedDateTime recoveryDate) {
        if (isBelowMinNtsl && baiAltDt == null && nextLateDt.compareTo(recoveryDate) >= 0) {
            return;
        }
        if (baiAltDt != null && nextLateDt.compareTo(DateRoutine.defaultDateTime()) == 0) {
            if (bilActInquiryRepository.existsById(new BilActInquiryId(bilAccountId, "ALT"))) {
                bilActInquiryRepository.deleteById(new BilActInquiryId(bilAccountId, "ALT"));
            }
            return;
        }
        updatetNxtAcyDt(bilAccountId, "ALT", nextLateDt);
    }

    private BilActInquiry updatetNxtAcyDt(String bilAccountId, String bilAcyTypeCd, ZonedDateTime bilNxtAcyDt) {
        BilActInquiry bilActInquiry = new BilActInquiry();
        BilActInquiryId bilActInquiryId = new BilActInquiryId(bilAccountId, bilAcyTypeCd);
        bilActInquiry.setBilActInquiryId(bilActInquiryId);
        bilActInquiry.setBilNxtAcyDt(bilNxtAcyDt);
        bilActInquiryRepository.save(bilActInquiry);
        return bilActInquiry;

    }

    private List<Character> getInvCodeList() {
        List<Character> invCodeList = new ArrayList<>();
        invCodeList.add(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue());
        invCodeList.add(InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());
        invCodeList.add(InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue());
        return invCodeList;
    }

    private void updateBilDates(BilSupportPlan bilSupportPlan, BilAccount bilaccount, ZonedDateTime nextNoncpiDt,
            ZonedDateTime secondNoticeDt, ZonedDateTime nextRecoveryDate) {
        if (bilSupportPlan.getBsp2ndNotInd() == CHAR_N && bilSupportPlan.getBspNoncpiLetInd() == CHAR_Y
                && secondNoticeDt.compareTo(DateRoutine.defaultDateTime()) == 0) {

            bilDatesRepository.updateByBilAccountIdRec(CHAR_S, bilaccount.getAccountId(), nextRecoveryDate,
                    Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()),
                    CHAR_N);

        }
        if (bilSupportPlan.getBsp2ndNotInd() == CHAR_N && bilSupportPlan.getBspNoncpiLetInd() == CHAR_N
                && nextNoncpiDt.compareTo(DateRoutine.defaultDateTime()) == 0
                && secondNoticeDt.compareTo(DateRoutine.defaultDateTime()) == 0) {
            bilDatesRepository.updateByBilAccountIdRec(CHAR_N, bilaccount.getAccountId(), nextRecoveryDate,
                    Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()),
                    CHAR_N);

        }
        if (bilSupportPlan.getBsp2ndNotInd() == CHAR_Y && bilSupportPlan.getBspNoncpiLetInd() == CHAR_N
                && nextNoncpiDt.compareTo(DateRoutine.defaultDateTime()) == 0) {

            bilDatesRepository.updateByBilAccountIdRec(CHAR_Y, bilaccount.getAccountId(), nextRecoveryDate,
                    Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()),
                    CHAR_N);
        }

    }

    private ZonedDateTime resetArv(BilAccount bilAccount, AccountProcessingAndRecovery accountProcessingAndRecovery,
            short pendingCancelNumber, ZonedDateTime nextReviewDt) {
        ZonedDateTime crgPastDate;
        Boolean brtExtCncIndicator = null;

        if (accountProcessingAndRecovery.getDriverIndicator() == CHAR_O) {
            nextReviewDt = DateRoutine.defaultDateTime();
            return nextReviewDt;
        }

        List<Character> invoiceCodes = Arrays.asList(
                InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue(),
                InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue());
        List<Character> billPolStatusCd = Arrays.asList(BilPolicyStatusCode.OPEN.getValue(),
                BilPolicyStatusCode.SUSPENDBILLING_FOLLOWUP.getValue(), BilPolicyStatusCode.SUSPEND_BILLING.getValue(),
                BilPolicyStatusCode.SUSPEND_FOLLOWUP.getValue(), BilPolicyStatusCode.CLOSED.getValue(),
                BilPolicyStatusCode.CANCELLED_SUBJECT_TO_AUDIT.getValue(), BilPolicyStatusCode.QUOTE.getValue(),
                BilPolicyStatusCode.PENDING_TRANSFER.getValue(),
                BilPolicyStatusCode.PENDING_TRANSFER_APPLYPAYMENTS.getValue(),
                BilPolicyStatusCode.PENDING_TRANSFER_HOLDPAYMENTS.getValue(),
                BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue(),
                BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue(), '4');
        ZonedDateTime resetArvDate = bilIstScheduleRepository.getMaxAdjDueDtByPolicyStatus(bilAccount.getAccountId(),
                accountProcessingAndRecovery.getRecoveryDate(), billPolStatusCd, invoiceCodes, "BIT");

        if (resetArvDate == null) {
            return nextReviewDt;
        }

        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("ECNC", bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), SEPARATOR_BLANK);
        if (bilRulesUct != null) {
            brtExtCncIndicator = true;
        }

        if ((bilAccount.getBillTypeCd().compareTo("SP") == 0 || bilAccount.getBillTypeCd().compareTo("AB") == 0)
                && Boolean.TRUE.equals(brtExtCncIndicator)) {
            crgPastDate = bilCrgAmountsRepository.getMaxbilAdjDueDt(bilAccount.getAccountId(), invoiceCodes);

            if (crgPastDate != null && crgPastDate.compareTo(DateRoutine.defaultDateTime()) != 0
                    && crgPastDate.compareTo(resetArvDate) > 0) {
                resetArvDate = crgPastDate;
            }
        }

        nextReviewDt = DateRoutine.adjustDateWithOffset(resetArvDate, false, pendingCancelNumber, PLUS_SIGN, null);
        return nextReviewDt;

    }

    private void includePend1xach(Boolean excludeAchPayment, BilAccount bilAccount) {
        if (Boolean.FALSE.equals(excludeAchPayment)) {
            return;
        }
        bilCashDspRepository.updateRsusIndForAChByBilAccountId(ReverseReSuspendIndicator.BLANK.getValue(),
                bilAccount.getAccountId(), BilDspTypeCode.SUSPENDED.getValue(),
                ManualSuspendIndicator.ENABLED.getValue(), CHAR_I);
        excludeAchPayment = false;
    }

    private void updateSrcDisburse(String accountId, AccountProcessingAndRecovery accountProcessingAndRecovery,
            ZonedDateTime nextDisbDt, String srcTriggerType) {
        if (!isCashAcyCall(accountProcessingAndRecovery.getCashAcyCallIndicator())) {
            return;
        }
        if (srcTriggerType.compareTo(BLANK_STRING) == 0 && nextDisbDt.compareTo(DateRoutine.defaultDateTime()) == 0) {
            return;
        }
        List<BilActInquiry> bilActInquiryList = bilActInquiryRepository.getSuspendDisbRow(accountId,
                Arrays.asList(srcTriggerType), accountProcessingAndRecovery.getRecoveryDate());
        if (bilActInquiryList != null && !bilActInquiryList.isEmpty()) {
            BilActInquiry bilActInquiryUpdate = bilActInquiryList.get(0);
            bilActInquiryUpdate.setBilNxtAcyDt(nextDisbDt);
            bilActInquiryRepository.save(bilActInquiryUpdate);
        }
        List<BilCashDsp> bilCashDspList = bilCashDspRepository.getCreditCashInPast(accountId,
                BilDspTypeCode.SUSPENDED.getValue(), CHAR_Y, BLANK_CHAR, accountProcessingAndRecovery.getRecoveryDate());
        for (BilCashDsp bilCashDsp : bilCashDspList) {
            BilCashReceipt bilCashReceipt = bilCashReceiptRepository.findById(new BilCashReceiptId(accountId,
                    bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr()))
                    .orElse(null);
            if (bilCashReceipt != null) {
                bilCashDsp.setDisbursementDate(nextDisbDt);
                bilCashDspRepository.save(bilCashDsp);
            }
        }
    }

    private void updtNextDisburse(String accountId, AccountProcessingAndRecovery accountProcessingAndRecovery,
            ZonedDateTime nextDisbDt, boolean cashInSuspense, boolean actLvlDisbSw, boolean polLvlCnDisb,
            boolean polLvlCrDisb) {

        if (!isCashAcyCall(accountProcessingAndRecovery.getCashAcyCallIndicator())) {
            return;
        }
        if (!cashInSuspense || (!actLvlDisbSw && !polLvlCrDisb && !polLvlCnDisb)) {
            nextDisbDt = DateRoutine.defaultDateTime();
        }

        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(accountId, ActivityType.ACCOUNT_DISBURSEMENT.getValue())).orElse(null);
        if (bilActInquiry == null) {
            bilActInquiry = new BilActInquiry();
            BilActInquiryId bilActInquiryId = new BilActInquiryId(accountId,
                    ActivityType.ACCOUNT_DISBURSEMENT.getValue());
            bilActInquiry.setBilActInquiryId(bilActInquiryId);
        }
        bilActInquiry.setBilNxtAcyDt(nextDisbDt);
        bilActInquiryRepository.saveAndFlush(bilActInquiry);
    }

    private void checkCashapplytoCancelPolicy(String accountId,
            AccountProcessingAndRecovery accountProcessingAndRecovery) {

        ZonedDateTime recoveryDate = accountProcessingAndRecovery.getRecoveryDate();
        List<Character> billPolStatusCd = Arrays.asList(BilPolicyStatusCode.CANCELLED.getValue(),
                BilPolicyStatusCode.CANCELLED_SUBJECT_TO_AUDIT.getValue(),
                BilPolicyStatusCode.FLAT_CANCELLATION.getValue());
        List<BilPolicyTerm> bilPolicyTermList = bilPolicyTermRepository.findCancelPolicyByBilAccountId(accountId,
                billPolStatusCd);
        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {

            String policyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
            ZonedDateTime bptPolEffectiveDt = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
            Integer count = bilCashDspRepository.findCashAppliedToPolicy(accountId, policyId,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), recoveryDate,
                    BilDspTypeCode.APPLIED.getValue(), ReverseReSuspendIndicator.BLANK.getValue());
            if (count == null || count == SHORT_ZERO) {
                continue;
            }

            writeOrDeletePcc(recoveryDate, accountId, policyId, bptPolEffectiveDt);

        }
    }

    private void writeOrDeletePcc(ZonedDateTime recoveryDate, String accountId, String policyId,
            ZonedDateTime bptPolEffectiveDt) {

        BilPolicy bilpolicy = bilPolicyRepository.findById(new BilPolicyId(policyId, accountId)).orElse(null);
        if (bilpolicy == null) {
            throw new DataNotFoundException("policy.number.not.found", new Object[] { policyId });
        }

        Short sequenceNumber = bilActSummaryRepository.findByMaxSeqNumberForCancelRow(accountId, recoveryDate,
                bilpolicy.getPolNbr(), DECIMAL_ZERO);
        if (sequenceNumber != null) {

            List<String> bilDesReaTypList = Arrays.asList(BilDesReasonType.COL.getValue(),
                    BilDesReasonType.PYT.getValue(), BilDesReasonType.CPY.getValue(), BilDesReasonType.PYS.getValue(),
                    BilDesReasonType.ACH.getValue(), BilDesReasonType.PYC.getValue(),
                    BilDesReasonType.ONE_XA.getValue(), BilDesReasonType.GPY.getValue(),
                    BilDesReasonType.GPT.getValue(), BilDesReasonType.ASW.getValue(),
                    BilDesReasonType.WALLET_PAYMENT_TYPE.getValue());
            Short maxSequenceNumber = bilActSummaryRepository.findMaxSeqNumberByPaymentType(accountId,
                    recoveryDate, BilReceiptTypeCode.EFT.getValue(), bilDesReaTypList);
            if (maxSequenceNumber == null) {
                maxSequenceNumber = SHORT_ZERO;
            }
            if (maxSequenceNumber <= sequenceNumber) {
                return;
            }

        }

        BilPolActivity bilPolActivity = new BilPolActivity();
        BilPolActivityId bilPolActivityId = new BilPolActivityId(accountId, policyId, bptPolEffectiveDt,
                ActivityType.POLICY_CANCELLED_CASH_APPLIED.getValue());
        bilPolActivity.setBillPolActivityId(bilPolActivityId);
        bilPolActivity.setBilNxtAcyDt(recoveryDate);
        bilPolActivityRepository.saveAndFlush(bilPolActivity);

    }

    private boolean isCashAcyCall(char cashAcyCallIndicator) {
        return cashAcyCallIndicator == CASH_ACTIVITY || cashAcyCallIndicator == CHECK_FOR_REINSTATEMENT;
    }

    public Object[] processCashAcyCall(Map<String, Object> nextDisburseDates,
            AccountProcessingAndRecovery accountProcessingAndRecovery, BilAccount bilAccount,
            AccountReviewDetails accountReviewDetails, ZonedDateTime nextRecoveryDate) {
        String accountId = bilAccount.getAccountId();
        BilActRules bilActRules = accountReviewDetails.getBilActRules();
        BilSupportPlan bilSupportPlan = accountReviewDetails.getBilSupportPlan();
        boolean excludeAchPayment = false;
        boolean exit = false;

        checkCashapplytoCancelPolicy(accountId, accountProcessingAndRecovery);

        if (accountProcessingAndRecovery.getCashStatusCd() == ACTIVE_DISBURSEMENT
                || bilAccount.getCashStatusCd() != SUSPEND_DISBURSEMENT) {
            excludeAchPayment = excludePendingAch(accountId);
        }

        if (accountProcessingAndRecovery.getCashStatusCd() == ACTIVE_DISBURSEMENT
                || bilAccount.getCashStatusCd() != SUSPEND_DISBURSEMENT) {
            if (bilActRules.getBruDsbRctDtInd() != CHAR_Y) {
                nextDisburseDates = billingRulesService.paymentDisburse(bilAccount, bilSupportPlan, nextDisburseDates);
            } else {
                nextDisburseDates = billingRulesService.disburseToSource(bilAccount, bilActRules, nextDisburseDates);
                accountProcessingAndRecovery
                        .setDistributionDate((ZonedDateTime) nextDisburseDates.get(DisburseKeys.BILDTBDATE.toString()));
                accountProcessingAndRecovery.setDistributionSequenceNumber(
                        (Short) nextDisburseDates.get(DisburseKeys.BILDTBSEQUENCE.toString()));
            }

        }

        Integer policyCount = bilPolicyTermRepository.checkIfExistsByBilAccountId(accountId);
        if (policyCount == null || policyCount == 0) {
            exit = processBilAccountHasPolicy(accountId, bilActRules.getBruDsbRctDtInd(), nextRecoveryDate,
                    accountProcessingAndRecovery, false, false, nextDisburseDates);

        }
        Object[] nextDate = { excludeAchPayment, exit };
        logger.debug("Processed processCashAcyCall menthod.");
        return nextDate;
    }

    public boolean excludePendingAch(String accountId) {
        boolean excludeAchPayment = false;
        List<Character> eftReceivedTypeList = Arrays.asList(EftRecordType.RECONCILED_CASH.getValue(),
                EftRecordType.UNIDENTIFIED.getValue(), EftRecordType.ONE_TIME_ACH.getValue(),
                EftRecordType.DIGITAL_WALLET.getValue(), EftRecordType.AGENCY_SWEEP.getValue());
        List<BilEftPendingTape> bilEftPndingTapeList = bilEftPendingTapeRepositoy.findEftpendingTapeRows(accountId,
                CHAR_A, eftReceivedTypeList, DECIMAL_ZERO);
        if (bilEftPndingTapeList != null && !bilEftPndingTapeList.isEmpty()) {
            for (BilEftPendingTape bilEftPndingTape : bilEftPndingTapeList) {

                Integer count = bilCashDspRepository.updateRsusIndByBilAccountId(CHAR_I, accountId,
                        BilDspTypeCode.SUSPENDED.getValue(), bilEftPndingTape.getEftPostPaymentDate(),
                        bilEftPndingTape.getReceiptSequenceNumber(), ManualSuspendIndicator.ENABLED.getValue(),
                        ReverseReSuspendIndicator.BLANK.getValue());
                if (count != null && count != 0) {
                    excludeAchPayment = true;
                }

            }
        }
        return excludeAchPayment;
    }

    @Override
    public void writeHistory(String accountId, String activityDesCd, String activityDesType, String policyNumber,
            String policySymbolCd, ZonedDateTime activityDesc1Dt, ZonedDateTime activityDesc2Dt,
            String additionalDataTxt, String userId, ZonedDateTime recoveryDate) {
        Short bilAcySeq = 0;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, recoveryDate);
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId, recoveryDate.with(LocalTime.MIN), bilAcySeq);
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(policySymbolCd);
        bilActSummary.setPolNbr(policyNumber);
        bilActSummary.setBilAcyDesCd(activityDesCd);
        bilActSummary.setBilDesReaTyp(activityDesType);
        bilActSummary.setBilAcyDes1Dt(activityDesc1Dt);
        bilActSummary.setBilAcyDes2Dt(activityDesc2Dt);
        bilActSummary.setBilAcyAmt(DECIMAL_ZERO);
        bilActSummary.setUserId(userId);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());
        bilActSummary.setBasAddDataTxt(additionalDataTxt);
        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

    private void updatePolicyStatus(String accountId, String policyId, ZonedDateTime se3Date) {
        List<BilPolicyTerm> termlist;
        if (policyId == null || policyId.compareTo("") == 0) {
            termlist = bilPolicyTermRepository.findByAccountid1(accountId);
        } else {
            termlist = bilPolicyTermRepository.findByAccountIdPolicyId(accountId, policyId);
        }
        for (BilPolicyTerm bilPolicyTerm : termlist) {
            if (bilPolicyTerm.getBillPolStatusCd() == CHAR_X || bilPolicyTerm.getBillPolStatusCd() == CHAR_O) {
                Double policyTermBalance = bilIstScheduleRepository.findPolicyTermBalance(
                        bilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                if (policyTermBalance != null && policyTermBalance != 0) {
                    if (bilPolicyTerm.getBillPolStatusCd() == CHAR_X) {
                        updatePolicyTermRow(se3Date, bilPolicyTerm, CHAR_O);
                    }
                } else {
                    updatePolicyTermRow(se3Date, bilPolicyTerm, CHAR_X);
                }
            }
        }
    }

    private void updatePolicyTermRow(ZonedDateTime se3Date, BilPolicyTerm bilPolicyTerm, char policyStatus) {
        bilPolicyTerm.setBillPolStatusCd(policyStatus);
        bilPolicyTerm.setBillSusFuReaCd(BLANK_STRING);
        bilPolicyTerm.setBptStatusEffDt(se3Date);
        bilPolicyTermRepository.save(bilPolicyTerm);
    }
}
