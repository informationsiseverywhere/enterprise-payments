package billing.handler.impl;

import static billing.utils.BillingConstants.SYSTEM_USER_ID;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_E;
import static core.utils.CommonConstants.CHAR_G;
import static core.utils.CommonConstants.CHAR_H;
import static core.utils.CommonConstants.CHAR_I;
import static core.utils.CommonConstants.CHAR_M;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_U;
import static core.utils.CommonConstants.CHAR_V;
import static core.utils.CommonConstants.CHAR_W;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import application.utils.service.ActivityService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilActInquiry;
import billing.data.entity.BilActRules;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilAmounts;
import billing.data.entity.BilBillPlan;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCreSchedule;
import billing.data.entity.BilIstSchedule;
import billing.data.entity.BilPolActivity;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilStRulesUct;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.BilSystemRules;
import billing.data.entity.HalBoMduXrf;
import billing.data.entity.LegalDaysNotice;
import billing.data.entity.id.BilActInquiryId;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilIstScheduleId;
import billing.data.entity.id.BilPolActivityId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilAmountsRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCreScheduleRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilSystemRulesRepository;
import billing.data.repository.HalBoMduXrfRepository;
import billing.data.repository.HalUniversalCtl2Repository;
import billing.data.repository.LegalDaysNoticeRepository;
import billing.handler.impl.PolicyReviewServiceImpl.NsfPcnDataType;
import billing.handler.impl.PolicyReviewServiceImpl.TermAcyType;
import billing.handler.impl.SplitBillingActivityServiceImpl.NoncausalType;
import billing.handler.model.AccountProcessingAndRecovery;
import billing.handler.model.AccountReviewDetails;
import billing.handler.model.Cancellation;
import billing.handler.model.MaxPendingCancellation;
import billing.handler.model.PcnRescindCollection;
import billing.handler.model.PolicyReview;
import billing.handler.service.AccountReviewService;
import billing.handler.service.CalculatePaidToDateService;
import billing.handler.service.CancellationService;
import billing.handler.service.SplitBillingActivityService;
import billing.model.Contract;
import billing.model.PendingCancellationNotice;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants.AccountStatus;
import billing.utils.BillingConstants.BilAmountsEftIndicator;
import billing.utils.BillingConstants.BilDesReasonCode;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.WipAcitivityId;
import billing.utils.BillingConstants.WipObjectCode;
import billing.utils.PB360Service;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;

@Service("cancellationService")
public class CancellationServiceImpl implements CancellationService {

    public static final Logger logger = LogManager.getLogger(CancellationServiceImpl.class);

    @Autowired
    private DateService dateService;

    @Autowired
    private AccountReviewService accountReviewService;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilAmountsRepository bilAmountsRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private HalUniversalCtl2Repository halUniversalCtl2Repository;

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private BilSystemRulesRepository bilSystemRulesRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilCreScheduleRepository bilCreScheduleRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private HalBoMduXrfRepository halBoMduXrfRepository;

    @Autowired
    private LegalDaysNoticeRepository legalDaysNoticeRepository;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private PB360Service pb360Service;

    @Autowired
    private CalculatePaidToDateService calculatePaidToDateService;

    @Autowired
    private SplitBillingActivityService splitBillingActivityService;

    @Autowired
    private BillingApiService billingApiService;

    private static final String CANTT_USER = "SYSCANTT";
    private static final char ACCT_IS_SPLIT = CHAR_Y;
    private static final char EFT = CHAR_Y;
    private static final char CANCEL_PCN_REQ = CHAR_Y;
    private static final char AUTO_COL_MTH_CHG = CHAR_Y;
    private static final char POL_TERM_TYPE_CD_RN_1 = 'G';
    private static final char POL_TERM_TYPE_CD_RN_2 = 'R';
    private static final char POL_TERM_TYPE_CD_RN_3 = 'U';
    private static final char POL_TERM_TYPE_CD_RN_4 = 'W';
    private static final char POL_TERM_TYPE_CD_RN_5 = '9';
    private static final char NO_RESP_RENEWAL = 'R';
    private static final char LAPSE_CANC = CHAR_Y;
    private static final char SEND_ALT_COL_MTH = CHAR_Y;
    private static final char PREMIUM_ACY = CHAR_Y;
    private static final char CANCEL_EQUITY = 'E';
    private static final char CANTT_RULE_ACTIVE = CHAR_Y;
    private static final char MINUS_SIGN = '-';
    private static final char PLUS_SIGN = '+';
    private static final String BCMOAX = "BCMOAX";
    private boolean legalDaysFndInd = false;

    @Override
    public Object[] processNsfPcn(boolean isPcnmRule, MaxPendingCancellation maxPendingCancellation,
            BilAccount bilAccount, BilPolicy bilPolicy, BilPolicyTerm bilPolicyTerm, BilBillPlan bilBillPlan,
            AccountReviewDetails accountReviewDetails, Cancellation cancellation, Map<String, Object> pedAcyMap,
            Map<String, Object> termDataMap, AccountProcessingAndRecovery accountProcessingAndRecovery,
            Map<String, Object> pcnnDataMap) {
        char bilPolicyStatusCode = bilPolicyTerm.getBillPolStatusCd();
        char bisTermInvoiceIndicator = (char) termDataMap.get(TermAcyType.TERMINVOICEINDICATOR.getValue());
        BilActRules bilActRules = accountReviewDetails.getBilActRules();
        BilSupportPlan bilSupportPlan = accountReviewDetails.getBilSupportPlan();

        cancellation: {
            if (checkIsCanttTerm(cancellation.getAccountId(), bilPolicyTerm)) {
                break cancellation;
            }
            if (checkIsNewBusinessTerm(cancellation.getAccountId(), bilPolicyTerm, bisTermInvoiceIndicator,
                    bilActRules.getBruNsfCncInd(), bilBillPlan.getBbpInvDuringPcnInd())) {
                break cancellation;
            }
            Object[] nsfPcn = nsfPcn(isPcnmRule, maxPendingCancellation, bilAccount, bilPolicyTerm, bilPolicy,
                    bilBillPlan, bilSupportPlan, cancellation, pedAcyMap, pcnnDataMap, termDataMap,
                    accountProcessingAndRecovery);
            bilPolicyStatusCode = (char) nsfPcn[0];
            if ((MaxPendingCancellation) nsfPcn[1] != null) {
                maxPendingCancellation = (MaxPendingCancellation) nsfPcn[1];
            }
        }
        return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
    }

    private Object[] nsfPcn(boolean isPcnmRule, MaxPendingCancellation maxPendingCancellation, BilAccount bilAccount,
            BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy, BilBillPlan bilBillPlan, BilSupportPlan bilSupportPlan,
            Cancellation cancellation, Map<String, Object> pedAcyMap, Map<String, Object> pcnnDataMap,
            Map<String, Object> termDataMap, AccountProcessingAndRecovery accountProcessingAndRecovery) {

        boolean termNeverBilled = (boolean) termDataMap.get(NsfPcnDataType.TERMNEVERBILLED.getValue());
        double nsfPcnPercent = (double) termDataMap.get(NsfPcnDataType.NSFPCNPERCENT.getValue());
        double bilIstDueAmount = (double) termDataMap.get(NsfPcnDataType.BILISTDUEAMOUNT.getValue());
        double nsfPcnAmount = (double) termDataMap.get(NsfPcnDataType.NSFPCNAMOUNT.getValue());
        short ldnMailDays = SHORT_ZERO;
        short ldnLegDays = SHORT_ZERO;
        short ldnMaxDays = SHORT_ZERO;
        char bilPolicyStatusCode = bilPolicyTerm.getBillPolStatusCd();

        char splitBillInd = accountReviewService.checkSplitIndicator(bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
        if (splitBillInd == CHAR_Y && maxPendingCancellation != null) {
            maxPendingCancellation.setSplitPolicy(true);
        }

        char polTermTypeCode = getPolTermTypeCode(bilAccount.getAccountId(),
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());

        ZonedDateTime pedAcrEquityDate = DateRoutine.defaultDateTime();
        ZonedDateTime standCncDate;

        if (polTermTypeCode != BLANK_CHAR) {
            BilStRulesUct bilStRulesUct = accountReviewService.getBilStRuleUct("PCNN",
                    bilPolicyTerm.getBillStatePvnCd(), bilPolicyTerm.getLobCd(), bilPolicyTerm.getMasterCompanyNbr());
            if (bilStRulesUct != null && bilStRulesUct.getBilRuleCd() == CHAR_Y) {
                char bypassPcnNsfInd = evaluatePcnn(polTermTypeCode, bilAccount.getAccountId(), bilPolicyTerm,
                        bilStRulesUct.getBilParmListTxt(), pcnnDataMap, accountProcessingAndRecovery.getRecoveryDate());
                char pcnFlatCncInd = (char) pcnnDataMap.get(PcnnType.PCNFLATCNCIND.getValue());

                if (bypassPcnNsfInd != CHAR_Y && pcnFlatCncInd == CHAR_Y) {
                    pedAcrEquityDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
                    standCncDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
                    pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), pedAcrEquityDate);

                    if (isPcnmRule && maxPendingCancellation != null) {
                        loadPcnmNsfPcn(termNeverBilled, nsfPcnAmount, bilIstDueAmount, maxPendingCancellation,
                                pedAcyMap, pcnnDataMap, ldnMaxDays, ldnMailDays, ldnLegDays);
                    } else {
                        bilPolicyStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan,
                                standCncDate, polTermTypeCode, splitBillInd, bilSupportPlan.getBspPndCncNbr(),
                                cancellation, pedAcyMap, pcnnDataMap, termDataMap, accountProcessingAndRecovery);
                        if (splitBillInd == ACCT_IS_SPLIT) {
                            callSplitBillingActivity(bilAccount.getAccountId(), NoncausalType.PCN_REQUEST.toString(),
                                    cancellation.isPolicyPedValued(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                        }
                    }
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                }
            }
        }
        pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), pedAcrEquityDate);

        Object[] detectMinPcnObject = null;
        char pedEquityInd = (char) pedAcyMap.get(PedAcyType.EQUITYIND.getValue());
        ZonedDateTime pedPolEffDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.POLEFFDATE.getValue());
        ZonedDateTime pedPolExpDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.POLEXPDATE.getValue());
        ZonedDateTime pedNextAcyDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.NEXTACYDATE.getValue());

        if (pedEquityInd != CHAR_Y) {
            detectMinPcnObject = detectMinPcnDate("PNP", pedEquityInd, pedPolEffDate,
                    accountProcessingAndRecovery.getRecoveryDate(), bilBillPlan, bilPolicyTerm, bilPolicy);
        } else if (pedPolEffDate.compareTo(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()) != 0
                || pedPolExpDate.compareTo(bilPolicyTerm.getPlnExpDt()) == 0
                        && pedNextAcyDate.compareTo(pedPolExpDate) == 0) {
            return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
        }

        pedAcyMap.put(PedAcyType.NEXTACYDATE.getValue(), pedAcyMap.get(PedAcyType.NSFACYDATE.getValue()));

        if (pedEquityInd == CHAR_Y
                && pedPolEffDate.compareTo(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()) == 0) {
            detectMinPcnObject = detectMinPcnDate("PNP", pedEquityInd, pedPolEffDate,
                    accountProcessingAndRecovery.getRecoveryDate(), bilBillPlan, bilPolicyTerm, bilPolicy);
        }

        ZonedDateTime minPcnDate = (ZonedDateTime) detectMinPcnObject[0];
        standCncDate = (ZonedDateTime) detectMinPcnObject[1];
        ZonedDateTime pedMinDate = (ZonedDateTime) detectMinPcnObject[2];
        ZonedDateTime pedMaxDate = (ZonedDateTime) detectMinPcnObject[3];
        ldnMaxDays = (short) detectMinPcnObject[4];
        ldnMailDays = (short) detectMinPcnObject[5];
        ldnLegDays = (short) detectMinPcnObject[6];

        char polTrmEpcnRule = BLANK_CHAR;
        BilStRulesUct sberStRule = accountReviewService.getBilStRuleUct("SBER", bilPolicyTerm.getBillStatePvnCd(),
                bilPolicyTerm.getLobCd(), bilPolicyTerm.getMasterCompanyNbr());
        if (sberStRule != null) {
            polTrmEpcnRule = sberStRule.getBilParmListTxt().trim().charAt(0);
        }

        boolean isPolicyPedValued;
        if (pedNextAcyDate.compareTo(pedMinDate) <= 0) {
            pedAcrEquityDate = pedMinDate;
            Object[] policyPedObject = detectPolicyPedValued(bilAccount.getAccountId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), cancellation.getReviewDatesIndicator(),
                    splitBillInd, sberStRule, polTrmEpcnRule, pedMinDate, pedPolEffDate, pedPolExpDate,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            isPolicyPedValued = (boolean) policyPedObject[0];
            if (isPolicyPedValued) {
                pedAcrEquityDate = (ZonedDateTime) policyPedObject[1];
            }
        } else {
            if (pedNextAcyDate.compareTo(pedMaxDate) > 0
                    || accountProcessingAndRecovery.getRecoveryDate().compareTo(minPcnDate) < 0
                            && minPcnDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
                if (accountProcessingAndRecovery.getDriverIndicator() != CHAR_Y) {
                    setNrvDateTrigger(bilAccount.getAccountId(), pedNextAcyDate, minPcnDate, ldnLegDays, ldnMailDays,
                            bilAccount, bilPolicy, bilPolicyTerm, accountProcessingAndRecovery.getRecoveryDate());
                }
                return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
            } else {
                Object[] policyPedObject = detectPolicyPedValued(bilAccount.getAccountId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(), cancellation.getReviewDatesIndicator(),
                        splitBillInd, sberStRule, polTrmEpcnRule, pedMaxDate, pedPolEffDate, pedPolExpDate,
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                isPolicyPedValued = (boolean) policyPedObject[0];
                if (pedNextAcyDate.compareTo(pedMinDate) > 0 && pedNextAcyDate.compareTo(pedMaxDate) <= 0
                        || accountProcessingAndRecovery.getRecoveryDate().compareTo(minPcnDate) >= 0
                                && minPcnDate.compareTo(DateRoutine.defaultDateTime()) != 0
                                && (sberStRule.getBilRuleCd() == CHAR_N || polTrmEpcnRule == '1')) {
                    pedAcrEquityDate = pedNextAcyDate;
                }
                if (isPolicyPedValued) {
                    pedAcrEquityDate = (ZonedDateTime) policyPedObject[1];
                }
            }
        }
        cancellation.setPolicyPedValued(isPolicyPedValued);
        pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), pedAcrEquityDate);
        if (bilBillPlan.getBbpPndCncInd() == CHAR_Y
                || bilPolicyTerm.getPlnExpDt().compareTo(accountProcessingAndRecovery.getRecoveryDate()) == 0
                || bilPolicyTerm.getPlnExpDt().compareTo(accountProcessingAndRecovery.getRecoveryDate()) < 0
                || nsfPcnAmount == 0 && nsfPcnPercent == 0 || cancellation.getEftCollectionIndicator() == EFT
                        && bilAccount.getStatus() == AccountStatus.ACTIVE.toChar()) {
            return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
        }

        if (pedEquityInd == CHAR_Y && pedAcrEquityDate.compareTo(pedMaxDate) <= 0) {
            if (bilBillPlan.getBbpPndCncAmt() == 0 && bilBillPlan.getBbpPndCncPct() != 0
                    && nsfPcnPercent >= bilBillPlan.getBbpPndCncPct()
                    || bilBillPlan.getBbpPndCncAmt() != 0 && bilBillPlan.getBbpPndCncPct() == 0
                            && nsfPcnAmount >= bilBillPlan.getBbpPndCncAmt()
                    || nsfPcnAmount >= bilBillPlan.getBbpPndCncAmt()
                            && nsfPcnPercent >= bilBillPlan.getBbpPndCncPct()) {
                bilPolicyStatusCode = pendingCancel(splitBillInd, isPcnmRule, minPcnDate, bilPolicyTerm, bilAccount,
                        bilPolicy, bilBillPlan, standCncDate, polTermTypeCode, bilSupportPlan.getBspPndCncNbr(),
                        cancellation, pedAcyMap, pcnnDataMap, termDataMap, accountProcessingAndRecovery,
                        maxPendingCancellation, ldnLegDays, ldnMailDays, ldnMaxDays);
            }
        } else if (bilBillPlan.getBbpPndCncAmt() == 0 && bilBillPlan.getBbpPndCncPct() != 0
                && nsfPcnPercent >= bilBillPlan.getBbpPndCncPct()
                || bilBillPlan.getBbpPndCncAmt() != 0 && bilBillPlan.getBbpPndCncPct() == 0
                        && nsfPcnAmount >= bilBillPlan.getBbpPndCncAmt()
                || nsfPcnAmount >= bilBillPlan.getBbpPndCncAmt() && nsfPcnPercent >= bilBillPlan.getBbpPndCncPct()) {
            bilPolicyStatusCode = standCancel(bilAccount, bilPolicyTerm, bilPolicy, standCncDate, polTermTypeCode,
                    bilBillPlan, splitBillInd, minPcnDate, isPcnmRule, bilSupportPlan.getBspPndCncNbr(), cancellation,
                    pedAcyMap, pcnnDataMap, termDataMap, accountProcessingAndRecovery, maxPendingCancellation,
                    ldnLegDays, ldnMailDays, ldnMaxDays);
        }

        return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
    }

    @Override
    public Object[] processPnpPcn(boolean isPcnmRule, char useCrgDateIndicator, BilAccount bilAccount,
            BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy, BilBillPlan bilBillPlan,
            AccountProcessingAndRecovery accountProcessingAndRecovery, AccountReviewDetails accountReviewDetails,
            MaxPendingCancellation maxPendingCancellation, Cancellation cancellation, PolicyReview policyReview,
            Map<String, Object> pedAcyMap, Map<String, Object> pcnnDataMap, Map<String, Object> termDataMap,
            List<Contract> contractList) {

        BilSupportPlan bilSupportPlan = accountReviewDetails.getBilSupportPlan();

        ZonedDateTime defaultDateTime = DateRoutine.defaultDateTime();
        ZonedDateTime polExpirationDate = bilPolicyTerm.getPlnExpDt();
        ZonedDateTime se3Date = accountProcessingAndRecovery.getRecoveryDate();
        ZonedDateTime polEffectiveDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
        char bilPolicyStatusCode = bilPolicyTerm.getBillPolStatusCd();
        char pedEquityInd = (char) pedAcyMap.get(PedAcyType.EQUITYIND.getValue());
        String policyNumber = bilPolicy.getPolNbr();
        String accountId = bilAccount.getAccountId();
        String policyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
        Short ldnMaxDays = SHORT_ZERO;
        Short ldnMailDays = SHORT_ZERO;
        Short ldnLegDays = SHORT_ZERO;
        double bbpPndCncAmt = bilBillPlan.getBbpPndCncAmt();
        double bbpPndCncPct = bilBillPlan.getBbpPndCncPct();

        char splitBillInd = accountReviewService.checkSplitIndicator(policyId, polEffectiveDate);
        if (splitBillInd == CHAR_Y && maxPendingCancellation != null) {
            maxPendingCancellation.setSplitPolicy(true);
        }

        if (bilBillPlan.getBbpPndCncInd() == CHAR_Y) {
            return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
        }

        Object[] detectMinPcnObject = null;
        ZonedDateTime nextReviewDate = policyReview.getNextReviewDate();
        ZonedDateTime standCncDate = defaultDateTime;
        ZonedDateTime minPcnDate = defaultDateTime;
        ZonedDateTime pedMinDate;
        ZonedDateTime pedMaxDate;
        ZonedDateTime pedPolEffDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.POLEFFDATE.getValue());
        ZonedDateTime pedPolExpDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.POLEXPDATE.getValue());
        ZonedDateTime pedNextAcyDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.NEXTACYDATE.getValue());
        ZonedDateTime termPcnDate = (ZonedDateTime) termDataMap.get(TermAcyType.TERMPCNDATE.getValue());

        if (pedEquityInd != CHAR_Y) {
            getLegalDays(accountProcessingAndRecovery.getPendingCancelType(), bilBillPlan, bilPolicyTerm, bilPolicy);
            if (!legalDaysFndInd && (nextReviewDate.isBefore(se3Date) || nextReviewDate.equals(se3Date))) {
                nextReviewDate = DateRoutine.getAdjustedDate(se3Date, false, 1, null);
                termPcnDate = defaultDateTime;
                policyReview.setNextReviewDate(nextReviewDate);
                termDataMap.put(TermAcyType.TERMPCNDATE.getValue(), termPcnDate);
                return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
            } else {
                detectMinPcnObject = detectMinPcnDate(accountProcessingAndRecovery.getPendingCancelType(), pedEquityInd,
                        pedPolEffDate, se3Date, bilBillPlan, bilPolicyTerm, bilPolicy);
                minPcnDate = (ZonedDateTime) detectMinPcnObject[0];
                standCncDate = (ZonedDateTime) detectMinPcnObject[1];
                ldnMaxDays = (Short) detectMinPcnObject[4];
                ldnMailDays = (Short) detectMinPcnObject[5];
                ldnLegDays = (Short) detectMinPcnObject[6];
                if (maxPendingCancellation != null) {
                    maxPendingCancellation.setStandredCancellationDate(standCncDate);
                    maxPendingCancellation.setMinPendingCancellationDate(minPcnDate);
                }
            }
        }

        ZonedDateTime pedAcrEquityDate = DateRoutine.defaultDateTime();
        boolean isPolicyPedValued;

        char polTrmEpcnRule = BLANK_CHAR;
        BilStRulesUct sberStRule = accountReviewService.getBilStRuleUct("SBER", bilPolicyTerm.getBillStatePvnCd(),
                bilPolicyTerm.getLobCd(), bilPolicyTerm.getMasterCompanyNbr());
        if (sberStRule != null) {
            polTrmEpcnRule = sberStRule.getBilParmListTxt().trim().charAt(0);
        }

        if (pedEquityInd == CHAR_Y && pedPolEffDate.equals(polEffectiveDate)) {
            getLegalDays(accountProcessingAndRecovery.getPendingCancelType(), bilBillPlan, bilPolicyTerm, bilPolicy);
            if (!legalDaysFndInd && (nextReviewDate.isBefore(se3Date) || nextReviewDate.equals(se3Date))) {
                nextReviewDate = DateRoutine.getAdjustedDate(se3Date, false, 1, null);
                termPcnDate = defaultDateTime;
                policyReview.setNextReviewDate(nextReviewDate);
                termDataMap.put(TermAcyType.TERMPCNDATE.getValue(), termPcnDate);
                return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
            } else {
                detectMinPcnObject = detectMinPcnDate(accountProcessingAndRecovery.getPendingCancelType(), pedEquityInd,
                        pedPolEffDate, se3Date, bilBillPlan, bilPolicyTerm, bilPolicy);
                minPcnDate = (ZonedDateTime) detectMinPcnObject[0];
                standCncDate = (ZonedDateTime) detectMinPcnObject[1];
                pedMinDate = (ZonedDateTime) detectMinPcnObject[2];
                pedMaxDate = (ZonedDateTime) detectMinPcnObject[3];
                ldnMaxDays = (Short) detectMinPcnObject[4];
                ldnMailDays = (Short) detectMinPcnObject[5];
                ldnLegDays = (Short) detectMinPcnObject[6];
                if (maxPendingCancellation != null) {
                    maxPendingCancellation.setStandredCancellationDate(standCncDate);
                    maxPendingCancellation.setMinPendingCancellationDate(minPcnDate);
                }
                if (pedNextAcyDate.isBefore(pedMinDate) || pedNextAcyDate.equals(pedMinDate)) {
                    if (pedMinDate.isBefore(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt())
                            && accountProcessingAndRecovery.getDriverIndicator() == CHAR_O) {
                        pedMinDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
                    }
                    pedAcrEquityDate = pedMinDate;
                    Object[] policyPedObject = detectPolicyPedValued(accountId, policyId,
                            cancellation.getReviewDatesIndicator(), splitBillInd, sberStRule, polTrmEpcnRule,
                            pedMinDate, pedPolEffDate, pedPolExpDate, polEffectiveDate);
                    isPolicyPedValued = (boolean) policyPedObject[0];
                    if (isPolicyPedValued) {
                        pedAcrEquityDate = (ZonedDateTime) policyPedObject[1];
                    }
                } else {
                    if (pedNextAcyDate.isAfter(pedMaxDate)
                            || minPcnDate.isAfter(se3Date) && !minPcnDate.equals(defaultDateTime)) {
                        setRevDate(accountProcessingAndRecovery, policyReview, termDataMap, pedAcyMap, ldnMaxDays,
                                useCrgDateIndicator);
                        if (maxPendingCancellation != null) {
                            maxPendingCancellation.setEquityDate(pedNextAcyDate);
                        }
                        return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                    } else {
                        Object[] policyPedObject = detectPolicyPedValued(accountId, policyId,
                                cancellation.getReviewDatesIndicator(), splitBillInd, sberStRule, polTrmEpcnRule,
                                pedMaxDate, pedPolEffDate, pedPolExpDate, polEffectiveDate);
                        isPolicyPedValued = (boolean) policyPedObject[0];
                        if (pedNextAcyDate.isAfter(pedMinDate)
                                && (pedNextAcyDate.isBefore(pedMaxDate) || pedNextAcyDate.equals(pedMaxDate))
                                || (minPcnDate.isBefore(se3Date) || minPcnDate.equals(se3Date))
                                        && !minPcnDate.equals(defaultDateTime)
                                        && (sberStRule.getBilRuleCd() == CHAR_N || polTrmEpcnRule == '1')) {
                            pedAcrEquityDate = pedNextAcyDate;
                        }
                        if (isPolicyPedValued) {
                            pedAcrEquityDate = (ZonedDateTime) policyPedObject[1];
                        }
                    }
                }
            }
        } else if (pedEquityInd == CHAR_Y && !pedPolEffDate.equals(polEffectiveDate)) {
            detectMinPcnObject = detectMinPcnDate(accountProcessingAndRecovery.getPendingCancelType(), pedEquityInd,
                    polEffectiveDate, se3Date, bilBillPlan, bilPolicyTerm, bilPolicy);
            minPcnDate = (ZonedDateTime) detectMinPcnObject[0];
            standCncDate = (ZonedDateTime) detectMinPcnObject[1];
            pedMinDate = (ZonedDateTime) detectMinPcnObject[2];
            ldnMaxDays = (Short) detectMinPcnObject[4];
            ldnMailDays = (Short) detectMinPcnObject[5];
            ldnLegDays = (Short) detectMinPcnObject[6];
            if (maxPendingCancellation != null) {
                maxPendingCancellation.setStandredCancellationDate(standCncDate);
                maxPendingCancellation.setMinPendingCancellationDate(minPcnDate);
            }
            if (pedNextAcyDate.isBefore(pedMinDate) || pedNextAcyDate.equals(pedMinDate)) {
                if (pedMinDate.isBefore(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt())
                        && accountProcessingAndRecovery.getDriverIndicator() == CHAR_O) {
                    pedMinDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
                }
                pedAcrEquityDate = pedMinDate;
                Object[] policyPedObject = detectPolicyPedValued(accountId, policyId,
                        cancellation.getReviewDatesIndicator(), splitBillInd, sberStRule, polTrmEpcnRule, pedMinDate,
                        pedPolEffDate, pedPolExpDate, polEffectiveDate);
                isPolicyPedValued = (boolean) policyPedObject[0];
                if (isPolicyPedValued) {
                    pedAcrEquityDate = (ZonedDateTime) policyPedObject[1];
                }
            }
        }

        pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), pedAcrEquityDate);

        ZonedDateTime termPastDate = (ZonedDateTime) termDataMap.get(TermAcyType.TERMPASTDATE.getValue());
        Double termPremiumAmount = (Double) termDataMap.get(TermAcyType.TERMPREMIUMAMOUNT.getValue());
        char polTermTypeCode = getPolTermTypeCode(accountId, policyId, polEffectiveDate);

        for (Contract contract : contractList) {
            if (contract.getPolicyId().equalsIgnoreCase(policyId)) {
                if (isPcnmRule && maxPendingCancellation != null) {
                    maxPendingCancellation = loadPcnmArv(maxPendingCancellation, termDataMap, pedAcyMap, ldnMaxDays,
                            ldnMailDays, ldnLegDays);
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                } else {
                    bilPolicyStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan, standCncDate,
                            polTermTypeCode, splitBillInd, bilSupportPlan.getBspPndCncNbr(), cancellation, pedAcyMap,
                            pcnnDataMap, termDataMap, accountProcessingAndRecovery);
                    if (splitBillInd == ACCT_IS_SPLIT) {
                        callSplitBillingActivity(accountId, NoncausalType.PCN_REQUEST.toString(),
                                cancellation.isPolicyPedValued(), policyId, polEffectiveDate);
                    }
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                }
            }
        }

        label02: {
            if (maxPendingCancellation != null) {
                maxPendingCancellation.setEquityDate(pedNextAcyDate);
            }
            if (polExpirationDate.equals(se3Date) || polExpirationDate.isBefore(se3Date)
                    || termPcnDate.equals(defaultDateTime) || termPastDate.equals(defaultDateTime)
                    || termPremiumAmount == 0
                    || cancellation.getEftCollectionIndicator() == EFT && bilAccount.getStatus() == CHAR_A) {
                return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
            }
            break label02;
        }

        double termPastAmount = (double) termDataMap.get(TermAcyType.TERMPASTAMOUNT.getValue());

        if (termPcnDate.isBefore(se3Date) || termPcnDate.equals(se3Date)) {
            if (bbpPndCncAmt == 0 && bbpPndCncPct == 0 && termPastAmount > 0) {
                if ((standCncDate.isAfter(polExpirationDate) || standCncDate.equals(polExpirationDate))
                        && bilBillPlan.getBbpInvDuringPcnInd() == CHAR_N
                        || (pedAcrEquityDate.isAfter(polExpirationDate) || pedAcrEquityDate.equals(polExpirationDate))
                                && !pedAcrEquityDate.equals(defaultDateTime)) {
                    char cancelPcnReqInd = checkForPrevHistory("CPE", accountId, policyNumber,
                            bilPolicy.getPolSymbolCd(), polEffectiveDate, polExpirationDate);
                    if (cancelPcnReqInd == CANCEL_PCN_REQ) {
                        wipLink(accountId, bilAccount.getAccountNumber(), WipAcitivityId.CANCELPCN.getValue());
                    }
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                }
                if (minPcnDate.isAfter(se3Date) && !minPcnDate.equals(defaultDateTime)
                        && accountProcessingAndRecovery.getDriverIndicator() != NO_RESP_RENEWAL) {
                    char cancelPcnReq2Ind = checkForPrevHistory("CPN", accountId, policyNumber,
                            bilPolicy.getPolSymbolCd(), polEffectiveDate, polExpirationDate);
                    if (cancelPcnReq2Ind == CANCEL_PCN_REQ) {
                        wipLink(accountId, bilAccount.getAccountNumber(), WipAcitivityId.CANCELPCN2.getValue());
                    }
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                }
                if (isPcnmRule && maxPendingCancellation != null) {
                    maxPendingCancellation = loadPcnmArv(maxPendingCancellation, termDataMap, pedAcyMap, ldnMaxDays,
                            ldnMailDays, ldnLegDays);
                } else {
                    bilPolicyStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan, standCncDate,
                            polTermTypeCode, splitBillInd, bilSupportPlan.getBspPndCncNbr(), cancellation, pedAcyMap,
                            pcnnDataMap, termDataMap, accountProcessingAndRecovery);
                    if (splitBillInd == ACCT_IS_SPLIT) {
                        callSplitBillingActivity(accountId, NoncausalType.PCN_REQUEST.toString(),
                                cancellation.isPolicyPedValued(), policyId, polEffectiveDate);
                    }
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                }
                return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
            } else {
                if (bbpPndCncAmt == 0 && bbpPndCncPct == 0 && termPastAmount == 0) {
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                }
            }
        }

        double termPastPercent = (double) termDataMap.get(TermAcyType.TERMPASTPERCENT.getValue());

        if (termPcnDate.isBefore(se3Date) || termPcnDate.equals(se3Date)) {
            if (bbpPndCncAmt == 0 && bilBillPlan.getBbpPndCncPct() != 0 && termPastPercent >= bbpPndCncPct) {
                if ((standCncDate.isAfter(polExpirationDate) || standCncDate.equals(polExpirationDate))
                        && bilBillPlan.getBbpInvDuringPcnInd() == CHAR_N
                        || (pedAcrEquityDate.isAfter(bilPolicyTerm.getPlnExpDt())
                                || pedAcrEquityDate.equals(polExpirationDate))
                                && !pedAcrEquityDate.equals(defaultDateTime)) {
                    char cancelPcnReqInd = checkForPrevHistory("CPE", accountId, policyNumber,
                            bilPolicy.getPolSymbolCd(), polEffectiveDate, polExpirationDate);
                    if (cancelPcnReqInd == CANCEL_PCN_REQ) {
                        wipLink(accountId, bilAccount.getAccountNumber(), WipAcitivityId.CANCELPCN.getValue());
                    }
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                }
                if (minPcnDate.isAfter(se3Date) && !minPcnDate.equals(defaultDateTime)
                        && accountProcessingAndRecovery.getDriverIndicator() != NO_RESP_RENEWAL) {
                    char cancelPcnReq2Ind = checkForPrevHistory("CPN", accountId, policyNumber,
                            bilPolicy.getPolSymbolCd(), polEffectiveDate, polExpirationDate);
                    if (cancelPcnReq2Ind == CANCEL_PCN_REQ) {
                        wipLink(accountId, bilAccount.getAccountNumber(), WipAcitivityId.CANCELPCN2.getValue());
                    }
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                }
                if (isPcnmRule && maxPendingCancellation != null) {
                    maxPendingCancellation = loadPcnmArv(maxPendingCancellation, termDataMap, pedAcyMap, ldnMaxDays,
                            ldnMailDays, ldnLegDays);
                } else {
                    bilPolicyStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan, standCncDate,
                            polTermTypeCode, splitBillInd, bilSupportPlan.getBspPndCncNbr(), cancellation, pedAcyMap,
                            pcnnDataMap, termDataMap, accountProcessingAndRecovery);
                    if (splitBillInd == ACCT_IS_SPLIT) {
                        callSplitBillingActivity(accountId, NoncausalType.PCN_REQUEST.toString(),
                                cancellation.isPolicyPedValued(), policyId, polEffectiveDate);
                    }
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                }
                return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
            } else {
                if (bbpPndCncAmt == 0 && bbpPndCncPct != 0 && termPastPercent < bbpPndCncPct) {
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                }
            }
        }

        if ((termPcnDate.isBefore(se3Date) || termPcnDate.equals(se3Date))
                && (termPastAmount >= bbpPndCncAmt && termPastPercent >= bbpPndCncPct)) {
            if ((standCncDate.isAfter(polExpirationDate) || standCncDate.equals(polExpirationDate))
                    && bilBillPlan.getBbpInvDuringPcnInd() == CHAR_N
                    || (pedAcrEquityDate.isAfter(polExpirationDate) || pedAcrEquityDate.equals(polExpirationDate))
                            && !pedAcrEquityDate.equals(defaultDateTime)) {
                char cancelPcnReqInd = checkForPrevHistory("CPE", accountId, policyNumber, bilPolicy.getPolSymbolCd(),
                        polEffectiveDate, polExpirationDate);
                if (cancelPcnReqInd == CANCEL_PCN_REQ) {
                    wipLink(accountId, bilAccount.getAccountNumber(), WipAcitivityId.CANCELPCN.getValue());
                }
                return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
            }
            if (minPcnDate.isAfter(se3Date) && !minPcnDate.equals(defaultDateTime)
                    && accountProcessingAndRecovery.getDriverIndicator() != NO_RESP_RENEWAL) {
                char cancelPcnReq2Ind = checkForPrevHistory("CPN", accountId, policyNumber, bilPolicy.getPolSymbolCd(),
                        polEffectiveDate, polExpirationDate);
                if (cancelPcnReq2Ind == CANCEL_PCN_REQ) {
                    wipLink(accountId, bilAccount.getAccountNumber(), WipAcitivityId.CANCELPCN2.getValue());
                }
                return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
            }
            if (isPcnmRule && maxPendingCancellation != null) {
                maxPendingCancellation = loadPcnmArv(maxPendingCancellation, termDataMap, pedAcyMap, ldnMaxDays,
                        ldnMailDays, ldnLegDays);
            } else {
                bilPolicyStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan, standCncDate,
                        polTermTypeCode, splitBillInd, bilSupportPlan.getBspPndCncNbr(), cancellation, pedAcyMap,
                        pcnnDataMap, termDataMap, accountProcessingAndRecovery);
                if (splitBillInd == ACCT_IS_SPLIT) {
                    callSplitBillingActivity(accountId, NoncausalType.PCN_REQUEST.toString(),
                            cancellation.isPolicyPedValued(), policyId, polEffectiveDate);
                }
                return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
            }
            return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
        }
        return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object[] processRnpPcn(boolean isPcnmRule, char useCrgDateIndicator, char renewDpCancInd,
            ZonedDateTime termRnpDate, ZonedDateTime firstInstDueDt, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm,
            BilPolicy bilPolicy, BilBillPlan bilBillPlan, AccountProcessingAndRecovery accountProcessingAndRecovery,
            AccountReviewDetails accountReviewDetails, MaxPendingCancellation maxPendingCancellation,
            Cancellation cancellation, PolicyReview policyReview, Map<String, Object> pedAcyMap,
            Map<String, Object> termDataMap, Map<String, Object> pcnnDataMap, char lapseCancIndicator,
            List<Contract> contractList) {

        BilSupportPlan bilSupportPlan = accountReviewDetails.getBilSupportPlan();

        char bilPolicyStatusCode = bilPolicyTerm.getBillPolStatusCd();
        String accountId = bilAccount.getAccountId();
        String policyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
        ZonedDateTime polEffectiveDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
        double termPastAmount = (double) termDataMap.get(TermAcyType.TERMPASTAMOUNT.getValue());
        double termPastPercent = (double) termDataMap.get(TermAcyType.TERMPASTPERCENT.getValue());

        char splitBillInd = accountReviewService.checkSplitIndicator(policyId, polEffectiveDate);
        if (splitBillInd == CHAR_Y && maxPendingCancellation != null) {
            maxPendingCancellation.setSplitPolicy(true);
        }

        if (isPcnmRule && maxPendingCancellation != null) {
            maxPendingCancellation.setPendingCancelFlatCnc(CHAR_N);
            accountProcessingAndRecovery
                    .setPendingCancelType(BilDesReasonType.PRORATA_CANCELLATION_NONPAYMENT_PREMIUMS.getValue());
        }

        Object[] detectMinPcnObject = null;
        char pedEquityInd = (char) pedAcyMap.get(PedAcyType.EQUITYIND.getValue());
        Short ldnMaxDays;
        Short ldnMailDays;
        Short ldnLegDays;
        ZonedDateTime defaultDateTime = DateRoutine.defaultDateTime();
        ZonedDateTime nextReviewDate = policyReview.getNextReviewDate();
        ZonedDateTime se3Date = accountProcessingAndRecovery.getRecoveryDate();
        ZonedDateTime pedPolEffDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.POLEFFDATE.getValue());
        ZonedDateTime pedNextAcyDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.NEXTACYDATE.getValue());
        ZonedDateTime standCncDate;
        ZonedDateTime minPcnDate;
        ZonedDateTime pedMaxDate;
        detectMinPcnObject = detectMinPcnDate(accountProcessingAndRecovery.getPendingCancelType(), pedEquityInd,
                pedPolEffDate, se3Date, bilBillPlan, bilPolicyTerm, bilPolicy);
        minPcnDate = (ZonedDateTime) detectMinPcnObject[0];
        standCncDate = (ZonedDateTime) detectMinPcnObject[1];
        pedMaxDate = (ZonedDateTime) detectMinPcnObject[3];
        ldnMaxDays = (Short) detectMinPcnObject[4];
        ldnMailDays = (Short) detectMinPcnObject[5];
        ldnLegDays = (Short) detectMinPcnObject[6];

        if (pedNextAcyDate.isAfter(pedMaxDate) || minPcnDate.isAfter(se3Date) && !minPcnDate.equals(defaultDateTime)) {
            if (pedEquityInd == CHAR_Y) {
                Object[] revDate = setRevDate(accountProcessingAndRecovery, policyReview, termDataMap, pedAcyMap,
                        ldnMaxDays, useCrgDateIndicator);
                policyReview = (PolicyReview) revDate[0];
                termDataMap = (Map<String, Object>) revDate[1];
                nextReviewDate = policyReview.getNextReviewDate();
            } else {
                if (minPcnDate.isBefore(nextReviewDate)) {
                    nextReviewDate = minPcnDate;
                } else {
                    if (minPcnDate.isAfter(nextReviewDate)
                            && (nextReviewDate.isBefore(se3Date) || nextReviewDate.equals(se3Date))) {
                        nextReviewDate = minPcnDate;
                    }
                }
            }
            if (maxPendingCancellation != null) {
                maxPendingCancellation.setEquityDate(pedNextAcyDate);
            }
            return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
        }

        if (bilBillPlan.getBbpPndCncInd() == CHAR_Y
                || (bilPolicyTerm.getPlnExpDt().isBefore(se3Date) || bilPolicyTerm.getPlnExpDt().equals(se3Date))
                || cancellation.getEftCollectionIndicator() == EFT && bilAccount.getStatus() == CHAR_A) {
            return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
        }

        if (maxPendingCancellation != null && splitBillInd == ACCT_IS_SPLIT) {
            maxPendingCancellation.setSplitPolicy(true);
            maxPendingCancellation.setStandredCancellationDate(defaultDateTime);
            maxPendingCancellation.setMinPendingCancellationDate(defaultDateTime);
        }

        char polTermTypeCode = getPolTermTypeCode(accountId, policyId, polEffectiveDate);

        for (Contract contract : contractList) {
            if (contract.getPolicyId().equalsIgnoreCase(policyId)) {
                standCncDate = polEffectiveDate;
                pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), polEffectiveDate);
                if (isPcnmRule && maxPendingCancellation != null) {
                    maxPendingCancellation.setStandredCancellationDate(standCncDate);
                    maxPendingCancellation = loadPcnmArv(maxPendingCancellation, termDataMap, pedAcyMap, ldnMaxDays,
                            ldnMailDays, ldnLegDays);
                    maxPendingCancellation.setPendingCancelFlatCnc(CHAR_Y);
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                } else {
                    bilPolicyStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan, standCncDate,
                            polTermTypeCode, splitBillInd, bilSupportPlan.getBspPndCncNbr(), cancellation, pedAcyMap,
                            pcnnDataMap, termDataMap, accountProcessingAndRecovery);
                    if (splitBillInd == ACCT_IS_SPLIT) {
                        callSplitBillingActivity(accountId, NoncausalType.PCN_REQUEST.toString(),
                                cancellation.isPolicyPedValued(), policyId, polEffectiveDate);
                    }
                    return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
                }
            }
        }

        label01: {
            if (renewDpCancInd == CHAR_Y) {
                pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), polEffectiveDate);
                standCncDate = polEffectiveDate;
                if (isPcnmRule && maxPendingCancellation != null) {
                    maxPendingCancellation = loadPcnmArv(maxPendingCancellation, termDataMap, pedAcyMap, ldnMaxDays,
                            ldnMailDays, ldnLegDays);
                } else {
                    bilPolicyStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan, standCncDate,
                            polTermTypeCode, splitBillInd, bilSupportPlan.getBspPndCncNbr(), cancellation, pedAcyMap,
                            pcnnDataMap, termDataMap, accountProcessingAndRecovery);
                    if (splitBillInd == ACCT_IS_SPLIT) {
                        callSplitBillingActivity(accountId, NoncausalType.PCN_REQUEST.toString(),
                                cancellation.isPolicyPedValued(), policyId, polEffectiveDate);
                    }
                }
            } else {
                if (bilBillPlan.getBbpPndCncAmt() == 0 && bilBillPlan.getBbpPndCncPct() == 0 && termPastAmount > 0) {
                    pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), standCncDate);
                    if (isPcnmRule && maxPendingCancellation != null) {
                        maxPendingCancellation = loadPcnmArv(maxPendingCancellation, termDataMap, pedAcyMap, ldnMaxDays,
                                ldnMailDays, ldnLegDays);
                        maxPendingCancellation.setStandredCancellationDate(standCncDate);
                    } else {
                        bilPolicyStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan,
                                standCncDate, polTermTypeCode, splitBillInd, bilSupportPlan.getBspPndCncNbr(),
                                cancellation, pedAcyMap, pcnnDataMap, termDataMap, accountProcessingAndRecovery);
                        if (splitBillInd == ACCT_IS_SPLIT) {
                            callSplitBillingActivity(accountId, NoncausalType.PCN_REQUEST.toString(),
                                    cancellation.isPolicyPedValued(), policyId, polEffectiveDate);
                        }
                    }
                } else {
                    if (bilBillPlan.getBbpPndCncAmt() == 0 && bilBillPlan.getBbpPndCncPct() != 0
                            && termPastPercent >= bilBillPlan.getBbpPndCncPct()) {
                        pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), standCncDate);
                        if (isPcnmRule && maxPendingCancellation != null) {
                            maxPendingCancellation = loadPcnmArv(maxPendingCancellation, termDataMap, pedAcyMap,
                                    ldnMaxDays, ldnMailDays, ldnLegDays);
                            maxPendingCancellation.setStandredCancellationDate(standCncDate);
                        } else {
                            bilPolicyStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan,
                                    standCncDate, polTermTypeCode, splitBillInd, bilSupportPlan.getBspPndCncNbr(),
                                    cancellation, pedAcyMap, pcnnDataMap, termDataMap, accountProcessingAndRecovery);
                            if (splitBillInd == ACCT_IS_SPLIT) {
                                callSplitBillingActivity(accountId, NoncausalType.PCN_REQUEST.toString(),
                                        cancellation.isPolicyPedValued(), policyId, polEffectiveDate);
                            }
                        }
                    } else {
                        if (bilBillPlan.getBbpPndCncPct() == 0 && bilBillPlan.getBbpPndCncAmt() != 0
                                && termPastAmount >= bilBillPlan.getBbpPndCncAmt()) {
                            pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), standCncDate);
                            if (isPcnmRule && maxPendingCancellation != null) {
                                maxPendingCancellation = loadPcnmArv(maxPendingCancellation, termDataMap, pedAcyMap,
                                        ldnMaxDays, ldnMailDays, ldnLegDays);
                                maxPendingCancellation.setStandredCancellationDate(standCncDate);
                            } else {
                                bilPolicyStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan,
                                        standCncDate, polTermTypeCode, splitBillInd, bilSupportPlan.getBspPndCncNbr(),
                                        cancellation, pedAcyMap, pcnnDataMap, termDataMap,
                                        accountProcessingAndRecovery);
                                if (splitBillInd == ACCT_IS_SPLIT) {
                                    callSplitBillingActivity(accountId, NoncausalType.PCN_REQUEST.toString(),
                                            cancellation.isPolicyPedValued(), policyId, polEffectiveDate);
                                }
                            }
                        } else {
                            if (termPastAmount >= bilBillPlan.getBbpPndCncAmt()
                                    && termPastPercent >= bilBillPlan.getBbpPndCncPct()) {
                                pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), standCncDate);
                                if (isPcnmRule && maxPendingCancellation != null) {
                                    maxPendingCancellation = loadPcnmArv(maxPendingCancellation, termDataMap, pedAcyMap,
                                            ldnMaxDays, ldnMailDays, ldnLegDays);
                                    maxPendingCancellation.setStandredCancellationDate(standCncDate);
                                } else {
                                    bilPolicyStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy,
                                            bilBillPlan, standCncDate, polTermTypeCode, splitBillInd,
                                            bilSupportPlan.getBspPndCncNbr(), cancellation, pedAcyMap, pcnnDataMap,
                                            termDataMap, accountProcessingAndRecovery);
                                    if (splitBillInd == ACCT_IS_SPLIT) {
                                        callSplitBillingActivity(accountId, NoncausalType.PCN_REQUEST.toString(),
                                                cancellation.isPolicyPedValued(), policyId, polEffectiveDate);
                                    }
                                }
                            }
                        }
                    }
                }
                if (maxPendingCancellation != null) {
                    maxPendingCancellation.setStandredCancellationDate(standCncDate);
                    pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), standCncDate);
                }
            }
            break label01;
        }
        return new Object[] { bilPolicyStatusCode, maxPendingCancellation };
    }

    private MaxPendingCancellation loadPcnmArv(MaxPendingCancellation maxPendingCancellation,
            Map<String, Object> termDataMap, Map<String, Object> pedAcyMap, short ldnMaxDays, short ldnMailDays,
            short ldnLegDays) {

        maxPendingCancellation
                .setPendingCancellationAmount((Double) termDataMap.get(TermAcyType.TERMPASTAMOUNT.getValue()));
        maxPendingCancellation.setPedEffectiveDate((ZonedDateTime) pedAcyMap.get(PedAcyType.POLEFFDATE.getValue()));
        maxPendingCancellation.setPedExpirationDate((ZonedDateTime) pedAcyMap.get(PedAcyType.POLEXPDATE.getValue()));
        maxPendingCancellation.setEquityDate((ZonedDateTime) pedAcyMap.get(PedAcyType.ACREQUITYDATE.getValue()));
        maxPendingCancellation.setLegalDays(ldnLegDays);
        maxPendingCancellation.setMailDays(ldnMailDays);
        maxPendingCancellation.setMaximumDays(ldnMaxDays);
        maxPendingCancellation.setQualifyPcn(true);

        return maxPendingCancellation;
    }

    private Object[] setRevDate(AccountProcessingAndRecovery accountProcessingAndRecovery, PolicyReview policyReview,
            Map<String, Object> termDataMap, Map<String, Object> pedAcyMap, short ldnMaxDays,
            char useCrgDateIndicator) {

        ZonedDateTime pedMaxDateS;
        ZonedDateTime nextReviewDate = policyReview.getNextReviewDate();
        ZonedDateTime se3Date = accountProcessingAndRecovery.getRecoveryDate();
        ZonedDateTime pedNextAcyDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.NEXTACYDATE.getValue());
        ZonedDateTime termPcnDate = (ZonedDateTime) termDataMap.get(TermAcyType.TERMPCNDATE.getValue());

        pedMaxDateS = DateRoutine.getAdjustedDate(pedNextAcyDate, false, ldnMaxDays, null);

        if (useCrgDateIndicator == CHAR_N) {
            termPcnDate = pedMaxDateS;
        }

        label4: {
            if (accountProcessingAndRecovery.getDriverIndicator() == CHAR_O
                    && (termPcnDate.isBefore(se3Date) || termPcnDate.equals(se3Date))) {
                break label4;
            } else if (termPcnDate.isBefore(nextReviewDate)) {
                nextReviewDate = termPcnDate;
            }
            break label4;
        }

        if (termPcnDate.isAfter(nextReviewDate)
                && (nextReviewDate.isBefore(se3Date) || nextReviewDate.equals(se3Date))) {
            nextReviewDate = termPcnDate;
        }

        policyReview.setNextReviewDate(nextReviewDate);
        termDataMap.put(TermAcyType.TERMPCNDATE.getValue(), termPcnDate);

        return new Object[] { policyReview, termDataMap };
    }

    @Override
    public char getPolTermTypeCode(String accountId, String policyId, ZonedDateTime polEffDate) {
        char polTermTypeCode = BLANK_CHAR;
        List<BilAmounts> bilAmountsList = bilAmountsRepository.findMaxAmountRow(accountId, policyId, polEffDate,
                Arrays.asList(BilAmountsEftIndicator.NEW_BUSINESS.getValue(),
                        BilAmountsEftIndicator.NEWBUSINESS.getValue(),
                        BilAmountsEftIndicator.REVERSE_AUDIT_OPERATOR.getValue(),
                        BilAmountsEftIndicator.RENEWAL.getValue(), BilAmountsEftIndicator.RENEWAL_UNDER.getValue(),
                        BilAmountsEftIndicator.RENEWALS.getValue(), BilAmountsEftIndicator.REISSUE_NEW_TERM.getValue()),
                CHAR_Y, PageRequest.of(SHORT_ZERO, SHORT_ONE));
        if (bilAmountsList != null && !bilAmountsList.isEmpty()) {
            polTermTypeCode = bilAmountsList.get(0).getBilAmtEffInd();
        }
        return polTermTypeCode;
    }

    private boolean checkIsNewBusinessTerm(String accountId, BilPolicyTerm bilPolicyTerm, char bisTermInvInd,
            char nsfCncInd, char invDuringPcnInd) {
        boolean isNewBusiness = false;
        if (nsfCncInd == CHAR_A && invDuringPcnInd == CHAR_N
                && (bisTermInvInd == BLANK_CHAR || bisTermInvInd == CHAR_A)) {
            BilAmounts bilAmounts = bilAmountsRepository.getBilAmountByEffInd(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilPolicyTerm.getPlnExpDt(),
                    new ArrayList<>(Arrays.asList(BilAmountsEftIndicator.NEW_BUSINESS.getValue(),
                            BilAmountsEftIndicator.NEWBUSINESS.getValue())),
                    CHAR_Y);
            if (bilAmounts != null) {
                isNewBusiness = true;
            }
        }
        return isNewBusiness;
    }

    private boolean checkIsCanttTerm(String accountId, BilPolicyTerm bilPolicyTerm) {
        boolean isCanttTermActive = false;
        char canttPolStatusCode = BLANK_CHAR;
        BilPolicyTerm canttPolicyterm = bilPolicyTermRepository
                .findById(new BilPolicyTermId(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()))
                .orElse(null);
        if (canttPolicyterm != null) {
            canttPolStatusCode = canttPolicyterm.getBillPolStatusCd();
        }
        if (canttPolStatusCode == BilPolicyStatusCode.PENDING_CANCELLATION.getValue()
                || canttPolStatusCode == BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue()
                || canttPolStatusCode == BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue()
                || canttPolStatusCode == BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue()) {
            isCanttTermActive = true;
        }
        return isCanttTermActive;
    }

    private char standCancel(BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy,
            ZonedDateTime standCncDate, char polTermTypeCode, BilBillPlan bilBillPlan, char splitBillInd,
            ZonedDateTime minPcnDate, boolean isPcnmRule, short pndCountNumber, Cancellation cancellation,
            Map<String, Object> pedAcyMap, Map<String, Object> pcnnDataMap, Map<String, Object> termDataMap,
            AccountProcessingAndRecovery accountProcessingAndRecovery, MaxPendingCancellation maxPendingCancellation,
            short ldnLegDays, short ldnMailDays, short ldnMaxDays) {

        char polStatusCode = bilPolicyTerm.getBillPolStatusCd();
        boolean termNeverBilled = (boolean) termDataMap.get(NsfPcnDataType.TERMNEVERBILLED.getValue());
        double bilIstDueAmount = (double) termDataMap.get(NsfPcnDataType.BILISTDUEAMOUNT.getValue());
        if (standCncDate.compareTo(bilPolicyTerm.getPlnExpDt()) >= 0 && bilBillPlan.getBbpInvDuringPcnInd() == CHAR_N
                && cancellation.getRestartPcnDate() == null) {
            char cancelPcnReqInd = checkForPrevHistory("CPE", bilAccount.getAccountId(), bilPolicy.getPolNbr(),
                    bilPolicy.getPolSymbolCd(), bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    bilPolicyTerm.getPlnExpDt());
            if (cancelPcnReqInd == CANCEL_PCN_REQ) {
                wipLink(bilAccount.getAccountId(), bilAccount.getAccountNumber(), WipAcitivityId.CANCELPCN.getValue());
            }
        } else if (minPcnDate.compareTo(accountProcessingAndRecovery.getRecoveryDate()) > 0
                && minPcnDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
            char cancelPcnReq2Ind = checkForPrevHistory("CPN", bilAccount.getAccountId(), bilPolicy.getPolNbr(),
                    bilPolicy.getPolSymbolCd(), bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    bilPolicyTerm.getPlnExpDt());
            if (cancelPcnReq2Ind == CANCEL_PCN_REQ) {
                wipLink(bilAccount.getAccountId(), bilAccount.getAccountNumber(), WipAcitivityId.CANCELPCN2.getValue());
            }
        } else {
            if (isPcnmRule && maxPendingCancellation != null) {
                Double nsfPcnAmount = (Double) termDataMap.get(NsfPcnDataType.NSFPCNAMOUNT.getValue());
                loadPcnmNsfPcn(termNeverBilled, nsfPcnAmount, bilIstDueAmount, maxPendingCancellation, pedAcyMap,
                        pcnnDataMap, ldnMaxDays, ldnMailDays, ldnLegDays);
            } else {
                polStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan, standCncDate,
                        polTermTypeCode, splitBillInd, pndCountNumber, cancellation, pedAcyMap, pcnnDataMap,
                        termDataMap, accountProcessingAndRecovery);
                if (splitBillInd == ACCT_IS_SPLIT) {
                    callSplitBillingActivity(bilAccount.getAccountId(), NoncausalType.PCN_REQUEST.toString(),
                            cancellation.isPolicyPedValued(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                            bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                }
            }
        }
        return polStatusCode;
    }

    private char pendingCancel(char splitBillInd, boolean isPcnmRule, ZonedDateTime minPcnDate,
            BilPolicyTerm bilPolicyTerm, BilAccount bilAccount, BilPolicy bilPolicy, BilBillPlan bilBillPlan,
            ZonedDateTime standCncDate, char polTermTypeCode, short pndCountNumber, Cancellation cancellation,
            Map<String, Object> pedAcyMap, Map<String, Object> pcnnDataMap, Map<String, Object> termDataMap,
            AccountProcessingAndRecovery accountProcessingAndRecovery, MaxPendingCancellation maxPendingCancellation,
            short ldnLegDays, short ldnMailDays, short ldnMaxDays) {
        char polStatusCode = bilPolicyTerm.getBillPolStatusCd();
        ZonedDateTime pedAcrEquityDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.ACREQUITYDATE.getValue());
        boolean termNeverBilled = (boolean) termDataMap.get(NsfPcnDataType.TERMNEVERBILLED.getValue());
        double bilIstDueAmount = (double) termDataMap.get(NsfPcnDataType.BILISTDUEAMOUNT.getValue());

        if (pedAcrEquityDate.compareTo(bilPolicyTerm.getPlnExpDt()) >= 0
                && pedAcrEquityDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
            char cancelPcnReqInd = checkForPrevHistory("CPE", bilAccount.getAccountId(), bilPolicy.getPolNbr(),
                    bilPolicy.getPolSymbolCd(), bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    bilPolicyTerm.getPlnExpDt());
            if (cancelPcnReqInd == CANCEL_PCN_REQ) {
                wipLink(bilAccount.getAccountId(), bilAccount.getAccountNumber(), WipAcitivityId.CANCELPCN.getValue());
            }
        } else if (minPcnDate.compareTo(accountProcessingAndRecovery.getRecoveryDate()) > 0
                && minPcnDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
            char cancelPcnReq2Ind = checkForPrevHistory("CPN", bilAccount.getAccountId(), bilPolicy.getPolNbr(),
                    bilPolicy.getPolSymbolCd(), bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    bilPolicyTerm.getPlnExpDt());
            if (cancelPcnReq2Ind == CANCEL_PCN_REQ) {
                wipLink(bilAccount.getAccountId(), bilAccount.getAccountNumber(), WipAcitivityId.CANCELPCN2.getValue());
            }
        } else if (isPcnmRule && maxPendingCancellation != null) {
            Double nsfPcnAmount = (Double) termDataMap.get(NsfPcnDataType.NSFPCNAMOUNT.getValue());
            loadPcnmNsfPcn(termNeverBilled, nsfPcnAmount, bilIstDueAmount, maxPendingCancellation, pedAcyMap,
                    pcnnDataMap, ldnMaxDays, ldnMailDays, ldnLegDays);
        } else {
            polStatusCode = pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan, standCncDate,
                    polTermTypeCode, splitBillInd, pndCountNumber, cancellation, pedAcyMap, pcnnDataMap, termDataMap,
                    accountProcessingAndRecovery);
            if (splitBillInd == ACCT_IS_SPLIT) {
                callSplitBillingActivity(bilAccount.getAccountId(), NoncausalType.PCN_REQUEST.toString(),
                        cancellation.isPolicyPedValued(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            }
        }
        return polStatusCode;
    }

    private Object[] detectPolicyPedValued(String accountId, String policyId, char reviewDatesInd, char splitBillInd,
            BilStRulesUct sberStRule, char polTrmEpcnRule, ZonedDateTime pedInputDate, ZonedDateTime pedPolEffDate,
            ZonedDateTime pedPolExpDate, ZonedDateTime polEffectiveDate) {

        boolean isPolicyPedValued = false;
        ZonedDateTime pedNextAcyDate = DateRoutine.defaultDateTime();

        if (reviewDatesInd == CHAR_P && splitBillInd == ACCT_IS_SPLIT && sberStRule != null
                && sberStRule.getBilRuleCd() == CHAR_Y && polTrmEpcnRule == '2') {
            short sbfCount = checkSplitCatSusAccount(policyId, polEffectiveDate);
            if (sbfCount <= 0) {
                pedNextAcyDate = callCalculatePolicyPaidToDate(accountId, policyId, pedPolEffDate, pedPolExpDate);
                if (pedNextAcyDate.compareTo(pedInputDate) <= 0) {
                    isPolicyPedValued = true;
                }
            }
        }
        return new Object[] { isPolicyPedValued, pedNextAcyDate };
    }

    @Override
    public char checkForPrevHistory(String bilAcyDes, String accountId, String polNumber, String polSymbolCd,
            ZonedDateTime polEffDate, ZonedDateTime planExpDate) {
        char cancelPcnReqInd = CHAR_N;
        ZonedDateTime maxBilAcyDate = bilActSummaryRepository.findMaxBilActivityByAcy2Date(accountId, polNumber,
                polSymbolCd, bilAcyDes, BLANK_STRING, polEffDate);
        if (maxBilAcyDate == null) {
            cancelPcnReqInd = CHAR_Y;
            writeHistory(accountId, bilAcyDes, polNumber, polSymbolCd, planExpDate, polEffDate, DECIMAL_ZERO,
                    BLANK_STRING, BLANK_STRING);
        }
        return cancelPcnReqInd;
    }

    private boolean setNrvDateTrigger(String accountId, ZonedDateTime pedNextAcyDate, ZonedDateTime minPcnDate,
            short ldnLegDays, short ldnMailDays, BilAccount bilAccount, BilPolicy bilPolicy,
            BilPolicyTerm bilPolicyTerm, ZonedDateTime recoveryDate) {
        boolean isFutureEquity = false;
        ZonedDateTime existFutureNrvDate = DateRoutine.defaultDateTime();
        BilActInquiry bilActInquiry = bilActInquiryRepository.findById(new BilActInquiryId(accountId, "NRV"))
                .orElse(null);
        if (bilActInquiry != null) {
            existFutureNrvDate = bilActInquiry.getBilNxtAcyDt();
        }

        ZonedDateTime pedMaxDateS = DateRoutine.adjustDateWithOffset(pedNextAcyDate, false, ldnLegDays + ldnMailDays,
                MINUS_SIGN, null);
        ZonedDateTime pedNsfAcyDt = pedMaxDateS;
        ZonedDateTime nextNrvInvDate = DateRoutine.dateTimeAsYYYYMMDD("0001-01-01");
        bilActInquiry = bilActInquiryRepository.findById(new BilActInquiryId(accountId, "AIV")).orElse(null);
        if (bilActInquiry != null) {
            nextNrvInvDate = bilActInquiry.getBilNxtAcyDt();
        }

        label1: {
            if (pedNsfAcyDt.compareTo(nextNrvInvDate) > 0
                    || existFutureNrvDate.compareTo(DateRoutine.defaultDateTime()) != 0
                            && existFutureNrvDate.compareTo(nextNrvInvDate) > 0) {
                if (minPcnDate.compareTo(recoveryDate) > 0
                        && minPcnDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
                    char cancelPcnReq2Ind = checkForPrevHistory("CPN", bilAccount.getAccountId(), bilPolicy.getPolNbr(),
                            bilPolicy.getPolSymbolCd(), bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                            bilPolicyTerm.getPlnExpDt());
                    if (cancelPcnReq2Ind == CANCEL_PCN_REQ) {
                        wipLink(accountId, bilAccount.getAccountNumber(), WipAcitivityId.CANCELPCN2.getValue());
                    }
                    break label1;
                }
                isFutureEquity = false;
                if (existFutureNrvDate.compareTo(nextNrvInvDate) >= 0) {
                    removeNrvTriggers(accountId);
                }
                break label1;
            }
            if (existFutureNrvDate.compareTo(DateRoutine.defaultDateTime()) != 0
                    && existFutureNrvDate.compareTo(pedNsfAcyDt) > 0) {
                isFutureEquity = true;
                bilActInquiry = bilActInquiryRepository.findById(new BilActInquiryId(accountId, "NRV")).orElse(null);
                if (bilActInquiry != null && bilActInquiry.getBilNxtAcyDt().compareTo(pedNsfAcyDt) != 0) {
                    bilActInquiry.setBilNxtAcyDt(pedNsfAcyDt);
                    bilActInquiryRepository.saveAndFlush(bilActInquiry);
                    break label1;
                }
            }
            if (existFutureNrvDate.compareTo(DateRoutine.defaultDateTime()) != 0
                    && existFutureNrvDate.compareTo(pedNsfAcyDt) <= 0) {
                isFutureEquity = true;
                if (minPcnDate.compareTo(recoveryDate) > 0
                        && minPcnDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
                    char cancelPcnReq2Sw = checkForPrevHistory("CPN", bilAccount.getAccountId(), bilPolicy.getPolNbr(),
                            bilPolicy.getPolSymbolCd(), bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                            bilPolicyTerm.getPlnExpDt());
                    if (cancelPcnReq2Sw == CANCEL_PCN_REQ) {
                        wipLink(accountId, bilAccount.getAccountNumber(), WipAcitivityId.CANCELPCN2.getValue());
                    }
                }
                break label1;
            }
            bilActInquiry = new BilActInquiry();
            bilActInquiry.setBilActInquiryId(new BilActInquiryId(accountId, "NRV"));
            bilActInquiry.setBilNxtAcyDt(pedMaxDateS);
            bilActInquiryRepository.saveAndFlush(bilActInquiry);
            isFutureEquity = true;
        }

        return isFutureEquity;
    }

    private void removeNrvTriggers(String accountId) {
        BilActInquiry bilActInquiry = bilActInquiryRepository.findById(new BilActInquiryId(accountId, "NRV"))
                .orElse(null);
        if (bilActInquiry != null) {
            bilActInquiryRepository.delete(bilActInquiry);
        }

        List<BilPolActivity> bilPolActivityList = bilPolActivityRepository.fetchBilPolActivityTriger(accountId, "NRV");
        if (bilPolActivityList != null && !bilPolActivityList.isEmpty()) {
            bilPolActivityRepository.deleteAllInBatch(bilPolActivityList);
        }
    }

    private short checkSplitCatSusAccount(String policyId, ZonedDateTime polEffectiveDate) {
        Integer splitCatSusCount = 0;
        List<String> bilAccountIdList = bilPolicyTermRepository.getDistinctBilAccountIdByEffDate(policyId,
                polEffectiveDate);
        if (bilAccountIdList != null && !bilAccountIdList.isEmpty()) {
            splitCatSusCount = bilAccountRepository.countSplitCatSusAccount(bilAccountIdList,
                    new ArrayList<>(Arrays.asList(AccountStatus.SUSPEND_BILLING_FOLLOW_UP.toChar(),
                            AccountStatus.SUSPEND_FOLLOW_UP.toChar())),
                    "SN");
            if (splitCatSusCount == null) {
                splitCatSusCount = 0;
            }
        }
        return splitCatSusCount.shortValue();
    }

    private ZonedDateTime[] calculateLegalDays(short ldnLegDays, short ldnMailDays, short ldnMaxDays, char pedEquityInd,
            ZonedDateTime recoveryDate) {
        ZonedDateTime standCncDate;
        ZonedDateTime pedMinDate = DateRoutine.defaultDateTime();
        ZonedDateTime pedMaxDate = DateRoutine.defaultDateTime();

        if (pedEquityInd != CHAR_Y) {
            standCncDate = DateRoutine.getAdjustedDate(recoveryDate, false, ldnLegDays + ldnMailDays, null);
        } else {
            standCncDate = DateRoutine.getAdjustedDate(recoveryDate, false, ldnLegDays + ldnMailDays, null);
            pedMinDate = DateRoutine.getAdjustedDate(recoveryDate, false, ldnLegDays + ldnMailDays, null);
            pedMaxDate = DateRoutine.getAdjustedDate(recoveryDate, false, ldnMaxDays, null);
        }

        return new ZonedDateTime[] { standCncDate, pedMinDate, pedMaxDate };
    }

    private ZonedDateTime getMinPcnDate(ZonedDateTime pedPolEffDate, short ldnLegDays, short ldnMailDays,
            char invDuringPcnInd) {
        ZonedDateTime minPcnDate = DateRoutine.defaultDateTime();
        if (invDuringPcnInd != CHAR_Y) {
            minPcnDate = DateRoutine.adjustDateWithOffset(pedPolEffDate, false, ldnLegDays + ldnMailDays, MINUS_SIGN,
                    null);
        }
        return minPcnDate;
    }

    @Override
    public char pcnRnpRequest(BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy,
            BilBillPlan bilBillPlan, ZonedDateTime standCncDate, char polTermTypeCode, char splitIndicator,
            short pndCncNbr, Cancellation cancellation, Map<String, Object> pedAcyMap, Map<String, Object> pcnnDataMap,
            Map<String, Object> termDataMap, AccountProcessingAndRecovery accountProcessingAndRecovery) {

        short reinstNewTrmCount = 0;
        ZonedDateTime pcnDate = null;
        ZonedDateTime pedDate = null;
        char restartPcnInd = BLANK_CHAR;
        char nsfActivityInd = BLANK_CHAR;
        char cancelType = BLANK_CHAR;
        String pcnReason;
        PcnRescindCollection pcnRescindCollection = new PcnRescindCollection();

        boolean termNeverBilled = (boolean) termDataMap.get(NsfPcnDataType.TERMNEVERBILLED.getValue());
        boolean isNsfPcnCancel = (boolean) termDataMap.get(NsfPcnDataType.NSFPCNCANCEL.getValue());
        ZonedDateTime bilIstDueDate = (ZonedDateTime) termDataMap.get(NsfPcnDataType.BILISTDUEDATE.getValue());

        char[] cmcnRuleCode = processByCmcnRule(cancellation.getEftReversalIndicator(), bilAccount,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), pndCncNbr,
                accountProcessingAndRecovery.getRecoveryDate());
        char sendAltColMthInd = cmcnRuleCode[0];
        char premiumNsfAcyInd = cmcnRuleCode[1];

        writeNewNblTrigger(termNeverBilled, isNsfPcnCancel, bilAccount.getAccountId(), bilIstDueDate, bilPolicyTerm);

        char newPolStatusCode = evaluateNewPolStatus(cancellation.getPcnmMaxIndicator(), isNsfPcnCancel,
                accountProcessingAndRecovery.getDriverIndicator());

        boolean isFpnpRule = getFpnpRuleData(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(), bilPolicyTerm);

        char pcnFlatCncInd = (char) pcnnDataMap.get(PcnnType.PCNFLATCNCIND.getValue());
        if (cancellation.getOriginalPcnReason() != null && !cancellation.getOriginalPcnReason().isEmpty()) {
            pcnReason = cancellation.getOriginalPcnReason();
            if (accountProcessingAndRecovery.getDriverIndicator() == CHAR_O && cancellation.isPcnmApplied()) {
                if (pcnReason.equalsIgnoreCase("RPC")) {
                    pcnReason = "RNP";
                } else if (pcnReason.equalsIgnoreCase("PPC")) {
                    pcnReason = "PNP";
                }
            }
        } else {
            pcnReason = evaluatePcnReasonCode(cancellation, pcnFlatCncInd, bilAccount.getAccountId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
        }
        String apiPolPcnReason = pcnReason;
        pcnRescindCollection.setPolPcnReason(apiPolPcnReason);

        Short[] legalDays = getLegalDays(apiPolPcnReason, bilBillPlan, bilPolicyTerm, bilPolicy);
        short ldnMaxDays = legalDays[0];
        short ldnMailDays = legalDays[1];
        short ldnLegDays = legalDays[2];

        double nsfPcnAmount = (Double) termDataMap.get(NsfPcnDataType.NSFPCNAMOUNT.getValue());
        double termPastAmount = (double) termDataMap.get(TermAcyType.TERMPASTAMOUNT.getValue());
        if ((cancellation.getRestartPcnDate() == null && cancellation.getPcnmMaxIndicator() != CHAR_Y
                && cancellation.getPcnmMaxIndicator() != CHAR_I)
                && (!termNeverBilled && isNsfPcnCancel || !isNsfPcnCancel)) {
            BilStRulesUct bilStRulesUct = accountReviewService.getBilStRuleUct("PCNI",
                    bilPolicyTerm.getBillStatePvnCd(), bilPolicyTerm.getLobCd(), bilPolicyTerm.getMasterCompanyNbr());
            if (bilStRulesUct != null && bilStRulesUct.getBilRuleCd() == CHAR_Y) {
                Double[] nsfPcnObject = evaluatePcnNext(apiPolPcnReason, isNsfPcnCancel, bilPolicy.getPolNbr(),
                        bilPolicy.getPolSymbolCd(), bilStRulesUct.getBilParmListTxt(), bilAccount, bilPolicyTerm,
                        accountProcessingAndRecovery.getRecoveryDate(), termDataMap);
                nsfPcnAmount = nsfPcnObject[0];
                termPastAmount = nsfPcnObject[1];
            }

        }

        if (isFpnpRule) {
            pcnReason = "PNP";
        }

        PendingCancellationNotice pendingCancellationNotice = new PendingCancellationNotice();
        writeNewPcnRnpHistoryRow(bilAccount, bilPolicy, bilPolicyTerm, pcnReason, nsfPcnAmount, termPastAmount,
                ldnLegDays, ldnMailDays, ldnMaxDays, sendAltColMthInd, premiumNsfAcyInd, isFpnpRule, reinstNewTrmCount,
                polTermTypeCode, splitIndicator, cancellation, pedAcyMap, pcnnDataMap, termDataMap,
                accountProcessingAndRecovery, pcnRescindCollection, standCncDate, pendingCancellationNotice);

        boolean isPnpWritten = false;
        boolean isBcmoapiCalled = false;
        char eftCollectionInd = BLANK_CHAR;
        char exceedPolicyInd = BLANK_CHAR;
        if ((isFpnpRule && (cancellation.getLapseCancIndicator() == LAPSE_CANC && reinstNewTrmCount != 0
                || cancellation.getLapseCancIndicator() == CHAR_N) && apiPolPcnReason.equalsIgnoreCase("PNP"))
                || apiPolPcnReason.equalsIgnoreCase("PNP")) {
            isPnpWritten = true;
        }

        if (apiPolPcnReason.equalsIgnoreCase("RNP") || isFpnpRule && apiPolPcnReason.equalsIgnoreCase("PNP")
                && reinstNewTrmCount == 0 && cancellation.getLapseCancIndicator() == CHAR_Y && isPnpWritten) {
            exceedPolicyInd = CHAR_Y;
        }

        if (isFpnpRule && apiPolPcnReason.equalsIgnoreCase("PNP") && reinstNewTrmCount == 0
                && cancellation.getLapseCancIndicator() == CHAR_Y && isBcmoapiCalled) {
            isBcmoapiCalled = false;
        }

        if (!((apiPolPcnReason.equalsIgnoreCase("RNP") || isFpnpRule && apiPolPcnReason.equalsIgnoreCase("PNP")
                && reinstNewTrmCount == 0 && cancellation.getLapseCancIndicator() == CHAR_Y)
                && exceedPolicyInd == CHAR_Y && isPnpWritten && isBcmoapiCalled)
                && accountProcessingAndRecovery.getQuoteIndicator() != CHAR_Q) {
            if (accountProcessingAndRecovery.getQuoteIndicator() != CHAR_Q) {
                logger.debug("Call to BillingApi module");
                pcnRescindCollection.setApiType("ACR");
                pcnRescindCollection.setPolSymbolCode(bilPolicy.getPolSymbolCd());
                pcnRescindCollection.setPolNumber(bilPolicy.getPolNbr());
                pcnRescindCollection.setMasterCompanyNumber(bilPolicyTerm.getMasterCompanyNbr());
                pcnRescindCollection.setLineOfBusinessCode(bilPolicyTerm.getLobCd());
                pcnRescindCollection.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
                pcnRescindCollection.setAccountId(bilAccount.getAccountId());
                pcnRescindCollection.setReinstCode((char) pcnnDataMap.get(PcnnType.REINSTCODE.getValue()));
                pcnRescindCollection.setStatePvnCode(bilPolicyTerm.getBillStatePvnCd());
                pcnRescindCollection.setActivityDate(accountProcessingAndRecovery.getRecoveryDate());
                pcnRescindCollection.setUserSequenceId(accountProcessingAndRecovery.getUserSequenceId());
                billingApiService.processPcnOrRescindCollection(BCMOAX, accountReviewService.getQueueWrtIndicator(),
                        pcnRescindCollection);
            }
            String additionalDataTxt = createBilAddTxtData(pcnReason, pcnDate, pedDate,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), cancelType, restartPcnInd,
                    cancellation.getRestartPcnDate(), nsfActivityInd, ldnLegDays, ldnMailDays, ldnMaxDays,
                    newPolStatusCode, polTermTypeCode, BLANK_CHAR);
            if (accountProcessingAndRecovery.getDriverIndicator() == CHAR_O) {
                writeHistory(bilAccount.getAccountId(), BilDesReasonCode.MANUAL_PENDING_CANCEL.getValue(),
                        bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), DateRoutine.defaultDateTime(),
                        DECIMAL_ZERO, additionalDataTxt, "SYSTEM");
            }
            pendingCancellationNotice = setParamPendingCancellationNotice(pendingCancellationNotice, bilAccount,
                    bilPolicyTerm, bilPolicy, pcnRescindCollection);
            pcnNotice(bilAccount, bilPolicyTerm, apiPolPcnReason, ldnLegDays, ldnMailDays, ldnMaxDays, bilPolicy,
                    pcnReason, pcnDate, pedDate, cancelType, restartPcnInd, cancellation.getRestartPcnDate(),
                    nsfActivityInd, newPolStatusCode, polTermTypeCode, accountProcessingAndRecovery,
                    pendingCancellationNotice);

        }

        if (eftCollectionInd == EFT && bilAccount.getSuspendedFullReasonCd().equalsIgnoreCase("SS")) {
            cancellation.setEftResumeSus(false);
        }

        if (cancellation.isPolicyPedValued()) {
            writeHistory(bilAccount.getAccountId(), "PTD", bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                    (ZonedDateTime) pedAcyMap.get(PedAcyType.NEXTACYDATE.getValue()), DateRoutine.defaultDateTime(),
                    DECIMAL_ZERO, BLANK_STRING, SYSTEM_USER_ID);
        }

        return newPolStatusCode;
    }

    private Map<String, Object> writeNewPcnRnpHistoryRow(BilAccount bilAccount, BilPolicy bilPolicy,
            BilPolicyTerm bilPolicyTerm, String pcnReason, double nsfPcnAmount, double termPastAmount, short ldnLegDays,
            short ldnMailDays, short ldnMaxDays, char sendAltColMthInd, char premiumNsfAcyInd, boolean isFpnpRule,
            short reinstNewTrmCount, char polTermTypeCode, char splitIndicator, Cancellation cancellation,
            Map<String, Object> pedAcyMap, Map<String, Object> pcnnDataMap, Map<String, Object> termDataMap,
            AccountProcessingAndRecovery accountProcessingAndRecovery, PcnRescindCollection pcnRescindCollection,
            ZonedDateTime standCncDate, PendingCancellationNotice pendingCancellationNotice) {

        char pcnFlatCncInd = (char) pcnnDataMap.get(PcnnType.PCNFLATCNCIND.getValue());
        char addBasRnrInd = (char) pcnnDataMap.get(PcnnType.ADDBASRNRIND.getValue());

        boolean termNeverBilled = (boolean) termDataMap.get(NsfPcnDataType.TERMNEVERBILLED.getValue());
        double bilIstDueAmount = (double) termDataMap.get(NsfPcnDataType.BILISTDUEAMOUNT.getValue());
        boolean isNsfPcnCancel = (boolean) termDataMap.get(NsfPcnDataType.NSFPCNCANCEL.getValue());

        ZonedDateTime pedNextAcyDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.NEXTACYDATE.getValue());
        ZonedDateTime pedPolEffDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.POLEFFDATE.getValue());
        ZonedDateTime pedPolExpDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.POLEXPDATE.getValue());
        ZonedDateTime savePedDate = DateRoutine.defaultDateTime();
        if (pcnReason.equalsIgnoreCase("RNP") || pedNextAcyDate == null) {
            pedPolEffDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
            pedPolExpDate = bilPolicyTerm.getPlnExpDt();
            if (cancellation.isPolicyPedValued()) {
                pedNextAcyDate = callCalculatePolicyPaidToDate(bilAccount.getAccountId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(), pedPolEffDate, pedPolExpDate);
                savePedDate = pedNextAcyDate;
            } else {
                savePedDate = callCalculatePaidToDate(bilAccount.getAccountId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(), pedPolEffDate, pedPolExpDate);
            }

            pedAcyMap.put(PedAcyType.NEXTACYDATE.getValue(), pedNextAcyDate);
            pedAcyMap.put(PedAcyType.POLEFFDATE.getValue(), pedPolEffDate);
            pedAcyMap.put(PedAcyType.POLEXPDATE.getValue(), pedPolExpDate);
        }

        String basBilDesReaType = BLANK_STRING;
        if (cancellation.getPcnmMaxIndicator() == CHAR_I) {
            basBilDesReaType = "CAM";
        } else if (cancellation.getPcnmMaxIndicator() == CHAR_Y) {
            basBilDesReaType = "NQM";
        }

        char apiRestartPcnInd = BLANK_CHAR;
        String basBilAcyDesCd = "C";
        ZonedDateTime apiRestartPcnDate = null;
        ZonedDateTime basBilAcyDes2Date = DateRoutine.defaultDateTime();
        if (cancellation.getRestartPcnDate() != null && pcnFlatCncInd != CHAR_Y) {
            basBilAcyDesCd = "RCN";
            basBilAcyDes2Date = cancellation.getRestartPcnDate();
            apiRestartPcnInd = CHAR_Y;
            apiRestartPcnDate = cancellation.getRestartPcnDate();
        }

        double basBilAcyAmount;
        if (termNeverBilled && isNsfPcnCancel) {
            basBilAcyAmount = bilIstDueAmount;
        } else if (isNsfPcnCancel) {
            basBilAcyAmount = nsfPcnAmount;
        } else {
            basBilAcyAmount = termPastAmount;
        }
        pendingCancellationNotice.setPcnAmount(basBilAcyAmount);
        pcnRescindCollection.setAutoCollectionMethodChange(CHAR_N);
        if (sendAltColMthInd == SEND_ALT_COL_MTH) {
            pcnRescindCollection.setAutoCollectionMethodChange(CHAR_Y);
        }
        char apiNsfActivityInd = CHAR_N;
        if (premiumNsfAcyInd == PREMIUM_ACY) {
            apiNsfActivityInd = CHAR_Y;
        }

        String pedPolicyId = (String) pedAcyMap.get(PedAcyType.POLICYID.getValue());
        String apiPolicyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
        if (pedPolicyId != null && pedPolicyId.compareTo(BLANK_STRING) != 0) {
            apiPolicyId = pedPolicyId;
        }
        pcnRescindCollection.setEffectiveDate(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
        if (pedPolEffDate.compareTo(DateRoutine.defaultDateTime()) != 0
                && pedPolEffDate.compareTo(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()) > 0) {
            pcnRescindCollection.setEffectiveDate(pedPolEffDate);
        }
        pcnRescindCollection.setPlanExpDate(bilPolicyTerm.getPlnExpDt());
        if (pedPolExpDate.compareTo(DateRoutine.defaultDateTime()) != 0
                && pedPolExpDate.compareTo(bilPolicyTerm.getPlnExpDt()) > 0) {
            pcnRescindCollection.setPlanExpDate(pedPolExpDate);
        }

        String apiPolPcnReason = pcnReason;
        char apiCancelType;
        ZonedDateTime apiEquityDate = null;
        ZonedDateTime apiStandCncDate = null;
        char pedEquityInd = (char) pedAcyMap.get(PedAcyType.EQUITYIND.getValue());
        if (pedEquityInd == CHAR_Y) {
            apiCancelType = CHAR_E;
            apiEquityDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.ACREQUITYDATE.getValue());
            if (isFpnpRule && apiPolPcnReason.compareTo("PNP") == 0 && reinstNewTrmCount == 0
                    && cancellation.getLapseCancIndicator() == CHAR_Y) {
                ZonedDateTime[] legalDays = calculateLegalDays(ldnLegDays, ldnMailDays, ldnMaxDays, pedEquityInd,
                        accountProcessingAndRecovery.getRecoveryDate());
                standCncDate = legalDays[0];
                if (standCncDate.compareTo(apiEquityDate) > 0) {
                    apiStandCncDate = standCncDate;
                }
            }
            if (apiEquityDate.compareTo(DateRoutine.defaultDateTime()) == 0 || apiEquityDate == null) {
                throw new InvalidDataException("eq.cnc.dt.err");
            }
        } else {
            apiCancelType = CHAR_S;
            if (isFpnpRule && apiPolPcnReason.compareTo("PNP") == 0 && reinstNewTrmCount == 0
                    && cancellation.getLapseCancIndicator() == CHAR_Y) {
                ZonedDateTime[] legalDays = calculateLegalDays(ldnLegDays, ldnMailDays, ldnMaxDays, pedEquityInd,
                        accountProcessingAndRecovery.getRecoveryDate());
                standCncDate = legalDays[0];
            }
            if (standCncDate == null || standCncDate.compareTo(DateRoutine.defaultDateTime()) == 0
                    || bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt().compareTo(standCncDate) >= 0) {
                standCncDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
            }
            apiStandCncDate = standCncDate;

            if (apiStandCncDate.compareTo(DateRoutine.defaultDateTime()) == 0) {
                apiStandCncDate = null;
            }
            apiEquityDate = null;
        }

        String pedIssueSysId = (String) pedAcyMap.get(PedAcyType.ISSUESYSID.getValue());
        String apiIssueSysId = bilPolicyTerm.getBptIssueSysId();
        if (!pedIssueSysId.trim().isEmpty()) {
            apiIssueSysId = pedIssueSysId;
        }
        if (apiIssueSysId.trim().isEmpty()) {
            apiIssueSysId = "UW";
        }

        String basCanRea = apiPolPcnReason;

        ZonedDateTime basPcnDate = null;
        ZonedDateTime basPedDate = null;
        if (apiCancelType == CANCEL_EQUITY) {
            basPcnDate = apiEquityDate;
            if (isFpnpRule && apiPolPcnReason.compareTo("PNP") == 0 && reinstNewTrmCount == 0
                    && cancellation.getLapseCancIndicator() == CHAR_Y && standCncDate.compareTo(apiEquityDate) > 0) {
                basPcnDate = standCncDate;
                basPedDate = standCncDate;
                apiEquityDate = standCncDate;
            }
        } else {
            basPcnDate = apiStandCncDate;
            basPedDate = savePedDate;
        }

        char basNsfActivityInd = apiNsfActivityInd;
        short basLdnLegalDays = ldnLegDays;
        short basLdnMailDays = ldnMailDays;
        short basLdnMaxDays = ldnMaxDays;
        char basPolStatusCd = bilPolicyTerm.getBillPolStatusCd();
        char basCancelType = apiCancelType;
        char basRestartPcnInd = apiRestartPcnInd;
        String basPolicyId = apiPolicyId;
        ZonedDateTime basRestartPcnDate = apiRestartPcnDate;

        if (polTermTypeCode == BLANK_CHAR) {
            List<BilAmounts> bilAmountsList = bilAmountsRepository.findMaxAmountRow(bilAccount.getAccountId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    new ArrayList<>(Arrays.asList(CHAR_N, CHAR_M, CHAR_G, CHAR_R, CHAR_U, CHAR_W, '9')), CHAR_Y,
                    PageRequest.of(SHORT_ZERO, SHORT_ONE));
            if (bilAmountsList != null && !bilAmountsList.isEmpty()) {
                polTermTypeCode = bilAmountsList.get(0).getBilAmtEffInd();
            }
        }
        char basTermTypeCd = BLANK_CHAR;
        if (polTermTypeCode == CHAR_N || polTermTypeCode == CHAR_M) {
            basTermTypeCd = CHAR_N;
        }
        if (polTermTypeCode == POL_TERM_TYPE_CD_RN_1 || polTermTypeCode == POL_TERM_TYPE_CD_RN_2
                || polTermTypeCode == POL_TERM_TYPE_CD_RN_3 || polTermTypeCode == POL_TERM_TYPE_CD_RN_4
                || polTermTypeCode == POL_TERM_TYPE_CD_RN_5) {
            basTermTypeCd = CHAR_R;
        }
        polTermTypeCode = BLANK_CHAR;
        ZonedDateTime minDueDate = null;
        ZonedDateTime maxDueDate = null;
        char basPcnDpInstInd = CHAR_Y;
        List<Object[]> adjustDueDateRage = bilIstScheduleRepository.fetchAdjDueDateRange(bilAccount.getAccountId(),
                getInvoiceCodeList(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
        if (adjustDueDateRage != null && adjustDueDateRage.get(0)[0] != null && adjustDueDateRage.get(0)[1] != null) {
            minDueDate = (ZonedDateTime) adjustDueDateRage.get(0)[0];
            maxDueDate = (ZonedDateTime) adjustDueDateRage.get(0)[1];
            if (minDueDate.compareTo(maxDueDate) != 0) {
                basPcnDpInstInd = CHAR_N;
            }
        }

        if (cancellation.getRestartPcnDate() != null && apiRestartPcnInd == CHAR_Y) {
            basPcnDate = cancellation.getRestartPcnDate();
        }

        String basUserId = SYSTEM_USER_ID;
        String basAddDataTxt = createBilAddTxtData(basCanRea, basPcnDate, basPedDate, basPolicyId, basCancelType,
                basRestartPcnInd, basRestartPcnDate, basNsfActivityInd, basLdnLegalDays, basLdnMailDays, basLdnMaxDays,
                basPolStatusCd, basTermTypeCd, basPcnDpInstInd);

        Short bilActSequenNumber = writeHistory(bilAccount.getAccountId(), basBilAcyDesCd, bilPolicy.getPolNbr(),
                bilPolicy.getPolSymbolCd(), bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), basBilAcyDes2Date,
                basBilAcyAmount, basAddDataTxt, basUserId);

        if (addBasRnrInd == CHAR_Y) {
            basBilAcyDesCd = "RNR";
            basBilDesReaType = BLANK_STRING;
            basBilAcyDes2Date = DateRoutine.defaultDateTime();
            bilActSequenNumber = writeHistory(bilAccount.getAccountId(), basBilAcyDesCd, bilPolicy.getPolNbr(),
                    bilPolicy.getPolSymbolCd(), bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    basBilAcyDes2Date, DECIMAL_ZERO, BLANK_STRING, basUserId);
        }

        boolean rescindProcess = false;
        char canttRuleInd = readUnivCtl(accountProcessingAndRecovery.getRecoveryDate());
        if (canttRuleInd == CANTT_RULE_ACTIVE) {
            rescindProcess = false;
            boolean isRenewalPresent = checkForRenewal(bilAccount.getAccountId(), bilPolicyTerm, rescindProcess,
                    basBilAcyDesCd, isNsfPcnCancel, bilPolicy, splitIndicator, cancellation.isPolicyPedValued());
            if (isRenewalPresent) {
                basUserId = "CANTT";
            }
        }

        pcnRescindCollection.setBilAcyDesCode(basBilAcyDesCd);
        pcnRescindCollection.setBilAcyDate(accountProcessingAndRecovery.getRecoveryDate());
        pcnRescindCollection.setBilDesReaType(basBilDesReaType);
        pcnRescindCollection.setBilSequenceNumber(bilActSequenNumber);
        pcnRescindCollection.setPolPcnReason(pcnReason);
        pcnRescindCollection.setUserId(basUserId);
        pcnRescindCollection.setEquityDate(apiEquityDate);
        pcnRescindCollection.setCancelType(apiCancelType);
        pcnRescindCollection.setIssueSysId(apiIssueSysId);
        pcnRescindCollection.setNsfActivityIndicator(apiNsfActivityInd);
        pcnRescindCollection.setRestartPcnIndicator(apiRestartPcnInd);
        pcnRescindCollection.setRestartPcnDate(apiRestartPcnDate);
        pcnRescindCollection.setStandCncDate(standCncDate);
        pcnRescindCollection.setLdnLegalDays(basLdnLegalDays);
        pcnRescindCollection.setLdnMailDays(ldnMailDays);
        pcnRescindCollection.setLdnMaxDays(ldnMaxDays);
        pcnRescindCollection.setPcnFlatCncIndicator(pcnFlatCncInd);

        if (accountProcessingAndRecovery.getQuoteIndicator() != CHAR_Q) {
            wipPcn(bilAccount.getAccountId(), bilAccount.getAccountNumber(), bilPolicy.getPolNbr(),
                    bilPolicy.getPolSymbolCd(), bilAccount.getBillTypeCd());
        }

        return pedAcyMap;
    }

    private List<Character> getInvoiceCodeList() {
        List<Character> invoiceCodeList = new ArrayList<>();
        invoiceCodeList.add(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue());
        invoiceCodeList.add(InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());
        invoiceCodeList.add(InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue());
        return invoiceCodeList;
    }

    private void writeNewNblTrigger(boolean isTermNeverBilled, boolean isNsfPcnCancel, String accountId,
            ZonedDateTime bilIstDueDate, BilPolicyTerm bilPolicyTerm) {
        if (isTermNeverBilled && isNsfPcnCancel && bilPolicyTerm.getBillPolStatusCd() != CHAR_W) {
            BilPolActivityId bilPolActivityId = new BilPolActivityId(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), "NBL");
            BilPolActivity bilPolActivity = bilPolActivityRepository.findById(bilPolActivityId).orElse(null);
            if (bilPolActivity == null || bilPolActivity.getBilNxtAcyDt().compareTo(bilIstDueDate) != 0) {
                bilPolActivity = new BilPolActivity();
                bilPolActivity.setBillPolActivityId(bilPolActivityId);
                bilPolActivity.setBilNxtAcyDt(bilIstDueDate);
                bilPolActivityRepository.saveAndFlush(bilPolActivity);
            }
        }
    }

    private String evaluatePcnReasonCode(Cancellation cancellation, char pcnFlatCncInd, String accountId,
            String policyId, ZonedDateTime polEffDate) {
        String pcnReason = BLANK_STRING;
        if (pcnFlatCncInd != CHAR_Y) {
            pcnReason = "PNP";
            if (cancellation.getLapseCancIndicator() == LAPSE_CANC) {
                short reinstNewTrmCnt = checkTermType(accountId, policyId, polEffDate);
                if (reinstNewTrmCnt == 0) {
                    pcnReason = "RNP";
                }
            } else if (cancellation.getRestartPcnDate() != null) {
                pcnReason = cancellation.getOriginalPcnReason();
            }
        }
        return pcnReason;
    }

    private boolean getFpnpRuleData(String bilTypeCode, String bilClassCode, BilPolicyTerm bilPolicyTerm) {
        boolean isFpnpRule = false;
        BilRulesUct fpnpRuleUct = bilRulesUctService.readBilRulesUct("FPNP", bilTypeCode, bilClassCode, BLANK_STRING);
        if (fpnpRuleUct != null && fpnpRuleUct.getBrtRuleCd() == CHAR_Y) {
            isFpnpRule = true;
        } else if (fpnpRuleUct != null && fpnpRuleUct.getBrtRuleCd() == CHAR_S) {
            BilStRulesUct bilStRulesUct = accountReviewService.getBilStRuleUct("FPNP",
                    bilPolicyTerm.getBillStatePvnCd(), bilPolicyTerm.getLobCd(), bilPolicyTerm.getMasterCompanyNbr());
            if (bilStRulesUct != null && bilStRulesUct.getBilRuleCd() == CHAR_Y) {
                isFpnpRule = true;
            }
        }
        return isFpnpRule;
    }

    private char evaluateNewPolStatus(char pcnmMaxInd, boolean isNsfPcnCancel, char driverInd) {
        char newPolStatusCode;
        if (pcnmMaxInd == CHAR_Y || pcnmMaxInd == CHAR_I) {
            newPolStatusCode = BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue();
        } else if (isNsfPcnCancel) {
            newPolStatusCode = BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue();
        } else if (driverInd == NO_RESP_RENEWAL) {
            newPolStatusCode = BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue();
        } else {
            newPolStatusCode = BilPolicyStatusCode.PENDING_CANCELLATION.getValue();
        }
        return newPolStatusCode;
    }

    private char[] processByCmcnRule(char bcmoeavInd, BilAccount bilAccount, String policyId,
            ZonedDateTime polEffectiveDate, short pndCncNbr, ZonedDateTime recoveryDate) {
        char sendAltColMthInd = BLANK_CHAR;
        char premiumNsfAcyInd = BLANK_CHAR;
        BilRulesUct cmcnRuleUct = bilRulesUctService.readBilRulesUct("CMCN", bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), BLANK_STRING);
        if (cmcnRuleUct.getBrtRuleCd() == CHAR_Y) {
            if (bcmoeavInd == AUTO_COL_MTH_CHG) {
                sendAltColMthInd = CHAR_Y;
                premiumNsfAcyInd = CHAR_Y;
            } else {
                sendAltColMthInd = checkAutoColMthChg(bilAccount.getAccountId(), polEffectiveDate, recoveryDate);
                premiumNsfAcyInd = checkForNsfActivity(bilAccount.getAccountId(), policyId, polEffectiveDate, pndCncNbr,
                        recoveryDate);
            }
        }
        return new char[] { sendAltColMthInd, premiumNsfAcyInd };
    }

    private void pcnNotice(BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, String apiPolPcnReason,
            short ldnLegalDays, short ldnMailDays, short ldnMaxDays, BilPolicy bilPolicy, String basCanRea,
            ZonedDateTime basPcnDate, ZonedDateTime basPedDate, char basCancelType, char basRestartPcnInd,
            ZonedDateTime basRestartPcnDate, char basNsfActivityInd, char basPolStatusCd, char basTermTypeCd,
            AccountProcessingAndRecovery accountProcessingAndRecovery,
            PendingCancellationNotice pendingCancellationNotice) {
        String pcnNoticeSysId = bilPolicyTerm.getBptIssueSysId();
        ZonedDateTime basBilAcyDes2Date;
        if (pcnNoticeSysId.trim().isEmpty()) {
            pcnNoticeSysId = "UW";
        }
        String objectName = "BIL-PCN-NOTICE-" + pcnNoticeSysId;
        HalBoMduXrf halBoMduXrf = halBoMduXrfRepository.findById(objectName).orElse(null);
        if (halBoMduXrf != null && !halBoMduXrf.getHbmxBobjMduNm().trim().isEmpty()) {
            pendingCancellationNotice.setPolPcnReason(apiPolPcnReason);
            pendingCancellationNotice.setLdnLegalDays(ldnLegalDays);
            pendingCancellationNotice.setLdnMailDays(ldnMailDays);
            pendingCancellationNotice.setLdnMaxDays(ldnMaxDays);
            basBilAcyDes2Date = DateRoutine.getAdjustedDate(accountProcessingAndRecovery.getRecoveryDate(), false,
                    ldnLegalDays + ldnMailDays, null);
            if (basBilAcyDes2Date.isBefore(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt())
                    && accountProcessingAndRecovery.getDriverIndicator() == CHAR_O) {
                basBilAcyDes2Date = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
            }
            ZonedDateTime paidToDate = callCalculatePaidToDate(pendingCancellationNotice.getAccountId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilPolicyTerm.getPlnExpDt());
            if (basBilAcyDes2Date.isBefore(paidToDate) && accountProcessingAndRecovery.getDriverIndicator() == CHAR_O
                    && pendingCancellationNotice.getCancelType() == CHAR_E) {
                basBilAcyDes2Date = paidToDate;
            }
            pendingCancellationNotice.setCalcLegDate(DateRoutine.dateTimeAsYYYYMMDDString(basBilAcyDes2Date));
            String additionalDataTxt = createBilAddTxtData(basCanRea, basPcnDate, basPedDate,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), basCancelType, basRestartPcnInd,
                    basRestartPcnDate, basNsfActivityInd, ldnLegalDays, ldnMailDays, ldnMaxDays, basPolStatusCd,
                    basTermTypeCd, BLANK_CHAR);
            writeHistory(bilAccount.getAccountId(), "PCN", bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), DateRoutine.defaultDateTime(),
                    DECIMAL_ZERO, additionalDataTxt, "SYSTEM");

            callPcnNotice(bilAccount, bilPolicyTerm, accountProcessingAndRecovery.getRecoveryDate());
        }
    }

    private boolean checkForRenewal(String accountId, BilPolicyTerm bilPolicyTerm, boolean rescindProcess,
            String bilAcyDesCode, boolean isNsfPcnCancel, BilPolicy bilPolicy, char splitIndicator,
            boolean isPolicyPedValued) {
        boolean isRenewalPresent = false;
        char renewalStatusCd;
        BilPolicyTerm bilPolicyTerms = bilPolicyTermRepository.findRenewalTermIsPresent(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                new ArrayList<>(Arrays.asList(BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue(),
                        BilPolicyStatusCode.FLAT_CANCELLATION.getValue(), BilPolicyStatusCode.CANCELLED.getValue(),
                        BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(), 'R',
                        BilPolicyStatusCode.TRANSFERRED.getValue(),
                        BilPolicyStatusCode.SUSPENDBILLING_FOLLOWUP.getValue(),
                        BilPolicyStatusCode.SUSPEND_FOLLOWUP.getValue(),
                        BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                        BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue())));
        if (bilPolicyTerms == null) {
            return isRenewalPresent;
        }
        renewalStatusCd = bilPolicyTerms.getBillPolStatusCd();

        if (rescindProcess) {
            rescindProcess = false;
            if (renewalStatusCd != BilPolicyStatusCode.PENDING_CANCELLATION.getValue()
                    || renewalStatusCd != BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue()
                    || renewalStatusCd != BilPolicyStatusCode.OPEN.getValue()) {
                return isRenewalPresent;
            }
        }

        List<BilIstSchedule> bilIstSchedules = bilIstScheduleRepository.getRenewalPresentRows(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                new ArrayList<>(
                        Arrays.asList(BLANK_CHAR, InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue())),
                bilPolicyTerm.getPlnExpDt());
        if (bilIstSchedules != null && !bilIstSchedules.isEmpty()) {
            isRenewalPresent = true;
        } else {
            boolean isCancelSplitAccRwl = false;
            boolean isRescindAccRwl = false;
            char polStatusCode;
            if (bilAcyDesCode.equalsIgnoreCase("B")) {
                if (bilPolicyTerm.getBillPolStatusCd() == BilPolicyStatusCode.PENDING_CANCELLATION.getValue()
                        || bilPolicyTerm
                                .getBillPolStatusCd() == BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS
                                        .getValue()) {
                    polStatusCode = BilPolicyStatusCode.OPEN.getValue();
                    isCancelSplitAccRwl = true;
                    isRescindAccRwl = true;
                } else {
                    return isRenewalPresent;
                }
            } else {
                polStatusCode = BilPolicyStatusCode.PENDING_CANCELLATION.getValue();
                isCancelSplitAccRwl = true;
            }

            if (isNsfPcnCancel) {
                polStatusCode = BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue();
            }

            BilPolicyTerm updateBilPolicyTerm = bilPolicyTermRepository.findById(new BilPolicyTermId(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilPolicyTerm.getPlnExpDt())).orElse(null);
            if (updateBilPolicyTerm != null && updateBilPolicyTerm
                    .getBillPolStatusCd() != BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue()) {
                updateBilPolicyTerm.setBillPolStatusCd(polStatusCode);
                bilPolicyTermRepository.save(updateBilPolicyTerm);
            }

            String additionalDataTxt = BLANK_STRING;
            if (!bilAcyDesCode.equalsIgnoreCase("B")) {
                additionalDataTxt = "RNP";
            }

            writeHistory(accountId, BLANK_STRING, bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                    bilPolicyTerm.getPlnExpDt(), bilPolicyTerm.getPlnExpDt(), DECIMAL_ZERO, additionalDataTxt,
                    CANTT_USER);

            if (splitIndicator == ACCT_IS_SPLIT && isCancelSplitAccRwl) {
                String nonCausalTypeCd = NoncausalType.PCN_REQUEST.toString();
                if (isRescindAccRwl) {
                    nonCausalTypeCd = NoncausalType.RESCIND_REQUEST.toString();
                }
                callSplitBillingActivity(accountId, nonCausalTypeCd, isPolicyPedValued,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            }
        }
        return isRenewalPresent;
    }

    private void wipPcn(String accountId, String accountNumber, String polNumber, String polSymbolCd,
            String bilTypeCode) {
        int pcnWipNumber = 0;
        BilSystemRules bilSystemRules = bilSystemRulesRepository.findById(bilTypeCode).orElse(null);
        if (bilSystemRules != null) {
            pcnWipNumber = bilSystemRules.getBsrPcnWipNbr();
        }
        int pcnCount = 0;
        List<BilActSummary> bilActSummaries = bilActSummaryRepository.findActSummaryByUser(accountId, polNumber,
                polSymbolCd, new ArrayList<>(Arrays.asList("C", "RCN")), Arrays.asList(BLANK_STRING, "CAM"),
                CANTT_USER);
        if (bilActSummaries != null && !bilActSummaries.isEmpty()) {
            pcnCount = bilActSummaries.size();
        }

        if (pcnCount >= pcnWipNumber) {
            wipLink(accountId, accountNumber, WipAcitivityId.PCNEXCEED.getValue());
        }
    }

    private char readUnivCtl(ZonedDateTime recoveryDate) {
        char canttRuleInd = BLANK_CHAR;
        if (halUniversalCtl2Repository.findMaxEffectiveDate("CANTT", "Y", recoveryDate) != null) {
            canttRuleInd = CANTT_RULE_ACTIVE;
        }
        return canttRuleInd;
    }

    private String createBilAddTxtData(String basCanRea, ZonedDateTime basPcnDate, ZonedDateTime basPedDate,
            String basPolicyId, char basCancelType, char basRestartPcnInd, ZonedDateTime basRestartPcnDate,
            char basNsfActivityInd, short basLdnLegalDays, short basLdnMailDays, short basLdnMaxDays,
            char basPolStatusCd, char basTermTypeCd, char basPcnDpInstInd) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(StringUtils.leftPad(basCanRea, 3, BLANK_CHAR));
        String pcnDate = basPcnDate != null ? DateRoutine.dateTimeAsYYYYMMDDString(basPcnDate) : BLANK_STRING;
        stringBuilder.append(StringUtils.rightPad(pcnDate, 10, BLANK_CHAR));
        String pedDate = basPedDate != null ? DateRoutine.dateTimeAsYYYYMMDDString(basPedDate) : BLANK_STRING;
        stringBuilder.append(StringUtils.rightPad(pedDate, 10, BLANK_CHAR));
        stringBuilder.append(StringUtils.rightPad(basPolicyId, 16, BLANK_CHAR));
        stringBuilder.append(basCancelType);
        stringBuilder.append(basRestartPcnInd);
        if (basRestartPcnInd == BLANK_CHAR) {
            stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 10, BLANK_CHAR));
        } else {
            String restartDate = basRestartPcnDate != null ? DateRoutine.dateTimeAsYYYYMMDDString(basRestartPcnDate)
                    : BLANK_STRING;
            stringBuilder.append(StringUtils.rightPad(restartDate, 10, BLANK_CHAR));
        }
        stringBuilder.append(basNsfActivityInd);
        stringBuilder.append(StringUtils.rightPad(String.valueOf(basLdnLegalDays), 4, '0'));
        stringBuilder.append(StringUtils.rightPad(String.valueOf(basLdnMailDays), 4, '0'));
        stringBuilder.append(StringUtils.rightPad(String.valueOf(basLdnMaxDays), 4, '0'));
        stringBuilder.append(basPolStatusCd);
        stringBuilder.append(basTermTypeCd);
        stringBuilder.append(basPcnDpInstInd);
        stringBuilder.append(StringUtils.rightPad("0", 2, '0'));
        return stringBuilder.toString();
    }

    private Double[] evaluatePcnNext(String polPcnReason, boolean isNsfPcnCancel, String polNumber, String polSymbolCd,
            String pcniRuleData, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, ZonedDateTime recoveryDate,
            Map<String, Object> termDataMap) {
        char pcnPnpCode = pcniRuleData.substring(0, 1).charAt(0);
        char pcnRnpCode = pcniRuleData.substring(1, 2).charAt(0);
        short bsuPcnDays = Short.parseShort(pcniRuleData.substring(2, 5).trim());
        double nsfPcnAmount = DECIMAL_ZERO;
        double termPastAmount = DECIMAL_ZERO;

        label1: {
            if (polPcnReason.equalsIgnoreCase("RNP") && pcnRnpCode == CHAR_N
                    || polPcnReason.equalsIgnoreCase("PNP") && pcnPnpCode == CHAR_N) {
                break label1;
            }

            char pcnInvoiceCd = BLANK_CHAR;
            ZonedDateTime pcnInvDate = null;
            ZonedDateTime pcnAdjDueDate = null;
            ZonedDateTime pcnSysDueDate = null;
            ZonedDateTime pcnRefDate = null;
            ZonedDateTime maxBilReferenceDate = bilIstScheduleRepository.getMaxReferenceDateByInvoiceCd(
                    bilAccount.getAccountId(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    new ArrayList<>(Arrays.asList(BusCodeTranslationType.BILLING_ITEM.getValue())),
                    getInvoiceCodeList());
            if (maxBilReferenceDate != null) {
                BilIstSchedule bilIstSchedule = bilIstScheduleRepository.getScheduleRowByInvoiceCd(
                        bilAccount.getAccountId(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                        new ArrayList<>(Arrays.asList(BusCodeTranslationType.BILLING_ITEM.getValue())),
                        getInvoiceCodeList(), maxBilReferenceDate);
                if (bilIstSchedule != null) {
                    pcnInvDate = bilIstSchedule.getBillInvDt();
                    pcnInvoiceCd = bilIstSchedule.getBillInvoiceCd();
                    pcnAdjDueDate = bilIstSchedule.getBillAdjDueDt();
                    pcnSysDueDate = bilIstSchedule.getBillSysDueDt();
                    pcnRefDate = bilIstSchedule.getBillReferenceDt();
                } else {
                    break label1;
                }
            }

            List<Character> billInvoiceCdList = new ArrayList<>(
                    Arrays.asList(InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue(), BLANK_CHAR));
            ZonedDateTime minInvoiceDate = null;
            ZonedDateTime pcnAddDate = DateRoutine.getAdjustedDate(recoveryDate, false, bsuPcnDays, null);
            ZonedDateTime minReferenceDate = bilIstScheduleRepository.getMinReferenceDateByInvoiceCd(
                    bilAccount.getAccountId(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    new ArrayList<>(Arrays.asList(BusCodeTranslationType.BILLING_ITEM.getValue())), billInvoiceCdList);
            if (minReferenceDate != null) {
                minInvoiceDate = bilIstScheduleRepository.getMinInvoidDateRowByInvoiceCd(bilAccount.getAccountId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), pcnAddDate,
                        new ArrayList<>(Arrays.asList(BusCodeTranslationType.BILLING_ITEM.getValue())),
                        billInvoiceCdList, minReferenceDate);
            }
            if (minInvoiceDate == null) {
                break label1;
            }

            Object[] shedObject = processShed(isNsfPcnCancel, polNumber, polSymbolCd, pcnAdjDueDate, pcnAddDate,
                    pcnInvoiceCd, pcnInvDate, pcnSysDueDate, pcnRefDate, bilAccount, bilPolicyTerm, recoveryDate,
                    termDataMap);
            nsfPcnAmount = (double) shedObject[1];
            termPastAmount = (double) shedObject[2];
        }
        return new Double[] { nsfPcnAmount, termPastAmount };
    }

    private Object[] processShed(boolean isNsfPcnCancel, String polNumber, String polSymbolCd,
            ZonedDateTime pcnAdjDueDate, ZonedDateTime pcnAddDate, char pcnInvoiceCd, ZonedDateTime pcnInvDate,
            ZonedDateTime pcnSysDueDate, ZonedDateTime pcnRefDate, BilAccount bilaccount, BilPolicyTerm bilPolicyTerm,
            ZonedDateTime recoveryDate, Map<String, Object> termDataMap) {
        Double nsfPcnAmount = (Double) termDataMap.get(NsfPcnDataType.NSFPCNAMOUNT.getValue());
        Double termPastAmount = (Double) termDataMap.get(TermAcyType.TERMPASTAMOUNT.getValue());
        double totalPcnAdded = DECIMAL_ZERO;
        char nibTriggerInsertInd = BLANK_CHAR;
        Object[] pcnUpdateObj;

        List<Object[]> bilIstScheduleObject = bilIstScheduleRepository.getPcnObjectPresent(bilaccount.getAccountId(),
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                new ArrayList<>(Arrays.asList(BusCodeTranslationType.BILLING_ITEM.getValue())),
                new ArrayList<>(
                        Arrays.asList(BLANK_CHAR, InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue())),
                pcnAddDate);
        if (bilIstScheduleObject != null && !bilIstScheduleObject.isEmpty()) {
            pcnUpdateObj = pcnUpdate(bilIstScheduleObject, pcnInvoiceCd, pcnInvDate, pcnAdjDueDate, pcnSysDueDate,
                    pcnRefDate, isNsfPcnCancel, polNumber, polSymbolCd, bilaccount, bilPolicyTerm, recoveryDate);
            nibTriggerInsertInd = (char) pcnUpdateObj[0];
            termPastAmount = (double) pcnUpdateObj[1];
        } else {
            if (isNsfPcnCancel) {
                nsfPcnAmount = nsfPcnAmount + totalPcnAdded;
            }
            termPastAmount = termPastAmount + totalPcnAdded;
            if (totalPcnAdded > 0) {
                writeHistory(bilaccount.getAccountId(), "NIB", polNumber, polSymbolCd,
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), pcnAdjDueDate, totalPcnAdded,
                        BLANK_STRING, SYSTEM_USER_ID);
                nibTriggerInsertInd = CHAR_Y;
            }
        }
        return new Object[] { nibTriggerInsertInd, nsfPcnAmount, termPastAmount };
    }

    private Object[] pcnUpdate(List<Object[]> bilIstScheduleList, char pcnInvoiceCd, ZonedDateTime pcnInvDate,
            ZonedDateTime pcnAdjDueDate, ZonedDateTime pcnSysDueDate, ZonedDateTime pcnRefDate, boolean isNsfPcnCancel,
            String polNumber, String polSymbolCd, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm,
            ZonedDateTime recoveryDate) {
        char nibTriggerInsertInd = BLANK_CHAR;
        double nsfPcnAmount = DECIMAL_ZERO;
        double termPastAmount = DECIMAL_ZERO;
        double totalPcnAdded = DECIMAL_ZERO;
        String accountId = bilAccount.getAccountId();

        for (Object[] bisData : bilIstScheduleList) {
            double addAmount = (double) bisData[0];
            double commissionPct = (double) bisData[1];
            String payableItem = (String) bisData[5];
            Short bilSeqNumber = (Short) bisData[7];

            if ((pcnInvoiceCd == InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()
                    || pcnInvoiceCd == InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()
                    || pcnInvoiceCd == InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue())
                    && addAmount > 0) {
                if (pcnInvoiceCd != InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue()) {
                    pcnFws(bilAccount, commissionPct, addAmount, payableItem, bilPolicyTerm, recoveryDate);
                }
                totalPcnAdded = totalPcnAdded + addAmount;

                BilIstScheduleId bilIstScheduleId = new BilIstScheduleId(accountId,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilSeqNumber);
                BilIstSchedule bilIstSchedule = bilIstScheduleRepository.findById(bilIstScheduleId).orElse(null);
                if (bilIstSchedule != null) {
                    bilIstSchedule.setBillIstScheduleId(bilIstScheduleId);
                    bilIstSchedule.setBillInvDt(pcnInvDate);
                    bilIstSchedule.setBillAdjDueDt(pcnAdjDueDate);
                    bilIstSchedule.setBillSysDueDt(pcnSysDueDate);
                    bilIstSchedule.setBillReferenceDt(pcnRefDate);
                    bilIstSchedule.setBillInvoiceCd(pcnInvoiceCd);
                    bilIstScheduleRepository.save(bilIstSchedule);
                }

                updatePcnCreScheduleRow(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilSeqNumber,
                        pcnInvDate, pcnAdjDueDate, pcnSysDueDate);

                updatePcnCashDspRow(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilSeqNumber,
                        pcnInvDate, pcnAdjDueDate, pcnSysDueDate);

                if (isNsfPcnCancel) {
                    nsfPcnAmount = nsfPcnAmount + totalPcnAdded;
                }
                termPastAmount = termPastAmount + totalPcnAdded;
                if (totalPcnAdded > 0) {
                    writeHistory(accountId, "NIB", polNumber, polSymbolCd,
                            bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), pcnAdjDueDate, totalPcnAdded,
                            BLANK_STRING, SYSTEM_USER_ID);
                    nibTriggerInsertInd = CHAR_Y;
                }
            }
        }
        return new Object[] { nibTriggerInsertInd, termPastAmount };
    }

    private void updatePcnCashDspRow(String accountId, String policyId, short bilSequenceNumber,
            ZonedDateTime pcnInvDate, ZonedDateTime pcnAdjDueDate, ZonedDateTime pcnSysDueDate) {
        List<BilCashDsp> bilCashDspList = bilCashDspRepository.fetchPcnCashRow(policyId, accountId,
                BilDspTypeCode.APPLIED.getValue(), bilSequenceNumber);
        if (bilCashDspList != null) {
            for (BilCashDsp bilCashDsp : bilCashDspList) {
                bilCashDsp.setInvoiceDate(pcnInvDate);
                bilCashDsp.setAdjustmentDueDate(pcnAdjDueDate);
                bilCashDsp.setSystemDueDate(pcnSysDueDate);
                bilCashDspRepository.save(bilCashDsp);
            }
        }
    }

    private void updatePcnCreScheduleRow(String accountId, String policyId, short bilSeqNumber,
            ZonedDateTime pcnInvDate, ZonedDateTime pcnAdjDueDate, ZonedDateTime pcnSysDueDate) {
        List<BilCreSchedule> bilCreScheduleList = bilCreScheduleRepository.fetchBilCreScheduleRow(accountId, policyId,
                bilSeqNumber);
        if (bilCreScheduleList != null && !bilCreScheduleList.isEmpty()) {
            for (BilCreSchedule bilCreSchedule : bilCreScheduleList) {
                bilCreSchedule.setBilInvDt(pcnInvDate);
                bilCreSchedule.setBilAdjDueDt(pcnAdjDueDate);
                bilCreSchedule.setBilSysDueDt(pcnSysDueDate);
                bilCreScheduleRepository.save(bilCreSchedule);
            }
        }
    }

    private void pcnFws(BilAccount bilAccount, double commissionPct, double addAmount, String payableItemCode,
            BilPolicyTerm bilPolicyTerm, ZonedDateTime recoveryDate) {
        double commAmount = Math.round(commissionPct * 0.01 * addAmount);
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setFunctionCode(CHAR_A);
        financialApiActivity.setFolderId(bilAccount.getAccountId());
        financialApiActivity.setReferenceId(bilAccount.getAccountId());
        financialApiActivity.setApplicationName(BCMOAX);
        financialApiActivity.setUserId(BLANK_STRING);
        financialApiActivity.setErrorCode("0000");
        financialApiActivity.setTransactionObjectCode("PRM");
        financialApiActivity.setTransactionActionCode("BIL");
        financialApiActivity.setApplicationProductIdentifier("BCMS");
        financialApiActivity.setMasterCompanyNbr(bilPolicyTerm.getMasterCompanyNbr());
        financialApiActivity.setCompanyLocationNbr("00");
        financialApiActivity.setFinancialIndicator(CHAR_Y);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(recoveryDate));
        financialApiActivity.setOperatorId(BLANK_STRING);
        financialApiActivity.setAppProgramId(BCMOAX);
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setStateCode(bilPolicyTerm.getBillStatePvnCd());
        financialApiActivity.setCountryCode(bilPolicyTerm.getBillCountryCd());
        financialApiActivity.setLineOfBusCode(bilPolicyTerm.getLobCd());
        financialApiActivity.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setActivityAmount(addAmount);
        financialApiActivity.setActivityNetAmount(addAmount - commAmount);
        financialApiActivity.setCurrencyCode(bilAccount.getCurrencyCode());
        financialApiActivity.setOriginalEffectiveDate(
                DateRoutine.dateTimeAsYYYYMMDDString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()));
        financialApiActivity.setBilBankCode(BLANK_STRING);
        financialApiActivity.setReferenceDate(
                DateRoutine.dateTimeAsYYYYMMDDString(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(recoveryDate));
        financialApiActivity.setBilAccountId(bilAccount.getAccountNumber());
        financialApiActivity.setBilReasonCode(BLANK_STRING);
        financialApiActivity.setBilReceiptTypeCode(BLANK_STRING);
        financialApiActivity.setBilTypeCode(bilAccount.getBillTypeCd());
        financialApiActivity.setBilClassCode(bilAccount.getBillClassCd());
        financialApiActivity.setPayableItemCode(payableItemCode);
        financialApiActivity.setBilDatabaseKey(BLANK_STRING);
        financialApiActivity.setAgentTtyId(BLANK_STRING);
        financialApiActivity.setBilSourceCode(BLANK_STRING);
        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                dateService.currentDateTime());
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
    }

    private short checkTermType(String accountId, String policyId, ZonedDateTime polEffDate) {
        short reinstNewTrmCount = 0;
        BilAmounts bilAmounts = bilAmountsRepository.getInstTermType(accountId, policyId, polEffDate, '9',
                new ArrayList<>(Arrays.asList(CHAR_H, CHAR_V)));
        if (bilAmounts != null) {
            reinstNewTrmCount = 1;
        }
        return reinstNewTrmCount;
    }

    private char checkForNsfActivity(String accountId, String policyId, ZonedDateTime polEffectiveDate, short pndCncNbr,
            ZonedDateTime recoveryDate) {
        char premiumNsfAcyInd;
        ZonedDateTime currAdjDueDate = bilIstScheduleRepository.getMaxAdjDueDtByInvoice(accountId, policyId,
                getInvoiceCodeList());
        if (currAdjDueDate == null) {
            premiumNsfAcyInd = CHAR_N;
        } else {
            ZonedDateTime prevAdjDueDate = bilIstScheduleRepository.getMaxAdjDueDtByAdjustDate(accountId, policyId,
                    currAdjDueDate, CHAR_Y);
            if (prevAdjDueDate == null) {
                ZonedDateTime bilAcyDate = bilActSummaryRepository.findMaxNsfAcyDate(accountId, polEffectiveDate,
                        recoveryDate, "RN", "ENS", "AN", "ENS", "RN", "PRV", "NSF");
                if (bilAcyDate != null) {
                    premiumNsfAcyInd = CHAR_Y;
                } else {
                    premiumNsfAcyInd = CHAR_N;
                }
            } else {
                ZonedDateTime prevReviewDate = DateRoutine.getAdjustedDate(prevAdjDueDate, false, pndCncNbr, null);
                ZonedDateTime bilAcyDate = bilActSummaryRepository.findMaxNsfAcyDate(accountId, prevReviewDate,
                        recoveryDate, "RN", "ENS", "AN", "ENS", "RN", "PRV", "NSF");
                if (bilAcyDate != null) {
                    premiumNsfAcyInd = CHAR_Y;
                } else {
                    premiumNsfAcyInd = CHAR_N;
                }
            }
        }
        return premiumNsfAcyInd;
    }

    private char checkAutoColMthChg(String accountId, ZonedDateTime polEffectiveDate, ZonedDateTime recoveryDate) {
        char sendAltColMthInd = BLANK_CHAR;
        ZonedDateTime maxBilAcyDate = bilActSummaryRepository.findMaxAutoColAcyDate(accountId,
                new ArrayList<>(Arrays.asList("RN", "AN")), polEffectiveDate, DECIMAL_ZERO, "ENS");
        if (maxBilAcyDate != null) {
            maxBilAcyDate = bilActSummaryRepository.findMaxAutoColAcyDateByDesCode(accountId, "CMA", maxBilAcyDate,
                    BLANK_STRING);
            if (maxBilAcyDate == null) {
                return sendAltColMthInd;
            }
            BilActSummary bilActSummary = bilActSummaryRepository.getBilAcyDateByDate(accountId, maxBilAcyDate,
                    recoveryDate, new ArrayList<>(Arrays.asList("C", "RCN")), BLANK_STRING, "C", "CAM");
            if (bilActSummary != null) {
                sendAltColMthInd = CHAR_Y;
            }
        }
        return sendAltColMthInd;
    }

    private MaxPendingCancellation loadPcnmNsfPcn(boolean isTermNeverBilled, double nsfPcnAmount,
            double bilIstDueAmount, MaxPendingCancellation maxPendingCancellation, Map<String, Object> pedAcyMap,
            Map<String, Object> pcnnDataMap, short ldnMaxDays, short ldnMailDays, short ldnLegDays) {
        maxPendingCancellation.setPendingCancellationAmount(nsfPcnAmount);
        if (isTermNeverBilled) {
            maxPendingCancellation.setPendingCancellationAmount(bilIstDueAmount);
        }
        maxPendingCancellation.setEquityDate((ZonedDateTime) pedAcyMap.get(PedAcyType.ACREQUITYDATE.getValue()));
        maxPendingCancellation.setPedEffectiveDate((ZonedDateTime) pedAcyMap.get(PedAcyType.POLEFFDATE.getValue()));
        maxPendingCancellation.setPedExpirationDate((ZonedDateTime) pedAcyMap.get(PedAcyType.POLEXPDATE.getValue()));
        maxPendingCancellation.setLegalDays(ldnLegDays);
        maxPendingCancellation.setMailDays(ldnMailDays);
        maxPendingCancellation.setMaximumDays(ldnMaxDays);
        maxPendingCancellation.setPendingCancelFlatCnc((char) pcnnDataMap.get(PcnnType.PCNFLATCNCIND.getValue()));
        maxPendingCancellation.setReinstCode((char) pcnnDataMap.get(PcnnType.REINSTCODE.getValue()));
        maxPendingCancellation.setAddBasRenewalIndicator((char) pcnnDataMap.get(PcnnType.ADDBASRNRIND.getValue()));
        maxPendingCancellation.setQualifyPcn(true);
        return maxPendingCancellation;
    }

    private char evaluatePcnn(char polTermTypeCode, String accountId, BilPolicyTerm bilPolicyTerm,
            String bilParmListTxt, Map<String, Object> pcnnDataMap, ZonedDateTime recoveryDate) {
        char pcnnNbFlCncCode = bilParmListTxt.substring(0, 1).charAt(0);
        char pcnnNbReqDnrCd = bilParmListTxt.substring(1, 2).charAt(0);
        char pcnnRnFlCncCd = bilParmListTxt.substring(2, 3).charAt(0);
        char pcnnRnReqDnrCd = bilParmListTxt.substring(3, 4).charAt(0);
        char pcnFlatCncInd = BLANK_CHAR;
        char byPassPcnNsfInd = BLANK_CHAR;
        String pcnReason = BLANK_STRING;

        label1: {
            if (!(polTermTypeCode == CHAR_N
                    || polTermTypeCode == CHAR_M && (pcnnNbFlCncCode == CHAR_A || pcnnNbFlCncCode == CHAR_B)
                    || polTermTypeCode == POL_TERM_TYPE_CD_RN_1 || polTermTypeCode == POL_TERM_TYPE_CD_RN_2
                    || polTermTypeCode == POL_TERM_TYPE_CD_RN_3 || polTermTypeCode == POL_TERM_TYPE_CD_RN_4
                    || polTermTypeCode == POL_TERM_TYPE_CD_RN_5
                            && (pcnnNbFlCncCode == CHAR_A || pcnnNbFlCncCode == CHAR_B))) {
                break label1;
            }

            List<Object[]> adjustDueDateRage = bilIstScheduleRepository.fetchAdjDueDateRange(accountId,
                    getInvoiceCodeList(), bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            if (adjustDueDateRage != null && adjustDueDateRage.get(0)[0] != null
                    && adjustDueDateRage.get(0)[1] != null) {
                pcnnDataMap.put(PcnnType.MINDUEDATE.getValue(), adjustDueDateRage.get(0)[0]);
                pcnnDataMap.put(PcnnType.MAXDUEDATE.getValue(), adjustDueDateRage.get(0)[1]);
            }

            Integer pcnnCount = bilCashDspRepository.checkPcnnRow(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), BLANK_CHAR,
                    BilDspTypeCode.APPLIED.getValue(), DECIMAL_ZERO);
            if (pcnnCount != null) {
                break label1;
            }

            if (polTermTypeCode == CHAR_N || polTermTypeCode == CHAR_M) {
                if (pcnnNbFlCncCode == CHAR_A) {
                    pcnFlatCncInd = CHAR_Y;
                    pcnReason = "PNP";
                } else if (pcnnNbFlCncCode == CHAR_B) {
                    if (bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt().compareTo(recoveryDate) <= 0) {
                        pcnFlatCncInd = CHAR_Y;
                        pcnReason = "PNP";
                    } else {
                        byPassPcnNsfInd = CHAR_Y;
                        break label1;
                    }
                } else {
                    break label1;
                }
            }
            if (polTermTypeCode == POL_TERM_TYPE_CD_RN_1 || polTermTypeCode == POL_TERM_TYPE_CD_RN_2
                    || polTermTypeCode == POL_TERM_TYPE_CD_RN_3 || polTermTypeCode == POL_TERM_TYPE_CD_RN_4
                    || polTermTypeCode == POL_TERM_TYPE_CD_RN_5) {
                if (pcnnRnFlCncCd == CHAR_A) {
                    pcnFlatCncInd = CHAR_Y;
                    pcnReason = "RNP";
                } else if (pcnnRnFlCncCd == CHAR_B) {
                    if (bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt().compareTo(recoveryDate) <= 0) {
                        pcnFlatCncInd = CHAR_Y;
                        pcnReason = "RNP";
                    } else {
                        byPassPcnNsfInd = CHAR_Y;
                    }
                    break label1;
                }
            }

            if ((polTermTypeCode == CHAR_N || polTermTypeCode == POL_TERM_TYPE_CD_RN_1
                    || polTermTypeCode == POL_TERM_TYPE_CD_RN_2 || polTermTypeCode == POL_TERM_TYPE_CD_RN_3
                    || polTermTypeCode == POL_TERM_TYPE_CD_RN_4)
                    && (polTermTypeCode == CHAR_M && (pcnnNbReqDnrCd == CHAR_A || pcnnNbReqDnrCd == CHAR_B)
                            || polTermTypeCode == POL_TERM_TYPE_CD_RN_5
                                    && (pcnnRnReqDnrCd == CHAR_A || pcnnRnReqDnrCd == CHAR_B))) {
                pcnnDataMap.put(PcnnType.REINSTCODE.getValue(), CHAR_N);
                if (polTermTypeCode == CHAR_M && pcnnNbReqDnrCd == CHAR_A
                        || polTermTypeCode == POL_TERM_TYPE_CD_RN_5 && pcnnRnReqDnrCd == CHAR_A) {
                    pcnnDataMap.put(PcnnType.ADDBASRNRIND.getValue(), CHAR_Y);
                }

            }
        }

        pcnnDataMap.put(PcnnType.PCNFLATCNCIND.getValue(), pcnFlatCncInd);
        pcnnDataMap.put(PcnnType.BYPASSPCNNSFIND.getValue(), byPassPcnNsfInd);
        pcnnDataMap.put(PcnnType.PCNREASON.getValue(), pcnReason);

        return byPassPcnNsfInd;
    }

    private Short writeHistory(String accountId, String activityDesCd, String policyNumber, String policySymbolCd,
            ZonedDateTime activityDesc1Dt, ZonedDateTime activityDesc2Dt, double amount, String additionalDataTxt,
            String userId) {
        Short bilAcySeq = 0;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, dateService.currentDate());
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId,
                dateService.currentDate().with(LocalTime.MIN), bilAcySeq);
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(policySymbolCd);
        bilActSummary.setPolNbr(policyNumber);
        bilActSummary.setBilAcyDesCd(activityDesCd);
        bilActSummary.setBilDesReaTyp(BLANK_STRING);
        bilActSummary.setBilAcyDes1Dt(activityDesc1Dt);
        bilActSummary.setBilAcyDes2Dt(activityDesc2Dt);
        bilActSummary.setBilAcyAmt(amount);
        bilActSummary.setUserId(userId);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());
        bilActSummary.setBasAddDataTxt(additionalDataTxt);
        bilActSummaryRepository.saveAndFlush(bilActSummary);

        return bilAcySeq;
    }

    private void callPcnNotice(BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, ZonedDateTime recoveryDate) {
        String additionalKey = buildAdditionalKeyText(bilAccount.getAccountNumber(),
                bilPolicyTerm.getMasterCompanyNbr(), bilPolicyTerm.getBillStatePvnCd(), recoveryDate);

        logger.debug("Send a POST RestTemplate for PrintInterface to Pb360 Backend");
        pb360Service.postGeneratePrintNoticeRequest(bilAccount.getAccountId(), "BCMONTC1",
                bilAccount.getPayorClientId(), "BCMS4000", additionalKey, "PCNNOTICE");
    }

    private String buildAdditionalKeyText(String accountNumber, String masterCompanyNumber,
            String polPrintRiskStateCode, ZonedDateTime recoveryDate) {
        logger.debug("Started: making additional key tex for new print request body");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(CHAR_A);
        stringBuilder.append(StringUtils.left(DateRoutine.dateTimeAsYYYYMMDDString(recoveryDate), 10));
        stringBuilder.append("01");
        stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 20, SEPARATOR_BLANK));
        stringBuilder.append(StringUtils.rightPad(accountNumber, 30, SEPARATOR_BLANK));
        stringBuilder.append(StringUtils.rightPad(masterCompanyNumber, 2, SEPARATOR_BLANK));
        stringBuilder.append(StringUtils.rightPad(polPrintRiskStateCode, 3, SEPARATOR_BLANK));
        stringBuilder.append(SEPARATOR_BLANK);
        logger.debug("Binding additional key data: {}", stringBuilder);
        logger.debug("Finished: Building AdditionalKeyText completed");
        return stringBuilder.toString();
    }

    private String getModuleName(String interfaceObjName) {
        String interfacePgmName = BLANK_STRING;
        HalBoMduXrf halBoMduXrf = halBoMduXrfRepository.findById(interfaceObjName).orElse(null);
        if (halBoMduXrf != null) {
            interfacePgmName = halBoMduXrf.getHbmxBobjMduNm();
        }
        return interfacePgmName;
    }

    @Override
    public void wipLink(String accountId, String accountNumber, String activityTemplateId) {
        logger.debug("Starts calling to PostActivityDriver module");
        activityService.postActivity(activityTemplateId, WipObjectCode.BILL_ACCOUNT.getValue(), accountNumber,
                accountId);
    }

    @Override
    public ZonedDateTime callCalculatePolicyPaidToDate(String accountId, String policyId, ZonedDateTime pedEffDate,
            ZonedDateTime pedExpDate) {
        logger.debug("Starts calling to CalculatePolicyPaidToDate module");
        return calculatePaidToDateService.getPaidToDate(accountId, policyId, pedEffDate, pedExpDate, null, false, true);
    }

    @Override
    public ZonedDateTime callCalculatePaidToDate(String accountId, String policyId, ZonedDateTime pedEffDate,
            ZonedDateTime pedExpDate) {
        logger.debug("Starts calling to CalculatePaidToDate module");
        return calculatePaidToDateService.getPaidToDate(accountId, policyId, pedEffDate, pedExpDate, null, false,
                false);
    }

    @Override
    public void callSplitBillingActivity(String accountId, String nonCausalTypeCode, boolean isPolicyPedValued,
            String policyId, ZonedDateTime polEffectiveDate) {
        logger.debug("Starts calling to SplitBillingActivity module");
        splitBillingActivityService.noncausalRequest(nonCausalTypeCode, isPolicyPedValued, accountId, policyId,
                polEffectiveDate);
    }

    public enum PedAcyType {
        POLEFFDATE("pedPolEffDate"),
        POLEXPDATE("pedPolExpDate"),
        NEXTACYDATE("pedNextAcyDate"),
        POLICYID("pedPolicyId"),
        ISSUESYSID("pedIssueSysId"),
        EQUITYIND("pedEquityInd"),
        ACREQUITYDATE("pedAcrEquityDate"),
        BILPLANCODE("pedBilPlanCode"),
        NSFACYDATE("pedNsfAcyDate");

        private final String pedAcyTyp;

        private PedAcyType(String pedAcyTyp) {
            this.pedAcyTyp = pedAcyTyp;
        }

        public String getValue() {
            return pedAcyTyp;
        }
    }

    public enum PcnnType {
        PCNFLATCNCIND("pcnFlatCncInd"),
        REINSTCODE("reinstCode"),
        ADDBASRNRIND("addBasRnrInd"),
        BYPASSPCNNSFIND("byPassPcnNsfInd"),
        PCNREASON("pcnReason"),
        MINDUEDATE("minDueDate"),
        MAXDUEDATE("maxDueDate");

        private final String pcnnTyp;

        private PcnnType(String pcnnTyp) {
            this.pcnnTyp = pcnnTyp;
        }

        public String getValue() {
            return pcnnTyp;
        }
    }

    private PendingCancellationNotice setParamPendingCancellationNotice(
            PendingCancellationNotice pendingCancellationNotice, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm,
            BilPolicy bilPolicy, PcnRescindCollection pcnRescindCollection) {
        pendingCancellationNotice.setAccountId(bilAccount.getAccountId());
        pendingCancellationNotice.setAccountNumber(bilAccount.getAccountNumber());
        pendingCancellationNotice.setBilTypeCd(bilAccount.getBillTypeCd());
        pendingCancellationNotice.setBilClassCd(bilAccount.getBillClassCd());
        pendingCancellationNotice.setBilCollectionPln(bilAccount.getCollectionPlan());
        pendingCancellationNotice.setPayCltId(bilAccount.getPayorClientId());
        pendingCancellationNotice.setPayAdrSeq(bilAccount.getAddressSequenceNumber());
        pendingCancellationNotice.setPayAdrSeqSign(PLUS_SIGN);
        if (bilAccount.getAddressSequenceNumber() < 0) {
            pendingCancellationNotice.setPayAdrSeqSign(MINUS_SIGN);
        }
        pendingCancellationNotice.setSe3Dt(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()));
        pendingCancellationNotice.setPolicyId(pcnRescindCollection.getPolicyId());
        pendingCancellationNotice.setPolSymbolCd(bilPolicy.getPolSymbolCd());
        pendingCancellationNotice.setPolicyNumber(bilPolicy.getPolNbr());
        if (pcnRescindCollection.getEffectiveDate() != null) {
            pendingCancellationNotice
                    .setPolEffDt(DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getEffectiveDate()));
        }
        if (pcnRescindCollection.getPlanExpDate() != null) {
            pendingCancellationNotice
                    .setPolExpDt(DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getPlanExpDate()));
        }
        pendingCancellationNotice.setLobCd(bilPolicyTerm.getLobCd());
        pendingCancellationNotice.setMasterCoNbr(bilPolicyTerm.getMasterCompanyNbr());
        pendingCancellationNotice.setPolPriRskStCd(bilPolicyTerm.getBillStatePvnCd());
        pendingCancellationNotice.setIsuCltId(bilPolicyTerm.getBptIsuCltId());
        pendingCancellationNotice.setIsuAdrSeq(bilPolicyTerm.getBptIsuAdrSeq());
        pendingCancellationNotice.setIsuAdrSeqSign(PLUS_SIGN);
        if (bilPolicyTerm.getBptIsuAdrSeq() < 0) {
            pendingCancellationNotice.setIsuAdrSeqSign(MINUS_SIGN);
        }
        pendingCancellationNotice.setAgtCltId(bilPolicyTerm.getBptAgtCltId());
        pendingCancellationNotice.setAgtAdrSeq(bilPolicyTerm.getBptAgtAdrSeq());
        pendingCancellationNotice.setAgtAdrSeqSign(PLUS_SIGN);
        if (bilPolicyTerm.getBptAgtAdrSeq() < 0) {
            pendingCancellationNotice.setAgtAdrSeqSign(MINUS_SIGN);
        }
        pendingCancellationNotice.setPolPcnReason(pcnRescindCollection.getPolPcnReason());
        pendingCancellationNotice.setEffectiveTypeCd(pcnRescindCollection.getEffTypeCode());
        pendingCancellationNotice.setUserId(pcnRescindCollection.getUserId());
        if (pcnRescindCollection.getActivityDate() != null) {
            pendingCancellationNotice
                    .setActivityDt(DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getActivityDate()));
        }
        pendingCancellationNotice.setCancelType(pcnRescindCollection.getCancelType());
        if (pcnRescindCollection.getEquityDate() != null) {
            pendingCancellationNotice
                    .setEquityDt(DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getEquityDate()));
        }
        if (pcnRescindCollection.getStandCncDate() != null) {
            pendingCancellationNotice
                    .setStandCncDate(DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getStandCncDate()));
        }

        pendingCancellationNotice.setIssueSystemId(pcnRescindCollection.getIssueSysId());
        pendingCancellationNotice.setAutoColMthChg(pcnRescindCollection.getAutoCollectionMethodChange());
        pendingCancellationNotice.setNsfActivityInd(pcnRescindCollection.getNsfActivityIndicator());
        pendingCancellationNotice.setRestartPcnInd(pcnRescindCollection.getRestartPcnIndicator());
        if (pcnRescindCollection.getRestartPcnDate() != null) {
            pendingCancellationNotice
                    .setRestartPcnDt(DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getRestartPcnDate()));
        }
        pendingCancellationNotice.setReqPgmName(BCMOAX);
        pendingCancellationNotice.setQueueWrtInd(accountReviewService.getQueueWrtIndicator());
        return pendingCancellationNotice;
    }

    private Short[] getLegalDays(String processCode, BilBillPlan bilBillPlan, BilPolicyTerm bilPolicyTerm,
            BilPolicy bilPolicy) {
        String issueSysId = "UW";
        short ldnMaxDays = (short) 0;
        short ldnMailDays = (short) 0;
        short ldnLegDays = (short) 0;
        legalDaysFndInd = false;
        if (bilBillPlan.getBbpInvDuringPcnInd() == CHAR_Y && bilBillPlan.getBbpAltLglDays() > 0) {
            ldnMaxDays = bilBillPlan.getBbpAltLglDays();
            ldnLegDays = bilBillPlan.getBbpAltLglDays();
            ldnMailDays = 0;
            legalDaysFndInd = true;
        }

        if (bilBillPlan.getBbpInvDuringPcnInd() == CHAR_N || bilBillPlan.getBbpAltLglDays() == 0) {
            if (!bilPolicyTerm.getBptIssueSysId().trim().isEmpty()) {
                issueSysId = bilPolicyTerm.getBptIssueSysId();
            }
            Short[] policyLegalObject = getPolicyLegalDays(processCode, issueSysId, bilPolicyTerm, bilPolicy);
            if (policyLegalObject != null && policyLegalObject.length != 0) {
                ldnMaxDays = policyLegalObject[0];
                ldnMailDays = policyLegalObject[1];
                ldnLegDays = policyLegalObject[2];
                legalDaysFndInd = true;
            }
        }
        return new Short[] { ldnMaxDays, ldnMailDays, ldnLegDays };
    }

    private Object[] detectMinPcnDate(String processCode, char pedEquityInd, ZonedDateTime pedPolEffDate,
            ZonedDateTime recoveryDate, BilBillPlan bilBillPlan, BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy) {
        ZonedDateTime minPcnDate = null;
        Short[] legalDays = getLegalDays(processCode, bilBillPlan, bilPolicyTerm, bilPolicy);
        short ldnMaxDays = legalDays[0];
        short ldnMailDays = legalDays[1];
        short ldnLegDays = legalDays[2];
        if (bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt().compareTo(recoveryDate) > 0) {
            minPcnDate = getMinPcnDate(pedPolEffDate, ldnLegDays, ldnMailDays, bilBillPlan.getBbpInvDuringPcnInd());
        } else {
            minPcnDate = DateRoutine.defaultDateTime();
        }
        ZonedDateTime[] legalDateArray = calculateLegalDays(ldnLegDays, ldnMailDays, ldnMaxDays, pedEquityInd,
                recoveryDate);
        ZonedDateTime standCncDate = legalDateArray[0];
        ZonedDateTime pedMinDate = legalDateArray[1];
        ZonedDateTime pedMaxDate = legalDateArray[2];
        return new Object[] { minPcnDate, standCncDate, pedMinDate, pedMaxDate, ldnMaxDays, ldnMailDays, ldnLegDays };
    }

    @Override
    public Short[] getPolicyLegalDays(String processCode, String issueSysId, BilPolicyTerm bilPolicyTerm,
            BilPolicy bilPolicy) {

        logger.debug("Starts calling to PolicyLegalDayInterface module get LegalDay set");

        String interfaceObjName = "BIL-INT-POL-LEG-DAYS-";

        interfaceObjName = interfaceObjName + issueSysId.trim();
        String interfacePgmName = getModuleName(interfaceObjName);

        if (interfacePgmName.trim().isEmpty()) {
            throw new DataNotFoundException("module.not.exists", new Object[] { interfaceObjName });
        } else {
            List<LegalDaysNotice> legalDaysNoticeList = legalDaysNoticeRepository.getLegalDaysNoticeData(
                    Arrays.asList(bilPolicyTerm.getLobCd().trim(), "ZZZ"),
                    Arrays.asList(bilPolicy.getPolSymbolCd().trim(), "ZZZ"),
                    Arrays.asList(bilPolicyTerm.getBillStatePvnCd().trim(), "ZZZ"),
                    Arrays.asList(bilPolicyTerm.getMasterCompanyNbr().trim(), "99"), processCode,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), Arrays.asList('P', 'R', '*'));
            if (legalDaysNoticeList != null && !legalDaysNoticeList.isEmpty()) {
                LegalDaysNotice legalDaysNotice = legalDaysNoticeList.get(SHORT_ZERO);
                return new Short[] { legalDaysNotice.getMaximumLegalDaysNumber(),
                        legalDaysNotice.getMailLegalDaysNumber(), legalDaysNotice.getLegalDaysNumber() };

            }
        }
        return new Short[0];
    }
}
