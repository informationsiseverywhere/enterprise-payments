package billing.handler.impl;

import static billing.utils.BillingConstants.SYSTEM_USER_ID;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_E;
import static core.utils.CommonConstants.CHAR_I;
import static core.utils.CommonConstants.CHAR_M;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import application.handler.model.MaxPcnData;
import application.handler.service.MaxPcnUserExitService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilActInquiry;
import billing.data.entity.BilActRules;
import billing.data.entity.BilAmounts;
import billing.data.entity.BilBillPlan;
import billing.data.entity.BilPolActivity;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.LegalDaysNotice;
import billing.data.entity.id.BilAmountsId;
import billing.data.entity.id.BilPolActivityId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilAmountsRepository;
import billing.data.repository.BilBillPlanRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilDatesRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.LegalDaysNoticeRepository;
import billing.handler.impl.CancellationServiceImpl.PcnnType;
import billing.handler.impl.CancellationServiceImpl.PedAcyType;
import billing.handler.impl.SplitBillingActivityServiceImpl.NoncausalType;
import billing.handler.model.AccountProcessingAndRecovery;
import billing.handler.model.AccountReviewDetails;
import billing.handler.model.Cancellation;
import billing.handler.model.MaxPendingCancellation;
import billing.handler.model.PolicyReview;
import billing.handler.model.Rescission;
import billing.handler.service.AccountProcessingAndRecoveryService;
import billing.handler.service.BillingRulesService;
import billing.handler.service.CancellationService;
import billing.handler.service.PolicyReviewService;
import billing.handler.service.RescissionService;
import billing.model.Contract;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.AccountStatus;
import billing.utils.BillingConstants.AccountTypeCode;
import billing.utils.BillingConstants.BilAcyDesCode;
import billing.utils.BillingConstants.BilAmountsEftIndicator;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CreditIndicator;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.ManualSuspendIndicator;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import billing.utils.BillingConstants.WipAcitivityId;
import core.exception.database.DataNotFoundException;
import core.utils.DateRoutine;

@Service("policyReviewService")
public class PolicyReviewServiceImpl implements PolicyReviewService {

    public static final Logger logger = LogManager.getLogger(PolicyReviewServiceImpl.class);

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilBillPlanRepository bilBillPlanRepository;

    @Autowired
    private CancellationService cancellationService;

    @Autowired
    private RescissionService rescissionService;

    @Autowired
    private BilAmountsRepository bilAmountsRepository;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private BilDatesRepository bilDatesRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BillingRulesService billingRulesService;

    @Autowired
    private AccountProcessingAndRecoveryService accountProcessingAndRecoveryService;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private MaxPcnUserExitService maxPcnUserExitService;

    @Autowired
    private LegalDaysNoticeRepository legalDaysNoticeRepository;

    private static final char PREV_CANC = CHAR_Y;
    private static final char INVOICING = CHAR_Y;
    private static final char DRIVER_ACTIVE = CHAR_Y;
    private static final char CASH_ACTIVITY = CHAR_Y;
    private static final char CHECK_FOR_REINSTATEMENT = 'R';
    private static final String POLICY_EQUITY_DATE = "PED";
    private static final char PLUS_SIGN = '+';
    private static final char CANCEL_PCN_REQ = CHAR_Y;
    private static final char ACCT_IS_SPLIT = CHAR_Y;
    private static final String PCN_IST_DUE_NOT_FOUND = "pcn.ist.due.not.found";
    private static final char NO_RESP_RENEWAL = 'R';

    @Override
    public BilBillPlan processPolicies(BilAccount bilAccount, AccountReviewDetails accountReviewDetails,
            AccountProcessingAndRecovery accountProcessingAndRecovery, PolicyReview policyReview,
            boolean isBillPlanChange) {

        String prevPolicyId = BLANK_STRING;
        String prevBilPlanCode = BLANK_STRING;
        BilBillPlan bilBillPlan = null;
        BilPolicy bilPolicy = null;

        boolean isRescind = accountProcessingAndRecovery.isForceResind();
        boolean isEftResumeSus = false;
        boolean isPolicyPedValued = false;
        boolean isEquityUsed = false;
        char reviewDatesIndicator = BLANK_CHAR;
        char lapseCancIndicator = BLANK_CHAR;
        char pcnmMaxIndicator = BLANK_CHAR;
        char invoicingIndicator = BLANK_CHAR;
        char eftCollectionIndicator = BLANK_CHAR;
        char useCrgDateIndicator = BLANK_CHAR;
        char renewDpCancInd = BLANK_CHAR;
        String originalPcnReason = accountProcessingAndRecovery.getPendingCancelType();
        ZonedDateTime restartPcnDate = null;
        ZonedDateTime termRnpDate = DateRoutine.defaultDateTime();
        ZonedDateTime firstInstDueDt = DateRoutine.defaultDateTime();
        Map<String, Object> pedAcyMap = null;
        List<MaxPendingCancellation> maxPendingCancellationList = new ArrayList<>();
        Map<String, Object> termDataMap = initializationTermDataMap();
        Map<String, Object> pcnnDataMap = initializationPcnnData();
        Cancellation cancellation = null;
        int pcnmPcnCount = 0;
        BilRulesUct pcnmRules = null;
        boolean isPcnmRule = false;
        Integer pcnmNumOfMonth = 0;
        ZonedDateTime firstPcnCntDt = DateRoutine.defaultStartDateTime();
        int pcnmRulePcnCount = 0;
        if ((accountReviewDetails.getSuspendBillIndicator() != CHAR_Y)
                && (accountProcessingAndRecovery.getDriverIndicator() == CHAR_O
                        || accountProcessingAndRecovery.isNsfPcn())) {
            pcnmRules = bilRulesUctService.readBilRulesUct("PCNM", bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd(), BLANK_STRING);
            if (pcnmRules != null && pcnmRules.getBrtRuleCd() == CHAR_Y) {
                isPcnmRule = true;

                try {
                    pcnmRulePcnCount = Integer.valueOf(pcnmRules.getBrtParmListTxt().trim().substring(0, 2));
                } catch (Exception e) {
                    logger.debug("Parsing error: {}", e.getMessage());
                }

                if (pcnmRules != null && pcnmRules.getBrtParmListTxt() != null
                        && pcnmRules.getBrtParmListTxt().length() >= 4) {
                    pcnmNumOfMonth = Integer.valueOf(pcnmRules.getBrtParmListTxt().trim().substring(3, 4));
                    if (pcnmNumOfMonth > 0) {
                        firstPcnCntDt = accountProcessingAndRecovery.getRecoveryDate().minusMonths(pcnmNumOfMonth);
                    }
                }

            }
        }

        isEquityUsed = deterIsEquityProcessing(bilAccount.getAccountId());
        char prevPolStatus;
        char writeOffProcessInd = BLANK_CHAR;

        List<BilPolicyTerm> bilPolicyTermList = new ArrayList<>();
        List<Contract> contractList = accountProcessingAndRecovery.getContractList();
        if (contractList != null && !contractList.isEmpty()
                && accountProcessingAndRecovery.getDriverIndicator() == CHAR_O) {
            if (isPcnmRule) {
                Integer pcnmCounter = 0;
                Integer currentPcnCount = 0;
                currentPcnCount = bilActSummaryRepository.countPcnByActDate(bilAccount.getAccountId(), firstPcnCntDt,
                        accountProcessingAndRecovery.getRecoveryDate(), BilAcyDesCode.REQUEST_NONPAY_CANCEL.getValue(),
                        BLANK_STRING, BilAcyDesCode.RESTART_PCN.getValue(), BLANK_STRING,
                        BilAcyDesCode.REQUEST_NONPAY_CANCEL.getValue(), "CAM");
                pcnmCounter = currentPcnCount;

                if (pcnmCounter >= pcnmRulePcnCount) {
                    bilPolicyTermList = bilPolicyTermRepository.findBilPolicyByPolicyStatus(bilAccount.getAccountId(),
                            getPolStatusList());
                }
            }
            if (bilPolicyTermList.isEmpty()) {
                for (Contract contract : contractList) {
                    BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository.getPolicyTerm(bilAccount.getAccountId(),
                            contract.getPolicyEffectiveDate(), contract.getPolicyId());
                    bilPolicyTermList.add(bilPolicyTerm);
                }
            }
        } else {
            bilPolicyTermList = bilPolicyTermRepository.findBilPolicyByPolicyStatus(bilAccount.getAccountId(),
                    getPolStatusList());
        }

        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
            if (!bilPolicyTerm.getBillPolicyTermId().getPolicyId().equalsIgnoreCase(prevPolicyId)) {
                pedAcyMap = initializationPedAcyData();
            }
            MaxPendingCancellation maxPendingCancellation = null;
            char polStatusCode = bilPolicyTerm.getBillPolStatusCd();
            String policyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
            String bilPlanCode = bilPolicyTerm.getBillPlanCd();
            writeOffProcessInd = bilPolicyTerm.getBptWroPrcInd();

            if (bilPlanCode.compareTo(prevBilPlanCode) != 0) {
                bilBillPlan = getBilBillPlan(bilPlanCode);
            }
            if (!policyId.equalsIgnoreCase(prevPolicyId)) {
                bilPolicy = bilPolicyRepository.findById(new BilPolicyId(policyId, bilAccount.getAccountId()))
                        .orElse(null);
                processCashActivity(bilAccount, bilPolicyTerm, bilBillPlan, accountReviewDetails,
                        accountProcessingAndRecovery, policyReview);
            }

            if (!accountProcessingAndRecovery.isNsfPcn()) {
                Short[] legalDays = getLegalDays(originalPcnReason, bilBillPlan, bilPolicyTerm, bilPolicy);
                short legalDaysNoticeMailDays = legalDays[1];
                short legalDaysNoticeLegDays = legalDays[2];

                ZonedDateTime polExpirationDate = bilPolicyTerm.getPlnExpDt();
                ZonedDateTime cancellationEffectiveDate = DateRoutine.getAdjustedDate(
                        accountProcessingAndRecovery.getRecoveryDate(),
                        false, legalDaysNoticeLegDays + legalDaysNoticeMailDays, null);
                if (cancellationEffectiveDate.isAfter(polExpirationDate)) {
                    continue;
                }
            }

            double termBalanceAmount = DECIMAL_ZERO;
            ZonedDateTime lastInvoiceAdjDueDate = DateRoutine.defaultDateTime();

            if (bilPolicyTerm.getBptAgreementInd() == CHAR_Y) {
                termBalanceAmount = getAgreementTrmBalance(bilAccount.getAccountId(), policyId,
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            } else if (isCheckTermData(polStatusCode, bilPolicyTerm.getPlnExpDt(), bilPolicyTerm.getBptStatusEffDt(),
                    accountProcessingAndRecovery.getRecoveryDate())) {
                if (!policyId.equalsIgnoreCase(prevPolicyId) && accountProcessingAndRecovery.isNsfPcn()) {
                    pedAcyMap = equityCanc(bilAccount.getAccountId(), policyId, bilPolicyTerm, bilBillPlan,
                            accountProcessingAndRecovery);
                }
                Object[] termObject = termData(bilAccount, bilPolicyTerm, bilBillPlan, invoicingIndicator,
                        eftCollectionIndicator, accountReviewDetails.getBilSupportPlan().getBspPndCncNbr(), termDataMap,
                        pedAcyMap, accountProcessingAndRecovery);
                termBalanceAmount = (double) termObject[0];
                lastInvoiceAdjDueDate = (ZonedDateTime) termObject[1];
                useCrgDateIndicator = (char) termObject[2];
            }

            prevPolStatus = bilPolicyTerm.getBillPolStatusCd();
            if (polStatusCode == BilPolicyStatusCode.CLOSED.getValue()
                    || polStatusCode == BilPolicyStatusCode.OPEN.getValue()) {
                Object[] statusObject = rescissionService.status(isRescind, isBillPlanChange, termBalanceAmount,
                        polStatusCode, bilPolicyTerm.getBillSusFuReaCd());
                polStatusCode = (char) statusObject[0];
            }

            if (isRescission(polStatusCode)) {
                Rescission rescission = new Rescission(bilAccount.getAccountId(), isRescind, termBalanceAmount,
                        isBillPlanChange, isRescind, policyReview.getNextReviewDate(),
                        policyReview.getNextInvoiceDate());
                polStatusCode = rescissionService.processRescission(polStatusCode, accountProcessingAndRecovery,
                        bilAccount, bilBillPlan, bilPolicy, bilPolicyTerm, accountReviewDetails, rescission,
                        termDataMap, isEquityUsed);
                isRescind = rescission.isRescind();
                policyReview.setNextReviewDate(rescission.getNextReviewDate());
                policyReview.setNextInvoiceDate(rescission.getNextInvoiceDate());
            }

            char prevCancIndicator;
            if (polStatusCode == BilPolicyStatusCode.SUSPEND_BILLING.getValue()) {
                prevCancIndicator = readLastBam(bilAccount.getAccountId(), bilPolicyTerm);
            } else {
                prevCancIndicator = CHAR_N;
            }

            Object[] check1stInst = null;
            if (polStatusCode == BilPolicyStatusCode.OPEN.getValue()
                    && accountReviewDetails.getSuspendBillIndicator() != CHAR_Y
                    && accountProcessingAndRecovery.getDriverIndicator() == CHAR_O && invoicingIndicator != INVOICING
                    && bilPolicyTerm.getBptAgreementInd() != CHAR_Y) {

                BilSupportPlan bilSupportPlan = accountReviewDetails.getBilSupportPlan();
                check1stInst = check1stInst(bilAccount, bilPolicyTerm, bilBillPlan, bilSupportPlan, lapseCancIndicator);
                firstInstDueDt = (ZonedDateTime) check1stInst[0];
                renewDpCancInd = (char) check1stInst[1];
                lapseCancIndicator = (char) check1stInst[2];
                termRnpDate = (ZonedDateTime) check1stInst[3];
            }
            boolean isNsfPcnCancel = (boolean) termDataMap.get(NsfPcnDataType.NSFPCNCANCEL.getValue());
            boolean isFutureEquity = (boolean) termDataMap.get(NsfPcnDataType.FUTUREEQUITY.getValue());
            char bisTermInvoiceIndicator = (char) termDataMap.get(TermAcyType.TERMINVOICEINDICATOR.getValue());
            boolean isTermNeverBilled = (boolean) termDataMap.get(NsfPcnDataType.TERMNEVERBILLED.getValue());
            ZonedDateTime bilIstDueDt = (ZonedDateTime) termDataMap.get(NsfPcnDataType.BILISTDUEDATE.getValue());

            if (isPcnmRule && polStatusCode == BilPolicyStatusCode.OPEN.getValue()
                    || polStatusCode == BilPolicyStatusCode.SUSPEND_BILLING.getValue()) {
                if (accountProcessingAndRecovery.getDriverIndicator() == CHAR_O) {
                    if (bilBillPlan.getBbpPndCncInd() == CHAR_Y || bilPolicyTerm.getBptAgreementInd() == CHAR_Y
                            || termBalanceAmount <= DECIMAL_ZERO
                            || (lapseCancIndicator == CHAR_Y
                                    && (bilPolicyTerm.getPlnExpDt()
                                            .compareTo(accountProcessingAndRecovery.getRecoveryDate()) <= 0)
                                    || firstInstDueDt.equals(DateRoutine.defaultDateTime())
                                    || lastInvoiceAdjDueDate.isAfter(accountProcessingAndRecovery.getRecoveryDate())
                                    || (eftCollectionIndicator == CHAR_Y && bilAccount.getStatus() == CHAR_Y))) {
                        maxPendingCancellation = null;
                    } else {
                        maxPendingCancellation = loadPcnmData(polStatusCode, isNsfPcnCancel, bisTermInvoiceIndicator,
                                isTermNeverBilled, bilIstDueDt, bilPolicyTerm, bilPolicy, bilBillPlan);
                    }
                } else {
                    if (bilBillPlan.getBbpPndCncInd() == CHAR_Y || bilPolicyTerm.getBptAgreementInd() == CHAR_Y
                            || termBalanceAmount <= DECIMAL_ZERO || lapseCancIndicator == CHAR_Y
                            || bilPolicyTerm.getPlnExpDt()
                                    .compareTo(accountProcessingAndRecovery.getRecoveryDate()) <= 0
                            || eftCollectionIndicator == CHAR_Y && bilAccount.getStatus() == CHAR_Y) {
                        maxPendingCancellation = null;
                    } else {
                        maxPendingCancellation = loadPcnmData(polStatusCode, isNsfPcnCancel, bisTermInvoiceIndicator,
                                isTermNeverBilled, bilIstDueDt, bilPolicyTerm, bilPolicy, bilBillPlan);
                    }
                }
            }

            if (isCancellation(polStatusCode, accountReviewDetails.getSuspendBillIndicator(), invoicingIndicator,
                    bilPolicyTerm.getBptAgreementInd(), prevCancIndicator, accountProcessingAndRecovery.isNsfPcn(),
                    accountProcessingAndRecovery.getDriverIndicator(), isFutureEquity, isNsfPcnCancel)) {
                if (bilBillPlan.getBbpPndCncInd() == CHAR_Y) {
                    continue;
                }

                cancellation = new Cancellation(bilAccount.getAccountId(), invoicingIndicator, eftCollectionIndicator,
                        accountProcessingAndRecovery.getEftReversalIndicator(), reviewDatesIndicator,
                        lapseCancIndicator, pcnmMaxIndicator, isEftResumeSus, isPolicyPedValued, originalPcnReason,
                        restartPcnDate, isEquityUsed);
                Object[] nsfPcn = cancellationService.processNsfPcn(isPcnmRule, maxPendingCancellation, bilAccount,
                        bilPolicy, bilPolicyTerm, bilBillPlan, accountReviewDetails, cancellation, pedAcyMap,
                        termDataMap, accountProcessingAndRecovery, pcnnDataMap);
                polStatusCode = (char) nsfPcn[0];
                if ((MaxPendingCancellation) nsfPcn[1] != null) {
                    maxPendingCancellation = (MaxPendingCancellation) nsfPcn[1];
                    pcnmPcnCount += 1;
                }
            } else if (originalPcnReason != null
                    && originalPcnReason.equals(BilDesReasonType.RENEWAL_NONPAYMENT_CANCELLATION.getValue())
                    && polStatusCode != BilPolicyStatusCode.QUOTE.getValue()
                    && polStatusCode != BilPolicyStatusCode.PENDING_CANCELLATION.getValue()
                    && polStatusCode != BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue()
                    && polStatusCode != BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue()
                    && polStatusCode != BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue()) {
                if (bilBillPlan.getBbpInvDuringPcnInd() != CHAR_Y) {

                    cancellation = new Cancellation(bilAccount.getAccountId(), invoicingIndicator,
                            eftCollectionIndicator, accountProcessingAndRecovery.getEftReversalIndicator(),
                            reviewDatesIndicator, lapseCancIndicator, pcnmMaxIndicator, isEftResumeSus,
                            isPolicyPedValued, originalPcnReason, restartPcnDate, isEquityUsed);

                    Object[] rnpPcn = cancellationService.processRnpPcn(isPcnmRule, useCrgDateIndicator, renewDpCancInd,
                            termRnpDate, firstInstDueDt, bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan,
                            accountProcessingAndRecovery, accountReviewDetails, maxPendingCancellation, cancellation,
                            policyReview, pedAcyMap, termDataMap, pcnnDataMap, lapseCancIndicator, contractList);
                    polStatusCode = (char) rnpPcn[0];
                    if ((MaxPendingCancellation) rnpPcn[1] != null) {
                        maxPendingCancellation = (MaxPendingCancellation) rnpPcn[1];
                        pcnmPcnCount += 1;
                    }
                }
            } else if ((polStatusCode == BilPolicyStatusCode.OPEN.getValue()
                    || polStatusCode == BilPolicyStatusCode.SUSPEND_BILLING.getValue())
                    && accountReviewDetails.getSuspendBillIndicator() != CHAR_Y
                    && (accountProcessingAndRecovery.getDriverIndicator() == CHAR_O
                            || accountProcessingAndRecovery.getDriverIndicator() == NO_RESP_RENEWAL)
                    && invoicingIndicator != INVOICING && bilPolicyTerm.getBptAgreementInd() != CHAR_Y
                    && prevCancIndicator != PREV_CANC) {

                cancellation = new Cancellation(bilAccount.getAccountId(), invoicingIndicator, eftCollectionIndicator,
                        accountProcessingAndRecovery.getEftReversalIndicator(), reviewDatesIndicator,
                        lapseCancIndicator, pcnmMaxIndicator, isEftResumeSus, isPolicyPedValued, originalPcnReason,
                        restartPcnDate, isEquityUsed);

                Object[] pcnPcn = cancellationService.processPnpPcn(isPcnmRule, useCrgDateIndicator, bilAccount,
                        bilPolicyTerm, bilPolicy, bilBillPlan, accountProcessingAndRecovery, accountReviewDetails,
                        maxPendingCancellation, cancellation, policyReview, pedAcyMap, pcnnDataMap, termDataMap,
                        contractList);
                polStatusCode = (char) pcnPcn[0];
                if ((MaxPendingCancellation) pcnPcn[1] != null) {
                    maxPendingCancellation = (MaxPendingCancellation) pcnPcn[1];
                    pcnmPcnCount += 1;
                }
            }

            if ((bilPolicyTerm.getBptWroPrcInd() == BLANK_CHAR || bilPolicyTerm.getBptWroPrcInd() == CHAR_M)
                    && bilPolicyTerm.getBptAuditCd() != CHAR_Y && bilPolicyTerm.getBptAgreementInd() != CHAR_Y) {

                short cntPolSta = checkPolicyStatus(bilAccount, accountProcessingAndRecovery.getRecoveryDate());
                Object[] writeObject = writeOff(polStatusCode, bilAccount, bilPolicyTerm, cntPolSta,
                        policyReview.getNextWriteoffDate(), bilBillPlan, accountProcessingAndRecovery.getRecoveryDate());
                ZonedDateTime nextWriteoffDate = (ZonedDateTime) writeObject[0];
                writeOffProcessInd = (char) writeObject[1];

                policyReview.setNextWriteoffDate(nextWriteoffDate);
            }

            if (!isCancelForReopen(prevPolStatus)) {
                checkPendingCancelTerm(bilAccount.getAccountId(), polStatusCode, prevPolStatus, writeOffProcessInd,
                        bilPolicyTerm);
            }

            if (!policyId.equals(prevPolicyId) && isBillPlanChange) {
                List<BilPolicyTerm> bilPolicyTermsList = bilPolicyTermRepository
                        .getPolicyByMaxPolEffectiveDate(bilAccount.getAccountId(), policyId, Arrays.asList('T', 'V'));
                if (bilPolicyTermsList != null && !bilPolicyTermsList.isEmpty()) {
                    BilPolicyTerm bilPolicyTerms = bilPolicyTermsList.get(SHORT_ZERO);
                    BilPolicy bilPolicys = bilPolicyRepository
                            .findById(new BilPolicyId(policyId, bilAccount.getAccountId())).orElse(null);
                    if (bilPolicys != null) {
                        if (bilPolicys.getBilPlanCd().compareTo(bilPolicyTerms.getBillPlanCd()) != 0) {
                            bilPolicys.setBilPlanCd(bilPolicyTerms.getBillPlanCd());
                            bilPolicyRepository.saveAndFlush(bilPolicys);
                        } else {
                            policyReview.setNextReviewDate(DateRoutine.defaultDateTime());
                        }
                    }
                }
            }

            prevPolicyId = policyId;
            prevBilPlanCode = bilPlanCode;

            if (maxPendingCancellation != null) {
                maxPendingCancellationList.add(maxPendingCancellation);
            }
        }

        if (bilBillPlan == null) {
            bilBillPlan = new BilBillPlan();
        }

        if (maxPendingCancellationList != null && !maxPendingCancellationList.isEmpty()) {
            Integer pcnCount = 0;

            if (pcnmRules != null && pcnmRules.getBrtParmListTxt() != null
                    && pcnmRules.getBrtParmListTxt().length() >= 4) {
                pcnmNumOfMonth = Integer.valueOf(pcnmRules.getBrtParmListTxt().trim().substring(3, 4));
            }
            if (pcnmNumOfMonth > 0) {
                firstPcnCntDt = accountProcessingAndRecovery.getRecoveryDate().minusMonths(pcnmNumOfMonth);
            }

            pcnCount = bilActSummaryRepository.countPcnByActDate(bilAccount.getAccountId(), firstPcnCntDt,
                    accountProcessingAndRecovery.getRecoveryDate(), BilAcyDesCode.REQUEST_NONPAY_CANCEL.getValue(),
                    BLANK_STRING, BilAcyDesCode.RESTART_PCN.getValue(), BLANK_STRING,
                    BilAcyDesCode.REQUEST_NONPAY_CANCEL.getValue(), "CAM");
            if (pcnCount > 0) {
                pcnmPcnCount += pcnCount;
            }

            if (cancellation == null) {
                cancellation = new Cancellation(bilAccount.getAccountId(), invoicingIndicator, eftCollectionIndicator,
                        accountProcessingAndRecovery.getEftReversalIndicator(), reviewDatesIndicator,
                        lapseCancIndicator, pcnmMaxIndicator, isEftResumeSus, isPolicyPedValued, originalPcnReason,
                        restartPcnDate, isEquityUsed);
            }
            processPcnMax(bilAccount, maxPendingCancellationList, pcnmRules, pedAcyMap, bilBillPlan, termDataMap,
                    accountProcessingAndRecovery, cancellation, pcnnDataMap, accountReviewDetails.getBilSupportPlan(),
                    writeOffProcessInd, pcnmPcnCount, pcnmRulePcnCount, pcnmNumOfMonth);
        }

        return bilBillPlan;
    }

    private Object[] check1stInst(BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, BilBillPlan bilBillPlan,
            BilSupportPlan bilSupportPlan, char lapseCancIndicator) {

        char renewDpCancInd = CHAR_N;
        String accountId = bilPolicyTerm.getBillPolicyTermId().getBilAccountId();
        String policyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
        Double othInstPaidAmount;
        Double firstInstPaidAmount;
        ZonedDateTime defaultDate = DateRoutine.defaultDateTime();
        ZonedDateTime termRnpDate = defaultDate;
        ZonedDateTime firstInstDueDt = defaultDate;
        ZonedDateTime polEffectiveDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();

        List<Character> invoiceTypeCodeList = Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue(),
                InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue());

        ZonedDateTime minBilAdjDueDate = bilIstScheduleRepository.getMinBilAdjDueDate(accountId, invoiceTypeCodeList,
                policyId, BusCodeTranslationType.BILLING_ITEM.getValue());
        if (minBilAdjDueDate != null) {
            firstInstDueDt = minBilAdjDueDate;
        } else {
            return new Object[] { firstInstDueDt, renewDpCancInd, lapseCancIndicator, termRnpDate };
        }

        Double firstInstPaidAmountResult1 = bilIstScheduleRepository.getFirstInstPaidAmount(accountId,
                invoiceTypeCodeList, policyId, BusCodeTranslationType.BILLING_ITEM.getValue(), firstInstDueDt);
        if (firstInstPaidAmountResult1 != null) {
            firstInstPaidAmount = firstInstPaidAmountResult1;
        } else {
            return new Object[] { firstInstDueDt, renewDpCancInd, lapseCancIndicator, termRnpDate };
        }

        Object[] renewDpPymt = null;
        Object[] rnpDate = null;
        if (firstInstPaidAmount > 0 && bilAccount.getBillTypeCd().compareTo("SP") == 0
                && bilBillPlan.getBbpFltCanPpInd() == CHAR_Y) {
            renewDpPymt = checkRenewDpPymt(bilPolicyTerm, bilBillPlan, firstInstDueDt, firstInstPaidAmountResult1);
            renewDpCancInd = (char) renewDpPymt[0];
        }

        if (firstInstPaidAmount == 0 || renewDpCancInd == CHAR_Y) {
            Double firstInstPaidAmountResult2 = bilIstScheduleRepository.getFirstInstPaidAmountWithoutBillAdjDueDate(
                    accountId, invoiceTypeCodeList, policyId, polEffectiveDate,
                    BusCodeTranslationType.BILLING_ITEM.getValue());
            if (firstInstPaidAmountResult2 != null) {
                othInstPaidAmount = firstInstPaidAmountResult2;
                if (othInstPaidAmount == 0 || renewDpCancInd == CHAR_Y) {
                    rnpDate = calculateRnpDate(bilSupportPlan, firstInstDueDt);
                    termRnpDate = (ZonedDateTime) rnpDate[0];
                    lapseCancIndicator = (char) rnpDate[1];
                } else {
                    return new Object[] { firstInstDueDt, renewDpCancInd, lapseCancIndicator, termRnpDate };
                }
            } else {
                rnpDate = calculateRnpDate(bilSupportPlan, firstInstDueDt);
                termRnpDate = (ZonedDateTime) rnpDate[0];
                lapseCancIndicator = (char) rnpDate[1];
            }
        }

        return new Object[] { firstInstDueDt, renewDpCancInd, lapseCancIndicator, termRnpDate };
    }

    private Object[] calculateRnpDate(BilSupportPlan bilSupportPlan, ZonedDateTime firstInstDueDt) {

        char lapseCancIndicator = CHAR_Y;

        ZonedDateTime termRnpDate = DateRoutine.getAdjustedDate(firstInstDueDt, false, bilSupportPlan.getBspPndCncNbr(),
                null);

        return new Object[] { termRnpDate, lapseCancIndicator };
    }

    private Object[] checkRenewDpPymt(BilPolicyTerm bilPolicyTerm, BilBillPlan bilBillPlan,
            ZonedDateTime firstInstDueDt, Double firstInstPaidAmount) {

        char renewDpCancInd = CHAR_N;
        String policyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
        String accountId = bilPolicyTerm.getBillPolicyTermId().getBilAccountId();
        ZonedDateTime polEffectiveDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
        Double firstInstPastAmount = DECIMAL_ZERO;
        Double firstInstPastPct;

        Double firstInstDueAmount = bilIstScheduleRepository.getFirstInstDueAmount(accountId, policyId,
                polEffectiveDate, firstInstDueDt, BusCodeTranslationType.BILLING_ITEM.getValue());

        if (firstInstDueAmount != null) {
            firstInstPastAmount = firstInstDueAmount - firstInstPaidAmount;
        }

        firstInstPastPct = BigDecimal.valueOf(firstInstPastAmount / firstInstDueAmount * 100)
                .setScale(6, RoundingMode.UP).doubleValue();
        if (bilBillPlan.getBbpPenCanAmt() == 0 && bilBillPlan.getBbpPenCanPct() == 0 && firstInstPastAmount > 0) {
            renewDpCancInd = CHAR_Y;
        } else {
            if (bilBillPlan.getBbpPenCanAmt() == 0 && bilBillPlan.getBbpPenCanPct() != 0
                    && firstInstPastPct >= bilBillPlan.getBbpPenCanPct()) {
                renewDpCancInd = CHAR_Y;
            } else {
                if ((bilBillPlan.getBbpPenCanPct() == 0 && bilBillPlan.getBbpPenCanAmt() != 0
                        && firstInstPastAmount >= bilBillPlan.getBbpPenCanAmt())
                        || (firstInstPastAmount >= bilBillPlan.getBbpPenCanAmt()
                                && firstInstPastPct >= bilBillPlan.getBbpPenCanPct())) {
                    renewDpCancInd = CHAR_Y;
                }
            }
        }

        return new Object[] { renewDpCancInd };
    }

    private void checkPendingCancelTerm(String accountId, char polStatusCode, char prevPolStatus,
            char writeOffProcessInd, BilPolicyTerm bilPolicyTerm) {

        if (polStatusCode != prevPolStatus || writeOffProcessInd != bilPolicyTerm.getBptWroPrcInd()) {
            char pcnStatus = checkPendingCancelStatus(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilPolicyTerm.getPlnExpDt());
            if (pcnStatus != BilPolicyStatusCode.OPEN.getValue()
                    || pcnStatus != BilPolicyStatusCode.PENDING_CANCELLATION.getValue()) {
                BilPolicyTerm bilPolicyTermUpdate = new BilPolicyTerm();
                bilPolicyTermUpdate.setBillPolStatusCd(polStatusCode);
                bilPolicyTermUpdate.setBptWroPrcInd(writeOffProcessInd);

                BilPolicyTermId bilPolicyTermIdUpdate = new BilPolicyTermId(accountId,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
                bilPolicyTermUpdate.setBillPolicyTermId(bilPolicyTermIdUpdate);

                bilPolicyTermUpdate.setPlnExpDt(bilPolicyTerm.getPlnExpDt());
                bilPolicyTermUpdate.setBptStatusEffDt(bilPolicyTerm.getBptStatusEffDt());
                bilPolicyTermUpdate.setBillSusFuReaCd(bilPolicyTerm.getBillSusFuReaCd());
                bilPolicyTermUpdate.setBptPcnCausalInd(bilPolicyTerm.getBptPcnCausalInd());

                updateBilPolicyTerm(bilPolicyTermUpdate);
            }
        }
    }

    private void processPcnMax(BilAccount bilAccount, List<MaxPendingCancellation> maxPendingCancellationList,
            BilRulesUct pcnmRules, Map<String, Object> pedAcyMap, BilBillPlan bilBillPlan,
            Map<String, Object> termDataMap, AccountProcessingAndRecovery accountProcessingAndRecovery,
            Cancellation cancellation, Map<String, Object> pcnnDataMap, BilSupportPlan bilSupportPlan,
            char writeOffProcessInd, int pcnmPcnCount, int pcnmRulePcnCount, Integer pcnmNumOfMonth) {
        try {
            pcnmRulePcnCount = Integer.valueOf(pcnmRules.getBrtParmListTxt().trim().substring(0, 2));

        } catch (Exception e) {
            logger.debug("Parsing error: {}", e.getMessage());
        }
        MaxPcnData maxPcnData = new MaxPcnData(bilAccount.getAccountId().trim(), pcnmPcnCount, pcnmRulePcnCount,
                pcnmNumOfMonth, false, false);
        maxPcnUserExitService.maxPcnProcess(maxPcnData);
        if (maxPcnData.isVerifyMaxPCN()) {
            if (maxPcnData.isMaxPcn()) {
                maxPcnProcess(bilAccount, maxPendingCancellationList, pedAcyMap, bilBillPlan, termDataMap,
                        accountProcessingAndRecovery, cancellation, pcnnDataMap, bilSupportPlan, writeOffProcessInd);
            } else {
                regularPcnProcess(bilAccount, maxPendingCancellationList, pedAcyMap, bilBillPlan, termDataMap,
                        accountProcessingAndRecovery, cancellation, pcnnDataMap, bilSupportPlan, writeOffProcessInd);
            }
        } else {
            if (maxPendingCancellationList.size() < pcnmRulePcnCount) {
                regularPcnProcess(bilAccount, maxPendingCancellationList, pedAcyMap, bilBillPlan, termDataMap,
                        accountProcessingAndRecovery, cancellation, pcnnDataMap, bilSupportPlan, writeOffProcessInd);
            } else {
                maxPcnProcess(bilAccount, maxPendingCancellationList, pedAcyMap, bilBillPlan, termDataMap,
                        accountProcessingAndRecovery, cancellation, pcnnDataMap, bilSupportPlan, writeOffProcessInd);
            }
        }
    }

    private void maxPcnProcess(BilAccount bilAccount, List<MaxPendingCancellation> maxPendingCancellationList,
            Map<String, Object> pedAcyMap, BilBillPlan bilBillPlan, Map<String, Object> termDataMap,
            AccountProcessingAndRecovery accountProcessingAndRecovery, Cancellation cancellation,
            Map<String, Object> pcnnDataMap, BilSupportPlan bilSupportPlan, char writeOffProcessInd) {
        String accountId = bilAccount.getAccountId();

        accountProcessingAndRecoveryService.writeHistory(accountId, BilAcyDesCode.REQUEST_NONPAY_CANCEL.getValue(),
                "AMP", BLANK_STRING, BLANK_STRING, DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime(),
                BLANK_STRING, SYSTEM_USER_ID, accountProcessingAndRecovery.getRecoveryDate());
        pedAcyMap.put(PedAcyType.POLEFFDATE.getValue(), DateRoutine.defaultDateTime());
        pedAcyMap.put(PedAcyType.POLEXPDATE.getValue(), DateRoutine.defaultDateTime());

        for (MaxPendingCancellation maxPendingCancellation : maxPendingCancellationList) {
            char pcnmMaxIndicator = cancellation.getPcnmMaxIndicator();
            String policyId = maxPendingCancellation.getPolicyId();
            boolean isPriorPcnPresent = false;
            Integer priorTerminPcn = bilPolicyTermRepository.getPriorTermCount(accountId, policyId,
                    maxPendingCancellation.getEffectiveDate(),
                    new ArrayList<>(Arrays.asList(CHAR_P, CHAR_E, CHAR_M, CHAR_N)));
            if (priorTerminPcn != null && priorTerminPcn > 0) {
                isPriorPcnPresent = true;
            }
            if (policyId == null || policyId.trim().isEmpty() || maxPendingCancellation.isNoPcnCpe()
                    || maxPendingCancellation.isNoPcnCpn() || !accountProcessingAndRecovery.isNsfPcn()
                            && !maxPendingCancellation.isBisTermInvoice() && !isPriorPcnPresent) {
                continue;
            }

            pedAcyMap.put(PedAcyType.EQUITYIND.getValue(), CHAR_N);
            if (maxPendingCancellation.getCancellationIndicator() == CHAR_E
                    && maxPendingCancellation.getEquityDate().compareTo(DateRoutine.defaultDateTime()) != 0) {
                pedAcyMap.put(PedAcyType.EQUITYIND.getValue(), CHAR_Y);
            }
            if (!maxPendingCancellation.isQualifyPcn()) {
                boolean isNoPcnWip = false;
                if (maxPendingCancellation.getStandredCancellationDate()
                        .compareTo(maxPendingCancellation.getExpirationDate()) >= 0
                        && maxPendingCancellation.getStandredCancellationDate()
                                .compareTo(DateRoutine.defaultDateTime()) != 0
                        || maxPendingCancellation.getEquityDate()
                                .compareTo(maxPendingCancellation.getExpirationDate()) >= 0
                                && maxPendingCancellation.getEquityDate()
                                        .compareTo(DateRoutine.defaultDateTime()) != 0) {
                    isNoPcnWip = true;
                    char cancelPcnReqInd = cancellationService.checkForPrevHistory("CPE", accountId,
                            maxPendingCancellation.getPolicyNumber(), maxPendingCancellation.getPolicySymbol(),
                            maxPendingCancellation.getEffectiveDate(), maxPendingCancellation.getExpirationDate());
                    if (cancelPcnReqInd == CANCEL_PCN_REQ) {
                        cancellationService.wipLink(accountId, bilAccount.getAccountNumber(),
                                WipAcitivityId.CANCELPCN.getValue());
                    }
                } else if (maxPendingCancellation.getMinPendingCancellationDate()
                        .compareTo(accountProcessingAndRecovery.getRecoveryDate()) > 0
                        && maxPendingCancellation.getMinPendingCancellationDate()
                                .compareTo(DateRoutine.defaultDateTime()) != 0) {
                    isNoPcnWip = true;
                    char cancelPcnReq2Ind = cancellationService.checkForPrevHistory("CPN", accountId,
                            maxPendingCancellation.getPolicyNumber(), maxPendingCancellation.getPolicySymbol(),
                            maxPendingCancellation.getEffectiveDate(), maxPendingCancellation.getExpirationDate());
                    if (cancelPcnReq2Ind == CANCEL_PCN_REQ) {
                        cancellationService.wipLink(accountId, bilAccount.getAccountNumber(),
                                WipAcitivityId.CANCELPCN2.getValue());
                    }
                }

                if (!isNoPcnWip) {
                    double termPremBalancePercent = Math.round(maxPendingCancellation.getTermBalance()
                            / maxPendingCancellation.getTermPremimumAmount() * 100.00);
                    if (bilBillPlan.getBbpPndCncAmt() == 0 && bilBillPlan.getBbpPndCncPct() == 0
                            && maxPendingCancellation.getTermBalance() > 0
                            || bilBillPlan.getBbpPndCncAmt() == 0 && bilBillPlan.getBbpPndCncPct() != 0
                                    && termPremBalancePercent >= bilBillPlan.getBbpPndCncPct()
                            || bilBillPlan.getBbpPndCncAmt() != 0 && bilBillPlan.getBbpPndCncPct() == 0
                                    && maxPendingCancellation.getTermBalance() >= bilBillPlan.getBbpPndCncAmt()
                            || bilBillPlan.getBbpPndCncAmt() != 0 && bilBillPlan.getBbpPndCncPct() != 0
                                    && termPremBalancePercent >= bilBillPlan.getBbpPndCncPct()
                                    && maxPendingCancellation.getTermBalance() >= bilBillPlan.getBbpPndCncAmt()) {
                        maxPendingCancellation.setQualifyPcn(true);
                        pcnmMaxIndicator = CHAR_Y;
                    }
                }
            }

            char pcnStatus = checkPendingCancelStatus(accountId, policyId, maxPendingCancellation.getEffectiveDate(),
                    maxPendingCancellation.getExpirationDate());
            if (pcnStatus != BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue()
                    && maxPendingCancellation.isQualifyPcn()) {
                if (pcnmMaxIndicator != CHAR_Y) {
                    pcnmMaxIndicator = CHAR_I;
                }

                char splitIndicator = Boolean.TRUE.equals(maxPendingCancellation.isSplitPolicy()) ? CHAR_Y : BLANK_CHAR;
                char polTermTypeCode = cancellationService.getPolTermTypeCode(accountId, policyId,
                        maxPendingCancellation.getEffectiveDate());

                termDataMap.put(NsfPcnDataType.NSFPCNCANCEL.getValue(), maxPendingCancellation.isNsfPcnCancel());
                termDataMap.put(TermAcyType.TERMPASTAMOUNT.getValue(), maxPendingCancellation.getTermBalance());
                if (maxPendingCancellation.isNsfPcnCancel()) {
                    termDataMap.put(NsfPcnDataType.BILISTDUEAMOUNT.getValue(), maxPendingCancellation.getTermBalance());
                    termDataMap.put(NsfPcnDataType.NSFPCNAMOUNT.getValue(), maxPendingCancellation.getTermBalance());
                    pcnnDataMap.put(PcnnType.PCNFLATCNCIND.getValue(),
                            maxPendingCancellation.getPendingCancelFlatCnc());
                    pcnnDataMap.put(PcnnType.REINSTCODE.getValue(), maxPendingCancellation.getReinstCode());
                    pcnnDataMap.put(PcnnType.ADDBASRNRIND.getValue(),
                            maxPendingCancellation.getAddBasRenewalIndicator());
                } else {
                    termDataMap.put(NsfPcnDataType.BILISTDUEAMOUNT.getValue(), DECIMAL_ZERO);
                    termDataMap.put(NsfPcnDataType.NSFPCNAMOUNT.getValue(), DECIMAL_ZERO);
                }

                pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), maxPendingCancellation.getEquityDate());

                BilPolicy bilPolicy = new BilPolicy();
                bilPolicy.setPolNbr(maxPendingCancellation.getPolicyNumber());
                bilPolicy.setPolSymbolCd(maxPendingCancellation.getPolicySymbol());
                BilPolicyTerm bilPolicyTerm = new BilPolicyTerm();
                BilPolicyTermId bilPolicyTermId = new BilPolicyTermId(accountId, policyId,
                        maxPendingCancellation.getEffectiveDate());
                bilPolicyTerm.setBillPolicyTermId(bilPolicyTermId);
                bilPolicyTerm.setPlnExpDt(maxPendingCancellation.getExpirationDate());
                bilPolicyTerm.setMasterCompanyNbr(maxPendingCancellation.getMasterCompany());
                bilPolicyTerm.setBillStatePvnCd(maxPendingCancellation.getRiskState());
                bilPolicyTerm.setBptIssueSysId(maxPendingCancellation.getIssueSystemId());
                bilPolicyTerm.setBillPolStatusCd(maxPendingCancellation.getPolicyStatus());
                bilPolicyTerm.setBptStatusEffDt(accountProcessingAndRecovery.getRecoveryDate());
                bilPolicyTerm.setLobCd(maxPendingCancellation.getLineOfBusiness());
                bilPolicyTerm.setBillSusFuReaCd(BLANK_STRING);
                cancellation.setPcnmMaxIndicator(pcnmMaxIndicator);
                if (accountProcessingAndRecovery.getDriverIndicator() == CHAR_O) {
                    if (maxPendingCancellation.getPendingCancelFlatCnc() == CHAR_Y) {
                        cancellation.setOriginalPcnReason("RPC");
                    } else if (maxPendingCancellation.getPendingCancelFlatCnc() == CHAR_N) {
                        cancellation.setOriginalPcnReason("PPC");
                    }
                }
                cancellation.setPcnmApplied(true);
                char policyStatus = cancellationService.pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan,
                        maxPendingCancellation.getStandredCancellationDate(), polTermTypeCode, splitIndicator,
                        bilSupportPlan.getBspPndCncNbr(), cancellation, pedAcyMap, pcnnDataMap, termDataMap,
                        accountProcessingAndRecovery);

                if (splitIndicator == ACCT_IS_SPLIT) {
                    bilPolicyTerm.setBptPcnCausalInd(CHAR_Y);
                    cancellationService.callSplitBillingActivity(accountId, NoncausalType.PCN_REQUEST.toString(),
                            cancellation.isPolicyPedValued(), policyId, maxPendingCancellation.getEffectiveDate());
                }

                bilPolicyTerm.setBillPolStatusCd(policyStatus);
                bilPolicyTerm.setBptWroPrcInd(writeOffProcessInd);

                updateBilPolicyTerm(bilPolicyTerm);
            }
        }
    }

    private void updateBilPolicyTerm(BilPolicyTerm bilPolicyTerm) {
        BilPolicyTerm bilPolicyTermExists = bilPolicyTermRepository.findById(bilPolicyTerm.getBillPolicyTermId())
                .orElse(null);
        if (bilPolicyTermExists != null) {
            bilPolicyTermExists.setBillPolStatusCd(bilPolicyTerm.getBillPolStatusCd());
            bilPolicyTermExists.setBptWroPrcInd(bilPolicyTerm.getBptWroPrcInd());
            if (bilPolicyTerm.getBptStatusEffDt() != null) {
                bilPolicyTermExists.setBptStatusEffDt(bilPolicyTerm.getBptStatusEffDt());
            }
            if (bilPolicyTerm.getBillSusFuReaCd() != null) {
                bilPolicyTermExists.setBillSusFuReaCd(bilPolicyTerm.getBillSusFuReaCd());
            }
            bilPolicyTermExists.setBptPcnCausalInd(bilPolicyTerm.getBptPcnCausalInd());
            bilPolicyTermRepository.saveAndFlush(bilPolicyTermExists);
        }
    }

    private char checkPendingCancelStatus(String accountId, String policyId, ZonedDateTime effectiveDate,
            ZonedDateTime expirationDate) {
        char pcnStatus = BLANK_CHAR;
        BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository.getPendingCancelTerm(accountId, policyId, effectiveDate,
                expirationDate);
        if (bilPolicyTerm != null) {
            pcnStatus = bilPolicyTerm.getBillPolStatusCd();
        }
        return pcnStatus;
    }

    private void regularPcnProcess(BilAccount bilAccount, List<MaxPendingCancellation> maxPendingCancellationList,
            Map<String, Object> pedAcyMap, BilBillPlan bilBillPlan, Map<String, Object> termDataMap,
            AccountProcessingAndRecovery accountProcessingAndRecovery, Cancellation cancellation,
            Map<String, Object> pcnnDataMap, BilSupportPlan bilSupportPlan, char writeOffProcessInd) {

        for (MaxPendingCancellation maxPendingCancellation : maxPendingCancellationList) {
            String policyId = maxPendingCancellation.getPolicyId();
            if (policyId == null || policyId.trim().isEmpty()) {
                continue;
            }

            char splitIndicator = Boolean.TRUE.equals(maxPendingCancellation.isSplitPolicy()) ? CHAR_Y : BLANK_CHAR;
            char polTermTypeCode = cancellationService.getPolTermTypeCode(bilAccount.getAccountId(), policyId,
                    maxPendingCancellation.getEffectiveDate());

            if (maxPendingCancellation.isQualifyPcn() && !maxPendingCancellation.isNoPcnCpe()
                    && !maxPendingCancellation.isNoPcnCpn()) {
                pedAcyMap.put(PedAcyType.POLICYID.getValue(), policyId);
                pedAcyMap.put(PedAcyType.EQUITYIND.getValue(), CHAR_N);
                if (maxPendingCancellation.getCancellationIndicator() == CHAR_E) {
                    pedAcyMap.put(PedAcyType.EQUITYIND.getValue(), CHAR_Y);
                }
                pedAcyMap.put(PedAcyType.POLEFFDATE.getValue(), maxPendingCancellation.getEffectiveDate());
                pedAcyMap.put(PedAcyType.POLEXPDATE.getValue(), maxPendingCancellation.getExpirationDate());
                pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), maxPendingCancellation.getEquityDate());

                BilPolicy bilPolicy = new BilPolicy();
                bilPolicy.setPolNbr(maxPendingCancellation.getPolicyNumber());
                bilPolicy.setPolSymbolCd(maxPendingCancellation.getPolicySymbol());
                BilPolicyTerm bilPolicyTerm = new BilPolicyTerm();
                BilPolicyTermId bilPolicyTermId = new BilPolicyTermId(bilAccount.getAccountId(), policyId,
                        maxPendingCancellation.getEffectiveDate());
                bilPolicyTerm.setBillPolicyTermId(bilPolicyTermId);
                bilPolicyTerm.setMasterCompanyNbr(maxPendingCancellation.getMasterCompany());
                bilPolicyTerm.setLobCd(maxPendingCancellation.getLineOfBusiness());
                bilPolicyTerm.setBillStatePvnCd(maxPendingCancellation.getRiskState());
                bilPolicyTerm.setBptIssueSysId(maxPendingCancellation.getIssueSystemId());
                bilPolicyTerm.setPlnExpDt(maxPendingCancellation.getExpirationDate());
                bilPolicyTerm.setBillPolStatusCd(maxPendingCancellation.getPolicyStatus());

                termDataMap.put(NsfPcnDataType.NSFPCNCANCEL.getValue(), maxPendingCancellation.isNsfPcnCancel());
                termDataMap.put(NsfPcnDataType.TERMNEVERBILLED.getValue(), maxPendingCancellation.isTermNeverBilled());

                char pcnFlatCncInd;
                char reinstCode;
                char addBasRnrInd;
                if (maxPendingCancellation.isTermNeverBilled() && maxPendingCancellation.isNsfPcnCancel()) {
                    termDataMap.put(NsfPcnDataType.BILISTDUEAMOUNT.getValue(),
                            maxPendingCancellation.getPendingCancellationAmount());
                    pcnFlatCncInd = maxPendingCancellation.getPendingCancelFlatCnc();
                    reinstCode = maxPendingCancellation.getReinstCode();
                    addBasRnrInd = maxPendingCancellation.getAddBasRenewalIndicator();
                } else if (maxPendingCancellation.isNsfPcnCancel()) {
                    termDataMap.put(NsfPcnDataType.NSFPCNAMOUNT.getValue(),
                            maxPendingCancellation.getPendingCancellationAmount());
                    pcnFlatCncInd = maxPendingCancellation.getPendingCancelFlatCnc();
                    reinstCode = maxPendingCancellation.getReinstCode();
                    addBasRnrInd = maxPendingCancellation.getAddBasRenewalIndicator();
                } else {
                    termDataMap.put(TermAcyType.TERMPASTAMOUNT.getValue(),
                            maxPendingCancellation.getPendingCancellationAmount());
                    pcnFlatCncInd = BLANK_CHAR;
                    reinstCode = BLANK_CHAR;
                    addBasRnrInd = BLANK_CHAR;
                }
                pcnnDataMap.put(PcnnType.PCNFLATCNCIND.getValue(), pcnFlatCncInd);
                pcnnDataMap.put(PcnnType.REINSTCODE.getValue(), reinstCode);
                pcnnDataMap.put(PcnnType.ADDBASRNRIND.getValue(), addBasRnrInd);
                cancellation.setPcnmApplied(false);

                char policyStatus = cancellationService.pcnRnpRequest(bilAccount, bilPolicyTerm, bilPolicy, bilBillPlan,
                        maxPendingCancellation.getStandredCancellationDate(), polTermTypeCode, splitIndicator,
                        bilSupportPlan.getBspPndCncNbr(), cancellation, pedAcyMap, pcnnDataMap, termDataMap,
                        accountProcessingAndRecovery);
                if (splitIndicator == ACCT_IS_SPLIT) {
                    bilPolicyTerm.setBptPcnCausalInd(CHAR_Y);
                    cancellationService.callSplitBillingActivity(bilAccount.getAccountId(),
                            NoncausalType.PCN_REQUEST.toString(), cancellation.isPolicyPedValued(), policyId,
                            maxPendingCancellation.getEffectiveDate());
                }

                bilPolicyTerm.setBillPolStatusCd(policyStatus);
                bilPolicyTerm.setBptWroPrcInd(writeOffProcessInd);
                bilPolicyTerm.setBptStatusEffDt(accountProcessingAndRecovery.getRecoveryDate());

                updateBilPolicyTerm(bilPolicyTerm);
            }
        }
    }

    private boolean deterIsEquityProcessing(String accountId) {
        boolean isEquityUser = false;
        List<Character> equityPolStatusList = new ArrayList<>(Arrays.asList(BilPolicyStatusCode.TRANSFERRED.getValue(),
                BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(),
                BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue()));
        List<BilPolicyTerm> bilPolicyTerms = bilPolicyTermRepository.getDownPaymentPolicies(accountId, CHAR_Y,
                equityPolStatusList);
        if (bilPolicyTerms != null && !bilPolicyTerms.isEmpty()) {
            for (BilPolicyTerm bilPolicyTerm : bilPolicyTerms) {
                BilBillPlan bilBillPlan = bilBillPlanRepository.findById(bilPolicyTerm.getBillPlanCd()).orElse(null);
                if (bilBillPlan != null && bilBillPlan.getBbpCancelInd() == CHAR_E) {
                    isEquityUser = true;
                    break;
                }
            }
        }
        return isEquityUser;
    }

    private MaxPendingCancellation loadPcnmData(char polStatusCode, boolean isNsfPcnCancel,
            char bisTermInvoiceIndicator, boolean isTermNeverBilled, ZonedDateTime bilIstDueDt,
            BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy, BilBillPlan bilBillPlan) {

        Double totIssuedAmount = DECIMAL_ZERO;
        Double totIssuedBalance = DECIMAL_ZERO;
        List<Double[]> pcnmSumAmounts = bilIstScheduleRepository.findPcnmSumAmounts(
                bilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                BusCodeTranslationType.BILLING_ITEM.getValue());
        if (pcnmSumAmounts != null && !pcnmSumAmounts.isEmpty()) {
            totIssuedAmount = pcnmSumAmounts.get(0)[0];
            totIssuedBalance = pcnmSumAmounts.get(0)[1];
        }
        if (totIssuedAmount == null && totIssuedBalance == null) {
            throw new DataNotFoundException("pcnm.amounts.not.found");
        }
        MaxPendingCancellation maxPendingCancellation = new MaxPendingCancellation();
        maxPendingCancellation.setPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId());
        maxPendingCancellation.setPolicyNumber(bilPolicy.getPolNbr());
        maxPendingCancellation.setPolicySymbol(bilPolicy.getPolSymbolCd());
        maxPendingCancellation.setEffectiveDate(bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
        maxPendingCancellation.setMasterCompany(bilPolicyTerm.getMasterCompanyNbr());
        maxPendingCancellation.setLineOfBusiness(bilPolicyTerm.getLobCd());
        maxPendingCancellation.setRiskState(bilPolicyTerm.getBillStatePvnCd());
        maxPendingCancellation.setIssueSystemId(bilPolicyTerm.getBptIssueSysId());
        maxPendingCancellation.setExpirationDate(bilPolicyTerm.getPlnExpDt());
        maxPendingCancellation.setPolicyStatus(polStatusCode);
        maxPendingCancellation.setTermBalance(totIssuedBalance);
        maxPendingCancellation.setTermPremimumAmount(totIssuedAmount);
        maxPendingCancellation.setCancellationIndicator(bilBillPlan.getBbpCancelInd());
        maxPendingCancellation.setPendingCancellationAmount(bilBillPlan.getBbpPndCncAmt());
        maxPendingCancellation.setPendingCancellationPercent(bilBillPlan.getBbpPndCncPct());
        maxPendingCancellation.setNsfPcnCancel(isNsfPcnCancel);
        maxPendingCancellation.setBisTermInvoice(bisTermInvoiceIndicator == CHAR_Y);
        maxPendingCancellation.setTermNeverBilled(isTermNeverBilled);
        if (isTermNeverBilled) {
            maxPendingCancellation.setBilIstDueDate(bilIstDueDt);
        }

        return maxPendingCancellation;
    }

    private void processCashActivity(BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, BilBillPlan bilBillPlan,
            AccountReviewDetails accountReviewDetails, AccountProcessingAndRecovery accountProcessingAndRecovery,
            PolicyReview policyReview) {

        boolean polLvlCnDisb = policyReview.isPolLvlCnDisb();
        boolean polLvlCrDisb;
        BilActRules bilActRules = accountReviewDetails.getBilActRules();
        BilSupportPlan bilSupportPlan = accountReviewDetails.getBilSupportPlan();
        ZonedDateTime nextDisbDt = policyReview.getNextDisbursmentDate();
        ZonedDateTime nextAcwDt = policyReview.getNextAcwDt();

        if (bilActRules.getBruDsbRctDtInd() != CHAR_Y
                && isCashAcyCall(accountProcessingAndRecovery.getCashAcyCallIndicator())
                && (accountProcessingAndRecovery.getCashStatusCd() == CHAR_A
                        || bilAccount.getCashStatusCd() != AccountStatus.DISBURSEMENT.toChar())
                && policyReview.isCashInSuspense()) {
            Object[] nextDisbDates = creditdisburse(bilAccount, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm, bilSupportPlan, bilBillPlan, nextDisbDt, nextAcwDt);
            nextDisbDt = (ZonedDateTime) nextDisbDates[0];
            nextAcwDt = (ZonedDateTime) nextDisbDates[1];
            polLvlCrDisb = (boolean) nextDisbDates[2];

            if (bilPolicyTerm.getBptAgreementInd() != CHAR_Y) {
                Object[] cancellationDisbursement = cancelationdisburse(bilAccount,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilPolicyTerm, bilSupportPlan, bilBillPlan,
                        nextDisbDt, nextAcwDt);
                nextDisbDt = (ZonedDateTime) cancellationDisbursement[0];
                nextAcwDt = (ZonedDateTime) cancellationDisbursement[1];
                polLvlCnDisb = (boolean) cancellationDisbursement[2];
            }

            policyReview.setNextDisbursmentDate(nextDisbDt);
            policyReview.setNextAcwDt(nextAcwDt);
            policyReview.setPolLvlCrDisb(polLvlCrDisb);
            policyReview.setPolLvlCnDisb(polLvlCnDisb);
        }
    }

    private Object[] termData(BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, BilBillPlan bilBillPlan,
            char invoicingIndicator, char eftCollectionIndicator, short pndCncNumber, Map<String, Object> termDataMap,
            Map<String, Object> pedAcyMap, AccountProcessingAndRecovery accountProcessingAndRecovery) {

        String policyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId();
        String accountId = bilAccount.getAccountId();
        Double termBalanceAmount = DECIMAL_ZERO;
        char bisTermInvoiceIndicator = (char) termDataMap.get(TermAcyType.TERMINVOICEINDICATOR.getValue());
        char useCrgDateIndicator = BLANK_CHAR;

        List<Object[]> bisObject = bilIstScheduleRepository.fetchTermBalanceInvoiceCd(accountId, policyId,
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
        if (bisObject != null && !bisObject.isEmpty()) {
            termBalanceAmount = bisObject.get(0)[0] == null ? DECIMAL_ZERO : (Double) bisObject.get(0)[0];
            bisTermInvoiceIndicator = bisObject.get(0)[1] == null ? BLANK_CHAR : (char) bisObject.get(0)[1];
        }

        if (bilPolicyTerm.getPlnExpDt().compareTo(accountProcessingAndRecovery.getRecoveryDate()) <= 0
                && !isRescission(bilPolicyTerm.getBillPolStatusCd())) {
            return new Object[] { termBalanceAmount, DateRoutine.dateTimeAsYYYYMMDD(SHORT_ZERO), useCrgDateIndicator };
        }

        calculateTermPremiumAmount(accountId, policyId, bilPolicyTerm, termDataMap, accountProcessingAndRecovery.getRecoveryDate());
        ZonedDateTime lastInvoiceAdjDueDate = calculateInvoiceAdjDueDate(accountId, policyId, bisTermInvoiceIndicator,
                bilPolicyTerm, accountProcessingAndRecovery.getRecoveryDate());

        boolean isNsfPcnCancel = (boolean) termDataMap.get(NsfPcnDataType.NSFPCNCANCEL.getValue());
        boolean isFutureEquity = (boolean) termDataMap.get(NsfPcnDataType.FUTUREEQUITY.getValue());
        boolean isTermNeverBilled = (boolean) termDataMap.get(NsfPcnDataType.TERMNEVERBILLED.getValue());
        ZonedDateTime nrvNextAcyDate = (ZonedDateTime) termDataMap.get(NsfPcnDataType.NRVNEXTACYDATE.getValue());

        if (accountProcessingAndRecovery.isNsfPcn()) {
            nrvNextAcyDate = deterPolNrv(accountId, policyId, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
            if (nrvNextAcyDate == null) {
                isNsfPcnCancel = false;
                nrvNextAcyDate = DateRoutine.defaultDateTime();
            } else {
                isNsfPcnCancel = true;
            }
        }
        if (accountProcessingAndRecovery.getDriverIndicator() == DRIVER_ACTIVE && invoicingIndicator != INVOICING) {
            isFutureEquity = deterFutureEquity(accountId, accountProcessingAndRecovery.getRecoveryDate());
            if (isFutureEquity) {
                isNsfPcnCancel = true;
                nrvNextAcyDate = accountProcessingAndRecovery.getRecoveryDate();
            }
        }

        if (isNsfPcnCancel) {
            Double nsfPcnAmount = (Double) termDataMap.get(NsfPcnDataType.NSFPCNAMOUNT.getValue());
            Double nsfPcnPercent = (Double) termDataMap.get(NsfPcnDataType.NSFPCNPERCENT.getValue());

            if (isCurrentInvoicedTerm(bisTermInvoiceIndicator)) {

                isTermNeverBilled = checkIsTermNeverBilled(bisTermInvoiceIndicator, bilPolicyTerm);
                if (!isTermNeverBilled) {
                    Object[] calcNsfPcnObject = calcNsfPcnAmount(accountId, bilPolicyTerm, nrvNextAcyDate,
                            isNsfPcnCancel);
                    isNsfPcnCancel = (boolean) calcNsfPcnObject[0];
                    nsfPcnAmount = (Double) calcNsfPcnObject[1];
                    nsfPcnPercent = (Double) calcNsfPcnObject[2];
                }
            } else {
                isTermNeverBilled = true;
            }

            isNsfPcnCancel = deterNsfPcnEquity(accountId, bilBillPlan.getBbpCancelInd(),
                    accountProcessingAndRecovery.getRecoveryDate(), bilPolicyTerm, isNsfPcnCancel, pedAcyMap);

            if (isTermNeverBilled) {
                Object[] nsfPcnEquityObject = calculateNsfPcnEquityAmount(accountId, bilPolicyTerm, pedAcyMap);
                nsfPcnAmount = (Double) nsfPcnEquityObject[0];
                nsfPcnPercent = (Double) nsfPcnEquityObject[1];
                Double bilIstDueAmount = (Double) nsfPcnEquityObject[2];
                ZonedDateTime bilIstDueDate = (ZonedDateTime) nsfPcnEquityObject[3];
                termDataMap.put(NsfPcnDataType.BILISTDUEAMOUNT.getValue(), bilIstDueAmount);
                termDataMap.put(NsfPcnDataType.BILISTDUEDATE.getValue(), bilIstDueDate);
            }

            termDataMap.put(NsfPcnDataType.NSFPCNAMOUNT.getValue(), nsfPcnAmount);
            termDataMap.put(NsfPcnDataType.NSFPCNPERCENT.getValue(), nsfPcnPercent);
        }

        termDataMap.put(NsfPcnDataType.NSFPCNCANCEL.getValue(), isNsfPcnCancel);
        termDataMap.put(NsfPcnDataType.FUTUREEQUITY.getValue(), isFutureEquity);
        termDataMap.put(NsfPcnDataType.TERMNEVERBILLED.getValue(), isTermNeverBilled);
        termDataMap.put(NsfPcnDataType.NRVNEXTACYDATE.getValue(), nrvNextAcyDate);
        termDataMap.put(TermAcyType.TERMINVOICEINDICATOR.getValue(), bisTermInvoiceIndicator);

        if (isCurrentInvoicedTerm(bisTermInvoiceIndicator)) {
            ZonedDateTime termPastDate = (ZonedDateTime) termDataMap.get(TermAcyType.TERMPASTDATE.getValue());
            ZonedDateTime termPcnDate = (ZonedDateTime) termDataMap.get(TermAcyType.TERMPCNDATE.getValue());
            Double termPastAmount = (Double) termDataMap.get(TermAcyType.TERMPASTAMOUNT.getValue());
            Double termPastPercent = (Double) termDataMap.get(TermAcyType.TERMPASTPERCENT.getValue());
            Double termPremiumAmount = (Double) termDataMap.get(TermAcyType.TERMPREMIUMAMOUNT.getValue());
            label1: {
                ZonedDateTime fromDate = null;
                Object[] termPastDataObject = getTermPastData(accountId, policyId, termPremiumAmount, bilPolicyTerm,
                        bilAccount, accountProcessingAndRecovery.getRecoveryDate(),
                        accountProcessingAndRecovery.getDriverIndicator());
                Boolean isInvoiceFound = (Boolean) termPastDataObject[0];
                termPastDate = (ZonedDateTime) termPastDataObject[1];
                termPremiumAmount = (Double) termPastDataObject[2];
                useCrgDateIndicator = (char) termPastDataObject[3];
                if (isInvoiceFound != null && !isInvoiceFound) {
                    break label1;
                }
                fromDate = termPastDate;
                Character billDatesIndicator = bilDatesRepository.getMaxBillDatesInd(accountId, getInvoiceCode(),
                        termPastDate, CHAR_X);
                if (billDatesIndicator == null) {
                    billDatesIndicator = BLANK_CHAR;
                }
                termDataMap.put(TermAcyType.BILLDATESINDICATOR.getValue(), billDatesIndicator);

                if (eftCollectionIndicator == CHAR_Y) {
                    Object[] eftTermPastDataObject = getEftTermPastData(accountId, policyId, isInvoiceFound,
                            bilPolicyTerm, accountProcessingAndRecovery.getRecoveryDate());
                    isInvoiceFound = (boolean) eftTermPastDataObject[0];
                    ZonedDateTime eftTermPastDate = (ZonedDateTime) eftTermPastDataObject[1];
                    if (Boolean.FALSE.equals(isInvoiceFound)) {
                        termPcnDate = DateRoutine.defaultDateTime();
                        termPastAmount = DECIMAL_ZERO;
                        termPastPercent = DECIMAL_ZERO;
                        break label1;
                    }
                    fromDate = eftTermPastDate;
                }

                termPcnDate = DateRoutine.getAdjustedDate(fromDate, false, pndCncNumber, null);
                char polStatusCode = bilPolicyTerm.getBillPolStatusCd();

                if (termPcnDate.compareTo(accountProcessingAndRecovery.getRecoveryDate()) <= 0 && (isCancelledPolicy(polStatusCode)
                        || polStatusCode == BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue()
                        || polStatusCode == BilPolicyStatusCode.SUSPEND_FOLLOWUP.getValue()
                        || polStatusCode == BilPolicyStatusCode.SUSPENDBILLING_FOLLOWUP.getValue())) {
                    termPastDate = DateRoutine.defaultDateTime();
                    termPcnDate = DateRoutine.defaultDateTime();
                    termPastAmount = DECIMAL_ZERO;
                    termPremiumAmount = DECIMAL_ZERO;
                    termPastPercent = DECIMAL_ZERO;
                } else {

                    termPastAmount = calculatetermPastAmount(accountId, policyId, termPastDate, bilPolicyTerm,
                            accountProcessingAndRecovery);

                    if (termPremiumAmount == DECIMAL_ZERO) {
                        termPastPercent = DECIMAL_ZERO;
                    } else {
                        termPastPercent = (double) Math.round(termPastAmount / termPremiumAmount * 100.00);
                    }
                }
            }

            termDataMap.put(TermAcyType.TERMPCNDATE.getValue(), termPcnDate);
            termDataMap.put(TermAcyType.TERMPASTAMOUNT.getValue(), termPastAmount);
            termDataMap.put(TermAcyType.TERMPASTPERCENT.getValue(), termPastPercent);
            termDataMap.put(TermAcyType.TERMPASTDATE.getValue(), termPastDate);
            termDataMap.put(TermAcyType.TERMPREMIUMAMOUNT.getValue(), termPremiumAmount);
        } else {
            termDataMap.put(TermAcyType.TERMPASTDATE.getValue(), DateRoutine.defaultDateTime());
            termDataMap.put(TermAcyType.TERMPCNDATE.getValue(), DateRoutine.defaultDateTime());
            termDataMap.put(TermAcyType.TERMPREMIUMAMOUNT.getValue(), DECIMAL_ZERO);
            termDataMap.put(TermAcyType.TERMPASTAMOUNT.getValue(), DECIMAL_ZERO);
            termDataMap.put(TermAcyType.TERMPASTPERCENT.getValue(), DECIMAL_ZERO);
        }

        return new Object[] { termBalanceAmount, lastInvoiceAdjDueDate, useCrgDateIndicator };
    }

    private Double calculatetermPastAmount(String accountId, String policyId, ZonedDateTime termPastDate,
            BilPolicyTerm bilPolicyTerm, AccountProcessingAndRecovery accountProcessingAndRecovery) {
        Double termPastAmount;
        if (accountProcessingAndRecovery.getDriverIndicator() == CHAR_O) {
            termPastAmount = bilIstScheduleRepository.findTermLastAmtByAdjDateWithBillInvoiceCd(accountId, policyId,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), termPastDate,
                    BusCodeTranslationType.BILLING_ITEM.getValue(), getInvoiceCode());
        } else {
            termPastAmount = bilIstScheduleRepository.findTermLastAmtByAdjDate(accountId, policyId,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), termPastDate,
                    BusCodeTranslationType.BILLING_ITEM.getValue());
        }
        if (termPastAmount == null) {
            logger.debug("Get Term Past Due Amount failed by accountId: {}", accountId);
            throw new DataNotFoundException(PCN_IST_DUE_NOT_FOUND,
                    new Object[] { "Term Past Due Amount", accountId, policyId,
                            DateRoutine.dateTimeAsMMDDYYYYAsString(
                                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()),
                            DateRoutine.dateTimeAsMMDDYYYYAsString(termPastDate),
                            BusCodeTranslationType.BILLING_ITEM.getValue() });
        }

        return termPastAmount;

    }

    private Object[] getEftTermPastData(String accountId, String policyId, Boolean isInvoiceFound,
            BilPolicyTerm bilPolicyTerm, ZonedDateTime recoveryDate) {

        ZonedDateTime eftTermPastDate = bilIstScheduleRepository.getMinTermPastAdjDate(accountId,
                Arrays.asList(InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue(),
                        InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()),
                policyId, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), recoveryDate, DECIMAL_ZERO,
                BusCodeTranslationType.BILLING_ITEM.getValue());
        if (eftTermPastDate == null) {
            Object[] invoiceCheckObject = checkEftInvoiceToday(accountId, policyId,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), recoveryDate);
            isInvoiceFound = (Boolean) invoiceCheckObject[0];
            eftTermPastDate = (ZonedDateTime) invoiceCheckObject[1];
        }
        return new Object[] { isInvoiceFound, eftTermPastDate };
    }

    private Object[] getTermPastData(String accountId, String policyId, Double termPremiumAmount,
            BilPolicyTerm bilPolicyTerm, BilAccount bilAccount, ZonedDateTime recoveryDate, char driverIndicator) {

        ZonedDateTime termPastDate = null;
        Boolean isInvoiceFound = null;
        char useCrgDateIndicator = CHAR_N;

        if (driverIndicator == CHAR_O) {
            termPastDate = bilIstScheduleRepository.getMaxTermPastAdjDateWithouBillAdjDueDt(accountId, getInvoiceCode(),
                    policyId, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), DECIMAL_ZERO,
                    BusCodeTranslationType.BILLING_ITEM.getValue());
        } else {
            termPastDate = bilIstScheduleRepository.getMaxTermPastAdjDate(accountId, getInvoiceCode(), policyId,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), recoveryDate, DECIMAL_ZERO,
                    BusCodeTranslationType.BILLING_ITEM.getValue());
        }
        if (termPastDate == null) {
            Object[] invoiceCheckObject = checkInvoiceToday(accountId, policyId,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), recoveryDate);
            termPastDate = (ZonedDateTime) invoiceCheckObject[1] == null ? DateRoutine.defaultDateTime()
                    : (ZonedDateTime) invoiceCheckObject[1];
            if (termPastDate == null) {
                termPastDate = DateRoutine.defaultDateTime();
                isInvoiceFound = false;
                termPremiumAmount = DECIMAL_ZERO;
            } else {
                isInvoiceFound = (boolean) invoiceCheckObject[0];
                termPremiumAmount = (Double) invoiceCheckObject[2];
            }

            if ((Boolean.TRUE.equals(isInvoiceFound)) && (bilAccount.getBillTypeCd()
                    .equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                    || bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.ACCOUNT_BILLCD.getValue()))) {
                BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("ECNC", bilAccount.getBillTypeCd(),
                        bilAccount.getBillClassCd(), BLANK_STRING);
                if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                    ZonedDateTime maxCrgAdjDate = bilCrgAmountsRepository.getMaxbilAdjDueDt(accountId,
                            getInvoiceCode());
                    if (maxCrgAdjDate != null && maxCrgAdjDate.compareTo(DateRoutine.defaultDateTime()) != 0
                            && maxCrgAdjDate.compareTo(termPastDate) > 0) {
                        termPastDate = maxCrgAdjDate;
                        useCrgDateIndicator = CHAR_Y;
                    }
                }
            }
        }

        return new Object[] { isInvoiceFound, termPastDate, termPremiumAmount, useCrgDateIndicator };
    }

    private ZonedDateTime calculateInvoiceAdjDueDate(String accountId, String policyId, char bisTermInvoiceIndicator,
            BilPolicyTerm bilPolicyTerm, ZonedDateTime recoveryDate) {
        ZonedDateTime lastInvoiceAdjDueDate = DateRoutine.dateTimeAsYYYYMMDD(SHORT_ZERO);
        if (bisTermInvoiceIndicator == InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()
                || bisTermInvoiceIndicator == InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE
                        .getValue()) {
            lastInvoiceAdjDueDate = bilIstScheduleRepository.getLastInvoiceAdjDate(accountId,
                    new ArrayList<>(Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue())),
                    policyId, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), recoveryDate,
                    BusCodeTranslationType.BILLING_ITEM.getValue());
            if (lastInvoiceAdjDueDate == null) {
                lastInvoiceAdjDueDate = DateRoutine.dateTimeAsYYYYMMDD(SHORT_ZERO);
            }
        }

        return lastInvoiceAdjDueDate;
    }

    private void calculateTermPremiumAmount(String accountId, String policyId, BilPolicyTerm bilPolicyTerm,
            Map<String, Object> termDataMap, ZonedDateTime recoveryDate) {

        Double termPremiumAmount = bilIstScheduleRepository.findTermBalanceByAdjDate(accountId, policyId,
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), recoveryDate,
                BusCodeTranslationType.BILLING_ITEM.getValue());
        if (termPremiumAmount == null) {
            termPremiumAmount = DECIMAL_ZERO;
        }

        termDataMap.put(TermAcyType.TERMPREMIUMAMOUNT.getValue(), termPremiumAmount);
    }

    private Object[] calculateNsfPcnEquityAmount(String accountId, BilPolicyTerm bilPolicyTerm,
            Map<String, Object> pedAcyMap) {

        Double bilIstDueAmount = DECIMAL_ZERO;
        Double nsfTermPremiumAmount = DECIMAL_ZERO;
        ZonedDateTime pedNsfAcyDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.NSFACYDATE.getValue());

        ZonedDateTime maxBilIstDueDate = bilIstScheduleRepository.getMaxBilIstDueDate(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), pedNsfAcyDate,
                BusCodeTranslationType.BILLING_ITEM.getValue());
        if (maxBilIstDueDate == null) {
            maxBilIstDueDate = bilIstScheduleRepository.getMinBilIstDueDate(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    BusCodeTranslationType.BILLING_ITEM.getValue());
            if (maxBilIstDueDate == null) {
                logger.debug("Get Max Ist Due Date failed by accountId: {}", accountId);
                throw new DataNotFoundException(PCN_IST_DUE_NOT_FOUND,
                        new Object[] { "max Ist due date", accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                DateRoutine.dateTimeAsMMDDYYYYAsString(
                                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()),
                                DateRoutine.dateTimeAsMMDDYYYYAsString(pedNsfAcyDate),
                                BusCodeTranslationType.BILLING_ITEM.getValue() });
            }
        }

        List<Object[]> nsfSumAmounts = bilIstScheduleRepository.findNsfSumAmountsByDueDate(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), maxBilIstDueDate,
                BusCodeTranslationType.BILLING_ITEM.getValue());
        if (nsfSumAmounts != null && !nsfSumAmounts.isEmpty()) {
            nsfTermPremiumAmount = (Double) nsfSumAmounts.get(0)[0];
            bilIstDueAmount = (Double) nsfSumAmounts.get(0)[1];
        }

        if (bilIstDueAmount == null || bilIstDueAmount == DECIMAL_ZERO) {
            logger.debug("Get Ist Due Amount failed by accountId: {}", accountId);
            throw new DataNotFoundException(PCN_IST_DUE_NOT_FOUND,
                    new Object[] { "Ist due amount", accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                            DateRoutine.dateTimeAsMMDDYYYYAsString(
                                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()),
                            DateRoutine.dateTimeAsMMDDYYYYAsString(pedNsfAcyDate),
                            BusCodeTranslationType.BILLING_ITEM.getValue() });
        }

        if (nsfTermPremiumAmount == null) {
            nsfTermPremiumAmount = DECIMAL_ZERO;
        }

        double nsfPcnAmount = bilIstDueAmount;
        double nsfPcnPercent = DECIMAL_ZERO;
        if (nsfTermPremiumAmount != DECIMAL_ZERO) {
            nsfPcnPercent = Math.round(nsfPcnAmount / nsfTermPremiumAmount * 100.00);
        }
        return new Object[] { nsfPcnAmount, nsfPcnPercent, bilIstDueAmount, maxBilIstDueDate };
    }

    private boolean checkIsTermNeverBilled(char bisTermInvoiceIndicator, BilPolicyTerm bilPolicyTerm) {

        if (isFutureInvoicedTerm(bisTermInvoiceIndicator)) {
            return true;
        } else if (isCurrentInvoicedTerm(bisTermInvoiceIndicator)) {
            return false;
        } else {
            List<Character> bisCheckList = bilIstScheduleRepository.fetchInvoiceCode(
                    bilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), getInvoiceCode());
            if (bisCheckList == null || bisCheckList.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    private Object[] checkInvoiceToday(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime recoveryDate) {
        boolean isInvoiceFound = false;
        Double termPremiumAmount = DECIMAL_ZERO;
        ZonedDateTime termPastDate = bilIstScheduleRepository.getLastTermPastInvDate(accountId, getInvoiceCode(),
                policyId, polEffectiveDate, recoveryDate, BusCodeTranslationType.BILLING_ITEM.getValue());
        if (termPastDate != null) {
            isInvoiceFound = true;
            termPremiumAmount = bilIstScheduleRepository.findTermPremiumAmtByInvDate(accountId, policyId,
                    polEffectiveDate, recoveryDate, BusCodeTranslationType.BILLING_ITEM.getValue());
            if (termPremiumAmount == null) {
                termPremiumAmount = DECIMAL_ZERO;
            }
        }

        return new Object[] { isInvoiceFound, termPastDate, termPremiumAmount };
    }

    private Object[] checkEftInvoiceToday(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime recoveryDate) {
        Boolean isInvoiceFound = false;
        ZonedDateTime termPastDate = bilIstScheduleRepository.getMinTermPastInvDate(accountId,
                Arrays.asList(InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue(),
                        InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()),
                policyId, polEffectiveDate, recoveryDate, BusCodeTranslationType.BILLING_ITEM.getValue());
        if (termPastDate != null) {
            isInvoiceFound = true;
        }

        return new Object[] { isInvoiceFound, termPastDate };
    }

    private boolean deterFutureEquity(String accountId, ZonedDateTime recoveryDate) {

        List<BilActInquiry> bilActInquiries = bilActInquiryRepository.getSuspendDisbRow(accountId,
                new ArrayList<>(Arrays.asList("NRV")), recoveryDate);
        return bilActInquiries != null && !bilActInquiries.isEmpty();
    }

    private boolean deterNsfPcnEquity(String accountId, char bbpCancelIndicator, ZonedDateTime recoveryDate,
            BilPolicyTerm bilPolicyTerm, boolean isNsfPcnCancel, Map<String, Object> pedAcyMap) {

        ZonedDateTime pedNsfAcyDate = bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt();
        char pedEquityIndicator = (char) pedAcyMap.get(PedAcyType.EQUITYIND.getValue());

        if (bbpCancelIndicator == CHAR_E) {
            List<BilPolActivity> bilPolActivities = bilPolActivityRepository.fetchBilPolActivityTriger(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), POLICY_EQUITY_DATE);
            if (bilPolActivities != null && !bilPolActivities.isEmpty()) {
                pedNsfAcyDate = bilPolActivities.get(0).getBilNxtAcyDt();
                if (pedNsfAcyDate.compareTo(DateRoutine.defaultDateTime()) == 0
                        || bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt().compareTo(recoveryDate) > 0
                        || pedEquityIndicator == BLANK_CHAR) {
                    nsfEquityCanc(accountId, bilPolicyTerm, bbpCancelIndicator, recoveryDate, pedAcyMap);
                    ZonedDateTime pedNextAcyDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.NEXTACYDATE.getValue());
                    pedNsfAcyDate = pedNextAcyDate;
                }

            } else {
                isNsfPcnCancel = false;
            }
        }
        pedAcyMap.put(PedAcyType.NSFACYDATE.getValue(), pedNsfAcyDate);

        return isNsfPcnCancel;
    }

    private void nsfEquityCanc(String accountId, BilPolicyTerm bilPolicyTerm, char bbpCancelIndicator,
            ZonedDateTime recoveryDate, Map<String, Object> pedAcyMap) {

        ZonedDateTime pedPolEffDate = bilPolicyTermRepository.getMinPolicyEffDateByAgreement(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), recoveryDate, CHAR_N);
        if (pedPolEffDate == null) {
            pedPolEffDate = bilPolicyTermRepository.getMaxPolicyEffDateByAgreement(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(), recoveryDate, recoveryDate, CHAR_N);
        }

        if (pedPolEffDate != null) {
            pedAcyMap.put(PedAcyType.POLEFFDATE.getValue(), pedPolEffDate);

            if (bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt().isEqual(pedPolEffDate)) {

                pedAcyMap.put(PedAcyType.POLEXPDATE.getValue(), bilPolicyTerm.getPlnExpDt());
                pedAcyMap.put(PedAcyType.ISSUESYSID.getValue(), bilPolicyTerm.getBptIssueSysId());

                if (bilPolicyTerm.getBptAgreementInd() != CHAR_Y) {
                    checkPolicyNsfEquityTriggers(bilPolicyTerm, bilPolicyTerm, bbpCancelIndicator, pedAcyMap);
                }
            } else {
                BilPolicyTerm pedBilPolicyTerm = bilPolicyTermRepository.findById(new BilPolicyTermId(accountId,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(), pedPolEffDate)).orElse(null);
                if (pedBilPolicyTerm == null) {
                    logger.debug("Get max policy expiration date failed by accountId: {}", accountId);
                    throw new DataNotFoundException("ped.effective.date.not.found",
                            new Object[] { accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                                    DateRoutine.dateTimeAsMMDDYYYYAsString(pedPolEffDate) });
                }

                pedAcyMap.put(PedAcyType.POLEXPDATE.getValue(), pedBilPolicyTerm.getPlnExpDt());
                pedAcyMap.put(PedAcyType.ISSUESYSID.getValue(), pedBilPolicyTerm.getBptIssueSysId());

                if (pedBilPolicyTerm.getBptAgreementInd() != CHAR_Y) {
                    checkPolicyNsfEquityTriggers(pedBilPolicyTerm, bilPolicyTerm, bbpCancelIndicator, pedAcyMap);
                }

            }
        }
    }

    private Object[] calcNsfPcnAmount(String accountId, BilPolicyTerm bilPolicyTerm, ZonedDateTime nrvNextAcyDate,
            boolean isNsfPcnCancel) {

        double nsfPcnPercent = DECIMAL_ZERO;

        Double nsfTermPremiumAmount = calculateNsfTermPremiumAmount(accountId, nrvNextAcyDate, bilPolicyTerm);
        Double nsfPcnAmount = calculateNsfPcnAmount(accountId, nrvNextAcyDate, bilPolicyTerm);
        if (nsfPcnAmount == null) {
            isNsfPcnCancel = false;
            nsfPcnAmount = DECIMAL_ZERO;
        }

        if (nsfTermPremiumAmount != DECIMAL_ZERO) {
            nsfPcnPercent = Math.round(nsfPcnAmount / nsfTermPremiumAmount * 100.00);
        }
        return new Object[] { isNsfPcnCancel, nsfPcnAmount, nsfPcnPercent };
    }

    private Double calculateNsfPcnAmount(String accountId, ZonedDateTime nrvNextAcyDate, BilPolicyTerm bilPolicyTerm) {
        Double nsfPcnAmount = bilIstScheduleRepository.findTermLastAmtByInvoiceDate(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), nrvNextAcyDate,
                BusCodeTranslationType.BILLING_ITEM.getValue());
        if (nsfPcnAmount == null) {
            ZonedDateTime nsfInvDate = bilIstScheduleRepository.getMinBilInvoiceDateByPayItem(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    BusCodeTranslationType.BILLING_ITEM.getValue());
            if (nsfInvDate == null) {
                return null;
            }

            if (nsfInvDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
                nsfPcnAmount = bilIstScheduleRepository.findTermLastAmtByInvoiceDate(accountId,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), nsfInvDate,
                        BusCodeTranslationType.BILLING_ITEM.getValue());
                if (nsfPcnAmount == null) {
                    return null;
                }
            }
        }

        return nsfPcnAmount;
    }

    private Double calculateNsfTermPremiumAmount(String accountId, ZonedDateTime nrvNextAcyDate,
            BilPolicyTerm bilPolicyTerm) {
        Double nsfTermPremiumAmount = bilIstScheduleRepository.findNsfTermPremiumAmtByInvDate(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), nrvNextAcyDate,
                BusCodeTranslationType.BILLING_ITEM.getValue());
        if (nsfTermPremiumAmount == null) {
            ZonedDateTime nsfInvDate = bilIstScheduleRepository.getMinBilInvoiceDateByPayItem(accountId,
                    bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    BusCodeTranslationType.BILLING_ITEM.getValue());
            if (nsfInvDate == null) {
                nsfInvDate = DateRoutine.defaultDateTime();
                nsfTermPremiumAmount = DECIMAL_ZERO;
            }

            if (nsfInvDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
                nsfTermPremiumAmount = bilIstScheduleRepository.findNsfTermPremiumAmtByInvDate(accountId,
                        bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), nsfInvDate,
                        BusCodeTranslationType.BILLING_ITEM.getValue());
                if (nsfTermPremiumAmount == null) {
                    nsfTermPremiumAmount = DECIMAL_ZERO;
                }
            }
        }
        return nsfTermPremiumAmount;
    }

    private ZonedDateTime deterPolNrv(String accountId, String policyId, ZonedDateTime polEffectiveDate) {

        BilPolActivity bilPolActivity = bilPolActivityRepository
                .findById(new BilPolActivityId(accountId, policyId, polEffectiveDate, "NRV")).orElse(null);
        if (bilPolActivity != null && bilPolActivity.getBilNxtAcyDt().compareTo(DateRoutine.defaultDateTime()) != 0) {
            return bilPolActivity.getBilNxtAcyDt();
        }

        return null;
    }

    private Map<String, Object> equityCanc(String accountId, String policyId, BilPolicyTerm bilPolicyTerm,
            BilBillPlan bilBillPlan, AccountProcessingAndRecovery accountProcessingAndRecovery) {

        List<Character> polStatusList = Arrays.asList(BilPolicyStatusCode.TRANSFERRED.getValue(),
                BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(),
                BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue());

        Map<String, Object> pedAcyMap = initializationPedAcyData();
        ZonedDateTime effTermsDate = bilPolicyTermRepository.getMaxPolicyEffDate(accountId, policyId,
                accountProcessingAndRecovery.getRecoveryDate(), accountProcessingAndRecovery.getRecoveryDate(), CHAR_N,
                polStatusList);
        if (effTermsDate == null) {
            effTermsDate = bilPolicyTermRepository.getMinPolicyEffDate(accountId, policyId,
                    accountProcessingAndRecovery.getRecoveryDate(), accountProcessingAndRecovery.getRecoveryDate(),
                    CHAR_N, polStatusList);
        }
        if (effTermsDate != null) {
            pedAcyMap.put(PedAcyType.POLEFFDATE.getValue(), effTermsDate);
            if (bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt().isEqual(effTermsDate)) {

                pedAcyMap.put(PedAcyType.POLEXPDATE.getValue(), bilPolicyTerm.getPlnExpDt());
                pedAcyMap.put(PedAcyType.ISSUESYSID.getValue(), bilPolicyTerm.getBptIssueSysId());

                if (bilPolicyTerm.getBptAgreementInd() != CHAR_Y) {
                    return checkPolicyEquityTriggers(bilPolicyTerm, bilPolicyTerm, bilBillPlan.getBbpCancelInd(),
                            accountProcessingAndRecovery.getDriverIndicator(),
                            accountProcessingAndRecovery.getCashAcyCallIndicator(), pedAcyMap);
                }

            } else {
                BilPolicyTerm pedBilPolicyTerm = bilPolicyTermRepository
                        .findById(new BilPolicyTermId(accountId, policyId, effTermsDate)).orElse(null);
                if (pedBilPolicyTerm == null) {
                    logger.debug("Get max Policy Expiration date failed by accountId: {}", accountId);
                    throw new DataNotFoundException("ped.effective.date.not.found",
                            new Object[] { accountId, policyId, DateRoutine.dateTimeAsMMDDYYYYAsString(effTermsDate) });
                }
                pedAcyMap.put(PedAcyType.POLEXPDATE.getValue(), pedBilPolicyTerm.getPlnExpDt());
                pedAcyMap.put(PedAcyType.ISSUESYSID.getValue(), pedBilPolicyTerm.getBptIssueSysId());

                if (pedBilPolicyTerm.getBptAgreementInd() != CHAR_Y) {
                    return checkPolicyEquityTriggers(pedBilPolicyTerm, bilPolicyTerm, bilBillPlan.getBbpCancelInd(),
                            accountProcessingAndRecovery.getDriverIndicator(),
                            accountProcessingAndRecovery.getCashAcyCallIndicator(), pedAcyMap);
                }
            }
        }

        return pedAcyMap;

    }

    private void checkPolicyNsfEquityTriggers(BilPolicyTerm pedBilPolicyTerm, BilPolicyTerm bilPolicyTerm,
            char cancelledIndicator, Map<String, Object> pedAcyMap) {

        char pedCancelIndicator = BLANK_CHAR;
        ZonedDateTime pedNextAcyDate = (ZonedDateTime) pedAcyMap.get(PedAcyType.NEXTACYDATE.getValue());
        char pedEquityIndicator = (char) pedAcyMap.get(PedAcyType.EQUITYIND.getValue());

        if (bilPolicyTerm.getBillPlanCd().equals(pedBilPolicyTerm.getBillPlanCd())) {
            pedCancelIndicator = cancelledIndicator;
        } else {
            BilBillPlan bilBillPlan = bilBillPlanRepository.findById(pedBilPolicyTerm.getBillPlanCd()).orElse(null);
            if (bilBillPlan != null) {
                pedCancelIndicator = bilBillPlan.getBbpCancelInd();
            }
        }

        if (pedCancelIndicator != CHAR_E) {
            List<BilPolActivity> bilPolActivities = bilPolActivityRepository.fetchBilPolActivityTriger(
                    pedBilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                    pedBilPolicyTerm.getBillPolicyTermId().getPolicyId(), POLICY_EQUITY_DATE);
            if (bilPolActivities != null && !bilPolActivities.isEmpty()) {
                bilPolActivityRepository.deleteAllInBatch(bilPolActivities);
            }
        } else {
            ZonedDateTime pedNextAcyDateHold = null;
            List<BilPolActivity> bilPolActivities = bilPolActivityRepository.fetchBilPolActivityTriger(
                    pedBilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                    pedBilPolicyTerm.getBillPolicyTermId().getPolicyId(), POLICY_EQUITY_DATE);
            if (bilPolActivities != null && !bilPolActivities.isEmpty()) {
                pedNextAcyDateHold = bilPolActivities.get(0).getBilNxtAcyDt();
            } else {
                updatePolActivity(pedBilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                        pedBilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        pedBilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), DateRoutine.defaultDateTime());
            }

            pedEquityIndicator = CHAR_Y;
            pedNextAcyDate = cancellationService.callCalculatePaidToDate(
                    pedBilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                    pedBilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    pedBilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), pedBilPolicyTerm.getPlnExpDt());

            if (pedNextAcyDateHold == null
                    || (pedNextAcyDateHold != null && pedNextAcyDateHold.compareTo(pedNextAcyDate) != 0)) {
                updatePolActivity(pedBilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                        pedBilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        pedBilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), pedNextAcyDate);
            }
        }

        pedAcyMap.put(PedAcyType.NEXTACYDATE.getValue(), pedNextAcyDate);
        pedAcyMap.put(PedAcyType.EQUITYIND.getValue(), pedEquityIndicator);

    }

    private Map<String, Object> checkPolicyEquityTriggers(BilPolicyTerm pedBilPolicyTerm, BilPolicyTerm bilPolicyTerm,
            char cancelledIndicator, char driverIndicator, char cashAcyCallIndicator, Map<String, Object> pedAcyMap) {

        char pedCancelIndicator = BLANK_CHAR;
        ZonedDateTime pedNextAcyDate = DateRoutine.defaultDateTime();
        if (bilPolicyTerm.getBillPlanCd().equals(pedBilPolicyTerm.getBillPlanCd())) {
            pedCancelIndicator = cancelledIndicator;
        } else {
            BilBillPlan bilBillPlan = bilBillPlanRepository.findById(pedBilPolicyTerm.getBillPlanCd()).orElse(null);
            if (bilBillPlan != null) {
                pedCancelIndicator = bilBillPlan.getBbpCancelInd();
            }
        }

        if (pedCancelIndicator != CHAR_E) {

            if (driverIndicator != DRIVER_ACTIVE) {
                List<BilPolActivity> bilPolActivities = bilPolActivityRepository.fetchBilPolActivityTriger(
                        pedBilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                        pedBilPolicyTerm.getBillPolicyTermId().getPolicyId(), POLICY_EQUITY_DATE);
                if (bilPolActivities != null && !bilPolActivities.isEmpty()) {
                    bilPolActivityRepository.deleteAllInBatch(bilPolActivities);
                }
            }
            return pedAcyMap;
        } else {
            List<BilPolActivity> pedBilPolActivities = bilPolActivityRepository.fetchBilPolActivityByDate(
                    pedBilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                    pedBilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                    pedBilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), POLICY_EQUITY_DATE);
            if (pedBilPolActivities != null && !pedBilPolActivities.isEmpty()) {
                bilPolActivityRepository.deleteAllInBatch(pedBilPolActivities);
            }

            if (driverIndicator != DRIVER_ACTIVE) {
                updatePolActivity(pedBilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                        pedBilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        pedBilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), DateRoutine.defaultDateTime());
            }

            if (driverIndicator != DRIVER_ACTIVE
                    || driverIndicator == DRIVER_ACTIVE && isCashAcyCall(cashAcyCallIndicator)) {
                pedNextAcyDate = cancellationService.callCalculatePaidToDate(
                        pedBilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                        pedBilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        pedBilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), pedBilPolicyTerm.getPlnExpDt());
            }

            ZonedDateTime pedNextAcyDateHold = null;
            BilPolActivity bilPolActivity = bilPolActivityRepository
                    .findById(new BilPolActivityId(pedBilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                            pedBilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                            pedBilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), POLICY_EQUITY_DATE))
                    .orElse(null);
            if (bilPolActivity != null) {
                pedNextAcyDateHold = bilPolActivity.getBilNxtAcyDt();
            }

            if (driverIndicator == DRIVER_ACTIVE) {
                if (bilPolActivity == null || bilPolActivity != null
                        && bilPolActivity.getBilNxtAcyDt().compareTo(DateRoutine.defaultDateTime()) != 0) {
                    pedNextAcyDate = pedNextAcyDateHold;
                }

                if (pedNextAcyDateHold == null
                        || pedNextAcyDateHold != null
                                && pedNextAcyDateHold.compareTo(DateRoutine.defaultDateTime()) == 0
                        || (pedNextAcyDateHold.isEqual(pedBilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt())
                                || pedNextAcyDateHold
                                        .isBefore(pedBilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt()))) {
                    pedNextAcyDate = cancellationService.callCalculatePaidToDate(
                            pedBilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                            pedBilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                            pedBilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), pedBilPolicyTerm.getPlnExpDt());
                }
            }

            if (pedNextAcyDateHold == null
                    || pedNextAcyDateHold != null && pedNextAcyDateHold.compareTo(pedNextAcyDate) != 0) {
                updatePolActivity(pedBilPolicyTerm.getBillPolicyTermId().getBilAccountId(),
                        pedBilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        pedBilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), pedNextAcyDate);
            }

            pedAcyMap.put(PedAcyType.NEXTACYDATE.getValue(), pedNextAcyDate);
            pedAcyMap.put(PedAcyType.EQUITYIND.getValue(), CHAR_Y);

            return pedAcyMap;
        }

    }

    private void updatePolActivity(String accountId, String pedPolicyId, ZonedDateTime pedPolEffDate,
            ZonedDateTime pedNextAcyDate) {

        BilPolActivity bilPolActivity = new BilPolActivity();
        BilPolActivityId bilPolActivityId = new BilPolActivityId(accountId, pedPolicyId, pedPolEffDate,
                POLICY_EQUITY_DATE);
        bilPolActivity.setBillPolActivityId(bilPolActivityId);
        bilPolActivity.setBilNxtAcyDt(pedNextAcyDate);
        bilPolActivityRepository.saveAndFlush(bilPolActivity);
    }

    private boolean isCheckTermData(char polStatusCode, ZonedDateTime planExpDate, ZonedDateTime statusEffDate,
            ZonedDateTime recoveryDate) {

        return (isPolicyNotCancelled(polStatusCode)
                || polStatusCode == BilPolicyStatusCode.SUSPEND_BILLING.getValue()
                        && planExpDate.compareTo(recoveryDate) >= 0
                || isCancelledPolicy(polStatusCode)
                || polStatusCode == BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue()
                        && statusEffDate.compareTo(recoveryDate) == 0
                || isRescission(polStatusCode));
    }

    private boolean isPolicyNotCancelled(char policyStatusCode) {
        return Arrays
                .asList(BilPolicyStatusCode.OPEN.getValue(), BilPolicyStatusCode.CLOSED.getValue(),
                        BilPolicyStatusCode.QUOTE.getValue())
                .stream().anyMatch(policyStatus -> policyStatus == policyStatusCode);
    }

    private boolean isCancelledPolicy(char policyStatusCode) {
        return Arrays
                .asList(BilPolicyStatusCode.CANCELLED.getValue(), BilPolicyStatusCode.FLAT_CANCELLATION.getValue(),
                        BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue())
                .stream().anyMatch(policyStatus -> policyStatus == policyStatusCode);
    }

    private boolean isRescission(char policyStatusCode) {
        return Arrays
                .asList(BilPolicyStatusCode.PENDING_CANCELLATION.getValue(),
                        BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue(),
                        BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue(),
                        BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue())
                .stream().anyMatch(policyStatus -> policyStatus == policyStatusCode);
    }

    private boolean isCancelForReopen(char policyStatusCode) {
        return Arrays
                .asList(BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                        BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(),
                        BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(),
                        BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue())
                .stream().anyMatch(policyStatus -> policyStatus == policyStatusCode);
    }

    private boolean isCurrentInvoicedTerm(char invoiceTypeCode) {
        return Arrays
                .asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                        InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                        InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue())
                .stream().anyMatch(invoiceType -> invoiceType == invoiceTypeCode);
    }

    private boolean isFutureInvoicedTerm(char invoiceTypeCode) {
        return Arrays.asList(BLANK_CHAR, InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue()).stream()
                .anyMatch(invoiceType -> invoiceType == invoiceTypeCode);
    }

    private double getAgreementTrmBalance(String accountId, String policyId, ZonedDateTime polEffectiveDt) {
        Double termBalanceAmount = bilIstScheduleRepository.calculateBalanceAmount(accountId, policyId, polEffectiveDt);
        if (termBalanceAmount == null) {
            termBalanceAmount = DECIMAL_ZERO;
        }
        return termBalanceAmount.doubleValue();
    }

    private boolean isCancellation(char polStatusCode, char suspendBillIndicator, char invoicingIndicator,
            char agreementInd, char prevCancIndicator, boolean nsfPcnIndicator, char driverIndicator,
            boolean isFutureEquity, boolean isNsfPcnCancel) {
        return ((polStatusCode == BilPolicyStatusCode.OPEN.getValue()
                || polStatusCode == BilPolicyStatusCode.SUSPEND_BILLING.getValue()) && suspendBillIndicator != CHAR_Y
                && invoicingIndicator != INVOICING && agreementInd != CHAR_Y && prevCancIndicator != PREV_CANC
                && (nsfPcnIndicator || driverIndicator == DRIVER_ACTIVE && isFutureEquity) && isNsfPcnCancel);
    }

    private char readLastBam(String accountId, BilPolicyTerm bilPolicyTerm) {
        char prevCancIndicator;
        char amountEffIndicator = BLANK_CHAR;
        List<Character> bilAmtEffIndList = new ArrayList<>(Arrays.asList(
                BilAmountsEftIndicator.CANCELLATION_F.getValue(), BilAmountsEftIndicator.CANCELLATION_P.getValue(),
                BilAmountsEftIndicator.CANCELLATION_S.getValue(), BilAmountsEftIndicator.REINSTATEMENT_K.getValue(),
                BilAmountsEftIndicator.REINSTATEMENT_L.getValue(), BilAmountsEftIndicator.REINSTATEMENT_O.getValue(),
                BilAmountsEftIndicator.REINSTATEMENT_T.getValue(), BilAmountsEftIndicator.REINSTATEMENT_Z.getValue(),
                BilAmountsEftIndicator.REINSTATEMENT_Y.getValue()));
        Short maxBilSequence = bilAmountsRepository.getMaxLastBilSeqNbr(accountId,
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), bilPolicyTerm.getPlnExpDt(), bilAmtEffIndList,
                CHAR_Y);
        if (maxBilSequence != null) {
            BilAmounts bilAmounts = bilAmountsRepository.findById(
                    new BilAmountsId(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(), maxBilSequence))
                    .orElse(null);
            if (bilAmounts != null) {
                amountEffIndicator = bilAmounts.getBilAmtEffInd();
            }
        }
        if (amountEffIndicator == BilAmountsEftIndicator.CANCELLATION_F.getValue()
                || amountEffIndicator == BilAmountsEftIndicator.CANCELLATION_S.getValue()
                || amountEffIndicator == BilAmountsEftIndicator.CANCELLATION_P.getValue()) {
            prevCancIndicator = PREV_CANC;
        } else {
            prevCancIndicator = CHAR_N;
        }
        return prevCancIndicator;
    }

    private Object[] writeOff(char polStatusCode, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, short cntPolSta,
            ZonedDateTime nextWriteoffDt, BilBillPlan bilBillPlan, ZonedDateTime recoveryDate) {
        ZonedDateTime writeoffDate;
        ZonedDateTime writeoffDueDate;
        ZonedDateTime writeoffDateOld = null;

        if (nextWriteoffDt.compareTo(DateRoutine.dateTimeAsYYYYMMDD("8888-12-31")) == 0) {
            nextWriteoffDt = DateRoutine.defaultDateTime();
        }

        if (bilBillPlan.getBbpWroInd() == CHAR_Y) {
            return new Object[] { nextWriteoffDt, CHAR_N };
        }

        if ((polStatusCode == BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue()
                || polStatusCode == BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue())
                && (bilPolicyTerm.getBptPolUwStaCd() == BilPolicyStatusCode.CANCELLED.getValue()
                        || bilPolicyTerm.getBptPolUwStaCd() == BilPolicyStatusCode.FLAT_CANCELLATION.getValue()
                        || bilPolicyTerm.getBptPolUwStaCd() == BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue()
                        || bilPolicyTerm.getBptPolUwStaCd() == BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED
                                .getValue())) {
            writeoffDate = bilPolicyTerm.getBptUwStaEffDt();
        } else {
            if (polStatusCode == BilPolicyStatusCode.CANCELLED.getValue()
                    || polStatusCode == BilPolicyStatusCode.FLAT_CANCELLATION.getValue()
                    || polStatusCode == BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue()
                    || polStatusCode == BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue()) {
                writeoffDate = bilPolicyTerm.getBptStatusEffDt();
            } else {
                writeoffDate = bilPolicyTerm.getPlnExpDt();
            }
        }
        if (cntPolSta == 0) {
            writeoffDueDate = bilIstScheduleRepository.selectMaxAdjDueDtByBilAccountId(bilAccount.getAccountId(),
                    writeoffDate);

        } else {
            writeoffDueDate = bilIstScheduleRepository.selectMaxAdjDueDtByBilAccountId(bilAccount.getAccountId(),
                    recoveryDate);
        }
        if (writeoffDueDate != null) {
            writeoffDate = writeoffDueDate;
        }
        if (nextWriteoffDt.equals(DateRoutine.defaultDateTime())) {
            writeoffDateOld = writeoffDate;
        }

        if (writeoffDateOld != null && writeoffDateOld.compareTo(writeoffDate) > 0) {
            writeoffDate = writeoffDateOld;
        }

        nextWriteoffDt = DateRoutine.adjustDateWithOffset(writeoffDate, false, bilBillPlan.getBbpWroNbr(), PLUS_SIGN,
                null);
        return new Object[] { nextWriteoffDt, bilPolicyTerm.getBptWroPrcInd() };
    }

    private short checkPolicyStatus(BilAccount bilAccount, ZonedDateTime recoveryDate) {
        short cntPolSta = 0;
        List<Character> billPolStatusCd = Arrays.asList(BilPolicyStatusCode.CLOSED.getValue(),
                BilPolicyStatusCode.CANCELLED.getValue(), BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(),
                BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue(),
                BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue(), BilPolicyStatusCode.FLAT_CANCELLATION.getValue(),
                BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue(), BilPolicyStatusCode.TRANSFERRED.getValue());
        Integer count = bilPolicyTermRepository.getPolicStatusCount(bilAccount.getAccountId(), billPolStatusCd,
                recoveryDate);
        if (count == null) {
            cntPolSta = (short) 1;
        } else {
            cntPolSta = 0;
        }
        return cntPolSta;
    }

    private Object[] cancelationdisburse(BilAccount bilAccount, String policyId, BilPolicyTerm bilPolicyTerm,
            BilSupportPlan bilSupportPlan, BilBillPlan bilBillPlan, ZonedDateTime nextDisbDt, ZonedDateTime nextAcwDt) {

        String bilAccountId = bilAccount.getAccountId();
        boolean polLvlCnDisb = false;
        List<Character> manualSuspenseIndList = Arrays.asList(ManualSuspendIndicator.BLANK.getValue(),
                ManualSuspendIndicator.DISABLED.getValue());
        List<Character> creditIndicatorList = Arrays.asList(CreditIndicator.UNIDENTIFIED_CASH.getValue(),
                CreditIndicator.DISBURSED_CASH.getValue());

        Double susDspAmt = bilCashDspRepository.getCancelationCreditSuspenseRow(bilAccountId, policyId,
                BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(), creditIndicatorList,
                manualSuspenseIndList, "CR");
        if (susDspAmt != null && susDspAmt != DECIMAL_ZERO) {
            nextAcwDt = getNextAcwDate(bilPolicyTerm, bilAccount, nextAcwDt);
        }

        susDspAmt = bilCashDspRepository.getCancelationCreditSuspenseRow(bilAccountId, policyId,
                BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                Arrays.asList(CreditIndicator.CANCELLATION_CREDIT.getValue(),
                        CreditIndicator.CANCELLATION_FOR_REWRITE.getValue(),
                        CreditIndicator.CANCELLATION_SUBJECT_TO_AUDIT.getValue()),
                manualSuspenseIndList, "CR");
        if (susDspAmt != null && susDspAmt != DECIMAL_ZERO) {
            if (susDspAmt.compareTo(bilBillPlan.getBbpCncMaxDsb()) > 0) {
                nextAcwDt = getNextAcwDate(bilPolicyTerm, bilAccount, nextAcwDt);
            } else {
                ZonedDateTime disburseDate = bilCashDspRepository.getMinDspDtCrCNSuspenseDspAmt(bilAccountId, policyId,
                        BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                        Arrays.asList(CreditIndicator.CANCELLATION_CREDIT.getValue(),
                                CreditIndicator.CANCELLATION_FOR_REWRITE.getValue(),
                                CreditIndicator.CANCELLATION_SUBJECT_TO_AUDIT.getValue()),
                        Arrays.asList(ManualSuspendIndicator.BLANK.getValue(),
                                ManualSuspendIndicator.DISABLED.getValue()),
                        bilSupportPlan.getBspOvpMaxDsb(), "CR");

                polLvlCnDisb = true;
                ZonedDateTime differenceDate = DateRoutine.adjustDateWithOffset(disburseDate, false,
                        bilBillPlan.getBbpCncHoldNbr(), PLUS_SIGN, null);
                if (differenceDate.compareTo(nextDisbDt) < 0) {
                    nextDisbDt = differenceDate;
                }
            }
        }
        return new Object[] { nextDisbDt, nextAcwDt, polLvlCnDisb };

    }

    private ZonedDateTime getNextAcwDate(BilPolicyTerm bilPolicyTerm, BilAccount bilAccount, ZonedDateTime nextAcwDt) {

        Short arwoDays = billingRulesService.readArwoRuleCheck(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                bilPolicyTerm.getBillPlanCd());

        if (arwoDays != null) {
            ZonedDateTime arwoPolMaxDt = getCancelationAcwAcyDate(bilAccount, bilPolicyTerm, arwoDays);
            if (arwoPolMaxDt.compareTo(nextAcwDt) < 0) {
                nextAcwDt = arwoPolMaxDt;
            }
        }
        return nextAcwDt;
    }

    private ZonedDateTime getCancelationAcwAcyDate(BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, Short arwoDays) {
        ZonedDateTime arwoPolMaxDt = DateRoutine.defaultDateTime();
        ZonedDateTime minDspDt = bilCashDspRepository.getMinDspDtForCancelationCredit(bilAccount.getAccountId(),
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), BilDspTypeCode.SUSPENDED.getValue(),
                Arrays.asList(CreditIndicator.UNIDENTIFIED_CASH.getValue(), CreditIndicator.DISBURSED_CASH.getValue()),
                Arrays.asList(ReverseReSuspendIndicator.RECONCILIATION_NOT_COMPLETE.getValue(),
                        ReverseReSuspendIndicator.BLANK.getValue()));

        if (minDspDt != null) {
            arwoPolMaxDt = DateRoutine.adjustDateWithOffset(minDspDt, false, arwoDays, PLUS_SIGN, null);
        }
        return arwoPolMaxDt;
    }

    private Object[] creditdisburse(BilAccount bilAccount, String policyId, BilPolicyTerm bilPolicyTerm,
            BilSupportPlan bilSupportPlan, BilBillPlan bilBillPlan, ZonedDateTime nextDisbDt, ZonedDateTime nextAcwDt) {
        String bilAccountId = bilAccount.getAccountId();
        ZonedDateTime nextDisbDtNew = null;
        boolean polLvlCrDisb = false;
        Short arwoDays = null;

        Double susDspAmt = bilCashDspRepository.getCreditSuspenseDspAmt(bilAccountId, policyId,
                BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                Arrays.asList(CreditIndicator.CREDIT.getValue(), CreditIndicator.EXCESS_CREDIT_CASH.getValue()),
                Arrays.asList(ManualSuspendIndicator.BLANK.getValue(), ManualSuspendIndicator.DISABLED.getValue()),
                "CR");
        if (susDspAmt != null && susDspAmt != DECIMAL_ZERO) {

            if (bilPolicyTerm.getBptAgreementInd() == CHAR_Y
                    && susDspAmt.compareTo(bilSupportPlan.getBspOvpMaxDsb()) > 0) {
                arwoDays = billingRulesService.readArwoRuleCheck(bilAccount.getBillTypeCd(),
                        bilAccount.getBillClassCd(), BLANK_STRING);
            } else if (susDspAmt.compareTo(bilBillPlan.getBbpCreMaxDsb()) > 0) {
                arwoDays = billingRulesService.readArwoRuleCheck(bilAccount.getBillTypeCd(),
                        bilAccount.getBillClassCd(), bilPolicyTerm.getBillPlanCd());
            }

            if (arwoDays != null) {
                ZonedDateTime arwoPolMaxDt = getPolAcwAcyDate(bilAccount, policyId, arwoDays);
                if (arwoPolMaxDt.compareTo(nextAcwDt) < 0) {
                    nextAcwDt = arwoPolMaxDt;
                }
                return new Object[] { nextDisbDt, nextAcwDt, polLvlCrDisb };

            }

            if (bilPolicyTerm.getBptAgreementInd() == CHAR_Y) {
                ZonedDateTime disburseDate = bilCashDspRepository.getMinDspDtCrCNSuspenseDspAmt(bilAccountId, policyId,
                        BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                        Arrays.asList(CreditIndicator.CREDIT.getValue(), CreditIndicator.EXCESS_CREDIT_CASH.getValue()),
                        Arrays.asList(ManualSuspendIndicator.BLANK.getValue(),
                                ManualSuspendIndicator.DISABLED.getValue()),
                        bilSupportPlan.getBspOvpMaxDsb(), "CR");
                if (disburseDate != null) {
                    nextDisbDtNew = DateRoutine.adjustDateWithOffset(disburseDate, false,
                            bilSupportPlan.getBspOvpHoldNbr(), PLUS_SIGN, null);
                }
            } else {
                ZonedDateTime disburseDate = bilCashDspRepository.getMinDspDtCrCNSuspenseDspAmt(bilAccountId, policyId,
                        BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                        Arrays.asList(CreditIndicator.CREDIT.getValue(), CreditIndicator.EXCESS_CREDIT_CASH.getValue()),
                        Arrays.asList(ManualSuspendIndicator.BLANK.getValue(),
                                ManualSuspendIndicator.DISABLED.getValue()),
                        bilBillPlan.getBbpCreMaxDsb(), "CR");
                if (disburseDate != null) {
                    nextDisbDtNew = DateRoutine.adjustDateWithOffset(disburseDate, false,
                            bilBillPlan.getBbpCreHoldNbr(), PLUS_SIGN, null);
                }
            }

            polLvlCrDisb = true;
            if (nextDisbDtNew != null && nextDisbDtNew.compareTo(nextDisbDt) < 0) {
                nextDisbDt = nextDisbDtNew;
            }
        }

        return new Object[] { nextDisbDt, nextAcwDt, polLvlCrDisb };
    }

    private ZonedDateTime getPolAcwAcyDate(BilAccount bilAccount, String policyId, Short arwoHoldDays) {

        ZonedDateTime arwoPolMaxDt = DateRoutine.defaultDateTime();
        ZonedDateTime minDspDt = bilCashDspRepository.getMinDspDtForExcessCredit(bilAccount.getAccountId(), policyId,
                BilDspTypeCode.SUSPENDED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                Arrays.asList(CreditIndicator.CREDIT.getValue(), CreditIndicator.EXCESS_CREDIT_CASH.getValue()),
                Arrays.asList(ManualSuspendIndicator.BLANK.getValue(), ManualSuspendIndicator.DISABLED.getValue()),
                "CR");

        if (minDspDt != null) {
            arwoPolMaxDt = DateRoutine.adjustDateWithOffset(minDspDt, false, arwoHoldDays, PLUS_SIGN, null);
        }
        return arwoPolMaxDt;
    }

    private boolean isCashAcyCall(char cashAcyCallIndicator) {
        return (cashAcyCallIndicator == CASH_ACTIVITY || cashAcyCallIndicator == CHECK_FOR_REINSTATEMENT);
    }

    private BilBillPlan getBilBillPlan(String bilPlanCode) {
        BilBillPlan bilBillPlan = bilBillPlanRepository.findById(bilPlanCode).orElse(null);
        if (bilBillPlan == null) {
            logger.error("Rows not found in bil Bill plan");
            throw new DataNotFoundException("support.data.missing", new Object[] { bilPlanCode });
        }
        return bilBillPlan;
    }

    private List<Character> getInvoiceCode() {
        List<Character> invoiceCodeList = new ArrayList<>();
        invoiceCodeList.add(InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue());
        invoiceCodeList.add(InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());
        invoiceCodeList.add(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue());
        return invoiceCodeList;
    }

    private List<Character> getPolStatusList() {
        List<Character> bilPolStatusList = new ArrayList<>();
        bilPolStatusList.add(BilPolicyStatusCode.PENDING_TRANSFER.getValue());
        bilPolStatusList.add('3');
        bilPolStatusList.add('4');
        bilPolStatusList.add(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue());
        bilPolStatusList.add(BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue());
        bilPolStatusList.add(BilPolicyStatusCode.PENDING_TRANSFER_APPLYPAYMENTS.getValue());
        bilPolStatusList.add(BilPolicyStatusCode.PENDING_TRANSFER_HOLDPAYMENTS.getValue());
        bilPolStatusList.add(BilPolicyStatusCode.TRANSFERRED.getValue());
        return bilPolStatusList;
    }

    private Map<String, Object> initializationTermDataMap() {
        Map<String, Object> termDataMap = new HashMap<>();
        termDataMap.put(TermAcyType.TERMPASTDATE.getValue(), DateRoutine.defaultDateTime());
        termDataMap.put(TermAcyType.TERMPCNDATE.getValue(), DateRoutine.defaultDateTime());
        termDataMap.put(TermAcyType.TERMPASTAMOUNT.getValue(), DECIMAL_ZERO);
        termDataMap.put(TermAcyType.TERMPASTPERCENT.getValue(), DECIMAL_ZERO);
        termDataMap.put(TermAcyType.TERMPREMIUMAMOUNT.getValue(), DECIMAL_ZERO);
        termDataMap.put(TermAcyType.BILLDATESINDICATOR.getValue(), BLANK_CHAR);
        termDataMap.put(TermAcyType.TERMINVOICEINDICATOR.getValue(), CHAR_X);
        termDataMap.put(NsfPcnDataType.NRVNEXTACYDATE.getValue(), DateRoutine.defaultDateTime());
        termDataMap.put(NsfPcnDataType.BILISTDUEDATE.getValue(), DateRoutine.defaultDateTime());
        termDataMap.put(NsfPcnDataType.NSFPCNAMOUNT.getValue(), DECIMAL_ZERO);
        termDataMap.put(NsfPcnDataType.NSFPCNPERCENT.getValue(), DECIMAL_ZERO);
        termDataMap.put(NsfPcnDataType.BILISTDUEAMOUNT.getValue(), DECIMAL_ZERO);
        termDataMap.put(NsfPcnDataType.NSFPCNCANCEL.getValue(), false);
        termDataMap.put(NsfPcnDataType.FUTUREEQUITY.getValue(), false);
        termDataMap.put(NsfPcnDataType.TERMNEVERBILLED.getValue(), false);

        return termDataMap;
    }

    private Map<String, Object> initializationPedAcyData() {
        Map<String, Object> pedAcyMap = new HashMap<>();
        pedAcyMap.put(PedAcyType.POLEFFDATE.getValue(), DateRoutine.defaultDateTime());
        pedAcyMap.put(PedAcyType.POLEXPDATE.getValue(), DateRoutine.defaultDateTime());
        pedAcyMap.put(PedAcyType.ACREQUITYDATE.getValue(), DateRoutine.defaultDateTime());
        pedAcyMap.put(PedAcyType.NEXTACYDATE.getValue(), DateRoutine.defaultDateTime());
        pedAcyMap.put(PedAcyType.EQUITYIND.getValue(), BLANK_CHAR);
        pedAcyMap.put(PedAcyType.ISSUESYSID.getValue(), BLANK_STRING);
        return pedAcyMap;
    }

    private Map<String, Object> initializationPcnnData() {
        Map<String, Object> pcnnDataMap = new HashMap<>();
        pcnnDataMap.put(PcnnType.PCNFLATCNCIND.getValue(), BLANK_CHAR);
        pcnnDataMap.put(PcnnType.REINSTCODE.getValue(), BLANK_CHAR);
        pcnnDataMap.put(PcnnType.ADDBASRNRIND.getValue(), BLANK_CHAR);
        pcnnDataMap.put(PcnnType.BYPASSPCNNSFIND.getValue(), BLANK_CHAR);
        pcnnDataMap.put(PcnnType.PCNREASON.getValue(), BLANK_STRING);
        pcnnDataMap.put(PcnnType.MINDUEDATE.getValue(), DateRoutine.defaultDateTime());
        pcnnDataMap.put(PcnnType.MAXDUEDATE.getValue(), DateRoutine.defaultDateTime());
        return pcnnDataMap;
    }

    public enum TermAcyType {
        TERMPASTDATE("termPastDate"),
        TERMPCNDATE("termPcnDate"),
        TERMPASTAMOUNT("termPastAmount"),
        TERMPASTPERCENT("termPastPercent"),
        TERMPREMIUMAMOUNT("termPremiumAmount"),
        BILLDATESINDICATOR("billDatesIndicator"),
        TERMINVOICEINDICATOR("termInvoiceIndicator");

        private final String termAcType;

        private TermAcyType(String termAcType) {
            this.termAcType = termAcType;
        }

        public String getValue() {
            return termAcType;
        }
    }

    public enum NsfPcnDataType {
        NSFPCNAMOUNT("nsfPcnAmount"),
        NSFPCNPERCENT("nsfPcnPercent"),
        BILISTDUEAMOUNT("bilIstDueAmount"),
        BILISTDUEDATE("bilIstDueDate"),
        NSFPCNCANCEL("nsfPcnCancel"),
        FUTUREEQUITY("futureEquity"),
        TERMNEVERBILLED("termNeverBilled"),
        NRVNEXTACYDATE("nrvNextAcyDate");

        private final String nsfPcnDatType;

        private NsfPcnDataType(String nsfPcnDatType) {
            this.nsfPcnDatType = nsfPcnDatType;
        }

        public String getValue() {
            return nsfPcnDatType;
        }
    }

    private Short[] getLegalDays(String processCode, BilBillPlan bilBillPlan, BilPolicyTerm bilPolicyTerm,
            BilPolicy bilPolicy) {

        short legalDaysNoticeMaxDays = SHORT_ZERO;
        short legalDaysNoticeMailDays = SHORT_ZERO;
        short legalDaysNoticeLegDays = SHORT_ZERO;

        if (bilBillPlan.getBbpInvDuringPcnInd() == CHAR_Y && bilBillPlan.getBbpAltLglDays() > SHORT_ZERO) {
            legalDaysNoticeMaxDays = bilBillPlan.getBbpAltLglDays();
            legalDaysNoticeLegDays = bilBillPlan.getBbpAltLglDays();
        }

        if (bilBillPlan.getBbpInvDuringPcnInd() == CHAR_N || bilBillPlan.getBbpAltLglDays() == SHORT_ZERO) {
            Short[] policyLegalObject = readPolicyLegalDays(processCode, bilPolicyTerm, bilPolicy);
            if (policyLegalObject != null && policyLegalObject.length != 0) {
                legalDaysNoticeMaxDays = policyLegalObject[0];
                legalDaysNoticeMailDays = policyLegalObject[1];
                legalDaysNoticeLegDays = policyLegalObject[2];
            }
        }
        return new Short[] { legalDaysNoticeMaxDays, legalDaysNoticeMailDays, legalDaysNoticeLegDays };
    }

    private Short[] readPolicyLegalDays(String processCode, BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy) {

        List<LegalDaysNotice> legalDaysNoticeList = legalDaysNoticeRepository.getLegalDaysNoticeData(
                Arrays.asList(bilPolicyTerm.getLobCd(), "ZZZ"), Arrays.asList(bilPolicy.getPolSymbolCd(), "ZZZ"),
                Arrays.asList(bilPolicyTerm.getBillStatePvnCd(), "ZZZ"),
                Arrays.asList(bilPolicyTerm.getMasterCompanyNbr(), BillingConstants.MASTER_COMPANY_NUMBER), processCode,
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), Arrays.asList('P', 'R', '*'));
        if (legalDaysNoticeList != null && !legalDaysNoticeList.isEmpty()) {
            LegalDaysNotice legalDaysNotice = legalDaysNoticeList.get(SHORT_ZERO);
            return new Short[] { legalDaysNotice.getMaximumLegalDaysNumber(), legalDaysNotice.getMailLegalDaysNumber(),
                    legalDaysNotice.getLegalDaysNumber() };
        }

        return new Short[0];
    }
}