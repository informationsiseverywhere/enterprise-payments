package billing.handler.impl;

import static billing.utils.BillingConstants.ERROR_CODE;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilMstCoSt;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilCashDspId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilMstCoStRepository;
import billing.handler.model.WriteOffPayment;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.ManualSuspendIndicator;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class WriteOffDueToOverPaymentServiceImpl {

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilMstCoStRepository bilMstCoStRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private DateService dateService;

    @Transactional
    public BilCashDsp writeOffDueToOverPayment(BilAccount bilAccount, WriteOffPayment writeOffPayment) {

        List<BilCashDsp> bilCashDspList = bilCashDspRepository.findSuspendOverPaymentRows(bilAccount.getAccountId(),
                BilDspTypeCode.SUSPENDED.getValue(), writeOffPayment.getAdjustDueDate(), BLANK_STRING, BLANK_STRING,
                ReverseReSuspendIndicator.BLANK.getValue(), ManualSuspendIndicator.ENABLED.getValue());
        if (bilCashDspList != null && !bilCashDspList.isEmpty()) {
            for (BilCashDsp bilCashDsp : bilCashDspList) {
                bilCashDspRepository.deleteById(bilCashDsp.getBilCashDspId());
                insertBilCashDsp(bilCashDsp, bilAccount, writeOffPayment);
            }
        }
        return null;
    }

    private void insertBilCashDsp(BilCashDsp bilCashDsp, BilAccount bilAccount, WriteOffPayment writeOffPayment) {

        BilCashDsp bilCashDspSave = new BilCashDsp();
        Short bilDspSequenceNbr = getBilDspSeqNbr(bilCashDsp.getBilCashDspId().getAccountId(),
                bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr());
        BilCashDspId bilCashDspId = new BilCashDspId(bilCashDsp.getBilCashDspId().getAccountId(),
                bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr(),
                bilDspSequenceNbr);
        bilCashDspSave.setBilCashDspId(bilCashDspId);
        bilCashDspSave.setPolicyId(bilCashDsp.getPolicyId());
        bilCashDspSave.setPolicySymbolCd(bilCashDsp.getPolicySymbolCd());
        bilCashDspSave.setPolicyNumber(bilCashDsp.getPolicyNumber());
        bilCashDspSave.setPayableItemCd(bilCashDsp.getPayableItemCd());
        bilCashDspSave.setAdjustmentDueDate(bilCashDsp.getAdjustmentDueDate());
        bilCashDspSave.setChargeTypeCd(BLANK_CHAR);
        bilCashDspSave.setUserId(bilCashDsp.getUserId());
        bilCashDspSave.setDspDate(writeOffPayment.getApplicationDate());
        bilCashDspSave.setDspTypeCd(BilDspTypeCode.WRITE_OFF.getValue());
        bilCashDspSave.setDspReasonCd("CI");
        bilCashDspSave.setManualSuspenseIndicator(bilCashDsp.getManualSuspenseIndicator());
        bilCashDspSave.setDspAmount(bilCashDsp.getDspAmount());
        bilCashDspSave.setPayeeClientId(bilCashDsp.getPayeeClientId());
        bilCashDspSave.setPayeeAddressSequenceNumber(bilCashDsp.getPayeeAddressSequenceNumber());
        bilCashDspSave.setDraftNbr(SHORT_ZERO);
        bilCashDspSave.setCreditIndicator(bilCashDsp.getCreditIndicator());
        bilCashDspSave.setCreditPolicyId(bilCashDsp.getCreditPolicyId());
        bilCashDspSave.setCreditAmountType(bilCashDsp.getCreditAmountType());
        bilCashDspSave.setRevsRsusIndicator(bilCashDsp.getRevsRsusIndicator());
        bilCashDspSave.setToFromTransferNbr(SEPARATOR_BLANK);
        bilCashDspSave.setTransferTypeCd(BLANK_CHAR);
        bilCashDspSave.setStatusCd(bilCashDsp.getStatusCd());
        bilCashDspSave.setDisbursementId(bilCashDsp.getDisbursementId());
        bilCashDspSave.setCheckProdMethodCd(bilCashDsp.getCheckProdMethodCd());
        bilCashDspSave.setPolicyEffectiveDate(bilCashDsp.getPolicyEffectiveDate());
        bilCashDspSave.setBilSequenceNumber(bilCashDsp.getBilSequenceNumber());
        bilCashDspSave.setSystemDueDate(bilCashDsp.getSystemDueDate());
        bilCashDspSave.setInvoiceDate(bilCashDsp.getInvoiceDate());
        bilCashDspSave.setDisbursementDate(bilCashDsp.getDisbursementDate());
        bilCashDspSave.setStatementDetailTypeCd(bilCashDsp.getStatementDetailTypeCd());
        bilCashDspRepository.saveAndFlush(bilCashDspSave);

        insertBilAccountSummary(bilAccount.getAccountId(), bilCashDspSave.getAdjustmentDueDate(),
                bilCashDspSave.getDspAmount(), "CI", writeOffPayment.getApplicationDate(), "CWA", "SYSTEMIW",
                BLANK_STRING, BLANK_STRING);
        if (writeOffPayment.getQuoteIndicator().charAt(SHORT_ZERO) != CHAR_Q) {
            BilCashReceipt bilCashReceipt = bilCashReceiptRepository
                    .findById(new BilCashReceiptId(bilCashDsp.getBilCashDspId().getAccountId(),
                            bilCashDsp.getBilCashDspId().getBilDtbDate(),
                            bilCashDsp.getBilCashDspId().getDtbSequenceNbr()))
                    .orElse(null);
            if (bilCashReceipt == null) {
                throw new DataNotFoundException("no.data.found");
            }
            String databaseKey = getBilDatabaseKey(bilCashReceipt.getEntryDate(), bilCashReceipt.getEntryNumber(),
                    bilCashReceipt.getUserId(), bilCashReceipt.getEntrySequenceNumber());
            String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(), bilAccount.getBillTypeCd());

            callFinancialApi(bilCashDspSave.getDspAmount(), writeOffPayment.getApplicationDate(),
                    bilCashReceipt.getBankCd(), bilAccount.getAccountNumber(), ObjectCode.CASH.toString(),
                    ActionCode.WRITE_OFF.toString(), BLANK_STRING,
                    String.valueOf(bilCashReceipt.getCashEntryMethodCd()), bilCashReceipt.getReceiptTypeCd(),
                    BLANK_STRING, bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                    bilCashDsp.getBilCashDspId().getBilDtbDate(), databaseKey, "SYSTEMIW", BLANK_STRING,
                    ApplicationProgramIdentifier.BCMOIW.toString(), BLANK_STRING, BLANK_STRING, BLANK_STRING,
                    bilmasterCompanyState[0], bilmasterCompanyState[1], bilAccount.getCurrencyCode());
        }
    }

    private void insertBilAccountSummary(String accountId, ZonedDateTime adjustDueDate, double amount,
            String bilAcyDesCd, ZonedDateTime se3DateTime, String bilDesReaTyp, String userId, String polNbr,
            String polSymbolCd) {

        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId, se3DateTime,
                getBilAcySeq(accountId, se3DateTime));
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(polSymbolCd);
        bilActSummary.setPolNbr(polNbr);
        bilActSummary.setBilAcyDesCd(bilAcyDesCd);
        bilActSummary.setBilDesReaTyp(bilDesReaTyp);
        bilActSummary.setBasAddDataTxt(SEPARATOR_BLANK);

        bilActSummary.setBilAcyDes1Dt(adjustDueDate);
        bilActSummary.setBilAcyDes2Dt(DateRoutine.defaultDateTime());
        bilActSummary.setBilAcyAmt(amount);
        bilActSummary.setUserId(userId);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());

        bilActSummaryRepository.saveAndFlush(bilActSummary);

    }

    private Short getBilAcySeq(String accountId, ZonedDateTime se3DateTime) {
        Short bilAcySeq = 0;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, se3DateTime.with(LocalTime.MIN));
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        return bilAcySeq;
    }

    private String getBilDatabaseKey(ZonedDateTime entryDate, String entryNumber, String operatorId,
            short bilDtbSequenceNumber) {
        String seqNumberSign = "+";

        String dataString = DateRoutine.dateTimeAsYYYYMMDDString(entryDate);
        dataString += entryNumber;
        dataString += StringUtils.rightPad(operatorId, 8, BLANK_CHAR);
        dataString += seqNumberSign;
        dataString += StringUtils.leftPad(String.valueOf(bilDtbSequenceNumber), 5, CHAR_ZERO);

        return dataString;
    }

    private String[] readBilMstCoSt(String bilClassCode, String bilTypeCode) {
        String masterCompany = BillingConstants.MASTER_COMPANY_NUMBER;
        String riskState = BLANK_STRING;
        BilMstCoSt bilMstCoSt = bilMstCoStRepository.findRow(bilClassCode, bilTypeCode, BLANK_STRING);
        if (bilMstCoSt != null) {
            masterCompany = bilMstCoSt.getMasterCompanyNbr();
            riskState = bilMstCoSt.getBilStatePvnCd();
        }
        return new String[] { masterCompany, riskState };
    }

    private void callFinancialApi(Double amount, ZonedDateTime applicationDate, String bankCode, String accountNumber,
            String transactionObjectCode, String transactionActionCode, String paybleItemCode, String sourceCode,
            String paymentType, String bilReasonCode, String bilTypeCode, String bilClassCode,
            ZonedDateTime referenceDate, String databaseKey, String operatorId, String orginalEffectiveDate,
            String applicationProgram, String policyId, String countryCode, String lineOfBusiness, String masterCompany,
            String riskState, String currencyCode) {

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();

        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(BLANK_STRING);
        financialApiActivity.setApplicationName(ApplicationName.BCMOCA.toString());
        financialApiActivity.setUserId(BLANK_STRING);
        financialApiActivity.setErrorCode(ERROR_CODE);

        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setMasterCompanyNbr(masterCompany);
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setAgentTtyId(BLANK_STRING);
        financialApiActivity.setAppProgramId(applicationProgram);
        financialApiActivity.setBilAccountId(accountNumber);
        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setBilClassCode(bilClassCode);
        financialApiActivity.setBilDatabaseKey(databaseKey);
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilReceiptTypeCode(paymentType);
        financialApiActivity.setBilReasonCode(bilReasonCode);
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(referenceDate));
        financialApiActivity.setBilSourceCode(sourceCode);
        financialApiActivity.setBilTypeCode(bilTypeCode);
        financialApiActivity.setClientId(BLANK_STRING);
        financialApiActivity.setCountryCode(countryCode);
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setCurrencyCode(currencyCode);
        financialApiActivity.setDepositeBankCode(BLANK_STRING);
        financialApiActivity.setDisbursementId(BLANK_STRING);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setLineOfBusCode(lineOfBusiness);
        financialApiActivity.setMarketSectorCode(BLANK_STRING);
        financialApiActivity.setOriginalEffectiveDate(orginalEffectiveDate);
        financialApiActivity.setOperatorId(operatorId);
        financialApiActivity.setPayableItemCode(paybleItemCode);
        financialApiActivity.setPolicyId(policyId);
        financialApiActivity.setStateCode(riskState);
        financialApiActivity.setStatusCode(BLANK_CHAR);

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {

            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

    private Short getBilDspSeqNbr(String accountId, ZonedDateTime bilDtbDate, Short bilDtbSeqNbr) {
        Short bilDspSeqNbr = 0;
        bilDspSeqNbr = bilCashDspRepository.findMaxDispositionNumber(accountId, bilDtbDate, bilDtbSeqNbr);
        if (bilDspSeqNbr == null) {
            bilDspSeqNbr = 0;
        } else {
            bilDspSeqNbr = (short) (bilDspSeqNbr + 1);
        }
        return bilDspSeqNbr;
    }
}
