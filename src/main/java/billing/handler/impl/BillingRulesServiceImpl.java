package billing.handler.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActInquiry;
import billing.data.entity.BilActRules;
import billing.data.entity.BilCashDsp;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilPolActivity;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.id.BilActInquiryId;
import billing.data.entity.id.BilCashReceiptId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilDatesRepository;
import billing.data.repository.BilEftBankRepository;
import billing.data.repository.BilEftPendingTapeRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.handler.service.BillingRulesService;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants.ActivityType;
import billing.utils.BillingConstants.BilDesReasonCode;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BilTechKeyTypeCode;
import billing.utils.BillingConstants.BillingMethod;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CashEntryMethod;
import billing.utils.BillingConstants.CreditIndicator;
import billing.utils.BillingConstants.EftStatusCode;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.ManualSuspendIndicator;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;

@Service("billingRulesService")
public class BillingRulesServiceImpl implements BillingRulesService {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilEftPendingTapeRepository bilEftPendingTapeRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilDatesRepository bilDatesRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    private static final char PLUS_SIGN = '+';
    private static final char SECOND_NOTICE_NO = CHAR_N;
    private static final char SECOND_NOTICE_OFF = BLANK_CHAR;
    private static final char EFT = CHAR_Y;
    private static final char OVERRIDE_PAYEE_RULES = CHAR_O;
    private static final String CREDIT_COMMENT = "CR";

    @Override
    public Map<String, Object> disburseToSource(BilAccount bilAccount, BilActRules bilActRules,
            Map<String, Object> nextDisburseDates) {

        ZonedDateTime nextDisbDate;
        ZonedDateTime nextAcwDate = (ZonedDateTime) nextDisburseDates.get(DisburseKeys.NEXTACWDATE.toString());
        String accountId = bilAccount.getAccountId();

        Double susDspAmount = bilCashDspRepository.getSuspendDspAmount(accountId, BilDspTypeCode.SUSPENDED.getValue(),
                ManualSuspendIndicator.ENABLED.getValue(), getCreditIndList(),
                ReverseReSuspendIndicator.BLANK.getValue(), CREDIT_COMMENT);
        if (susDspAmount == null || susDspAmount == DECIMAL_ZERO) {
            nextAcwDate = DateRoutine.defaultDateTime();
        } else {
            List<BilCashDsp> bilCashDspList = bilCashDspRepository.getAcwTriggerIsExisting(accountId,
                    BilDspTypeCode.SUSPENDED.getValue(), ManualSuspendIndicator.ENABLED.getValue(), getCreditIndList(),
                    ReverseReSuspendIndicator.BLANK.getValue(), DateRoutine.defaultDateTime());
            if (bilCashDspList != null && !bilCashDspList.isEmpty()) {
                nextAcwDate = getAutomaticCashWriteOffDate(bilCashDspList, bilAccount.getBillTypeCd(),
                        bilAccount.getBillClassCd(), nextAcwDate);

            }

        }

        susDspAmount = bilCashDspRepository.getTransferDspAmount(accountId, BilDspTypeCode.SUSPENDED.getValue(),
                ManualSuspendIndicator.ENABLED.getValue(), getCreditIndList(),
                ReverseReSuspendIndicator.BLANK.getValue(), CREDIT_COMMENT);
        if (susDspAmount == null || susDspAmount == DECIMAL_ZERO) {
            nextDisbDate = DateRoutine.defaultDateTime();
            updateNextDisbDateToDefault(accountId, nextDisbDate);
            updateBilActNextAcyDate(accountId, nextDisbDate);
        } else {
            modifyBilAcyTrigger(nextDisburseDates, accountId, bilAccount.getCollectionMethod(),
                    bilAccount.getDisbPayeeCd(), bilAccount.getCollectionPlan(), bilActRules);
            return nextDisburseDates;
        }

        if (nextDisburseDates.get(DisburseKeys.SOURCETRANSFERTYPE.toString()) == null) {
            nextDisburseDates.put(DisburseKeys.SOURCETRANSFERTYPE.toString(), BLANK_STRING);
        }

        nextDisburseDates.put(DisburseKeys.NEXTACWDATE.toString(), nextAcwDate);
        nextDisburseDates.put(DisburseKeys.NEXTDISBURSEMENTDATE.toString(), nextDisbDate);
        return nextDisburseDates;
    }

    private Map<String, Object> modifyBilAcyTrigger(Map<String, Object> disbInfo, String accountId,
            String collectionMethod, char disbPayeeCd, String collectionPlan, BilActRules bilActRules) {
        ZonedDateTime nextDisbDate;

        Double dispositionAmount = bilCashDspRepository.getSuspendDspAmountInPast(accountId,
                BilDspTypeCode.SUSPENDED.getValue(), ManualSuspendIndicator.ENABLED.getValue(), getCreditIndList(),
                ReverseReSuspendIndicator.BLANK.getValue(), execContext.getApplicationDate(), CREDIT_COMMENT);
        if (dispositionAmount == null || dispositionAmount == DECIMAL_ZERO) {
            nextDisbDate = DateRoutine.defaultDateTime();
            updateBilActNextAcyDateByDisbDate(accountId, nextDisbDate);
        }

        dispositionAmount = bilCashDspRepository.getSuspendDspAmountByDisbursementDate(accountId,
                BilDspTypeCode.SUSPENDED.getValue(), ManualSuspendIndicator.ENABLED.getValue(), getCreditIndList(),
                ReverseReSuspendIndicator.BLANK.getValue(), DateRoutine.defaultDateTime(), CREDIT_COMMENT);
        if (dispositionAmount != null && dispositionAmount != DECIMAL_ZERO) {
            ZonedDateTime effectiveDate = null;
            String disbToSource = BLANK_STRING;
            if (collectionMethod.trim().equalsIgnoreCase(BillingMethod.DI.name())) {
                effectiveDate = DateRoutine.defaultDateTime();
            }
            if (disbPayeeCd == OVERRIDE_PAYEE_RULES) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                effectiveDate = DateRoutine.defaultDateTime();
            } else if ((collectionMethod.equalsIgnoreCase(BillingMethod.EFT.name())
                    || collectionMethod.equalsIgnoreCase(BillingMethod.ECC.name())
                    || collectionMethod.equalsIgnoreCase(BillingMethod.ELECTRONIC_WALLET.toString()))
                    && !collectionPlan.trim().isEmpty()) {
                List<BilEftBank> bilEftBankPage = bilEftBankRepository.findBilEftBankEntry(accountId,
                        BilTechKeyTypeCode.BILL_ACCOUNT.getValue(), execContext.getApplicationDate(),
                        EftStatusCode.ACTIVE.getValue(), PageRequest.of(SHORT_ZERO, SHORT_ONE));
                if (bilEftBankPage != null && !bilEftBankPage.isEmpty()) {
                    effectiveDate = bilEftBankPage.get(0).getBilEftBankId().getBebEffectiveDt();
                } else {
                    disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                    effectiveDate = DateRoutine.defaultDateTime();
                }
            }

            if (effectiveDate != null && effectiveDate.compareTo(DateRoutine.defaultDateTime()) != 0
                    && !collectionPlan.trim().isEmpty()) {
                if (collectionMethod.equalsIgnoreCase(BillingMethod.EFT.name())) {
                    disbToSource = ActivityType.DISBURSE_EFT.getValue();
                } else if (collectionMethod.equalsIgnoreCase(BillingMethod.ECC.name())
                        || collectionMethod.equalsIgnoreCase(BillingMethod.ELECTRONIC_WALLET.toString())) {
                    disbToSource = ActivityType.DISBURSE_EFT_CREDIT.getValue();
                }
            } else if (collectionMethod.equalsIgnoreCase(BillingMethod.EFT.name())
                    || collectionMethod.equalsIgnoreCase(BillingMethod.ECC.name())
                    || collectionMethod.equalsIgnoreCase(BillingMethod.ELECTRONIC_WALLET.toString())) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
            }

            Object[] maxReceiptInfo = null;
            if (disbPayeeCd != OVERRIDE_PAYEE_RULES) {
                maxReceiptInfo = getMaxReceipt(accountId, disbToSource, collectionPlan, effectiveDate,
                        bilActRules.getBruDsbCrcrdDays(), disbPayeeCd);
                disbToSource = (String) maxReceiptInfo[0];
            } else {
                maxReceiptInfo = new Object[] { disbToSource, false, DateRoutine.defaultDateTime(),
                        DateRoutine.defaultDateTime(), SHORT_ZERO };
            }

            Map<String, ZonedDateTime> currentTriggersMap = getCurrentTrigger(accountId);
            setAcyInquiryTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, bilActRules, currentTriggersMap);
            disbInfo.put(DisburseKeys.BILDTBDATE.toString(), maxReceiptInfo[3]);
            disbInfo.put(DisburseKeys.BILDTBSEQUENCE.toString(), maxReceiptInfo[4]);
        }
        return disbInfo;
    }

    private Map<String, Object> setAcyInquiryTrigger(Map<String, Object> disbInfo, String accountId,
            String disbToSource, Object[] maxReceiptInfo, BilActRules bilActRules,
            Map<String, ZonedDateTime> currentTriggersMap) {

        if (disbToSource.equals(ActivityType.DISBURSE_EFT.getValue())) {
            disbToSource = setAdeTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                    bilActRules);
        }

        if (disbToSource.equals(ActivityType.DISBURSE_EFT_CREDIT.getValue())) {
            disbToSource = setAdcTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                    bilActRules);
        }

        if (disbToSource.equals(ActivityType.DISBURSE_CHECK_DRAFT.getValue())) {
            disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                    bilActRules);

        }

        if (disbToSource.equals(ActivityType.DISBURSE_ACH.getValue())) {
            disbToSource = setAdpTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                    bilActRules);
        }

        if (disbToSource.equals(ActivityType.ONETIME_ACH.getValue())) {
            disbToSource = setAdaTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                    bilActRules);
        }

        if (disbToSource.equals(ActivityType.DISBURSE_CREDIT.getValue())) {
            disbToSource = setAdoTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                    bilActRules);
        }

        disbInfo.put(DisburseKeys.SOURCETRANSFERTYPE.toString(), disbToSource);
        return disbInfo;
    }

    private String setAdoTrigger(Map<String, Object> disbInfo, String accountId, String disbToSource,
            Object[] maxReceiptInfo, Map<String, ZonedDateTime> currentTriggersMap, BilActRules bilActRules) {
        ZonedDateTime bilDtbDate = (ZonedDateTime) maxReceiptInfo[2];
        ZonedDateTime nextAdoDate = DateRoutine.getAdjustedDate(bilDtbDate, false, bilActRules.getBruDsbCrcrdDays(),
                null);
        if (nextAdoDate.compareTo(execContext.getApplicationDate()) < 0) {
            nextAdoDate = execContext.getApplicationDate();
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_CHECK_DRAFT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdoDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_ACH.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_ACH.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdoDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.ONETIME_ACH.getValue())) {
            String currentTriggerType = ActivityType.ONETIME_ACH.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdoDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        ZonedDateTime currentTriggerDate = DateRoutine.defaultDateTime();
        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_CREDIT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CREDIT.getValue();
            currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(DateRoutine.defaultDateTime()) != 0
                    && currentTriggerDate.compareTo(nextAdoDate) > 0) {
                updateDisbursementDate(accountId, DateRoutine.defaultDateTime(), nextAdoDate, currentTriggerDate);
                return disbToSource;
            }
        }

        disbToSource = ActivityType.DISBURSE_CREDIT.getValue();
        updateDisbursementDate(accountId, DateRoutine.defaultDateTime(), currentTriggerDate, nextAdoDate);
        nextAdoDate = disburseToSourceTrigger(accountId, disbToSource, nextAdoDate);

        disbInfo.put(DisburseKeys.NEXTDISBURSEMENTDATE.toString(), nextAdoDate);
        return disbToSource;
    }

    private String setAdaTrigger(Map<String, Object> disbInfo, String accountId, String disbToSource,
            Object[] maxReceiptInfo, Map<String, ZonedDateTime> currentTriggersMap, BilActRules bilActRules) {
        ZonedDateTime bilDtbDate = (ZonedDateTime) maxReceiptInfo[2];
        ZonedDateTime nextAdaDate = DateRoutine.getAdjustedDate(bilDtbDate, false, bilActRules.getBruDsbPacsDays(),
                null);
        if (nextAdaDate.compareTo(execContext.getApplicationDate()) < 0) {
            nextAdaDate = execContext.getApplicationDate();
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_CHECK_DRAFT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdaDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_CREDIT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CREDIT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdaDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_ACH.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_ACH.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdaDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        ZonedDateTime currentTriggerDate = DateRoutine.defaultDateTime();
        if (currentTriggersMap.containsKey(ActivityType.ONETIME_ACH.getValue())) {
            String currentTriggerType = ActivityType.ONETIME_ACH.getValue();
            currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(DateRoutine.defaultDateTime()) != 0
                    && currentTriggerDate.compareTo(nextAdaDate) > 0) {
                updateDisbursementDate(accountId, DateRoutine.defaultDateTime(), nextAdaDate, currentTriggerDate);
                return disbToSource;
            }
        }

        disbToSource = ActivityType.ONETIME_ACH.getValue();
        updateDisbursementDate(accountId, DateRoutine.defaultDateTime(), currentTriggerDate, nextAdaDate);
        nextAdaDate = disburseToSourceTrigger(accountId, disbToSource, nextAdaDate);

        disbInfo.put(DisburseKeys.NEXTDISBURSEMENTDATE.toString(), nextAdaDate);
        return disbToSource;
    }

    private String setAdpTrigger(Map<String, Object> disbInfo, String accountId, String disbToSource,
            Object[] maxReceiptInfo, Map<String, ZonedDateTime> currentTriggersMap, BilActRules bilActRules) {
        ZonedDateTime bilDtbDate = (ZonedDateTime) maxReceiptInfo[2];
        ZonedDateTime nextAdpDate = DateRoutine.getAdjustedDate(bilDtbDate, false, bilActRules.getBruDsbPacsDays(),
                null);
        if (nextAdpDate.compareTo(execContext.getApplicationDate()) < 0) {
            nextAdpDate = execContext.getApplicationDate();
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_CHECK_DRAFT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdpDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_CREDIT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CREDIT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdpDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.ONETIME_ACH.getValue())) {
            String currentTriggerType = ActivityType.ONETIME_ACH.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdpDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        ZonedDateTime currentTriggerDate = DateRoutine.defaultDateTime();
        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_ACH.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_ACH.getValue();
            currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(DateRoutine.defaultDateTime()) != 0
                    && currentTriggerDate.compareTo(nextAdpDate) > 0) {
                updateDisbursementDate(accountId, DateRoutine.defaultDateTime(), nextAdpDate, currentTriggerDate);
                return disbToSource;
            }
        }

        disbToSource = ActivityType.DISBURSE_ACH.getValue();
        updateDisbursementDate(accountId, DateRoutine.defaultDateTime(), currentTriggerDate, nextAdpDate);
        nextAdpDate = disburseToSourceTrigger(accountId, disbToSource, nextAdpDate);

        disbInfo.put(DisburseKeys.NEXTDISBURSEMENTDATE.toString(), nextAdpDate);
        return disbToSource;
    }

    private String setAdcTrigger(Map<String, Object> disbInfo, String accountId, String disbToSource,
            Object[] maxReceiptInfo, Map<String, ZonedDateTime> currentTriggersMap, BilActRules bilActRules) {
        ZonedDateTime bilDtbDate = (ZonedDateTime) maxReceiptInfo[2];
        ZonedDateTime nextAdcDate = DateRoutine.getAdjustedDate(bilDtbDate, false, bilActRules.getBruDsbCrcrdDays(),
                null);
        if (nextAdcDate.compareTo(execContext.getApplicationDate()) < 0) {
            nextAdcDate = execContext.getApplicationDate();
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_EFT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_EFT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdcDate) == 0) {
                disbToSource = ActivityType.DISBURSE_EFT_CREDIT.getValue();
                updateActTriggerByTypeCode(accountId, currentTriggerType, currentTriggerDate);
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_CHECK_DRAFT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdcDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }

        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_CREDIT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CREDIT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdcDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_ACH.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_ACH.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdcDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.ONETIME_ACH.getValue())) {
            String currentTriggerType = ActivityType.ONETIME_ACH.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdcDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        ZonedDateTime currentTriggerDate = DateRoutine.defaultDateTime();
        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_EFT_CREDIT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_EFT_CREDIT.getValue();
            currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(DateRoutine.defaultDateTime()) != 0
                    && currentTriggerDate.compareTo(nextAdcDate) > 0) {
                updateDisbursementDate(accountId, DateRoutine.defaultDateTime(), nextAdcDate, currentTriggerDate);
                return disbToSource;
            }
        }

        disbToSource = ActivityType.DISBURSE_EFT_CREDIT.getValue();
        updateDisbursementDate(accountId, DateRoutine.defaultDateTime(), currentTriggerDate, nextAdcDate);
        nextAdcDate = disburseToSourceTrigger(accountId, disbToSource, nextAdcDate);

        disbInfo.put(DisburseKeys.NEXTDISBURSEMENTDATE.toString(), nextAdcDate);
        return disbToSource;
    }

    private String setAdeTrigger(Map<String, Object> disbInfo, String accountId, String disbToSource,
            Object[] maxReceiptInfo, Map<String, ZonedDateTime> currentTriggersMap, BilActRules bilActRules) {
        ZonedDateTime bilDtbDate = (ZonedDateTime) maxReceiptInfo[2];
        ZonedDateTime nextAdeDate = DateRoutine.getAdjustedDate(bilDtbDate, false, bilActRules.getBruDsbEftDays(),
                null);
        if (nextAdeDate.compareTo(execContext.getApplicationDate()) < 0) {
            nextAdeDate = execContext.getApplicationDate();
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_EFT_CREDIT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_EFT_CREDIT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdeDate) == 0) {
                disbToSource = ActivityType.DISBURSE_EFT.getValue();
                updateActTriggerByTypeCode(accountId, currentTriggerType, currentTriggerDate);
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_CHECK_DRAFT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdeDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_CREDIT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CREDIT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdeDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_ACH.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_ACH.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdeDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.ONETIME_ACH.getValue())) {
            String currentTriggerType = ActivityType.ONETIME_ACH.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAdeDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                disbToSource = setAddTrigger(disbInfo, accountId, disbToSource, maxReceiptInfo, currentTriggersMap,
                        bilActRules);
                return disbToSource;
            }
        }

        ZonedDateTime currentTriggerDate = DateRoutine.defaultDateTime();

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_EFT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_EFT.getValue();
            currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(DateRoutine.defaultDateTime()) != 0
                    && currentTriggerDate.compareTo(nextAdeDate) > 0) {
                updateDisbursementDate(accountId, DateRoutine.defaultDateTime(), nextAdeDate, currentTriggerDate);
                return disbToSource;
            }
        }

        disbToSource = ActivityType.DISBURSE_EFT.getValue();
        updateDisbursementDate(accountId, DateRoutine.defaultDateTime(), currentTriggerDate, nextAdeDate);
        nextAdeDate = disburseToSourceTrigger(accountId, disbToSource, nextAdeDate);

        disbInfo.put(DisburseKeys.NEXTDISBURSEMENTDATE.toString(), nextAdeDate);
        return disbToSource;
    }

    private String setAddTrigger(Map<String, Object> disbInfo, String accountId, String disbToSource,
            Object[] maxReceiptInfo, Map<String, ZonedDateTime> currentTriggersMap, BilActRules bilActRules) {
        ZonedDateTime bilDtbDate = (ZonedDateTime) maxReceiptInfo[2];
        boolean overrideAdoDateSwitch = (boolean) maxReceiptInfo[1];
        ZonedDateTime nextAddDate = DateRoutine.getAdjustedDate(bilDtbDate, false, bilActRules.getBruDsbCrcrdDays(),
                null);
        if (nextAddDate.compareTo(execContext.getApplicationDate()) < 0) {
            nextAddDate = execContext.getApplicationDate();
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_EFT_CREDIT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_EFT_CREDIT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAddDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                updateActTriggerByTypeCode(accountId, currentTriggerType, currentTriggerDate);
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_EFT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_EFT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAddDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                updateActTriggerByTypeCode(accountId, currentTriggerType, currentTriggerDate);
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_ACH.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_ACH.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAddDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                updateActTriggerByTypeCode(accountId, currentTriggerType, currentTriggerDate);
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.ONETIME_ACH.getValue())) {
            String currentTriggerType = ActivityType.ONETIME_ACH.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAddDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                updateActTriggerByTypeCode(accountId, currentTriggerType, currentTriggerDate);
            }
        }

        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_CREDIT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CREDIT.getValue();
            ZonedDateTime currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(nextAddDate) == 0) {
                disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                updateActTriggerByTypeCode(accountId, currentTriggerType, currentTriggerDate);
            }
        }

        ZonedDateTime currentTriggerDate = DateRoutine.defaultDateTime();
        if (currentTriggersMap.containsKey(ActivityType.DISBURSE_CHECK_DRAFT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
            currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            if (currentTriggerDate.compareTo(DateRoutine.defaultDateTime()) != 0
                    && currentTriggerDate.compareTo(nextAddDate) > 0) {
                updateDisbursementDate(accountId, DateRoutine.defaultDateTime(), nextAddDate, currentTriggerDate);
                return disbToSource;
            }
        }

        disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
        updateDisbursementDate(accountId, DateRoutine.defaultDateTime(), currentTriggerDate, nextAddDate);
        nextAddDate = disburseToSourceTrigger(accountId, disbToSource, nextAddDate);

        if (overrideAdoDateSwitch && currentTriggersMap.containsKey(ActivityType.DISBURSE_CREDIT.getValue())) {
            String currentTriggerType = ActivityType.DISBURSE_CREDIT.getValue();
            currentTriggerDate = currentTriggersMap.get(currentTriggerType);
            List<BilActInquiry> bilActInquiryList = bilActInquiryRepository.getBilActInquiryByTypeCode(accountId,
                    new ArrayList<>(Arrays.asList(ActivityType.DISBURSE_CREDIT.getValue())), currentTriggerDate);
            for (BilActInquiry bilActInquiry : bilActInquiryList) {
                bilActInquiry.setBilNxtAcyDt(DateRoutine.defaultDateTime());
                bilActInquiryRepository.saveAndFlush(bilActInquiry);
            }

            updateDisbursementDate(accountId, nextAddDate, nextAddDate, currentTriggerDate);
        }

        disbInfo.put(DisburseKeys.NEXTDISBURSEMENTDATE.toString(), nextAddDate);
        return disbToSource;
    }

    private ZonedDateTime disburseToSourceTrigger(String accountId, String disbToSourceType,
            ZonedDateTime disbToSourceDate) {
        if (disbToSourceDate.compareTo(execContext.getApplicationDate()) < 0) {
            disbToSourceDate = execContext.getApplicationDate();
        }

        BilActInquiry bilActInquiry = new BilActInquiry();
        BilActInquiryId bilActInquiryId = new BilActInquiryId(accountId, disbToSourceType);
        bilActInquiry.setBilActInquiryId(bilActInquiryId);
        bilActInquiry.setBilNxtAcyDt(disbToSourceDate);
        bilActInquiryRepository.saveAndFlush(bilActInquiry);

        List<String> bilAcyTypeCodeList = getBilAcyTypeCodeList();
        if (bilAcyTypeCodeList.indexOf(disbToSourceType) >= 0) {
            bilAcyTypeCodeList.remove(disbToSourceType);
        }
        List<BilActInquiry> bilActInquiryListUpdate = bilActInquiryRepository.getBilActInquiryByTypeCode(accountId,
                bilAcyTypeCodeList, disbToSourceDate);
        for (BilActInquiry bilActInquirySave : bilActInquiryListUpdate) {
            bilActInquirySave.setBilNxtAcyDt(DateRoutine.defaultDateTime());
            bilActInquiryRepository.saveAndFlush(bilActInquirySave);
        }

        return disbToSourceDate;
    }

    private void updateDisbursementDate(String accountId, ZonedDateTime firstDisbursementDate,
            ZonedDateTime secondDisbursementDate, ZonedDateTime currentTriggerDate) {
        List<BilCashDsp> bilCashDspList = bilCashDspRepository.getCreditCashIsExisting(accountId,
                BilDspTypeCode.SUSPENDED.getValue(), ManualSuspendIndicator.ENABLED.getValue(), getCreditIndList(),
                ReverseReSuspendIndicator.BLANK.getValue(), firstDisbursementDate, secondDisbursementDate);
        for (BilCashDsp bilCashDsp : bilCashDspList) {
            BilCashReceipt bilCashReceipt = bilCashReceiptRepository.findById(new BilCashReceiptId(accountId,
                    bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr()))
                    .orElse(null);
            if (bilCashReceipt != null) {
                bilCashDsp.setDisbursementDate(currentTriggerDate);
                bilCashDspRepository.saveAndFlush(bilCashDsp);
            }
        }
    }

    private void updateActTriggerByTypeCode(String accountId, String bilAcyType, ZonedDateTime nextAcyDate) {
        List<BilActInquiry> bilActInquiries = bilActInquiryRepository.getBilActInquiryByTypeCode(accountId,
                new ArrayList<>(Arrays.asList(bilAcyType)), nextAcyDate);
        for (BilActInquiry bilActInquiry : bilActInquiries) {
            bilActInquiry.setBilNxtAcyDt(DateRoutine.defaultDateTime());
            bilActInquiryRepository.saveAndFlush(bilActInquiry);
        }
    }

    private Map<String, ZonedDateTime> getCurrentTrigger(String accountId) {
        Map<String, ZonedDateTime> currentTriggersMap = new HashMap<>();
        List<String> bilAcyTypeCodeList = getBilAcyTypeCodeList();
        for (String bilAcyType : bilAcyTypeCodeList) {
            BilActInquiry bilActInquiry = bilActInquiryRepository.findById(new BilActInquiryId(accountId, bilAcyType))
                    .orElse(null);
            if (bilActInquiry != null && bilActInquiry.getBilNxtAcyDt().compareTo(DateRoutine.defaultDateTime()) != 0) {
                currentTriggersMap.put(bilAcyType, bilActInquiry.getBilNxtAcyDt());
            }
        }
        return currentTriggersMap;
    }

    private Object[] getMaxReceipt(String accountId, String disbToSource, String collectionPlan,
            ZonedDateTime effectiveDate, short dsbCrcrdDays, char disbPayeeCd) {
        boolean isOverrideAdoDate = false;
        char transferIndicator = BLANK_CHAR;
        char disbTempIndicator = BLANK_CHAR;
        double onetimeCcAmount = DECIMAL_ZERO;
        ZonedDateTime bilDtbDate = DateRoutine.defaultDateTime();
        ZonedDateTime maxBilDtbDate = DateRoutine.defaultDateTime();
        short maxBilDtbSequenceNbr = SHORT_ZERO;
        boolean endOfCursor = false;

        List<BilCashReceipt> bilCashReceiptList = bilCashReceiptRepository.getCreditCashRow(accountId, CREDIT_COMMENT);
        for (BilCashReceipt bilCashReceipt : bilCashReceiptList) {

            if (bilDtbDate == null || bilDtbDate.compareTo(bilCashReceipt.getBilCashReceiptId().getBilDtbDate()) != 0) {
                bilDtbDate = bilCashReceipt.getBilCashReceiptId().getBilDtbDate();
            }

            if (disbPayeeCd == OVERRIDE_PAYEE_RULES) {
                break;
            }
            if (bilCashReceipt.getCashEntryMethodCd() == CashEntryMethod.BANK_ACCOUNT.getCashEntryMethod()
                    && bilCashReceipt.getCashEntryMethodCd() == CashEntryMethod.AGENCY_SWEEP.getCashEntryMethod()
                    && checkEftPending(accountId, bilCashReceipt)) {
                continue;
            }

            Object[] adoInfo = getAdoTriggerInfo(accountId, dsbCrcrdDays);
            Double transferAmount = bilCashDspRepository.getTransferDspAmount1(accountId,
                    BilDspTypeCode.TRANSFERRED.getValue(), bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                    bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), getCreditIndList(), CREDIT_COMMENT);
            if (transferAmount == null || transferAmount == DECIMAL_ZERO) {
                endOfCursor = true;
                transferIndicator = CHAR_N;
            } else if (transferAmount == bilCashReceipt.getReceiptAmount()) {
                transferIndicator = CHAR_T;
            } else if (transferAmount < bilCashReceipt.getReceiptAmount()) {
                onetimeCcAmount = bilCashReceipt.getReceiptAmount() - transferAmount;
                transferIndicator = CHAR_P;
            }

            if (!disbToSource.equals(ActivityType.DISBURSE_EFT.getValue())
                    && !disbToSource.equals(ActivityType.DISBURSE_EFT_CREDIT.getValue())) {
                disbTempIndicator = bilCashReceipt.getCashEntryMethodCd();
            }
            disbToSource = adjustSourceValueByMethodCode(disbToSource, disbTempIndicator, collectionPlan,
                    effectiveDate);
            if (disbToSource.equalsIgnoreCase(ActivityType.DISBURSE_CREDIT.getValue())) {
                List<Character> cashEntryMethodList = new ArrayList<>();
                cashEntryMethodList.add(CashEntryMethod.CREDIT_CARD.getCashEntryMethod());
                List<String> busCodeList = busCdTranslationRepository.findCodeListByTypeList(
                        Arrays.asList(BusCodeTranslationType.DIGITAL_WALLET_TYPE.getValue()),
                        execContext.getLanguage());
                if (!busCodeList.isEmpty()) {
                    busCodeList.forEach(busCode -> cashEntryMethodList.add(busCode.charAt(0)));
                }

                List<BilCashReceipt> bilCashReceiptList1 = bilCashReceiptRepository.getMaxBilDtbDateRow(accountId,
                        cashEntryMethodList);
                if (bilCashReceiptList != null && !bilCashReceiptList.isEmpty()) {
                    maxBilDtbDate = bilCashReceiptList1.get(SHORT_ZERO).getBilCashReceiptId().getBilDtbDate();
                    maxBilDtbSequenceNbr = bilCashReceiptList1.get(SHORT_ZERO).getBilCashReceiptId()
                            .getDtbSequenceNbr();
                    double bilReceiptAmount = bilCashReceiptList1.get(SHORT_ZERO).getReceiptAmount();

                    Double bilDspAmount = bilCashDspRepository.getSuspendDspAmountIsExisting(accountId,
                            BilDspTypeCode.SUSPENDED.getValue(), ManualSuspendIndicator.ENABLED.getValue(),
                            getCreditIndList(), ReverseReSuspendIndicator.BLANK.getValue(),
                            DateRoutine.defaultDateTime(), (ZonedDateTime) adoInfo[1], CREDIT_COMMENT);
                    if (bilDspAmount != null && bilDspAmount > onetimeCcAmount && onetimeCcAmount != DECIMAL_ZERO
                            || onetimeCcAmount > bilReceiptAmount
                            || bilDspAmount != null && bilDspAmount > bilReceiptAmount
                            || (Double) adoInfo[0] > bilReceiptAmount) {
                        disbToSource = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
                        isOverrideAdoDate = true;
                    }

                }
            }

            if (endOfCursor || transferIndicator == CHAR_N) {
                break;
            }
        }
        return new Object[] { disbToSource, isOverrideAdoDate, bilDtbDate, maxBilDtbDate, maxBilDtbSequenceNbr };
    }

    private String adjustSourceValueByMethodCode(String disbToSourceIndicator, char cashEntryMethod,
            String collectionPlan, ZonedDateTime effectiveDate) {

        switch (CashEntryMethod.getEnumValue(cashEntryMethod)) {
        case CREDIT_CARD:
            disbToSourceIndicator = ActivityType.DISBURSE_CREDIT.getValue();
            break;
        case ONE_TIME_ACH:
            disbToSourceIndicator = ActivityType.DISBURSE_ACH.getValue();
            break;
        case BANK_ACCOUNT:
            disbToSourceIndicator = ActivityType.ONETIME_ACH.getValue();
            break;
        case AGENCY_SWEEP:
        case AGENT_ACH:
        case INSURED:
        case ONLINE_ENTRY:
        case LOCKBOX:
        case CREDIT:
        case AGENT_CREDIT_CARD:
        case VENDOR_COLLECTIONS:
            disbToSourceIndicator = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
            break;
        case EFT:
        case EFT_CREDIT_CARD:
            if (collectionPlan.trim().isEmpty() || effectiveDate == null
                    || effectiveDate.compareTo(DateRoutine.defaultDateTime()) == 0) {
                disbToSourceIndicator = ActivityType.DISBURSE_CHECK_DRAFT.getValue();
            }
            break;
        default:
            if (isDigitalWalletPayment(cashEntryMethod)) {
                disbToSourceIndicator = ActivityType.DISBURSE_CREDIT.getValue();
            }
            break;
        }
        return disbToSourceIndicator;
    }

    private Object[] getAdoTriggerInfo(String accountId, short dsbCrcrdDays) {
        Double existAdoAmount = DECIMAL_ZERO;
        ZonedDateTime existAdoDate = DateRoutine.defaultDateTime();
        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(accountId, ActivityType.DISBURSE_CREDIT.getValue())).orElse(null);
        if (bilActInquiry != null && bilActInquiry.getBilNxtAcyDt().compareTo(DateRoutine.defaultDateTime()) != 0) {
            existAdoDate = bilActInquiry.getBilNxtAcyDt();
            existAdoAmount = bilCashDspRepository.getSuspendDspAmountIsExisting(accountId,
                    BilDspTypeCode.SUSPENDED.getValue(), ManualSuspendIndicator.ENABLED.getValue(), getCreditIndList(),
                    ReverseReSuspendIndicator.BLANK.getValue(), DateRoutine.defaultDateTime(), existAdoDate,
                    CREDIT_COMMENT);
            ZonedDateTime differenceDate = DateRoutine.getAdjustedDate(execContext.getApplicationDate(), false,
                    dsbCrcrdDays, null);
            if (differenceDate.compareTo(existAdoDate) != 0) {
                existAdoDate = DateRoutine.defaultDateTime();
            }
        }
        return new Object[] { existAdoAmount, existAdoDate };
    }

    private boolean checkEftPending(String accountId, BilCashReceipt bilCashReceipt) {
        ZonedDateTime isExisting = bilEftPendingTapeRepository.getTimeStamp(accountId,
                bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr());
        return isExisting != null;
    }

    private void updateBilActNextAcyDateByDisbDate(String accountId, ZonedDateTime nextDisbDate) {
        List<BilActInquiry> bilActInquiryList = bilActInquiryRepository.getSuspendDisbRow(accountId,
                getBilAcyTypeCodeList(), execContext.getApplicationDate());
        for (BilActInquiry bilActInquiry : bilActInquiryList) {
            bilActInquiry.setBilNxtAcyDt(nextDisbDate);
            bilActInquiryRepository.saveAndFlush(bilActInquiry);
        }
    }

    private void updateNextDisbDateToDefault(String accountId, ZonedDateTime nextDisbDate) {
        List<BilCashDsp> bilCashDspList = bilCashDspRepository.getFirstDisburToSourceRow(accountId,
                BilDspTypeCode.SUSPENDED.getValue(), ManualSuspendIndicator.ENABLED.getValue(), getCreditIndList(),
                ReverseReSuspendIndicator.BLANK.getValue());
        for (BilCashDsp bilCashDsp : bilCashDspList) {
            if (bilCashReceiptRepository.existsById(new BilCashReceiptId(accountId,
                    bilCashDsp.getBilCashDspId().getBilDtbDate(), bilCashDsp.getBilCashDspId().getDtbSequenceNbr()))) {
                bilCashDsp.setDisbursementDate(nextDisbDate);
                bilCashDspRepository.saveAndFlush(bilCashDsp);
            }
        }
    }

    private void updateBilActNextAcyDate(String accountId, ZonedDateTime nextDisbDate) {

        bilActInquiryRepository.updateExistTrigger(nextDisbDate, accountId, getBilAcyTypeCodeList());
    }

    private ZonedDateTime getAutomaticCashWriteOffDate(List<BilCashDsp> bilCashDspList, String billTypeCd,
            String billClassCd, ZonedDateTime nextAcwDate) {
        Short arwoDays = null;
        String holdBilPlanCode = BLANK_STRING;
        for (BilCashDsp bilCashDsp : bilCashDspList) {
            BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository
                    .findById(new BilPolicyTermId(bilCashDsp.getBilCashDspId().getAccountId(),
                            bilCashDsp.getCreditPolicyId(), bilCashDsp.getPolicyEffectiveDate()))
                    .orElse(null);
            if (bilPolicyTerm == null) {
                throw new DataNotFoundException("policy.notfound");
            }
            if (!bilPolicyTerm.getBillPlanCd().trim().equalsIgnoreCase(holdBilPlanCode)) {

                if (arwoDays == null) {
                    arwoDays = readArwoRuleCheck(billTypeCd, billClassCd, bilPolicyTerm.getBillPlanCd());
                }
                ZonedDateTime arwoMaxDate = getArwoInfo(bilCashDsp, bilPolicyTerm, arwoDays);
                if (arwoMaxDate.compareTo(nextAcwDate) < 0) {
                    nextAcwDate = arwoMaxDate;
                    holdBilPlanCode = bilPolicyTerm.getBillPlanCd();
                }
            }
        }
        return nextAcwDate;
    }

    private ZonedDateTime getArwoInfo(BilCashDsp bilCashDsp, BilPolicyTerm bilPolicyTerm, Short arwoDays) {

        ZonedDateTime arwoPolicyDate = DateRoutine.defaultDateTime();
        ZonedDateTime minDspDate = bilCashDspRepository.getMinDspDateByDisbToSource(
                bilCashDsp.getBilCashDspId().getAccountId(), BilDspTypeCode.SUSPENDED.getValue(), getManualCodeList(),
                bilPolicyTerm.getBillPolicyTermId().getPolicyId(), bilCashDsp.getPolicyEffectiveDate(),
                getCreditIndList(), ReverseReSuspendIndicator.BLANK.getValue(), CREDIT_COMMENT);

        if (minDspDate != null) {
            arwoPolicyDate = DateRoutine.getAdjustedDate(minDspDate, false, arwoDays, null);
        }
        return arwoPolicyDate;
    }

    @Override
    public Boolean overrideWkendInd(char excludeWkendIndicator, char includeWkendIndicator) {
        if (excludeWkendIndicator == CHAR_Y && includeWkendIndicator == CHAR_Y) {
            excludeWkendIndicator = CHAR_N;
        }
        return excludeWkendIndicator == CHAR_Y;
    }

    @Override
    public Map<String, Object> paymentDisburse(BilAccount bilAccount, BilSupportPlan bilSupportPlan,
            Map<String, Object> nextDisburseDates) {

        boolean isCashInSuspend = (boolean) nextDisburseDates.get(DisburseKeys.CASHINSUSPEND.toString());
        boolean isAccountLvlDisb = (boolean) nextDisburseDates.get(DisburseKeys.ACCOUNTLVLDISB.toString());
        ZonedDateTime nextAcwDate = (ZonedDateTime) nextDisburseDates.get(DisburseKeys.NEXTACWDATE.toString());
        ZonedDateTime nextDisbDate = (ZonedDateTime) nextDisburseDates
                .get(DisburseKeys.NEXTDISBURSEMENTDATE.toString());
        String accountId = bilAccount.getAccountId();

        Double suspendDspAmount = bilCashDspRepository.getSuspendPaymentDspAmount(accountId,
                BilDspTypeCode.SUSPENDED.getValue(), ManualSuspendIndicator.ENABLED.getValue(),
                ReverseReSuspendIndicator.BLANK.getValue(), CREDIT_COMMENT);
        if (suspendDspAmount != null && suspendDspAmount != DECIMAL_ZERO) {
            isCashInSuspend = true;
            suspendDspAmount = bilCashDspRepository.getCashEnteredByAnOperator(accountId,
                    BilDspTypeCode.SUSPENDED.getValue(), getManualCodeList(), getPaymentCreditInd(),
                    ReverseReSuspendIndicator.BLANK.getValue(), CREDIT_COMMENT);
            if (suspendDspAmount == null || suspendDspAmount > bilSupportPlan.getBspOvpMaxDsb()) {
                if (suspendDspAmount == null) {
                    nextAcwDate = DateRoutine.defaultDateTime();
                }
                Short arwoDays = readArwoRuleCheck(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                        BLANK_STRING);
                if (arwoDays != null) {
                    ZonedDateTime arwoMaxDate = getArwoInfo(accountId, arwoDays);

                    if (arwoMaxDate.compareTo(nextAcwDate) < 0) {
                        nextAcwDate = arwoMaxDate;
                    }
                }

            } else {
                ZonedDateTime minDisburseDate = bilCashDspRepository.getMinDisburseDate(accountId,
                        BilDspTypeCode.SUSPENDED.getValue(), getManualCodeList(), getPaymentCreditInd(),
                        ReverseReSuspendIndicator.BLANK.getValue(), bilSupportPlan.getBspOvpMaxDsb(), CREDIT_COMMENT);

                if (minDisburseDate != null) {
                    isAccountLvlDisb = true;
                    ZonedDateTime differenceDate = DateRoutine.getAdjustedDate(minDisburseDate, false,
                            bilSupportPlan.getBspOvpHoldNbr(), null);
                    if (differenceDate.compareTo(nextDisbDate) < 0) {
                        nextDisbDate = differenceDate;
                    }
                }
            }
        } else {
            nextAcwDate = DateRoutine.defaultDateTime();
            nextDisbDate = DateRoutine.defaultDateTime();
        }

        nextDisburseDates.put(DisburseKeys.NEXTDISBURSEMENTDATE.toString(), nextDisbDate);
        nextDisburseDates.put(DisburseKeys.NEXTACWDATE.toString(), nextAcwDate);
        nextDisburseDates.put(DisburseKeys.CASHINSUSPEND.toString(), isCashInSuspend);
        nextDisburseDates.put(DisburseKeys.ACCOUNTLVLDISB.toString(), isAccountLvlDisb);
        return nextDisburseDates;
    }

    @Override
    public Short readArwoRuleCheck(String billTypeCd, String billClassCd, String planCode) {

        Short arwoDays = null;
        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("ARWO", billTypeCd, billClassCd, planCode);

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            int length = bilRulesUct.getBrtParmListTxt().trim().length();
            if (length >= 3) {
                String arwoDateParam = bilRulesUct.getBrtParmListTxt().substring(3, 6);
                arwoDays = Short.valueOf(arwoDateParam);
            }
        }

        return arwoDays;
    }

    private ZonedDateTime getArwoInfo(String accountId, Short arwoDays) {
        ZonedDateTime arwoDate = DateRoutine.defaultDateTime();
        ZonedDateTime minCashEnteredDate = bilCashDspRepository.getMinCashEnteredDate(accountId,
                BilDspTypeCode.SUSPENDED.getValue(), getManualCodeList(), getManualCodeList(),
                ReverseReSuspendIndicator.BLANK.getValue(), CREDIT_COMMENT);
        if (minCashEnteredDate != null) {
            arwoDate = DateRoutine.getAdjustedDate(minCashEnteredDate, false, arwoDays, null);
        }
        return arwoDate;
    }

    @Override
    public ZonedDateTime getNonComplianceDate(String accountId, BilSupportPlan bilSupportPlan) {
        ZonedDateTime nextNoncpiDate;
        ZonedDateTime bilAdjDueDate = bilIstScheduleRepository.getMaxAdjDueDt(accountId,
                Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()));
        if (bilAdjDueDate == null) {
            nextNoncpiDate = DateRoutine.defaultDateTime();
            return nextNoncpiDate;
        }
        nextNoncpiDate = bilDatesRepository.getdMinAdjDueDate(accountId,
                InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(), bilAdjDueDate, CHAR_X);
        if (nextNoncpiDate == null) {
            nextNoncpiDate = DateRoutine.defaultDateTime();
            return nextNoncpiDate;
        }
        nextNoncpiDate = DateRoutine.adjustDateWithOffset(nextNoncpiDate, false, bilSupportPlan.getBspNoncpiLetNbr(),
                PLUS_SIGN, null);
        return nextNoncpiDate;
    }

    @Override
    public Boolean checkForRenewDownpay(String accountId) {
        boolean invoiceRequireSwitch = true;
        List<BilPolActivity> bilPolActivities = bilPolActivityRepository.fetchBilPolActivityByNextAcyDate(accountId,
                "RDP", DateRoutine.defaultDateTime());
        if (bilPolActivities == null || bilPolActivities.isEmpty()) {
            invoiceRequireSwitch = false;
        }
        return invoiceRequireSwitch;
    }

    @Override
    public ZonedDateTime checkForSecondNotice(String accountId, char suspendBillIndicator, char driverIndicator,
            char eftCollectionIndicator, short secondNoticeNumber) {
        ZonedDateTime secondNoticeDate = DateRoutine.defaultDateTime();
        if (suspendBillIndicator != CHAR_Y) {
            switch (driverIndicator) {
            case SECOND_NOTICE_NO:
                secondNoticeDate = DateRoutine.defaultDateTime();
                break;
            case SECOND_NOTICE_OFF:
                secondNoticeDate = getSecondNoticeDate(accountId);
                break;
            default:
                ZonedDateTime secondNoticeInvoiceDate = null;
                secondNoticeDate = DateRoutine.defaultDateTime();
                String maxPolicy = bilPolicyTermRepository.getMaxPolicyIdForPayee(accountId, getPolStatusCodeList());
                if (maxPolicy != null) {
                    label1: {
                        secondNoticeInvoiceDate = bilDatesRepository.getMaxNoticeInvDate(accountId,
                                InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(), getNoncpiIndList(), CHAR_X);
                        if (secondNoticeInvoiceDate == null) {
                            break label1;
                        }
                        ZonedDateTime bilAcyDate = bilActSummaryRepository.getMaxNoticeBilAcyDate(accountId,
                                BilDesReasonCode.SECOND_NOTICE.getValue(), secondNoticeInvoiceDate);
                        if (bilAcyDate != null) {
                            break label1;
                        }
                        if (eftCollectionIndicator == EFT) {
                            bilAcyDate = bilActSummaryRepository.getMaxNoticeBilAcyDate(accountId,
                                    BilDesReasonCode.RPRINT.getValue(), secondNoticeInvoiceDate);
                            if (bilAcyDate == null) {
                                break label1;
                            }
                        }
                        secondNoticeDate = bilDatesRepository.getMaxNoticeDueDate(accountId,
                                InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(), getNoncpiIndList(), CHAR_X);
                        if (secondNoticeDate == null) {
                            secondNoticeDate = DateRoutine.defaultDateTime();
                        } else {
                            secondNoticeDate = DateRoutine.getAdjustedDate(secondNoticeDate, false, secondNoticeNumber,
                                    null);
                        }
                    }
                }
                break;
            }
        }
        return secondNoticeDate;
    }

    private ZonedDateTime getSecondNoticeDate(String accountId) {
        ZonedDateTime secondNoticeDate = DateRoutine.defaultDateTime();
        BilActInquiry bilActInquiry = bilActInquiryRepository
                .findById(new BilActInquiryId(accountId, BilDesReasonCode.SECOND_NOTICE.getValue())).orElse(null);
        if (bilActInquiry != null) {
            secondNoticeDate = bilActInquiry.getBilNxtAcyDt();
        }
        return secondNoticeDate;
    }

    private List<Character> getNoncpiIndList() {
        List<Character> noncpiIndList = new ArrayList<>();
        noncpiIndList.add(CHAR_N);
        noncpiIndList.add(CHAR_Y);
        return noncpiIndList;
    }

    private List<Character> getPolStatusCodeList() {
        List<Character> polStatusCodeList = new ArrayList<>();
        polStatusCodeList.add(BilPolicyStatusCode.PENDING_TRANSFER.getValue());
        polStatusCodeList.add(BilPolicyStatusCode.CANCELLED_FOR_REWRITE.getValue());
        polStatusCodeList.add(BilPolicyStatusCode.BILL_FOLLOWUP_SUSPEND.getValue());
        polStatusCodeList.add(BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue());
        polStatusCodeList.add(BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue());
        polStatusCodeList.add(BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue());
        polStatusCodeList.add(BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue());
        polStatusCodeList.add(BilPolicyStatusCode.PENDING_TRANSFER_APPLYPAYMENTS.getValue());
        polStatusCodeList.add(BilPolicyStatusCode.PENDING_TRANSFER_HOLDPAYMENTS.getValue());
        polStatusCodeList.add(BilPolicyStatusCode.TRANSFERRED.getValue());
        polStatusCodeList.add('3');
        polStatusCodeList.add('4');
        return polStatusCodeList;
    }

    private List<String> getBilAcyTypeCodeList() {
        List<String> bilAcyTypeList = new ArrayList<>();
        bilAcyTypeList.add(ActivityType.ONETIME_ACH.getValue());
        bilAcyTypeList.add(ActivityType.DISBURSE_EFT_CREDIT.getValue());
        bilAcyTypeList.add(ActivityType.DISBURSE_EFT.getValue());
        bilAcyTypeList.add(ActivityType.DISBURSE_CHECK_DRAFT.getValue());
        bilAcyTypeList.add(ActivityType.DISBURSE_CREDIT.getValue());
        bilAcyTypeList.add(ActivityType.DISBURSE_ACH.getValue());
        return bilAcyTypeList;
    }

    private List<Character> getManualCodeList() {
        List<Character> manualCodeList = new ArrayList<>();
        manualCodeList.add(ManualSuspendIndicator.DISABLED.getValue());
        manualCodeList.add(ManualSuspendIndicator.BLANK.getValue());
        return manualCodeList;
    }

    private List<Character> getCreditIndList() {
        List<Character> creditIndList = new ArrayList<>();
        creditIndList.add(CreditIndicator.UNIDENTIFIED_CASH.getValue());
        creditIndList.add(CreditIndicator.DISBURSED_CASH.getValue());
        return creditIndList;
    }

    private List<Character> getPaymentCreditInd() {
        List<Character> creditIndicatorList = new ArrayList<>();
        creditIndicatorList.add(CreditIndicator.OPERATOR.getValue());
        creditIndicatorList.add(CHAR_S);
        creditIndicatorList.add(BLANK_CHAR);
        return creditIndicatorList;
    }

    public enum DisburseKeys {
        NEXTACWDATE("NextAcwDt"),
        SOURCETRANSFERTYPE("DisburType"),
        NEXTDISBURSEMENTDATE("DisburDate"),
        BILDTBDATE("BilDtbDate"),
        BILDTBSEQUENCE("BilDtbSequence"),
        CASHINSUSPEND("CashInSuspend"),
        ACCOUNTLVLDISB("AccountLvlDisb");

        private final String key;

        private DisburseKeys(String key) {
            this.key = key;
        }

        public boolean hasValue(String keyValue) {
            return keyValue != null && key.equals(keyValue);
        }

        @Override
        public String toString() {
            return this.key;
        }

        public static DisburseKeys getEnumKey(String keyValue) {
            for (DisburseKeys disburseKeys : values()) {
                if (disburseKeys.key.equals(keyValue)) {
                    return disburseKeys;
                }
            }
            return null;
        }
    }

    private boolean isDigitalWalletPayment(Character cashEntryMethodCode) {
        BusCdTranslation busCdTranslation = busCdTranslationRepository.findByCodeAndType(
                String.valueOf(cashEntryMethodCode), BusCodeTranslationType.DIGITAL_WALLET_TYPE.getValue(),
                execContext.getLanguage());

        return busCdTranslation != null ? Boolean.TRUE : Boolean.FALSE;
    }
}
