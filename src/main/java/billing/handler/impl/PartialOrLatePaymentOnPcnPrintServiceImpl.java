package billing.handler.impl;

import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilMstCoSt;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilMstCoStRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.handler.model.PartialOrLatePaymentOnPcnPrint;
import billing.handler.service.PartialOrLatePaymentOnPcnPrintService;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.ChargeType;
import billing.utils.BillingConstants.ChargeTypeCodes;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.PB360Service;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.schedule.service.ScheduleService;
import core.utils.DateRoutine;

@Service
public class PartialOrLatePaymentOnPcnPrintServiceImpl implements PartialOrLatePaymentOnPcnPrintService {

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilMstCoStRepository bilMstCoStRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private PB360Service pb360Service;

    @Autowired
    private DateService dateService;

    private static final String NO_DATA_FOUND = "no.data.found";

    @Override
    public void processPartialOrLatePaymentOnPcnPrint(PartialOrLatePaymentOnPcnPrint partialOrLatePaymentOnPcnPrint) {

        ZonedDateTime pcnDate = null;
        Double pcnAmount = null;

        BilPolicyTerm bilPolicyTerm = readBilPolicyTerm(partialOrLatePaymentOnPcnPrint);
        if (isRescission(bilPolicyTerm.getBillPolStatusCd())) {
            BilAccount bilAccount = readBilAccountData(partialOrLatePaymentOnPcnPrint.getAccountId());
            BilPolicy bilPolicy = readBilPolicy(partialOrLatePaymentOnPcnPrint);
            BilActSummary bilActSummary = readBilActSummay(bilPolicyTerm, bilPolicy, bilAccount);

            char pnrdRuleCd = getPnrRuleCode(bilAccount, bilActSummary,
                    partialOrLatePaymentOnPcnPrint.getNoRescindDate());
            if (pnrdRuleCd != CHAR_N) {
                boolean suppressPrintLetter = checkSuppressPrintLetter(bilAccount, bilPolicyTerm, bilActSummary,
                        bilPolicy, partialOrLatePaymentOnPcnPrint.getReasonCode());
                if (suppressPrintLetter) {
                    return;
                }

                if (bilActSummary.getBilAcyDes2Dt().compareTo(DateRoutine.defaultDateTime()) == 0) {
                    reschedulePartialOrLatePayment(partialOrLatePaymentOnPcnPrint);
                } else {
                    Object[] pcnDataObject = calculatePcnData(bilPolicyTerm, bilPolicy, bilAccount);
                    if (pcnDataObject.length == 0) {
                        pcnDate = bilActSummary.getBillActSummaryId().getBilAcyDt();
                        pcnAmount = bilActSummary.getBilAcyAmt();
                    } else {
                        pcnDate = (ZonedDateTime) pcnDataObject[0];
                        if ((Double) pcnDataObject[1] > DECIMAL_ZERO) {
                            pcnAmount = (Double) pcnDataObject[1];
                        } else {
                            pcnAmount = bilActSummary.getBilAcyAmt();
                        }
                    }
                    Double policyBalance = calculatePriorPolicyBalance(bilPolicyTerm, bilPolicy, bilAccount);
                    Double accessedAmount = getAccessedAmount(bilAccount, pcnDate);
                    Double invoiceFeeAmount = getInvoiceFeeAmount(bilAccount, pcnDate);
                    buildPrintRequest(partialOrLatePaymentOnPcnPrint, bilPolicyTerm, bilActSummary, policyBalance,
                            accessedAmount, invoiceFeeAmount, pcnAmount, bilAccount);
                    insertBilAccountSummary(bilActSummary.getBillActSummaryId().getBilAccountId(),
                            DateRoutine.defaultDateTime(), bilActSummary.getBilAcyAmt(), "PPC",
                            partialOrLatePaymentOnPcnPrint.getNoRescindDate(), SEPARATOR_BLANK, "SYSTEM", bilPolicy.getPolNbr(),
                            bilPolicy.getPolSymbolCd(), DateRoutine.defaultDateTime(),
                            bilActSummary.getBasAddDataTxt());
                }

            }
        }

    }

    private void reschedulePartialOrLatePayment(PartialOrLatePaymentOnPcnPrint partialOrLatePaymentOnPcnPrint) {
        String parmListData = partialOrLatePaymentOnPcnPrint.getAccountId();
        parmListData += StringUtils.rightPad(partialOrLatePaymentOnPcnPrint.getPolicyId(), 16, BLANK_STRING);
        parmListData += StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(partialOrLatePaymentOnPcnPrint.getEffectiveDate()), 10,
                BLANK_STRING);
        parmListData += StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(partialOrLatePaymentOnPcnPrint.getExpirationDate()), 10,
                BLANK_STRING);
        parmListData += "01";
        parmListData += StringUtils
                .leftPad(String.valueOf(partialOrLatePaymentOnPcnPrint.getPastDueAmount()).replace(".", ""), 14, '0');
        parmListData += StringUtils.rightPad(String.valueOf(partialOrLatePaymentOnPcnPrint.getReasonCode()), 1,
                BLANK_STRING);
        parmListData += StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(partialOrLatePaymentOnPcnPrint.getNoRescindDate()), 10,
                BLANK_STRING);
        if (partialOrLatePaymentOnPcnPrint.getReceiptAmount() == DECIMAL_ZERO) {
            parmListData += StringUtils.leftPad(BLANK_STRING, 8, BLANK_STRING);
        } else {
            parmListData += StringUtils.leftPad(
                    String.valueOf(partialOrLatePaymentOnPcnPrint.getReceiptAmount()).replace(".", ""), 8, '0');
        }
        parmListData += partialOrLatePaymentOnPcnPrint.getStaticReviewIndicator();
        parmListData = StringUtils.rightPad(parmListData, 76);

        scheduleService.scheduleDeferActivity("BCMOPPC", parmListData);
    }

    private void buildPrintRequest(PartialOrLatePaymentOnPcnPrint partialOrLatePaymentOnPcnPrint,
            BilPolicyTerm bilPolicyTerm, BilActSummary bilActSummary, Double policyBalance, Double accessedAmount,
            Double invoiceFeeAmount, Double pcnAmount, BilAccount bilAccount) {
        String masterCompanyNumber;
        String riskState;
        if (partialOrLatePaymentOnPcnPrint.getPolicyId().trim().isEmpty()
                || bilPolicyTerm.getBptAgreementInd() == CHAR_Y) {
            String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(), bilAccount.getBillTypeCd());
            masterCompanyNumber = bilmasterCompanyState[0];
            riskState = bilmasterCompanyState[1];
        } else {
            masterCompanyNumber = bilPolicyTerm.getMasterCompanyNbr();
            riskState = bilPolicyTerm.getBillStatePvnCd();
        }
        String additionalKey = buildAdditionalKeyText(bilAccount.getAccountNumber(), masterCompanyNumber, riskState,
                partialOrLatePaymentOnPcnPrint.getReasonCode());
        pb360Service.postGeneratePrintNoticeRequest(partialOrLatePaymentOnPcnPrint.getAccountId(), "BCMOPPC",
                bilAccount.getPayorClientId(), "BCMS1003", additionalKey, "PARTIAL/LATE PAY ON PND CANC");

    }

    private String[] readBilMstCoSt(String bilClassCode, String bilTypeCode) {
        String masterCompany = BillingConstants.MASTER_COMPANY_NUMBER;
        String riskState = BLANK_STRING;
        BilMstCoSt bilMstCoSt = bilMstCoStRepository.findRow(bilClassCode, bilTypeCode, BLANK_STRING);
        if (bilMstCoSt != null) {
            masterCompany = bilMstCoSt.getMasterCompanyNbr();
            riskState = bilMstCoSt.getBilStatePvnCd();
        }
        return new String[] { masterCompany, riskState };
    }

    private String buildAdditionalKeyText(String accountNumber, String masterCompanyNumber,
            String polPrintRiskStateCode, char reasonCode) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(CHAR_A);
        stringBuilder
                .append(StringUtils.left(DateRoutine.dateTimeAsYYYYMMDDString(dateService.currentDate()), 10));
        stringBuilder.append("01");
        stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 20, SEPARATOR_BLANK));
        stringBuilder.append(StringUtils.rightPad(accountNumber, 30, SEPARATOR_BLANK));
        stringBuilder.append(StringUtils.rightPad(masterCompanyNumber, 2, SEPARATOR_BLANK));
        stringBuilder.append(StringUtils.rightPad(polPrintRiskStateCode, 3, SEPARATOR_BLANK));
        stringBuilder.append(reasonCode);
        return stringBuilder.toString();
    }

    private BilPolicyTerm readBilPolicyTerm(PartialOrLatePaymentOnPcnPrint partialOrLatePaymentOnPcnPrint) {

        BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository
                .findById(new BilPolicyTermId(partialOrLatePaymentOnPcnPrint.getAccountId(),
                        partialOrLatePaymentOnPcnPrint.getPolicyId(),
                        partialOrLatePaymentOnPcnPrint.getEffectiveDate()))
                .orElse(null);
        if (bilPolicyTerm == null) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }
        return bilPolicyTerm;
    }

    private BilPolicy readBilPolicy(PartialOrLatePaymentOnPcnPrint partialOrLatePaymentOnPcnPrint) {
        BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(partialOrLatePaymentOnPcnPrint.getPolicyId(),
                partialOrLatePaymentOnPcnPrint.getAccountId())).orElse(null);
        if (bilPolicy == null) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }
        return bilPolicy;
    }

    private BilAccount readBilAccountData(String accountId) {
        BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
        if (bilAccount == null) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }
        return bilAccount;
    }

    private boolean checkSuppressPrintLetter(BilAccount bilAccount, BilPolicyTerm bilPolicyTerm,
            BilActSummary bilActSummary, BilPolicy bilPolicy, char reasonCode) {
        boolean suppressLetter = false;
        short offsetDays;

        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("PPCS", bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), bilPolicyTerm.getBillPlanCd());
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
            short cancelDateOffset = (short) Integer.parseInt(bilRulesUct.getBrtParmListTxt().substring(0, 4).trim());
            boolean isWriteSummaryRow = bilRulesUct.getBrtParmListTxt().charAt(4) == CHAR_Y;
            boolean rdcSuppress = bilRulesUct.getBrtParmListTxt().charAt(5) == CHAR_Y;
            boolean rdrSuppress = bilRulesUct.getBrtParmListTxt().charAt(6) == CHAR_Y;
            boolean rdbSuppress = bilRulesUct.getBrtParmListTxt().charAt(7) == CHAR_Y;
            boolean rdpSuppress = bilRulesUct.getBrtParmListTxt().charAt(8) == CHAR_Y;
            if (reasonCode == 'C' && rdcSuppress || reasonCode == 'R' && rdrSuppress || reasonCode == 'B' && rdbSuppress
                    || reasonCode == 'P' && rdpSuppress) {
                suppressLetter = true;
            } else {
                if (!bilActSummary.getBasAddDataTxt().trim().isEmpty()) {
                    int ldnLegalDays = Integer.parseInt(bilActSummary.getBasAddDataTxt().substring(52, 56));
                    int ldnMailDays = Integer.parseInt(bilActSummary.getBasAddDataTxt().substring(56, 60));
                    offsetDays = (short) (cancelDateOffset + ldnLegalDays + ldnMailDays);
                } else {
                    offsetDays = cancelDateOffset;
                }
                ZonedDateTime toleranceDate = DateRoutine.adjustDateWithOffset(
                        bilActSummary.getBillActSummaryId().getBilAcyDt(), false, offsetDays, '+', null);
                if (dateService.currentDate().compareTo(toleranceDate) >= 0) {
                    suppressLetter = true;
                }
            }

            if (suppressLetter) {
                if (isWriteSummaryRow) {
                    insertBilAccountSummary(bilActSummary.getBillActSummaryId().getBilAccountId(),
                            bilActSummary.getBilAcyDes1Dt(), DECIMAL_ZERO, "PPS", dateService.currentDate(),
                            SEPARATOR_BLANK, bilActSummary.getUserId(), bilPolicy.getPolNbr(),
                            bilPolicy.getPolSymbolCd(), bilActSummary.getBilAcyDes2Dt(),
                            bilActSummary.getBasAddDataTxt());
                }
                return suppressLetter;
            }

        }

        return suppressLetter;
    }

    private char getPnrRuleCode(BilAccount bilAccount, BilActSummary bilActSummary, ZonedDateTime bcmoppcNrdDate) {
        if (bilActSummary.getBillActSummaryId().getBilAcyDt().isEqual(bcmoppcNrdDate)) {
            BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("PNRD", bilAccount.getBillTypeCd(),
                    bilAccount.getBillClassCd(), BLANK_STRING);
            if (bilRulesUct != null) {
                return bilRulesUct.getBrtRuleCd();
            }
        }

        return CHAR_Y;
    }

    private Double getInvoiceFeeAmount(BilAccount bilAccount, ZonedDateTime pcnDate) {
        Double invoiceFeeAmount = bilCrgAmountsRepository.getInvoiceFeeAmount(bilAccount.getAccountId(), pcnDate,
                Arrays.asList(ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue(), ChargeTypeCodes.LATE_CHARGE.getValue(),
                        ChargeTypeCodes.PENALTY_CHARGE.getValue(), ChargeType.SERVICECHARGES.getChargeType()),
                Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                        InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                        InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()));
        if (invoiceFeeAmount == null) {
            invoiceFeeAmount = DECIMAL_ZERO;
        }
        return invoiceFeeAmount;
    }

    private Double getAccessedAmount(BilAccount bilAccount, ZonedDateTime pcnDate) {

        Double accessedAmount = bilCrgAmountsRepository.getAccessedBalanceAmount(
                bilAccount.getAccountId(), pcnDate, Arrays.asList(ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue(),
                        ChargeTypeCodes.LATE_CHARGE.getValue(), ChargeTypeCodes.PENALTY_CHARGE.getValue()),
                ChargeType.SERVICECHARGES.getChargeType(),
                Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                        InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                        InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()));
        if (accessedAmount == null) {
            accessedAmount = DECIMAL_ZERO;
        }
        return accessedAmount;
    }

    private Double calculatePriorPolicyBalance(BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy,
            BilAccount bilAccount) {
        Double policyBalance = DECIMAL_ZERO;
        List<String> policyIdList = bilPolicyRepository.findPolicyIdsByPolicyNumber(bilAccount.getAccountId(),
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd());
        if (policyIdList != null && !policyIdList.isEmpty()) {
            policyBalance = bilIstScheduleRepository.getPriorPolicyBalance(bilAccount.getAccountId(), policyIdList,
                    bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                            InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()));

            if (policyBalance == null) {
                policyBalance = DECIMAL_ZERO;
            }
        }
        return policyBalance;
    }

    private Object[] calculatePcnData(BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy, BilAccount bilAccount) {
        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.getMaxRescindData(bilAccount.getAccountId(),
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), Arrays.asList("PCN"));
        if (bilActSummaryList != null && !bilActSummaryList.isEmpty()) {
            return new Object[] { bilActSummaryList.get(SHORT_ZERO).getBillActSummaryId().getBilAcyDt(),
                    bilActSummaryList.get(SHORT_ZERO).getBilAcyAmt() };
        }

        return new Object[0];

    }

    private BilActSummary readBilActSummay(BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy, BilAccount bilAccount) {
        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.findUserIdRescinded(bilAccount.getAccountId(),
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(),
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(), Arrays.asList("C"),
                Arrays.asList(" ", "CAM", "NQM"));
        if (bilActSummaryList == null || bilActSummaryList.isEmpty()) {
            throw new DataNotFoundException(NO_DATA_FOUND);
        }
        return bilActSummaryList.get(SHORT_ZERO);

    }

    private boolean isRescission(char policyStatusCode) {
        return Arrays
                .asList(BilPolicyStatusCode.PENDING_CANCELLATION.getValue(),
                        BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue(),
                        BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue(),
                        BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue())
                .stream().anyMatch(policyStatus -> policyStatus == policyStatusCode);
    }

    private void insertBilAccountSummary(String accountId, ZonedDateTime adjustDueDate, double amount,
            String bilAcyDesCd, ZonedDateTime se3DateTime, String bilDesReaTyp, String userId, String polNbr,
            String polSymbolCd, ZonedDateTime bilAcyDes2Dt, String basAddDataTxt) {

        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId, se3DateTime,
                getBilAcySeq(accountId, se3DateTime));
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(polSymbolCd);
        bilActSummary.setPolNbr(polNbr);
        bilActSummary.setBilAcyDesCd(bilAcyDesCd);
        bilActSummary.setBilDesReaTyp(bilDesReaTyp);
        bilActSummary.setBilAcyDes1Dt(adjustDueDate);
        bilActSummary.setBilAcyDes2Dt(bilAcyDes2Dt);
        bilActSummary.setBilAcyAmt(amount);
        bilActSummary.setUserId(userId);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());
        bilActSummary.setBasAddDataTxt(basAddDataTxt);

        bilActSummaryRepository.saveAndFlush(bilActSummary);

    }

    private Short getBilAcySeq(String accountId, ZonedDateTime se3DateTime) {
        Short bilAcySeq = 0;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, se3DateTime.with(LocalTime.MIN));
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        return bilAcySeq;
    }

}
