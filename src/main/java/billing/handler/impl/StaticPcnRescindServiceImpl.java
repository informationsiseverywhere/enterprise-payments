package billing.handler.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_E;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilActSummary;
import billing.data.entity.BilAmounts;
import billing.data.entity.BilCashReceipt;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilAmountsRepository;
import billing.data.repository.BilAmtSchRltRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.handler.model.StaticPcnRescind;
import billing.handler.service.StaticPcnRescindService;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import core.utils.DateRoutine;

@Service
public class StaticPcnRescindServiceImpl implements StaticPcnRescindService {

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilAmountsRepository bilAmountsRepository;

    @Autowired
    private BilAmtSchRltRepository bilAmtSchRltRepository;

    @Override
    public void processStaticPcnRescind(StaticPcnRescind staticPcnRescind) {

        boolean isStaticPcn = false;
        Double pcnAmount = null;
        double staticPcnPercentage = DECIMAL_ZERO;
        BilActSummary bilActSummary = getStaticPcnRescindData(staticPcnRescind);
        if (bilActSummary != null) {
            isStaticPcn = checkStaticPcn(staticPcnRescind, bilActSummary);
            if (isStaticPcn) {
                Integer count = paymentsSameDayCount(bilActSummary);
                ZonedDateTime pcnDueDate = calculatePcnDueDate(staticPcnRescind);
                Double receiptAmount = calculateTotalReciptAmount(staticPcnRescind, bilActSummary, pcnDueDate, count);

                ZonedDateTime bilTimeStamp = getMinimumTimestamp(staticPcnRescind, bilActSummary);
                if (bilTimeStamp != null) {
                    pcnAmount = calculateTotalPcnAmount(staticPcnRescind, pcnDueDate, bilTimeStamp);
                    if (pcnAmount != null) {
                        pcnAmount += receiptAmount;
                    }
                }

                Double staticPcnAmount = bilActSummary.getBilAcyAmt() - pcnAmount;
                if (staticPcnAmount < DECIMAL_ZERO) {
                    staticPcnAmount = DECIMAL_ZERO;
                }

                if (staticPcnRescind.getTermPremiumAmount() != DECIMAL_ZERO) {
                    staticPcnPercentage = staticPcnAmount / staticPcnRescind.getTermPremiumAmount() * 100;
                    int roundSepRec = (int) staticPcnPercentage;
                    double fraction = staticPcnPercentage - roundSepRec;
                    BigDecimal twoDecimalFraction = BigDecimal.valueOf(fraction).setScale(2, RoundingMode.HALF_EVEN);
                    staticPcnPercentage = roundSepRec + twoDecimalFraction.doubleValue();
                }

                if (receiptAmount != DECIMAL_ZERO || bilTimeStamp != null || pcnAmount != null) {
                    staticPcnRescind.setStaticPcnRescind(true);
                    staticPcnRescind.setTermPastAmount(staticPcnAmount);
                    staticPcnRescind.setTermPastPercent(staticPcnPercentage);
                    staticPcnRescind.setReceiptAmount(receiptAmount);
                }

            }
        }
    }

    private ZonedDateTime getMinimumTimestamp(StaticPcnRescind staticPcnRescind, BilActSummary bilActSummary) {

        return bilActSummaryRepository.getMinBilAcyTs(staticPcnRescind.getAccountId(),
                bilActSummary.getBillActSummaryId().getBilAcyDt(), bilActSummary.getBillActSummaryId().getBilAcySeq(),
                staticPcnRescind.getPolicyNumber(), staticPcnRescind.getPolicySymbolCode(), Arrays.asList("BAM", "AMT"),
                Arrays.asList("A", "E"));
    }

    private Double calculateTotalPcnAmount(StaticPcnRescind staticPcnRescind, ZonedDateTime pcnDueDate,
            ZonedDateTime bilTimeStamp) {

        List<BilAmounts> bilAmountsList = bilAmountsRepository.findStaticPcnBamAmountRows(
                staticPcnRescind.getAccountId(), staticPcnRescind.getPolicyId(), staticPcnRescind.getExpirationDate(),
                Arrays.asList(CHAR_A, CHAR_E), bilTimeStamp, DECIMAL_ZERO);
        if (bilAmountsList != null && !bilAmountsList.isEmpty()) {
            Double pcnAmount = DECIMAL_ZERO;
            for (BilAmounts bilAmounts : bilAmountsList) {
                Double premiumScheduleAmount = bilAmtSchRltRepository.getStaticPcnBamAmount(
                        staticPcnRescind.getAccountId(), staticPcnRescind.getPolicyId(),
                        bilAmounts.getBilAmountsId().getBilSeqNbr(), pcnDueDate);
                if (premiumScheduleAmount != null) {
                    if (premiumScheduleAmount < DECIMAL_ZERO) {
                        premiumScheduleAmount = premiumScheduleAmount * -1;
                    }

                    pcnAmount += premiumScheduleAmount;
                }
            }
            return pcnAmount;

        }

        return null;
    }

    private Double calculateTotalReciptAmount(StaticPcnRescind staticPcnRescind, BilActSummary bilActSummary,
            ZonedDateTime pcnDueDate, Integer paymentsCount) {
        double totalPaymentsAmount = DECIMAL_ZERO;
        List<BilCashReceipt> bilCashReceiptList = bilCashReceiptRepository.getPaymentReceiptsByDate(
                bilActSummary.getBillActSummaryId().getBilAccountId(),
                bilActSummary.getBillActSummaryId().getBilAcyDt(), paymentsCount.shortValue());
        if (bilCashReceiptList != null && !bilCashReceiptList.isEmpty()) {
            for (BilCashReceipt bilCashReceipt : bilCashReceiptList) {
                Double appliedAmount = bilCashDspRepository.getDspAmountForAppliedBydistributionDate(
                        staticPcnRescind.getAccountId(), bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                        bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), BilDspTypeCode.APPLIED.getValue(),
                        staticPcnRescind.getPolicyNumber(), staticPcnRescind.getPolicySymbolCode(), pcnDueDate,
                        ReverseReSuspendIndicator.BLANK.getValue(), BLANK_CHAR);
                if (appliedAmount != null) {
                    totalPaymentsAmount += appliedAmount;
                }
            }
        }
        return totalPaymentsAmount;
    }

    private ZonedDateTime calculatePcnDueDate(StaticPcnRescind staticPcnRescind) {

        if (staticPcnRescind.getPolicyStatus() == BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue()) {
            return DateRoutine.defaultDateTime();
        }

        return bilIstScheduleRepository.getMaxAdjDueDate(staticPcnRescind.getAccountId(),
                Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                        InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                        InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()),
                staticPcnRescind.getPolicyId(), staticPcnRescind.getEffectiveDate());

    }

    private Integer paymentsSameDayCount(BilActSummary bilActSummary) {
        Integer count = bilActSummaryRepository.paymentsSameDayCount(
                bilActSummary.getBillActSummaryId().getBilAccountId(),
                bilActSummary.getBillActSummaryId().getBilAcyDt(), bilActSummary.getBillActSummaryId().getBilAcySeq(),
                Arrays.asList("PYT", "CPY", "ACY", "COL", "DWP", "EWP"), Arrays.asList("EY", "EC"));
        if (count == null || count == SHORT_ZERO) {
            return -1;
        }

        return count - 1;
    }

    private boolean checkStaticPcn(StaticPcnRescind staticPcnRescind, BilActSummary bilActSummary) {

        boolean isCashReverse = false;
        boolean isCashTransfer = false;
        boolean isPremiumWriteOffReverse = false;

        if (staticPcnRescind.getPolicyStatus() == BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue()) {
            return true;
        }
        if (staticPcnRescind.getStaticRuleParm() != null && !staticPcnRescind.getStaticRuleParm().trim().isEmpty()) {
            isCashReverse = staticPcnRescind.getStaticRuleParm().trim().charAt(SHORT_ZERO) == CHAR_Y ;
            isCashTransfer = staticPcnRescind.getStaticRuleParm().trim().charAt(1) == CHAR_Y ;
            isPremiumWriteOffReverse = staticPcnRescind.getStaticRuleParm().trim().charAt(2) == CHAR_Y ;
        }

        if (isCashReverse) {
            Integer count = bilActSummaryRepository.checkCashReverseData(
                    bilActSummary.getBillActSummaryId().getBilAccountId(),
                    bilActSummary.getBillActSummaryId().getBilAcyDt(),
                    bilActSummary.getBillActSummaryId().getBilAcySeq(), Arrays.asList("NSF", "ENS", "PRV", "ERP"),
                    DECIMAL_ZERO);
            if (count != null && count != SHORT_ZERO) {
                return true;
            }
        }
        if (isCashTransfer) {
            Integer count = bilActSummaryRepository.checkCashTransferData(
                    bilActSummary.getBillActSummaryId().getBilAccountId(),
                    bilActSummary.getBillActSummaryId().getBilAcyDt(),
                    bilActSummary.getBillActSummaryId().getBilAcySeq(), "%%F", "CTR", DECIMAL_ZERO);
            if (count != null && count != SHORT_ZERO) {
                return true;
            }
        }

        if (isPremiumWriteOffReverse) {
            Integer count = bilActSummaryRepository.checkPremiumnWriteOffRevertAmountData(
                    bilActSummary.getBillActSummaryId().getBilAccountId(),
                    bilActSummary.getBillActSummaryId().getBilAcyDt(),
                    bilActSummary.getBillActSummaryId().getBilAcySeq(), staticPcnRescind.getPolicyNumber(),
                    staticPcnRescind.getPolicySymbolCode(), "WRR", staticPcnRescind.getTermPastDueDate(), DECIMAL_ZERO);
            if (count != null && count != SHORT_ZERO) {
                return true;
            }
        }
        return false;
    }

    private BilActSummary getStaticPcnRescindData(StaticPcnRescind staticPcnRescind) {

        List<String> bilAcyDesCdList = Arrays.asList("C", "RCN");

        List<BilActSummary> bilActSummaryList = bilActSummaryRepository.getMaxRescindData(
                staticPcnRescind.getAccountId(), staticPcnRescind.getPolicyNumber(),
                staticPcnRescind.getPolicySymbolCode(), staticPcnRescind.getEffectiveDate(), bilAcyDesCdList);
        if (bilActSummaryList != null && !bilActSummaryList.isEmpty()) {
            return bilActSummaryList.get(SHORT_ZERO);
        }

        return null;

    }

}
