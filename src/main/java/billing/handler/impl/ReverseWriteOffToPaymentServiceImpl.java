package billing.handler.impl;

import static billing.utils.BillingConstants.ERROR_CODE;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_C;
import static core.utils.CommonConstants.CHAR_E;
import static core.utils.CommonConstants.CHAR_M;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilCrgAmounts;
import billing.data.entity.BilIstSchedule;
import billing.data.entity.BilMstCoSt;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilMstCoStRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.handler.model.ReverseWriteOffPayment;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.ChargeTypeCodes;
import billing.utils.BillingConstants.InvoiceTypeCode;
import billing.utils.BillingConstants.PolicyIssueIndicators;
import billing.utils.BillingConstants.ProcessIndicator;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.FinancialIndicator;
import fis.utils.FinancialIntegratorConstants.FunctionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class ReverseWriteOffToPaymentServiceImpl {

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilMstCoStRepository bilMstCoStRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private DateService dateService;

    @Transactional
    public void reverseWriteOffToPayment(ReverseWriteOffPayment reverseWriteOffPayment, BilAccount bilAccount) {

        boolean reverseAllWriteOffRows = false;
        double amountToReverseCharges = reverseWriteOffPayment.getDownPaymentWriteOffAmount()
                + reverseWriteOffPayment.getServiceChargeWriteOffAmount()
                + reverseWriteOffPayment.getLateWriteOffAmount() + reverseWriteOffPayment.getPenaltyWriteOffAmount();
        double totleWriteOffAmount = amountToReverseCharges + reverseWriteOffPayment.getPremiumWriteOffcAmount()
                + reverseWriteOffPayment.getPremiumWriteOffeAmount();

        BilPolicy bilPolicy = bilPolicyRepository
                .findById(new BilPolicyId(reverseWriteOffPayment.getPolicyId(), bilAccount.getAccountId()))
                .orElse(null);
        if (bilPolicy == null) {
            throw new DataNotFoundException("no.data.found");
        }

        if (reverseWriteOffPayment.getCashInSuspenseAmount() < totleWriteOffAmount) {
            if (reverseWriteOffPayment.getPremiumWriteOffcAmount() != DECIMAL_ZERO
                    || reverseWriteOffPayment.getPremiumWriteOffeAmount() != DECIMAL_ZERO) {
                reversePremiumWriteOffAmount(bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), bilAccount,
                        reverseWriteOffPayment, reverseAllWriteOffRows);
                if (reverseWriteOffPayment.getCashInSuspenseAmount() > DECIMAL_ZERO
                        && amountToReverseCharges != DECIMAL_ZERO) {
                    reverseChargesWriteOffAmount(amountToReverseCharges, bilAccount, reverseWriteOffPayment,
                            reverseAllWriteOffRows);
                }

            }
        } else {
            reverseAllWriteOffRows = true;
            if (reverseWriteOffPayment.getPremiumWriteOffcAmount() != DECIMAL_ZERO
                    || reverseWriteOffPayment.getPremiumWriteOffeAmount() != DECIMAL_ZERO
                    || reverseWriteOffPayment.getPenaltyWriteOffAmount() != DECIMAL_ZERO
                    || reverseWriteOffPayment.getLateWriteOffAmount() != DECIMAL_ZERO
                    || reverseWriteOffPayment.getServiceChargeWriteOffAmount() != DECIMAL_ZERO
                    || reverseWriteOffPayment.getDownPaymentWriteOffAmount() != DECIMAL_ZERO) {
                reversePremiumWriteOffAmount(bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), bilAccount,
                        reverseWriteOffPayment, reverseAllWriteOffRows);
                if (amountToReverseCharges != DECIMAL_ZERO) {
                    reverseChargesWriteOffAmount(amountToReverseCharges, bilAccount, reverseWriteOffPayment,
                            reverseAllWriteOffRows);
                }

            }
            bilPolicyTermRepository.updateWroPrcIndicator(bilAccount.getAccountId(),
                    Arrays.asList(ProcessIndicator.PROCESSED.getValue(), CHAR_M), BLANK_CHAR);
        }

    }

    private void reversePremiumWriteOffAmount(String polNbr, String polSymbolCd, BilAccount bilAccount,
            ReverseWriteOffPayment reverseWriteOffPayment, boolean reverseAllWriteOffRows) {

        String previousPolicyId = BLANK_STRING;
        ZonedDateTime previousPolicyEffectiveDate = null;
        double totalReverseWcAmount = DECIMAL_ZERO;
        double totalReverseWeAmount = DECIMAL_ZERO;
        BilPolicyTerm bilPolicyTerm = null;
        char reverseType = BLANK_CHAR;
        double reverseAmount = DECIMAL_ZERO;
        double newWriteOffAmount = DECIMAL_ZERO;
        double amount = DECIMAL_ZERO;

        if (reverseWriteOffPayment.getPremiumWriteOffcAmount() > DECIMAL_ZERO
                && reverseWriteOffPayment.getPremiumWriteOffeAmount() == DECIMAL_ZERO) {
            reverseType = CHAR_C;
            reverseAmount = reverseWriteOffPayment.getPremiumWriteOffcAmount();
        } else if (reverseWriteOffPayment.getPremiumWriteOffeAmount() > DECIMAL_ZERO
                && reverseWriteOffPayment.getPremiumWriteOffcAmount() == DECIMAL_ZERO) {
            reverseType = CHAR_E;
            reverseAmount = reverseWriteOffPayment.getPremiumWriteOffeAmount();
        }

        if (!reverseAllWriteOffRows && reverseWriteOffPayment.getCashInSuspenseAmount() == DECIMAL_ZERO
                || reverseAmount == DECIMAL_ZERO) {
            return;
        }

        List<BilPolicy> bilPolicyList = bilPolicyRepository.findBilPolicyByPolicyNumber(polNbr, polSymbolCd,
                Arrays.asList(PolicyIssueIndicators.CANCELLED_REISSUE.getValue(),
                        PolicyIssueIndicators.ISSUED_EDIT.getValue(),
                        PolicyIssueIndicators.ISSUED_NOT_EDIT.getValue()));
        if (bilPolicyList != null && !bilPolicyList.isEmpty()) {
            for (BilPolicy bilPolicy : bilPolicyList) {

                List<BilIstSchedule> bilIstScheduleList = bilIstScheduleRepository.getPremiumWriteOffRows(
                        bilAccount.getAccountId(), bilPolicy.getBillPolicyId().getPolicyId(), DECIMAL_ZERO);
                if (bilIstScheduleList != null && bilIstScheduleList.isEmpty()) {
                    for (BilIstSchedule bilIstSchedule : bilIstScheduleList) {
                        if (!previousPolicyId.equals(bilIstSchedule.getBillIstScheduleId().getPolicyId())
                                || previousPolicyEffectiveDate != bilIstSchedule.getPolicyEffectiveDt()) {
                            bilPolicyTerm = bilPolicyTermRepository.findById(
                                    new BilPolicyTermId(bilIstSchedule.getBillIstScheduleId().getBillAccountId(),
                                            bilIstSchedule.getBillIstScheduleId().getPolicyId(),
                                            bilIstSchedule.getPolicyEffectiveDt()))
                                    .orElse(null);
                            if (bilPolicyTerm == null) {
                                throw new DataNotFoundException("no.data.found");
                            }
                            previousPolicyId = bilIstSchedule.getBillIstScheduleId().getPolicyId();
                            previousPolicyEffectiveDate = bilIstSchedule.getPolicyEffectiveDt();

                        }
                        if (!reverseAllWriteOffRows
                                && reverseWriteOffPayment.getCashInSuspenseAmount() <= bilIstSchedule.getBisWroPrmAmt()
                                && reverseWriteOffPayment.getCashInSuspenseAmount() <= reverseAmount) {
                            newWriteOffAmount = bilIstSchedule.getBisWroPrmAmt()
                                    - reverseWriteOffPayment.getCashInSuspenseAmount();
                            amount = reverseWriteOffPayment.getCashInSuspenseAmount() * -1;
                            if (reverseType == CHAR_C) {
                                totalReverseWcAmount = totalReverseWcAmount
                                        + reverseWriteOffPayment.getCashInSuspenseAmount();
                            } else if (reverseType == CHAR_E) {
                                totalReverseWeAmount = totalReverseWeAmount
                                        + reverseWriteOffPayment.getCashInSuspenseAmount();
                            }
                            reverseWriteOffPayment.setCashInSuspenseAmount(DECIMAL_ZERO);
                        } else if (!reverseAllWriteOffRows
                                && reverseWriteOffPayment.getCashInSuspenseAmount() <= bilIstSchedule.getBisWroPrmAmt()
                                && reverseWriteOffPayment.getCashInSuspenseAmount() > reverseAmount) {
                            reverseWriteOffPayment.setCashInSuspenseAmount(
                                    reverseWriteOffPayment.getCashInSuspenseAmount() - reverseAmount);
                            newWriteOffAmount = bilIstSchedule.getBisWroPrmAmt() - reverseAmount;
                            amount = reverseAmount * -1;
                            if (reverseType == CHAR_C) {
                                totalReverseWcAmount = totalReverseWcAmount + reverseAmount;
                            } else if (reverseType == CHAR_E) {
                                totalReverseWeAmount = totalReverseWeAmount + reverseAmount;
                            }
                            reverseAmount = DECIMAL_ZERO;
                        } else if (!reverseAllWriteOffRows
                                && reverseWriteOffPayment.getCashInSuspenseAmount() > bilIstSchedule.getBisWroPrmAmt()
                                && reverseAmount >= bilIstSchedule.getBisWroPrmAmt()) {
                            reverseWriteOffPayment
                                    .setCashInSuspenseAmount(reverseWriteOffPayment.getCashInSuspenseAmount()
                                            - bilIstSchedule.getBisWroPrmAmt());
                            reverseAmount = reverseAmount - bilIstSchedule.getBisWroPrmAmt();
                            amount = bilIstSchedule.getBisWroPrmAmt() * -1;
                            newWriteOffAmount = DECIMAL_ZERO;
                            if (reverseType == CHAR_C) {
                                totalReverseWcAmount = totalReverseWcAmount + bilIstSchedule.getBisWroPrmAmt();
                            } else if (reverseType == CHAR_E) {
                                totalReverseWeAmount = totalReverseWeAmount + bilIstSchedule.getBisWroPrmAmt();
                            }
                        } else if (!reverseAllWriteOffRows
                                && reverseWriteOffPayment.getCashInSuspenseAmount() > bilIstSchedule.getBisWroPrmAmt()
                                && reverseAmount < bilIstSchedule.getBisWroPrmAmt()) {
                            reverseWriteOffPayment.setCashInSuspenseAmount(
                                    reverseWriteOffPayment.getCashInSuspenseAmount() - reverseAmount);
                            amount = reverseAmount * -1;
                            newWriteOffAmount = bilIstSchedule.getBisWroPrmAmt() - reverseAmount;
                            if (reverseType == CHAR_C) {
                                totalReverseWcAmount = totalReverseWcAmount + reverseAmount;
                            } else if (reverseType == CHAR_E) {
                                totalReverseWeAmount = totalReverseWeAmount + reverseAmount;
                            }
                        } else if (reverseAllWriteOffRows && reverseAmount < bilIstSchedule.getBisWroPrmAmt()) {
                            amount = reverseAmount * -1;
                            newWriteOffAmount = bilIstSchedule.getBisWroPrmAmt() - reverseAmount;
                            if (reverseType == CHAR_C) {
                                totalReverseWcAmount = totalReverseWcAmount + reverseAmount;
                            } else if (reverseType == CHAR_E) {
                                totalReverseWeAmount = totalReverseWeAmount + reverseAmount;
                            }
                            reverseAmount = DECIMAL_ZERO;

                        } else if (reverseAllWriteOffRows && reverseAmount >= bilIstSchedule.getBisWroPrmAmt()) {
                            reverseAmount = reverseAmount - bilIstSchedule.getBisWroPrmAmt();
                            amount = bilIstSchedule.getBisWroPrmAmt() * -1;
                            newWriteOffAmount = DECIMAL_ZERO;
                            if (reverseType == CHAR_C) {
                                totalReverseWcAmount = totalReverseWcAmount + bilIstSchedule.getBisWroPrmAmt();
                            } else if (reverseType == CHAR_E) {
                                totalReverseWeAmount = totalReverseWeAmount + bilIstSchedule.getBisWroPrmAmt();
                            }
                        }

                        bilIstSchedule.setBisWroPrmAmt(newWriteOffAmount);
                        bilIstScheduleRepository.saveAndFlush(bilIstSchedule);
                        if (reverseWriteOffPayment.getQuoteIndicator().charAt(0) != CHAR_Q && amount != DECIMAL_ZERO) {
                            writePremiumFinancialApi(amount, reverseWriteOffPayment, bilAccount, bilIstSchedule,
                                    reverseType, bilPolicyTerm);

                        }

                        if (!reverseAllWriteOffRows && reverseWriteOffPayment.getCashInSuspenseAmount() == DECIMAL_ZERO
                                || reverseAmount == DECIMAL_ZERO) {
                            writePremiumIntoSummary(totalReverseWcAmount, totalReverseWeAmount, polNbr, polSymbolCd,
                                    reverseWriteOffPayment, bilAccount);
                            return;
                        }
                    }
                }
            }
            writePremiumIntoSummary(totalReverseWcAmount, totalReverseWeAmount, polNbr, polSymbolCd,
                    reverseWriteOffPayment, bilAccount);

        }

    }

    private void writePremiumIntoSummary(double totalReverseWcAmount, double totalReverseWeAmount, String polNbr,
            String polSymbolCd, ReverseWriteOffPayment reverseWriteOffPayment, BilAccount bilAccount) {
        if (totalReverseWcAmount > DECIMAL_ZERO) {
            insertBilAccountSummary(bilAccount.getAccountId(), DateRoutine.defaultDateTime(), totalReverseWcAmount,
                    "WCR", reverseWriteOffPayment.getApplicationDate(), "WRR", BillingConstants.SYSTEM_USER_ID, polNbr,
                    polSymbolCd);
        }

        if (totalReverseWeAmount > DECIMAL_ZERO) {
            insertBilAccountSummary(bilAccount.getAccountId(), DateRoutine.defaultDateTime(), totalReverseWeAmount,
                    "WER", reverseWriteOffPayment.getApplicationDate(), "WRR", BillingConstants.SYSTEM_USER_ID, polNbr,
                    polSymbolCd);
        }

    }

    private void writePremiumFinancialApi(double amount, ReverseWriteOffPayment reverseWriteOffPayment,
            BilAccount bilAccount, BilIstSchedule bilIstSchedule, char reverseType, BilPolicyTerm bilPolicyTerm) {

        String countryCode = BLANK_STRING;
        String lineOfBusiness = BLANK_STRING;
        String policyId = BLANK_STRING;
        String payableItem = BLANK_STRING;
        String objectCode = BLANK_STRING;
        String actionCode = BLANK_STRING;
        String orginalEffectiveDate = DateRoutine.dateTimeAsYYYYMMDDString(reverseWriteOffPayment.getApplicationDate());
        ZonedDateTime referenceDate = reverseWriteOffPayment.getApplicationDate();
        String bilReasonCode = BLANK_STRING;

        if (reverseType == CHAR_C) {
            countryCode = bilPolicyTerm.getBillCountryCd();
            lineOfBusiness = bilPolicyTerm.getLobCd();
            policyId = bilIstSchedule.getBillIstScheduleId().getPolicyId();
            orginalEffectiveDate = DateRoutine.dateTimeAsYYYYMMDDString(bilIstSchedule.getPolicyEffectiveDt());
            referenceDate = bilIstSchedule.getPolicyEffectiveDt();
            payableItem = bilIstSchedule.getBillPblItemCd();
            bilReasonCode = "WCR";
            objectCode = ObjectCode.PREMIUM.toString();
            if (bilIstSchedule.getBillInvoiceCd() == InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()
                    || bilIstSchedule
                            .getBillInvoiceCd() == InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE
                                    .getValue()) {
                actionCode = ActionCode.WRITE_OFF_INSTALL.toString();
            } else {
                actionCode = ActionCode.WRITE_OFF_UNBILLED.toString();
            }
        } else if (reverseType == CHAR_E) {
            countryCode = bilPolicyTerm.getBillCountryCd();
            lineOfBusiness = bilPolicyTerm.getLobCd();
            policyId = bilIstSchedule.getBillIstScheduleId().getPolicyId();
            orginalEffectiveDate = DateRoutine.dateTimeAsYYYYMMDDString(bilIstSchedule.getPolicyEffectiveDt());
            referenceDate = bilIstSchedule.getPolicyEffectiveDt();
            payableItem = bilIstSchedule.getBillPblItemCd();
            bilReasonCode = "WER";
            objectCode = ObjectCode.PREMIUM.toString();
            if (bilIstSchedule.getBillInvoiceCd() == InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue()
                    || bilIstSchedule
                            .getBillInvoiceCd() == InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE
                                    .getValue()) {
                actionCode = ActionCode.WRITE_OFF_INSTALL.toString();
            } else {
                actionCode = ActionCode.WRITE_OFF_UNBILLED.toString();
            }
        }

        // calling CCMOBCM is required only for online processing
        if (!bilIstSchedule.getBillIstScheduleId().getPolicyId().trim().isEmpty()
                && bilPolicyTerm.getBptAgreementInd() != CHAR_Y) {
            // To Do call CCMOBCM
        }

        String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(), bilAccount.getBillTypeCd());

        callFinancialApi(amount, reverseWriteOffPayment.getApplicationDate(), BLANK_STRING,
                bilAccount.getAccountNumber(), objectCode, actionCode, payableItem, BLANK_STRING, BLANK_STRING,
                bilReasonCode, bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(), referenceDate, BLANK_STRING,
                BillingConstants.SYSTEM_USER_ID, orginalEffectiveDate, ApplicationProgramIdentifier.BCMORWO.toString(),
                policyId, countryCode, lineOfBusiness, bilmasterCompanyState[0], bilmasterCompanyState[1],
                bilAccount.getCurrencyCode());

    }

    private void reverseChargesWriteOffAmount(double amountToReverse, BilAccount bilAccount,
            ReverseWriteOffPayment reverseWriteOffPayment, boolean reverseAllWriteOffRows) {
        boolean reversePenaltyAmount = false;
        boolean reverseLateAmount = false;
        boolean reverseServiceChargeAmount = false;
        boolean reverseDownPaymentAmount = false;
        double autoWriteOffAmount = DECIMAL_ZERO;
        double newWriteOffAmount = DECIMAL_ZERO;
        double amount = DECIMAL_ZERO;
        double totalReversePenaltyAmount = DECIMAL_ZERO;
        double totalReverseLateAmount = DECIMAL_ZERO;
        double totalReverseServiceChargeAmount = DECIMAL_ZERO;
        double totalReverseDownPaymentAmount = DECIMAL_ZERO;

        if (reverseWriteOffPayment.getPenaltyWriteOffAmount() > DECIMAL_ZERO) {
            reversePenaltyAmount = true;
        }
        if (reverseWriteOffPayment.getLateWriteOffAmount() > DECIMAL_ZERO) {
            reverseLateAmount = true;
        }
        if (reverseWriteOffPayment.getServiceChargeWriteOffAmount() > DECIMAL_ZERO) {
            reverseServiceChargeAmount = true;
        }
        if (reverseWriteOffPayment.getDownPaymentWriteOffAmount() > DECIMAL_ZERO) {
            reverseDownPaymentAmount = true;
        }

        List<BilCrgAmounts> bilCrgAmountsList = bilCrgAmountsRepository.readReverseChargeRows(bilAccount.getAccountId(),
                DECIMAL_ZERO);
        if (bilCrgAmountsList != null && !bilCrgAmountsList.isEmpty()) {
            for (BilCrgAmounts bilCrgAmounts : bilCrgAmountsList) {
                if (!reversePenaltyAmount
                        && bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.PENALTY_CHARGE.getValue()
                        || !reverseLateAmount
                                && bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.LATE_CHARGE.getValue()
                        || !reverseServiceChargeAmount
                                && bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.SERVICE_CHARGE.getValue()
                        || !reverseDownPaymentAmount
                                && bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()) {
                    continue;
                }
                if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.PENALTY_CHARGE.getValue()) {
                    autoWriteOffAmount = reverseWriteOffPayment.getPenaltyWriteOffAmount();
                } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.LATE_CHARGE.getValue()) {
                    autoWriteOffAmount = reverseWriteOffPayment.getLateWriteOffAmount();
                } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.SERVICE_CHARGE.getValue()) {
                    autoWriteOffAmount = reverseWriteOffPayment.getServiceChargeWriteOffAmount();
                } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()) {
                    autoWriteOffAmount = reverseWriteOffPayment.getDownPaymentWriteOffAmount();
                }
                if (!reverseAllWriteOffRows
                        && reverseWriteOffPayment.getCashInSuspenseAmount() <= bilCrgAmounts.getBcaWroCrgAmt()
                        && reverseWriteOffPayment.getCashInSuspenseAmount() <= amountToReverse
                        && reverseWriteOffPayment.getCashInSuspenseAmount() <= autoWriteOffAmount) {
                    newWriteOffAmount = bilCrgAmounts.getBcaWroCrgAmt()
                            - reverseWriteOffPayment.getCashInSuspenseAmount();
                    amount = reverseWriteOffPayment.getCashInSuspenseAmount() * -1;
                    if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.PENALTY_CHARGE.getValue()) {
                        totalReversePenaltyAmount = totalReversePenaltyAmount
                                + reverseWriteOffPayment.getCashInSuspenseAmount();
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.LATE_CHARGE.getValue()) {
                        totalReverseLateAmount = totalReverseLateAmount
                                + reverseWriteOffPayment.getCashInSuspenseAmount();
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.SERVICE_CHARGE.getValue()) {
                        totalReverseServiceChargeAmount = totalReverseServiceChargeAmount
                                + reverseWriteOffPayment.getCashInSuspenseAmount();
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()) {
                        totalReverseDownPaymentAmount = totalReverseDownPaymentAmount
                                + reverseWriteOffPayment.getCashInSuspenseAmount();
                    }
                    reverseWriteOffPayment.setCashInSuspenseAmount(DECIMAL_ZERO);
                } else if (!reverseAllWriteOffRows
                        && amountToReverse <= reverseWriteOffPayment.getCashInSuspenseAmount()
                        && amountToReverse <= bilCrgAmounts.getBcaWroCrgAmt()
                        && amountToReverse <= autoWriteOffAmount) {
                    reverseWriteOffPayment.setCashInSuspenseAmount(
                            reverseWriteOffPayment.getCashInSuspenseAmount() - amountToReverse);
                    newWriteOffAmount = bilCrgAmounts.getBcaWroCrgAmt() - amountToReverse;
                    amount = amountToReverse * -1;
                    if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.PENALTY_CHARGE.getValue()) {
                        totalReversePenaltyAmount = totalReversePenaltyAmount + amountToReverse;
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.LATE_CHARGE.getValue()) {
                        totalReverseLateAmount = totalReverseLateAmount + amountToReverse;
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.SERVICE_CHARGE.getValue()) {
                        totalReverseServiceChargeAmount = totalReverseServiceChargeAmount + amountToReverse;
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()) {
                        totalReverseDownPaymentAmount = totalReverseDownPaymentAmount + amountToReverse;
                    }
                    amountToReverse = DECIMAL_ZERO;
                } else if (!reverseAllWriteOffRows
                        && bilCrgAmounts.getBcaWroCrgAmt() < reverseWriteOffPayment.getCashInSuspenseAmount()
                        && bilCrgAmounts.getBcaWroCrgAmt() < amountToReverse
                        && bilCrgAmounts.getBcaWroCrgAmt() < autoWriteOffAmount) {
                    reverseWriteOffPayment.setCashInSuspenseAmount(
                            reverseWriteOffPayment.getCashInSuspenseAmount() - bilCrgAmounts.getBcaWroCrgAmt());
                    amountToReverse = amountToReverse - bilCrgAmounts.getBcaWroCrgAmt();
                    newWriteOffAmount = DECIMAL_ZERO;
                    amount = bilCrgAmounts.getBcaWroCrgAmt() * -1;
                    if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.PENALTY_CHARGE.getValue()) {
                        totalReversePenaltyAmount = totalReversePenaltyAmount + bilCrgAmounts.getBcaWroCrgAmt();
                        reverseWriteOffPayment.setPenaltyWriteOffAmount(
                                reverseWriteOffPayment.getPenaltyWriteOffAmount() - bilCrgAmounts.getBcaWroCrgAmt());
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.LATE_CHARGE.getValue()) {
                        totalReverseLateAmount = totalReverseLateAmount + bilCrgAmounts.getBcaWroCrgAmt();
                        reverseWriteOffPayment.setLateWriteOffAmount(
                                reverseWriteOffPayment.getLateWriteOffAmount() - bilCrgAmounts.getBcaWroCrgAmt());
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.SERVICE_CHARGE.getValue()) {
                        totalReverseServiceChargeAmount = totalReverseServiceChargeAmount
                                + bilCrgAmounts.getBcaWroCrgAmt();
                        reverseWriteOffPayment
                                .setServiceChargeWriteOffAmount(reverseWriteOffPayment.getServiceChargeWriteOffAmount()
                                        - bilCrgAmounts.getBcaWroCrgAmt());
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()) {
                        totalReverseDownPaymentAmount = totalReverseDownPaymentAmount + bilCrgAmounts.getBcaWroCrgAmt();
                        reverseWriteOffPayment
                                .setDownPaymentWriteOffAmount(reverseWriteOffPayment.getDownPaymentWriteOffAmount()
                                        - bilCrgAmounts.getBcaWroCrgAmt());
                    }
                } else if (!reverseAllWriteOffRows
                        && autoWriteOffAmount < reverseWriteOffPayment.getCashInSuspenseAmount()
                        && autoWriteOffAmount < amountToReverse && autoWriteOffAmount < bilCrgAmounts.getBcaWroCrgAmt()
                        || autoWriteOffAmount == bilCrgAmounts.getBcaWroCrgAmt()) {
                    reverseWriteOffPayment.setCashInSuspenseAmount(
                            reverseWriteOffPayment.getCashInSuspenseAmount() - autoWriteOffAmount);
                    amountToReverse = amountToReverse - autoWriteOffAmount;
                    newWriteOffAmount = bilCrgAmounts.getBcaWroCrgAmt() - autoWriteOffAmount;
                    amount = autoWriteOffAmount * -1;
                    if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.PENALTY_CHARGE.getValue()) {
                        totalReversePenaltyAmount = totalReversePenaltyAmount + autoWriteOffAmount;
                        reverseWriteOffPayment.setPenaltyWriteOffAmount(DECIMAL_ZERO);
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.LATE_CHARGE.getValue()) {
                        totalReverseLateAmount = totalReverseLateAmount + autoWriteOffAmount;
                        reverseWriteOffPayment.setLateWriteOffAmount(DECIMAL_ZERO);
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.SERVICE_CHARGE.getValue()) {
                        totalReverseServiceChargeAmount = totalReverseServiceChargeAmount + autoWriteOffAmount;
                        reverseWriteOffPayment.setServiceChargeWriteOffAmount(DECIMAL_ZERO);
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()) {
                        totalReverseDownPaymentAmount = totalReverseDownPaymentAmount + autoWriteOffAmount;
                        reverseWriteOffPayment.setDownPaymentWriteOffAmount(DECIMAL_ZERO);
                    }
                } else if (reverseAllWriteOffRows && amountToReverse <= bilCrgAmounts.getBcaWroCrgAmt()
                        && amountToReverse <= autoWriteOffAmount) {
                    newWriteOffAmount = bilCrgAmounts.getBcaWroCrgAmt() - amountToReverse;
                    amount = amountToReverse * -1;
                    if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.PENALTY_CHARGE.getValue()) {
                        totalReversePenaltyAmount = totalReversePenaltyAmount + amountToReverse;
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.LATE_CHARGE.getValue()) {
                        totalReverseLateAmount = totalReverseLateAmount + amountToReverse;
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.SERVICE_CHARGE.getValue()) {
                        totalReverseServiceChargeAmount = totalReverseServiceChargeAmount + amountToReverse;
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()) {
                        totalReverseDownPaymentAmount = totalReverseDownPaymentAmount + amountToReverse;
                    }
                    amountToReverse = DECIMAL_ZERO;
                } else if (reverseAllWriteOffRows && autoWriteOffAmount < bilCrgAmounts.getBcaWroCrgAmt()) {
                    amountToReverse = amountToReverse - autoWriteOffAmount;
                    newWriteOffAmount = bilCrgAmounts.getBcaWroCrgAmt() - autoWriteOffAmount;
                    amount = autoWriteOffAmount * -1;
                    if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.PENALTY_CHARGE.getValue()) {
                        totalReversePenaltyAmount = totalReversePenaltyAmount + autoWriteOffAmount;
                        reverseWriteOffPayment.setPenaltyWriteOffAmount(DECIMAL_ZERO);
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.LATE_CHARGE.getValue()) {
                        totalReverseLateAmount = totalReverseLateAmount + autoWriteOffAmount;
                        reverseWriteOffPayment.setLateWriteOffAmount(DECIMAL_ZERO);
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.SERVICE_CHARGE.getValue()) {
                        totalReverseServiceChargeAmount = totalReverseServiceChargeAmount + autoWriteOffAmount;
                        reverseWriteOffPayment.setServiceChargeWriteOffAmount(DECIMAL_ZERO);
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()) {
                        totalReverseDownPaymentAmount = totalReverseDownPaymentAmount + autoWriteOffAmount;
                        reverseWriteOffPayment.setDownPaymentWriteOffAmount(DECIMAL_ZERO);
                    }
                } else if (reverseAllWriteOffRows) {
                    amountToReverse = amountToReverse - bilCrgAmounts.getBcaWroCrgAmt();
                    amount = bilCrgAmounts.getBcaWroCrgAmt() * -1;
                    newWriteOffAmount = DECIMAL_ZERO;
                    if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.PENALTY_CHARGE.getValue()) {
                        totalReversePenaltyAmount = totalReversePenaltyAmount + bilCrgAmounts.getBcaWroCrgAmt();
                        reverseWriteOffPayment.setPenaltyWriteOffAmount(
                                reverseWriteOffPayment.getPenaltyWriteOffAmount() - bilCrgAmounts.getBcaWroCrgAmt());
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.LATE_CHARGE.getValue()) {
                        totalReverseLateAmount = totalReverseLateAmount + bilCrgAmounts.getBcaWroCrgAmt();
                        reverseWriteOffPayment.setLateWriteOffAmount(
                                reverseWriteOffPayment.getLateWriteOffAmount() - bilCrgAmounts.getBcaWroCrgAmt());
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.SERVICE_CHARGE.getValue()) {
                        totalReverseServiceChargeAmount = totalReverseServiceChargeAmount
                                + bilCrgAmounts.getBcaWroCrgAmt();
                        reverseWriteOffPayment
                                .setServiceChargeWriteOffAmount(reverseWriteOffPayment.getServiceChargeWriteOffAmount()
                                        - bilCrgAmounts.getBcaWroCrgAmt());
                    } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()) {
                        totalReverseDownPaymentAmount = totalReverseDownPaymentAmount + bilCrgAmounts.getBcaWroCrgAmt();
                        reverseWriteOffPayment
                                .setDownPaymentWriteOffAmount(reverseWriteOffPayment.getDownPaymentWriteOffAmount()
                                        - bilCrgAmounts.getBcaWroCrgAmt());
                    }
                }

                bilCrgAmounts.setBcaWroCrgAmt(newWriteOffAmount);
                bilCrgAmountsRepository.saveAndFlush(bilCrgAmounts);

                if (reverseWriteOffPayment.getQuoteIndicator().charAt(0) != CHAR_Q && amount != DECIMAL_ZERO) {
                    writeChargesFinancialApi(amount, reverseWriteOffPayment, bilAccount, bilCrgAmounts);

                }
                if (!reverseAllWriteOffRows && reverseWriteOffPayment.getCashInSuspenseAmount() == DECIMAL_ZERO
                        || amountToReverse == DECIMAL_ZERO) {
                    writeChargesIntoSummary(totalReversePenaltyAmount, totalReverseLateAmount,
                            totalReverseServiceChargeAmount, totalReverseDownPaymentAmount, bilAccount,
                            reverseWriteOffPayment);
                    return;

                }
            }

            writeChargesIntoSummary(totalReversePenaltyAmount, totalReverseLateAmount, totalReverseServiceChargeAmount,
                    totalReverseDownPaymentAmount, bilAccount, reverseWriteOffPayment);
        }

    }

    private void writeChargesFinancialApi(double amount, ReverseWriteOffPayment reverseWriteOffPayment,
            BilAccount bilAccount, BilCrgAmounts bilCrgAmounts) {
        String countryCode = BLANK_STRING;
        String lineOfBusiness = BLANK_STRING;
        String policyId = BLANK_STRING;
        String payableItem = BLANK_STRING;
        String objectCode = BLANK_STRING;
        String actionCode = BLANK_STRING;
        String orginalEffectiveDate = DateRoutine.dateTimeAsYYYYMMDDString(reverseWriteOffPayment.getApplicationDate());
        ZonedDateTime referenceDate = reverseWriteOffPayment.getApplicationDate();
        String bilReasonCode = BLANK_STRING;

        if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.SERVICE_CHARGE.getValue()) {
            bilReasonCode = "WAR";
            objectCode = ObjectCode.SERVICE_CHARGE.toString();
            actionCode = ActionCode.WRITE_OFF_SERVICE_CHARGE.toString();
        } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue()) {
            bilReasonCode = "WDR";
            objectCode = ObjectCode.DOWN_PAYMENT_FEE.toString();
            if (bilCrgAmounts.getBilInvoiceCd() == InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue() || bilCrgAmounts
                    .getBilInvoiceCd() == InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()) {
                actionCode = ActionCode.WRITE_OFF_INSTALL.toString();
            } else {
                actionCode = ActionCode.WRITE_OFF_UNBILLED.toString();
            }
        } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.LATE_CHARGE.getValue()) {
            bilReasonCode = "WLR";
            objectCode = ObjectCode.LATE_CHARGE.toString();
            if (bilCrgAmounts.getBilInvoiceCd() == InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue() || bilCrgAmounts
                    .getBilInvoiceCd() == InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()) {
                actionCode = ActionCode.WRITE_OFF_INSTALL.toString();
            } else {
                actionCode = ActionCode.WRITE_OFF_UNBILLED.toString();
            }
        } else if (bilCrgAmounts.getBilCrgTypeCd() == ChargeTypeCodes.PENALTY_CHARGE.getValue()) {
            bilReasonCode = "WPR";
            objectCode = ObjectCode.PENALTY_CHARGE.toString();
            if (bilCrgAmounts.getBilInvoiceCd() == InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue() || bilCrgAmounts
                    .getBilInvoiceCd() == InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()) {
                actionCode = ActionCode.WRITE_OFF_INSTALL.toString();
            } else {
                actionCode = ActionCode.WRITE_OFF_UNBILLED.toString();
            }
        }
        String[] bilmasterCompanyState = readBilMstCoSt(bilAccount.getBillClassCd(), bilAccount.getBillTypeCd());

        callFinancialApi(amount, reverseWriteOffPayment.getApplicationDate(), BLANK_STRING,
                bilAccount.getAccountNumber(), objectCode, actionCode, payableItem, BLANK_STRING, BLANK_STRING,
                bilReasonCode, bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(), referenceDate, BLANK_STRING,
                BillingConstants.SYSTEM_USER_ID, orginalEffectiveDate, ApplicationProgramIdentifier.BCMORWO.toString(),
                policyId, countryCode, lineOfBusiness, bilmasterCompanyState[0], bilmasterCompanyState[1],
                bilAccount.getCurrencyCode());

    }

    private void writeChargesIntoSummary(double totalReversePenaltyAmount, double totalReverseLateAmount,
            double totalReverseServiceChargeAmount, double totalReverseDownPaymentAmount, BilAccount bilAccount,
            ReverseWriteOffPayment reverseWriteOffPayment) {

        if (totalReversePenaltyAmount > DECIMAL_ZERO) {
            insertBilAccountSummary(bilAccount.getAccountId(), DateRoutine.defaultDateTime(), totalReversePenaltyAmount,
                    "WPR", reverseWriteOffPayment.getApplicationDate(), "WRR", BillingConstants.SYSTEM_USER_ID,
                    BLANK_STRING, BLANK_STRING);
        }

        if (totalReverseLateAmount > DECIMAL_ZERO) {
            insertBilAccountSummary(bilAccount.getAccountId(), DateRoutine.defaultDateTime(), totalReverseLateAmount,
                    "WLR", reverseWriteOffPayment.getApplicationDate(), "WRR", BillingConstants.SYSTEM_USER_ID,
                    BLANK_STRING, BLANK_STRING);
        }

        if (totalReverseServiceChargeAmount > DECIMAL_ZERO) {
            insertBilAccountSummary(bilAccount.getAccountId(), DateRoutine.defaultDateTime(),
                    totalReverseServiceChargeAmount, "WAR", reverseWriteOffPayment.getApplicationDate(), "WRR",
                    BillingConstants.SYSTEM_USER_ID, BLANK_STRING, BLANK_STRING);
        }

        if (totalReverseDownPaymentAmount > DECIMAL_ZERO) {
            insertBilAccountSummary(bilAccount.getAccountId(), DateRoutine.defaultDateTime(),
                    totalReverseDownPaymentAmount, "WDR", reverseWriteOffPayment.getApplicationDate(), "WRR",
                    BillingConstants.SYSTEM_USER_ID, BLANK_STRING, BLANK_STRING);
        }
    }

    private void insertBilAccountSummary(String accountId, ZonedDateTime adjustDueDate, double amount,
            String bilAcyDesCd, ZonedDateTime se3DateTime, String bilDesReaTyp, String userId, String polNbr,
            String polSymbolCd) {

        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId, se3DateTime,
                getBilAcySeq(accountId, se3DateTime));
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(polSymbolCd);
        bilActSummary.setPolNbr(polNbr);
        bilActSummary.setBilAcyDesCd(bilAcyDesCd);
        bilActSummary.setBilDesReaTyp(bilDesReaTyp);
        bilActSummary.setBasAddDataTxt(SEPARATOR_BLANK);

        bilActSummary.setBilAcyDes1Dt(adjustDueDate);
        bilActSummary.setBilAcyDes2Dt(DateRoutine.defaultDateTime());
        bilActSummary.setBilAcyAmt(amount);
        bilActSummary.setUserId(userId);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());

        bilActSummaryRepository.saveAndFlush(bilActSummary);

    }

    private Short getBilAcySeq(String accountId, ZonedDateTime se3DateTime) {
        Short bilAcySeq = 0;
        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, se3DateTime.with(LocalTime.MIN));
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        return bilAcySeq;
    }

    private String[] readBilMstCoSt(String bilClassCode, String bilTypeCode) {
        String masterCompany = BillingConstants.MASTER_COMPANY_NUMBER;
        String riskState = BLANK_STRING;
        BilMstCoSt bilMstCoSt = bilMstCoStRepository.findRow(bilClassCode, bilTypeCode, BLANK_STRING);
        if (bilMstCoSt != null) {
            masterCompany = bilMstCoSt.getMasterCompanyNbr();
            riskState = bilMstCoSt.getBilStatePvnCd();
        }
        return new String[] { masterCompany, riskState };
    }

    private void callFinancialApi(Double amount, ZonedDateTime applicationDate, String bankCode, String accountNumber,
            String transactionObjectCode, String transactionActionCode, String paybleItemCode, String sourceCode,
            String paymentType, String bilReasonCode, String bilTypeCode, String bilClassCode,
            ZonedDateTime referenceDate, String databaseKey, String operatorId, String orginalEffectiveDate,
            String applicationProgram, String policyId, String countryCode, String lineOfBusiness, String masterCompany,
            String riskState, String currencyCode) {

        FinancialApiActivity financialApiActivity = new FinancialApiActivity();

        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setFolderId(BLANK_STRING);
        financialApiActivity.setApplicationName(ApplicationName.BCMOCA.toString());
        financialApiActivity.setUserId(BLANK_STRING);
        financialApiActivity.setErrorCode(ERROR_CODE);

        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setMasterCompanyNbr(masterCompany);
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setFinancialIndicator(FinancialIndicator.ENABLE.getValue());
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setAgentTtyId(BLANK_STRING);
        financialApiActivity.setAppProgramId(applicationProgram);
        financialApiActivity.setBilAccountId(accountNumber);
        financialApiActivity.setBilBankCode(bankCode);
        financialApiActivity.setBilClassCode(bilClassCode);
        financialApiActivity.setBilDatabaseKey(databaseKey);
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setBilReceiptTypeCode(paymentType);
        financialApiActivity.setBilReasonCode(bilReasonCode);
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(referenceDate));
        financialApiActivity.setBilSourceCode(sourceCode);
        financialApiActivity.setBilTypeCode(bilTypeCode);
        financialApiActivity.setClientId(BLANK_STRING);
        financialApiActivity.setCountryCode(countryCode);
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setCurrencyCode(currencyCode);
        financialApiActivity.setDepositeBankCode(BLANK_STRING);
        financialApiActivity.setDisbursementId(BLANK_STRING);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(applicationDate));
        financialApiActivity.setLineOfBusCode(lineOfBusiness);
        financialApiActivity.setMarketSectorCode(BLANK_STRING);
        financialApiActivity.setOriginalEffectiveDate(orginalEffectiveDate);
        financialApiActivity.setOperatorId(operatorId);
        financialApiActivity.setPayableItemCode(paybleItemCode);
        financialApiActivity.setPolicyId(policyId);
        financialApiActivity.setStateCode(riskState);
        financialApiActivity.setStatusCode(BLANK_CHAR);

        String[] fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {

            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }
}
