package billing.handler.impl;

import static billing.utils.BillingConstants.SYSTEM_USER_ID;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_C;
import static core.utils.CommonConstants.CHAR_H;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_V;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_Z;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilBillPlan;
import billing.data.entity.BilCashReceipt;
import billing.data.entity.BilPolActivity;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRenDays;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilPolActivityId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.BilRenDaysId;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilAmountsRepository;
import billing.data.repository.BilBillPlanRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCashReceiptRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.handler.model.PcnRescindCollection;
import billing.handler.service.AccountReviewService;
import billing.handler.service.ReinstatementService;
import billing.handler.service.RescissionService;
import billing.utils.BillingConstants.ActivityType;
import billing.utils.BillingConstants.BilAcyDesCode;
import billing.utils.BillingConstants.BilAmountsEftIndicator;
import billing.utils.BillingConstants.BilDesReasonType;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BilReceiptTypeCode;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CreditIndicator;
import billing.utils.BillingConstants.ManualSuspendIndicator;
import billing.utils.BillingConstants.PcnReasonType;
import billing.utils.BillingConstants.ReinstatePrintCode;
import billing.utils.BillingConstants.ReinstatementTypeCode;
import billing.utils.BillingConstants.ReverseReSuspendIndicator;
import billing.utils.PB360Service;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;

@Service
public class ReinstatementServiceImpl implements ReinstatementService {

    @Autowired
    private DateService dateService;

    @Autowired
    private RescissionService rescissionService;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilBillPlanRepository bilBillPlanRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilCashReceiptRepository bilCashReceiptRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilAmountsRepository bilAmountsRepository;

    @Autowired
    private BillingApiService billingApiService;

    @Autowired
    private PB360Service pb360Service;

    @Autowired
    private AccountReviewService accountReviewService;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;
    
    @Value("${language}")
    private String language;

    private static final String ACCOUNT_PROCESSING_AND_RECOVERY = "BCMOAX";
    private static final String DEFAULT_MASTER_CODE = "99";
    private static final String LAST_DAY_TO_REINSTATE_NOT_FOUND = "last.day.to.reinstate.not.found";
    private static final char USE_POSTMARK_DATE = CHAR_Y;

    @Override
    public void checkReinstatement(BilAccount bilAccount, Short bilDtbSequence, ZonedDateTime recoveryDate, String userSequenceId) {
        String accountId = bilAccount.getAccountId();
        Short maxPaymentSequence = rescissionService.checkForPayment(CHAR_N, accountId, BLANK_STRING, BLANK_STRING,
                bilDtbSequence, recoveryDate);
        if (maxPaymentSequence == null) {
            return;
        }
        Short maxCreditSequence = rescissionService.checkCreditInd(accountId, recoveryDate);

        Short seqNbrOfLastPayment = SHORT_ZERO;
        Double accumCoffTotalAmount = DECIMAL_ZERO;
        ZonedDateTime cancelGraceDate = DateRoutine.defaultDateTime();
        ZonedDateTime minLastDayReinstateDate = DateRoutine.defaultDateTime();
        ZonedDateTime minPcnAcyDate = DateRoutine.defaultDateTime();
        BilRenDays bilRenDays = null;
        List<Object> cancelCashObject = new ArrayList<>();
        List<Map<String, Object>> lastDayReinstateList = new ArrayList<>();

        if (maxCreditSequence == null || maxCreditSequence != null && maxPaymentSequence == null
                || maxPaymentSequence != null && maxCreditSequence != null && maxCreditSequence > maxPaymentSequence) {
            lastDayReinstateList = buildLastDayReinstateData(accountId, recoveryDate, cancelCashObject,
                    minLastDayReinstateDate, minPcnAcyDate, cancelGraceDate);
            if (lastDayReinstateList == null || lastDayReinstateList.isEmpty()) {
                return;
            }
            if (!cancelCashObject.isEmpty()) {
                seqNbrOfLastPayment = (Short) cancelCashObject.get(0);
                minPcnAcyDate = (ZonedDateTime) cancelCashObject.get(1);
                minLastDayReinstateDate = (ZonedDateTime) cancelCashObject.get(2);
                accumCoffTotalAmount = (Double) cancelCashObject.get(3);
                cancelGraceDate = (ZonedDateTime) cancelCashObject.get(4);
                bilRenDays = (BilRenDays) cancelCashObject.get(cancelCashObject.size() - 1);
            }
        }
        if (bilRenDays == null) {
            bilRenDays = new BilRenDays();
            bilRenDays.setBilRenDaysId(new BilRenDaysId());
        }

        List<BilCashReceipt> bilCashReceipts = bilCashReceiptRepository.getPaymentReceiptsByDate(accountId,
                minPcnAcyDate, seqNbrOfLastPayment);

        Double polDirAccum = DECIMAL_ZERO;
        Double actSpAccum = DECIMAL_ZERO;
        ZonedDateTime maxRecvDate = DateRoutine.defaultDateTime();
        ZonedDateTime maxPostmarkDate = DateRoutine.defaultDateTime();
        Double[] bilCashResultObject = null;
        if (bilCashReceipts != null && !bilCashReceipts.isEmpty()) {
            BilCashReceipt bilCashReceipt = bilCashReceipts.get(0);
            if (maxRecvDate.compareTo(DateRoutine.defaultDateTime()) == 0
                    || bilCashReceipt.getBilCashReceiptId().getBilDtbDate().compareTo(maxRecvDate) > 0) {
                maxRecvDate = bilCashReceipt.getBilCashReceiptId().getBilDtbDate();
            }
            if (bilCashReceipt.getPostMarkDate().compareTo(cancelGraceDate) <= 0
                    && (maxPostmarkDate.compareTo(DateRoutine.defaultDateTime()) == 0
                            || bilCashReceipt.getPostMarkDate().compareTo(maxPostmarkDate) > 0)) {
                maxPostmarkDate = bilCashReceipt.getPostMarkDate();
            }

            bilCashResultObject = processBilCashReceipt(accountId, bilCashReceipt, cancelGraceDate,
                    lastDayReinstateList, minLastDayReinstateDate, bilRenDays);
        }
        if (bilCashResultObject != null) {
            polDirAccum = bilCashResultObject[0];
            actSpAccum = bilCashResultObject[1];
        }

        makeReinstatementRequest(accountId, bilRenDays, actSpAccum, polDirAccum, accumCoffTotalAmount,
                lastDayReinstateList, maxRecvDate, maxPostmarkDate, bilAccount, recoveryDate, 
                userSequenceId);
    }

    private void makeReinstatementRequest(String accountId, BilRenDays bilRenDays, Double actSpAccum,
            Double polDirAccum, Double accumCoffTotalAmount, List<Map<String, Object>> lastDaytoReinstateList,
            ZonedDateTime maxRecvDate, ZonedDateTime maxPostmarkDate, BilAccount bilAccount,
            ZonedDateTime recoveryDate, String userSequenceId) {
        char prtReason = BLANK_CHAR;
        boolean isLdrPrtBas = false;
        boolean isPrintTgr = false;
        boolean isCheckPolicyDir = false;
        boolean isCheckAmount = false;
        Object[] reinstateTriggerObject = null;
        Double peiBilRctAmount = DECIMAL_ZERO;
        Double canCreAmount = bilCashDspRepository.getDspAmountForCanCredit(accountId,
                ManualSuspendIndicator.ENABLED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                creditIndicatorList(), BLANK_CHAR, BilDspTypeCode.SUSPENDED.getValue());
        if (canCreAmount == null) {
            canCreAmount = DECIMAL_ZERO;
        }

        if (actSpAccum == DECIMAL_ZERO && polDirAccum == DECIMAL_ZERO && canCreAmount == DECIMAL_ZERO) {
            prtReason = CHAR_C;
            if (bilRenDays.getBrdPstmrkDtInd() == USE_POSTMARK_DATE
                    && bilRenDays.getBrdRenTypeCd() != ReinstatementTypeCode.SPACE.getValue()) {
                prtReason = CHAR_P;
            }

            isLdrPrtBas = true;
        } else if (actSpAccum > DECIMAL_ZERO && polDirAccum == DECIMAL_ZERO) {
            prtReason = CHAR_C;
            if (actSpAccum >= accumCoffTotalAmount) {
                reinstateTriggerObject = processReinstateTrigger(accountId, lastDaytoReinstateList, isCheckAmount,
                        peiBilRctAmount, isLdrPrtBas, isCheckPolicyDir, isPrintTgr, maxRecvDate, bilRenDays,
                        maxPostmarkDate, recoveryDate, userSequenceId);
                if (reinstateTriggerObject != null) {
                    isLdrPrtBas = (boolean) reinstateTriggerObject[0];
                }
            } else {
                isLdrPrtBas = true;
            }
        } else if (actSpAccum == DECIMAL_ZERO && polDirAccum > DECIMAL_ZERO) {
            prtReason = CHAR_C;
            isCheckAmount = true;
            reinstateTriggerObject = processReinstateTrigger(accountId, lastDaytoReinstateList, isCheckAmount,
                    peiBilRctAmount, isLdrPrtBas, isCheckPolicyDir, isPrintTgr, maxRecvDate, bilRenDays,
                    maxPostmarkDate, recoveryDate, userSequenceId);
            if (reinstateTriggerObject != null) {
                isLdrPrtBas = (boolean) reinstateTriggerObject[0];
            }
        } else if (actSpAccum > DECIMAL_ZERO && polDirAccum > DECIMAL_ZERO) {
            prtReason = CHAR_C;
            isCheckPolicyDir = true;
            reinstateTriggerObject = processReinstateTrigger(accountId, lastDaytoReinstateList, isCheckAmount,
                    peiBilRctAmount, isLdrPrtBas, isCheckPolicyDir, isPrintTgr, maxRecvDate, bilRenDays,
                    maxPostmarkDate, recoveryDate, userSequenceId);
            if (reinstateTriggerObject != null) {
                isLdrPrtBas = (boolean) reinstateTriggerObject[0];
                peiBilRctAmount = (Double) reinstateTriggerObject[1];
            }

            Double newReinstTotal = calculateNewReinstTotal(lastDaytoReinstateList);
            if (newReinstTotal > DECIMAL_ZERO) {
                reinstateTriggerObject = checkAccountDir(actSpAccum, newReinstTotal, peiBilRctAmount, isLdrPrtBas,
                        accountId, lastDaytoReinstateList, maxRecvDate, bilRenDays, maxPostmarkDate, recoveryDate,
                        userSequenceId);
            } else if (newReinstTotal == DECIMAL_ZERO) {
                isPrintTgr = true;
                reinstateTriggerObject = processReinstateTrigger(accountId, lastDaytoReinstateList, isCheckAmount,
                        peiBilRctAmount, isLdrPrtBas, isCheckPolicyDir, isPrintTgr, maxRecvDate, bilRenDays,
                        maxPostmarkDate, recoveryDate, userSequenceId);
            }

            if (reinstateTriggerObject != null) {
                isLdrPrtBas = (boolean) reinstateTriggerObject[0];
            }
        }

        if (isLdrPrtBas) {
            printLastDayReinstateRequest(accountId, bilRenDays.getBrdRenPrtCd(), lastDaytoReinstateList, prtReason,
                    bilAccount, recoveryDate);
        }
    }

    private void printLastDayReinstateRequest(String accountId, char renPrintCode,
            List<Map<String, Object>> lastDaytoReinstateList, char prtReason, BilAccount bilAccount,
            ZonedDateTime recoveryDate) {

        if (renPrintCode == ReinstatePrintCode.CASH_RECEIVED.getValue()) {
            insertNreNrdSummary(accountId, BilAcyDesCode.NO_AUTO_REINSTATEMENT.getValue(), recoveryDate);

            String masterCompany = BLANK_STRING;
            String riskState = BLANK_STRING;
            for (Map<String, Object> lastDayReinstate : lastDaytoReinstateList) {
                String lastPolicyId = (String) lastDayReinstate.get(LastDayReinstate.POLICYID.toString());
                if (lastPolicyId == null || lastPolicyId.trim().isEmpty()) {
                    continue;
                }

                masterCompany = (String) lastDayReinstate.get(LastDayReinstate.MASTERCOMPANY.toString());
                riskState = (String) lastDayReinstate.get(LastDayReinstate.STATEPVNCD.toString());
            }

            lastDayReinsPrintTrigger(accountId, bilAccount.getPayorClientId(), prtReason, bilAccount.getAccountNumber(),
                    masterCompany, riskState, recoveryDate);

        } else if (renPrintCode == ReinstatePrintCode.BATCH.getValue()) {
            for (Map<String, Object> lastDayReinstate : lastDaytoReinstateList) {
                String lastPolicyId = (String) lastDayReinstate.get(LastDayReinstate.POLICYID.toString());
                if (lastPolicyId == null || lastPolicyId.trim().isEmpty()) {
                    continue;
                }
                Character lastPrintTgr = (Character) lastDayReinstate.get(LastDayReinstate.PRINTTGR.toString());
                if (lastPrintTgr != null && lastPrintTgr == CHAR_Y) {
                    ZonedDateTime polEffectiveDate = (ZonedDateTime) lastDayReinstate
                            .get(LastDayReinstate.POLEFFECTIVEDATE.toString());
                    writeNreActivityNfo(accountId, lastPolicyId, polEffectiveDate, prtReason, recoveryDate);
                }
            }
        }
    }

    private List<Map<String, Object>> buildLastDayReinstateData(String accountId, ZonedDateTime recoveryDate,
            List<Object> cancelCashObject, ZonedDateTime minLastDayReinstateDate, ZonedDateTime minPcnAcyDate,
            ZonedDateTime cancelGraceDate) {
        String prevPolicyId = null;
        String prevPlanCode = null;
        BilBillPlan bilBillPlan = null;
        BilPolicy bilPolicy = null;
        BilRenDays bilRenDays = null;
        List<Map<String, Object>> lastDaytoReinstateList = new ArrayList<>();
        List<BilPolActivity> bilPolActivities = bilPolActivityRepository.fetchActivityByNextAcyDate(accountId,
                ActivityType.LAST_DAY_REINSTATE.getValue(), recoveryDate);
        for (BilPolActivity bilPolActivity : bilPolActivities) {
            ZonedDateTime lastDayReinstateDate = bilPolActivity.getBilNxtAcyDt();
            BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository.findRenewalTermIsPresent(
                    bilPolActivity.getBillPolActivityId().getBilAccountId(),
                    bilPolActivity.getBillPolActivityId().getPolicyId(),
                    bilPolActivity.getBillPolActivityId().getPolEffectiveDt(), polStatusCodeList());
            if (bilPolicyTerm == null) {
                continue;
            }

            if (isCancelCashCheck(bilPolicyTerm.getBillPolStatusCd())) {
                bilRenDays = rescissionService.readBilRenday(bilPolicyTerm);
                if (prevPlanCode == null || !prevPlanCode.equalsIgnoreCase(bilPolicyTerm.getBillPlanCd())) {
                    bilBillPlan = getBilBillPlan(bilPolicyTerm.getBillPlanCd());
                }

                if (prevPolicyId == null
                        || !prevPolicyId.equalsIgnoreCase(bilPolicyTerm.getBillPolicyTermId().getPolicyId())) {
                    bilPolicy = getBilPolicy(bilPolicyTerm.getBillPolicyTermId().getPolicyId(), accountId);
                }
                lookAtCancelCash(accountId, bilPolicyTerm, bilBillPlan, bilPolicy, recoveryDate,
                        bilPolActivity.getBillPolActivityId().getPolEffectiveDt(), minLastDayReinstateDate,
                        lastDayReinstateDate, cancelGraceDate, minPcnAcyDate, lastDaytoReinstateList,
                        bilRenDays.getBrdRenTypeCd(), cancelCashObject);
            }

            prevPlanCode = bilPolicyTerm.getBillPlanCd().trim();
            prevPolicyId = bilPolicyTerm.getBillPolicyTermId().getPolicyId().trim();
        }

        if (bilRenDays != null) {
            cancelCashObject.add(cancelCashObject.size(), bilRenDays);
        }

        return lastDaytoReinstateList;
    }

    private boolean isCancelCashCheck(char billPolStatusCd) {
        return Arrays
                .asList(BilPolicyStatusCode.FLAT_CANCELLATION.getValue(), BilPolicyStatusCode.CANCELLED.getValue(),
                        BilPolicyStatusCode.FLATCANCELLED_FOR_REISSUED.getValue(),
                        BilPolicyStatusCode.CANCELLED_FOR_REISSUED.getValue(),
                        BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue(),
                        BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue())
                .stream().anyMatch(polStatusCode -> polStatusCode == billPolStatusCd);
    }

    private void writeNreActivityNfo(String accountId, String lastPolicyId, ZonedDateTime polEffectiveDate,
            char prtReason, ZonedDateTime recoveryDate) {

        BilPolActivityId bilPolActivityId = new BilPolActivityId(accountId, lastPolicyId, polEffectiveDate,
                BilAcyDesCode.NO_AUTO_REINSTATEMENT.getValue());
        BilPolActivity bilPolActivity = bilPolActivityRepository.findById(bilPolActivityId).orElse(null);
        if (bilPolActivity != null) {
            List<BilPolActivity> bilPolActivities = bilPolActivityRepository.fetchNrdActivityTriger(accountId,
                    lastPolicyId, polEffectiveDate, new ArrayList<>(Arrays.asList(BilDesReasonType.REC.getValue(),
                            BilDesReasonType.MAN_REINV_CANCELLED.getValue())));
            if (bilPolActivities != null && !bilPolActivities.isEmpty()) {
                bilPolActivityRepository.deleteAllInBatch(bilPolActivities);
            }
        }

        String bilAcyTypeCode = "RE" + prtReason;
        bilPolActivityId = new BilPolActivityId(accountId, lastPolicyId, polEffectiveDate, bilAcyTypeCode);
        bilPolActivity = bilPolActivityRepository.findById(bilPolActivityId).orElse(null);
        if (bilPolActivity == null) {
            bilPolActivity = new BilPolActivity();
            bilPolActivity.setBillPolActivityId(bilPolActivityId);
            bilPolActivity.setBilNxtAcyDt(recoveryDate);
            bilPolActivityRepository.save(bilPolActivity);
        }
    }

    @Override
    public void lastDayReinsPrintTrigger(String accountId, String payorClientId, char prtReason, String accountNumber,
            String masterCompany, String riskState, ZonedDateTime recoveryDate) {
        final String refFromId = "NONREINST";
        final String businessTransaction = "REINSTATE NOTICE";

        if (masterCompany == null || masterCompany.trim().isEmpty()) {
            masterCompany = DEFAULT_MASTER_CODE;
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(CHAR_A);
        stringBuilder.append(StringUtils.leftPad(DateRoutine.dateTimeAsYYYYMMDDString(recoveryDate), 10, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad("01", 2, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(BLANK_STRING, 20, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(accountNumber, 30, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(masterCompany, 2, BLANK_STRING));
        stringBuilder.append(StringUtils.leftPad(riskState, 3, BLANK_STRING));
        stringBuilder.append(prtReason);
        String additionalKey = stringBuilder.toString();

        pb360Service.postGeneratePrintNoticeRequest(accountId, ACCOUNT_PROCESSING_AND_RECOVERY, payorClientId,
                refFromId, additionalKey, businessTransaction);
    }

    @Override
    public void insertNreNrdSummary(String accountId, String bilAcyDesCd, ZonedDateTime recoveryDate) {
        String preBilAcyDes = BLANK_STRING;
        ZonedDateTime maxBilAcyDate = bilActSummaryRepository.findByMaxAcyDateBilAccountId(accountId);
        if (maxBilAcyDate != null) {
            Short maxSeqNumber = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, maxBilAcyDate);
            if (maxSeqNumber != null) {
                BilActSummary bilActSummary = bilActSummaryRepository
                        .findById(new BilActSummaryId(accountId, maxBilAcyDate, maxSeqNumber)).orElse(null);
                if (bilActSummary != null) {
                    preBilAcyDes = bilActSummary.getBilAcyDesCd().trim();
                }
            }
        }
        if (preBilAcyDes.equalsIgnoreCase(bilAcyDesCd)) {
            return;
        }

        writeHistory(accountId, bilAcyDesCd, BLANK_STRING, BLANK_STRING, BLANK_STRING, DateRoutine.defaultDateTime(),
                DateRoutine.defaultDateTime(), BLANK_STRING, SYSTEM_USER_ID, recoveryDate);
    }

    private Object[] checkAccountDir(Double actSpAccum, Double newReinstTotal, Double peiBilRctAmount,
            boolean isLdrPrtBas, String accountId, List<Map<String, Object>> lastDaytoReinstateList,
            ZonedDateTime maxRecvDate, BilRenDays bilRenDays, ZonedDateTime maxPostmarkDate,
            ZonedDateTime recoveryDate, String userSequenceId) {
        boolean isPrintTgr = false;
        if (actSpAccum < newReinstTotal) {
            isPrintTgr = true;
            peiBilRctAmount = actSpAccum;
            isLdrPrtBas = true;
        }
        Object[] reinstateTriggerObject = processReinstateTrigger(accountId, lastDaytoReinstateList, false,
                peiBilRctAmount, isLdrPrtBas, false, isPrintTgr, maxRecvDate, bilRenDays, maxPostmarkDate,
                recoveryDate, userSequenceId);
        if (reinstateTriggerObject != null) {
            isLdrPrtBas = (boolean) reinstateTriggerObject[0];
            peiBilRctAmount = (Double) reinstateTriggerObject[1];
        }

        return new Object[] { isLdrPrtBas, peiBilRctAmount };
    }

    private Double calculateNewReinstTotal(List<Map<String, Object>> lastDaytoReinstateList) {
        Double newReinstTotal = DECIMAL_ZERO;
        for (Map<String, Object> lastDayReinstate : lastDaytoReinstateList) {
            String lastPolicyId = (String) lastDayReinstate.get(LastDayReinstate.POLICYID.toString());
            if (lastPolicyId == null || lastPolicyId.trim().isEmpty()) {
                continue;
            }

            Character lastPrintTgr = (Character) lastDayReinstate.get(LastDayReinstate.PRINTTGR.toString());
            if (lastPrintTgr != null && lastPrintTgr == CHAR_Y) {
                Double directedAmount = (Double) lastDayReinstate.get(LastDayReinstate.DIRECTEDAMT.toString());
                Double lastReinstateTotal = (Double) lastDayReinstate.get(LastDayReinstate.REINSTTOTAL.toString());
                Double reinstDifferent = lastReinstateTotal - directedAmount;
                newReinstTotal += reinstDifferent;
            }
        }
        return newReinstTotal;
    }

    private Object[] processReinstateTrigger(String accountId, List<Map<String, Object>> lastDaytoReinstateList,
            boolean isCheckAmount, Double peiBilRctAmount, boolean isLdrPrtBas, boolean isCheckPolicyDir,
            boolean isPrintTgr, ZonedDateTime maxRecvDate, BilRenDays bilRenDays, ZonedDateTime maxPostmarkDate,
            ZonedDateTime recoveryDate, String userSequenceId) {

        for (Map<String, Object> lastDayReinstate : lastDaytoReinstateList) {
            String lastPolicyId = (String) lastDayReinstate.get(LastDayReinstate.POLICYID.type);
            if (lastPolicyId == null || lastPolicyId.trim().isEmpty()) {
                continue;
            }
            if (isPrintTgr) {
                Character lastPrintTgr = (Character) lastDayReinstate.get(LastDayReinstate.PRINTTGR.type);
                if (lastPrintTgr == null || lastPrintTgr == CHAR_Y) {
                    continue;
                }
            }
            if (isCheckAmount) {
                Double directedAmount = (Double) lastDayReinstate.get(LastDayReinstate.DIRECTEDAMT.type);
                Double lastReinstateTotal = (Double) lastDayReinstate.get(LastDayReinstate.REINSTTOTAL.type);
                if (directedAmount < lastReinstateTotal) {
                    peiBilRctAmount += directedAmount;
                    isLdrPrtBas = true;
                    continue;
                }
            }

            String issueSystemId = (String) lastDayReinstate.get(LastDayReinstate.ISSUESYSTEM.type);
            String masterCompany = (String) lastDayReinstate.get(LastDayReinstate.MASTERCOMPANY.type);
            String statePvnCode = (String) lastDayReinstate.get(LastDayReinstate.STATEPVNCD.type);
            String lobCode = (String) lastDayReinstate.get(LastDayReinstate.LOBCD.type);
            String polSymbolCd = (String) lastDayReinstate.get(LastDayReinstate.POLSYMBOLCD.type);
            String polNumber = (String) lastDayReinstate.get(LastDayReinstate.POLNUMBER.type);
            ZonedDateTime polEfftiveDate = (ZonedDateTime) lastDayReinstate.get(LastDayReinstate.POLEFFECTIVEDATE.type);
            ZonedDateTime planExpDate = (ZonedDateTime) lastDayReinstate.get(LastDayReinstate.PLANEXPDATE.type);
            char renTypeCode = (char) lastDayReinstate.get(LastDayReinstate.RENTYPECD.type);

            lastDayReinstate.put(LastDayReinstate.PRINTTGR.type, CHAR_N);

            if (!isCheckPolicyDir) {
                initiateReinstate(accountId, lastPolicyId, issueSystemId, polEfftiveDate, planExpDate, polNumber,
                        polSymbolCd, renTypeCode, maxRecvDate, masterCompany, statePvnCode, lobCode, bilRenDays,
                        maxPostmarkDate, recoveryDate, userSequenceId);

                deleteLDRTrigger(accountId, lastPolicyId, polEfftiveDate);
            }
        }

        return new Object[] { isLdrPrtBas, peiBilRctAmount };
    }

    @Override
    public void deleteLDRTrigger(String accountId, String lastPolicyId, ZonedDateTime polEfftiveDate) {
        List<BilPolActivity> bilPolActivities = bilPolActivityRepository.fetchNrdActivityTriger(accountId, lastPolicyId,
                polEfftiveDate, new ArrayList<>(Arrays.asList(ActivityType.LAST_DAY_REINSTATE.getValue(),
                        ActivityType.CANCEL_GRACE_DATE.getValue(), BilAcyDesCode.NO_AUTO_REINSTATEMENT.getValue())));
        if (bilPolActivities != null && !bilPolActivities.isEmpty()) {
            bilPolActivityRepository.deleteAllInBatch(bilPolActivities);
        }
    }

    private void initiateReinstate(String accountId, String lastPolicyId, String issueSystemId,
            ZonedDateTime polEfftiveDate, ZonedDateTime planExpDate, String polNumber, String polSymbolCd,
            char renTypeCode, ZonedDateTime maxRecvDate, String masterCompany, String statePvnCode, String lobCode,
            BilRenDays bilRenDays, ZonedDateTime maxPostmarkDate, ZonedDateTime recoveryDate, String userSequenceId) {
        List<Character> bamAmtPrcIndList = new ArrayList<>(Arrays.asList(CHAR_H, CHAR_V));
        String apiTypeCode;
        boolean isPendPolAdminTrans = checkForPendingTranfer(accountId, polNumber, polSymbolCd, recoveryDate);
        if (issueSystemId.trim().isEmpty()) {
            issueSystemId = "UW";
        }
        if (isPendPolAdminTrans) {
            return;
        }
        if (renTypeCode == ReinstatementTypeCode.REISSUE_NEWTERM.getValue()
                || renTypeCode == ReinstatementTypeCode.REINSTATE_LAPSE_REISSUE_NEWTERM.getValue()
                        && planExpDate.compareTo(maxRecvDate) < 0) {
            apiTypeCode = BilAcyDesCode.AUTO_REISSUE_NEW_TERM.getValue();
            newTermReinst(accountId, lastPolicyId, issueSystemId, planExpDate, polNumber, polSymbolCd, maxRecvDate,
                    masterCompany, statePvnCode, lobCode, apiTypeCode, recoveryDate, userSequenceId);
        } else {
            apiTypeCode = BilAcyDesCode.REQUEST_AUTO_REINSTATEMENT.getValue();
        }

        ZonedDateTime maxBilAmountTimeStamp = bilAmountsRepository.getMaxReInstateTime(accountId, lastPolicyId,
                planExpDate, bilAmtEffIndicatorList(),
                new ArrayList<>(Arrays.asList(BilDesReasonType.PRORATA_CANCELLATION_NONPAYMENT_PREMIUMS.getValue(),
                        BilDesReasonType.RENEWAL_NONPAYMENT_CANCELLATION.getValue())),
                bamAmtPrcIndList);
        if (maxBilAmountTimeStamp == null) {
            throw new DataNotFoundException("new.term.resinst", new Object[] { "statement #2" });
        }
        Character bilAmtEftInd = BLANK_CHAR;
        ZonedDateTime bilAmtEftDate = null;

        List<Object[]> amountEftsObject = bilAmountsRepository.getDistinctAmtEfts(accountId, lastPolicyId,
                maxBilAmountTimeStamp, bilAmtEffIndicatorList(), bamAmtPrcIndList);
        if (amountEftsObject != null && !amountEftsObject.isEmpty()) {
            bilAmtEftInd = (Character) amountEftsObject.get(0)[0];
            bilAmtEftDate = (ZonedDateTime) amountEftsObject.get(0)[1];
        }

        char bilEftTypeCode = BLANK_CHAR;
        String polPcnReason = BLANK_STRING;
        ZonedDateTime reinstateEffDate = null;
        if (renTypeCode == ReinstatementTypeCode.FLAT.getValue()
                || renTypeCode == ReinstatementTypeCode.LAPSE.getValue()
                || renTypeCode == ReinstatementTypeCode.REINSTATE_LAPSE_REISSUE_NEWTERM.getValue()) {
            reinstateEffDate = bilAmtEftDate;
            if (bilAmtEftInd.equals(BilAmountsEftIndicator.CANCELLATION_F.getValue())) {
                bilEftTypeCode = BilAmountsEftIndicator.REINSTATEMENT_K.getValue();
                polPcnReason = PcnReasonType.KFR.getValue();
            } else if (bilAmtEftInd.equals(BilAmountsEftIndicator.CANCELLATION_P.getValue())) {
                bilEftTypeCode = BilAmountsEftIndicator.REINSTATEMENT_O.getValue();
                polPcnReason = PcnReasonType.OPR.getValue();
            } else if (bilAmtEftInd.equals(BilAmountsEftIndicator.CANCELLATION_S.getValue())) {
                bilEftTypeCode = BilAmountsEftIndicator.REINSTATEMENT_T.getValue();
                polPcnReason = PcnReasonType.SHORTRATE_REINSTATEMENT.getValue();
            }

            if (renTypeCode == ReinstatementTypeCode.LAPSE.getValue()
                    || renTypeCode == ReinstatementTypeCode.REINSTATE_LAPSE_REISSUE_NEWTERM.getValue()) {
                if (bilRenDays.getBrdPstmrkDtInd() == USE_POSTMARK_DATE) {
                    reinstateEffDate = maxPostmarkDate;
                    if (bilAmtEftDate != null && bilAmtEftDate.compareTo(maxPostmarkDate) > 0) {
                        reinstateEffDate = bilAmtEftDate;
                    }
                } else {
                    reinstateEffDate = maxRecvDate;
                    if (bilAmtEftDate != null && maxRecvDate.compareTo(bilAmtEftDate) < 0) {
                        reinstateEffDate = bilAmtEftDate;
                    }
                }

                if (bilAmtEftDate != null && reinstateEffDate != null
                        && bilAmtEftDate.compareTo(reinstateEffDate) < 0) {
                    if (bilAmtEftInd.equals(BilAmountsEftIndicator.CANCELLATION_F.getValue())) {
                        bilEftTypeCode = BilAmountsEftIndicator.REINSTATEMENT_L.getValue();
                        polPcnReason = PcnReasonType.FLAT_REINSTATEMENT_LAPSE.getValue();
                    } else if (bilAmtEftInd.equals(BilAmountsEftIndicator.CANCELLATION_P.getValue())) {
                        bilEftTypeCode = BilAmountsEftIndicator.REINSTATEMENT_Y.getValue();
                        polPcnReason = PcnReasonType.PRORATA_REINSTATEMENT_WITH_LAPSE.getValue();
                    } else if (bilAmtEftInd == CHAR_S) {
                        bilEftTypeCode = CHAR_Z;
                        polPcnReason = PcnReasonType.SHORTRATE_REINSTATEMENT_LAPSE.getValue();
                    }
                }
            }
        }

        PcnRescindCollection pcnRescindCollection = new PcnRescindCollection();
        pcnRescindCollection.setApiType(apiTypeCode);
        pcnRescindCollection.setAccountId(accountId);
        pcnRescindCollection.setPolicyId(lastPolicyId);
        pcnRescindCollection.setActivityDate(recoveryDate);
        pcnRescindCollection.setOriginEffectiveDate(polEfftiveDate);
        pcnRescindCollection.setEffectiveDate(reinstateEffDate);
        pcnRescindCollection.setPlanExpDate(planExpDate);
        pcnRescindCollection.setPolPcnReason(polPcnReason);
        pcnRescindCollection.setEffTypeCode(bilEftTypeCode);
        pcnRescindCollection.setPolNumber(polNumber);
        pcnRescindCollection.setPolSymbolCode(polSymbolCd);
        pcnRescindCollection.setIssueSysId(issueSystemId);
        pcnRescindCollection.setMasterCompanyNumber(masterCompany);
        pcnRescindCollection.setStatePvnCode(statePvnCode);
        pcnRescindCollection.setLineOfBusinessCode(lobCode);
        pcnRescindCollection.setUserSequenceId(userSequenceId);
        
        billingApiService.processPcnOrRescindCollection(ACCOUNT_PROCESSING_AND_RECOVERY,
                accountReviewService.getQueueWrtIndicator(), pcnRescindCollection);

        writeHistory(accountId, BilAcyDesCode.REQUEST_AUTO_REINSTATEMENT.getValue(), BLANK_STRING, polNumber,
                polSymbolCd, reinstateEffDate, DateRoutine.defaultDateTime(), BLANK_STRING, SYSTEM_USER_ID,
                recoveryDate);

        insertReinCashTrigger(accountId, lastPolicyId, polEfftiveDate, recoveryDate);
    }

    private void insertReinCashTrigger(String accountId, String lastPolicyId, ZonedDateTime polEfftiveDate,
            ZonedDateTime recoveryDate) {
        BilPolActivityId bilPolActivityId = new BilPolActivityId(accountId, lastPolicyId, polEfftiveDate,
                ActivityType.PCN_NO_RESPONSE_RENEWAL.getValue());
        BilPolActivity bilPolActivity = new BilPolActivity();
        bilPolActivity.setBillPolActivityId(bilPolActivityId);
        bilPolActivity.setBilNxtAcyDt(recoveryDate);
        bilPolActivityRepository.saveAndFlush(bilPolActivity);
    }

    private void newTermReinst(String accountId, String lastPolicyId, String issueSystemId, ZonedDateTime planExpDate,
            String polNumber, String polSymbolCd, ZonedDateTime maxRecvDate, String masterCompany, String statePvnCode,
            String lobCode, String apiTypeCode, ZonedDateTime recoveryDate, String userSequenceId) {
        ZonedDateTime polEfftiveDate = null;
        List<Character> bamAmtPrcIndList = new ArrayList<>(Arrays.asList(CHAR_H));
        ZonedDateTime maxBilAmountTimeStamp = bilAmountsRepository.getMaxReInstateTime(accountId, lastPolicyId,
                planExpDate, bilAmtEffIndicatorList(),
                new ArrayList<>(Arrays.asList(BilDesReasonType.PRORATA_CANCELLATION_NONPAYMENT_PREMIUMS.getValue(),
                        BilDesReasonType.RENEWAL_NONPAYMENT_CANCELLATION.getValue())),
                bamAmtPrcIndList);
        if (maxBilAmountTimeStamp == null) {
            throw new DataNotFoundException("new.term.resinst", new Object[] { "statement #2" });
        }
        ZonedDateTime bilAmtEftDate = null;
        List<Object[]> amountEftsObject = bilAmountsRepository.getDistinctAmtEfts(accountId, lastPolicyId,
                maxBilAmountTimeStamp, bilAmtEffIndicatorList(), bamAmtPrcIndList);
        if (amountEftsObject != null && !amountEftsObject.isEmpty()) {
            bilAmtEftDate = (ZonedDateTime) amountEftsObject.get(0)[1];
        }

        if (bilAmtEftDate != null && maxRecvDate.compareTo(bilAmtEftDate) < 0) {
            polEfftiveDate = bilAmtEftDate;
        } else {
            polEfftiveDate = maxRecvDate;
        }
        String newExpDate = getAnniversaryDate(polEfftiveDate, planExpDate);

        PcnRescindCollection pcnRescindCollection = new PcnRescindCollection();
        pcnRescindCollection.setApiType(apiTypeCode);
        pcnRescindCollection.setActivityDate(recoveryDate);
        pcnRescindCollection.setAccountId(accountId);
        pcnRescindCollection.setPolicyId(lastPolicyId);
        pcnRescindCollection.setEffectiveDate(polEfftiveDate);
        pcnRescindCollection.setPlanExpDate(planExpDate);
        pcnRescindCollection.setPolNumber(polNumber);
        pcnRescindCollection.setPolSymbolCode(polSymbolCd);
        pcnRescindCollection.setIssueSysId(issueSystemId);
        pcnRescindCollection.setMasterCompanyNumber(masterCompany);
        pcnRescindCollection.setStatePvnCode(statePvnCode);
        pcnRescindCollection.setLineOfBusinessCode(lobCode);
        pcnRescindCollection.setEffTypeCode(CHAR_R);
        pcnRescindCollection.setNewExpDate(DateRoutine.dateTimeAsYYYYMMDD(newExpDate));
        pcnRescindCollection.setOriginEffectiveDate(polEfftiveDate);
        pcnRescindCollection.setUserSequenceId(userSequenceId);
        billingApiService.processPcnOrRescindCollection(ACCOUNT_PROCESSING_AND_RECOVERY,
                accountReviewService.getQueueWrtIndicator(), pcnRescindCollection);

        writeHistory(accountId, BilAcyDesCode.AUTO_REISSUE_NEW_TERM.getValue(), BLANK_STRING, polNumber, polSymbolCd,
                polEfftiveDate, DateRoutine.defaultDateTime(), BLANK_STRING, SYSTEM_USER_ID, recoveryDate);
    }

    private String getAnniversaryDate(ZonedDateTime polEfftiveDate, ZonedDateTime planExpDate) {
        int termMonths = getAdjustMonths(polEfftiveDate, planExpDate);
        ZonedDateTime newExpirationDate = polEfftiveDate.plusMonths(termMonths);
        return DateRoutine.dateTimeAsYYYYMMDDString(newExpirationDate);
    }

    private boolean checkForPendingTranfer(String accountId, String polNumber, String polSymbolCd, ZonedDateTime recoveryDate) {

        String returnValue = BLANK_STRING;
        // call PolicyPendingReinstatementCheck - read FOLDER_CONTROL
        // by PolicyId then return code for activityDesType
        String activityDesType = BilAcyDesCode.REISSUE_TRANSACTION.getValue();
        if (returnValue.equalsIgnoreCase("PR")) {
            activityDesType = BilAcyDesCode.REQUEST_AUTO_REINSTATEMENT.getValue();
        }

        writeHistory(accountId, BilAcyDesCode.PND.getValue(), activityDesType, polNumber, polSymbolCd,
                DateRoutine.defaultDateTime(), DateRoutine.defaultDateTime(), BLANK_STRING, SYSTEM_USER_ID, recoveryDate);
        return false;
    }

    private Double[] processBilCashReceipt(String accountId, BilCashReceipt bilCashReceipt,
            ZonedDateTime cancelGraceDate, List<Map<String, Object>> lastDayReinstateList,
            ZonedDateTime minLastDayReinstateDate, BilRenDays bilRenDays) {
        Double polDirAccum = DECIMAL_ZERO;
        Double actSpAccum = DECIMAL_ZERO;

        for (Map<String, Object> lastDaytoReinstate : lastDayReinstateList) {
            String policyId = (String) lastDaytoReinstate.get(LastDayReinstate.POLICYID.type);
            if (policyId == null || policyId.trim().isEmpty()) {
                continue;
            }

            String polNumber = (String) lastDaytoReinstate.get(LastDayReinstate.POLNUMBER.type);
            String polSymbolCd = (String) lastDaytoReinstate.get(LastDayReinstate.POLSYMBOLCD.type);
            Double bilCashAmount = bilCashDspRepository.getDspAmountForReinstateCash(accountId,
                    bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                    bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), polNumber,
                    new ArrayList<>(Arrays.asList(polSymbolCd, BLANK_STRING)),
                    ManualSuspendIndicator.ENABLED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                    creditIndicatorList(), new ArrayList<>(
                            Arrays.asList(BilDspTypeCode.APPLIED.getValue(), BilDspTypeCode.SUSPENDED.getValue())));
            if (bilCashAmount != null) {
                Double directedAmount = (Double) lastDaytoReinstate.get(LastDayReinstate.DIRECTEDAMT.type);
                if (directedAmount == null) {
                    directedAmount = DECIMAL_ZERO;
                }

                if (bilRenDays.getBrdPstmrkDtInd() == USE_POSTMARK_DATE
                        && bilRenDays.getBrdRenTypeCd() != ReinstatePrintCode.SPACE.getValue()
                        && bilCashReceipt.getBilCashReceiptId().getBilDtbDate().compareTo(minLastDayReinstateDate) <= 0
                        && bilCashReceipt.getPostMarkDate().compareTo(cancelGraceDate) <= 0
                        || bilCashReceipt.getBilCashReceiptId().getBilDtbDate()
                                .compareTo(minLastDayReinstateDate) <= 0) {
                    directedAmount += bilCashAmount;
                    polDirAccum += bilCashAmount;
                }

                lastDaytoReinstate.put(LastDayReinstate.DIRECTEDAMT.type, directedAmount);
            }
        }

        Double cashAmountAfterActDir = bilCashDspRepository.getDspAmountAfterActDir(accountId,
                bilCashReceipt.getBilCashReceiptId().getBilDtbDate(),
                bilCashReceipt.getBilCashReceiptId().getDtbSequenceNbr(), BLANK_STRING, BLANK_STRING,
                ManualSuspendIndicator.ENABLED.getValue(), ReverseReSuspendIndicator.BLANK.getValue(),
                creditIndicatorList(), BLANK_CHAR, BilDspTypeCode.SUSPENDED.getValue());
        if (cashAmountAfterActDir != null && (bilRenDays.getBrdPstmrkDtInd() == USE_POSTMARK_DATE
                && bilRenDays.getBrdRenTypeCd() != ReinstatePrintCode.SPACE.getValue()
                && bilCashReceipt.getBilCashReceiptId().getBilDtbDate().compareTo(minLastDayReinstateDate) <= 0
                && bilCashReceipt.getPostMarkDate().compareTo(cancelGraceDate) <= 0
                || bilCashReceipt.getBilCashReceiptId().getBilDtbDate().compareTo(minLastDayReinstateDate) <= 0)) {
            actSpAccum += cashAmountAfterActDir;
        }

        return new Double[] { polDirAccum, actSpAccum };
    }

    private BilPolicy getBilPolicy(String policyId, String accountId) {
        BilPolicy bilPolicy = bilPolicyRepository.findById(new BilPolicyId(policyId, accountId)).orElse(null);
        if (bilPolicy == null) {
            throw new DataNotFoundException("split.policy.not.found", new Object[] { accountId, policyId });
        }
        return bilPolicy;
    }

    private BilBillPlan getBilBillPlan(String billPlanCode) {
        BilBillPlan bilBillPlan = bilBillPlanRepository.findById(billPlanCode).orElse(null);
        if (bilBillPlan == null) {
            throw new DataNotFoundException("no.bil.bill.plan.exits", new Object[] { billPlanCode });
        }
        return bilBillPlan;
    }

    private void lookAtCancelCash(String accountId, BilPolicyTerm bilPolicyTerm, BilBillPlan bilBillPlan,
            BilPolicy bilPolicy, ZonedDateTime recoveryDate, ZonedDateTime bilPolEffDate,
            ZonedDateTime minLastDayReinstateDate, ZonedDateTime lastDayReinstateDate, ZonedDateTime cancelGraceDate,
            ZonedDateTime minPcnAcyDate, List<Map<String, Object>> lastDaytoReinstateList, char bilRenTypeCode,
            List<Object> cancelCashObject) {

        Double accumCoffPctAmount;
        Double accumCoffAmount;
        Double tempValue;
        Double accumCoffTotalAmount = DECIMAL_ZERO;
        short seqNbrOfLastPayment = 0;

        ZonedDateTime maxBilAcyDate = bilActSummaryRepository.findMaxActivityDateByAcyDesCode(accountId,
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), bilAcyDesCodeList(), recoveryDate);
        if (maxBilAcyDate == null) {
            throw new DataNotFoundException(LAST_DAY_TO_REINSTATE_NOT_FOUND,
                    new Object[] { "PCN Date", accountId, bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd() });
        }

        Short maxBilSequenceNumber = bilActSummaryRepository.findByMaxSeqNumberByActivityDate(accountId,
                bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd(), maxBilAcyDate, bilAcyDesCodeList());
        if (maxBilSequenceNumber == null) {
            throw new DataNotFoundException(LAST_DAY_TO_REINSTATE_NOT_FOUND, new Object[] { "PCN Sequence number",
                    accountId, bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd() });
        }

        Integer paymentSameDayCounts = checkPaymentSameDay(accountId, maxBilAcyDate, maxBilSequenceNumber);
        if (paymentSameDayCounts != null) {
            seqNbrOfLastPayment = (short) (paymentSameDayCounts - 1);
        } else {
            seqNbrOfLastPayment = (short) -1;
        }

        BilActSummary bilActSummary = bilActSummaryRepository.fetchBilAmountsByDesCode(accountId, bilPolicy.getPolNbr(),
                bilPolicy.getPolSymbolCd(), maxBilAcyDate, maxBilSequenceNumber, bilAcyDesCodeList());
        if (bilActSummary == null) {
            throw new DataNotFoundException(LAST_DAY_TO_REINSTATE_NOT_FOUND, new Object[] { "PCN Activity amount",
                    accountId, bilPolicy.getPolNbr(), bilPolicy.getPolSymbolCd() });
        }

        Double bilAcyAmounts = bilActSummary.getBilAcyAmt();

        ZonedDateTime bilCGDNextDate = DateRoutine.defaultDateTime();
        BilPolActivity bilPolActivity = bilPolActivityRepository
                .findById(new BilPolActivityId(accountId, bilPolicyTerm.getBillPolicyTermId().getPolicyId(),
                        bilPolEffDate, ActivityType.CANCEL_GRACE_DATE.getValue()))
                .orElse(null);
        if (bilPolActivity != null) {
            bilCGDNextDate = bilPolActivity.getBilNxtAcyDt();
        }

        if (lastDayReinstateDate.compareTo(minLastDayReinstateDate) < 0) {
            minLastDayReinstateDate = lastDayReinstateDate;
        }
        if (bilCGDNextDate.compareTo(cancelGraceDate) < 0
                && cancelGraceDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
            cancelGraceDate = bilCGDNextDate;
        }
        if (maxBilAcyDate.compareTo(minPcnAcyDate) < 0) {
            minPcnAcyDate = maxBilAcyDate;
        }

        Map<String, Object> lastDayReinstate = new HashMap<>();
        lastDayReinstate.put(LastDayReinstate.ISSUESYSTEM.type, bilPolicyTerm.getBptIssueSysId());
        lastDayReinstate.put(LastDayReinstate.MASTERCOMPANY.type, bilPolicyTerm.getMasterCompanyNbr());
        lastDayReinstate.put(LastDayReinstate.STATEPVNCD.type, bilPolicyTerm.getBillStatePvnCd());
        lastDayReinstate.put(LastDayReinstate.LOBCD.type, bilPolicyTerm.getLobCd());
        lastDayReinstate.put(LastDayReinstate.POLICYID.type, bilPolicyTerm.getBillPolicyTermId().getPolicyId());
        lastDayReinstate.put(LastDayReinstate.POLNUMBER.type, bilPolicy.getPolNbr());
        lastDayReinstate.put(LastDayReinstate.POLSYMBOLCD.type, bilPolicy.getPolSymbolCd());
        lastDayReinstate.put(LastDayReinstate.PCNAMOUNT.type, bilAcyAmounts);
        lastDayReinstate.put(LastDayReinstate.POLEFFECTIVEDATE.type,
                bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt());
        lastDayReinstate.put(LastDayReinstate.PLANEXPDATE.type, bilPolicyTerm.getPlnExpDt());
        lastDayReinstate.put(LastDayReinstate.RENTYPECD.type, bilRenTypeCode);
        lastDayReinstate.put(LastDayReinstate.PRINTTGR.type, CHAR_Y);
        if (bilBillPlan.getBbpCoffCncPct() == 0 && bilBillPlan.getBbpCoffCncAmt() == 0) {
            lastDayReinstate.put(LastDayReinstate.REINSTTOTAL.type, bilAcyAmounts);
            accumCoffTotalAmount += bilAcyAmounts;
        } else if (bilBillPlan.getBbpCoffCncPct() > 0 && bilBillPlan.getBbpCoffCncAmt() == 0) {
            tempValue = bilBillPlan.getBbpCoffCncPct() / 100 * bilAcyAmounts;
            accumCoffPctAmount = bilAcyAmounts - tempValue;
            lastDayReinstate.put(LastDayReinstate.REINSTTOTAL.type, accumCoffPctAmount);
            accumCoffTotalAmount += accumCoffPctAmount;
        } else if (bilBillPlan.getBbpCoffCncPct() == 0 && bilBillPlan.getBbpCoffCncAmt() > 0) {
            accumCoffAmount = bilAcyAmounts - bilBillPlan.getBbpCoffCncAmt();
            lastDayReinstate.put(LastDayReinstate.REINSTTOTAL.type, accumCoffAmount);
            accumCoffTotalAmount += accumCoffAmount;
        } else {
            tempValue = bilBillPlan.getBbpCoffCncPct() / 100 * bilAcyAmounts;
            accumCoffPctAmount = bilAcyAmounts - tempValue;
            accumCoffAmount = bilAcyAmounts - bilBillPlan.getBbpCoffCncAmt();
            if (accumCoffPctAmount > accumCoffAmount) {
                lastDayReinstate.put(LastDayReinstate.REINSTTOTAL.type, accumCoffPctAmount);
                accumCoffTotalAmount += accumCoffPctAmount;
            } else {
                lastDayReinstate.put(LastDayReinstate.REINSTTOTAL.type, accumCoffAmount);
                accumCoffTotalAmount += accumCoffAmount;
            }
        }
        lastDaytoReinstateList.add(lastDayReinstate);

        cancelCashObject.add(0, seqNbrOfLastPayment);
        cancelCashObject.add(1, minPcnAcyDate);
        cancelCashObject.add(2, minLastDayReinstateDate);
        cancelCashObject.add(3, accumCoffTotalAmount);
        cancelCashObject.add(4, cancelGraceDate);
    }

    private void writeHistory(String accountId, String activityDesCd, String activityDesType, String policyNumber,
            String policySymbolCd, ZonedDateTime activityDesc1Dt, ZonedDateTime activityDesc2Dt,
            String additionalDataTxt, String userId, ZonedDateTime recoveryDate) {

        Short bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId,
                recoveryDate);
        if (bilAcySeq == null) {
            bilAcySeq = SHORT_ZERO;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }
        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountId,
                recoveryDate.with(LocalTime.MIN), bilAcySeq);
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(policySymbolCd);
        bilActSummary.setPolNbr(policyNumber);
        bilActSummary.setBilAcyDesCd(activityDesCd);
        bilActSummary.setBilDesReaTyp(activityDesType);
        bilActSummary.setBilAcyDes1Dt(activityDesc1Dt);
        bilActSummary.setBilAcyDes2Dt(activityDesc2Dt);
        bilActSummary.setBilAcyAmt(DECIMAL_ZERO);
        bilActSummary.setUserId(userId);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());
        bilActSummary.setBasAddDataTxt(additionalDataTxt);
        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

    private Integer checkPaymentSameDay(String accountId, ZonedDateTime maxBilAcyDate, Short maxBilSequenceNumber) {
        List<String> bilAcyTypeCodeList = new ArrayList<>(Arrays.asList(BusCodeTranslationType.CASH_PAYMENT.getValue(),
                BusCodeTranslationType.CREDIT_CARD_PAYMENT.getValue(), BilDesReasonType.ACH.getValue(),
                BilDesReasonType.COL.getValue(), BilDesReasonType.GPY.getValue(), BilDesReasonType.ONE_XA.getValue(),
                BilDesReasonType.ASW.getValue(), BilDesReasonType.PYC.getValue(),
                BilDesReasonType.WALLET_PAYMENT_TYPE.getValue()));
        List<String> bilAcyDesCodeList = getBilAcyDesCodeList();
        return bilActSummaryRepository.checkPaymentSameDay(accountId, maxBilAcyDate, maxBilSequenceNumber,
                bilAcyTypeCodeList, bilAcyDesCodeList);
    }

    private List<String> getBilAcyDesCodeList() {
        List<String> bilAcyDesCodeList = new ArrayList<>(
                Arrays.asList(BilReceiptTypeCode.EFT.getValue(), BilReceiptTypeCode.EFT_CREDIT_CARD.getValue()));
        List<String> eftWalletReceiptTypeCodeList = busCdTranslationRepository
                .findCodeListByType(BilDesReasonType.EFT_WALLET_PAYMENT_TYPE.getValue(), language);
        if (!eftWalletReceiptTypeCodeList.isEmpty()) {
            bilAcyDesCodeList.addAll(eftWalletReceiptTypeCodeList);
        }
        return bilAcyDesCodeList;
    }

    private int getAdjustMonths(ZonedDateTime startDate, ZonedDateTime endDate) {
        int differentMonths;
        differentMonths = endDate.getYear() - startDate.getYear();
        differentMonths = differentMonths * 12;
        differentMonths = endDate.getMonthValue() + differentMonths;
        differentMonths = differentMonths - startDate.getMonthValue();
        return differentMonths;
    }

    private List<String> bilAcyDesCodeList() {
        return new ArrayList<>(
                Arrays.asList(BilAcyDesCode.REQUEST_NONPAY_CANCEL.getValue(), BilAcyDesCode.RESTART_PCN.getValue()));
    }

    private List<Character> creditIndicatorList() {
        List<Character> creditIndList = new ArrayList<>();
        creditIndList.add(BLANK_CHAR);
        creditIndList.add(CreditIndicator.OPERATOR.getValue());
        return creditIndList;
    }

    private List<Character> bilAmtEffIndicatorList() {
        List<Character> bilAmtEffIndList = new ArrayList<>();
        bilAmtEffIndList.add(BilAmountsEftIndicator.CANCELLATION_F.getValue());
        bilAmtEffIndList.add(BilAmountsEftIndicator.CANCELLATION_P.getValue());
        bilAmtEffIndList.add(CHAR_S);
        return bilAmtEffIndList;
    }

    private List<Character> polStatusCodeList() {
        List<Character> polStatusCodeList = new ArrayList<>();
        polStatusCodeList.add(BilPolicyStatusCode.PENDING_CANCELLATION.getValue());
        polStatusCodeList.add(BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue());
        polStatusCodeList.add(BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue());
        polStatusCodeList.add(BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue());
        return polStatusCodeList;
    }

    public enum LastDayReinstate {
        ISSUESYSTEM("IssueSystem"),
        MASTERCOMPANY("MasterCompany"),
        STATEPVNCD("StatePvnCd"),
        LOBCD("LobCd"),
        POLICYID("PolicyId"),
        POLNUMBER("PolNumber"),
        POLSYMBOLCD("PolSymbolCd"),
        PCNAMOUNT("PcnAmount"),
        POLEFFECTIVEDATE("PolEffectiveDate"),
        PLANEXPDATE("PlanExpDate"),
        RENTYPECD("RenTypeCode"),
        PRINTTGR("PrintTgr"),
        REINSTTOTAL("ReinstTotal"),
        DIRECTEDAMT("DirectedAmt"),
        SPACE(BLANK_STRING);

        private final String type;

        private LastDayReinstate(String type) {
            this.type = type;
        }

        public boolean hasValue(String value) {
            return value != null && type.equals(value);
        }

        @Override
        public String toString() {
            return this.type;
        }

        public static LastDayReinstate getEnumKey(String type) {
            for (LastDayReinstate lastDayReinstate : values()) {
                if (lastDayReinstate.type.equals(type)) {
                    return lastDayReinstate;
                }
            }
            return SPACE;
        }
    }

}
