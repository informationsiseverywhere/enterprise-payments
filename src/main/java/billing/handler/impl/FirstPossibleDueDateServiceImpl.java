package billing.handler.impl;

import static billing.utils.AdjustDueDatesConstants.DATE_LOW;
import static billing.utils.AdjustDueDatesConstants.RENEWAL_HALF_DOWNPAY;
import static billing.utils.BillingConstants.MINUS_SIGN;
import static billing.utils.BillingConstants.PLUS_SIGN;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_C;
import static core.utils.CommonConstants.CHAR_D;
import static core.utils.CommonConstants.CHAR_E;
import static core.utils.CommonConstants.CHAR_F;
import static core.utils.CommonConstants.CHAR_H;
import static core.utils.CommonConstants.CHAR_I;
import static core.utils.CommonConstants.CHAR_L;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_O;
import static core.utils.CommonConstants.CHAR_P;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_T;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_TWO;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import application.handler.model.AccountChargesData;
import application.handler.service.ChargesUserExitService;
import billing.data.entity.BilAccount;
import billing.data.entity.BilAmounts;
import billing.data.entity.BilCrgAmounts;
import billing.data.entity.BilDates;
import billing.data.entity.BilEftPlnCrg;
import billing.data.entity.BilIstSchedule;
import billing.data.entity.BilObjectCfg;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSeasExcl;
import billing.data.entity.BilSptPlnCrg;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.HolidayPrc;
import billing.data.entity.id.BilCrgAmountsId;
import billing.data.entity.id.BilDatesId;
import billing.data.entity.id.BilObjectCfgId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilAmountsRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCreScheduleRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilDatesRepository;
import billing.data.repository.BilInvTtyActRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilObjectCfgRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilSeasExclRepository;
import billing.data.repository.HolidayPrcRepository;
import billing.handler.model.AccountAdjustDates;
import billing.handler.model.AccountAdjustSupportData;
import billing.handler.model.AdjustDatesReviewDetails;
import billing.handler.model.ChargeVariables;
import billing.handler.model.FirstPossibleDueDate;
import billing.handler.model.VariableSeviceCharge;
import billing.handler.model.VariableSeviceChargeOverride;
import billing.handler.service.FirstPossibleDueDateService;
import billing.utils.AdjustDueDatesConstants;
import billing.utils.AdjustDueDatesConstants.BillingExclusionIndicator;
import billing.utils.AdjustDueDatesConstants.BillingTypeCode;
import billing.utils.AdjustDueDatesConstants.DateIndicator;
import billing.utils.AdjustDueDatesConstants.DateTypeCode;
import billing.utils.AdjustDueDatesConstants.PolicyRedistributeCode;
import billing.utils.AdjustDueDatesConstants.ServiceChargeActionCode;
import billing.utils.AdjustDueDatesConstants.WaiveServiceChargeCode;
import billing.utils.BillingConstants.AccountTypeCode;
import billing.utils.BillingConstants.BilDspTypeCode;
import billing.utils.BillingConstants.ChargeType;
import billing.utils.BillingConstants.ChargeTypeCodes;
import billing.utils.BillingConstants.InvoiceTypeCode;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.utils.DateRoutine;

@Service
public class FirstPossibleDueDateServiceImpl implements FirstPossibleDueDateService {

    private static final Logger logger = LoggerFactory.getLogger(FirstPossibleDueDateServiceImpl.class);

    private static final String HOLIDAY_8 = "H8";
    private static final String HOLIDAY_6 = "H6";
    private static final String HOLIDAY_5 = "H5";
    private static final String HOLIDAY_3 = "H3";
    private static final String HOLIDAY_1 = "H1";
    private static final String BCMOCRG1 = "BCMOCRG1";

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilDatesRepository bilDatesRepository;

    @Autowired
    private BilInvTtyActRepository bilInvTtyActRepository;

    @Autowired
    private HolidayPrcRepository holidayPrcRepository;

    @Autowired
    private BilCreScheduleRepository bilCreScheduleRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilSeasExclRepository bilSeasExclRepository;

    @Autowired
    private BilAmountsRepository bilAmountsRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;

    @Autowired
    private BilObjectCfgRepository bilObjectCfgRepository;

    @Autowired
    private ChargesUserExitService chargesUserExitService;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    //@Autowired
    //private ExecContext execContext;

    private static final List<Character> FUTURE_INVOICE_CODES = Arrays.asList(BLANK_CHAR,
            InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue());

    private static final List<Character> CURRENT_INVOICE_CODES = Arrays.asList(
            InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
            InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue());

    @Override
    @Transactional
    public FirstPossibleDueDate processFirstPossibleDueDateLogic(char includeWeekend, boolean isXHOLRule,
            BilRulesUct bilRulesUct, AccountAdjustDates accountAdjustDates,
            AdjustDatesReviewDetails adjustDatesReviewDetails) {
        ZonedDateTime fpddDate = null;
        BilAccount bilAccount = adjustDatesReviewDetails.getBilAccount();
        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();
        boolean isEftCollectionMethod = adjustDatesReviewDetails.isEftCollectionMethod();
        short dueDateDisplacementNumber = adjustDatesReviewDetails.getDueDateDisplacementNumber();
        int referenceDuration = adjustDatesReviewDetails.getReferenceDuration();
        char fpddIndicator = adjustDatesReviewDetails.getFpddIndicator();

        FirstPossibleDueDate firstPossibleDueDate = new FirstPossibleDueDate();

        boolean isInvoicedToday = determineIfInvoicedToday(accountAdjustDates.getAccountId(),
                accountAdjustDates.getProcessDate());

        fpddDate = getFpddDate(isInvoicedToday, accountAdjustDates, includeWeekend, dueDateDisplacementNumber,
                isXHOLRule, bilRulesUct);

        logger.debug("evaluate fppdInvoiceDate");
        boolean excludeWeekend = getOverrideWeekendIndicator(true, includeWeekend);
        ZonedDateTime fpddInvoiceDate = getFpddInvoiceDate(fpddDate, accountAdjustDates.getProcessDate(),
                includeWeekend, dueDateDisplacementNumber, isXHOLRule, bilRulesUct);

        logger.debug("evaluate pcnDate");
        ZonedDateTime pendingCancellationDate = DateRoutine.getAdjustedDate(fpddDate, false,
                bilSupportPlan.getBspPndCncNbr(), null);
        ZonedDateTime pcnPlusOneDay = getPcnPlusOneDay(pendingCancellationDate, accountAdjustDates.getProcessDate(),
                excludeWeekend, isXHOLRule, bilRulesUct);

        logger.debug("evaluate lookAheadDate");
        ZonedDateTime lookAheadDate = DateRoutine.getAdjustedDate(pcnPlusOneDay, false, dueDateDisplacementNumber,
                null);

        logger.debug("evaluate fpddReferenceDate");
        ZonedDateTime[] fpddReferenceDateData = getFpddReferenceDate(referenceDuration, fpddDate, bilAccount,
                adjustDatesReviewDetails.isFpddOnTimeProcessing());
        ZonedDateTime fpddReferenceDate = fpddReferenceDateData[SHORT_ZERO];
        ZonedDateTime fpddReferenceDateOnTime = fpddReferenceDateData[SHORT_ONE];

        if (isEftCollectionMethod && bilAccount.getCollectionPlan().compareTo(BLANK_STRING) != 0
                || fpddIndicator == CHAR_Y) {
            ZonedDateTime minInvoiceDate = bilDatesRepository.getMinInvoicedDateByInvoiceDate(
                    accountAdjustDates.getAccountId(), accountAdjustDates.getProcessDate(),
                    Arrays.asList(InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue(),
                            InvoiceTypeCode.EMPTY_CHAR.getValue()),
                    BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
            if (minInvoiceDate == null) {
                minInvoiceDate = bilDatesRepository.getMinInvoicedDate(accountAdjustDates.getAccountId(),
                        BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
            }
            if (minInvoiceDate != null) {
                oneDateAdd(accountAdjustDates, adjustDatesReviewDetails, fpddDate, isXHOLRule, bilRulesUct,
                        includeWeekend, bilSupportPlan);
            }
        }
        logger.debug("set the FirstPossibleDueDateLogic model");

        firstPossibleDueDate.setDate(fpddDate);
        firstPossibleDueDate.setInvoiceDate(fpddInvoiceDate);
        firstPossibleDueDate.setPendingCancellationDate(pendingCancellationDate);
        firstPossibleDueDate.setLookAheadDate(lookAheadDate);
        firstPossibleDueDate.setReferenceDate(fpddReferenceDate);
        firstPossibleDueDate.setReferenceDateOnTime(fpddReferenceDateOnTime);
        firstPossibleDueDate.setInvoicedToday(isInvoicedToday);

        return firstPossibleDueDate;
    }

    @Override
    public BilDates newFpddBillCheck(AccountAdjustDates accountAdjustDates, ZonedDateTime newSystemDate,
            ZonedDateTime newAdjustDate, ZonedDateTime newReferenceDate, ZonedDateTime newInvoiceDate,
            BilSupportPlan bilSupportPlan, boolean isErcvIssDateInv, boolean isActBilled) {

        BilDates bilDates = bilDatesRepository
                .findById(new BilDatesId(accountAdjustDates.getAccountId(), newReferenceDate)).orElse(null);
        if (bilDates == null) {
            bilDates = writeBilDatesRow(BLANK_CHAR, accountAdjustDates.getAccountId(), newReferenceDate, newSystemDate,
                    newAdjustDate, newInvoiceDate, bilSupportPlan);
            testForPrevDates(accountAdjustDates.getAccountId(), newAdjustDate, newInvoiceDate,
                    bilDates.getBdtNoncpiInd());

        } else {
            logger.debug("Fetch BilDate row with DateType, InvoiceCode and ExcInd");
            if (isValidBilDate(bilDates)) {

                if (bilDates.getBdtDateType() == CHAR_S && isErcvIssDateInv && !isActBilled) {
                    bilDates.setBilAdjDueDt(accountAdjustDates.getProcessDate());
                    bilDates.setBilInvDt(accountAdjustDates.getProcessDate());
                } else if (bilDates.getBdtDateType() == CHAR_F) {
                    bilDates.setBdtDateType(CHAR_I);
                }
                bilDatesRepository.saveAndFlush(bilDates);
            } else {
                bilDates = writeBilDatesRow(CHAR_D, accountAdjustDates.getAccountId(), newReferenceDate, newSystemDate,
                        newAdjustDate, newInvoiceDate, bilSupportPlan);

            }
        }

        return bilDates;
    }

    private BilDates writeBilDatesRow(char newBillIndicator, String accountId, ZonedDateTime newReferenceDate,
            ZonedDateTime newSystemDate, ZonedDateTime newAdjustDate, ZonedDateTime newInvoiceDate,
            BilSupportPlan bilSupportPlan) {
        BilDates bilDates = new BilDates();
        bilDates.setBilDatesId(new BilDatesId(accountId, newReferenceDate));
        if (newBillIndicator == CHAR_D) {
            newReferenceDate = DateRoutine.getAdjustedDate(newReferenceDate, false, 1, null);
            bilDates.setBilDatesId(new BilDatesId(accountId, newReferenceDate));
        }

        bilDates.setBilInvoiceCd(BLANK_CHAR);
        bilDates.setBilSysDueDt(newSystemDate);
        bilDates.setBilAdjDueDt(newAdjustDate);
        bilDates.setBilInvDt(newInvoiceDate);
        bilDates.setBdtDateInd(CHAR_N);
        bilDates.setBdtDateType(CHAR_I);
        bilDates.setBdtLateCrgInd(CHAR_N);
        bilDates.setBdtNoncpiInd(CHAR_Y);
        if (bilSupportPlan.getBspNoncpiLetInd() == CHAR_N) {
            bilDates.setBdtNoncpiInd(bilSupportPlan.getBspNoncpiLetInd());
        }

        bilDatesRepository.saveAndFlush(bilDates);
        return bilDates;
    }

    private boolean isValidBilDate(BilDates bilDates) {
        return (bilDates.getBdtDateType() == CHAR_I || bilDates.getBdtDateType() == CHAR_F
                || bilDates.getBdtDateType() == CHAR_S) && isValidInvoiceCode(bilDates.getBilInvoiceCd())
                && bilDates.getBilExcInd() != CHAR_X;
    }

    private void testForPrevDates(String accountId, ZonedDateTime newAdjustDate, ZonedDateTime newInvoiceDate,
            char noncpiIndicator) {
        ZonedDateTime minAdjustDueDate = bilDatesRepository.getPrevMinAdjDueDate(accountId, newInvoiceDate, CHAR_I,
                FUTURE_INVOICE_CODES, CHAR_X);
        if (minAdjustDueDate == null || newAdjustDate.isEqual(minAdjustDueDate)) {
            return;
        }

        ZonedDateTime oldInvoiceDate = newInvoiceDate;

        bilIstScheduleRepository.updateRescindScheduleRowByAdjustDate(newAdjustDate, newInvoiceDate, BLANK_CHAR,
                accountId, minAdjustDueDate, oldInvoiceDate);
        bilCreScheduleRepository.updateRescindCreScheduleRowBybilAdjDueDt(newAdjustDate, newInvoiceDate, accountId,
                minAdjustDueDate, oldInvoiceDate);
        bilCashDspRepository.updateRescindCashRowByAdjDueDate(newAdjustDate, newInvoiceDate, accountId,
                minAdjustDueDate, oldInvoiceDate, ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue());
        bilCrgAmountsRepository.updateRescindCrgAmountsBybilAdjDueDt(newAdjustDate, newInvoiceDate, accountId,
                minAdjustDueDate, oldInvoiceDate,
                Arrays.asList(ChargeTypeCodes.LATE_CHARGE.getValue(), ChargeTypeCodes.PENALTY_CHARGE.getValue()));
        bilDatesRepository.updateRescindBilDatesByAdjDueDate(newAdjustDate, newInvoiceDate, CHAR_S, noncpiIndicator,
                accountId, minAdjustDueDate, oldInvoiceDate);

    }

    private boolean isValidInvoiceCode(char invoiceCode) {
        return FUTURE_INVOICE_CODES.stream().anyMatch(bilInvoiceCd -> bilInvoiceCd == invoiceCode);
    }

    @Override
    public boolean determineIfInvoicedToday(String accountId, ZonedDateTime applicationDateTime) {

        ZonedDateTime maxAdjustDueDate = bilDatesRepository.getMaxAdjustDueDate(accountId, applicationDateTime,
                AdjustDueDatesConstants.getInvoiceCodes(), BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
        if (maxAdjustDueDate != null) {
            return true;
        }
        Integer biaInvoiceCount = bilInvTtyActRepository.getIfInvoicedToday(accountId, CHAR_A, accountId,
                applicationDateTime);
        return biaInvoiceCount != null && biaInvoiceCount != SHORT_ZERO;
    }

    @Override
    public ZonedDateTime getFpddDate(boolean isInvoicedToday, AccountAdjustDates accountAdjustDates,
            char includeWeekend, short dueDateDisplacementNumber, boolean isXHOLRule, BilRulesUct bilRulesUct) {

        boolean excludeWeekend = getOverrideWeekendIndicator(true, includeWeekend);
        short fpddAdjustDays = SHORT_ZERO;

        if (isInvoicedToday) {
            fpddAdjustDays = SHORT_ONE;
        }

        ZonedDateTime fpddAdjustDate = DateRoutine.getAdjustedDate(accountAdjustDates.getProcessDate(), excludeWeekend,
                fpddAdjustDays, accountAdjustDates.getProcessDate());
        if (fpddAdjustDays > SHORT_ZERO && isXHOLRule) {
            fpddAdjustDate = holidayDateProcessing(fpddAdjustDate, accountAdjustDates.getProcessDate(), bilRulesUct);
        }

        if (isInvoicedToday && fpddAdjustDate.isEqual(accountAdjustDates.getProcessDate())) {
            fpddAdjustDate = DateRoutine.getAdjustedDate(accountAdjustDates.getProcessDate(), excludeWeekend, SHORT_TWO,
                    accountAdjustDates.getProcessDate());
            if (dueDateDisplacementNumber > SHORT_ZERO && isXHOLRule) {
                fpddAdjustDate = holidayDateProcessing(fpddAdjustDate, accountAdjustDates.getProcessDate(),
                        bilRulesUct);
            }
        }

        excludeWeekend = getOverrideWeekendIndicator(false, includeWeekend);
        return DateRoutine.getAdjustedDate(fpddAdjustDate, excludeWeekend, dueDateDisplacementNumber, null);
    }

    @Override
    public ZonedDateTime holidayDateProcessing(ZonedDateTime dateTime, ZonedDateTime se3Date, BilRulesUct bilRulesUct) {

        char holidayValue = 'T';
        boolean isHoliday = false;
        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == 'Y' && bilRulesUct.getBrtParmListTxt() != null) {
            if (dateTime.compareTo(se3Date) == 0) {
                if (bilRulesUct.getBrtParmListTxt().trim().equalsIgnoreCase(HOLIDAY_8)) {
                    holidayValue = '9';
                } else if (bilRulesUct.getBrtParmListTxt().trim().equalsIgnoreCase(HOLIDAY_6)) {
                    holidayValue = '7';
                } else if (bilRulesUct.getBrtParmListTxt().trim().equalsIgnoreCase(HOLIDAY_5)) {
                    holidayValue = '4';
                } else if (bilRulesUct.getBrtParmListTxt().trim().equalsIgnoreCase(HOLIDAY_3)) {
                    holidayValue = '4';
                } else if (bilRulesUct.getBrtParmListTxt().trim().equalsIgnoreCase(HOLIDAY_1)) {
                    holidayValue = '2';
                } else {
                    holidayValue = bilRulesUct.getBrtParmListTxt().trim().replace("H", "").charAt(0);
                }
            } else {
                holidayValue = bilRulesUct.getBrtParmListTxt().trim().replace("H", "").charAt(0);
            }

        }

        HolidayPrc holidayPrc = holidayPrcRepository.findHolidayRow(dateTime);
        if (holidayPrc != null) {
            isHoliday = true;
        }
        dateTime = DateRoutine.dateTimeWithHolidayProcess(dateTime, holidayValue, isHoliday);
        boolean holidayDateCheck = true;
        while (holidayDateCheck) {
            isHoliday = false;
            holidayPrc = holidayPrcRepository.findHolidayRow(dateTime);
            if (holidayPrc != null) {
                isHoliday = true;
            }
            if (isHoliday) {
                dateTime = DateRoutine.dateTimeWithHolidayProcess(dateTime, holidayValue, isHoliday);
            } else {
                holidayDateCheck = false;
            }
        }
        return dateTime;
    }

    @Override
    public ZonedDateTime getFpddInvoiceDate(ZonedDateTime fpddDate, ZonedDateTime se3Date, char includeWeekend,
            short dueDateDisplacementNumber, boolean isXHOLRule, BilRulesUct bilRulesUct) {

        boolean excludeWeekend = getOverrideWeekendIndicator(true, includeWeekend);
        ZonedDateTime invoiceDate = DateRoutine.adjustDateWithOffset(fpddDate, excludeWeekend,
                dueDateDisplacementNumber, '-', se3Date);
        if (isXHOLRule) {
            return holidayDateProcessing(invoiceDate, se3Date, bilRulesUct);
        }

        return invoiceDate;

    }

    @Override
    @Transactional
    public void processFirstPossibleDueDateResetLogic(BilAccount bilAccount, FirstPossibleDueDate firstPossibleDueDate,
            double minimumInvoiceAmount, short pendingCancellationDays, char includeWeekend,
            AccountAdjustDates accountAdjustDates, short dueDateDisplacementNumber, boolean isXHOLRule,
            BilRulesUct bilRulesUct, BilSupportPlan bilSupportPlan, VariableSeviceCharge variableSeviceCharge,
            ChargeVariables chargeVariables, VariableSeviceChargeOverride variableSeviceChargeOverride,
            AdjustDatesReviewDetails adjustDatesReviewDetails, AccountAdjustSupportData accountAdjustSupportData,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, int cancelCount) {
        logger.debug("FPDD reset Logic- For Uninvoiced BDT BIS BCS");
        String accountId = bilAccount.getAccountId();
        Double firstPossibleDueDateBalance = DECIMAL_ZERO;
        boolean isFpddReset = false;
        ZonedDateTime systemDueDate = null;
        ZonedDateTime fpddDate = firstPossibleDueDate.getDate();
        ZonedDateTime lookAheadDate = firstPossibleDueDate.getLookAheadDate();

        /*
         * BIS cursor to get future FPDD -Adj due equals FPDD date rows and
         * Sys-date <= FPDD
         */
        List<Object[]> fpddInstallmentScheduleList = bilIstScheduleRepository.getFirstPossibleDueDateSum(accountId,
                fpddDate, fpddDate, FUTURE_INVOICE_CODES);

        if (fpddInstallmentScheduleList != null && !fpddInstallmentScheduleList.isEmpty()) {

            for (Object[] fpddInstallmentSchedule : fpddInstallmentScheduleList) {

                systemDueDate = (ZonedDateTime) fpddInstallmentSchedule[0];
                Double firstPossibleDueDateSum = (Double) fpddInstallmentSchedule[1];
                if (firstPossibleDueDateSum.compareTo(minimumInvoiceAmount) < SHORT_ZERO
                        || minimumInvoiceAmount == DECIMAL_ZERO
                                && firstPossibleDueDateSum.compareTo(minimumInvoiceAmount) == SHORT_ZERO) {
                    resetAdjToSys(accountId, true, systemDueDate, BLANK_CHAR, minimumInvoiceAmount, accountAdjustDates,
                            includeWeekend, dueDateDisplacementNumber, isXHOLRule, bilRulesUct);
                    isFpddReset = true;

                } else {
                    firstPossibleDueDateBalance += firstPossibleDueDateSum;
                }
            }
        } else {
            firstPossibleDueDateBalance = readWithFpinvoice(accountId, pendingCancellationDays, includeWeekend,
                    accountAdjustDates.getProcessDate(), dueDateDisplacementNumber, isXHOLRule, bilRulesUct);
        }

        /*
         * Calculate lookAheadDateBalance based on sys due date between FPDD and
         * LADD Date
         */
        Double lookAheadDateBalance = getLookAheadDateBalanceAmount(accountId, fpddDate, lookAheadDate);

        if (lookAheadDateBalance.compareTo(minimumInvoiceAmount) < 0
                && firstPossibleDueDateBalance.compareTo(minimumInvoiceAmount) >= 0) {
            return;
        } else if (minimumInvoiceAmount != DECIMAL_ZERO) {
            if (lookAheadDateBalance.compareTo(minimumInvoiceAmount) >= 0
                    && firstPossibleDueDateBalance.compareTo(minimumInvoiceAmount) >= 0) {
                return;
            }
        } else if (minimumInvoiceAmount == DECIMAL_ZERO) {
            if (lookAheadDateBalance.compareTo(minimumInvoiceAmount) > 0
                    && firstPossibleDueDateBalance.compareTo(minimumInvoiceAmount) > 0) {
                return;
            } else {
                if (lookAheadDateBalance.compareTo(minimumInvoiceAmount) >= 0
                        && firstPossibleDueDateBalance.compareTo(minimumInvoiceAmount) > 0) {
                    return;
                }
            }
        }

        /*
         * IF LADD BALANCE >= MIN INV AMOUNT AND FPDD BALANCE < MIN INV AMT -
         * Check for future BIS rows HAVE A BIL_SYS_DUE_DT LESS THAN OR EQUAL
         * FPDD
         */
        if (lookAheadDateBalance.compareTo(minimumInvoiceAmount) >= 0
                && firstPossibleDueDateBalance.compareTo(minimumInvoiceAmount) < 0 && !isFpddReset) {
            Integer scheduleCount = bilIstScheduleRepository.getBisRowCountBySysDueDt(accountId, fpddDate, BLANK_CHAR);

            if (scheduleCount == null || scheduleCount == 0) {
                boolean resetLookAhead = laddResetCheck(accountId, fpddDate, lookAheadDate, minimumInvoiceAmount);
                if (!resetLookAhead) {
                    return;
                }
            }
        }

        /* if maxSysDueDate is <= FPDD DO not Reset */
        if (firstPossibleDueDateBalance.compareTo(minimumInvoiceAmount) > 0
                || firstPossibleDueDateBalance.compareTo(minimumInvoiceAmount) == 0
                        && firstPossibleDueDateBalance.compareTo(DECIMAL_ZERO) > 0) {
            ZonedDateTime maxSysDueDate = bilIstScheduleRepository.selectMaxSysDueDate(accountId, fpddDate);
            if (maxSysDueDate.compareTo(fpddDate) <= 0) {
                return;
            }
        }

        /*
         * cursor to fetch those BDT rows whose SYS_DUE_DT <> ADJ_DUE_DT and
         * ADJ_DUE_DT not overridden by User
         */
        List<BilDates> bilDatesList = bilDatesRepository.findAdjustDateReset(accountId,
                DateIndicator.DATEOVERRIDEN_BYUSER.getValue(), FUTURE_INVOICE_CODES, fpddDate,
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            for (BilDates bilDate : bilDatesList) {
                resetAdjToSys(accountId, false, bilDate.getBilSysDueDt(), bilDate.getBdtDateInd(), minimumInvoiceAmount,
                        accountAdjustDates, includeWeekend, dueDateDisplacementNumber, isXHOLRule, bilRulesUct);
            }
        }

        updatePenalty(bilAccount, accountAdjustDates, bilSupportPlan, variableSeviceCharge, chargeVariables,
                cancelCount, adjustDatesReviewDetails.getNewAdjustDueDate(),
                adjustDatesReviewDetails.getNewInvoiceDate(), variableSeviceChargeOverride, adjustDatesReviewDetails,
                accountAdjustSupportData, bilEftPlanChargeList, bilSupportPlanChargeList, isXHOLRule, bilRulesUct,
                firstPossibleDueDate);

        /*
         * check to see if any FPDD rows inserted Get Minimum System Date of I
         * (FPDD) Rows
         */
        ZonedDateTime minBilSysDueDt = bilDatesRepository.getMinSysDueDtBybilAdjDueDt(accountId,
                DateTypeCode.FIRST_POSSIBLEDUE_LATE.getValue(), fpddDate, FUTURE_INVOICE_CODES,
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
        if (minBilSysDueDt == null) {
            ZonedDateTime checkDate = bilDatesRepository.getMaxCivAdjDueDate(accountId,
                    AdjustDueDatesConstants.getInvoiceCodes(),
                    BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
            if (checkDate == null) {
                return;
            }
            if (checkDate != null) {
                boolean excludeWeekend = getOverrideWeekendIndicator(true, includeWeekend);
                checkDate = DateRoutine.getAdjustedDate(checkDate, false, pendingCancellationDays, null);
                checkDate = getPcnPlusOneDay(checkDate, accountAdjustDates.getProcessDate(), excludeWeekend, isXHOLRule,
                        bilRulesUct);
                ZonedDateTime npInvDueDt = DateRoutine.getAdjustedDate(checkDate, false, dueDateDisplacementNumber,
                        null);

                minBilSysDueDt = bilDatesRepository.getMinSysDueDtBybilAdjDueDt(accountId,
                        DateTypeCode.FIRST_POSSIBLEDUE_LATE.getValue(), npInvDueDt, FUTURE_INVOICE_CODES,
                        BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
                if (minBilSysDueDt == null) {
                    return;
                }
                fpddDate = npInvDueDt;
            }
        }

        /* Combined 3 queries to below single one query with order by */
        List<BilDates> bilDateList = bilDatesRepository.getMinSysDueDtDatebilAdjDueDt(accountId,
                DateTypeCode.SCHEDULED.getValue(), fpddDate, FUTURE_INVOICE_CODES,
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

        if (bilDateList == null || bilDateList.isEmpty()) {
            throw new DataNotFoundException("bil.dates.rows.not.found", new Object[] { accountId });
        }
        BilDates bilDatesSchedule = bilDateList.get(0);
        ZonedDateTime newSysDate = bilDatesSchedule.getBilSysDueDt();
        ZonedDateTime newAdjDate = bilDatesSchedule.getBilAdjDueDt();
        ZonedDateTime newInvDate = bilDatesSchedule.getBilInvDt();

        updateOcbFpdd(accountId, newAdjDate, newInvDate, newSysDate, minBilSysDueDt);
        updatePenalty(bilAccount, accountAdjustDates, bilSupportPlan, variableSeviceCharge, chargeVariables,
                cancelCount, newAdjDate, newInvDate, variableSeviceChargeOverride, adjustDatesReviewDetails,
                accountAdjustSupportData, bilEftPlanChargeList, bilSupportPlanChargeList, isXHOLRule, bilRulesUct,
                firstPossibleDueDate);
    }

    private Double readWithFpinvoice(String accountId, short pendingCancellationDays, char includeWeekend,
            ZonedDateTime se3Date, short dueDateDisplacementNumber, boolean isXHOLRule, BilRulesUct bilRulesUct) {

        logger.debug(
                "FPDD reset Logic- Get Installment Balance by Next possible invoice Due Date - based on calculated PCN date ");
        ZonedDateTime checkDate = bilDatesRepository.getMaxCivAdjDueDate(accountId,
                AdjustDueDatesConstants.getInvoiceCodes(), BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
        if (checkDate != null) {
            boolean excludeWeekend = getOverrideWeekendIndicator(true, includeWeekend);
            checkDate = DateRoutine.getAdjustedDate(checkDate, false, pendingCancellationDays, null);
            checkDate = getPcnPlusOneDay(checkDate, se3Date, excludeWeekend, isXHOLRule, bilRulesUct);
            ZonedDateTime npInvDueDt = DateRoutine.getAdjustedDate(checkDate, false, dueDateDisplacementNumber, null);

            Double fpddBalance = bilIstScheduleRepository.findBalanceByAccountIdDueDate(accountId, npInvDueDt);

            if (fpddBalance != null && fpddBalance != DECIMAL_ZERO) {
                return fpddBalance;
            }
        }

        return DECIMAL_ZERO;
    }

    private Double getLookAheadDateBalanceAmount(String accountId, ZonedDateTime fpddDate,
            ZonedDateTime lookAheadDate) {
        Double lookAheadDateBalance = bilIstScheduleRepository.getLookAheadDateSum(accountId, fpddDate, lookAheadDate);
        if (lookAheadDateBalance != null) {
            return lookAheadDateBalance;
        }
        return DECIMAL_ZERO;
    }

    @Override
    public void updateOcbFpdd(String bilAccountId, ZonedDateTime newAdjDate, ZonedDateTime newInvDate,
            ZonedDateTime newSysDate, ZonedDateTime minBilSysDueDt) {
        logger.debug("Update Out of Cycle Fpdd AdjDate SysDate BilSysDueDt");
        bilIstScheduleRepository.updatAdjInvSysDates(newAdjDate, newInvDate, newSysDate, bilAccountId, minBilSysDueDt,
                FUTURE_INVOICE_CODES);
        bilCreScheduleRepository.updateCreScheduleRowByFpdd(newAdjDate, newInvDate, newSysDate, bilAccountId,
                minBilSysDueDt, Arrays.asList('2', '4'));
        bilDatesRepository.updatAdjInvSysDates(newAdjDate, newInvDate, newSysDate, CHAR_N, bilAccountId,
                minBilSysDueDt);
        bilCashDspRepository.updateInvDateAndAdjDateRowByOcb(newInvDate, newAdjDate, newSysDate, bilAccountId,
                minBilSysDueDt);
    }

    private boolean laddResetCheck(String accountId, ZonedDateTime fpddDate, ZonedDateTime lookAheadDate,
            Double minInvoiceAmount) {
        logger.debug(
                "FPDD reset Logic- Get Installment Balance by Next possible invoice Due Date - based on calculated PCN date ");

        List<BilDates> bilDatesList = bilDatesRepository.getAdjDueDateAndInvoiceDate(accountId,
                AdjustDueDatesConstants.getInvoiceCodes(), Arrays.asList(DateTypeCode.SCHEDULED.getValue()),
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

        if (bilDatesList == null || bilDatesList.isEmpty()) {
            return false;
        }

        ZonedDateTime bilDatesRfrDt = bilDatesList.get(bilDatesList.size() - 1).getBilDatesId().getBilReferenceDt();

        Double checkBalance = bilIstScheduleRepository.getAccountBalancebyDueDates(accountId, bilDatesRfrDt, fpddDate,
                fpddDate, lookAheadDate);
        if (checkBalance == null) {
            checkBalance = DECIMAL_ZERO;
        }
        return checkBalance.compareTo(minInvoiceAmount) < 0 || checkBalance.compareTo(DECIMAL_ZERO) == 0;
    }

    private void resetAdjToSys(String bilAccountId, boolean fpddSumSysDt, ZonedDateTime systemDueDate,
            char dateIndicator, Double minimumInvoiceAmount, AccountAdjustDates accountAdjustDates, char includeWeekend,
            short dueDateDisplacementNumber, boolean isXHOLRule, BilRulesUct bilRulesUct) {

        logger.debug("FPDD reset Logic- For reset BDT BIS BCS rows which is below minimum invoice");
        ZonedDateTime bdtBilSysDueDt = systemDueDate;
        if (!fpddSumSysDt && dateIndicator == DateIndicator.LOOKAHEAD_DUEDATE.getValue()) {
            Double resetSysDtBalance = bilIstScheduleRepository.findBalanceByAccountIdDueDate(bilAccountId,
                    bdtBilSysDueDt);
            if (resetSysDtBalance == null) {
                throw new DataNotFoundException("bis.record.not.found",
                        new Object[] { bilAccountId, DateRoutine.dateTimeAsMMDDYYYYAsString(bdtBilSysDueDt) });
            }
            if (resetSysDtBalance.compareTo(minimumInvoiceAmount) > 0) {
                return;
            }
        }

        ZonedDateTime invoiceDate = getFpddInvoiceDate(bdtBilSysDueDt, accountAdjustDates.getProcessDate(),
                includeWeekend, dueDateDisplacementNumber, isXHOLRule, bilRulesUct);
        bilIstScheduleRepository.updateRescindScheduleRowByInvoiceCode(bdtBilSysDueDt, invoiceDate, bilAccountId,
                bdtBilSysDueDt, FUTURE_INVOICE_CODES);
        bilCreScheduleRepository.updateRescindCreScheduleRowBySystemDate(bdtBilSysDueDt, invoiceDate, bilAccountId,
                bdtBilSysDueDt, Arrays.asList('2', '4'));
        bilCashDspRepository.updateRescindCashRowBySysDate(bdtBilSysDueDt, invoiceDate, bilAccountId, bdtBilSysDueDt,
                ChargeTypeCodes.DOWN_PAYMENT_FEE.getValue());
        bilDatesRepository.updateRescindCashRowBySysDate(bdtBilSysDueDt, invoiceDate, CHAR_N, bilAccountId,
                bdtBilSysDueDt);

    }

    private ZonedDateTime getPcnPlusOneDay(ZonedDateTime pendingCancellationDate, ZonedDateTime se3Date,
            boolean excludeWeekend, boolean isXHOLRule, BilRulesUct bilRulesUct) {

        ZonedDateTime pcnPlusOneDay = DateRoutine.getAdjustedDate(pendingCancellationDate, excludeWeekend, SHORT_ONE,
                se3Date);
        if (isXHOLRule) {
            return holidayDateProcessing(pcnPlusOneDay, se3Date, bilRulesUct);
        }
        return pcnPlusOneDay;

    }

    @Override
    public ZonedDateTime[] getFpddReferenceDate(int referenceDuration, ZonedDateTime fpddDate, BilAccount bilAccount,
            boolean isFpddOnTimeProcessing) {
        ZonedDateTime fpddReferenceDate = null;
        ZonedDateTime fpddReferenceDateOnTime = null;
        if (referenceDuration == SHORT_ZERO) {
            fpddReferenceDate = getAdjustedReferenceDate(fpddDate, bilAccount.getAccountId());
            fpddReferenceDateOnTime = fpddDate;

        } else {
            ZonedDateTime[] fpddReferenceDateData = getReferenceDateWithReferenceDuration(fpddDate,
                    isFpddOnTimeProcessing, referenceDuration, bilAccount);
            fpddReferenceDate = fpddReferenceDateData[SHORT_ZERO];
            fpddReferenceDateOnTime = fpddReferenceDateData[SHORT_ONE];
        }

        return new ZonedDateTime[] { fpddReferenceDate, fpddReferenceDateOnTime };
    }

    private ZonedDateTime getAdjustedReferenceDate(ZonedDateTime fpddDate, String accountId) {

        ZonedDateTime maxInvoicedReferenceDate = bilDatesRepository.getMaxReferenceDateByInvoiceCode(accountId,
                FUTURE_INVOICE_CODES, BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

        if (maxInvoicedReferenceDate != null && fpddDate.compareTo(maxInvoicedReferenceDate) <= SHORT_ZERO) {
            return DateRoutine.getAdjustedDate(maxInvoicedReferenceDate, false, SHORT_ONE, null);
        }
        return fpddDate;
    }

    private ZonedDateTime[] getReferenceDateWithReferenceDuration(ZonedDateTime fpddDate,
            boolean isFpddOnTimeProcessing, int referenceDuration, BilAccount bilAccount) {

        ZonedDateTime fpddReferenceDate = null;
        ZonedDateTime fpddReferenceDateOnTime = null;
        ZonedDateTime adjustedFpddDate = null;
        if (bilAccount.getStartReferenceDate().compareTo(bilAccount.getStartDueDate()) > SHORT_ZERO) {
            adjustedFpddDate = DateRoutine.getAdjustedDate(fpddDate, false, referenceDuration, null);
        } else {
            adjustedFpddDate = DateRoutine.getAdjustedDate(fpddDate, false, referenceDuration * -1, null);
        }

        if (adjustedFpddDate.compareTo(fpddDate) > SHORT_ZERO && isFpddOnTimeProcessing) {
            fpddReferenceDate = getAdjustedReferenceDate(fpddDate, bilAccount.getAccountId());
            fpddReferenceDateOnTime = fpddReferenceDate;

            if (fpddReferenceDateOnTime.compareTo(adjustedFpddDate) < SHORT_ZERO) {
                fpddReferenceDate = adjustedFpddDate;
            }
        } else {
            fpddReferenceDate = getAdjustedReferenceDate(adjustedFpddDate, bilAccount.getAccountId());
        }
        return new ZonedDateTime[] { fpddReferenceDate, fpddReferenceDateOnTime };
    }

    private boolean getOverrideWeekendIndicator(boolean excludeWeekend, char includeWeekend) {
        if (excludeWeekend && includeWeekend == CHAR_Y) {
            return false;
        }
        return excludeWeekend;

    }

    @Override
    public void oneDateAdd(AccountAdjustDates accountAdjustDates, AdjustDatesReviewDetails adjustDatesReviewDetails,
            ZonedDateTime date, boolean isXHOLRule, BilRulesUct bilRulesUct, char includeWeekend,
            BilSupportPlan bilSupportPlan) {

        BilAccount bilAccount = adjustDatesReviewDetails.getBilAccount();
        int offsetDays = adjustDatesReviewDetails.getDueDateDisplacementNumber()
                + adjustDatesReviewDetails.getBilSupportPlan().getBspPndCncNbr();
        ZonedDateTime maxReferenceDueDate = bilAccount.getStartReferenceDate();
        ZonedDateTime maxSystemDueDate = bilAccount.getStartDueDate();
        ZonedDateTime maxInvoiceAdjustDueDate = DateRoutine.dateTimeAsYYYYMMDD(0);
        boolean isInvoicePaid = false;
        ZonedDateTime invoicePaidDate = DateRoutine.defaultDateTime();

        Object[] maxDueDates = getMaxDueDates(bilAccount.getAccountId());
        if (maxDueDates.length != 0) {
            maxReferenceDueDate = (ZonedDateTime) maxDueDates[SHORT_ZERO];
            if (((ZonedDateTime) maxDueDates[SHORT_ONE]).compareTo(DateRoutine.dateTimeAsYYYYMMDD(0)) > SHORT_ZERO) {
                Object[] invoicePaid = deterIfInvoicePaid(bilAccount.getAccountId(),
                        (ZonedDateTime) maxDueDates[SHORT_ONE], bilSupportPlan.getBspMinInvAmt());
                isInvoicePaid = (boolean) invoicePaid[SHORT_ZERO];
                invoicePaidDate = (ZonedDateTime) invoicePaid[SHORT_ONE];
                if (!isInvoicePaid) {
                    if (adjustDatesReviewDetails.isEftCollectionMethod()) {
                        maxInvoiceAdjustDueDate = DateRoutine.adjustDateWithOffset(
                                (ZonedDateTime) maxDueDates[SHORT_ONE], false,
                                adjustDatesReviewDetails.getDueDateDisplacementNumber(), '+', null);
                        maxSystemDueDate = DateRoutine.adjustDateWithOffset((ZonedDateTime) maxDueDates[SHORT_TWO],
                                false, adjustDatesReviewDetails.getDueDateDisplacementNumber(), '+', null);
                    } else {
                        maxInvoiceAdjustDueDate = DateRoutine.adjustDateWithOffset(
                                (ZonedDateTime) maxDueDates[SHORT_ONE], false, offsetDays, '+', null);
                        maxSystemDueDate = DateRoutine.adjustDateWithOffset((ZonedDateTime) maxDueDates[SHORT_TWO],
                                false, offsetDays, '+', null);
                    }
                }
            } else {
                if (adjustDatesReviewDetails.isEftCollectionMethod()) {
                    maxSystemDueDate = DateRoutine.adjustDateWithOffset((ZonedDateTime) maxDueDates[SHORT_TWO], false,
                            adjustDatesReviewDetails.getDueDateDisplacementNumber(), '+', null);
                } else {
                    maxSystemDueDate = DateRoutine.adjustDateWithOffset((ZonedDateTime) maxDueDates[SHORT_TWO], false,
                            offsetDays, '+', null);
                }
            }

        } else {
            maxInvoiceAdjustDueDate = DateRoutine.dateTimeAsYYYYMMDD(0);
        }

        char excludeIndicator = BLANK_CHAR;

        while (maxReferenceDueDate.compareTo(date) <= 0 && excludeIndicator != CHAR_X) {
            BilDates bilDates = propDates(accountAdjustDates, adjustDatesReviewDetails, maxReferenceDueDate,
                    maxSystemDueDate, maxInvoiceAdjustDueDate, isXHOLRule, bilRulesUct, includeWeekend, isInvoicePaid,
                    invoicePaidDate);
            maxReferenceDueDate = bilDates.getBilDatesId().getBilReferenceDt();
            maxSystemDueDate = bilDates.getBilSysDueDt();
            maxInvoiceAdjustDueDate = bilDates.getBilInvDt();
            excludeIndicator = bilDates.getBilExcInd();
        }

    }

    private BilDates propDates(AccountAdjustDates accountAdjustDates, AdjustDatesReviewDetails adjustDatesReviewDetails,
            ZonedDateTime maxReferenceDueDate, ZonedDateTime maxSystemDueDate, ZonedDateTime maxInvoiceAdjustDueDate,
            boolean isXHOLRule, BilRulesUct bilRulesUct, char includeWeekend, boolean isInvoicePaid,
            ZonedDateTime invoicePaidDate) {
        BilAccount bilAccount = adjustDatesReviewDetails.getBilAccount();
        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();
        ZonedDateTime nextSchedDt = bilAccount.getStartDueDate();
        ZonedDateTime nextRefDt = bilAccount.getStartReferenceDate();
        while (!(nextRefDt.compareTo(maxReferenceDueDate) > 0 && nextSchedDt.compareTo(maxInvoiceAdjustDueDate) > 0
                && nextSchedDt.compareTo(maxSystemDueDate) > 0)) {
            nextSchedDt = getNextSchedDate(bilSupportPlan, nextSchedDt, bilAccount, CHAR_S);
            nextRefDt = getNextSchedDate(bilSupportPlan, nextRefDt, bilAccount, CHAR_R);
        }
        maxSystemDueDate = nextSchedDt;
        maxReferenceDueDate = nextRefDt;

        ZonedDateTime newInvDate = getFpddInvoiceDate(maxSystemDueDate, accountAdjustDates.getProcessDate(),
                includeWeekend, adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, bilRulesUct);
        if (isInvoicePaid && invoicePaidDate != null && newInvDate.compareTo(invoicePaidDate) <= 0
                && invoicePaidDate.compareTo(DateRoutine.defaultDateTime()) != 0) {
            return null;
        }
        if (adjustDatesReviewDetails.isEftCollectionMethod() && adjustDatesReviewDetails.getFpddIndicator() != CHAR_Y
                && (maxSystemDueDate.compareTo(accountAdjustDates.getProcessDate()) <= 0
                        || maxSystemDueDate.compareTo(adjustDatesReviewDetails.getEarliestEftDueDt()) < 0)) {
            nextSchedDt = maxSystemDueDate;
            while (!(nextSchedDt.compareTo(accountAdjustDates.getProcessDate()) > 0
                    && nextSchedDt.compareTo(adjustDatesReviewDetails.getEarliestEftDueDt()) >= 0)) {

                nextSchedDt = getNextSchedDate(bilSupportPlan, nextSchedDt, bilAccount, CHAR_S);
                newInvDate = getFpddInvoiceDate(nextSchedDt, accountAdjustDates.getProcessDate(), includeWeekend,
                        adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, bilRulesUct);
                if (nextSchedDt.compareTo(accountAdjustDates.getProcessDate()) > 0
                        && newInvDate.compareTo(accountAdjustDates.getProcessDate()) < 0
                        && nextSchedDt.compareTo(adjustDatesReviewDetails.getEarliestEftDueDt()) >= 0) {
                    newInvDate = accountAdjustDates.getProcessDate();
                }
            }
        }
        return insertBilDates(bilAccount.getAccountId(), maxSystemDueDate, maxReferenceDueDate, newInvDate, nextSchedDt,
                BLANK_CHAR, CHAR_N, CHAR_S, adjustDatesReviewDetails, bilAccount.getSeasionalExclusionCode().trim());
    }

    @Override
    public ZonedDateTime getNextSchedDate(BilSupportPlan bilSupportPlan, ZonedDateTime nextSchedDt,
            BilAccount bilAccount, char typeDatePropSw) {
        if (bilSupportPlan.getBilRvwFqyCd().compareTo("WK") == 0) {
            return DateRoutine.calculateDateTimeByFrequency("WK", nextSchedDt);

        }
        if (bilSupportPlan.getBilRvwFqyCd().compareTo("BW") == 0) {
            return DateRoutine.calculateDateTimeByFrequency("BW", nextSchedDt);
        }
        if (bilSupportPlan.getBilRvwFqyCd().compareTo("SM") == 0) {
            return DateRoutine.calculateDateTimeByFrequency("SM", nextSchedDt);
        }

        nextSchedDt = DateRoutine.calculateDateTimeByFrequency(bilSupportPlan.getBilRvwFqyCd(), nextSchedDt);

        if (bilAccount.getStartDueDate().getDayOfMonth() > 28 || bilAccount.getStartDueDate().getDayOfMonth() == 28) {
            if (typeDatePropSw == CHAR_S) {
                nextSchedDt = checkForDays(nextSchedDt, bilAccount, bilAccount.getLastDayIndicator(), typeDatePropSw);
            } else {
                nextSchedDt = checkForDays(nextSchedDt, bilAccount, bilAccount.getReferenceLastDay(), typeDatePropSw);
            }

        }
        return nextSchedDt;

    }

    private ZonedDateTime checkForDays(ZonedDateTime nextSchedDt, BilAccount bilAccount, char lastDayIndicator,
            char typeDatePropSw) {

        int month = nextSchedDt.getMonthValue();
        int day = 0;
        if (month == 2) {
            return checkLeapYear(nextSchedDt, lastDayIndicator);
        }

        if (bilAccount.getLastDayIndicator() == '9' && typeDatePropSw == CHAR_S
                || bilAccount.getReferenceLastDay() == '9' && typeDatePropSw == CHAR_R) {
            if (nextSchedDt.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth() > 29) {
                day = 29 - nextSchedDt.getDayOfMonth();
            }

        } else if (bilAccount.getLastDayIndicator() == '0' && typeDatePropSw == CHAR_S
                || bilAccount.getReferenceLastDay() == '0' && typeDatePropSw == CHAR_R) {

            if (nextSchedDt.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth() > 30) {
                day = 30 - nextSchedDt.getDayOfMonth();
            }

        } else if (bilAccount.getLastDayIndicator() == '1' && typeDatePropSw == CHAR_S
                || bilAccount.getReferenceLastDay() == '1' && typeDatePropSw == CHAR_R) {

            if (nextSchedDt.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth() == 31) {
                day = 31 - nextSchedDt.getDayOfMonth();
            } else {
                day = 30 - nextSchedDt.getDayOfMonth();
            }

        }

        if (bilAccount.getStartDueDate().getDayOfMonth() == 31
                && nextSchedDt.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth() < 31) {
            day = nextSchedDt.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth() - nextSchedDt.getDayOfMonth();
        }

        nextSchedDt = nextSchedDt.plusDays(day);

        return nextSchedDt;

    }

    private ZonedDateTime checkLeapYear(ZonedDateTime nextSchedDt, char lastDayIndicator) {
        int year = nextSchedDt.getYear();

        if (year % 100 == 0) {
            int reminder = year % 400;
            if (reminder == SHORT_ZERO && lastDayIndicator != CHAR_Y) {
                int day = 29 - nextSchedDt.getDayOfMonth();
                nextSchedDt = nextSchedDt.plusDays(day);
            } else {
                int day = 28 - nextSchedDt.getDayOfMonth();
                nextSchedDt = nextSchedDt.plusDays(day);
            }
            return nextSchedDt;
        }

        int reminder = year % 4;
        if (reminder == SHORT_ZERO && lastDayIndicator != CHAR_Y) {
            int day = 29 - nextSchedDt.getDayOfMonth();
            nextSchedDt = nextSchedDt.plusDays(day);
        } else {
            int day = 28 - nextSchedDt.getDayOfMonth();
            nextSchedDt = nextSchedDt.plusDays(day);
        }

        return nextSchedDt;

    }

    private Object[] getMaxDueDates(String accountId) {

        ZonedDateTime maxReferenceDueDate = bilDatesRepository.getMaxReferenceDateByDateType(accountId,
                DateTypeCode.FIRSTPOSSIBLEDUEDATE_SINGLEPOLICYCANCELLATIONINVOICE.getValue());

        if (maxReferenceDueDate != null) {
            BilDates bilDates = bilDatesRepository.findByReferenceDate(accountId, maxReferenceDueDate,
                    BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

            ZonedDateTime maxInvoiceAdjustDueDate = bilDatesRepository.getMaxAdjustDueDateByInvoiceCd(accountId,
                    AdjustDueDatesConstants.getInvoiceCodes(),
                    BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
            if (maxInvoiceAdjustDueDate == null) {
                maxInvoiceAdjustDueDate = DateRoutine.dateTimeAsYYYYMMDD(SHORT_ZERO);
            }
            return new Object[] { maxReferenceDueDate, maxInvoiceAdjustDueDate, bilDates.getBilSysDueDt(),
                    bilDates.getBdtDateType(), bilDates.getBilInvoiceCd() };

        }

        return new Object[0];
    }

    @Override
    public Object[] deterIfInvoicePaid(String accountId, ZonedDateTime checkDate, double bspMinInvoiceAmounts) {
        boolean isInvoicePaid = false;
        ZonedDateTime invoicePaidDate = DateRoutine.defaultDateTime();

        Double balanceAmounts = bilIstScheduleRepository.getWorkAmtForInvoiceSchedule(accountId, checkDate,
                CURRENT_INVOICE_CODES);
        if (balanceAmounts == null) {
            balanceAmounts = DECIMAL_ZERO;
        }
        if (balanceAmounts < bspMinInvoiceAmounts
                || bspMinInvoiceAmounts == 0 && balanceAmounts == bspMinInvoiceAmounts) {
            isInvoicePaid = true;
            invoicePaidDate = bilDatesRepository.getMaxInvoicedDateByInvoiceCode(accountId, checkDate,
                    CURRENT_INVOICE_CODES, CHAR_X);
            if (invoicePaidDate == null) {
                throw new DataNotFoundException("min.invoice.paid.date.not.found", new Object[] { accountId });
            }
        }

        logger.debug("Retrieved isInvoicePaid - invoicePaidDate");
        return new Object[] { isInvoicePaid, invoicePaidDate };
    }

    private BilDates insertBilDates(String accountId, ZonedDateTime maxSystemDueDate, ZonedDateTime maxReferenceDueDate,
            ZonedDateTime newInvDate, ZonedDateTime adjustDate, char invoiceCode, char dateIndicator, char dateType,
            AdjustDatesReviewDetails adjustDatesReviewDetails, String seasonalExclusionCode) {

        BilDates bilDates = new BilDates();
        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();
        bilDates.setBilDatesId(new BilDatesId(accountId, maxReferenceDueDate));
        bilDates.setBilSysDueDt(maxSystemDueDate);
        bilDates.setBdtDateInd(dateIndicator);
        bilDates.setBdtLateCrgInd(CHAR_N);
        bilDates.setBilInvoiceCd(invoiceCode);
        if (bilSupportPlan.getBspNoncpiLetInd() == CHAR_N) {
            bilDates.setBdtNoncpiInd(CHAR_N);
        } else {
            bilDates.setBdtNoncpiInd(CHAR_Y);
        }
        bilDates.setBdtDateType(dateType);
        bilDates.setBilInvDt(newInvDate);

        bilDates.setBilAdjDueDt(adjustDate);

        bilDates.setBilExcInd(BLANK_CHAR);
        if (adjustDatesReviewDetails.isSeasonalBill() && dateIndicator != 'Y') {
            char blackoutDtInd = getSeasonalDate(seasonalExclusionCode, bilDates.getBilSysDueDt());
            if (blackoutDtInd == 'Y') {
                bilDates.setBilExcInd('X');
            } else {
                blackoutDtInd = getSeasonalDate(seasonalExclusionCode, bilDates.getBilAdjDueDt());
                if (blackoutDtInd == 'Y') {
                    bilDates.setBilExcInd('X');
                }
            }

        }

        bilDatesRepository.saveAndFlush(bilDates);
        return bilDates;

    }

    @Override
    public char getSeasonalDate(String seasonalExclusionCode, ZonedDateTime deductionDt) {
        char blackoutDtInd = CHAR_N;
        BilSeasExcl bilSeasExcl = bilSeasExclRepository.getBilSeaRowByDate(seasonalExclusionCode, deductionDt);
        if (bilSeasExcl != null) {
            blackoutDtInd = CHAR_Y;
        }
        return blackoutDtInd;
    }

    @Override
    public void updatePenalty(BilAccount bilAccount, AccountAdjustDates accountAdjustDates,
            BilSupportPlan bilSupportPlan, VariableSeviceCharge variableSeviceCharge, ChargeVariables chargeVariables,
            int cancelCount, ZonedDateTime newAdjustDate, ZonedDateTime newInvoiceDate,
            VariableSeviceChargeOverride variableSeviceChargeOverride,
            AdjustDatesReviewDetails adjustDatesReviewDetails, AccountAdjustSupportData accountAdjustSupportData,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, boolean isXHOLRule,
            BilRulesUct bilRulesUct, FirstPossibleDueDate firstPossibleDueDate) {

        if (bilAccount.getBillTypeCd().equals(AccountTypeCode.SINGLE_POLICY.getValue())) {
            if (accountAdjustDates.getAddChargeIndicator() == CHAR_R
                    || accountAdjustDates.getDateResetIndicator() == CHAR_B) {
                List<BilDates> bilDatesList = bilDatesRepository.getAdjDueDateAndInvoiceDate(bilAccount.getAccountId(),
                        FUTURE_INVOICE_CODES, Arrays.asList('F', 'I'),
                        BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
                if (bilDatesList != null && !bilDatesList.isEmpty()) {
                    BilDates bilDates = bilDatesList.get(0);
                    newAdjustDate = bilDates.getBilAdjDueDt();
                    newInvoiceDate = bilDates.getBilInvDt();
                } else {
                    accountAdjustDates.setAddChargeIndicator(BLANK_CHAR);
                }
            }

            if (cancelCount > 0 || accountAdjustDates.getAddChargeIndicator() == CHAR_R
                    || accountAdjustDates.getDateResetIndicator() == CHAR_B) {
                Double chargePaidAmounts = bilCrgAmountsRepository.getChargePaidAmount(
                        accountAdjustDates.getAccountId(), Arrays.asList('L', 'P'), FUTURE_INVOICE_CODES, DECIMAL_ZERO);

                if (chargePaidAmounts == null || chargePaidAmounts <= DECIMAL_ZERO) {
                    bilCrgAmountsRepository.updateChargePaidAmount(newAdjustDate, newInvoiceDate,
                            accountAdjustDates.getAccountId(), FUTURE_INVOICE_CODES, Arrays.asList('F', 'I'));
                    if (accountAdjustDates.getAddChargeIndicator() == CHAR_R) {
                        accountAdjustDates.setAddChargeIndicator(BLANK_CHAR);
                    }
                    return;
                }

            }
        }

        processPenalty(accountAdjustDates, bilAccount, bilSupportPlan, cancelCount, variableSeviceChargeOverride,
                adjustDatesReviewDetails, accountAdjustSupportData, bilEftPlanChargeList, bilSupportPlanChargeList,
                chargeVariables, variableSeviceCharge, isXHOLRule, bilRulesUct, firstPossibleDueDate);

    }

    private void processPenalty(AccountAdjustDates accountAdjustDates, BilAccount bilAccount,
            BilSupportPlan bilSupportPlan, int cancelCount, VariableSeviceChargeOverride variableSeviceChargeOverride,
            AdjustDatesReviewDetails adjustDatesReviewDetails, AccountAdjustSupportData accountAdjustSupportData,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList,
            ChargeVariables chargeVariables, VariableSeviceCharge variableSeviceCharge, boolean isXHOLRule,
            BilRulesUct bilRulesUct, FirstPossibleDueDate firstPossibleDueDate) {

        List<BilCrgAmounts> bilCrgAmountsList = bilCrgAmountsRepository
                .FetchPenaltyCharges(accountAdjustDates.getAccountId(), Arrays.asList('L', 'P'), FUTURE_INVOICE_CODES);

        if (bilCrgAmountsList != null && !bilCrgAmountsList.isEmpty()) {
            for (BilCrgAmounts bilCrgAmounts : bilCrgAmountsList) {

                Double serviceChargeAmount = bilCrgAmounts.getBcaCrgAmt();
                ZonedDateTime[] newDates = selectPenaltyDates(bilAccount, accountAdjustDates, cancelCount,
                        adjustDatesReviewDetails, isXHOLRule, bilRulesUct, firstPossibleDueDate);
                if (newDates != null && newDates.length != 0) {
                    ZonedDateTime newAdjustDate = newDates[SHORT_ZERO];
                    ZonedDateTime newInvoiceDate = newDates[SHORT_ONE];

                    if (newAdjustDate.compareTo(bilCrgAmounts.getBilAdjDueDt()) != SHORT_ZERO
                            || newInvoiceDate.compareTo(bilCrgAmounts.getBilInvDt()) != SHORT_ZERO) {

                        bilCrgAmountsRepository.updateRescindCrgAmountsBybilAdjDueDt(newAdjustDate, newInvoiceDate,
                                bilCrgAmounts.getBilCrgAmountsId().getBilAccountId(), bilCrgAmounts.getBilAdjDueDt(),
                                bilCrgAmounts.getBilInvDt(), Arrays.asList(ChargeTypeCodes.LATE_CHARGE.getValue(),
                                        ChargeTypeCodes.PENALTY_CHARGE.getValue()));

                        bilCashDspRepository.updateRescindCashRowByAdjDueDate(newAdjustDate, newInvoiceDate,
                                bilCrgAmounts.getBilCrgAmountsId().getBilAccountId(), bilCrgAmounts.getBilAdjDueDt(),
                                bilCrgAmounts.getBilInvDt(), Arrays.asList(ChargeTypeCodes.LATE_CHARGE.getValue(),
                                        ChargeTypeCodes.PENALTY_CHARGE.getValue()));

                        Object[] dateTypeObject = getDateType(bilAccount.getAccountId(), newAdjustDate, newInvoiceDate);
                        char bilDateType = (char) dateTypeObject[0];

                        Double balanceAmounts = getBalance(bilAccount.getAccountId(), newAdjustDate, newInvoiceDate);
                        char newInvoiceIndicator = BLANK_CHAR;
                        if ((bilDateType == DateTypeCode.SCHEDULED.getValue()
                                || bilDateType == DateTypeCode.FIRSTPOSSIBLEDUEDATE_SINGLEPOLICYCANCELLATIONINVOICE
                                        .getValue()
                                || bilDateType == DateTypeCode.FIRST_POSSIBLEDUE_LATE.getValue()
                                || bilDateType == DateTypeCode.OUTOFCYCLE_BILLING.getValue())
                                && balanceAmounts < bilSupportPlan.getBspMinInvAmt()
                                || balanceAmounts == DECIMAL_ZERO) {
                            newInvoiceIndicator = InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue();
                            serviceChargeAmount = DECIMAL_ZERO;
                        }

                        updateInvoiceIndicator(bilAccount.getAccountId(), newInvoiceIndicator, newAdjustDate,
                                newInvoiceDate);

                        pendingCancellationServiceChargeUpdate(newAdjustDate, newInvoiceDate, adjustDatesReviewDetails,
                                bilAccount, variableSeviceCharge, chargeVariables, newInvoiceIndicator,
                                serviceChargeAmount, bilDateType, variableSeviceChargeOverride,
                                accountAdjustSupportData, bilEftPlanChargeList, bilSupportPlanChargeList,
                                accountAdjustDates);
                    }
                }
            }
        }

    }

    private ZonedDateTime[] selectPenaltyDates(BilAccount bilAccount, AccountAdjustDates accountAdjustDates,
            int cancelCount, AdjustDatesReviewDetails adjustDatesReviewDetails, boolean isXHOLRule,
            BilRulesUct bilRulesUct, FirstPossibleDueDate firstPossibleDueDate) {

        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();
        List<BilDates> bilDatesList = null;
        if (bilAccount.getBillTypeCd().equals(AccountTypeCode.SINGLE_POLICY.getValue()) && cancelCount > 0) {
            bilDatesList = bilDatesRepository.getMinAdjustDueDateByInvoiceCode(accountAdjustDates.getAccountId(),
                    FUTURE_INVOICE_CODES, accountAdjustDates.getProcessDate(), Arrays.asList('F'),
                    BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

        } else {
            bilDatesList = bilDatesRepository.getMinAdjustDueDateByInvoiceCode(accountAdjustDates.getAccountId(),
                    FUTURE_INVOICE_CODES, accountAdjustDates.getProcessDate(),
                    BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
        }

        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            BilDates bilDates = bilDatesList.get(SHORT_ZERO);
            return new ZonedDateTime[] { bilDates.getBilAdjDueDt(), bilDates.getBilInvDt() };
        }

        oneDateAdd(accountAdjustDates, adjustDatesReviewDetails, firstPossibleDueDate.getDate(), isXHOLRule,
                bilRulesUct, adjustDatesReviewDetails.getBilActRules().getBruIncWknInd(), bilSupportPlan);
        bilDatesList = bilDatesRepository.getMinAdjustDueDateByInvoiceCode(accountAdjustDates.getAccountId(),
                FUTURE_INVOICE_CODES, BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());
        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            BilDates bilDates = bilDatesList.get(SHORT_ZERO);
            return new ZonedDateTime[] { bilDates.getBilAdjDueDt(), bilDates.getBilInvDt() };
        }
        return new ZonedDateTime[0];
    }

    @Override
    public Object[] getDateType(String accountId, ZonedDateTime bilAdjustDate, ZonedDateTime bilInvoiceDate) {
        List<Object[]> dateTypeAndInd = bilDatesRepository.getBilDateTypeAndInd(accountId, bilAdjustDate,
                bilInvoiceDate, CHAR_X);
        if (dateTypeAndInd == null || dateTypeAndInd.isEmpty() || dateTypeAndInd.get(0)[0] == null) {
            logger.debug("Error occurred: Getting Max BilDateType and Min BilDateInd");
            throw new DataNotFoundException("max.bil.date.type.min.bil.date.ind.not.found", new Object[] { accountId });
        }

        logger.debug("Retrieved Max BilDateType and Min BilDateInd by Object type");
        return dateTypeAndInd.get(0);
    }

    @Override
    public Double getBalance(String accountId, ZonedDateTime bilAdjustDate, ZonedDateTime bilInvoiceDate) {

        Double balanceAmounts = bilIstScheduleRepository.getBalanceAmtForInvoiceSchedule(accountId, bilAdjustDate,
                bilInvoiceDate);
        if (balanceAmounts == null) {
            balanceAmounts = DECIMAL_ZERO;
        }
        Double chargeAmounts = bilCrgAmountsRepository.getChargeAmountForSchedule(accountId, bilAdjustDate,
                bilInvoiceDate,
                Arrays.asList(ChargeTypeCodes.LATE_CHARGE.getValue(), ChargeTypeCodes.PENALTY_CHARGE.getValue()));
        if (chargeAmounts == null) {
            chargeAmounts = DECIMAL_ZERO;
        }

        balanceAmounts += chargeAmounts;
        logger.debug("Retrieved newInvoiceInd - bilChargeAmount - balanceAmounts - chargeAmounts");
        return balanceAmounts;
    }

    @Override
    public void updateInvoiceIndicator(String accountId, char newInvoiceIndicator, ZonedDateTime bilAdjDueDt,
            ZonedDateTime bilInvDt) {

        bilDatesRepository.updateInvoiceCodeByAdjDueDate(newInvoiceIndicator, accountId, bilAdjDueDt, bilInvDt,
                FUTURE_INVOICE_CODES);
        bilCrgAmountsRepository.updateInvoiceCdBybilAdjDueDt(newInvoiceIndicator, accountId, bilAdjDueDt, bilInvDt,
                FUTURE_INVOICE_CODES);
        bilIstScheduleRepository.updateInvoiceCodeByAdjustDate(newInvoiceIndicator, accountId, bilAdjDueDt, bilInvDt,
                FUTURE_INVOICE_CODES);

    }

    private void pendingCancellationServiceChargeUpdate(ZonedDateTime adjustDueDate, ZonedDateTime invoiceDate,
            AdjustDatesReviewDetails adjustDatesReviewDetails, BilAccount bilAccount,
            VariableSeviceCharge variableSeviceCharge, ChargeVariables chargeVariables, char newInvoiceIndicator,
            Double chargeAmount, char dateType, VariableSeviceChargeOverride variableSeviceChargeOverride,
            AccountAdjustSupportData accountAdjustSupportData, List<BilEftPlnCrg> bilEftPlanChargeList,
            List<BilSptPlnCrg> bilSupportPlanChargeList, AccountAdjustDates accountAdjustDates) {

        readServiceAmount(adjustDatesReviewDetails, bilAccount, variableSeviceCharge, adjustDueDate, chargeVariables,
                bilEftPlanChargeList, bilSupportPlanChargeList, newInvoiceIndicator, accountAdjustSupportData,
                accountAdjustDates, variableSeviceChargeOverride);

        if (variableSeviceChargeOverride.getRuleInd() == CHAR_Y && newInvoiceIndicator == BLANK_CHAR) {

            getServiceChargeTermAmount(variableSeviceChargeOverride, adjustDueDate, bilAccount.getAccountId(),
                    variableSeviceCharge, accountAdjustSupportData);

            chargeAmount = accountAdjustSupportData.getServiceChargeAmount();
        } else if (bilAccount.getBillTypeCd().equals(BilDspTypeCode.SUSPENDED.getValue())
                && accountAdjustSupportData.getServiceChargeIndicator() == CHAR_Y
                && accountAdjustSupportData.getServiceChargeTypeCode().equalsIgnoreCase("GA")
                && variableSeviceCharge.getCancelResetIndicator() == CHAR_Y && newInvoiceIndicator == BLANK_CHAR) {
            getServiceChargePerTerm(accountAdjustDates.getQuoteIndicator(), variableSeviceCharge, adjustDueDate,
                    accountAdjustSupportData, bilAccount.getAccountId());
            chargeAmount = accountAdjustSupportData.getServiceChargeAmount();
        }
        if (newInvoiceIndicator == BLANK_CHAR) {
            BilObjectCfg bilObjectCfg = bilObjectCfgRepository.findById(new BilObjectCfgId(BCMOCRG1, 'U')).orElse(null);
            if (bilObjectCfg != null && bilObjectCfg.getBocObjPrcCd() == 'Y'
                    && variableSeviceChargeOverride.getRuleInd() != CHAR_Y
                    && accountAdjustSupportData.getServiceChargeAmount() > 0) {
                AccountChargesData accountChargesData = mapAccountChargesData(bilAccount.getAccountId(), adjustDueDate,
                        invoiceDate, accountAdjustSupportData, bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                        accountAdjustDates.getProcessDate(), accountAdjustDates.getUserSequenceId());
                chargesUserExitService.chargeProcess(accountChargesData);
                accountAdjustSupportData.setAccountPlanChargeAmount(accountChargesData.getServiceChargeAmount());
                accountAdjustSupportData.setServiceChargeCode(accountChargesData.getServiceChargeCode());
            }
            chargeAmount = accountAdjustSupportData.getServiceChargeAmount();
        }
        Integer count = 0;
        if (chargeAmount == DECIMAL_ZERO) {
            count = bilCrgAmountsRepository.deleteServiceChargesByDates(bilAccount.getAccountId(), adjustDueDate,
                    invoiceDate, ChargeType.SERVICECHARGES.getChargeType(), FUTURE_INVOICE_CODES);
        }

        if (count == null || count == 0) {

            Integer countValue = bilCrgAmountsRepository.updateInvoiceDateChargeTypeInvoiceCode(newInvoiceIndicator,
                    chargeAmount, bilAccount.getAccountId(), adjustDueDate, invoiceDate,
                    ChargeType.SERVICECHARGES.getChargeType(), FUTURE_INVOICE_CODES);

            if (countValue == SHORT_ZERO && chargeAmount > DECIMAL_ZERO) {

                insertUpdateChargeAmounts(bilAccount.getAccountId(), adjustDueDate, invoiceDate, chargeAmount,
                        newInvoiceIndicator, bilAccount.getBillTypeCd(), dateType, adjustDatesReviewDetails,
                        accountAdjustSupportData, accountAdjustDates.getProcessDate(), accountAdjustDates.getUserSequenceId());
            }
        }
    }

    private AccountChargesData mapAccountChargesData(String accountId, ZonedDateTime adjustDueDate,
            ZonedDateTime invoiceDate, AccountAdjustSupportData accountAdjustSupportData, String accountType,
            String accountPlan,ZonedDateTime currentDate, String userSequenceId) {
        AccountChargesData accountChargesData = new AccountChargesData();
        accountChargesData.setAccountId(accountId);
        accountChargesData.setApplicationName("BCMOAD");
        accountChargesData.setDueDate(adjustDueDate);
        accountChargesData.setInvoiceDate(invoiceDate);
        accountChargesData.setServiceChargeIndicator(accountAdjustSupportData.getServiceChargeIndicator());
        accountChargesData.setServiceChargeAmount(accountAdjustSupportData.getServiceChargeAmount());
        accountChargesData.setServiceChargeCode('S');
        accountChargesData.setServiceChargeTypeCode(accountAdjustSupportData.getServiceChargeTypeCode());
        accountChargesData.setCurrentSE3Date(currentDate);
        accountChargesData.setUserId(userSequenceId);
        accountChargesData.setAccountType(accountType);
        accountChargesData.setAccountPlan(accountPlan);
        return accountChargesData;
    }

    @Override
    public void insertUpdateChargeAmounts(String accountId, ZonedDateTime maxAdjustDueDate,
            ZonedDateTime maxInvoiceDate, Double serviceChargeAmount, char newInvoiceIndicator, String bilTypeCode,
            Character dateType, AdjustDatesReviewDetails adjustDatesReviewDetails,
            AccountAdjustSupportData accountAdjustSupportData, ZonedDateTime currentDate, String userSequenceId) {

        if (accountAdjustSupportData.getServiceChargeAmount() != DECIMAL_ZERO && serviceChargeAmount != DECIMAL_ZERO
                && dateType.charValue() != DateTypeCode.OUTOFCYCLE_BILLING.getValue()) {
            int count = checkForExistingServiceCharges(accountId, maxAdjustDueDate, maxInvoiceDate);
            if (count == SHORT_ZERO) {
                boolean isInsert = true;
                boolean isWaive = checkWaiveServiceCharge(adjustDatesReviewDetails.getWsceRule(), accountId,
                        maxAdjustDueDate);

                if (!isWaive) {
                    BilObjectCfg bilObjectCfg = bilObjectCfgRepository.findById(new BilObjectCfgId(BCMOCRG1, 'U'))
                            .orElse(null);
                    if (bilObjectCfg != null && bilObjectCfg.getBocObjPrcCd() == 'Y') {
                        BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
                        if (bilAccount == null) {
                            throw new InvalidDataException("no.account.exist");
                        }
                        AccountChargesData accountChargesData = mapAccountChargesData(accountId, maxAdjustDueDate,
                                maxInvoiceDate, accountAdjustSupportData, bilAccount.getBillTypeCd(),
                                bilAccount.getBillClassCd(), currentDate, userSequenceId);
                        chargesUserExitService.chargeProcess(accountChargesData);
                        accountAdjustSupportData
                                .setAccountPlanChargeAmount(accountChargesData.getServiceChargeAmount());
                        accountAdjustSupportData.setServiceChargeCode(accountChargesData.getServiceChargeCode());
                    }
                    if (bilTypeCode.equals(BillingTypeCode.SINGLEPOLICY.toString())) {
                        isInsert = validateInsertForSinglePolicyAccount(accountAdjustSupportData.getServiceChargeCode(),
                                accountId, maxAdjustDueDate, maxInvoiceDate);
                    }
                    if (isInsert) {
                        insertServiceChargeRow(accountId, maxAdjustDueDate, maxInvoiceDate, serviceChargeAmount,
                                newInvoiceIndicator);
                    }
                }

            }
        }
    }

    private void insertServiceChargeRow(String accountId, ZonedDateTime adjustDueDate, ZonedDateTime minimumInvoiceDate,
            double chargeAmount, char newInvoiceIndicator) {
        logger.debug("insert service charge row to BIL_CRG_AMOUNTS");

        short sequenceNumber = getChargeMaxSequenceNumber(accountId);
        BilCrgAmounts serviceChargeAmounts = new BilCrgAmounts();
        serviceChargeAmounts.setBilCrgAmountsId(new BilCrgAmountsId(accountId, sequenceNumber));
        serviceChargeAmounts.setBilAdjDueDt(adjustDueDate);
        serviceChargeAmounts.setBilInvDt(minimumInvoiceDate);
        serviceChargeAmounts.setBcaCrgAmt(chargeAmount);
        serviceChargeAmounts.setBcaWroCrgAmt(DECIMAL_ZERO);
        serviceChargeAmounts.setBcaCrgPaidAmt(DECIMAL_ZERO);
        serviceChargeAmounts.setBilInvoiceCd(newInvoiceIndicator);
        serviceChargeAmounts.setBilCrgTypeCd(ChargeType.SERVICECHARGES.getChargeType());
        bilCrgAmountsRepository.saveAndFlush(serviceChargeAmounts);

    }

    private short getChargeMaxSequenceNumber(String accountId) {
        Integer sequenceNumber = bilCrgAmountsRepository.getMaxSeqNumberByAccountId(accountId);
        if (sequenceNumber == null) {
            return SHORT_ZERO;
        } else {
            return (short) (sequenceNumber + SHORT_ONE);
        }
    }

    private boolean validateInsertForSinglePolicyAccount(char serviceChargeAction, String accountId,
            ZonedDateTime adjustDueDate, ZonedDateTime invoiceDate) {
        logger.debug("perform validations for Single Policy Account prior inserting service charge row");
        boolean isInsert = true;
        switch (ServiceChargeActionCode.getEnumValue(serviceChargeAction)) {
        case ASSES_ALLSCHEDULEDINVOICES:
            break;
        case DONOTASSESS:
            isInsert = false;
            break;
        case SUPPRESS_RENEWALDOWNPAYINVOICE:
            isInsert = suppressRenewalDownpayment(accountId, adjustDueDate, invoiceDate);
            break;
        case SUPPRESS_PRERENEWAL_DOWNPAYINVOICE:
            isInsert = suppressPreRenewalDownpayment(accountId, adjustDueDate, invoiceDate);
            break;
        case SUPPRESS_RENEWAL_PRERENEWAL_DOWNPAYINVOICFE:
            isInsert = suppressRenewalDownpayment(accountId, adjustDueDate, invoiceDate);
            if (isInsert) {
                isInsert = suppressPreRenewalDownpayment(accountId, adjustDueDate, invoiceDate);
            }
            break;
        case EMPTY_CHAR:
            break;
        }
        return isInsert;
    }

    private boolean suppressRenewalDownpayment(String accountId, ZonedDateTime adjustDueDate,
            ZonedDateTime invoiceDate) {
        Integer count = bilAmountsRepository.findCountByAmountEffectiveAndProcessIndicator(accountId,
                AdjustDueDatesConstants.getRenewalAmtEffectiveIndicator(),
                AdjustDueDatesConstants.getRenewalAmtProcessIndicator());
        if (count != null && count != SHORT_ZERO) {
            logger.debug("get minimum invoice date to determine whether to suppress renewal downpayment");
            ZonedDateTime minimumInvoiceDate = getMinimumInvoiceDate(accountId, adjustDueDate, invoiceDate);
            if (minimumInvoiceDate != null && minimumInvoiceDate.compareTo(invoiceDate) == SHORT_ZERO) {
                return false;
            }
        }
        return true;
    }

    private ZonedDateTime getMinimumInvoiceDate(String accountId, ZonedDateTime adjustDueDate,
            ZonedDateTime invoiceDate) {
        ZonedDateTime maxSystemDueDate = bilIstScheduleRepository.findMaxSystemDueDate(accountId, adjustDueDate,
                invoiceDate, AdjustDueDatesConstants.getInvoiceCodes(), BLANK_STRING);
        if (maxSystemDueDate != null) {
            ZonedDateTime maxPolicyEffectiveDate = bilIstScheduleRepository.findMaxPolicyEffectiveDateByAdjustDueDate(
                    accountId, adjustDueDate, maxSystemDueDate, AdjustDueDatesConstants.getInvoiceCodes(),
                    BLANK_STRING);
            if (maxPolicyEffectiveDate != null) {
                return bilIstScheduleRepository.findMinInvoiceDateByPolicyEffectiveDate(accountId,
                        maxPolicyEffectiveDate);
            }

        }
        return null;
    }

    private boolean suppressPreRenewalDownpayment(String accountId, ZonedDateTime adjustDueDate,
            ZonedDateTime invoiceDate) {
        Integer count = bilIstScheduleRepository.findCountByDatesInvoiceCodeAndPayableItem(accountId, adjustDueDate,
                invoiceDate, AdjustDueDatesConstants.getInvoiceCodes(), RENEWAL_HALF_DOWNPAY);
        return !(count != null && count != SHORT_ZERO);
    }

    private boolean checkWaiveServiceCharge(char ruleCode, String accountId, ZonedDateTime adjustDueDate) {
        logger.debug("determine whether to waive service charge row based on WSCE rule");
        boolean isWaive = false;
        switch (WaiveServiceChargeCode.getEnumValue(ruleCode)) {
        case EXCLUDESERVICECHARGE:
            isWaive = checkBilAmountsForExcludeServiceChargeRule(accountId, adjustDueDate);
            break;
        case INCLUDESERVICECHARGE:
            isWaive = checkBilAmountsForIncludeServiceChargeRule(accountId, adjustDueDate);
            break;
        case EXCLUDESERVICECHARGE_NBSFIRSTINSTALLMENT:
            isWaive = checkBilAmountsForExcludeServiceChargeNBSRule(accountId, adjustDueDate);
            break;
        case OFF:
            break;
        case EMPTY_CHAR:
            break;
        }
        return isWaive;
    }

    private int checkForExistingServiceCharges(String accountId, ZonedDateTime adjustDueDate,
            ZonedDateTime minimumInvoiceDate) {
        logger.debug("check for any existing service charge");
        Integer count = bilCrgAmountsRepository.findServiceChargesCount(accountId, adjustDueDate, minimumInvoiceDate,
                ChargeType.SERVICECHARGES.getChargeType());
        if (count != null) {
            return count;
        }
        return SHORT_ZERO;
    }

    private boolean checkBilAmountsForExcludeServiceChargeRule(String accountId, ZonedDateTime adjustDueDate) {
        boolean isWaive = false;
        Integer bilAmountsCount = bilAmountsRepository.findCountByEffectiveIndicatorOrReasonType(accountId,
                AdjustDueDatesConstants.getAmountEffectiveIndicator(), AdjustDueDatesConstants.getDescriptionReasons());

        if (bilAmountsCount != null && bilAmountsCount != SHORT_ZERO) {
            isWaive = checkBilAmountsExistance(accountId, adjustDueDate);
        } else {
            isWaive = checkFutureInstallmentExistance(accountId, adjustDueDate);
        }
        return isWaive;

    }

    private boolean checkFutureInstallmentExistance(String accountId, ZonedDateTime adjustDueDate) {
        Integer futureInstallmentCount = bilIstScheduleRepository.findCountByAdjustDateInvoiceCodeAndAmount(accountId,
                adjustDueDate, FUTURE_INVOICE_CODES, DECIMAL_ZERO);
        return futureInstallmentCount == null || futureInstallmentCount == SHORT_ZERO;
    }

    private boolean checkBilAmountsForIncludeServiceChargeRule(String accountId, ZonedDateTime adjustDueDate) {
        boolean isWaive = false;
        Integer penaltyChargesCount = bilCrgAmountsRepository.findPenaltyChargesCount(accountId, adjustDueDate,
                AdjustDueDatesConstants.getChargeTypes());
        if (penaltyChargesCount == null || penaltyChargesCount == SHORT_ZERO) {
            Integer bilAmountsCount = bilAmountsRepository.findCountByEffectiveIndicatorOrReasonType(accountId,
                    AdjustDueDatesConstants.getAmountEffectiveIndicator(),
                    AdjustDueDatesConstants.getDescriptionReasons());
            if (bilAmountsCount != null && bilAmountsCount != SHORT_ZERO) {
                isWaive = checkBilAmountsExistance(accountId, adjustDueDate);
            } else {
                isWaive = checkFutureInstallmentExistance(accountId, adjustDueDate);
            }
        }
        return isWaive;
    }

    private boolean checkBilAmountsForExcludeServiceChargeNBSRule(String accountId, ZonedDateTime adjustDueDate) {
        Integer count = bilAmountsRepository.checkBilAmountsExistance(accountId, adjustDueDate,
                AdjustDueDatesConstants.getAmountEffectiveIndicator(), AdjustDueDatesConstants.getDescriptionReasons(),
                FUTURE_INVOICE_CODES, DECIMAL_ZERO);
        return count == null || count == SHORT_ZERO;
    }

    @Override
    public boolean checkBilAmountsExistance(String accountId, ZonedDateTime adjustDueDate) {
        Integer count = bilAmountsRepository.checkBilAmountsExistanceForExcludeIncludeRule(accountId, adjustDueDate,
                AdjustDueDatesConstants.getAmountEffectiveIndicator(), AdjustDueDatesConstants.getDescriptionReasons(),
                FUTURE_INVOICE_CODES, DECIMAL_ZERO);
        return count == null || count == SHORT_ZERO;
    }

    @Override
    public void readServiceAmount(AdjustDatesReviewDetails adjustDatesReviewDetails, BilAccount bilAccount,
            VariableSeviceCharge variableSeviceCharge, ZonedDateTime adjustDueDate, ChargeVariables chargeVariables,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, char newInvInd,
            AccountAdjustSupportData accountAdjustSupportData, AccountAdjustDates accountAdjustDates,
            VariableSeviceChargeOverride variableSeviceChargeOverride) {

        boolean isOneBegRow = false;
        boolean isOneBsgRow = false;
        ZonedDateTime dfltEffectiveDate = null;
        char dfltEftScgIndicator = BLANK_CHAR;
        String dfltBspScgchgTypCd = BLANK_STRING;

        boolean collectionMethod = adjustDatesReviewDetails.isEftCollectionMethod();
        BilEftPlnCrg bilEftPlnCrg = new BilEftPlnCrg();
        boolean isTableEntryFnd = false;

        if (bilEftPlanChargeList != null && !bilEftPlanChargeList.isEmpty()
                && bilEftPlanChargeList.size() == SHORT_ONE) {
            isOneBegRow = true;
            bilEftPlnCrg = bilEftPlanChargeList.get(bilEftPlanChargeList.size() - 1);
        }

        if (bilSupportPlanChargeList != null && !bilSupportPlanChargeList.isEmpty()
                && bilSupportPlanChargeList.size() == SHORT_ONE) {
            isOneBsgRow = true;
        }

        if (newInvInd != BLANK_CHAR) {
            return;
        }

        if (bilAccount.getThirdPartyIdentifier().trim().isEmpty()
                && bilAccount.getAgentAccountNumber().trim().isEmpty()) {
            if (isOneBegRow && isOneBsgRow && collectionMethod && !bilAccount.getCollectionPlan().isEmpty()
                    && bilEftPlnCrg.getBilEftScgInd() == CHAR_Y) {
                accountAdjustSupportData.setServiceChargeAmount(bilEftPlnCrg.getBilSvcCrgAmt());
                variableSeviceCharge.setAcctPlanSvcAmt(bilEftPlnCrg.getBilSvcCrgAmt());
                return;
            } else if (!collectionMethod && isOneBsgRow
                    && bilAccount.getBillTypeCd().equals(AccountTypeCode.SINGLE_POLICY.getValue())
                    && accountAdjustSupportData.getServiceChargeIndicator() == CHAR_Y
                    && accountAdjustSupportData.getServiceChargeTypeCode().equals("GA")) {
                variableSeviceCharge.setGradPrmAmt(accountAdjustSupportData.getPremiumIncAmount());
                variableSeviceCharge.setGradScgAmt(accountAdjustSupportData.getSeviceChargeIncAmount());
                variableSeviceCharge.setCalculatedScgAmt(accountAdjustSupportData.getServiceChargeAmount());
                variableSeviceCharge.setCalculatedPrmAmt(accountAdjustSupportData.getPremiumAmount());
                return;
            }

            if (adjustDatesReviewDetails.getServiceDueDate() == null
                    || adjustDueDate != adjustDatesReviewDetails.getServiceDueDate()) {
                chargeVariables.setPrevTrmEffDate(null); // need to add
                chargeVariables.setAcctDueDate(adjustDueDate);
                adjustDatesReviewDetails.setServiceDueDate(adjustDueDate);
                chargeVariables.setAccountId(bilAccount.getAccountId());
                chargeVariables.setSpcrRule(adjustDatesReviewDetails.isSpcrRule());
                chargeVariables.setEffectiveDate(null);
                getMaxTerm(bilAccount.getAccountId(), chargeVariables);
                if (chargeVariables.getEffectiveDate() == null) {
                    chargeVariables.setEffectiveDate(accountAdjustDates.getProcessDate());
                }
            }

            if (chargeVariables.getEffectiveDate().compareTo(adjustDatesReviewDetails.getServiceDueDate()) != 0
                    || chargeVariables.getChargeEffectiveTypeCd() != adjustDatesReviewDetails.getEfftiveTypeCode()) {
                adjustDatesReviewDetails.setTermEffDate(chargeVariables.getEffectiveDate());
                adjustDatesReviewDetails.setEfftiveTypeCode(chargeVariables.getChargeEffectiveTypeCd());
                bilEftPlnCrg.setBilEftScgInd(BLANK_CHAR);
                if (variableSeviceChargeOverride.getRuleInd() != CHAR_Y
                        && adjustDatesReviewDetails.isEftCollectionMethod()
                        && !bilAccount.getCollectionPlan().isEmpty()) {
                    dfltEffectiveDate = null;
                    dfltEftScgIndicator = BLANK_CHAR;
                    double dfltSvcCrgAmount = DECIMAL_ZERO;

                    for (BilEftPlnCrg bilEfPlnCrgs : bilEftPlanChargeList) {
                        if (bilEfPlnCrgs.getBilEftPlnCrgId().getEffectiveDt()
                                .compareTo(chargeVariables.getEffectiveDate()) > 0) {
                            if (bilEfPlnCrgs.getBilEftPlnCrgId().getBilCrgEffTypCd() == chargeVariables
                                    .getChargeEffectiveTypeCd()
                                    || bilEfPlnCrgs.getBilEftPlnCrgId().getBilCrgEffTypCd() == CHAR_A
                                            && (dfltEffectiveDate == null || dfltEffectiveDate.compareTo(
                                                    bilEfPlnCrgs.getBilEftPlnCrgId().getEffectiveDt()) > 0)) {
                                dfltEffectiveDate = bilEfPlnCrgs.getBilEftPlnCrgId().getEffectiveDt();
                                dfltEftScgIndicator = bilEfPlnCrgs.getBilEftScgInd();
                                dfltSvcCrgAmount = bilEfPlnCrgs.getBilSvcCrgAmt();
                            } else if (bilEfPlnCrgs.getBilEftPlnCrgId().getBilCrgEffTypCd() == chargeVariables
                                    .getChargeEffectiveTypeCd()
                                    || bilEfPlnCrgs.getBilEftPlnCrgId().getBilCrgEffTypCd() == CHAR_A) {
                                isTableEntryFnd = true;
                                bilEftPlnCrg.setBilEftScgInd(bilEfPlnCrgs.getBilEftScgInd());
                                bilEftPlnCrg.setBilSvcCrgAmt(bilEfPlnCrgs.getBilSvcCrgAmt());
                                break;
                            }
                        }
                    }

                    if (!isTableEntryFnd && dfltEffectiveDate != null) {
                        bilEftPlnCrg.setBilEftScgInd(dfltEftScgIndicator);
                        bilEftPlnCrg.setBilSvcCrgAmt(dfltSvcCrgAmount);
                    }
                }
                isTableEntryFnd = false;
                char dfltBspSvcCd = BLANK_CHAR;

                double dfltBspScgAmt = DECIMAL_ZERO;
                double dfltBspScgIncAmt = DECIMAL_ZERO;
                double dfltBspPrmAmt = DECIMAL_ZERO;
                double dfltBspPrmIncAmt = DECIMAL_ZERO;
                double dfltBspMaxScgAmt = DECIMAL_ZERO;
                dfltEffectiveDate = null;
                for (BilSptPlnCrg bilSptPlnCrg : bilSupportPlanChargeList) {
                    if (bilSptPlnCrg.getBilSptPlnCrgId().getEffectiveDt()
                            .compareTo(chargeVariables.getEffectiveDate()) > 0) {
                        if (bilSptPlnCrg.getBilSptPlnCrgId().getBilCrgEffTypCd() == chargeVariables
                                .getChargeEffectiveTypeCd()
                                || bilSptPlnCrg.getBilSptPlnCrgId().getBilCrgEffTypCd() == CHAR_A
                                        && (dfltEffectiveDate == null || dfltEffectiveDate
                                                .compareTo(bilSptPlnCrg.getBilSptPlnCrgId().getEffectiveDt()) > 0)) {
                            dfltEffectiveDate = bilSptPlnCrg.getBilSptPlnCrgId().getEffectiveDt();
                            dfltEftScgIndicator = bilSptPlnCrg.getBilSptScgInd();
                            dfltBspScgchgTypCd = bilSptPlnCrg.getBilScgTypeCd();
                            dfltBspSvcCd = bilSptPlnCrg.getBilScgActionCd();
                            dfltBspScgAmt = bilSptPlnCrg.getBilSvcCrgAmt();
                            dfltBspScgIncAmt = bilSptPlnCrg.getBilScgIcrAmt();
                            dfltBspPrmAmt = bilSptPlnCrg.getBilPrmAmt();
                            dfltBspPrmIncAmt = bilSptPlnCrg.getBilPrmIcrAmt();
                            dfltBspMaxScgAmt = bilSptPlnCrg.getBilMaxScgAmt();
                        } else if (bilSptPlnCrg.getBilSptPlnCrgId().getBilCrgEffTypCd() == chargeVariables
                                .getChargeEffectiveTypeCd()
                                || bilSptPlnCrg.getBilSptPlnCrgId().getBilCrgEffTypCd() == CHAR_A) {
                            isTableEntryFnd = true;
                            accountAdjustSupportData.setServiceChargeIndicator(bilSptPlnCrg.getBilSptScgInd());
                            accountAdjustSupportData.setServiceChargeTypeCode(bilSptPlnCrg.getBilScgTypeCd());
                            accountAdjustSupportData.setServiceChargeCode(bilSptPlnCrg.getBilScgActionCd());
                            accountAdjustSupportData.setServiceChargeAmount(bilSptPlnCrg.getBilSvcCrgAmt());
                            variableSeviceCharge.setAcctPlanSvcAmt(bilSptPlnCrg.getBilSvcCrgAmt());
                            accountAdjustSupportData.setSeviceChargeIncAmount(bilSptPlnCrg.getBilScgIcrAmt());
                            accountAdjustSupportData.setPremiumAmount(bilSptPlnCrg.getBilPrmAmt());
                            accountAdjustSupportData.setPremiumIncAmount(bilSptPlnCrg.getBilPrmIcrAmt());
                            accountAdjustSupportData.setMaxServiceChargeAmount(bilSptPlnCrg.getBilMaxScgAmt());
                            break;
                        }
                    }
                }
                if (!isTableEntryFnd) {
                    if (dfltEffectiveDate != null) {
                        accountAdjustSupportData.setServiceChargeIndicator(dfltEftScgIndicator);
                        accountAdjustSupportData.setServiceChargeTypeCode(dfltBspScgchgTypCd);
                        accountAdjustSupportData.setServiceChargeCode(dfltBspSvcCd);
                        accountAdjustSupportData.setServiceChargeAmount(dfltBspScgAmt);
                        variableSeviceCharge.setAcctPlanSvcAmt(dfltBspScgAmt);
                        accountAdjustSupportData.setSeviceChargeIncAmount(dfltBspScgIncAmt);
                        accountAdjustSupportData.setPremiumAmount(dfltBspPrmAmt);
                        accountAdjustSupportData.setPremiumIncAmount(dfltBspPrmIncAmt);
                        accountAdjustSupportData.setMaxServiceChargeAmount(dfltBspMaxScgAmt);
                    }
                } else if (adjustDatesReviewDetails.isEftCollectionMethod() && !bilAccount.getCollectionPlan().isEmpty()
                        && bilEftPlnCrg.getBilEftScgInd() == CHAR_Y) {
                    accountAdjustSupportData.setServiceChargeAmount(bilEftPlnCrg.getBilSvcCrgAmt());
                    variableSeviceCharge.setAcctPlanSvcAmt(bilEftPlnCrg.getBilSvcCrgAmt());
                }
            }
        }

        if (bilAccount.getBillTypeCd().equals(AccountTypeCode.SINGLE_POLICY.getValue())
                && accountAdjustSupportData.getServiceChargeIndicator() == CHAR_Y
                && accountAdjustSupportData.getServiceChargeTypeCode().equals("GA")) {
            variableSeviceCharge.setGradPrmAmt(accountAdjustSupportData.getPremiumIncAmount());
            variableSeviceCharge.setGradScgAmt(accountAdjustSupportData.getSeviceChargeIncAmount());
            variableSeviceCharge.setCalculatedScgAmt(accountAdjustSupportData.getServiceChargeAmount());
            variableSeviceCharge.setCalculatedPrmAmt(accountAdjustSupportData.getPremiumAmount());
        }
    }

    @Override
    @Transactional
    public void fpddCheck(char dateType, AdjustDatesReviewDetails adjustDatesReviewDetails,
            FirstPossibleDueDate firstPossibleDueDate, BilSupportPlan bilSupportPlan,
            AccountAdjustDates accountAdjustDates, BilAccount bilAccount,
            VariableSeviceChargeOverride variableSeviceChargeOverride, VariableSeviceCharge variableSeviceCharge,
            ChargeVariables chargeVariables, boolean isDisableFpdd, boolean isAssignDates,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, boolean isXHOLRule,
            BilRulesUct bilRulesUct, AccountAdjustSupportData accountAdjustSupportData) {

        String accountId = accountAdjustDates.getAccountId();
        ZonedDateTime fpddRefDateHold = null;
        Integer referDurationHold = null;
        boolean isSomethingBilled = true;

        if (adjustDatesReviewDetails.isFpddOnTimeProcessing()) { // need to
                                                                 // implement
            fpddRefDateHold = firstPossibleDueDate.getReferenceDate();
            firstPossibleDueDate.setReferenceDate(firstPossibleDueDate.getReferenceDateOnTime());
            referDurationHold = adjustDatesReviewDetails.getReferenceDuration();
            adjustDatesReviewDetails.setReferenceDuration(SHORT_ZERO);
        }

        boolean isFpddInsert = false;
        Short maxBilSequenceNumber = bilIstScheduleRepository.getMaxBilSequenceNumber(accountId, FUTURE_INVOICE_CODES);

        if (maxBilSequenceNumber == null) {
            ZonedDateTime referenceDate = bilDatesRepository.getMaxReferenceDateByInvoiceCode(accountId,
                    FUTURE_INVOICE_CODES, CHAR_X);
            if (referenceDate == null) {
                isSomethingBilled = false;
                nothingBilled(accountId, dateType, bilAccount, isFpddInsert, isAssignDates, isSomethingBilled,
                        isDisableFpdd, bilSupportPlan, firstPossibleDueDate, variableSeviceCharge,
                        variableSeviceChargeOverride, chargeVariables, accountAdjustDates, bilEftPlanChargeList,
                        bilSupportPlanChargeList, adjustDatesReviewDetails, isXHOLRule, bilRulesUct,
                        accountAdjustSupportData);
            }
        }

        if (isSomethingBilled && !isDisableFpdd) {

            everythingBilled(accountId, dateType, isFpddInsert, isAssignDates, isSomethingBilled, bilSupportPlan,
                    firstPossibleDueDate, adjustDatesReviewDetails, accountAdjustDates, bilAccount,
                    variableSeviceChargeOverride, variableSeviceCharge, chargeVariables, bilEftPlanChargeList,
                    bilSupportPlanChargeList, isXHOLRule, bilRulesUct, accountAdjustSupportData);
        }

        if (adjustDatesReviewDetails.isFpddOnTimeProcessing()) {
            adjustDatesReviewDetails.setReferenceDuration(referDurationHold);
            firstPossibleDueDate.setReferenceDate(fpddRefDateHold);
        }
    }

    private void nothingBilled(String accountId, char dateType, BilAccount bilAccount, boolean isFpddInsert,
            boolean isAssignDates, boolean isSomethingBilled, boolean isDisableFpdd, BilSupportPlan bilSupportPlan,
            FirstPossibleDueDate firstPossibleDueDate, VariableSeviceCharge variableSeviceCharge,
            VariableSeviceChargeOverride variableSeviceChargeOverride, ChargeVariables chargeVariables,
            AccountAdjustDates accountAdjustDates, List<BilEftPlnCrg> bilEftPlanChargeList,
            List<BilSptPlnCrg> bilSupportPlanChargeList, AdjustDatesReviewDetails adjustDatesReviewDetails,
            boolean isXHOLRule, BilRulesUct bilRulesUct, AccountAdjustSupportData accountAdjustSupportData) {

        char newInvInd = BLANK_CHAR;
        Object[] fpddEffectiveCheck = null;
        Object[] fppdDatesObject = null;
        boolean isExist = false;
        ZonedDateTime bilIstDueDate = null;
        boolean isOverlapNextAccount = false;

        if (bilAccount.getStartDueDate().compareTo(firstPossibleDueDate.getDate()) > 0) {
            return;
        }
        ZonedDateTime systemDueDt = bilIstScheduleRepository.getMinSysDueDate(accountId, FUTURE_INVOICE_CODES,
                firstPossibleDueDate.getDate());
        if (systemDueDt != null || accountAdjustDates.getBillPlanChangeIndicator() == CHAR_Y) {
            bilIstDueDate = bilIstScheduleRepository.getMinIstDueDateBySysDueDate(accountId,
                    DateRoutine.defaultDateTime(), FUTURE_INVOICE_CODES);
            if (bilIstDueDate != null) {
                fpddEffectiveCheck = fpddEffectiveCheck(accountId, isFpddInsert, isAssignDates, isSomethingBilled,
                        isDisableFpdd, bilSupportPlan, firstPossibleDueDate, accountAdjustDates.getAddDateIndicator());
                if ((char) fpddEffectiveCheck[4] == CHAR_N) {
                    return;
                }

                if ((char) fpddEffectiveCheck[0] != CHAR_Y) {
                    char forceFpddInt = (char) fpddEffectiveCheck[1];

                    fppdDatesObject = insertFpddDate(dateType, forceFpddInt, accountAdjustDates, firstPossibleDueDate,
                            adjustDatesReviewDetails, isXHOLRule, bilRulesUct);

                    isOverlapNextAccount = (boolean) fppdDatesObject[0];
                    BilDates bilDates = (BilDates) fppdDatesObject[1];

                    if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y) {
                        if (!isOverlapNextAccount) {

                            readServiceAmount(adjustDatesReviewDetails, bilAccount, variableSeviceCharge,
                                    bilDates.getBilAdjDueDt(), chargeVariables, bilEftPlanChargeList,
                                    bilSupportPlanChargeList, newInvInd, accountAdjustSupportData, accountAdjustDates,
                                    variableSeviceChargeOverride);

                        }
                        if (variableSeviceChargeOverride.getRuleInd() == CHAR_Y) {
                            getServiceChargeTermAmount(variableSeviceChargeOverride, bilDates.getBilAdjDueDt(),
                                    bilAccount.getAccountId(), variableSeviceCharge, accountAdjustSupportData);
                        } else if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                                && accountAdjustSupportData.getServiceChargeIndicator() == CHAR_Y
                                && accountAdjustSupportData.getServiceChargeTypeCode().equalsIgnoreCase("GA")
                                && variableSeviceCharge.getCancelResetIndicator() == CHAR_Y) {
                            getServiceChargePerTerm(accountAdjustDates.getQuoteIndicator(), variableSeviceCharge,
                                    bilDates.getBilAdjDueDt(), accountAdjustSupportData, bilAccount.getAccountId());
                        }

                        insertUpdateChargeAmounts(bilAccount.getAccountId(), bilDates.getBilAdjDueDt(),
                                bilDates.getBilInvDt(), accountAdjustSupportData.getServiceChargeAmount(), newInvInd,
                                bilAccount.getBillTypeCd(), bilDates.getBdtDateType(), adjustDatesReviewDetails,
                                accountAdjustSupportData, accountAdjustDates.getProcessDate(), accountAdjustDates.getUserSequenceId());

                        processBelowMinimumServiceCharge(bilAccount, firstPossibleDueDate.isFpddSet(),
                                bilSupportPlan.getBspMinInvAmt(), accountAdjustSupportData.getServiceChargeAmount(),
                                accountAdjustDates, adjustDatesReviewDetails, variableSeviceCharge, chargeVariables,
                                variableSeviceChargeOverride, accountAdjustSupportData, bilEftPlanChargeList,
                                bilSupportPlanChargeList);
                        isExist = true;
                    }
                }
            }
        } else {
            ZonedDateTime systemDueDate = bilDatesRepository.getMinSysDueDtBybilSysDueDt(accountId,
                    firstPossibleDueDate.getDate(), FUTURE_INVOICE_CODES, CHAR_X);
            if (systemDueDate == null) {
                fpddEffectiveCheck = fpddEffectiveCheck(accountId, true, isAssignDates, isSomethingBilled,
                        isDisableFpdd, bilSupportPlan, firstPossibleDueDate, accountAdjustDates.getAddDateIndicator());
                if ((char) fpddEffectiveCheck[4] == CHAR_N) {
                    return;
                }
                char forceFpddInt = (char) fpddEffectiveCheck[1];
                fppdDatesObject = insertFpddDate(dateType, forceFpddInt, accountAdjustDates, firstPossibleDueDate,
                        adjustDatesReviewDetails, isXHOLRule, bilRulesUct);
                isOverlapNextAccount = (boolean) fppdDatesObject[0];
                BilDates bilDates = (BilDates) fppdDatesObject[1];
                if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y) {
                    if (!isOverlapNextAccount) {

                        readServiceAmount(adjustDatesReviewDetails, bilAccount, variableSeviceCharge,
                                bilDates.getBilAdjDueDt(), chargeVariables, bilEftPlanChargeList,
                                bilSupportPlanChargeList, newInvInd, accountAdjustSupportData, accountAdjustDates,
                                variableSeviceChargeOverride);

                    }

                    if (variableSeviceChargeOverride.getRuleInd() == CHAR_Y) {
                        getServiceChargeTermAmount(variableSeviceChargeOverride, bilDates.getBilAdjDueDt(),
                                bilAccount.getAccountId(), variableSeviceCharge, accountAdjustSupportData);
                    } else if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                            && accountAdjustSupportData.getServiceChargeIndicator() == CHAR_Y
                            && accountAdjustSupportData.getServiceChargeTypeCode().equalsIgnoreCase("GA")
                            && variableSeviceCharge.getCancelResetIndicator() == CHAR_Y) {
                        getServiceChargePerTerm(accountAdjustDates.getQuoteIndicator(), variableSeviceCharge,
                                bilDates.getBilAdjDueDt(), accountAdjustSupportData, bilAccount.getAccountId());
                    }

                    insertUpdateChargeAmounts(bilAccount.getAccountId(), bilDates.getBilAdjDueDt(),
                            bilDates.getBilInvDt(), accountAdjustSupportData.getServiceChargeAmount(), newInvInd,
                            bilAccount.getBillTypeCd(), bilDates.getBdtDateType(), adjustDatesReviewDetails,
                            accountAdjustSupportData, accountAdjustDates.getProcessDate(), accountAdjustDates.getUserSequenceId());

                    processBelowMinimumServiceCharge(bilAccount, firstPossibleDueDate.isFpddSet(),
                            bilSupportPlan.getBspMinInvAmt(), accountAdjustSupportData.getServiceChargeAmount(),
                            accountAdjustDates, adjustDatesReviewDetails, variableSeviceCharge, chargeVariables,
                            variableSeviceChargeOverride, accountAdjustSupportData, bilEftPlanChargeList,
                            bilSupportPlanChargeList);
                    isExist = true;
                }
            }

        }

        if (!isExist) {
            ZonedDateTime minSysDueDate = bilIstScheduleRepository.getMinSystemDueDate(accountId, FUTURE_INVOICE_CODES,
                    firstPossibleDueDate.getLookAheadDate());
            if (minSysDueDate != null) {

                fpddEffectiveCheck = fpddEffectiveCheck(accountId, isFpddInsert, isAssignDates, isSomethingBilled,
                        isDisableFpdd, bilSupportPlan, firstPossibleDueDate, accountAdjustDates.getAddDateIndicator());

                if ((char) fpddEffectiveCheck[4] == CHAR_N) {
                    return;
                }
                char forceFpddInt = (char) fpddEffectiveCheck[1];
                boolean isUseLadd = laddEffectiveCheck(bilAccount, firstPossibleDueDate, adjustDatesReviewDetails,
                        forceFpddInt);

                if (!isUseLadd) {
                    return;
                }
                fetchLaddCursor(false, accountId, dateType, bilSupportPlan, firstPossibleDueDate,
                        adjustDatesReviewDetails, accountAdjustDates, bilAccount, variableSeviceChargeOverride,
                        variableSeviceCharge, chargeVariables, newInvInd, bilEftPlanChargeList,
                        bilSupportPlanChargeList, isXHOLRule, bilRulesUct, accountAdjustSupportData);

            } else {
                fpddEffectiveCheck = fpddEffectiveCheck(accountId, isFpddInsert, isAssignDates, isSomethingBilled,
                        isDisableFpdd, bilSupportPlan, firstPossibleDueDate, accountAdjustDates.getAddDateIndicator());
                if ((char) fpddEffectiveCheck[4] == CHAR_N) {
                    return;
                }

                char forceFpddInt = (char) fpddEffectiveCheck[1];
                ZonedDateTime maxReferenceDate = (ZonedDateTime) fpddEffectiveCheck[2];
                bilIstDueDate = (ZonedDateTime) fpddEffectiveCheck[5];

                if (bilIstDueDate.compareTo(firstPossibleDueDate.getReferenceDate()) < 0
                        && bilIstDueDate.compareTo(maxReferenceDate) > 0) {

                    fppdDatesObject = insertFpddDate(dateType, forceFpddInt, accountAdjustDates, firstPossibleDueDate,
                            adjustDatesReviewDetails, isXHOLRule, bilRulesUct);

                    isOverlapNextAccount = (boolean) fppdDatesObject[0];
                    BilDates bilDates = (BilDates) fppdDatesObject[1];
                    if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y) {
                        if (!isOverlapNextAccount) {
                            readServiceAmount(adjustDatesReviewDetails, bilAccount, variableSeviceCharge,
                                    bilDates.getBilAdjDueDt(), chargeVariables, bilEftPlanChargeList,
                                    bilSupportPlanChargeList, newInvInd, accountAdjustSupportData, accountAdjustDates,
                                    variableSeviceChargeOverride);

                        }
                        if (variableSeviceChargeOverride.getRuleInd() == CHAR_Y) {
                            getServiceChargeTermAmount(variableSeviceChargeOverride, bilDates.getBilAdjDueDt(),
                                    bilAccount.getAccountId(), variableSeviceCharge, accountAdjustSupportData);
                        } else if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                                && accountAdjustSupportData.getServiceChargeIndicator() == CHAR_Y
                                && accountAdjustSupportData.getServiceChargeTypeCode().equalsIgnoreCase("GA")
                                && variableSeviceCharge.getCancelResetIndicator() == CHAR_Y) {
                            getServiceChargePerTerm(accountAdjustDates.getQuoteIndicator(), variableSeviceCharge,
                                    bilDates.getBilAdjDueDt(), accountAdjustSupportData, bilAccount.getAccountId());
                        }

                        insertUpdateChargeAmounts(bilAccount.getAccountId(), bilDates.getBilAdjDueDt(),
                                bilDates.getBilInvDt(), accountAdjustSupportData.getServiceChargeAmount(), newInvInd,
                                bilAccount.getBillTypeCd(), bilDates.getBdtDateType(), adjustDatesReviewDetails,
                                accountAdjustSupportData, accountAdjustDates.getProcessDate(), accountAdjustDates.getUserSequenceId());

                        processBelowMinimumServiceCharge(bilAccount, firstPossibleDueDate.isFpddSet(),
                                bilSupportPlan.getBspMinInvAmt(), accountAdjustSupportData.getServiceChargeAmount(),
                                accountAdjustDates, adjustDatesReviewDetails, variableSeviceCharge, chargeVariables,
                                variableSeviceChargeOverride, accountAdjustSupportData, bilEftPlanChargeList,
                                bilSupportPlanChargeList);
                    }
                } else {
                    fetchLaddCursor(true, accountId, dateType, bilSupportPlan, firstPossibleDueDate,
                            adjustDatesReviewDetails, accountAdjustDates, bilAccount, variableSeviceChargeOverride,
                            variableSeviceCharge, chargeVariables, newInvInd, bilEftPlanChargeList,
                            bilSupportPlanChargeList, isXHOLRule, bilRulesUct, accountAdjustSupportData);

                }

            }
        }
    }

    private void everythingBilled(String accountId, char dateType, boolean isFpddInsert, boolean isAssignDates,
            boolean isSomethingBilled, BilSupportPlan bilSupportPlan, FirstPossibleDueDate firstPossibleDueDate,
            AdjustDatesReviewDetails adjustDatesReviewDetails, AccountAdjustDates accountAdjustDates,
            BilAccount bilAccount, VariableSeviceChargeOverride variableSeviceChargeOverride,
            VariableSeviceCharge variableSeviceCharge, ChargeVariables chargeVariables,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, boolean isXHOLRule,
            BilRulesUct bilRulesUct, AccountAdjustSupportData accountAdjustSupportData) {

        boolean isOverlapNextAcct = false;
        char forceFpddInt = CHAR_N;
        char newInvInd = BLANK_CHAR;
        Object[] fppdDatesObject = null;
        boolean isLaddupdated = false;

        Integer bilIstScheduleCount = bilIstScheduleRepository.countBilIstScheduleByBillInvoiceCd(accountId,
                InvoiceTypeCode.EMPTY_CHAR.getValue(), DateRoutine.defaultDateTime());
        if (bilIstScheduleCount == null || bilIstScheduleCount == SHORT_ZERO) {

            Object[] fpddEffectiveCheck = fpddEffectiveCheck(accountId, isFpddInsert, isAssignDates, isSomethingBilled,
                    false, bilSupportPlan, firstPossibleDueDate, accountAdjustDates.getAddDateIndicator());
            forceFpddInt = (char) fpddEffectiveCheck[1];
            char useFpddIndicator = (char) fpddEffectiveCheck[4];

            if (useFpddIndicator == CHAR_N) {
                return;
            }
            boolean isUseLadd = laddEffectiveCheck(bilAccount, firstPossibleDueDate, adjustDatesReviewDetails,
                    forceFpddInt);

            if (!isUseLadd) {
                return;
            }

            isLaddupdated = fetchLaddCursor(false, accountId, dateType, bilSupportPlan, firstPossibleDueDate,
                    adjustDatesReviewDetails, accountAdjustDates, bilAccount, variableSeviceChargeOverride,
                    variableSeviceCharge, chargeVariables, newInvInd, bilEftPlanChargeList, bilSupportPlanChargeList,
                    isXHOLRule, bilRulesUct, accountAdjustSupportData);

        }
        if (!isLaddupdated) {
            isFpddInsert = true;
            Object[] fpddEffectiveCheck = fpddEffectiveCheck(accountId, isFpddInsert, isAssignDates, isSomethingBilled,
                    false, bilSupportPlan, firstPossibleDueDate, accountAdjustDates.getAddDateIndicator());
            char useFpddIndicator = (char) fpddEffectiveCheck[4];
            if (useFpddIndicator == CHAR_N) {
                return;
            }
            fppdDatesObject = insertFpddDate(dateType, forceFpddInt, accountAdjustDates, firstPossibleDueDate,
                    adjustDatesReviewDetails, isXHOLRule, bilRulesUct);

            isOverlapNextAcct = (boolean) fppdDatesObject[0];
            BilDates bilDates = (BilDates) fppdDatesObject[1];

            if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y && !isOverlapNextAcct) {

                readServiceAmount(adjustDatesReviewDetails, bilAccount, variableSeviceCharge, bilDates.getBilAdjDueDt(),
                        chargeVariables, bilEftPlanChargeList, bilSupportPlanChargeList, newInvInd,
                        accountAdjustSupportData, accountAdjustDates, variableSeviceChargeOverride);

                if (variableSeviceChargeOverride.getRuleInd() == CHAR_Y) {
                    getServiceChargeTermAmount(variableSeviceChargeOverride, bilDates.getBilAdjDueDt(),
                            bilAccount.getAccountId(), variableSeviceCharge, accountAdjustSupportData);
                } else if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                        && accountAdjustSupportData.getServiceChargeIndicator() == CHAR_Y
                        && accountAdjustSupportData.getServiceChargeTypeCode().equalsIgnoreCase("GA")
                        && variableSeviceCharge.getCancelResetIndicator() == CHAR_Y) {
                    getServiceChargePerTerm(accountAdjustDates.getQuoteIndicator(), variableSeviceCharge,
                            bilDates.getBilAdjDueDt(), accountAdjustSupportData, bilAccount.getAccountId());
                }

                insertUpdateChargeAmounts(bilAccount.getAccountId(), bilDates.getBilAdjDueDt(), bilDates.getBilInvDt(),
                        accountAdjustSupportData.getServiceChargeAmount(), newInvInd, bilAccount.getBillTypeCd(),
                        bilDates.getBdtDateType(), adjustDatesReviewDetails, accountAdjustSupportData,
                        accountAdjustDates.getProcessDate(), accountAdjustDates.getUserSequenceId());

            }
        }
    }

    @Override
    public void getServiceChargePerTerm(String quoteIndicator, VariableSeviceCharge variableSeviceCharge,
            ZonedDateTime adjustDueDate, AccountAdjustSupportData accountAdjustSupportData, String accountId) {

        variableSeviceCharge.setBccBilAdjDueDt(adjustDueDate);

        getIstPolicyTerm(accountId, variableSeviceCharge);

        if (variableSeviceCharge.getIstPolEffDt() != DateRoutine.defaultDateTime()
                && variableSeviceCharge.getIstPolEffDt() != null) {
            calculateServicePerTerm(quoteIndicator, variableSeviceCharge, accountId, accountAdjustSupportData);
            if (variableSeviceCharge.getNewTermScgAmt() != 0) {
                accountAdjustSupportData.setServiceChargeAmount(variableSeviceCharge.getNewTermScgAmt());
            }
        } else {
            accountAdjustSupportData.setServiceChargeAmount(variableSeviceCharge.getAcctPlanSvcAmt());
        }
    }

    public void getIstPolicyTerm(String accountId, VariableSeviceCharge variableSeviceCharge) {
        Integer count = bilPolicyTermRepository.countPolicyTerm(accountId);
        if (count == null || count == SHORT_ZERO) {
            variableSeviceCharge.setNumOfTerms(SHORT_ZERO);
            variableSeviceCharge.setApMaxEffDt(DateRoutine.defaultDateTime());
        } else {
            variableSeviceCharge.setNumOfTerms(count.shortValue());
        }

        List<BilIstSchedule> bilIstScheduleList = bilIstScheduleRepository.getBilIstScheduleByBilSysDueDate(accountId,
                DateRoutine.defaultDateTime());
        if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
            variableSeviceCharge.setPolicyId(bilIstScheduleList.get(SHORT_ZERO).getBillIstScheduleId().getPolicyId());
        } else {
            List<String> policyIdList = bilIstScheduleRepository.getPolicyIdByBilAdjDueDt(accountId,
                    variableSeviceCharge.getBccBilAdjDueDt());
            if (policyIdList != null && !policyIdList.isEmpty()) {
                variableSeviceCharge.setPolicyId(policyIdList.get(SHORT_ZERO));
                ZonedDateTime policyEffectiveDate = bilIstScheduleRepository.getMaxPolicyEffectiveDateByPolicyId(
                        accountId, variableSeviceCharge.getPolicyId(), variableSeviceCharge.getBccBilAdjDueDt());
                variableSeviceCharge.setIstPolEffDt(policyEffectiveDate);
            } else {
                variableSeviceCharge.setApMaxEffDt(DateRoutine.defaultDateTime());
            }
        }

    }

    @Override
    public void calculateServicePerTerm(String quoteIndicator, VariableSeviceCharge variableSeviceCharge,
            String accountId, AccountAdjustSupportData accountAdjustSupportData) {

        variableSeviceCharge.setCancelResetIndicator(CHAR_N);
        variableSeviceCharge.setMaxPolEffDt(variableSeviceCharge.getIstPolEffDt());

        if (variableSeviceCharge.getIstPolEffDt() == variableSeviceCharge.getApMaxEffDt()
                && variableSeviceCharge.getBcmoppKickIndicator() != CHAR_Y) {
            return;
        }
        if (variableSeviceCharge.getBcmoppKickIndicator() == CHAR_Y) {
            variableSeviceCharge.setBcmoppKickIndicator(CHAR_N);
        }

        BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository.findById(new BilPolicyTermId(accountId,
                variableSeviceCharge.getPolicyId(), variableSeviceCharge.getMaxPolEffDt())).orElse(null);

        if (bilPolicyTerm == null) {
            throw new DataNotFoundException("no.data.found.policy",
                    new Object[] { accountId, variableSeviceCharge.getPolicyId() });
        }
        variableSeviceCharge.setMaxPolicyExpDt(bilPolicyTerm.getPlnExpDt());
        variableSeviceCharge.setIssueSystemId(bilPolicyTerm.getBptIssueSysId());

        getTotalPremium(quoteIndicator, variableSeviceCharge, accountId);
        if (variableSeviceCharge.getAnnualPremium() < accountAdjustSupportData.getPremiumAmount()) {
            variableSeviceCharge.setCalculatedScgAmt(accountAdjustSupportData.getPremiumAmount());
        } else {
            while (variableSeviceCharge.getCancelResetIndicator() != CHAR_Y) {
                addServiceCharge(variableSeviceCharge, accountAdjustSupportData);
            }
        }
        if (variableSeviceCharge.getCalculatedScgAmt() > accountAdjustSupportData.getMaxServiceChargeAmount()
                && accountAdjustSupportData.getMaxServiceChargeAmount() != 0) {
            variableSeviceCharge.setCalculatedScgAmt(accountAdjustSupportData.getMaxServiceChargeAmount());
        }
        variableSeviceCharge.setNewTermScgAmt(variableSeviceCharge.getCalculatedScgAmt());
    }

    private void addServiceCharge(VariableSeviceCharge variableSeviceCharge,
            AccountAdjustSupportData accountAdjustSupportData) {
        if (variableSeviceCharge.getCalculatedPrmAmt() < variableSeviceCharge.getAnnualPremium()) {
            if (accountAdjustSupportData.getMaxServiceChargeAmount() == 0) {
                variableSeviceCharge.setCalculatedPrmAmt(
                        variableSeviceCharge.getCalculatedPrmAmt() + variableSeviceCharge.getGradPrmAmt());
                variableSeviceCharge.setCalculatedScgAmt(
                        variableSeviceCharge.getCalculatedScgAmt() + variableSeviceCharge.getGradScgAmt());
            } else {
                if (variableSeviceCharge.getCalculatedScgAmt() < accountAdjustSupportData.getMaxServiceChargeAmount()) {
                    variableSeviceCharge.setCalculatedPrmAmt(
                            variableSeviceCharge.getCalculatedPrmAmt() + variableSeviceCharge.getGradPrmAmt());
                    variableSeviceCharge.setCalculatedScgAmt(
                            variableSeviceCharge.getCalculatedScgAmt() + variableSeviceCharge.getGradScgAmt());
                } else {
                    variableSeviceCharge.setCancelResetIndicator(CHAR_Y);
                }
            }
        } else {
            variableSeviceCharge.setCancelResetIndicator(CHAR_Y);
        }

    }

    private void getTotalPremium(String quoteIndicator, VariableSeviceCharge variableSeviceCharge, String accountId) {
        BilAmounts bilAmounts = new BilAmounts();
        if (quoteIndicator.charAt(SHORT_ZERO) == 'Q') {
            String bilDesReasonType = bilAmountsRepository.findBilDesReaTypeByBilAmountEffDateAndBilAmountExDate(
                    accountId, variableSeviceCharge.getPolicyId(), Arrays.asList("BAM", "AMT"),
                    variableSeviceCharge.getApMaxEffDt(), variableSeviceCharge.getMaxPolicyExpDt(), CHAR_H);
            if (bilDesReasonType != null) {
                bilAmounts.setBilDesReaTyp(bilDesReasonType);
            }
            setSeviceAnnualPremium(variableSeviceCharge, accountId);
        } else {
            setSeviceAnnualPremium(variableSeviceCharge, accountId);
        }
    }

    private Object[] insertFpddDate(char bilDateType, char forceFpddInt, AccountAdjustDates accountAdjustDates,
            FirstPossibleDueDate firstPossibleDueDate, AdjustDatesReviewDetails adjustDatesReviewDetails,
            boolean isXHOLRule, BilRulesUct bilRulesUct) {

        char fpinvSetIndicator = BLANK_CHAR;
        boolean isOverlapNextAccount = false;
        boolean isOverlap = false;
        boolean isInvoicePaid = false;
        ZonedDateTime maxSysDueDate = null;
        ZonedDateTime maxRefDueDate = null;
        ZonedDateTime newInvoiceDate = null;
        ZonedDateTime crRefDt = null;
        BilAccount bilAccount = adjustDatesReviewDetails.getBilAccount();
        String accountId = bilAccount.getAccountId();
        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();

        ZonedDateTime checkDate = bilDatesRepository.getMaxCivAdjDueDate(accountId, CURRENT_INVOICE_CODES, CHAR_X);
        if (checkDate != null) {

            Object[] invoicePaidObject = deterIfInvoicePaid(accountId, checkDate, bilSupportPlan.getBspMinInvAmt());
            isInvoicePaid = (boolean) invoicePaidObject[0];

            if (!isInvoicePaid || forceFpddInt == CHAR_Y) {

                ZonedDateTime[] pcnPlustObject = getPendingCancelPlusFirst(adjustDatesReviewDetails, checkDate,
                        bilSupportPlan, bilAccount, isXHOLRule, bilRulesUct, accountAdjustDates);
                logger.debug("Retrieved checkDate - npInvoiceDueDate - npReferenceDate - npInvoiceRefDateOnTime"
                        + " - npPcnPlusFirst");
                checkDate = pcnPlustObject[0];
                ZonedDateTime npInvDueDt = pcnPlustObject[1];
                ZonedDateTime npInvoiceRefDate = pcnPlustObject[2];
                if (bilDateType != 'O' && checkDate.compareTo(accountAdjustDates.getProcessDate()) > 0) {
                    isOverlap = true;
                    isOverlapNextAccount = testNextAccountDate(accountId, pcnPlustObject[3]);
                    if (!isOverlapNextAccount) {
                        maxSysDueDate = npInvDueDt;
                        maxRefDueDate = npInvoiceRefDate;
                        newInvoiceDate = checkDate;
                        fpinvSetIndicator = CHAR_Y;
                    } else {
                        return new Object[] { isOverlapNextAccount, null, null, checkDate, fpinvSetIndicator };
                    }
                }
            }
        }

        if (!isOverlap) {
            maxSysDueDate = firstPossibleDueDate.getDate();
            maxRefDueDate = firstPossibleDueDate.getReferenceDate();
            newInvoiceDate = firstPossibleDueDate.getInvoiceDate();
        }

        BilDates bilDates = new BilDates();
        bilDates.setBilDatesId(new BilDatesId(accountId, maxRefDueDate));
        bilDates.setBilSysDueDt(maxSysDueDate);
        bilDates.setBilAdjDueDt(maxSysDueDate);
        bilDates.setBilInvDt(newInvoiceDate);
        bilDates.setBdtDateType(CHAR_I);
        bilDates.setBdtDateInd(CHAR_N);
        bilDates.setBilInvoiceCd(BLANK_CHAR);
        if (bilSupportPlan.getBspNoncpiLetInd() == CHAR_N) {
            bilDates.setBdtNoncpiInd(CHAR_N);
        } else {
            bilDates.setBdtNoncpiInd(CHAR_Y);
        }
        bilDates.setBdtLateCrgInd(CHAR_N);
        bilDates.setBilExcInd(BLANK_CHAR);

        BilDates bilDatesCheck = bilDatesRepository.findByReferenceDate(accountId, maxRefDueDate, CHAR_X);
        if (bilDatesCheck == null) {
            bilDatesRepository.saveAndFlush(bilDates);
            crRefDt = bilDates.getBilDatesId().getBilReferenceDt();
        } else {
            if (bilDatesCheck.getBilInvoiceCd() == CHAR_A) {
                bilIstScheduleRepository.updateScheduleRowsByFpdd(maxSysDueDate, maxSysDueDate, BLANK_CHAR,
                        newInvoiceDate, accountId, maxRefDueDate, FUTURE_INVOICE_CODES);
                bilCreScheduleRepository.updateCreScheduleRowByFpdd(maxSysDueDate, newInvoiceDate, maxSysDueDate,
                        accountId, bilDatesCheck.getBilSysDueDt(), Arrays.asList('2', '4'));
                bilCashDspRepository.updateInvDateAndAdjDateRowByFpdd(newInvoiceDate, maxSysDueDate, maxSysDueDate,
                        accountId, bilDatesCheck.getBilSysDueDt(), ChargeType.DOWNPAYMENTCHARGES.getChargeType());
                bilDatesCheck.setBilSysDueDt(maxSysDueDate);
                bilDatesCheck.setBilAdjDueDt(maxSysDueDate);
                bilDatesCheck.setBilInvDt(newInvoiceDate);
                bilDatesCheck.setBilInvoiceCd(BLANK_CHAR);
                bilDatesCheck.setBdtDateInd(CHAR_N);
                bilDatesRepository.saveAndFlush(bilDatesCheck);
            }
        }

        return new Object[] { isOverlapNextAccount, bilDates, crRefDt, checkDate, fpinvSetIndicator };
    }

    private boolean fetchLaddCursor(boolean islookupAhead, String accountId, char dateType,
            BilSupportPlan bilSupportPlan, FirstPossibleDueDate firstPossibleDueDate,
            AdjustDatesReviewDetails adjustDatesReviewDetails, AccountAdjustDates accountAdjustDates,
            BilAccount bilAccount, VariableSeviceChargeOverride variableSeviceChargeOverride,
            VariableSeviceCharge variableSeviceCharge, ChargeVariables chargeVariables, char newInvInd,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, boolean isXHOLRule,
            BilRulesUct bilRulesUct, AccountAdjustSupportData accountAdjustSupportData) {

        boolean isLaddupdated = false;
        List<Object[]> bilDatesList = null;
        if (!islookupAhead) {
            bilDatesList = bilDatesRepository.fetchDistinctAdjustDateAndInvoiceDate(accountId,
                    firstPossibleDueDate.getLookAheadDate(), FUTURE_INVOICE_CODES, CHAR_X);
        } else {
            bilDatesList = bilDatesRepository.fetchDistinctAdjustDateAndInvoiceDatebyFpdd(accountId,
                    firstPossibleDueDate.getDate(), FUTURE_INVOICE_CODES, CHAR_X);
        }

        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            for (Object[] bilDates : bilDatesList) {
                ZonedDateTime adjustDueDate = (ZonedDateTime) bilDates[0];
                ZonedDateTime invoiceDate = (ZonedDateTime) bilDates[1];
                isLaddupdated = true;
                Object[] updateForFpdd = updateForFpdd(accountId, dateType, bilSupportPlan, bilAccount, BLANK_CHAR,
                        adjustDueDate, invoiceDate, firstPossibleDueDate, adjustDatesReviewDetails, accountAdjustDates,
                        isXHOLRule, bilRulesUct);

                boolean isOverlapNextAcct = (boolean) updateForFpdd[0];
                char fpinvSetIndicator = (char) updateForFpdd[1];
                ZonedDateTime checkDate = (ZonedDateTime) updateForFpdd[2];
                ZonedDateTime npInvoiceDueDate = (ZonedDateTime) updateForFpdd[3];

                if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y) {
                    bilCrgAmountsRepository.deleteServiceChargesByDates(accountId, adjustDueDate, invoiceDate, CHAR_S,
                            FUTURE_INVOICE_CODES);

                }

                if (!isOverlapNextAcct && accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y) {
                    if (fpinvSetIndicator == CHAR_Y) {
                        invoiceDate = checkDate;
                        adjustDueDate = npInvoiceDueDate;
                    } else {
                        adjustDueDate = firstPossibleDueDate.getDate();
                        invoiceDate = firstPossibleDueDate.getInvoiceDate();
                    }

                    readServiceAmount(adjustDatesReviewDetails, bilAccount, variableSeviceCharge, adjustDueDate,
                            chargeVariables, bilEftPlanChargeList, bilSupportPlanChargeList, newInvInd,
                            accountAdjustSupportData, accountAdjustDates, variableSeviceChargeOverride);

                    if (variableSeviceChargeOverride.getRuleInd() == CHAR_Y) {
                        getServiceChargeTermAmount(variableSeviceChargeOverride, adjustDueDate,
                                bilAccount.getAccountId(), variableSeviceCharge, accountAdjustSupportData);
                    } else if (bilAccount.getBillTypeCd().equalsIgnoreCase(AccountTypeCode.SINGLE_POLICY.getValue())
                            && accountAdjustSupportData.getServiceChargeIndicator() == CHAR_Y
                            && accountAdjustSupportData.getServiceChargeTypeCode().equalsIgnoreCase("GA")
                            && variableSeviceCharge.getCancelResetIndicator() == CHAR_Y) {
                        getServiceChargePerTerm(accountAdjustDates.getQuoteIndicator(), variableSeviceCharge,
                                adjustDueDate, accountAdjustSupportData, bilAccount.getAccountId());
                    }

                    insertUpdateChargeAmounts(bilAccount.getAccountId(), adjustDueDate, invoiceDate,
                            accountAdjustSupportData.getServiceChargeAmount(), newInvInd, bilAccount.getBillTypeCd(),
                            dateType, adjustDatesReviewDetails, accountAdjustSupportData,
                            accountAdjustDates.getProcessDate(), accountAdjustDates.getUserSequenceId());

                }
                if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y) {
                    processBelowMinimumServiceCharge(bilAccount, firstPossibleDueDate.isFpddSet(),
                            bilSupportPlan.getBspMinInvAmt(), accountAdjustSupportData.getServiceChargeAmount(),
                            accountAdjustDates, adjustDatesReviewDetails, variableSeviceCharge, chargeVariables,
                            variableSeviceChargeOverride, accountAdjustSupportData, bilEftPlanChargeList,
                            bilSupportPlanChargeList);
                }
            }

        }

        return isLaddupdated;

    }

    private Object[] fpddEffectiveCheck(String accountId, boolean isFpddInsert, boolean isAssignDates,
            boolean isSomethingBilled, boolean isDisableFpdd, BilSupportPlan bilSupportPlan,
            FirstPossibleDueDate firstPossibleDueDate, char addDateIndicator) {

        char nonInvBdtFndInt = CHAR_N;
        char forceFpddInt = CHAR_N;
        ZonedDateTime maxReferenceDate = null;
        ZonedDateTime forceFpddMinRfrDt = null;
        ZonedDateTime minBilIstDueDate = null;

        char useFpddIndicator;
        if (!isFpddInsert && isAssignDates && isSomethingBilled) {
            isDisableFpdd = fpddCheckIfBehind(accountId, bilSupportPlan);
        }
        if (isDisableFpdd) {
            useFpddIndicator = CHAR_N;
        } else {
            if (!isFpddInsert) {
                maxReferenceDate = bilDatesRepository.getMaxReferenceDateBySysDueDate(accountId,
                        firstPossibleDueDate.getDate(), FUTURE_INVOICE_CODES, CHAR_X);
                if (maxReferenceDate != null) {
                    nonInvBdtFndInt = CHAR_Y;
                    forceFpddInt = CHAR_X;
                    maxReferenceDate = bilDatesRepository.getMaxReferenceDateByFpddDate(accountId,
                            firstPossibleDueDate.getLookAheadDate(), firstPossibleDueDate.getDate(),
                            FUTURE_INVOICE_CODES, CHAR_X);
                    if (maxReferenceDate == null) {
                        throw new DataNotFoundException("data.not.found.for.account", new Object[] { accountId });
                    }
                } else {
                    maxReferenceDate = firstPossibleDueDate.getReferenceDate();
                }
            } else {

                maxReferenceDate = firstPossibleDueDate.getReferenceDate();
            }

            useFpddIndicator = CHAR_Y;
            minBilIstDueDate = bilIstScheduleRepository.getMinIstDueDateBySysDueDate(accountId,
                    DateRoutine.defaultDateTime(), FUTURE_INVOICE_CODES);

            if (minBilIstDueDate == null && (addDateIndicator == CHAR_D || addDateIndicator == '1')) {
                ZonedDateTime minIstDueDate = bilIstScheduleRepository.getMinIstDueDate(accountId,
                        FUTURE_INVOICE_CODES);
                if (minIstDueDate == null) {
                    useFpddIndicator = CHAR_N;
                }
            }

            if (minBilIstDueDate != null) {
                if (minBilIstDueDate.compareTo(maxReferenceDate) > 0
                        && minBilIstDueDate.compareTo(firstPossibleDueDate.getReferenceDate()) > 0) {
                    useFpddIndicator = CHAR_N;
                }
                if (maxReferenceDate == firstPossibleDueDate.getReferenceDate()
                        && minBilIstDueDate.compareTo(maxReferenceDate) > 0 && useFpddIndicator == CHAR_N
                        && forceFpddInt != CHAR_X) {
                    maxReferenceDate = bilDatesRepository.getMaxReferenceDateForFpddEffectiveCheck(accountId,
                            minBilIstDueDate, CHAR_S, AdjustDueDatesConstants.getInvoiceCodes(), accountId,
                            AdjustDueDatesConstants.getInvoiceCodes(), CHAR_X);

                    if (maxReferenceDate != null) {
                        maxReferenceDate = firstPossibleDueDate.getReferenceDate();
                        useFpddIndicator = CHAR_Y;
                        forceFpddInt = CHAR_Y;
                        forceFpddMinRfrDt = minBilIstDueDate;
                    } else {
                        forceFpddInt = CHAR_N;
                    }

                }
                if (nonInvBdtFndInt == CHAR_Y && minBilIstDueDate.compareTo(maxReferenceDate) > 0) {
                    nonInvBdtFndInt = CHAR_N;
                }
            }

        }

        return new Object[] { nonInvBdtFndInt, forceFpddInt, maxReferenceDate, forceFpddMinRfrDt, useFpddIndicator,
                minBilIstDueDate };
    }

    private boolean laddEffectiveCheck(BilAccount bilAccount, FirstPossibleDueDate firstPossibleDueDate,
            AdjustDatesReviewDetails adjustDatesReviewDetails, char forceFpddInt) {

        String accountId = bilAccount.getAccountId();
        ZonedDateTime[] referenceDate = getFpddReferenceDate(adjustDatesReviewDetails.getReferenceDuration(),
                firstPossibleDueDate.getDate(), bilAccount, adjustDatesReviewDetails.isFpddOnTimeProcessing());

        firstPossibleDueDate.setReferenceDate(referenceDate[SHORT_ZERO]);
        firstPossibleDueDate.setReferenceDateOnTime(referenceDate[SHORT_ONE]);

        boolean isUseLadd = true;
        ZonedDateTime minBilIstDueDate = bilIstScheduleRepository.getMinIstDueDateBySysDueDate(accountId,
                DateRoutine.defaultDateTime(), FUTURE_INVOICE_CODES);
        BilIstSchedule bilIstSchedule = new BilIstSchedule();
        if (minBilIstDueDate != null) {
            bilIstSchedule.setBillIstDueDt(minBilIstDueDate);
        } else {
            minBilIstDueDate = bilIstScheduleRepository.getMinIstDueDate(accountId, FUTURE_INVOICE_CODES);
            if (minBilIstDueDate != null) {
                bilIstSchedule.setBillIstDueDt(minBilIstDueDate);
            } else {
                isUseLadd = false;
            }
        }
        if (bilIstSchedule.getBillIstDueDt().compareTo(firstPossibleDueDate.getReferenceDate()) > 0
                && forceFpddInt != CHAR_Y) {
            isUseLadd = false;
        }
        return isUseLadd;
    }

    private boolean fpddCheckIfBehind(String accountId, BilSupportPlan bilSupportPlan) {

        ZonedDateTime minUninvBdtRfrDate = bilDatesRepository.getMinReferenceDateForCheckFpdd(accountId,
                FUTURE_INVOICE_CODES, CHAR_S, CHAR_X);
        if (minUninvBdtRfrDate == null) {
            return false;
        }
        ZonedDateTime dateMinus1Freq = DateRoutine.calculateDateTimeBySubtractFrequency(bilSupportPlan.getBilRvwFqyCd(),
                minUninvBdtRfrDate);
        Integer countBilIstSchedule = bilIstScheduleRepository.countBilIstScheduleforCheckFpdd(accountId,
                DateRoutine.defaultDateTime(), FUTURE_INVOICE_CODES, dateMinus1Freq, minUninvBdtRfrDate, BLANK_CHAR);
        return countBilIstSchedule == null || countBilIstSchedule <= 0;
    }

    @Override
    @Transactional
    public Object[] updateForFpdd(String accountId, char bilDateType, BilSupportPlan bilSupportPlan,
            BilAccount bilAccount, char invoiceCode, ZonedDateTime bilAdjustDate, ZonedDateTime bilInvoiceDate,
            FirstPossibleDueDate firstPossibleDueDate, AdjustDatesReviewDetails adjustDatesReviewDetails,
            AccountAdjustDates accountAdjustDates, boolean isXHOLRule, BilRulesUct bilRulesUct) {

        char fpinvSetIndicator = BLANK_CHAR;
        boolean isOverlapNextAccount = false;
        boolean isOverlap = false;
        ZonedDateTime newInvoiceDate = null;
        ZonedDateTime newAdjustDate = null;
        ZonedDateTime npInvDueDt = null;

        ZonedDateTime checkDate = bilDatesRepository.getMaxCivAdjDueDate(accountId, CURRENT_INVOICE_CODES, CHAR_X);

        if (checkDate != null) {
            Object[] invoicePaidObject = deterIfInvoicePaid(accountId, checkDate, bilSupportPlan.getBspMinInvAmt());
            boolean isInvoicePaid = (boolean) invoicePaidObject[0];
            if (!isInvoicePaid) {

                ZonedDateTime[] pcnPlustObject = getPendingCancelPlusFirst(adjustDatesReviewDetails, checkDate,
                        bilSupportPlan, bilAccount, isXHOLRule, bilRulesUct, accountAdjustDates);

                checkDate = pcnPlustObject[0];
                npInvDueDt = pcnPlustObject[1];
                if (bilDateType != 'O' && checkDate.compareTo(accountAdjustDates.getProcessDate()) > 0) {
                    isOverlap = true;
                    isOverlapNextAccount = testNextAccountDate(accountId, pcnPlustObject[3]);
                    if (!isOverlapNextAccount) {
                        newInvoiceDate = checkDate;
                        newAdjustDate = npInvDueDt;
                        fpinvSetIndicator = CHAR_Y;
                    } else {
                        return new Object[] { isOverlapNextAccount, fpinvSetIndicator, checkDate, npInvDueDt };
                    }
                }
            }
        }

        if (!isOverlap) {

            newAdjustDate = firstPossibleDueDate.getDate();
            newInvoiceDate = firstPossibleDueDate.getInvoiceDate();
        }

        bilDatesRepository.updateBilDatesByAdjDueDate(newAdjustDate, newInvoiceDate, invoiceCode, accountId,
                bilAdjustDate, bilInvoiceDate);
        bilIstScheduleRepository.updateRescindScheduleRowByAdjustDate(newAdjustDate, newInvoiceDate, invoiceCode,
                accountId, bilAdjustDate, bilInvoiceDate);
        bilCreScheduleRepository.updateRescindCreScheduleRowBybilAdjDueDt(newAdjustDate, newInvoiceDate, accountId,
                bilAdjustDate, bilInvoiceDate);

        if (invoiceCode == CHAR_Y || invoiceCode == CHAR_L) {

            bilCashDspRepository.updateRescindCashRowByAdjDueDate(newAdjustDate, newInvoiceDate, accountId,
                    bilAdjustDate, bilInvoiceDate);
        } else {
            bilCashDspRepository.updateRescindCashRowByAdjDueDate(newAdjustDate, newInvoiceDate, accountId,
                    bilAdjustDate, bilInvoiceDate, ChargeType.DOWNPAYMENTCHARGES.getChargeType());
        }

        bilCrgAmountsRepository.updateRescindCrgAmountsBybilAdjDueDt(newAdjustDate, newInvoiceDate, accountId,
                bilAdjustDate, bilInvoiceDate, AdjustDueDatesConstants.getChargeTypes());

        return new Object[] { isOverlapNextAccount, fpinvSetIndicator, checkDate, npInvDueDt };
    }

    @Override
    public void getServiceChargeTermAmount(VariableSeviceChargeOverride variableSeviceChargeOverride,
            ZonedDateTime adjustDueDate, String accountId, VariableSeviceCharge variableSeviceCharge,
            AccountAdjustSupportData accountAdjustSupportData) {
        variableSeviceChargeOverride.setCurrentDueDate(adjustDueDate);
        Integer count = bilPolicyTermRepository.countPolicyTerm(accountId);
        if (count != null) {
            variableSeviceChargeOverride.setNumOfTerms(count.shortValue());
        } else {
            variableSeviceChargeOverride.setNumOfTerms(SHORT_ZERO);
            variableSeviceChargeOverride.setIstPolicyEffectiveDate(DateRoutine.defaultDateTime());
            return;
        }

        String policyId = bilPolicyRepository.getPolicyIdByBilAccount(accountId, Arrays.asList('Y', 'Z'));
        if (policyId != null) {
            variableSeviceCharge.setPolicyId(policyId);
        }

        List<BilAmounts> bilAmountsList = bilAmountsRepository.getAmountEffectiveInd(accountId,
                variableSeviceCharge.getPolicyId(), Arrays.asList('7'), Arrays.asList('H', 'V'));
        if (bilAmountsList != null && !bilAmountsList.isEmpty()) {
            BilAmounts bilAmounts = bilAmountsList.get(SHORT_ZERO);
            variableSeviceCharge.setIssueAcyTs(bilAmounts.getBamAmtTs());
            variableSeviceCharge.setBamEffInd(bilAmounts.getBilAmtEffInd());
        }
        variableSeviceChargeOverride.setPolicyId(variableSeviceCharge.getPolicyId());

        // end function getpolicy
        variableSeviceCharge.setPolicyIdTrm(variableSeviceCharge.getPolicyId());
        ZonedDateTime maxPolicyEffectiveDate = bilIstScheduleRepository.getMaxPolicyEffectiveDateByPolicyId(accountId,
                variableSeviceChargeOverride.getPolicyId(), variableSeviceChargeOverride.getCurrentDueDate());
        if (maxPolicyEffectiveDate != null) {
            variableSeviceChargeOverride.setIstPolicyEffectiveDate(maxPolicyEffectiveDate);
        } else {
            variableSeviceChargeOverride.setIstPolicyEffectiveDate(DateRoutine.defaultDateTime());
        }

        variableSeviceChargeOverride
                .setCurrentPolicyEffectiveDate(variableSeviceChargeOverride.getIstPolicyEffectiveDate());
        // ccccCallSvco1PgmNm();
        accountAdjustSupportData.setServiceChargeAmount(variableSeviceChargeOverride.getCalculatedChargeAmount());
        variableSeviceChargeOverride.setPrevChargeAmount(variableSeviceChargeOverride.getCalculatedChargeAmount());
        variableSeviceChargeOverride
                .setPrevPolicyEffectiveDate(variableSeviceChargeOverride.getCurrentPolicyEffectiveDate());
        variableSeviceChargeOverride.setPrevDueDate(variableSeviceChargeOverride.getCurrentDueDate());
        variableSeviceChargeOverride.setCalculatedChargeAmount(DECIMAL_ZERO);
    }

    public boolean testNextAccountDate(String accountId, ZonedDateTime npPcnPlusFirst) {
        boolean isOverlapNextAccount = false;
        ZonedDateTime minAdjustDueDate = bilDatesRepository.getNextMinAdjDueDate(accountId, npPcnPlusFirst,
                FUTURE_INVOICE_CODES, CHAR_X);
        if (minAdjustDueDate != null) {
            isOverlapNextAccount = true;
        }
        return isOverlapNextAccount;
    }

    @Override
    public void processBelowMinimumServiceCharge(BilAccount bilAccount, boolean fpddset, Double minInvoiceAmount,
            double serviceChargeAmount, AccountAdjustDates accountAdjustDates,
            AdjustDatesReviewDetails adjustDatesReviewDetails, VariableSeviceCharge variableSeviceCharge,
            ChargeVariables chargeVariables, VariableSeviceChargeOverride variableSeviceChargeOverride,
            AccountAdjustSupportData accountAdjustSupportData, List<BilEftPlnCrg> bilEftPlanChargeList,
            List<BilSptPlnCrg> bilSupportPlanChargeList) {

        String accountId = bilAccount.getAccountId();
        List<BilDates> bilDatesList = bilDatesRepository.getMaxInvoicedDate(accountId,
                accountAdjustDates.getProcessDate(), FUTURE_INVOICE_CODES,
                BillingExclusionIndicator.DEDUCTIONDATE_EXCLUDED.getValue());

        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            BilDates bilDates = bilDatesList.get(SHORT_ZERO);

            if (bilDates.getBdtDateType() != DateTypeCode.OUTOFCYCLE_BILLING.getValue()) {
                Double scheduleBalanceAmount = bilIstScheduleRepository.getBalanceAmtForInvoiceSchedule(accountId,
                        bilDates.getBilAdjDueDt(), bilDates.getBilInvDt());
                if (scheduleBalanceAmount == null) {
                    scheduleBalanceAmount = DECIMAL_ZERO;
                }

                if (fpddset || scheduleBalanceAmount.compareTo(minInvoiceAmount) >= SHORT_ZERO
                        || accountAdjustDates.getInvoiceIndicator() == CHAR_Y
                        || accountAdjustDates.getSuspendBillIndicator() == CHAR_A
                        || accountAdjustDates.getSuspendBillIndicator() == CHAR_P) {

                    scheduleBalanceAmount = bilIstScheduleRepository.getBalanceAmountForNonZeroPremiumAmount(accountId,
                            bilDates.getBilAdjDueDt(), bilDates.getBilInvDt(), FUTURE_INVOICE_CODES);
                    if (scheduleBalanceAmount == null) {
                        scheduleBalanceAmount = DECIMAL_ZERO;
                    }

                    Double chargeBalanceAmount = bilCrgAmountsRepository.getPenaltyOrLateChargeBalanceAmount(accountId,
                            bilDates.getBilAdjDueDt(), bilDates.getBilInvDt(),
                            Arrays.asList(InvoiceTypeCode.EMPTY_CHAR.getValue(),
                                    InvoiceTypeCode.SCHEDULEDAMOUNT_BELOWMINIMUM_ATTEMPTEDINVOICING.getValue(),
                                    InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue()),
                            AdjustDueDatesConstants.getChargeTypes());

                    if (chargeBalanceAmount == null) {
                        chargeBalanceAmount = DECIMAL_ZERO;
                    }

                    scheduleBalanceAmount += chargeBalanceAmount;

                    char newInvoiceIndicator = getNewInvoiceIndicator(bilDates.getBdtDateType(), scheduleBalanceAmount,
                            minInvoiceAmount);
                    if (newInvoiceIndicator == InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue()) {
                        serviceChargeAmount = DECIMAL_ZERO;
                    }

                    updateInvoiceIndicator(accountId, newInvoiceIndicator, bilDates.getBilAdjDueDt(),
                            bilDates.getBilInvDt());

                    pendingCancellationServiceChargeUpdate(bilDates.getBilAdjDueDt(), bilDates.getBilInvDt(),
                            adjustDatesReviewDetails, bilAccount, variableSeviceCharge, chargeVariables,
                            newInvoiceIndicator, serviceChargeAmount, bilDates.getBdtDateType(),
                            variableSeviceChargeOverride, accountAdjustSupportData, bilEftPlanChargeList,
                            bilSupportPlanChargeList, accountAdjustDates);

                }

            }
        }

    }

    private char getNewInvoiceIndicator(char dateType, Double totalBalanceAmount, Double minimumInvoiceAmount) {
        if ((dateType == DateTypeCode.SCHEDULED.getValue()
                || dateType == DateTypeCode.FIRSTPOSSIBLEDUEDATE_SINGLEPOLICYCANCELLATIONINVOICE.getValue()
                || dateType == DateTypeCode.FIRST_POSSIBLEDUE_LATE.getValue()
                || dateType == DateTypeCode.OUTOFCYCLE_BILLING.getValue())
                && (totalBalanceAmount.compareTo(minimumInvoiceAmount) < SHORT_ZERO
                        || totalBalanceAmount.compareTo(DECIMAL_ZERO) == SHORT_ZERO)) {

            return InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue();
        }
        return BLANK_CHAR;
    }

    @Override
    public ZonedDateTime[] getPendingCancelPlusFirst(AdjustDatesReviewDetails adjustDatesReviewDetails,
            ZonedDateTime checkDateInput, BilSupportPlan bilSupportPlan, BilAccount bilAccount, boolean isXHOLRule,
            BilRulesUct bilRulesUct, AccountAdjustDates accountAdjustDates) {

        boolean excludeWeekend = getOverrideWeekendIndicator(true,
                adjustDatesReviewDetails.getBilActRules().getBruIncWknInd());
        ZonedDateTime checkDate = DateRoutine.getAdjustedDate(checkDateInput, false, bilSupportPlan.getBspPndCncNbr(),
                null);
        checkDate = getPcnPlusOneDay(checkDate, accountAdjustDates.getProcessDate(), excludeWeekend, isXHOLRule,
                bilRulesUct);
        ZonedDateTime npInvDueDt = DateRoutine.getAdjustedDate(checkDate, false,
                adjustDatesReviewDetails.getDueDateDisplacementNumber(), null);

        ZonedDateTime npInvoiceRefDate = getNpReferenceDate(npInvDueDt, adjustDatesReviewDetails.getReferenceDuration(),
                bilAccount);

        ZonedDateTime npPcnPlusFirst = DateRoutine.getAdjustedDate(npInvDueDt, false, bilSupportPlan.getBspPndCncNbr(),
                null);
        npPcnPlusFirst = DateRoutine.getAdjustedDate(npPcnPlusFirst, excludeWeekend, 1, null);

        logger.debug("Retrieved checkDate - npInvoiceDueDate - npReferenceDate - npInvoiceRefDateOnTime"
                + " - npPcnPlusFirst");
        return new ZonedDateTime[] { checkDate, npInvDueDt, npInvoiceRefDate, npPcnPlusFirst };
    }

    private ZonedDateTime getNpReferenceDate(ZonedDateTime npInvoiceDueDate, int referenceDuration,
            BilAccount bilAccount) {

        ZonedDateTime npInvoiceRefDate = npInvoiceDueDate;

        if (referenceDuration != 0) {
            char signIndicator = MINUS_SIGN.charAt(SHORT_ZERO);
            if (bilAccount.getStartReferenceDate().compareTo(bilAccount.getStartDueDate()) > 0) {
                signIndicator = PLUS_SIGN.charAt(SHORT_ZERO);
            }

            npInvoiceRefDate = DateRoutine.adjustDateWithOffset(npInvoiceDueDate, false, referenceDuration,
                    signIndicator, null);
        }

        logger.debug("Retrieved npInvoiceRefDate - npInvoiceRefDateOnTime");
        return npInvoiceRefDate;
    }

    @Override
    public Object[] checkDateAdd(boolean isRestAll, AdjustDatesReviewDetails adjustDatesReviewDetails,
            BilAccount bilAccount, AccountAdjustDates accountAdjustDates, boolean isXHOLRule, BilRulesUct bilRulesUct) {

        ZonedDateTime maxRefenceDueDate = DateRoutine.dateTimeAsYYYYMMDD(0);
        ZonedDateTime maxSystemDueDate = DateRoutine.dateTimeAsYYYYMMDD(0);
        ZonedDateTime expirationDate = null;
        BilDates bilDates = null;
        boolean isBbdtRowExist = false;
        boolean isInvoicePaid = false;
        ZonedDateTime invoicePaidDate = null;
        char dateType = BLANK_CHAR;
        char invoiceCode = BLANK_CHAR;
        ZonedDateTime maxInvoiceAdjustDueDate = DateRoutine.dateTimeAsYYYYMMDD(0);

        Object[] maxDueDates = getMaxDueDates(bilAccount.getAccountId());
        if (maxDueDates != null && maxDueDates.length != 0) {
            maxRefenceDueDate = (ZonedDateTime) maxDueDates[0];
            maxInvoiceAdjustDueDate = (ZonedDateTime) maxDueDates[1];
            maxSystemDueDate = (ZonedDateTime) maxDueDates[2];
            dateType = (char) maxDueDates[3];
            invoiceCode = (char) maxDueDates[4];
            isBbdtRowExist = true;
        }

        ZonedDateTime maxInvoiceDate = bilDatesRepository.getMaxInvoicedDateByDateType(bilAccount.getAccountId(),
                CHAR_F, CHAR_X);
        if (maxInvoiceDate == null) {
            maxInvoiceDate = accountAdjustDates.getProcessDate();
        }

        if (maxDueDates.length == 0) {
            ZonedDateTime[] transferData = transferTest(bilAccount.getAccountId());
            if (transferData != null && transferData.length != 0) {
                maxRefenceDueDate = transferData[0];
                maxSystemDueDate = transferData[1];
                maxInvoiceDate = transferData[2];
            }

        }

        if (maxRefenceDueDate.compareTo(DateRoutine.dateTimeAsYYYYMMDD(0)) == 0
                && bilAccount.getStartReferenceDate().compareTo(maxRefenceDueDate) > 0) {
            maxSystemDueDate = bilAccount.getStartDueDate();
            maxRefenceDueDate = bilAccount.getStartReferenceDate();
            ZonedDateTime nextSchedDt = maxSystemDueDate;
            ZonedDateTime invoiceDate = getFpddInvoiceDate(maxSystemDueDate, accountAdjustDates.getProcessDate(),
                    adjustDatesReviewDetails.getBilActRules().getBruIncWknInd(),
                    adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, bilRulesUct);

            if (adjustDatesReviewDetails.isEftCollectionMethod()
                    && adjustDatesReviewDetails.getFpddIndicator() != CHAR_Y) {
                if (maxSystemDueDate.compareTo(accountAdjustDates.getProcessDate()) <= SHORT_ZERO
                        || maxSystemDueDate.compareTo(adjustDatesReviewDetails.getEarliestEftDueDt()) < SHORT_ZERO) {

                    while (!(nextSchedDt.compareTo(accountAdjustDates.getProcessDate()) > 0
                            && nextSchedDt.compareTo(adjustDatesReviewDetails.getEarliestEftDueDt()) >= 0)) {

                        nextSchedDt = getNextSchedDate(adjustDatesReviewDetails.getBilSupportPlan(), nextSchedDt,
                                bilAccount, CHAR_S);

                        invoiceDate = getFpddInvoiceDate(nextSchedDt, accountAdjustDates.getProcessDate(),
                                adjustDatesReviewDetails.getBilActRules().getBruIncWknInd(),
                                adjustDatesReviewDetails.getDueDateDisplacementNumber(), isXHOLRule, bilRulesUct);
                        if (nextSchedDt.compareTo(accountAdjustDates.getProcessDate()) > 0
                                && invoiceDate.compareTo(accountAdjustDates.getProcessDate()) < 0
                                && nextSchedDt.compareTo(adjustDatesReviewDetails.getEarliestEftDueDt()) >= 0) {
                            invoiceDate = accountAdjustDates.getProcessDate();
                        }
                    }

                } else {
                    if (maxSystemDueDate.compareTo(accountAdjustDates.getProcessDate()) > 0
                            && maxSystemDueDate.compareTo(adjustDatesReviewDetails.getEarliestEftDueDt()) >= 0
                            && invoiceDate.compareTo(accountAdjustDates.getProcessDate()) < 0) {
                        invoiceDate = accountAdjustDates.getProcessDate();
                    }
                }
            }

            bilDates = insertBilDates(bilAccount.getAccountId(), maxSystemDueDate, maxRefenceDueDate, invoiceDate,
                    nextSchedDt, BLANK_CHAR, CHAR_N, CHAR_S, adjustDatesReviewDetails,
                    bilAccount.getSeasionalExclusionCode().trim());
            dateType = bilDates.getBdtDateType();
            invoiceCode = bilDates.getBilInvoiceCd();
            maxInvoiceDate = bilDates.getBilInvDt();
        }

        if (accountAdjustDates.getAddDateIndicator() == CHAR_A) {
            expirationDate = accountAdjustDates.getNewDueDate();
        } else {
            if (accountAdjustDates.getAddDateIndicator() == CHAR_R) {
                expirationDate = maxRefenceDueDate;
            } else {
                expirationDate = getMaxPlanExpirationDate(bilAccount.getAccountId(), accountAdjustDates);
            }
        }

        if (maxRefenceDueDate.compareTo(expirationDate) > 0
                && maxInvoiceDate.compareTo(accountAdjustDates.getProcessDate()) > 0 && dateType == CHAR_S
                && (invoiceCode == 'A' || invoiceCode == BLANK_CHAR)
                && (bilDates == null || bilDates.getBilExcInd() != CHAR_X)) {
            if (bilDates != null) {
                return new Object[] { bilDates.getBilDatesId().getBilReferenceDt(), bilDates.getBilAdjDueDt(),
                        bilDates.getBilInvDt(), bilDates.getBilSysDueDt(), dateType };
            } else {
                return new Object[] { null, null, null, null, dateType };
            }

        }
        if (isRestAll && isBbdtRowExist) {
            int offsetDays = adjustDatesReviewDetails.getDueDateDisplacementNumber()
                    + adjustDatesReviewDetails.getBilSupportPlan().getBspPndCncNbr();

            if (maxInvoiceAdjustDueDate.compareTo(DateRoutine.dateTimeAsYYYYMMDD(0)) > SHORT_ZERO) {
                Object[] invoicePaid = deterIfInvoicePaid(bilAccount.getAccountId(), maxInvoiceAdjustDueDate,
                        adjustDatesReviewDetails.getBilSupportPlan().getBspMinInvAmt());
                isInvoicePaid = (boolean) invoicePaid[SHORT_ZERO];
                invoicePaidDate = (ZonedDateTime) invoicePaid[SHORT_ONE];
                if (!isInvoicePaid) {
                    if (adjustDatesReviewDetails.isEftCollectionMethod()) {
                        maxInvoiceAdjustDueDate = DateRoutine.adjustDateWithOffset(maxInvoiceAdjustDueDate, false,
                                adjustDatesReviewDetails.getDueDateDisplacementNumber(), '+', null);
                        maxSystemDueDate = DateRoutine.adjustDateWithOffset(maxSystemDueDate, false,
                                adjustDatesReviewDetails.getDueDateDisplacementNumber(), '+', null);
                    } else {
                        maxInvoiceAdjustDueDate = DateRoutine.adjustDateWithOffset(maxInvoiceAdjustDueDate, false,
                                offsetDays, '+', null);
                        maxSystemDueDate = DateRoutine.adjustDateWithOffset(maxSystemDueDate, false, offsetDays, '+',
                                null);
                    }

                }
            } else {
                if (adjustDatesReviewDetails.isEftCollectionMethod()) {
                    maxSystemDueDate = DateRoutine.adjustDateWithOffset(maxSystemDueDate, false,
                            adjustDatesReviewDetails.getDueDateDisplacementNumber(), '+', null);
                } else {
                    maxSystemDueDate = DateRoutine.adjustDateWithOffset(maxSystemDueDate, false, offsetDays, '+', null);
                }
            }

        }

        while (!(maxRefenceDueDate.compareTo(expirationDate) > 0
                && maxInvoiceDate.compareTo(accountAdjustDates.getProcessDate()) > 0

                && (bilDates == null || bilDates.getBilExcInd() != CHAR_X) && dateType == CHAR_S
                && (invoiceCode == 'A' || invoiceCode == BLANK_CHAR))) {
            BilDates bilDatess = propDates(accountAdjustDates, adjustDatesReviewDetails, maxRefenceDueDate,
                    maxSystemDueDate, maxInvoiceAdjustDueDate, isXHOLRule, bilRulesUct,
                    adjustDatesReviewDetails.getBilActRules().getBruIncWknInd(), isInvoicePaid, invoicePaidDate);
            maxRefenceDueDate = bilDatess.getBilDatesId().getBilReferenceDt();
            maxSystemDueDate = bilDatess.getBilSysDueDt();
            maxInvoiceDate = bilDatess.getBilInvDt();
            invoiceCode = bilDatess.getBilInvoiceCd();
            dateType = bilDatess.getBdtDateType();
        }

        if (adjustDatesReviewDetails.isLateEntered() && !adjustDatesReviewDetails.isAccountBilled()
                && adjustDatesReviewDetails.isErcvRule() && adjustDatesReviewDetails.isErcvIssDtInvoice()) {
            if (bilAccount.getStartDueDate().compareTo(accountAdjustDates.getProcessDate()) <= 0) {

                bilDates = newFpddBillCheck(accountAdjustDates, accountAdjustDates.getProcessDate(),
                        accountAdjustDates.getProcessDate(), accountAdjustDates.getProcessDate(),
                        accountAdjustDates.getProcessDate(), adjustDatesReviewDetails.getBilSupportPlan(),
                        adjustDatesReviewDetails.isErcvIssDtInvoice(), adjustDatesReviewDetails.isAccountBilled());
            } else {
                if (bilAccount.getStartDueDate().compareTo(accountAdjustDates.getProcessDate()) > 0 && bilAccount
                        .getStartDueDate().compareTo(adjustDatesReviewDetails.getEarliestEftDueDt()) <= 0) {

                    bilDates = newFpddBillCheck(accountAdjustDates, bilAccount.getStartDueDate(),
                            bilAccount.getStartDueDate(), bilAccount.getStartDueDate(),
                            accountAdjustDates.getProcessDate(), adjustDatesReviewDetails.getBilSupportPlan(),
                            adjustDatesReviewDetails.isErcvIssDtInvoice(), adjustDatesReviewDetails.isAccountBilled());
                }
            }

            dateType = bilDates.getBdtDateType();

        }

        if (isRestAll && adjustDatesReviewDetails.isErcvIssDtInvoice() && !adjustDatesReviewDetails.isAccountBilled()) {
            if (bilAccount.getStartDueDate().compareTo(accountAdjustDates.getProcessDate()) <= 0) {

                bilDates = newFpddBillCheck(accountAdjustDates, accountAdjustDates.getProcessDate(),
                        accountAdjustDates.getProcessDate(), accountAdjustDates.getProcessDate(),
                        accountAdjustDates.getProcessDate(), adjustDatesReviewDetails.getBilSupportPlan(),
                        adjustDatesReviewDetails.isErcvIssDtInvoice(), adjustDatesReviewDetails.isAccountBilled());
            } else {
                if (bilAccount.getStartDueDate().compareTo(accountAdjustDates.getProcessDate()) > 0 && bilAccount
                        .getStartDueDate().compareTo(adjustDatesReviewDetails.getEarliestEftDueDt()) <= 0) {

                    bilDates = newFpddBillCheck(accountAdjustDates, bilAccount.getStartDueDate(),
                            bilAccount.getStartDueDate(), bilAccount.getStartDueDate(),
                            accountAdjustDates.getProcessDate(), adjustDatesReviewDetails.getBilSupportPlan(),
                            adjustDatesReviewDetails.isErcvIssDtInvoice(), adjustDatesReviewDetails.isAccountBilled());

                }
            }

            dateType = bilDates.getBdtDateType();

        }

        if (bilDates != null) {
            return new Object[] { bilDates.getBilDatesId().getBilReferenceDt(), bilDates.getBilAdjDueDt(),
                    bilDates.getBilInvDt(), bilDates.getBilSysDueDt(), dateType };
        }

        return new Object[] { null, null, null, null, dateType };
    }

    private ZonedDateTime getMaxPlanExpirationDate(String accountId, AccountAdjustDates accountAdjustDates) {

        ZonedDateTime maxInstallmentScheduleDueDate = DATE_LOW;
        ZonedDateTime maxPlanExpirationDate = null;
        Integer count = bilPolicyTermRepository.findCountByRedistributeCode(accountId,
                AdjustDueDatesConstants.getPolicyStatusCodesRedistribute(),
                PolicyRedistributeCode.REDISTRIBUTE_PROCESSED.getValue());
        if (count != null && count != SHORT_ZERO) {
            maxInstallmentScheduleDueDate = getMaxInstallmentScheduleDueDate(accountId);
        }

        maxPlanExpirationDate = bilPolicyTermRepository.getMaxPlanExpirationDate(accountId);
        if (maxPlanExpirationDate == null && (accountAdjustDates.getAddChargeIndicator() == CHAR_P
                || accountAdjustDates.getAddChargeIndicator() == CHAR_C
                || accountAdjustDates.getAddChargeIndicator() == CHAR_L
                || accountAdjustDates.getSupportPlanIndicator() == CHAR_Y
                || accountAdjustDates.getStartDateIndicator() == CHAR_Y
                || accountAdjustDates.getSupportPlanIndicator() == CHAR_E
                || accountAdjustDates.getReferenceDateIndicator() == CHAR_Y
                || accountAdjustDates.getDateResetIndicator() == CHAR_T
                || accountAdjustDates.getDateResetIndicator() == CHAR_O)) {

            return DATE_LOW;
        }

        if (maxPlanExpirationDate == null) {
            maxPlanExpirationDate = DATE_LOW;
        }

        if (maxInstallmentScheduleDueDate.compareTo(maxPlanExpirationDate) > SHORT_ZERO) {
            maxPlanExpirationDate = maxInstallmentScheduleDueDate;
        }

        return maxPlanExpirationDate;
    }

    private ZonedDateTime getMaxInstallmentScheduleDueDate(String accountId) {
        ZonedDateTime maxInstallmentScheduleDueDate = bilIstScheduleRepository
                .findMaxInstallmentScheduleDueDate(accountId, FUTURE_INVOICE_CODES);
        if (maxInstallmentScheduleDueDate == null) {

            return DATE_LOW;
        }

        return maxInstallmentScheduleDueDate;

    }

    private ZonedDateTime[] transferTest(String accountId) {

        List<BilIstSchedule> bilIstScheduleList = bilIstScheduleRepository.getMaxReferenceDateByInvoiceCd(accountId,
                AdjustDueDatesConstants.getInvoiceCodes());
        if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
            BilIstSchedule bilIstSchedule = bilIstScheduleList.get(SHORT_ZERO);
            return new ZonedDateTime[] { bilIstSchedule.getBillReferenceDt(), bilIstSchedule.getBillSysDueDt(),
                    bilIstSchedule.getBillInvDt() };
        }

        return new ZonedDateTime[0];
    }

    private void setSeviceAnnualPremium(VariableSeviceCharge variableSeviceCharge, String accountId) {

        List<BilAmounts> bilAmountsList = bilAmountsRepository.findBilAmountByPolicyReasonAmountCode(accountId,
                variableSeviceCharge.getPolicyId(), "QTE", variableSeviceCharge.getApMaxEffDt(),
                variableSeviceCharge.getMaxPolicyExpDt(), CHAR_H);
        if (bilAmountsList == null || bilAmountsList.isEmpty()) {
            return;
        }
        BilAmounts bilAmounts = bilAmountsList.get(SHORT_ZERO);

        int numberOfPolicyDuration = (int) ChronoUnit.DAYS.between(
                variableSeviceCharge.getMaxPolEffDt().with(LocalTime.MIN),
                variableSeviceCharge.getMaxPolicyExpDt().with(LocalTime.MIN));
        variableSeviceCharge.setPolicyDuration(numberOfPolicyDuration);

        if (bilAmounts.getBamAmtEffDt() == variableSeviceCharge.getMaxPolEffDt()
                && bilAmounts.getBamAmtExpDt() == variableSeviceCharge.getMaxPolicyExpDt()) {
            variableSeviceCharge.setCoverageDuration(numberOfPolicyDuration);
        } else {
            int numberOfCoverageDuration = (int) ChronoUnit.DAYS.between(
                    bilAmounts.getBamAmtEffDt().with(LocalTime.MIN), bilAmounts.getBamAmtExpDt().with(LocalTime.MIN));
            variableSeviceCharge.setCoverageDuration(numberOfCoverageDuration);
        }

        if (variableSeviceCharge.getCoverageDuration() > variableSeviceCharge.getPolicyDuration()) {
            variableSeviceCharge.setCoverageDuration(variableSeviceCharge.getPolicyDuration());
        }

        if (variableSeviceCharge.getCoverageDuration() != 0 || variableSeviceCharge.getPolicyDuration() != 0) {
            Double coverageFct = variableSeviceCharge.getCoverageDuration()
                    / (double) variableSeviceCharge.getPolicyDuration();
            variableSeviceCharge.setCovFct(coverageFct);
        }
        if (variableSeviceCharge.getCovFct() != 0) {
            Double dateFct = 1. / variableSeviceCharge.getCovFct();
            variableSeviceCharge.setDateTimeFct(dateFct);
        }

        Double coveragePremium = variableSeviceCharge.getDateTimeFct() * bilAmounts.getBamAmt();
        variableSeviceCharge.setCoveragePrm(coveragePremium);
        if (variableSeviceCharge.getPolicyDuration() >= 363 && variableSeviceCharge.getPolicyDuration() <= 367) {
            variableSeviceCharge
                    .setAnnualPremium(variableSeviceCharge.getCoveragePrm() + variableSeviceCharge.getAnnualPremium());
        }
        variableSeviceCharge.setTermPrm(variableSeviceCharge.getCoveragePrm());
        Double invoiceFct = (double) variableSeviceCharge.getPolicyDuration() / 365;
        variableSeviceCharge.setInvFct(invoiceFct);
        if (variableSeviceCharge.getInvFct() != 0) {
            variableSeviceCharge.setDateTimeFct(1 / variableSeviceCharge.getInvFct());
        }
        Double annualPrm = variableSeviceCharge.getDateTimeFct() * variableSeviceCharge.getTermPrm();
        variableSeviceCharge.setAnnualPremium(annualPrm);
        variableSeviceCharge.setAnnualPremium(annualPrm + variableSeviceCharge.getAnnualPremium());

    }

    private void getMaxTerm(String accountId, ChargeVariables chargeVariables) {

        List<Object[]> bilIstScheduleList = bilIstScheduleRepository.getPolicyIdAndPolicyEffectiveDate(accountId,
                chargeVariables.getAcctDueDate(), Arrays.asList('W', 'T', 'D', 'G', 'V'));
        if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
            chargeVariables.setPolicyId((String) bilIstScheduleList.get(0)[0]);
            chargeVariables.setEffectiveDate((ZonedDateTime) bilIstScheduleList.get(0)[1]);

        } else {
            bilIstScheduleList = bilIstScheduleRepository.getPolicyIdAndPolicyEffectiveDate(accountId,
                    Arrays.asList(InvoiceTypeCode.SCHEDULE_AMOUNT_INVOICED.getValue(),
                            InvoiceTypeCode.PREVIOUSSCHEDULEAMOUNT_BELOWMINIMUM_NOWINVOICE.getValue()),
                    Arrays.asList('T', 'V'));
            if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
                chargeVariables.setPolicyId((String) bilIstScheduleList.get(0)[0]);
                chargeVariables.setEffectiveDate((ZonedDateTime) bilIstScheduleList.get(0)[1]);

            } else {
                chargeVariables.setPolicyId(BLANK_STRING);
                chargeVariables.setEffectiveDate(null);
            }
        }
        if (chargeVariables.getEffectiveDate() == null) {
            if (chargeVariables.isSpcrRule()) {
                chargeVariables.setChargeEffectiveTypeCd(CHAR_N);
            } else {
                chargeVariables.setChargeEffectiveTypeCd(CHAR_A);
            }

            return;
        }
        if (!chargeVariables.isSpcrRule()) {
            chargeVariables.setChargeEffectiveTypeCd(CHAR_A);
            return;
        }
        if (chargeVariables.isPersActive()) {
            return;
        }
        if (chargeVariables.getEffectiveDate().compareTo(chargeVariables.getPrevTermEffDate()) == SHORT_ZERO) {
            return;
        }
        List<Character> bilAmountsEffectiveIndList = bilAmountsRepository.getBilAmountsEffectiveInd(
                chargeVariables.getAccountId(), chargeVariables.getPolicyId(), chargeVariables.getEffectiveDate(),
                Arrays.asList('M', 'N', 'G', 'R', 'U', 'W', '9'));
        if (bilAmountsEffectiveIndList != null && !bilAmountsEffectiveIndList.isEmpty()) {

            chargeVariables.setPolEffTypCd(bilAmountsEffectiveIndList.get(0));

            if (chargeVariables.getPolEffTypCd() == 'M' || chargeVariables.getPolEffTypCd() == CHAR_N) {
                chargeVariables.setChargeEffectiveTypeCd(CHAR_N);
            } else {
                if (chargeVariables.getPolEffTypCd() == 'G' || chargeVariables.getPolEffTypCd() == CHAR_R
                        || chargeVariables.getPolEffTypCd() == 'U' || chargeVariables.getPolEffTypCd() == 'W') {
                    chargeVariables.setChargeEffectiveTypeCd(CHAR_R);
                } else {
                    deterReissue(chargeVariables);
                }
            }

        } else {
            chargeVariables.setChargeEffectiveTypeCd(CHAR_A);
        }
    }

    private void deterReissue(ChargeVariables chargeVariables) {

        BilPolicy bilPolicy = bilPolicyRepository
                .findById(new BilPolicyId(chargeVariables.getAccountId(), chargeVariables.getPolicyId())).orElse(null);
        if (bilPolicy == null) {
            throw new DataNotFoundException("bil.policy.row.not.found", new Object[] { chargeVariables.getPolicyId() });
        }

        chargeVariables.setPolicyNumber(bilPolicy.getPolNbr());
        chargeVariables.setPolSymbolCd(bilPolicy.getPolSymbolCd());

        List<Character> bilAmountsEffectiveIndList = bilAmountsRepository.getBilAmountsEffectiveIndByPolicyNbr(
                chargeVariables.getAccountId(), Arrays.asList('G', 'R', 'U', 'W', 'N', 'M'),
                chargeVariables.getPolicyNumber(), chargeVariables.getPolSymbolCd());
        if (bilAmountsEffectiveIndList != null && !bilAmountsEffectiveIndList.isEmpty()) {

            chargeVariables.setPolEffTypCd(bilAmountsEffectiveIndList.get(0));

            if (chargeVariables.getPolEffTypCd() == 'M' || chargeVariables.getPolEffTypCd() == CHAR_N) {
                chargeVariables.setChargeEffectiveTypeCd(CHAR_N);
            } else {
                if (chargeVariables.getPolEffTypCd() == 'G' || chargeVariables.getPolEffTypCd() == CHAR_R
                        || chargeVariables.getPolEffTypCd() == 'U' || chargeVariables.getPolEffTypCd() == 'W') {
                    chargeVariables.setChargeEffectiveTypeCd(CHAR_R);
                }
            }

        }

    }

}