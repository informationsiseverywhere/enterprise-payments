package billing.handler.impl;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_M;
import static core.utils.CommonConstants.CHAR_R;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActInquiry;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRulesUct;
import billing.data.entity.HalBoMduXrf;
import billing.data.entity.id.BilActInquiryId;
import billing.data.entity.id.BilActSummaryId;
import billing.data.entity.id.BilPolActivityId;
import billing.data.entity.id.BilPolicyId;
import billing.data.entity.id.BilPolicyTermId;
import billing.data.repository.BilAccountRepository;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilPolActivityRepository;
import billing.data.repository.BilPolicyRepository;
import billing.data.repository.BilPolicyTermRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.HalBoMduXrfRepository;
import billing.handler.impl.RequestPolicyPcnOrRescindPiServiceImpl.RequestPolicyType;
import billing.handler.model.CollectionVendorTapeProcessing;
import billing.handler.model.PcnRescindCollection;
import billing.handler.model.PolicyPcnAndRescInterface;
import billing.handler.model.ReverseWriteOffPayment;
import billing.handler.service.RequestPolicyPcnOrRescindPiService;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants;
import billing.utils.BillingConstants.BilPolicyStatusCode;
import billing.utils.BillingConstants.BillCollections;
import billing.utils.BillingConstants.CollectionLayout;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.schedule.service.ScheduleService;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.FunctionCode;

@Service
public class BillingApiService {

    private static final Logger logger = LogManager.getLogger(BillingApiService.class);

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilPolicyTermRepository bilPolicyTermRepository;

    @Autowired
    private BilPolActivityRepository bilPolActivityRepository;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private DateService dateService;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private ReverseWriteOffToPaymentServiceImpl reverseWriteOffToPaymentServiceImpl;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private HalBoMduXrfRepository halBoMduXrfRepository;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private RequestPolicyPcnOrRescindPiService requestPolicyPcnOrRescindPiService;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private BilPolicyRepository bilPolicyRepository;
    
    @Value("${language}")
    private String language;

    private static final String SYSTEM_USER_ID = "SYSTEM";
    private static final String REQUEST_POLICY_PCN_OR_RESCINDPI = "BCMOPI10";
    private static final String ERROR_DESCRIPTION = "Unable to update the data in  BIL_POLICY_TRM with Account Id:";
    private static final String POLICY_ID = "policy Id: ";
    private static final String POLICY_EFFECTIVE_DATE = ", policyEffective Date: ";
    private static final String UNABLE_TO_UPDATE_BIL_POL_TRM = "unable.to.update.bil.pol.trm";

    public void getCollectionVendor(CollectionVendorTapeProcessing collectionVendorTapeProcessing) {

        BusCdTranslation busCdTranslation;
        switch (collectionVendorTapeProcessing.getApiType()) {

        case "COF":
            busCdTranslation = findBusCdTranslationData(collectionVendorTapeProcessing.getBilTypeCd());

            if (busCdTranslation != null
                    && busCdTranslation.getBusCdTranslationId().getBusParentCd().trim().equals("BTY")
                    && collectionVendorTapeProcessing.getApiType().equals("COF")) {
                processCallOffCollect(collectionVendorTapeProcessing);
            }
            break;

        case "ACF":
            busCdTranslation = findBusCdTranslationData(collectionVendorTapeProcessing.getBilTypeCd());
            if (busCdTranslation != null
                    && busCdTranslation.getBusCdTranslationId().getBusParentCd().trim().equals("BTY")) {
                processAccountCallOffCollect(collectionVendorTapeProcessing);
            }
            break;
        default:
            break;
        }
    }

    public void processPcnOrRescindCollection(String requestProgramId, char queueWrtIndicator,
            PcnRescindCollection pcnRescindCollection) {
        String interfaceObjName;
        String interfacePgmName;
        String[] editMessages = null;

        switch (pcnRescindCollection.getApiType()) {
        case "ACR":
            interfaceObjName = "BIL-INT-BCMOAPI-PCN-REQ-";
            if (pcnRescindCollection.getPolPcnReason().trim().isEmpty()) {
                interfaceObjName = "BIL-INT-BCMOAPI-RESCIND-REQ-";
            }
            interfaceObjName = interfaceObjName + pcnRescindCollection.getIssueSysId().trim();
            interfacePgmName = getModuleName(interfaceObjName);
            editMessages = processPolicyPcnAndRescind(interfacePgmName, requestProgramId, queueWrtIndicator,
                    pcnRescindCollection);

            break;

        case "RDR":
            interfaceObjName = "BIL-INT-BCMOAPI-AUTO-RNT-REQ-";
            interfaceObjName = interfaceObjName + pcnRescindCollection.getIssueSysId().trim();
            interfacePgmName = getModuleName(interfaceObjName);
            if (interfacePgmName == null || interfacePgmName.trim().isEmpty()) {
                throw new DataNotFoundException("billing.auto.reinstatement.not.found",
                        new Object[] { interfaceObjName, pcnRescindCollection.getIssueSysId() });
            }

            editMessages = processAutoReissueTransaction(requestProgramId, queueWrtIndicator, pcnRescindCollection);

            break;

        case "REI":
            String context = "REQUEST AUTO REI";
            char activeCode = RequestPolicyType.AUTO_REI_ACTION.getValue();
            interfaceObjName = "BIL-INT-BCMOAPI-AUTO-REI-REQ-";
            interfaceObjName = interfaceObjName + pcnRescindCollection.getIssueSysId().trim();
            interfacePgmName = getModuleName(interfaceObjName);
            if (interfacePgmName == null || interfacePgmName.trim().isEmpty()) {
                throw new DataNotFoundException("billing.auto.reinstatement.not.found",
                        new Object[] { interfaceObjName, pcnRescindCollection.getIssueSysId() });
            }

            PolicyPcnAndRescInterface policyPcnAndRescInterface = mappingRequestParamForPcnOrRescind(requestProgramId,
                    queueWrtIndicator, pcnRescindCollection, activeCode, context);
            editMessages = requestPolicyPcnOrRescindPiService.processPcnOrRescind(policyPcnAndRescInterface);

            break;
        default:
            break;
        }

        if (editMessages != null && editMessages.length > 1) {
            throw new InvalidDataException(editMessages[0] + editMessages[1]);
        }
    }

    private String[] processAutoReissueTransaction(String requestProgramId, char queueWrtIndicator,
            PcnRescindCollection pcnRescindCollection) {
        String context = "REQUEST AUTO RNT";
        char activeCode = RequestPolicyType.AUTO_RNT_ACTION.getValue();

        PolicyPcnAndRescInterface policyPcnAndRescInterface = mappingRequestParamForPcnOrRescind(requestProgramId,
                queueWrtIndicator, pcnRescindCollection, activeCode, context);
        policyPcnAndRescInterface.setRntReaAmdCode("RNT");
        policyPcnAndRescInterface.setEffectiveDate(pcnRescindCollection.getOriginEffectiveDate());
        policyPcnAndRescInterface.setRntNewEffDate(pcnRescindCollection.getEffectiveDate());
        policyPcnAndRescInterface.setNewExpDate(pcnRescindCollection.getNewExpDate());

        return requestPolicyPcnOrRescindPiService.processPcnOrRescind(policyPcnAndRescInterface);
    }

    private void processCallOffCollect(CollectionVendorTapeProcessing collectionVendorTapeProcessing) {

        logger.debug("Enter processCallOffCollect menthod.");

        ZonedDateTime activityDate1 = null;
        String accountId = collectionVendorTapeProcessing.getBilAccountId();
        String policyId = collectionVendorTapeProcessing.getPolicyId();
        ZonedDateTime activityDate = collectionVendorTapeProcessing.getActivityDt();
        String policyNumber = collectionVendorTapeProcessing.getPolNbr();
        String symbolCode = collectionVendorTapeProcessing.getPolSymbolCd();

        if (logger.isTraceEnabled()) {
            logger.trace("processCallOffCollect menthod parameters collectionVendorTapeProcessing:{}",
                    ReflectionToStringBuilder.toString(collectionVendorTapeProcessing));
        }

        List<BilPolicyTerm> bilPolicyTermList = readBilPolicyTermData(accountId, policyId);
        if (bilPolicyTermList == null || bilPolicyTermList.isEmpty()) {
            return;
        }

        char collRulesCode = BLANK_CHAR;
        BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct("COLL",
                collectionVendorTapeProcessing.getBilTypeCd(), collectionVendorTapeProcessing.getBilClassCd(),
                BLANK_STRING);
        if (bilRulesUct != null) {
            collRulesCode = bilRulesUct.getBrtRuleCd();
        }

        if (collRulesCode == CHAR_Y || collectionVendorTapeProcessing.getClassChgSw() == CHAR_Y) {
            char collectionStatus = collectionPolicyStatus(accountId, policyId);
            deleteBilPolActivityRow(accountId, policyId);

            BilPolicyTerm bilPolicyTerm;

            if (collectionVendorTapeProcessing.getCollectionType() == CollectionLayout.COLLECTION_FULL.getValue()) {
                writeActSummary(collectionVendorTapeProcessing, policyNumber, symbolCode,
                        collectionVendorTapeProcessing.getApiType(), BLANK_STRING, DECIMAL_ZERO,
                        collectionVendorTapeProcessing.getUserId(), activityDate1);
                bilPolicyTerm = resumeCollect(collectionVendorTapeProcessing, bilPolicyTermList);

                if (bilPolicyTerm.getBptWroPrcInd() == CHAR_M) {
                    updateMwoTrigger(accountId, policyId, activityDate, collectionVendorTapeProcessing.getPolEffDate());
                }

                if (collRulesCode == CHAR_Y) {
                    calculateWriteOffAmounts(accountId, policyId, policyNumber, symbolCode,
                            bilPolicyTerm.getBptActColCd(), collectionVendorTapeProcessing.getCollectionQuote());
                }
            } else {
                bilPolicyTerm = fetchBilPolicyTermData(accountId, policyId,
                        collectionVendorTapeProcessing.getPolEffDate());
            }

            FinancialApiActivity financialApiActivity = new FinancialApiActivity();
            double balanceAmount = bilPolicyTerm.getBptPolColAmt() * -1;
            financialApiActivity.setBilReasonCode("SPC");
            financialApiActivity.setActivityAmount(balanceAmount);
            financialApiActivity.setActivityNetAmount(balanceAmount);
            financialApiActivity.setTransactionActionCode("BAL");
            if (collectionStatus == BillCollections.COLLECTION.getValue()) {
                financialApiActivity.setTransactionObjectCode("COL");
            } else {
                financialApiActivity.setTransactionObjectCode("PCL");
            }

            financialApiUpdate(financialApiActivity, bilPolicyTerm, collectionVendorTapeProcessing);

            deleteCwoData(accountId, policyId);
        }

        logger.debug("Exit processCallOffCollect menthod.");

    }

    private List<BilPolicyTerm> readBilPolicyTermData(String accountId, String policyId) {

        return bilPolicyTermRepository.findPolicyData1(accountId, policyId, getPolicyStatusList());

    }

    private BusCdTranslation findBusCdTranslationData(String billTypeCd) {

        if (logger.isTraceEnabled()) {
            logger.trace("findBusCdTranslationData menthod parameters billTypeCd:{}", billTypeCd);
        }
        List<BusCdTranslation> busCdTranslations = busCdTranslationRepository.findAllByCodeAndTypeList(billTypeCd,
                getBusCdList(), language);
        if (busCdTranslations != null && !busCdTranslations.isEmpty()) {
            return busCdTranslations.get(0);
        }
        return null;
    }

    private List<String> getBusCdList() {
        List<String> busTypeCdList = new ArrayList<>();
        busTypeCdList.add("BTG");
        busTypeCdList.add("BTP");
        busTypeCdList.add("BTY");
        return busTypeCdList;
    }

    private List<Character> getPolicyStatusList() {
        List<Character> policyStatusList = new ArrayList<>();
        policyStatusList.add(BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue());
        policyStatusList.add(BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue());
        return policyStatusList;
    }

    private char collectionPolicyStatus(String accountId, String policyId) {

        boolean rowExist = bilPolActivityRepository
                .existsById(new BilPolActivityId(accountId, policyId, DateRoutine.defaultDateTime(), "BCL"));
        if (!rowExist) {
            return BillCollections.COLLECTION.getValue();
        }
        return BillCollections.PRE_COLLECTION.getValue();
    }

    private void writeActSummary(CollectionVendorTapeProcessing collectionVendorTapeProcessing, String policyNumber,
            String policySymbol, String descriptionCode, String reasonType, double bilAcyAmount, String userId,
            ZonedDateTime actiivtyDate1) {
        BilActSummary bilActSummary = new BilActSummary();

        bilActSummary.setBillActSummaryId(new BilActSummaryId(collectionVendorTapeProcessing.getBilAccountId(),
                collectionVendorTapeProcessing.getActivityDt(),
                fetchMaxSequenceNumber(collectionVendorTapeProcessing.getBilAccountId(),
                        collectionVendorTapeProcessing.getActivityDt())));
        bilActSummary.setPolSymbolCd(policySymbol);
        bilActSummary.setPolNbr(policyNumber);
        bilActSummary.setBilAcyDesCd(descriptionCode);
        bilActSummary.setBilDesReaTyp(reasonType);
        if (actiivtyDate1 == null) {
            bilActSummary.setBilAcyDes1Dt(DateRoutine.defaultDateTime());
        } else {
            bilActSummary.setBilAcyDes1Dt(actiivtyDate1);
        }
        bilActSummary.setBilAcyDes2Dt(DateRoutine.defaultDateTime());
        bilActSummary.setBilAcyAmt(bilAcyAmount);
        bilActSummary.setUserId(userId);
        bilActSummary.setBilAcyTs(dateService.currentDateTime());
        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

    private short fetchMaxSequenceNumber(String accountId, ZonedDateTime activityDate) {

        if (logger.isTraceEnabled()) {
            logger.trace("fetchMaxSequenceNumber menthod parameters accountId:{}, activityDate:{}", accountId,
                    activityDate);
        }
        Short seqInteger = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountId, activityDate);
        if (seqInteger == null) {
            seqInteger = 0;
        } else {
            seqInteger = (short) (seqInteger + 1);
        }

        return seqInteger.shortValue();
    }

    private BilPolicyTerm resumeCollect(CollectionVendorTapeProcessing collectionVendorTapeProcessing,
            List<BilPolicyTerm> bilPolicyTermList) {

        if (logger.isTraceEnabled()) {
            logger.trace("resumeCollect menthod parameters collectionVendorTapeProcessing:{}",
                    ReflectionToStringBuilder.toString(collectionVendorTapeProcessing));
        }

        String accountId = collectionVendorTapeProcessing.getBilAccountId();
        String policyId = collectionVendorTapeProcessing.getPolicyId();
        ZonedDateTime policyEffDate = collectionVendorTapeProcessing.getPolEffDate();
        ZonedDateTime activityDate = collectionVendorTapeProcessing.getActivityDt();

        BilPolicyTerm bilPolicyTerm = fetchBilPolicyTermData(accountId, policyId, policyEffDate);

        resumePolicyCollect(accountId, policyId, policyEffDate, activityDate, bilPolicyTermList);
        updateBillingData(collectionVendorTapeProcessing, bilPolicyTerm.getBillSusFuReaCd(),
                bilPolicyTerm.getBillPolStatusCd(), bilPolicyTerm.getBptActColCd());

        return bilPolicyTerm;
    }

    private void deleteBilPolActivityRow(String accountId, String policyId) {
        if (logger.isTraceEnabled()) {
            logger.trace("deleteBilPolActivityRow menthod parameters accountId:{}, policyId:{}", accountId, policyId);
        }
        bilPolActivityRepository.deleteBilPolActivityByAccountId(accountId, policyId, "CL0", "CL9",
                getBilAcyTypeCdList());
    }

    private List<String> getBilAcyTypeCdList() {
        List<String> bilAcyTypeCdList = new ArrayList<>();
        bilAcyTypeCdList.add("BCL");
        bilAcyTypeCdList.add("CCL");
        bilAcyTypeCdList.add("SCL");
        bilAcyTypeCdList.add("CLE");
        bilAcyTypeCdList.add("CLF");
        bilAcyTypeCdList.add("CLC");
        return bilAcyTypeCdList;
    }

    private void resumePolicyCollect(String accountId, String policyId, ZonedDateTime policyEffDate,
            ZonedDateTime activityDate, List<BilPolicyTerm> bilPolicyTermList) {
        if (logger.isTraceEnabled()) {
            logger.trace(
                    "resumePolicyCollect menthod parameters accountId:{}, policyId:{}, activityDate:{}, policyEffDate:{}",
                    accountId, policyId, activityDate, policyEffDate);
        }
        for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
            if (bilPolicyTerm.getBptPolUwStaCd() == BilPolicyStatusCode.CANCELLED.getValue()
                    || bilPolicyTerm.getBptPolUwStaCd() == BilPolicyStatusCode.FLAT_CANCELLATION.getValue()) {
                Integer count = bilPolicyTermRepository.updatePolicyStatus(accountId, policyId, policyEffDate,
                        activityDate, DECIMAL_ZERO, SEPARATOR_BLANK,
                        BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue(),
                        bilPolicyTerm.getBptPolUwStaCd(), getPolicyStatusList());
                if (count == null) {
                    String errorDescription = ERROR_DESCRIPTION + accountId.trim() + ", " + POLICY_ID + policyId.trim()
                            + POLICY_EFFECTIVE_DATE + DateRoutine.dateTimeAsMMDDYYYYAsString(policyEffDate);
                    logger.debug(errorDescription);
                    throw new DataNotFoundException(UNABLE_TO_UPDATE_BIL_POL_TRM, new Object[] { accountId.trim(),
                            policyId.trim(), DateRoutine.dateTimeAsMMDDYYYYAsString(policyEffDate) });
                }
                continue;
            }
            updatePolicyStatusByAmount(accountId, policyId, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                    activityDate);
        }
    }

    private void financialApiUpdate(FinancialApiActivity financialApiActivity, BilPolicyTerm bilPolicyTerm,
            CollectionVendorTapeProcessing collectionVendorTapeProcessing) {

        String currencyCode = BLANK_STRING;
        BilAccount bilAccount = bilAccountRepository.findById(collectionVendorTapeProcessing.getBilAccountId())
                .orElse(null);
        if (bilAccount != null) {
            currencyCode = bilAccount.getCurrencyCode();
        }
        ZonedDateTime se3Date = dateService.currentDate();
        financialApiActivity.setFunctionCode(FunctionCode.APPLY.getValue());
        financialApiActivity.setApplicationProductIdentifier(ApplicationProduct.BCMS.toString());
        financialApiActivity.setMasterCompanyNbr(bilPolicyTerm.getMasterCompanyNbr());
        financialApiActivity.setCompanyLocationNbr(BillingConstants.COMPANY_LOCATION_NUMBER);
        financialApiActivity.setStatusCode(BLANK_CHAR);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(se3Date));
        financialApiActivity.setOperatorId(BLANK_STRING);
        financialApiActivity.setAppProgramId("BCMOAPI");
        financialApiActivity.setCountyCode(bilPolicyTerm.getBillCountryCd());
        financialApiActivity.setStateCode(bilPolicyTerm.getBillStatePvnCd());
        financialApiActivity.setCountryCode(bilPolicyTerm.getBillCountryCd());
        financialApiActivity.setLineOfBusCode(bilPolicyTerm.getLobCd());
        financialApiActivity.setPolicyId(collectionVendorTapeProcessing.getPolicyId());
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setBusinessCaseId(BLANK_STRING);
        financialApiActivity.setMarketSectorCode(BLANK_STRING);
        financialApiActivity.setClientId(BLANK_STRING);
        financialApiActivity.setCustomerInfo1(BLANK_STRING);
        financialApiActivity.setCustomerInfo2(BLANK_STRING);
        financialApiActivity.setCustomerInfo3(BLANK_STRING);
        financialApiActivity.setCustomerInfo4(BLANK_STRING);
        financialApiActivity.setCustomerInfo5(BLANK_STRING);
        financialApiActivity.setCurrencyCode(currencyCode);
        financialApiActivity.setOriginalEffectiveDate(
                DateRoutine.dateTimeAsYYYYMMDDString(collectionVendorTapeProcessing.getPolEffDate()));
        financialApiActivity.setBilBankCode(BLANK_STRING);
        financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(se3Date));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(se3Date));
        financialApiActivity.setBilAccountId(collectionVendorTapeProcessing.getBilAccountNbr());
        financialApiActivity.setBilReceiptTypeCode(BLANK_STRING);
        financialApiActivity.setBilTypeCode(collectionVendorTapeProcessing.getBilTypeCd());
        financialApiActivity.setBilClassCode(collectionVendorTapeProcessing.getBilClassCd());
        financialApiActivity.setPayableItemCode(BLANK_STRING);
        financialApiActivity.setBilDatabaseKey(BLANK_STRING);
        financialApiActivity.setAgentTtyId(BLANK_STRING);
        financialApiActivity.setBilSourceCode(BLANK_STRING);
        financialApiActivity.setSummaryEffectiveDate(DateRoutine.defaultDateTime());
        financialApiActivity.setDepositeBankCode(BLANK_STRING);
        financialApiActivity.setDisbursementId(BLANK_STRING);
        financialApiActivity.setFolderId(collectionVendorTapeProcessing.getBilAccountId());

        insertBcmBusActivity(financialApiActivity);

    }

    private void insertBcmBusActivity(FinancialApiActivity financialApiActivity) {

        String[] fwsReturnMessage = null;

        fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, dateService.currentDateTime());

        if (fwsReturnMessage[0] != null) {
            logger.error(fwsReturnMessage[1]);
            throw new InvalidDataException(fwsReturnMessage[1]);
        }
    }

    private BilPolicyTerm fetchBilPolicyTermData(String accountId, String policyId, ZonedDateTime policyEffDate) {
        BilPolicyTerm bilPolicyTerm = bilPolicyTermRepository
                .findById(new BilPolicyTermId(accountId, policyId, policyEffDate)).orElse(null);

        if (bilPolicyTerm == null) {
            String errorDescription = "Data in BIL_POLICY_TRM with Account Id: " + accountId.trim() + ", policy Id: "
                    + policyId.trim() + POLICY_EFFECTIVE_DATE + DateRoutine.dateTimeAsMMDDYYYYAsString(policyEffDate)
                    + " could not be found.";
            logger.error(errorDescription);
            throw new DataNotFoundException("data.bil.policy.trm.not.found", new Object[] { accountId.trim(),
                    policyId.trim(), DateRoutine.dateTimeAsMMDDYYYYAsString(policyEffDate) });
        }
        return bilPolicyTerm;

    }

    private void deleteCwoData(String accountId, String policyId) {

        if (bilPolActivityRepository
                .existsById(new BilPolActivityId(accountId, policyId, DateRoutine.defaultDateTime(), "CWO"))) {
            bilPolActivityRepository
                    .deleteById(new BilPolActivityId(accountId, policyId, DateRoutine.defaultDateTime(), "CWO"));
        }

    }

    private void updatePolicyStatusByAmount(String accountId, String policyId, ZonedDateTime policyEffectiveDate,
            ZonedDateTime activityDate) {

        char policyStatusCode = BilPolicyStatusCode.OPEN.getValue();
        Double policyAmount = bilIstScheduleRepository.calculateBalanceAmount(accountId, policyId, policyEffectiveDate);

        if (policyAmount == null) {
            String errorDescription = "Data in  BIL_IST_SCHEDULE with Account Id:" + accountId.trim() + ", " + POLICY_ID
                    + policyId.trim() + POLICY_EFFECTIVE_DATE
                    + DateRoutine.dateTimeAsMMDDYYYYAsString(policyEffectiveDate) + " could not be found.";
            logger.error(errorDescription);
            throw new DataNotFoundException("data.bil.ist.schedule.not.found", new Object[] { accountId.trim(),
                    policyId.trim(), DateRoutine.dateTimeAsMMDDYYYYAsString(policyEffectiveDate) });
        }

        if (policyAmount.equals(DECIMAL_ZERO)) {
            policyStatusCode = BilPolicyStatusCode.CLOSED.getValue();
        }
        Integer count = bilPolicyTermRepository.updatePolicyStatus1(accountId, policyId, policyEffectiveDate,
                activityDate, DECIMAL_ZERO, SEPARATOR_BLANK,
                BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue(), policyStatusCode);

        if (count == null) {
            String errorDescription = ERROR_DESCRIPTION + accountId.trim() + ", " + POLICY_ID + policyId.trim()
                    + POLICY_EFFECTIVE_DATE + DateRoutine.dateTimeAsMMDDYYYYAsString(policyEffectiveDate);
            logger.error(errorDescription);
            throw new DataNotFoundException(UNABLE_TO_UPDATE_BIL_POL_TRM, new Object[] { accountId.trim(),
                    policyId.trim(), DateRoutine.dateTimeAsMMDDYYYYAsString(policyEffectiveDate) });
        }
    }

    private void updateBillingData(CollectionVendorTapeProcessing collectionVendorTapeProcessing, String reasonCode,
            char statusCode, char collectionCode) {
        if (logger.isTraceEnabled()) {
            logger.trace(
                    "updateBillingData menthod parameters collectionVendorTapeProcessing:{}, reasonCode:{}, statusCode:{}, collectionCode:{}",
                    ReflectionToStringBuilder.toString(collectionVendorTapeProcessing), reasonCode, statusCode,
                    collectionCode);
        }
        String userId;
        String reasonTypeCode;
        ZonedDateTime activityDate1 = null;
        String descriptionCode = reasonCode.subSequence(0, 2) + String.valueOf(CHAR_R);

        if (statusCode == BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue()) {
            if (collectionVendorTapeProcessing.getUserId().trim().equals(BLANK_STRING)
                    || collectionVendorTapeProcessing.getUserId().trim().equals(SYSTEM_USER_ID)) {
                userId = SYSTEM_USER_ID;
                reasonTypeCode = "RPA";
            } else {
                userId = collectionVendorTapeProcessing.getUserId();
                reasonTypeCode = "RPM";
            }
        } else {
            if (collectionVendorTapeProcessing.getUserId().trim().equals(BLANK_STRING)
                    || collectionVendorTapeProcessing.getUserId().trim().equals(SYSTEM_USER_ID)) {
                userId = SYSTEM_USER_ID;
                reasonTypeCode = "RCA";
            } else {
                userId = collectionVendorTapeProcessing.getUserId();
                reasonTypeCode = "RCM";
            }
        }
        writeActSummary(collectionVendorTapeProcessing, collectionVendorTapeProcessing.getPolNbr(),
                collectionVendorTapeProcessing.getPolSymbolCd(), descriptionCode, reasonTypeCode, DECIMAL_ZERO, userId,
                activityDate1);

        if (collectionCode == CHAR_Y) {
            writeSummaryData(collectionVendorTapeProcessing, statusCode);
            updateBillAccountStatus(collectionVendorTapeProcessing.getBilAccountId());
        }
    }

    private void writeSummaryData(CollectionVendorTapeProcessing collectionVendorTapeProcessing, char statusCode) {

        if (logger.isTraceEnabled()) {
            logger.trace("writeSummaryData menthod parameters collectionVendorTapeProcessing:{}, statusCode:{}",
                    ReflectionToStringBuilder.toString(collectionVendorTapeProcessing), statusCode);
        }

        String userId;
        String reasonTypeCode;
        String descriptionCode;
        ZonedDateTime activityDate1 = null;

        if (statusCode == BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue()) {
            descriptionCode = "APR";
            if (collectionVendorTapeProcessing.getUserId().trim().equals(BLANK_STRING)
                    || collectionVendorTapeProcessing.getUserId().trim().equals(SYSTEM_USER_ID)) {
                userId = SYSTEM_USER_ID;
                reasonTypeCode = "RPA";
            } else {
                userId = collectionVendorTapeProcessing.getUserId();
                reasonTypeCode = "RPM";
            }
        } else {

            descriptionCode = "ACR";
            if (collectionVendorTapeProcessing.getUserId().trim().equals(BLANK_STRING)
                    || collectionVendorTapeProcessing.getUserId().trim().equals(SYSTEM_USER_ID)) {
                userId = SYSTEM_USER_ID;
                reasonTypeCode = "RCA";
            } else {
                userId = collectionVendorTapeProcessing.getUserId();
                reasonTypeCode = "RCM";
            }
        }

        writeActSummary(collectionVendorTapeProcessing, BLANK_STRING, BLANK_STRING, descriptionCode, reasonTypeCode,
                DECIMAL_ZERO, userId, activityDate1);
    }

    private void updateBillAccountStatus(String accountId) {

        if (logger.isTraceEnabled()) {
            logger.trace("updateBillAccountStatus menthod parameters accountId:{}", accountId);
        }

        char policyStatusCode = BilPolicyStatusCode.CANCELLED_SUBJECT_TO_AUDIT.getValue();

        Double balanceAmount = bilIstScheduleRepository.getAccountBalance(accountId, getPolicyStatusList1());

        if (balanceAmount == null) {
            balanceAmount = DECIMAL_ZERO;
        }
        if (balanceAmount.equals(DECIMAL_ZERO)) {
            policyStatusCode = BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue();
        }

        Integer count = bilAccountRepository.updateByAccountId(accountId, policyStatusCode, SEPARATOR_BLANK);
        if (count == null) {
            String errorDescription = "Unable to update the data in  BIL_ACCOUNT with Account Id:" + accountId.trim();
            logger.error(errorDescription);
            throw new DataNotFoundException("unable.to.update.bil.account", new Object[] { accountId.trim() });
        }

    }

    private List<Character> getPolicyStatusList1() {
        List<Character> policyStatusList = new ArrayList<>();
        policyStatusList.add(BilPolicyStatusCode.SUSPEND_BILLING.getValue());
        policyStatusList.add(BilPolicyStatusCode.SUSPENDBILLING_FOLLOWUP.getValue());
        policyStatusList.add(BilPolicyStatusCode.SUSPEND_COLLECTIONS.getValue());
        policyStatusList.add(BilPolicyStatusCode.SUSPEND_PRECOLLECTIONS.getValue());
        policyStatusList.add(BilPolicyStatusCode.PENDING_CANCELLATION.getValue());
        policyStatusList.add(BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue());
        policyStatusList.add(BilPolicyStatusCode.PENDINGCANCELLATION_NORESPONSE.getValue());
        policyStatusList.add(BilPolicyStatusCode.BILLACCOUNT_PENDINGCANCELLATION_MAX.getValue());
        return policyStatusList;
    }

    private void calculateWriteOffAmounts(String accountId, String policyId, String policyNumber, String symbolCode,
            char collectionCode, char quoteSwitch) {
        if (logger.isTraceEnabled()) {
            logger.trace(
                    "calculateWriteOffAmounts menthod parameters accountId:{}, policyId:{}, policyNumber:{}, symbolCode:{}, collectionCode:{}, quoteSwitch:{}",
                    accountId, policyId, policyNumber, symbolCode, collectionCode, quoteSwitch);
        }
        double totalPremiumWcAmount = getBalanceAmount(accountId, Arrays.asList("WC"), "WPA", policyNumber, symbolCode);
        double totalPremiumWcReverseAmount = getBalanceAmount(accountId, Arrays.asList("WCR"), "WRR", policyNumber,
                symbolCode);
        double totalPremiumWeAmount = getBalanceAmount(accountId, Arrays.asList("WE"), "WPA", policyNumber, symbolCode);
        double totalPremiumWeReverseAmount = getBalanceAmount(accountId, Arrays.asList("WER"), "WRR", policyNumber,
                symbolCode);

        double totalPenaltyWpAmount = DECIMAL_ZERO;
        double totalPenaltyWpReverseAmount = DECIMAL_ZERO;
        double totalLateWlAmount = DECIMAL_ZERO;
        double totalLateWlReverseAmount = DECIMAL_ZERO;
        double totalSurChargeWaAmount = DECIMAL_ZERO;
        double totalSurChargeWaReverseAmount = DECIMAL_ZERO;
        double totalDownAmountWdAmount = DECIMAL_ZERO;
        double totalDownAmountWdReverseAmount = DECIMAL_ZERO;

        BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
        if (bilAccount == null) {
            throw new DataNotFoundException("no.bil.account.exists", new Object[] { accountId });
        }
        if (collectionCode == CHAR_Y || bilAccount.getBillTypeCd().equals("SP")) {
            totalPenaltyWpAmount = getBalaceAmounts(accountId, "WP", "WPA");
            totalPenaltyWpReverseAmount = getBalaceAmounts(accountId, "WPR", "WRR");
            totalLateWlAmount = getBalaceAmounts(accountId, "WL", "WPA");
            totalLateWlReverseAmount = getBalaceAmounts(accountId, "WLR", "WRR");
            totalSurChargeWaAmount = getBalaceAmounts(accountId, "WA", "WPA");
            totalSurChargeWaReverseAmount = getBalaceAmounts(accountId, "WAR", "WRR");
            totalDownAmountWdAmount = getBalaceAmounts(accountId, "WD", "WPA");
            totalDownAmountWdReverseAmount = getBalaceAmounts(accountId, "WDR", "WRR");
        }

        double totalRw1ReverseAmount = getBalaceAmounts1(accountId, "RW1", policyNumber, symbolCode);

        if (totalRw1ReverseAmount < DECIMAL_ZERO) {
            totalRw1ReverseAmount = totalRw1ReverseAmount * -1;
        }

        if (totalPremiumWcAmount > DECIMAL_ZERO && totalPremiumWeAmount == DECIMAL_ZERO) {
            totalPremiumWcAmount = totalPremiumWeAmount + totalPremiumWcAmount;
            totalPremiumWcReverseAmount = totalPremiumWcReverseAmount + totalPremiumWeReverseAmount
                    + totalRw1ReverseAmount;
            totalPremiumWeAmount = DECIMAL_ZERO;
            totalPremiumWeReverseAmount = DECIMAL_ZERO;
        } else {
            totalPremiumWeAmount = totalPremiumWcAmount + totalPremiumWeAmount;
            totalPremiumWeReverseAmount = totalPremiumWeReverseAmount + totalPremiumWcReverseAmount
                    + totalRw1ReverseAmount;
            totalPremiumWcAmount = DECIMAL_ZERO;
            totalPremiumWcReverseAmount = DECIMAL_ZERO;

        }

        double totalPreWcAmount = totalPremiumWcAmount - totalPremiumWcReverseAmount;
        double totalPreWeAmount = totalPremiumWeAmount - totalPremiumWeReverseAmount;
        double totalPenaltyAmount = totalPenaltyWpAmount - totalPenaltyWpReverseAmount;
        double totalLateAmount = totalLateWlAmount - totalLateWlReverseAmount;
        double totalSurChargeAmount = totalSurChargeWaAmount - totalSurChargeWaReverseAmount;
        double totalDownPayment = totalDownAmountWdAmount - totalDownAmountWdReverseAmount;
        double totalCashInSuspense = totalDownPayment + totalSurChargeAmount + totalLateAmount + totalPenaltyAmount
                + totalPreWeAmount + totalPreWcAmount;

        ReverseWriteOffPayment reverseWriteOffPayment = setReverseAutomaticWriteoffData(policyId, totalPreWcAmount,
                totalPreWeAmount, totalPenaltyAmount, totalLateAmount, totalSurChargeAmount, totalDownPayment,
                totalCashInSuspense, quoteSwitch);

        reverseWriteOffToPaymentServiceImpl.reverseWriteOffToPayment(reverseWriteOffPayment, bilAccount);

    }

    private ReverseWriteOffPayment setReverseAutomaticWriteoffData(String policyId, double totalPreWcAmount,
            double totalPreWeAmount, double totalPenaltyAmount, double totalLateAmount, double totalSurChargeAmount,
            double totalDownPayment, double totalCashInSuspense, char quoteSwitch) {

        ReverseWriteOffPayment reverseWriteOffPayment = new ReverseWriteOffPayment();
        reverseWriteOffPayment.setCashInSuspenseAmount(totalCashInSuspense);
        reverseWriteOffPayment.setPremiumWriteOffcAmount(totalPreWcAmount);
        reverseWriteOffPayment.setPremiumWriteOffeAmount(totalPreWeAmount);
        reverseWriteOffPayment.setPenaltyWriteOffAmount(totalPenaltyAmount);
        reverseWriteOffPayment.setLateWriteOffAmount(totalLateAmount);
        reverseWriteOffPayment.setServiceChargeWriteOffAmount(totalSurChargeAmount);
        reverseWriteOffPayment.setDownPaymentWriteOffAmount(totalDownPayment);
        reverseWriteOffPayment.setQuoteIndicator(String.valueOf(quoteSwitch));
        reverseWriteOffPayment.setApplicationDate(dateService.currentDate());
        reverseWriteOffPayment.setPolicyId(policyId);

        return reverseWriteOffPayment;
    }

    private double getBalanceAmount(String accountId, List<String> descriptionCode, String reasonType,
            String policyNumber, String symbolCode) {
        Double amount = bilActSummaryRepository.getPremiumActivityAmount(accountId, descriptionCode, reasonType,
                policyNumber, symbolCode);
        if (amount == null) {
            amount = DECIMAL_ZERO;
        }

        return amount;
    }

    private double getBalaceAmounts(String accountId, String descriptionCode, String reasonType) {
        Double amount = bilActSummaryRepository.getActivityAmount(accountId, descriptionCode, reasonType);
        if (amount == null) {
            amount = DECIMAL_ZERO;
        }

        return amount;
    }

    private double getBalaceAmounts1(String accountId, String descriptionCode, String policyNumber, String symbolCode) {

        Double amount = bilActSummaryRepository.getPremiumActivityAmount(accountId, descriptionCode, policyNumber,
                symbolCode);
        if (amount == null) {
            amount = DECIMAL_ZERO;
        }

        return amount;
    }

    private void updateMwoTrigger(String accountId, String policyId, ZonedDateTime activityDate,
            ZonedDateTime policyEffectiveDate) {

        if (logger.isTraceEnabled()) {
            logger.trace(
                    "updateMwoTrigger menthod parameters accountId:{}, policyId:{}, activityDate:{}, policyEffectiveDate:{}",
                    accountId, policyId, activityDate, policyEffectiveDate);
        }

        Integer count = bilPolicyTermRepository
                .updateWroProcessIndicator(new BilPolicyTermId(accountId, policyId, policyEffectiveDate), BLANK_CHAR);
        if (count == null) {
            String errorDescription = ERROR_DESCRIPTION + accountId.trim() + ", " + POLICY_ID + policyId.trim()
                    + POLICY_EFFECTIVE_DATE + DateRoutine.dateTimeAsMMDDYYYYAsString(policyEffectiveDate);
            logger.error(errorDescription);
            throw new DataNotFoundException(UNABLE_TO_UPDATE_BIL_POL_TRM, new Object[] { accountId.trim(),
                    policyId.trim(), DateRoutine.dateTimeAsMMDDYYYYAsString(policyEffectiveDate) });
        }

        bilActInquiryRepository.updateMwoBilActInquiry(new BilActInquiryId(accountId, "MWO"), activityDate,
                DateRoutine.defaultDateTime());
    }

    private String getModuleName(String interfaceObjName) {
        String interfacePgmName = BLANK_STRING;
        HalBoMduXrf halBoMduXrf = halBoMduXrfRepository.findById(interfaceObjName).orElse(null);
        if (halBoMduXrf != null) {
            interfacePgmName = halBoMduXrf.getHbmxBobjMduNm();
        }
        return interfacePgmName;
    }

    private String[] processPolicyPcnAndRescind(String interfacePgmName, String programId, char queueWrtIndicator,
            PcnRescindCollection pcnRescindCollection) {
        String[] editMessages = null;
        char activeCode;
        String context;

        if (interfacePgmName.trim().isEmpty() || interfacePgmName.equalsIgnoreCase("BCMOXCR")) {
            schedulePcnRescindRequest(pcnRescindCollection);
        } else if (interfacePgmName.trim().equalsIgnoreCase(REQUEST_POLICY_PCN_OR_RESCINDPI)) {
            activeCode = RequestPolicyType.PCN_ACTION.getValue();
            if (pcnRescindCollection.getPolPcnReason().trim().isEmpty()) {
                activeCode = RequestPolicyType.RESCIND_ACTION.getValue();
            }
            context = "REQUEST PCN/RESCIND";

            PolicyPcnAndRescInterface policyPcnAndRescInterface = mappingRequestParamForPcnOrRescind(programId,
                    queueWrtIndicator, pcnRescindCollection, activeCode, context);

            editMessages = requestPolicyPcnOrRescindPiService.processPcnOrRescind(policyPcnAndRescInterface);
        }
        return editMessages;
    }

    private PolicyPcnAndRescInterface mappingRequestParamForPcnOrRescind(String programId, char queueWrtIndicator,
            PcnRescindCollection pcnRescindCollection, char activeCode, String context) {
        PolicyPcnAndRescInterface policyPcnAndRescInterface = new PolicyPcnAndRescInterface();
        policyPcnAndRescInterface.setApiType(pcnRescindCollection.getApiType());
        policyPcnAndRescInterface.setActionCode(activeCode);
        policyPcnAndRescInterface.setActivityDate(pcnRescindCollection.getActivityDate());
        policyPcnAndRescInterface.setRequestProgramName(programId);
        policyPcnAndRescInterface.setContext(context);
        policyPcnAndRescInterface.setAccountId(pcnRescindCollection.getAccountId());
        policyPcnAndRescInterface.setPolicyId(pcnRescindCollection.getPolicyId());
        policyPcnAndRescInterface.setIssueSysId(pcnRescindCollection.getIssueSysId());
        policyPcnAndRescInterface.setPolSymbolCode(pcnRescindCollection.getPolSymbolCode());
        policyPcnAndRescInterface.setPolNumber(pcnRescindCollection.getPolNumber());
        policyPcnAndRescInterface.setEffectiveDate(pcnRescindCollection.getEffectiveDate());
        policyPcnAndRescInterface.setPlanExpDate(pcnRescindCollection.getPlanExpDate());
        policyPcnAndRescInterface.setStatePvnCode(pcnRescindCollection.getStatePvnCode());
        policyPcnAndRescInterface.setMasterCompanyNumber(pcnRescindCollection.getMasterCompanyNumber());
        policyPcnAndRescInterface.setLineOfBusinessCode(pcnRescindCollection.getLineOfBusinessCode());
        policyPcnAndRescInterface.setLdnLegalDays(pcnRescindCollection.getLdnLegalDays());
        policyPcnAndRescInterface.setLdnMailDays(pcnRescindCollection.getLdnMailDays());
        policyPcnAndRescInterface.setLdnMaxDays(pcnRescindCollection.getLdnMaxDays());
        policyPcnAndRescInterface.setCancelType(pcnRescindCollection.getCancelType());
        policyPcnAndRescInterface.setPolPcnReason(pcnRescindCollection.getPolPcnReason());
        policyPcnAndRescInterface.setStandCncDate(pcnRescindCollection.getStandCncDate());
        policyPcnAndRescInterface.setEquityDate(pcnRescindCollection.getEquityDate());
        policyPcnAndRescInterface.setEffTypeCode(pcnRescindCollection.getEffTypeCode());
        policyPcnAndRescInterface.setAutoCollectionMethodChange(pcnRescindCollection.getAutoCollectionMethodChange());
        policyPcnAndRescInterface.setNsfActivityIndicator(pcnRescindCollection.getNsfActivityIndicator());
        policyPcnAndRescInterface.setRestartPcnIndicator(pcnRescindCollection.getRestartPcnIndicator());
        policyPcnAndRescInterface.setRestartPcnDate(pcnRescindCollection.getRestartPcnDate());
        policyPcnAndRescInterface.setBilAcyDate(pcnRescindCollection.getBilAcyDate());
        policyPcnAndRescInterface.setBilSequenceNumber(pcnRescindCollection.getBilSequenceNumber());
        policyPcnAndRescInterface.setBilAcyDesCode(pcnRescindCollection.getBilAcyDesCode());
        policyPcnAndRescInterface.setBilDesReaType(pcnRescindCollection.getBilDesReaType());
        policyPcnAndRescInterface.setReinstNtcIndicator(pcnRescindCollection.getReinstNtcIndicator());
        policyPcnAndRescInterface.setPcnFlatCncIndicator(pcnRescindCollection.getPcnFlatCncIndicator());
        policyPcnAndRescInterface.setReinstCode(pcnRescindCollection.getReinstCode());
        policyPcnAndRescInterface.setQueueWrtIndicator(queueWrtIndicator);
        if (pcnRescindCollection.getUserId() == null || pcnRescindCollection.getUserId().trim().isEmpty()) {
            policyPcnAndRescInterface.setUserId(pcnRescindCollection.getUserSequenceId());
        }
        return policyPcnAndRescInterface;
    }

    private void schedulePcnRescindRequest(PcnRescindCollection pcnRescindCollection) {
        String userData = createPcnRescindParam(pcnRescindCollection);
        scheduleService.scheduleDeferActivity("BCMOXCR", userData, pcnRescindCollection.getPolicyId());
    }

    private String createPcnRescindParam(PcnRescindCollection pcnRescindCollection) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(StringUtils.leftPad(pcnRescindCollection.getApiType(), 3));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getPolicyId(), 16, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getEffectiveDate()), 10, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getPlanExpDate()), 10, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getPolPcnReason(), 3, BLANK_STRING));
        stringBuilder
                .append(StringUtils.rightPad(String.valueOf(pcnRescindCollection.getEffTypeCode()), 1, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getUserId(), 8, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getActivityDate()), 10, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getEquityDate()), 10, BLANK_STRING));
        stringBuilder
                .append(StringUtils.rightPad(String.valueOf(pcnRescindCollection.getCancelType()), 1, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getIssueSysId(), 2, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 1, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getProgramName(), 8, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getParaName(), 30, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad("0", 8, "0"));
        stringBuilder.append(StringUtils.rightPad(BLANK_STRING, 100, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(String.valueOf(pcnRescindCollection.getAutoCollectionMethodChange()),
                1, BLANK_STRING));
        stringBuilder.append(
                StringUtils.rightPad(String.valueOf(pcnRescindCollection.getNsfActivityIndicator()), 1, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getAccountId(), 8, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getPolSymbolCode(), 3, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getPolNumber(), 25, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getStatePvnCode(), 3, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad("0", 6, "0"));
        stringBuilder.append(
                StringUtils.rightPad(String.valueOf(pcnRescindCollection.getRestartPcnIndicator()), 1, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getRestartPcnDate()), 10, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getStandCncDate()), 10, BLANK_STRING));
        stringBuilder
                .append(StringUtils.rightPad(String.valueOf(pcnRescindCollection.getLdnLegalDays()), 4, BLANK_STRING));
        stringBuilder
                .append(StringUtils.rightPad(String.valueOf(pcnRescindCollection.getLdnMailDays()), 4, BLANK_STRING));
        stringBuilder
                .append(StringUtils.rightPad(String.valueOf(pcnRescindCollection.getLdnMaxDays()), 4, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getMasterCompanyNumber(), 2, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getLineOfBusinessCode(), 3, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getAccountId(), 8, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(
                DateRoutine.dateTimeAsYYYYMMDDString(pcnRescindCollection.getBilAcyDate()), 10, BLANK_STRING));
        stringBuilder.append(
                StringUtils.rightPad(String.valueOf(pcnRescindCollection.getBilSequenceNumber()), 6, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getBilAcyDesCode(), 3, BLANK_STRING));
        stringBuilder.append(StringUtils.rightPad(pcnRescindCollection.getBilDesReaType(), 3, BLANK_STRING));
        stringBuilder.append(
                StringUtils.rightPad(String.valueOf(pcnRescindCollection.getReinstNtcIndicator()), 1, BLANK_STRING));
        stringBuilder.append(
                StringUtils.rightPad(String.valueOf(pcnRescindCollection.getPcnFlatCncIndicator()), 1, BLANK_STRING));
        stringBuilder
                .append(StringUtils.rightPad(String.valueOf(pcnRescindCollection.getReinstCode()), 1, BLANK_STRING));

        return stringBuilder.toString();
    }

    private void processAccountCallOffCollect(CollectionVendorTapeProcessing collectionVendorTapeProcessing) {

        logger.debug("Enter processAccountCallOffCollect method.");
        if (logger.isTraceEnabled()) {
            logger.trace("processAccountCallOffCollect method parameters collectionVendorTapeProcessing:{}",
                    ReflectionToStringBuilder.toString(collectionVendorTapeProcessing));
        }
        String accountId = collectionVendorTapeProcessing.getBilAccountId();
        ZonedDateTime activityDate = collectionVendorTapeProcessing.getActivityDt();

        BilRulesUct bilRulesUct = fetchBilRulesUctData("COLL", activityDate,
                collectionVendorTapeProcessing.getBilTypeCd(), collectionVendorTapeProcessing.getBilClassCd());

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_A
                || collectionVendorTapeProcessing.getClassChgSw() == CHAR_Y) {
            checkBclExists(accountId);
            deleteBilActInquiryRow(accountId);
            List<BilPolicyTerm> bilPolicyTermList = readAccountBilPolicyTermData(accountId);
            if (bilPolicyTermList != null && !bilPolicyTermList.isEmpty()) {
                for (BilPolicyTerm bilPolicyTerm : bilPolicyTermList) {
                    resumeAccountCollect(bilPolicyTerm, collectionVendorTapeProcessing);
                }
            }
        }
        updateBillAccountStatus(collectionVendorTapeProcessing.getBilAccountId());
        logger.debug("Exit processAccountCallOffCollect method");
    }

    private BilRulesUct fetchBilRulesUctData(String ruleId, ZonedDateTime date, String billTypeCd, String classCd) {

        if (logger.isTraceEnabled()) {
            logger.trace("fetchBilRulesUctData menthod parameters ruleId:{}, date:{}, billTypeCd:{}, classCd:{}",
                    ruleId, date, billTypeCd, classCd);
        }

        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(ruleId, billTypeCd, classCd, BLANK_STRING, date,
                date);
        if (bilRulesUct == null) {
            bilRulesUct = bilRulesUctRepository.getBilRulesRow(ruleId, BLANK_STRING, classCd, BLANK_STRING, date, date);
            if (bilRulesUct == null) {
                bilRulesUct = bilRulesUctRepository.getBilRulesRow(ruleId, BLANK_STRING, BLANK_STRING, BLANK_STRING,
                        date, date);
            }
        }
        return bilRulesUct;
    }

    private char checkBclExists(String accountId) {
        BilActInquiry bilActInquiry = bilActInquiryRepository.findById(new BilActInquiryId(accountId, "BCL"))
                .orElse(null);
        if (bilActInquiry == null) {
            return BLANK_CHAR;
        }
        return CHAR_Y;
    }

    private void deleteBilActInquiryRow(String accountId) {
        if (logger.isTraceEnabled()) {
            logger.trace("deleteBilActInquiryRow menthod parameters accountId:{}", accountId);
        }
        bilActInquiryRepository.deleteByTechnicalKeyIdentifier(accountId, getAccountCollectionTypeCdList());
    }

    private List<String> getAccountCollectionTypeCdList() {
        List<String> bilAcyTypeCdList = new ArrayList<>();
        bilAcyTypeCdList.add("BCL");
        bilAcyTypeCdList.add("CCL");
        bilAcyTypeCdList.add("SCL");
        bilAcyTypeCdList.add("CLE");
        bilAcyTypeCdList.add("CLF");
        bilAcyTypeCdList.add("CLC");
        bilAcyTypeCdList.add("CL0");
        bilAcyTypeCdList.add("CL1");
        bilAcyTypeCdList.add("CL2");
        bilAcyTypeCdList.add("CL3");
        bilAcyTypeCdList.add("CL4");
        bilAcyTypeCdList.add("CL5");
        bilAcyTypeCdList.add("CL6");
        bilAcyTypeCdList.add("CL7");
        bilAcyTypeCdList.add("CL8");
        bilAcyTypeCdList.add("CL9");
        return bilAcyTypeCdList;
    }

    private BilPolicyTerm resumeAccountCollect(BilPolicyTerm bilPolicyTerm1,
            CollectionVendorTapeProcessing collectionVendorTapeProcessing) {

        if (logger.isTraceEnabled()) {
            logger.trace("resumeCollect menthod parameters collectionVendorTapeProcessing:{}",
                    ReflectionToStringBuilder.toString(collectionVendorTapeProcessing));
        }

        BilPolicyTerm bilPolicyTerm = fetchBilPolicyTermData(collectionVendorTapeProcessing.getBilAccountId(),
                bilPolicyTerm1.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm1.getBillPolicyTermId().getPolEffectiveDt());

        BilPolicy bilPolicy = findBilPolicyData(bilPolicyTerm, collectionVendorTapeProcessing.getBilAccountId());
        if (bilPolicy != null) {
            collectionVendorTapeProcessing.setPolNbr(bilPolicy.getPolNbr());
            collectionVendorTapeProcessing.setPolSymbolCd(bilPolicy.getPolSymbolCd());
        }
        collectionVendorTapeProcessing.setPolEffDate(bilPolicyTerm1.getBillPolicyTermId().getPolEffectiveDt());
        collectionVendorTapeProcessing.setPolicyId(bilPolicyTerm1.getBillPolicyTermId().getPolicyId());

        resumeAccountPolicyCollect(collectionVendorTapeProcessing.getBilAccountId(),
                bilPolicyTerm1.getBillPolicyTermId().getPolicyId(),
                bilPolicyTerm1.getBillPolicyTermId().getPolEffectiveDt(),
                collectionVendorTapeProcessing.getActivityDt(), bilPolicyTerm);
        if (collectionVendorTapeProcessing.getApiType().equals("ACF")
                || collectionVendorTapeProcessing.getApiType().equals("ACS")
                        && collectionVendorTapeProcessing.getStopManStartSw() != CHAR_Y) {
            FinancialApiActivity financialApiActivity = new FinancialApiActivity();
            double balanceAmount = bilPolicyTerm.getBptPolColAmt() * -1;
            financialApiActivity.setBilReasonCode("SPC");
            financialApiActivity.setActivityAmount(balanceAmount);
            financialApiActivity.setActivityNetAmount(balanceAmount);
            financialApiActivity.setTransactionActionCode("BAL");
            if (bilPolicyTerm.getBillPolStatusCd() == BillCollections.COLLECTION.getValue()) {
                financialApiActivity.setTransactionObjectCode("COL");
            } else {
                financialApiActivity.setTransactionObjectCode("PCL");
            }

            financialApiUpdate(financialApiActivity, bilPolicyTerm, collectionVendorTapeProcessing);
        }
        updateBillingData(collectionVendorTapeProcessing, bilPolicyTerm.getBillSusFuReaCd(),
                bilPolicyTerm.getBillPolStatusCd(), bilPolicyTerm.getBptActColCd());

        return bilPolicyTerm;
    }

    private BilPolicy findBilPolicyData(BilPolicyTerm bilPolicyTerm, String accountId) {

        return bilPolicyRepository
                .findById(new BilPolicyId(bilPolicyTerm.getBillPolicyTermId().getPolicyId(), accountId)).orElse(null);

    }

    private void resumeAccountPolicyCollect(String accountId, String policyId, ZonedDateTime policyEffDate,
            ZonedDateTime activityDate, BilPolicyTerm bilPolicyTerm) {
        if (logger.isTraceEnabled()) {
            logger.trace(
                    "resumePolicyCollect menthod parameters accountId:{}, policyId:{}, activityDate:{}, policyEffDate:{}",
                    accountId, policyId, activityDate, policyEffDate);
        }
        if (bilPolicyTerm.getBptPolUwStaCd() == BilPolicyStatusCode.CANCELLED.getValue()
                || bilPolicyTerm.getBptPolUwStaCd() == BilPolicyStatusCode.FLAT_CANCELLATION.getValue()) {
            Integer count = bilPolicyTermRepository.updatePolicyStatus(accountId, policyId, policyEffDate, activityDate,
                    DECIMAL_ZERO, SEPARATOR_BLANK,
                    BilPolicyStatusCode.PENDING_CANCELLATION_NONSUFFICIENTFUNDS.getValue(),
                    bilPolicyTerm.getBptPolUwStaCd(), getPolicyStatusList());
            if (count == null) {
                String errorDescription = ERROR_DESCRIPTION + accountId.trim() + ", " + POLICY_ID + policyId.trim()
                        + POLICY_EFFECTIVE_DATE + policyEffDate.toString();
                logger.debug(errorDescription);
                throw new DataNotFoundException("unable.to.update.bil.policy.trm",
                        new Object[] { accountId.trim(), policyId.trim(), policyEffDate.toString() });
            }
            return;
        }
        updatePolicyStatusByAmount(accountId, policyId, bilPolicyTerm.getBillPolicyTermId().getPolEffectiveDt(),
                activityDate);
    }

    private List<BilPolicyTerm> readAccountBilPolicyTermData(String accountId) {
        return bilPolicyTermRepository.getPolicyTermsByStatusCode(accountId, getPolicyStatusList());
    }

}
