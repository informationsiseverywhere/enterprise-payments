package billing.handler.impl;

import static billing.utils.BillingConstants.SYSTEM_USER_ID;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_F;
import static core.utils.CommonConstants.CHAR_Q;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_X;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActRules;
import billing.data.entity.BilActSummary;
import billing.data.entity.BilCrgAmounts;
import billing.data.entity.BilDates;
import billing.data.entity.BilEftBank;
import billing.data.entity.BilEftPlnCrg;
import billing.data.entity.BilIstSchedule;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSptPlnCrg;
import billing.data.entity.BilSupportPlan;
import billing.data.entity.id.BilActSummaryId;
import billing.data.repository.BilActInquiryRepository;
import billing.data.repository.BilActSummaryRepository;
import billing.data.repository.BilCashDspRepository;
import billing.data.repository.BilCreScheduleRepository;
import billing.data.repository.BilCrgAmountsRepository;
import billing.data.repository.BilDatesRepository;
import billing.data.repository.BilEftBankRepository;
import billing.data.repository.BilEftPlnCrgRepository;
import billing.data.repository.BilIstScheduleRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilSptPlnCrgRepository;
import billing.handler.model.AccountAdjustDates;
import billing.handler.model.AccountAdjustSupportData;
import billing.handler.model.AdjustDatesReviewDetails;
import billing.handler.model.ChargeVariables;
import billing.handler.model.DownPaymentFee;
import billing.handler.model.FirstPossibleDueDate;
import billing.handler.model.VariableSeviceCharge;
import billing.handler.model.VariableSeviceChargeOverride;
import billing.handler.service.AccountAdjustDatesService;
import billing.handler.service.FirstPossibleDueDateService;
import billing.handler.service.ResumeDateResetService;
import billing.utils.AdjustDueDatesConstants.ScheduleCode;
import billing.utils.BillingConstants.ActivityType;
import billing.utils.BillingConstants.ChargeType;
import billing.utils.BillingConstants.InvoiceTypeCode;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.utils.DateRoutine;

@Service
public class AccountAdjustDatesResumeResetServiceImpl implements ResumeDateResetService {

    private static final Logger logger = LoggerFactory.getLogger(AccountAdjustDatesResumeResetServiceImpl.class);

    @Autowired
    private BilIstScheduleRepository bilIstScheduleRepository;

    @Autowired
    private BilDatesRepository bilDatesRepository;

    @Autowired
    private BilCreScheduleRepository bilCreScheduleRepository;

    @Autowired
    private BilCashDspRepository bilCashDspRepository;

    @Autowired
    private BilCrgAmountsRepository bilCrgAmountsRepository;

    @Autowired
    private BilActInquiryRepository bilActInquiryRepository;

    @Autowired
    private AccountAdjustDatesService accountAdjustDatesService;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private FirstPossibleDueDateService firstPossibleDueDateService;

    @Autowired
    private DateService dateService;

    @Autowired
    private BilActSummaryRepository bilActSummaryRepository;

    @Autowired
    private BilEftBankRepository bilEftBankRepository;

    @Autowired
    private BilSptPlnCrgRepository bilSptPlnCrgRepository;

    @Autowired
    private BilEftPlnCrgRepository bilEftPlnCrgRepository;

    private static final String RULE_XHOL = "XHOL";

    @Override
    @Transactional
    public void processResumeDateReset(AccountAdjustDates accountAdjustDates) {

        AdjustDatesReviewDetails adjustDatesReviewDetails = accountAdjustDatesService
                .processAdjustDates(accountAdjustDates);

        BilActRules bilActRules = adjustDatesReviewDetails.getBilActRules();
        BilAccount bilAccount = adjustDatesReviewDetails.getBilAccount();
        BilSupportPlan bilSupportPlan = adjustDatesReviewDetails.getBilSupportPlan();
        char fpddIndicator = adjustDatesReviewDetails.getFpddIndicator();
        boolean isManualReinvoice = adjustDatesReviewDetails.isManualReinvoice();
        boolean disableFpdd = false;
        // need to add all sevice classes starts
        VariableSeviceCharge variableSeviceCharge = new VariableSeviceCharge();
        ChargeVariables chargeVariables = new ChargeVariables();
        VariableSeviceChargeOverride variableSeviceChargeOverride = new VariableSeviceChargeOverride();
        AccountAdjustSupportData accountAdjustSupportData = new AccountAdjustSupportData();
        if (bilAccount.getBillTypeCd().equals("SP")) {
            variableSeviceChargeOverride = accountAdjustDatesService.readOverrideServiceCharge(bilAccount);
            if (variableSeviceChargeOverride == null) {
                variableSeviceChargeOverride = new VariableSeviceChargeOverride();
            }
            if (accountAdjustDates.getAddDateIndicator() != CHAR_Y && accountAdjustDates.getAddDateIndicator() != CHAR_Q
                    && accountAdjustDates.getAddDateIndicator() != CHAR_S
                    && accountAdjustDates.getAddDateIndicator() != CHAR_F
                    && variableSeviceChargeOverride.getRuleInd() != CHAR_Y) {
                Integer count = bilSptPlnCrgRepository.getCount(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd(),
                        SHORT_ZERO, CHAR_Y, "GA");
                if (count != null && count > SHORT_ZERO) {
                    accountAdjustSupportData.setServiceChargeIndicator(CHAR_Y);
                    accountAdjustSupportData.setServiceChargeTypeCode("GA");
                } else {
                    accountAdjustSupportData.setServiceChargeIndicator(BLANK_CHAR);
                    accountAdjustSupportData.setServiceChargeTypeCode(BLANK_STRING);
                }
            }
        }

        List<BilEftPlnCrg> bilEftPlanChargeList = null;
        if (variableSeviceChargeOverride.getRuleInd() != CHAR_Y && adjustDatesReviewDetails.isEftCollectionMethod()) {
            bilEftPlanChargeList = bilEftPlnCrgRepository.findByCollectionPlan(bilAccount.getCollectionPlan());

        }

        List<BilCrgAmounts> downPaymentList = null;
        DownPaymentFee downPaymentFee = new DownPaymentFee();
        if (accountAdjustDates.getBillPlanChangeIndicator() != CHAR_Y
                && (adjustDatesReviewDetails.isDpifRule() || accountAdjustDates.getSupportPlanIndicator() == CHAR_Y)) {
            downPaymentList = accountAdjustDatesService.getFutureDownpaymentRows(accountAdjustDates.getAccountId(),
                    downPaymentFee);
        }

        List<BilSptPlnCrg> bilSupportPlanChargeList = bilSptPlnCrgRepository
                .findByBilTypeCodeClassCode(bilAccount.getBillTypeCd(), bilAccount.getBillClassCd());
        logger.debug("get bill support plan charges for bilTypeCode:{} and bilClassCode:{}", bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd());

        if (bilSupportPlanChargeList.size() == SHORT_ONE) {
            BilSptPlnCrg bilSptPlnCrg = bilSupportPlanChargeList.get(SHORT_ZERO);
            accountAdjustSupportData.setServiceChargeIndicator(bilSptPlnCrg.getBilSptScgInd());
            accountAdjustSupportData.setServiceChargeTypeCode(bilSptPlnCrg.getBilScgTypeCd());
            accountAdjustSupportData.setServiceChargeCode(bilSptPlnCrg.getBilScgActionCd());
            accountAdjustSupportData.setServiceChargeAmount(bilSptPlnCrg.getBilSvcCrgAmt());
            accountAdjustSupportData.setPremiumAmount(bilSptPlnCrg.getBilPrmAmt());
            accountAdjustSupportData.setSeviceChargeIncAmount(bilSptPlnCrg.getBilScgIcrAmt());
            accountAdjustSupportData.setPremiumIncAmount(bilSptPlnCrg.getBilPrmIcrAmt());
            accountAdjustSupportData.setMaxServiceChargeAmount(bilSptPlnCrg.getBilMaxScgAmt());
        } else {
            accountAdjustSupportData.setServiceChargeIndicator(bilSupportPlan.getBspScgChgInd());
            accountAdjustSupportData.setServiceChargeTypeCode(bilSupportPlan.getBspScgchgTypCd());
            accountAdjustSupportData.setServiceChargeCode(bilSupportPlan.getBspSvcCd());
            accountAdjustSupportData.setServiceChargeAmount(bilSupportPlan.getBspScgAmt());
            accountAdjustSupportData.setPremiumAmount(bilSupportPlan.getBspPrmAmt());
            accountAdjustSupportData.setSeviceChargeIncAmount(bilSupportPlan.getBspScgIncAmt());
            accountAdjustSupportData.setPremiumIncAmount(bilSupportPlan.getBspPrmIncAmt());
            accountAdjustSupportData.setMaxServiceChargeAmount(bilSupportPlan.getBspMaxScgAmt());
        }
        boolean isXHOLRule = false;
        BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(RULE_XHOL, BLANK_STRING, BLANK_STRING,
                BLANK_STRING, accountAdjustDates.getProcessDate(), accountAdjustDates.getProcessDate());

        if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == 'Y' && bilRulesUct.getBrtParmListTxt() != null
                && !bilRulesUct.getBrtParmListTxt().trim().isEmpty()) {
            isXHOLRule = true;

        }

        char previousSchedule = ScheduleCode.NO_SCHEDULE.getValue();
        if (fpddIndicator == CHAR_Y) {
            previousSchedule = accountAdjustDatesService.getPreviousSchedule(adjustDatesReviewDetails.isAccountBilled(),
                    accountAdjustDates);
            if (previousSchedule == ScheduleCode.FUTURE_SCHEDULE.getValue()) {
                previousSchedule = accountAdjustDatesService.getNonPreviousSchedule(accountAdjustDates,
                        bilSupportPlan.getBspMinInvAmt());
            }
        }

        FirstPossibleDueDate firstPossibleDueDate = firstPossibleDueDateService.processFirstPossibleDueDateLogic(
                bilActRules.getBruIncWknInd(), isXHOLRule, bilRulesUct, accountAdjustDates, adjustDatesReviewDetails);

        List<BilIstSchedule> bilIstSchedules = bilIstScheduleRepository.findResumeSchedule(
                accountAdjustDates.getAccountId(), firstPossibleDueDate.getReferenceDate(), getInvoiceCodes());

        if (bilIstSchedules != null && !bilIstSchedules.isEmpty()) {
            if (isManualReinvoice) {
                isManualReinvoice = cancelManualReinvoice(accountAdjustDates, DateRoutine.dateTimeAsYYYYMMDD(0));
                adjustDatesReviewDetails.setManualReinvoice(isManualReinvoice);
            }

            dateResetAll(accountAdjustDates.getAccountId());

            processSkipIst(accountAdjustDates.getAccountId(), firstPossibleDueDate.getDate(),
                    firstPossibleDueDate.getReferenceDate(), bilSupportPlan.getBilRvwFqyCd());
        }

        Object[] newDatesData = firstPossibleDueDateService.checkDateAdd(true, adjustDatesReviewDetails, bilAccount,
                accountAdjustDates, isXHOLRule, bilRulesUct);
        ZonedDateTime newReferemceDate = (ZonedDateTime) newDatesData[0];
        ZonedDateTime newAdjustDate = (ZonedDateTime) newDatesData[1];
        ZonedDateTime newInvoiceDate = (ZonedDateTime) newDatesData[2];
        ZonedDateTime newSystemDate = (ZonedDateTime) newDatesData[3];
        char dateType = (char) newDatesData[4];
        adjustDatesReviewDetails.setNewAdjustDueDate(newAdjustDate);
        adjustDatesReviewDetails.setNewReferenceDate(newReferemceDate);
        adjustDatesReviewDetails.setNewInvoiceDate(newInvoiceDate);
        adjustDatesReviewDetails.setNewSystemDate(newSystemDate);
        adjustDatesReviewDetails.setDateType(dateType);

        accountAdjustDatesService.processDatesAssignment(dateType, bilAccount, accountAdjustDates,
                adjustDatesReviewDetails, firstPossibleDueDate, variableSeviceChargeOverride, variableSeviceCharge,
                chargeVariables, accountAdjustSupportData, disableFpdd, bilEftPlanChargeList, bilSupportPlanChargeList,
                isXHOLRule, bilRulesUct, bilSupportPlan);

        accountAdjustDatesService.processChargeRows(SHORT_ZERO, previousSchedule, bilAccount, accountAdjustDates,
                firstPossibleDueDate, adjustDatesReviewDetails, isXHOLRule, bilRulesUct, bilEftPlanChargeList,
                bilSupportPlanChargeList, variableSeviceChargeOverride, variableSeviceCharge, accountAdjustSupportData,
                chargeVariables, downPaymentList, downPaymentFee);
    }

    private void processSkipIst(String accountId, ZonedDateTime fpddDate, ZonedDateTime trueFpddReferenceDate,
            String bilFrequencyCode) {
        ZonedDateTime resetFpddDate = DateRoutine.calculateDateTimeByFrequency(bilFrequencyCode, fpddDate);
        ZonedDateTime resetFpddReferenceDate = DateRoutine.calculateDateTimeByFrequency(bilFrequencyCode,
                trueFpddReferenceDate);
        List<BilDates> bilDatesList = bilDatesRepository.fetchBilDatesBySetDates(accountId,
                new ArrayList<>(Arrays.asList(BLANK_CHAR)), resetFpddReferenceDate, resetFpddDate, resetFpddDate,
                CHAR_X);
        if (bilDatesList != null && !bilDatesList.isEmpty()) {
            skipIstRow(accountId, bilDatesList);
        }
    }

    private void skipIstRow(String accountId, List<BilDates> bilDatesList) {
        for (BilDates bilDates : bilDatesList) {
            List<BilIstSchedule> bilIstScheduleList = bilIstScheduleRepository.getScheduleByReferenceDate(accountId,
                    bilDates.getBilDatesId().getBilReferenceDt());
            if (bilIstScheduleList != null && !bilIstScheduleList.isEmpty()) {
                continue;
            }
            Character crgIndicator = bilCrgAmountsRepository.findInvoiceCode(accountId, bilDates.getBilAdjDueDt(),
                    bilDates.getBilInvDt());
            if (crgIndicator != null && crgIndicator != BLANK_CHAR) {
                continue;
            }
            logger.debug("Remove BilDates row - accountId: {} reference date: {}", accountId,
                    bilDates.getBilDatesId().getBilReferenceDt());
            bilDatesRepository.delete(bilDates);
        }
    }

    private void dateResetAll(String accountId) {
        List<ZonedDateTime> bilSysDueDtList = bilDatesRepository.selectSysDueDateByInvoiceCdList(accountId,
                getInvoiceCodes());
        if (bilSysDueDtList != null && !bilSysDueDtList.isEmpty()) {
            for (ZonedDateTime bilSystemDueDate : bilSysDueDtList) {
                ZonedDateTime oldSystemDate = bilSystemDueDate;

                updateScheduleAll(accountId, oldSystemDate);
                updateCreditScheduleAll(accountId, oldSystemDate);
                updateCashAll(accountId, oldSystemDate);
                deleteBilDatesAll(accountId, oldSystemDate);
            }

        }
    }

    private void updateScheduleAll(String accountId, ZonedDateTime oldSystemDate) {

        ZonedDateTime defaultDate = DateRoutine.defaultDateTime();
        bilIstScheduleRepository.updateScheduleRowByInvoiceCode(defaultDate, defaultDate, defaultDate, defaultDate,
                BLANK_CHAR, InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue(), accountId, oldSystemDate,
                getInvoiceCodes());
        logger.debug("Update All Bil Ist scheduling row to new date");

    }

    private void updateCreditScheduleAll(String accountId, ZonedDateTime oldSystemDate) {

        ZonedDateTime defaultDate = DateRoutine.defaultDateTime();
        bilCreScheduleRepository.updateCreScheduleRowBySystemDate(defaultDate, defaultDate, defaultDate, accountId,
                oldSystemDate);
        logger.debug("Update All Bil Credit scheduling row to new date");

    }

    private void updateCashAll(String accountId, ZonedDateTime oldSystemDate) {

        ZonedDateTime defaultDate = DateRoutine.defaultDateTime();
        bilCashDspRepository.updateCashRowBySysDate(defaultDate, defaultDate, defaultDate, accountId, oldSystemDate,
                ChargeType.EMPTY_CHAR.getChargeType());
        logger.debug("Update All Bil Cash Deposit row to new date ");

    }

    private void deleteBilDatesAll(String accountId, ZonedDateTime oldSystemDate) {

        bilDatesRepository.deleteRowsByInvoiceCd(accountId, oldSystemDate, getInvoiceCodes());

    }

    private List<Character> getInvoiceCodes() {
        List<Character> invoiceCodeList = new ArrayList<>();
        invoiceCodeList.add(InvoiceTypeCode.SCHEDULEAMOUNT_BELOWMINIMUM_FUTURE.getValue());
        invoiceCodeList.add(InvoiceTypeCode.EMPTY_CHAR.getValue());
        return invoiceCodeList;
    }

    private boolean cancelManualReinvoice(AccountAdjustDates accountAdjustDates, ZonedDateTime holdNewDate) {

        bilActInquiryRepository.deleteManualReInvoiceRows(accountAdjustDates.getAccountId(), "MRI");

        String bilAcyDesCode = "REP";
        String bilDesReasonType = "MRI";
        double bilAmounts = DECIMAL_ZERO;

        if (isWriteHistory(accountAdjustDates.getQuoteIndicator(), accountAdjustDates.getAddChargeIndicator(),
                accountAdjustDates.getChargeAmount())) {
            accountSummary(bilAcyDesCode, bilDesReasonType, bilAmounts, holdNewDate, accountAdjustDates);
        }

        return false;
    }

    private boolean isWriteHistory(String quoteIndicator, char addChargeIndicator, Double chargeAmount) {
        return quoteIndicator.charAt(SHORT_ZERO) != CHAR_Q
                && !((addChargeIndicator == ChargeType.PENALTYCHARGES.getChargeType()
                        || addChargeIndicator == ChargeType.LATECHARGES.getChargeType())
                        && chargeAmount == DECIMAL_ZERO);
    }

    private void accountSummary(String bilAcyDesCode, String bilDesReasonType, double bilAmounts,
            ZonedDateTime holdNewDate, AccountAdjustDates accountAdjustDates) {

        ZonedDateTime acyDate1 = DateRoutine.defaultDateTime();
        ZonedDateTime acyDate2 = DateRoutine.defaultDateTime();
        Short bilAcySeq = 0;

        bilAcySeq = bilActSummaryRepository.findByMaxSeqNumberBilAccountId(accountAdjustDates.getAccountId(),
                accountAdjustDates.getProcessDate());
        if (bilAcySeq == null) {
            bilAcySeq = 0;
        } else {
            bilAcySeq = (short) (bilAcySeq + 1);
        }

        if (accountAdjustDates.getReferenceDateIndicator() == CHAR_Y
                || accountAdjustDates.getStartDateIndicator() == CHAR_Y) {
            acyDate1 = accountAdjustDates.getCurrentDueDate();
            acyDate2 = accountAdjustDates.getNewDueDate();
            if (holdNewDate.compareTo(DateRoutine.dateTimeAsYYYYMMDD(0)) != 0) {
                acyDate2 = holdNewDate;
            }
        }

        if (bilAcyDesCode.equalsIgnoreCase(ActivityType.ECC.getValue())) {
            List<BilEftBank> bilEftBankList = bilEftBankRepository.findBilEftBankEntry(
                    accountAdjustDates.getAccountId(), CHAR_A, accountAdjustDates.getProcessDate(), 'A',
                    PageRequest.of(SHORT_ZERO, SHORT_ONE));

            if (bilEftBankList != null && !bilEftBankList.isEmpty()) {
                acyDate1 = bilEftBankList.get(SHORT_ZERO).getBilEftBankId().getBebEffectiveDt();
            }
        }

        BilActSummary bilActSummary = new BilActSummary();
        BilActSummaryId bilActSummaryId = new BilActSummaryId(accountAdjustDates.getAccountId(),
                accountAdjustDates.getProcessDate().with(LocalTime.MIN), bilAcySeq.shortValue());
        bilActSummary.setBillActSummaryId(bilActSummaryId);
        bilActSummary.setPolSymbolCd(BLANK_STRING);
        bilActSummary.setPolNbr(BLANK_STRING);
        bilActSummary.setBilAcyDesCd(bilAcyDesCode);
        bilActSummary.setBilDesReaTyp(bilDesReasonType);
        bilActSummary.setBilAcyDes1Dt(acyDate1);
        bilActSummary.setBilAcyDes2Dt(acyDate2);
        bilActSummary.setBilAcyAmt(bilAmounts);
        if (!accountAdjustDates.getUserSequenceId().trim().isEmpty()) {
            bilActSummary.setUserId(accountAdjustDates.getUserSequenceId());
        } else {
            bilActSummary.setUserId(SYSTEM_USER_ID);
        }

        bilActSummary.setBilAcyTs(dateService.currentDateTime());
        bilActSummary.setBasAddDataTxt(BLANK_STRING);
        bilActSummaryRepository.saveAndFlush(bilActSummary);
    }

}