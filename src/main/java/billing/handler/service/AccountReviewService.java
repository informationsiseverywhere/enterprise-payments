package billing.handler.service;

import java.time.ZonedDateTime;

import billing.data.entity.BilStRulesUct;
import billing.handler.model.AccountReview;
import billing.handler.model.AccountReviewDetails;

public interface AccountReviewService {

    public AccountReviewDetails accountProcessReview(AccountReview accountReview);

    public boolean processAccountStatus(String accountId, char status, String suspendedFullReasonCd,
            char renewDownpayBillIndicator);

    public void writeHistory(String accountId, String activityDesCd, String activityDesType, String policyNumber,
            String policySymbolCd, ZonedDateTime activityDesc1Dt, ZonedDateTime activityDesc2Dt, double amount,
            String additionalDataTxt, String userId);

    public char checkSplitIndicator(String policyId, ZonedDateTime polEffectiveDate);

    public BilStRulesUct getBilStRuleUct(String ruleId, String billStatePvnCode, String lobCode,
            String masterCompanyNbr);

    public char getQueueWrtIndicator();

}
