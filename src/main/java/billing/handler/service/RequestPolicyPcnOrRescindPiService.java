package billing.handler.service;

import billing.handler.model.PolicyPcnAndRescInterface;

public interface RequestPolicyPcnOrRescindPiService {

    public String[] processPcnOrRescind(PolicyPcnAndRescInterface policyPcnAndRescInterface);

}
