package billing.handler.service;

import java.time.ZonedDateTime;

public interface SplitBillingActivityService {

    public void noncausalRequest(String transactionType, boolean isPolicyPed, String accountId, String policyId,
            ZonedDateTime polEffectiveDate);

}
