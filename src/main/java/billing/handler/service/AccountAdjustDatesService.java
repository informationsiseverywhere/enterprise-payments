package billing.handler.service;

import java.time.ZonedDateTime;
import java.util.List;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActRules;
import billing.data.entity.BilCrgAmounts;
import billing.data.entity.BilEftPlnCrg;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSptPlnCrg;
import billing.data.entity.BilSupportPlan;
import billing.handler.model.AccountAdjustDates;
import billing.handler.model.AccountAdjustSupportData;
import billing.handler.model.AdjustDatesReviewDetails;
import billing.handler.model.ChargeVariables;
import billing.handler.model.DownPaymentFee;
import billing.handler.model.FirstPossibleDueDate;
import billing.handler.model.VariableSeviceCharge;
import billing.handler.model.VariableSeviceChargeOverride;

public interface AccountAdjustDatesService {

    public List<BilCrgAmounts> getFutureDownpaymentRows(String accountId, DownPaymentFee downPaymentFee);

    public void processDownpaymentFees(String accountId, boolean isQuote, double downPaymentFeeAmount,
            DownPaymentFee downpaymentFeeData, VariableSeviceChargeOverride variableSeviceChargeOverride,
            VariableSeviceCharge variableSeviceCharge, AccountAdjustSupportData accountAdjustSupportData,
            String userSequenceId);

    public void checkForExcessCredit(String accountId, ZonedDateTime referenceDate);

    public boolean checkForStateSpecificServiceCharge(String bilTypeCode, String accountId, String bilClassCode);

    public void processDatesAssignment(char dateType, BilAccount bilAccount, AccountAdjustDates accountAdjustDates,
            AdjustDatesReviewDetails adjustDatesReviewDetails, FirstPossibleDueDate firstPossibleDueDate,
            VariableSeviceChargeOverride variableSeviceChargeOverride, VariableSeviceCharge variableSeviceCharge,
            ChargeVariables chargeVariables, AccountAdjustSupportData accountAdjustSupportData, boolean isDisableFpdd,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, boolean isXHOLRule,
            BilRulesUct bilRulesUct, BilSupportPlan bilSupportPlan);

    public AdjustDatesReviewDetails processAdjustDates(AccountAdjustDates accountAdjustDates);

    public VariableSeviceChargeOverride readOverrideServiceCharge(BilAccount bilAccount);

    public void processServiceWriteOff(BilAccount bilAccount, BilActRules bilActRules,
            AccountAdjustDates accountAdjustDates);

    public char getPreviousSchedule(boolean isDicvRule, AccountAdjustDates accountAdjustDates);

    public char getNonPreviousSchedule(AccountAdjustDates accountAdjustDates, Double minimumInvoiceAmount);

    public void processChargeRows(int cancelCount, char previousSchedule, BilAccount bilAccount,
            AccountAdjustDates accountAdjustDates, FirstPossibleDueDate firstPossibleDueDate,
            AdjustDatesReviewDetails adjustDatesReviewDetails, boolean isXHOLRule, BilRulesUct bilRulesUct,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList,
            VariableSeviceChargeOverride variableSeviceChargeOverride, VariableSeviceCharge variableSeviceCharge,
            AccountAdjustSupportData accountAdjustSupportData, ChargeVariables chargeVariables,
            List<BilCrgAmounts> downPaymentList, DownPaymentFee downPaymentFee);

}
