package billing.handler.service;

import java.time.ZonedDateTime;
import java.util.Map;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActRules;
import billing.data.entity.BilSupportPlan;

public interface BillingRulesService {

    public Map<String, Object> disburseToSource(BilAccount bilAccount, BilActRules bilActRules,
            Map<String, Object> nextDisburseDates);

    public Map<String, Object> paymentDisburse(BilAccount bilAccount, BilSupportPlan bilSupportPlan,
            Map<String, Object> nextDisburseDates);

    public Boolean overrideWkendInd(char excludeWkendIndicator, char includeWkendIndicator);

    public ZonedDateTime getNonComplianceDate(String accountId, BilSupportPlan bilSupportPlan);

    public Boolean checkForRenewDownpay(String accountId);

    public ZonedDateTime checkForSecondNotice(String accountId, char suspendBillIndicator, char driverIndicator,
            char eftCollectionIndicator, short secondNoticeNumber);

    public Short readArwoRuleCheck(String bilTypeCode, String bilClassCode, String planCode);

}
