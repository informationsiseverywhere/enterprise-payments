package billing.handler.service;

import billing.handler.model.StaticPcnRescind;

public interface StaticPcnRescindService {

    public void processStaticPcnRescind(StaticPcnRescind staticPcnRescind);

}
