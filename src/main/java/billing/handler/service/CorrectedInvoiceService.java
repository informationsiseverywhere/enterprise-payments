package billing.handler.service;

import java.time.ZonedDateTime;

import billing.data.entity.BilActRules;
import billing.handler.model.AccountProcessingAndRecovery;

public interface CorrectedInvoiceService {

    public void processCorrectedInvoiceTriggers(ZonedDateTime nextInvoiceDate, ZonedDateTime recoveryDate,
            BilActRules bilActRules, AccountProcessingAndRecovery accountProcessingAndRecovery);

}
