package billing.handler.service;

import java.time.ZonedDateTime;
import java.util.List;

import billing.data.entity.BilAccount;
import billing.data.entity.BilDates;
import billing.data.entity.BilEftPlnCrg;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSptPlnCrg;
import billing.data.entity.BilSupportPlan;
import billing.handler.model.AccountAdjustDates;
import billing.handler.model.AccountAdjustSupportData;
import billing.handler.model.AdjustDatesReviewDetails;
import billing.handler.model.ChargeVariables;
import billing.handler.model.FirstPossibleDueDate;
import billing.handler.model.VariableSeviceCharge;
import billing.handler.model.VariableSeviceChargeOverride;

public interface FirstPossibleDueDateService {

    public FirstPossibleDueDate processFirstPossibleDueDateLogic(char includeWeekend, boolean isXHOLRule,
            BilRulesUct bilRulesUct, AccountAdjustDates accountAdjustDates,
            AdjustDatesReviewDetails adjustDatesReviewDetails);

    public boolean determineIfInvoicedToday(String accountId, ZonedDateTime applicationZonedDateTime);

    public ZonedDateTime getFpddDate(boolean isInvoicedToday, AccountAdjustDates accountAdjustDates,
            char includeWeekend, short dueDateDisplacementNumber, boolean isXHOLRule, BilRulesUct bilRulesUct);

    public ZonedDateTime getFpddInvoiceDate(ZonedDateTime fpddDate, ZonedDateTime processDate, char includeWeekend,
            short dueDateDisplacementNumber, boolean isXHOLRule, BilRulesUct bilRulesUct);

    public ZonedDateTime[] getFpddReferenceDate(int referenceDuration, ZonedDateTime fpddDate, BilAccount bilAccount,
            boolean isFpddOnTimeProcessing);

    public ZonedDateTime holidayDateProcessing(ZonedDateTime ZonedDateTime, ZonedDateTime processDate,
            BilRulesUct bilRulesUct);

    public BilDates newFpddBillCheck(AccountAdjustDates accountAdjustDates, ZonedDateTime newSystemDate,
            ZonedDateTime newAdjustDate, ZonedDateTime newReferenceDate, ZonedDateTime newInvoiceDate,
            BilSupportPlan bilSupportPlan, boolean isErcvIssDateInv, boolean isActBilled);

    public Object[] deterIfInvoicePaid(String accountId, ZonedDateTime checkDate, double bspMinInvoiceAmounts);

    public Object[] getDateType(String accountId, ZonedDateTime bilAdjustDate, ZonedDateTime bilInvoiceDate);

    public Double getBalance(String accountId, ZonedDateTime bilAdjustDate, ZonedDateTime bilInvoiceDate);

    public void updateInvoiceIndicator(String accountId, char newInvoiceIndicator, ZonedDateTime bilAdjDueDt,
            ZonedDateTime bilInvDt);

    public void processFirstPossibleDueDateResetLogic(BilAccount bilAccount, FirstPossibleDueDate firstPossibleDueDate,
            double minimumInvoiceAmount, short pendingCancellationDays, char includeWeekend,
            AccountAdjustDates accountAdjustDates, short dueDateDisplacementNumber, boolean isXHOLRule,
            BilRulesUct bilRulesUct, BilSupportPlan bilSupportPlan, VariableSeviceCharge variableSeviceCharge,
            ChargeVariables chargeVariables, VariableSeviceChargeOverride variableSeviceChargeOverride,
            AdjustDatesReviewDetails adjustDatesReviewDetails, AccountAdjustSupportData accountAdjustSupportData,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, int cancelCount);

    public void updateOcbFpdd(String bilAccountId, ZonedDateTime newAdjDate, ZonedDateTime newInvDate,
            ZonedDateTime newSysDate, ZonedDateTime minBilSysDueDt);

    public void processBelowMinimumServiceCharge(BilAccount bilAccount, boolean fpddset, Double minInvoiceAmount,
            double serviceChargeAmount, AccountAdjustDates accountAdjustDates,
            AdjustDatesReviewDetails adjustDatesReviewDetails, VariableSeviceCharge variableSeviceCharge,
            ChargeVariables chargeVariables, VariableSeviceChargeOverride variableSeviceChargeOverride,
            AccountAdjustSupportData accountAdjustSupportData, List<BilEftPlnCrg> bilEftPlanChargeList,
            List<BilSptPlnCrg> bilSupportPlanChargeList);

    public Object[] updateForFpdd(String accountId, char bilDateType, BilSupportPlan bilSupportPlan,
            BilAccount bilAccount, char invoiceCode, ZonedDateTime bilAdjustDate, ZonedDateTime bilInvoiceDate,
            FirstPossibleDueDate firstPossibleDueDate, AdjustDatesReviewDetails adjustDatesReviewDetails,
            AccountAdjustDates accountAdjustDates, boolean isXHOLRule, BilRulesUct bilRulesUct);

    public void getServiceChargePerTerm(String quoteIndicator, VariableSeviceCharge variableSeviceCharge,
            ZonedDateTime adjustDueDate, AccountAdjustSupportData accountAdjustSupportData, String accountId);

    public void getServiceChargeTermAmount(VariableSeviceChargeOverride variableSeviceChargeOverride,
            ZonedDateTime adjustDueDate, String accountId, VariableSeviceCharge variableSeviceCharge,
            AccountAdjustSupportData accountAdjustSupportData);

    public void insertUpdateChargeAmounts(String accountId, ZonedDateTime ajustDueDate, ZonedDateTime invoiceDate,
            Double serviceChargeAmount, char newInvoiceIndicator, String bilTypeCode, Character dateType,
            AdjustDatesReviewDetails adjustDatesReviewDetails, AccountAdjustSupportData accountAdjustSupportData,
            ZonedDateTime currentDate, String userSequenceId);

    public void readServiceAmount(AdjustDatesReviewDetails adjustDatesReviewDetails, BilAccount bilAccount,
            VariableSeviceCharge variableSeviceCharge, ZonedDateTime adjustDueDate, ChargeVariables chargeVariables,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, char newInvInd,
            AccountAdjustSupportData accountAdjustSupportData, AccountAdjustDates accountAdjustDates,
            VariableSeviceChargeOverride variableSeviceChargeOverride);

    public void fpddCheck(char dateType, AdjustDatesReviewDetails adjustDatesReviewDetails,
            FirstPossibleDueDate firstPossibleDueDate, BilSupportPlan bilSupportPlan,
            AccountAdjustDates accountAdjustDates, BilAccount bilAccount,
            VariableSeviceChargeOverride variableSeviceChargeOverride, VariableSeviceCharge variableSeviceCharge,
            ChargeVariables chargeVariables, boolean isDisableFpdd, boolean isAssignDates,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, boolean isXHOLRule,
            BilRulesUct bilRulesUct, AccountAdjustSupportData accountAdjustSupportData);

    public void updatePenalty(BilAccount bilAccount, AccountAdjustDates accountAdjustDates,
            BilSupportPlan bilSupportPlan, VariableSeviceCharge variableSeviceCharge, ChargeVariables chargeVariables,
            int cancelCount, ZonedDateTime newAdjustDate, ZonedDateTime newInvoiceDate,
            VariableSeviceChargeOverride variableSeviceChargeOverride,
            AdjustDatesReviewDetails adjustDatesReviewDetails, AccountAdjustSupportData accountAdjustSupportData,
            List<BilEftPlnCrg> bilEftPlanChargeList, List<BilSptPlnCrg> bilSupportPlanChargeList, boolean isXHOLRule,
            BilRulesUct bilRulesUct, FirstPossibleDueDate firstPossibleDueDate);

    public Object[] checkDateAdd(boolean isRestAll, AdjustDatesReviewDetails adjustDatesReviewDetails,
            BilAccount bilAccount, AccountAdjustDates accountAdjustDates, boolean isXHOLRule, BilRulesUct bilRulesUct);

    public void oneDateAdd(AccountAdjustDates accountAdjustDates, AdjustDatesReviewDetails adjustDatesReviewDetails,
            ZonedDateTime date, boolean isXHOLRule, BilRulesUct bilRulesUct, char includeWeekend,
            BilSupportPlan bilSupportPlan);

    public ZonedDateTime getNextSchedDate(BilSupportPlan bilSupportPlan, ZonedDateTime nextSchedDt,
            BilAccount bilAccount, char typeDatePropSw);

    public char getSeasonalDate(String accountId, ZonedDateTime deductionDt);

    public ZonedDateTime[] getPendingCancelPlusFirst(AdjustDatesReviewDetails adjustDatesReviewDetails,
            ZonedDateTime checkDateInput, BilSupportPlan bilSupportPlan, BilAccount bilAccount, boolean isXHOLRule,
            BilRulesUct bilRulesUct, AccountAdjustDates accountAdjustDates);

    public void calculateServicePerTerm(String quoteIndicator, VariableSeviceCharge variableSeviceCharge,
            String accountId, AccountAdjustSupportData accountAdjustSupportData);

    public boolean checkBilAmountsExistance(String accountId, ZonedDateTime adjustDueDate);
}
