package billing.handler.service;

import billing.handler.model.AccountAdjustDates;

public interface OneDateResetService {

    public void processOneDateReset(AccountAdjustDates accountAdjustDates);

}
