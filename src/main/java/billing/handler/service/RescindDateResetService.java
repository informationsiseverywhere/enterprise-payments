package billing.handler.service;

import billing.handler.model.AccountAdjustDates;

public interface RescindDateResetService {

    public void processRescindDateReset(AccountAdjustDates accountAdjustDates);

}
