package billing.handler.service;

import java.time.ZonedDateTime;

public interface CalculatePaidToDateService {

    public ZonedDateTime getPaidToDate(String accountId, String policyId, ZonedDateTime polEffectiveDate,
            ZonedDateTime polExpirationDate, ZonedDateTime firstDate, boolean isEditPolBits, boolean policyLevel);

}
