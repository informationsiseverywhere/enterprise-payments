package billing.handler.service;

import java.time.ZonedDateTime;

import billing.data.entity.BilAccount;
import billing.data.entity.BilActRules;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilSupportPlan;
import billing.handler.model.AccountAdjustDates;
import billing.handler.model.FirstPossibleDueDate;

public interface AllDatesResetService {

    public void processAllDatesReset(AccountAdjustDates accountAdjustDates);

    public boolean pcnOnNoResponseRenewalCheck(String accountId, ZonedDateTime se3Date);

    public int cancelReset(int referenceDuration, BilAccount bilAccount, AccountAdjustDates accountAdjustDates,
            FirstPossibleDueDate firstPossibleDueDate, BilSupportPlan bilSupportPlan, BilActRules bilActRules,
            boolean isXHOLRule, BilRulesUct bilRulesUct, short dueDateDisplacementNumber, Object[] dates);

}
