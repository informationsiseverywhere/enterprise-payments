package billing.handler.service;

import billing.data.entity.BilAccount;
import billing.data.entity.BilBillPlan;
import billing.handler.model.AccountProcessingAndRecovery;
import billing.handler.model.AccountReviewDetails;
import billing.handler.model.PolicyReview;

public interface PolicyReviewService {

    public BilBillPlan processPolicies(BilAccount bilAccount, AccountReviewDetails accountReviewDetails,
            AccountProcessingAndRecovery accountProcessingAndRecovery, PolicyReview policyReview,
            boolean isBillPlanChange);

}
