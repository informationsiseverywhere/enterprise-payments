package billing.handler.service;

import billing.handler.model.AccountAdjustDates;

public interface ChargesAccountAdjustDatesService {

    public void processChargesAccountAdjustDates(AccountAdjustDates accountAdjustDates);
}
