package billing.handler.service;

import billing.handler.model.AccountAdjustDates;

public interface ResumeDateResetService {

    public void processResumeDateReset(AccountAdjustDates accountAdjustDates);

}
