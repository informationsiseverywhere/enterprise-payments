package billing.handler.service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

import billing.data.entity.BilAccount;
import billing.data.entity.BilBillPlan;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.handler.model.AccountProcessingAndRecovery;
import billing.handler.model.AccountReviewDetails;
import billing.handler.model.Cancellation;
import billing.handler.model.MaxPendingCancellation;
import billing.handler.model.PolicyReview;
import billing.model.Contract;

public interface CancellationService {

    public Object[] processNsfPcn(boolean isPcnmRule, MaxPendingCancellation maxPendingCancellation, BilAccount bilAccount,
            BilPolicy bilPolicy, BilPolicyTerm bilPolicyTerm, BilBillPlan bilBillPlan,
            AccountReviewDetails accountReviewDetails, Cancellation cancellation, Map<String, Object> pedAcyMap,
            Map<String, Object> termDataMap, AccountProcessingAndRecovery accountProcessingAndRecovery,
            Map<String, Object> pcnnDataMap);

    public ZonedDateTime callCalculatePolicyPaidToDate(String accountId, String policyId, ZonedDateTime pedEffDate,
            ZonedDateTime pedExpDate);

    public ZonedDateTime callCalculatePaidToDate(String accountId, String policyId, ZonedDateTime pedEffDate,
            ZonedDateTime pedExpDate);

    public void callSplitBillingActivity(String accountId, String nonCausalTypeCode, boolean isPolicyPedValued,
            String policyId, ZonedDateTime polEffectiveDate);

    public void wipLink(String accountId, String accountNumber, String activityTemplateId);

    public char pcnRnpRequest(BilAccount bilAccount, BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy,
    		BilBillPlan bilBillPlan, ZonedDateTime standCncDate, char polTermTypeCode, char splitIndicator, short pndCncNbr,
            Cancellation cancellation, Map<String, Object> pedAcyMap, Map<String, Object> pcnnDataMap,
            Map<String, Object> termDataMap, AccountProcessingAndRecovery accountProcessingAndRecovery);

    public char checkForPrevHistory(String bilAcyDes, String accountId, String polNumber, String polSymbolCd,
            ZonedDateTime polEffDate, ZonedDateTime planExpDate);

    public char getPolTermTypeCode(String accountId, String policyId, ZonedDateTime polEffDate);
    
    public Object[] processPnpPcn(boolean isPcnmRule, char useCrgDateIndicator, BilAccount bilAccount,
            BilPolicyTerm bilPolicyTerm, BilPolicy bilPolicy, BilBillPlan bilBillPlan,
            AccountProcessingAndRecovery accountProcessingAndRecovery, AccountReviewDetails accountReviewDetails,
            MaxPendingCancellation maxPendingCancellation, Cancellation cancellation, PolicyReview policyReview,
            Map<String, Object> pedAcyMap, Map<String, Object> pcnnDataMap, Map<String, Object> termDataMap,
            List<Contract> contractList);
    
    public Object[] processRnpPcn(boolean isPcnmRule, char useCrgDateIndicator, char renewDpCancInd,
            ZonedDateTime termRnpDate, ZonedDateTime firstInstDueDt, BilAccount bilAccount, BilPolicyTerm bilPolicyTerm,
            BilPolicy bilPolicy, BilBillPlan bilBillPlan, AccountProcessingAndRecovery accountProcessingAndRecovery,
            AccountReviewDetails accountReviewDetails, MaxPendingCancellation maxPendingCancellation,
            Cancellation cancellation, PolicyReview policyReview, Map<String, Object> pedAcyMap,
            Map<String, Object> termDataMap, Map<String, Object> pcnnDataMap, char lapseCancIndicator,
            List<Contract> contractList);
    
    public Short[] getPolicyLegalDays(String processCode, String issueSysId, BilPolicyTerm bilPolicyTerm,
            BilPolicy bilPolicy);
}
