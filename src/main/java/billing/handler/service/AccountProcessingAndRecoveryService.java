package billing.handler.service;

import java.time.ZonedDateTime;

import billing.handler.model.AccountProcessingAndRecovery;

public interface AccountProcessingAndRecoveryService {

    public void accountAndRecoveryProcess(AccountProcessingAndRecovery accountProcessingAndRecovery);

    public ZonedDateTime getNextInvoiceDate(String accountId);

    public void writeHistory(String accountId, String activityDesCd, String activityDesType, String policyNumber,
            String policySymbolCd, ZonedDateTime activityDesc1Dt, ZonedDateTime activityDesc2Dt,
            String additionalDataTxt, String userId, ZonedDateTime recoveryDate);

}
