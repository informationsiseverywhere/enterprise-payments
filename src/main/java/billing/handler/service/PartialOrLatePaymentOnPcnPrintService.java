package billing.handler.service;

import billing.handler.model.PartialOrLatePaymentOnPcnPrint;

public interface PartialOrLatePaymentOnPcnPrintService {

    public void processPartialOrLatePaymentOnPcnPrint(PartialOrLatePaymentOnPcnPrint partialOrLatePaymentOnPcnPrint);

}
