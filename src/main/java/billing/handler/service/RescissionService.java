package billing.handler.service;

import java.time.ZonedDateTime;
import java.util.Map;

import billing.data.entity.BilAccount;
import billing.data.entity.BilBillPlan;
import billing.data.entity.BilPolicy;
import billing.data.entity.BilPolicyTerm;
import billing.data.entity.BilRenDays;
import billing.handler.model.AccountProcessingAndRecovery;
import billing.handler.model.AccountReviewDetails;
import billing.handler.model.Rescission;

public interface RescissionService {

    public char processRescission(char polStatusCode, AccountProcessingAndRecovery accountProcessingAndRecovery,
            BilAccount bilAccount, BilBillPlan bilBillPlan, BilPolicy bilPolicy, BilPolicyTerm bilPolicyTerm,
            AccountReviewDetails accountReviewDetails, Rescission rescission, Map<String, Object> termDataMap,
            boolean isEquityUsed);

    public Object[] status(boolean isRescind, boolean isBpcCheck, double termBalanceAmount, char polStatus,
            String billSusFuReaCd);

    public Short checkForPayment(char checkForRescindInd, String accountId, String polNumber, String polSymbolCd,
            short bilDtbSequence, ZonedDateTime recoveryDate);

    public Short checkCreditInd(String accountId, ZonedDateTime recoveryDate);

    public BilRenDays readBilRenday(BilPolicyTerm bilPolicyTerm);

}
