package billing.handler.service;

import java.time.ZonedDateTime;

import billing.data.entity.BilAccount;

public interface ReinstatementService {

    public void checkReinstatement(BilAccount bilAccount, Short bilDtbSequence, ZonedDateTime recoveryDate, String userSequenceId);

    public void deleteLDRTrigger(String accountId, String lastPolicyId, ZonedDateTime polEfftiveDate);

    public void insertNreNrdSummary(String accountId, String bilAcyDesCd, ZonedDateTime recoveryDate);

    public void lastDayReinsPrintTrigger(String accountId, String payorClientId, char prtReason, String accountNumber,
            String masterCompany, String riskState, ZonedDateTime recoveryDate);

}
