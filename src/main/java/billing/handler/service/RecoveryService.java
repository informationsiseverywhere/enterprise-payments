package billing.handler.service;

import java.time.ZonedDateTime;

import billing.data.entity.BilAccount;
import billing.handler.model.AccountReviewDetails;
import billing.handler.model.RecoveryAccount;

public interface RecoveryService {

    public RecoveryAccount processRecovery(RecoveryAccount recoveryAccount, AccountReviewDetails accountReviewDetails,
            ZonedDateTime recoveryDate, String userSequenceId);

    public boolean resumeAccount(String accountId, BilAccount bilAccount, RecoveryAccount recoveryAccount,
            boolean isDateReset, AccountReviewDetails accountReviewDetails, ZonedDateTime recoveryDate,
            String userSequenceId);

    public boolean updateAccountAdjustDates(RecoveryAccount recoveryAccount, boolean isDateReset,
            AccountReviewDetails accountReviewDetails, ZonedDateTime recoveryDate, String userSequenceId);

}
