package billing.mongo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

import billing.mongo.converter.ZonedDateTimeReadConverter;
import billing.mongo.converter.ZonedDateTimeWriteConverter;

@Configuration
@EnableMongoRepositories(basePackages = { "billing.mongo.data" })
public class MongoConfiguration extends AbstractMongoClientConfiguration {
    @Value("${app.localTimeZone}")
    String localTimeZone;

    @Value("${mongo.db.name}")
    String database;

    @Value("${mongo.db.host}")
    String host;

    @Value("${mongo.db.port}")
    String port;

    @Value("${mongo.db.user}")
    String user;

    @Value("${mongo.db.password}")
    String password;

    @Value("${mongo.db.retry.writes}")
    String retryWrites;

    @Override
    protected String getDatabaseName() {
        return database;
    }

    @Bean
    @Override
    public MongoCustomConversions customConversions() {
        List<Converter<?, ?>> converterList = new ArrayList<Converter<?, ?>>();
        converterList.add(new ZonedDateTimeWriteConverter());
        converterList.add(new ZonedDateTimeReadConverter(localTimeZone));
        return new MongoCustomConversions(converterList);
    }

    @Override
    public MongoClient mongoClient() {
        if (user != null && !user.isEmpty()) {
            MongoClientSettings settings = null;
            MongoCredential credential = MongoCredential.createCredential(user, database, password.toCharArray());
            if (retryWrites != null && !retryWrites.trim().isEmpty()) {
                settings = MongoClientSettings.builder().credential(credential)
                        .retryWrites(Boolean.valueOf(retryWrites))
                        .applyToClusterSettings(
                                builder -> builder.hosts(Arrays.asList(new ServerAddress(host, Integer.valueOf(port)))))
                        .build();
            } else {
                settings = MongoClientSettings.builder().credential(credential)
                        .applyToClusterSettings(
                                builder -> builder.hosts(Arrays.asList(new ServerAddress(host, Integer.valueOf(port)))))
                        .build();
            }

            return MongoClients.create(settings);
        } else {
            MongoClientSettings settings = null;
            if (retryWrites != null && !retryWrites.trim().isEmpty()) {
                settings = MongoClientSettings.builder().retryWrites(Boolean.valueOf(retryWrites))
                        .applyToClusterSettings(
                                builder -> builder.hosts(Arrays.asList(new ServerAddress(host, Integer.valueOf(port)))))
                        .build();
            } else {
                settings = MongoClientSettings.builder()
                        .applyToClusterSettings(
                                builder -> builder.hosts(Arrays.asList(new ServerAddress(host, Integer.valueOf(port)))))
                        .build();
            }
            return MongoClients.create(settings);
        }
    }
}
