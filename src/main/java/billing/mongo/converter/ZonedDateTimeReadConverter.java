package billing.mongo.converter;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import billing.handler.impl.CancellationServiceImpl;

public class ZonedDateTimeReadConverter implements Converter<Date, ZonedDateTime> {

    public static final Logger logger = LogManager.getLogger(ZonedDateTimeReadConverter.class);

    private String timeZone;

    public ZonedDateTimeReadConverter(String timeZone) {
        super();
        this.timeZone = timeZone;
    }

    @Override
    public ZonedDateTime convert(Date date) {
        if (timeZone == null) {
            timeZone = "UTC";
            logger.info(timeZone);
        }
        return date.toInstant().atZone(ZoneId.of(timeZone));
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

}