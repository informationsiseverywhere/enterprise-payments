package billing.mongo.data.model;

public class DataMetadata {

    private int fontColor;
    private String fillColor;
    private int fontSize;
    private String font;
    private boolean bold;
    private boolean italic;
    private String underLineType;
    private int rowHeight;
    private boolean wrapText;

    public DataMetadata(int fontColor, String fillColor, int fontSize, String font, boolean bold, boolean italic,
            String underLineType, int rowHeight, boolean wrapText) {
        super();
        this.fontColor = fontColor;
        this.fillColor = fillColor;
        this.fontSize = fontSize;
        this.font = font;
        this.bold = bold;
        this.italic = italic;
        this.underLineType = underLineType;
        this.rowHeight = rowHeight;
        this.wrapText = wrapText;
    }

    public DataMetadata() {
        super();
    }

    public boolean isWrapText() {
        return wrapText;
    }

    public void setWrapText(boolean wrapText) {
        this.wrapText = wrapText;
    }

    public int getFontColor() {
        return fontColor;
    }

    public void setFontColor(int fontColor) {
        this.fontColor = fontColor;
    }

    public String getFillColor() {
        return fillColor;
    }

    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }

    public boolean isItalic() {
        return italic;
    }

    public void setItalic(boolean italic) {
        this.italic = italic;
    }

    public String getUnderLineType() {
        return underLineType;
    }

    public void setUnderLineType(String underLineType) {
        this.underLineType = underLineType;
    }

    public int getRowHeight() {
        return rowHeight;
    }

    public void setRowHeight(int rowHeight) {
        this.rowHeight = rowHeight;
    }

}
