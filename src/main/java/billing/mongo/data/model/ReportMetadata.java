package billing.mongo.data.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "reportMetadata")
public class ReportMetadata implements Serializable {

    @Id
    private String id;

    private static final long serialVersionUID = 1L;

    @Indexed
    private String metadataId;
    private String reportTitle;
    private String reportTitle1;
    private String reportTitle2;
    private List<String> reportHeader;
    private List<String> reportHeader1;
    private Map<String, String> reportProperties;
    private String reportName;
    private boolean landscapeOrientation;
    private boolean printGridLines;
    private int columnWidth;
    private Heading1Metadata heading1Metadata;
    private TitleMetadata titleMetadata;
    private PropertyMetadata propertyMetadata;
    private HeaderMetadata headerMetadata;
    private DataMetadata dataMetadata;
    private Map<String, Integer> individualColumnWidth;

    public ReportMetadata() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMetadataId() {
        return metadataId;
    }

    public void setMetadataId(String metadataId) {
        this.metadataId = metadataId;
    }

    public boolean isLandscapeOrientation() {
        return landscapeOrientation;
    }

    public void setLandscapeOrientation(boolean landscapeOrientation) {
        this.landscapeOrientation = landscapeOrientation;
    }

    public boolean isPrintGridLines() {
        return printGridLines;
    }

    public void setPrintGridLines(boolean printGridLines) {
        this.printGridLines = printGridLines;
    }

    public int getColumnWidth() {
        return columnWidth;
    }

    public void setColumnWidth(int columnWidth) {
        this.columnWidth = columnWidth;
    }

    public TitleMetadata getTitleMetadata() {
        return titleMetadata;
    }

    public void setTitleMetadata(TitleMetadata titleMetadata) {
        this.titleMetadata = titleMetadata;
    }

    public PropertyMetadata getPropertyMetadata() {
        return propertyMetadata;
    }

    public void setPropertyMetadata(PropertyMetadata propertyMetadata) {
        this.propertyMetadata = propertyMetadata;
    }

    public HeaderMetadata getHeaderMetadata() {
        return headerMetadata;
    }

    public void setHeaderMetadata(HeaderMetadata headerMetadata) {
        this.headerMetadata = headerMetadata;
    }

    public DataMetadata getDataMetadata() {
        return dataMetadata;
    }

    public void setDataMetadata(DataMetadata dataMetadata) {
        this.dataMetadata = dataMetadata;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public List<String> getReportHeader() {
        return reportHeader;
    }

    public void setReportHeader(List<String> reportHeader) {
        this.reportHeader = reportHeader;
    }

    public Map<String, String> getReportProperties() {
        return reportProperties;
    }

    public void setReportProperties(Map<String, String> reportProperties) {
        this.reportProperties = reportProperties;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public Map<String, Integer> getIndividualColumnWidth() {
        return individualColumnWidth;
    }

    public void setIndividualColumnWidth(Map<String, Integer> individualColumnWidth) {
        this.individualColumnWidth = individualColumnWidth;
    }

    public List<String> getReportHeader1() {
        return reportHeader1;
    }

    public void setReportHeader1(List<String> reportHeader1) {
        this.reportHeader1 = reportHeader1;
    }

    public String getReportTitle1() {
        return reportTitle1;
    }

    public void setReportTitle1(String reportTitle1) {
        this.reportTitle1 = reportTitle1;
    }

    public String getReportTitle2() {
        return reportTitle2;
    }

    public void setReportTitle2(String reportTitle2) {
        this.reportTitle2 = reportTitle2;
    }

    public Heading1Metadata getHeading1Metadata() {
        return heading1Metadata;
    }

    public void setHeading1Metadata(Heading1Metadata heading1Metadata) {
        this.heading1Metadata = heading1Metadata;
    }

}
