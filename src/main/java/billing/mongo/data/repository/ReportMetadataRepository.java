package billing.mongo.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import billing.mongo.data.model.ReportMetadata;

public interface ReportMetadataRepository extends MongoRepository<ReportMetadata, String> {

    ReportMetadata findBymetadataId(String string);
}
