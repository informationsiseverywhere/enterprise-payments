package billing.kafka.service;

import java.io.IOException;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.service.CashReceiptService;
import core.exception.service.ErrorService;


@Service
public class ConsumerService {
	
	private static final Logger logger = LoggerFactory.getLogger(ConsumerService.class);
	
	@Autowired
    private CashReceiptService cashReceiptService;
	
	@Autowired
	private ErrorService errorService;
	
	@KafkaListener(topics = "#{'${topic.name}'}", groupId = "#{'${consumer.groupId}'}")
	@Transactional
	public void consume(String message) throws IOException {
        logger.debug("bil cash entry tot row : {}", message);
        try {
            String[] paymentArray = message.split(";");
            cashReceiptService.processBatchSystem(paymentArray[0], paymentArray[1], paymentArray[2]);
        } catch (Exception ex) {
            //errorService.save(ex, ex.getMessage(), "Kafaka Failed");
            throw new RuntimeException(ex);
            
        }
       logger.error("process Completed"); 
        
    }

}
