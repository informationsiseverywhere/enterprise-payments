package disbursement.utils;

import core.utils.CommonConstants;

public class DisbursementConstants {

    public static final String BIL_BANK_CODE = "000";
    public static final String COMPANY_LOCATION_NUMBER = "00";
    public static final String MASTER_COMPANY_NUMBER = "99";
    public static final String DISBURSEMENT_RULE_CODE = "B";

    public enum DisbursementType {
        MANUAL("M"), SYSTEM("S"), ELECTRONIC("G"), CANCEL("C"), EMPTY_STRING(CommonConstants.BLANK_STRING);

        private final String disburseType;

        private DisbursementType(String disburseType) {
            this.disburseType = disburseType;
        }

        public String getDisburseType() {
            return disburseType;
        }

        public boolean hasValue(String value) {
            return value != null && disburseType.equals(value);
        }

        public static DisbursementType getEnumValue(String disbursementType) {
            for (DisbursementType disbursementMethod : values()) {
                if (disbursementMethod.disburseType.equalsIgnoreCase(disbursementType.trim())) {
                    return disbursementMethod;
                }
            }
            return EMPTY_STRING;
        }
    }

    public enum ElectronicInformation {
        BAC("BAC"), BPC("BPC"), BEF("BEF"), BCC("BCC"), BEC("BEC"), BAS("BAS");

        private final String electronicInfo;

        private ElectronicInformation(String electronicInfo) {
            this.electronicInfo = electronicInfo;
        }

        public String getElectronicInformation() {
            return electronicInfo;
        }
    }

    public enum DisbursementMediumCode {

        ALLMEDIUMS("ALL"),
        CHECK("C"),
        DRAFT("D"),
        ELECTRONICFUNDTRANSFER("E"),
        CREDITCARD("EC"),
        AUTOMATEDCLEARINGHOUSE("EP"),
        BANKACCOUNT("EA"),
        MANUALCHECK("MC"),
        MANUALDRAFT("MD"),
        WIRETRANSFERCHECK("WC"),
        WIRETRANSFERDRAFT("WD"),
        WIRE("W");

        private final String type;

        private DisbursementMediumCode(String type) {
            this.type = type;
        }

        public boolean hasValue(String value) {
            return value != null && type.equals(value);
        }

        @Override
        public String toString() {
            return this.type;
        }
    }

    public enum UsageCode {
        MANUAL_ALLOCATED("MA"), OFFSET_USAGE("O"), DISBURSED("D");

        private String usageCd;

        private UsageCode(String usageCd) {
            this.usageCd = usageCd;
        }

        public String getUsageCd() {
            return usageCd;
        }

    }

    public enum DisbursementStatusCode {

        CANCEL('C'),
        DISBURSED('D'),
        ESCHEATED('E'),
        HOLD('H'),
        COMBINEDPAYMENT('I'),
        OFFSETFORONSET('O'),
        PENDING('P'),
        SELECTCOMBINED('Q'),
        STOPPAYREQUEST('R'),
        STOPPAY('S'),
        HONORED('U'),
        VOIDNOREPRINT('V'),
        ESCHEATWAIT('W');

        private final char type;

        private DisbursementStatusCode(char type) {
            this.type = type;
        }

        public boolean hasValue(char value) {
            return type == value;
        }

        @Override
        public String toString() {
            return String.valueOf(this.type);
        }

        public char toChar() {
            return this.type;
        }
    }

    public enum DisbursementTypeClientCode {

        PAYEE("P"), MAILTO("M"), AGENCY("AG"), ADJUSTER("AD"), CLAIMANT("C");

        private final String type;

        private DisbursementTypeClientCode(String type) {
            this.type = type;
        }

        public boolean hasValue(String value) {
            return value != null && type.equals(value);
        }

        @Override
        public String toString() {
            return this.type;
        }
    }

    public enum TextTypeCode {
        USER_MEMO("U"), STANDARD_MEMO("S"), REASON("R");

        private final String textType;

        private TextTypeCode(String textType) {
            this.textType = textType;
        }

        public boolean hasValue(String value) {
            return textType.equals(value);
        }

        @Override
        public String toString() {
            return this.textType;
        }

    }

    public enum DisbursementIdType {
        BEFM("BEFM"), BACM("BACM"), BCCM("BCCM"), BPCM("BPCM"), BCMS("BCMS"), BEFT("BEFT"), BACH("BACH"), BASM("BASM");

        private final String disbursementIdTyp;

        private DisbursementIdType(String disbursementIdTyp) {
            this.disbursementIdTyp = disbursementIdTyp;
        }

        public String getDisbursementIdType() {
            return disbursementIdTyp;
        }
    }

    public enum DisbursementTypeCode {
        ADDITIONAL_REQUEST("AD"), STOP_REQUEST("R");

        private final String type;

        private DisbursementTypeCode(String type) {
            this.type = type;
        }

        public String getValue() {
            return this.type;
        }
    }
}
