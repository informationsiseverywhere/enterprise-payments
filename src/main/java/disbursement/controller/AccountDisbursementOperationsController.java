package disbursement.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import core.security.annotation.UserAuthorization;
import disbursement.model.DisbursementOperation;
import disbursement.model.DisbursementOperationList;
import disbursement.service.AccountDisbursementOperationsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/billing/disbursements")
@Api(tags = { "Account Disbursement Operations" }, description = "Account Disbursement Operations")
public class AccountDisbursementOperationsController {

    @Autowired
    private AccountDisbursementOperationsService accountDisbursementOperationsService;

    @UserAuthorization("postUnidentifiedAccountDisbursementStatusUpdate")
    @PostMapping(value = "/_statusUpdate")
    @ApiOperation(value = "Post Unidentified Account Disbursement Status Update")
    public HttpEntity<DisbursementOperation> postBillAccountDisbursementStatusUpdate(
            @RequestBody DisbursementOperationList disbursementOperationList) {
        accountDisbursementOperationsService.statusUpdate(disbursementOperationList.getDisbursementOperationList());
        DisbursementOperation saveDisbursementOperation = new DisbursementOperation();
        saveDisbursementOperation.add(linkTo(methodOn(AccountDisbursementOperationsController.class)
                .postBillAccountDisbursementStatusUpdate(disbursementOperationList)).withSelfRel().expand());
        return new ResponseEntity<>(saveDisbursementOperation, HttpStatus.CREATED);
    }

    @UserAuthorization("postUnidentifiedAccountDisbursementCombinedPayment")
    @PostMapping(value = "/{disbursementId}/_deleteFromCombinedPayment")
    @ApiOperation(value = "Post Unidentified Account Disbursement Payment Delete From Combine Payment")
    public HttpEntity<DisbursementOperation> postBillAccountDisbursementCombinedPayment(
            @PathVariable("disbursementId") String disbursementId) {
        accountDisbursementOperationsService.paymentDeleteFromCombinedPayment(disbursementId);
        DisbursementOperation disbursementOperation = new DisbursementOperation();
        disbursementOperation.add(linkTo(methodOn(AccountDisbursementOperationsController.class)
                .postBillAccountDisbursementCombinedPayment(disbursementId)).withSelfRel().expand());
        return new ResponseEntity<>(disbursementOperation, HttpStatus.CREATED);
    }
}
