package disbursement.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import disbursement.data.entity.id.DwsIvtControlId;

@Entity
@Table(name = "DWS_IVT_CONTROL")
public class DwsIvtControl implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DwsIvtControlId dwsIvtControlId;

    @Column(name = "DWS_NEXT_CK_NBR", length = 10, nullable = true)
    private Integer dwsNextCkNbr;

    @Column(name = "DWS_CUR_BEG_CK_NBR", length = 10, nullable = true)
    private Integer dwsCurBegCkNbr;

    @Column(name = "DWS_CUR_EN_CK_NBR", length = 10, nullable = true)
    private Integer dwsCurEnCkNbr;

    @Column(name = "DWS_CUR_RGE_ENT_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime dwsCurRgeEntDt;

    @Column(name = "DWS_NXT_BEG_CK_NBR", length = 10, nullable = true)
    private Integer dwsNxtBegCkNbr;

    @Column(name = "DWS_NXT_EN_CK_NBR", length = 10, nullable = true)
    private Integer dwsNxtEnCkNbr;

    @Column(name = "DWS_NXT_RGE_ENT_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime dwsNxtRgeEntDt;

    @Column(name = "DWS_MIN_CK_QTY", length = 10, nullable = false)
    private Integer dwsMinCkQty;

    @Column(name = "DWS_HST_VLD_NBR", length = 5, nullable = false)
    private Integer dwsHstVldNbr;

    @Column(name = "DWS_EXPIRATION_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime dwsExpirationTs;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    public DwsIvtControlId getDwsIvtControlId() {
        return dwsIvtControlId;
    }

    public void setDwsIvtControlId(DwsIvtControlId dwsIvtControlId) {
        this.dwsIvtControlId = dwsIvtControlId;
    }

    public Integer getDwsNextCkNbr() {
        return dwsNextCkNbr;
    }

    public void setDwsNextCkNbr(Integer dwsNextCkNbr) {
        this.dwsNextCkNbr = dwsNextCkNbr;
    }

    public Integer getDwsCurBegCkNbr() {
        return dwsCurBegCkNbr;
    }

    public void setDwsCurBegCkNbr(Integer dwsCurBegCkNbr) {
        this.dwsCurBegCkNbr = dwsCurBegCkNbr;
    }

    public Integer getDwsCurEnCkNbr() {
        return dwsCurEnCkNbr;
    }

    public void setDwsCurEnCkNbr(Integer dwsCurEnCkNbr) {
        this.dwsCurEnCkNbr = dwsCurEnCkNbr;
    }

    public ZonedDateTime getDwsCurRgeEntDt() {
        return dwsCurRgeEntDt;
    }

    public void setDwsCurRgeEntDt(ZonedDateTime dwsCurRgeEntDt) {
        this.dwsCurRgeEntDt = dwsCurRgeEntDt;
    }

    public Integer getDwsNxtBegCkNbr() {
        return dwsNxtBegCkNbr;
    }

    public void setDwsNxtBegCkNbr(Integer dwsNxtBegCkNbr) {
        this.dwsNxtBegCkNbr = dwsNxtBegCkNbr;
    }

    public Integer getDwsNxtEnCkNbr() {
        return dwsNxtEnCkNbr;
    }

    public void setDwsNxtEnCkNbr(Integer dwsNxtEnCkNbr) {
        this.dwsNxtEnCkNbr = dwsNxtEnCkNbr;
    }

    public ZonedDateTime getDwsNxtRgeEntDt() {
        return dwsNxtRgeEntDt;
    }

    public void setDwsNxtRgeEntDt(ZonedDateTime dwsNxtRgeEntDt) {
        this.dwsNxtRgeEntDt = dwsNxtRgeEntDt;
    }

    public Integer getDwsMinCkQty() {
        return dwsMinCkQty;
    }

    public void setDwsMinCkQty(Integer dwsMinCkQty) {
        this.dwsMinCkQty = dwsMinCkQty;
    }

    public Integer getDwsHstVldNbr() {
        return dwsHstVldNbr;
    }

    public void setDwsHstVldNbr(Integer dwsHstVldNbr) {
        this.dwsHstVldNbr = dwsHstVldNbr;
    }

    public ZonedDateTime getDwsExpirationTs() {
        return dwsExpirationTs;
    }

    public void setDwsExpirationTs(ZonedDateTime dwsExpirationTs) {
        this.dwsExpirationTs = dwsExpirationTs;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
