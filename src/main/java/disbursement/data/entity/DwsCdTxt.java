package disbursement.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import disbursement.data.entity.id.DwsCdTxtId;

@Entity
@Table(name = "DWS_CD_TXT")
public class DwsCdTxt implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DwsCdTxtId dwsCdTxtId;

    @Column(name = "DCT_CD_DES", length = 80, nullable = false)
    private String dctCdDes = SEPARATOR_BLANK;

    public DwsCdTxtId getDwsCdTxtId() {
        return dwsCdTxtId;
    }

    public void setDwsCdTxtId(DwsCdTxtId dwsCdTxtId) {
        this.dwsCdTxtId = dwsCdTxtId;
    }

    public String getDctCdDes() {
        return dctCdDes;
    }

    public void setDctCdDes(String dctCdDes) {
        this.dctCdDes = dctCdDes;
    }
}
