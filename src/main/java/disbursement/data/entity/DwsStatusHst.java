package disbursement.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import disbursement.data.entity.id.DwsStatusHstId;

@Entity
@Table(name = "DWS_STATUS_HST")
public class DwsStatusHst implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DwsStatusHstId dwsStatusHstId;

    @Column(name = "DWS_PRE_STATUS_CD", nullable = false)
    private Character dwsPreStatusCd;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    public DwsStatusHstId getDwsStatusHstId() {
        return dwsStatusHstId;
    }

    public void setDwsStatusHstId(DwsStatusHstId dwsStatusHstId) {
        this.dwsStatusHstId = dwsStatusHstId;
    }

    public Character getDwsPreStatusCd() {
        return dwsPreStatusCd;
    }

    public void setDwsPreStatusCd(Character dwsPreStatusCd) {
        this.dwsPreStatusCd = dwsPreStatusCd;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
