package disbursement.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import disbursement.data.entity.id.DwsLocationCnvId;

@Entity
@Table(name = "DWS_LOCATION_CNV")
public class DwsLocationCnv implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DwsLocationCnvId dwsLocationCnvId;

    @Column(name = "DWS_PRINT_LOC_CD", length = 3, nullable = true)
    private String dwsPrintLocCd = SEPARATOR_BLANK;

    public DwsLocationCnvId getDwsLocationCnvId() {
        return dwsLocationCnvId;
    }

    public void setDwsLocationCnvId(DwsLocationCnvId dwsLocationCnvId) {
        this.dwsLocationCnvId = dwsLocationCnvId;
    }

    public String getDwsPrintLocCd() {
        return dwsPrintLocCd;
    }

    public void setDwsPrintLocCd(String dwsPrintLocCd) {
        this.dwsPrintLocCd = dwsPrintLocCd;
    }

}
