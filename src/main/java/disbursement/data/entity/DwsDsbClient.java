package disbursement.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import disbursement.data.entity.id.DwsDsbClientId;

@Entity
@Table(name = "DWS_DSB_CLIENT")
public class DwsDsbClient implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DwsDsbClientId dwsDsbClientId;

    @Column(name = "CLIENT_ID", length = 20, nullable = false)
    private String clientId = SEPARATOR_BLANK;

    @Column(name = "ADR_SEQ_NBR", length = 5, nullable = false)
    private Integer adrSeqNbr;

    public DwsDsbClientId getDwsDsbClientId() {
        return dwsDsbClientId;
    }

    public void setDwsDsbClientId(DwsDsbClientId dwsDsbClientId) {
        this.dwsDsbClientId = dwsDsbClientId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Integer getAdrSeqNbr() {
        return adrSeqNbr;
    }

    public void setAdrSeqNbr(Integer adrSeqNbr) {
        this.adrSeqNbr = adrSeqNbr;
    }

}
