package disbursement.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class DwsIvtDetailId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "DWS_PRC_LOC_CD", length = 3, nullable = false)
    private String dwsPrcLocCd = SEPARATOR_BLANK;

    @Column(name = "DWS_MEDIUM_CD", length = 2, nullable = false)
    private String dwsMediumCd = SEPARATOR_BLANK;

    @Column(name = "DWS_CK_DRF_NBR", length = 10, nullable = false)
    private Integer dwsCkDrfNbr;

    @Column(name = "DWS_BANK_NBR", length = 5, nullable = false)
    private Integer dwsBankNbr;

    @Column(name = "DWS_BANK_ACT_NBR", length = 15, nullable = false)
    private String dwsBankActNbr = SEPARATOR_BLANK;

    @Column(name = "DWS_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime dwsAcyTs;

    public DwsIvtDetailId() {
        super();
    }

    public DwsIvtDetailId(String dwsPrcLocCd, String dwsMediumCd, Integer dwsCkDrfNbr, Integer dwsBankNbr,
            String dwsBankActNbr, ZonedDateTime dwsAcyTs) {
        super();
        this.dwsPrcLocCd = dwsPrcLocCd;
        this.dwsMediumCd = dwsMediumCd;
        this.dwsCkDrfNbr = dwsCkDrfNbr;
        this.dwsBankNbr = dwsBankNbr;
        this.dwsBankActNbr = dwsBankActNbr;
        this.dwsAcyTs = dwsAcyTs;
    }

    public String getDwsPrcLocCd() {
        return dwsPrcLocCd;
    }

    public void setDwsPrcLocCd(String dwsPrcLocCd) {
        this.dwsPrcLocCd = dwsPrcLocCd;
    }

    public String getDwsMediumCd() {
        return dwsMediumCd;
    }

    public void setDwsMediumCd(String dwsMediumCd) {
        this.dwsMediumCd = dwsMediumCd;
    }

    public Integer getDwsCkDrfNbr() {
        return dwsCkDrfNbr;
    }

    public void setDwsCkDrfNbr(Integer dwsCkDrfNbr) {
        this.dwsCkDrfNbr = dwsCkDrfNbr;
    }

    public Integer getDwsBankNbr() {
        return dwsBankNbr;
    }

    public void setDwsBankNbr(Integer dwsBankNbr) {
        this.dwsBankNbr = dwsBankNbr;
    }

    public String getDwsBankActNbr() {
        return dwsBankActNbr;
    }

    public void setDwsBankActNbr(String dwsBankActNbr) {
        this.dwsBankActNbr = dwsBankActNbr;
    }

    public ZonedDateTime getDwsAcyTs() {
        return dwsAcyTs;
    }

    public void setDwsAcyTs(ZonedDateTime dwsAcyTs) {
        this.dwsAcyTs = dwsAcyTs;
    }

}
