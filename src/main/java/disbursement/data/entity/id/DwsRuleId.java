package disbursement.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DwsRuleId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "RULE_TYPE_CD", length = 8, nullable = false)
    private String ruleTypeCd = SEPARATOR_BLANK;

    @Column(name = "DRT_CODE1", length = 4, nullable = false)
    private String drtCode1 = SEPARATOR_BLANK;

    @Column(name = "DRT_CODE2", length = 4, nullable = false)
    private String drtCode2 = SEPARATOR_BLANK;

    public DwsRuleId() {
        super();
    }

    public DwsRuleId(String ruleTypeCd, String drtCode1, String drtCode2) {
        super();
        this.ruleTypeCd = ruleTypeCd;
        this.drtCode1 = drtCode1;
        this.drtCode2 = drtCode2;
    }

    public String getRuleTypeCd() {
        return ruleTypeCd;
    }

    public void setRuleTypeCd(String ruleTypeCd) {
        this.ruleTypeCd = ruleTypeCd;
    }

    public String getDrtCode1() {
        return drtCode1;
    }

    public void setDrtCode1(String drtCode1) {
        this.drtCode1 = drtCode1;
    }

    public String getDrtCode2() {
        return drtCode2;
    }

    public void setDrtCode2(String drtCode2) {
        this.drtCode2 = drtCode2;
    }

}
