package disbursement.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DwsTxtId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "DWS_DSB_ID", length = 21, nullable = false)
    private String dwsDsbId = SEPARATOR_BLANK;

    @Column(name = "DTT_TXT_TYP_CD", length = 3, nullable = false)
    private String dttTxtTypCd = SEPARATOR_BLANK;

    public DwsTxtId() {
        super();
    }

    public DwsTxtId(String dwsDsbId, String dttTxtTypCd) {
        super();
        this.dwsDsbId = dwsDsbId;
        this.dttTxtTypCd = dttTxtTypCd;
    }

    public String getDwsDsbId() {
        return dwsDsbId;
    }

    public void setDwsDsbId(String dwsDsbId) {
        this.dwsDsbId = dwsDsbId;
    }

    public String getDttTxtTypCd() {
        return dttTxtTypCd;
    }

    public void setDttTxtTypCd(String dttTxtTypCd) {
        this.dttTxtTypCd = dttTxtTypCd;
    }

}
