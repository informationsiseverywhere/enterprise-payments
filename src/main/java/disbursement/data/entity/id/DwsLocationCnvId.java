package disbursement.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DwsLocationCnvId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "DWS_PRC_LOC_CD", length = 3, nullable = false)
    private String dwsPrcLocCd = SEPARATOR_BLANK;

    @Column(name = "DWS_MEDIUM_CD", length = 2, nullable = false)
    private String dwsMediumCd = SEPARATOR_BLANK;

    @Column(name = "DWS_DSB_TYP_CD", length = 3, nullable = false)
    private String dwsDspTypCd = SEPARATOR_BLANK;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNbr = SEPARATOR_BLANK;

    @Column(name = "RISK_STATE_CD", length = 3, nullable = false)
    private String riskStateCd = SEPARATOR_BLANK;

    @Column(name = "LOB_CD", length = 3, nullable = false)
    private String lobCd = SEPARATOR_BLANK;

    public DwsLocationCnvId() {
        super();
    }

    public DwsLocationCnvId(String dwsPrcLocCd, String dwsMediumCd, String dwsDspTypCd, String masterCompanyNbr,
            String riskStateCd, String lobCd) {
        super();
        this.dwsPrcLocCd = dwsPrcLocCd;
        this.dwsMediumCd = dwsMediumCd;
        this.dwsDspTypCd = dwsDspTypCd;
        this.masterCompanyNbr = masterCompanyNbr;
        this.riskStateCd = riskStateCd;
        this.lobCd = lobCd;
    }

    public String getDwsPrcLocCd() {
        return dwsPrcLocCd;
    }

    public void setDwsPrcLocCd(String dwsPrcLocCd) {
        this.dwsPrcLocCd = dwsPrcLocCd;
    }

    public String getDwsMediumCd() {
        return dwsMediumCd;
    }

    public void setDwsMediumCd(String dwsMediumCd) {
        this.dwsMediumCd = dwsMediumCd;
    }

    public String getDwsDspTypCd() {
        return dwsDspTypCd;
    }

    public void setDwsDspTypCd(String dwsDspTypCd) {
        this.dwsDspTypCd = dwsDspTypCd;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public String getRiskStateCd() {
        return riskStateCd;
    }

    public void setRiskStateCd(String riskStateCd) {
        this.riskStateCd = riskStateCd;
    }

    public String getLobCd() {
        return lobCd;
    }

    public void setLobCd(String lobCd) {
        this.lobCd = lobCd;
    }

}
