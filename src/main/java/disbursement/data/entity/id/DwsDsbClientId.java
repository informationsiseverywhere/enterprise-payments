package disbursement.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DwsDsbClientId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "DWS_DSB_ID", length = 21, nullable = false)
    private String dwsDsbId = SEPARATOR_BLANK;

    @Column(name = "DDC_TYP_CLT_CD", length = 3, nullable = false)
    private String ddcTypCltCd = SEPARATOR_BLANK;

    @Column(name = "DDC_SEQ_NBR", length = 5, nullable = false)
    private Integer ddcSeqNbr;

    public DwsDsbClientId() {
        super();
    }

    public DwsDsbClientId(String dwsDsbId, String ddcTypCltCd, Integer ddcSeqNbr) {
        super();
        this.dwsDsbId = dwsDsbId;
        this.ddcTypCltCd = ddcTypCltCd;
        this.ddcSeqNbr = ddcSeqNbr;
    }

    public String getDwsDsbId() {
        return dwsDsbId;
    }

    public void setDwsDsbId(String dwsDsbId) {
        this.dwsDsbId = dwsDsbId;
    }

    public String getDdcTypCltCd() {
        return ddcTypCltCd;
    }

    public void setDdcTypCltCd(String ddcTypCltCd) {
        this.ddcTypCltCd = ddcTypCltCd;
    }

    public Integer getDdcSeqNbr() {
        return ddcSeqNbr;
    }

    public void setDdcSeqNbr(Integer ddcSeqNbr) {
        this.ddcSeqNbr = ddcSeqNbr;
    }

}
