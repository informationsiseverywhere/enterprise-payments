package disbursement.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

public class DwsStatusHstId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "DWS_DSB_ID", length = 21, nullable = false)
    private String dwsDsbId = SEPARATOR_BLANK;

    @Column(name = "DSH_CHANGE_TS", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime dshChangeTs;

    public DwsStatusHstId(String dwsDsbId, ZonedDateTime dshChangeTs) {
        super();
        this.dwsDsbId = dwsDsbId;
        this.dshChangeTs = dshChangeTs;
    }

    public DwsStatusHstId() {
        super();
    }

    public String getDwsDsbId() {
        return dwsDsbId;
    }

    public void setDwsDsbId(String dwsDsbId) {
        this.dwsDsbId = dwsDsbId;
    }

    public ZonedDateTime getDshChangeTs() {
        return dshChangeTs;
    }

    public void setDshChangeTs(ZonedDateTime dshChangeTs) {
        this.dshChangeTs = dshChangeTs;
    }

}
