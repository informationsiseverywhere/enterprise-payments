package disbursement.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DwsCdTxtId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "DCT_CD_TYP", length = 8, nullable = false)
    private String dctCdType = SEPARATOR_BLANK;

    @Column(name = "DCT_CD", length = 3, nullable = false)
    private String dctCd = SEPARATOR_BLANK;

    public String getDctCdType() {
        return dctCdType;
    }

    public void setDctCdType(String dctCdType) {
        this.dctCdType = dctCdType;
    }

    public String getDctCd() {
        return dctCd;
    }

    public void setDctCd(String dctCd) {
        this.dctCd = dctCd;
    }

    public DwsCdTxtId(String dctCdType, String dctCd) {
        super();
        this.dctCdType = dctCdType;
        this.dctCd = dctCd;
    }

    public DwsCdTxtId() {
        super();
    }

}
