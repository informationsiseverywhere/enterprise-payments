package disbursement.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "DWS_DISBURSEMENT")
public class DwsDisbursement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "DWS_DSB_ID", length = 21, nullable = false)
    private String dwsDsbId = SEPARATOR_BLANK;

    @Column(name = "DDS_DSB_TS", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime ddsDsbTs;

    @Column(name = "DDS_OGN_TS", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime ddsOgnTs;

    @Column(name = "BUS_UNIT_ID", length = 10, nullable = false)
    private Integer busUnitId;

    @Column(name = "DWS_DSB_TYP_CD", length = 3, nullable = false)
    private String dwsDsbTypeCd = SEPARATOR_BLANK;

    @Column(name = "DDS_DSB_AMT", precision = 15, scale = 2, nullable = false)
    private double ddsDspAmt;

    @Column(name = "DDS_DSB_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ddsDsbDt;

    @Column(name = "DDS_SCH_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ddsSchDt;

    @Column(name = "DWS_STATUS_CD", nullable = false)
    private Character dwsStatusCd;

    @Column(name = "DWS_INTF_BATCH_ID", length = 20, nullable = false)
    private String dwsIntfBatchId = SEPARATOR_BLANK;

    @Column(name = "DDS_CURRENCY_CD", length = 4, nullable = false)
    private String ddsCurrencyCd = SEPARATOR_BLANK;

    @Column(name = "DWS_CK_DRF_NBR", length = 10, nullable = true)
    private Integer dwsCkDrfNbr;

    @Column(name = "DDS_SPE_HDL_CD", length = 3, nullable = true)
    private String ddsSpeHdlCd = SEPARATOR_BLANK;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "DWS_BANK_NBR", length = 5, nullable = false)
    private Integer dwsBankNbr;

    @Column(name = "DWS_BANK_ACT_NBR", length = 15, nullable = false)
    private String dwsBankActNbr = SEPARATOR_BLANK;

    @Column(name = "DDS_GROUP_CK_IND", nullable = false)
    private Character ddsGroupCkInd;

    @Column(name = "DWS_PRC_LOC_CD", length = 3, nullable = true)
    private String dwsPrcLocCd = SEPARATOR_BLANK;

    @Column(name = "DDS_USR_DSB_ID", length = 40, nullable = false)
    private String ddsUsrDsbId = SEPARATOR_BLANK;

    @Column(name = "DDS_USR_POL_NBR", length = 25, nullable = false)
    private String ddsUsrPolNbr = SEPARATOR_BLANK;

    @Column(name = "CIP_POL_STATE_CD", length = 3, nullable = false)
    private String cipPolStateCd = SEPARATOR_BLANK;

    @Column(name = "DDS_CAR_RGS_NBR", length = 12, nullable = false)
    private String ddsCarRgsNbr = SEPARATOR_BLANK;

    @Column(name = "DWS_MEDIUM_CD", length = 2, nullable = false)
    private String dwsMediumCd = SEPARATOR_BLANK;

    @Column(name = "DWS_MKT_SCT_CD", length = 2, nullable = false)
    private String dwsMktSctCd = SEPARATOR_BLANK;

    @Column(name = "DWS_DST_OFFICE_NM", length = 15, nullable = false)
    private String dwsDstOfficeNm = SEPARATOR_BLANK;

    @Column(name = "DWS_LIN_OF_BUS_CD", length = 3, nullable = false)
    private String dwsLinOfBusCd = SEPARATOR_BLANK;

    @Column(name = "DWS_PRODUCT_CD", length = 3, nullable = false)
    private String dwsProductCd = SEPARATOR_BLANK;

    @Column(name = "DWS_REFERENCE_NBR", length = 15, nullable = false)
    private String dwsReferenceNbr = SEPARATOR_BLANK;

    @Column(name = "DWS_INVOICE_NBR", length = 15, nullable = false)
    private String dwsInvoiceNbr = SEPARATOR_BLANK;

    @Column(name = "DDS_PMT_TRACE_NBR", length = 10, nullable = false)
    private Integer ddsPmtTraceNbr;

    @Column(name = "DDS_SCH_RUL_TYP_CD", nullable = false)
    private Character ddsSchRulTypCd;

    @Column(name = "DDS_CMB_PMT_CD", nullable = false)
    private Character ddsCmbPmtCd;

    @Column(name = "DDS_SERVICE_FRM_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ddsServiceFrmDt;

    @Column(name = "DDS_SERVICE_TO_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ddsServiceToDt;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNbr = SEPARATOR_BLANK;

    @Column(name = "RISK_STATE_CD", length = 3, nullable = true)
    private String riskStateCd = SEPARATOR_BLANK;

    @Column(name = "DWS_PRINT_LOC_CD", length = 3, nullable = true)
    private String dwsPrintLocCd = SEPARATOR_BLANK;

    public String getDwsDsbId() {
        return dwsDsbId;
    }

    public void setDwsDsbId(String dwsDsbId) {
        this.dwsDsbId = dwsDsbId;
    }

    public ZonedDateTime getDdsDsbTs() {
        return ddsDsbTs;
    }

    public void setDdsDsbTs(ZonedDateTime ddsDsbTs) {
        this.ddsDsbTs = ddsDsbTs;
    }

    public ZonedDateTime getDdsOgnTs() {
        return ddsOgnTs;
    }

    public void setDdsOgnTs(ZonedDateTime ddsOgnTs) {
        this.ddsOgnTs = ddsOgnTs;
    }

    public Integer getBusUnitId() {
        return busUnitId;
    }

    public void setBusUnitId(Integer busUnitId) {
        this.busUnitId = busUnitId;
    }

    public String getDwsDsbTypeCd() {
        return dwsDsbTypeCd;
    }

    public void setDwsDsbTypeCd(String dwsDsbTypeCd) {
        this.dwsDsbTypeCd = dwsDsbTypeCd;
    }

    public double getDdsDspAmt() {
        return ddsDspAmt;
    }

    public void setDdsDspAmt(double ddsDspAmt) {
        this.ddsDspAmt = ddsDspAmt;
    }

    public ZonedDateTime getDdsDsbDt() {
        return ddsDsbDt;
    }

    public void setDdsDsbDt(ZonedDateTime ddsDsbDt) {
        this.ddsDsbDt = ddsDsbDt;
    }

    public ZonedDateTime getDdsSchDt() {
        return ddsSchDt;
    }

    public void setDdsSchDt(ZonedDateTime ddsSchDt) {
        this.ddsSchDt = ddsSchDt;
    }

    public Character getDwsStatusCd() {
        return dwsStatusCd;
    }

    public void setDwsStatusCd(Character dwsStatusCd) {
        this.dwsStatusCd = dwsStatusCd;
    }

    public String getDwsIntfBatchId() {
        return dwsIntfBatchId;
    }

    public void setDwsIntfBatchId(String dwsIntfBatchId) {
        this.dwsIntfBatchId = dwsIntfBatchId;
    }

    public String getDdsCurrencyCd() {
        return ddsCurrencyCd;
    }

    public void setDdsCurrencyCd(String ddsCurrencyCd) {
        this.ddsCurrencyCd = ddsCurrencyCd;
    }

    public Integer getDwsCkDrfNbr() {
        return dwsCkDrfNbr;
    }

    public void setDwsCkDrfNbr(Integer dwsCkDrfNbr) {
        this.dwsCkDrfNbr = dwsCkDrfNbr;
    }

    public String getDdsSpeHdlCd() {
        return ddsSpeHdlCd;
    }

    public void setDdsSpeHdlCd(String ddsSpeHdlCd) {
        this.ddsSpeHdlCd = ddsSpeHdlCd;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getDwsBankNbr() {
        return dwsBankNbr;
    }

    public void setDwsBankNbr(Integer dwsBankNbr) {
        this.dwsBankNbr = dwsBankNbr;
    }

    public String getDwsBankActNbr() {
        return dwsBankActNbr;
    }

    public void setDwsBankActNbr(String dwsBankActNbr) {
        this.dwsBankActNbr = dwsBankActNbr;
    }

    public Character getDdsGroupCkInd() {
        return ddsGroupCkInd;
    }

    public void setDdsGroupCkInd(Character ddsGroupCkInd) {
        this.ddsGroupCkInd = ddsGroupCkInd;
    }

    public String getDwsPrcLocCd() {
        return dwsPrcLocCd;
    }

    public void setDwsPrcLocCd(String dwsPrcLocCd) {
        this.dwsPrcLocCd = dwsPrcLocCd;
    }

    public String getDdsUsrDsbId() {
        return ddsUsrDsbId;
    }

    public void setDdsUsrDsbId(String ddsUsrDsbId) {
        this.ddsUsrDsbId = ddsUsrDsbId;
    }

    public String getDdsUsrPolNbr() {
        return ddsUsrPolNbr;
    }

    public void setDdsUsrPolNbr(String ddsUsrPolNbr) {
        this.ddsUsrPolNbr = ddsUsrPolNbr;
    }

    public String getCipPolStateCd() {
        return cipPolStateCd;
    }

    public void setCipPolStateCd(String cipPolStateCd) {
        this.cipPolStateCd = cipPolStateCd;
    }

    public String getDdsCarRgsNbr() {
        return ddsCarRgsNbr;
    }

    public void setDdsCarRgsNbr(String ddsCarRgsNbr) {
        this.ddsCarRgsNbr = ddsCarRgsNbr;
    }

    public String getDwsMediumCd() {
        return dwsMediumCd;
    }

    public void setDwsMediumCd(String dwsMediumCd) {
        this.dwsMediumCd = dwsMediumCd;
    }

    public String getDwsMktSctCd() {
        return dwsMktSctCd;
    }

    public void setDwsMktSctCd(String dwsMktSctCd) {
        this.dwsMktSctCd = dwsMktSctCd;
    }

    public String getDwsDstOfficeNm() {
        return dwsDstOfficeNm;
    }

    public void setDwsDstOfficeNm(String dwsDstOfficeNm) {
        this.dwsDstOfficeNm = dwsDstOfficeNm;
    }

    public String getDwsLinOfBusCd() {
        return dwsLinOfBusCd;
    }

    public void setDwsLinOfBusCd(String dwsLinOfBusCd) {
        this.dwsLinOfBusCd = dwsLinOfBusCd;
    }

    public String getDwsProductCd() {
        return dwsProductCd;
    }

    public void setDwsProductCd(String dwsProductCd) {
        this.dwsProductCd = dwsProductCd;
    }

    public String getDwsReferenceNbr() {
        return dwsReferenceNbr;
    }

    public void setDwsReferenceNbr(String dwsReferenceNbr) {
        this.dwsReferenceNbr = dwsReferenceNbr;
    }

    public String getDwsInvoiceNbr() {
        return dwsInvoiceNbr;
    }

    public void setDwsInvoiceNbr(String dwsInvoiceNbr) {
        this.dwsInvoiceNbr = dwsInvoiceNbr;
    }

    public Integer getDdsPmtTraceNbr() {
        return ddsPmtTraceNbr;
    }

    public void setDdsPmtTraceNbr(Integer ddsPmtTraceNbr) {
        this.ddsPmtTraceNbr = ddsPmtTraceNbr;
    }

    public Character getDdsSchRulTypCd() {
        return ddsSchRulTypCd;
    }

    public void setDdsSchRulTypCd(Character ddsSchRulTypCd) {
        this.ddsSchRulTypCd = ddsSchRulTypCd;
    }

    public Character getDdsCmbPmtCd() {
        return ddsCmbPmtCd;
    }

    public void setDdsCmbPmtCd(Character ddsCmbPmtCd) {
        this.ddsCmbPmtCd = ddsCmbPmtCd;
    }

    public ZonedDateTime getDdsServiceFrmDt() {
        return ddsServiceFrmDt;
    }

    public void setDdsServiceFrmDt(ZonedDateTime ddsServiceFrmDt) {
        this.ddsServiceFrmDt = ddsServiceFrmDt;
    }

    public ZonedDateTime getDdsServiceToDt() {
        return ddsServiceToDt;
    }

    public void setDdsServiceToDt(ZonedDateTime ddsServiceToDt) {
        this.ddsServiceToDt = ddsServiceToDt;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public String getRiskStateCd() {
        return riskStateCd;
    }

    public void setRiskStateCd(String riskStateCd) {
        this.riskStateCd = riskStateCd;
    }

    public String getDwsPrintLocCd() {
        return dwsPrintLocCd;
    }

    public void setDwsPrintLocCd(String dwsPrintLocCd) {
        this.dwsPrintLocCd = dwsPrintLocCd;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
