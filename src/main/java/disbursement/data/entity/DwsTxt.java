package disbursement.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import disbursement.data.entity.id.DwsTxtId;

@Entity
@Table(name = "DWS_TXT")
public class DwsTxt implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DwsTxtId dwsTxtId;

    @Column(name = "DTT_DES_TXT_CD", length = 10, nullable = false)
    private String dttDesTxtCd = SEPARATOR_BLANK;

    @Column(name = "DTT_DES_TXT", length = 190, nullable = false)
    private String dttDesTxt = SEPARATOR_BLANK;

    public DwsTxtId getDwsTxtId() {
        return dwsTxtId;
    }

    public void setDwsTxtId(DwsTxtId dwsTxtId) {
        this.dwsTxtId = dwsTxtId;
    }

    public String getDttDesTxtCd() {
        return dttDesTxtCd;
    }

    public void setDttDesTxtCd(String dttDesTxtCd) {
        this.dttDesTxtCd = dttDesTxtCd;
    }

    public String getDttDesTxt() {
        return dttDesTxt;
    }

    public void setDttDesTxt(String dttDesTxt) {
        this.dttDesTxt = dttDesTxt;
    }

}
