package disbursement.data.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import disbursement.data.entity.id.DwsRuleId;

@Entity
@Table(name = "DWS_RULE")
public class DwsRule implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DwsRuleId dwsRuleId;

    public DwsRuleId getDwsRuleId() {
        return dwsRuleId;
    }

    public void setDwsRuleId(DwsRuleId dwsRuleId) {
        this.dwsRuleId = dwsRuleId;
    }

}
