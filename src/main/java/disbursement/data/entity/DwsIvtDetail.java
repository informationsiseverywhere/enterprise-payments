package disbursement.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import disbursement.data.entity.id.DwsIvtDetailId;

@Entity
@Table(name = "DWS_IVT_DETAIL")
public class DwsIvtDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DwsIvtDetailId dwsIvtDetailId;

    @Column(name = "DWS_DSB_ID", length = 21, nullable = false)
    private String dwsDsbId = SEPARATOR_BLANK;

    @Column(name = "DWS_DSB_BATCH_ID", length = 12, nullable = true)
    private String dwsDsbBatchId = SEPARATOR_BLANK;

    @Column(name = "DWS_HONORED_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime dwsHonoredDt;

    @Column(name = "DWS_USAGE_CD", length = 2, nullable = false)
    private String dwsUsageCd = SEPARATOR_BLANK;

    @Column(name = "DWS_HST_VLD_NBR", length = 5, nullable = false)
    private Integer dwsHstVldNbr;

    @Column(name = "DWS_EXPIRATION_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime dwsExpirationTs;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "DDS_PMT_TRACE_NBR", length = 10, nullable = false)
    private Integer ddsPmtTraceNbr;

    public DwsIvtDetailId getDwsIvtDetailId() {
        return dwsIvtDetailId;
    }

    public void setDwsIvtDetailId(DwsIvtDetailId dwsIvtDetailId) {
        this.dwsIvtDetailId = dwsIvtDetailId;
    }

    public String getDwsDsbId() {
        return dwsDsbId;
    }

    public void setDwsDsbId(String dwsDsbId) {
        this.dwsDsbId = dwsDsbId;
    }

    public String getDwsDsbBatchId() {
        return dwsDsbBatchId;
    }

    public void setDwsDsbBatchId(String dwsDsbBatchId) {
        this.dwsDsbBatchId = dwsDsbBatchId;
    }

    public ZonedDateTime getDwsHonoredDt() {
        return dwsHonoredDt;
    }

    public void setDwsHonoredDt(ZonedDateTime dwsHonoredDt) {
        this.dwsHonoredDt = dwsHonoredDt;
    }

    public String getDwsUsageCd() {
        return dwsUsageCd;
    }

    public void setDwsUsageCd(String dwsUsageCd) {
        this.dwsUsageCd = dwsUsageCd;
    }

    public Integer getDwsHstVldNbr() {
        return dwsHstVldNbr;
    }

    public void setDwsHstVldNbr(Integer dwsHstVldNbr) {
        this.dwsHstVldNbr = dwsHstVldNbr;
    }

    public ZonedDateTime getDwsExpirationTs() {
        return dwsExpirationTs;
    }

    public void setDwsExpirationTs(ZonedDateTime dwsExpirationTs) {
        this.dwsExpirationTs = dwsExpirationTs;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getDdsPmtTraceNbr() {
        return ddsPmtTraceNbr;
    }

    public void setDdsPmtTraceNbr(Integer ddsPmtTraceNbr) {
        this.ddsPmtTraceNbr = ddsPmtTraceNbr;
    }

}
