package disbursement.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import disbursement.data.entity.DwsTxt;
import disbursement.data.entity.id.DwsTxtId;

public interface DwsTxtRepository extends JpaRepository<DwsTxt, DwsTxtId> {

}
