package disbursement.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import disbursement.data.entity.DwsRule;
import disbursement.data.entity.id.DwsRuleId;

public interface DwsRuleRepository extends JpaRepository<DwsRule, DwsRuleId> {

}
