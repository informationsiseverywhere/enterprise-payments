package disbursement.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import disbursement.data.entity.DwsCdTxt;
import disbursement.data.entity.id.DwsCdTxtId;

public interface DwsCdTxtRepository extends JpaRepository<DwsCdTxt, DwsCdTxtId> {

    @Query("select b from DwsCdTxt b where b.dwsCdTxtId.dctCdType =:dctCdType and UPPER(b.dwsCdTxtId.dctCd) like '%'||UPPER(:dctCd)||'%'")
    public List<DwsCdTxt> findDescription(@Param("dctCdType") String dctCdType, @Param("dctCd") String dctCd);
}
