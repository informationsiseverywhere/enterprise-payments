package disbursement.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import disbursement.data.entity.DwsLocationCnv;
import disbursement.data.entity.id.DwsLocationCnvId;

public interface DwsLocationCnvRepository extends JpaRepository<DwsLocationCnv, DwsLocationCnvId> {

    @Query(value = "select b from DwsLocationCnv b where b.dwsLocationCnvId.dwsPrcLocCd = :dwsPrcLocCd and "
            + "b.dwsLocationCnvId.dwsMediumCd = :dwsMediumCd order by b.dwsLocationCnvId.masterCompanyNbr ASC, b.dwsLocationCnvId.riskStateCd, b.dwsLocationCnvId.lobCd DESC ")
    public DwsLocationCnv findDwsLocationCnv(@Param("dwsPrcLocCd") String dwsPrcLocCd,
            @Param("dwsMediumCd") String dwsMediumCd);
}
