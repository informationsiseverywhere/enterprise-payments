package disbursement.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import disbursement.data.entity.DwsStatusHst;
import disbursement.data.entity.id.DwsStatusHstId;

public interface DwsStatusHstRepository extends JpaRepository<DwsStatusHst, DwsStatusHstId> {

}
