package disbursement.data.repository;

import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import disbursement.data.entity.DwsIvtDetail;
import disbursement.data.entity.id.DwsIvtDetailId;

public interface DwsIvtDetailRepository extends JpaRepository<DwsIvtDetail, DwsIvtDetailId> {

    @Modifying
    @Query(value = "update DwsIvtDetail d set d.dwsExpirationTs = :dwsExpirationTs, d.dwsHstVldNbr = :historyVldNumber where d.dwsIvtDetailId.dwsPrcLocCd = :dwsPrcLocCd and d.dwsIvtDetailId.dwsMediumCd =:dwsMediumCd "
            + "and d.dwsIvtDetailId.dwsCkDrfNbr =:dwsCkDrfNbr and d.dwsHstVldNbr =:dwsHstVldNbr")
    public void updateDisbursementInventoryDetail(@Param("dwsPrcLocCd") String dwsPrcLocCd,
            @Param("dwsMediumCd") String dwsMediumCd, @Param("dwsCkDrfNbr") Integer dwsCkDrfNbr,
            @Param("dwsHstVldNbr") Integer dwsHstVldNbr, @Param("dwsExpirationTs") ZonedDateTime dwsExpirationTs,
            @Param("historyVldNumber") Integer historyVldNumber);

    @Query(value = "select d from DwsIvtDetail d where d.dwsIvtDetailId.dwsPrcLocCd = :dwsPrcLocCd and d.dwsIvtDetailId.dwsMediumCd =:dwsMediumCd and d.dwsIvtDetailId.dwsCkDrfNbr =:dwsCkDrfNbr and d.dwsHstVldNbr =:dwsHstVldNbr")
    public DwsIvtDetail getDisbursementInventoryDetail(@Param("dwsPrcLocCd") String dwsPrcLocCd,
            @Param("dwsMediumCd") String dwsMediumCd, @Param("dwsCkDrfNbr") Integer dwsCkDrfNbr,
            @Param("dwsHstVldNbr") Integer dwsHstVldNbr);

}
