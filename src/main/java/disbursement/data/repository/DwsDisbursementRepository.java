package disbursement.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import disbursement.data.entity.DwsDisbursement;

public interface DwsDisbursementRepository extends JpaRepository<DwsDisbursement, String> {

    public DwsDisbursement findByDwsDsbId(String disbursementId);
}
