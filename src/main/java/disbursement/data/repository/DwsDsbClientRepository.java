package disbursement.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import disbursement.data.entity.DwsDsbClient;
import disbursement.data.entity.id.DwsDsbClientId;

public interface DwsDsbClientRepository extends JpaRepository<DwsDsbClient, DwsDsbClientId> {

}
