package disbursement.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import disbursement.data.entity.DwsIvtControl;
import disbursement.data.entity.id.DwsIvtControlId;

public interface DwsIvtControlRepository extends JpaRepository<DwsIvtControl, DwsIvtControlId> {

    public DwsIvtControl findByDwsIvtControlIdDwsPrcLocCdAndDwsIvtControlIdDwsMediumCdAndDwsHstVldNbr(
            String printLocationCode, String mediumCode, Integer historyValidationNumber);

    @Query("select COUNT(d) from DwsIvtControl d where d.dwsIvtControlId.dwsPrcLocCd =:dwsPrcLocCd and ((d.dwsCurBegCkNbr <=:checkNumber and d.dwsCurEnCkNbr >=:checkNumber) or (d.dwsNxtBegCkNbr <=:checkNumber and d.dwsNxtEnCkNbr >=:checkNumber)) "
            + "and d.dwsIvtControlId.dwsMediumCd =:mediumCode")
    public Short findCheckNumber(@Param("dwsPrcLocCd") String dwsPrcLocCd, @Param("checkNumber") Integer checkNumber,
            @Param("mediumCode") String mediumCode);

    @Query("select d from DwsIvtControl d where d.dwsIvtControlId.dwsPrcLocCd =:processLocationCode "
            + "and d.dwsIvtControlId.dwsMediumCd =:mediumCode and d.dwsHstVldNbr =:historyValidationNumber")
    public DwsIvtControl findCheckRange(@Param("processLocationCode") String processLocationCode,
            @Param("mediumCode") String mediumCode, @Param("historyValidationNumber") Integer historyValidationNumber);
}
