package disbursement.service;

import java.util.List;

import disbursement.model.DisbursementOperation;

public interface AccountDisbursementOperationsService {

    public void paymentDeleteFromCombinedPayment(String disbursementId);

    public void statusUpdate(List<DisbursementOperation> disbursementOperationList);
}
