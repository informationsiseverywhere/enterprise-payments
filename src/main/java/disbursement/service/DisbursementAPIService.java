package disbursement.service;

import disbursement.data.entity.DwsDisbursement;
import disbursement.utils.DisbursementConstants.DisbursementIdType;

public interface DisbursementAPIService {

    public void checkLocationCode(String processingLocationCode, Integer checkNumber, String mediumCode);

    public String saveDisbursementActivity(String folderId, String refundMethod, Character statusCode,
            String processingLocationCode, Integer checkNumber, String reason, String mediumCode,
            DisbursementIdType disbursementIdType, String accountNumber, String clientId, Double amount,
            String memoText, String userMemoText, String dsbTypeCode, Short addressSequenceNumber,
            String technicalDisbursementId, String currencyCode);

    public String generateDisbursementId(String technicalDisbursementId, DisbursementIdType disbursementIdType);

    public DwsDisbursement checkDisbursement(String disbursementId);

    public void cancelDisbursement(String folderId, String clientId, String reason, String mediumCode,
            String processingLocationCode, Double amount, Integer checkNumber, String disbursementId, String typeCode,
            Short addressSequenceNumber);

    public boolean voidDisbursement(String disbursementId, Character statusCode, String reason);

}
