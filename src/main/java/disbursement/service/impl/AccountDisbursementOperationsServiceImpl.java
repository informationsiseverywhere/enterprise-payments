package disbursement.service.impl;

import static billing.utils.BillingConstants.PLUS_SIGN;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.CHAR_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;
import static disbursement.utils.DisbursementConstants.COMPANY_LOCATION_NUMBER;
import static disbursement.utils.DisbursementConstants.MASTER_COMPANY_NUMBER;

import java.time.ZonedDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import billing.data.entity.BilCashEntryTot;
import billing.data.entity.BilRulesUct;
import billing.data.entity.BilUnIdCash;
import billing.data.entity.BilUnidCshHst;
import billing.data.entity.id.BilCashEntryTotId;
import billing.data.entity.id.BilUnIdCshHstId;
import billing.data.repository.BilCashEntryTotRepository;
import billing.data.repository.BilRulesUctRepository;
import billing.data.repository.BilUnIdCashRepository;
import billing.data.repository.BilUnidCshHstRepository;
import billing.service.BilRulesUctService;
import billing.utils.BillingConstants.BilDesReasonCode;
import billing.utils.BillingConstants.BilRules;
import billing.utils.BillingConstants.BusCodeTranslationType;
import billing.utils.BillingConstants.CheckProductionMethod;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import disbursement.model.DisbursementOperation;
import disbursement.service.AccountDisbursementOperationsService;
import disbursement.utils.DisbursementConstants.DisbursementIdType;
import disbursement.utils.DisbursementConstants.DisbursementStatusCode;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ApplicationName;
import fis.utils.FinancialIntegratorConstants.ApplicationProgramIdentifier;
import fis.utils.FinancialIntegratorConstants.ObjectCode;

@Service
public class AccountDisbursementOperationsServiceImpl implements AccountDisbursementOperationsService {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private BilUnIdCashRepository bilUnIdCashRepository;

    @Autowired
    private BilCashEntryTotRepository bilCashEntryTotRepository;

    @Autowired
    private BilRulesUctRepository bilRulesUctRepository;

    @Autowired
    private BilUnidCshHstRepository bilUnidCshHstRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private BilRulesUctService bilRulesUctService;

    @Autowired
    private DateService dateService;

    private static final String ERROR_CODE = "0000";

    private static final String WIRE_TRANSFER_CHECK_MEDIUM = "WC";

    private static final Logger LOGGER = LogManager.getLogger(AccountDisbursementOperationsServiceImpl.class);

    private static final char DOWN_PAYMENT_HOLD = 'H';
    private static final char DOWN_PAYMENT_NO = 'N';
    private static final char DOWN_PAYMENT_APPLY = 'A';

    @Override
    @Transactional
    public void paymentDeleteFromCombinedPayment(String disbursementId) {
        processBillingAndFWS(disbursementId, null);
    }

    @Override
    @Transactional
    public void statusUpdate(List<DisbursementOperation> disbursementOperationList) {
        for (DisbursementOperation disbursementOperation : disbursementOperationList) {
            processBillingAndFWS(disbursementOperation.getDisbursementId(), disbursementOperation);
        }
    }

    private void processBillingAndFWS(String disbursementId, DisbursementOperation disbursementOperation) {
        String disbursementActivityType = disbursementOperation.getDisbursementId().substring(0, 4);
        List<BilUnIdCash> bilUnIdCashList = bilUnIdCashRepository.findByBilDsbId(disbursementId);
        if (bilUnIdCashList != null && !bilUnIdCashList.isEmpty()) {
            for (BilUnIdCash bilUnIdCash : bilUnIdCashList) {
                String oldStatus = String.valueOf(bilUnIdCash.getDwsStatusCd());
                String currencyCode = BLANK_STRING;
                BilCashEntryTot bilCashEntryTot = bilCashEntryTotRepository
                        .findById(new BilCashEntryTotId(bilUnIdCash.getBilUnIdCashId().getBilEntryDt(),
                                bilUnIdCash.getBilUnIdCashId().getBilEntryNbr(),
                                bilUnIdCash.getBilUnIdCashId().getUserId()))
                        .orElse(null);
                if (bilCashEntryTot != null) {
                    currencyCode = bilCashEntryTot.getCurrencyCd();
                }
                ZonedDateTime applicationDate = dateService.currentDateTime();
                ZonedDateTime effectiveDate = bilUnIdCash.getDdsDsbDt();

                if (disbursementOperation.getStatus() != null) {
                    String newStatus = String.valueOf(disbursementOperation.getStatus());
                    if (disbursementOperation.getCheckNumber() != null && disbursementOperation.getCheckNumber() != 0
                            && disbursementOperation.getCheckNumber() == bilUnIdCash.getDwsCkDrfNbr()
                            && oldStatus.equals(newStatus)) {
                        continue;

                    }
                    if (disbursementOperation.getCheckNumber() == null) {
                        disbursementOperation.setCheckNumber(bilUnIdCash.getDwsCkDrfNbr());
                    }

                    BilRulesUct bilRulesUct = bilRulesUctRepository.getBilRulesRow(
                            BilRules.UNIDENTIFIED_CASH_HISTORY.getValue(), BLANK_STRING, BLANK_STRING, BLANK_STRING,
                            dateService.currentDate(), dateService.currentDate());
                    if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                        saveUnidentifiedCashHistory(bilUnIdCash, disbursementOperation.getStatus());
                    }

                    if (disbursementOperation.getStatus().equals(DisbursementStatusCode.PENDING.toChar())
                            || disbursementOperation.getStatus().equals(DisbursementStatusCode.SELECTCOMBINED.toChar())
                            || disbursementOperation.getStatus().equals(DisbursementStatusCode.COMBINEDPAYMENT.toChar())
                            || disbursementOperation.getStatus().equals(DisbursementStatusCode.HOLD.toChar())
                            || disbursementOperation.getStatus()
                                    .equals(DisbursementStatusCode.STOPPAYREQUEST.toChar())) {
                        if (!disbursementOperation.getStatus().equals(DisbursementStatusCode.STOPPAYREQUEST.toChar())) {
                            disbursementOperation.setCheckNumber(0);
                        }
                        bilUnIdCash.setDwsStatusCd(disbursementOperation.getStatus());
                        bilUnIdCash.setDdsDsbDt(disbursementOperation.getDisbursementDate());
                        bilUnIdCash.setDwsCkDrfNbr(disbursementOperation.getCheckNumber());
                        bilUnIdCash.setBilAcyTs(dateService.currentDate());
                        setCheckMethodCode(bilUnIdCash, disbursementOperation, disbursementActivityType);
                        saveFinancialActivity(bilUnIdCash, bilUnIdCash.getBilRctAmt(), applicationDate, effectiveDate,
                                ObjectCode.UNIDENTIFIED_CASH.toString(), ActionCode.UPDATED.toString(), CHAR_N,
                                ApplicationName.BCWSUNID.toString(), CHAR_A,
                                DisbursementIdType.BCMS.getDisbursementIdType(),
                                ApplicationProgramIdentifier.BCMODUD.toString(), disbursementId, newStatus,
                                currencyCode);

                    } else if (disbursementOperation.getStatus().equals(DisbursementStatusCode.DISBURSED.toChar())) {
                        processDisbursedDisbursement(bilUnIdCash, bilCashEntryTot, disbursementOperation,
                                disbursementActivityType);

                    } else if (disbursementOperation.getStatus().equals(DisbursementStatusCode.ESCHEATWAIT.toChar())
                            || disbursementOperation.getStatus().equals(DisbursementStatusCode.ESCHEATED.toChar())) {
                        if (disbursementOperation.getStatus().equals(DisbursementStatusCode.ESCHEATWAIT.toChar())
                                && bilUnIdCash.getDwsStatusCd() == DisbursementStatusCode.ESCHEATED.toChar()) {
                            Double activityAmount = bilUnIdCash.getBilRctAmt() * -1;
                            saveFinancialActivity(bilUnIdCash, activityAmount, dateService.currentDate(),
                                    disbursementOperation.getDisbursementDate(),
                                    ObjectCode.UNIDENTIFIED_CASH.toString(), ActionCode.UPDATED.toString(), CHAR_N,
                                    ApplicationName.BCWSUNID.toString(), CHAR_A,
                                    DisbursementIdType.BCMS.getDisbursementIdType(),
                                    ApplicationProgramIdentifier.BCMODUD.toString(),
                                    disbursementOperation.getDisbursementId(), oldStatus,
                                    bilCashEntryTot.getCurrencyCd());
                        }

                        bilUnIdCash.setDwsStatusCd(disbursementOperation.getStatus());
                        bilUnIdCash.setDdsDsbDt(disbursementOperation.getDisbursementDate());
                        setCheckMethodCode(bilUnIdCash, disbursementOperation, disbursementActivityType);
                        bilUnIdCash.setDwsCkDrfNbr(disbursementOperation.getCheckNumber());
                        saveFinancialActivity(bilUnIdCash, bilUnIdCash.getBilRctAmt(), dateService.currentDate(),
                                disbursementOperation.getDisbursementDate(), ObjectCode.UNIDENTIFIED_CASH.toString(),
                                ActionCode.UPDATED.toString(), CHAR_N, ApplicationName.BCWSUNID.toString(), CHAR_A,
                                DisbursementIdType.BCMS.getDisbursementIdType(),
                                ApplicationProgramIdentifier.BCMODUD.toString(),
                                disbursementOperation.getDisbursementId(), newStatus, bilCashEntryTot.getCurrencyCd());

                    } else if (disbursementOperation.getStatus().equals(DisbursementStatusCode.CANCEL.toChar())
                            || disbursementOperation.getStatus().equals(DisbursementStatusCode.VOIDNOREPRINT.toChar())
                            || disbursementOperation.getStatus().equals(DisbursementStatusCode.STOPPAY.toChar())) {

                        if (bilUnIdCash.getDwsStatusCd() == DisbursementStatusCode.HOLD.toChar()
                                || bilUnIdCash.getDwsStatusCd() == DisbursementStatusCode.ESCHEATWAIT.toChar()
                                        && disbursementOperation.getStatus() == DisbursementStatusCode.VOIDNOREPRINT
                                                .toChar()
                                || bilUnIdCash.getDwsStatusCd() == DisbursementStatusCode.ESCHEATWAIT.toChar()
                                || bilUnIdCash.getDwsStatusCd() == DisbursementStatusCode.STOPPAYREQUEST.toChar()
                                        && disbursementOperation.getStatus() == DisbursementStatusCode.VOIDNOREPRINT
                                                .toChar()
                                || bilUnIdCash.getDwsStatusCd() == DisbursementStatusCode.ESCHEATWAIT.toChar()
                                        && disbursementOperation.getStatus() == DisbursementStatusCode.STOPPAY
                                                .toChar()) {
                            Double activityAmount = bilUnIdCash.getBilRctAmt() * -1;
                            saveFinancialActivity(bilUnIdCash, activityAmount, dateService.currentDate(),
                                    disbursementOperation.getDisbursementDate(),
                                    ObjectCode.UNIDENTIFIED_CASH.toString(), ActionCode.UPDATED.toString(), CHAR_N,
                                    ApplicationName.BCWSUNID.toString(), CHAR_A,
                                    DisbursementIdType.BCMS.getDisbursementIdType(),
                                    ApplicationProgramIdentifier.BCMODUD.toString(),
                                    disbursementOperation.getDisbursementId(), oldStatus,
                                    bilCashEntryTot.getCurrencyCd());

                        } else if (bilUnIdCash.getDwsStatusCd() == BLANK_CHAR) {
                            Double activityAmount = bilUnIdCash.getBilRctAmt() * -1;
                            saveFinancialActivity(bilUnIdCash, activityAmount, dateService.currentDate(),
                                    disbursementOperation.getDisbursementDate(),
                                    ObjectCode.UNIDENTIFIED_CASH.toString(), ActionCode.UPDATED.toString(), CHAR_N,
                                    ApplicationName.BCWSUNID.toString(), CHAR_A,
                                    DisbursementIdType.BCMS.getDisbursementIdType(),
                                    ApplicationProgramIdentifier.BCMODUD.toString(),
                                    disbursementOperation.getDisbursementId(), newStatus,
                                    bilCashEntryTot.getCurrencyCd());

                        }
                        if (disbursementActivityType.equals("BASM")) {
                            bilUnIdCash.setBilPayeeCltId(BLANK_STRING);
                            bilUnIdCash.setBilPayeeAdrSeq((short) 0);
                        }
                        bilUnIdCash.setBilDspDt(disbursementOperation.getDisbursementDate());
                        bilUnIdCash.setBilDspTypeCd("SP");
                        bilUnIdCash.setDwsCkDrfNbr(0);
                        bilUnIdCash.setBilAcyTs(dateService.currentDateTime());
                        bilUnIdCash.setDdsDsbDt(DateRoutine.defaultDateTime());
                        bilUnIdCash.setDwsStatusCd(BLANK_CHAR);
                        bilUnIdCash.setBilDsbId(BLANK_STRING);
                        bilUnIdCash.setBilChkPrdMthCd(BLANK_CHAR);
                        char downPaymentIndicator = checkForDownPayment(bilUnIdCash.getBilRctTypeCd(),
                                bilUnIdCash.getBilUnIdCashId().getBilEntryDt());
                        if (downPaymentIndicator == DOWN_PAYMENT_HOLD) {
                            bilUnIdCash.setBilDspReasonCd(BilDesReasonCode.DOWN_PAYMENT_RECEIPT.getValue());
                            bilUnIdCash.setBilManualSusInd('Y');
                        }

                        String reason = bilUnIdCash.getBilDspReasonCd().trim() + "R";
                        saveFinancialActivity(bilUnIdCash, bilUnIdCash.getBilRctAmt(), applicationDate, effectiveDate,
                                ObjectCode.UNIDENTIFIED_CASH.toString(), ActionCode.AUTHOR_DISBURSEMENT.toString(),
                                CHAR_N, ApplicationName.BCWSUNID.toString(), CHAR_A,
                                DisbursementIdType.BCMS.getDisbursementIdType(),
                                ApplicationProgramIdentifier.BCMODUD.toString(), disbursementId, reason, currencyCode);

                        saveFinancialActivity(bilUnIdCash, bilUnIdCash.getBilRctAmt(), dateService.currentDate(),
                                disbursementOperation.getDisbursementDate(), ObjectCode.UNIDENTIFIED_CASH.toString(),
                                ActionCode.UPDATED.toString(), CHAR_N, ApplicationName.BCWSUNID.toString(), CHAR_A,
                                DisbursementIdType.BCMS.getDisbursementIdType(),
                                ApplicationProgramIdentifier.BCMODUD.toString(),
                                disbursementOperation.getDisbursementId(), newStatus, bilCashEntryTot.getCurrencyCd());

                    } else if (disbursementOperation.getStatus().equals(DisbursementStatusCode.HONORED.toChar())) {
                        bilUnIdCash.setDwsStatusCd(disbursementOperation.getStatus());
                        bilUnIdCash.setBilAcyTs(dateService.currentDate());
                        bilUnIdCash.setDdsDsbDt(dateService.currentDate());
                        if (bilUnIdCash.getDwsStatusCd() != DisbursementStatusCode.STOPPAYREQUEST.toChar()) {
                            saveFinancialActivity(bilUnIdCash, bilUnIdCash.getBilRctAmt(), dateService.currentDate(),
                                    disbursementOperation.getDisbursementDate(),
                                    ObjectCode.UNIDENTIFIED_CASH.toString(), ActionCode.UPDATED.toString(), CHAR_N,
                                    ApplicationName.BCWSUNID.toString(), CHAR_A,
                                    DisbursementIdType.BCMS.getDisbursementIdType(),
                                    ApplicationProgramIdentifier.BCMODUD.toString(),
                                    disbursementOperation.getDisbursementId(), newStatus,
                                    bilCashEntryTot.getCurrencyCd());
                        }

                    }

                }

            }
        }
    }

    private void setCheckMethodCode(BilUnIdCash bilUnIdCash, DisbursementOperation disbursementOperation,
            String disbursementActivityType) {
        if (disbursementOperation.getStatus().equals(DisbursementStatusCode.DISBURSED.toChar())
                || disbursementOperation.getStatus().equals(DisbursementStatusCode.PENDING.toChar())) {
            if (disbursementOperation.getMediumCode().equals(WIRE_TRANSFER_CHECK_MEDIUM)) {
                if (disbursementOperation.getCombinedPaymentCode() == 'S') {
                    bilUnIdCash.setBilChkPrdMthCd(CheckProductionMethod.WIRE_TRANSFER_COMBINED.getValue());
                } else {
                    bilUnIdCash.setBilChkPrdMthCd(CheckProductionMethod.WIRE_TRANSFER.getValue());
                }

            } else if (disbursementActivityType.compareTo("BCCM") == 0
                    || disbursementActivityType.compareTo("BPCM") == 0) {
                bilUnIdCash.setBilChkPrdMthCd('G');
            } else if (disbursementOperation.getCombinedPaymentCode() == 'S') {
                bilUnIdCash.setBilChkPrdMthCd(CheckProductionMethod.SYSTEM_COMBINED_PAY.getValue());
            } else if (disbursementOperation.getCheckNumber() > 0) {
                bilUnIdCash.setBilChkPrdMthCd(CheckProductionMethod.SYSTEM_GENERATED.getValue());
            }

        } else if (disbursementOperation.getStatus().equals(DisbursementStatusCode.COMBINEDPAYMENT.toChar())
                && disbursementOperation.getMediumCode().equals(WIRE_TRANSFER_CHECK_MEDIUM)) {
            if (disbursementOperation.getCombinedPaymentCode() == 'S') {

                bilUnIdCash.setBilChkPrdMthCd(CheckProductionMethod.WIRE_TRANSFER_COMBINED.getValue());
            } else {
                bilUnIdCash.setBilChkPrdMthCd(CheckProductionMethod.WIRE_TRANSFER.getValue());
            }

        }
    }

    private void saveFinancialActivity(BilUnIdCash bilUnidentifiedCash, Double amount, ZonedDateTime applicationDate,
            ZonedDateTime effectiveDate, String transactionObjectCode, String transactionActionCode,
            Character financialIndicator, String applicationName, Character functionCode, String productIdentifier,
            String programId, String disbursementId, String reasonCode, String currencyCode) {
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setActivityNetAmount(amount);
        financialApiActivity.setFinancialIndicator(financialIndicator);
        financialApiActivity.setDisbursementId(disbursementId);
        financialApiActivity.setFunctionCode(functionCode);
        financialApiActivity.setFolderId(BLANK_STRING);
        financialApiActivity.setApplicationName(applicationName);
        financialApiActivity.setUserId(execContext.getUserSeqeunceId());
        financialApiActivity.setErrorCode(ERROR_CODE);
        financialApiActivity.setApplicationProductIdentifier(productIdentifier);
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(effectiveDate));
        financialApiActivity.setOperatorId(execContext.getUserSeqeunceId());
        financialApiActivity.setAppProgramId(programId);
        financialApiActivity.setReferenceDate(
                DateRoutine.dateTimeAsYYYYMMDDString(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt()));
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(effectiveDate));
        financialApiActivity.setBilAccountId(bilUnidentifiedCash.getBilAccountNbr());
        financialApiActivity.setBilReceiptTypeCode(bilUnidentifiedCash.getBilRctTypeCd());
        financialApiActivity.setBilBankCode(bilUnidentifiedCash.getBilBankCd());
        financialApiActivity.setBilSourceCode(String.valueOf(bilUnidentifiedCash.getBilCshEtrMthCd()));
        financialApiActivity.setBilReasonCode(reasonCode);
        financialApiActivity
                .setBilDatabaseKey(generateDatabaseKey(bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(), bilUnidentifiedCash.getBilEntrySeqNbr()));
        financialApiActivity.setSummaryEffectiveDate(DateRoutine.defaultDateTime());
        financialApiActivity.setPolicyId(bilUnidentifiedCash.getPolNbr());
        financialApiActivity.setCurrencyCode(currencyCode);
        if (!bilUnidentifiedCash.getAgencyNbr().equals(BLANK_STRING)) {
            financialApiActivity.setAgentTtyId(bilUnidentifiedCash.getBilAgtActNbr());
        }
        if (!bilUnidentifiedCash.getBilTtyNbr().equals(BLANK_STRING)) {
            financialApiActivity.setAgentTtyId(bilUnidentifiedCash.getBilTtyNbr());
        }
        String[] fwsReturnMessage = null;
        fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity, applicationDate);
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            LOGGER.error(fwsReturnMessage[1]);
            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

    private void saveUnidentifiedCashHistory(BilUnIdCash bilUnidentifiedCash, Character status) {
        BilUnidCshHst unidentifiedCashHistory = new BilUnidCshHst();
        unidentifiedCashHistory
                .setBilUnIdCshHstId(new BilUnIdCshHstId(bilUnidentifiedCash.getBilUnIdCashId().getBilDtbDt(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilDtbSeqNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryNbr(),
                        bilUnidentifiedCash.getBilUnIdCashId().getUserId(),
                        bilUnidentifiedCash.getBilUnIdCashId().getBilEntryDt(), bilUnidentifiedCash.getBilAcyTs()));
        unidentifiedCashHistory.setBilAccountNbr(bilUnidentifiedCash.getBilAccountNbr());
        unidentifiedCashHistory.setActivityUserId(bilUnidentifiedCash.getActivityUserId());
        unidentifiedCashHistory.setBilAdditionalId(bilUnidentifiedCash.getBilAdditionalId());
        unidentifiedCashHistory.setBilAdjDueDt(bilUnidentifiedCash.getBilAdjDueDt());
        unidentifiedCashHistory.setBilAgtActNbr(bilUnidentifiedCash.getBilAgtActNbr());
        unidentifiedCashHistory.setBilChkPrdMthCd(bilUnidentifiedCash.getBilChkPrdMthCd());
        unidentifiedCashHistory.setBilCwaId(bilUnidentifiedCash.getBilCwaId());
        unidentifiedCashHistory.setBilDepositDt(bilUnidentifiedCash.getBilDepositDt());
        unidentifiedCashHistory.setBilDsbId(bilUnidentifiedCash.getBilDsbId());
        unidentifiedCashHistory.setBilDspDt(bilUnidentifiedCash.getBilDspDt());
        unidentifiedCashHistory.setBilDspReasonCd(bilUnidentifiedCash.getBilDspReasonCd());
        if (status.equals(DisbursementStatusCode.STOPPAY.toChar())) {
            unidentifiedCashHistory.setBilDspTypeCd("DS");
        } else if (status.equals(DisbursementStatusCode.VOIDNOREPRINT.toChar())) {
            unidentifiedCashHistory.setBilDspTypeCd("DV");
        } else {
            unidentifiedCashHistory.setBilDspTypeCd(bilUnidentifiedCash.getBilDspTypeCd());
        }

        unidentifiedCashHistory.setBilEntrySeqNbr(bilUnidentifiedCash.getBilEntrySeqNbr());
        unidentifiedCashHistory.setBilManualSusInd(bilUnidentifiedCash.getBilManualSusInd());
        unidentifiedCashHistory.setBilPayeeAdrSeq(bilUnidentifiedCash.getBilPayeeAdrSeq());
        unidentifiedCashHistory.setBilPayeeCltId(bilUnidentifiedCash.getBilPayeeCltId());
        unidentifiedCashHistory.setBilPblItemCd(bilUnidentifiedCash.getBilPblItemCd());
        unidentifiedCashHistory.setBilRctAmt(bilUnidentifiedCash.getBilRctAmt());
        unidentifiedCashHistory.setBilRctCmt(bilUnidentifiedCash.getBilRctCmt());
        unidentifiedCashHistory.setBilRctId(bilUnidentifiedCash.getBilRctId());
        unidentifiedCashHistory.setBilRctTypeCd(bilUnidentifiedCash.getBilRctTypeCd());
        unidentifiedCashHistory.setBilTtyNbr(bilUnidentifiedCash.getBilTtyNbr());
        unidentifiedCashHistory.setDdsDsbDt(bilUnidentifiedCash.getDdsDsbDt());
        unidentifiedCashHistory.setDwsCkDrfNbr(bilUnidentifiedCash.getDwsCkDrfNbr());
        unidentifiedCashHistory.setDwsStatusCd(bilUnidentifiedCash.getDwsStatusCd());
        unidentifiedCashHistory.setPolNbr(bilUnidentifiedCash.getPolNbr());
        unidentifiedCashHistory.setPolSymbolCd(bilUnidentifiedCash.getPolSymbolCd());
        bilUnidCshHstRepository.save(unidentifiedCashHistory);

    }

    private String generateDatabaseKey(ZonedDateTime entryDate, String entryNumber, String userId,
            Short entrySequenceNumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.rightPad(DateRoutine.dateTimeAsYYYYMMDDString(entryDate), 10));
        sb.append(StringUtils.rightPad(entryNumber, 4));
        sb.append(StringUtils.rightPad(userId, 8));
        sb.append(PLUS_SIGN);
        sb.append(StringUtils.rightPad(String.valueOf(entrySequenceNumber), 5, CHAR_ZERO));
        return sb.toString();
    }

    private void processDisbursedDisbursement(BilUnIdCash bilUnIdCash, BilCashEntryTot bilCashEntryTot,
            DisbursementOperation disbursementOperation, String disbursementActivityType) {
        String oldStatus = String.valueOf(bilUnIdCash.getDwsStatusCd());
        String newStatus = String.valueOf(disbursementOperation.getStatus());
        if (bilUnIdCash.getDwsStatusCd() == DisbursementStatusCode.STOPPAYREQUEST.toChar()
                || bilUnIdCash.getDwsStatusCd() == DisbursementStatusCode.ESCHEATWAIT.toChar()
                || bilUnIdCash.getDwsStatusCd() == DisbursementStatusCode.HONORED.toChar()) {
            Double activityAmount = bilUnIdCash.getBilRctAmt() * -1;
            saveFinancialActivity(bilUnIdCash, activityAmount, dateService.currentDate(),
                    disbursementOperation.getDisbursementDate(), ObjectCode.UNIDENTIFIED_CASH.toString(),
                    ActionCode.UPDATED.toString(), CHAR_N, ApplicationName.BCWSUNID.toString(), CHAR_A,
                    DisbursementIdType.BCMS.getDisbursementIdType(), ApplicationProgramIdentifier.BCMODUD.toString(),
                    disbursementOperation.getDisbursementId(), oldStatus, bilCashEntryTot.getCurrencyCd());

            bilUnIdCash.setDwsStatusCd(disbursementOperation.getStatus());
            bilUnIdCash.setDdsDsbDt(disbursementOperation.getDisbursementDate());
            setCheckMethodCode(bilUnIdCash, disbursementOperation, disbursementActivityType);
            bilUnIdCash.setDwsCkDrfNbr(disbursementOperation.getCheckNumber());
            if (bilUnIdCash.getDwsStatusCd() == DisbursementStatusCode.HONORED.toChar()) {
                bilUnIdCash.setBilAcyTs(dateService.currentDateTime());

            }

        } else {
            bilUnIdCash.setDwsStatusCd(disbursementOperation.getStatus());
            bilUnIdCash.setDdsDsbDt(disbursementOperation.getDisbursementDate());
            setCheckMethodCode(bilUnIdCash, disbursementOperation, disbursementActivityType);
            bilUnIdCash.setDwsCkDrfNbr(disbursementOperation.getCheckNumber());
            saveFinancialActivity(bilUnIdCash, bilUnIdCash.getBilRctAmt(), dateService.currentDate(),
                    disbursementOperation.getDisbursementDate(), ObjectCode.UNIDENTIFIED_CASH.toString(),
                    ActionCode.UPDATED.toString(), CHAR_N, ApplicationName.BCWSUNID.toString(), CHAR_A,
                    DisbursementIdType.BCMS.getDisbursementIdType(), ApplicationProgramIdentifier.BCMODUD.toString(),
                    disbursementOperation.getDisbursementId(), newStatus, bilCashEntryTot.getCurrencyCd());

        }

    }

    private char checkForDownPayment(String receiptTypeCode, ZonedDateTime bilEntryDt) {
        char downPaymentIndicator = DOWN_PAYMENT_NO;
        BusCdTranslation busCodeTranslation = busCdTranslationRepository.findByCodeAndType(receiptTypeCode,
                BusCodeTranslationType.DOWN_PAYMENT.getValue(), execContext.getLanguage());
        if (busCodeTranslation != null) {
            downPaymentIndicator = DOWN_PAYMENT_HOLD;
            BilRulesUct bilRulesUct = bilRulesUctService.readBilRulesUct(
                    BilRules.DOWN_PAYMENT_CASH_ALLOCATION.getValue(), SEPARATOR_BLANK, SEPARATOR_BLANK,
                    SEPARATOR_BLANK);
            if (bilRulesUct != null && bilRulesUct.getBrtRuleCd() == CHAR_Y) {
                int downPaymentDay = Integer.parseInt(bilRulesUct.getBrtParmListTxt().substring(0, 3));
                if (downPaymentDay != SHORT_ZERO) {
                    ZonedDateTime displaceDate = DateRoutine.getAdjustedDate(dateService.currentDate(), false,
                            downPaymentDay, null);
                    if (bilEntryDt.compareTo(displaceDate) <= SHORT_ZERO) {
                        downPaymentIndicator = DOWN_PAYMENT_APPLY;
                    }
                }
            }
        }

        return downPaymentIndicator;
    }

}