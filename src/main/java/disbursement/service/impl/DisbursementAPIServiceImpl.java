package disbursement.service.impl;

import static billing.utils.BillingConstants.DEFAULT_DATE;
import static billing.utils.BillingConstants.ZERO_VALUE;
import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_10;
import static core.utils.CommonConstants.GENERATED_ID_LENGTH_8;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;
import static disbursement.utils.DisbursementConstants.COMPANY_LOCATION_NUMBER;
import static disbursement.utils.DisbursementConstants.DISBURSEMENT_RULE_CODE;

import java.time.ZonedDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import application.utils.service.MultiCurrencyService;
import billing.data.entity.BilBank;
import billing.data.repository.BilBankRepository;
import billing.utils.BillingConstants.AccountDescriptionCode;
import core.api.ExecContext;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.utils.CommonConstants;
import core.utils.DateRoutine;
import core.utils.IdGenerator;
import disbursement.data.entity.DwsDisbursement;
import disbursement.data.entity.DwsDsbClient;
import disbursement.data.entity.DwsIvtControl;
import disbursement.data.entity.DwsIvtDetail;
import disbursement.data.entity.DwsLocationCnv;
import disbursement.data.entity.DwsRule;
import disbursement.data.entity.DwsStatusHst;
import disbursement.data.entity.DwsTxt;
import disbursement.data.entity.id.DwsDsbClientId;
import disbursement.data.entity.id.DwsIvtDetailId;
import disbursement.data.entity.id.DwsRuleId;
import disbursement.data.entity.id.DwsStatusHstId;
import disbursement.data.entity.id.DwsTxtId;
import disbursement.data.repository.DwsDisbursementRepository;
import disbursement.data.repository.DwsDsbClientRepository;
import disbursement.data.repository.DwsIvtControlRepository;
import disbursement.data.repository.DwsIvtDetailRepository;
import disbursement.data.repository.DwsLocationCnvRepository;
import disbursement.data.repository.DwsRuleRepository;
import disbursement.data.repository.DwsStatusHstRepository;
import disbursement.data.repository.DwsTxtRepository;
import disbursement.service.DisbursementAPIService;
import disbursement.utils.DisbursementConstants.DisbursementIdType;
import disbursement.utils.DisbursementConstants.DisbursementMediumCode;
import disbursement.utils.DisbursementConstants.DisbursementStatusCode;
import disbursement.utils.DisbursementConstants.DisbursementType;
import disbursement.utils.DisbursementConstants.DisbursementTypeClientCode;
import disbursement.utils.DisbursementConstants.TextTypeCode;
import disbursement.utils.DisbursementConstants.UsageCode;
import fis.service.FinancialApiService;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants;
import fis.utils.FinancialIntegratorConstants.ActionCode;
import fis.utils.FinancialIntegratorConstants.ObjectCode;
import party.data.entity.ClientTab;
import party.data.repository.ClientTabRepository;

@Service
public class DisbursementAPIServiceImpl implements DisbursementAPIService {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private DwsDisbursementRepository dwsDisbursementRepository;

    @Autowired
    private DwsTxtRepository dwsTxtRepository;

    @Autowired
    private DwsDsbClientRepository dwsDsbClientRepository;

    @Autowired
    private DwsIvtDetailRepository dwsIvtDetailRepository;

    @Autowired
    private DwsIvtControlRepository dwsIvtControlRepository;

    @Autowired
    private DwsLocationCnvRepository dwsLocationCnvRepository;

    @Autowired
    private DwsRuleRepository dwsRuleRepository;

    @Autowired
    private FinancialApiService financialApiService;

    @Autowired
    private DwsStatusHstRepository dwsStatusHstRepository;

    @Autowired
    private BilBankRepository bilBankRepository;

    @Autowired
    private DateService dateService;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    @Autowired
    private ClientTabRepository clientTabRepository;

    private static final String APPLICATION_NAME = "DWSODAR";
    private static final String PRODUCT_ID = "DWS";
    private static final String MASTER_COMPANY = "99";
    private static final Integer SEQUENCE_ONE = 1;
    private static final Logger LOGGER = LogManager.getLogger(DisbursementAPIServiceImpl.class);

    @Override
    @Transactional
    public String saveDisbursementActivity(String folderId, String refundMethod, Character statusCode,
            String processingLocationCode, Integer checkNumber, String reason, String mediumCode,
            DisbursementIdType disbursementIdType, String dsbId, String clientId, Double amount, String memoText,
            String userMemoText, String dsbTypeCode, Short addressSequenceNumber, String technicalDisbursementId,
            String currencyCode) {
        LOGGER.debug("Save disbursement activity : Start");
        checkDwsRules("EXTAPPID", Character.toString(CHAR_B), BLANK_STRING);
        checkDwsRules("DSBTYPE", Character.toString(CHAR_B), dsbTypeCode);

        String printLocationCode = findPrintLocation(processingLocationCode, mediumCode, BLANK_STRING);
        if (printLocationCode == null || printLocationCode.trim().isEmpty()) {
            printLocationCode = processingLocationCode;
        }
        if (refundMethod.trim().equalsIgnoreCase(DisbursementType.MANUAL.getDisburseType())) {
            checkLocationCode(processingLocationCode, checkNumber, mediumCode);
        }
        String disbursementId = generateDisbursementId(technicalDisbursementId, disbursementIdType);

        DwsDisbursement dwsDisbursement = findBankInformation(printLocationCode, mediumCode);

        saveDwsDisbursement(dwsDisbursement, amount, dsbId, disbursementId, mediumCode, printLocationCode,
                execContext.getApplicationDateTime(), statusCode, processingLocationCode, dsbTypeCode, checkNumber,
                currencyCode);

        saveDisbursementText(disbursementId, TextTypeCode.USER_MEMO.toString(), userMemoText);

        saveDisbursementText(disbursementId, TextTypeCode.REASON.toString(), memoText);

        saveDisbursementClient(disbursementId, clientId, SEQUENCE_ONE, DisbursementTypeClientCode.PAYEE.toString(),
                (int) addressSequenceNumber);

        saveDisbursementClient(disbursementId, clientId, ZERO_VALUE, DisbursementTypeClientCode.MAILTO.toString(),
                (int) addressSequenceNumber);
        ClientTab userClient = clientTabRepository.findByUserId(execContext.getUserSeqeunceId(), (short) 0,
                DateRoutine.defaultDateTime());
        if (userClient == null) {
            saveDisbursementClient(disbursementId, execContext.getUserSeqeunceId(), ZERO_VALUE,
                    DisbursementTypeClientCode.ADJUSTER.toString(), ZERO_VALUE);
        } else {
            saveDisbursementClient(disbursementId, userClient.getClientTabId().getClientId(), ZERO_VALUE,
                    DisbursementTypeClientCode.ADJUSTER.toString(), ZERO_VALUE);
        }

        callFinancialApi(folderId, AccountDescriptionCode.DSB.toString(), ActionCode.RECEIVED.toString(), CHAR_N,
                dwsDisbursement.getDwsBankNbr(), dwsDisbursement.getDwsBankActNbr(), statusCode,
                generateBusinessCaseId(), disbursementId, amount, checkNumber, processingLocationCode, mediumCode,
                clientId, dsbTypeCode, addressSequenceNumber, dwsDisbursement.getDdsCurrencyCd());

        String disbursementIdTypeCode = dwsDisbursement.getDwsDsbId().substring(0, 4);
        if (refundMethod.trim().equalsIgnoreCase(DisbursementType.MANUAL.getDisburseType())) {
            LOGGER.info("updating current inventory row as history row.");
            DwsIvtDetail dwsInventoryDetail = dwsIvtDetailRepository
                    .getDisbursementInventoryDetail(processingLocationCode, mediumCode, checkNumber, ZERO_VALUE);
            dwsIvtDetailRepository.updateDisbursementInventoryDetail(processingLocationCode, mediumCode, checkNumber,
                    ZERO_VALUE, execContext.getApplicationDate(), SEQUENCE_ONE);
            if (dwsInventoryDetail != null) {
                LOGGER.info("inserting new inventory row with disbursed status.");
                saveInventoryDetail(dwsInventoryDetail, disbursementId);
            }
            callFinancialApi(folderId, AccountDescriptionCode.DSB.toString(), ActionCode.DISBUSRED_1.toString(), CHAR_Y,
                    dwsDisbursement.getDwsBankNbr(), dwsDisbursement.getDwsBankActNbr(), statusCode, BLANK_STRING,
                    disbursementId, amount, checkNumber, processingLocationCode, mediumCode, clientId, dsbTypeCode,
                    addressSequenceNumber, dwsDisbursement.getDdsCurrencyCd());
        } else if (refundMethod.trim().equalsIgnoreCase(DisbursementType.ELECTRONIC.getDisburseType())
                && (disbursementIdTypeCode.equalsIgnoreCase(DisbursementIdType.BACM.getDisbursementIdType())
                        || disbursementIdTypeCode.equalsIgnoreCase(DisbursementIdType.BEFM.getDisbursementIdType()))) {
            callFinancialApi(folderId, AccountDescriptionCode.DSB.toString(), ActionCode.DISBUSRED_1.toString(), CHAR_Y,
                    dwsDisbursement.getDwsBankNbr(), dwsDisbursement.getDwsBankActNbr(), statusCode, BLANK_STRING,
                    disbursementId, amount, checkNumber, processingLocationCode, mediumCode, clientId, dsbTypeCode,
                    addressSequenceNumber, dwsDisbursement.getDdsCurrencyCd());
        }
        LOGGER.debug("Save disbursement activity : End");
        return disbursementId;
    }

    private void saveInventoryDetail(DwsIvtDetail dwsIvtDetail, String disbursementId) {
        DwsIvtDetail dwsInventoryDetail = new DwsIvtDetail();
        DwsIvtDetailId dwsIvtDetailId = new DwsIvtDetailId(dwsIvtDetail.getDwsIvtDetailId().getDwsPrcLocCd(),
                dwsIvtDetail.getDwsIvtDetailId().getDwsMediumCd(), dwsIvtDetail.getDwsIvtDetailId().getDwsCkDrfNbr(),
                dwsIvtDetail.getDwsIvtDetailId().getDwsBankNbr(), dwsIvtDetail.getDwsIvtDetailId().getDwsBankActNbr(),
                dateService.currentDateTime());
        dwsInventoryDetail.setDwsIvtDetailId(dwsIvtDetailId);
        dwsInventoryDetail.setDwsDsbId(disbursementId);
        dwsInventoryDetail.setDwsHstVldNbr(ZERO_VALUE);
        dwsInventoryDetail.setUserId(execContext.getUserSeqeunceId());
        dwsInventoryDetail.setDwsUsageCd(UsageCode.DISBURSED.getUsageCd());
        dwsInventoryDetail.setDwsDsbBatchId(dwsIvtDetail.getDwsDsbBatchId());
        dwsInventoryDetail.setDwsHonoredDt(dwsIvtDetail.getDwsHonoredDt());
        dwsInventoryDetail.setDwsExpirationTs(dwsIvtDetail.getDwsExpirationTs());
        dwsInventoryDetail.setDdsPmtTraceNbr(dwsIvtDetail.getDdsPmtTraceNbr());
        dwsIvtDetailRepository.save(dwsInventoryDetail);
    }

    private DwsDisbursement saveDwsDisbursement(DwsDisbursement dwsDisbursement, Double amount, String dsbId,
            String disbursementId, String mediumCode, String printLocationCode, ZonedDateTime applicationDate,
            Character statusCode, String processingLocationCode, String dsbTypeCode, Integer checkNumber,
            String currencyCode) {
        LOGGER.debug("Save to dwsDisbursement : Start");
        dwsDisbursement.setBusUnitId(ZERO_VALUE);
        dwsDisbursement.setCipPolStateCd(BLANK_STRING);
        dwsDisbursement.setDdsCarRgsNbr(BLANK_STRING);
        dwsDisbursement.setDdsCmbPmtCd(BLANK_CHAR);
        String currency = multiCurrencyService.getCurrencyCode(currencyCode);
        dwsDisbursement.setDdsCurrencyCd(currency == null ? SEPARATOR_BLANK : currency);
        if (statusCode.equals(DisbursementStatusCode.PENDING.toChar())) {
            dwsDisbursement.setDdsDsbDt(DateRoutine.defaultDateTime());
        } else {
            dwsDisbursement.setDdsDsbDt(applicationDate);
        }
        dwsDisbursement.setDdsDsbTs(execContext.getApplicationDateTime());
        dwsDisbursement.setDdsDspAmt(amount);
        dwsDisbursement.setDdsGroupCkInd(CHAR_N);
        dwsDisbursement.setDdsOgnTs(execContext.getApplicationDateTime());
        dwsDisbursement.setDdsPmtTraceNbr(ZERO_VALUE);
        dwsDisbursement.setDdsSchDt(DateRoutine.defaultDateTime());
        dwsDisbursement.setDdsSchRulTypCd(BLANK_CHAR);
        dwsDisbursement.setDdsSpeHdlCd(BLANK_STRING);
        dwsDisbursement.setDdsUsrDsbId(dsbId);
        dwsDisbursement.setDdsUsrPolNbr(BLANK_STRING);
        dwsDisbursement.setDwsCkDrfNbr(checkNumber);
        dwsDisbursement.setDwsDsbId(disbursementId);
        dwsDisbursement.setDwsDsbTypeCd(dsbTypeCode);
        dwsDisbursement.setDwsDstOfficeNm(BLANK_STRING);
        dwsDisbursement.setDwsIntfBatchId(BLANK_STRING);
        dwsDisbursement.setDwsInvoiceNbr(BLANK_STRING);
        dwsDisbursement.setDwsLinOfBusCd(BLANK_STRING);
        dwsDisbursement.setDwsMediumCd(mediumCode);
        dwsDisbursement.setDwsMktSctCd(BLANK_STRING);
        dwsDisbursement.setDwsPrcLocCd(processingLocationCode);
        dwsDisbursement.setDwsPrintLocCd(printLocationCode);
        dwsDisbursement.setDwsProductCd(BLANK_STRING);
        dwsDisbursement.setDwsReferenceNbr(BLANK_STRING);
        dwsDisbursement.setDwsStatusCd(statusCode);
        dwsDisbursement.setMasterCompanyNbr(MASTER_COMPANY);
        dwsDisbursement.setRiskStateCd(BLANK_STRING);
        dwsDisbursement.setUserId(execContext.getUserSeqeunceId());
        dwsDisbursement.setDdsServiceFrmDt(DateRoutine.defaultSinceDateTime());
        dwsDisbursement.setDdsServiceToDt(DateRoutine.defaultDateTime());
        dwsDisbursementRepository.save(dwsDisbursement);
        LOGGER.debug("Save to dwsDisbursement : End");
        return dwsDisbursement;
    }

    private void saveDisbursementText(String disbursementId, String reasonTextType, String memoText) {
        LOGGER.debug("Save to dwsText : Start");
        DwsTxt disbursementText = new DwsTxt();
        disbursementText.setDwsTxtId(new DwsTxtId(disbursementId, reasonTextType));
        disbursementText.setDttDesTxtCd(BLANK_STRING);
        disbursementText.setDttDesTxt(memoText);
        dwsTxtRepository.saveAndFlush(disbursementText);
        LOGGER.debug("Save to dwsText : End");
    }

    private void saveDisbursementClient(String disbursementId, String payeeId, Integer sequenceNumber,
            String clientTypeCode, Integer addressSequenceNumber) {
        LOGGER.debug("Save to dwsClient : Start");
        DwsDsbClient disbursementClient = new DwsDsbClient();
        disbursementClient.setDwsDsbClientId(new DwsDsbClientId(disbursementId, clientTypeCode, sequenceNumber));
        disbursementClient.setAdrSeqNbr(addressSequenceNumber);
        disbursementClient.setClientId(payeeId);
        dwsDsbClientRepository.saveAndFlush(disbursementClient);
        LOGGER.debug("Save to dwsClient : End");
    }

    @Override
    public void checkLocationCode(String processingLocation, Integer checkNumber, String mediumCode) {
        Short count = dwsIvtControlRepository.findCheckNumber(processingLocation, checkNumber, mediumCode);
        BilBank bilBank = bilBankRepository.findById(processingLocation).orElse(null);
        if (count == null || count <= SHORT_ZERO) {
            DwsIvtControl inventoryControl = dwsIvtControlRepository.findCheckRange(processingLocation, mediumCode,
                    ZERO_VALUE);
            if (inventoryControl == null) {
                LOGGER.error("Check range not defined for processing location code");
                throw new InvalidDataException("check.detail.not.found");
            } else if (inventoryControl.getDwsCurBegCkNbr().equals(ZERO_VALUE)) {
                if (bilBank != null && !bilBank.getBbkAccountNm().trim().isEmpty()) {
                    processingLocation = bilBank.getBbkAccountNm().trim();
                }
                LOGGER.error("Check range is not set.");
                throw new InvalidDataException("check.range.not.set", new Object[] { processingLocation });
            } else {
                LOGGER.error("Disbursement Cannot be processed as check number is not found in DWS_IVT_CONTROL");
                throw new InvalidDataException("invalid.check.number", new Object[] { checkNumber,
                        inventoryControl.getDwsCurBegCkNbr(), inventoryControl.getDwsCurEnCkNbr() });
            }
        }
        DwsIvtDetail inventoryDetail = dwsIvtDetailRepository.getDisbursementInventoryDetail(processingLocation,
                mediumCode, checkNumber, ZERO_VALUE);
        if (inventoryDetail != null) {
            String checkUsageCode = inventoryDetail.getDwsUsageCd();
            if (!(checkUsageCode.equals(UsageCode.MANUAL_ALLOCATED.getUsageCd())
                    || checkUsageCode.equals(UsageCode.OFFSET_USAGE.getUsageCd())
                            && inventoryDetail.getDwsDsbId().equals(BLANK_STRING))) {
                LOGGER.error("Disbursement Cannot be processed as check number has already been used");
                throw new InvalidDataException("check.number.already.used", new Object[] { checkNumber });
            }
        } else {
            if (bilBank != null && !bilBank.getBbkAccountNm().trim().isEmpty()) {
                processingLocation = bilBank.getBbkAccountNm().trim();
            }
            LOGGER.error("Disbursement Cannot be processed as usage number is not found in DWS_IVT_DETAIL");
            throw new InvalidDataException("check.usage.not.found", new Object[] { processingLocation });
        }
    }

    public void callFinancialApi(String accountId, String transactionObjectCode, String transactionActionCode,
            Character financialIndicator, Integer bankNumber, String bankAccountNumber, Character statusCode,
            String businessCaseId, String disbursementId, Double amount, Integer checkNumber,
            String processingLocationCode, String mediumCode, String clientId, String typeCode,
            Short addressSequenceNumber, String currencyCd) {
        LOGGER.debug("Call to Financial Api Service: Start");
        FinancialApiActivity financialApiActivity = new FinancialApiActivity();
        financialApiActivity.setTransactionObjectCode(transactionObjectCode);
        financialApiActivity.setTransactionActionCode(transactionActionCode);
        financialApiActivity.setBusinessCaseId(businessCaseId);
        financialApiActivity.setFinancialIndicator(financialIndicator);
        financialApiActivity.setDisbursementId(disbursementId);
        financialApiActivity.setWorkNetMessageId(BLANK_STRING);
        financialApiActivity.setFunctionCode(CHAR_A);
        financialApiActivity.setFolderId(accountId);
        financialApiActivity.setReferenceId(BLANK_STRING);
        financialApiActivity.setApplicationName(APPLICATION_NAME);
        financialApiActivity.setUserId(execContext.getUserSeqeunceId());
        financialApiActivity.setErrorCode(BLANK_STRING);
        financialApiActivity.setApplicationProductIdentifier(PRODUCT_ID);
        financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY);
        financialApiActivity.setStateCode(BLANK_STRING);
        financialApiActivity.setCompanyLocationNbr(COMPANY_LOCATION_NUMBER);
        financialApiActivity.setEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setOperatorId(execContext.getUserSeqeunceId());
        financialApiActivity.setAppProgramId(APPLICATION_NAME);
        financialApiActivity.setCountryCode(BLANK_STRING);
        financialApiActivity.setLineOfBusCode(BLANK_STRING);
        financialApiActivity.setPolicyId(BLANK_STRING);
        financialApiActivity.setAgentId(BLANK_STRING);
        financialApiActivity.setActivityAmount(amount);
        financialApiActivity.setCurrencyCode(currencyCd);
        financialApiActivity.setCheckNumber(checkNumber);
        financialApiActivity.setBankNumber(bankNumber.shortValue());
        financialApiActivity.setBankAccountNumber(bankAccountNumber);
        financialApiActivity.setProcessingLocationCode(processingLocationCode);
        financialApiActivity.setDisbursementTypeCode(typeCode);
        financialApiActivity.setDwsProductId(BLANK_STRING);
        financialApiActivity.setMarketSectorCode(BLANK_STRING);
        financialApiActivity.setDistrictOfficeName(BLANK_STRING);
        financialApiActivity.setMediumCode(mediumCode);
        financialApiActivity.setPayee1Id(clientId);
        financialApiActivity.setPayee2Id(clientId);
        financialApiActivity.setPayee3Id(clientId);
        financialApiActivity.setMailRecipientId(clientId);
        financialApiActivity.setAddressSequenceNumber(addressSequenceNumber);
        financialApiActivity.setStatusCode(BLANK_CHAR);
        financialApiActivity.setNextCheckNumber(ZERO_VALUE);
        financialApiActivity.setCurrentBeginCheckNumber(ZERO_VALUE);
        financialApiActivity.setCurrentEndCheckNumber(ZERO_VALUE);
        financialApiActivity.setCurrentRangeDate(DEFAULT_DATE);
        financialApiActivity.setNextBeginCheckNumber(ZERO_VALUE);
        financialApiActivity.setNextEndCheckNumber(ZERO_VALUE);
        financialApiActivity.setNextRangeDate(DEFAULT_DATE);
        financialApiActivity
                .setDisbursementDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setCheckUsageCode(BLANK_STRING);
        financialApiActivity.setPreNextCheck(ZERO_VALUE);
        financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(execContext.getApplicationDate()));
        financialApiActivity.setAgentTtyId(BLANK_STRING);
        financialApiActivity.setClientId(BLANK_STRING);
        financialApiActivity.setCountyCode(BLANK_STRING);
        financialApiActivity.setCustomerInfo1(BLANK_STRING);
        financialApiActivity.setCustomerInfo2(BLANK_STRING);
        financialApiActivity.setCustomerInfo3(BLANK_STRING);
        financialApiActivity.setCustomerInfo4(BLANK_STRING);
        financialApiActivity.setCustomerInfo5(BLANK_STRING);
        financialApiActivity.setDwsStatusCode(statusCode);
        financialApiActivity.setSummaryEffectiveDate(DateRoutine.defaultDateTime());
        String[] fwsReturnMessage = null;
        fwsReturnMessage = financialApiService.processFWSFunction(financialApiActivity,
                execContext.getApplicationDateTime());
        LOGGER.debug("Call to Financial Api Service: End");
        if (fwsReturnMessage[1] != null && !fwsReturnMessage[1].trim().isEmpty()) {
            LOGGER.error("Error Occured in FinancialApi while writing DWS_BUS_ACTIVITY");
            throw new InvalidDataException(fwsReturnMessage[1]);
        }

    }

    public String findPrintLocation(String processingLocationCode, String mediumCode, String typeCode) {
        String printLocationCode = null;
        DwsLocationCnv locationConversion = dwsLocationCnvRepository.findDwsLocationCnv(processingLocationCode,
                mediumCode);
        // Location type code check
        if (locationConversion != null
                && (locationConversion.getDwsLocationCnvId().getDwsDspTypCd().trim().equals(typeCode)
                        || locationConversion.getDwsLocationCnvId().getDwsDspTypCd().trim()
                                .equals(DisbursementMediumCode.ALLMEDIUMS.toString()))
                && (locationConversion.getDwsLocationCnvId().getRiskStateCd().trim().equals(BLANK_STRING)
                        || locationConversion.getDwsLocationCnvId().getRiskStateCd().trim().equals("ZZZ"))
                && (locationConversion.getDwsLocationCnvId().getMasterCompanyNbr().trim().equals(MASTER_COMPANY)
                        || locationConversion.getDwsLocationCnvId().getMasterCompanyNbr().trim().equals(BLANK_STRING))
                && locationConversion.getDwsLocationCnvId().getLobCd().trim().equals(BLANK_STRING)) {
            printLocationCode = locationConversion.getDwsPrintLocCd();
        }
        return printLocationCode;
    }

    public void checkDwsRules(String ruleTypeCode, String ruleCode, String secondaryRuleCode) {
        DwsRule dwsRule = dwsRuleRepository.findById(new DwsRuleId(ruleTypeCode, ruleCode, secondaryRuleCode))
                .orElse(null);
        if (dwsRule == null) {
            LOGGER.error("{} rule not found in DWS_RULE", ruleCode);
            throw new InvalidDataException("application.not.supported", new Object[] { ruleCode });
        }
    }

    public DwsDisbursement findBankInformation(String printLocationCode, String mediumCode) {
        DwsDisbursement dwsDisbursement = new DwsDisbursement();
        DwsIvtControl inventoryControl = dwsIvtControlRepository
                .findByDwsIvtControlIdDwsPrcLocCdAndDwsIvtControlIdDwsMediumCdAndDwsHstVldNbr(printLocationCode,
                        mediumCode, ZERO_VALUE);
        if (inventoryControl != null) {
            dwsDisbursement.setDwsBankNbr(inventoryControl.getDwsIvtControlId().getDwsBankNbr());
            dwsDisbursement.setDwsBankActNbr(inventoryControl.getDwsIvtControlId().getDwsBankActNbr());
        } else {
            LOGGER.error("Bank Information not found in DWS_IVT_CONTROL");
            throw new DataNotFoundException("bank.info.not.found");
        }

        return dwsDisbursement;
    }

    @Override
    public String generateDisbursementId(String technicalDisbursementId, DisbursementIdType disbursementIdType) {
        String disbursementId = IdGenerator.generateUniqueIdUpperCase(GENERATED_ID_LENGTH_8);
        StringBuilder sb = new StringBuilder();
        sb.append(disbursementIdType.getDisbursementIdType());
        sb.append(technicalDisbursementId);
        sb.append(disbursementId);
        return sb.toString();
    }

    @Override
    public void cancelDisbursement(String folderId, String clientId, String reason, String mediumCode,
            String processingLocationCode, Double amount, Integer checkNumber, String disbursementId, String typeCode,
            Short addressSequenceNumber) {
        LOGGER.debug("Cancel disbursement activity : Start");
        checkDwsRules("EXTAPPID", DISBURSEMENT_RULE_CODE, BLANK_STRING);
        checkDwsRules("DSBTYPE", DISBURSEMENT_RULE_CODE, typeCode);
        findPrintLocation(processingLocationCode, mediumCode, BLANK_STRING);
        DwsDisbursement dwsDisbursement = checkDisbursement(disbursementId);
        saveDisbursementHistory(disbursementId, dwsDisbursement.getDwsStatusCd());
        updateDisbursement(dwsDisbursement, DisbursementStatusCode.CANCEL.toChar());
        saveDisbursementText(disbursementId, TextTypeCode.REASON.toString(), reason);
        callFinancialApi(folderId, ObjectCode.DISBURSEMENT.toString(), ActionCode.CANCELLED.toString(), CHAR_N,
                dwsDisbursement.getDwsBankNbr(), dwsDisbursement.getDwsBankActNbr(), dwsDisbursement.getDwsStatusCd(),
                generateBusinessCaseId(), disbursementId, amount * -1, checkNumber, processingLocationCode, mediumCode,
                clientId, typeCode, addressSequenceNumber, dwsDisbursement.getDdsCurrencyCd());
        LOGGER.debug("Cancel disbursement activity : End");
    }

    private void saveDisbursementHistory(String disbursementId, Character statusCode) {
        DwsStatusHst dwsStatusHst = new DwsStatusHst();
        dwsStatusHst.setDwsStatusHstId(new DwsStatusHstId(disbursementId, execContext.getApplicationDateTime()));
        dwsStatusHst.setDwsPreStatusCd(statusCode);
        dwsStatusHst.setUserId(execContext.getUserSeqeunceId());
        dwsStatusHstRepository.save(dwsStatusHst);
    }

    private void updateDisbursement(DwsDisbursement dwsDisbursement, Character statusCd) {
        dwsDisbursement.setDdsDsbTs(execContext.getApplicationDateTime());
        dwsDisbursement.setDwsStatusCd(statusCd);
        dwsDisbursement.setUserId(execContext.getUserSeqeunceId());
        dwsDisbursementRepository.save(dwsDisbursement);
    }

    private String generateBusinessCaseId() {
        String businessCaseId = IdGenerator.generateUniqueIdUpperCase(GENERATED_ID_LENGTH_10);
        StringBuilder sb = new StringBuilder(20);
        sb.append(businessCaseId);
        sb.append(CommonConstants.SEPARATOR_BLANK);
        sb.append(CommonConstants.SEPARATOR_BLANK);
        sb.append(FinancialIntegratorConstants.ApplicationName.BCMSDISB.toString());
        return sb.toString();
    }

    @Override
    public DwsDisbursement checkDisbursement(String disbursementId) {
        LOGGER.debug("Verify disbursement activity : Start");
        DwsDisbursement dwsDisbursement = dwsDisbursementRepository.findByDwsDsbId(disbursementId);
        if (dwsDisbursement == null) {
            disbursementId = DISBURSEMENT_RULE_CODE.concat(disbursementId);
            dwsDisbursement = dwsDisbursementRepository.findByDwsDsbId(disbursementId);
            if (dwsDisbursement == null) {
                throw new DataNotFoundException("disbursement.information.not.found");
            }
        }
        if (dwsDisbursement.getDwsStatusCd() == DisbursementStatusCode.CANCEL.toChar()
                || dwsDisbursement.getDwsStatusCd() == DisbursementStatusCode.OFFSETFORONSET.toChar()) {

            throw new InvalidDataException("cancel.disbursement.cannot.process");
        }
        LOGGER.debug("Verify disbursement activity : End");
        return dwsDisbursement;
    }

    @Override
    public boolean voidDisbursement(String disbursementId, Character statusCode, String reason) {
        LOGGER.debug("Void disbursement activity : Start");
        DwsDisbursement dwsDisbursement = dwsDisbursementRepository.findByDwsDsbId(disbursementId);
        if (dwsDisbursement == null) {
            throw new DataNotFoundException("disbursement.information.not.found");
        } else {
            saveDisbursementHistory(dwsDisbursement.getDwsDsbId(), dwsDisbursement.getDwsStatusCd());
            updateDisbursement(dwsDisbursement, statusCode);
            DwsTxt dwsTxt = dwsTxtRepository.findById(new DwsTxtId(disbursementId, TextTypeCode.REASON.toString()))
                    .orElse(null);
            if (dwsTxt != null) {
                dwsTxt.setDttDesTxtCd(BLANK_STRING);
                dwsTxt.setDttDesTxt(reason);
                dwsTxtRepository.save(dwsTxt);
            }
        }
        LOGGER.debug("Void disbursement activity : End");
        return true;
    }
}