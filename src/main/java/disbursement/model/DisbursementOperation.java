package disbursement.model;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DisbursementOperation extends ResourceSupport {

    private String disbursementId;
    private String type;
    private String mediumCode;
    private Character combinedPaymentCode;
    private Character status;
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime disbursementDate;
    private Integer checkNumber;
    private Double amount;

    public DisbursementOperation() {
        super();
    }

    @JsonCreator
    public DisbursementOperation(@JsonProperty("disbursementId") String disbursementId,
            @JsonProperty("type") String type, @JsonProperty("mediumCode") String mediumCode,
            @JsonProperty("combinedPaymentCode") Character combinedPaymentCode,
            @JsonProperty("status") Character status, @JsonProperty("disbursementDate") ZonedDateTime disbursementDate,
            @JsonProperty("checkNumber") Integer checkNumber, @JsonProperty("amount") Double amount) {
        super();
        this.disbursementId = disbursementId;
        this.type = type;
        this.mediumCode = mediumCode;
        this.combinedPaymentCode = combinedPaymentCode;
        this.status = status;
        this.disbursementDate = disbursementDate;
        this.checkNumber = checkNumber;
        this.amount = amount;
    }

    public String getDisbursementId() {
        return disbursementId;
    }

    public void setDisbursementId(String disbursementId) {
        this.disbursementId = disbursementId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public ZonedDateTime getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(ZonedDateTime disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public Integer getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(Integer checkNumber) {
        this.checkNumber = checkNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMediumCode() {
        return mediumCode;
    }

    public void setMediumCode(String mediumCode) {
        this.mediumCode = mediumCode;
    }

    public Character getCombinedPaymentCode() {
        return combinedPaymentCode;
    }

    public void setCombinedPaymentCode(Character combinedPaymentCode) {
        this.combinedPaymentCode = combinedPaymentCode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(amount, checkNumber, combinedPaymentCode, disbursementDate,
                disbursementId, mediumCode, status, type);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        DisbursementOperation other = (DisbursementOperation) obj;
        return Objects.equals(amount, other.amount) && Objects.equals(checkNumber, other.checkNumber)
                && Objects.equals(combinedPaymentCode, other.combinedPaymentCode)
                && Objects.equals(disbursementDate, other.disbursementDate)
                && Objects.equals(disbursementId, other.disbursementId) && Objects.equals(mediumCode, other.mediumCode)
                && Objects.equals(status, other.status) && Objects.equals(type, other.type);
    }

}
