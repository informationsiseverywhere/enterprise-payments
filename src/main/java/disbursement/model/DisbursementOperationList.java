package disbursement.model;

import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DisbursementOperationList extends ResourceSupport {

    private List<DisbursementOperation> disbursementOperationList;

    public DisbursementOperationList() {
        super();
    }

    @JsonCreator
    public DisbursementOperationList(
            @JsonProperty("disbursementOperationList") List<DisbursementOperation> disbursementOperationList) {
        super();
        this.disbursementOperationList = disbursementOperationList;
    }

    public List<DisbursementOperation> getDisbursementOperationList() {
        return disbursementOperationList;
    }

    public void setDisbursementOperationList(List<DisbursementOperation> disbursementOperationList) {
        this.disbursementOperationList = disbursementOperationList;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(disbursementOperationList);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        DisbursementOperationList other = (DisbursementOperationList) obj;
        return Objects.equals(disbursementOperationList, other.disbursementOperationList);
    }

}
