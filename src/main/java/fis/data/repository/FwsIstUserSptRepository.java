package fis.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fis.data.entity.FwsIstUserSpt;
import fis.data.entity.id.FwsIstUserSptId;

public interface FwsIstUserSptRepository extends JpaRepository<FwsIstUserSpt, FwsIstUserSptId> {

    @Query("select b from FwsIstUserSpt b where b.fwsIstUserSptId.busaAppProdId = :busaAppProdId and b.fwsIstUserSptId.fwsAppDataCd IN :fwsAppDataCdList and "
            + "b.fwsIstUserSptId.busaHistoryInd = :busaHistoryInd and b.fwsIstUserSptId.busaExpirationDt = :busaExpirationDt and b.fwsIstUserSptId.busaEffectiveTs <= :busaEffectiveTs order by b.fwsIstUserSptId.busaEffectiveTs desc")
    public List<FwsIstUserSpt> findByMaxEffectiveTs(@Param("busaAppProdId") String busaAppProdId,
            @Param("fwsAppDataCdList") List<String> fwsAppDataCdList, @Param("busaHistoryInd") short busaHistoryInd,
            @Param("busaExpirationDt") ZonedDateTime busaExpirationDt,
            @Param("busaEffectiveTs") ZonedDateTime busaEffectiveTs, Pageable pageable);

    @Query(value = "Select b from FwsIstUserSpt b where b.fwsIstUserSptId.busaAppProdId = :busaAppProdId "
            + " and b.fwsGliPtpInd = :fwsGliPtpInd ")
    public List<FwsIstUserSpt> checkMisInstalled(@Param("busaAppProdId") String busaAppProdId,
            @Param("fwsGliPtpInd") char fwsGliPtpInd);

    @Query(value = "Select b from FwsIstUserSpt b where b.fwsIstUserSptId.busaAppProdId = :busaAppProdId and b.fwsIstUserSptId.busaExpirationDt = :busaExpirationDt and b.fwsIstUserSptId.busaHistoryInd = :busaHistoryInd "
            + "and b.fwsIstUserSptId.busaEffectiveTs <= :busaEffectiveTs order by b.fwsIstUserSptId.busaEffectiveTs desc")
    public List<FwsIstUserSpt> checkForUserExit(@Param("busaAppProdId") String busaAppProdId,
            @Param("busaExpirationDt") ZonedDateTime busaExpirationDt, @Param("busaHistoryInd") short busaHistoryInd,
            @Param("busaEffectiveTs") ZonedDateTime busaEffectiveTs, Pageable pageable);

    @Query(value = "Select b from FwsIstUserSpt b where b.fwsIstUserSptId.busaAppProdId = :busaAppProdId ")
    public List<FwsIstUserSpt> checkFwsInstalled(@Param("busaAppProdId") String busaAppProdId);

    @Query("Select b from FwsIstUserSpt b where b.fwsIstUserSptId.busaExpirationDt = :busaExpirationDate and b.fwsIstUserSptId.busaHistoryInd = :busaHistoryInd "
            + " and b.fwsSumPtpInd <> :fwsSumPtpInd and b.fwsIstUserSptId.fwsAppDataCd IN :fwsAppDataCodeList and "
            + " b.fwsIstUserSptId.busaEffectiveTs = (Select MAX(c.fwsIstUserSptId.busaEffectiveTs) from FwsIstUserSpt c where c.fwsIstUserSptId.fwsAppDataCd IN :fwsAppDataCodeList "
            + " and c.fwsSumPtpInd  <> :fwsSumPtpInd and c.fwsIstUserSptId.busaEffectiveTs <= :busaEffectiveTime and "
            + " c.fwsIstUserSptId.busaExpirationDt = :busaExpirationDate and c.fwsIstUserSptId.busaHistoryInd = :busaHistoryInd) "
            + " order by b.fwsIstUserSptId.busaAppProdId, b.fwsIstUserSptId.fwsAppDataCd ")
    public List<FwsIstUserSpt> findSummarizationData(@Param("busaExpirationDate") ZonedDateTime busaExpirationDate,
            @Param("busaHistoryInd") short busaHistoryInd, @Param("fwsSumPtpInd") char fwsSumPtpInd,
            @Param("fwsAppDataCodeList") List<String> fwsAppDataCodeList,
            @Param("busaEffectiveTime") ZonedDateTime busaEffectiveTime);

    @Query(value = "select b from FwsIstUserSpt b where b.fwsIstUserSptId.fwsAppDataCd IN :fwsAppDataCdList and b.fwsSumPtpInd <> :fwsSumPtpInd "
            + "and b.fwsIstUserSptId.busaHistoryInd = :busaHistoryInd and b.fwsIstUserSptId.busaExpirationDt = :busaExpirationDt and b.fwsIstUserSptId.busaEffectiveTs = "
            + "(select max(c.fwsIstUserSptId.busaEffectiveTs) from FwsIstUserSpt c where c.fwsIstUserSptId.fwsAppDataCd IN :fwsAppDataCdList and c.fwsSumPtpInd <> :fwsSumPtpInd and "
            + "c.fwsIstUserSptId.busaHistoryInd = :busaHistoryInd and c.fwsIstUserSptId.busaExpirationDt = :busaExpirationDt and c.fwsIstUserSptId.busaEffectiveTs <= :busaEffectiveTs) "
            + "order by b.fwsIstUserSptId.busaAppProdId,b.fwsIstUserSptId.fwsAppDataCd")
    public List<FwsIstUserSpt> findByMaxTsAndSumPtpIndicator(@Param("busaEffectiveTs") ZonedDateTime busaEffectiveTs,
            @Param("busaExpirationDt") ZonedDateTime busaExpirationDt, @Param("busaHistoryInd") short busaHistoryInd,
            @Param("fwsSumPtpInd") char fwsSumPtpInd, @Param("fwsAppDataCdList") List<String> fwsAppDataCdList,
            Pageable pageable);

    @Query("Select b from FwsIstUserSpt b where b.fwsIstUserSptId.fwsAppDataCd IN :fwsAppDataCdList and b.fwsIstUserSptId.busaHistoryInd = :busaHistoryInd "
            + " and b.fwsIstUserSptId.busaExpirationDt = :busaExpirationDate and b.fwsIstUserSptId.busaEffectiveTs = "
            + " (Select Max(c.fwsIstUserSptId.busaEffectiveTs) from FwsIstUserSpt c where c.fwsIstUserSptId.fwsAppDataCd IN :fwsAppDataCdList and "
            + " c.fwsIstUserSptId.busaHistoryInd = :busaHistoryInd and c.fwsIstUserSptId.busaExpirationDt = :busaExpirationDate and c.fwsIstUserSptId.busaEffectiveTs <= :busaEffectiveTime)"
            + " order by b.fwsIstUserSptId.busaAppProdId")
    public List<FwsIstUserSpt> fetchPostingInstalledApps(@Param("fwsAppDataCdList") List<String> fwsAppDataCdList,
            @Param("busaHistoryInd") short busaHistoryInd,
            @Param("busaExpirationDate") ZonedDateTime busaExpirationDate,
            @Param("busaEffectiveTime") ZonedDateTime busaEffectiveTime);

}
