package fis.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fis.data.entity.BcmBusActivity;
import fis.data.entity.id.BcmBusActivityId;

public interface BcmBusActivityRepository extends JpaRepository<BcmBusActivity, BcmBusActivityId> {

}
