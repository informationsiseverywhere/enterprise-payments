package fis.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fis.data.entity.DwsBusActivity;
import fis.data.entity.id.DwsBusActivityId;

public interface DwsBusActivityRepository extends JpaRepository<DwsBusActivity, DwsBusActivityId> {

    @Query(value = "Select d From DwsBusActivity d Where d.busaEffectiveDt <= :busaEffectiveDate and "
            + " d.dwsBusActivityId.busaTimestamp between :firstBusaTimestamp and :secondBusaTimestamp ")
    public List<DwsBusActivity> checkSummaryDwsExist(@Param("busaEffectiveDate") String busaEffectiveDate,
            @Param("firstBusaTimestamp") ZonedDateTime firstBusaTimestamp,
            @Param("secondBusaTimestamp") ZonedDateTime secondBusaTimestamp);

    @Query(value = "Select c From DwsBusActivity c Where c.dwsBusActivityId.busaTransAtnCd = :busaTransAtnCode and c.dwsBusActivityId.busaTransObjCd = :busaTransObjCode "
            + " and c.dwsBusActivityId.busaTimestamp <= :firstBusaTimestamp and c.dwsBusActivityId.busaTimestamp >= :secondBusaTimestamp and "
            + " c.busaEffectiveDt <= :busaEffectiveDate and c.dwsBusActivityId.busaAppProdId = :busaAppProdId and "
            + " c.dwsBusActivityId.masterCompanyNbr = :masterCompanyNumber and c.dwsBusActivityId.cmpLocationNbr = :cmpLocationNumber")
    public List<DwsBusActivity> fetchDwsPrimeData(@Param("busaTransAtnCode") String busaTransAtnCode,
            @Param("busaTransObjCode") String busaTransObjCode,
            @Param("firstBusaTimestamp") ZonedDateTime firstBusaTimestamp,
            @Param("secondBusaTimestamp") ZonedDateTime secondBusaTimestamp,
            @Param("busaEffectiveDate") String busaEffectiveDate, @Param("busaAppProdId") String busaAppProdId,
            @Param("masterCompanyNumber") String masterCompanyNumber,
            @Param("cmpLocationNumber") String cmpLocationNumber);

    @Modifying
    @Query(value = "Update DwsBusActivity b Set b.bussSumEffDt = :bussSumEffDateChange Where b.dwsBusActivityId.busaFinancialInd = :busaFinancialInd and "
            + " b.busaHistoryInd = :busaHistoryInd and b.busaExpirationDt =:busaExpirationDate and b.busaStatusCd IN :busaStatusCodeList "
            + " and b.bussSumEffDt = :bussSumEffDateOrg and  b.dwsBusActivityId.busaTransAtnCd=:busaTransAtnCode and b.dwsBusActivityId.busaTransObjCd = :busaTransObjCode and "
            + " b.dwsBusActivityId.busaTimestamp = :busaTimestamp and b.dwsBusActivityId.busaAppProdId =:busaAppProdId and b.dwsBusActivityId.masterCompanyNbr=:masterCompanyNbr and "
            + " b.dwsBusActivityId.cmpLocationNbr = :cmpLocationNbr")
    public Integer updateSummaryEffectiveDate(@Param("bussSumEffDateChange") ZonedDateTime bussSumEffDateChange,
            @Param("busaFinancialInd") char busaFinancialInd, @Param("busaHistoryInd") short busaHistoryInd,
            @Param("busaExpirationDate") ZonedDateTime busaExpirationDate,
            @Param("busaStatusCodeList") List<Character> busaStatusCodeList,
            @Param("bussSumEffDateOrg") ZonedDateTime bussSumEffDateOrg,
            @Param("busaTransAtnCode") String busaTransAtnCode, @Param("busaTransObjCode") String busaTransObjCode,
            @Param("busaTimestamp") ZonedDateTime busaTimestamp, @Param("busaAppProdId") String busaAppProdId,
            @Param("masterCompanyNbr") String masterCompanyNbr, @Param("cmpLocationNbr") String cmpLocationNbr);

}
