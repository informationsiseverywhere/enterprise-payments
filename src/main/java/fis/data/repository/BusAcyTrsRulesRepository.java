package fis.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fis.data.entity.BusAcyTrsRules;
import fis.data.entity.id.BusAcyTrsRulesId;

public interface BusAcyTrsRulesRepository extends JpaRepository<BusAcyTrsRules, BusAcyTrsRulesId> {

    @Query("select count(b) from BusAcyTrsRules b where b.busAcyTrsRulesId.masterCompanyNumber In :masterCompanyNumberList and b.busAcyTrsRulesId.cmpLocationNbr IN :cmpLocationNbrList and "
            + "b.busAcyTrsRulesId.busaAppProdId = :busaAppProdId and b.busAcyTrsRulesId.busaTransObjCd = :busaTransObjCd and b.busAcyTrsRulesId.busaTransAtnCd = :busaTransAtnCd "
            + "and b.busaHistoryInd = :busaHistoryInd and b.busrExpirationDt = :busrExpirationDt and b.busrAudOnlyInd = :busrAudOnlyInd ")
    public Short checkForExistence(@Param("masterCompanyNumberList") List<String> masterCompanyNumberList,
            @Param("cmpLocationNbrList") List<String> cmpLocationNbrList, @Param("busaAppProdId") String busaAppProdId,
            @Param("busaTransObjCd") String busaTransObjCd, @Param("busaTransAtnCd") String busaTransAtnCd,
            @Param("busaHistoryInd") short busaHistoryInd, @Param("busrExpirationDt") ZonedDateTime busrExpirationDt,
            @Param("busrAudOnlyInd") char busrAudOnlyInd);

    @Query("select count(b) from BusAcyTrsRules b where b.busAcyTrsRulesId.masterCompanyNumber In :masterCompanyNumberList and b.busAcyTrsRulesId.cmpLocationNbr IN :cmpLocationNbrList and "
            + "b.busAcyTrsRulesId.busaAppProdId = :busaAppProdId and b.busAcyTrsRulesId.busaTransObjCd = :busaTransObjCd and b.busAcyTrsRulesId.busaTransAtnCd = :busaTransAtnCd "
            + "and b.busaHistoryInd = :busaHistoryInd and b.busrExpirationDt = :busrExpirationDt and b.busrAudOnlyInd = :busrAudOnlyInd and (b.busrPostingInd IN :busrPostingIndList OR b.busrSummarizeInd In :busrSummarizeIndList)")
    public Short checkForExistence2(@Param("masterCompanyNumberList") List<String> masterCompanyNumberList,
            @Param("cmpLocationNbrList") List<String> cmpLocationNbrList, @Param("busaAppProdId") String busaAppProdId,
            @Param("busaTransObjCd") String busaTransObjCd, @Param("busaTransAtnCd") String busaTransAtnCd,
            @Param("busaHistoryInd") short busaHistoryInd, @Param("busrExpirationDt") ZonedDateTime busrExpirationDt,
            @Param("busrAudOnlyInd") char busrAudOnlyInd,
            @Param("busrPostingIndList") List<Character> busrPostingIndList,
            @Param("busrSummarizeIndList") List<Character> busrSummarizeIndList);

    @Query("select b from BusAcyTrsRules b where b.busAcyTrsRulesId.masterCompanyNumber In :masterCompanyNumberList and b.busAcyTrsRulesId.cmpLocationNbr IN :cmpLocationNbrList and "
            + "b.busAcyTrsRulesId.busaAppProdId = :productIdentifier and b.busAcyTrsRulesId.busaTransObjCd = :transactionObjectCode and b.busAcyTrsRulesId.busaTransAtnCd = :transactionActionCode and b.busaHistoryInd = :busaHistoryInd and "
            + "b.busrExpirationDt = :defaultDate")
    public List<BusAcyTrsRules> findDataForEditRecords(
            @Param("masterCompanyNumberList") List<String> masterCompanyNumberList,
            @Param("cmpLocationNbrList") List<String> cmpLocationNbrList,
            @Param("transactionObjectCode") String transactionObjectCode,
            @Param("transactionActionCode") String transactionActionCode,
            @Param("productIdentifier") String productIdentifier, @Param("busaHistoryInd") short busaHistoryInd,
            @Param("defaultDate") ZonedDateTime defaultDate);

    @Query("select count(b) from BusAcyTrsRules b where b.busAcyTrsRulesId.masterCompanyNumber In :masterCompanyNumberList and b.busAcyTrsRulesId.cmpLocationNbr IN :cmpLocationNbrList and "
            + "b.busAcyTrsRulesId.busaAppProdId = :productIdentifier and b.busAcyTrsRulesId.busaTransObjCd = :transactionObjectCode and b.busAcyTrsRulesId.busaTransAtnCd = :transactionActionCode and b.busaHistoryInd = :busaHistoryInd and "
            + "b.busrExpirationDt = :defaultDate and b.busAcyTrsRulesId.busrDataItem =:dataItem and b.busrAudOnlyInd in :indicatorList or b. busrPostingInd in :indicatorList "
            + "or b.busrCostCenInd in :indicatorList or b.busrSummarizeInd in :indicatorList")
    public Short findDataForEditNumeric(@Param("masterCompanyNumberList") List<String> masterCompanyNumberList,
            @Param("cmpLocationNbrList") List<String> cmpLocationNbrList,
            @Param("transactionObjectCode") String transactionObjectCode,
            @Param("transactionActionCode") String transactionActionCode,
            @Param("productIdentifier") String productIdentifier, @Param("busaHistoryInd") short busaHistoryInd,
            @Param("defaultDate") ZonedDateTime defaultDate, @Param("dataItem") double dataItem,
            @Param("indicatorList") List<Character> indicatorList);

    @Query("Select b From BusAcyTrsRules b Where b.busAcyTrsRulesId.busaAppProdId = :busaAppProdId and  (b.busAcyTrsRulesId.masterCompanyNumber Between :firstMasterCompany And :secondMasterCompany) And "
            + "(b.busAcyTrsRulesId.cmpLocationNbr Between :firstCmpLocation And :secondCmpLocation) And (b.busAcyTrsRulesId.busaTransObjCd Between :firstTransObjCode And :secondTransObjCode) And "
            + "(b.busAcyTrsRulesId.busaTransAtnCd Between :firstTransAtnCode And :secondTransAtnCode) And b.busaHistoryInd = :busaHistoryInd And "
            + " b.busrExpirationDt = :busrExpirationDate And (b.busrPostingInd IN :busrPostingIndList OR b.busrCostCenInd In :busrCostCenIndList OR b.busrSummarizeInd In :busrSummarizeIndList) "
            + " Order by b.busAcyTrsRulesId.busaAppProdId, b.busAcyTrsRulesId.busaTransObjCd, b.busAcyTrsRulesId.busaTransAtnCd")
    public List<BusAcyTrsRules> fetchRulesPrimeData(@Param("busaAppProdId") String busaAppProdId,
            @Param("firstMasterCompany") String firstMasterCompany,
            @Param("secondMasterCompany") String secondMasterCompany,
            @Param("firstCmpLocation") String firstCmpLocation, @Param("secondCmpLocation") String secondCmpLocation,
            @Param("firstTransObjCode") String firstTransObjCode,
            @Param("secondTransObjCode") String secondTransObjCode,
            @Param("firstTransAtnCode") String firstTransAtnCode,
            @Param("secondTransAtnCode") String secondTransAtnCode, @Param("busaHistoryInd") short busaHistoryInd,
            @Param("busrExpirationDate") ZonedDateTime busrExpirationDate,
            @Param("busrPostingIndList") List<Character> busrPostingIndList,
            @Param("busrCostCenIndList") List<Character> busrCostCenIndList,
            @Param("busrSummarizeIndList") List<Character> busrSummarizeIndList);
}
