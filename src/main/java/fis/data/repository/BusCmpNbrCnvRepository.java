package fis.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fis.data.entity.BusCmpNbrCnv;
import fis.data.entity.id.BusCmpNbrCnvId;

public interface BusCmpNbrCnvRepository extends JpaRepository<BusCmpNbrCnv, BusCmpNbrCnvId> {

}
