package fis.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import fis.data.entity.id.FwsIstUserSptId;

@Entity
@Table(name = "FWS_IST_USER_SPT")
public class FwsIstUserSpt implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private FwsIstUserSptId fwsIstUserSptId;

    @Column(name = "FWS_WS_API_PTP_IND", nullable = false)
    private char fwsWsApiPtpInd = BLANK_CHAR;

    @Column(name = "FWS_HS_API_PTP_IND", nullable = false)
    private char fwsHsApiPtpInd = BLANK_CHAR;

    @Column(name = "FWS_BT_API_PTP_IND", nullable = false)
    private char fwsBtApiPtpInd = BLANK_CHAR;

    @Column(name = "FWS_RPT_PTP_IND", nullable = false)
    private char fwsRptPtpInd = BLANK_CHAR;

    @Column(name = "FWS_SUM_PTP_IND", nullable = false)
    private char fwsSumPtpInd = BLANK_CHAR;

    @Column(name = "FWS_CCR_PTP_IND", nullable = false)
    private char fwsCcrPtpInd = BLANK_CHAR;

    @Column(name = "FWS_RT_BAL_PTP_IND", nullable = false)
    private char fwsRtBalPtpInd = BLANK_CHAR;

    @Column(name = "FWS_GLI_PTP_IND", nullable = false)
    private char fwsGliPtpInd = BLANK_CHAR;

    @Column(name = "FWS_ERR_MSG_IND", nullable = false)
    private char fwsErrMsgInd = BLANK_CHAR;

    @Column(name = "FWS_DEBUG_CD", nullable = false)
    private char fwsDebugCd = BLANK_CHAR;

    @Column(name = "FWS_APP_DATA_PARMS", length = 100, nullable = false)
    private String fwsAppDataParams = BLANK_STRING;

    public FwsIstUserSpt() {
        super();
    }

    public FwsIstUserSpt(FwsIstUserSptId fwsIstUserSptId, char fwsWsApiPtpInd, char fwsHsApiPtpInd, char fwsBtApiPtpInd,
            char fwsRptPtpInd, char fwsSumPtpInd, char fwsCcrPtpInd, char fwsRtBalPtpInd, char fwsGliPtpInd,
            char fwsErrMsgInd, char fwsDebugCd, String fwsAppDataParams) {
        super();
        this.fwsIstUserSptId = fwsIstUserSptId;
        this.fwsWsApiPtpInd = fwsWsApiPtpInd;
        this.fwsHsApiPtpInd = fwsHsApiPtpInd;
        this.fwsBtApiPtpInd = fwsBtApiPtpInd;
        this.fwsRptPtpInd = fwsRptPtpInd;
        this.fwsSumPtpInd = fwsSumPtpInd;
        this.fwsCcrPtpInd = fwsCcrPtpInd;
        this.fwsRtBalPtpInd = fwsRtBalPtpInd;
        this.fwsGliPtpInd = fwsGliPtpInd;
        this.fwsErrMsgInd = fwsErrMsgInd;
        this.fwsDebugCd = fwsDebugCd;
        this.fwsAppDataParams = fwsAppDataParams;
    }

    public FwsIstUserSptId getFwsIstUserSptId() {
        return fwsIstUserSptId;
    }

    public void setFwsIstUserSptId(FwsIstUserSptId fwsIstUserSptId) {
        this.fwsIstUserSptId = fwsIstUserSptId;
    }

    public char getFwsWsApiPtpInd() {
        return fwsWsApiPtpInd;
    }

    public void setFwsWsApiPtpInd(char fwsWsApiPtpInd) {
        this.fwsWsApiPtpInd = fwsWsApiPtpInd;
    }

    public char getFwsHsApiPtpInd() {
        return fwsHsApiPtpInd;
    }

    public void setFwsHsApiPtpInd(char fwsHsApiPtpInd) {
        this.fwsHsApiPtpInd = fwsHsApiPtpInd;
    }

    public char getFwsBtApiPtpInd() {
        return fwsBtApiPtpInd;
    }

    public void setFwsBtApiPtpInd(char fwsBtApiPtpInd) {
        this.fwsBtApiPtpInd = fwsBtApiPtpInd;
    }

    public char getFwsRptPtpInd() {
        return fwsRptPtpInd;
    }

    public void setFwsRptPtpInd(char fwsRptPtpInd) {
        this.fwsRptPtpInd = fwsRptPtpInd;
    }

    public char getFwsSumPtpInd() {
        return fwsSumPtpInd;
    }

    public void setFwsSumPtpInd(char fwsSumPtpInd) {
        this.fwsSumPtpInd = fwsSumPtpInd;
    }

    public char getFwsCcrPtpInd() {
        return fwsCcrPtpInd;
    }

    public void setFwsCcrPtpInd(char fwsCcrPtpInd) {
        this.fwsCcrPtpInd = fwsCcrPtpInd;
    }

    public char getFwsRtBalPtpInd() {
        return fwsRtBalPtpInd;
    }

    public void setFwsRtBalPtpInd(char fwsRtBalPtpInd) {
        this.fwsRtBalPtpInd = fwsRtBalPtpInd;
    }

    public char getFwsGliPtpInd() {
        return fwsGliPtpInd;
    }

    public void setFwsGliPtpInd(char fwsGliPtpInd) {
        this.fwsGliPtpInd = fwsGliPtpInd;
    }

    public char getFwsErrMsgInd() {
        return fwsErrMsgInd;
    }

    public void setFwsErrMsgInd(char fwsErrMsgInd) {
        this.fwsErrMsgInd = fwsErrMsgInd;
    }

    public char getFwsDebugCd() {
        return fwsDebugCd;
    }

    public void setFwsDebugCd(char fwsDebugCd) {
        this.fwsDebugCd = fwsDebugCd;
    }

    public String getFwsAppDataParams() {
        return fwsAppDataParams;
    }

    public void setFwsAppDataParams(String fwsAppDataParams) {
        this.fwsAppDataParams = fwsAppDataParams;
    }

}
