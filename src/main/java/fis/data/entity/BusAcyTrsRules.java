package fis.data.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import fis.data.entity.id.BusAcyTrsRulesId;

@Entity
@Table(name = "BUS_ACY_TRS_RULES")
public class BusAcyTrsRules implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BusAcyTrsRulesId busAcyTrsRulesId;

    @Column(name = "BUSR_EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime busrEffectiveDt;

    @Column(name = "BUSR_EXPIRATION_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime busrExpirationDt;

    @Column(name = "BUSA_HISTORY_IND", length = 5, nullable = false)
    private short busaHistoryInd;

    @Column(name = "BUSR_AUD_ONLY_IND", nullable = false)
    private char busrAudOnlyInd;

    @Column(name = "BUSR_POSTING_IND", nullable = false)
    private char busrPostingInd;

    @Column(name = "BUSR_COST_CEN_IND", nullable = false)
    private char busrCostCenInd;

    @Column(name = "BUSR_COST_CEN_LVL", precision = 14, scale = 2, nullable = false)
    private double busrCostCenLvl;

    @Column(name = "BUSR_SUMMARIZE_IND", nullable = false)
    private char busrSummarizeInd;

    public BusAcyTrsRules() {
        super();
    }

    public BusAcyTrsRules(BusAcyTrsRulesId busAcyTrsRulesId, ZonedDateTime busrEffectiveDt,
            ZonedDateTime busrExpirationDt, short busaHistoryInd, char busrAudOnlyInd, char busrPostingInd,
            char busrCostCenInd, double busrCostCenLvl, char busrSummarizeInd) {
        super();
        this.busAcyTrsRulesId = busAcyTrsRulesId;
        this.busrEffectiveDt = busrEffectiveDt;
        this.busrExpirationDt = busrExpirationDt;
        this.busaHistoryInd = busaHistoryInd;
        this.busrAudOnlyInd = busrAudOnlyInd;
        this.busrPostingInd = busrPostingInd;
        this.busrCostCenInd = busrCostCenInd;
        this.busrCostCenLvl = busrCostCenLvl;
        this.busrSummarizeInd = busrSummarizeInd;
    }

    public BusAcyTrsRulesId getBusAcyTrsRulesId() {
        return busAcyTrsRulesId;
    }

    public void setBusAcyTrsRulesId(BusAcyTrsRulesId busAcyTrsRulesId) {
        this.busAcyTrsRulesId = busAcyTrsRulesId;
    }

    public ZonedDateTime getBusrEffectiveDt() {
        return busrEffectiveDt;
    }

    public void setBusrEffectiveDt(ZonedDateTime busrEffectiveDt) {
        this.busrEffectiveDt = busrEffectiveDt;
    }

    public ZonedDateTime getBusrExpirationDt() {
        return busrExpirationDt;
    }

    public void setBusrExpirationDt(ZonedDateTime busrExpirationDt) {
        this.busrExpirationDt = busrExpirationDt;
    }

    public short getBusaHistoryInd() {
        return busaHistoryInd;
    }

    public void setBusaHistoryInd(short busaHistoryInd) {
        this.busaHistoryInd = busaHistoryInd;
    }

    public char getBusrAudOnlyInd() {
        return busrAudOnlyInd;
    }

    public void setBusrAudOnlyInd(char busrAudOnlyInd) {
        this.busrAudOnlyInd = busrAudOnlyInd;
    }

    public char getBusrPostingInd() {
        return busrPostingInd;
    }

    public void setBusrPostingInd(char busrPostingInd) {
        this.busrPostingInd = busrPostingInd;
    }

    public char getBusrCostCenInd() {
        return busrCostCenInd;
    }

    public void setBusrCostCenInd(char busrCostCenInd) {
        this.busrCostCenInd = busrCostCenInd;
    }

    public double getBusrCostCenLvl() {
        return busrCostCenLvl;
    }

    public void setBusrCostCenLvl(double busrCostCenLvl) {
        this.busrCostCenLvl = busrCostCenLvl;
    }

    public char getBusrSummarizeInd() {
        return busrSummarizeInd;
    }

    public void setBusrSummarizeInd(char busrSummarizeInd) {
        this.busrSummarizeInd = busrSummarizeInd;
    }
}
