package fis.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;

@Embeddable
public class BusAcyTrsRulesId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "BUSA_TRANS_OBJ_CD", length = 3, nullable = false)
    private String busaTransObjCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_TRANS_ATN_CD", length = 3, nullable = false)
    private String busaTransAtnCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_APP_PROD_ID", length = 5, nullable = false)
    private String busaAppProdId = SEPARATOR_BLANK;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNumber = SEPARATOR_BLANK;

    @Column(name = "CMP_LOCATION_NBR", length = 2, nullable = false)
    private String cmpLocationNbr = SEPARATOR_BLANK;

    @Column(name = "BUSR_DATA_ITEM", precision = 14, scale = 3, nullable = false)
    private double busrDataItem;

    @Column(name = "BUSR_TIMESTAMP", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime busrTimestamp;

    public BusAcyTrsRulesId() {
        super();
    }

    public BusAcyTrsRulesId(String busaTransObjCd, String busaTransAtnCd, String busaAppProdId,
            String masterCompanyNumber, String cmpLocationNbr, double busrDataItem, ZonedDateTime busrTimestamp) {
        super();
        this.busaTransObjCd = busaTransObjCd;
        this.busaTransAtnCd = busaTransAtnCd;
        this.busaAppProdId = busaAppProdId;
        this.masterCompanyNumber = masterCompanyNumber;
        this.cmpLocationNbr = cmpLocationNbr;
        this.busrDataItem = busrDataItem;
        this.busrTimestamp = busrTimestamp;
    }

    public String getBusaTransObjCd() {
        return busaTransObjCd;
    }

    public void setBusaTransObjCd(String busaTransObjCd) {
        this.busaTransObjCd = busaTransObjCd;
    }

    public String getBusaTransAtnCd() {
        return busaTransAtnCd;
    }

    public void setBusaTransAtnCd(String busaTransAtnCd) {
        this.busaTransAtnCd = busaTransAtnCd;
    }

    public String getBusaAppProdId() {
        return busaAppProdId;
    }

    public void setBusaAppProdId(String busaAppProdId) {
        this.busaAppProdId = busaAppProdId;
    }

    public String getMasterCompanyNumber() {
        return masterCompanyNumber;
    }

    public void setMasterCompanyNumber(String masterCompanyNumber) {
        this.masterCompanyNumber = masterCompanyNumber;
    }

    public String getCmpLocationNbr() {
        return cmpLocationNbr;
    }

    public void setCmpLocationNbr(String cmpLocationNbr) {
        this.cmpLocationNbr = cmpLocationNbr;
    }

    public double getBusrDataItem() {
        return busrDataItem;
    }

    public void setBusrDataItem(double busrDataItem) {
        this.busrDataItem = busrDataItem;
    }

    public ZonedDateTime getBusrTimestamp() {
        return busrTimestamp;
    }

    public void setBusrTimestamp(ZonedDateTime busrTimestamp) {
        this.busrTimestamp = busrTimestamp;
    }

}
