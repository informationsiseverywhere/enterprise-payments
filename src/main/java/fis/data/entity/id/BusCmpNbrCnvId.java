package fis.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the BUS_CMP_NBR_CNV database table.
 *
 */
@Embeddable
public class BusCmpNbrCnvId implements Serializable {
    // default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNbr = SEPARATOR_BLANK;

    @Column(name = "CMP_LOCATION_NBR", length = 2, nullable = false)
    private String cmpLocationNbr = SEPARATOR_BLANK;

    public BusCmpNbrCnvId() {
        super();

    }

    public BusCmpNbrCnvId(String masterCompanyNbr, String cmpLocationNbr) {
        super();
        this.masterCompanyNbr = masterCompanyNbr;
        this.cmpLocationNbr = cmpLocationNbr;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public String getCmpLocationNbr() {
        return cmpLocationNbr;
    }

    public void setCmpLocationNbr(String cmpLocationNbr) {
        this.cmpLocationNbr = cmpLocationNbr;
    }

}