package fis.data.entity.id;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;

/**
 * The primary key class for the BCM_BUS_ACTIVITY database table.
 *
 */
@Embeddable
public class BcmBusActivityId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "BUSA_TRANS_OBJ_CD", length = 3, nullable = false)
    private String busaTransObjCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_TRANS_ATN_CD", length = 3, nullable = false)
    private String busaTransAtnCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_TIMESTAMP", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime busaTimestamp;

    @Column(name = "BUSA_APP_PROD_ID", length = 5, nullable = false)
    private String busaAppProdId = SEPARATOR_BLANK;

    @Column(name = "MASTER_COMPANY_NBR", length = 2, nullable = false)
    private String masterCompanyNbr = SEPARATOR_BLANK;

    @Column(name = "CMP_LOCATION_NBR", length = 2, nullable = false)
    private String cmpLocationNbr = SEPARATOR_BLANK;

    @Column(name = "BUSA_FINANCIAL_IND", nullable = false)
    private char busaFinancialInd = BLANK_CHAR;

    public BcmBusActivityId() {
        super();

    }

    public BcmBusActivityId(String busaTransObjCd, String busaTransAtnCd, ZonedDateTime busaTimestamp,
            String busaAppProdId, String masterCompanyNbr, String cmpLocationNbr, char busaFinancialInd) {
        super();
        this.busaTransObjCd = busaTransObjCd;
        this.busaTransAtnCd = busaTransAtnCd;
        this.busaTimestamp = busaTimestamp;
        this.busaAppProdId = busaAppProdId;
        this.masterCompanyNbr = masterCompanyNbr;
        this.cmpLocationNbr = cmpLocationNbr;
        this.busaFinancialInd = busaFinancialInd;
    }

    public String getBusaTransObjCd() {
        return busaTransObjCd;
    }

    public void setBusaTransObjCd(String busaTransObjCd) {
        this.busaTransObjCd = busaTransObjCd;
    }

    public String getBusaTransAtnCd() {
        return busaTransAtnCd;
    }

    public void setBusaTransAtnCd(String busaTransAtnCd) {
        this.busaTransAtnCd = busaTransAtnCd;
    }

    public ZonedDateTime getBusaTimestamp() {
        return busaTimestamp;
    }

    public void setBusaTimestamp(ZonedDateTime busaTimestamp) {
        this.busaTimestamp = busaTimestamp;
    }

    public String getBusaAppProdId() {
        return busaAppProdId;
    }

    public void setBusaAppProdId(String busaAppProdId) {
        this.busaAppProdId = busaAppProdId;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public String getCmpLocationNbr() {
        return cmpLocationNbr;
    }

    public void setCmpLocationNbr(String cmpLocationNbr) {
        this.cmpLocationNbr = cmpLocationNbr;
    }

    public char getBusaFinancialInd() {
        return busaFinancialInd;
    }

    public void setBusaFinancialInd(char busaFinancialInd) {
        this.busaFinancialInd = busaFinancialInd;
    }

}