package fis.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class FwsIstUserSptId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "BUSA_APP_PROD_ID", length = 5, nullable = false)
    private String busaAppProdId = SEPARATOR_BLANK;

    @Column(name = "FWS_APP_DATA_CD", length = 10, nullable = false)
    private String fwsAppDataCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_EFFECTIVE_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime busaEffectiveTs;

    @Column(name = "BUSA_EXPIRATION_DT")
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime busaExpirationDt;

    @Column(name = "BUSA_HISTORY_IND", length = 5, nullable = false)
    private short busaHistoryInd;

    public FwsIstUserSptId() {
        super();
    }

    public FwsIstUserSptId(String busaAppProdId, String fwsAppDataCd, ZonedDateTime busaEffectiveTs,
            ZonedDateTime busaExpirationDt, short busaHistoryInd) {
        super();
        this.busaAppProdId = busaAppProdId;
        this.fwsAppDataCd = fwsAppDataCd;
        this.busaEffectiveTs = busaEffectiveTs;
        this.busaExpirationDt = busaExpirationDt;
        this.busaHistoryInd = busaHistoryInd;
    }

    public String getBusaAppProdId() {
        return busaAppProdId;
    }

    public void setBusaAppProdId(String busaAppProdId) {
        this.busaAppProdId = busaAppProdId;
    }

    public String getFwsAppDataCd() {
        return fwsAppDataCd;
    }

    public void setFwsAppDataCd(String fwsAppDataCd) {
        this.fwsAppDataCd = fwsAppDataCd;
    }

    public ZonedDateTime getBusaEffectiveTs() {
        return busaEffectiveTs;
    }

    public void setBusaEffectiveTs(ZonedDateTime busaEffectiveTs) {
        this.busaEffectiveTs = busaEffectiveTs;
    }

    public ZonedDateTime getBusaExpirationDt() {
        return busaExpirationDt;
    }

    public void setBusaExpirationDt(ZonedDateTime busaExpirationDt) {
        this.busaExpirationDt = busaExpirationDt;
    }

    public short getBusaHistoryInd() {
        return busaHistoryInd;
    }

    public void setBusaHistoryInd(short busaHistoryInd) {
        this.busaHistoryInd = busaHistoryInd;
    }
}
