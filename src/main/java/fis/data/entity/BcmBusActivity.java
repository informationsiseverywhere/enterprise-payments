package fis.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import fis.data.entity.id.BcmBusActivityId;

/**
 * The persistent class for the BCM_BUS_ACTIVITY database table.
 *
 */
@Entity
@Table(name = "BCM_BUS_ACTIVITY")
public class BcmBusActivity implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BcmBusActivityId bcmBusActivityId;

    @Column(name = "BUSA_ACTIVITY_AMT", precision = 15, scale = 2, nullable = false)
    private double busaActivityAmt;

    @Column(name = "BUSA_ACY_NET_AMT", precision = 15, scale = 2, nullable = false)
    private double busaAcyNetAmt;

    @Column(name = "BUSA_AGENT_ID", length = 10, nullable = false)
    private String busaAgentId = SEPARATOR_BLANK;

    @Column(name = "BUSA_AGT_TTY_ID", length = 20, nullable = false)
    private String busaAgtTtyId = SEPARATOR_BLANK;

    @Column(name = "BUSA_APP_PGM_ID", length = 8, nullable = false)
    private String busaAppPgmId = SEPARATOR_BLANK;

    @Column(name = "BUSA_BIL_ACCOUNTID", length = 30, nullable = false)
    private String busaBilAccountid = SEPARATOR_BLANK;

    @Column(name = "BUSA_BIL_BANK_CD", length = 3, nullable = false)
    private String busaBilBankCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_BIL_CLS_CD", length = 3, nullable = false)
    private String busaBilClsCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_BIL_DB_KEY", length = 38, nullable = false)
    private String busaBilDbKey = SEPARATOR_BLANK;

    @Column(name = "BUSA_BIL_POST_DT", length = 10, nullable = false)
    private String busaBilPostDt = SEPARATOR_BLANK;

    @Column(name = "BUSA_BIL_RCT_TYC", length = 2, nullable = false)
    private String busaBilRctTyc = SEPARATOR_BLANK;

    @Column(name = "BUSA_BIL_REASON_CD", length = 3, nullable = false)
    private String busaBilReasonCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_BIL_RFR_DT", length = 10, nullable = false)
    private String busaBilRfrDt = SEPARATOR_BLANK;

    @Column(name = "BUSA_BIL_SRC_CD", length = 2, nullable = false)
    private String busaBilSrcCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_BIL_TYPE_CD", length = 2, nullable = false)
    private String busaBilTypeCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_BUS_CASE_ID", length = 21, nullable = false)
    private String busaBusCaseId = SEPARATOR_BLANK;

    @Column(name = "BUSA_CLIENT_ID", length = 20, nullable = false)
    private String busaClientId = SEPARATOR_BLANK;

    @Column(name = "BUSA_COUNTRY_CD", length = 4, nullable = false)
    private String busaCountryCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_COUNTY_CD", length = 5, nullable = false)
    private String busaCountyCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_CURRENCY_CD", length = 4, nullable = false)
    private String busaCurrencyCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_DEP_BANK_CD", length = 3, nullable = false)
    private String busaDepBankCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_DWS_DSB_ID", length = 21, nullable = false)
    private String busaDwsDsbId = SEPARATOR_BLANK;

    @Column(name = "BUSA_EFFECTIVE_DT", length = 10, nullable = false)
    private String busaEffectiveDt = SEPARATOR_BLANK;

    @Column(name = "BUSA_EXPIRATION_DT")
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime busaExpirationDt;

    @Column(name = "BUSA_GNRC_CSR_INF1", length = 20, nullable = false)
    private String busaGnrcCsrInf1 = SEPARATOR_BLANK;

    @Column(name = "BUSA_GNRC_CSR_INF2", length = 20, nullable = false)
    private String busaGnrcCsrInf2 = SEPARATOR_BLANK;

    @Column(name = "BUSA_GNRC_CSR_INF3", length = 20, nullable = false)
    private String busaGnrcCsrInf3 = SEPARATOR_BLANK;

    @Column(name = "BUSA_GNRC_CSR_INF4", length = 20, nullable = false)
    private String busaGnrcCsrInf4 = SEPARATOR_BLANK;

    @Column(name = "BUSA_GNRC_CSR_INF5", length = 20, nullable = false)
    private String busaGnrcCsrInf5 = SEPARATOR_BLANK;

    @Column(name = "BUSA_HISTORY_IND", length = 5, nullable = false)
    private short busaHistoryInd;

    @Column(name = "BUSA_LIN_OF_BUS_CD", length = 3, nullable = false)
    private String busaLinOfBusCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_MKT_SCT_CD", length = 2, nullable = false)
    private String busaMktSctCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_OGN_EFF_DT", length = 10, nullable = false)
    private String busaOgnEffDt = SEPARATOR_BLANK;

    @Column(name = "BUSA_OGN_TIMESTAMP", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime busaOgnTimestamp;

    @Column(name = "BUSA_OPERATOR_ID", length = 8, nullable = false)
    private String busaOperatorId = SEPARATOR_BLANK;

    @Column(name = "BUSA_PBL_ITM_CD", length = 3, nullable = false)
    private String busaPblItmCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_POLICY_ID", length = 16, nullable = false)
    private String busaPolicyId = SEPARATOR_BLANK;

    @Column(name = "BUSA_STATE_CODE", length = 3, nullable = false)
    private String busaStateCode = SEPARATOR_BLANK;

    @Column(name = "BUSA_STATUS_CD", nullable = false)
    private char busaStatusCd = BLANK_CHAR;

    @Column(name = "BUSS_SUM_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bussSumEffDt;

    public BcmBusActivityId getBcmBusActivityId() {
        return bcmBusActivityId;
    }

    public void setBcmBusActivityId(BcmBusActivityId bcmBusActivityId) {
        this.bcmBusActivityId = bcmBusActivityId;
    }

    public double getBusaActivityAmt() {
        return busaActivityAmt;
    }

    public void setBusaActivityAmt(double busaActivityAmt) {
        this.busaActivityAmt = busaActivityAmt;
    }

    public double getBusaAcyNetAmt() {
        return busaAcyNetAmt;
    }

    public void setBusaAcyNetAmt(double busaAcyNetAmt) {
        this.busaAcyNetAmt = busaAcyNetAmt;
    }

    public String getBusaAgentId() {
        return busaAgentId;
    }

    public void setBusaAgentId(String busaAgentId) {
        this.busaAgentId = busaAgentId;
    }

    public String getBusaAgtTtyId() {
        return busaAgtTtyId;
    }

    public void setBusaAgtTtyId(String busaAgtTtyId) {
        this.busaAgtTtyId = busaAgtTtyId;
    }

    public String getBusaAppPgmId() {
        return busaAppPgmId;
    }

    public void setBusaAppPgmId(String busaAppPgmId) {
        this.busaAppPgmId = busaAppPgmId;
    }

    public String getBusaBilAccountid() {
        return busaBilAccountid;
    }

    public void setBusaBilAccountid(String busaBilAccountid) {
        this.busaBilAccountid = busaBilAccountid;
    }

    public String getBusaBilBankCd() {
        return busaBilBankCd;
    }

    public void setBusaBilBankCd(String busaBilBankCd) {
        this.busaBilBankCd = busaBilBankCd;
    }

    public String getBusaBilClsCd() {
        return busaBilClsCd;
    }

    public void setBusaBilClsCd(String busaBilClsCd) {
        this.busaBilClsCd = busaBilClsCd;
    }

    public String getBusaBilDbKey() {
        return busaBilDbKey;
    }

    public void setBusaBilDbKey(String busaBilDbKey) {
        this.busaBilDbKey = busaBilDbKey;
    }

    public String getBusaBilPostDt() {
        return busaBilPostDt;
    }

    public void setBusaBilPostDt(String busaBilPostDt) {
        this.busaBilPostDt = busaBilPostDt;
    }

    public String getBusaBilRctTyc() {
        return busaBilRctTyc;
    }

    public void setBusaBilRctTyc(String busaBilRctTyc) {
        this.busaBilRctTyc = busaBilRctTyc;
    }

    public String getBusaBilReasonCd() {
        return busaBilReasonCd;
    }

    public void setBusaBilReasonCd(String busaBilReasonCd) {
        this.busaBilReasonCd = busaBilReasonCd;
    }

    public String getBusaBilRfrDt() {
        return busaBilRfrDt;
    }

    public void setBusaBilRfrDt(String busaBilRfrDt) {
        this.busaBilRfrDt = busaBilRfrDt;
    }

    public String getBusaBilSrcCd() {
        return busaBilSrcCd;
    }

    public void setBusaBilSrcCd(String busaBilSrcCd) {
        this.busaBilSrcCd = busaBilSrcCd;
    }

    public String getBusaBilTypeCd() {
        return busaBilTypeCd;
    }

    public void setBusaBilTypeCd(String busaBilTypeCd) {
        this.busaBilTypeCd = busaBilTypeCd;
    }

    public String getBusaBusCaseId() {
        return busaBusCaseId;
    }

    public void setBusaBusCaseId(String busaBusCaseId) {
        this.busaBusCaseId = busaBusCaseId;
    }

    public String getBusaClientId() {
        return busaClientId;
    }

    public void setBusaClientId(String busaClientId) {
        this.busaClientId = busaClientId;
    }

    public String getBusaCountryCd() {
        return busaCountryCd;
    }

    public void setBusaCountryCd(String busaCountryCd) {
        this.busaCountryCd = busaCountryCd;
    }

    public String getBusaCountyCd() {
        return busaCountyCd;
    }

    public void setBusaCountyCd(String busaCountyCd) {
        this.busaCountyCd = busaCountyCd;
    }

    public String getBusaCurrencyCd() {
        return busaCurrencyCd;
    }

    public void setBusaCurrencyCd(String busaCurrencyCd) {
        this.busaCurrencyCd = busaCurrencyCd;
    }

    public String getBusaDepBankCd() {
        return busaDepBankCd;
    }

    public void setBusaDepBankCd(String busaDepBankCd) {
        this.busaDepBankCd = busaDepBankCd;
    }

    public String getBusaDwsDsbId() {
        return busaDwsDsbId;
    }

    public void setBusaDwsDsbId(String busaDwsDsbId) {
        this.busaDwsDsbId = busaDwsDsbId;
    }

    public String getBusaEffectiveDt() {
        return busaEffectiveDt;
    }

    public void setBusaEffectiveDt(String busaEffectiveDt) {
        this.busaEffectiveDt = busaEffectiveDt;
    }

    public ZonedDateTime getBusaExpirationDt() {
        return busaExpirationDt;
    }

    public void setBusaExpirationDt(ZonedDateTime busaExpirationDt) {
        this.busaExpirationDt = busaExpirationDt;
    }

    public String getBusaGnrcCsrInf1() {
        return busaGnrcCsrInf1;
    }

    public void setBusaGnrcCsrInf1(String busaGnrcCsrInf1) {
        this.busaGnrcCsrInf1 = busaGnrcCsrInf1;
    }

    public String getBusaGnrcCsrInf2() {
        return busaGnrcCsrInf2;
    }

    public void setBusaGnrcCsrInf2(String busaGnrcCsrInf2) {
        this.busaGnrcCsrInf2 = busaGnrcCsrInf2;
    }

    public String getBusaGnrcCsrInf3() {
        return busaGnrcCsrInf3;
    }

    public void setBusaGnrcCsrInf3(String busaGnrcCsrInf3) {
        this.busaGnrcCsrInf3 = busaGnrcCsrInf3;
    }

    public String getBusaGnrcCsrInf4() {
        return busaGnrcCsrInf4;
    }

    public void setBusaGnrcCsrInf4(String busaGnrcCsrInf4) {
        this.busaGnrcCsrInf4 = busaGnrcCsrInf4;
    }

    public String getBusaGnrcCsrInf5() {
        return busaGnrcCsrInf5;
    }

    public void setBusaGnrcCsrInf5(String busaGnrcCsrInf5) {
        this.busaGnrcCsrInf5 = busaGnrcCsrInf5;
    }

    public short getBusaHistoryInd() {
        return busaHistoryInd;
    }

    public void setBusaHistoryInd(short busaHistoryInd) {
        this.busaHistoryInd = busaHistoryInd;
    }

    public String getBusaLinOfBusCd() {
        return busaLinOfBusCd;
    }

    public void setBusaLinOfBusCd(String busaLinOfBusCd) {
        this.busaLinOfBusCd = busaLinOfBusCd;
    }

    public String getBusaMktSctCd() {
        return busaMktSctCd;
    }

    public void setBusaMktSctCd(String busaMktSctCd) {
        this.busaMktSctCd = busaMktSctCd;
    }

    public String getBusaOgnEffDt() {
        return busaOgnEffDt;
    }

    public void setBusaOgnEffDt(String busaOgnEffDt) {
        this.busaOgnEffDt = busaOgnEffDt;
    }

    public ZonedDateTime getBusaOgnTimestamp() {
        return busaOgnTimestamp;
    }

    public void setBusaOgnTimestamp(ZonedDateTime busaOgnTimestamp) {
        this.busaOgnTimestamp = busaOgnTimestamp;
    }

    public String getBusaOperatorId() {
        return busaOperatorId;
    }

    public void setBusaOperatorId(String busaOperatorId) {
        this.busaOperatorId = busaOperatorId;
    }

    public String getBusaPblItmCd() {
        return busaPblItmCd;
    }

    public void setBusaPblItmCd(String busaPblItmCd) {
        this.busaPblItmCd = busaPblItmCd;
    }

    public String getBusaPolicyId() {
        return busaPolicyId;
    }

    public void setBusaPolicyId(String busaPolicyId) {
        this.busaPolicyId = busaPolicyId;
    }

    public String getBusaStateCode() {
        return busaStateCode;
    }

    public void setBusaStateCode(String busaStateCode) {
        this.busaStateCode = busaStateCode;
    }

    public char getBusaStatusCd() {
        return busaStatusCd;
    }

    public void setBusaStatusCd(char busaStatusCd) {
        this.busaStatusCd = busaStatusCd;
    }

    public ZonedDateTime getBussSumEffDt() {
        return bussSumEffDt;
    }

    public void setBussSumEffDt(ZonedDateTime bussSumEffDt) {
        this.bussSumEffDt = bussSumEffDt;
    }

}