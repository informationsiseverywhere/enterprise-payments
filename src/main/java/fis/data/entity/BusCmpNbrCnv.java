package fis.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import fis.data.entity.id.BusCmpNbrCnvId;

/**
 * The persistent class for the BUS_CMP_NBR_CNV database table.
 *
 */
@Entity
@Table(name = "BUS_CMP_NBR_CNV")
public class BusCmpNbrCnv implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private BusCmpNbrCnvId busCmpNbrCnvId;

    @Column(name = "BUS_CMP_NBR_DES", length = 75, nullable = false)
    private String busCmpNbrDes = SEPARATOR_BLANK;

    public BusCmpNbrCnvId getBusCmpNbrCnvId() {
        return busCmpNbrCnvId;
    }

    public void setBusCmpNbrCnvId(BusCmpNbrCnvId busCmpNbrCnvId) {
        this.busCmpNbrCnvId = busCmpNbrCnvId;
    }

    public String getBusCmpNbrDes() {
        return busCmpNbrDes;
    }

    public void setBusCmpNbrDes(String busCmpNbrDes) {
        this.busCmpNbrDes = busCmpNbrDes;
    };

}