package fis.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import fis.data.entity.id.DwsBusActivityId;

@Entity
@Table(name = "DWS_BUS_ACTIVITY")
public class DwsBusActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DwsBusActivityId dwsBusActivityId;

    @Column(name = "BUSA_STATUS_CD", length = 1, nullable = false)
    private char busaStatusCd = BLANK_CHAR;

    @Column(name = "BUSA_OGN_TIMESTAMP", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime busaOgnTimestamp;

    @Column(name = "BUSA_EFFECTIVE_DT", length = 10, nullable = false)
    private String busaEffectiveDt = SEPARATOR_BLANK;

    @Column(name = "BUSA_OPERATOR_ID", length = 8, nullable = false)
    private String busaOperatorId = SEPARATOR_BLANK;

    @Column(name = "BUSA_APP_PGM_ID", length = 8, nullable = false)
    private String busaAppPgmId = SEPARATOR_BLANK;

    @Column(name = "BUSA_COUNTY_CD", length = 5, nullable = false)
    private String busaCountyCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_STATE_CODE", length = 3, nullable = false)
    private String busaStateCode = SEPARATOR_BLANK;

    @Column(name = "BUSA_COUNTRY_CD", length = 4, nullable = false)
    private String busaCountryCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_LIN_OF_BUS_CD", length = 3, nullable = false)
    private String busaLinOfBusCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_POLICY_ID", length = 16, nullable = false)
    private String busaPolicyId = SEPARATOR_BLANK;

    @Column(name = "BUSA_AGENT_ID", length = 10, nullable = false)
    private String busaAgentId = SEPARATOR_BLANK;

    @Column(name = "BUSA_BUS_CASE_ID", length = 21, nullable = false)
    private String busaBusCaseId = SEPARATOR_BLANK;

    @Column(name = "BUSA_MKT_SCT_CD", length = 2, nullable = false)
    private String busaMktSctCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_CLIENT_ID", length = 16, nullable = false)
    private String busaClientId = SEPARATOR_BLANK;

    @Column(name = "BUSA_GNRC_CSR_INF1", length = 20, nullable = false)
    private String busaGnrcCsrInf1 = SEPARATOR_BLANK;

    @Column(name = "BUSA_GNRC_CSR_INF2", length = 20, nullable = false)
    private String busaGnrcCsrInf2 = SEPARATOR_BLANK;

    @Column(name = "BUSA_GNRC_CSR_INF3", length = 20, nullable = false)
    private String busaGnrcCsrInf3 = SEPARATOR_BLANK;

    @Column(name = "BUSA_GNRC_CSR_INF4", length = 20, nullable = false)
    private String busaGnrcCsrInf4 = SEPARATOR_BLANK;

    @Column(name = "BUSA_GNRC_CSR_INF5", length = 20, nullable = false)
    private String busaGnrcCsrInf5 = SEPARATOR_BLANK;

    @Column(name = "BUSA_ACTIVITY_AMT", precision = 15, scale = 2, nullable = false)
    private double busaActivityAmt = DECIMAL_ZERO;

    @Column(name = "BUSA_CURRENCY_CD", length = 4, nullable = false)
    private String busaCurrencyCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_DWS_DSB_ID", length = 21, nullable = false)
    private String busaDwsDsbId = SEPARATOR_BLANK;

    @Column(name = "BUSA_DWS_CK_NBR", nullable = false)
    private int busaDwsCkNbr = SHORT_ZERO;

    @Column(name = "BUSA_DWS_BANK_NBR", length = 5, nullable = false)
    private short busaDwsBankNbr = SHORT_ZERO;

    @Column(name = "BUSA_BANK_ACT_NBR", length = 15, nullable = false)
    private String busaBankActNbr = SEPARATOR_BLANK;

    @Column(name = "BUSA_PRC_LOC_CD", length = 3, nullable = false)
    private String busaPrcLocCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_DWS_TYPE_CD", length = 3, nullable = false)
    private String busaDwsTypeCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_DWS_PRD_ID", length = 3, nullable = false)
    private String busaDwsPrdId = SEPARATOR_BLANK;

    @Column(name = "BUSA_DST_OFFICE_NM", length = 15, nullable = false)
    private String busaDstOfficeNm = SEPARATOR_BLANK;

    @Column(name = "BUSA_DWS_MID_CD", length = 2, nullable = false)
    private String busaDwsMidCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_PAYEE1_ID", length = 20, nullable = false)
    private String busaPayee1Id = SEPARATOR_BLANK;

    @Column(name = "BUSA_PAYEE2_ID", length = 20, nullable = false)
    private String busaPayee2Id = SEPARATOR_BLANK;

    @Column(name = "BUSA_PAYEE3_ID", length = 20, nullable = false)
    private String busaPayee3Id = SEPARATOR_BLANK;

    @Column(name = "BUSA_MAIL_REC_ID", length = 20, nullable = false)
    private String busaMailRecId = SEPARATOR_BLANK;

    @Column(name = "BUSA_ADR_SEQ_NBR", length = 5, nullable = false)
    private short busaAdrSeqNbr = SHORT_ZERO;

    @Column(name = "BUSA_DWS_STATUS_CD", length = 1, nullable = false)
    private char busaDwsStatusCd = BLANK_CHAR;

    @Column(name = "BUSA_NEXT_CK_NBR", nullable = false)
    private int busaNextCkNbr = SHORT_ZERO;

    @Column(name = "BUSA_CUR_BEG_CK", nullable = false)
    private int busaCurBegCk = SHORT_ZERO;

    @Column(name = "BUSA_CUR_EN_CK_NBR", nullable = false)
    private int busaCurEnCkNbr = SHORT_ZERO;

    @Column(name = "BUSA_CUR_RGE_DT", length = 10, nullable = false)
    private String busaCurRgeDt = SEPARATOR_BLANK;

    @Column(name = "BUSA_NXT_BEG_CK", nullable = false)
    private int busaNxtBegCk = SHORT_ZERO;

    @Column(name = "BUSA_NXT_EN_CK_NBR", nullable = false)
    private int busaNxtEnCkNbr = SHORT_ZERO;

    @Column(name = "BUSA_NXT_RGE_DT", length = 10, nullable = false)
    private String busaNxtRgeDt = SEPARATOR_BLANK;

    @Column(name = "BUSA_DWS_DSB_DT", length = 10, nullable = false)
    private String busaDwsDsbDt = SEPARATOR_BLANK;

    @Column(name = "BUSA_CK_USAGE_CD", length = 2, nullable = false)
    private String busaCkUsageCd = SEPARATOR_BLANK;

    @Column(name = "BUSA_PRE_NEXT_CK", nullable = false)
    private int busaPreNextCk = SHORT_ZERO;

    @Column(name = "BUSA_HISTORY_IND", length = 5, nullable = false)
    private short busaHistoryInd = SHORT_ZERO;

    @Column(name = "BUSA_EXPIRATION_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime busaExpirationDt;

    @Column(name = "BUSS_SUM_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime bussSumEffDt;

    public DwsBusActivity() {
        super();
    }

    public DwsBusActivityId getDwsBusActivityId() {
        return dwsBusActivityId;
    }

    public void setDwsBusActivityId(DwsBusActivityId dwsBusActivityId) {
        this.dwsBusActivityId = dwsBusActivityId;
    }

    public char getBusaStatusCd() {
        return busaStatusCd;
    }

    public void setBusaStatusCd(char busaStatusCd) {
        this.busaStatusCd = busaStatusCd;
    }

    public ZonedDateTime getBusaOgnTimestamp() {
        return busaOgnTimestamp;
    }

    public void setBusaOgnTimestamp(ZonedDateTime busaOgnTimestamp) {
        this.busaOgnTimestamp = busaOgnTimestamp;
    }

    public String getBusaEffectiveDt() {
        return busaEffectiveDt;
    }

    public void setBusaEffectiveDt(String busaEffectiveDt) {
        this.busaEffectiveDt = busaEffectiveDt;
    }

    public String getBusaOperatorId() {
        return busaOperatorId;
    }

    public void setBusaOperatorId(String busaOperatorId) {
        this.busaOperatorId = busaOperatorId;
    }

    public String getBusaAppPgmId() {
        return busaAppPgmId;
    }

    public void setBusaAppPgmId(String busaAppPgmId) {
        this.busaAppPgmId = busaAppPgmId;
    }

    public String getBusaCountyCd() {
        return busaCountyCd;
    }

    public void setBusaCountyCd(String busaCountyCd) {
        this.busaCountyCd = busaCountyCd;
    }

    public String getBusaStateCode() {
        return busaStateCode;
    }

    public void setBusaStateCode(String busaStateCode) {
        this.busaStateCode = busaStateCode;
    }

    public String getBusaCountryCd() {
        return busaCountryCd;
    }

    public void setBusaCountryCd(String busaCountryCd) {
        this.busaCountryCd = busaCountryCd;
    }

    public String getBusaLinOfBusCd() {
        return busaLinOfBusCd;
    }

    public void setBusaLinOfBusCd(String busaLinOfBusCd) {
        this.busaLinOfBusCd = busaLinOfBusCd;
    }

    public String getBusaPolicyId() {
        return busaPolicyId;
    }

    public void setBusaPolicyId(String busaPolicyId) {
        this.busaPolicyId = busaPolicyId;
    }

    public String getBusaAgentId() {
        return busaAgentId;
    }

    public void setBusaAgentId(String busaAgentId) {
        this.busaAgentId = busaAgentId;
    }

    public String getBusaBusCaseId() {
        return busaBusCaseId;
    }

    public void setBusaBusCaseId(String busaBusCaseId) {
        this.busaBusCaseId = busaBusCaseId;
    }

    public String getBusaMktSctCd() {
        return busaMktSctCd;
    }

    public void setBusaMktSctCd(String busaMktSctCd) {
        this.busaMktSctCd = busaMktSctCd;
    }

    public String getBusaClientId() {
        return busaClientId;
    }

    public void setBusaClientId(String busaClientId) {
        this.busaClientId = busaClientId;
    }

    public String getBusaGnrcCsrInf1() {
        return busaGnrcCsrInf1;
    }

    public void setBusaGnrcCsrInf1(String busaGnrcCsrInf1) {
        this.busaGnrcCsrInf1 = busaGnrcCsrInf1;
    }

    public String getBusaGnrcCsrInf2() {
        return busaGnrcCsrInf2;
    }

    public void setBusaGnrcCsrInf2(String busaGnrcCsrInf2) {
        this.busaGnrcCsrInf2 = busaGnrcCsrInf2;
    }

    public String getBusaGnrcCsrInf3() {
        return busaGnrcCsrInf3;
    }

    public void setBusaGnrcCsrInf3(String busaGnrcCsrInf3) {
        this.busaGnrcCsrInf3 = busaGnrcCsrInf3;
    }

    public String getBusaGnrcCsrInf4() {
        return busaGnrcCsrInf4;
    }

    public void setBusaGnrcCsrInf4(String busaGnrcCsrInf4) {
        this.busaGnrcCsrInf4 = busaGnrcCsrInf4;
    }

    public String getBusaGnrcCsrInf5() {
        return busaGnrcCsrInf5;
    }

    public void setBusaGnrcCsrInf5(String busaGnrcCsrInf5) {
        this.busaGnrcCsrInf5 = busaGnrcCsrInf5;
    }

    public double getBusaActivityAmt() {
        return busaActivityAmt;
    }

    public void setBusaActivityAmt(double busaActivityAmt) {
        this.busaActivityAmt = busaActivityAmt;
    }

    public String getBusaCurrencyCd() {
        return busaCurrencyCd;
    }

    public void setBusaCurrencyCd(String busaCurrencyCd) {
        this.busaCurrencyCd = busaCurrencyCd;
    }

    public String getBusaDwsDsbId() {
        return busaDwsDsbId;
    }

    public void setBusaDwsDsbId(String busaDwsDsbId) {
        this.busaDwsDsbId = busaDwsDsbId;
    }

    public int getBusaDwsCkNbr() {
        return busaDwsCkNbr;
    }

    public void setBusaDwsCkNbr(int busaDwsCkNbr) {
        this.busaDwsCkNbr = busaDwsCkNbr;
    }

    public short getBusaDwsBankNbr() {
        return busaDwsBankNbr;
    }

    public void setBusaDwsBankNbr(short busaDwsBankNbr) {
        this.busaDwsBankNbr = busaDwsBankNbr;
    }

    public String getBusaBankActNbr() {
        return busaBankActNbr;
    }

    public void setBusaBankActNbr(String busaBankActNbr) {
        this.busaBankActNbr = busaBankActNbr;
    }

    public String getBusaPrcLocCd() {
        return busaPrcLocCd;
    }

    public void setBusaPrcLocCd(String busaPrcLocCd) {
        this.busaPrcLocCd = busaPrcLocCd;
    }

    public String getBusaDwsTypeCd() {
        return busaDwsTypeCd;
    }

    public void setBusaDwsTypeCd(String busaDwsTypeCd) {
        this.busaDwsTypeCd = busaDwsTypeCd;
    }

    public String getBusaDwsPrdId() {
        return busaDwsPrdId;
    }

    public void setBusaDwsPrdId(String busaDwsPrdId) {
        this.busaDwsPrdId = busaDwsPrdId;
    }

    public String getBusaDstOfficeNm() {
        return busaDstOfficeNm;
    }

    public void setBusaDstOfficeNm(String busaDstOfficeNm) {
        this.busaDstOfficeNm = busaDstOfficeNm;
    }

    public String getBusaDwsMidCd() {
        return busaDwsMidCd;
    }

    public void setBusaDwsMidCd(String busaDwsMidCd) {
        this.busaDwsMidCd = busaDwsMidCd;
    }

    public String getBusaPayee1Id() {
        return busaPayee1Id;
    }

    public void setBusaPayee1Id(String busaPayee1Id) {
        this.busaPayee1Id = busaPayee1Id;
    }

    public String getBusaPayee2Id() {
        return busaPayee2Id;
    }

    public void setBusaPayee2Id(String busaPayee2Id) {
        this.busaPayee2Id = busaPayee2Id;
    }

    public String getBusaPayee3Id() {
        return busaPayee3Id;
    }

    public void setBusaPayee3Id(String busaPayee3Id) {
        this.busaPayee3Id = busaPayee3Id;
    }

    public String getBusaMailRecId() {
        return busaMailRecId;
    }

    public void setBusaMailRecId(String busaMailRecId) {
        this.busaMailRecId = busaMailRecId;
    }

    public short getBusaAdrSeqNbr() {
        return busaAdrSeqNbr;
    }

    public void setBusaAdrSeqNbr(short busaAdrSeqNbr) {
        this.busaAdrSeqNbr = busaAdrSeqNbr;
    }

    public char getBusaDwsStatusCd() {
        return busaDwsStatusCd;
    }

    public void setBusaDwsStatusCd(char busaDwsStatusCd) {
        this.busaDwsStatusCd = busaDwsStatusCd;
    }

    public int getBusaNextCkNbr() {
        return busaNextCkNbr;
    }

    public void setBusaNextCkNbr(int busaNextCkNbr) {
        this.busaNextCkNbr = busaNextCkNbr;
    }

    public int getBusaCurBegCk() {
        return busaCurBegCk;
    }

    public void setBusaCurBegCk(int busaCurBegCk) {
        this.busaCurBegCk = busaCurBegCk;
    }

    public int getBusaCurEnCkNbr() {
        return busaCurEnCkNbr;
    }

    public void setBusaCurEnCkNbr(int busaCurEnCkNbr) {
        this.busaCurEnCkNbr = busaCurEnCkNbr;
    }

    public String getBusaCurRgeDt() {
        return busaCurRgeDt;
    }

    public void setBusaCurRgeDt(String busaCurRgeDt) {
        this.busaCurRgeDt = busaCurRgeDt;
    }

    public int getBusaNxtBegCk() {
        return busaNxtBegCk;
    }

    public void setBusaNxtBegCk(int busaNxtBegCk) {
        this.busaNxtBegCk = busaNxtBegCk;
    }

    public int getBusaNxtEnCkNbr() {
        return busaNxtEnCkNbr;
    }

    public void setBusaNxtEnCkNbr(int busaNxtEnCkNbr) {
        this.busaNxtEnCkNbr = busaNxtEnCkNbr;
    }

    public String getBusaNxtRgeDt() {
        return busaNxtRgeDt;
    }

    public void setBusaNxtRgeDt(String busaNxtRgeDt) {
        this.busaNxtRgeDt = busaNxtRgeDt;
    }

    public String getBusaDwsDsbDt() {
        return busaDwsDsbDt;
    }

    public void setBusaDwsDsbDt(String busaDwsDsbDt) {
        this.busaDwsDsbDt = busaDwsDsbDt;
    }

    public String getBusaCkUsageCd() {
        return busaCkUsageCd;
    }

    public void setBusaCkUsageCd(String busaCkUsageCd) {
        this.busaCkUsageCd = busaCkUsageCd;
    }

    public int getBusaPreNextCk() {
        return busaPreNextCk;
    }

    public void setBusaPreNextCk(int busaPreNextCk) {
        this.busaPreNextCk = busaPreNextCk;
    }

    public short getBusaHistoryInd() {
        return busaHistoryInd;
    }

    public void setBusaHistoryInd(short busaHistoryInd) {
        this.busaHistoryInd = busaHistoryInd;
    }

    public ZonedDateTime getBusaExpirationDt() {
        return busaExpirationDt;
    }

    public void setBusaExpirationDt(ZonedDateTime busaExpirationDt) {
        this.busaExpirationDt = busaExpirationDt;
    }

    public ZonedDateTime getBussSumEffDt() {
        return bussSumEffDt;
    }

    public void setBussSumEffDt(ZonedDateTime bussSumEffDt) {
        this.bussSumEffDt = bussSumEffDt;
    }

}
