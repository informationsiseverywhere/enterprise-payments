package fis.service;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.CHAR_A;
import static core.utils.CommonConstants.CHAR_B;
import static core.utils.CommonConstants.CHAR_E;
import static core.utils.CommonConstants.CHAR_H;
import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.CHAR_S;
import static core.utils.CommonConstants.CHAR_W;
import static core.utils.CommonConstants.CHAR_Y;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.INVALID_DOUBLE_VAL;
import static core.utils.CommonConstants.INVALID_INT_VAL;
import static core.utils.CommonConstants.INVALID_SHORT_VAL;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import application.fis.service.BusinessActivityUserExitService;
import application.fis.service.MasterCompanyUserExitService;
import application.utils.service.MultiCurrencyService;
import core.datetime.service.DateService;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.translation.data.entity.BusCdTranslation;
import core.translation.data.entity.id.BusCdTranslationId;
import core.translation.repository.BusCdTranslationRepository;
import core.utils.DateRoutine;
import core.utils.IdGenerator;
import fis.data.entity.BcmBusActivity;
import fis.data.entity.BusAcyTrsRules;
import fis.data.entity.BusCmpNbrCnv;
import fis.data.entity.DwsBusActivity;
import fis.data.entity.FwsIstUserSpt;
import fis.data.entity.id.BcmBusActivityId;
import fis.data.entity.id.BusCmpNbrCnvId;
import fis.data.entity.id.DwsBusActivityId;
import fis.data.repository.BcmBusActivityRepository;
import fis.data.repository.BusAcyTrsRulesRepository;
import fis.data.repository.BusCmpNbrCnvRepository;
import fis.data.repository.DwsBusActivityRepository;
import fis.data.repository.FwsIstUserSptRepository;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.DataItem;
import fis.utils.FinancialIntegratorConstants.TableNames;

@Service
public class FinancialApiService {

    private static final Logger logger = LoggerFactory.getLogger(FinancialApiService.class);
    private static final String DEFAULT_NBR = "99";
    private static final String BUS_CASE_ID = "DUMMY";
    private static final String BAD_REQUEST_MISSING_PARAMETER = "bad.request.missing.parameter";
    private static final String INVALID_DATE = "invalid.date";
    private static final String EFFECTIVE_DATE = "Effective Date";

    @Autowired
    private FwsIstUserSptRepository fwsIstUserSptRepository;

    @Autowired
    private BusAcyTrsRulesRepository busAcyTrsRulesRepository;

    @Autowired
    private BcmBusActivityRepository bcmBusActivityRepository;

    @Autowired
    private BusCmpNbrCnvRepository busCmpNbrCnvRepository;

    @Autowired
    private DateService dateService;

    @Autowired
    private BusinessActivityUserExitService businessActivityUserExitService;

    @Autowired
    private DwsBusActivityRepository dwsBusActivityRepository;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private MultiCurrencyService multiCurrencyService;

    @Autowired
    private MasterCompanyUserExitService masterCompanyUserExitService;

    public String verifyErrorMessageIndicator(ZonedDateTime date, Pageable pageable) {
        String errorDescription;
        List<String> fwsAppDataCdList = new ArrayList<>();
        fwsAppDataCdList.add(String.valueOf(CHAR_S));
        List<FwsIstUserSpt> fwsIstUserSptList = fwsIstUserSptRepository.findByMaxEffectiveTs(
                ApplicationProduct.FWS.toString(), fwsAppDataCdList, SHORT_ZERO, DateRoutine.defaultDateTime(), date,
                pageable);

        char errorMessageIndicator;
        if (fwsIstUserSptList == null || fwsIstUserSptList.isEmpty()) {
            return String.valueOf(CHAR_N);
        } else {
            errorMessageIndicator = fwsIstUserSptList.get(SHORT_ZERO).getFwsErrMsgInd();
            if (!(errorMessageIndicator == CHAR_W || errorMessageIndicator == CHAR_H
                    || errorMessageIndicator == CHAR_B)) {
                errorDescription = "INVALID ERROR MESSAGE INDICATOR RETURNED FROM FWS_IST_USER_SPT TABLE, ERROR MESSAGE INDICATOR: "
                        + errorMessageIndicator;
                logger.error(errorDescription);
                return errorDescription;
            }
        }

        return String.valueOf(fwsIstUserSptList.get(SHORT_ZERO).getFwsDebugCd());
    }

    public String[] processFWSFunction(FinancialApiActivity financialApiActivity, ZonedDateTime date) {
        int pageNumber = SHORT_ZERO;
        Pageable pageable = PageRequest.of(pageNumber, SHORT_ONE);
        String tableName = BLANK_STRING;
        String returnedMessage = verifyErrorMessageIndicator(date, pageable);

        if (!(returnedMessage.equalsIgnoreCase(String.valueOf(CHAR_Y))
                || returnedMessage.equalsIgnoreCase(String.valueOf(CHAR_N)))) {
            return new String[] { "ERR", returnedMessage };
        }
        if (!financialApiActivity.getApplicationProductIdentifier().isEmpty()) {
            List<FwsIstUserSpt> fwsIstUserSptList = fwsIstUserSptRepository.findByMaxEffectiveTs(
                    financialApiActivity.getApplicationProductIdentifier(), getFwsAppDataCdList(), SHORT_ZERO,
                    DateRoutine.defaultDateTime(), date, pageable);
            if (fwsIstUserSptList == null || fwsIstUserSptList.isEmpty()) {
                return new String[] { "ERR", "INVALID APP-PROD-ID, APP-PROD-ID = "
                        + financialApiActivity.getApplicationProductIdentifier() };
            } else {
                String dataParam = fwsIstUserSptList.get(SHORT_ZERO).getFwsAppDataParams().trim();
                if (dataParam.length() <= SHORT_ZERO) {
                    return new String[] { "ERRF", "INCOMPLETE ENTRY FOR FWS_IST_USER_SPT,  APP-PROD-ID= "
                            + financialApiActivity.getApplicationProductIdentifier() };
                }
                tableName = dataParam.trim();
            }

        }

        if (financialApiActivity.getFunctionCode() == CHAR_A) {
            boolean isRowFound = false;
            financialApiActivity.setFinancialIndicator(CHAR_N);
            ZonedDateTime busaTimeStamp = dateService.currentDateTime();

            if (tableName.equalsIgnoreCase(TableNames.BCM_BUS_ACTIVITY_V.toString())) {
                isRowFound = checkBusAcyTrsRules(financialApiActivity);
                if (!isRowFound) {
                    financialApiActivity = callUserExit(financialApiActivity, tableName, pageable);
                    editRecords(financialApiActivity, tableName);
                    insertIntoBcmBusActivity(financialApiActivity, busaTimeStamp);
                }

            }
            if (tableName.equalsIgnoreCase(TableNames.DWS_BUS_ACTIVITY_V.toString())) {
                isRowFound = checkBusAcyTrsRules(financialApiActivity);
                if (!isRowFound) {
                    financialApiActivity = callUserExit(financialApiActivity, tableName, pageable);
                    editRecords(financialApiActivity, tableName);
                    insertIntoDwsBusActivity(financialApiActivity, busaTimeStamp);
                }
            }
        }

        return new String[] { financialApiActivity.getErrorCode(), BLANK_STRING };
    }

    private boolean checkBusAcyTrsRules(FinancialApiActivity financialApiActivity) {

        Short count = busAcyTrsRulesRepository.checkForExistence(
                getMasterCompanyNumberList(financialApiActivity.getMasterCompanyNbr()),
                getCmpLocationNbrList(financialApiActivity.getCompanyLocationNbr()),
                financialApiActivity.getApplicationProductIdentifier(), financialApiActivity.getTransactionObjectCode(),
                financialApiActivity.getTransactionActionCode(), SHORT_ZERO, DateRoutine.defaultDateTime(), CHAR_S);
        if (count != null && count > SHORT_ZERO) {
            count = busAcyTrsRulesRepository.checkForExistence2(
                    getMasterCompanyNumberList(financialApiActivity.getMasterCompanyNbr()),
                    getCmpLocationNbrList(financialApiActivity.getCompanyLocationNbr()),
                    financialApiActivity.getApplicationProductIdentifier(),
                    financialApiActivity.getTransactionObjectCode(), financialApiActivity.getTransactionActionCode(),
                    SHORT_ZERO, DateRoutine.defaultDateTime(), CHAR_S, getBusrIndList(), getBusrIndList());
            if (count != null && count > SHORT_ZERO) {
                throw new InvalidDataException("invalid.data.row");
            }
            Short count3 = busAcyTrsRulesRepository.checkForExistence2(
                    getMasterCompanyNumberList(financialApiActivity.getMasterCompanyNbr()).subList(1, 2),
                    getCmpLocationNbrList(financialApiActivity.getCompanyLocationNbr()).subList(1, 2),
                    financialApiActivity.getApplicationProductIdentifier(),
                    financialApiActivity.getTransactionObjectCode(), financialApiActivity.getTransactionActionCode(),
                    SHORT_ZERO, DateRoutine.defaultDateTime(), CHAR_S, getBusrIndList(), getBusrIndList());
            if (count3 != null && count3 > SHORT_ZERO) {
                throw new InvalidDataException("invalid.data.row");
            }

            return true;
        }
        return false;
    }

    private FinancialApiActivity callUserExit(FinancialApiActivity financialApiActivity, String tableName,
            Pageable pageable) {
        masterCompanyUserExitService.getMasterCompany(financialApiActivity);
        return businessActivityUserExitService.getBusinessActivityUserExit(financialApiActivity, tableName, pageable);
    }

    private FinancialApiActivity editRecords(FinancialApiActivity financialApiActivity, String tableName) {

        String masterCompanyNumber = financialApiActivity.getMasterCompanyNbr();
        String companyLocationNumber = financialApiActivity.getCompanyLocationNbr();
        String transactionObjectCode = financialApiActivity.getTransactionObjectCode();
        String transactionActionCode = financialApiActivity.getTransactionActionCode();
        String productIdentifier = financialApiActivity.getApplicationProductIdentifier();
        BusCmpNbrCnv busNbrCnv = busCmpNbrCnvRepository
                .findById(new BusCmpNbrCnvId(masterCompanyNumber, companyLocationNumber)).orElse(null);

        if (busNbrCnv == null) {
            throw new InvalidDataException("invalid.master.company.location",
                    new Object[] { masterCompanyNumber, companyLocationNumber });
        }

        List<BusAcyTrsRules> busAcyTrsRulesList = busAcyTrsRulesRepository.findDataForEditRecords(
                getMasterCompanyNumberList(masterCompanyNumber), getCmpLocationNbrList(companyLocationNumber),
                transactionObjectCode, transactionActionCode, productIdentifier, SHORT_ZERO,
                DateRoutine.defaultDateTime());

        if (busAcyTrsRulesList != null && !busAcyTrsRulesList.isEmpty()) {

            for (BusAcyTrsRules bcyAcyTrsRules : busAcyTrsRulesList) {
                char postingIndicator = bcyAcyTrsRules.getBusrPostingInd();
                char summarizeIndicator = bcyAcyTrsRules.getBusrSummarizeInd();
                Double dataItem = bcyAcyTrsRules.getBusAcyTrsRulesId().getBusrDataItem();

                if (postingIndicator == CHAR_Y || summarizeIndicator == CHAR_Y || postingIndicator == CHAR_B
                        || summarizeIndicator == CHAR_B) {
                    financialApiActivity.setFinancialIndicator(CHAR_Y);

                }

                editItems(financialApiActivity, dataItem);
            }

        } else {

            throw new InvalidDataException("no.data.found");
        }

        if (tableName.equalsIgnoreCase(TableNames.BCM_BUS_ACTIVITY_V.toString())) {
            editRecordsBcmBusActivity(financialApiActivity);
        } else if (tableName.equalsIgnoreCase(TableNames.DWS_BUS_ACTIVITY_V.toString())) {
            editRecordsDwsBusActivity(financialApiActivity);
        }

        return financialApiActivity;

    }

    private FinancialApiActivity editRecordsDwsBusActivity(FinancialApiActivity financialApiActivity) {

        if (!isInvalidDouble(financialApiActivity.getActivityAmount())) {
            throw new InvalidDataException("invalid.amount");
        }

        if (!isInvalidInteger(financialApiActivity.getCheckNumber())) {
            financialApiActivity.setCheckNumber(SHORT_ZERO);
            double dataItem = 45.0;
            editNumeric(financialApiActivity, dataItem);
        }

        if (!isInvalidShort(financialApiActivity.getBankNumber())) {
            financialApiActivity.setBankNumber(SHORT_ZERO);
            double dataItem = 46.0;
            editNumeric(financialApiActivity, dataItem);
        }

        if (!isInvalidShort(financialApiActivity.getAddressSequenceNumber())) {
            financialApiActivity.setAddressSequenceNumber(SHORT_ZERO);
            double dataItem = 58.0;
            editNumeric(financialApiActivity, dataItem);
        }

        if (!isInvalidInteger(financialApiActivity.getNextCheckNumber())) {
            financialApiActivity.setNextCheckNumber(SHORT_ZERO);
            double dataItem = 60.0;
            editNumeric(financialApiActivity, dataItem);
        }

        if (!isInvalidInteger(financialApiActivity.getCurrentBeginCheckNumber())) {
            financialApiActivity.setCurrentBeginCheckNumber(SHORT_ZERO);
            double dataItem = 61.0;
            editNumeric(financialApiActivity, dataItem);
        }

        if (!isInvalidInteger(financialApiActivity.getCurrentEndCheckNumber())) {
            financialApiActivity.setCurrentEndCheckNumber(SHORT_ZERO);
            double dataItem = 62.0;
            editNumeric(financialApiActivity, dataItem);
        }

        if (!isInvalidInteger(financialApiActivity.getNextBeginCheckNumber())) {
            financialApiActivity.setNextBeginCheckNumber(SHORT_ZERO);
            double dataItem = 64.0;
            editNumeric(financialApiActivity, dataItem);
        }

        if (!isInvalidInteger(financialApiActivity.getNextEndCheckNumber())) {
            financialApiActivity.setCurrentEndCheckNumber(SHORT_ZERO);
            double dataItem = 65.0;
            editNumeric(financialApiActivity, dataItem);
        }

        if (!isInvalidInteger(financialApiActivity.getPreNextCheck())) {
            financialApiActivity.setPreNextCheck(SHORT_ZERO);
            double dataItem = 87.0;
            editNumeric(financialApiActivity, dataItem);
        }

        if (financialApiActivity.getEffectiveDate().trim().compareTo(BLANK_STRING) == 0) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { EFFECTIVE_DATE });
        } else {
            try {
                DateRoutine.dateTimeAsYYYYMMDD(financialApiActivity.getEffectiveDate());
            } catch (IllegalArgumentException exception) {
                throw new InvalidDataException(INVALID_DATE, new Object[] { EFFECTIVE_DATE });
            }
        }

        if (financialApiActivity.getCurrentRangeDate().trim().compareTo(BLANK_STRING) == 0) {

            financialApiActivity
                    .setCurrentRangeDate(DateRoutine.dateTimeAsYYYYMMDDString(DateRoutine.defaultDateTime()));
        } else {
            try {
                DateRoutine.dateTimeAsYYYYMMDD(financialApiActivity.getEffectiveDate());
            } catch (IllegalArgumentException exception) {
                throw new InvalidDataException(INVALID_DATE, new Object[] { "Current Range Date" });
            }
        }

        if (financialApiActivity.getNextRangeDate().trim().compareTo(BLANK_STRING) == 0) {

            financialApiActivity.setNextRangeDate(DateRoutine.dateTimeAsYYYYMMDDString(DateRoutine.defaultDateTime()));
        } else {
            try {
                DateRoutine.dateTimeAsYYYYMMDD(financialApiActivity.getEffectiveDate());
            } catch (IllegalArgumentException exception) {
                throw new InvalidDataException(INVALID_DATE, new Object[] { "NextRange Date" });
            }
        }

        if (financialApiActivity.getDisbursementDate().trim().compareTo(BLANK_STRING) == 0) {

            financialApiActivity
                    .setDisbursementDate(DateRoutine.dateTimeAsYYYYMMDDString(DateRoutine.defaultDateTime()));
        } else {
            try {
                DateRoutine.dateTimeAsYYYYMMDD(financialApiActivity.getEffectiveDate());
            } catch (IllegalArgumentException exception) {
                throw new InvalidDataException(INVALID_DATE, new Object[] { "Disbursement Date" });
            }
        }

        return financialApiActivity;
    }

    private FinancialApiActivity editRecordsBcmBusActivity(FinancialApiActivity financialApiActivity) {

        if (!isInvalidDouble(financialApiActivity.getActivityAmount())) {
            throw new InvalidDataException("invalid.amount");
        }

        if (!isInvalidDouble(financialApiActivity.getActivityNetAmount())) {
            financialApiActivity.setActivityNetAmount(DECIMAL_ZERO);
            double dataItem = 89.0;
            editNumeric(financialApiActivity, dataItem);
        }

        if (financialApiActivity.getOriginalEffectiveDate().trim().compareTo(BLANK_STRING) == 0) {

            financialApiActivity
                    .setOriginalEffectiveDate(DateRoutine.dateTimeAsYYYYMMDDString(DateRoutine.defaultDateTime()));
        } else {
            try {
                DateRoutine.dateTimeAsYYYYMMDD(financialApiActivity.getOriginalEffectiveDate());
            } catch (IllegalArgumentException exception) {
                throw new InvalidDataException(INVALID_DATE, new Object[] { "Original Effective Date" });
            }
        }

        if (financialApiActivity.getReferenceDate().trim().compareTo(BLANK_STRING) == 0) {

            financialApiActivity.setReferenceDate(DateRoutine.dateTimeAsYYYYMMDDString(DateRoutine.defaultDateTime()));
        } else {
            try {
                DateRoutine.dateTimeAsYYYYMMDD(financialApiActivity.getReferenceDate());
            } catch (IllegalArgumentException exception) {
                throw new InvalidDataException(INVALID_DATE, new Object[] { "Bill Reference Date" });
            }
        }

        if (financialApiActivity.getBilPostDate().trim().compareTo(BLANK_STRING) == 0) {

            financialApiActivity.setBilPostDate(DateRoutine.dateTimeAsYYYYMMDDString(DateRoutine.defaultDateTime()));
        } else {
            try {
                DateRoutine.dateTimeAsYYYYMMDD(financialApiActivity.getBilPostDate());
            } catch (IllegalArgumentException exception) {
                throw new InvalidDataException(INVALID_DATE, new Object[] { "Posting Date" });
            }
        }

        if (financialApiActivity.getEffectiveDate().trim().compareTo(BLANK_STRING) == 0) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { EFFECTIVE_DATE });
        } else {
            try {
                DateRoutine.dateTimeAsYYYYMMDD(financialApiActivity.getEffectiveDate());
            } catch (IllegalArgumentException exception) {
                throw new InvalidDataException(INVALID_DATE, new Object[] { EFFECTIVE_DATE });
            }
        }

        return financialApiActivity;
    }

    private boolean isInvalidDouble(double amount) {
        return amount != INVALID_DOUBLE_VAL;
    }

    private boolean isInvalidInteger(int number) {
        return number != INVALID_INT_VAL;
    }

    private boolean isInvalidShort(short number) {
        return number != INVALID_SHORT_VAL;
    }

    private void editNumeric(FinancialApiActivity financialApiActivity, double dataItem) {

        List<Character> indicatorList = new ArrayList<>();
        indicatorList.add(CHAR_Y);
        indicatorList.add(CHAR_B);

        Short count = busAcyTrsRulesRepository.findDataForEditNumeric(
                getMasterCompanyNumberList(financialApiActivity.getMasterCompanyNbr()),
                getCmpLocationNbrList(financialApiActivity.getCompanyLocationNbr()),
                financialApiActivity.getTransactionObjectCode(), financialApiActivity.getTransactionActionCode(),
                financialApiActivity.getApplicationProductIdentifier(), SHORT_ZERO, DateRoutine.defaultDateTime(),
                dataItem, indicatorList);

        if (count != null && !count.equals(SHORT_ZERO)) {

            throw new InvalidDataException("non.numeric.required", new Object[] { getDescription(dataItem) });

        }

    }

    private void editItems(FinancialApiActivity financialApiActivity, Double dataItem) {

        if (dataItem.equals(DataItem.DECIMAL_ONE.getDataItem()) && financialApiActivity.getOperatorId().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Operator Id" });
        }

        if (dataItem.equals(DataItem.DECIMAL_TWO.getDataItem()) && financialApiActivity.getCountyCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "County Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_THREE.getDataItem()) && financialApiActivity.getAppProgramId().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Program Id" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FOUR.getDataItem()) && financialApiActivity.getStateCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "State Code" });
        }
        if (dataItem.equals(DataItem.DECIMAL_FIVE.getDataItem()) && financialApiActivity.getCountryCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Country Code" });
        }
        if (dataItem.equals(DataItem.DECIMAL_SIX.getDataItem()) && financialApiActivity.getLineOfBusCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Line Of Business" });
        }
        if (dataItem.equals(DataItem.DECIMAL_SEVEN.getDataItem()) && financialApiActivity.getPolicyId().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Policy Id" });
        }
        if (dataItem.equals(DataItem.DECIMAL_EIGHT.getDataItem()) && financialApiActivity.getAgentId().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Agent Id" });
        }
        if (dataItem.equals(DataItem.DECIMAL_TWENTYSEVEN.getDataItem())
                && financialApiActivity.getBilDatabaseKey().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Database Key" });
        }
        if (dataItem.equals(DataItem.DECIMAL_TWENTYEIGHT.getDataItem())
                && financialApiActivity.getOriginalEffectiveDate().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Original Effective Date" });
        }

        if (dataItem.equals(DataItem.DECIMAL_TWENTYNINE.getDataItem())
                && financialApiActivity.getBilBankCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Bank Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_ONEZERONINE.getDataItem())
                && financialApiActivity.getBilBankCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Deposit Bank Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_ONEONEZERO.getDataItem())
                && financialApiActivity.getDisbursementId().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Disbursement Id" });
        }

        if (dataItem.equals(DataItem.DECIMAL_THIRTY.getDataItem())
                && financialApiActivity.getReferenceDate().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Reference Date" });
        }

        if (dataItem.equals(DataItem.DECIMAL_THIRTYONE.getDataItem())
                && financialApiActivity.getBilPostDate().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Posting Date" });
        }

        if (dataItem.equals(DataItem.DECIMAL_THIRTYTWO.getDataItem())
                && financialApiActivity.getBilAccountId().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Account Id" });
        }

        if (dataItem.equals(DataItem.DECIMAL_THIRTYTHREE.getDataItem())
                && financialApiActivity.getBilReasonCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Reason Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_THIRTYFOUR.getDataItem())
                && financialApiActivity.getBilReceiptTypeCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Receipt Type Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_THIRTYFIVE.getDataItem())
                && financialApiActivity.getBilTypeCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Bill Type Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_THIRTYSIX.getDataItem())
                && financialApiActivity.getBilClassCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Class Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FORTYTHREE.getDataItem())
                && financialApiActivity.getPayableItemCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Payable Item Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_SIXTYNINE.getDataItem())
                && financialApiActivity.getAgentTtyId().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Agent/Third Party Id" });
        }

        if (dataItem.equals(DataItem.DECIMAL_EIGHTYFIVE.getDataItem())
                && financialApiActivity.getBilSourceCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Billing Source Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FORTYSEVEN.getDataItem())
                && financialApiActivity.getBankAccountNumber().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Bank Account Number" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FORTYEIGHT.getDataItem())
                && financialApiActivity.getProcessingLocationCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER,
                    new Object[] { "Disbursement Processing Location" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FORTYNINE.getDataItem())
                && financialApiActivity.getDisbursementTypeCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Disbursement Type Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FIFTY.getDataItem()) && financialApiActivity.getDwsProductId().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Disbursement Product Id" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FIFTYONE.getDataItem())
                && financialApiActivity.getMarketSectorCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Market Sector Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FIFTYTWO.getDataItem())
                && financialApiActivity.getDistrictOfficeName().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "District Office Number" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FIFTYTHREE.getDataItem())
                && financialApiActivity.getMediumCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Medium Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FIFTYFOUR.getDataItem()) && financialApiActivity.getPayee1Id().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Disbursement Payee One" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FIFTYFIVE.getDataItem()) && financialApiActivity.getPayee2Id().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Disbursement Payee Two" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FIFTYSIX.getDataItem()) && financialApiActivity.getPayee3Id().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Disbursement Payee Three" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FIFTYSEVEN.getDataItem())
                && financialApiActivity.getMailRecipientId().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Mail Recipient Id" });
        }

        if (dataItem.equals(DataItem.DECIMAL_FIFTYNINE.getDataItem())
                && financialApiActivity.getDwsStatusCode() == BLANK_CHAR) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Disbursement Status Code" });
        }

        if (dataItem.equals(DataItem.DECIMAL_SIXTYTHREE.getDataItem())
                && financialApiActivity.getCurrentRangeDate().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Current Range Date" });
        }

        if (dataItem.equals(DataItem.DECIMAL_SIXTYSIX.getDataItem())
                && financialApiActivity.getNextRangeDate().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Next Range Date" });
        }

        if (dataItem.equals(DataItem.DECIMAL_SIXTYSEVEN.getDataItem())
                && financialApiActivity.getDisbursementDate().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Disbursement Date" });
        }

        if (dataItem.equals(DataItem.DECIMAL_SIXTYEIGHT.getDataItem())
                && financialApiActivity.getCheckUsageCode().isEmpty()) {

            throw new DataNotFoundException(BAD_REQUEST_MISSING_PARAMETER, new Object[] { "Check Usage Code" });
        }

    }

    private void insertIntoBcmBusActivity(FinancialApiActivity financialApiActivity, ZonedDateTime busaTimeStamp) {

        short selectAttempts = SHORT_ZERO;
        short retryNbr = (short) 8;

        BcmBusActivity bcmBusActivity;
        if (financialApiActivity.getBusinessCaseId().isEmpty()) {
            financialApiActivity
                    .setBusinessCaseId(getBusCaseId(financialApiActivity.getApplicationProductIdentifier()));
        }

        bcmBusActivity = getBcmBusActivityObject(financialApiActivity, busaTimeStamp);

        while (selectAttempts <= retryNbr) {

            if (bcmBusActivityRepository.existsById(bcmBusActivity.getBcmBusActivityId())) {
                busaTimeStamp = busaTimeStamp.plus(Integer.valueOf(IdGenerator.generateUniqueNumber(3)),
                        ChronoUnit.MILLIS);
                selectAttempts = (short) (selectAttempts + 1);
                bcmBusActivity.getBcmBusActivityId().setBusaTimestamp(busaTimeStamp);
            } else {
                selectAttempts = (short) (retryNbr + 1);
            }
        }
        bcmBusActivityRepository.saveAndFlush(bcmBusActivity);

    }

    private String getBusCaseId(String applicationProductIdentifier) {
        return IdGenerator.generateUniqueStringUpperCase(10) + SEPARATOR_BLANK + SEPARATOR_BLANK
                + applicationProductIdentifier.substring(SHORT_ZERO, 3) + BUS_CASE_ID;
    }

    private BcmBusActivity getBcmBusActivityObject(FinancialApiActivity financialApiActivity,
            ZonedDateTime busaTimeStamp) {
        BcmBusActivity bcmBusActivity = new BcmBusActivity();
        bcmBusActivity.setBcmBusActivityId(new BcmBusActivityId(financialApiActivity.getTransactionObjectCode(),
                financialApiActivity.getTransactionActionCode(), busaTimeStamp,
                financialApiActivity.getApplicationProductIdentifier(), financialApiActivity.getMasterCompanyNbr(),
                financialApiActivity.getCompanyLocationNbr(), financialApiActivity.getFinancialIndicator()));

        bcmBusActivity.setBusaActivityAmt(financialApiActivity.getActivityAmount());
        bcmBusActivity.setBusaAcyNetAmt(financialApiActivity.getActivityNetAmount());
        bcmBusActivity.setBusaAgentId(financialApiActivity.getAgentId());
        bcmBusActivity.setBusaAgtTtyId(financialApiActivity.getAgentTtyId());
        bcmBusActivity.setBusaAppPgmId(financialApiActivity.getAppProgramId());
        bcmBusActivity.setBusaBilAccountid(financialApiActivity.getBilAccountId());
        bcmBusActivity.setBusaBilBankCd(financialApiActivity.getBilBankCode());
        bcmBusActivity.setBusaBilClsCd(financialApiActivity.getBilClassCode());
        bcmBusActivity.setBusaBilDbKey(financialApiActivity.getBilDatabaseKey());
        bcmBusActivity.setBusaBilPostDt(financialApiActivity.getBilPostDate());
        bcmBusActivity.setBusaBilRctTyc(financialApiActivity.getBilReceiptTypeCode());
        bcmBusActivity.setBusaBilReasonCd(financialApiActivity.getBilReasonCode());
        bcmBusActivity.setBusaBilRfrDt(financialApiActivity.getReferenceDate());
        bcmBusActivity.setBusaBilSrcCd(financialApiActivity.getBilSourceCode());
        bcmBusActivity.setBusaBilTypeCd(financialApiActivity.getBilTypeCode());
        bcmBusActivity.setBusaBusCaseId(financialApiActivity.getBusinessCaseId());
        bcmBusActivity.setBusaClientId(financialApiActivity.getClientId());
        bcmBusActivity.setBusaCountryCd(financialApiActivity.getCountryCode());
        bcmBusActivity.setBusaCountyCd(financialApiActivity.getCountyCode());
        String currencyCode = multiCurrencyService.getCurrencyCode(financialApiActivity.getCurrencyCode());
        bcmBusActivity.setBusaCurrencyCd(currencyCode == null ? SEPARATOR_BLANK : currencyCode);
        bcmBusActivity.setBusaDepBankCd(financialApiActivity.getDepositeBankCode());
        bcmBusActivity.setBusaDwsDsbId(financialApiActivity.getDisbursementId());
        bcmBusActivity.setBusaEffectiveDt(financialApiActivity.getEffectiveDate());
        bcmBusActivity.setBusaExpirationDt(DateRoutine.defaultDateTime());
        bcmBusActivity.setBusaGnrcCsrInf1(financialApiActivity.getCustomerInfo1());
        bcmBusActivity.setBusaGnrcCsrInf2(financialApiActivity.getCustomerInfo2());
        bcmBusActivity.setBusaGnrcCsrInf3(financialApiActivity.getCustomerInfo3());
        bcmBusActivity.setBusaGnrcCsrInf4(financialApiActivity.getCustomerInfo4());
        bcmBusActivity.setBusaGnrcCsrInf5(financialApiActivity.getCustomerInfo5());
        bcmBusActivity.setBusaHistoryInd(SHORT_ZERO);
        bcmBusActivity.setBusaLinOfBusCd(financialApiActivity.getLineOfBusCode());
        bcmBusActivity.setBusaMktSctCd(financialApiActivity.getMarketSectorCode());
        bcmBusActivity.setBusaOgnEffDt(financialApiActivity.getOriginalEffectiveDate());
        bcmBusActivity.setBusaOgnTimestamp(busaTimeStamp);
        bcmBusActivity.setBusaOperatorId(financialApiActivity.getOperatorId());
        bcmBusActivity.setBusaPblItmCd(financialApiActivity.getPayableItemCode());
        bcmBusActivity.setBusaPolicyId(financialApiActivity.getPolicyId());
        bcmBusActivity.setBusaStateCode(financialApiActivity.getStateCode());
        bcmBusActivity.setBusaStatusCd(financialApiActivity.getStatusCode());
        bcmBusActivity.setBussSumEffDt(DateRoutine.defaultDateTime());
        return bcmBusActivity;
    }

    private void insertIntoDwsBusActivity(FinancialApiActivity financialApiActivity, ZonedDateTime busaTimeStamp) {

        short selectAttempts = SHORT_ZERO;
        short retryNbr = (short) 3;

        DwsBusActivity dwsBusActivity;
        if (financialApiActivity.getBusinessCaseId().isEmpty()) {
            financialApiActivity
                    .setBusinessCaseId(getBusCaseId(financialApiActivity.getApplicationProductIdentifier()));
        }
        dwsBusActivity = getDwsBusActivityObject(financialApiActivity, busaTimeStamp);

        while (selectAttempts <= retryNbr) {

            if (dwsBusActivityRepository.existsById(dwsBusActivity.getDwsBusActivityId())) {
                busaTimeStamp = busaTimeStamp.plus(Integer.valueOf(IdGenerator.generateUniqueNumber(3)),
                        ChronoUnit.MILLIS);
                selectAttempts = (short) (selectAttempts + 1);
                dwsBusActivity.getDwsBusActivityId().setBusaTimestamp(busaTimeStamp);
            } else {
                selectAttempts = (short) (retryNbr + 1);
            }
        }
        dwsBusActivityRepository.save(dwsBusActivity);
    }

    private DwsBusActivity getDwsBusActivityObject(FinancialApiActivity financialApiActivity,
            ZonedDateTime busaTimeStamp) {

        DwsBusActivity dwsBusActivity = new DwsBusActivity();

        dwsBusActivity.setDwsBusActivityId(new DwsBusActivityId(financialApiActivity.getTransactionObjectCode(),
                financialApiActivity.getTransactionActionCode(), busaTimeStamp,
                financialApiActivity.getApplicationProductIdentifier(), financialApiActivity.getMasterCompanyNbr(),
                financialApiActivity.getCompanyLocationNbr(), financialApiActivity.getFinancialIndicator()));

        dwsBusActivity.setBusaHistoryInd(SHORT_ZERO);
        dwsBusActivity.setBusaStatusCd(financialApiActivity.getStatusCode());
        dwsBusActivity.setBusaExpirationDt(DateRoutine.defaultDateTime());
        dwsBusActivity.setBusaOgnTimestamp(financialApiActivity.getOriginalTimeStamp());
        dwsBusActivity.setBusaEffectiveDt(financialApiActivity.getEffectiveDate());
        dwsBusActivity.setBusaOperatorId(financialApiActivity.getOperatorId());
        dwsBusActivity.setBusaAppPgmId(financialApiActivity.getAppProgramId());
        dwsBusActivity.setBusaCountyCd(financialApiActivity.getCountyCode());
        dwsBusActivity.setBusaStateCode(financialApiActivity.getStateCode());
        dwsBusActivity.setBusaCountryCd(financialApiActivity.getCountryCode());
        dwsBusActivity.setBusaLinOfBusCd(financialApiActivity.getLineOfBusCode());
        dwsBusActivity.setBusaPolicyId(financialApiActivity.getPolicyId());
        dwsBusActivity.setBusaAgentId(financialApiActivity.getAgentId());
        dwsBusActivity.setBusaBusCaseId(financialApiActivity.getBusinessCaseId());
        dwsBusActivity.setBusaMktSctCd(financialApiActivity.getMarketSectorCode());
        dwsBusActivity.setBusaClientId(financialApiActivity.getClientId());
        dwsBusActivity.setBusaGnrcCsrInf1(financialApiActivity.getCustomerInfo1());
        dwsBusActivity.setBusaGnrcCsrInf2(financialApiActivity.getCustomerInfo2());
        dwsBusActivity.setBusaGnrcCsrInf3(financialApiActivity.getCustomerInfo3());
        dwsBusActivity.setBusaGnrcCsrInf4(financialApiActivity.getCustomerInfo4());
        dwsBusActivity.setBusaGnrcCsrInf5(financialApiActivity.getCustomerInfo5());
        dwsBusActivity.setBusaActivityAmt(financialApiActivity.getActivityAmount());
        String currencyCode = multiCurrencyService.getCurrencyCode(financialApiActivity.getCurrencyCode());
        dwsBusActivity.setBusaCurrencyCd(currencyCode == null ? SEPARATOR_BLANK : currencyCode);
        dwsBusActivity.setBusaDwsDsbId(financialApiActivity.getDisbursementId());
        dwsBusActivity.setBusaDwsCkNbr(financialApiActivity.getCheckNumber());
        dwsBusActivity.setBusaDwsBankNbr(financialApiActivity.getBankNumber());
        dwsBusActivity.setBusaBankActNbr(financialApiActivity.getBankAccountNumber());
        dwsBusActivity.setBusaPrcLocCd(financialApiActivity.getProcessingLocationCode());
        dwsBusActivity.setBusaDwsTypeCd(financialApiActivity.getDisbursementTypeCode());
        dwsBusActivity.setBusaDwsPrdId(financialApiActivity.getDwsProductId());
        dwsBusActivity.setBusaDstOfficeNm(financialApiActivity.getDistrictOfficeName());
        dwsBusActivity.setBusaDwsMidCd(financialApiActivity.getMediumCode());
        dwsBusActivity.setBusaPayee1Id(financialApiActivity.getPayee1Id());
        dwsBusActivity.setBusaPayee2Id(financialApiActivity.getPayee2Id());
        dwsBusActivity.setBusaPayee3Id(financialApiActivity.getPayee3Id());
        dwsBusActivity.setBusaMailRecId(financialApiActivity.getMailRecipientId());
        dwsBusActivity.setBusaAdrSeqNbr(financialApiActivity.getAddressSequenceNumber());
        dwsBusActivity.setBusaDwsStatusCd(financialApiActivity.getDwsStatusCode());
        dwsBusActivity.setBusaNextCkNbr(financialApiActivity.getNextCheckNumber());
        dwsBusActivity.setBusaCurBegCk(financialApiActivity.getCurrentBeginCheckNumber());
        dwsBusActivity.setBusaCurEnCkNbr(financialApiActivity.getCurrentEndCheckNumber());
        dwsBusActivity.setBusaCurRgeDt(financialApiActivity.getCurrentRangeDate());
        dwsBusActivity.setBusaNxtBegCk(financialApiActivity.getNextBeginCheckNumber());
        dwsBusActivity.setBusaNxtEnCkNbr(financialApiActivity.getNextEndCheckNumber());
        dwsBusActivity.setBusaNxtRgeDt(financialApiActivity.getNextRangeDate());
        dwsBusActivity.setBusaDwsDsbDt(financialApiActivity.getDisbursementDate());
        dwsBusActivity.setBusaCkUsageCd(financialApiActivity.getCheckUsageCode());
        dwsBusActivity.setBusaPreNextCk(financialApiActivity.getPreNextCheck());
        dwsBusActivity.setBussSumEffDt(financialApiActivity.getSummaryEffectiveDate());
        dwsBusActivity.setBusaOgnTimestamp(busaTimeStamp);

        return dwsBusActivity;
    }

    private List<Character> getBusrIndList() {
        List<Character> busrIndList = new ArrayList<>();
        busrIndList.add(CHAR_B);
        busrIndList.add(CHAR_Y);
        return busrIndList;
    }

    private List<String> getCmpLocationNbrList(String companyLocationNumber) {
        List<String> cmpLocationNbrList = new ArrayList<>();
        cmpLocationNbrList.add(DEFAULT_NBR);
        cmpLocationNbrList.add(companyLocationNumber);
        return cmpLocationNbrList;
    }

    private List<String> getMasterCompanyNumberList(String masterCoNumber) {
        List<String> masterCompanyNumberList = new ArrayList<>();
        masterCompanyNumberList.add(DEFAULT_NBR);
        masterCompanyNumberList.add(masterCoNumber);
        return masterCompanyNumberList;
    }

    private List<String> getFwsAppDataCdList() {
        List<String> fwsAppDataCdList = new ArrayList<>();
        fwsAppDataCdList.add(String.valueOf(CHAR_E));
        fwsAppDataCdList.add(String.valueOf(CHAR_N));
        return fwsAppDataCdList;
    }

    public String getDescription(Double dataItem) {

        String busCodeType = "BusinessActivity";
        String busParentCode = "BUSACY";

        String dataItemString = String.valueOf(dataItem.intValue());

        BusCdTranslation busCdTranslation = busCdTranslationRepository
                .findById(new BusCdTranslationId(String.valueOf(dataItemString), busCodeType, "EN", busParentCode))
                .orElse(null);

        if (busCdTranslation == null) {
            logger.error("No Description Found");

            throw new DataNotFoundException("support.data.not.found",
                    new Object[] { String.valueOf(dataItemString), busCodeType });
        }

        return busCdTranslation.getBusDescription().trim();
    }

}