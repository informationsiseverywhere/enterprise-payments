package fis.utils;

public class FisConstants {

    public enum DataItem {
        DECIMAL_ONE(1.0),
        DECIMAL_TWO(2.0),
        DECIMAL_THREE(3.0),
        DECIMAL_FOUR(4.0),
        DECIMAL_FIVE(5.0),
        DECIMAL_SIX(6.0),
        DECIMAL_SEVEN(7.0),
        DECIMAL_EIGHT(8.0),
        DECIMAL_TWENTYSEVEN(27.0),
        DECIMAL_TWENTYEIGHT(28.0),
        DECIMAL_TWENTYNINE(29.0),
        DECIMAL_ONEZERONINE(109.0),
        DECIMAL_ONEONEZERO(110.0),
        DECIMAL_THIRTY(30.0),
        DECIMAL_THIRTYONE(31.0),
        DECIMAL_THIRTYTWO(32.0),
        DECIMAL_THIRTYTHREE(33.0),
        DECIMAL_THIRTYFOUR(34.0),
        DECIMAL_THIRTYFIVE(35.0),
        DECIMAL_THIRTYSIX(36.0),
        DECIMAL_FORTYTHREE(43.0),
        DECIMAL_FORTYSEVEN(47.0),
        DECIMAL_FORTYEIGHT(48.0),
        DECIMAL_FORTYNINE(49.0),
        DECIMAL_FIFTY(50.0),
        DECIMAL_FIFTYONE(51.0),
        DECIMAL_FIFTYTWO(52.0),
        DECIMAL_FIFTYTHREE(53.0),
        DECIMAL_FIFTYFOUR(54.0),
        DECIMAL_FIFTYFIVE(55.0),
        DECIMAL_FIFTYSIX(56.0),
        DECIMAL_FIFTYSEVEN(57.0),
        DECIMAL_FIFTYNINE(59.0),
        DECIMAL_SIXTYTHREE(63.0),
        DECIMAL_SIXTYSIX(66.0),
        DECIMAL_SIXTYSEVEN(67.0),
        DECIMAL_SIXTYEIGHT(68.0),
        DECIMAL_SIXTYNINE(69.0),
        DECIMAL_EIGHTYFIVE(85.0);

        private final Double dataItem;

        private DataItem(Double dataItem) {
            this.dataItem = dataItem;
        }

        public Double getDataItem() {
            return dataItem;
        }

    }

    public enum ErrorCode {
        FWSU2050("FWSU2050"),
        FWSU0050("FWSU0050"),
        FWSU0051("FWSU0051"),
        FWSU2051("FWSU2051"),
        FWSU0020("FWSU0020"),
        FWSU3005("FWSU3005"),
        FWSU3004("FWSU3004"),
        FWSU1000("FWSU1000"),
        FWSU1101("FWSU1101"),
        FWSU0600("FWSU0600"),
        FWSU0820("FWSU0820"),
        FWSU0850("FWSU0850"),
        FWSU0840("FWSU0840"),
        FWSU0830("FWSU0830"),
        FWSU0810("FWSU0810"),
        FWSU0811("FWSU0811"),
        FWSU0401("FWSU0401"),
        FWSU2102("FWSU2102"),
        FWSU1100("FWSU1100"),
        FWSR7003("FWSR7003"),
        FWSR7023("FWSR7023"),
        FWSR7002("FWSR7002"),
        FWSR7010("FWSR7010"),
        FWSR7008("FWSR7008"),
        FWSR7016("FWSR7016"),
        FWSS7106("FWSS7106"),
        FWSS7103("FWSS7103"),
        FWST7212("FWST7212"),
        FWSH4003("FWSH4003"),
        FWSE4157("FWSE4157"),
        FWSE4158("FWSE4158"),
        FWSE4160("FWSE4160"),
        FWSH4012("FWSH4012"),
        FWSH4013("FWSH4013"),
        FWSH4011("FWSH4011"),
        FWSH4005("FWSH4005"),
        FWSH4006("FWSH4006"),
        FWSH4014("FWSH4014"),
        FWSE4142("FWSE4142"),
        FWSH4018("FWSH4018"),
        FWSH4147("FWSH4147"),
        FWSE4155("FWSE4155"),
        FWSE4132("FWSE4132"),
        FWSE4151("FWSE4151"),
        FWSE4020("FWSE4020"),
        FWSE4126("FWSE4126"),
        FWSN6000("FWSN6000"),
        FWSN6072("FWSN6072"),
        FWSN6071("FWSN6071"),
        FWSN6008("FWSN6008"),
        FWSN6009("FWSN6009"),
        FWSN6050("FWSN6050"),
        FWSN6048("FWSN6048"),
        FWSN6007("FWSN6007"),
        FWSN6006("FWSN6006"),
        FWSN6014("FWSN6014"),
        FWSN6063("FWSN6063"),
        FWSN6012("FWSN6012"),
        FWSN6069("FWSN6069"),
        FWSN6010("FWSN6010"),
        FWSN6016("FWSN6016"),
        FWSN6070("FWSN6070"),
        FWSN6022("FWSN6022"),
        FWSN6028("FWSN6028"),
        FWSN6025("FWSN6025"),
        FWSN6062("FWSN6062"),
        FWSN6019("FWSN6019"),
        FWSN6030("FWSN6030"),
        FWSN6034("FWSN6034"),
        FWSN6033("FWSN6033"),
        FWSN6036("FWSN6036"),
        FWSN6066("FWSN6066"),
        FWSN6067("FWSN6067");

        private final String errorCd;

        private ErrorCode(String errorCd) {
            this.errorCd = errorCd;
        }

        public boolean hasValue(String value) {
            return value == null ? false : errorCd.equals(value);
        }

        @Override
        public String toString() {
            return this.errorCd;
        }
    }

}
