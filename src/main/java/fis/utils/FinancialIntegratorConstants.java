package fis.utils;

public class FinancialIntegratorConstants {

    public static final String CUSTOMER_INFO1 = "FWSOADBC TEST DATA 1";
    public static final String CUSTOMER_INFO2 = "FWSOADBC TEST DATA 2";
    public static final String CUSTOMER_INFO3 = "FWSOADBC TEST DATA 3";
    public static final String CUSTOMER_INFO4 = "FWSOADBC TEST DATA 4";
    public static final String CUSTOMER_INFO5 = "FWSOADBC TEST DATA 5";

    public enum ApplicationProduct {

        BCMS("BCMS"),
        CCMS("CCMS"),
        DWS("DWS"),
        PRM("PRM"),
        CWS("CWS"),
        MIS("MIS"),
        RMS("RMS"),
        UWS("UWS"),
        FWS("FWS"),
        BCUSR("BCUSR"),
        DWUSR("DWUSR");

        private final String type;

        private ApplicationProduct(String type) {
            this.type = type;
        }

        public boolean hasValue(String value) {
            return value != null && type.equals(value);
        }

        @Override
        public String toString() {
            return this.type;
        }
    }

    public enum ObjectCode {

        LATE_CHARGE("LTE"),
        PENALTY_CHARGE("PNY"),
        SERVICE_CHARGE("SCG"),
        DOWN_PAYMENT_FEE("DPF"),
        PREMIUM("PRM"),
        CASH("CSH"),
        UNIDENTIFIED_CASH("UCS"),
        BILLING_COLLECTION("BCM"),
        DAILY_BALANCE("DIF"),
        DISBURSEMENT("DSB"),
        CHECK("CHK"),
        BALANCE("BAL"),
        COLLECTIONS("COL"),
        PRE_COLLECTIONS("PCL");

        private final String type;

        private ObjectCode(String type) {
            this.type = type;
        }

        public boolean hasValue(String value) {
            return value != null && type.equals(value);
        }

        @Override
        public String toString() {
            return this.type;
        }

    }

    public enum ActionCode {

        SCHEDULE_TO_BILL("SBL"),
        BILLED("BIL"),
        WRITE_OFF_INSTALL("WOI"),
        WRITE_OFF_UNBILLED("WOU"),
        WRITE_OFF_SERVICE_CHARGE("WOS"),
        WRITE_OFF("WRO"),
        REVERSE_WRITE_OFF("RWR"),
        REVERSE_DISBURSEMENT("RDA"),
        SUSPENSE("SUS"),
        REVERSE_SUSPENSE("RSU"),
        TRANSFER("TRF"),
        APPLIED("APP"),
        REVERSE_APPLIED("RAP"),
        DISBUSRED("DB"),
        SUPPLEMENTAL_PAYMENT("SP"),
        CASH_DISPOSITION("BCD"),
        APPLY_TO_INSTALL("API"),
        APPLY_TO_UNBILLED("APU"),
        ADVANCE_PAY_INVOICED("ADI"),
        ADVANCE_PAY_UNINVOICED("ADU"),
        ADVANCE_PREMIUM("ADV"),
        SELECT_COMBINED_PAYMENT("SCP"),
        DISBUSRED_1("DSB"),
        VOID("VOD"),
        COMBINED_PAYMENT("CBN"),
        RECEIVED("RCV"),
        CANCELLED("CNL"),
        DEPOSIT("DEP"),
        AUTHOR_DISBURSEMENT("DIA"),
        UPDATED("UPD"),
        APPLY_SERVICE_CHARGE("APS"),
        CLOSED_BILLED("CLI"),
        CLOSED_UNBILLED("CLU"),
        CREDIT_AGST_DEBIT("CAD"),
        CREDIT_SCHEDULE_TO_BILL("CSB"),
        BALANCE("BAL");

        private final String type;

        private ActionCode(String type) {
            this.type = type;
        }

        public boolean hasValue(String value) {
            return value != null && type.equals(value);
        }

        @Override
        public String toString() {
            return this.type;
        }

    }

    public enum FunctionCode {

        SEND('S'), APPLY('A');

        private final Character type;

        private FunctionCode(Character type) {
            this.type = type;
        }

        public Character getValue() {
            return type;
        }

    }

    public enum TableNames {
        BCM_BUS_ACTIVITY_V("BCM_BUS_ACTIVITY_V"),
        CCM_BUS_ACTIVITY_V("CCM_BUS_ACTIVITY_V"),
        CWS_BUS_ACTIVITY_V("CWS_BUS_ACTIVITY_V"),
        DWS_BUS_ACTIVITY_V("DWS_BUS_ACTIVITY_V"),
        MIS_BUS_ACTIVITY_V("MIS_BUS_ACTIVITY_V"),
        RMS_BUS_ACTIVITY_V("RMS_BUS_ACTIVITY_V"),
        UWS_BUS_ACTIVITY_V("UWS_BUS_ACTIVITY_V");

        private final String tableName;

        private TableNames(String tableName) {
            this.tableName = tableName;
        }

        public boolean hasValue(String value) {
            return value != null && tableName.equals(value);
        }

        @Override
        public String toString() {
            return this.tableName;
        }
    }

    public enum DataItem {
        DECIMAL_ONE(1.0),
        DECIMAL_TWO(2.0),
        DECIMAL_THREE(3.0),
        DECIMAL_FOUR(4.0),
        DECIMAL_FIVE(5.0),
        DECIMAL_SIX(6.0),
        DECIMAL_SEVEN(7.0),
        DECIMAL_EIGHT(8.0),
        DECIMAL_TWENTYSEVEN(27.0),
        DECIMAL_TWENTYEIGHT(28.0),
        DECIMAL_TWENTYNINE(29.0),
        DECIMAL_ONEZERONINE(109.0),
        DECIMAL_ONEONEZERO(110.0),
        DECIMAL_THIRTY(30.0),
        DECIMAL_THIRTYONE(31.0),
        DECIMAL_THIRTYTWO(32.0),
        DECIMAL_THIRTYTHREE(33.0),
        DECIMAL_THIRTYFOUR(34.0),
        DECIMAL_THIRTYFIVE(35.0),
        DECIMAL_THIRTYSIX(36.0),
        DECIMAL_FORTYTHREE(43.0),
        DECIMAL_FORTYSEVEN(47.0),
        DECIMAL_FORTYEIGHT(48.0),
        DECIMAL_FORTYNINE(49.0),
        DECIMAL_FIFTY(50.0),
        DECIMAL_FIFTYONE(51.0),
        DECIMAL_FIFTYTWO(52.0),
        DECIMAL_FIFTYTHREE(53.0),
        DECIMAL_FIFTYFOUR(54.0),
        DECIMAL_FIFTYFIVE(55.0),
        DECIMAL_FIFTYSIX(56.0),
        DECIMAL_FIFTYSEVEN(57.0),
        DECIMAL_FIFTYNINE(59.0),
        DECIMAL_SIXTYTHREE(63.0),
        DECIMAL_SIXTYSIX(66.0),
        DECIMAL_SIXTYSEVEN(67.0),
        DECIMAL_SIXTYEIGHT(68.0),
        DECIMAL_SIXTYNINE(69.0),
        DECIMAL_EIGHTYFIVE(85.0);

        private final Double dataItem;

        private DataItem(Double dataItem) {
            this.dataItem = dataItem;
        }

        public Double getDataItem() {
            return dataItem;
        }

    }

    public enum ErrorCode {
        FWSU2050("FWSU2050"),
        FWSU0050("FWSU0050"),
        FWSU0051("FWSU0051"),
        FWSU2051("FWSU2051"),
        FWSU0020("FWSU0020"),
        FWSU3005("FWSU3005"),
        FWSU3004("FWSU3004"),
        FWSU1000("FWSU1000"),
        FWSU1101("FWSU1101"),
        FWSU0600("FWSU0600"),
        FWSU0820("FWSU0820"),
        FWSU0850("FWSU0850"),
        FWSU0840("FWSU0840"),
        FWSU0830("FWSU0830"),
        FWSU0810("FWSU0810"),
        FWSU0811("FWSU0811"),
        FWSU0401("FWSU0401"),
        FWSU2102("FWSU2102"),
        FWSU1100("FWSU1100"),
        FWSR7003("FWSR7003"),
        FWSR7023("FWSR7023"),
        FWSR7002("FWSR7002"),
        FWSR7010("FWSR7010"),
        FWSR7008("FWSR7008"),
        FWSR7016("FWSR7016"),
        FWSS7106("FWSS7106"),
        FWSS7103("FWSS7103"),
        FWST7212("FWST7212"),
        FWSH4003("FWSH4003"),
        FWSE4157("FWSE4157"),
        FWSE4158("FWSE4158"),
        FWSE4160("FWSE4160"),
        FWSH4012("FWSH4012"),
        FWSH4013("FWSH4013"),
        FWSH4011("FWSH4011"),
        FWSH4005("FWSH4005"),
        FWSH4006("FWSH4006"),
        FWSH4014("FWSH4014"),
        FWSE4142("FWSE4142"),
        FWSH4018("FWSH4018"),
        FWSH4147("FWSH4147"),
        FWSE4155("FWSE4155"),
        FWSE4132("FWSE4132"),
        FWSE4151("FWSE4151"),
        FWSE4020("FWSE4020"),
        FWSE4126("FWSE4126"),
        FWSE1003("FWSE1003"),
        FWSE1005("FWSE1005"),
        FWSE1017("FWSE1017"),
        FWSXXXX("FWSXXXX"),
        FWSE1002("FWSE1002"),
        FWSE1012("FWSE1012"),
        FWSE1013("FWSE1013"),
        FWSE1004("FWSE1004"),
        FWSE1014("FWSE1014"),
        FWSE1099("FWSE1099"),
        FWSE1007("FWSE1007");

        private final String errorCd;

        private ErrorCode(String errorCd) {
            this.errorCd = errorCd;
        }

        public boolean hasValue(String value) {
            return value != null && errorCd.equals(value);
        }

        @Override
        public String toString() {
            return this.errorCd;
        }
    }

    public enum SummaryStatusCode {
        BALANCE("B"), OUT_OF_BALANCE("O"), COMPLETED("C"), PARTIALLY_COMPLETE("P");

        private final String summaryStatusCd;

        private SummaryStatusCode(String summaryStatusCd) {
            this.summaryStatusCd = summaryStatusCd;
        }

        public boolean hasValue(String value) {
            return value != null && summaryStatusCd.equals(value);
        }

        @Override
        public String toString() {
            return this.summaryStatusCd;
        }
    }

    public enum CycleTypeCode {
        GENERAL_LEDGER_CLOSEOUT('G'), CLAIMS_LOSSES_CLOSEOUT('L'), PREMIUM_CLOSEOUT('P');

        private final char cycleTypeCd;

        private CycleTypeCode(char cycleTypeCd) {
            this.cycleTypeCd = cycleTypeCd;
        }

        public char getCycleTypeCode() {
            return cycleTypeCd;
        }
    }

    public enum CycleInitiateIndicator {
        INITIATE_CLOSEOUT('Y'), NOT_CURRENT_CLOSEOUT('N');

        private final char cycleInitiateInd;

        private CycleInitiateIndicator(char cycleInitiateInd) {
            this.cycleInitiateInd = cycleInitiateInd;
        }

        public char getCycleInitiateIndicator() {
            return cycleInitiateInd;
        }
    }

    public enum ApplicationDataCode {
        PB360_APPLICATION_DATA("E"),
        NON_PB360_APPLICATION_DATA("N"),
        PROGRAM_DATA("D"),
        NON_PB360_SUB_PROGRAMS("NONEXPGMNM"),
        USER_EXIT("USEREXIT");

        private final String applicationDataCd;

        private ApplicationDataCode(String applicationDataCd) {
            this.applicationDataCd = applicationDataCd;
        }

        public boolean hasValue(String value) {
            return value != null && applicationDataCd.equals(value);
        }

        @Override
        public String toString() {
            return this.applicationDataCd;
        }
    }

    public enum ApplicationName {

        DWSODAR("DWSODAR"),
        BCWSACCT("BCWSACCT"),
        BACPTDIR("BACPTDIR"),
        BCMSDISB("BCMSDISB"),
        BCWSTTY("BCWSTTY"),
        BACPCCP("BACPCCP"),
        BCMOCX("BCMOCX"),
        BCWSUNID("BCWSUNID"),
        BCMOCA("BCMOCA"),
        BACPDUC("BACPDUC"),
        BCMOAD("BCMOAD"),
        BACPGDIR("BACPGDIR");

        private final String appName;

        private ApplicationName(String appName) {
            this.appName = appName;
        }

        public boolean hasValue(String value) {
            return appName.equals(value);
        }

        @Override
        public String toString() {
            return this.appName;
        }

    }

    public enum ApplicationProgramIdentifier {

        BACPTDSB("BACPTDSB"),
        BACPTDIR("BACPTDIR"),
        BACPTRW("BACPTRW"),
        BACPTMAN("BACPTMAN"),
        BCMORC("BCMORC"),
        BACPTRV("BACPTRV"),
        BACPCCP("BACPCCP"),
        BACPTCCP("BACPTCCP"),
        BACPUDS("BACPUDS"),
        BCMOIW("BCMOIW"),
        BCMOWTF("BCMOWTF"),
        BCMORWO("BCMORWO"),
        BACPURR("BACPURR"),
        BACPUCP("BACPUCP"),
        BACPUMS("BACPUMS"),
        BACPDUC("BACPDUC"),
        BCMOAD("BCMOAD"),
        BCMODUD("BCMODUD");

        private final String applicationProgramId;

        private ApplicationProgramIdentifier(String applicationProgramId) {
            this.applicationProgramId = applicationProgramId;
        }

        public boolean hasValue(String value) {
            return applicationProgramId.equals(value);
        }

        @Override
        public String toString() {
            return this.applicationProgramId;
        }

    }

    public enum FinancialIndicator {
        ENABLE('Y'), DISABLE('N');

        private final Character financialInd;

        private FinancialIndicator(Character financialInd) {
            this.financialInd = financialInd;
        }

        public Character getValue() {
            return financialInd;
        }

    }
}
