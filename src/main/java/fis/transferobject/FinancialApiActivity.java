package fis.transferobject;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.INVALID_DOUBLE_VAL;
import static core.utils.CommonConstants.INVALID_INT_VAL;
import static core.utils.CommonConstants.INVALID_SHORT_VAL;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.time.ZonedDateTime;

public class FinancialApiActivity {
    private char functionCode;
    private String workNetMessageId = BLANK_STRING;
    private String folderId = BLANK_STRING;
    private String referenceId = BLANK_STRING;
    private String applicationName = BLANK_STRING;
    private String userId = BLANK_STRING;
    private String errorCode = BLANK_STRING;
    private int returnSqlCode = INVALID_INT_VAL;
    private String returnMessage = BLANK_STRING;
    private String transactionObjectCode = BLANK_STRING;
    private String transactionActionCode = BLANK_STRING;
    private String applicationProductIdentifier = BLANK_STRING;
    private String masterCompanyNbr = BLANK_STRING;
    private String companyLocationNbr = BLANK_STRING;
    private char financialIndicator;
    private String effectiveDate = BLANK_STRING;
    private String operatorId = BLANK_STRING;
    private String appProgramId = BLANK_STRING;
    private String countyCode = BLANK_STRING;
    private String stateCode = BLANK_STRING;
    private String countryCode = BLANK_STRING;
    private String lineOfBusCode = BLANK_STRING;
    private String policyId = SEPARATOR_BLANK;
    private String agentId = BLANK_STRING;
    private String businessCaseId = BLANK_STRING;
    private String marketSectorCode = BLANK_STRING;
    private String clientId = BLANK_STRING;
    private String customerInfo1 = BLANK_STRING;
    private String customerInfo2 = BLANK_STRING;
    private String customerInfo3 = BLANK_STRING;
    private String customerInfo4 = BLANK_STRING;
    private String customerInfo5 = BLANK_STRING;
    private double activityAmount = INVALID_DOUBLE_VAL;
    private String currencyCode = BLANK_STRING;
    private String originalEffectiveDate = BLANK_STRING;
    private ZonedDateTime originalTimeStamp;
    private String bilBankCode = BLANK_STRING;
    private String referenceDate = BLANK_STRING;
    private String bilPostDate = BLANK_STRING;
    private String bilAccountId = BLANK_STRING;
    private String bilReasonCode = BLANK_STRING;
    private String bilReceiptTypeCode = BLANK_STRING;
    private String bilTypeCode = BLANK_STRING;
    private String bilClassCode = BLANK_STRING;
    private String payableItemCode = BLANK_STRING;
    private String bilDatabaseKey = BLANK_STRING;
    private String agentTtyId = BLANK_STRING;
    private String bilSourceCode = BLANK_STRING;
    private double activityNetAmount = INVALID_DOUBLE_VAL;
    private double instalCommissionPercentage = INVALID_DOUBLE_VAL;
    private String depositeBankCode = BLANK_STRING;
    private String disbursementId = BLANK_STRING;
    private String dataApplicationCode = BLANK_STRING;
    private char statusCode = BLANK_CHAR;
    private ZonedDateTime summaryEffectiveDate;
    private int checkNumber = INVALID_INT_VAL;
    private short bankNumber = INVALID_SHORT_VAL;
    private String bankAccountNumber = BLANK_STRING;
    private String processingLocationCode = BLANK_STRING;
    private String disbursementTypeCode = BLANK_STRING;
    private String districtOfficeName = BLANK_STRING;
    private String mediumCode = BLANK_STRING;
    private String payee1Id = BLANK_STRING;
    private String payee2Id = BLANK_STRING;
    private String payee3Id = BLANK_STRING;
    private String mailRecipientId = BLANK_STRING;
    private short addressSequenceNumber = INVALID_SHORT_VAL;
    private char dwsStatusCode;
    private int nextCheckNumber = INVALID_INT_VAL;
    private int currentBeginCheckNumber = INVALID_INT_VAL;
    private int currentEndCheckNumber = INVALID_INT_VAL;
    private String currentRangeDate = BLANK_STRING;
    private int nextBeginCheckNumber = INVALID_INT_VAL;
    private int nextEndCheckNumber = INVALID_INT_VAL;
    private String nextRangeDate = BLANK_STRING;
    private String checkUsageCode = BLANK_STRING;
    private int preNextCheck = INVALID_INT_VAL;
    private String disbursementDate = BLANK_STRING;
    private String dwsProductId = BLANK_STRING;

    public FinancialApiActivity() {
        super();
    }

    public FinancialApiActivity(char functionCode, String workNetMessageId, String folderId, String referenceId,
            String applicationName, String userId, String errorCode, int returnSqlCode, String returnMessage,
            String transactionObjectCode, String transactionActionCode, String applicationProductIdentifier,
            String masterCompanyNbr, String companyLocationNbr, char financialIndicator, String effectiveDate,
            String operatorId, String appProgramId, String countyCode, String stateCode, String countryCode,
            String lineOfBusCode, String policyId, String agentId, String businessCaseId, String marketSectorCode,
            String clientId, String customerInfo1, String customerInfo2, String customerInfo3, String customerInfo4,
            String customerInfo5, double activityAmount, String currencyCode, String originalEffectiveDate,
            ZonedDateTime originalTimeStamp, String bilBankCode, String referenceDate, String bilPostDate,
            String bilAccountId, String bilReasonCode, String bilReceiptTypeCode, String bilTypeCode,
            String bilClassCode, String payableItemCode, String bilDatabaseKey, String agentTtyId, String bilSourceCode,
            double activityNetAmount, double instalCommissionPercentage, String depositeBankCode, String disbursementId,
            String dataApplicationCode, char statusCode, ZonedDateTime summaryEffectiveDate, int checkNumber,
            short bankNumber, String bankAccountNumber, String processingLocationCode, String disbursementTypeCode,
            String districtOfficeName, String mediumCode, String payee1Id, String payee2Id, String payee3Id,
            String mailRecipientId, short addressSequenceNumber, char dwsStatusCode, int nextCheckNumber,
            int currentBeginCheckNumber, int currentEndCheckNumber, String currentRangeDate, int nextBeginCheckNumber,
            int nextEndCheckNumber, String nextRangeDate, String checkUsageCode, int preNextCheck,
            String disbursementDate, String dwsProductId) {
        super();
        this.functionCode = functionCode;
        this.workNetMessageId = workNetMessageId;
        this.folderId = folderId;
        this.referenceId = referenceId;
        this.applicationName = applicationName;
        this.userId = userId;
        this.errorCode = errorCode;
        this.returnSqlCode = returnSqlCode;
        this.returnMessage = returnMessage;
        this.transactionObjectCode = transactionObjectCode;
        this.transactionActionCode = transactionActionCode;
        this.applicationProductIdentifier = applicationProductIdentifier;
        this.masterCompanyNbr = masterCompanyNbr;
        this.companyLocationNbr = companyLocationNbr;
        this.financialIndicator = financialIndicator;
        this.effectiveDate = effectiveDate;
        this.operatorId = operatorId;
        this.appProgramId = appProgramId;
        this.countyCode = countyCode;
        this.stateCode = stateCode;
        this.countryCode = countryCode;
        this.lineOfBusCode = lineOfBusCode;
        this.policyId = policyId;
        this.agentId = agentId;
        this.businessCaseId = businessCaseId;
        this.marketSectorCode = marketSectorCode;
        this.clientId = clientId;
        this.customerInfo1 = customerInfo1;
        this.customerInfo2 = customerInfo2;
        this.customerInfo3 = customerInfo3;
        this.customerInfo4 = customerInfo4;
        this.customerInfo5 = customerInfo5;
        this.activityAmount = activityAmount;
        this.currencyCode = currencyCode;
        this.originalEffectiveDate = originalEffectiveDate;
        this.originalTimeStamp = originalTimeStamp;
        this.bilBankCode = bilBankCode;
        this.referenceDate = referenceDate;
        this.bilPostDate = bilPostDate;
        this.bilAccountId = bilAccountId;
        this.bilReasonCode = bilReasonCode;
        this.bilReceiptTypeCode = bilReceiptTypeCode;
        this.bilTypeCode = bilTypeCode;
        this.bilClassCode = bilClassCode;
        this.payableItemCode = payableItemCode;
        this.bilDatabaseKey = bilDatabaseKey;
        this.agentTtyId = agentTtyId;
        this.bilSourceCode = bilSourceCode;
        this.activityNetAmount = activityNetAmount;
        this.instalCommissionPercentage = instalCommissionPercentage;
        this.depositeBankCode = depositeBankCode;
        this.disbursementId = disbursementId;
        this.dataApplicationCode = dataApplicationCode;
        this.statusCode = statusCode;
        this.summaryEffectiveDate = summaryEffectiveDate;
        this.checkNumber = checkNumber;
        this.bankNumber = bankNumber;
        this.bankAccountNumber = bankAccountNumber;
        this.processingLocationCode = processingLocationCode;
        this.disbursementTypeCode = disbursementTypeCode;
        this.districtOfficeName = districtOfficeName;
        this.mediumCode = mediumCode;
        this.payee1Id = payee1Id;
        this.payee2Id = payee2Id;
        this.payee3Id = payee3Id;
        this.mailRecipientId = mailRecipientId;
        this.addressSequenceNumber = addressSequenceNumber;
        this.dwsStatusCode = dwsStatusCode;
        this.nextCheckNumber = nextCheckNumber;
        this.currentBeginCheckNumber = currentBeginCheckNumber;
        this.currentEndCheckNumber = currentEndCheckNumber;
        this.currentRangeDate = currentRangeDate;
        this.nextBeginCheckNumber = nextBeginCheckNumber;
        this.nextEndCheckNumber = nextEndCheckNumber;
        this.nextRangeDate = nextRangeDate;
        this.checkUsageCode = checkUsageCode;
        this.preNextCheck = preNextCheck;
        this.disbursementDate = disbursementDate;
        this.dwsProductId = dwsProductId;
    }

    public char getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(char functionCode) {
        this.functionCode = functionCode;
    }

    public String getWorkNetMessageId() {
        return workNetMessageId;
    }

    public void setWorkNetMessageId(String workNetMessageId) {
        this.workNetMessageId = workNetMessageId;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public int getReturnSqlCode() {
        return returnSqlCode;
    }

    public void setReturnSqlCode(int returnSqlCode) {
        this.returnSqlCode = returnSqlCode;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getTransactionObjectCode() {
        return transactionObjectCode;
    }

    public void setTransactionObjectCode(String transactionObjectCode) {
        this.transactionObjectCode = transactionObjectCode;
    }

    public String getTransactionActionCode() {
        return transactionActionCode;
    }

    public void setTransactionActionCode(String transactionActionCode) {
        this.transactionActionCode = transactionActionCode;
    }

    public String getApplicationProductIdentifier() {
        return applicationProductIdentifier;
    }

    public void setApplicationProductIdentifier(String applicationProductIdentifier) {
        this.applicationProductIdentifier = applicationProductIdentifier;
    }

    public String getMasterCompanyNbr() {
        return masterCompanyNbr;
    }

    public void setMasterCompanyNbr(String masterCompanyNbr) {
        this.masterCompanyNbr = masterCompanyNbr;
    }

    public String getCompanyLocationNbr() {
        return companyLocationNbr;
    }

    public void setCompanyLocationNbr(String companyLocationNbr) {
        this.companyLocationNbr = companyLocationNbr;
    }

    public char getFinancialIndicator() {
        return financialIndicator;
    }

    public void setFinancialIndicator(char financialIndicator) {
        this.financialIndicator = financialIndicator;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getAppProgramId() {
        return appProgramId;
    }

    public void setAppProgramId(String appProgramId) {
        this.appProgramId = appProgramId;
    }

    public String getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(String countyCode) {
        this.countyCode = countyCode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLineOfBusCode() {
        return lineOfBusCode;
    }

    public void setLineOfBusCode(String lineOfBusCode) {
        this.lineOfBusCode = lineOfBusCode;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getBusinessCaseId() {
        return businessCaseId;
    }

    public void setBusinessCaseId(String businessCaseId) {
        this.businessCaseId = businessCaseId;
    }

    public String getMarketSectorCode() {
        return marketSectorCode;
    }

    public void setMarketSectorCode(String marketSectorCode) {
        this.marketSectorCode = marketSectorCode;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getCustomerInfo1() {
        return customerInfo1;
    }

    public void setCustomerInfo1(String customerInfo1) {
        this.customerInfo1 = customerInfo1;
    }

    public String getCustomerInfo2() {
        return customerInfo2;
    }

    public void setCustomerInfo2(String customerInfo2) {
        this.customerInfo2 = customerInfo2;
    }

    public String getCustomerInfo3() {
        return customerInfo3;
    }

    public void setCustomerInfo3(String customerInfo3) {
        this.customerInfo3 = customerInfo3;
    }

    public String getCustomerInfo4() {
        return customerInfo4;
    }

    public void setCustomerInfo4(String customerInfo4) {
        this.customerInfo4 = customerInfo4;
    }

    public String getCustomerInfo5() {
        return customerInfo5;
    }

    public void setCustomerInfo5(String customerInfo5) {
        this.customerInfo5 = customerInfo5;
    }

    public double getActivityAmount() {
        return activityAmount;
    }

    public void setActivityAmount(double activityAmount) {
        this.activityAmount = activityAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getOriginalEffectiveDate() {
        return originalEffectiveDate;
    }

    public void setOriginalEffectiveDate(String originalEffectiveDate) {
        this.originalEffectiveDate = originalEffectiveDate;
    }

    public ZonedDateTime getOriginalTimeStamp() {
        return originalTimeStamp;
    }

    public void setOriginalTimeStamp(ZonedDateTime originalTimeStamp) {
        this.originalTimeStamp = originalTimeStamp;
    }

    public String getBilBankCode() {
        return bilBankCode;
    }

    public void setBilBankCode(String bilBankCode) {
        this.bilBankCode = bilBankCode;
    }

    public String getReferenceDate() {
        return referenceDate;
    }

    public void setReferenceDate(String referenceDate) {
        this.referenceDate = referenceDate;
    }

    public String getBilPostDate() {
        return bilPostDate;
    }

    public void setBilPostDate(String bilPostDate) {
        this.bilPostDate = bilPostDate;
    }

    public String getBilAccountId() {
        return bilAccountId;
    }

    public void setBilAccountId(String bilAccountId) {
        this.bilAccountId = bilAccountId;
    }

    public String getBilReasonCode() {
        return bilReasonCode;
    }

    public void setBilReasonCode(String bilReasonCode) {
        this.bilReasonCode = bilReasonCode;
    }

    public String getBilReceiptTypeCode() {
        return bilReceiptTypeCode;
    }

    public void setBilReceiptTypeCode(String bilReceiptTypeCode) {
        this.bilReceiptTypeCode = bilReceiptTypeCode;
    }

    public String getBilTypeCode() {
        return bilTypeCode;
    }

    public void setBilTypeCode(String bilTypeCode) {
        this.bilTypeCode = bilTypeCode;
    }

    public String getBilClassCode() {
        return bilClassCode;
    }

    public void setBilClassCode(String bilClassCode) {
        this.bilClassCode = bilClassCode;
    }

    public String getPayableItemCode() {
        return payableItemCode;
    }

    public void setPayableItemCode(String payableItemCode) {
        this.payableItemCode = payableItemCode;
    }

    public String getBilDatabaseKey() {
        return bilDatabaseKey;
    }

    public void setBilDatabaseKey(String bilDatabaseKey) {
        this.bilDatabaseKey = bilDatabaseKey;
    }

    public String getAgentTtyId() {
        return agentTtyId;
    }

    public void setAgentTtyId(String agentTtyId) {
        this.agentTtyId = agentTtyId;
    }

    public String getBilSourceCode() {
        return bilSourceCode;
    }

    public void setBilSourceCode(String bilSourceCode) {
        this.bilSourceCode = bilSourceCode;
    }

    public double getActivityNetAmount() {
        return activityNetAmount;
    }

    public void setActivityNetAmount(double activityNetAmount) {
        this.activityNetAmount = activityNetAmount;
    }

    public double getInstalCommissionPercentage() {
        return instalCommissionPercentage;
    }

    public void setInstalCommissionPercentage(double instalCommissionPercentage) {
        this.instalCommissionPercentage = instalCommissionPercentage;
    }

    public String getDepositeBankCode() {
        return depositeBankCode;
    }

    public void setDepositeBankCode(String depositeBankCode) {
        this.depositeBankCode = depositeBankCode;
    }

    public String getDisbursementId() {
        return disbursementId;
    }

    public void setDisbursementId(String disbursementId) {
        this.disbursementId = disbursementId;
    }

    public String getDataApplicationCode() {
        return dataApplicationCode;
    }

    public void setDataApplicationCode(String dataApplicationCode) {
        this.dataApplicationCode = dataApplicationCode;
    }

    public char getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(char statusCode) {
        this.statusCode = statusCode;
    }

    public ZonedDateTime getSummaryEffectiveDate() {
        return summaryEffectiveDate;
    }

    public void setSummaryEffectiveDate(ZonedDateTime summaryEffectiveDate) {
        this.summaryEffectiveDate = summaryEffectiveDate;
    }

    public int getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(int checkNumber) {
        this.checkNumber = checkNumber;
    }

    public String getProcessingLocationCode() {
        return processingLocationCode;
    }

    public void setProcessingLocationCode(String processingLocationCode) {
        this.processingLocationCode = processingLocationCode;
    }

    public String getDisbursementTypeCode() {
        return disbursementTypeCode;
    }

    public void setDisbursementTypeCode(String disbursementTypeCode) {
        this.disbursementTypeCode = disbursementTypeCode;
    }

    public String getDistrictOfficeName() {
        return districtOfficeName;
    }

    public void setDistrictOfficeName(String districtOfficeName) {
        this.districtOfficeName = districtOfficeName;
    }

    public String getMediumCode() {
        return mediumCode;
    }

    public void setMediumCode(String mediumCode) {
        this.mediumCode = mediumCode;
    }

    public String getPayee1Id() {
        return payee1Id;
    }

    public void setPayee1Id(String payee1Id) {
        this.payee1Id = payee1Id;
    }

    public String getPayee2Id() {
        return payee2Id;
    }

    public void setPayee2Id(String payee2Id) {
        this.payee2Id = payee2Id;
    }

    public String getPayee3Id() {
        return payee3Id;
    }

    public void setPayee3Id(String payee3Id) {
        this.payee3Id = payee3Id;
    }

    public String getMailRecipientId() {
        return mailRecipientId;
    }

    public void setMailRecipientId(String mailRecipientId) {
        this.mailRecipientId = mailRecipientId;
    }

    public short getAddressSequenceNumber() {
        return addressSequenceNumber;
    }

    public void setAddressSequenceNumber(short addressSequenceNumber) {
        this.addressSequenceNumber = addressSequenceNumber;
    }

    public char getDwsStatusCode() {
        return dwsStatusCode;
    }

    public void setDwsStatusCode(char dwsStatusCode) {
        this.dwsStatusCode = dwsStatusCode;
    }

    public int getNextCheckNumber() {
        return nextCheckNumber;
    }

    public void setNextCheckNumber(int nextCheckNumber) {
        this.nextCheckNumber = nextCheckNumber;
    }

    public int getCurrentBeginCheckNumber() {
        return currentBeginCheckNumber;
    }

    public void setCurrentBeginCheckNumber(int currentBeginCheckNumber) {
        this.currentBeginCheckNumber = currentBeginCheckNumber;
    }

    public int getCurrentEndCheckNumber() {
        return currentEndCheckNumber;
    }

    public void setCurrentEndCheckNumber(int currentEndCheckNumber) {
        this.currentEndCheckNumber = currentEndCheckNumber;
    }

    public String getCurrentRangeDate() {
        return currentRangeDate;
    }

    public void setCurrentRangeDate(String currentRangeDate) {
        this.currentRangeDate = currentRangeDate;
    }

    public int getNextBeginCheckNumber() {
        return nextBeginCheckNumber;
    }

    public void setNextBeginCheckNumber(int nextBeginCheckNumber) {
        this.nextBeginCheckNumber = nextBeginCheckNumber;
    }

    public int getNextEndCheckNumber() {
        return nextEndCheckNumber;
    }

    public void setNextEndCheckNumber(int nextEndCheckNumber) {
        this.nextEndCheckNumber = nextEndCheckNumber;
    }

    public String getNextRangeDate() {
        return nextRangeDate;
    }

    public void setNextRangeDate(String nextRangeDate) {
        this.nextRangeDate = nextRangeDate;
    }

    public String getCheckUsageCode() {
        return checkUsageCode;
    }

    public void setCheckUsageCode(String checkUsageCode) {
        this.checkUsageCode = checkUsageCode;
    }

    public int getPreNextCheck() {
        return preNextCheck;
    }

    public void setPreNextCheck(int preNextCheck) {
        this.preNextCheck = preNextCheck;
    }

    public String getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(String disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public short getBankNumber() {
        return bankNumber;
    }

    public void setBankNumber(short bankNumber) {
        this.bankNumber = bankNumber;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getDwsProductId() {
        return dwsProductId;
    }

    public void setDwsProductId(String dwsProductId) {
        this.dwsProductId = dwsProductId;
    }

}