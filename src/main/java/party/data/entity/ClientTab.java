package party.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import party.data.entity.id.ClientTabId;

@Entity
@Table(name = "CLIENT_TAB")
public final class ClientTab {

    @EmbeddedId
    private ClientTabId clientTabId;

    @Column(name = "CICL_PRI_SUB_CD", length = 4, nullable = false)
    private String ciclPriSubCd = SEPARATOR_BLANK;

    @Column(name = "CICL_FST_NM", length = 64, nullable = false)
    private String ciclFstNm = SEPARATOR_BLANK;

    @Column(name = "CICL_LST_NM", length = 256, nullable = false)
    private String ciclLstNm = SEPARATOR_BLANK;

    @Column(name = "CICL_MDL_NM", length = 64, nullable = false)
    private String ciclMdlNm = SEPARATOR_BLANK;

    @Column(name = "NM_PFX", length = 4, nullable = false)
    private String nmPfx = SEPARATOR_BLANK;

    @Column(name = "NM_SFX", length = 4, nullable = false)
    private String nmSfx = SEPARATOR_BLANK;

    @Column(name = "PRIMARY_PRO_DSN_CD", length = 4, nullable = true)
    private String primaryProDsnCd = SEPARATOR_BLANK;

    @Column(name = "LEG_ENT_CD", length = 2, nullable = false)
    private String legEntCd = SEPARATOR_BLANK;

    @Column(name = "CICL_SDX_CD", length = 8, nullable = false)
    private String ciclSdxCd = SEPARATOR_BLANK;

    @Column(name = "CICL_OGN_INCEPT_DT", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ciclOgnInceptDt;

    @Column(name = "CICL_ADD_NM_IND", nullable = true)
    private Character ciclAddNmInd = BLANK_CHAR;

    @Column(name = "CICL_DOB_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ciclDobDt;

    @Column(name = "CICL_BIR_ST_CD", length = 3, nullable = false)
    private String ciclBirStCd = SEPARATOR_BLANK;

    @Column(name = "GENDER_CD", nullable = false)
    private char genderCd = BLANK_CHAR;

    @Column(name = "PRI_LGG_CD", length = 3, nullable = false)
    private String priLggCd = SEPARATOR_BLANK;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "STATUS_CD", nullable = false)
    private char statusCd = BLANK_CHAR;

    @Column(name = "TERMINAL_ID", length = 8, nullable = false)
    private String terminalId = SEPARATOR_BLANK;

    @Column(name = "CICL_EXP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ciclExpDt;

    @Column(name = "CICL_EFF_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime ciclEffAcyTs;

    @Column(name = "CICL_EXP_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime ciclExpAcyTs;

    @Column(name = "STATUTORY_TLE_CD", length = 3, nullable = false)
    private String statutoryTleCd = SEPARATOR_BLANK;

    @Column(name = "CICL_LNG_NM", length = 512, nullable = true)
    private String ciclLngNm = SEPARATOR_BLANK;

    public ClientTabId getClientTabId() {
        return clientTabId;
    }

    public void setClientTabId(ClientTabId clientTabId) {
        this.clientTabId = clientTabId;
    }

    public String getCiclPriSubCd() {
        return ciclPriSubCd;
    }

    public void setCiclPriSubCd(String ciclPriSubCd) {
        this.ciclPriSubCd = ciclPriSubCd;
    }

    public String getCiclFstNm() {
        return ciclFstNm;
    }

    public void setCiclFstNm(String ciclFstNm) {
        this.ciclFstNm = ciclFstNm;
    }

    public String getCiclLstNm() {
        return ciclLstNm;
    }

    public void setCiclLstNm(String ciclLstNm) {
        this.ciclLstNm = ciclLstNm;
    }

    public String getCiclMdlNm() {
        return ciclMdlNm;
    }

    public void setCiclMdlNm(String ciclMdlNm) {
        this.ciclMdlNm = ciclMdlNm;
    }

    public String getNmPfx() {
        return nmPfx;
    }

    public void setNmPfx(String nmPfx) {
        this.nmPfx = nmPfx;
    }

    public String getNmSfx() {
        return nmSfx;
    }

    public void setNmSfx(String nmSfx) {
        this.nmSfx = nmSfx;
    }

    public String getPrimaryProDsnCd() {
        return primaryProDsnCd;
    }

    public void setPrimaryProDsnCd(String primaryProDsnCd) {
        this.primaryProDsnCd = primaryProDsnCd;
    }

    public String getLegEntCd() {
        return legEntCd;
    }

    public void setLegEntCd(String legEntCd) {
        this.legEntCd = legEntCd;
    }

    public String getCiclSdxCd() {
        return ciclSdxCd;
    }

    public void setCiclSdxCd(String ciclSdxCd) {
        this.ciclSdxCd = ciclSdxCd;
    }

    public ZonedDateTime getCiclOgnInceptDt() {
        return ciclOgnInceptDt;
    }

    public void setCiclOgnInceptDt(ZonedDateTime ciclOgnInceptDt) {
        this.ciclOgnInceptDt = ciclOgnInceptDt;
    }

    public Character getCiclAddNmInd() {
        return ciclAddNmInd;
    }

    public void setCiclAddNmInd(Character ciclAddNmInd) {
        this.ciclAddNmInd = ciclAddNmInd;
    }

    public ZonedDateTime getCiclDobDt() {
        return ciclDobDt;
    }

    public void setCiclDobDt(ZonedDateTime ciclDobDt) {
        this.ciclDobDt = ciclDobDt;
    }

    public String getCiclBirStCd() {
        return ciclBirStCd;
    }

    public void setCiclBirStCd(String ciclBirStCd) {
        this.ciclBirStCd = ciclBirStCd;
    }

    public char getGenderCd() {
        return genderCd;
    }

    public void setGenderCd(char genderCd) {
        this.genderCd = genderCd;
    }

    public String getPriLggCd() {
        return priLggCd;
    }

    public void setPriLggCd(String priLggCd) {
        this.priLggCd = priLggCd;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public char getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(char statusCd) {
        this.statusCd = statusCd;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public ZonedDateTime getCiclExpDt() {
        return ciclExpDt;
    }

    public void setCiclExpDt(ZonedDateTime ciclExpDt) {
        this.ciclExpDt = ciclExpDt;
    }

    public ZonedDateTime getCiclEffAcyTs() {
        return ciclEffAcyTs;
    }

    public void setCiclEffAcyTs(ZonedDateTime clientEffectiveTs) {
        this.ciclEffAcyTs = clientEffectiveTs;
    }

    public ZonedDateTime getCiclExpAcyTs() {
        return ciclExpAcyTs;
    }

    public void setCiclExpAcyTs(ZonedDateTime ciclExpAcyTs) {
        this.ciclExpAcyTs = ciclExpAcyTs;
    }

    public String getStatutoryTleCd() {
        return statutoryTleCd;
    }

    public void setStatutoryTleCd(String statutoryTleCd) {
        this.statutoryTleCd = statutoryTleCd;
    }

    public String getCiclLngNm() {
        return ciclLngNm;
    }

    public void setCiclLngNm(String ciclLngNm) {
        this.ciclLngNm = ciclLngNm;
    }

}
