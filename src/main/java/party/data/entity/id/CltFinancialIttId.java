package party.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class CltFinancialIttId implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "CLIENT_ID", length = 20, nullable = false)
    private String clientId = SEPARATOR_BLANK;

    @Column(name = "HISTORY_VLD_NBR", length = 5, nullable = false)
    private short historyVldNbr = SHORT_ZERO;

    @Column(name = "TCH_OBJECT_KEY", length = 20, nullable = false)
    private String tchObjectKey = SEPARATOR_BLANK;

    @Column(name = "EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime effectiveDt;

    public CltFinancialIttId(String clientId, short historyVldNbr, String tchObjectKey, ZonedDateTime effectiveDt) {
        super();
        this.clientId = clientId;
        this.historyVldNbr = historyVldNbr;
        this.tchObjectKey = tchObjectKey;
        this.effectiveDt = effectiveDt;
    }

    public CltFinancialIttId() {
        super();
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public short getHistoryVldNbr() {
        return historyVldNbr;
    }

    public void setHistoryVldNbr(short historyVldNbr) {
        this.historyVldNbr = historyVldNbr;
    }

    public String getTchObjectKey() {
        return tchObjectKey;
    }

    public void setTchObjectKey(String tchObjectKey) {
        this.tchObjectKey = tchObjectKey;
    }

    public ZonedDateTime getEffectiveDt() {
        return effectiveDt;
    }

    public void setEffectiveDt(ZonedDateTime effectiveDt) {
        this.effectiveDt = effectiveDt;
    }

}
