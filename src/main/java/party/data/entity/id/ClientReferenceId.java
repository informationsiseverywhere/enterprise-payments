package party.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class ClientReferenceId implements Serializable {

    private static final long serialVersionUID = 7729017072360964014L;

    @Column(name = "CLIENT_ID", length = 20, nullable = false)
    private String clientId = SEPARATOR_BLANK;

    @Column(name = "CIRF_REF_SEQ_NBR", length = 5, nullable = false)
    private short cirfRefSeqNbr = SHORT_ZERO;

    @Column(name = "HISTORY_VLD_NBR", length = 5, nullable = false)
    private short historyVldNbr = SHORT_ZERO;

    @Column(name = "CIRF_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime cirfEffDt;

    public ClientReferenceId(String clientId, short cirfRefSeqNbr, short historyVldNbr, ZonedDateTime cirfEffDt) {
        super();
        this.clientId = clientId;
        this.cirfRefSeqNbr = cirfRefSeqNbr;
        this.historyVldNbr = historyVldNbr;
        this.cirfEffDt = cirfEffDt;
    }

    public ClientReferenceId() {
        super();
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public short getCirfRefSeqNbr() {
        return cirfRefSeqNbr;
    }

    public void setCirfRefSeqNbr(short cirfRefSeqNbr) {
        this.cirfRefSeqNbr = cirfRefSeqNbr;
    }

    public short getHistoryVldNbr() {
        return historyVldNbr;
    }

    public void setHistoryVldNbr(short historyVldNbr) {
        this.historyVldNbr = historyVldNbr;
    }

    public ZonedDateTime getCirfEffDt() {
        return cirfEffDt;
    }

    public void setCirfEffDt(ZonedDateTime cirfEffDt) {
        this.cirfEffDt = cirfEffDt;
    }

}
