package party.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class CltObjRelationId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "TCH_OBJECT_KEY", length = 20, nullable = false)
    private String tchObjectKey = SEPARATOR_BLANK;

    @Column(name = "HISTORY_VLD_NBR", length = 5, nullable = false)
    private short historyVldNbr = SHORT_ZERO;

    @Column(name = "CIOR_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ciorEffDt;

    @Column(name = "OBJ_SYS_ID", length = 4, nullable = false)
    private String objSysId = SEPARATOR_BLANK;

    @Column(name = "CIOR_OBJ_SEQ_NBR", length = 5, nullable = false)
    private short ciorObjSeqNbr = SHORT_ZERO;

    public CltObjRelationId(String tchObjectKey, short historyVldNbr, ZonedDateTime ciorEffDt, String objSysId,
            short ciorObjSeqNbr) {
        super();
        this.tchObjectKey = tchObjectKey;
        this.historyVldNbr = historyVldNbr;
        this.ciorEffDt = ciorEffDt;
        this.objSysId = objSysId;
        this.ciorObjSeqNbr = ciorObjSeqNbr;
    }

    public CltObjRelationId() {
        super();
    }

    public String getTchObjectKey() {
        return tchObjectKey;
    }

    public void setTchObjectKey(String tchObjectKey) {
        this.tchObjectKey = tchObjectKey;
    }

    public short getHistoryVldNbr() {
        return historyVldNbr;
    }

    public void setHistoryVldNbr(short historyVldNbr) {
        this.historyVldNbr = historyVldNbr;
    }

    public ZonedDateTime getCiorEffDt() {
        return ciorEffDt;
    }

    public void setCiorEffDt(ZonedDateTime ciorEffDt) {
        this.ciorEffDt = ciorEffDt;
    }

    public String getObjSysId() {
        return objSysId;
    }

    public void setObjSysId(String objSysId) {
        this.objSysId = objSysId;
    }

    public short getCiorObjSeqNbr() {
        return ciorObjSeqNbr;
    }

    public void setCiorObjSeqNbr(short ciorObjSeqNbr) {
        this.ciorObjSeqNbr = ciorObjSeqNbr;
    }

}
