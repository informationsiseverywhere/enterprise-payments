package party.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class CltBankAccountId implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "CLIENT_ID", length = 20, nullable = false)
    private String clientId = SEPARATOR_BLANK;

    @Column(name = "HISTORY_VLD_NBR", length = 5, nullable = false)
    private short historyVldNbr = SHORT_ZERO;

    @Column(name = "CIBA_ACT_SEQ_NBR", length = 5, nullable = false)
    private short cibaActSeqNbr = SHORT_ZERO;

    @Column(name = "EFFECTIVE_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime effectiveDt;

    public CltBankAccountId(String clientId, short historyVldNbr, short cibaActSeqNbr, ZonedDateTime effectiveDt) {
        super();
        this.clientId = clientId;
        this.historyVldNbr = historyVldNbr;
        this.cibaActSeqNbr = cibaActSeqNbr;
        this.effectiveDt = effectiveDt;
    }

    public CltBankAccountId() {
        super();
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public short getHistoryVldNbr() {
        return historyVldNbr;
    }

    public void setHistoryVldNbr(short historyVldNbr) {
        this.historyVldNbr = historyVldNbr;
    }

    public short getCibaActSeqNbr() {
        return cibaActSeqNbr;
    }

    public void setCibaActSeqNbr(short cibaActSeqNbr) {
        this.cibaActSeqNbr = cibaActSeqNbr;
    }

    public ZonedDateTime getEffectiveDt() {
        return effectiveDt;
    }

    public void setEffectiveDt(ZonedDateTime effectiveDt) {
        this.effectiveDt = effectiveDt;
    }

}
