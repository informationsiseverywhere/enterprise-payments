package party.data.entity.id;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class ClientTabId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "CLIENT_ID", length = 20, nullable = false)
    private String clientId = SEPARATOR_BLANK;

    @Column(name = "HISTORY_VLD_NBR", length = 5, nullable = false)
    private short historyVldNbr = SHORT_ZERO;

    @Column(name = "CICL_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ciclEffDt;

    public ClientTabId(String clientId, short historyVldNbr, ZonedDateTime ciclEffDt) {
        super();
        this.clientId = clientId;
        this.historyVldNbr = historyVldNbr;
        this.ciclEffDt = ciclEffDt;
    }

    public ClientTabId() {
        super();
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public short getHistoryVldNbr() {
        return historyVldNbr;
    }

    public void setHistoryVldNbr(short historyVldNbr) {
        this.historyVldNbr = historyVldNbr;
    }

    public ZonedDateTime getCiclEffDt() {
        return ciclEffDt;
    }

    public void setCiclEffDt(ZonedDateTime ciclEffDt) {
        this.ciclEffDt = ciclEffDt;
    }

}
