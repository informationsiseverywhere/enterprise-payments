package party.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import party.data.entity.id.CltFinancialIttId;

@Entity
@Table(name = "CLT_FINANCIAL_ITT")
public class CltFinancialItt implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private CltFinancialIttId cltFinancialIttId;

    @Column(name = "CIFI_FNI_TYPE_CD", length = 3, nullable = false)
    private String cifiFniTypeCd = SEPARATOR_BLANK;

    @Column(name = "CIFI_FNI_FMT_CD", length = 3, nullable = false)
    private String cifiFniFmtCd = SEPARATOR_BLANK;

    @Column(name = "CIFI_PRF_RTE_NBR", length = 10, nullable = false)
    private String cifiPrfRteNbr = SEPARATOR_BLANK;

    @Column(name = "ADR_SEQ_NBR", length = 5, nullable = false)
    private short adrSeqNbr = SHORT_ZERO;

    @Column(name = "PHN_SEQ_NBR", length = 5, nullable = false)
    private short phnSeqNbr = SHORT_ZERO;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "EFFECTIVE_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime effectiveAcyTs;

    @Column(name = "EXPIRATION_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime expirationAcyTs;

    @Column(name = "EXPIRATION_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime expirationDt;

    public CltFinancialIttId getCltFinancialIttId() {
        return cltFinancialIttId;
    }

    public void setCltFinancialIttId(CltFinancialIttId cltFinancialIttId) {
        this.cltFinancialIttId = cltFinancialIttId;
    }

    public String getCifiFniTypeCd() {
        return cifiFniTypeCd;
    }

    public void setCifiFniTypeCd(String cifiFniTypeCd) {
        this.cifiFniTypeCd = cifiFniTypeCd;
    }

    public String getCifiFniFmtCd() {
        return cifiFniFmtCd;
    }

    public void setCifiFniFmtCd(String cifiFniFmtCd) {
        this.cifiFniFmtCd = cifiFniFmtCd;
    }

    public String getCifiPrfRteNbr() {
        return cifiPrfRteNbr;
    }

    public void setCifiPrfRteNbr(String cifiPrfRteNbr) {
        this.cifiPrfRteNbr = cifiPrfRteNbr;
    }

    public short getAdrSeqNbr() {
        return adrSeqNbr;
    }

    public void setAdrSeqNbr(short adrSeqNbr) {
        this.adrSeqNbr = adrSeqNbr;
    }

    public short getPhnSeqNbr() {
        return phnSeqNbr;
    }

    public void setPhnSeqNbr(short phnSeqNbr) {
        this.phnSeqNbr = phnSeqNbr;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ZonedDateTime getEffectiveAcyTs() {
        return effectiveAcyTs;
    }

    public void setEffectiveAcyTs(ZonedDateTime effectiveAcyTs) {
        this.effectiveAcyTs = effectiveAcyTs;
    }

    public ZonedDateTime getExpirationAcyTs() {
        return expirationAcyTs;
    }

    public void setExpirationAcyTs(ZonedDateTime expirationAcyTs) {
        this.expirationAcyTs = expirationAcyTs;
    }

    public ZonedDateTime getExpirationDt() {
        return expirationDt;
    }

    public void setExpirationDt(ZonedDateTime expirationDt) {
        this.expirationDt = expirationDt;
    }

}
