package party.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import party.data.entity.id.ClientReferenceId;

@Entity
@Table(name = "CLT_REF_RELATION")
public final class ClientReference implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ClientReferenceId clientReferenceId;

    @Column(name = "CIRF_REF_ID", length = 30, nullable = false)
    private String cirfRefId = SEPARATOR_BLANK;

    @Column(name = "REF_TYP_CD", length = 4, nullable = false)
    private String refTypCd = SEPARATOR_BLANK;

    @Column(name = "CIRF_EXP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime cirfExpDt;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "STATUS_CD", nullable = false)
    private char statusCd = BLANK_CHAR;

    @Column(name = "TERMINAL_ID", length = 8, nullable = false)
    private String terminalId = SEPARATOR_BLANK;

    @Column(name = "CIRF_EFF_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime cirfEffAcyTs;

    @Column(name = "CIRF_EXP_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime cirfExpAcyTs;

    public ClientReferenceId getClientReferenceId() {
        return clientReferenceId;
    }

    public void setClientReferenceId(ClientReferenceId clientReferenceId) {
        this.clientReferenceId = clientReferenceId;
    }

    public String getCirfRefId() {
        return cirfRefId;
    }

    public void setCirfRefId(String cirfRefId) {
        this.cirfRefId = cirfRefId;
    }

    public String getRefTypCd() {
        return refTypCd;
    }

    public void setRefTypCd(String refTypCd) {
        this.refTypCd = refTypCd;
    }

    public ZonedDateTime getCirfExpDt() {
        return cirfExpDt;
    }

    public void setCirfExpDt(ZonedDateTime cirfExpDt) {
        this.cirfExpDt = cirfExpDt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public char getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(char statusCd) {
        this.statusCd = statusCd;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public ZonedDateTime getCirfEffAcyTs() {
        return cirfEffAcyTs;
    }

    public void setCirfEffAcyTs(ZonedDateTime cirfEffAcyTs) {
        this.cirfEffAcyTs = cirfEffAcyTs;
    }

    public ZonedDateTime getCirfExpAcyTs() {
        return cirfExpAcyTs;
    }

    public void setCirfExpAcyTs(ZonedDateTime cirfExpAcyTs) {
        this.cirfExpAcyTs = cirfExpAcyTs;
    }

}
