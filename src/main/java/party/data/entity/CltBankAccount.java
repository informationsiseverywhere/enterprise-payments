package party.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.DECIMAL_ZERO;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import party.data.entity.id.CltBankAccountId;

@Entity
@Table(name = "CLT_BANK_ACCOUNT")
public class CltBankAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private CltBankAccountId cltBankAccountId;

    @Column(name = "CIBA_BK_ACT_TYP_CD", length = 3, nullable = false)
    private String cibaBkActTypCd = SEPARATOR_BLANK;

    @Column(name = "CIBA_BK_ACT_NBR", length = 25, nullable = false)
    private String cibaBkActNbr = SEPARATOR_BLANK;

    @Column(name = "CIBA_BK_RTE_NBR", length = 10, nullable = false)
    private String cibaBkRteNbr = SEPARATOR_BLANK;

    @Column(name = "CIBA_CRCRD_TYP_CD", nullable = false)
    private char cibaCrcrdTypCd = BLANK_CHAR;

    @Column(name = "CIBA_CRCRD_EXP_DT", length = 4, nullable = false)
    private String cibaCrcrdExpDt = SEPARATOR_BLANK;

    @Column(name = "CIBA_CRCRD_VRF_NBR", length = 3, nullable = false)
    private String cibaCrcrdVrfNbr = SEPARATOR_BLANK;

    @Column(name = "ADR_SEQ_NBR", length = 5, nullable = false)
    private short adrSeqNbr = SHORT_ZERO;

    @Column(name = "PHN_SEQ_NBR", length = 5, nullable = false)
    private short phnSeqNbr = SHORT_ZERO;

    @Column(name = "ASC_GRP_ID", length = 20, nullable = false)
    private String ascGrpId = SEPARATOR_BLANK;

    @Column(name = "CIBA_ASC_GRP_DES", length = 40, nullable = false)
    private String cibaAscGrpDes = SEPARATOR_BLANK;

    @Column(name = "CIBA_ASC_EFF_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime cibaAscEffDt;

    @Column(name = "CIBA_ASC_EXP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime cibaAscExpDt;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "EFFECTIVE_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime effectiveAcyTs;

    @Column(name = "EXPIRATION_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime expirationAcyTs;

    @Column(name = "CIBA_AUT_EFT_IND", nullable = false)
    private char cibaAutEftInd = BLANK_CHAR;

    @Column(name = "CIBA_AUT_EFT_TS", nullable = true)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime cibaAutEftTs;

    @Column(name = "EXPIRATION_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime expirationDt;

    @Column(name = "CIBA_BK_REC_TYP_CD", length = 3, nullable = false)
    private String cibaBkRecTypCd = SEPARATOR_BLANK;

    @Column(name = "CIBA_BK_DTB_PCT", length = 5, nullable = true)
    private double cibaBkDtbPct = DECIMAL_ZERO;

    @Column(name = "CIBA_BK_DTB_AMT", length = 14, nullable = true)
    private double cibaBkDtbAmt = DECIMAL_ZERO;

    public CltBankAccountId getCltBankAccountId() {
        return cltBankAccountId;
    }

    public void setCltBankAccountId(CltBankAccountId cltBankAccountId) {
        this.cltBankAccountId = cltBankAccountId;
    }

    public String getCibaBkActTypCd() {
        return cibaBkActTypCd;
    }

    public void setCibaBkActTypCd(String cibaBkActTypCd) {
        this.cibaBkActTypCd = cibaBkActTypCd;
    }

    public String getCibaBkActNbr() {
        return cibaBkActNbr;
    }

    public void setCibaBkActNbr(String cibaBkActNbr) {
        this.cibaBkActNbr = cibaBkActNbr;
    }

    public String getCibaBkRteNbr() {
        return cibaBkRteNbr;
    }

    public void setCibaBkRteNbr(String cibaBkRteNbr) {
        this.cibaBkRteNbr = cibaBkRteNbr;
    }

    public char getCibaCrcrdTypCd() {
        return cibaCrcrdTypCd;
    }

    public void setCibaCrcrdTypCd(char cibaCrcrdTypCd) {
        this.cibaCrcrdTypCd = cibaCrcrdTypCd;
    }

    public String getCibaCrcrdExpDt() {
        return cibaCrcrdExpDt;
    }

    public void setCibaCrcrdExpDt(String cibaCrcrdExpDt) {
        this.cibaCrcrdExpDt = cibaCrcrdExpDt;
    }

    public String getCibaCrcrdVrfNbr() {
        return cibaCrcrdVrfNbr;
    }

    public void setCibaCrcrdVrfNbr(String cibaCrcrdVrfNbr) {
        this.cibaCrcrdVrfNbr = cibaCrcrdVrfNbr;
    }

    public short getAdrSeqNbr() {
        return adrSeqNbr;
    }

    public void setAdrSeqNbr(short adrSeqNbr) {
        this.adrSeqNbr = adrSeqNbr;
    }

    public short getPhnSeqNbr() {
        return phnSeqNbr;
    }

    public void setPhnSeqNbr(short phnSeqNbr) {
        this.phnSeqNbr = phnSeqNbr;
    }

    public String getAscGrpId() {
        return ascGrpId;
    }

    public void setAscGrpId(String ascGrpId) {
        this.ascGrpId = ascGrpId;
    }

    public String getCibaAscGrpDes() {
        return cibaAscGrpDes;
    }

    public void setCibaAscGrpDes(String cibaAscGrpDes) {
        this.cibaAscGrpDes = cibaAscGrpDes;
    }

    public ZonedDateTime getCibaAscEffDt() {
        return cibaAscEffDt;
    }

    public void setCibaAscEffDt(ZonedDateTime cibaAscEffDt) {
        this.cibaAscEffDt = cibaAscEffDt;
    }

    public ZonedDateTime getCibaAscExpDt() {
        return cibaAscExpDt;
    }

    public void setCibaAscExpDt(ZonedDateTime cibaAscExpDt) {
        this.cibaAscExpDt = cibaAscExpDt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ZonedDateTime getEffectiveAcyTs() {
        return effectiveAcyTs;
    }

    public void setEffectiveAcyTs(ZonedDateTime effectiveAcyTs) {
        this.effectiveAcyTs = effectiveAcyTs;
    }

    public ZonedDateTime getExpirationAcyTs() {
        return expirationAcyTs;
    }

    public void setExpirationAcyTs(ZonedDateTime expirationAcyTs) {
        this.expirationAcyTs = expirationAcyTs;
    }

    public char getCibaAutEftInd() {
        return cibaAutEftInd;
    }

    public void setCibaAutEftInd(char cibaAutEftInd) {
        this.cibaAutEftInd = cibaAutEftInd;
    }

    public ZonedDateTime getCibaAutEftTs() {
        return cibaAutEftTs;
    }

    public void setCibaAutEftTs(ZonedDateTime cibaAutEftTs) {
        this.cibaAutEftTs = cibaAutEftTs;
    }

    public ZonedDateTime getExpirationDt() {
        return expirationDt;
    }

    public void setExpirationDt(ZonedDateTime expirationDt) {
        this.expirationDt = expirationDt;
    }

    public String getCibaBkRecTypCd() {
        return cibaBkRecTypCd;
    }

    public void setCibaBkRecTypCd(String cibaBkRecTypCd) {
        this.cibaBkRecTypCd = cibaBkRecTypCd;
    }

    public double getCibaBkDtbPct() {
        return cibaBkDtbPct;
    }

    public void setCibaBkDtbPct(double cibaBkDtbPct) {
        this.cibaBkDtbPct = cibaBkDtbPct;
    }

    public double getCibaBkDtbAmt() {
        return cibaBkDtbAmt;
    }

    public void setCibaBkDtbAmt(double cibaBkDtbAmt) {
        this.cibaBkDtbAmt = cibaBkDtbAmt;
    }

}
