package party.data.entity;

import static core.utils.CommonConstants.BLANK_CHAR;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import party.data.entity.id.CltObjRelationId;

@Entity
@Table(name = "CLT_OBJ_RELATION")
public class CltObjRelation implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private CltObjRelationId cltObjRelationId;

    @Column(name = "CLIENT_ID", length = 20, nullable = false)
    private String clientId = SEPARATOR_BLANK;

    @Column(name = "RLT_TYP_CD", length = 4, nullable = false)
    private String rltTypCd = SEPARATOR_BLANK;

    @Column(name = "OBJ_CD", length = 4, nullable = false)
    private String objCd = SEPARATOR_BLANK;

    @Column(name = "CIOR_SHW_OBJ_KEY", length = 30, nullable = false)
    private String ciorShwObjKey = SEPARATOR_BLANK;

    @Column(name = "ADR_SEQ_NBR", length = 5, nullable = false)
    private short adrSeqNbr = SHORT_ZERO;

    @Column(name = "USER_ID", length = 8, nullable = false)
    private String userId = SEPARATOR_BLANK;

    @Column(name = "STATUS_CD", nullable = false)
    private char statusCd = BLANK_CHAR;

    @Column(name = "TERMINAL_ID", length = 8, nullable = false)
    private String terminalId = SEPARATOR_BLANK;

    @Column(name = "CIOR_EXP_DT", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ciorExpDt;

    @Column(name = "CIOR_EFF_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime ciorEffAcyTs;

    @Column(name = "CIOR_EXP_ACY_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    private ZonedDateTime ciorExpAcyTs;

    public CltObjRelationId getCltObjRelationId() {
        return cltObjRelationId;
    }

    public void setCltObjRelationId(CltObjRelationId cltObjRelationId) {
        this.cltObjRelationId = cltObjRelationId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRltTypCd() {
        return rltTypCd;
    }

    public void setRltTypCd(String rltTypCd) {
        this.rltTypCd = rltTypCd;
    }

    public String getObjCd() {
        return objCd;
    }

    public void setObjCd(String objCd) {
        this.objCd = objCd;
    }

    public String getCiorShwObjKey() {
        return ciorShwObjKey;
    }

    public void setCiorShwObjKey(String ciorShwObjKey) {
        this.ciorShwObjKey = ciorShwObjKey;
    }

    public short getAdrSeqNbr() {
        return adrSeqNbr;
    }

    public void setAdrSeqNbr(short adrSeqNbr) {
        this.adrSeqNbr = adrSeqNbr;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public char getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(char statusCd) {
        this.statusCd = statusCd;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public ZonedDateTime getCiorExpDt() {
        return ciorExpDt;
    }

    public void setCiorExpDt(ZonedDateTime ciorExpDt) {
        this.ciorExpDt = ciorExpDt;
    }

    public ZonedDateTime getCiorEffAcyTs() {
        return ciorEffAcyTs;
    }

    public void setCiorEffAcyTs(ZonedDateTime ciorEffAcyTs) {
        this.ciorEffAcyTs = ciorEffAcyTs;
    }

    public ZonedDateTime getCiorExpAcyTs() {
        return ciorExpAcyTs;
    }

    public void setCiorExpAcyTs(ZonedDateTime ciorExpAcyTs) {
        this.ciorExpAcyTs = ciorExpAcyTs;
    }

}
