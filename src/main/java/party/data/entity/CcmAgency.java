package party.data.entity;

import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "CCM_AGENCY")
public class CcmAgency implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CCM_AGENCY_ID", length = 4, nullable = false)
    private String ccmAgencyId = SEPARATOR_BLANK;

    @Column(name = "CAG_AGENCY_NBR", length = 12, nullable = false)
    private String cagAgencyNbr = SEPARATOR_BLANK;

    @Column(name = "CAG_AGENCY_TYPE_CD", length = 3, nullable = false)
    private String cagAgencyTypeCd = SEPARATOR_BLANK;

    @Column(name = "CAG_LIC_VLD_IND", nullable = false)
    private Character cagLicVldInd;

    @Column(name = "CAG_AGC_ADR_NBR", length = 5, nullable = false)
    private Short cagAgcAdrNbr = SHORT_ZERO;

    @Column(name = "CAG_AGC_CLT_ID", length = 20, nullable = false)
    private String cagAgcCltId = SEPARATOR_BLANK;

    @Column(name = "CCM_LOCK_USER_ID", length = 8, nullable = false)
    private String ccmLockUserId = SEPARATOR_BLANK;

    @Column(name = "ISSUE_NODE_ID", length = 15, nullable = false)
    private String issueNodeId = SEPARATOR_BLANK;

    @Column(name = "CCM_LOCK_CLT_ID", length = 20, nullable = false)
    private String ccmLockCltId = SEPARATOR_BLANK;

    @Column(name = "CCM_FUNCTION_NM", length = 8, nullable = false)
    private String ccmFunctionNm = SEPARATOR_BLANK;

    @Column(name = "CCM_IN_FLIGHT_IND", nullable = false)
    private String ccmInFlightInd = SEPARATOR_BLANK;

    @Column(name = "CCM_USER_LOCK_NBR", length = 5, nullable = false)
    private Short ccmUserLockNbr = SHORT_ZERO;

    @Column(name = "CCM_LOCK_TS", nullable = false)
    @Type(type = "org.hibernate.type.ZonedDateTimeType")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private ZonedDateTime ccmLockTs;

    public String getCcmAgencyId() {
        return ccmAgencyId;
    }

    public void setCcmAgencyId(String ccmAgencyId) {
        this.ccmAgencyId = ccmAgencyId;
    }

    public String getCagAgencyNbr() {
        return cagAgencyNbr;
    }

    public void setCagAgencyNbr(String cagAgencyNbr) {
        this.cagAgencyNbr = cagAgencyNbr;
    }

    public String getCagAgencyTypeCd() {
        return cagAgencyTypeCd;
    }

    public void setCagAgencyTypeCd(String cagAgencyTypeCd) {
        this.cagAgencyTypeCd = cagAgencyTypeCd;
    }

    public Character getCagLicVldInd() {
        return cagLicVldInd;
    }

    public void setCagLicVldInd(Character cagLicVldInd) {
        this.cagLicVldInd = cagLicVldInd;
    }

    public Short getCagAgcAdrNbr() {
        return cagAgcAdrNbr;
    }

    public void setCagAgcAdrNbr(Short cagAgcAdrNbr) {
        this.cagAgcAdrNbr = cagAgcAdrNbr;
    }

    public String getCagAgcCltId() {
        return cagAgcCltId;
    }

    public void setCagAgcCltId(String cagAgcCltId) {
        this.cagAgcCltId = cagAgcCltId;
    }

    public String getCcmLockUserId() {
        return ccmLockUserId;
    }

    public void setCcmLockUserId(String ccmLockUserId) {
        this.ccmLockUserId = ccmLockUserId;
    }

    public String getIssueNodeId() {
        return issueNodeId;
    }

    public void setIssueNodeId(String issueNodeId) {
        this.issueNodeId = issueNodeId;
    }

    public String getCcmLockCltId() {
        return ccmLockCltId;
    }

    public void setCcmLockCltId(String ccmLockCltId) {
        this.ccmLockCltId = ccmLockCltId;
    }

    public String getCcmFunctionNm() {
        return ccmFunctionNm;
    }

    public void setCcmFunctionNm(String ccmFunctionNm) {
        this.ccmFunctionNm = ccmFunctionNm;
    }

    public String getCcmInFlightInd() {
        return ccmInFlightInd;
    }

    public void setCcmInFlightInd(String ccmInFlightInd) {
        this.ccmInFlightInd = ccmInFlightInd;
    }

    public Short getCcmUserLockNbr() {
        return ccmUserLockNbr;
    }

    public void setCcmUserLockNbr(Short ccmUserLockNbr) {
        this.ccmUserLockNbr = ccmUserLockNbr;
    }

    public ZonedDateTime getCcmLockTs() {
        return ccmLockTs;
    }

    public void setCcmLockTs(ZonedDateTime ccmLockTs) {
        this.ccmLockTs = ccmLockTs;
    }

}
