package party.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import party.data.entity.CcmAgency;

public interface CcmAgencyRepository extends CrudRepository<CcmAgency, String> {

    public CcmAgency findCagAgcCltIdByCagAgencyNbr(String agencyNumber);
    
    @Query(value = "SELECT c FROM CcmAgency c WHERE c.cagAgencyNbr = :cagAgencyNbr")
    public List<CcmAgency> findByAgencyNumber(@Param("cagAgencyNbr") String cagAgencyNbr);
}
