package party.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import party.data.entity.CltFinancialItt;
import party.data.entity.id.CltFinancialIttId;

public interface CltFinancialIttRepository extends CrudRepository<CltFinancialItt, CltFinancialIttId> {
    @Query(value = "select c from CltFinancialItt c where c.cltFinancialIttId.clientId = :clientId")
    public List<CltFinancialItt> findByClientId(@Param("clientId") String clientId);

    public CltFinancialItt findByCltFinancialIttIdTchObjectKey(String tchObjectKey);
}
