package party.data.repository;

import java.time.ZonedDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import party.data.entity.ClientTab;
import party.data.entity.id.ClientTabId;

public interface ClientTabRepository extends JpaRepository<ClientTab, ClientTabId> {

    public ClientTab findByClientTabIdClientId(String clientId);

    @Query(value = "select c from ClientTab c where c.clientTabId.historyVldNbr = 0 and c.ciclExpDt = :ciclExpDt")
    public Page<ClientTab> findAllClients(@Param("ciclExpDt") ZonedDateTime ciclExpDt, Pageable pageable);

    @Query(value = "select COUNT(*) from ClientTab c where c.clientTabId.historyVldNbr = 0 and c.ciclExpDt = :ciclExpDt")
    public Integer countAllClients(@Param("ciclExpDt") ZonedDateTime ciclExpDt);

    @Query(value = "select c from ClientTab c where c.clientTabId.clientId = :clientId and c.clientTabId.historyVldNbr = :historyVldNbr and c.ciclExpDt = :ciclExpDt")
    public ClientTab findClient(@Param("clientId") String clientId, @Param("historyVldNbr") short historyVldNbr,
            @Param("ciclExpDt") ZonedDateTime ciclExpDt);

    @Query(value = "select c from ClientTab c where c.clientTabId.historyVldNbr = :historyVldNbr and c.userId = :userId and c.ciclExpDt = :ciclExpDt")
    public ClientTab findByUserId(@Param("userId") String userId, @Param("historyVldNbr") short historyVldNbr,
            @Param("ciclExpDt") ZonedDateTime ciclExpDt);

}
