package party.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import party.data.entity.CltBankAccount;
import party.data.entity.id.CltBankAccountId;

public interface CltBankAccountRepository extends CrudRepository<CltBankAccount, CltBankAccountId> {

    @Query(value = "select c from CltBankAccount c where c.cltBankAccountId.clientId = :clientId")
    public List<CltBankAccount> findByClientId(@Param("clientId") String clientId);

    @Query(value = "select c from CltBankAccount c where c.cltBankAccountId.clientId = :clientId and c.cltBankAccountId.historyVldNbr = :historyVldNbr "
            + " and c.cltBankAccountId.effectiveDt = :effectiveDt and c.cltBankAccountId.cibaActSeqNbr = :sequenceNumber")
    public CltBankAccount findBankTapeData(@Param("clientId") String clientId,
            @Param("historyVldNbr") short historyVldNbr, @Param("effectiveDt") ZonedDateTime effectiveDt,
            @Param("sequenceNumber") Short sequenceNumber);

    @Query(value = "select MAX(c.cltBankAccountId.effectiveDt), MAX(c.cltBankAccountId.cibaActSeqNbr) from CltBankAccount c where c.cltBankAccountId.clientId = :clientId "
            + " and c.cltBankAccountId.historyVldNbr = :historyVldNbr and c.cltBankAccountId.effectiveDt <= :effectiveDate")
    public List<Object[]> getMaxEffectiveDateAndAccountSequenceNumber(@Param("clientId") String clientId,
            @Param("historyVldNbr") short historyVldNbr, @Param("effectiveDate") ZonedDateTime effectiveDate);
    
    @Query(value = "select c from CltBankAccount c where c.cltBankAccountId.clientId = :clientId "
            + " and c.cltBankAccountId.historyVldNbr = :historyVldNbr and c.cltBankAccountId.effectiveDt <= :effectiveDate "
            + " order by c.cltBankAccountId.effectiveDt, c.cltBankAccountId.cibaActSeqNbr")
    public List<CltBankAccount> getMaxCltBankAccountRow(@Param("clientId") String clientId,
            @Param("historyVldNbr") short historyVldNbr, @Param("effectiveDate") ZonedDateTime effectiveDate);
    
    @Query(value = "select c from CltBankAccount c where c.cltBankAccountId.clientId = :clientId and c.cltBankAccountId.historyVldNbr = :historyVldNbr"
            + " and c.cltBankAccountId.effectiveDt in :effectiveDate and c.cltBankAccountId.cibaActSeqNbr in :cibaActSeqNbr ")
    public List<CltBankAccount> fetchPolicyDataRetrieve(@Param("clientId") String clientId,
            @Param("historyVldNbr") short historyVldNbr, @Param("effectiveDate") ZonedDateTime effectiveDate,
            @Param("cibaActSeqNbr") short cibaActSeqNbr, Pageable pageable);
    
    

}
