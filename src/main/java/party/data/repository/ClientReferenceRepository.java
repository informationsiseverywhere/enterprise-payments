package party.data.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import party.data.entity.ClientReference;
import party.data.entity.id.ClientReferenceId;

public interface ClientReferenceRepository extends CrudRepository<ClientReference, ClientReferenceId> {

    @Query(value = "select c from ClientReference c where c.clientReferenceId.clientId = :clientId and c.clientReferenceId.historyVldNbr = :historyVldNbr "
            + "and c.cirfExpDt = :cirfExpDt and c.refTypCd in :typeList")
    public ClientReference findByClientIdAndType(@Param("clientId") String clientId,
            @Param("historyVldNbr") short historyVldNbr, @Param("cirfExpDt") ZonedDateTime cirfExpDt,
            @Param("typeList") List<String> typeList);

    @Query(value = "select c from ClientReference c where c.clientReferenceId.clientId = :clientId and c.clientReferenceId.historyVldNbr = :historyVldNbr "
            + "and c.cirfExpDt = :cirfExpDt and c.refTypCd in :typeList and c.cirfRefId = :cirfRefId")
    public ClientReference findByClientIdAndReference(@Param("clientId") String clientId,
            @Param("historyVldNbr") short historyVldNbr, @Param("cirfExpDt") ZonedDateTime cirfExpDt,
            @Param("typeList") List<String> typeList, @Param("cirfRefId") String cirfRefId);

    @Query(value = "select c from ClientReference c where c.cirfRefId = :referenceId and c.refTypCd in :refTypCdList")
    public List<ClientReference> findClientReferenceIdClientIdByCirfRefIdAndRefTypCd(@Param("referenceId") String referenceId,
           @Param("refTypCdList") List<String> refTypCdList);
    
    @Query(value = "select c from ClientReference c where c.cirfRefId = :cirfRefId and c.refTypCd in :refTypCd and c.clientReferenceId.historyVldNbr = :historyVldNbr")
    public List<ClientReference> findByClientReferenceId(@Param("cirfRefId") String cirfRefId,
            @Param("refTypCd") List<String> refTypCd, @Param("historyVldNbr") short historyVldNbr);
    
    @Query(value = "select c from ClientReference c where c.clientReferenceId.clientId = :clientId and c.clientReferenceId.cirfEffDt > :cirfEffDt "
            + " and c.clientReferenceId.historyVldNbr = :historyVldNbr")
    public List<ClientReference> fetchByClientId(@Param("clientId") String clientId,
            @Param("cirfEffDt") ZonedDateTime cirfEffDt, @Param("historyVldNbr") short historyVldNbr, Pageable pageable);

    @Query(value = "select c from ClientReference c where c.clientReferenceId.clientId = :clientId and c.cirfExpDt > :cirfExpDt  "
            + "and c.clientReferenceId.cirfEffDt <= :cirfEffDt")
    public List<ClientReference> fetchByClientIdAndDate(@Param("clientId") String clientId,
            @Param("cirfExpDt") ZonedDateTime cirfExpDt, @Param("cirfEffDt") ZonedDateTime cirfEffDt, Pageable pageable);

}
