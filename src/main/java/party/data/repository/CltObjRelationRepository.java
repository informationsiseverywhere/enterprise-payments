package party.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import party.data.entity.CltObjRelation;
import party.data.entity.id.CltObjRelationId;

public interface CltObjRelationRepository extends JpaRepository<CltObjRelation, CltObjRelationId> {

    @Query(value = "select c from CltObjRelation c where c.cltObjRelationId.tchObjectKey = :tchObjectKey "
            + "and c.rltTypCd = :typeCode and c.ciorShwObjKey = :showableObjectKey and  c.cltObjRelationId.ciorEffDt < c.ciorExpDt order by c.ciorExpDt Asc")
    public List<CltObjRelation> findPayeeNameByType(@Param("tchObjectKey") String tchObjectKey,
            @Param("typeCode") String typeCode, @Param("showableObjectKey") String showableObjectKey);

    @Query(value = "select c from CltObjRelation c where c.clientId = :clientId and c.ciorShwObjKey = :showableObjectKey and c.rltTypCd = :relationType")
    public CltObjRelation findClientRelation(@Param("clientId") String clientId,
            @Param("showableObjectKey") String showableObjectKey, @Param("relationType") String relationType);

}
