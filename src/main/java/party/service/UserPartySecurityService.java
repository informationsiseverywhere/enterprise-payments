package party.service;

import java.time.ZonedDateTime;

import party.model.UserParty;

public interface UserPartySecurityService {

    public Boolean isUserPartyAccess(String accountId, String payorId);

    public UserParty getPartyByUserId();
    
    public String getAgencyClientIdFromClienReference(ZonedDateTime eftAcyDate, String agencyNumber, 
            String agencyClientId);

}
