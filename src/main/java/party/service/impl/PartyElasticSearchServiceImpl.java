package party.service.impl;

import java.io.IOException;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import billing.model.embedded.Party;
import billing.utils.elasticsearch.ESClient;

@Service
public class PartyElasticSearchServiceImpl {
    private static final String PARTY_INDEX = "parties";
    @Autowired
    ESClient esClient;

    public Party fetchPartyDocument(String partyId) throws IOException {
        RestHighLevelClient client = esClient.getClient();
        Party partySearch = new Party();
        ObjectMapper mapper = new ObjectMapper();

        GetRequest getRequest = new GetRequest().index(PARTY_INDEX).id(partyId.trim());
        try {
            GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);
            String source = getResponse.getSourceAsString();
            if (source != null) {
                partySearch = mapper.readValue(source, Party.class);
            }
        } catch (ElasticsearchException e) {

        }
        return partySearch;
    }

}