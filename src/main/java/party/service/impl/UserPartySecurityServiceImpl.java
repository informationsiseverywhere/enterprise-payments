package party.service.impl;

import static core.utils.CommonConstants.BLANK_STRING;
import static core.utils.CommonConstants.SEPARATOR_BLANK;
import static core.utils.CommonConstants.SHORT_ONE;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import application.utils.model.embedded.AccountSearch;
import application.utils.service.impl.AccountsElasticSearchServiceImpl;
import billing.data.entity.BilAccount;
import billing.data.entity.BilRulesUct;
import billing.data.repository.BilAccountRepository;
import billing.model.embedded.Party;
import billing.service.BilRulesUctService;
import core.api.ExecContext;
import core.utils.DateRoutine;
import party.data.entity.ClientReference;
import party.data.entity.ClientTab;
import party.data.repository.ClientReferenceRepository;
import party.data.repository.ClientTabRepository;
import party.model.UserParty;
import party.service.UserPartySecurityService;

@Service
public class UserPartySecurityServiceImpl implements UserPartySecurityService {

    @Autowired
    private ExecContext execContext;

    @Autowired
    private ClientTabRepository clientTabRepository;

    @Autowired
    private BilAccountRepository bilAccountRepository;

    @Autowired
    private PartyElasticSearchServiceImpl partyElasticSearchServiceImpl;
    
    @Autowired
    private AccountsElasticSearchServiceImpl accountsElasticSearchServiceImpl;

    @Autowired
    private BilRulesUctService bilRulesUctService;
    
    @Autowired
    private ClientReferenceRepository clientReferenceRepository;

    @Value("${enterprise.parties.url}")
    private String externalPayorService;

    private static final String AGENT_ACCESS_RULE = "ACAG";

    public static final Logger logger = LogManager.getLogger(UserPartySecurityServiceImpl.class);

    @Override
    public UserParty getPartyByUserId() {
        String userId = execContext.getUserSeqeunceId();
        logger.debug("Search party with user id:{} ", userId);
        UserParty userParty = new UserParty();
        if (externalPayorService == null || externalPayorService.isEmpty()) {
            userParty.setRole(Role.EMPLOYEE.getValue());
            return userParty;
        }
        ClientTab clientTab = clientTabRepository.findByUserId(userId, SHORT_ZERO, DateRoutine.defaultDateTime());
        if (clientTab != null) {
            userParty.setPartyId(clientTab.getClientTabId().getClientId());
            userParty.setRole(clientTab.getCiclPriSubCd().trim());
            BilRulesUct acagBilRulesUct = bilRulesUctService.readBilRulesUct(AGENT_ACCESS_RULE, BLANK_STRING,
                    BLANK_STRING, BLANK_STRING);
            if (acagBilRulesUct != null) {
                userParty.setIsAgentAccess(acagBilRulesUct.getBrtRuleCd() == 'Y');
            }
            List<String> agencies = getAgenciesByPartyId(clientTab.getClientTabId().getClientId());
            if (!agencies.isEmpty()) {
                userParty.setAgencies(agencies);
            }
        }
        return userParty;
    }

    private List<String> getAgenciesByPartyId(String partyId) {
        List<String> agencies = new ArrayList<>();
        try {
            Party party = partyElasticSearchServiceImpl.fetchPartyDocument(partyId);
            if (party != null) {
                if (party.getAgencies() != null && !party.getAgencies().isEmpty()) {
                    agencies.addAll(party.getAgencies());
                } else if (party.getAgentNumber() != null && !party.getAgentNumber().trim().isEmpty()) {
                    agencies.add(party.getAgentNumber().trim());
                }
                if (!agencies.isEmpty()) {
                    agencies = new ArrayList<>(new HashSet<>(agencies));
                }
            }
        } catch (IOException e) {
            logger.error("Error in fetching the Party : {}", partyId);
        }
        return agencies;
    }

    @Override
    public Boolean isUserPartyAccess(String accountId, String payorId) {
        if (externalPayorService == null || externalPayorService.isEmpty()) {
            return true;
        }
        if (payorId == null) {
            BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
            if (bilAccount != null) {
                payorId = bilAccount.getPayorClientId().trim();
            }
        }
        UserParty userParty = getPartyByUserId();
        if (userParty.getRole() != null && userParty.getRole().equals(Role.USER.getValue())) {
            if (payorId != null && payorId.equals(userParty.getPartyId())) {
                return true;
            }
        } else if (userParty.getRole() != null && isAgent(userParty.getRole())) {
            if (userParty.getAgencies() != null && !userParty.getAgencies().isEmpty()
                    && validateAgent(accountId, userParty.getAgencies())) {
                return true;
            }
        } else if (userParty.getRole() != null && (userParty.getRole().equals(Role.EMPLOYEE.getValue())
                || userParty.getRole().equals(Role.CUSTOMER_REPRESENTATIVE.getValue()))) {
            return true;
        }
        return false;
    }

    private boolean validateAgent(String accountId, List<String> agencies) {
        BilAccount bilAccount = bilAccountRepository.findById(accountId).orElse(null);
        if (bilAccount == null) {
            return false;
        }
        BilRulesUct acagBilRulesUct = bilRulesUctService.readBilRulesUct(AGENT_ACCESS_RULE, bilAccount.getBillTypeCd(),
                bilAccount.getBillClassCd(), BLANK_STRING);
        if (acagBilRulesUct != null) {
            boolean isAgencyAccess = acagBilRulesUct.getBrtRuleCd() == 'Y';
            if (isAgencyAccess) {
                try {
                    AccountSearch accountSearch = accountsElasticSearchServiceImpl.fetchAccountDocument(accountId);
                    if (accountSearch != null && accountSearch.getAccountId() != null
                            && !accountSearch.getAccountId().isEmpty() && accountSearch.getAgencies() != null
                            && !accountSearch.getAgencies().isEmpty()) {
                        return accountSearch.getAgencies().stream().anyMatch(agencies::contains);
                    }
                } catch (IOException e) {
                    logger.error("Account not Found");
                }
                return false;
            }
        }
        return true;
    }

    private boolean isAgent(String role) {
        return role.equals(Role.AGENT.getValue()) || role.equals(Role.AGENCY.getValue());
    }

    public enum Role {
        AGENT("AGT"), AGENCY("AGYN"), EMPLOYEE("EMP"), CUSTOMER_REPRESENTATIVE("CSR"), USER("USR"), ADMIN("ADM");

        private final String type;

        private Role(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    @Override
    public String getAgencyClientIdFromClienReference(ZonedDateTime eftAcyDate, String agencyNumber,
            String agencyClientId) {
        
        String clientId = SEPARATOR_BLANK;
        List<ClientReference> clientReferenceList = null;
        
        if (agencyClientId.isEmpty()) {
            clientReferenceList = clientReferenceRepository.findByClientReferenceId(agencyNumber, getReferenceTypeCodeList(), SHORT_ZERO);
        } else {
            clientReferenceList = clientReferenceRepository.fetchByClientIdAndDate(agencyClientId, eftAcyDate,
                    eftAcyDate, PageRequest.of(SHORT_ZERO, SHORT_ONE));

            if (clientReferenceList == null || clientReferenceList.isEmpty()) {
                clientReferenceList = clientReferenceRepository.fetchByClientId(agencyClientId, eftAcyDate, SHORT_ZERO,
                        PageRequest.of(SHORT_ZERO, SHORT_ONE));
            }
        }

        if (clientReferenceList != null && !clientReferenceList.isEmpty()) {
            clientId = clientReferenceList.get(SHORT_ZERO).getClientReferenceId().getClientId();
        }

        return clientId;
    }
    
    private ArrayList<String> getReferenceTypeCodeList() {
        ArrayList<String> referenceTypeCdList = new ArrayList<>();
        referenceTypeCdList.add("ANBR");
        referenceTypeCdList.add("AGYN");
        return referenceTypeCdList;
    }

}
