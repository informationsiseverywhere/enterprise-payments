package party.model;

import java.util.List;
import java.util.Objects;

import org.springframework.data.annotation.Id;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserParty extends ResourceSupport {
    @Id
    private String partyId;
    private String fullname;
    private List<String> agencies;
    private String role;
    private Boolean isAgentAccess;

    public UserParty(@JsonProperty("partyId") String partyId, @JsonProperty("fullname") String fullname,
            @JsonProperty("agencies") List<String> agencies, @JsonProperty("role") String role,
            @JsonProperty("isAgentAccess") Boolean isAgentAccess) {
        super();
        this.partyId = partyId;
        this.fullname = fullname;
        this.agencies = agencies;
        this.role = role;
        this.isAgentAccess = isAgentAccess;
    }

    public UserParty() {
        super();
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public List<String> getAgencies() {
        return agencies;
    }

    public void setAgencies(List<String> agencies) {
        this.agencies = agencies;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Boolean getIsAgentAccess() {
        return isAgentAccess;
    }

    public void setIsAgentAccess(Boolean isAgentAccess) {
        this.isAgentAccess = isAgentAccess;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(agencies, fullname, isAgentAccess, partyId, role);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserParty other = (UserParty) obj;
        return Objects.equals(agencies, other.agencies) && Objects.equals(fullname, other.fullname)
                && Objects.equals(isAgentAccess, other.isAgentAccess) && Objects.equals(partyId, other.partyId)
                && Objects.equals(role, other.role);
    }

}
