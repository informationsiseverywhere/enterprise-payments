package application.handler.service;

import application.handler.model.AccountChargesData;

public interface ChargesUserExitService {

    public AccountChargesData chargeProcess(AccountChargesData accountChargesData);

}
