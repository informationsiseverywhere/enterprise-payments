package application.handler.service;

import application.handler.model.MaxPcnData;

public interface MaxPcnUserExitService {

    public MaxPcnData maxPcnProcess(MaxPcnData maxPcnData);

}
