package application.handler.service.impl;

import org.springframework.stereotype.Service;

import application.handler.model.AccountChargesData;
import application.handler.service.ChargesUserExitService;

@Service
public class ChargesUserExitServiceImpl implements ChargesUserExitService {

    @Override
    public AccountChargesData chargeProcess(AccountChargesData accountChargesData) {
        return accountChargesData;
    }

}
