package application.handler.service.impl;

import org.springframework.stereotype.Service;

import application.handler.model.MaxPcnData;
import application.handler.service.MaxPcnUserExitService;

@Service
public class MaxPcnUserExitServiceImpl implements MaxPcnUserExitService {

    @Override
    public MaxPcnData maxPcnProcess(MaxPcnData maxPcnData) {
        return maxPcnData;
    }

}
