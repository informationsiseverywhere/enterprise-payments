package application.handler.model;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AccountChargesData {
    private String accountId;
    private String accountType;
    private String accountPlan;
    private String folderId;
    private String businessCaseId;
    private String applicationName;
    private char serviceChargeIndicator;
    private String serviceChargeTypeCode;
    private char serviceChargeCode;
    private Double serviceChargeAmount;
    private ZonedDateTime invoiceDate;
    private ZonedDateTime dueDate;
    private ZonedDateTime currentSE3Date;
    private String userId;

    public AccountChargesData() {
        super();
    }

    public AccountChargesData(String accountId, String accountType, String accountPlan, String folderId,
            String businessCaseId, String applicationName, char serviceChargeIndicator, String serviceChargeTypeCode,
            char serviceChargeCode, Double serviceChargeAmount, ZonedDateTime invoiceDate, ZonedDateTime dueDate,
            ZonedDateTime currentSE3Date, String userId) {
        super();
        this.accountId = accountId;
        this.accountType = accountType;
        this.accountPlan = accountPlan;
        this.folderId = folderId;
        this.businessCaseId = businessCaseId;
        this.applicationName = applicationName;
        this.serviceChargeIndicator = serviceChargeIndicator;
        this.serviceChargeTypeCode = serviceChargeTypeCode;
        this.serviceChargeCode = serviceChargeCode;
        this.serviceChargeAmount = serviceChargeAmount;
        this.invoiceDate = invoiceDate;
        this.dueDate = dueDate;
        this.currentSE3Date = currentSE3Date;
        this.userId = userId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountPlan() {
        return accountPlan;
    }

    public void setAccountPlan(String accountPlan) {
        this.accountPlan = accountPlan;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getBusinessCaseId() {
        return businessCaseId;
    }

    public void setBusinessCaseId(String businessCaseId) {
        this.businessCaseId = businessCaseId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public char getServiceChargeIndicator() {
        return serviceChargeIndicator;
    }

    public void setServiceChargeIndicator(char serviceChargeIndicator) {
        this.serviceChargeIndicator = serviceChargeIndicator;
    }

    public String getServiceChargeTypeCode() {
        return serviceChargeTypeCode;
    }

    public void setServiceChargeTypeCode(String serviceChargeTypeCode) {
        this.serviceChargeTypeCode = serviceChargeTypeCode;
    }

    public char getServiceChargeCode() {
        return serviceChargeCode;
    }

    public void setServiceChargeCode(char serviceChargeCode) {
        this.serviceChargeCode = serviceChargeCode;
    }

    public Double getServiceChargeAmount() {
        return serviceChargeAmount;
    }

    public void setServiceChargeAmount(Double serviceChargeAmount) {
        this.serviceChargeAmount = serviceChargeAmount;
    }

    public ZonedDateTime getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(ZonedDateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public ZonedDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(ZonedDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public ZonedDateTime getCurrentSE3Date() {
        return currentSE3Date;
    }

    public void setCurrentSE3Date(ZonedDateTime currentSE3Date) {
        this.currentSE3Date = currentSE3Date;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
