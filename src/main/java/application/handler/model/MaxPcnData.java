package application.handler.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MaxPcnData {
    private String accountId;
    private Integer pcnmPcnCount;
    private Integer pcnmRulePcnCount;
    private Integer pcnmNumOfMonth;
    private boolean verifyMaxPCN;
    private boolean isMaxPcn;

    public MaxPcnData(@JsonProperty("pcnmPcnCount") String accountId,
            @JsonProperty("pcnmPcnCount") Integer pcnmPcnCount,
            @JsonProperty("pcnmRulePcnCount") Integer pcnmRulePcnCount,
            @JsonProperty("pcnmNumOfMonth") Integer pcnmNumOfMonth, @JsonProperty("verifyMaxPCN") boolean verifyMaxPCN,
            @JsonProperty("isMaxPcn") boolean isMaxPcn) {
        super();
        this.accountId = accountId;
        this.pcnmPcnCount = pcnmPcnCount;
        this.pcnmRulePcnCount = pcnmRulePcnCount;
        this.pcnmNumOfMonth = pcnmNumOfMonth;
        this.verifyMaxPCN = verifyMaxPCN;
        this.isMaxPcn = isMaxPcn;
    }

    public MaxPcnData() {
        super();
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Integer getPcnmPcnCount() {
        return pcnmPcnCount;
    }

    public void setPcnmPcnCount(Integer pcnmPcnCount) {
        this.pcnmPcnCount = pcnmPcnCount;
    }

    public Integer getPcnmRulePcnCount() {
        return pcnmRulePcnCount;
    }

    public void setPcnmRulePcnCount(Integer pcnmRulePcnCount) {
        this.pcnmRulePcnCount = pcnmRulePcnCount;
    }

    public Integer getPcnmNumOfMonth() {
        return pcnmNumOfMonth;
    }

    public void setPcnmNumOfMonth(Integer pcnmNumOfMonth) {
        this.pcnmNumOfMonth = pcnmNumOfMonth;
    }

    public boolean isVerifyMaxPCN() {
        return verifyMaxPCN;
    }

    public void setVerifyMaxPCN(boolean verifyMaxPCN) {
        this.verifyMaxPCN = verifyMaxPCN;
    }

    public boolean isMaxPcn() {
        return isMaxPcn;
    }

    public void setMaxPcn(boolean isMaxPcn) {
        this.isMaxPcn = isMaxPcn;
    }

}
