package application.fis.service;

import fis.transferobject.FinancialApiActivity;

public interface BcmBusinessActivityUserExitService {

        public FinancialApiActivity getBcmBusinessActivityUserExit(FinancialApiActivity financialApiActivity);

}
