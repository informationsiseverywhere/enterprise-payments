package application.fis.service;

import fis.transferobject.FinancialApiActivity;

public interface DwsBusinessActivityUserExitService {

    public FinancialApiActivity getDwsBusinessActivityUserExit(FinancialApiActivity financialApiActivity);

}
