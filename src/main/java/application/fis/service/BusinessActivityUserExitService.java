package application.fis.service;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fis.transferobject.FinancialApiActivity;

@Service
public interface BusinessActivityUserExitService {

    public FinancialApiActivity getBusinessActivityUserExit(FinancialApiActivity financialApiActivity,String tableName,
            Pageable pageable);

}
