package application.fis.service;

import fis.transferobject.FinancialApiActivity;

public interface MasterCompanyUserExitService {

    public FinancialApiActivity getMasterCompany(FinancialApiActivity financialApiActivity);

}
