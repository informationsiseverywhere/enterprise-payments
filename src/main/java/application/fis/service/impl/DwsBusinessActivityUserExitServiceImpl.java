package application.fis.service.impl;

import org.springframework.stereotype.Service;

import application.fis.service.DwsBusinessActivityUserExitService;
import fis.transferobject.FinancialApiActivity;

@Service
public class DwsBusinessActivityUserExitServiceImpl implements DwsBusinessActivityUserExitService {

    @Override
    public FinancialApiActivity getDwsBusinessActivityUserExit(FinancialApiActivity financialApiActivity) {
        // will be implemented as per customer requirement.
        return financialApiActivity;
    }

}
