package application.fis.service.impl;

import static core.utils.CommonConstants.CHAR_N;
import static core.utils.CommonConstants.SHORT_ZERO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import application.fis.service.BcmBusinessActivityUserExitService;
import application.fis.service.BusinessActivityUserExitService;
import application.fis.service.DwsBusinessActivityUserExitService;
import core.datetime.service.DateService;
import core.utils.DateRoutine;
import fis.data.entity.FwsIstUserSpt;
import fis.data.repository.FwsIstUserSptRepository;
import fis.transferobject.FinancialApiActivity;
import fis.utils.FinancialIntegratorConstants.ApplicationProduct;
import fis.utils.FinancialIntegratorConstants.TableNames;

@Service
public class BusinessActivityUserExitServiceImpl implements BusinessActivityUserExitService {

    @Autowired
    private BcmBusinessActivityUserExitService bcmBusinessActivityUserExitService;

    @Autowired
    private DwsBusinessActivityUserExitService dwsBusinessActivityUserExitService;

    @Autowired
    private FwsIstUserSptRepository fwsIstUserSptRepository;

    @Autowired
    private DateService dateService;

    @Override
    public FinancialApiActivity getBusinessActivityUserExit(FinancialApiActivity financialApiActivity, String tableName,
            Pageable pageable) {

        if (tableName.equalsIgnoreCase(TableNames.BCM_BUS_ACTIVITY_V.toString())) {
            List<FwsIstUserSpt> fwsIstUserSptList = fwsIstUserSptRepository.checkForUserExit(
                    ApplicationProduct.BCUSR.toString(), DateRoutine.defaultDateTime(), SHORT_ZERO,
                    dateService.currentDate(), pageable);

            if (fwsIstUserSptList != null && fwsIstUserSptList.get(SHORT_ZERO).getFwsHsApiPtpInd() != CHAR_N) {
                return bcmBusinessActivityUserExitService.getBcmBusinessActivityUserExit(financialApiActivity);
            }
        } else {
            if (tableName.equalsIgnoreCase(TableNames.DWS_BUS_ACTIVITY_V.toString())) {
                List<FwsIstUserSpt> fwsIstUserSptList = fwsIstUserSptRepository.checkForUserExit(
                        ApplicationProduct.DWUSR.toString(), DateRoutine.defaultDateTime(), SHORT_ZERO,
                        dateService.currentDate(), pageable);

                if (fwsIstUserSptList != null && fwsIstUserSptList.get(SHORT_ZERO).getFwsHsApiPtpInd() != CHAR_N) {
                    return dwsBusinessActivityUserExitService.getDwsBusinessActivityUserExit(financialApiActivity);
                }
            }
        }

        return financialApiActivity;
    }

}
