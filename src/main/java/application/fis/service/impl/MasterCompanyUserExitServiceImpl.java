package application.fis.service.impl;

import static billing.utils.BillingConstants.MASTER_COMPANY_NUMBER;

import org.springframework.stereotype.Service;

import application.fis.service.MasterCompanyUserExitService;
import fis.transferobject.FinancialApiActivity;

@Service
public class MasterCompanyUserExitServiceImpl implements MasterCompanyUserExitService {

    @Override
    public FinancialApiActivity getMasterCompany(FinancialApiActivity financialApiActivity) {
        if (financialApiActivity.getMasterCompanyNbr() == null
                || financialApiActivity.getMasterCompanyNbr().trim().isEmpty()) {
            financialApiActivity.setMasterCompanyNbr(MASTER_COMPANY_NUMBER);
        }
        return financialApiActivity;
    }

}
