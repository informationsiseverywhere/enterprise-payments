package application.fis.service.impl;

import org.springframework.stereotype.Service;

import application.fis.service.BcmBusinessActivityUserExitService;
import fis.transferobject.FinancialApiActivity;

@Service
public class BcmBusinessActivityUserExitServiceImpl implements BcmBusinessActivityUserExitService {

    @Override
    public FinancialApiActivity getBcmBusinessActivityUserExit(FinancialApiActivity financialApiActivity) {
        return financialApiActivity;
    }

}
