package application.utils.model;

import java.time.ZonedDateTime;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import core.utils.CustomDateTimeDeSerializer;
import core.utils.CustomDateTimeSerializer;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthorizeOption extends ResourceSupport {

    private String policyNumber;
    private String policySymbol;
    private String policyId;
    private Double commissionPercentage;
    private Double amount;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ZonedDateTime effectiveDate;
    private String actionCode;

    public AuthorizeOption(@JsonProperty("policyNumber") String policyNumber,
            @JsonProperty("policySymbol") String policySymbol, @JsonProperty("policyId") String policyId,
            @JsonProperty("commissionPercentage") Double commissionPercentage, @JsonProperty("amount") Double amount,
            @JsonProperty("effectiveDate") ZonedDateTime effectiveDate, @JsonProperty("actionCode") String actionCode) {
        super();

        this.policyNumber = policyNumber;
        this.policySymbol = policySymbol;
        this.policyId = policyId;
        this.commissionPercentage = commissionPercentage;
        this.amount = amount;
        this.effectiveDate = effectiveDate;
        this.actionCode = actionCode;
    }

    public AuthorizeOption() {
        super();
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicySymbol() {
        return policySymbol;
    }

    public void setPolicySymbol(String policySymbol) {
        this.policySymbol = policySymbol;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public Double getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(Double commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public ZonedDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(actionCode, amount, commissionPercentage, effectiveDate, policyId,
                policyNumber, policySymbol);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AuthorizeOption other = (AuthorizeOption) obj;
        return Objects.equals(actionCode, other.actionCode) && Objects.equals(amount, other.amount)
                && Objects.equals(commissionPercentage, other.commissionPercentage)
                && Objects.equals(effectiveDate, other.effectiveDate) && Objects.equals(policyId, other.policyId)
                && Objects.equals(policyNumber, other.policyNumber) && Objects.equals(policySymbol, other.policySymbol);
    }

}
