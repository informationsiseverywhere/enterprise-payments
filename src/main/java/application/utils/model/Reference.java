package application.utils.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import application.utils.model.embedded.Link;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Reference {

    private String referenceId;
    private String type;
    private String reference;
    private String referenceKey;
    private List<Link> links;

    public Reference() {
        super();
    }

    @JsonCreator
    public Reference(@JsonProperty("referenceId") String referenceId, @JsonProperty("type") String type,
            @JsonProperty("reference") String reference, @JsonProperty("referenceKey") String referenceKey) {
        super();
        this.referenceId = referenceId;
        this.type = type;
        this.reference = reference;
        this.referenceKey = referenceKey;
    }

    public Reference(Reference reference) {
        this(reference.getReferenceId(), reference.getType(), reference.getReference(), reference.getReferenceKey());
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getReferenceKey() {
        return referenceKey;
    }

    public void setReferenceKey(String referenceKey) {
        this.referenceKey = referenceKey;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}