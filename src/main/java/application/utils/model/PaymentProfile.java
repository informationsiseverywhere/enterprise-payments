package application.utils.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PaymentProfile {
    private String paymentProfileId;
    private String name;
    private String address;
    private BankAccount bankAccount;
    private CreditCard creditCard;

    public PaymentProfile(@JsonProperty("paymentProfileId") String paymentProfileId, @JsonProperty("name") String name,
            @JsonProperty("address") String address, @JsonProperty("bankAccount") BankAccount bankAccount,
            @JsonProperty("creditCard") CreditCard creditCard) {
        super();
        this.paymentProfileId = paymentProfileId;
        this.name = name;
        this.address = address;
        this.bankAccount = bankAccount;
        this.creditCard = creditCard;
    }

    public PaymentProfile() {
        super();
    }

    public String getPaymentProfileId() {
        return paymentProfileId;
    }

    public void setPaymentProfileId(String paymentProfileId) {
        this.paymentProfileId = paymentProfileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, bankAccount, creditCard, name, paymentProfileId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PaymentProfile other = (PaymentProfile) obj;
        return Objects.equals(address, other.address) && Objects.equals(bankAccount, other.bankAccount)
                && Objects.equals(creditCard, other.creditCard) && Objects.equals(name, other.name)
                && Objects.equals(paymentProfileId, other.paymentProfileId);
    }

}
