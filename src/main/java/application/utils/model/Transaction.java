package application.utils.model;

import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Transaction extends ResourceSupport {
    private String paymentId;
    private String transactionId;
    private String responseCode;
    private String authCode;
    private String accountNumber;
    private String accountType;
    private String avsCode;
    private String cvvCode;
    private String cavvCode;
    private String transactionMessage;
    private String customerProfileId;
    private String paymentProfileId;
    private String transactionTimeUTC;
    private String transactionTimeLocal;
    private String transactionType;
    private String transactionStatus;
    private String responseReasonDescription;
    private String batchId;
    private String settlementTimeUTC;
    private String settlementTimeLocal;
    private String settlementState;
    private Double authAmount;
    private Double settleAmount;
    private PaymentProfile paymentProfile;
    private Boolean recurringBilling;

    public Transaction() {
        super();
    }

    public Transaction(@JsonProperty("paymentId") String paymentId, @JsonProperty("transactionId") String transactionId,
            @JsonProperty("responseCode") String responseCode, @JsonProperty("authCode") String authCode,
            @JsonProperty("accountNumber") String accountNumber, @JsonProperty("accountType") String accountType,
            @JsonProperty("avsCode") String avsCode, @JsonProperty("cvvCode") String cvvCode,
            @JsonProperty("cavvCode") String cavvCode, @JsonProperty("transactionMessage") String transactionMessage,
            @JsonProperty("customerProfileId") String customerProfileId,
            @JsonProperty("paymentProfileId") String paymentProfileId,
            @JsonProperty("transactionTimeUTC") String transactionTimeUTC,
            @JsonProperty("transactionTimeLocal") String transactionTimeLocal,
            @JsonProperty("transactionType") String transactionType,
            @JsonProperty("transactionStatus") String transactionStatus,
            @JsonProperty("responseReasonDescription") String responseReasonDescription,
            @JsonProperty("batchId") String batchId, @JsonProperty("settlementTimeUTC") String settlementTimeUTC,
            @JsonProperty("settlementTimeLocal") String settlementTimeLocal,
            @JsonProperty("settlementState") String settlementState, @JsonProperty("authAmount") Double authAmount,
            @JsonProperty("settleAmount") Double settleAmount,
            @JsonProperty("paymentProfile") PaymentProfile paymentProfile,
            @JsonProperty("recurringBilling") Boolean recurringBilling) {
        super();
        this.paymentId = paymentId;
        this.transactionId = transactionId;
        this.responseCode = responseCode;
        this.authCode = authCode;
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.avsCode = avsCode;
        this.cvvCode = cvvCode;
        this.cavvCode = cavvCode;
        this.transactionMessage = transactionMessage;
        this.customerProfileId = customerProfileId;
        this.paymentProfileId = paymentProfileId;
        this.transactionTimeUTC = transactionTimeUTC;
        this.transactionTimeLocal = transactionTimeLocal;
        this.transactionType = transactionType;
        this.transactionStatus = transactionStatus;
        this.responseReasonDescription = responseReasonDescription;
        this.batchId = batchId;
        this.settlementTimeUTC = settlementTimeUTC;
        this.settlementTimeLocal = settlementTimeLocal;
        this.settlementState = settlementState;
        this.authAmount = authAmount;
        this.settleAmount = settleAmount;
        this.paymentProfile = paymentProfile;
        this.recurringBilling = recurringBilling;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAvsCode() {
        return avsCode;
    }

    public void setAvsCode(String avsCode) {
        this.avsCode = avsCode;
    }

    public String getCvvCode() {
        return cvvCode;
    }

    public void setCvvCode(String cvvCode) {
        this.cvvCode = cvvCode;
    }

    public String getCavvCode() {
        return cavvCode;
    }

    public void setCavvCode(String cavvCode) {
        this.cavvCode = cavvCode;
    }

    public String getTransactionMessage() {
        return transactionMessage;
    }

    public void setTransactionMessage(String transactionMessage) {
        this.transactionMessage = transactionMessage;
    }

    public String getCustomerProfileId() {
        return customerProfileId;
    }

    public void setCustomerProfileId(String customerProfileId) {
        this.customerProfileId = customerProfileId;
    }

    public String getPaymentProfileId() {
        return paymentProfileId;
    }

    public void setPaymentProfileId(String paymentProfileId) {
        this.paymentProfileId = paymentProfileId;
    }

    public String getTransactionTimeUTC() {
        return transactionTimeUTC;
    }

    public void setTransactionTimeUTC(String transactionTimeUTC) {
        this.transactionTimeUTC = transactionTimeUTC;
    }

    public String getTransactionTimeLocal() {
        return transactionTimeLocal;
    }

    public void setTransactionTimeLocal(String transactionTimeLocal) {
        this.transactionTimeLocal = transactionTimeLocal;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getResponseReasonDescription() {
        return responseReasonDescription;
    }

    public void setResponseReasonDescription(String responseReasonDescription) {
        this.responseReasonDescription = responseReasonDescription;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getSettlementTimeUTC() {
        return settlementTimeUTC;
    }

    public void setSettlementTimeUTC(String settlementTimeUTC) {
        this.settlementTimeUTC = settlementTimeUTC;
    }

    public String getSettlementTimeLocal() {
        return settlementTimeLocal;
    }

    public void setSettlementTimeLocal(String settlementTimeLocal) {
        this.settlementTimeLocal = settlementTimeLocal;
    }

    public String getSettlementState() {
        return settlementState;
    }

    public void setSettlementState(String settlementState) {
        this.settlementState = settlementState;
    }

    public Double getAuthAmount() {
        return authAmount;
    }

    public void setAuthAmount(Double authAmount) {
        this.authAmount = authAmount;
    }

    public Double getSettleAmount() {
        return settleAmount;
    }

    public void setSettleAmount(Double settleAmount) {
        this.settleAmount = settleAmount;
    }

    public PaymentProfile getPaymentProfile() {
        return paymentProfile;
    }

    public void setPaymentProfile(PaymentProfile paymentProfile) {
        this.paymentProfile = paymentProfile;
    }

    public Boolean getRecurringBilling() {
        return recurringBilling;
    }

    public void setRecurringBilling(Boolean recurringBilling) {
        this.recurringBilling = recurringBilling;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountNumber, accountType, authAmount, authCode, avsCode, batchId,
                cavvCode, customerProfileId, cvvCode, paymentId, paymentProfile, paymentProfileId, recurringBilling,
                responseCode, responseReasonDescription, settleAmount, settlementState, settlementTimeLocal,
                settlementTimeUTC, transactionId, transactionMessage, transactionStatus, transactionTimeLocal,
                transactionTimeUTC, transactionType);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Transaction other = (Transaction) obj;
        return Objects.equals(accountNumber, other.accountNumber) && Objects.equals(accountType, other.accountType)
                && Objects.equals(authAmount, other.authAmount) && Objects.equals(authCode, other.authCode)
                && Objects.equals(avsCode, other.avsCode) && Objects.equals(batchId, other.batchId)
                && Objects.equals(cavvCode, other.cavvCode)
                && Objects.equals(customerProfileId, other.customerProfileId) && Objects.equals(cvvCode, other.cvvCode)
                && Objects.equals(paymentId, other.paymentId) && Objects.equals(paymentProfile, other.paymentProfile)
                && Objects.equals(paymentProfileId, other.paymentProfileId)
                && Objects.equals(recurringBilling, other.recurringBilling)
                && Objects.equals(responseCode, other.responseCode)
                && Objects.equals(responseReasonDescription, other.responseReasonDescription)
                && Objects.equals(settleAmount, other.settleAmount)
                && Objects.equals(settlementState, other.settlementState)
                && Objects.equals(settlementTimeLocal, other.settlementTimeLocal)
                && Objects.equals(settlementTimeUTC, other.settlementTimeUTC)
                && Objects.equals(transactionId, other.transactionId)
                && Objects.equals(transactionMessage, other.transactionMessage)
                && Objects.equals(transactionStatus, other.transactionStatus)
                && Objects.equals(transactionTimeLocal, other.transactionTimeLocal)
                && Objects.equals(transactionTimeUTC, other.transactionTimeUTC)
                && Objects.equals(transactionType, other.transactionType);
    }

}
