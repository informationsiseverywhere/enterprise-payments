package application.utils.model;

import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import application.security.model.User;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Role extends ResourceSupport {
    private Long roleSequenceId;
    private String roleName;
    private String description;
    @JsonProperty("users")
    private List<User> userList;
    private Integer numberofUsers;
    private Integer numberofApplications;

    @JsonCreator
    public Role(@JsonProperty("roleSequenceId") Long roleSequenceId, @JsonProperty("roleName") String roleName,
            @JsonProperty("description") String description, @JsonProperty("users") List<User> userList,
            @JsonProperty("numberofUsers") Integer numberofUsers,
            @JsonProperty("numberofApplications") Integer numberofApplications) {
        super();
        this.roleSequenceId = roleSequenceId;
        this.roleName = roleName;
        this.description = description;
        this.userList = userList;
        this.numberofUsers = numberofUsers;
        this.numberofApplications = numberofApplications;
    }

    public Role() {
    }

    public Role(Role role) {
        this(role.getRoleSequenceId(), role.getRoleName(), role.getDescription(), role.getUserList(),
                role.getNumberofUsers(), role.getNumberofApplications());
    }

    public Long getRoleSequenceId() {
        return roleSequenceId;
    }

    public void setRoleSequenceId(Long roleSequenceId) {
        this.roleSequenceId = roleSequenceId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public Integer getNumberofUsers() {
        return numberofUsers;
    }

    public void setNumberofUsers(Integer numberofUsers) {
        this.numberofUsers = numberofUsers;
    }

    public Integer getNumberofApplications() {
        return numberofApplications;
    }

    public void setNumberofApplications(Integer numberofApplications) {
        this.numberofApplications = numberofApplications;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + Objects.hash(description, numberofApplications, numberofUsers, roleName, roleSequenceId, userList);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Role other = (Role) obj;
        return Objects.equals(description, other.description)
                && Objects.equals(numberofApplications, other.numberofApplications)
                && Objects.equals(numberofUsers, other.numberofUsers) && Objects.equals(roleName, other.roleName)
                && Objects.equals(roleSequenceId, other.roleSequenceId) && Objects.equals(userList, other.userList);
    }

}
