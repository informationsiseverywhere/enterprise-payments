package application.utils.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CreditCard {
    private String cardType;
    private String cardNumber;
    private String expirationDate;

    public CreditCard() {
        super();
    }

    public CreditCard(@JsonProperty("cardType") String cardType, @JsonProperty("cardNumber") String cardNumber,
            @JsonProperty("expirationDate") String expirationDate) {
        super();
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardNumber, cardType, expirationDate);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CreditCard other = (CreditCard) obj;
        return Objects.equals(cardNumber, other.cardNumber) && Objects.equals(cardType, other.cardType)
                && Objects.equals(expirationDate, other.expirationDate);
    }

}
