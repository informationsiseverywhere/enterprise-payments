package application.utils.model.embedded;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PolicySearch {

    private String policySymbol;
    private String policyNumber;
    private String insuredId;
    private String insuredName;
    private String insuredAddress;
    private Integer insuredAddressId;
    private List<PolicyTermSearch> terms;

    @JsonCreator
    public PolicySearch(@JsonProperty("policySymbol") String policySymbol,
            @JsonProperty("policyNumber") String policyNumber, @JsonProperty("insuredId") String insuredId,
            @JsonProperty("insuredName") String insuredName, @JsonProperty("insuredAddress") String insuredAddress,
            @JsonProperty("insuredAddressId") Integer insuredAddressId,
            @JsonProperty("terms") List<PolicyTermSearch> terms) {
        super();
        this.policySymbol = policySymbol;
        this.policyNumber = policyNumber;
        this.insuredId = insuredId;
        this.insuredName = insuredName;
        this.insuredAddress = insuredAddress;
        this.insuredAddressId = insuredAddressId;
        this.terms = terms;
    }

    public PolicySearch() {
        super();
    }

    public String getPolicySymbol() {
        return policySymbol;
    }

    public void setPolicySymbol(String policySymbol) {
        this.policySymbol = policySymbol;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getInsuredId() {
        return insuredId;
    }

    public void setInsuredId(String insuredId) {
        this.insuredId = insuredId;
    }

    public String getInsuredName() {
        return insuredName;
    }

    public void setInsuredName(String insuredName) {
        this.insuredName = insuredName;
    }

    public String getInsuredAddress() {
        return insuredAddress;
    }

    public void setInsuredAddress(String insuredAddress) {
        this.insuredAddress = insuredAddress;
    }

    public Integer getInsuredAddressId() {
        return insuredAddressId;
    }

    public void setInsuredAddressId(Integer insuredAddressId) {
        this.insuredAddressId = insuredAddressId;
    }

    public List<PolicyTermSearch> getTerms() {
        return terms;
    }

    public void setTerms(List<PolicyTermSearch> terms) {
        this.terms = terms;
    }

}
