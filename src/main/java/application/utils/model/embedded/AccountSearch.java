package application.utils.model.embedded;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AccountSearch extends ResourceSupport {
    private String accountId;
    private String accountNumber;
    private String type;
    private String payorId;
    private String payorName;
    private String payorAddress;
    private Integer payorAddressId;
    private String emailId;
    private String phoneNumber;
    private List<PolicySearch> policies;
    private String additionalId;
    private String parentAccountId;
    private String parentAccountNumber;
    private Integer accountSize;
    private String imageId;
    private List<String> agencies;
    private Date timestamp;
    private String subGroup;
    private String status;
    private String currency;
    private String businessGroup;

    @JsonCreator
    public AccountSearch(@JsonProperty("accountId") String accountId,
            @JsonProperty("accountNumber") String accountNumber, @JsonProperty("type") String type,
            @JsonProperty("payorId") String payorId, @JsonProperty("payorName") String payorName,
            @JsonProperty("payorAddress") String payorAddress, @JsonProperty("payorAddressId") Integer payorAddressId,
            @JsonProperty("emailId") String emailId, @JsonProperty("phoneNumber") String phoneNumber,
            @JsonProperty("policies") List<PolicySearch> policies, @JsonProperty("additionalId") String additionalId,
            @JsonProperty("parentAccountId") String parentAccountId,
            @JsonProperty("parentAccountNumber") String parentAccountNumber,
            @JsonProperty("accountSize") Integer accountSize, @JsonProperty("imageId") String imageId,
            @JsonProperty("agencies") List<String> agencies, @JsonProperty("subGroup") String subGroup,
            @JsonProperty("status") String status, @JsonProperty("currency") String currency,
            @JsonProperty("businessGroup") String businessGroup) {
        super();
        this.accountId = accountId;
        this.accountNumber = accountNumber;
        this.type = type;
        this.payorId = payorId;
        this.payorName = payorName;
        this.payorAddress = payorAddress;
        this.payorAddressId = payorAddressId;
        this.emailId = emailId;
        this.phoneNumber = phoneNumber;
        this.policies = policies;
        this.additionalId = additionalId;
        this.parentAccountId = parentAccountId;
        this.parentAccountNumber = parentAccountNumber;
        this.accountSize = accountSize;
        this.imageId = imageId;
        this.agencies = agencies;
        this.subGroup = subGroup;
        this.status = status;
        this.currency = currency;
        this.businessGroup = businessGroup;
    }

    public AccountSearch() {
        super();
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPayorId() {
        return payorId;
    }

    public void setPayorId(String payorId) {
        this.payorId = payorId;
    }

    public String getPayorName() {
        return payorName;
    }

    public void setPayorName(String payorName) {
        this.payorName = payorName;
    }

    public String getPayorAddress() {
        return payorAddress;
    }

    public void setPayorAddress(String payorAddress) {
        this.payorAddress = payorAddress;
    }

    public Integer getPayorAddressId() {
        return payorAddressId;
    }

    public void setPayorAddressId(Integer payorAddressId) {
        this.payorAddressId = payorAddressId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<PolicySearch> getPolicies() {
        return policies;
    }

    public void setPolicies(List<PolicySearch> policies) {
        this.policies = policies;
    }

    public String getAdditionalId() {
        return additionalId;
    }

    public void setAdditionalId(String additionalId) {
        this.additionalId = additionalId;
    }

    public String getParentAccountId() {
        return parentAccountId;
    }

    public void setParentAccountId(String parentAccountId) {
        this.parentAccountId = parentAccountId;
    }

    public String getParentAccountNumber() {
        return parentAccountNumber;
    }

    public void setParentAccountNumber(String parentAccountNumber) {
        this.parentAccountNumber = parentAccountNumber;
    }

    public Integer getAccountSize() {
        return accountSize;
    }

    public void setAccountSize(Integer accountSize) {
        this.accountSize = accountSize;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public List<String> getAgencies() {
        return agencies;
    }

    public void setAgencies(List<String> agencies) {
        this.agencies = agencies;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getSubGroup() {
        return subGroup;
    }

    public void setSubGroup(String subGroup) {
        this.subGroup = subGroup;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBusinessGroup() {
        return businessGroup;
    }

    public void setBusinessGroup(String businessGroup) {
        this.businessGroup = businessGroup;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(accountId, accountNumber, accountSize, additionalId, agencies,
                businessGroup, currency, emailId, imageId, parentAccountId, parentAccountNumber, payorAddress,
                payorAddressId, payorId, payorName, phoneNumber, policies, status, subGroup, timestamp, type);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AccountSearch other = (AccountSearch) obj;
        return Objects.equals(accountId, other.accountId) && Objects.equals(accountNumber, other.accountNumber)
                && Objects.equals(accountSize, other.accountSize) && Objects.equals(additionalId, other.additionalId)
                && Objects.equals(agencies, other.agencies) && Objects.equals(businessGroup, other.businessGroup)
                && Objects.equals(currency, other.currency) && Objects.equals(emailId, other.emailId)
                && Objects.equals(imageId, other.imageId) && Objects.equals(parentAccountId, other.parentAccountId)
                && Objects.equals(parentAccountNumber, other.parentAccountNumber)
                && Objects.equals(payorAddress, other.payorAddress)
                && Objects.equals(payorAddressId, other.payorAddressId) && Objects.equals(payorId, other.payorId)
                && Objects.equals(payorName, other.payorName) && Objects.equals(phoneNumber, other.phoneNumber)
                && Objects.equals(policies, other.policies) && Objects.equals(status, other.status)
                && Objects.equals(subGroup, other.subGroup) && Objects.equals(timestamp, other.timestamp)
                && Objects.equals(type, other.type);
    }

}
