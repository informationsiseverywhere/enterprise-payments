package application.utils.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BankAccount {
    private String accountType;
    private String routingNumber;
    private String accountNumber;
    private String nameOnAccount;
    private String bankName;

    public BankAccount(@JsonProperty("accountType") String accountType,
            @JsonProperty("routingNumber") String routingNumber, @JsonProperty("accountNumber") String accountNumber,
            @JsonProperty("nameOnAccount") String nameOnAccount, @JsonProperty("bankName") String bankName) {
        super();
        this.accountType = accountType;
        this.routingNumber = routingNumber;
        this.accountNumber = accountNumber;
        this.nameOnAccount = nameOnAccount;
        this.bankName = bankName;
    }

    public BankAccount() {
        super();
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getNameOnAccount() {
        return nameOnAccount;
    }

    public void setNameOnAccount(String nameOnAccount) {
        this.nameOnAccount = nameOnAccount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountNumber, accountType, bankName, nameOnAccount, routingNumber);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BankAccount other = (BankAccount) obj;
        return Objects.equals(accountNumber, other.accountNumber) && Objects.equals(accountType, other.accountType)
                && Objects.equals(bankName, other.bankName) && Objects.equals(nameOnAccount, other.nameOnAccount)
                && Objects.equals(routingNumber, other.routingNumber);
    }

}
