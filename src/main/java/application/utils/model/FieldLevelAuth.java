package application.utils.model;

import java.util.Objects;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FieldLevelAuth extends ResourceSupport {
    private String fieldAscType;
    private String fieldLevelId;
    private Double fieldAuthAmount;

    @JsonCreator
    public FieldLevelAuth(@JsonProperty("fieldAscType") String fieldAscType,
            @JsonProperty("fieldLevelId") String fieldLevelId,
            @JsonProperty("fieldAuthAmount") Double fieldAuthAmount) {
        super();
        this.fieldAscType = fieldAscType;
        this.fieldLevelId = fieldLevelId;
        this.fieldAuthAmount = fieldAuthAmount;
    }

    public FieldLevelAuth() {
    }

    public FieldLevelAuth(FieldLevelAuth field) {
        this(field.getFieldAscType(), field.getFieldLevelId(), field.getFieldAuthAmount());
    }

    public String getFieldAscType() {
        return fieldAscType;
    }

    public void setFieldAscType(String fieldAscType) {
        this.fieldAscType = fieldAscType;
    }

    public String getFieldLevelId() {
        return fieldLevelId;
    }

    public void setFieldLevelId(String fieldLevelId) {
        this.fieldLevelId = fieldLevelId;
    }

    public Double getFieldAuthAmount() {
        return fieldAuthAmount;
    }

    public void setFieldAuthAmount(Double fieldAuthAmount) {
        this.fieldAuthAmount = fieldAuthAmount;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(fieldAscType, fieldAuthAmount, fieldLevelId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        FieldLevelAuth other = (FieldLevelAuth) obj;
        return Objects.equals(fieldAscType, other.fieldAscType)
                && Objects.equals(fieldAuthAmount, other.fieldAuthAmount)
                && Objects.equals(fieldLevelId, other.fieldLevelId);
    }

}
