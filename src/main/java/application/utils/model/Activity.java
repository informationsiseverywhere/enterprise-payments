package application.utils.model;

import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import application.utils.model.embedded.Link;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Activity {
    private String activityId;
    private String templateId;
    private String activityTitle;
    private String group;
    private String priority;
    private String status;
    private String assignedId;
    private ZonedDateTime startDate;
    private ZonedDateTime completionDate;
    private Short effort;
    private Short activityEffort;
    private Short checklistEffort;
    private List<Link> links;

    public Activity() {
        super();
    }

    @JsonCreator
    public Activity(@JsonProperty("activityId") String activityId, @JsonProperty("templateId") String templateId,
            @JsonProperty("activityTitle") String activityTitle, @JsonProperty("group") String group,
            @JsonProperty("priority") String priority, @JsonProperty("status") String status,
            @JsonProperty("assignedId") String assignedId, @JsonProperty("startDate") ZonedDateTime startDate,
            @JsonProperty("completionDate") ZonedDateTime completionDate, @JsonProperty("effort") Short effort,
            @JsonProperty("activityEffort") Short activityEffort,
            @JsonProperty("checklistEffort") Short checklistEffort, @JsonProperty("liks") List<Link> links) {
        super();
        this.activityId = activityId;
        this.templateId = templateId;
        this.activityTitle = activityTitle;
        this.group = group;
        this.priority = priority;
        this.status = status;
        this.assignedId = assignedId;
        this.startDate = startDate;
        this.completionDate = completionDate;
        this.effort = effort;
        this.activityEffort = activityEffort;
        this.checklistEffort = checklistEffort;
        this.links = links;
    }

    public Activity(Activity activity) {
        this(activity.getActivityId(), activity.getTemplateId(), activity.getActivityTitle(), activity.getGroup(),
                activity.getPriority(), activity.getStatus(), activity.getAssignedId(), activity.getStartDate(),
                activity.getCompletionDate(), activity.getEffort(), activity.getActivityEffort(),
                activity.getChecklistEffort(), activity.getLinks());
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAssignedId() {
        return assignedId;
    }

    public void setAssignedId(String assignedId) {
        this.assignedId = assignedId;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(ZonedDateTime completionDate) {
        this.completionDate = completionDate;
    }

    public Short getEffort() {
        return effort;
    }

    public void setEffort(Short effort) {
        this.effort = effort;
    }

    public Short getActivityEffort() {
        return activityEffort;
    }

    public void setActivityEffort(Short activityEffort) {
        this.activityEffort = activityEffort;
    }

    public Short getChecklistEffort() {
        return checklistEffort;
    }

    public void setChecklistEffort(Short checklistEffort) {
        this.checklistEffort = checklistEffort;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
