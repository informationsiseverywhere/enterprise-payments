package application.utils.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

public interface JsonToPdfConverter {

    public ResponseEntity<InputStreamResource> convert(Object jsonObject, String xsltFileName, String pdfFileName)
            throws Exception;

}