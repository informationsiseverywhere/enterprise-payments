package application.utils.service;

import core.options.model.SupportData;

public interface MultiCurrencyService {

    public String getCurrencyCode(String currencyCode);

    public void validateCurrencyCode(String currencyCode);

    public void setSupportedCurrency(SupportData supportData);

    public boolean compareCurrency(String fromCurrency, String toCurrency);

    public String getCurrencySymbolByCode(String currencyCode);
}
