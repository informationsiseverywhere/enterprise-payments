package application.utils.service;

import application.utils.model.Activity;
import application.utils.model.Reference;

public interface ActivityRestService {

    public Activity save(Activity activity);

    public Reference save(Reference reference, Activity activity);
}
