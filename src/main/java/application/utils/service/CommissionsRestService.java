package application.utils.service;

import application.utils.model.AuthorizeOption;

public interface CommissionsRestService {

    public void save(AuthorizeOption authorizeOption);

}
