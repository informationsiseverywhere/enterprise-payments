package application.utils.service.impl;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import application.utils.model.embedded.AccountSearch;
import billing.utils.elasticsearch.ESClient;

@Service
public class AccountsElasticSearchServiceImpl {

    private static final Logger logger = LogManager.getLogger(AccountsElasticSearchServiceImpl.class);

    private static final String ES_ACCOUNT_INDEX = "accounts";

    @Autowired
    ESClient esClient;

    public AccountSearch fetchAccountDocument(String accountId) throws IOException {
        RestHighLevelClient client = esClient.getClient();
        AccountSearch accountSearch = new AccountSearch();
        ObjectMapper mapper = new ObjectMapper();
        GetRequest getRequest = new GetRequest().index(ES_ACCOUNT_INDEX).id(accountId);
        try {
            GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);
            String source = getResponse.getSourceAsString();
            if (source != null) {
                accountSearch = mapper.readValue(source, AccountSearch.class);
            }
        } catch (ElasticsearchException e) {
            logger.error(e);
        }
        return accountSearch;
    }
}
