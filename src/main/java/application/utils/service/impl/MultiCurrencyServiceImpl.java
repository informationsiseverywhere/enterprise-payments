package application.utils.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import application.security.data.entity.FieldAttribute;
import application.security.data.repository.FieldAttributeRepository;
import application.security.data.repository.FieldDataMapRepository;
import application.utils.service.MultiCurrencyService;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.options.model.SupportData;
import core.translation.data.entity.BusCdTranslation;
import core.translation.data.entity.id.BusCdTranslationId;
import core.translation.repository.BusCdTranslationRepository;

@Service
public class MultiCurrencyServiceImpl implements MultiCurrencyService {

    @Value("${multi.currency}")
    String multiCurrency;

    @Value("${currency.code}")
    String defaultCurrency;

    @Value("${appId.value}")
    private String appId;

    @Autowired
    private BusCdTranslationRepository busCdTranslationRepository;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private FieldAttributeRepository fieldAttributeRepository;

    @Autowired
    private FieldDataMapRepository fieldDataMapRepository;

    @Override
    public String getCurrencyCode(String currencyCode) {
        if (Boolean.TRUE.equals(Boolean.valueOf(multiCurrency))) {
            if (currencyCode != null && !currencyCode.trim().isEmpty()) {
                return currencyCode.trim();
            } else {
                if (defaultCurrency != null && !defaultCurrency.trim().isEmpty()) {
                    return defaultCurrency.trim();
                }
                throw new DataNotFoundException("default.currency.missed");
            }
        }
        return null;
    }

    @Override
    public void validateCurrencyCode(String currencyCode) {
        if (Boolean.TRUE.equals(Boolean.valueOf(multiCurrency)) && currencyCode != null
                && !currencyCode.trim().isEmpty()) {
            BusCdTranslation busCdTranslation = busCdTranslationRepository
                    .findById(new BusCdTranslationId(currencyCode, "Currency", execContext.getLanguage(), ""))
                    .orElse(null);
            if (busCdTranslation == null) {
                throw new DataNotFoundException("currency.invalid", new Object[] { currencyCode });
            }
        }
    }

    @Override
    public void setSupportedCurrency(SupportData supportData) {
        if (Boolean.TRUE.equals(Boolean.valueOf(multiCurrency))) {
            getRestrictedFields(supportData, "Currency");
        }
    }

    @Override
    public boolean compareCurrency(String fromCurrency, String toCurrency) {
        if (Boolean.TRUE.equals(Boolean.valueOf(multiCurrency))) {
            if (fromCurrency.equals(toCurrency)) {
                return Boolean.TRUE;
            } else if (fromCurrency.equals("") || fromCurrency.equals(defaultCurrency)) {
                return toCurrency.equals("") || toCurrency.equals(defaultCurrency);
            } else {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    @Override
    public String getCurrencySymbolByCode(String currencyCode) {
        String currencySymbol = currencyCode;
        if (Boolean.FALSE.equals(Boolean.valueOf(multiCurrency))) {
            currencyCode = "USD";
        } else {
            if (currencyCode == null || currencyCode.trim().isEmpty()) {
                currencyCode = defaultCurrency.trim();
            } else {
                currencySymbol = currencyCode.trim();
            }
        }
        Locale locale = LOCALE_CURRENCY_MAP.get(currencyCode.trim());
        if (locale != null) {
            Currency currency = Currency.getInstance(locale);
            currencySymbol = currency.getSymbol(locale);
        }
        return currencySymbol;
    }

    public static final Map<String, Locale> LOCALE_CURRENCY_MAP;
    static {
        final Map<String, Locale> m = new HashMap<>();

        m.put("USD", Locale.US);
        m.put("EUR", Locale.GERMANY);
        m.put("GBP", Locale.UK);
        m.put("JPY", Locale.JAPAN);

        LOCALE_CURRENCY_MAP = Collections.unmodifiableMap(m);
    }

    private SupportData getRestrictedFields(SupportData supportData, String codeType) {
        int roleId = Integer.parseInt(execContext.getUserRole());
        List<Integer> fieldIdList = getFieldId(codeType);
        if (fieldIdList.isEmpty()) {
            supportData.setPropertyValues(getPropertyValueList(codeType));
            return supportData;
        }
        List<Integer> fieldIdsList = getFieldIdListFieldAttribute(fieldIdList, roleId);
        if (fieldIdsList == null || fieldIdsList.isEmpty()) {
            supportData.setPropertyValues(getPropertyValueList(codeType));
            return supportData;
        }
        List<FieldAttribute> fieldAttributesList = null;
        Integer fieldId = fieldIdsList.get(0);
        fieldAttributesList = fieldAttributeRepository.findByFieldAttributeIdFieldIdAndRoleId(fieldId, roleId);
        if (fieldAttributesList == null || fieldAttributesList.isEmpty()) {
            fieldAttributesList = fieldAttributeRepository.findByFieldAttributeIdFieldIdAndRoleIdIsNull(fieldId);
        }
        if (fieldAttributesList == null || fieldAttributesList.isEmpty()) {
            supportData.setPropertyValues(getPropertyValueList(codeType));
            return supportData;
        }
        String defaultValue;
        List<String> includeList;
        List<String> defaultList = fieldAttributesList.stream()
                .filter(list -> list.getAttrType().trim().equalsIgnoreCase("DEFAULT"))
                .map(fieldAttribute -> fieldAttribute.getAttrValue().trim()).collect(Collectors.toList());

        if (defaultList != null && !defaultList.isEmpty()) {
            defaultValue = defaultList.get(0);
        } else {
            FieldAttribute fieldAttribute = fieldAttributeRepository
                    .findByFieldAttributeIdFieldIdAndAttrTypeAndRoleIdIsNull(fieldId, "DEFAULT");
            defaultValue = fieldAttribute != null ? fieldAttribute.getAttrValue() : null;
        }

        includeList = fieldAttributesList.stream().filter(list -> list.getAttrType().trim().equalsIgnoreCase("INCLUDE"))
                .map(fieldAttribute -> fieldAttribute.getAttrValue().trim()).collect(Collectors.toList());
        if (includeList == null || includeList.isEmpty()) {
            List<String> excludeList = fieldAttributesList.stream()
                    .filter(list -> list.getAttrType().trim().equalsIgnoreCase("EXCLUDE"))
                    .map(fieldAttribute -> fieldAttribute.getAttrValue().trim()).collect(Collectors.toList());

            List<String> supportDataList = getPropertyValueList(codeType);
            includeList = supportDataList.stream()
                    .filter(list -> (excludeList.stream().noneMatch(exclude -> exclude.equalsIgnoreCase(list.trim()))))
                    .map(String::trim).collect(Collectors.toList());
        }
        if (defaultValue != null && !includeList.contains(defaultValue)) {
            includeList.add(defaultValue);
            includeList = includeList.stream().sorted(Comparator.comparing(String::toLowerCase)).map(String::trim)
                    .collect(Collectors.toList());
        }
        supportData.setDefaultValue(defaultValue);
        supportData.setPropertyValues(includeList);

        return supportData;

    }

    private List<String> getPropertyValueList(String codeType) {
        return busCdTranslationRepository.findDescriptionListByType(codeType, execContext.getLanguage());
    }

    private List<Integer> getFieldId(String codeType) {
        return fieldDataMapRepository.findByCodeType(codeType, Long.parseLong(appId));
    }

    public List<Integer> getFieldIdListFieldAttribute(List<Integer> fieldIdList, Integer roleId) {
        List<Integer> fieldAttributesList = fieldAttributeRepository.findByFieldIdAndRoleId(fieldIdList, roleId);
        if (fieldAttributesList.isEmpty()) {
            fieldAttributesList = fieldAttributeRepository.findByFieldIdAndRoleIdIsNull(fieldIdList);
        }
        return fieldAttributesList;

    }
}
