package application.utils.service.impl;

import static core.utils.CommonConstants.SEPARATOR_BLANK;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import application.utils.model.FieldLevelAuth;
import application.utils.model.Role;
import application.utils.service.FieldLevelAuthenticationService;
import core.api.ExecContext;
import core.exception.database.DataNotFoundException;
import core.exception.validation.InvalidDataException;
import core.security.service.RestService;

@Primary
@Service
public class FieldLevelAuthenticationServiceImpl implements FieldLevelAuthenticationService {

    @Autowired
    private ExecContext execContext;

    @Value("${enterprise.utilities.url}")
    private String utilitiesResourceUrl;

    @Autowired
    private RestService restService;

    private static final String ROLE_RESOURCE = "/utils/users/{userSequenceId}/auth-roles";
    private static final String AUTH_FILED_LEVEL_RESOURCE = "/utils/roles/{roleSequenceId}/auth-field-levels/{fieldLevelId}";
    private static final String USER_SEQUENCE_ID = "userSequenceId";
    private static final String AUTH_FILED_LEVEL_RESOURCE_01 = "/utils/roles/{roleSequenceId}/auth-field-levels";
    private static final String ROLE_SEQUENCE_ID = "roleSequenceId";

    @SuppressWarnings("unchecked")
    public String getRole() {
        String userSequenceId = execContext.getUserSeqeunceId();
        String roleId = "";
        HttpHeaders httpHeaders = execContext.getHttpHeaders();
        if (httpHeaders != null && !httpHeaders.isEmpty()) {

            Map<String, Object> uriVariablesMap = new HashMap<>();
            uriVariablesMap.put(USER_SEQUENCE_ID, userSequenceId);
            restService.setHeaders(httpHeaders);
            restService.setURL(utilitiesResourceUrl + ROLE_RESOURCE);
            restService.setUriVariablesMap(uriVariablesMap);
            restService.setModel(Resources.class);
        }
        HttpEntity<Resources<Resource<Role>>> response = (HttpEntity<Resources<Resource<Role>>>) restService
                .getResource();
        if (response != null && response.getBody() != null) {
            ArrayList<Resource<Role>> rolesResources = new ArrayList<>(response.getBody().getContent());
            Map<String, String> roleResourceMap = (Map<String, String>) rolesResources.get(0);
            roleId = String.valueOf(roleResourceMap.get(ROLE_SEQUENCE_ID));
        }
        return roleId;
    }

    @SuppressWarnings("unchecked")
    private Double getAuthField(String roleId, String type) {
        Double fieldLevelAmount = null;
        HttpHeaders httpHeaders = execContext.getHttpHeaders();
        if (httpHeaders != null && !httpHeaders.isEmpty()) {

            Map<String, Object> uriVariablesMap = new HashMap<>();
            uriVariablesMap.put(ROLE_SEQUENCE_ID, roleId);
            uriVariablesMap.put("fieldLevelId", type);
            restService.setHeaders(httpHeaders);
            restService.setURL(utilitiesResourceUrl + AUTH_FILED_LEVEL_RESOURCE);
            restService.setUriVariablesMap(uriVariablesMap);
            restService.setModel(FieldLevelAuth.class);
        }
        HttpEntity<FieldLevelAuth> response = (HttpEntity<FieldLevelAuth>) restService.getResource();
        if (response != null && response.getBody() != null) {
            FieldLevelAuth fieldLevelAuthMap = response.getBody();
            fieldLevelAmount = fieldLevelAuthMap.getFieldAuthAmount();
        }
        return fieldLevelAmount;
    }

    @Override
    public boolean validateAmount(String type, double amount, String currency) {
        String roleId = getRole();
        if (roleId == null) {
            throw new DataNotFoundException("no.user-role.exists", new Object[] { execContext.getUserSeqeunceId() });
        } else {
            Double fieldLevelAmount = getAuthField(roleId, type);
            if (fieldLevelAmount == null) {
                throw new InvalidDataException("no.threshold.value.exists", new Object[] { type });
            } else if (fieldLevelAmount >= amount) {
                return true;
            } else {
                throw new InvalidDataException("thresholdAmount.exceeds",
                        new Object[] { amount, fieldLevelAmount, currency });
            }
        }
    }

    @Override
    public List<String> findAllAuthFieldLevels(String type) {
        List<String> fieldLevelAuths = new ArrayList<>();
        if (utilitiesResourceUrl == null || utilitiesResourceUrl.isEmpty()) {
            return fieldLevelAuths;
        }
        String roleId = getRole();

        if (roleId == null || roleId.isEmpty()) {
            throw new DataNotFoundException("no.user-role.exists", new Object[] { execContext.getUserSeqeunceId() });
        } else {
            fieldLevelAuths = findFieldAscType(roleId, type);
            if (fieldLevelAuths.isEmpty()) {
                throw new InvalidDataException("no.fieldAscType.value.exists", new Object[] { type });
            }
        }
        return fieldLevelAuths;
    }

    @SuppressWarnings("unchecked")
    private List<String> findFieldAscType(String roleId, String type) {
        List<String> fieldLevelAuths = new ArrayList<>();
        HttpHeaders httpHeaders = execContext.getHttpHeaders();
        if (httpHeaders != null && !httpHeaders.isEmpty()) {

            Map<String, Object> uriVariablesMap = new HashMap<>();
            uriVariablesMap.put(ROLE_SEQUENCE_ID, roleId);
            uriVariablesMap.put("fieldLevelId", type);
            restService.setHeaders(httpHeaders);
            restService.setURL(utilitiesResourceUrl + AUTH_FILED_LEVEL_RESOURCE_01 + "?query=" + type);
            restService.setUriVariablesMap(uriVariablesMap);
            restService.setModel(Resources.class);
        }
        HttpEntity<Resources<Resource<FieldLevelAuth>>> response = (HttpEntity<Resources<Resource<FieldLevelAuth>>>) restService
                .getResource();
        if (response != null && response.getBody() != null && response.getBody().getContent() != null) {
            ArrayList<Resource<FieldLevelAuth>> fieldLevelAuthResources = new ArrayList<>(
                    response.getBody().getContent());
            Iterator<Resource<FieldLevelAuth>> fieldLevelAuthItr = fieldLevelAuthResources.iterator();
            if (fieldLevelAuthItr != null) {
                do {
                    Map<String, String> fieldLevelAuthResourceMap = (Map<String, String>) fieldLevelAuthItr.next();
                    String[] types = fieldLevelAuthResourceMap.get("fieldAscType").split(SEPARATOR_BLANK, 2);
                    if (types.length > 1) {
                        fieldLevelAuths.add(types[1]);
                    }
                } while (fieldLevelAuthItr.hasNext());
            }
        }
        return fieldLevelAuths;
    }

}