package application.utils.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import application.utils.model.Activity;
import application.utils.model.Reference;
import application.utils.service.ActivityRestService;
import core.api.ExecContext;
import core.security.service.RestHeadersService;
import core.security.service.RestService;
import core.utils.CommonConstants;

@Service
public class ActivityRestServiceImpl implements ActivityRestService {

    @Value("${enterprise.activities.url}")
    private String activitiesResourceUrl;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private RestService restService;
    
    @Autowired
    private RestHeadersService restHeadersSerive;

    private static final String ACTIVITIES_RESOURCE = "/activities";
    private static final String REFERENCES_RESOURCE = "/references";
    
    private static final String IDENTIFIER_TYPE = "activities";

    @Override
    @SuppressWarnings("unchecked")
    public Activity save(Activity activity) {
        if (activitiesResourceUrl == null || activitiesResourceUrl.equals(CommonConstants.BLANK_STRING)) {
            return null;
        }
        HttpHeaders httpHeaders = null;
        if (execContext != null) {
            httpHeaders = execContext.getHttpHeaders();
        } else {
            httpHeaders = restHeadersSerive.getProperties(IDENTIFIER_TYPE);
        }
        if (httpHeaders != null && !httpHeaders.isEmpty()) {

            restService.setHeaders(httpHeaders);
            restService.setURL(activitiesResourceUrl + ACTIVITIES_RESOURCE);
            restService.setModel(Activity.class);
            restService.setEntity(activity);
        }
        ResponseEntity<Activity> postResponse = (ResponseEntity<Activity>) restService.postResource();
        return postResponse.getBody();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Reference save(Reference reference, Activity activity) {
        if (activitiesResourceUrl == null || activitiesResourceUrl.equals(CommonConstants.BLANK_STRING)) {
            return null;
        }
        HttpHeaders httpHeaders = execContext.getHttpHeaders();
        if (httpHeaders != null && !httpHeaders.isEmpty()) {

            restService.setHeaders(httpHeaders);
            restService.setURL(
                    activitiesResourceUrl + ACTIVITIES_RESOURCE + "/" + activity.getActivityId() + REFERENCES_RESOURCE);
            restService.setModel(Reference.class);
            restService.setEntity(reference);
        }
        ResponseEntity<Reference> postResponse = (ResponseEntity<Reference>) restService.postResource();
        return postResponse.getBody();
    }
}
