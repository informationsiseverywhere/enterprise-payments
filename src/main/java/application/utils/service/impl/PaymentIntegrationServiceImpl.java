package application.utils.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import application.utils.model.PaymentProfile;
import application.utils.model.Transaction;
import application.utils.service.PaymentIntegrationService;
import core.api.ExecContext;
import core.exception.application.ApplicationException;
import core.security.service.RestService;

@Service
public class PaymentIntegrationServiceImpl implements PaymentIntegrationService {

    @Value("${payment.gateway.url}")
    private String paymentIntegrationUrl;

    @Autowired
    private ExecContext execContext;

    @Autowired
    private RestService restService;

    private static final String PAYMENTS = "/payments/";

    @Override
    @SuppressWarnings("unchecked")
    public PaymentProfile getPaymentProfile(String paymentId) {
        if (paymentIntegrationUrl == null || paymentIntegrationUrl.trim().isEmpty()) {
            throw new ApplicationException("config.missing", new Object[] { "payment.gateway.url" });
        }
        HttpHeaders httpHeaders = execContext.getHttpHeaders();
        if (httpHeaders != null && !httpHeaders.isEmpty()) {

            restService.setHeaders(httpHeaders);
            restService.setURL(paymentIntegrationUrl + PAYMENTS + paymentId);
            restService.setModel(Transaction.class);
            try {
                HttpEntity<Transaction> response = (HttpEntity<Transaction>) restService.getResource();
                if (response != null && response.getBody() != null && response.getBody().getPaymentProfile() != null) {
                    return response.getBody().getPaymentProfile();
                }
            } catch (HttpClientErrorException e) {
                throw new ApplicationException("external.service.issue", e);
            } catch (Exception e) {
                throw new ApplicationException("connection.issue", new Object[] { "Payment Integration" }, e);
            }
        }
        return null;
    }

}
