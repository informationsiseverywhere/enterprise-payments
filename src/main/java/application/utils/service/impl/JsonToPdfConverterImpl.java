package application.utils.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopConfParser;
import org.apache.fop.apps.FopFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import application.utils.service.JsonToPdfConverter;

@Service
public class JsonToPdfConverterImpl implements JsonToPdfConverter {

    @Value("${pdf.generation.resource.path:}")
    private String resourceOverridePath;

    private static final Logger logger = LogManager.getLogger(JsonToPdfConverterImpl.class);
    private static final String FOP_CONFIG_FILE = "fopConfig.xml";
    private static final String FOP_RESOURCE_PATH = "pdfgen/";

    @Override
    public ResponseEntity<InputStreamResource> convert(Object jsonObject, String xsltFileName, String pdfFileName)
            throws Exception {

        logger.info("JsonToPdfConverter starting to generate file: " + pdfFileName);

        byte[] pdfBytes;

        String resourcePath = resourceOverridePath.trim();

        if (resourcePath.isEmpty()) {
            resourcePath = FOP_RESOURCE_PATH;

            // Use ClassLoader to get the folder with the resource files
            ClassLoader classLoader = JsonToPdfConverterImpl.class.getClassLoader();

            try (InputStream xsltStream = classLoader.getResourceAsStream(resourcePath + xsltFileName);
                    InputStream fopConfigStream = classLoader.getResourceAsStream(resourcePath + FOP_CONFIG_FILE);
                    ByteArrayOutputStream pdfOutStream = new ByteArrayOutputStream();) {
                URI baseUri = classLoader.getResource(resourcePath).toURI();
                pdfBytes = convertWithStreams(jsonObject, baseUri, xsltStream, fopConfigStream, pdfOutStream);
            }
        } else {
            // Use the specified absolute path for loading graphics and other
            // resource files
            resourcePath = resourcePath.replace("\\", "/");

            if (!resourcePath.endsWith("/")) {
                resourcePath = resourcePath + "/";
            }

            try (InputStream xsltStream = new FileInputStream(resourcePath + xsltFileName);
                    InputStream fopConfigStream = new FileInputStream(resourcePath + FOP_CONFIG_FILE);
                    ByteArrayOutputStream pdfOutStream = new ByteArrayOutputStream();) {
                URI baseUri = new URI("file", "/" + resourcePath, null);
                pdfBytes = convertWithStreams(jsonObject, baseUri, xsltStream, fopConfigStream, pdfOutStream);
            }
        }

        logger.info("JsonToPdfConverter finished generating file: " + pdfFileName);

        InputStream pdfStream = new ByteArrayInputStream(pdfBytes);
        InputStreamResource isResource = new InputStreamResource(pdfStream);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setContentLength(pdfBytes.length);
        headers.add("Content-Disposition", "attachment; filename=" + pdfFileName);

        return new ResponseEntity<InputStreamResource>(isResource, headers, HttpStatus.OK);
    }

    private byte[] convertWithStreams(Object jsonObject, URI baseUri, InputStream xsltStream,
            InputStream fopConfigStream, ByteArrayOutputStream pdfOutStream) throws Exception {

        // Translate Java object to XML
        String xmlString = new XmlMapper().writeValueAsString(jsonObject);

        // Translate XML to XSL-FO using XSLT file
        TransformerFactory xformFactory = TransformerFactory.newInstance();

        Transformer xsltTransformer = xformFactory.newTransformer(new StreamSource(xsltStream));

        Source xmlSource = new StreamSource(new StringReader(xmlString));
        Writer foWriter = new StringWriter();
        xsltTransformer.transform(xmlSource, new StreamResult(foWriter));

        // Translate XSL-FO to PDF using FOP
        Reader foReader = new StringReader(foWriter.toString());
        Source foSource = new StreamSource(foReader);

        FopConfParser fopConfParser = new FopConfParser(fopConfigStream, baseUri);
        FopFactory fopFactory = fopConfParser.getFopFactoryBuilder().build();

        Fop fop = fopFactory.newFop(org.apache.xmlgraphics.util.MimeConstants.MIME_PDF, pdfOutStream);
        Result saxResult = new SAXResult(fop.getDefaultHandler());

        // Do the XSLT transformation and FOP processing
        Transformer foTransformer = xformFactory.newTransformer();
        foTransformer.transform(foSource, saxResult);

        return pdfOutStream.toByteArray();
    }

}