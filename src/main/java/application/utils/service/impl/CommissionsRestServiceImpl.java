package application.utils.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import application.utils.model.AuthorizeOption;
import application.utils.service.CommissionsRestService;
import core.api.ExecContext;
import core.exception.application.ApplicationException;
import core.security.service.RestHeadersService;
import core.security.service.RestService;
import core.utils.CommonConstants;

@Service
public class CommissionsRestServiceImpl implements CommissionsRestService {

    @Value("${enterprise.commissions.url}")
    private String commissionsResourceUrl;

    @Autowired
    private ExecContext execContext;
    
    @Autowired
    private RestHeadersService restHeadersSerive;

    @Autowired
    private RestService restService;

    private static final String AUTHORIZE_COMMISSIONS = "/commissions/_authorize";
    
    private static final String IDENTIFIER_TYPE = "commissions";

    @Override
    public void save(AuthorizeOption authorizeOption) {
        if (commissionsResourceUrl == null || commissionsResourceUrl.equals(CommonConstants.BLANK_STRING)) {
            return;
        }
        HttpHeaders httpHeaders = null;
        if (execContext != null) {
            httpHeaders = execContext.getHttpHeaders();
        } else {
            httpHeaders = restHeadersSerive.getProperties(IDENTIFIER_TYPE);
        }
        
        if (httpHeaders != null && !httpHeaders.isEmpty()) {
            restService.setHeaders(httpHeaders);
            restService.setURL(commissionsResourceUrl + AUTHORIZE_COMMISSIONS);
            restService.setModel(AuthorizeOption.class);
            restService.setEntity(authorizeOption);
            try {
                restService.postResource();
            } catch (HttpClientErrorException ex) {
                throw new ApplicationException("external.service.issue", ex);
            } catch (Exception ex) {
                throw new ApplicationException("connection.issue", new Object[] { "Commissions" }, ex);
            }
        }
    }

}
