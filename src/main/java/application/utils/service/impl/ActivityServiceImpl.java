package application.utils.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import application.utils.model.Activity;
import application.utils.model.Reference;
import application.utils.service.ActivityRestService;
import application.utils.service.ActivityService;

@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityRestService restService;

    @Override
    public void postActivity(String activityTemplateId, String referenceType, String referenceNumber,
            String referenceKey) {
        Activity activity = new Activity();
        activity.setTemplateId(activityTemplateId);
        Reference reference = new Reference();
        reference.setType(referenceType);
        reference.setReference(referenceNumber);
        reference.setReferenceKey(referenceKey);
        Activity savedActivity = restService.save(activity);
        if (savedActivity != null) {
            restService.save(reference, savedActivity);
        }
    }
}
