package application.utils.service;

public interface ActivityService {

    public void postActivity(String activityTemplateId, String referenceType, String referenceNumber,
            String referenceKey);
}
