package application.utils.service;

import application.utils.model.PaymentProfile;

public interface PaymentIntegrationService {

    public PaymentProfile getPaymentProfile(String paymentId);

}
