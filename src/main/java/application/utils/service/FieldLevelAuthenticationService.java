package application.utils.service;

import java.util.List;

public interface FieldLevelAuthenticationService {
    
    public boolean validateAmount(String type, double reconAmount, String currency);

    public List<String> findAllAuthFieldLevels(String type);

}
