From java:7
MAINTAINER Pallav Boonlia (pboonlia@csc.com) 
WORKDIR /apps
ADD     ./    /apps
EXPOSE  8080
CMD ["./gradlew" ,"jettyRun" , "-DenvDB=hsql"]